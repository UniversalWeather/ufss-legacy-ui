﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="FBOCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Logistics.FBOCatalog"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" ValidateRequest="false" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<%@ Import Namespace="FlightPak.Web.Framework.Constants" %>
<%@ Register TagPrefix="uc" TagName="DatePicker" Src="~/UserControls/UCDate.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript">

        function chkAirToGroundForARINC() {
            var aritogrou = document.getElementById("<%=tbAirToGnd.ClientID%>").value;
            var unicom = document.getElementById("<%=tbUnicom.ClientID%>").value;
            var cvlbAIRINC = $("#<%=REFtbARINC.ClientID%>").css('display');
            if (cvlbAIRINC == "none") {
                if (aritogrou == null || aritogrou == '' || unicom == aritogrou) {
                    document.getElementById("<%=tbAirToGnd.ClientID%>").value = document.getElementById("<%=tbARINC.ClientID%>").value;
                }
            }
        }

        function chkAirToGroundForUNICOM() {
            var aritogrou = document.getElementById("<%=tbAirToGnd.ClientID%>").value;
            var arinc = document.getElementById("<%=tbARINC.ClientID%>").value;
            if (aritogrou == null || aritogrou == '') {
                if(arinc == '' || arinc == null)
                document.getElementById("<%=tbAirToGnd.ClientID%>").value = document.getElementById("<%=tbUnicom.ClientID%>").value;
            }
        }

        function validateEmptyRadRateTextbox(sender, eventArgs) {
            if (sender.get_textBoxValue().length < 1) {
                sender.set_value('0.0');
            }
        }

        //This function is used to replace default value when textbox is empty
        function ValidateEmptyTextbox(ctrlID, e) {
            if (ctrlID.value == "") {
                ctrlID.value = "00.0000";
            }
        }
        //this function is used to replace default value when textbox is empty
        function ValidateEmptyTextbox1(ctrlID, e) {
            if (ctrlID.value == "") {
                ctrlID.value = "00000.0000";
            }
        }
        //this function is used to replace default value when textbox is empty
        function ValidateEmptyTextbox2(ctrlID, e) {
            if (ctrlID.value == "") {
                ctrlID.value = "00000000000.00";
            }
        }
        //this function is used to expand the panel on click
        function OnClientClick(strPanelToExpand) {
            var PanelBar1 = $find("<%= pnlNotes.ClientID %>");
            var PanelBar2 = $find("<%= pnlMaintenance.ClientID %>");
            PanelBar1.get_items().getItem(0).set_expanded(false);
            PanelBar2.get_items().getItem(0).set_expanded(false);
            if (strPanelToExpand == "Notes") {
                PanelBar1.get_items().getItem(0).set_expanded(true);
            }
            else if (strPanelToExpand == "Maintenance") {
                PanelBar2.get_items().getItem(0).set_expanded(true);
            }
            return false;
        }
        
    </script>
    <script type="text/javascript">
        // To Delete record in a database,Confirming that it is not UWA record
        function deleteFBORecord() {
            var masterTable = $find('<%= dgFBO.ClientID %>').get_masterTableView();
            var msg = 'Are you sure you want to delete this record?';
            
            var oManager = $find("<%= RadWindowManager1.ClientID %>");
            if (masterTable.get_selectedItems().length > 0) {
                var cell1 = masterTable.getCellByColumnUniqueName(masterTable.get_selectedItems()[0], "chkUWAID");
                if (cell1.innerHTML == "UWA") {
                    oManager.radalert('You Cant Delete UWA Record', 330, 100, "Delete", ""); 
                    return false;
                }
                else {
                    oManager.radconfirm(msg, callBackFn, 330, 100, '', 'Delete'); 
                    return false;
                }
            }
            else {
                //If no records are selected 
                oManager.radalert('Please select a record from the above table.', 330, 100, "Delete", ""); 
                return false;
            }
        }
        function callBackFn(confirmed) {
            if (confirmed) {
                var grid = $find('<%= dgFBO.ClientID %>');
                grid.get_masterTableView().fireCommand("DeleteSelected");
            }
        }
    </script>
    <script runat="server">
        // setting the properties for controls for navigation
        public string IcaoID
        {
            get
            {
                return tbIcao.Text;
            }
        }
        public string address
        {
            get
            {
                return tbAddr1.Text;
            }
        }
        public string city
        {
            get
            {
                return tbCity.Text;
            }
        }
        public string state
        {
            get
            {
                return tbState.Text;
            }
        }
        public string zipcode
        {
            get
            {
                return tbPostal.Text;
            }
        }
        //this event is used to assign  values of controls to the navigating url
        void lbtnAirportQueryString_Click(object sender, EventArgs e)
        {
            //string URL = "../Airports/AirportCatalog.aspx?Screen=Airport&IcaoID=" + hdnAirportId.Value; Response.Redirect(URL);
            string URL = "../Airports/AirportCatalog.aspx?Screen=Airport"; Response.Redirect(URL);
        }
        void lbtnMapQueryString_Click(object sender, EventArgs e)
        {
            if (dgFBO.Items.Count > 0)
            {
                RedirectToPage(string.Format("{0}?address={1}&city={2}&state={3}&zipcode={4}&country={5}&redirect=true", WebConstants.URL.MapQuest, Microsoft.Security.Application.Encoder.HtmlEncode(tbAddr1.Text), Microsoft.Security.Application.Encoder.HtmlEncode(tbCity.Text), Microsoft.Security.Application.Encoder.HtmlEncode(tbState.Text), Microsoft.Security.Application.Encoder.HtmlEncode(tbPostal.Text), Microsoft.Security.Application.Encoder.HtmlEncode(tbCountry.Text)));
            }

        }

        public string CountryName
        {
            get
            {
                return tbCountry.Text;
            }
        }
        public string AirportName
        {
            get
            {
                return tbAirport.Text;
            }
        }
        //this event is used to assign  values of controls to the navigating url
        void lbtnTransportQueryString_Click(object sender, EventArgs e)
        {
            RedirectToPage(string.Format("{0}?Screen=Airport&IcaoID={1}&CityName={2}&StateName={3}&CountryName={4}&AirportName={5}&AirportID={6}", WebConstants.URL.TransportCatalog, Microsoft.Security.Application.Encoder.UrlEncode(tbIcao.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbFBOCity.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbFBOState.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbFBOCountry.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbAirport.Text), Microsoft.Security.Application.Encoder.UrlEncode(hdnAirportId.Value)));
        }
        //this event is used to assign  values of controls to the navigating url
        void lbtnCateringQueryString_Click(object sender, EventArgs e)
        {
            RedirectToPage(string.Format("{0}?Screen=Airport&IcaoID={1}&CityName={2}&StateName={3}&CountryName={4}&AirportName={5}&AirportID={6}", WebConstants.URL.CateringCatalog, Microsoft.Security.Application.Encoder.UrlEncode(tbIcao.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbFBOCity.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbFBOState.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbFBOCountry.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbAirport.Text), Microsoft.Security.Application.Encoder.UrlEncode(hdnAirportId.Value)));
        }
        //this event is used to assign  values of controls to the navigating url
        void lbtnHotelQueryString_Click(object sender, EventArgs e)
        {
            RedirectToPage(string.Format("{0}?Screen=Airport&IcaoID={1}&CityName={2}&StateName={3}&CountryName={4}&AirportName={5}&AirportID={6}", WebConstants.URL.HotelCatalog, Microsoft.Security.Application.Encoder.UrlEncode(tbIcao.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbFBOCity.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbFBOState.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbFBOCountry.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbAirport.Text), Microsoft.Security.Application.Encoder.UrlEncode(hdnAirportId.Value)));
        }
        //this event is used to assign  values of controls to the navigating url
        //void lbtnFboQueryString_Click(object sender, EventArgs e)
        //{
        //    string url = "../Logistics/FBOCatalog.aspx?Screen=Airport&IcaoID=" + tbIcao.Text + "&CityName=" + tbCity.Text + "&StateName=" + tbState.Text + "&CountryName=" + tbCountry.Text.Trim() + "&AirportName=" + tbAirport.Text + "&AirportID=" + hdnAirportId.Value; Response.Redirect(url);
        //}   
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">         
    <telerik:radajaxmanager id="RadAjaxManager1" runat="server" onajaxsettingcreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix"   />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgFBO">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:radajaxmanager>
    <telerik:radajaxloadingpanel id="RadAjaxLoadingPanel1" runat="server" skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadCountryMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseCountryPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaymentTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClosePaymentTypePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/PaymentTypePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaymentTypeCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                Top="0px" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadExchangeMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseExchangeRatePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ExchangeRateMasterpopup.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <telerik:radcodeblock id="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            //this function is used to navigate to pop up screen's with the selected code
            function openWin(radWin) {
                var url = '';
                if (radWin == "RadCountryPopup") {
                    url = '../Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('<%=tbCountry.ClientID%>').value;
                }
                else if (radWin == "radPaymentTypePopup") {
                    url = '../Company/PaymentTypePopup.aspx?PaymentTypeCD=' + document.getElementById("<%=tbPaymentType.ClientID%>").value;
                }
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, radWin);
            }
            //this function is used to display the value of selected country code from popup
            function OnClientCloseCountryPopup(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCountry.ClientID%>").value = arg.CountryCD;
                        document.getElementById("<%=hdnCountryId.ClientID%>").value = arg.CountryID;
                        document.getElementById("<%=cvCountry.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbCountry.ClientID%>").value = "";
                        ocument.getElementById("<%=hdnCountryId.ClientID %>>").value = "";
                        combo.clearSelection();
                    }
                }
            }




            //this function is used to display the value of selected Payment code from popup
            function OnClientCloseExchangeRatePopup(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbExchangeRate.ClientID%>").value = arg.ExchangeRateCD;
                        document.getElementById("<%=cvExchangeRate.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbExchangeRate.ClientID%>").value = "";
                        document.getElementById("<%=cvExchangeRate.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }

            //this function is used to display the value of selected Payment code from popup
            function OnClientClosePaymentTypePopup(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbPaymentType.ClientID%>").value = arg.PaymentTypeCD;
                        document.getElementById("<%=cvPaymentType.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbPaymentType.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            //this function is used to get dimension
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            //this function is used to get radwindow frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            //Rad Date Picker
            var currentTextBox = null;
            var currentDatePicker = null;

            function CheckFutureDate(sender, args) {
                var ReturnValue = false;
                var objLastPurchage = document.forms[0].ctl00$ctl00$MainContent$SettingBodyContent$pnlMaintenance$i0$i0$ucLastPurchase$tbDate;
                var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                    if (shouldSubmit) {
                        this.click();
                        ReturnValue = false;
                    }
                    else {
                        objLastPurchage.value = "";
                        objLastPurchage.focus();
                        ReturnValue = false;
                    }
                });
                var newformat = "<%= Microsoft.Security.Application.Encoder.HtmlEncode(DateFormat) %>";
                
                if (newformat == "") {
                    newformat = "MM/dd/yyyy";
                }

                var LastPurchase = ""
                if (objLastPurchage != null && objLastPurchage.value != null) {
                    LastPurchase = objLastPurchage.value;
                }
                LastPurchase = LastPurchase.replace(/\s+/g, "");
                //                if (LastPurchase != "") {
                //                    LastPurchase = new Date(LastPurchase);
                //                    LastPurchase = LastPurchase.format(newformat);
                //                }
                var CurrentDate = new Date();
                CurrentDate = CurrentDate.format(newformat);
                var Msg = "Last Purchase Date Warning - Was a future date entered correctly?";
                if (Date.parse(LastPurchase) > Date.parse(CurrentDate)) {
                    
                    var oManager = $find("<%= RadWindowManager1.ClientID %>");
                    oManager.radconfirm(Msg, callBackFunction, 400, 100, null, "Airport - FBO");
                    args.set_cancel(true);
                }
                else {
                    ReturnValue = true;
                }
                return ReturnValue;
            }
            function LinkToMapQuest() {
                var ReturnValue = false;
                var grid = $find("<%=dgFBO.ClientID %>").get_masterTableView();
                var length = grid.get_selectedItems().length;
                var tbAddr1 = document.getElementById("<%=tbAddr1.ClientID%>").value;
                var tbCity = document.getElementById("<%=tbCity.ClientID%>").value;
                var tbState = document.getElementById("<%=tbState.ClientID%>").value;
                var tbPostal = document.getElementById("<%=tbPostal.ClientID%>").value;
                var tbCountry = document.getElementById("<%=tbCountry.ClientID%>").value;
                if (length > 0) {
                    var URL = "http://www.mapquest.com/maps?address=" + encodeURIComponent(tbAddr1) + "&city=" + tbCity + "&state=" + tbState + "&zipcode=" + tbPostal + "&country=" + tbCountry + "&redirect=true";
                    window.open(URL);
                }
                return ReturnValue;
            }

            function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            }
            
            $(document).ready(function () {

               
                UpdateConfirm();

            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:radcodeblock>
    <%-- <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" DateFormat="MM/dd/yyyy" runat="server">
        </DateInput>
    </telerik:RadDatePicker>--%>
    <table width="100%" cellpadding="0" cellspacing="0" runat="server"
        id="table1">
        <tr>
            <td>
                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left">
                            <div class="tab-nav-top">
                                <span class="head-title">
                                    <asp:LinkButton ID="lbtnAirport" Text="Airport" runat="server" OnClick="lbtnAirportQueryString_Click"
                                        OnClientClick="document.forms[0].target = '_self';"></asp:LinkButton>&nbsp;>
                                    FBO</span><span class="tab-nav-icons"><!--<a href="#" class="search-icon"></a><a href="#" class="save-icon">
                    </a><a href="#" class="print-icon"></a>--><a href="#" class="help-icon"></a></span>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" width="100%" class="sticky" runat="server"
        id="table2">
        <tr style="display: none;">
            <td>
                <uc:DatePicker ID="ucDatePicker" runat="server"></uc:DatePicker>
            </td>
        </tr>
        <tr>
            <td class="tdLabel40">
                ICAO
            </td>
            <td class="tdLabel60">
                <asp:TextBox ID="tbIcao" runat="server" Enabled="false" CssClass="readonly-text40"></asp:TextBox>
                <asp:HiddenField ID="hdnAirportId" runat="server" />
            </td>
            <td class="tdLabel35">
                City
            </td>
            <td class="tdLabel150">
                <asp:TextBox ID="tbFBOCity" runat="server" Enabled="false" CssClass="readonly-text100"></asp:TextBox>
            </td>
            <td class="tdLabel45">
                State
            </td>
            <td class="tdLabel150">
                <asp:TextBox ID="tbFBOState" runat="server" Enabled="false" CssClass="readonly-text100"></asp:TextBox>
            </td>
            <td class="tdLabel60">
                Country
            </td>
            <td class="tdLabel60">
                <asp:TextBox ID="tbFBOCountry" runat="server" Enabled="false" CssClass="readonly-text30"></asp:TextBox>
            </td>
            <td class="tdLabel50">
                Airport
            </td>
            <td>
                <asp:TextBox ID="tbAirport" runat="server" Enabled="false" CssClass="readonly-text120"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" class="head-sub-menu" id="table4" runat="server">
        <tr>
            <td>
                <div class="tags_select">
                    <%--<asp:LinkButton ID="lbtnFbo" Text="FBO" runat="server" OnClick="lbtnFboQueryString_Click"></asp:LinkButton>--%>
                    <asp:LinkButton ID="lbtnHotel" Text="Hotel" runat="server" OnClick="lbtnHotelQueryString_Click"></asp:LinkButton>
                    <asp:LinkButton ID="lbtnTransport" Text="Transportation" runat="server" OnClick="lbtnTransportQueryString_Click"></asp:LinkButton>
                    <asp:LinkButton ID="lbtnCatering" Text="Catering" runat="server" OnClick="lbtnCateringQueryString_Click"></asp:LinkButton>
                </div>
            </td>
        </tr>
    </table>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
    <div id="divInactive" runat="server">
        <table cellpadding="0" cellspacing="0" class="head-sub-menu" runat="server" id="table3">
            <tr>
                <td class="tdLabel120" align="left">
                    <asp:CheckBox ID="chkDisplayInctive" runat="server" Text="Display Inactive" OnCheckedChanged="chkDisplayInctive_CheckedChanged"
                        AutoPostBack="true" />
                </td>
                <td align="left" class="tdLabel100">
                    <asp:CheckBox ID="chkDisplayChoiceOnly" runat="server" Text="Choice Only" OnCheckedChanged="chkDisplayInctive_CheckedChanged"
                        AutoPostBack="true" />
                </td>
                <td align="left" class="tdLabel100">
                    <asp:CheckBox ID="chkDisplayUVOnly" runat="server" Text="UVAir Only" OnCheckedChanged="chkDisplayInctive_CheckedChanged"
                        AutoPostBack="true" />
                </td>
                <td align="right">
                    <%--<asp:LinkButton runat="server" ID="lnkMap" OnClick="lbtnMapQueryString_Click" CssClass="map-icon"
                                ToolTip="View Map" OnClientClick="document.forms[0].target = '_blank';" Style="display: none;" />--%>
                    <asp:LinkButton runat="server" ID="lnkMapQuest" CssClass="map-icon" ToolTip="View Map"
                        OnClientClick="javascript:LinkToMapQuest();" />
                </td>
            </tr>
        </table>
    </div>
    <!--  checkUWAID HeaderStyle-Width="50px" fix for misalignment --> 


        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <telerik:radgrid id="dgFBO" runat="server" allowsorting="true" onitemcreated="dgFBO_ItemCreated"
                onneeddatasource="dgFBO_BindData" onitemcommand="dgFBO_ItemCommand" onupdatecommand="dgFBO_UpdateCommand"
                oninsertcommand="dgFBO_InsertCommand" ondeletecommand="dgFBO_DeleteCommand" visible="true"
                onitemdatabound="dgFBO_ItemDataBound" autogeneratecolumns="false" pagesize="10"
                height="341px" allowpaging="true" onselectedindexchanged="dgFBO_SelectedIndexChanged"
                allowfilteringbycolumn="true" onprerender="dgFBO_PreRender" pagerstyle-alwaysvisible="true"
                onpageindexchanged="dgFBO_PageIndexChanged">
                <MasterTableView DataKeyNames="FBOID,AirportID,FBOCD,FBOCD,FBOVendor,PostedPrice,IsChoice,Addr1,Addr2,PhoneNUM1,PhoneNUM2,PostalZipCD,StateName,ExchangeRateID,CustomField1,CustomField2,NegotiatedTerms,SundayWorkHours,MondayWorkHours,TuesdayWorkHours,WednesdayWorkHours,ThursdayWorkHours,FridayWorkHours,SaturdayWorkHours,
        CityName,Frequency,CountryCD,FaxNum,FuelBrand,HoursOfOperation,FuelQty,ARINC,SITA,UNICOM,PaymentType,EmailAddress,Website,Contact,Filter,
        NegotiatedFuelPrice,LastFuelDT,LastFuelPrice,LastUpdUID,IsCrewCar,TollFreePhoneNum,LastUpdTS,IsInActive,IsUVAirPNR,IsUWAMaintFlag,UWAID,IsDeleted,CustomerID,CountryID,ContactBusinessPhone,ContactCellPhoneNum,ContactEmail,Addr3,Remarks,IsUWAAirPartner"
                    CommandItemDisplay="Bottom" ClientDataKeyNames="UWAID" Width="100%">
                    <Columns>
                        <telerik:GridBoundColumn DataField="FBOCD" HeaderText="FBO Code" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="50px" FilterDelay="500"
                            FilterControlWidth="30px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FBOID" HeaderText="FBOID" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="EqualTo" Display="false" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsChoice" HeaderText="Choice" AutoPostBackOnFilter="true"
                            HeaderStyle-Width="60px" ShowFilterIcon="false" AllowFiltering="false">
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridBoundColumn DataField="FBOVendor" HeaderText="Name" AutoPostBackOnFilter="false"
                            HeaderStyle-Width="150px" FilterControlWidth="130px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PhoneNUM1" HeaderText="Business Phone" AutoPostBackOnFilter="false"
                            HeaderStyle-Width="100px" FilterControlWidth="80px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Frequency" HeaderText="Air-To-Gnd" AutoPostBackOnFilter="false"
                            HeaderStyle-Width="80px" FilterControlWidth="60px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FuelBrand" HeaderText="Fuel Brand" AutoPostBackOnFilter="false"
                            HeaderStyle-Width="80px" FilterControlWidth="60px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="NegotiatedFuelPrice" HeaderText="Negotiated Price"
                            HeaderStyle-Width="80px" FilterControlWidth="60px" AutoPostBackOnFilter="false" FilterDelay="500"
                            ShowFilterIcon="false" CurrentFilterFunction="EqualTo">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" ShowFilterIcon="false"
                            AutoPostBackOnFilter="true" HeaderStyle-Width="60px" AllowFiltering="false">
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridCheckBoxColumn DataField="IsUWAAirPartner" HeaderText="UVAir" AutoPostBackOnFilter="true"
                            HeaderStyle-Width="50px" FilterControlWidth="30px" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                            AllowFiltering="false">
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridBoundColumn HeaderText="Filter" DataField="Filter" UniqueName="chkUWAID"
                            AutoPostBackOnFilter="false"  FilterControlWidth="30px" FilterDelay="500"
                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AirportID" HeaderText="AirportID" CurrentFilterFunction="EqualTo"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CustomerID" HeaderText="CustomerID" CurrentFilterFunction="EqualTo"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Addr1" HeaderText="Addr1" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Addr2" HeaderText="Addr2" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CityName" HeaderText="CityName" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="StateName" HeaderText="StateName" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PostalZipCD" HeaderText="PostalZipCD" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CountryCD" HeaderText="CountryCD" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PhoneNUM2" HeaderText="PhoneNUM2" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="TollFreePhoneNum" HeaderText="TollFreePhoneNum"
                            CurrentFilterFunction="StartsWith" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FaxNum" HeaderText="FaxNum" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Website" HeaderText="Web site" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="EmailAddress" HeaderText="E-mail Address" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Contact" HeaderText="Contact" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="HoursOfOperation" HeaderText="HoursOfOperation"
                            CurrentFilterFunction="StartsWith" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="IsCrewCar" HeaderText="IsCrewCar" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="SITA" HeaderText="SITA" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="UNICOM" HeaderText="UNICOM" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ARINC" HeaderText="ARINC" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FuelQty" HeaderText="FuelQty" CurrentFilterFunction="EqualTo"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PaymentType" HeaderText="PaymentType" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LastFuelPrice" HeaderText="LastFuelPrice" CurrentFilterFunction="EqualTo"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PostedPrice" HeaderText="PostedPrice" CurrentFilterFunction="EqualTo"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LastFuelDT" HeaderText="LastFuelDT" CurrentFilterFunction="EqualTo"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LastUpdUID" HeaderText="LastUpdUID" CurrentFilterFunction="StartsWith"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LastUpdTS" HeaderText="LastUpdTS" CurrentFilterFunction="EqualTo"
                            Display="false">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div style="padding: 5px 5px; float: left; clear: both;">
                            <asp:LinkButton ID="lbtnInitInsert" runat="server" Visible='<%# IsAuthorized(Permission.Database.AddFBO)%>'
                                ToolTip="Add" CommandName="InitInsert"><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                ToolTip="Edit" CommandName="Edit" Visible='<%# IsAuthorized(Permission.Database.EditFBO)%>'><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lnkCopy" OnClick="lnkCopy_Click" runat="server" OnClientClick="javascript:return ProcessCopy();"
                                ToolTip="Copy">
                    <img style="border:0px;vertical-align:middle;" alt="Copy" src="<%=ResolveClientUrl("~/App_Themes/Default/images/copy.png") %>"/></asp:LinkButton>
                            <asp:LinkButton ID="lbtnInitDelete" OnClientClick="javascript:return deleteFBORecord();"
                                Visible='<%# IsAuthorized(Permission.Database.DeleteFBO)%>' runat="server" CommandName="DeleteSelected"
                                ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label></div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:radgrid>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="right">
                        <table cellpadding="0" cellspacing="0" class="tblButtonArea-nw">
                            <tr>
                                <td>
                                    <asp:Button ID="btnNotes" runat="server" CssClass="ui_nav" Text="Notes" OnClientClick="javascript:OnClientClick('Notes');return false;" />
                                </td>
                                <td class="custom_radbutton">
                                    <telerik:radbutton id="btnSaveChangesTop" runat="server" text="Save" validationgroup="Save"
                                        onclick="SaveChanges_Click" onclientclicking="CheckFutureDate" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancelTop" runat="server" CssClass="button" Text="Cancel" OnClick="Cancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td>
                        <telerik:radpanelbar id="pnlMaintenance" width="100%" expandanimation-type="none"
                            collapseanimation-type="none" runat="server">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="true" Text="Main Information">
                                    <Items>
                                        <telerik:RadPanelItem>
                                            <ContentTemplate>
                                                <table width="100%" class="box1">
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                <tr>
                                                                    <td class="tdLabel80">
                                                                        <asp:CheckBox ID="chkChoice" runat="server" Text="Choice" />
                                                                    </td>
                                                                    <td class="tdLabel90">
                                                                        <asp:CheckBox ID="chkCrewCar" runat="server" Text="Crew Car" />
                                                                    </td>
                                                                    <td align="left" class="tdLabel80">
                                                                        <asp:CheckBox ID="chkUVair" runat="server" Text="UVAir" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkInactive" runat="server" Text="Inactive" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_10">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel140" valign="top">
                                                                        FBO Code
                                                                    </td>
                                                                    <td class="tdLabel240" valign="top">
                                                                        <asp:TextBox ID="tbFBOCode" runat="server" CssClass="text50 inpt_non_edit" MaxLength="4"></asp:TextBox>
                                                                    </td>
                                                                    <td class="tdLabel140" valign="top">
                                                                        <span class="mnd_text">FBO Name</span>
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td valign="top" align="left">
                                                                                    <asp:TextBox ID="tbName" runat="server" MaxLength="60" CssClass="text200" ValidationGroup="Save"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="tbName"
                                                                                        ErrorMessage="FBO Name is Required" ValidationGroup="Save" Display="Dynamic"
                                                                                        CssClass="alert-text" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                <tr>
                                                                    <td class="tdLabel140">
                                                                        Address Line 1
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbAddr1" runat="server" CssClass="text580" MaxLength="150"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                <tr>
                                                                    <td class="tdLabel140">
                                                                        Address Line 2
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbAddr2" runat="server" CssClass="text580" MaxLength="150"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                <tr>
                                                                    <td class="tdLabel140">
                                                                        Address Line 3
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="tbAddress3" runat="server" CssClass="text580" MaxLength="150"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel140">
                                                                        City
                                                                    </td>
                                                                    <td class="tdLabel240">
                                                                        <asp:TextBox ID="tbCity" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                                                    </td>
                                                                    <td class="tdLabel140">
                                                                        State/Province
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbState" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel140" valign="top">
                                                                        Country
                                                                    </td>
                                                                    <td align="left" class="tdLabel240">
                                                                        <table cellspacing="0" cellpadding="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbCountry" runat="server" OnTextChanged="CountryCode_TextChanged"
                                                                                        AutoPostBack="true" CssClass="text40" MaxLength="3"></asp:TextBox>
                                                                                    <asp:HiddenField ID="hdnCountry" runat="server" />
                                                                                    <asp:Button ID="btnCountry" runat="server" OnClientClick="javascript:openWin('RadCountryMasterPopup');return false;"
                                                                                        CssClass="browse-button" />
                                                                                    </button>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CustomValidator ID="cvCountry" runat="server" ControlToValidate="tbCountry"
                                                                                        ErrorMessage="Invalid Country Code." Display="Dynamic" CssClass="alert-text"
                                                                                        ValidationGroup="Save"></asp:CustomValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td class="tdLabel140" valign="top">
                                                                        Postal
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="tbPostal" runat="server" CssClass="text200" MaxLength="15" onKeyPress="return fnAllowAlphaNumeric(this, event,  '+','-')"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_10">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel140">
                                                                        Contact Name
                                                                    </td>
                                                                    <td class="tdLabel240">
                                                                        <asp:TextBox ID="tbContact" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel140">
                                                                        Business Phone
                                                                    </td>
                                                                    <td class="tdLabel240">
                                                                        <asp:TextBox ID="tbPhone1" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                    </td>
                                                                    <td class="tdLabel140">
                                                                        Alt Business Phone
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbPhone2" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel140" valign="top">
                                                                        Contact Business Phone
                                                                    </td>
                                                                    <td class="tdLabel240">
                                                                        <asp:TextBox ID="tbbusinessPhone" runat="server" CssClass="text200" MaxLength="25"
                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                    </td>
                                                                    <td class="tdLabel140">
                                                                        Toll-free Phone
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbTollFreePhone" runat="server" CssClass="text200" MaxLength="25"
                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel140" valign="top">
                                                                        Contact Mobile/Cell
                                                                    </td>
                                                                    <td valign="top" class="tdLabel240">
                                                                        <asp:TextBox ID="tbContactMobile" runat="server" CssClass="text200" MaxLength="25"
                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                    </td>
                                                                    <td class="tdLabel140" valign="top">
                                                                        Business Fax
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="tbFax" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel140" valign="top">
                                                                        Contact E-mail
                                                                    </td>
                                                                    <td align="left" class="tdLabel240">
                                                                        <table cellspacing="0" cellpadding="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbContactEmail" runat="server" CssClass="text200" MaxLength="250"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:RegularExpressionValidator ID="revContactEmail" runat="server" ControlToValidate="tbContactEmail"
                                                                                        ValidationGroup="Save" ErrorMessage="Invalid E-mail Format" ValidationExpression="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
                                                                                        Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td class="tdLabel140">
                                                                        Customer Service E-mail
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbEmail" runat="server" CssClass="text200" MaxLength="250"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="tbEmail"
                                                                                        ValidationGroup="Save" ErrorMessage="Invalid E-mail Format" ValidationExpression="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
                                                                                        Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel140">
                                                                        Web site
                                                                    </td>
                                                                    <td class="tdLabel240">
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbWebsite" runat="server" CssClass="text200" MaxLength="250"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <%--  <asp:RegularExpressionValidator ID="revWebsite" runat="server" ControlToValidate="tbWebsite"  ValidationGroup="Save"
                                                                                        ErrorMessage="Invalid Web Site Format" ValidationExpression="^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}$"
                                                                                        Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>--%>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel140" valign="top">
                                                                        Exchange Rate
                                                                    </td>
                                                                    <td align="left" class="tdLabel240" valign="top">
                                                                        <table cellspacing="0" cellpadding="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbExchangeRate" runat="server" OnTextChanged="ExchangeRate_TextChanged"
                                                                                        AutoPostBack="true" CssClass="text40" MaxLength="6"></asp:TextBox>
                                                                                    <asp:HiddenField ID="hdnExchangeRateID" runat="server" />
                                                                                    <asp:Button ID="btnSearchExchange" runat="server" OnClientClick="javascript:openWin('RadExchangeMasterPopup');return false;"
                                                                                        CssClass="browse-button" />
                                                                                    </button>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CustomValidator ID="cvExchangeRate" runat="server" ControlToValidate="tbExchangeRate"
                                                                                        ErrorMessage="Invalid Exchange Rate Code." Display="Dynamic" CssClass="alert-text"
                                                                                        ValidationGroup="Save"></asp:CustomValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td class="tdLabel140" valign="top">
                                                                        Negotiated Terms
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="tbNegotiateTerm" runat="server" TextMode="MultiLine" Height="30px"
                                                                            CssClass="text190" MaxLength="800"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel140" valign="top">
                                                                        Custom Field 1
                                                                    </td>
                                                                    <td valign="top" class="tdLabel240">
                                                                        <asp:TextBox ID="tbCustomField1" runat="server" TextMode="MultiLine" CssClass="textarea200x50"
                                                                            onkeyup="return checkMaxLen(this,251)" MaxLength="250"></asp:TextBox>
                                                                    </td>
                                                                    <%--<td valign="top">
                                                                        <span>(Maximum 250 characters)</span>
                                                                    </td>--%>
                                                                    <td class="tdLabel140" valign="top">
                                                                        Custom Field 2
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="tbCustomField2" runat="server" TextMode="MultiLine" CssClass="textarea200x50"
                                                                            onkeyup="return checkMaxLen(this,251)" MaxLength="250"></asp:TextBox>
                                                                    </td>
                                                                    <%--  <td valign="top">
                                                                        <span>(Maximum 250 characters)</span>
                                                                    </td>--%>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_10">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                <tr>
                                                                    <td valign="top" width="50%">
                                                                        <fieldset>
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="tdLabel120" valign="top">
                                                                                        Operating Hours:
                                                                                    </td>
                                                                                    <td class="tdLabel190" valign="top">
                                                                                        <asp:TextBox ID="tbOpHrs" runat="server" CssClass="text200" MaxLength="20" onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdLabel100">
                                                                                        Air-To-Gnd
                                                                                    </td>
                                                                                    <td class="tdLabel190">
                                                                                        <asp:TextBox ID="tbAirToGnd" runat="server" CssClass="text200" MaxLength="7" onKeyPress="return fnAllowAlphaNumericAndDot(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdLabel100">
                                                                                        SITA
                                                                                    </td>
                                                                                    <td align="left" class="tdLabel190">
                                                                                        <asp:TextBox ID="tbSITA" runat="server" CssClass="text200" MaxLength="15" onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdLabel100">
                                                                                        UNICOM
                                                                                    </td>
                                                                                    <td class="tdLabel190">
                                                                                        <asp:TextBox ID="tbUnicom" runat="server" onchange="chkAirToGroundForUNICOM();" CssClass="text200" MaxLength="10"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdLabel100">
                                                                                        ARINC
                                                                                    </td>
                                                                                    <td align="left">
                                                                                       <asp:TextBox ID="tbARINC" runat="server" CssClass="text200" MaxLength="15"
                                                                                              onchange="chkAirToGroundForARINC();" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td>
                                                                                         <asp:RegularExpressionValidator ID="REFtbARINC" runat="server" ControlToValidate="tbARINC"
                                                                                                        EnableClientScript="True" CssClass="alert-text" ValidationGroup="Save" ErrorMessage="Expected Format(000.000)"
                                                                                                        Display="Dynamic" ValidationExpression="^[0-9]{3,3}(\.[0-9]{3,3})?$"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </fieldset>
                                                                    </td>
                                                                    <td valign="top" width="50%">
                                                                        <fieldset>
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="tdLabel130">
                                                                                        Fuel Brand
                                                                                    </td>
                                                                                    <td class="tdLabel200">
                                                                                        <asp:TextBox ID="tbFuelBrand" runat="server" CssClass="text200" MaxLength="100" onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdLabel130" valign="top">
                                                                                        Negotiated Price
                                                                                    </td>
                                                                                    <td class="pr_radtextbox_200">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <%--<asp:TextBox ID="tbNegPrice" runat="server" CssClass="text200" MaxLength="7" ValidationGroup="Save"
                                                                                                        onBlur="return ValidateEmptyTextbox(this, event)" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>--%>
                                                                                                    <telerik:RadNumericTextBox ID="tbNegPrice" runat="server" Type="Currency" Culture="en-US"
                                                                                                        ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="2" Value="0.00"
                                                                                                        NumberFormat-DecimalSeparator="." ValidationGroup="Save" EnabledStyle-HorizontalAlign="Right">
                                                                                                    </telerik:RadNumericTextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                   <%-- <asp:RegularExpressionValidator ID="revNegPrice" runat="server" ControlToValidate="tbNegPrice"
                                                                                                        EnableClientScript="True" CssClass="alert-text" ValidationGroup="Save" ErrorMessage="Expected Format(00.0000)"
                                                                                                        Display="Dynamic" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdLabel130" valign="top">
                                                                                        Fuel Quantity
                                                                                    </td>
                                                                                    <td align="left" class="tdLabel200">
                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="tbFuelQuantity" runat="server" CssClass="text200" MaxLength="15"
                                                                                                        onBlur="return ValidateEmptyTextbox2(this, event)" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:RegularExpressionValidator ID="revFuelQuantity" runat="server" ControlToValidate="tbFuelQuantity"
                                                                                                        EnableClientScript="True" CssClass="alert-text" ValidationGroup="Save" ErrorMessage="Expected Format(000000000000.00)"
                                                                                                        Display="Dynamic" ValidationExpression="^[0-9]{0,12}(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdLabel130" valign="top">
                                                                                        Payment Type
                                                                                    </td>
                                                                                    <td align="left" class="tdLabel200">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="tbPaymentType" runat="server" OnTextChanged="PaymentType_TextChanged"
                                                                                                        onBlur="return RemoveSpecialChars(this)" AutoPostBack="true" CssClass="text40"
                                                                                                        MaxLength="2" onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hdnPaymentType" runat="server" />
                                                                                                    <asp:Button ID="btnPaymentType" runat="server" OnClientClick="javascript:openWin('radPaymentTypePopup');return false;"
                                                                                                        CssClass="browse-button" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:CustomValidator ID="cvPaymentType" runat="server" ControlToValidate="tbPaymentType"
                                                                                                        ErrorMessage="Invalid Payment Type Code." Display="Dynamic" CssClass="alert-text"
                                                                                                        ValidationGroup="Save"></asp:CustomValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdLabel130" valign="top">
                                                                                        Last Fuel Price
                                                                                    </td>
                                                                                    <td align="left" class="pr_radtextbox_200">
                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <%--<asp:TextBox ID="tbLastFuelPrice" runat="server" CssClass="text200" MaxLength="10"
                                                                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'.')" ValidationGroup="Save"
                                                                                                        onBlur="return ValidateEmptyTextbox1(this, event)"></asp:TextBox>--%>
                                                                                                    <telerik:RadNumericTextBox ID="tbLastFuelPrice" runat="server" Type="Currency" Culture="en-US"
                                                                                                        ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="5" Value="0.00"
                                                                                                        NumberFormat-DecimalSeparator="." ValidationGroup="Save" EnabledStyle-HorizontalAlign="Right">
                                                                                                    </telerik:RadNumericTextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <%--<asp:RegularExpressionValidator ID="rfvLastFuelPrice" runat="server" ControlToValidate="tbLastFuelPrice"
                                                                                                        EnableClientScript="True" CssClass="alert-text" ValidationGroup="Save" ErrorMessage="Expected Format(00000.0000)"
                                                                                                        Display="Dynamic" ValidationExpression="^[0-9]{0,5}(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdLabel130" valign="top">
                                                                                        Posted Price
                                                                                    </td>
                                                                                    <td align="left" class="pr_radtextbox_200">
                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <%-- <asp:TextBox ID="tbPostedPrice" runat="server" CssClass="text200" MaxLength="10"
                                                                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'.')" ValidationGroup="Save"
                                                                                                        onBlur="return ValidateEmptyTextbox1(this, event)"></asp:TextBox>--%>
                                                                                                    <telerik:RadNumericTextBox ID="tbPostedPrice" runat="server" Type="Currency" Culture="en-US"
                                                                                                        ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="5" Value="0.00"
                                                                                                        NumberFormat-DecimalSeparator="." ValidationGroup="Save" EnabledStyle-HorizontalAlign="Right">
                                                                                                    </telerik:RadNumericTextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                   <%-- <asp:RegularExpressionValidator ID="rfvPostedPrice" runat="server" ControlToValidate="tbPostedPrice"
                                                                                                        EnableClientScript="True" CssClass="alert-text" ValidationGroup="Save" ErrorMessage="Expected Format(00000.0000)"
                                                                                                        Display="Dynamic" ValidationExpression="^[0-9]{0,5}(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdLabel130" valign="top">
                                                                                        Last Purchase Date
                                                                                    </td>
                                                                                    <td align="left" class="tdLabel200">
                                                                                        <%--  <asp:TextBox ID="tbLastPurchase" runat="server" CssClass="text200" onKeyPress="return fnNotAllowKeys(this, event,'/')"
                                                                                            onclick="showPopup(this, event);" onfocus="showPopup(this, event);" MaxLength="10"></asp:TextBox>--%>
                                                                                        <uc:DatePicker ID="ucLastPurchase" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </fieldset>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_10">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel140" valign="top">
                                                                        Sunday Work Hours
                                                                    </td>
                                                                    <td valign="top" class="tdLabel240">
                                                                        <asp:TextBox ID="tbSundayWorkHours" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                                                    </td>
                                                                    <td class="tdLabel140" valign="top">
                                                                        Monday Work Hours
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="tbMondayWorkHours" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel140" valign="top">
                                                                        Tuesday Work Hours
                                                                    </td>
                                                                    <td valign="top" class="tdLabel240">
                                                                        <asp:TextBox ID="tbTuesdayWorkHours" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                                                    </td>
                                                                    <td class="tdLabel140" valign="top">
                                                                        Wednesday Work Hours
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="tbWednesdayWorkHours" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel140" valign="top">
                                                                        Thursday Work Hours
                                                                    </td>
                                                                    <td valign="top" class="tdLabel240">
                                                                        <asp:TextBox ID="tbThursdayWorkHours" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                                                    </td>
                                                                    <td class="tdLabel140" valign="top">
                                                                        Friday Work Hours
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="tbFridayWorkHours" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel140" valign="top">
                                                                        Saturday Work Hours
                                                                    </td>
                                                                    <td valign="top" class="tdLabel240">
                                                                        <asp:TextBox ID="tbSaturdayWorkHours" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        Additional Information
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="add-info-box" id="divAdditionalInfo" runat="server">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:radpanelbar>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <asp:Label ID="lbWarningMessage" Text="WARNING! This is an FBO maintained during Logistics Updates.Information entered may be changed."
                            CssClass="alert-text"  runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td class="custom_radbutton" style="text-align: right">
                        <telerik:radbutton id="btnSaveChanges" runat="server" text="Save" validationgroup="Save"
                            onclick="SaveChanges_Click" onclientclicking="CheckFutureDate" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" OnClick="Cancel_Click" CssClass="button" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnUWAID" runat="server" />
                        <asp:HiddenField ID="hdnCountryId" runat="server" />
                        <asp:HiddenField ID="hdnPayment" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" /> 
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td align="left">
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:radpanelbar id="pnlNotes" width="100%" expandanimation-type="none" collapseanimation-type="none"
                runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Notes" Expanded="false" CssClass="PanelHeaderStyle">
                        <Items>
                            <telerik:RadPanelItem>
                                <ContentTemplate>
                                    <table width="100%" cellspacing="0" class="note-box">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbNotes" runat="server" TextMode="MultiLine" CssClass="textarea-db">
                                                </asp:TextBox>
                                                <asp:HiddenField ID="hdnIsUWa" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:radpanelbar>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
