﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!--<![endif]-->
<head id="Head1" runat="server">
    <title>Hotel</title>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <style type="text/css">
        html, body, form {
            margin: 0;
            padding: 0;
            width: 940px;
            height: 450px;
        }
    </style>
    <script type="text/javascript">
        var selectedRowData = "";
        var jqgridTableId = '#gridHotel';
        var hotelId = null;
        var hotelcd = null;
        var icaoId = null;
        var selectedRowMultipleHotel = "";
        var isopenlookup = false;
        var ismultiSelect = false;
        var hotelCDs = [];
        $(document).ready(function () {
            icaoId = getUrlVars()["IcaoID"];
            $.ajax({
                async: false,
                type: 'Get',
                url: '/Views/Utilities/GETApi.aspx?apiType=FSS&method=airportbyairportid&airportId=' + icaoId,
                contentType: 'text/html',
                success: function (rowData, textStatus, xhr) {
                    rowData = $.parseJSON($('<div/>').html(rowData).text());
                    if (rowData != undefined && rowData != null) {
                        $('#lblICAO').text(rowData.IcaoID);
                        $('#lblCity').text(rowData.CityName);
                        $('#lblState').text(rowData.StateName);
                        $('#lblAirport').text(rowData.AirportName);
                        $('#lblCountry').text(rowData.CountryName);
                    }
                    else {
                        showMessageBox("Airport Does Not Exist", popupTitle);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var content = jqXHR.responseText;
                    var msg = "This Airport(" + icaoId + ")";
                    msg = msg.concat(" does not exist.");
                    showMessageBox(msg, popupTitle);
                }
            });


            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

            selectedRowData = decodeURI(getQuerystring("HotelCD", ""));
            selectedRowMultipleHotel = $.trim(decodeURI(getQuerystring("HotelCD", "")));
            jQuery("#hdnselectedfccd").val(selectedRowMultipleHotel);
            if (selectedRowData != "") {
                isopenlookup = true;
            }

            if (ismultiSelect) {
                if (selectedRowData.length < jQuery("#hdnselectedfccd").val().length) {
                    selectedRowData = jQuery("#hdnselectedfccd").value;
                }
                hotelCDs = selectedRowData.split(',');
            }
            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                if ((selr != null && ismultiSelect == false) || (ismultiSelect == true && selectedRowMultipleAirport.length > 0)) {
                    var rowData = $(jqgridTableId).getRowData(selr);
                    returnToParent(rowData, 0);
                }
                else {
                    showMessageBox('Please select a hotel.', popupTitle);
                }
                return false;
            });

            $("#recordAdd").click(function () {
                var widthDoc = $(document).width();
                if (Number(icaoId) > 0 &&  isNaN(icaoId)==false)
                    popupwindow("/Views/Settings/Logistics/HotelCatalog.aspx?IsPopup=Add&IcaoID=" + icaoId, popupTitle, 1100, 798, jqgridTableId);
                else
                    showMessageBox("Airport Does Not Exist", popupTitle);

                return false;
            });

            $("#recordDelete").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                hotelId = rowData['HotelID'];
                hotelcd = rowData['HotelCD'];
                var widthDoc = $(document).width();
                if (hotelId != undefined && hotelId != null && hotelId != '' && hotelId != 0) {
                    showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                }
                else {
                    showMessageBox('Please select a hotel.', popupTitle);
                }
                return false;
            });

            function confirmCallBackFn(arg) {
                if (arg) {
                    $.ajax({
                        type: 'GET',
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=Hotel&hotelId=' + hotelId,
                        contentType: 'text/html',
                        success: function (data) {
                            var message = "This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.";
                            DeleteApiResponse(jqgridTableId, data, message, popupTitle);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;
                            var msg = '';
                            if (content == 'PreconditionFailed') {
                                msg = "This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.";
                            }
                            else {
                                msg = "This Hotel (" + hotelcd + ")";
                                msg = msg.concat(" Can not be deleted.");
                            }
                            showMessageBox(msg, popupTitle);
                        }
                    });

                }
            }

            $("#recordEdit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                hotelId = rowData['HotelID'];
                var widthDoc = $(document).width();
                if (hotelId != undefined && hotelId != null && hotelId != '' && hotelId != 0) {
                    popupwindow("/Views/Settings/Logistics/HotelCatalog.aspx?IsPopup=&HotelID=" + hotelId + "&IcaoID=" + icaoId, popupTitle, 1100, 798, jqgridTableId);
                }
                else {
                    showMessageBox('Please select a hotel.', popupTitle);
                }
                return false;
            });

            $(jqgridTableId).jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    $(jqgridTableId).jqGrid("clearGridData");
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }
                    postData.apiType = 'fss';
                    postData.method = 'GetHotelsByAirportID';

                    postData.HotelID = 0;
                    postData.AirportID = icaoId;
                    postData.HotelCD = function () { if (ismultiSelect && selectedRowData.length > 0) { return hotelCDs[0]; } else { return selectedRowData; } };
                    postData.showInactive = $('#chkSearchActiveOnly').is(':checked') ? true : false;
                    postData.FetchIsCrewOnly = $('#chkSearchCrewOnly').is(':checked') ? true : false;
                    postData.FetchIsPaxOnly = $('#chkSearchPassengerOnly').is(':checked') ? true : false;
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;
                    return postData;
                },
                height: 230,
                width: 920,
                viewrecords: true,
                rowNum: $("#rowNum").val(),
                multiselect: ismultiSelect,
                pager: "#pg_gridPager",
                shrinkToFit: false,
                colNames: ['ID', 'Code', 'Crew', 'PAX', 'Hotel Name', 'Phone', 'Fax', 'Rating', 'Miles', 'From', 'Min', 'Rate', 'Metro', 'Filter', 'Inactive'],
                colModel: [
                     { name: 'HotelID', index: 'HotelID', key: true, hidden: true },
                     { name: 'HotelCD', index: 'HotelCD', width: 50 },
                     { name: 'IsCrew', index: 'IsCrew', width: 40, formatter: 'checkbox', search: false, align: 'center' },
                     { name: 'IsPassenger', index: 'IsPassenger', width: 40, formatter: 'checkbox', search: false, align: 'center' },
                     { name: 'Name', index: 'Name', width: 220 },
                     { name: 'PhoneNum', index: 'PhoneNum', width: 100 },
                     { name: 'FaxNum', index: 'FaxNum', width: 100 },
                     { name: 'HotelRating', index: 'HotelRating', width: 60 },
                     { name: 'MilesFromICAO', index: 'MilesFromICAO', width: 50 },
                     { name: 'IcaoID', index: 'IcaoID', width: 80 },
                     { name: 'MinutesFromICAO', index: 'MinutesFromICAO', width: 50 },
                     { name: 'NegociatedRate', index: 'NegociatedRate', width: 50 },
                     { name: 'MetroCD', index: 'MetroCD', width: 80 },
                     { name: 'UWAMaintFlag', index: 'UWAMaintFlag', width: 100, formatter: Filterformatter },
                     { name: 'IsInActive', index: 'IsInActive', width: 50, formatter: 'checkbox', sortable: false, search: false, align: 'center' }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData, 1);
                },
                onSelectRow: function (id, status) {
                    var rowData = $(this).getRowData(id);
                    selectedRowMultipleHotel = JqgridOnSelectRow(ismultiSelect, status, selectedRowMultipleHotel, rowData.HotelCD);
                },
                onSelectAll: function (id, status) {
                    selectedRowMultipleHotel = JqgridSelectAll(selectedRowMultipleHotel, jqgridTableId, id, status, 'HotelCD');
                    jQuery("#hdnselectedfccd").val(selectedRowMultipleHotel);
                },
                afterInsertRow: function (rowid, rowObject) {
                    JqgridSelectAfterInsertRow(ismultiSelect, selectedRowMultipleHotel, rowid, rowObject.HotelCD, jqgridTableId);
                },
                loadComplete: function (rowData) {
                    JqgridCreateSelectAllOption(ismultiSelect, jqgridTableId);
                }
            });
            $("#pagesizebox").insertBefore('.ui-paging-info');
            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });

        });

        function Filterformatter(cellvalue, options, rowObject) {
            if (cellvalue)
                cellvalue = 'UWA';
            else
                cellvalue = 'CUSTOM';
            return cellvalue;
        }

        function returnToParent(rowData, one) {
            selectedRowMultipleHotel = jQuery.trim(selectedRowMultipleHotel);
            if (selectedRowMultipleHotel.lastIndexOf(",") == selectedRowMultipleHotel.length - 1)
                selectedRowMultipleHotel = selectedRowMultipleHotel.substr(0, selectedRowMultipleHotel.length - 1);

            var oArg = new Object();
            if (one == 0) {
                oArg.HotelCD = selectedRowMultipleHotel;
            }
            else {
                oArg.HotelCD = rowData["HotelCD"];
            }

            oArg.Name = rowData["Name"];
            oArg.PhoneNum = rowData["PhoneNum"];
            oArg.HotelRating = rowData["HotelRating"];
            oArg.NegociatedRate = rowData["NegociatedRate"];
            oArg.HotelID = rowData["HotelID"];
            oArg.FaxNum = rowData["FaxNum"];
            oArg.AirportID = rowData["AirportID"];
            oArg.ContactEmail = rowData["ContactEmail"];

            oArg.Arg1 = oArg.HotelCD;
            oArg.CallingButton = "HotelCD";
            var oWnd = GetRadWindow();
            if (oArg) {
                oWnd.close(oArg);
            }
        }
        function reloadGrid() {
            $(jqgridTableId).trigger('reloadGrid');
        }
    </script>
</head>
<body>

    <form id="form1">

        <div class="jqgrid">
            <input type="hidden" id="hdnselectedfccd" value="" />
            <div>
                <table class="box1">
                    <tr>
                        <td>
                            <div class="evenly">
                                <div class="item">ICAO:</div>
                                <div id="lblICAO" class="airportdata"></div>
                                <div class="item">City:</div>
                                <div id="lblCity" class="airportdata"></div>
                                <div class="item">State:</div>
                                <div id="lblState" class="airportdata"></div>
                                <div class="item">Country:</div>
                                <div id="lblCountry" class="airportdata"></div>
                                <div class="item">Airport:</div>
                                <div id="lblAirport" class="airportdata"></div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <input type="checkbox" name="chkSearchActiveOnly" id="chkSearchActiveOnly" /><label for="chkSearchInActive">Display Inactive</label>
                            <input type="checkbox" name="chkSearchCrewOnly" id="chkSearchCrewOnly" /><label for="chkSearchCrew">Crew Only</label>
                            <input type="checkbox" name="chkSearchPassengerOnly" id="chkSearchPassengerOnly" /><label for="chkSearchPassenger">Passenger Only</label>
                            <input type="button" class="button" id="btnSearch" value="Search" name="btnSearch" onclick="reloadGrid(); return false;" />
                        </td>
                    </tr>
                    <tr>
                        <td>

                            <table id="gridHotel" class="table table-striped table-hover table-bordered"></table>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager"></div>
                                <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                                <div id="pagesizebox">
                                    <span>Size:</span>
                                    <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                    <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridTableId, $('#rowNum')); return false;" />
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK" type="button" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>

</body>
</html>

