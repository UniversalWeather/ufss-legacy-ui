﻿<%@ Import Namespace="System.Web.Optimization" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head runat="server">
    <title>Payable Vendor</title>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
   <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
   <link href="/Scripts/jquery.alerts.css" rel="stylesheet" media="all" />
    
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
   <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/jquery.alerts.js"></script>
    <script type="text/javascript">
        var jqgridTableId = '#gridPayableVendor';
        var paymentSelectedVendorCD = "";
        $(document).ready(function () {

            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

            paymentSelectedVendorCD = $.trim(unescape(getQuerystring("VendorCD", "")));
            var uireports = $.trim(unescape(getQuerystring("IsUIReports", "")));
            var ismultiselect = false;
            if (uireports == "1") {
                ismultiselect = true;
            }

            $(jqgridTableId).jqGrid({
                url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/GetPayableVendors',
                mtype: 'POST',
                datatype: "json",
                async: true,
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false, async: false },
                serializeGridData: function (postData) {
                    postData.showActiveOnly = $("#chkDisplayActiveOnly").is(':checked') ? true : false;
                    postData.homebaseOnly = $("#chkHomebaseOnly").is(':checked') ? true : false;
                    return JSON.stringify(postData);
                },
                height: "auto",
                width: 908,
                loadonce: true,
                autowidth: false,
                shrinkToFit: false,
                ignoreCase: true,
                multiselect: ismultiselect,
                pgbuttons: false,
                pginput: false,
                colNames: ['VendorID', 'Code', 'Vendor Name', 'Contact First Name','Contact Last Name' ,'InActive', 'Home Base'],
                colModel: [
                        { name: 'VendorID', index: 'VendorID', hidden: true, key: true },
                        { name: 'VendorCD', index: 'VendorCD', width: 100 },
                        { name: 'Name', index: 'Name', width: 320 },
                        { name: 'FirstName', index: 'FirstName', width: 130 },
                        { name: 'LastName', index: 'LastName', width: 130 },
                        {
                            name: 'InActive', index: 'InActive', width: 50, align: 'center', formatter: function (cellvalue, options, rowObject) {
                                if (cellvalue == false) {
                                    return '<input id="chkcompleted" disabled="disabled" type="checkbox" checked="checked" />';
                                } else { return '<input id="chkcompleted" disabled="disabled" type="checkbox"  />'; }
                            }
                        },
                        { name: 'HomebaseCD', index: 'HomebaseCD', width: 100 }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData);
                },
                loadComplete: function () {
                    var ids = $(this).jqGrid("getDataIDs");
                    for (i = 1; i <= ids.length; i++) {
                        rowData = $(this).jqGrid('getRowData', ids[i]);
                        if (rowData["VendorCD"] == paymentSelectedVendorCD) {
                            $(this).jqGrid('setSelection', ids[i]);
                            break;
                        }
                    }
                },
                onSelectRow: function (id) {
                    var rowNumber = $(this).jqGrid('getCell', id, 'VendorID');
                    var rowData = $(jqgridTableId).jqGrid('getRowData', rowNumber);
                    var lastSel = rowData['VendorID'];//replace name with any column
                    paymentSelectedVendorCD = $.trim(rowData['VendorCD']);

                    if (id !== lastSel) {
                        $(this).find(".selected").removeClass('selected');
                        $(jqgridTableId).jqGrid('resetSelection', lastSel, true);
                        $(this).find('.ui-state-highlight').addClass('selected');
                        lastSel = id;
                    }
                }
            });

            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });

            $("#chkDisplayActiveOnly").change(function () {
                rebindgrid();
            });
            
            $("#chkHomebaseOnly").change(function () {
                rebindgrid();
            });


            $("#btnSubmitPaymentType").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow')
                var rowData = $(jqgridTableId).getRowData(selr);
                if (rowData["VendorCD"] == undefined) {
                    BootboxAlert("Please select a record.");
                } else {
                    returnToParent(rowData);
                }
                return false;
            });

            $("#recordAdd").click(function () {
                openWin2("/Views/Settings/Logistics/PayableVendor.aspx?IsPopup=Add", 'radPayableVendorCRUDPopup');
                return false;
            });

            $("#recordEdit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                var paymentSelectedVendorId = rowData['VendorID'];
                if (!IsNullOrEmptyOrUndefined(paymentSelectedVendorId) && paymentSelectedVendorId != 0) {
                    openWin2("/Views/Settings/Logistics/PayableVendor.aspx?IsPopup=&SelectedPayableVendorID=" + paymentSelectedVendorId, 'radPayableVendorCRUDPopup');
                }
                else {
                    jAlert('Please select a record.', popupTitle);
                }
                return false;
            });

            $("#recordDelete").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                vendorIdToDelete = rowData['VendorID'];
                if (!IsNullOrEmptyOrUndefined(vendorIdToDelete) && vendorIdToDelete != 0) {
                    jConfirm("Are you sure you want to delete this record?", 'Confirmation', confirmCallBackFn);
                }
                else {
                    jAlert('Please select a record.', popupTitle);
                }
                return false;
            });

        });
        function confirmCallBackFn(arg) {
            if (arg) {
                $.ajax({
                    type: 'POST',
                    cache: false,
                    url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/DeletePayableVendor',
                    data: JSON.stringify({ 'vendorIdToDelete': vendorIdToDelete }),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var returnResult = verifyReturnedResult(data);
                        if (returnResult == false) {
                            var message = "This record is either not available or already in use. Deletion is not allowed.";
                            jAlert(message, "Payable Vendor");
                        }
                        else {
                            jAlert('Payable Vendor deleted successfully!', "Payable Vendor");
                        }
                        rebindgrid();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        reportPostflightError(jqXHR, textStatus, errorThrown);
                    }
                });
            }
        }

        function rebindgrid()
        {
            $(jqgridTableId).setGridParam({ datatype: 'json', page: 1 }).trigger('reloadGrid');
        }

        function returnToParent(rowDataSelected) {
            var oArg = new Object();
            oArg.VendorCD = "";
            oArg.Name = "";
            oArg.VendorID = "";
            if (rowDataSelected != undefined && rowDataSelected != null && !IsNullOrEmptyOrUndefined(rowDataSelected["VendorID"])) {
                oArg.VendorCD = rowDataSelected["VendorCD"];
                oArg.Name = rowDataSelected["Name"];
                oArg.VendorID = rowDataSelected["VendorID"];
            }
            else {
                var ids = $(jqgridTableId).jqGrid('getGridParam', 'selarrrow');
                for (i = 0; i < ids.length; i++) {
                    var rowData = $(jqgridTableId).jqGrid('getRowData', ids[i]);
                    oArg.VendorCD += rowData["VendorCD"] + ",";
                    oArg.Name += rowData["Name"];
                    oArg.VendorID += rowData["VendorID"];
                }
                if (oArg.VendorCD != "") {
                    oArg.VendorCD = oArg.VendorCD.substring(0, oArg.VendorCD.length - 1)
                }
                else {
                    oArg.VendorCD = "";
                }
                oArg.Arg1 = oArg.VendorCD;
                oArg.CallingButton = "VendorCD";
            }
            var oWnd = GetRadWindow();
            if (oArg) {
                oWnd.close(oArg);
            }

        }
    </script>
    <style type="text/css">
        body {
            background: none !important;
            overflow-x:hidden;
            height:100%; 
        }
    </style>
</head>
<body>
    <form id="form1">
       <div class="bootstrap_pop_minwrapper search_popup">
        <div class="jqgrid row">
                <table class="box1">
                    <tr>
                        <td>
                             <input type="checkbox" id="chkHomebaseOnly" name="chkHomebaseOnly"/>Home Base Only
                             <input type="checkbox" id="chkDisplayActiveOnly" name="chkDisplayActiveOnly"/>Active Only
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="gridPayableVendor" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager_paymentType"></div>                              
                            </div>
                            <div class="clear"></div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <a class="add-icon-grid" href="#" id="recordAdd"></a>
                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                                <input id="btnSubmitPaymentType" class="button okButton" style="margin-left: -20px;" value="OK"  type="button"/>
                            </div>
                        </td>
                    </tr>
                </table>
        </div>
        </div>
    </form>
</body>
</html>
