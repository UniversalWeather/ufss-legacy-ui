﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head id="Head1" runat="server">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" /> 
    <title>Transport</title>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <script type="text/javascript">
        var transportId = null;
        var tranportCode = null;
        var selectedRowData = null;
        var jqgridTableId = '#gridTransports';
        var airport = null;
        var transportSelectedCD = "";
        var isopenlookup = false;

        $(document).ready(function () {
            transportSelectedCD = $.trim(unescape(getQuerystring("TransportCD", "")));
            if (transportSelectedCD != "") {
                isopenlookup = true;
            }
            $.support.cors = true;
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

            var airportId = getQuerystring("IcaoID", "NoValue");
            getAirport(airportId);

            $("#chkActiveOnly").change(function () {
                $(jqgridTableId).trigger('reloadGrid');
            });

            $("#chkChoiceOnly").change(function () {
                $(jqgridTableId).trigger('reloadGrid');
            });

            $("#btnSubmit").click(function () {
                var selr = $('#gridTransports').jqGrid('getGridParam', 'selrow');
                var rowData = $('#gridTransports').getRowData(selr);

                if (rowData["TransportCD"] == undefined) {
                    showMessageBox('Please select a transport.', popupTitle);
                } else {
                    returnToParent(rowData);
                }
                return false;
            });

            $("#recordAdd").click(function () {
                var widthDoc = $(document).width();
                openWinAddEdit("/Views/Settings/Logistics/TransportCatalog.aspx?IsPopup=Add" +
                    "&IcaoID=" + airport.IcaoID +
                    "&CityName=" + airport.CityName +
                    "&StateName=" + airport.StateName +
                    "&CountryName=" + airport.CountryName +
                    "&AirportName=" + airport.AirportName +
                    "&AirportId=" + airport.AirportId, "rdAddpopupwindow", widthDoc + 100, 600, jqgridTableId);
                return false;
            });

            $("#recordDelete").click(function () {
                var selr = $('#gridTransports').jqGrid('getGridParam', 'selrow');
                var rowData = $('#gridTransports').getRowData(selr);
                transportId = rowData['TransportID'];
                tranportCode = rowData['TransportCD'];
                var isActive = rowData['IsActive'];
                if (isActive == 'Yes') {
                    showMessageBox('Active transport cannot be deleted.', popupTitle);
                    return false;
                }

                if (transportId != undefined && transportId != null && transportId != '' && transportId != 0) {
                    showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                }
                else {
                    showMessageBox('Please select a transport.', popupTitle);
                }
                return false;
            });

            function getAirport(airportIcaoId) {
                $.ajax({
                    async: true,
                    type: 'Get',
                    dataType: 'json',
                    crossDomain: true,
                    url: '/Views/Utilities/ApiCallerwithFilter.aspx?apiType=FSS&method=AirportByAirportId&AirportId=' + airportIcaoId,
                    contentType: 'application/json',
                    success: function (data) {
                        setAirport(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        var content;
                        if (jqXHR.responseText) {
                            content = jqXHR.responseText;
                        } else {
                            content = jqXHR.statusText;
                        }
                        var msg = "This Airport";
                        msg = msg.concat(" does not exist.");
                        if (content.indexOf('404'))
                            showMessageBox(msg, popupTitle);
                    }
                });
            }

            function setAirport(data) {
                airport = data;
                $("#lbICAO").text(airport.IcaoID);
                $("#lbCity").text(airport.CityName);
                $("#lbState").text(airport.StateName);
                $("#lbCountry").text(airport.CountryName);
                $("#lbAirport").text(airport.AirportName);
            }

            function confirmCallBackFn(arg) {
                if (arg) {
                    $.ajax({
                        async: true,
                        type: 'Get',
                        crossDomain: true,
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=Transport&transportId=' + transportId,
                        contentType: 'text/html',
                        success: function (data) {
                            var message = "This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.";
                            DeleteApiResponse(jqgridTableId, data, message, popupTitle);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;  
                            if (content.indexOf('404'))
                                showMessageBox("This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.", popupTitle);
                        }
                    });
                }
            }

            $("#recordEdit").click(function () {
                var selr = $('#gridTransports').jqGrid('getGridParam', 'selrow');
                var rowData = $('#gridTransports').getRowData(selr);
                transportId = rowData['TransportID'];
                var widthDoc = $(document).width();
                if (transportId != undefined && transportId != null && transportId != '' && transportId != 0) {
                    openWinAddEdit("/Views/Settings/Logistics/TransportCatalog.aspx?IsPopup=" +
                        "&IcaoID=" + airport.IcaoID +
                        "&CityName=" + airport.CityName +
                        "&StateName=" + airport.StateName +
                        "&CountryName=" + airport.CountryName +
                        "&AirportName=" + airport.AirportName +
                        "&AirportId=" + airport.AirportId +
                        "&TransportId=" + transportId, "rdEditpopupwindow", widthDoc + 100, 600, jqgridTableId);
                    $('#gridTransports').trigger('reloadGrid');
                }
                else {
                    showMessageBox('Please select a tranport.', popupTitle);
                }
                return false;
            });

            jQuery("#gridTransports").jqGrid({

                url: '/Views/Utilities/ApiCallerwithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    $("#gridTransports").jqGrid("clearGridData");
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }

                    postData.transportID = 0;
                    postData.apiType = 'fss';
                    postData.method = 'Transports';
                    postData.airportID = airportId;
                    postData.transportCD = '';
                    postData.fetchActiveOnly = $("#chkActiveOnly").prop('checked');
                    postData.fetchChoiceOnly = $("#chkChoiceOnly").prop('checked');
                    postData.SelectedTransportCD = transportSelectedCD;
                    postData.markSelectedRecord = isopenlookup;

                    return postData;
                },

                height: 290,
                width: 980,
                viewrecords: true,
                rowNum: $("#rowNum").val(),
                multiselect: false,
                pager: "#pg_gridPager",
                enableclear: true,
                emptyrecords: "No records to view.",
                colNames: ['TransportID', 'Transport Code', 'Choice', 'Name', 'Phone', 'Fax', 'Rate', 'Filter', 'Inactive', 'AirportID'],
                colModel: [
                    { name: 'TransportID', index: 'TransportID', key: true, hidden: true },
                    { name: 'TransportCD', index: 'TransportCD', align: 'left', width: 60 },
                    { name: 'IsChoice', index: 'IsChoice', align: 'center', editable: true, edittype: 'checkbox', 
                        formatter: "checkbox", formatoptions: { disabled: true }, search: false, width: 70 },
                    { name: 'TransportationVendor', index: 'TransportationVendor', 
                        align: 'left', formatter: 'currencyFmatter', width:290 },
                    { name: 'PhoneNum', index: 'PhoneNum', align: 'left', width:90 },
                    { name: 'FaxNum', index: 'FaxNum', align: 'left', width: 90 },
                    { name: 'NegotiatedRate', index: 'NegotiatedRate', align: 'left', width:80,
                        formatter: 'currency', formatoptions: { decimalSeparator: '.', decimalPlaces: 2 }
                    },
                    { name: 'Filter', index: 'UWAMaintFlag', width:60 },
                    {
                        name: 'IsInActive', index: 'IsInActive', align: 'center', editable: true, edittype: 'checkbox',
                        formatter: "checkbox", formatoptions: { disabled: true }, search: false, width: 70
                    },
                    { name: 'AirportID', index: 'AirportID', hidden: true }

                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData);
                },
                onSelectRow: function (id) {

                    var rowData = $(this).getRowData(id);
                    var lastSel = rowData['TransportCD'];//replace name with any column
                    transportSelectedCD = $.trim(rowData['TransportCD']);

                    if (id !== lastSel) {
                        $(this).find(".selected").removeClass('selected');
                        $('#results_table').jqGrid('resetSelection', lastSel, true);
                        $(this).find('.ui-state-highlight').addClass('selected');
                        lastSel = id;
                    }
                },
                afterInsertRow: function (rowid, rowdata, rowelem) {
                    if (rowdata.Filter == 'UWA') {
                        jQuery('#gridTransports').setCell(rowid, 'Filter', '', 'red-text');
                    }

                    var lastSel = rowdata['TransportCD'];//replace name with any column

                    if ($.trim(transportSelectedCD) == $.trim(lastSel)) {
                        $(this).find(".selected").removeClass('selected');
                        $(this).find('.ui-state-highlight').addClass('selected');
                        $(jqgridTableId).jqGrid('setSelection', rowid);
                    }
                }
            });
            $("#pagesizebox").insertBefore('.ui-paging-info');
            $("#gridTransports").jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
        });

        function reloadPageSize() {
            var myGrid = $('#gridTransports');
            var currentValue = $("#rowNum").val();
            myGrid.setGridParam({ rowNum: currentValue });
            $('#gridTransports').jqGrid().trigger('reloadGrid');
        }
    </script>

    <script type="text/javascript">

        function returnToParent(rowData) {
            var oArg = new Object();
            var oWnd = GetRadWindow();
            oArg.AirportID = rowData["AirportID"];
            oArg.TransportID = rowData["TransportID"];
            oArg.TransportCD = rowData["TransportCD"];
            oArg.IsChoice = rowData["IsChoice"];
            oArg.TransportationVendor = rowData["TransportationVendor"];
            oArg.PhoneNum = rowData["PhoneNum"];
            oArg.FaxNum = rowData["FaxNum"];
            oArg.NegotiatedRate = rowData["NegotiatedRate"];
            oArg.Filter = rowData["Filter"];
            oArg.IsInActive = rowData["IsInActive"];

            oArg.Arg1 = oArg.TransportCD;
            oArg.CallingButton = "TransportCD";

            var val = getUrlVarsWithParam(oWnd.GetUrl())["ControlName"];
            if (val === 'RequestorCD') {
                oArg.Arg1 = oArg.TransportCD;
                oArg.CallingButton = "RequestorCD";
            }
            if (oArg) {
                oWnd.close(oArg);
            }
        }

    </script>

    <style type="text/css">
        .showDiv {
            text-align: right;
            margin-right: 77px;
            color: red;
            visibility: visible !important;
        }

        .hideDiv {
            visibility: hidden;
        }

        body {
            overflow: hidden;
        }
        .red-text {
            color: red;font-weight: bold;
        }
    </style>
</head>
<body>
    <style>
        .ui-jqgrid .ui-jqgrid-htable th div { height: 30px; }
        .ui-jqgrid .ui-th-div-ie { height: 30px; }
    </style>
    <form id="form1" runat="server">
        <div class="jqgrid">
            <table class="box1">
                <tr>
                    <td align="left">
                        <table class="box1" style="padding: 0;">
                            <tbody>
                                <tr>
                                <td>
                                    ICAO:
                                </td>
                                <td>
                                    <label ID="lbICAO" name="lbICAO"></label>
                                </td>
                                <td>
                                    City:
                                </td>
                                <td>
                                    <label ID="lbCity" name="lbCity"></label>
                                </td>
                                <td>
                                    State:
                                </td>
                                <td>
                                    <label ID="lbState" name="lbState"></label>
                                </td>
                                <td>
                                    Country: 
                                </td>
                                <td>
                                    <label ID="lbCountry" name="lbCountry"></label>
                                </td>
                                <td>
                                    Airport: 
                                </td>
                                <td>
                                    <label ID="lbAirport" name="lbAirport"></label>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <div>
                            <input type="checkbox" name="chkActiveOnly" value="Active Only" id="chkActiveOnly" />
                            Display Inactive
                        <input type="checkbox" name="chkChoiceOnly" value="Choice Only" id="chkChoiceOnly" />
                            Display Choice Only
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td>
                        <table id="gridTransports" class="table table-striped table-hover table-bordered"></table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="grid_icon">
                            <div role="group" id="pg_gridPager"></div>
                            <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                            <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                            <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                            <div id="pagesizebox">
                                <span>Page Size:</span>
                                <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(); return false;" />
                            </div>
                        </div>
                        <div style="text-align: right;">
                            <input id="btnSubmit" class="button okButton" value="OK"  type="button"/>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
