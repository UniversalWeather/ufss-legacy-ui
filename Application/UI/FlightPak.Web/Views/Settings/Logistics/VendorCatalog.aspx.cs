﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Logistics
{
    public partial class VendorCatalog : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        public List<string> lstVendorMasterCodes = new List<string>();

        #region constants
        private const string ConstIsInactive = "IsInActive";
        #endregion

        private ExceptionManager exManager;
        private bool VendorPageNavigated = false;
        private string strVendorID = "";
        private string strVendorCD = "";
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgVendorCatalog, dgVendorCatalog, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgVendorCatalog.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewVendorCatalog);
                            if (!IsAuthorized(Permission.Database.AddVendorCatalog) && !IsAuthorized(Permission.Database.EditVendorCatalog) && !IsAuthorized(Permission.Database.DeleteVendorCatalog))
                            {
                                lnkVendorContacts.Enabled = false;
                            }
                            // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgVendorCatalog.Rebind();
                                ReadOnlyForm();
                                EnableForm(false);
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                if (UserPrincipal.Identity._fpSettings._IsDeactivateHomeBaseFilter == false)
                                {
                                    chkSearchHomebaseOnly.Checked = true;
                                }
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }
        }

        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(BindDataSwitch))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgVendorCatalog.Rebind();
                    }
                    if (dgVendorCatalog.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedVendorID"] = null;
                        //}
                        if (Session["SelectedVendorID"] == null)
                        {
                            dgVendorCatalog.SelectedIndexes.Add(0);
                            Session["SelectedVendorID"] = dgVendorCatalog.Items[0].GetDataKeyValue("VendorID").ToString();
                        }
                        else
                        {
                            foreach (GridDataItem item in dgVendorCatalog.MasterTableView.Items)
                            {
                                if (item.GetDataKeyValue("VendorID").ToString() == Session["SelectedVendorID"].ToString())
                                {
                                    item.Selected = true;
                                }
                            }
                        }

                        if (dgVendorCatalog.SelectedIndexes.Count == 0)
                            dgVendorCatalog.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                    GridEnable(true, true, true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void chkCertificate_OnCheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkCertificate.Checked)
                        {
                            tbCertificate.Text = string.Empty;
                            tbCertificate.Visible = true;
                        }
                        else
                        {
                            tbCertificate.Text = string.Empty;
                            tbCertificate.Visible = false;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgVendorCatalog;
                        }
                        else if (e.Initiator.ID.IndexOf("btnSaveChangesTop", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgVendorCatalog;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }
        }

        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedVendorID"] != null)
                    {
                        string ID = Session["SelectedVendorID"].ToString();
                        foreach (GridDataItem Item in dgVendorCatalog.MasterTableView.Items)
                        {
                            if (Item["VendorID"].Text.Trim() == ID)
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                        //if (dgVendorCatalog.SelectedItems.Count == 0)
                        //{
                        //    DefaultSelection(false);
                        //}
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }


        protected void Vendor_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (VendorPageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgVendorCatalog, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgVendorCatalog_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEditButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.Validate(group);
                        // get the first validator that failed
                        var validator = GetValidators(group)
                        .OfType<BaseValidator>()
                        .FirstOrDefault(v => !v.IsValid);
                        // set the focus to the control
                        // that the validator targets
                        if (validator != null)
                        {
                            Control target = validator
                            .NamingContainer
                            .FindControl(validator.ControlToValidate);
                            if (target != null)
                            {
                                target.Focus();
                                IsEmptyCheck = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }
        }
        /// <summary>
        /// Bind Vendor Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgVendorCatalog_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objVendorCatalogService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objPayableVendor = objVendorCatalogService.GetVendorInfo();
                            List<GetAllVendor> vendorList = new List<GetAllVendor>();
                            if (objPayableVendor.EntityList != null && objPayableVendor.EntityList.Count > 0)
                            {
                                vendorList.AddRange(objPayableVendor.EntityList.OrderBy(x => x.VendorCD));
                            }
                            dgVendorCatalog.DataSource = vendorList.OrderBy(x => x.VendorCD);
                            Session["Vendor"] = vendorList;

                            DoSearchFilter();
                        }
                        // BindGrid(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }

        }

        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DoSearchFilter();
                        DefaultSelection(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }
        }


        /// <summary>
        /// Item Command for Vendor Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgVendorCatalog_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                if (Session["SelectedVendorID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.Vendor, Convert.ToInt64(Session["SelectedVendorID"].ToString().Trim()));
                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Vendor);
                                            SelectItem();
                                            return;
                                        }
                                        pnlFilterForm.Enabled = false;
                                        SelectItem();
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        tbName.Focus();
                                        SelectItem();
                                        DisableLinks();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgVendorCatalog.SelectedIndexes.Clear();
                                tbCode.ReadOnly = false;
                                tbCode.BackColor = System.Drawing.Color.White;
                                pnlFilterForm.Enabled = false;
                                DisplayInsertForm();
                                if (UserPrincipal != null && UserPrincipal.Identity._airportICAOCd != null && UserPrincipal.Identity._airportICAOCd != string.Empty)
                                {
                                    tbHomeBase.Text = UserPrincipal.Identity._airportICAOCd;
                                    if (UserPrincipal != null && UserPrincipal.Identity._airportId != 0)
                                    {
                                        hdnHomeBaseID.Value = Convert.ToString(UserPrincipal.Identity._airportId);
                                    }
                                }
                                else
                                {
                                    tbHomeBase.Text = string.Empty;

                                }
                                chkActive.Checked = true;
                                GridEnable(true, false, false);
                                tbCode.Focus();
                                DisableLinks();
                                break;
                            case "Filter":
                                //foreach (GridColumn Column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }
        }

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            lnkVendorContacts.Enabled = false;
            lnkVendorContacts.CssClass = "fleet_link_disable";
            chkSearchActiveOnly.Enabled = false;
            chkSearchHomebaseOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            lnkVendorContacts.Enabled = true;
            lnkVendorContacts.CssClass = "fleet_link";
            chkSearchActiveOnly.Enabled = true;
            chkSearchHomebaseOnly.Enabled = true;
        }


        /// <summary>
        /// Update Command for Vendor Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgVendorCatalog_UpdateCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedVendorID"] != null)
                        {

                            e.Canceled = true;
                            bool IsValidate = true;
                            if (CheckAlreadyHomeBaseExist())
                            {
                                cvHomeBase.IsValid = false;
                                tbHomeBase.Focus();
                                IsValidate = false;
                            }
                            if (CheckAlreadyClosestIcaoExist())
                            {
                                cvClosestIcao.IsValid = false;
                                tbClosestIcao.Focus();
                                IsValidate = false;
                            }
                            if (CheckAlreadyBillCountryExist())
                            {
                                cvBillCountry.IsValid = false;
                                tbBillCountry.Focus();
                                IsValidate = false;
                            }
                            if (CheckAlreadyMetroCodeExist())
                            {
                                cvMetro.IsValid = false;
                                tbMetro.Focus();
                                IsValidate = false;
                            }
                            if (IsValidate)
                            {
                                e.Canceled = true;
                                using (FlightPakMasterService.MasterCatalogServiceClient objVendorService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objVen = objVendorService.UpdateVendorMaster(GetItems());
                                    if (objVen.ReturnFlag == true)
                                    {
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        ///////Update Method UnLock
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.Vendor, Convert.ToInt64(Session["SelectedVendorID"].ToString().Trim()));
                                        }
                                        GridEnable(true, true, true);
                                        SelectItem();
                                        ReadOnlyForm();

                                        ShowSuccessMessage();
                                        EnableLinks();
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objVen.ErrorMessage, ModuleNameConstants.Database.Vendor);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }

        }

        protected void Vendor_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgVendorCatalog.ClientSettings.Scrolling.ScrollTop = "0";
                        VendorPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }
        }

        /// <summary>
        /// Insert Command for Vendor Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgVendorCatalog_InsertCommand(object source, GridCommandEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        if (CheckAlreadyExist())
                        {
                            cvCode.IsValid = false;
                            tbCode.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyHomeBaseExist())
                        {
                            cvHomeBase.IsValid = false;
                            tbHomeBase.Focus();
                            IsValidate = false;

                        }
                        if (CheckAlreadyClosestIcaoExist())
                        {
                            cvClosestIcao.IsValid = false;
                            tbClosestIcao.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyBillCountryExist())
                        {
                            cvBillCountry.IsValid = false;
                            tbBillCountry.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyMetroCodeExist())
                        {
                            cvMetro.IsValid = false;
                            tbMetro.Focus();
                            IsValidate = false;
                        }
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient objVendorCatalogService = new MasterCatalogServiceClient())
                            {
                                var objVen = objVendorCatalogService.AddVendorMaster(GetItems());
                                if (objVen.ReturnFlag == true)
                                {
                                    dgVendorCatalog.Rebind();
                                    DefaultSelection(false);

                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objVen.ErrorMessage, ModuleNameConstants.Database.Vendor);
                                }
                            }
                        }
                        
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }


        }


        /// <summary>
        /// To check unique code already exists
        /// </summary>
        /// <returns></returns>
        private bool CheckAlreadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    using (FlightPakMasterService.MasterCatalogServiceClient objservice = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objservice.GetVendorInfo().EntityList.Where(x => x.VendorCD.Trim().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim()));
                        if (objRetVal.Count() > 0 && objRetVal != null)
                        {
                            cvCode.IsValid = false;
                            tbCode.Focus();
                            returnVal = true;
                        }
                        else
                        {
                            tbName.Focus();
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return returnVal;
            }
        }

        protected void Code_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null)
                        {
                            CheckAlreadyExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }

        }

        /// <summary>
        /// To check unique Home Base  on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HomeBase_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbHomeBase.Text != null)
                        {
                            CheckAlreadyHomeBaseExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }

        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }
        }
        private bool CheckAlreadyHomeBaseExist()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool retval = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbHomeBase.Text != string.Empty) && (tbHomeBase.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {

                            var RetValue = Service.GetAirportByAirportICaoID(tbHomeBase.Text).EntityList;
                            //var RetValue = Service.GetAllAirportList().EntityList.Where(x => x.IcaoID.Trim().ToUpper().Equals(tbHomeBase.Text.ToString().ToUpper().Trim()));
                            if (RetValue.Count() == 0 || RetValue == null)
                            {
                                cvHomeBase.IsValid = false;
                                tbHomeBase.Focus();
                                retval = true;
                            }
                            else
                            {
                                hdnHomeBaseID.Value = RetValue[0].AirportID.ToString();
                                tbHomeBase.Text = RetValue[0].IcaoID;
                                //foreach (FlightPakMasterService.GetAllAirport cm in RetValue)
                                //{
                                //    hdnHomeBaseID.Value = cm.AirportID.ToString();
                                //}
                                tbClosestIcao.Focus();
                                retval = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return retval;
            }
        }
        /// <summary>
        /// To check unique Airport on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClosestIcao_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbClosestIcao.Text != null)
                        {
                            CheckAlreadyClosestIcaoExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }
        }

        private bool CheckAlreadyClosestIcaoExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbClosestIcao.Text != string.Empty) && (tbClosestIcao.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var RetValue = Service.GetAirportByAirportICaoID(tbClosestIcao.Text).EntityList;
                            if (RetValue.Count() == 0 || RetValue == null)
                            {
                                cvClosestIcao.IsValid = false;
                                tbClosestIcao.Focus();
                                RetVal = true;
                            }

                            else
                            {
                                hdnAirportID.Value = RetValue[0].AirportID.ToString();
                                tbClosestIcao.Text = RetValue[0].IcaoID;
                                //foreach (FlightPakMasterService.GetAllAirport cm in RetValue)
                                //{
                                //    hdnAirportID.Value = cm.AirportID.ToString();
                                //}
                                tbAddlInsured.Focus();
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }

        /// <summary>
        /// To check unique Airport on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BillCountry_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbBillCountry.Text != null)
                        {
                            CheckAlreadyBillCountryExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }

        }

        private bool CheckAlreadyBillCountryExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbBillCountry.Text != string.Empty) && (tbBillCountry.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var RetValue = Service.GetCountryMasterList().EntityList.Where(x => x.CountryCD.Trim().ToUpper().Equals(tbBillCountry.Text.ToString().ToUpper().Trim()));
                            if (RetValue.Count() == 0 || RetValue == null)
                            {

                                cvBillCountry.IsValid = false;
                                tbBillCountry.Focus();
                                RetVal = true;
                            }
                            else
                            {
                                foreach (FlightPakMasterService.Country cm in RetValue)
                                {
                                    hdnCountryID.Value = cm.CountryID.ToString();
                                    tbBillCountry.Text = cm.CountryCD;
                                }
                                tbBillPostal.Focus();
                                RetVal = false;
                            }
                        }
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;

            }

        }
        /// <summary>
        /// To check unique Airport on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Metro_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbMetro.Text != null)
                        {
                            CheckAlreadyMetroCodeExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }
        }

        private bool CheckAlreadyMetroCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbMetro.Text != string.Empty) && (tbMetro.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var RetValue = Service.GetMetroCityList().EntityList.Where(x => x.MetroCD.Trim().ToUpper().Equals(tbMetro.Text.ToString().ToUpper().Trim()));
                            if (RetValue.Count() == 0 || RetValue == null)
                            {
                                cvMetro.IsValid = false;
                                tbMetro.Focus();
                                RetVal = true;
                            }
                            else
                            {
                                foreach (FlightPakMasterService.Metro cm in RetValue)
                                {
                                    hdnMetroID.Value = cm.MetroID.ToString();
                                    tbMetro.Text = cm.MetroCD;
                                }
                                tbBillAddr1.Focus();
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }


        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgVendorCatalog_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedVendorID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient VendorService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.Vendor Vendor = new FlightPakMasterService.Vendor();
                                string Code = Session["SelectedVendorID"].ToString();
                                strVendorCD = "";
                                strVendorID = "";
                                foreach (GridDataItem Item in dgVendorCatalog.MasterTableView.Items)
                                {
                                    if (Item["VendorID"].Text.Trim() == Code.Trim())
                                    {
                                        strVendorCD = Item["VendorCD"].Text.Trim();
                                        strVendorID = Item["VendorID"].Text.Trim();
                                        Vendor.VendorType = Item.GetDataKeyValue("VendorType").ToString();
                                        Vendor.CustomerID = Convert.ToInt64(Item.GetDataKeyValue("CustomerID").ToString());
                                        break;
                                    }
                                }
                                Vendor.VendorCD = strVendorCD;
                                Vendor.VendorID = Convert.ToInt64(strVendorID);
                                Vendor.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.Vendor, Convert.ToInt64(Session["SelectedVendorID"].ToString()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Vendor);
                                        return;
                                    }
                                }
                                VendorService.DeleteVendorMaster(Vendor);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                dgVendorCatalog.Rebind();
                                DefaultSelection(false);
                                GridEnable(true, true, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer                           
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.Vendor, Convert.ToInt64(Session["SelectedVendorID"].ToString()));
                    }
                }
            }

        }

        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void dgVendorCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem Item = dgVendorCatalog.SelectedItems[0] as GridDataItem;
                                pnlFilterForm.Enabled = true;
                                Session["SelectedVendorID"] = Item["VendorID"].Text;
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                    }
                }
            }
        }

        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    ClearForm();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Command Event Trigger for Save or Update Vendor Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgVendorCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgVendorCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                            pnlFilterForm.Enabled = true;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }
        }

        /// <summary>
        /// Cancel Vendor Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedVendorID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.Vendor, Convert.ToInt64(Session["SelectedVendorID"].ToString().Trim()));
                                    DefaultSelection(false);
                                }
                            }
                        }
                        pnlFilterForm.Enabled = true;
                        DefaultSelection(false);
                        EnableLinks();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        private Vendor GetItems()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.Vendor VendorCatalog = new FlightPakMasterService.Vendor();

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    if (hdnSave.Value == "Update")
                    {
                        if (Session["SelectedVendorID"] != null)
                            VendorCatalog.VendorID = Convert.ToInt64(Session["SelectedVendorID"].ToString());

                        if (!string.IsNullOrEmpty(hdnContactName.Value))
                        {
                            VendorCatalog.VendorContactName = hdnContactName.Value;
                        }
                    }
                    VendorCatalog.VendorCD = tbCode.Text;
                    VendorCatalog.Name = tbName.Text;
                    VendorCatalog.VendorType = "V";
                    if (!string.IsNullOrEmpty(tbBillName.Text))
                    {
                        VendorCatalog.BillingName = Convert.ToString(tbBillName.Text.Trim(), CultureInfo.CurrentCulture);
                    }

                    if (!string.IsNullOrEmpty(tbHomeBase.Text))
                    {
                        VendorCatalog.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                    }
                    if (!string.IsNullOrEmpty(tbClosestIcao.Text))
                    {
                        VendorCatalog.AirportID = Convert.ToInt64(hdnAirportID.Value);
                    }

                    if (!string.IsNullOrEmpty(tbSITANumber.Text))
                    {
                        VendorCatalog.SITA = tbSITANumber.Text;
                    }

                    if (!string.IsNullOrEmpty(tbBillCountry.Text))
                    {
                        VendorCatalog.CountryID = Convert.ToInt64(hdnCountryID.Value);
                    }

                    if (!string.IsNullOrEmpty(tbMetro.Text))
                    {
                        VendorCatalog.MetroID = Convert.ToInt64(hdnMetroID.Value);
                    }
                    if (!string.IsNullOrEmpty(tbBillAddr1.Text))
                    {
                        VendorCatalog.BillingAddr1 = tbBillAddr1.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbBillAddr2.Text))
                    {
                        VendorCatalog.BillingAddr2 = tbBillAddr2.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbBillCity.Text))
                    {
                        VendorCatalog.BillingCity = tbBillCity.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbBillState.Text))
                    {
                        VendorCatalog.BillingState = tbBillState.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbBillPostal.Text))
                    {
                        VendorCatalog.BillingZip = tbBillPostal.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbBillFax.Text))
                    {
                        VendorCatalog.BillingFaxNum = tbBillFax.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbAddlInsured.Text))
                    {
                        VendorCatalog.AdditionalInsurance = tbAddlInsured.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbNotes.Text))
                    {
                        VendorCatalog.Notes = tbNotes.Text.Trim();
                    }
                    VendorCatalog.IsInActive = chkActive.Checked;
                    VendorCatalog.IsApplicationFiled = chkApplication.Checked;
                    VendorCatalog.IsApproved = chkApproval.Checked;
                    VendorCatalog.IsTaxExempt = chkTaxExempt.Checked;
                    VendorCatalog.IsFAR135Approved = chkApprov.Checked;
                    VendorCatalog.IsFAR135CERT = chkCertificate.Checked;
                    VendorCatalog.CertificateNumber = tbCertificate.Text;
                    VendorCatalog.IsDrugTest = chkDrugTestApproval.Checked;
                    VendorCatalog.IsInsuranceCERT = chkInsCert.Checked;
                    if (!string.IsNullOrEmpty(tbLatitudeDeg.Text))
                    {
                        VendorCatalog.LatitudeDegree = Convert.ToDecimal(tbLatitudeDeg.Text.Trim(), CultureInfo.CurrentCulture);
                    }
                    if (!string.IsNullOrEmpty(tbLatitudeMin.Text))
                    {
                        VendorCatalog.LatitudeMinutes = Convert.ToDecimal(tbLatitudeMin.Text.Trim(), CultureInfo.CurrentCulture);
                    }
                    if (!string.IsNullOrEmpty(tbLatitudeNS.Text))
                    {
                        VendorCatalog.LatitudeNorthSouth = tbLatitudeNS.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbLongitudeDeg.Text))
                    {
                        VendorCatalog.LongitudeDegree = Convert.ToDecimal(tbLongitudeDeg.Text.Trim(), CultureInfo.CurrentCulture);
                    }
                    if (!string.IsNullOrEmpty(tbLongitudeMin.Text))
                    {
                        VendorCatalog.LongitudeMinutes = Convert.ToDecimal(tbLongitudeMin.Text.Trim(), CultureInfo.CurrentCulture);
                    }
                    if (!string.IsNullOrEmpty(tbLongitudeEW.Text))
                    {
                        VendorCatalog.LongitudeEastWest = tbLongitudeEW.Text.Trim();
                    }

                    if (!string.IsNullOrEmpty(tbBillPhone.Text))
                    {
                        VendorCatalog.BusinessPhone = tbBillPhone.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbTollFreePhone.Text))
                    {
                        VendorCatalog.TollFreePhone = tbTollFreePhone.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbEmailAddress.Text))
                    {
                        VendorCatalog.BusinessEmail = tbEmailAddress.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbWebAddress.Text))
                    {
                        VendorCatalog.Website = tbWebAddress.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbBillingAddr3.Text))
                    {
                        VendorCatalog.BillingAddr3 = tbBillingAddr3.Text.Trim();
                    }

                    VendorCatalog.IsDeleted = false;



                }, FlightPak.Common.Constants.Policy.UILayer);

                return VendorCatalog;
            }
        }


        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    tbCode.ReadOnly = true;
                    tbCode.BackColor = System.Drawing.Color.LightGray;
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }


        }


        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedVendorID"] != null)
                    {
                        strVendorID = "";
                        foreach (GridDataItem Item in dgVendorCatalog.MasterTableView.Items)
                        {
                            if (Item["VendorID"].Text.Trim() == Session["SelectedVendorID"].ToString().Trim())
                            {
                                tbCode.Text = Item.GetDataKeyValue("VendorCD").ToString().Trim();
                                tbName.Text = Item.GetDataKeyValue("Name").ToString().Trim();
                                if (Item.GetDataKeyValue("BillingName") != null)
                                {
                                    tbBillName.Text = Item.GetDataKeyValue("BillingName").ToString();
                                }
                                else
                                {
                                    tbBillName.Text = string.Empty;
                                }

                                if (Item.GetDataKeyValue("VendorContactName") != null)
                                {
                                    hdnContactName.Value = Item.GetDataKeyValue("VendorContactName").ToString().Trim();
                                }
                                else
                                {
                                    hdnContactName.Value = string.Empty;
                                }

                                if (Item.GetDataKeyValue("HomeBaseCD") != null)
                                {
                                    tbHomeBase.Text = Item.GetDataKeyValue("HomeBaseCD").ToString();
                                }
                                else
                                {
                                    tbHomeBase.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("ClosestICAO") != null)
                                {
                                    tbClosestIcao.Text = Item.GetDataKeyValue("ClosestICAO").ToString();
                                }
                                else
                                {
                                    tbClosestIcao.Text = string.Empty;
                                }

                                if (Item.GetDataKeyValue("AdditionalInsurance") != null)
                                {
                                    tbAddlInsured.Text = Item.GetDataKeyValue("AdditionalInsurance").ToString();
                                }
                                else
                                {
                                    tbAddlInsured.Text = string.Empty;
                                }


                                if (Item.GetDataKeyValue("NationalityCD") != null)
                                {
                                    tbBillCountry.Text = Item.GetDataKeyValue("NationalityCD").ToString();
                                }
                                else
                                {
                                    tbBillCountry.Text = string.Empty;
                                }

                                if (Item.GetDataKeyValue("SITA") != null)
                                {
                                    tbSITANumber.Text = Item.GetDataKeyValue("SITA").ToString();
                                }
                                else
                                {
                                    tbSITANumber.Text = string.Empty;
                                }

                                if (Item.GetDataKeyValue("MetroCD") != null)
                                {
                                    tbMetro.Text = Item.GetDataKeyValue("MetroCD").ToString();
                                }
                                else
                                {
                                    tbMetro.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkActive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkActive.Checked = false;
                                }
                                if (Item.GetDataKeyValue("IsApplicationFiled") != null)
                                {
                                    chkApplication.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsApplicationFiled").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkApplication.Checked = false;
                                }

                                if (Item.GetDataKeyValue("IsApproved") != null)
                                {
                                    chkApproval.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsApproved").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkApproval.Checked = false;
                                }

                                if (Item.GetDataKeyValue("IsTaxExempt") != null)
                                {
                                    chkTaxExempt.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsTaxExempt").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkTaxExempt.Checked = false;
                                }

                                if (Item.GetDataKeyValue("IsFAR135Approved") != null)
                                {
                                    chkApprov.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsFAR135Approved").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkApprov.Checked = false;
                                }

                                if (Item.GetDataKeyValue("IsFAR135CERT") != null)
                                {
                                    chkCertificate.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsFAR135CERT").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkCertificate.Checked = false;
                                }
                                if (chkCertificate.Checked)
                                {                                    
                                    tbCertificate.Visible = true;                                    
                                    if (Item.GetDataKeyValue("CertificateNumber") != null)
                                    {
                                        tbCertificate.Text = Item.GetDataKeyValue("CertificateNumber").ToString();
                                    }
                                    else
                                    {
                                        tbCertificate.Text = string.Empty;
                                    }
                                }
                                else
                                {
                                    tbCertificate.Visible = false;
                                }

                                if (Item.GetDataKeyValue("IsDrugTest") != null)
                                {
                                    chkDrugTestApproval.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsDrugTest").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkDrugTestApproval.Checked = false;
                                }

                                if (Item.GetDataKeyValue("IsInsuranceCERT") != null)
                                {
                                    chkInsCert.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInsuranceCERT").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkInsCert.Checked = false;
                                }

                                if (Item.GetDataKeyValue("BillingAddr1") != null)
                                {
                                    tbBillAddr1.Text = Item.GetDataKeyValue("BillingAddr1").ToString().Trim();
                                }
                                else
                                {
                                    tbBillAddr1.Text = string.Empty;
                                }

                                if (Item.GetDataKeyValue("BillingAddr2") != null)
                                {
                                    tbBillAddr2.Text = Item.GetDataKeyValue("BillingAddr2").ToString().Trim();
                                }
                                else
                                {
                                    tbBillAddr2.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("BillingCity") != null)
                                {
                                    tbBillCity.Text = Item.GetDataKeyValue("BillingCity").ToString().Trim();
                                }
                                else
                                {
                                    tbBillCity.Text = string.Empty;
                                }

                                if (Item.GetDataKeyValue("BillingState") != null)
                                {
                                    tbBillState.Text = Item.GetDataKeyValue("BillingState").ToString().Trim();
                                }
                                else
                                {
                                    tbBillState.Text = string.Empty;
                                }

                                if (Item.GetDataKeyValue("BillingZip") != null)
                                {
                                    tbBillPostal.Text = Item.GetDataKeyValue("BillingZip").ToString().Trim();
                                }
                                else
                                {
                                    tbBillPostal.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("BillingFaxNum") != null)
                                {
                                    tbBillFax.Text = Item.GetDataKeyValue("BillingFaxNum").ToString().Trim();
                                }
                                else
                                {
                                    tbBillFax.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LatitudeDegree") != null)
                                {
                                    tbLatitudeDeg.Text = Convert.ToString(Item.GetDataKeyValue("LatitudeDegree").ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    tbLatitudeDeg.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LatitudeMinutes") != null)
                                {
                                    tbLatitudeMin.Text = Convert.ToString(Item.GetDataKeyValue("LatitudeMinutes"), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    tbLatitudeMin.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LatitudeNorthSouth") != null)
                                {
                                    tbLatitudeNS.Text = Item.GetDataKeyValue("LatitudeNorthSouth").ToString().Trim();
                                }
                                else
                                {
                                    tbLatitudeNS.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LongitudeDegree") != null)
                                {
                                    tbLongitudeDeg.Text = Convert.ToString(Item.GetDataKeyValue("LongitudeDegree"), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    tbLongitudeDeg.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LongitudeMinutes") != null)
                                {
                                    tbLongitudeMin.Text = Convert.ToString(Item.GetDataKeyValue("LongitudeMinutes"), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    tbLongitudeMin.Text = string.Empty;
                                }

                                if (Item.GetDataKeyValue("LongitudeEastWest") != null)
                                {
                                    tbLongitudeEW.Text = Item.GetDataKeyValue("LongitudeEastWest").ToString().Trim();
                                }
                                else
                                {
                                    tbLongitudeEW.Text = string.Empty;
                                }
                                if ((Item.GetDataKeyValue("Notes") != null))
                                {
                                    tbNotes.Text = Item.GetDataKeyValue("Notes").ToString().Trim();
                                }
                                else
                                {
                                    tbNotes.Text = string.Empty;
                                }


                                if ((Item.GetDataKeyValue("BusinessPhone") != null))
                                {
                                    tbBillPhone.Text = Item.GetDataKeyValue("BusinessPhone").ToString().Trim();
                                }
                                else
                                {
                                    tbBillPhone.Text = string.Empty;
                                }
                                if ((Item.GetDataKeyValue("TollFreePhone") != null))
                                {
                                    tbTollFreePhone.Text = Item.GetDataKeyValue("TollFreePhone").ToString().Trim();
                                }
                                else
                                {
                                    tbTollFreePhone.Text = string.Empty;
                                }
                                if ((Item.GetDataKeyValue("Website") != null))
                                {
                                    tbWebAddress.Text = Item.GetDataKeyValue("Website").ToString().Trim();
                                }
                                else
                                {
                                    tbWebAddress.Text = string.Empty;
                                }
                                if ((Item.GetDataKeyValue("BusinessEmail") != null))
                                {
                                    tbEmailAddress.Text = Item.GetDataKeyValue("BusinessEmail").ToString().Trim();
                                }
                                else
                                {
                                    tbEmailAddress.Text = string.Empty;
                                }
                                if ((Item.GetDataKeyValue("BillingAddr3") != null))
                                {
                                    tbBillingAddr3.Text = Item.GetDataKeyValue("BillingAddr3").ToString().Trim();
                                }
                                else
                                {
                                    tbBillingAddr3.Text = string.Empty;
                                }



                                if (Item.GetDataKeyValue("FirstName") != null && Item.GetDataKeyValue("VendorCD") != null)
                                {
                                    if (Item.GetDataKeyValue("FirstName").ToString().Trim() != string.Empty)
                                    {

                                        using (FlightPakMasterService.MasterCatalogServiceClient objVendorContactsService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            List<GetAllVendorContact> lstVendorContact = new List<GetAllVendorContact>();
                                            GetAllVendorContact objVendorContact = new GetAllVendorContact();

                                            objVendorContact.VendorID = Convert.ToInt64(Session["SelectedVendorID"].ToString());
                                            objVendorContact.VendorType = "V";
                                            var objVen = objVendorContactsService.GetVendorContactMasterInfo(objVendorContact);

                                            if (objVen.ReturnFlag == true)
                                            {
                                                lstVendorContact = objVen.EntityList.ToList();
                                            }

                                            var results = (from code in lstVendorContact
                                                           where (code.IsChoice == true)
                                                           select code);


                                            foreach (var v in results)
                                            {
                                                if (v.FirstName != null)
                                                {
                                                    tbFirstName.Text = v.FirstName.ToString().Trim();
                                                }
                                                else
                                                {
                                                    tbFirstName.Text = string.Empty;
                                                }
                                                if (v.MiddleName != null)
                                                {
                                                    tbMiddleName.Text = v.MiddleName.ToString().Trim();
                                                }
                                                else
                                                {
                                                    tbMiddleName.Text = string.Empty;
                                                }
                                                if (v.LastName != null)
                                                {
                                                    tbLastName.Text = v.LastName.ToString().Trim();
                                                }
                                                else
                                                {
                                                    tbLastName.Text = string.Empty;
                                                }
                                                if (v.Title != null)
                                                {
                                                    tbTitle.Text = v.Title.ToString();
                                                }
                                                else
                                                {
                                                    tbTitle.Text = string.Empty;
                                                }
                                                if (v.Addr1 != null)
                                                {
                                                    tbAddr1.Text = v.Addr1.ToString();
                                                }
                                                else
                                                {
                                                    tbAddr1.Text = string.Empty;
                                                }
                                                if (v.Addr2 != null)
                                                {
                                                    tbAddr2.Text = v.Addr2.ToString();
                                                }
                                                else
                                                {
                                                    tbAddr2.Text = string.Empty;
                                                }
                                                if (v.CityName != null)
                                                {
                                                    tbCity.Text = v.CityName.ToString();
                                                }
                                                else
                                                {
                                                    tbCity.Text = string.Empty;
                                                }
                                                if (v.StateName != null)
                                                {
                                                    tbState.Text = v.StateName.ToString();
                                                }
                                                else
                                                {
                                                    tbState.Text = string.Empty;
                                                }
                                                if (v.PostalZipCD != null)
                                                {
                                                    tbPostal.Text = v.PostalZipCD.ToString();
                                                }
                                                else
                                                {
                                                    tbPostal.Text = string.Empty;
                                                }

                                                if (v.CountryID != null)
                                                {
                                                    tbCountry.Text = v.CountryCD.ToString();
                                                }
                                                else
                                                {
                                                    tbCountry.Text = string.Empty;
                                                }
                                                if (v.FaxNum != null)
                                                {
                                                    tbFax.Text = v.FaxNum.ToString();
                                                }
                                                else
                                                {
                                                    tbFax.Text = string.Empty;
                                                }
                                                if (v.EmailID != null)
                                                {
                                                    tbEmail.Text = v.EmailID.ToString();
                                                }
                                                else
                                                {
                                                    tbEmail.Text = string.Empty;
                                                }
                                                if (v.MoreInfo != null)
                                                {
                                                    tbAdditionalContact.Text = v.MoreInfo.ToString();
                                                }
                                                else
                                                {
                                                    tbAdditionalContact.Text = string.Empty;
                                                }

                                                if (v.BusinessPhone != null)
                                                {
                                                    tbBusinessPhone.Text = v.BusinessPhone.ToString();
                                                }
                                                else
                                                {
                                                    tbBusinessPhone.Text = string.Empty;
                                                }
                                                if (v.CellPhoneNum2 != null)
                                                {
                                                    tbCellPhoneNum2.Text = v.CellPhoneNum2.ToString();
                                                }
                                                else
                                                {
                                                    tbCellPhoneNum2.Text = string.Empty;
                                                }
                                                if (v.Website != null)
                                                {
                                                    tbWebsite.Text = v.Website.ToString();
                                                }
                                                else
                                                {
                                                    tbWebsite.Text = string.Empty;
                                                }
                                                //if (v.BusinessEmail != null)
                                                //{
                                                //    tbMCBusinessEmail.Text = v.BusinessEmail.ToString();
                                                //}
                                                //else
                                                //{
                                                //    tbMCBusinessEmail.Text = string.Empty;
                                                //}
                                                if (v.HomeFax != null)
                                                {
                                                    tbHomeFax.Text = v.HomeFax.ToString();
                                                }
                                                else
                                                {
                                                    tbHomeFax.Text = string.Empty;
                                                }
                                                if (v.OtherEmail != null)
                                                {
                                                    tbOtherEmail.Text = v.OtherEmail.ToString();
                                                }
                                                else
                                                {
                                                    tbOtherEmail.Text = string.Empty;
                                                }
                                                if (v.BusinessEmail != null)
                                                {
                                                    tbBusinessEmail.Text = v.BusinessEmail.ToString();
                                                }
                                                else
                                                {
                                                    tbBusinessEmail.Text = string.Empty;
                                                }
                                                if (v.PersonalEmail != null)
                                                {
                                                    tbPersonalEmail.Text = v.PersonalEmail.ToString();
                                                }
                                                else
                                                {
                                                    tbPersonalEmail.Text = string.Empty;
                                                }
                                                if (v.CellPhoneNum != null)
                                                {
                                                    tbCellPhoneNum.Text = v.CellPhoneNum.ToString();
                                                }
                                                else
                                                {
                                                    tbCellPhoneNum.Text = string.Empty;
                                                }
                                                if (v.OtherPhone != null)
                                                {
                                                    tbOtherPhone.Text = v.OtherPhone.ToString();
                                                }
                                                else
                                                {
                                                    tbOtherPhone.Text = string.Empty;
                                                }
                                                if (v.Addr3 != null)
                                                {
                                                    tbAddr3.Text = v.Addr3.ToString();
                                                }
                                                else
                                                {
                                                    tbAddr3.Text = string.Empty;
                                                }
                                                if (v.PhoneNum != null)
                                                {
                                                    tbPhone.Text = v.PhoneNum.ToString();
                                                }
                                                else
                                                {
                                                    tbPhone.Text = string.Empty;
                                                }
                                                break;
                                            }
                                        }
                                    }

                                    else
                                    {
                                        tbFirstName.Text = string.Empty;
                                        tbMiddleName.Text = string.Empty;
                                        tbLastName.Text = string.Empty;
                                        tbTitle.Text = string.Empty;
                                        tbAddr1.Text = string.Empty;
                                        tbAddr2.Text = string.Empty;
                                        tbCity.Text = string.Empty;
                                        tbState.Text = string.Empty;
                                        tbPostal.Text = string.Empty;
                                        tbCountry.Text = string.Empty;
                                        tbFax.Text = string.Empty;
                                        tbEmail.Text = string.Empty;
                                        tbAdditionalContact.Text = string.Empty;
                                        tbPhone.Text = string.Empty;

                                        tbBusinessPhone.Text = string.Empty;
                                        tbCellPhoneNum2.Text = string.Empty;
                                        tbWebsite.Text = string.Empty;
                                        //tbMCBusinessEmail.Text = string.Empty;
                                        tbHomeFax.Text = string.Empty;
                                        tbOtherEmail.Text = string.Empty;
                                        tbBusinessEmail.Text = string.Empty;
                                        tbPersonalEmail.Text = string.Empty;
                                        tbCellPhoneNum.Text = string.Empty;
                                        tbOtherPhone.Text = string.Empty;
                                        tbAddr3.Text = string.Empty;
                                    }
                                }
                                else
                                {
                                    tbFirstName.Text = string.Empty;
                                    tbMiddleName.Text = string.Empty;
                                    tbLastName.Text = string.Empty;
                                    tbTitle.Text = string.Empty;
                                    tbAddr1.Text = string.Empty;
                                    tbAddr2.Text = string.Empty;
                                    tbCity.Text = string.Empty;
                                    tbState.Text = string.Empty;
                                    tbPostal.Text = string.Empty;
                                    tbCountry.Text = string.Empty;
                                    tbFax.Text = string.Empty;
                                    tbEmail.Text = string.Empty;
                                    tbAdditionalContact.Text = string.Empty;
                                    tbPhone.Text = string.Empty;

                                    tbBusinessPhone.Text = string.Empty;
                                    tbCellPhoneNum2.Text = string.Empty;
                                    tbWebsite.Text = string.Empty;
                                    //tbMCBusinessEmail.Text = string.Empty;
                                    tbHomeFax.Text = string.Empty;
                                    tbOtherEmail.Text = string.Empty;
                                    tbBusinessEmail.Text = string.Empty;
                                    tbPersonalEmail.Text = string.Empty;
                                    tbCellPhoneNum.Text = string.Empty;
                                    tbOtherPhone.Text = string.Empty;
                                    tbAddr3.Text = string.Empty;
                                }
                                lbColumnName1.Text = "Vendor Code";
                                lbColumnName2.Text = "Vendor Name";
                                lbColumnValue1.Text = Item["VendorCD"].Text;
                                lbColumnValue2.Text = Item["Name"].Text;
                                break;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgVendorCatalog.SelectedItems.Count == 0)
                    {
                        DefaultSelection(false);
                    }
                    if (dgVendorCatalog.SelectedItems.Count > 0)
                    {
                        GridDataItem Item = dgVendorCatalog.SelectedItems[0] as GridDataItem;
                        Label lbLastUpdatedUser = (Label)dgVendorCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                        if (Item.GetDataKeyValue("LastUpdUID") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                        }
                        else
                        {
                            lbLastUpdatedUser.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("LastUpdTS") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                        }
                        LoadControlData();
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnContactName.Value = string.Empty;
                    tbCode.Text = string.Empty;
                    tbBillName.Text = string.Empty;
                    tbName.Text = string.Empty;
                    tbHomeBase.Text = string.Empty; 
                    chkActive.Checked = false;
                    chkApplication.Checked = false;
                    chkApproval.Checked = false;
                    tbClosestIcao.Text = string.Empty; 
                    chkTaxExempt.Checked = false;
                    chkApprov.Checked = false;
                    chkCertificate.Checked = false;
                    tbCertificate.Text = string.Empty;
                    tbCertificate.Visible = false;
                    tbAddlInsured.Text = string.Empty; 
                    chkDrugTestApproval.Checked = false;
                    chkInsCert.Checked = false;
                    tbSITANumber.Text = string.Empty;
                    tbBillAddr1.Text = string.Empty;
                    tbBillAddr2.Text = string.Empty;
                    tbBillCity.Text = string.Empty;
                    tbBillState.Text = string.Empty;
                    tbBillCountry.Text = string.Empty;
                    tbMetro.Text = string.Empty;
                    tbBillPostal.Text = string.Empty;
                    tbLatitudeDeg.Text = string.Empty;
                    tbLatitudeMin.Text = string.Empty;
                    tbLatitudeNS.Text = string.Empty;
                    tbLongitudeDeg.Text = string.Empty;
                    tbLongitudeMin.Text = string.Empty;
                    tbLongitudeEW.Text = string.Empty;
                    tbBillPhone.Text = string.Empty;
                    tbBillFax.Text = string.Empty;
                    tbNotes.Text = string.Empty;

                    tbEmailAddress.Text = string.Empty;
                    tbWebAddress.Text = string.Empty;

                    tbTollFreePhone.Text = string.Empty;
                    tbBusinessEmail.Text = string.Empty;
                    tbWebsite.Text = string.Empty;
                    tbBillingAddr3.Text = string.Empty;
                    tbBusinessPhone.Text = string.Empty;
                    tbCellPhoneNum.Text = string.Empty;
                    tbHomeFax.Text = string.Empty;
                    tbCellPhoneNum2.Text = string.Empty;
                    tbOtherPhone.Text = string.Empty;
                    //tbMCBusinessEmail.Text = string.Empty;
                    tbPersonalEmail.Text = string.Empty;
                    tbOtherEmail.Text = string.Empty;
                    tbAddr3.Text = string.Empty;

                    tbBusinessPhone.Text = string.Empty;
                    tbCellPhoneNum2.Text = string.Empty;
                    tbWebsite.Text = string.Empty;
                    //tbMCBusinessEmail.Text = string.Empty;
                    tbHomeFax.Text = string.Empty;
                    tbOtherEmail.Text = string.Empty;
                    tbBusinessEmail.Text = string.Empty;
                    tbPersonalEmail.Text = string.Empty;
                    tbCellPhoneNum.Text = string.Empty;
                    tbOtherPhone.Text = string.Empty;
                    tbAddr3.Text = string.Empty;
                    tbPhone.Text = string.Empty;

                    tbFirstName.Text = string.Empty;
                    tbMiddleName.Text = string.Empty;
                    tbLastName.Text = string.Empty;
                    tbTitle.Text = string.Empty;
                    tbAddr1.Text = string.Empty;
                    tbAddr2.Text = string.Empty;
                    tbAddr3.Text = string.Empty;
                    tbCity.Text = string.Empty;
                    tbState.Text = string.Empty;
                    tbCountry.Text = string.Empty;
                    tbPostal.Text = string.Empty;
                    tbPhone.Text = string.Empty;
                    tbEmail.Text = string.Empty;
                    tbFax.Text = string.Empty;

                    hdnAirportID.Value = "";
                    hdnCountryID.Value = "";
                    hdnMetroID.Value = "";
                    hdnHomeBaseID.Value = "";
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Enabled = enable;
                    tbName.Enabled = enable;
                    tbBillName.Enabled = enable;
                    tbHomeBase.Enabled = enable;
                    chkActive.Enabled = enable;
                    chkApplication.Enabled = enable;
                    chkApproval.Enabled = enable;
                    tbClosestIcao.Enabled = enable;
                    chkTaxExempt.Enabled = enable;
                    chkApprov.Enabled = enable;
                    chkCertificate.Enabled = enable;
                    tbCertificate.Enabled = enable;
                    tbAddlInsured.Enabled = enable;
                    chkDrugTestApproval.Enabled = enable;
                    chkInsCert.Enabled = enable;
                    tbNotes.Enabled = enable;
                    tbBillAddr1.Enabled = enable;
                    tbBillAddr2.Enabled = enable;
                    tbBillCity.Enabled = enable;
                    tbBillState.Enabled = enable;
                    tbBillCountry.Enabled = enable;
                    tbBillPostal.Enabled = enable;
                    tbMetro.Enabled = enable;
                    tbLatitudeDeg.Enabled = enable;
                    tbLatitudeMin.Enabled = enable;
                    tbLatitudeNS.Enabled = enable;
                    tbLongitudeDeg.Enabled = enable;
                    tbLongitudeMin.Enabled = enable;
                    tbLongitudeEW.Enabled = enable;
                    tbBillPhone.Enabled = enable;
                    tbBillFax.Enabled = enable;
                    tbSITANumber.Enabled = enable;
                    tbTollFreePhone.Enabled = enable;
                    tbBillingAddr3.Enabled = enable;
                    tbBusinessPhone.Enabled = false;
                    tbCellPhoneNum2.Enabled = false;
                    tbWebsite.Enabled = false;
                    //tbMCBusinessEmail.Enabled = false;
                    tbHomeFax.Enabled = false;
                    tbOtherEmail.Enabled = false;
                    tbBusinessEmail.Enabled = false;
                    tbPersonalEmail.Enabled = false;
                    tbCellPhoneNum.Enabled = false;
                    tbOtherPhone.Enabled = false;
                    tbAddr3.Enabled = false;
                    tbPhone.Enabled = false;

                    btnSaveChanges.Visible = enable;
                    btnCancel.Visible = enable;
                    btnSaveChangesTop.Visible = enable;
                    btnCancelTop.Visible = enable;

                    btnBillCountry.Enabled = enable;
                    btnHomeBase.Enabled = enable;
                    btnClosestIcao.Enabled = enable;
                    btnMetro.Enabled = enable;

                    tbEmailAddress.Enabled = enable;
                    tbWebAddress.Enabled = enable;


                    // Enable / Disable Vendor Contact Fields
                    tbFirstName.Enabled = false;
                    tbMiddleName.Enabled = false;
                    tbLastName.Enabled = false;
                    tbTitle.Enabled = false;
                    tbAddr1.Enabled = false;
                    tbAddr2.Enabled = false;
                    tbCity.Enabled = false;
                    tbState.Enabled = false;
                    tbPostal.Enabled = false;
                    tbPhone.Enabled = false;
                    tbCountry.Enabled = false;
                    tbFax.Enabled = false;
                    tbEmail.Enabled = false;
                    tbAdditionalContact.Enabled = false;

                }, FlightPak.Common.Constants.Policy.UILayer);


            }

        }
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtnInsertCtl = (LinkButton)dgVendorCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                    LinkButton lbtnDelCtl = (LinkButton)dgVendorCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                    LinkButton lbtnEditCtl = (LinkButton)dgVendorCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                    if (IsAuthorized(Permission.Database.AddVendorCatalog))
                    {
                        lbtnInsertCtl.Visible = true;
                        if (Add)
                        {
                            lbtnInsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtnInsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtnInsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteVendorCatalog))
                    {
                        lbtnDelCtl.Visible = true;
                        if (Delete)
                        {
                            lbtnDelCtl.Enabled = true;
                            lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            lbtnDelCtl.Enabled = false;
                            lbtnDelCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnDelCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditVendorCatalog))
                    {
                        lbtnEditCtl.Visible = true;
                        if (Edit)
                        {
                            lbtnEditCtl.Enabled = true;
                            lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtnEditCtl.Enabled = false;
                            lbtnEditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnEditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void lnkVendorContacts_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedVendorID"] != null)
                        {
                            //GridDataItem Item = (GridDataItem)Session["SelectedItem"];
                            if (dgVendorCatalog.Items.Count > 0)
                            {
                                string PayableVendorID = Session["SelectedVendorID"].ToString();
                                Session["SelectedVendorContactID"] = null;
                                RedirectToPage(string.Format("VendorContacts.aspx?Screen=Vendor&VendorID={0}&VendorType=V&VendorCode={1}&VendorName={2}", Microsoft.Security.Application.Encoder.UrlEncode(PayableVendorID), Microsoft.Security.Application.Encoder.UrlEncode(tbCode.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbName.Text)));
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }
        }

        protected void DoSearchFilter()
        {
            using (FlightPakMasterService.MasterCatalogServiceClient objPayableVendorCatalogService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objPayableVendor = objPayableVendorCatalogService.GetVendorInfo();

                List<FlightPakMasterService.GetAllVendor> lstAccount = new List<FlightPakMasterService.GetAllVendor>();
                if (Session["Vendor"] != null)
                {
                    lstAccount = (List<FlightPakMasterService.GetAllVendor>)Session["Vendor"];
                }
                if (lstAccount.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstAccount = lstAccount.Where(x => x.IsInActive == true).ToList<GetAllVendor>(); }
                    if (chkSearchHomebaseOnly.Checked == true) { lstAccount = lstAccount.Where(x => x.HomeBaseCD == UserPrincipal.Identity._airportICAOCd).ToList<GetAllVendor>(); }
                    dgVendorCatalog.DataSource = lstAccount.OrderBy(x => x.VendorCD);
                    //DefaultSelection(false);
                }
            }
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgVendorCatalog.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    chkSearchHomebaseOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var VendorValue = FPKMstService.GetVendorInfo();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, VendorValue);
            List<FlightPakMasterService.GetAllVendor> filteredList = GetFilteredList(VendorValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.VendorID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgVendorCatalog.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgVendorCatalog.CurrentPageIndex = PageNumber;
            dgVendorCatalog.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllVendor VendorValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedVendorID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = VendorValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().VendorID;
                Session["SelectedVendorID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllVendor> GetFilteredList(ReturnValueOfGetAllVendor VendorValue)
        {
            List<FlightPakMasterService.GetAllVendor> filteredList = new List<FlightPakMasterService.GetAllVendor>();

            if (VendorValue.ReturnFlag == true)
            {
                filteredList = VendorValue.EntityList.OrderBy(x => x.VendorCD).ToList();
            }

            if (filteredList.Count > 0)
            {
                if (chkSearchActiveOnly.Checked == true) { filteredList = filteredList.Where(x => x.IsInActive == true).ToList<GetAllVendor>(); }
                if (chkSearchHomebaseOnly.Checked == true) { filteredList = filteredList.Where(x => x.HomeBaseCD == UserPrincipal.Identity._airportICAOCd).ToList<GetAllVendor>(); }
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgVendorCatalog.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgVendorCatalog.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
    }
}
