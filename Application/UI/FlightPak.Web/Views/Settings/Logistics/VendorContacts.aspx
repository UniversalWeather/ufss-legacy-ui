﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master" ValidateRequest="false"
    AutoEventWireup="true" CodeBehind="VendorContacts.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Logistics.VendorContacts"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript">
        function OnClientClick(strPanelToExpand) {
            var PanelBar1 = $find("<%= pnlNotes.ClientID %>");
            var PanelBar2 = $find("<%= pnlbarmaintainance.ClientID %>");
            PanelBar1.get_items().getItem(0).set_expanded(false);
            PanelBar2.get_items().getItem(0).set_expanded(false);
            if (strPanelToExpand == "Notes") {
                PanelBar1.get_items().getItem(0).set_expanded(true);
            }
            return false;
        }

        function Onload() {
            var QSValue = getQuerystring('Screen');
            if (QSValue == "PayableVendor") {
                document.getElementById('<%=LinkButton1.ClientID%>').innerHTML = "Payable Vendor";
            }
            else if (QSValue == "Vendor") {
                document.getElementById('<%=LinkButton1.ClientID%>').innerHTML = "Vendor";
            }
        }

        function getQuerystring(thisVariable) {
            var theQueryString = window.location.search.substring(1);
            var variableArray = theQueryString.split("&");
            for (i = 0; i < variableArray.length; i++) {
                variableRetrieved = variableArray[i].split("=");
                if (variableRetrieved[0] == thisVariable) {
                    return variableRetrieved[1];
                }
            }
        }
    </script>
    <script runat="server">
        void QueryStringButton_Click(object sender, EventArgs e)
        {
            string QSValue = Request.QueryString["Screen"].ToString();
            string URL = string.Empty;
            if (QSValue == "PayableVendor")
            {
                URL = "PayableVendor.aspx?Screen=PayableVendor";
            }
            else if (QSValue == "Vendor")
            {
                URL = "VendorCatalog.aspx?Screen=Vendor";
            }
            Response.Redirect(URL);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgVendorContacts" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSaveChangesTop">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgVendorContacts" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgVendorContacts" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancelTop">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgVendorContacts" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgVendorContacts">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbCountryCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbCountryCode" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="cvCountry" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">
                        <asp:LinkButton ID="LinkButton1" Text="Vendor" runat="server" OnClick="QueryStringButton_Click"></asp:LinkButton>&nbsp;>&nbsp;Vendor
                        Contacts
                        <script type="text/javascript">                            Onload();</script>
                    </span><span class="tab-nav-icons">
                        <!--<a href="#" class="search-icon"></a>
                            <a href="#" class="save-icon"></a><a href="#" class="print-icon"></a>-->
                        <a href="#" title="Help" class="help-icon"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0" class="sticky">
        <tr>
            <td class="tdLabel100">
                Vendor Code:
            </td>
            <td>
                <asp:TextBox ID="tbVendorCode" runat="server"></asp:TextBox>
            </td>
            <td class="tdLabel100">
                Vendor Contacts:
            </td>
            <td>
                <asp:TextBox ID="tbVendorContacts" runat="server" ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
    </table>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function openWin(url, value, radWin) {

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url + value, radWin);
            }
            function openCountry() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Company/CountryMasterPopup.aspx?CountryCD=" + document.getElementById("<%=tbCountryCode.ClientID%>").value, "RadWindow1");

            }
            function ConfirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); //GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                }
            }
            function OnClientCloseCountryPopup(oWnd, args) {
                var combo = $find("<%= tbCountryCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCountryCode.ClientID%>").value = arg.CountryCD;
                        document.getElementById("<%=cvCountry.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnCountryID.ClientID%>").value = arg.CountryID;
                    }
                    else {
                        document.getElementById("<%=tbCountryCode.ClientID%>").value = "";
                        document.getElementById("<%=cvCountry.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnCountryID.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseCountryPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <!--  <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Vendor Contacts</span> <span class="tab-nav-icons"><a href="#"
                        class="search-icon"></a><a href="#" class="save-icon"></a><a href="#" class="print-icon">
                        </a><a href="#" class="help-icon"></a></span>
                </div>
            </td>
        </tr>
    </table>-->
    <telerik:RadGrid ID="dgVendorContacts" runat="server" AllowSorting="true" OnItemCreated="dgVendorContacts_ItemCreated"
        Visible="true" OnNeedDataSource="dgVendorContacts_BindData" OnItemCommand="dgVendorContacts_ItemCommand"
        OnPreRender="VendorContact_PreRender" OnUpdateCommand="dgVendorContacts_UpdateCommand"
        Height="341px" OnInsertCommand="dgVendorContacts_InsertCommand" OnPageIndexChanged="VendorContact_PageIndexChanged"
        OnDeleteCommand="dgVendorContacts_DeleteCommand" AutoGenerateColumns="false"
        PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgVendorContacts_SelectedIndexChanged"
        AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true">
        <MasterTableView DataKeyNames="CustomerID,VendorID,VendorContactID,VendorCD,ContactName,VendorType,VendorContactCD,IsChoice,Addr1,Addr2,CityName,StateName,CountryCD,PostalZipCD,PhoneNum,FaxNum,CreditName1,CreditName2,CreditNum1,CreditNum2,MoreInfo,Notes,LastUpdUID,LastUpdTS,Title,LastName,FirstName,MiddleName,EmailID,IsDeleted
        ,TollFreePhone,BusinessEmail,Website,Addr3,BusinessPhone,CellPhoneNum,HomeFax,CellPhoneNum2,OtherPhone,PersonalEmail,OtherEmail"
            CommandItemDisplay="Bottom">
            <Columns>
                <telerik:GridBoundColumn DataField="VendorContactCD" HeaderText="VendorContactCD"
                    CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="false" Display="false" FilterDelay="500"
                    ShowFilterIcon="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FirstName" HeaderText="First Name" CurrentFilterFunction="StartsWith"
                    AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="170px" FilterDelay="500"
                    FilterControlWidth="150px">
                </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="LastName" HeaderText="Last Name" CurrentFilterFunction="StartsWith"
                     AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="170px" FilterDelay="500"
                     FilterControlWidth="150px">
                </telerik:GridBoundColumn>
                <telerik:GridCheckBoxColumn DataField="IsChoice" HeaderText="Choice" ShowFilterIcon="false"
                    HeaderStyle-Width="60px" AutoPostBackOnFilter="true" CurrentFilterFunction="EqualTo">
                </telerik:GridCheckBoxColumn>
                <telerik:GridBoundColumn DataField="BusinessPhone" HeaderText="Business Phone" CurrentFilterFunction="StartsWith"
                    AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="90px" FilterDelay="500"
                    FilterControlWidth="70px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FaxNum" HeaderText="Business Fax" CurrentFilterFunction="StartsWith"
                    AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="90px" FilterDelay="500"
                    FilterControlWidth="70px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="EmailID" HeaderText="E-mail Address" CurrentFilterFunction="StartsWith"
                    AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="160px" FilterDelay="500"
                    FilterControlWidth="140px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="VendorContactID" HeaderText="VendorContactCD"
                    CurrentFilterFunction="EqualTo" Display="false" ShowFilterIcon="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="VendorID" HeaderText="VendorContactCD" CurrentFilterFunction="EqualTo"
                    Display="false" ShowFilterIcon="false">
                </telerik:GridBoundColumn>
            </Columns>
            <CommandItemTemplate>
                <div style="padding: 5px 5px; float: left; clear: both;">
                    <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                        ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lbtnDelete" OnClientClick="javascript:return ProcessDelete();"
                        runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                </div>
                <div>
                    <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                </div>
            </CommandItemTemplate>
        </MasterTableView>
        <ClientSettings EnablePostBackOnRowClick="true">
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        <GroupingSettings CaseSensitive="false" />
    </telerik:RadGrid>
    <div id="DivExternalForm" runat="server" class="dgpExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right">
                        <table cellpadding="0" cellspacing="0" class="tblButtonArea-nw">
                            <tr>
                                <td>
                                    <asp:Button ID="btnNotes" runat="server" CssClass="ui_nav" Text="Notes" OnClientClick="javascript:OnClientClick('Notes');return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnSaveChangesTop" runat="server" CssClass="button" Text="Save" ValidationGroup="save"
                                        OnClick="SaveChanges_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancelTop" runat="server" CssClass="button" Text="Cancel" CausesValidation="false"
                                        OnClick="Cancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td align="left">
                        <telerik:RadPanelBar ID="pnlbarmaintainance" Width="100%" ExpandAnimation-Type="none"
                            CollapseAnimation-Type="none" runat="server" ViewStateMode="Inherit" ClientIDMode="AutoID">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="true" Text="Main Contact Information">
                                    <ContentTemplate>
                                        <%-- <Items>
                                        <telerik:RadPanelItem>
                                            <ItemTemplate>--%>
                                        <table width="100%" class="box1">
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel140" valign="top">
                                                                <asp:CheckBox ID="chkMainChoice" runat="server" Text=" Main Choice" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <div class="tblspace_10">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                Code
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <asp:TextBox ID="tbCode" runat="server" CssClass="text50" onKeyPress="return fnNotAllowKeys(this, event)"></asp:TextBox>
                                                            </td>
                                                            <td class="tdLabel130" valign="top" style="display: none;">
                                                                <span class="mnd_text">Contact Name</span>
                                                            </td>
                                                            <td class="tdLabel200" valign="top" style="display: none;">
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbContactName" runat="server" CssClass="text200" MaxLength="40"
                                                                                ValidationGroup="save" onBlur="return RemoveSpecialChars(this);"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                           
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                <span class="mnd_text">First Name</span>
                                                            </td>
                                                            <td class="tdLabel150" valign="top">
                                                                <asp:TextBox ID="tbFirstName" runat="server" CssClass="text120" MaxLength="40"></asp:TextBox>
                                                            </td>
                                                            <td class="tdLabel80" valign="top">
                                                                Middle Name
                                                            </td>
                                                            <td class="tdLabel150" valign="top">
                                                                <asp:TextBox ID="tbMiddleName" runat="server" CssClass="text120" MaxLength="40"></asp:TextBox>
                                                            </td>
                                                            <td class="tdLabel70" valign="top">
                                                                <span class="mnd_text">Last Name</span>
                                                            </td>
                                                            <td  valign="top">
                                                                <asp:TextBox ID="tbLastName" runat="server" CssClass="text120" MaxLength="40"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="tbFirstName"
                                                                    ValidationGroup="save" SetFocusOnError="true" ErrorMessage="First Name is Required"
                                                                    CssClass="alert-text" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </td>
                                                            <td colspan="3">
                                                            </td>
                                                            <td>
                                                                <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="tbLastName"
                                                                    ValidationGroup="save" SetFocusOnError="true" ErrorMessage="Last Name is Required"
                                                                    CssClass="alert-text" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                Title
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <asp:TextBox ID="tbTitle" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                Address Line 1
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <asp:TextBox ID="tbAddr1" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                            </td>
                                                            <td class="tdLabel130" valign="top">
                                                                Address Line 2
                                                            </td>
                                                            <td class="tdLabel200" valign="top">
                                                                <asp:TextBox ID="tbAddr2" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150">
                                                                <asp:Label ID="lbAddr3" runat="server" Text="Address Line 3" />
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbAddr3" runat="server" MaxLength="40" CssClass="text200" ValidationGroup="save" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                City
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <asp:TextBox ID="tbCity" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                            </td>
                                                            <td class="tdLabel130" valign="top">
                                                                State/Province
                                                            </td>
                                                            <td class="tdLabel200" valign="top">
                                                                <asp:TextBox ID="tbState" runat="server" CssClass="text200" MaxLength="10"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                Country
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="tdLabel110">
                                                                            <asp:TextBox ID="tbCountryCode" runat="server" CssClass="text50" MaxLength="3" OnTextChanged="Country_TextChanged"
                                                                                ValidationGroup="save" AutoPostBack="true" onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                                            <asp:Button ID="btnCountry" runat="server" OnClientClick="javascript:openCountry();return false;"
                                                                                CssClass="browse-button"></asp:Button>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CustomValidator ID="cvCountry" runat="server" ControlToValidate="tbCountryCode"
                                                                                ErrorMessage="Invalid Country" Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel130" valign="top">
                                                                Postal
                                                            </td>
                                                            <td class="tdLabel200" valign="top">
                                                                <asp:TextBox ID="tbPostal" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <div class="tblspace_10">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150">
                                                                <asp:Label ID="lbBusinessPhone" runat="server" Text="Business Phone" />
                                                            </td>
                                                            <td class="tdLabel240">
                                                                <asp:TextBox ID="tbBusinessPhone" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                    runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save" />
                                                            </td>
                                                            <td class="tdLabel130">
                                                                Home Phone
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbPhone" runat="server" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                CssClass="text200" MaxLength="25"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150">
                                                                <asp:Label ID="lbTollFreePhone" runat="server" Text="Toll-free Phone" />
                                                            </td>
                                                            <td class="tdLabel240">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbTollFreePhone" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel130">
                                                                <asp:Label ID="lbOtherPhone" runat="server" Text="Other Phone" />
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbOtherPhone" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150">
                                                                <asp:Label ID="lbCellPhoneNum" runat="server" Text="Primary Mobile/Cell" />
                                                            </td>
                                                            <td class="tdLabel240">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbCellPhoneNum" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel130">
                                                                <asp:Label ID="lbCellPhoneNum2" runat="server" Text="Secondary Mobile/Cell" />
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbCellPhoneNum2" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                <asp:Label ID="lbBusinessEmail" runat="server" Text="Business E-mail" />
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbBusinessEmail" runat="server" MaxLength="250" CssClass="text200"
                                                                                ValidationGroup="save" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:RegularExpressionValidator ID="revBusinessEmail" runat="server" Display="Dynamic"
                                                                                ErrorMessage="Invalid Format" ControlToValidate="tbBusinessEmail" CssClass="alert-text"
                                                                                ValidationGroup="save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel130">
                                                                <asp:Label ID="lbPersonalEmail" runat="server" Text="Personal E-mail" />
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbPersonalEmail" runat="server" MaxLength="250" CssClass="text200"
                                                                                ValidationGroup="save" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:RegularExpressionValidator ID="revPersonalEmail" runat="server" Display="Dynamic"
                                                                                ErrorMessage="Invalid Format" ControlToValidate="tbPersonalEmail" CssClass="alert-text"
                                                                                ValidationGroup="save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150">
                                                                <asp:Label ID="lbOtherEmail" runat="server" Text="Other E-mail" />
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbOtherEmail" runat="server" MaxLength="250" CssClass="text200"
                                                                                ValidationGroup="save" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:RegularExpressionValidator ID="revOtherMail" runat="server" Display="Dynamic"
                                                                                ErrorMessage="Invalid Format" ControlToValidate="tbOtherEmail" CssClass="alert-text"
                                                                                ValidationGroup="save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150">
                                                                Business Fax
                                                            </td>
                                                            <td class="tdLabel240">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbFax" onKeyPress="return fnAllowPhoneFormat(this,event)" runat="server"
                                                                                CssClass="text200" MaxLength="25"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel130">
                                                                <asp:Label ID="lbHomeFax" runat="server" Text="Home Fax" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbHomeFax" onKeyPress="return fnAllowPhoneFormat(this,event)" runat="server"
                                                                    MaxLength="25" CssClass="text200" ValidationGroup="save" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150">
                                                                E-mail Address
                                                            </td>
                                                            <td class="tdLabel240">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbEmailId" runat="server" CssClass="text200" MaxLength="100"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:RegularExpressionValidator ID="revEmail" runat="server" Display="Dynamic" ErrorMessage="Invalid Format"
                                                                                ControlToValidate="tbEmailId" CssClass="alert-text" ValidationGroup="save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel130">
                                                                <asp:Label ID="lbWebsite" runat="server" Text="Web site" />
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbWebsite" runat="server" MaxLength="250" CssClass="text200" ValidationGroup="save" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <div class="tblspace_10">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td valign="top">
                                                                Additional Contact Information
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel140" valign="top">
                                                                <asp:TextBox ID="tbAdditionalContact" runat="server" TextMode="MultiLine" CssClass="textarea720x80"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <%-- </ItemTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>--%>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td align="left">
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlNotes" Width="100%" ExpandAnimation-Type="none" CollapseAnimation-Type="none"
                ViewStateMode="Inherit" runat="server" ClientIDMode="AutoID">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Notes" Expanded="false" CssClass="PanelHeaderStyle">
                        <Items>
                            <telerik:RadPanelItem>
                                <ContentTemplate>
                                    <table width="100%" cellspacing="0" class="note-box">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbNotes" runat="server" TextMode="MultiLine" CssClass="textarea-db">
                                                </asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <asp:Button ID="btnSaveChanges" Text="Save" runat="server" OnClick="SaveChanges_Click"
                            CssClass="button" ValidationGroup="save" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" CausesValidation="false" runat="server"
                            OnClick="Cancel_Click" CssClass="button" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnCode" runat="server" />
                        <asp:HiddenField ID="hdnCountryID" runat="server" />
                        <asp:HiddenField ID="hdnVendorID" runat="server" />
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
