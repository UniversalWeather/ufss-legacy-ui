﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Logistics
{
    public partial class VendorMasterPopUp : BaseSecuredPage
    {
        private string VendorCD;
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            string i = Request.QueryString["VendorCD"];

                            if (Request.QueryString["VendorCD"] != null)
                            {
                                if (Request.QueryString["VendorCD"].ToString() == "1")
                                {
                                    dgVendorCatalog.AllowMultiRowSelection = true;
                                }
                                else
                                {
                                    dgVendorCatalog.AllowMultiRowSelection = false;
                                }
                            }
                            else
                            {
                                dgVendorCatalog.AllowMultiRowSelection = false;
                            }
                        }

                        //Added for Reassign the Selected Value and highlight the specified row in the Grid            
                        if (!string.IsNullOrEmpty(Request.QueryString["VendorCD"]))
                        {
                            VendorCD = Request.QueryString["VendorCD"].ToUpper().Trim();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }

        }

        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgVendorCatalog_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objPayableVendorCatalogService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objVendor = objPayableVendorCatalogService.GetVendorInfo();
                            if (objVendor.ReturnFlag == true)
                            {
                                dgVendorCatalog.DataSource = objVendor.EntityList;
                                Session["VendorMasterPop"] = objVendor.EntityList;
                            }
                        }
                        if ((chkSearchActiveOnly.Checked == true) || (chkSearchHomebaseOnly.Checked == true))
                        {
                            DoSearchFilter(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }

        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgVendorCatalog;

                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Vendor);
                }
            }

        }
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(VendorCD))
                {

                    foreach (GridDataItem Item in dgVendorCatalog.MasterTableView.Items)
                    {
                        if (Item["VendorCD"].Text.Trim().ToUpper() == VendorCD)
                        {
                            Item.Selected = true;
                            break;
                        }
                    }
                }
                else
                {
                    if (dgVendorCatalog.MasterTableView.Items.Count > 0)
                    {
                        dgVendorCatalog.SelectedIndexes.Add(0);

                    }
                }
            }

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (dgVendorCatalog.Items.Count > 0)
                {
                    SelectItem();
                }
                else
                {
                    dgVendorCatalog.SelectedIndexes.Add(0);
                }
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DoSearchFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }

        private bool DoSearchFilter(bool IsDataBind)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient objPayableVendorCatalogService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objPayableVendor = objPayableVendorCatalogService.GetPayableVendorInfo();

                List<FlightPakMasterService.GetAllVendor> lstAccount = new List<FlightPakMasterService.GetAllVendor>();
                if (Session["VendorMasterPop"] != null)
                {
                    lstAccount = (List<FlightPakMasterService.GetAllVendor>)Session["VendorMasterPop"];
                }
                if (lstAccount.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstAccount = lstAccount.Where(x => x.IsInActive == true).ToList<GetAllVendor>(); }
                    if (chkSearchHomebaseOnly.Checked == true) { lstAccount = lstAccount.Where(x => x.HomeBaseCD == UserPrincipal.Identity._airportICAOCd).ToList<GetAllVendor>(); }
                    dgVendorCatalog.DataSource = lstAccount;
                }
                if (IsDataBind)
                {
                    dgVendorCatalog.DataBind();
                }
                return false;
            }
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            DoSearchFilter(true);
        }
        #endregion

        protected void dgVendorCatalog_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;
                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
            }
        }

        protected void dgVendorCatalog_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgVendorCatalog, Page.Session);
        }
    }
}