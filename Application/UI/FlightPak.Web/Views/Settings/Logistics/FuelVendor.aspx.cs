﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Globalization;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Text;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Logistics
{
    public partial class FuelVendor : BaseSecuredPage
    {
        private const bool IsValidateCustom = true;
        private ExceptionManager _exManager;
        private bool _fuelVendorPageNavigated ;
        public string StrErrUpload = string.Empty;
        private bool _selectLastModified ;
        #region "Page Event"
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgFuelVendor, dgFuelVendor, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgFuelVendor.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));
                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewFuelVendor);
                            // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgFuelVendor.Rebind();
                                ReadOnlyForm();
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                // Method to display first record in read only format
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelVendor);
                }
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgFuelVendor.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgFuelVendor.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgFuelVendor.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        #endregion

        #region "Grid Events"
        protected void dgFuelVendor_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient FuelVendorService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPakMasterService.FuelVendor> FuelVendorList = new List<FlightPakMasterService.FuelVendor>();
                            var FuelVendorServiceInfo = FuelVendorService.GetFuelVendor();
                            if (FuelVendorServiceInfo.ReturnFlag == true)
                            {
                                FuelVendorList = FuelVendorServiceInfo.EntityList.Where(x => x.IsDeleted == false).ToList<FlightPakMasterService.FuelVendor>();
                            }
                            dgFuelVendor.DataSource = FuelVendorList;
                            Session["FuelVendorList"] = FuelVendorList;
                            if ((chkSearchActiveOnly.Checked == true) && !IsPopUp)
                            {
                                SearchAndFilter(false);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelVendor);
                }
            }
        }
        protected void dgFuelVendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        _exManager.Process(() =>
                        {
                            GridDataItem item = dgFuelVendor.SelectedItems[0] as GridDataItem;
                            Session["FuelVendorID"] = item["FuelVendorID"].Text;
                            Session["VendorName"] = item["VendorName"].Text;
                            ReadOnlyForm();

                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelVendor);
                    }
                }
            }
        }
        protected void dgFuelVendor_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            string color = System.Configuration.ConfigurationManager.AppSettings["UWAColor"];
                            GridDataItem dataItem = e.Item as GridDataItem;
                            GridColumn column = dgFuelVendor.MasterTableView.GetColumn("VendorCD");
                            string VendorCD = dataItem["VendorCD"].Text;
                            if (VendorCD.ToUpper().Trim() == "UWA")
                            {
                                System.Drawing.ColorConverter colConvert = new System.Drawing.ColorConverter();
                                GridDataItem item = (GridDataItem)e.Item;
                                TableCell cell = (TableCell)item["VendorCD"];
                                cell.ForeColor = (System.Drawing.Color)colConvert.ConvertFromString(color);
                                cell.Font.Bold = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelVendor);
                }
            }
        }
        protected void dgFuelVendor_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        dgFuelVendor.ClientSettings.Scrolling.ScrollTop = "0";
                        _fuelVendorPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelVendor);
                }
            }
        }
        protected void dgFuelVendor_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (_fuelVendorPageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgFuelVendor, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelVendor);
                }
            }
        }
        #endregion

        #region "Control Events"
        #endregion

        #region "Private Functions"
        private void DefaultSelection(bool bindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(bindDataSwitch))
            {
                if (bindDataSwitch)
                {
                    dgFuelVendor.Rebind();
                }
                if (dgFuelVendor.MasterTableView.Items.Count > 0)
                {
                    if (Session["FuelVendorID"] == null)
                    {
                        dgFuelVendor.SelectedIndexes.Add(0);
                        Session["FuelVendorID"] = dgFuelVendor.Items[0].GetDataKeyValue("FuelVendorID").ToString();
                        Session["VendorName"] = dgFuelVendor.Items[0].GetDataKeyValue("VendorName").ToString();
                    }

                    if (dgFuelVendor.SelectedIndexes.Count == 0)
                        dgFuelVendor.SelectedIndexes.Add(0);

                    ReadOnlyForm();
                }
                else
                {
                    ClearForm();
                    EnableForm(false);
                }
            }
        }

        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["FuelVendorID"] != null)
                {
                    string ID = Session["FuelVendorID"].ToString();
                    foreach (GridDataItem Item in dgFuelVendor.MasterTableView.Items)
                    {
                        if (Item["FuelVendorID"].Text.Trim() == ID.Trim())
                        {
                            Item.Selected = true;
                            break;
                        }
                    }
                }
                else
                {
                    DefaultSelection(false);
                }
            }
        }

        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                tbCode.Enabled = enable;
                tbDescription.Enabled = enable;
                tbName.Enabled = enable;
                tbfilelocation.Enabled = enable;
                tbServerInformation.Enabled = enable;
                chkInactive.Enabled = enable;
            }
        }

        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbCode.Text = string.Empty;
                hdnFuelVendorID.Value = string.Empty;
                tbDescription.Text = string.Empty;
                tbfilelocation.Text = string.Empty;
                tbName.Text = string.Empty;
                tbServerInformation.Text = string.Empty;
                chkInactive.Checked = false;
            }
        }

        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                LoadControlData();
                EnableForm(false);
            }
        }

        protected void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["FuelVendorID"] != null)
                {
                    using (var client = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        long fuelVendorId = Convert.ToInt64(Session["FuelVendorID"].ToString().Trim());
                        var retFuelVendorInfo = client.GetFuelVendorInfoByFilter(fuelVendorId, "");
                        if (retFuelVendorInfo.ReturnFlag && retFuelVendorInfo.EntityInfo != null)
                        {
                            var fuelVendorInfo = retFuelVendorInfo.EntityInfo;
                            tbCode.Text = fuelVendorInfo.VendorCD.ToUpper();
                            hdnFuelVendorID.Value = fuelVendorInfo.FuelVendorID.ToString();
                            tbName.Text = fuelVendorInfo.VendorName;
                            if (fuelVendorInfo.VendorDescription != null)
                            {
                                tbDescription.Text = fuelVendorInfo.VendorDescription;
                            }
                            if (fuelVendorInfo.ServerInfo != null)
                            {
                                tbServerInformation.Text = fuelVendorInfo.ServerInfo;
                            }
                            if (fuelVendorInfo.FileLocation != null)
                            {
                                tbfilelocation.Text = fuelVendorInfo.FileLocation;
                            }
                            Label lbLastUpdatedUser = (Label)dgFuelVendor.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                            lbLastUpdatedUser.Text = fuelVendorInfo.LastUpdUID != null ? System.Web.HttpUtility.HtmlEncode("Last Updated User: " + fuelVendorInfo.LastUpdUID) : string.Empty;
                            if (fuelVendorInfo.LastUpdTS != null)
                            {
                                lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(fuelVendorInfo.LastUpdTS.ToString().Trim())));
                            }
                            chkInactive.Checked = fuelVendorInfo.IsInActive != null && Convert.ToBoolean(fuelVendorInfo.IsInActive.ToString(), CultureInfo.CurrentCulture);
                            lbColumnName1.Text = "Fuel Vendor Code";
                            lbColumnName2.Text = "Name";
                            lbColumnValue1.Text = fuelVendorInfo.VendorCD;
                            lbColumnValue2.Text = fuelVendorInfo.VendorName;
                            if (fuelVendorInfo.BaseFileName != null)
                            {
                                lblFilename.Text = fuelVendorInfo.BaseFileName.Trim();
                                hdnUploadedFileName.Value = fuelVendorInfo.BaseFileName.Trim();
                            }
                        }
                    }
                }
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        //if (e.Initiator.ID.IndexOf("btnSave", StringComparison.Ordinal) > -1)
                        //{
                        //    e.Updated = dgFuelVendor;
                        //    //To select item which they selected during navigating to transport,hotel,catering
                        //}
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelVendor);
                }
            }
        }

        private void DisableLinks()
        {
            chkSearchActiveOnly.Enabled = false;
        }

        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient fpkMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int fullItemIndex = 0;

            var fuelVendorrValue = fpkMstService.GetFuelVendor();
            Int64 primaryKeyValue = CheckOriginForPreSelection(isFromSearch, fuelVendorrValue);
            IEnumerable<FlightPakMasterService.FuelVendor> filteredList = GetFilteredList(fuelVendorrValue);

            foreach (var item in filteredList)
            {
                if (primaryKeyValue == item.FuelVendorID)
                {
                    fullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int pageSize = dgFuelVendor.PageSize;
            int pageNumber = fullItemIndex / pageSize;
            int itemIndex = fullItemIndex % pageSize;

            dgFuelVendor.CurrentPageIndex = pageNumber;
            dgFuelVendor.SelectedIndexes.Add(itemIndex);
        }

        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfFuelVendor fuelVendorrValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["FuelVendorID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = fuelVendorrValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().FuelVendorID;
                Session["FuelVendorID"] = PrimaryKeyValue;
                Session["VendorName"] = fuelVendorrValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().VendorName;


            }

            return PrimaryKeyValue;
        }

        private IEnumerable<FlightPakMasterService.FuelVendor> GetFilteredList(ReturnValueOfFuelVendor fuelVendorrValue)
        {
            List<FlightPakMasterService.FuelVendor> filteredList = new List<FlightPakMasterService.FuelVendor>();

            if (fuelVendorrValue.ReturnFlag)
            {
                filteredList = fuelVendorrValue.EntityList.Where(x => x.IsDeleted == false).ToList<FlightPakMasterService.FuelVendor>();
            }

            if (chkSearchActiveOnly.Checked && !IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkSearchActiveOnly.Checked) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<FlightPakMasterService.FuelVendor>(); }
                }
            }

            return filteredList;
        }
        #endregion

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        SearchAndFilter(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Passenger);
                }
            }
        }
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.FuelVendor> lstFuelVendor = new List<FlightPakMasterService.FuelVendor>();
                if (Session["FuelVendorList"] != null)
                {
                    lstFuelVendor = (List<FlightPakMasterService.FuelVendor>)Session["FuelVendorList"];
                }
                if (lstFuelVendor.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstFuelVendor = lstFuelVendor.Where(x => x.IsInActive == false).ToList<FlightPakMasterService.FuelVendor>(); }
                    dgFuelVendor.DataSource = lstFuelVendor;
                    if (IsDataBind)
                    {
                        dgFuelVendor.DataBind();
                    }
                }
                //LoadControlData();
                return false;
            }
        }
        #endregion


    }
}