﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Common;
using FlightPak.Common.Constants;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.Framework.Helpers;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Telerik.Web.UI;
using FlightpakFuel = FlightPak.Web.CommonService.FlightpakFuel;

namespace FlightPak.Web.Views.Settings.Logistics
{
    public partial class HistoricalFuelPrice : BaseSecuredPage
    {
        #region "Variable Declaration"

        private ExceptionManager _exManager;

        #endregion

        #region "Page Eventes"

        protected void Page_Load(object sender, EventArgs e)
        {
            // Store the clientID of the grid to reference it later on the client
            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgFuelPrice, dgFuelPrice, RadAjaxLoadingPanel1);
            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSearch, dgFuelPrice, RadAjaxLoadingPanel1);
            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, dgFuelPrice, null);
            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, pnlExternalForm, RadAjaxLoadingPanel1);
            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgFuelPrice.ClientID));
            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));
            CultureInfo culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            culture.DateTimeFormat.ShortDatePattern = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
            RadDatePicker1.Culture = culture;
            if (!IsPostBack)
            {
                CheckAutorization(Permission.Database.ViewHistoricalFuelFileData);
                if (Request.QueryString["fuelVendorID"] != null)
                    hdnVendorId.Value = Request.QueryString["fuelVendorID"];
                if (Request.QueryString["fuelVendorName"] != null)
                    tbVendorName.Text = Request.QueryString["fuelVendorName"];
                if (Request.QueryString["fuelVendorCode"] != null)
                    tbVendorCode.Text = Request.QueryString["fuelVendorCode"];
                if (Request.QueryString["fuelVendorDescription"] != null)
                    tbDescription.Text = Request.QueryString["fuelVendorDescription"];

                DefaultSelection(true);

            }
        }

        #endregion

        #region "Grid Eventes "

        protected void dgFuelPrice_OnInit(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        using (var client = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            long fuelVendorId = 0;
                            if (Request.QueryString["fuelVendorID"] != null)
                                fuelVendorId = Convert.ToInt64(Request.QueryString["fuelVendorID"]);

                            if (fuelVendorId != 0)
                            {
                                var retObj = client.CountNumberOfFlightpakFuelRecordsForVendor(fuelVendorId);
                                dgFuelPrice.VirtualItemCount = retObj.ReturnFlag ? retObj.EntityInfo : 100000;
                            } else
                            {
                                dgFuelPrice.VirtualItemCount = 100000;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.HistoricalFuelPrice);
                }
            }
        }

        protected void dgFuelPrice_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        using (var client = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            long? airportId = null;
                            string fbo = string.Empty;
                            DateTime? effectivedate = null;
                            if (!string.IsNullOrEmpty(hdnSearchIcao.Value))
                            {
                                airportId = Convert.ToInt64(hdnSearchIcao.Value);
                            }
                            if (!string.IsNullOrEmpty(tbSearchFbo.Text))
                            {
                                fbo = tbSearchFbo.Text.Trim();
                            }
                            if (!string.IsNullOrEmpty(tbSearchEffectiveDate.Text))
                            {
                                effectivedate = DateTime.ParseExact(tbSearchEffectiveDate.Text, UserPrincipal.Identity._fpSettings._ApplicationDateFormat, CultureInfo.InvariantCulture);
                            }

                            long fuelVendorId = Convert.ToInt64(hdnVendorId.Value);
                            var retObj = client.GetFlightpakFuelListByFilter(fuelVendorId, fbo, airportId, effectivedate, true, dgFuelPrice.PageSize, dgFuelPrice.CurrentPageIndex + 1);
                            if (retObj.ReturnFlag)
                            {
                                List<FlightpakFuelHistoricalData> oFlightpakFuelHistoricalData = retObj.EntityList;
                                dgFuelPrice.DataSource = oFlightpakFuelHistoricalData.Select(t => 
                                    new { t.FlightpakFuelID, t.CustomerID, t.DepartureICAOID, t.FBOName, GallonFrom = string.Format("{0:0}", t.GallonFrom), GallonTO = string.Format("{0:0}", t.GallonTO), 
                                          EffectiveDT = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", t.EffectiveDT),
                                          t.UnitPrice,
                                          t.FuelVendorID,
                                          t.DepartureICAO,
                                          t.FromTable,
                                          t.AirportName,
                                          t.IsActive,
                                          FileUploadedOn = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", t.LastUpdTS)
                                    }).ToList();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.HistoricalFuelPrice);
                }
            }
        }

        protected void dgFuelPrice_OnItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton deleteButton = (e.Item as GridCommandItem).FindControl("lnkDelete") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(deleteButton, dgFuelPrice, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.HistoricalFuelPrice);
                }
            }
        }

        protected void dgFuelPrice_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (hdnSave.Value == "Update")
            {
                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        _exManager.Process(() =>
                        {
                            if (dgFuelPrice.MasterTableView.Items.Count > 0)
                            {
                                GridDataItem item = dgFuelPrice.SelectedItems[0] as GridDataItem;
                                Session[FuelFileConstants.FlightpakFuelID] = item["FlightpakFuelID"].Text;
                                ReadOnlyForm();
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.HistoricalFuelPrice);
                    }
                }
            }
        }

        protected void dgFuelPrice_OnPageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        dgFuelPrice.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.HistoricalFuelPrice);
                }
            }
        }

        protected void dgFuelPrice_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {

                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                if ((Session[FuelFileConstants.FlightpakFuelID] != null) && (dgFuelPrice.SelectedItems.Count > 0))
                                {
                                    e.Canceled = true;
                                    e.Item.Selected = true;
                                    DisplayEditForm();
                                }
                                else
                                {
                                    e.Canceled = true;
                                    dgFuelPrice.SelectedIndexes.Clear();
                                    DisplayInsertForm();
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgFuelPrice.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.HistoricalFuelPrice);
                }
            }
        }

        protected void dgFuelPrice_OnUpdateCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    e.Canceled = true;
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        if ((Session[FuelFileConstants.FlightpakFuelID] != null))
                        {
                            using (var client = new MasterCatalogServiceClient())
                            {
                                var oFlightpakFuel = new FlightPakMasterService.FlightpakFuel();
                                var oHistoricalFlightpakFuel = new FlightPakMasterService.HistoricalFuelPrice();
                                string updateIn = hdnFromTable.Value;
                                if (!string.IsNullOrEmpty(updateIn))
                                {
                                    if (updateIn.ToLower() == "historicalfuelprice")
                                    {
                                        oHistoricalFlightpakFuel= GetHistoricalFlightpakFuel(oHistoricalFlightpakFuel);
                                        ReturnValueOfHistoricalFuelPrice oReturnValueOfFlightpakFuel =client.UpdateToHistoricalFlightpakFuel(oHistoricalFlightpakFuel);
                                        if (oReturnValueOfFlightpakFuel.ReturnFlag)
                                        {
                                            hdnIsSaved.Value = "true";
                                            ReadOnlyForm();
                                        } 
                                        else
                                        {
                                            ProcessErrorMessage(oReturnValueOfFlightpakFuel.ErrorMessage, ModuleNameConstants.Database.FuelVendor);
                                        }
                                    } else
                                    {
                                        oFlightpakFuel = GetFlightpakFuel(oFlightpakFuel);
                                        ReturnValueOfFlightpakFuel oReturnValueOfFlightpakFuel = hdnSave.Value == "Save" ? client.AddFlightpakFuel(oFlightpakFuel) : client.UpdateFlightpakFuel(oFlightpakFuel);
                                        if (oReturnValueOfFlightpakFuel.ReturnFlag)
                                        {
                                            hdnIsSaved.Value = "true";
                                            ReadOnlyForm();
                                        } 
                                        else
                                        {
                                            ProcessErrorMessage(oReturnValueOfFlightpakFuel.ErrorMessage, ModuleNameConstants.Database.FuelVendor);
                                        }
                                    }

                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.HistoricalFuelPrice);
                }
            }
        }

        protected void dgFuelPrice_OnInsertCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        e.Canceled = true;
                        using (var client = new MasterCatalogServiceClient())
                        {
                            ReturnValueOfFlightpakFuel oReturnValueOfFlightpakFuel;
                            FlightPakMasterService.FlightpakFuel oFlightpakFuel=new FlightPakMasterService.FlightpakFuel();
                            oFlightpakFuel = GetFlightpakFuel(oFlightpakFuel);
                            if (hdnSave.Value == "Save")
                            {
                                oReturnValueOfFlightpakFuel = client.AddFlightpakFuel(oFlightpakFuel);
                            }
                            else
                            {
                                oReturnValueOfFlightpakFuel = client.UpdateFlightpakFuel(oFlightpakFuel);
                            }
                            if (oReturnValueOfFlightpakFuel.ReturnFlag)
                            {
                                hdnIsSaved.Value = "true";
                                ReadOnlyForm();
                            }
                            else
                            {
                                ProcessErrorMessage(oReturnValueOfFlightpakFuel.ErrorMessage, ModuleNameConstants.Database.FuelVendor);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.HistoricalFuelPrice);
                }
            }
        }

        protected void dgFuelPrice_OnDeleteCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session[FuelFileConstants.FlightpakFuelID] != null)
                        {
                            using (MasterCatalogServiceClient client = new MasterCatalogServiceClient())
                            {
                                var oFlightpakFuel = new FlightPakMasterService.FlightpakFuel();
                                hdnSave.Value = "Update";
                                var oHistoricalFlightpakFuel = new FlightPakMasterService.HistoricalFuelPrice();
                                string updateIn = hdnFromTable.Value;
                                if (!string.IsNullOrEmpty(updateIn))
                                {
                                    if (updateIn.ToLower() == "historicalfuelprice")
                                    {
                                        oHistoricalFlightpakFuel = GetHistoricalFlightpakFuel(oHistoricalFlightpakFuel);
                                        client.DeleteToHistoricalFlightpakFuel(oHistoricalFlightpakFuel);
                                    }
                                    else
                                    {
                                        oFlightpakFuel = GetFlightpakFuel(oFlightpakFuel);
                                        client.DeleteFlightpakFuel(oFlightpakFuel);
                                    }
                                }
                                Session[FuelFileConstants.FlightpakFuelID] = null;
                                DefaultSelection(false);
                                hdnSave.Value = string.Empty;
                            }
                        }
                    }, Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.HistoricalFuelPrice);
                }

            }
        }
        #endregion

        #region "Private Function"

        private void DefaultSelection(bool bindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(bindDataSwitch))
            {
                if (bindDataSwitch)
                {
                    dgFuelPrice.Rebind();
                }
                if (dgFuelPrice.MasterTableView.Items.Count > 0)
                {
                    if (Session[FuelFileConstants.FlightpakFuelID] == null)
                    {
                        dgFuelPrice.SelectedIndexes.Add(0);
                        Session[FuelFileConstants.FlightpakFuelID] = dgFuelPrice.Items[0].GetDataKeyValue("FlightpakFuelID").ToString();
                        dgFuelPrice.Items[0].Selected = true;
                    }
                    if (dgFuelPrice.SelectedIndexes.Count == 0)
                    {
                        dgFuelPrice.SelectedIndexes.Add(0);
                    }
                    ReadOnlyForm();
                } else
                {
                    ClearForm();
                    EnableForm(false);
                }
            }
        }

        private void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                LoadControlData();
                EnableForm(false);
            }
        }

        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                var flightpakFuelInfo=new FlightPakMasterService.FlightpakFuel();
                if (Session[FuelFileConstants.FlightpakFuelID] != null && dgFuelPrice.SelectedItems.Count>0)
                {
                    long flightpakFuelId = Convert.ToInt64(Session[FuelFileConstants.FlightpakFuelID]);
                    try
                    {
                        _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        _exManager.Process(() =>
                        {
                            foreach (GridDataItem item in dgFuelPrice.SelectedItems)
                            {
                                hdnFlightpakFuelId.Value = Convert.ToString(item.GetDataKeyValue("FlightpakFuelID"));
                                hdnFromTable.Value = Convert.ToString(item.GetDataKeyValue("FromTable"));
                                tbAirportIcao.Text = Convert.ToString(item.GetDataKeyValue("DepartureICAO"));
                                lblAirportIcaoDescription.Text = Convert.ToString(item.GetDataKeyValue("AirportName"));
                                hdnAirportIcao.Value = Convert.ToString(item.GetDataKeyValue("DepartureICAOID"));
                                tbFboName.Text = Convert.ToString(item.GetDataKeyValue("FBOName"));
                                tbGallonFrom.Text = Convert.ToString(item.GetDataKeyValue("GallonFrom"));
                                tbGallonTo.Text = Convert.ToString(item.GetDataKeyValue("GallonTO"));
                                tbEffectiveDate.Text = Convert.ToString(item.GetDataKeyValue("EffectiveDT"));
                                tbUnitPrice.Text = Convert.ToString(item.GetDataKeyValue("UnitPrice"));
                                chkIsActive.Checked =item.GetDataKeyValue("IsActive")!=null && Convert.ToBoolean(item.GetDataKeyValue("IsActive"));
                            }

                                
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.HistoricalFuelPrice);
                    }
                }
            }
        }

        private void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                tbAirportIcao.Enabled = enable;
                tbFboName.Enabled = enable;
                tbGallonFrom.Enabled = enable;
                tbGallonTo.Enabled = enable;
                tbEffectiveDate.Enabled = enable;
                tbUnitPrice.Enabled = enable;
                chkIsActive.Enabled = enable;
                btnSave.Visible = enable;
                btnCancel.Visible = enable;
                btnAirportIcao.Enabled = enable;
                btnAirportIcao.CssClass = enable ? "browse-button" : "browse-button-disabled";
            }
        }

        private void ClearForm()
        {
            hdnFlightpakFuelId.Value = string.Empty;
            tbAirportIcao.Text = string.Empty;
            hdnAirportIcao.Value = string.Empty;
            tbFboName.Text = string.Empty;
            tbGallonFrom.Text = string.Empty;
            tbGallonTo.Text = string.Empty;
            tbEffectiveDate.Text = string.Empty;
            tbUnitPrice.Text = string.Empty;
            chkIsActive.Checked = false;
            lblAirportIcaoDescription.Text = "";
        }

        private void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                hdnSave.Value = "Update";
                LoadControlData();
                EnableForm(true);
            }
        }

        private void DisplayInsertForm()
        {
            hdnSave.Value = "Save";
            ClearForm();
            EnableForm(true);
        }

        private FlightPakMasterService.FlightpakFuel GetFlightpakFuel(FlightPakMasterService.FlightpakFuel oFlightpakFuel)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFlightpakFuel))
            {
                if (hdnSave.Value == "Update")
                {
                    if (!string.IsNullOrEmpty(hdnFlightpakFuelId.Value))
                    {
                        oFlightpakFuel.FlightpakFuelID = Convert.ToInt64(hdnFlightpakFuelId.Value);
                        Session[FuelFileConstants.FlightpakFuelID] = hdnFlightpakFuelId.Value;
                    }
                }
                if (!string.IsNullOrEmpty(hdnAirportIcao.Value))
                {
                    oFlightpakFuel.DepartureICAOID = Convert.ToInt64(hdnAirportIcao.Value);
                }
                if (!string.IsNullOrEmpty(tbEffectiveDate.Text))
                {
                    oFlightpakFuel.EffectiveDT = DateTime.ParseExact(tbEffectiveDate.Text, UserPrincipal.Identity._fpSettings._ApplicationDateFormat, CultureInfo.InvariantCulture);
                }
                if (!string.IsNullOrEmpty(tbFboName.Text))
                {
                    oFlightpakFuel.FBOName = tbFboName.Text;
                }
                if (!string.IsNullOrEmpty(hdnVendorId.Value))
                {
                    oFlightpakFuel.FuelVendorID = Convert.ToInt64(hdnVendorId.Value);
                }
                if (!string.IsNullOrEmpty(tbGallonFrom.Text))
                {
                    oFlightpakFuel.GallonFrom = Convert.ToDecimal(tbGallonFrom.Text);
                }
                if (!string.IsNullOrEmpty(tbGallonTo.Text))
                {
                    oFlightpakFuel.GallonTO = Convert.ToDecimal(tbGallonTo.Text);
                }
                if (!string.IsNullOrEmpty(tbUnitPrice.Text))
                {
                    oFlightpakFuel.UnitPrice = Convert.ToDecimal(tbUnitPrice.Text);
                }
                oFlightpakFuel.IsActive = chkIsActive.Checked;

            }
            return oFlightpakFuel;
        }

        private FlightPakMasterService.HistoricalFuelPrice GetHistoricalFlightpakFuel(FlightPakMasterService.HistoricalFuelPrice oFlightpakFuel)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFlightpakFuel))
            {
                if (hdnSave.Value == "Update")
                {
                    if (!string.IsNullOrEmpty(hdnFlightpakFuelId.Value))
                    {
                        oFlightpakFuel.HistoricalFuelID = Convert.ToInt64(hdnFlightpakFuelId.Value);
                        Session[FuelFileConstants.FlightpakFuelID] = hdnFlightpakFuelId.Value;
                    }
                }
                if (!string.IsNullOrEmpty(hdnAirportIcao.Value))
                {
                    oFlightpakFuel.DepartureICAOID = Convert.ToInt64(hdnAirportIcao.Value);
                }
                if (!string.IsNullOrEmpty(tbEffectiveDate.Text))
                {
                    oFlightpakFuel.EffectiveDT = DateTime.ParseExact(tbEffectiveDate.Text, UserPrincipal.Identity._fpSettings._ApplicationDateFormat, CultureInfo.InvariantCulture);
                }
                if (!string.IsNullOrEmpty(tbFboName.Text))
                {
                    oFlightpakFuel.FBOName = tbFboName.Text;
                }
                if (!string.IsNullOrEmpty(hdnVendorId.Value))
                {
                    oFlightpakFuel.FuelVendorID = Convert.ToInt64(hdnVendorId.Value);
                }
                if (!string.IsNullOrEmpty(tbGallonFrom.Text))
                {
                    oFlightpakFuel.GallonFrom = Convert.ToDecimal(tbGallonFrom.Text);
                }
                if (!string.IsNullOrEmpty(tbGallonTo.Text))
                {
                    oFlightpakFuel.GallonTO = Convert.ToDecimal(tbGallonTo.Text);
                }
                if (!string.IsNullOrEmpty(tbUnitPrice.Text))
                {
                    oFlightpakFuel.UnitPrice = Convert.ToDecimal(tbUnitPrice.Text);
                }
                oFlightpakFuel.IsActive = chkIsActive.Checked;

            }
            return oFlightpakFuel;
        }

        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[FuelFileConstants.FlightpakFuelID] != null)
                {
                    string ID = Session[FuelFileConstants.FlightpakFuelID].ToString();
                    foreach (GridDataItem Item in dgFuelPrice.MasterTableView.Items)
                    {
                        if (Item["FlightpakFuelID"].Text.Trim() == ID.Trim())
                        {
                            Item.Selected = true;
                            break;
                        }
                    }
                }
                else
                {
                    DefaultSelection(false);
                }
            }
        }
        #endregion

        #region "Control Event"

        protected void tbSearchIcao_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        using (var client = new MasterCatalogServiceClient())
                        {
                            if (!string.IsNullOrEmpty(tbSearchIcao.Text))
                            {
                                var airportRetInfo = client.GetAirportByAirportICaoID(tbSearchIcao.Text);
                                if (airportRetInfo.ReturnFlag && airportRetInfo.EntityList.Count > 0)
                                {
                                    GetAllAirport airportInfo = airportRetInfo.EntityList.FirstOrDefault();
                                    if (airportInfo != null)
                                    {
                                        customValidatorSearchIcao.IsValid = true;
                                        lblSearchIcaoDescription.Text = airportInfo.AirportName;
                                        hdnSearchIcao.Value = Convert.ToString(airportInfo.AirportID);
                                    } else
                                    {
                                        customValidatorSearchIcao.IsValid = false;
                                        lblSearchIcaoDescription.Text = string.Empty;
                                        hdnSearchIcao.Value = string.Empty;
                                    }
                                } 
                                else
                                {
                                    customValidatorSearchIcao.IsValid = false;
                                    lblSearchIcaoDescription.Text = string.Empty;
                                    hdnSearchIcao.Value = string.Empty;
                                }
                            }
                            else
                            {
                                customValidatorSearchIcao.IsValid = true;
                                lblSearchIcaoDescription.Text = string.Empty;
                                hdnSearchIcao.Value = string.Empty;
                            }
                        }
                    }, Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.HistoricalFuelPrice);
                }
            }
        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                dgFuelPrice.Rebind();
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            (dgFuelPrice.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgFuelPrice.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                    dgFuelPrice.Rebind();
                    hdnSave.Value = "";
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.HistoricalFuelPrice);
                }
            }
        }

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        hdnSave.Value = "";
                        DefaultSelection(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.HistoricalFuelPrice);
                }
            }
        }

        protected void tbAirportIcao_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        using (var client = new MasterCatalogServiceClient())
                        {
                            if (!string.IsNullOrEmpty(tbAirportIcao.Text))
                            {
                                var airportRetInfo = client.GetAirportByAirportICaoID(tbAirportIcao.Text);
                                if (airportRetInfo.ReturnFlag && airportRetInfo.EntityList.Count > 0)
                                {
                                    GetAllAirport airportInfo = airportRetInfo.EntityList.FirstOrDefault();
                                    if (airportInfo != null)
                                    {
                                        customValidatorAirportIcao.IsValid = true;
                                        lblAirportIcaoDescription.Text = airportInfo.AirportName;
                                        hdnAirportIcao.Value = Convert.ToString(airportInfo.AirportID);
                                    }
                                    else
                                    {
                                        customValidatorAirportIcao.IsValid = false;
                                        lblAirportIcaoDescription.Text = string.Empty;
                                        hdnAirportIcao.Value = string.Empty;
                                    }
                                }
                                else
                                {
                                    customValidatorAirportIcao.IsValid = false;
                                    lblAirportIcaoDescription.Text = string.Empty;
                                    hdnAirportIcao.Value = string.Empty;
                                }
                            }
                            else
                            {
                                customValidatorAirportIcao.IsValid = true;
                                lblAirportIcaoDescription.Text = string.Empty;
                                hdnAirportIcao.Value = string.Empty;
                            }
                        }
                    }, Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.HistoricalFuelPrice);
                }
            }
        }

        #endregion
    }
}