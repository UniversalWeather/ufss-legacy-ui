﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    ValidateRequest="false" AutoEventWireup="true" CodeBehind="PayableVendor.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Logistics.PayableVendor" ClientIDMode="AutoID"
    MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript">
        function OnClientClick(strPanelToExpand) {

            var PanelBar1 = $find("<%= pnlNotes.ClientID %>");
            var PanelBar2 = $find("<%= pnlMainContact.ClientID %>");
            var PanelBar3 = $find("<%= pnlMaintenance.ClientID %>");
            PanelBar1.get_items().getItem(0).set_expanded(false);
            PanelBar2.get_items().getItem(0).set_expanded(false);
            PanelBar3.get_items().getItem(0).set_expanded(false);
            if (strPanelToExpand == "Notes") {
                PanelBar1.get_items().getItem(0).set_expanded(true);
            }
            else if (strPanelToExpand == "Main Contact") {
                PanelBar2.get_items().getItem(0).set_expanded(true);
            }
            return false;
        }

        function File_onchange() {
            __doPostBack('__Page', 'LoadImage');
        }
        function GetBrowserName() {
            document.getElementById('<%=hdnBrowserName.ClientID%>').value = browserName();
        }
        function CheckName() {
            var nam = document.getElementById('<%=tbImgName.ClientID%>').value;
            if (nam != "") {
                document.getElementById("<%=fileUL.ClientID %>").disabled = false;
            }
            else {
                document.getElementById("<%=fileUL.ClientID %>").disabled = true;
            }
        }
        // this function is related to Image 
        function OpenRadWindow() {
            var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
            document.getElementById('<%=ImgPopup.ClientID%>').src = document.getElementById('<%=imgFile.ClientID%>').src;
            oWnd.show();
        }
        // this function is related to Image 
        function CloseRadWindow() {
            var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
            document.getElementById('<%=ImgPopup.ClientID%>').src = null;
            oWnd.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            var imgUrl = null;
            function confirmCallBackFn(arg) {

            }

            function openWin(url, value, radWin) {

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url + value, radWin);
            }
            function openHomeBase() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Airports/AirportMasterPopup.aspx?IcaoID=" + document.getElementById("<%=tbHomeBase.ClientID%>").value, "radAirportPopup");
                oWnd.add_close(OnClientCloseAirportMasterPopup);
            }
            function openClosestIcao() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Airports/AirportMasterPopup.aspx?IcaoID=" + document.getElementById("<%=tbClosestIcao.ClientID%>").value, "radAirportPopup");
                oWnd.add_close(OnClientCloseAirportMasterPopup1);
            }
            function openCountry() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Company/CountryMasterPopup.aspx?CountryCD=" + document.getElementById("<%=tbBillCountry.ClientID%>").value, "RadWindow2");

            }
            function openMetro() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Airports/MetroCityPopup.aspx?MetroCD=" + document.getElementById("<%=tbMetro.ClientID%>").value, "RadWindow3");

            }

            function ConfirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); //GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                }
            }
            function OnClientCloseAirportMasterPopup(oWnd, args) {
                var combo = $find("<%= tbHomeBase.ClientID %>");
                //get the transferred arguments.
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = arg.AirportID;
                    }
                    else {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = "";
                        document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = "";
                    }
                }
            }
            function OnClientCloseAirportMasterPopup1(oWnd, args) {
                var combo = $find("<%= tbClosestIcao.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClosestIcao.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=cvClosestIcao.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnAirportID.ClientID%>").value = arg.AirportID;

                    }
                    else {
                        document.getElementById("<%=tbClosestIcao.ClientID%>").value = "";
                        document.getElementById("<%=cvClosestIcao.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnAirportID.ClientID%>").value = "";
                    }
                }

            }
            function OnClientCloseCountryPopup(oWnd, args) {
                var combo = $find("<%= tbBillCountry.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbBillCountry.ClientID%>").value = arg.CountryCD;
                        document.getElementById("<%=cvBillCountry.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnCountryID.ClientID%>").value = arg.CountryID;
                    }
                    else {
                        document.getElementById("<%=tbBillCountry.ClientID%>").value = "";
                        document.getElementById("<%=cvBillCountry.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnCountryID.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function printImage() {
                var image = document.getElementById('<%=ImgPopup.ClientID%>');
                PrintWindow(image);
            }
            function ImageDeleteConfirm(sender, args) {
                var ReturnValue = false;
                var ddlImg = document.getElementById('<%=ddlImg.ClientID%>');
                if (ddlImg.length > 0) {
                    var ImageName = ddlImg.options[ddlImg.selectedIndex].value;
                    var Msg = "Are you sure you want to delete document type " + ImageName + " ?";
                    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                        if (shouldSubmit) {
                            ReturnValue = false;
                            this.click();
                        }
                        else {
                            ReturnValue = false;
                        }
                    });
                    var oManager = $find("<%= RadWindowManager1.ClientID %>"); 
                    oManager.radconfirm(Msg, callBackFunction, 400, 100, null, "Payable Vendor"); 
                    args.set_cancel(true);
                }
                return ReturnValue;
            }

            function OnClientCloseMetroCityPopup(oWnd, args) {
                var combo = $find("<%= tbMetro.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbMetro.ClientID%>").value = arg.MetroCD;
                        document.getElementById("<%=cvMetro.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnMetroID.ClientID%>").value = arg.MetroID;
                    }
                    else {
                        document.getElementById("<%=tbMetro.ClientID%>").value = "";
                        document.getElementById("<%=cvMetro.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnMetroID.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            
            $(document).ready(function(){
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");

                    if (redirectUrl == undefined || redirectUrl == 'undefined')
                        redirectUrl = '';

                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Airport" OnClientClose="OnClientCloseAirportMasterPopup" AutoSize="true"
                KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings//Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <%--<telerik:RadWindow ID="RadWindow4" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Airport" OnClientClose="OnClientCloseAirportMasterPopup1" AutoSize="true"
                KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings//Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>--%>
            <telerik:RadWindow ID="RadWindow2" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Country" OnClientClose="OnClientCloseCountryPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow3" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Metro" OnClientClose="OnClientCloseMetroCityPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/MetroCityPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadwindowImagePopup" runat="server" VisibleOnPageLoad="false"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                Behaviors="close" Title="Document Image" OnClientClose="CloseRadWindow">
                <ContentTemplate>
                    <div>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="float_printicon">
                                        <asp:ImageButton ID="ibtnPrint" runat="server" ImageUrl="~/App_Themes/Default/images/print.png"
                                            AlternateText="Print" OnClientClick="javascript:printImage();return false;" /></div>
                                    <asp:Image ID="ImgPopup" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Payables Vendor</span> <span class="tab-nav-icons">
                        <%-- <asp:LinkButton ID="lbtnReports" runat="server" title="Preview Report" class="search-icon"></asp:LinkButton>
                        <asp:LinkButton ID="lbtnSaveReports" runat="server" title="Export Report" class="save-icon"></asp:LinkButton>--%>
                        <a href="../../Help/ViewHelp.aspx?Screen=PayableVendorHelp" class="help-icon"
                            target="_blank" title="Help"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlPayableVendorForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td>
                        <div class="fleet_select">
                            <asp:LinkButton ID="lnkPayableVendorContacts" CssClass="fleet_link" runat="server"
                                Text="Contacts" OnClick="lnkPayableVendorContacts_Click"></asp:LinkButton></div>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="pnlFilterForm" runat="server">
                <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                    <tr>
                        <td>
                            <div class="status-list">
                                <span>
                                    <asp:CheckBox ID="chkSearchHomebaseOnly" runat="server" Text="Home Base Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                        AutoPostBack="true" /></span> <span>
                                            <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                                AutoPostBack="true" /></span>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <telerik:RadGrid ID="dgPayableVendorCatalog" runat="server" AllowSorting="true" OnNeedDataSource="dgPayableVendorCatalog_BindData"
                OnItemCommand="dgPayableVendorCatalog_ItemCommand" OnItemCreated="dgPayableVendorCatalog_ItemCreated"
                OnPageIndexChanged="PayableVendor_PageIndexChanged" AllowMultiRowSelection="false"
                OnUpdateCommand="dgPayableVendorCatalog_UpdateCommand" OnPreRender="PayableVendor_PreRender"
                OnInsertCommand="dgPayableVendorCatalog_InsertCommand" OnDeleteCommand="dgPayableVendorCatalog_DeleteCommand"
                Height="341px" AutoGenerateColumns="false" PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgPayableVendorCatalog_SelectedIndexChanged"
                AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true">
                <MasterTableView DataKeyNames="CustomerID,VendorID,VendorCD,Name,VendorType,IsApplicationFiled,IsApproved,IsDrugTest,IsInsuranceCERT,IsFAR135Approved,IsFAR135CERT,AdditionalInsurance,Contact,FirstName,LastName,BillingName,BillingAddr1,BillingAddr2,BillingCity,BillingState,BillingZip,NationalityCD,MetroCD,BillingPhoneNUM,BillingFaxNum,Notes,Credit,DiscountPercentage,TaxID,Terms,IsInActive,HomeBaseCD,LatitudeDegree,LatitudeMinutes,LatitudeNorthSouth,LongitudeDegree,LongitudeMinutes,LongitudeEastWest,ClosestICAO,LastUpdUID,LastUpdTS,IsTaxExempt,DateAddedDT,EmailID,WebAddress,IsDeleted
        ,TollFreePhone,BusinessEmail,Website,BillingAddr3,BusinessPhone,CellPhoneNum,HomeFax,CellPhoneNum2,OtherPhone,MCBusinessEmail,PersonalEmail,OtherEmail"
                    CommandItemDisplay="Bottom">
                    <Columns>
                        <telerik:GridBoundColumn DataField="VendorCD" HeaderText="Payables Vendor Code" CurrentFilterFunction="StartsWith"
                            HeaderStyle-Width="140px" FilterControlWidth="120px" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Name" HeaderText="Vendor Name" CurrentFilterFunction="StartsWith"
                            ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="180px" FilterDelay="500"
                            FilterControlWidth="160px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FirstName" HeaderText="Contact First Name" CurrentFilterFunction="StartsWith"
                            ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="160px" FilterDelay="500"
                            FilterControlWidth="140px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LastName" HeaderText="Contact Last Name" CurrentFilterFunction="StartsWith"
                            ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="160px" FilterDelay="500"
                            FilterControlWidth="140px">
                        </telerik:GridBoundColumn>
                        <%--<telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Active" ShowFilterIcon="false"
                        AutoPostBackOnFilter="true">
                    </telerik:GridCheckBoxColumn>--%>
                        <telerik:GridBoundColumn DataField="HomeBaseCD" HeaderText="Home Base" CurrentFilterFunction="StartsWith"
                            HeaderStyle-Width="100px" FilterControlWidth="80px" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="VendorID" Display="false" UniqueName="VendorID"
                            CurrentFilterFunction="EqualTo" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div style="padding: 5px 5px; float: left; clear: both;">
                            <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                Visible='<%# IsAuthorized(Permission.Database.AddPayableVendor)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                Visible='<%# IsAuthorized(Permission.Database.EditPayableVendor)%>' ToolTip="Edit"
                                CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtnDelete" OnClientClick="javascript:return ProcessDelete();"
                                Visible='<%# IsAuthorized(Permission.Database.DeletePayableVendor)%>' runat="server"
                                CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
            <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="tdLabel160">
                            <div id="tdSuccessMessage" class="success_msg">
                                Record saved successfully.</div>
                        </td>
                        <td align="right">
                            <div class="mandatory">
                                <span>Bold</span> Indicates required field</div>
                        </td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" class="tblButtonArea">
                    <tr>
                        <td>
                            <asp:Button ID="btnMainContact" runat="server" CssClass="ui_nav" Text="Main Contact"
                                OnClientClick="javascript:OnClientClick('Main Contact');return false;" />
                        </td>
                        <td>
                            <asp:Button ID="btnNotes" runat="server" CssClass="ui_nav" Text="Notes" OnClientClick="javascript:OnClientClick('Notes');return false;" />
                        </td>
                        <td>
                            <asp:Button ID="btnSaveChangesTop" runat="server" CssClass="button" Text="Save" ValidationGroup="save"
                                OnClick="SaveChanges_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancelTop" runat="server" CssClass="button" Text="Cancel" CausesValidation="false"
                                OnClick="Cancel_Click" />
                        </td>
                    </tr>
                </table>
                <telerik:RadPanelBar ID="pnlMaintenance" Width="100%" ExpandAnimation-Type="none"
                    ValidationGroup="save" CollapseAnimation-Type="none" runat="server">
                    <Items>
                        <telerik:RadPanelItem runat="server" Expanded="true" Text="Main Information" CssClass="PanelHeaderStyle">
                            <Items>
                                <telerik:RadPanelItem Value="Maintenance" runat="server">
                                    <ContentTemplate>
                                        <table width="100%" class="box1">
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                <asp:CheckBox ID="chkActive" runat="server" Text="Active" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <div class="tblspace_10">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                <span class="mnd_text">Payables Vendor Code</span>
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbCode" runat="server" CssClass="text50" MaxLength="5" ValidationGroup="save"
                                                                                OnTextChanged="Code_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique Code is Required"
                                                                                Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                                            <asp:RequiredFieldValidator ID="rfvCode" runat="server" ErrorMessage="Code is Required"
                                                                                Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbCode" CssClass="alert-text"
                                                                                ValidationGroup="save"></asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel130" valign="top">
                                                                <span class="mnd_text">Name</span>
                                                            </td>
                                                            <td class="tdLabel220" valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <asp:TextBox ID="tbName" runat="server" CssClass="text200" MaxLength="40"
                                                                                ValidationGroup="save"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="Vendor Name is Required"
                                                                                Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbName" CssClass="alert-text"
                                                                                ValidationGroup="save"></asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                Home Base
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="tdLabel110">
                                                                            <asp:TextBox ID="tbHomeBase" runat="server" CssClass="text40" MaxLength="4" OnTextChanged="HomeBase_TextChanged"
                                                                                onBlur="return RemoveSpecialChars(this)" AutoPostBack="true">
                                                                            </asp:TextBox>
                                                                            <asp:Button ID="btnHomeBase" runat="server" CssClass="browse-button" OnClientClick="javascript:openHomeBase();return false;" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CustomValidator ID="cvHomeBase" runat="server" ControlToValidate="tbHomeBase"
                                                                                ErrorMessage="Invalid Home Base Code." Display="Dynamic" CssClass="alert-text"
                                                                                ValidationGroup="save"></asp:CustomValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel130" valign="top">
                                                                Closest ICAO
                                                            </td>
                                                            <td class="tdLabel220" valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="tdLabel110">
                                                                            <asp:TextBox ID="tbClosestIcao" runat="server" CssClass="text40" MaxLength="4" OnTextChanged="ClosestIcao_TextChanged"
                                                                                onBlur="return RemoveSpecialChars(this)" AutoPostBack="true">
                                                                            </asp:TextBox>
                                                                            <asp:Button ID="btnClosestIcao" runat="server" OnClientClick="javascript:openClosestIcao();return false;"
                                                                                CssClass="browse-button"></asp:Button>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CustomValidator ID="cvClosestIcao" runat="server" ControlToValidate="tbClosestIcao"
                                                                                ErrorMessage="Invalid Airport ICAO Code." Display="Dynamic" CssClass="alert-text"
                                                                                ValidationGroup="save"></asp:CustomValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                Terms
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <asp:TextBox ID="tbTerms" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                                            </td>
                                                            <td class="tdLabel130" align="left">
                                                                Tax ID
                                                            </td>
                                                            <td class="tdLabel220" valign="top">
                                                                <asp:TextBox ID="tbTaxId" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <fieldset>
                                                                    <legend>Billing Information</legend>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel140" valign="top">
                                                                                            <span class="mnd_text">Name</span>
                                                                                        </td>
                                                                                        <td class="tdLabel240" valign="top">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbBillName" runat="server" CssClass="text200" MaxLength="40" ValidationGroup="save"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:RequiredFieldValidator ID="rfvBillName" runat="server" ErrorMessage="Name is Required"
                                                                                                            Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbBillName" CssClass="alert-text"
                                                                                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td class="tdLabel130" valign="top">
                                                                                            Metro
                                                                                        </td>
                                                                                        <td class="tdLabel200" valign="top">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel110">
                                                                                                        <asp:TextBox ID="tbMetro" runat="server" CssClass="text40" MaxLength="3" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                                            OnTextChanged="Metro_TextChanged" onBlur="return RemoveSpecialChars(this)" AutoPostBack="true"></asp:TextBox>
                                                                                                        <asp:Button ID="btnMetro" runat="server" OnClientClick="javascript:openMetro();return false;"
                                                                                                            CssClass="browse-button"></asp:Button>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:CustomValidator ID="cvMetro" runat="server" ControlToValidate="tbMetro" ErrorMessage="Invalid Metro Code."
                                                                                                            Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel140" valign="top">
                                                                                            Address Line 1
                                                                                        </td>
                                                                                        <td class="tdLabel240" valign="top">
                                                                                            <asp:TextBox ID="tbBillAddr1" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="tdLabel130" valign="top">
                                                                                            Address Line 2
                                                                                        </td>
                                                                                        <td class="tdLabel200" valign="top">
                                                                                            <asp:TextBox ID="tbBillAddr2" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel140">
                                                                                            <asp:Label ID="lbBillingAddr3" runat="server" Text="Address Line 3" />
                                                                                        </td>
                                                                                        <td class="tdLabel240">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbBillingAddr3" runat="server" MaxLength="40" CssClass="text200"
                                                                                                            ValidationGroup="save" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel140" valign="top">
                                                                                            City
                                                                                        </td>
                                                                                        <td class="tdLabel240" valign="top">
                                                                                            <asp:TextBox ID="tbBillCity" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="tdLabel130" valign="top">
                                                                                            State/Province
                                                                                        </td>
                                                                                        <td class="tdLabel200" valign="top">
                                                                                            <asp:TextBox ID="tbBillState" runat="server" CssClass="text200" MaxLength="10"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel140" valign="top">
                                                                                            Country
                                                                                        </td>
                                                                                        <td class="tdLabel240" valign="top">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td class="tdLabel110">
                                                                                                        <asp:TextBox ID="tbBillCountry" runat="server" CssClass="text50" MaxLength="3" ValidationGroup="save"
                                                                                                            OnTextChanged="BillCountry_TextChanged" onBlur="return RemoveSpecialChars(this)"
                                                                                                            AutoPostBack="true"></asp:TextBox>
                                                                                                        <asp:Button ID="btnBillCountry" runat="server" OnClientClick="javascript:openCountry();return false;"
                                                                                                            CssClass="browse-button"></asp:Button>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:CustomValidator ID="cvBillCountry" runat="server" ControlToValidate="tbBillCountry"
                                                                                                            ErrorMessage="Invalid Country Code." Display="Dynamic" CssClass="alert-text"
                                                                                                            ValidationGroup="save"></asp:CustomValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td class="tdLabel130" valign="top">
                                                                                            Postal
                                                                                        </td>
                                                                                        <td class="tdLabel200" valign="top">
                                                                                            <asp:TextBox ID="tbBillPostal" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel140" valign="top">
                                                                                            Business Phone
                                                                                        </td>
                                                                                        <td class="tdLabel240" valign="top">
                                                                                            <asp:TextBox ID="tbBillPhone" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                                runat="server" MaxLength="25" CssClass="text200"></asp:TextBox>
                                                                                        </td>
                                                                                        <td class="tdLabel130">
                                                                                            <asp:Label ID="lbTollFreePhone" runat="server" Text="Toll-free Phone" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbTollFreePhone" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                                            runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel140" valign="top">
                                                                                            Business E-mail
                                                                                        </td>
                                                                                        <td class="tdLabel240" valign="top">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbEmailAddress" runat="server" CssClass="text200" ValidationGroup="save"
                                                                                                            MaxLength="40">
                                                                                                        </asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:RegularExpressionValidator ID="revEmail" runat="server" Display="Dynamic" ErrorMessage="Invalid Format"
                                                                                                            ControlToValidate="tbEmailAddress" CssClass="alert-text" ValidationGroup="save"
                                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td class="tdLabel130" valign="top">
                                                                                            Web site
                                                                                        </td>
                                                                                        <td class="tdLabel200" valign="top">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbWebAddress" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <%--<asp:RegularExpressionValidator ID="revwebsite" runat="server" Display="Dynamic"
                                                                                                        ErrorMessage="Invalid Format" ControlToValidate="tbWebAddress" CssClass="alert-text"
                                                                                                        ValidationGroup="save" ValidationExpression="([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?"></asp:RegularExpressionValidator>--%>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                                    <tr>
                                                                                        <td class="tdLabel140">
                                                                                            Business Fax
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbBillFax" runat="server" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                                CssClass="text200" MaxLength="25"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>               
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td>
                            <div class="tblspace_10">
                            </div>
                        </td>
                    </tr>
                </table>               
                <telerik:RadPanelBar ID="pnlImage" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                                runat="server">
                    <Items>
                        <telerik:RadPanelItem runat="server" Expanded="true" Text="Attachments" CssClass="PanelHeaderStyle">
                            <ContentTemplate>                               
                                <table width="100%" class="box1">                                    
                                    <tr style="display: none;">
                                        <td class="tdLabel100">
                                            Document Name
                                        </td>
                                        <td style="vertical-align: top">
                                            <asp:TextBox ID="tbImgName" MaxLength="20" runat="server" CssClass="text210" OnBlur="CheckName();"
                                                ClientIDMode="Static"></asp:TextBox>
                                        </td>
                                    </tr>                                   
                                    <tr>
                                        <td valign="top" class="tdLabel270">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <asp:FileUpload ID="fileUL" runat="server" Enabled="false" ClientIDMode="Static"
                                                        onchange="javascript:return File_onchange();" />
                                                        <asp:Label ID="lblError" CssClass="alert-text" runat="server" Style="float: left;">All file types are allowed.</asp:Label>
                                                        <asp:HiddenField ID="hdnUrl" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>                                                                                
                                        <td valign="top" class="tdLabel110">                                            
                                            <asp:DropDownList ID="ddlImg" runat="server" DataTextField="UWAFileName" DataValueField="FileWarehouseID"
                                                OnSelectedIndexChanged="ddlImg_SelectedIndexChanged" AutoPostBack="true" CssClass="text200"
                                                ClientIDMode="AutoID">
                                            </asp:DropDownList>
                                        </td>
                                        <td valign="top" class="tdLabel80">
                                            <asp:Image ID="imgFile" Width="50px" Height="50px" runat="server" OnClick="OpenRadWindow();"
                                                AlternateText="" ClientIDMode="Static" />
                                            <asp:HyperLink ID="lnkFileName" runat="server" ClientIDMode="Static">Download File</asp:HyperLink>
                                        </td>
                                        <td valign="top" align="right" class="custom_radbutton">
                                            <%--<asp:Button ID="btndeleteImage" runat="server" Text="Delete" Enabled="false" OnClientClick="ImageDeleteConfirm"
                                            OnClick="DeleteImage_Click" ClientIDMode="Static" />--%>
                                            <telerik:RadButton ID="btndeleteImage" runat="server" Text="Delete" Enabled="false"
                                                OnClientClicking="ImageDeleteConfirm" OnClick="DeleteImage_Click" ClientIDMode="Static" />
                                        </td>                                               
                                    </tr>
                                </table>                                       
                            </ContentTemplate>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>                       
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td>
                            <div class="tblspace_10">
                            </div>
                        </td>
                    </tr>
                </table>
                <telerik:RadPanelBar ID="pnlMainContact" Width="100%" ExpandAnimation-Type="none"
                    CollapseAnimation-Type="none" runat="server">
                    <Items>
                        <telerik:RadPanelItem runat="server" Text="Main Contact Information" Expanded="false">
                            <Items>
                                <telerik:RadPanelItem>
                                    <ContentTemplate>
                                        <table width="100%" class="box1">
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                First Name
                                                            </td>
                                                            <td class="tdLabel150" valign="top">
                                                                <asp:TextBox ID="tbFirstName" runat="server" CssClass="text110" MaxLength="40" ReadOnly="true"></asp:TextBox>
                                                            </td>
                                                            <td class="tdLabel80" valign="top">
                                                                Middle Name
                                                            </td>
                                                            <td class="tdLabel150" valign="top">
                                                                <asp:TextBox ID="tbMiddleName" runat="server" CssClass="text110" MaxLength="40" ReadOnly="true">
                                                                </asp:TextBox>
                                                            </td>
                                                            <td class="tdLabel80" valign="top">
                                                                Last Name
                                                            </td>
                                                            <td class="tdLabel140" valign="top">
                                                                <asp:TextBox ID="tbLastName" runat="server" CssClass="text110" MaxLength="40" ReadOnly="true">
                                                                </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <div class="tblspace_10">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                Title
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <asp:TextBox ID="tbTitle" runat="server" CssClass="text200" MaxLength="40" ReadOnly="true">
                                                                </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                Address 1
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <asp:TextBox ID="tbAddr1" ReadOnly="true" runat="server" CssClass="text200" MaxLength="40">
                                                                </asp:TextBox>
                                                            </td>
                                                            <td class="tdLabel130" valign="top">
                                                                Address 2
                                                            </td>
                                                            <td class="tdLabel200" valign="top">
                                                                <asp:TextBox ID="tbAddr2" ReadOnly="true" runat="server" CssClass="text200" MaxLength="40">
                                                                </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                Address 3
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <asp:TextBox ID="tbAddr3" ReadOnly="true" runat="server" CssClass="text200" MaxLength="40">
                                                                </asp:TextBox>
                                                            </td>
                                                            <td class="tdLabel130" valign="top">
                                                            </td>
                                                            <td class="tdLabel200" valign="top">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                City
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <asp:TextBox ID="tbCity" ReadOnly="true" runat="server" CssClass="text200" MaxLength="40">
                                                                </asp:TextBox>
                                                            </td>
                                                            <td class="tdLabel130" valign="top">
                                                                State/Province
                                                            </td>
                                                            <td class="tdLabel200" valign="top">
                                                                <asp:TextBox ID="tbState" runat="server" ReadOnly="true" CssClass="text200" MaxLength="10">
                                                                </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                Country
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <asp:TextBox ID="tbCountry" runat="server" ReadOnly="true" CssClass="text50" MaxLength="3">
                                                                </asp:TextBox>&nbsp;
                                                                <button id="btnCountry" class="browse-button">
                                                                </button>
                                                            </td>
                                                            <td class="tdLabel130" valign="top">
                                                                Postal
                                                            </td>
                                                            <td class="tdLabel200" valign="top">
                                                                <asp:TextBox ID="tbPostal" ReadOnly="true" runat="server" CssClass="text200" MaxLength="15">
                                                                </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150">
                                                                <asp:Label ID="lbBusinessPhone" runat="server" Text="Business Phone" />
                                                            </td>
                                                            <td class="tdLabel240">
                                                                <asp:TextBox ID="tbBusinessPhone" runat="server" MaxLength="25" CssClass="text200"
                                                                    ValidationGroup="save" />
                                                            </td>
                                                            <td class="tdLabel130">
                                                                Home Phone
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbPhone" runat="server" ReadOnly="true" CssClass="text200" MaxLength="25">
                                                                            </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td class="tdLabel150">
                                                                <asp:Label ID="lbOtherPhone1" runat="server" Text="Other Phone" />
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbOtherPhone1" runat="server" MaxLength="25" CssClass="text200"
                                                                                ValidationGroup="save" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td class="tdLabel150">
                                                                <asp:Label ID="lbCellPhoneNum" runat="server" Text="Primary Mobile/Cell" />
                                                            </td>
                                                            <td class="tdLabel240">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbCellPhoneNum" runat="server" MaxLength="25" CssClass="text200"
                                                                                ValidationGroup="save" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel130">
                                                                <asp:Label ID="lbCellPhoneNum2" runat="server" Text="Secondary Mobile/Cell" />
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbCellPhoneNum2" runat="server" MaxLength="25" CssClass="text200"
                                                                                ValidationGroup="save" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150" valign="top">
                                                                E-mail Address
                                                            </td>
                                                            <td class="tdLabel240" valign="top">
                                                                <asp:TextBox ID="tbEmail" runat="server" ReadOnly="true" CssClass="text200" MaxLength="40">
                                                                </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150">
                                                                <asp:Label ID="lbMCBusinessEmail" runat="server" Text="Business E-mail" />
                                                            </td>
                                                            <td class="tdLabel240">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbMCBusinessEmail" runat="server" MaxLength="25" CssClass="text200"
                                                                                ValidationGroup="save" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel130">
                                                                <asp:Label ID="lbPersonalEmail" runat="server" Text="Personal E-mail" />
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbPersonalEmail" runat="server" MaxLength="25" CssClass="text200"
                                                                                ValidationGroup="save" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150">
                                                                <asp:Label ID="lbOtherEmail" runat="server" Text="Other E-mail" />
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbOtherEmail" runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150">
                                                               Business Fax
                                                            </td>
                                                            <td class="tdLabel240">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbFax" runat="server" ReadOnly="true" CssClass="text200" MaxLength="25">
                                                                            </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel130">
                                                                <asp:Label ID="lbHomeFax" runat="server" Text="Home Fax" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbHomeFax" runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="tdLabel150">
                                                                <asp:Label ID="lbWebsite" runat="server" Text="Web site" />
                                                            </td>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox ID="tbWebsite" runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tdLabel130" valign="top">
                                                            </td>
                                                            <td class="tdLabel200" valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <div class="tblspace_10">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td valign="top">
                                                                Additional Contact Information
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td valign="top">
                                                                <asp:TextBox ID="tbAdditionalContact" runat="server" ReadOnly="true" TextMode="MultiLine"
                                                                    CssClass="textarea720x80"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
                <table cellspacing="0" cellpadding="0" style="width: 100%;">
                    <tr>
                        <td align="left">
                            <div class="nav-space">
                            </div>
                        </td>
                    </tr>
                </table>
                <telerik:RadPanelBar ID="pnlNotes" Width="100%" ExpandAnimation-Type="none" CollapseAnimation-Type="none"
                    runat="server">
                    <Items>
                        <telerik:RadPanelItem runat="server" Text="Notes" Expanded="false" CssClass="PanelHeaderStyle">
                            <Items>
                                <telerik:RadPanelItem>
                                    <ContentTemplate>
                                        <table width="100%" cellspacing="0" class="note-box">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbNotes" runat="server" TextMode="MultiLine" CssClass="textarea-db">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
                <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                    <tr>
                        <td>
                            <asp:Button ID="btnSaveChanges" Text="Save" runat="server" ValidationGroup="save"
                                OnClick="SaveChanges_Click" CssClass="button" />
                            <asp:HiddenField ID="hdnSave" runat="server" />
                            <asp:HiddenField ID="hdnHomeBaseID" runat="server" />
                            <asp:HiddenField ID="hdnAirportID" runat="server" />
                            <asp:HiddenField ID="hdnCountryID" runat="server" />
                            <asp:HiddenField ID="hdnMetroID" runat="server" />
                            <asp:HiddenField ID="hdnContactName" runat="server" />
                            <asp:HiddenField ID="hdnBrowserName" runat="server" />
                            <asp:HiddenField ID="hdnRedirect" runat="server" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="button" CausesValidation="false"
                                OnClick="Cancel_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
