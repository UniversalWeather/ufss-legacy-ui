﻿using System.Collections;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Web.Script.Services;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml.Xsl;
using FlightPak.Common;
using FlightPak.Common.Constants;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.Framework.Masters;
using FlightPak.Web.ViewModels;
using Microsoft.Ajax.Utilities;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI.ExportInfrastructure;

namespace FlightPak.Web.Views.Settings.Logistics
{
    public partial class FuelFileUpload : BaseSecuredPage
    {

        private ExceptionManager exManager;
        protected bool isIEFrameUpload = false;
        #region "Fiel Upload Logic"
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString.HasKeys()) // replace with your check
                {
                    if (Request.QueryString["ie"]=="ie89")
                    {
                        isIEFrameUpload = true;
                    }
                }
                upload();                
            }

        }       
        public string BytesArrayToHexString(byte[] hash)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
        bool IsCsvFile(string filePath)
        {
            return filePath != null &&
                filePath.EndsWith(".csv", StringComparison.Ordinal);
        }
        private void upload()
        {   
            string callback = Request.Form["fd-callback"];
            string name="";
            byte[] data=null;
            Dictionary<string, string> uploadResult = new Dictionary<string, string>();            
            HttpPostedFile fdFile = Request.Files["fd-file"];
            if (fdFile != null)
            {
                if (IsCsvFile(fdFile.FileName))
                {
                    // Regular multipart/form-data upload.
                    name = fdFile.FileName.Substring(fdFile.FileName.LastIndexOf("\\") + 1);
                    data = new byte[fdFile.ContentLength];
                    fdFile.InputStream.Read(data, 0, fdFile.ContentLength);
                    if (!Utility.ValidateCsvContent(System.Text.Encoding.UTF8.GetString(data)).IsValid)
                        data = null;
                    name = fdFile.FileName.Length < 1 ? "Fuel_" + Guid.NewGuid() + ".csv" : name;
                }
            }
            else
            {
                // Raw POST data.
                name = HttpUtility.UrlDecode(Request.Headers["X-File-Name"]);
                if (IsCsvFile(name))
                {
                    data = new byte[Request.InputStream.Length];
                    Request.InputStream.Read(data, 0, (int)Request.InputStream.Length); //up to 2GB
                    if (!Utility.ValidateCsvContent(System.Text.Encoding.UTF8.GetString(data)).IsValid)
                        data = null;
                }
            }
            if (data!=null&& data.Length > 0)
            {
                bool result = SaveCsvFile(name, data);                
                if (result)
                {
                    uploadResult.Add("ResponseCode", "1");
                    uploadResult.Add("ResponseMessage", name + " file is uploaded successfully.");
                }
                else
                {
                    uploadResult.Add("ResponseCode", "2");
                    uploadResult.Add("ResponseMessage", name + " is fail to upload.");
                }
            }
            else
            {
                uploadResult.Add("ResponseCode", "2");
                uploadResult.Add("ResponseMessage", name + " is fail to upload.");
                if (isIEFrameUpload)
                {
                    uploadResult.Add("Error", "Upload Fail.File is empty or invalid.");                        
                }
            }
            if (isIEFrameUpload)
            {
                // Callback function given - the caller loads response into a hidden <iframe> so
                // it expects it to be a valid HTML calling this callback function.
                Response.Headers["Content-Type"] = "text/html; charset=utf-8";
                uploadResult.Add("FileName", name);
                Response.Write(
                   "<!DOCTYPE html><html><head></head><body><script type=\"text/javascript\">" +
                  "try{window.top." +  Microsoft.Security.Application.Encoder.HtmlEncode(callback) + "(\'" + JsonConvert.SerializeObject(uploadResult) + "\')}catch(e){}</script></body></html>");
            }
            else
            {
                Response.Write(JsonConvert.SerializeObject(uploadResult));
                Response.End();
            }
            //Response.End();
        }
        private bool SaveCsvFile(string name, byte[] data)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool isSaved = false;               
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string FolderPath = Server.MapPath(Page.ResolveUrl("~")) + ConfigurationManager.AppSettings["FuelCsvFilesFolder"];
                        if (!Directory.Exists(FolderPath))
                        {
                            Directory.CreateDirectory(FolderPath);
                        }
                        
                        string filename = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd-HH-mm-ss fff").Replace(" ","-") + ".csv";
                        string filepath = FolderPath +@"\"+filename;
                        System.IO.File.WriteAllBytes(filepath, data);
                        FlightPak.Web.FlightPakMasterService.FuelFileData fuelData = new FlightPak.Web.FlightPakMasterService.FuelFileData();
                        fuelData = FlightPak.Web.Framework.Helpers.MiscUtils.InitializeFuelFileData(fuelData);
                        fuelData.FilePath = Request.Url.Scheme + @"://" + Request.Url.Authority + @"/" + ConfigurationManager.AppSettings["FuelCsvFilesFolder"].Replace('\\','/') + @"/" + filename;
                        fuelData.FileName = filename;
                        fuelData.BaseFileName = name;
                        fuelData.VendorID = Convert.ToInt64(Session["FuelVendorID"].ToString());
                        fuelData.FullPhysicalFilePath = FolderPath + @"\" + filename;
                        using (StreamReader objReader = new StreamReader(File.OpenRead(filepath)))
                        {
                            fuelData.UploadFileData = objReader.ReadToEnd();
                        }
                        Session["FuelFileData"] = fuelData;
                        isSaved = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                return isSaved;
            }
        }
        #endregion

        [WebMethod]
        public static string GetUploadedFileData()
        {
            ExceptionManager exManager;
            string data="";
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                var result = new Dictionary<string, object>();
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (var client = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        
                        var fileData = (FlightPak.Web.FlightPakMasterService.FuelFileData)HttpContext.Current.Session["FuelFileData"];
                        var fileReadRecords=Utility.FetchPreviewRecordsInSession(fileData.UploadFileData, 6);
                        result.Add("data", fileReadRecords);
                        
                        if (fileReadRecords.Count > 0)
                        {
                            Regex removeWhitespacePattern = new Regex(@"\s+");
                            List<string> csvtitles =fileReadRecords.FirstOrDefault().ConvertAll(a => removeWhitespacePattern.Replace(a.ToLower().Trim(), "").Replace('/', '_')).ToList();
                            //The file format has changed since your last upload.  Please see changes highlighted in red.
                            var previousFile = client.GetPreviousSavedFormatForVendor(fileData, csvtitles);
                            if (previousFile.ReturnFlag && previousFile.EntityInfo!=null)
                            {
                                FuelFileXsltParser oFuelFileXsltParser = previousFile.EntityInfo;
                                fileData.csvXMLParser = string.Format("{0}{1}{2}{3}", FuelFileConstants.FirstXSLTPart, oFuelFileXsltParser.XsltFileContain,oFuelFileXsltParser.XsltFunctionContain, FuelFileConstants.LastXSLTPart);
                                result.Add("IsChanged", false);
                                fileData.IsCustomMapped = false;
                            }
                            else
                            {
                                // Match User uploaded File same as Errorlog format
                                var isErrorlogFile = client.MatcheCSVFormatWithErrorLogForamt(fileData, csvtitles);
                                if (isErrorlogFile.ReturnFlag && isErrorlogFile.EntityInfo != null)
                                {
                                    FuelFileXsltParser oFuelFileXsltParser = isErrorlogFile.EntityInfo;
                                    fileData.csvXMLParser = string.Format("{0}{1}{2}{3}", FuelFileConstants.FirstXSLTPart, oFuelFileXsltParser.XsltFileContain, oFuelFileXsltParser.XsltFunctionContain, FuelFileConstants.LastXSLTPart);
                                    result.Add("IsChanged", false);
                                    fileData.IsCustomMapped = false;
                                    fileData.IsErrorLogFile = true;
                                } else
                                {
                                    result.Add("IsChanged", true);
                                    fileData.IsCustomMapped = true;
                                }
                            }
                        }
                        else
                        {
                            result.Add("IsChanged", true);
                            fileData.IsCustomMapped = true;
                        }
                        data = JsonConvert.SerializeObject(result);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
            return data;
        }

        [WebMethod]
        public static string GetMappedFuelColumnData(string mappedColumns)
        {            
            ExceptionManager exManager;
            string data = "";
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    FlightPakMasterService.MasterCatalogServiceClient client = new FlightPakMasterService.MasterCatalogServiceClient();

                    FlightPak.Web.FlightPakMasterService.FuelFileData fileData = (FlightPak.Web.FlightPakMasterService.FuelFileData)HttpContext.Current.Session["FuelFileData"];
                    string[] columnIDAndName = mappedColumns.Split(',');
                    FlightPak.Web.FlightPakMasterService.FuelFileData newMappedFile = new FlightPak.Web.FlightPakMasterService.FuelFileData();
                    newMappedFile = Framework.Helpers.MiscUtils.InitializeFuelFileData(newMappedFile);

                    for (int i = 0; i < columnIDAndName.Length; i++)
                    {
                        if (columnIDAndName[i].Trim().Length < 1)
                        {
                            continue;
                        }
                        int id =Convert.ToInt32(columnIDAndName[i].Split(':')[0]);
                        string column = columnIDAndName[i].Split(':')[1];

                        switch (column)
                        {
                                // This Fields are use as Mapping text in html in Mapping Usercontrols FuelWizard.
                            case "FBO":
                                newMappedFile.FBO = id;
                                break;
                            case "ICAO":
                                newMappedFile.ICAO = id;
                                break;
                            case "IATA":
                                newMappedFile.IATA = id;
                                break;
                            case "Effective Date":
                                newMappedFile.EffectiveDate = id;
                                break;
                            case "Price":
                                newMappedFile.Price = id;
                                break;
                            case "Gallons From":
                                newMappedFile.Low = id;
                                break;
                            case "Gallons To":
                                newMappedFile.High = id;
                                break;
                        }
                    }

                    if (!FlightPak.Web.Framework.Helpers.MiscUtils.HaveRequiredFuelColumnsFound(newMappedFile))
                    {
                        if (newMappedFile.FBO < 0 && fileData.FBO > -1 && !FlightPak.Web.Framework.Helpers.MiscUtils.isAnyFieldHaveSameValue(newMappedFile,fileData.FBO))
                        {
                            newMappedFile.FBO = fileData.FBO;
                        }
                        if (newMappedFile.ICAO < 0 && fileData.ICAO > -1 && !FlightPak.Web.Framework.Helpers.MiscUtils.isAnyFieldHaveSameValue(newMappedFile, fileData.ICAO))
                        {
                            newMappedFile.ICAO = fileData.ICAO;
                        }
                        if (newMappedFile.IATA < 0 && fileData.IATA > -1 && !FlightPak.Web.Framework.Helpers.MiscUtils.isAnyFieldHaveSameValue(newMappedFile, fileData.IATA))
                        {
                            newMappedFile.IATA = fileData.IATA;
                        }
                        if (newMappedFile.EffectiveDate < 0 && fileData.EffectiveDate > -1 && !FlightPak.Web.Framework.Helpers.MiscUtils.isAnyFieldHaveSameValue(newMappedFile, fileData.EffectiveDate))
                        {
                            newMappedFile.EffectiveDate = fileData.EffectiveDate;
                        }
                        if (newMappedFile.Price < 0 && fileData.Price > -1 && !FlightPak.Web.Framework.Helpers.MiscUtils.isAnyFieldHaveSameValue(newMappedFile, fileData.Price))
                        {
                            newMappedFile.Price = fileData.Price;
                        }
                        if (newMappedFile.Low < 0 && fileData.Low > -1 && !FlightPak.Web.Framework.Helpers.MiscUtils.isAnyFieldHaveSameValue(newMappedFile, fileData.Low))
                        {
                            newMappedFile.Low = fileData.Low;
                        }
                        if (newMappedFile.High < 0 && fileData.High > -1 && !FlightPak.Web.Framework.Helpers.MiscUtils.isAnyFieldHaveSameValue(newMappedFile, fileData.High))
                        {
                            newMappedFile.High = fileData.High;
                        }
                        if (newMappedFile.Vendor < 0 && fileData.Vendor > -1 && !FlightPak.Web.Framework.Helpers.MiscUtils.isAnyFieldHaveSameValue(newMappedFile, fileData.Vendor))
                        {
                            newMappedFile.Vendor = fileData.Vendor;
                        }
                    }

                    newMappedFile.FileName = fileData.FileName;
                    newMappedFile.FilePath = fileData.FilePath;
                    newMappedFile.NumberOfColumns = fileData.NumberOfColumns;
                    newMappedFile.isAllFieldSet = fileData.isAllFieldSet;
                    newMappedFile.IsCustomMapped = fileData.IsCustomMapped;
                    newMappedFile.VendorID = fileData.VendorID;
                    newMappedFile.BaseFileName = fileData.BaseFileName;
                    newMappedFile.FullPhysicalFilePath = fileData.FullPhysicalFilePath;
                    newMappedFile.UploadFileData = fileData.UploadFileData;

                    var fdata= ReadFileAsIndexNumber(newMappedFile, 10);
                    data = JsonConvert.SerializeObject(CreateJsonStringFromFuelDataConfirmationView(fdata));
                    HttpContext.Current.Session["FuelFileData"] = newMappedFile;
                    
                    
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
            return data;
        }

        public static List<FlightpakFuelModal> ReadFileAsIndexNumber(FlightPak.Web.FlightPakMasterService.FuelFileData oFuelFileData, int readNoOfRecords)
        {
            char SplitDelimiters = ',';
            string[] ColumnSplitter = new string[] { "\",\"" };
            string line;
            string[] columns = null;
            int titleColumnsCount = 0;
            int cnt = 0;
            List<string> readFileRow = new List<string>();
            List<FlightpakFuelModal> readTotalFile = new List<FlightpakFuelModal>();
            List<int> blankColumns = new List<int>();

            using (StringReader sr = new StringReader(oFuelFileData.UploadFileData))
            {
                while ((line = sr.ReadLine()) != null && cnt < readNoOfRecords)
                {

                    if (line.Contains(ColumnSplitter[0]))
                    {
                        columns = line.Split(ColumnSplitter, StringSplitOptions.None);
                    }
                    else
                    {
                        columns = line.Split(SplitDelimiters);
                    }
                    for (int Index = 0; Index < columns.Count(); Index++)
                    {
                        if (columns[Index].Contains("\""))
                        {
                            columns[Index] = columns[Index].Replace("\"", "");
                        }
                    }
                    if (cnt == 0)
                    {
                        titleColumnsCount = columns.Length;
                    }
                    if ((cnt != 0) && (titleColumnsCount == columns.Length))
                    {
                        FlightpakFuelModal objFlightpak = new FlightpakFuelModal();
                        for (int i = 0; i < columns.Length; i++)
                        {
                            if (!blankColumns.Contains(i))
                            {
                                int tempIntValue;
                                if (i == oFuelFileData.FBO)
                                {
                                    objFlightpak.FBO = columns[oFuelFileData.FBO];
                                }
                                if (i == oFuelFileData.EffectiveDate)
                                {
                                    DateTime tempdate = new DateTime();
                                    if ((DateTime.TryParse(columns[oFuelFileData.EffectiveDate], out tempdate)))
                                    {
                                        objFlightpak.EffectiveDate = tempdate;
                                    }
                                }

                                if (i == oFuelFileData.High)
                                {

                                    if ((int.TryParse(columns[oFuelFileData.High], out tempIntValue)))
                                    {
                                        objFlightpak.GallonTo = tempIntValue;
                                    }

                                }
                                if (i == oFuelFileData.Low)
                                {

                                    if ((int.TryParse(columns[oFuelFileData.Low], out tempIntValue)))
                                    {
                                        objFlightpak.GallonFrom = tempIntValue;
                                    }
                                }
                                if (i == oFuelFileData.Price)
                                {
                                    decimal tempDecimalValue;
                                    if ((decimal.TryParse(columns[oFuelFileData.Price], out tempDecimalValue)))
                                    {
                                        objFlightpak.Price = tempDecimalValue;
                                    }
                                }
                                if (i == oFuelFileData.Text)
                                {
                                    objFlightpak.NoteText = oFuelFileData.Text > -1 ? columns[oFuelFileData.Text] : "";
                                }
                                if (i == oFuelFileData.Vendor)
                                {

                                    objFlightpak.FBO = oFuelFileData.FBO == -1 ? columns[oFuelFileData.Vendor] : columns[oFuelFileData.FBO];
                                }
                                if (i == oFuelFileData.Text)
                                {
                                    objFlightpak.NoteText = columns[oFuelFileData.Text];
                                }
                                if (i == oFuelFileData.ICAO)
                                {
                                    objFlightpak.ICAO = columns[oFuelFileData.ICAO];
                                }
                                if (i == oFuelFileData.IATA)
                                {
                                    objFlightpak.IATA = columns[oFuelFileData.IATA];
                                }
                            }
                        }
                        readTotalFile.Add(objFlightpak);
                    }
                    cnt++;
                }
            }
            return readTotalFile;
        }
        public static List<List<string>> CreateJsonStringFromFuelDataConfirmationView(List<FlightpakFuelModal> fdata)
        {
            List<string> rowList = new List<string>();
            List<List<string>> masterList = new List<List<string>>();

            rowList.Add("FBO");
            rowList.Add("Vendor");
            rowList.Add("ICAO");
            rowList.Add("IATA");
            rowList.Add("Gallon From");
            rowList.Add("Gallon To");
            rowList.Add("Price");
            rowList.Add("Effective Date");

            masterList.Add(rowList);

            for (int i = 0; i < fdata.Count; i++)
            {
                var oneFuelRow = fdata[i];
                rowList = new List<string>();
                rowList.Add(oneFuelRow.FBO);
                rowList.Add(HttpContext.Current.Session["VendorName"].ToString());
                rowList.Add(oneFuelRow.ICAO ?? "");
                rowList.Add(oneFuelRow.IATA ?? "");
                rowList.Add(oneFuelRow.GallonFrom.ToString() ?? "");
                rowList.Add(oneFuelRow.GallonTo.ToString() ?? "");
                rowList.Add(oneFuelRow.Price.ToString() ?? "");
                rowList.Add(oneFuelRow.EffectiveDate.Value.ToShortDateString());

                masterList.Add(rowList);
            }
            return masterList;
        }

        [WebMethod]
        public static void SetFlagInSession()
        {
            FlightPak.Web.FlightPakMasterService.FuelFileData objFuelFileData = (FlightPak.Web.FlightPakMasterService.FuelFileData)HttpContext.Current.Session["FuelFileData"];
            objFuelFileData.IsCustomMapped = true;
        }
        [WebMethod(EnableSession=true)]
        public static string GethighlightenFields()
        {
            string response="";
            if( HttpContext.Current.Session["highlightFuelField"]!=null){
                response = HttpContext.Current.Session["highlightFuelField"].ToString();
            }
            return response;
        }

        public static FuelFileData InitializeFuelFileColumnIndexValues(string fileTitlesCSVFormat,List<string> result)
        {
            using (var client = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                result = result.Select(s => s.ToLower()).ToList();
                var fileData = (FlightPak.Web.FlightPakMasterService.FuelFileData)HttpContext.Current.Session["FuelFileData"];
                if (fileData.FBO == -1 && result.Contains("fbo"))
                {
                    fileData.FBO = result.IndexOf("fbo");
                }
                if (fileData.IATA == -1 && result.Contains("iata"))
                {
                    fileData.IATA = result.IndexOf("iata");
                }
                if (fileData.ICAO == -1 && result.Contains("icao"))
                {
                    fileData.ICAO = result.IndexOf("icao");
                }
                if (fileData.EffectiveDate == -1 && result.Contains("effectivedate"))
                {
                    fileData.EffectiveDate = result.IndexOf("effectivedate");
                }
                if (fileData.Low == -1 && result.Contains("gallonfrom"))
                {
                    fileData.Low = result.IndexOf("gallonfrom");
                }
                if (fileData.High == -1 && result.Contains("gallonto"))
                {
                    fileData.High = result.IndexOf("gallonto");
                }
                if (fileData.Price == -1 && result.Contains("price"))
                {
                    fileData.Price = result.IndexOf("price");
                }
                if (fileData.Text == -1 && result.Contains("notetext"))
                {
                    fileData.Text = result.IndexOf("notetext");
                }   
                return fileData;
            }
        }

        [WebMethod(EnableSession = true)]
        public static string btnPreviewContinueScreenClick()
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            if (HttpContext.Current.Session["FuelFileData"] != null)
            {
                var fileData = (FlightPak.Web.FlightPakMasterService.FuelFileData)HttpContext.Current.Session["FuelFileData"];
                var result = Utility.FetchPreviewRecordsInSession(fileData.UploadFileData, 1);
                if (!string.IsNullOrWhiteSpace(fileData.csvXMLParser))
                {
                    fileData = TransformCsvBasedOnXSLT(fileData);
                    var headerRow = Utility.FetchPreviewRecordsInSession(fileData.UploadFileData, 1).FirstOrDefault();
                    var headerCsvFormate = string.Join(",", headerRow.ToArray());
                    fileData = InitializeFuelFileColumnIndexValues(headerCsvFormate, headerRow);
                    string jsonData = ReadFileDataAsJson(fileData);
                    response["action"] = "2";
                    response.Add("jsonData", JsonConvert.DeserializeObject<List<List<string>>>(jsonData));
                } 
                else
                {
                    response.Add("action", "1");
                }
            }
            else
            {
                response.Add("action", "3");
            }
            return JsonConvert.SerializeObject(response);
        }

        public static string ReadFileDataAsJson(FlightPak.Web.FlightPakMasterService.FuelFileData fileData)
        {
            var fdata = ReadFileAsParsedCSV(fileData, 10);
            List<string> rowList = new List<string>();
            List<List<string>> masterList = new List<List<string>>();

            rowList.Add("FBO");
            rowList.Add("Vendor");
            rowList.Add("ICAO");
            rowList.Add("IATA");
            rowList.Add("Gallon From");
            rowList.Add("Gallon To");
            rowList.Add("Price");
            rowList.Add("Effective Date");

            masterList.Add(rowList);

            for (int i = 0; i < fdata.Count; i++)
            {
                var oneFuelRow = fdata[i];
                rowList = new List<string>();
                rowList.Add(oneFuelRow.FBO);
                rowList.Add(oneFuelRow.Vendor);
                rowList.Add(oneFuelRow.ICAO ?? "");
                rowList.Add(oneFuelRow.IATA ?? "");
                rowList.Add(oneFuelRow.GallonFrom.ToString() ?? "");
                rowList.Add(oneFuelRow.GallonTo.ToString() ?? "");
                rowList.Add(oneFuelRow.Price.ToString() ?? "");
                rowList.Add(oneFuelRow.EffectiveDate.Value.ToShortDateString());
                masterList.Add(rowList);
            }
            HttpContext.Current.Session["FuelFileData"] = fileData;
            string jsondata = JsonConvert.SerializeObject(masterList);
            return jsondata;
        }

        public static List<FlightpakFuelModal> ReadFileAsParsedCSV(FlightPak.Web.FlightPakMasterService.FuelFileData oFuelFileData, int readNoOfRecords)
        {
            List<FlightpakFuelModal> readTotalFile = new List<FlightpakFuelModal>();
            int cnt=0;

            foreach (var item in oFuelFileData.parsedCSV)
            {

                if (cnt < readNoOfRecords || readNoOfRecords==-1)
                {
                    var objFlightpak = new FlightpakFuelModal();
                    if (HttpContext.Current.Session["VendorName"] != null)
                    {
                        objFlightpak.Vendor = HttpContext.Current.Session["VendorName"].ToString();
                    }
                    if (item.ContainsKey("ICAO"))
                    {
                        objFlightpak.ICAO = Convert.ToString(item["ICAO"]);
                    }
                    if (item.ContainsKey("IATA"))
                    {
                        objFlightpak.IATA = Convert.ToString(item["IATA"]);
                    }
                    if (item.ContainsKey("FBO"))
                    {
                        objFlightpak.FBO = Convert.ToString(item["FBO"].Replace("\"", ""));
                    }
                    if (item.ContainsKey("EFFECTIVEDATE"))
                    {
                        DateTime tempdate = new DateTime();
                        if ((DateTime.TryParse(Convert.ToString(item["EFFECTIVEDATE"]), out tempdate)))
                        {
                            objFlightpak.EffectiveDate = tempdate;
                        }
                    }
                    if (item.ContainsKey("LOW"))
                    {
                        objFlightpak.GallonFrom = string.IsNullOrEmpty(item["LOW"]) ? (int?)null : Convert.ToInt32(item["LOW"]);
                    }
                    if (item.ContainsKey("HIGH"))
                    {
                        objFlightpak.GallonTo =string.IsNullOrEmpty(item["HIGH"])?(int?) null:Convert.ToInt32(item["HIGH"]);
                    }
                    if (item.ContainsKey("PRICE"))
                    {
                        objFlightpak.Price = string.IsNullOrEmpty(item["PRICE"]) ? (decimal?)null : Convert.ToDecimal(item["PRICE"]);
                    }
                    if (item.ContainsKey("NOTE"))
                    {
                        objFlightpak.NoteText = Convert.ToString(item["NOTE"]);
                    }
                    if (HaveRequiredColumnsFound(objFlightpak))
                    {
                        readTotalFile.Add(objFlightpak);
                        cnt++;
                    }
                }
            }
            return readTotalFile;
        }

        public static string ConvertFlightpakFuelModalObjToCsvFormat(List<FlightpakFuelModal> oFlightpakFuelModalList)
        {
            StringBuilder csvData=new StringBuilder();
            string[] columnname={};
            foreach (var item in oFlightpakFuelModalList)
            {
                if (oFlightpakFuelModalList.IndexOf(item) == 0)
                {
                    columnname = TypeDescriptor.GetProperties(item).Cast<PropertyDescriptor>().Select(propertyInfo => propertyInfo.Name).ToArray();
                    csvData.AppendLine(TypeDescriptor.GetProperties(item).Cast<PropertyDescriptor>().Select(propertyInfo=> propertyInfo.Name).ToArray().Aggregate((current, next) => current + "," + next));
                }
                var values = new object[columnname.Length];
                for (var i = 0; i < values.Length; i++)
                {
                    string propValue = Convert.ToString(item.GetType().GetProperty(columnname[i]).GetValue(item));
                    if (!string.IsNullOrEmpty(propValue) && (propValue.Contains(",") || propValue.Contains("&")))
                        values[i] = string.Format("\"{0}\"", propValue);
                    else
                        values[i] = propValue;
                }
                string data = values.Aggregate(string.Empty, (current, i) => current + (i + ","));
                csvData.AppendLine(data.Remove(data.Length - 1));
            }
            return csvData.ToString();
        }

        public static FlightPak.Web.FlightPakMasterService.FuelFileData TransformCsvBasedOnXSLT(FlightPak.Web.FlightPakMasterService.FuelFileData oFuelFileData)
        {
            try
            {
                if (HttpContext.Current.Session["FuelFileData"] != null)
                {
                    oFuelFileData = (FlightPakMasterService.FuelFileData)HttpContext.Current.Session["FuelFileData"];
                    var fileReadRecords = Utility.FetchPreviewRecordsInSession(oFuelFileData.UploadFileData, -1);
                    var removeWhitespacePattern = new Regex(@"\s+");
                    string[] headers = fileReadRecords[0].ConvertAll(a => removeWhitespacePattern.Replace(a.ToLower().Trim(), "").Replace('/', '_')).ToArray();
                    fileReadRecords.RemoveAt(0);
                    var xmlDoc = new XmlDocument();
                    XmlNode rootNode = xmlDoc.CreateElement("fuelData");
                    foreach (var record in fileReadRecords)
                    {
                        XmlNode fuelInfo = xmlDoc.CreateElement("fuelInfo");
                        for (int count = 0; count < headers.Length; count++)
                        {
                            XmlNode fuelDetails = xmlDoc.CreateElement(headers[count]);
                            fuelDetails.InnerText = record[count];
                            fuelInfo.AppendChild(fuelDetails);
                        }
                        rootNode.AppendChild(fuelInfo);
                    }
                    xmlDoc.AppendChild(rootNode);

                    string xmlInput = xmlDoc.InnerXml;
                    string xsltInput = oFuelFileData.csvXMLParser;
                    string csvOutput;
                    var doc = new XPathDocument(new StringReader(xmlInput));
                    var xslt = new XslTransform();
                    using (XmlReader xmlreader = XmlReader.Create(new StringReader(xsltInput)))
                    {
                        xslt.Load(xmlreader);
                        using (var sw = new StringWriter())
                        {
                            xslt.Transform(doc, null, sw);
                            csvOutput = sw.ToString();
                        }
                    }
                    oFuelFileData.UploadFileData = csvOutput;
                    oFuelFileData.parsedCSV = ConvertCSVFormatToDictionaryList(csvOutput);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return oFuelFileData;
        }

        public static List<Dictionary<string, string>> ConvertCSVFormatToDictionaryList(string csvData)
        {
            var parsedCSV = new List<Dictionary<string, string>>();
            var csvRecords = Utility.FetchPreviewRecordsInSession(csvData, -1);
            //Get all headers and convert all in lowercase and trim it
            Regex removeWhitespacePattern = new Regex(@"\s+");
            string[] headers = csvRecords[0].ConvertAll(a => removeWhitespacePattern.Replace(a.Trim(), "")).ToArray();
            csvRecords.RemoveAt(0);
            foreach (var records in csvRecords)
            {
                Dictionary<string, string> csvRec = new Dictionary<string, string>();
                for (int count = 0; count < headers.Count(); count++)
                {
                    csvRec.Add(headers[count], records[count]);
                }
                parsedCSV.Add(csvRec);
            }

            return parsedCSV;
        }

        public static bool HaveRequiredColumnsFound(FlightpakFuelModal oFlightpakFuelModal)
        {
            if ((!string.IsNullOrEmpty(oFlightpakFuelModal.FBO) || !string.IsNullOrEmpty(oFlightpakFuelModal.Vendor)) 
                && (!string.IsNullOrEmpty(oFlightpakFuelModal.ICAO) || !string.IsNullOrEmpty(oFlightpakFuelModal.IATA))
                && oFlightpakFuelModal.Price != null && oFlightpakFuelModal.Price > -1 
                && oFlightpakFuelModal.EffectiveDate !=null
                && oFlightpakFuelModal.GallonFrom != null && oFlightpakFuelModal.GallonFrom>-1
                && oFlightpakFuelModal.GallonTo != null && oFlightpakFuelModal.GallonTo>-1)
            {
                return true;
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> SaveCSVData(bool isMaintainHistoricalFuelData)
        {
            FSSOperationResult<Hashtable> oFSSOperationResult = new FSSOperationResult<Hashtable>();
            Hashtable ret = new Hashtable();
            FlightPak.Web.FlightPakMasterService.FuelFileData objFuelFileData = (FlightPak.Web.FlightPakMasterService.FuelFileData)HttpContext.Current.Session["FuelFileData"];
            objFuelFileData.VendorID = Convert.ToInt64(HttpContext.Current.Session["FuelVendorID"].ToString());
            objFuelFileData.IsMaintainHistoricalFuelData = isMaintainHistoricalFuelData;
            using (FlightPakMasterService.MasterCatalogServiceClient client = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                objFuelFileData = TransformCsvBasedOnXSLT(objFuelFileData);
                List<FlightpakFuelModal> list = ReadFileAsParsedCSV(objFuelFileData, -1);
                objFuelFileData.UploadFileData= ConvertFlightpakFuelModalObjToCsvFormat(list);
                var headerRow = Utility.FetchPreviewRecordsInSession(objFuelFileData.UploadFileData, 1).FirstOrDefault();
                var headerCsvFormate = string.Join(",", headerRow.ToArray());
                objFuelFileData = InitializeFuelFileColumnIndexValues(headerCsvFormate, headerRow);
                if (FlightPak.Web.Framework.Helpers.MiscUtils.HaveRequiredFuelColumnsFound(objFuelFileData))
                {
                    HttpContext.Current.Session.Remove("highlightFuelField");
                    var result = client.AddBulkFlightpakFuel(objFuelFileData);
                    if (File.Exists(objFuelFileData.FullPhysicalFilePath))
                    {
                        string filepath = Path.GetDirectoryName(objFuelFileData.FullPhysicalFilePath) + @"\" + Path.GetFileNameWithoutExtension(objFuelFileData.FullPhysicalFilePath) + ".csv";
                        if (Path.GetFileName(filepath) == objFuelFileData.FileName)
                        {
                            FileValidators fileValidName = new FileValidators();
                            filepath = fileValidName.CleanFullPath(filepath);
                            var ValidateFolderPart = ConfigurationManager.AppSettings["FuelCsvFilesFolder"].ToLower();
                            if (File.Exists(filepath) && filepath.ToLower().Contains(ValidateFolderPart))
                                File.Delete(filepath);
                        }
                    }
                    
                    if (result.ReturnFlag == true)
                    {
                        StringBuilder msg = new StringBuilder(@"<b>");
                        msg.Append("<br/>Total saved records:");
                        msg.Append(result.EntityInfo.Count.ToString());// Total Records Saved
                        msg.Append("<br/>Invalid records:");
                        msg.Append(result.ErrorMessage);// Invalid Records- not saved
                        if (result.EntityList[0].Count > 0)
                        {
                            HttpContext.Current.Session[FuelFileConstants.InvalidFuelRecords] = result.EntityList[0]; // Invalid Records saved in session
                            msg.Append(" <a href='FuelFileInvalidRecordsHandler.ashx?filename=" + Path.GetFileNameWithoutExtension(objFuelFileData.FullPhysicalFilePath) + "_ErrorLog'>Download Error Log</a> ");
                        }
                        msg.Append("</b>");
                        ret["Message"] = msg.ToString();
                        oFSSOperationResult.Result = ret;
                        oFSSOperationResult.StatusCode = HttpStatusCode.OK;
                        return oFSSOperationResult;
                    }
                    else
                    {
                        oFSSOperationResult.StatusCode = HttpStatusCode.NotFound;
                    }
                }
                else
                {
                    oFSSOperationResult.StatusCode = HttpStatusCode.InternalServerError;
                }
            }

            return oFSSOperationResult;
        }
    }
}