﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="CateringCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Logistics.CateringCatalog"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript">
        //this function is used to replace default value when textbox is empty
        function ValidateEmptyTextbox(ctrlID, e) {
            if (ctrlID.value == "") {
                ctrlID.value = "000.00";
            }
        }    
    </script>
    <script type="text/javascript">
        function openWin(radWin) {
            var url = '';
            if (radWin == "RadCountryMasterPopup") {
                url = '../Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('<%=tbCateringCountry.ClientID%>').value;
            }
            if (radWin == "RadMetroCityPopup") {
                url = '../Airports/MetroCityPopup.aspx?MetroCD=' + document.getElementById('<%=tbMetroCD.ClientID%>').value;
            }
            
            var oManager = $find("<%= RadWindowManager1.ClientID %>");
            var oWnd = oManager.open(url, radWin);
        }
        // To Delete record in a database,Confirming that it is not UWA record
        function deleterecord() {
            var masterTable = $find('<%= dgCateringCatalog.ClientID %>').get_masterTableView();
            var msg = 'Are you sure you want to delete this record?';
            
            var oManager = $find("<%= RadWindowManager1.ClientID %>");
            if (masterTable.get_selectedItems().length > 0) {
                var cell1 = masterTable.getCellByColumnUniqueName(masterTable.get_selectedItems()[0], "chkUWAID");
                if (cell1.innerHTML == "UWA") {
                    oManager.radalert('You Cant Delete UWA Record', 330, 100, "Delete", ""); 
                    return false;
                }
                else {
                    oManager.radconfirm(msg, callBackFn, 330, 100, '', 'Delete'); 
                    return false;
                }
            }
            else {
                //If no records are selected 
                oManager.radalert('Please select a record from the above table.', 330, 100, "Delete", ""); 
                return false;
            }
        }
        function callBackFn(confirmed) {
            if (confirmed) {
                var grid = $find('<%= dgCateringCatalog.ClientID %>');
                grid.get_masterTableView().fireCommand("DeleteSelected");
            }
        }



    </script>
    <script type="text/javascript">
        // This function is to enable required field Validator 
       
    </script>
    <script runat="server">
        //this event is used to assign  values of controls to the navigating url
        void QueryStringButton_Click(object sender, EventArgs e)
        {
            // string URL = "../Airports/AirportCatalog.aspx?Screen=Airport&IcaoID=" + hdnAirportId.Value; Response.Redirect(URL);
            string URL = "../Airports/AirportCatalog.aspx?Screen=Airport"; Response.Redirect(URL);
        }
        
        public string CityName
        {
            get
            {
                return tbCity.Text;
            }
        }
        public string StateName
        {
            get
            {
                return tbState.Text;
            }
        }
        public string CountryName
        {
            get
            {
                return tbCountry.Text;
            }
        }
        public string AirportName
        {
            get
            {
                return tbAirport.Text;
            }
        }
        //this event is used to assign  values of controls to the navigating url
        void lbtnTransportQueryString_Click(object sender, EventArgs e)
        {
            RedirectFromCatering("TransportCatalog.aspx");
        }
        
        //this event is used to assign  values of controls to the navigating url
        void lbtnHotelQueryString_Click(object sender, EventArgs e)
        {
            RedirectFromCatering("HotelCatalog.aspx");
        }
        //this event is used to assign  values of controls to the navigating url
        void lbtnFboQueryString_Click(object sender, EventArgs e)
        {
            RedirectFromCatering("FBOCatalog.aspx");
        }

        private void RedirectFromCatering(string strRedirectPageName)
        {
            string strIcao = Microsoft.Security.Application.Encoder.UrlEncode(tbICAO.Text.Trim());
            string strCity = Microsoft.Security.Application.Encoder.UrlEncode(tbCity.Text.Trim());
            string strState = Microsoft.Security.Application.Encoder.UrlEncode(tbState.Text.Trim());
            string strCountry = Microsoft.Security.Application.Encoder.UrlEncode(tbCountry.Text.Trim());
            string strAirport = Microsoft.Security.Application.Encoder.UrlEncode(tbAirport.Text.Trim());
            string strAirportId = Microsoft.Security.Application.Encoder.UrlEncode(hdnAirportId.Value);
            RedirectToPage(string.Format("../Logistics/{0}?Screen=Airport&IcaoID={1}&CityName={2}&StateName={3}&CountryName={4}&AirportName={5}&AirportID={6}", strRedirectPageName, strIcao, strCity, strState, strCountry, strAirport, strAirportId));
        }

    </script>
    <script type="text/javascript">
        function CheckTxtBox(control) {
            if (control == 'Name') {
                ValidatorEnable(document.getElementById('<%=rfvName.ClientID%>'));
            }
        }   
 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgCateringCatalog">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <table style="width: 100%;" cellpadding="0" cellspacing="0" id="table5" runat="server">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">
                        <asp:LinkButton ID="LinkButton1" Text="Airport" runat="server" OnClick="QueryStringButton_Click"
                            OnClientClick="document.forms[0].target = '_self';"></asp:LinkButton>&nbsp;>
                        Catering</span><span class="tab-nav-icons" />
                </div>
            </td>
        </tr>
    </table>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadCountryMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="onClientCloseCountryPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadMetroCityPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="onClientCloseMetroCityPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/MetroCityPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadExchangeMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseExchangeRatePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ExchangeRateMasterpopup.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            // this function is used to close the pop up
            function confirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); //GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                }
            }
            //this function is used to display the value of selected country code from popup
            function onClientCloseCountryPopup(oWnd, args) {
                var combo = $find("<%= tbCountry.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCateringCountry.ClientID%>").value = arg.CountryCD;
                        document.getElementById("<%=hdnCountryId.ClientID%>").value = arg.CountryID;
                        document.getElementById("<%=cvCountry.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbCateringCountry.ClientID%>").value = "";
                        document.getElementById("<%=hdnCountryId.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            //this function is used to display the value of selected Payment code from popup
            function OnClientCloseExchangeRatePopup(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbExchangeRate.ClientID%>").value = arg.ExchangeRateCD;
                        document.getElementById("<%=cvExchangeRate.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbExchangeRate.ClientID%>").value = "";
                        document.getElementById("<%=cvExchangeRate.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }
            //this function is used to display the value of selected metro code from popup
            function onClientCloseMetroCityPopup(oWnd, args) {
                var combo = $find("<%= tbMetroCD.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbMetroCD.ClientID%>").value = arg.MetroCD;
                        document.getElementById("<%=hdnMetroId.ClientID%>").value = arg.MetroID;
                        document.getElementById("<%=cvMetro.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbMetroCD.ClientID%>").value = "";
                        document.getElementById("<%=hdnMetroId.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            //this function is used to get dimension
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            //this function is used to get radwindow frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) {
                    oWindow = window.radWindow;
                }
                else if (window.frameElement.radWindow) {
                    oWindow = window.frameElement.radWindow;
                }
                return oWindow;
            }

            function LinkToMapQuest() {
                var ReturnValue = false;
                var grid = $find("<%=dgCateringCatalog.ClientID %>").get_masterTableView();
                var length = grid.get_selectedItems().length;
                var tbAddr1 = document.getElementById("<%=tbAddr1.ClientID%>").value;
                var tbCateringCity = document.getElementById("<%=tbCateringCity.ClientID%>").value;
                var tbStateProvince = document.getElementById("<%=tbStateProvince.ClientID%>").value;
                var tbCateringCountry = document.getElementById("<%=tbCateringCountry.ClientID%>").value;
                if (length > 0) {
                    var URL = "http://www.mapquest.com/maps?address=" + encodeURIComponent(tbAddr1) + "&city=" + tbCateringCity + "&state=" + tbStateProvince + "&country=" + tbCateringCountry + "&redirect=true";
                    window.open(URL);
                }
                return ReturnValue;
            }

            function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSaveFlag.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            }
            $(document).ready(function () {
                UpdateConfirm();
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <table cellpadding="0" cellspacing="0" width="100%" class="sticky" runat="server"
        id="table1">
        <tr>
            <td class="tdLabel40">
                ICAO
            </td>
            <td class="tdLabel60">
                <asp:TextBox ID="tbICAO" Enabled="false" CssClass="readonly-text40" runat="server"></asp:TextBox>
                <asp:HiddenField ID="hdnAirportId" runat="server" />
            </td>
            <td class="tdLabel35">
                City
            </td>
            <td class="tdLabel150">
                <asp:TextBox ID="tbCity" Enabled="false" CssClass="readonly-text100" runat="server"></asp:TextBox>
            </td>
            <td class="tdLabel45">
                State
            </td>
            <td class="tdLabel150">
                <asp:TextBox ID="tbState" Enabled="false" CssClass="readonly-text100" runat="server"></asp:TextBox>
            </td>
            <td class="tdLabel60">
                Country
            </td>
            <td class="tdLabel60">
                <asp:TextBox ID="tbCountry" Enabled="false" CssClass="readonly-text30" runat="server"></asp:TextBox>
            </td>
            <td class="tdLabel50">
                Airport
            </td>
            <td>
                <asp:TextBox ID="tbAirport" Enabled="false" CssClass="readonly-text120" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" class="head-sub-menu" id="table4" runat="server">
        <tr>
            <td>
                <div class="tags_select">
                    <asp:LinkButton ID="lbtnFbo" Text="FBO" runat="server" OnClick="lbtnFboQueryString_Click"></asp:LinkButton>
                    <asp:LinkButton ID="lbtnHotel" Text="Hotel" runat="server" OnClick="lbtnHotelQueryString_Click"></asp:LinkButton>
                    <asp:LinkButton ID="lbtnTransport" Text="Transportation" runat="server" OnClick="lbtnTransportQueryString_Click"></asp:LinkButton>
                    <%--<asp:LinkButton ID="lbtnCatering" Text="Catering" runat="server" OnClick="lbtnCateringQueryString_Click"></asp:LinkButton>--%>
                </div>
            </td>
        </tr>
    </table>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <table width="100%" cellpadding="0" cellspacing="0" runat="server" id="table2" class="head-sub-menu">
            <tr>
                <td class="tdLabel120" align="left">
                    <asp:CheckBox ID="chkDisplayInctive" runat="server" Text="Display Inactive" OnCheckedChanged="chkDisplayInctive_CheckedChanged"
                        AutoPostBack="true" />
                </td>
                <td align="left">
                    <asp:CheckBox ID="chkDisplayChoiceOnly" runat="server" Text="Display Choice Only"
                        OnCheckedChanged="chkDisplayInctive_CheckedChanged" AutoPostBack="true" />
                </td>
                <td align="right">
                    <%-- <asp:LinkButton runat="server" ID="lnkMap" OnClick="lbtnMapQueryString_Click" CssClass="map-icon"
                                ToolTip="View Map" OnClientClick="document.forms[0].target = '_blank';" Style="display: none;" />--%>
                    <asp:LinkButton runat="server" ID="lnkMapQuest" CssClass="map-icon" ToolTip="View Map"
                        OnClientClick="javascript:LinkToMapQuest();" />
                </td>
            </tr>
        </table>
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <telerik:RadGrid ID="dgCateringCatalog" runat="server" AllowSorting="true" OnItemCreated="dgCateringCatalog_ItemCreated"
                OnItemDataBound="dgCateringCatalog_ItemDataBound" OnNeedDataSource="dgCateringCatalog_BindData"
                OnItemCommand="dgCateringCatalog_ItemCommand" OnUpdateCommand="dgCateringCatalog_UpdateCommand"
                OnInsertCommand="dgCateringCatalog_InsertCommand" OnDeleteCommand="dgCateringCatalog_DeleteCommand"
                AutoGenerateColumns="false" PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgCateringCatalog_SelectedIndexChanged"
                AllowFilteringByColumn="true" OnPreRender="dgCateringCatalog_PreRender" OnPageIndexChanged="dgCateringCatalog_PageIndexChanged"
                Height="341px">
                <MasterTableView DataKeyNames="AirportID,CateringID,CateringCD,IsChoice,CateringVendor,PhoneNUM,IsInActive,CustomerID,FaxNUM,ContactName,UWAUpdates,Remarks,NegotiatedRate,LastUpdTS,LastUpdUID,UpdateDT,RecordType,SourceID,ControlNUM,IsInactive,TollFreePhoneNum,WebSite,UWAMaintFlag,UWAID,IsDeleted,BusinessEmail,ContactBusinessPhone,CellPhoneNum,ContactEmail,Addr1,Addr2,Addr3,City,StateProvince,MetroCD,CountryCD,PostalZipCD,Filter,ExchangeRateID,NegotiatedTerms,SundayWorkHours,MondayWorkHours,TuesdayWorkHours,WednesdayWorkHours,ThursdayWorkHours,FridayWorkHours,SaturdayWorkHours"
                    CommandItemDisplay="Bottom" Width="100%">
                    <Columns>
                        <telerik:GridBoundColumn DataField="CateringCD" HeaderText="Catering Code" CurrentFilterFunction="StartsWith"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="60px" FilterControlWidth="40px"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsChoice" HeaderText="Choice" ShowFilterIcon="false"
                            AutoPostBackOnFilter="true" HeaderStyle-Width="50px" AllowFiltering="false">
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridBoundColumn DataField="CateringVendor" HeaderText="Name" CurrentFilterFunction="StartsWith"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="360px" FilterDelay="500"
                            FilterControlWidth="340px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PhoneNUM" HeaderText="Business Phone" CurrentFilterFunction="StartsWith"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="140px" FilterDelay="500"
                            FilterControlWidth="120px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Filter" DataField="Filter" UniqueName="chkUWAID"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterDelay="500"
                            HeaderStyle-Width="80px" FilterControlWidth="60px">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" ShowFilterIcon="false"
                            AutoPostBackOnFilter="true" AllowFiltering="false" >
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridBoundColumn DataField="AirportID" HeaderText="AirportID" CurrentFilterFunction="EqualTo"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CateringID" HeaderText="CateringID" CurrentFilterFunction="EqualTo"
                            Display="false">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div style="padding: 5px 5px; float: left; clear: both;">
                            <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                Visible='<%# IsAuthorized(Permission.Database.AddCatering)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                Visible='<%# IsAuthorized(Permission.Database.EditCatering)%>' ToolTip="Edit"
                                CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>"  /></asp:LinkButton>
                            <asp:LinkButton ID="lnkCopy" OnClick="lnkCopy_Click" OnClientClick="javascript:return ProcessCopy();"
                                runat="server" ToolTip="Copy">
                    <img style="border:0px;vertical-align:middle;" alt="Copy" src="<%=ResolveClientUrl("~/App_Themes/Default/images/copy.png") %>"/></asp:LinkButton>
                            <asp:LinkButton ID="lbtnDelete" OnClientClick="javascript:return deleterecord();"
                                Visible='<%# IsAuthorized(Permission.Database.DeleteCatering)%>' runat="server"
                                CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>"  /></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
            <table cellpadding="0" cellspacing="0" runat="server" id="table3" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table class="border-box">
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td class="tdLabel80">
                                    <asp:CheckBox ID="chkChoice" runat="server" />Choice
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkInactive" runat="server" />Inactive
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" valign="top">
                                    Catering Code
                                </td>
                                <td class="tdLabel240" valign="top">
                                    <asp:TextBox ID="tbCaterCode" runat="server" CssClass="text50" MaxLength="4" onKeyPress="return fnAllowNumeric(this, event)"
                                        AutoPostBack="true"></asp:TextBox>
                                </td>
                                <td class="tdLabel140" valign="top">
                                    <span class="mnd_text">Company Name</span>
                                </td>
                                <td class="tdLabel200" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbName" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="tbName"
                                                    ValidationGroup="Save" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">Company Name is Required.</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140">
                                    Address Line 1
                                </td>
                                <td class="tdLabel240">
                                    <asp:TextBox ID="tbAddr1" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                </td>
                                <td class="tdLabel140">
                                    Address Line 2
                                </td>
                                <td>
                                    <asp:TextBox ID="tbAddr2" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" valign="top">
                                    Address Line 3
                                </td>
                                <td class="tdLabel240">
                                    <asp:TextBox ID="tbAddr3" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                </td>
                                <td class="tdLabel140">
                                    City
                                </td>
                                <td>
                                    <asp:TextBox ID="tbCateringCity" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140">
                                    State/Province
                                </td>
                                <td class="tdLabel240">
                                    <asp:TextBox ID="tbStateProvince" runat="server" CssClass="text200" MaxLength="10"></asp:TextBox>
                                </td>
                                <td class="tdLabel140">
                                    Metro
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbMetroCD" runat="server" CssClass="text50" MaxLength="3" AutoPostBack="true"
                                                    OnTextChanged="tbMetroCD_TextChanged"></asp:TextBox>
                                                <asp:Button ID="btnMetro" runat="server" OnClientClick="javascript:openWin('RadMetroCityPopup');return false;"
                                                    CssClass="browse-button" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator ID="cvMetro" runat="server" ControlToValidate="tbMetroCD" ErrorMessage="Invalid Metro Code."
                                                    Display="Dynamic" class="alert-text" ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                <asp:HiddenField ID="hdnMetroId" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140">
                                    Country
                                </td>
                                <td class="tdLabel240">
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbCateringCountry" runat="server" CssClass="text50" MaxLength="3"
                                                    AutoPostBack="true" OnTextChanged="tbCateringCountry_TextChanged"></asp:TextBox>
                                                <asp:Button runat="server" ID="btnCountry" CssClass="browse-button" OnClientClick="javascript:openWin('RadCountryMasterPopup');return false;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator ID="cvCountry" runat="server" ControlToValidate="tbCateringCountry"
                                                    ErrorMessage="Invalid Country Code." Display="Dynamic" CssClass="alert-text"
                                                    ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                <asp:HiddenField ID="hdnCountryId" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                 <td class="tdLabel140" align="left" valign="top">
                                    Postal
                                </td>
                                <td valign="top">
                                    <asp:TextBox ID="tbPostalCode" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" valign="top">
                                    Contact Name
                                </td>
                                <td class="tdLabel240" valign="top">
                                    <asp:TextBox ID="tbContact" runat="server" CssClass="text200" MaxLength="25"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" valign="top">
                                    Business Phone
                                </td>
                                <td class="tdLabel240" valign="top">
                                    <asp:TextBox ID="tbPhone" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140">
                                    Contact Business Phone
                                </td>
                                <td class="tdLabel240">
                                    <asp:TextBox ID="tbContactBusinessPhone" runat="server" CssClass="text200" MaxLength="25"
                                        onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                </td>
                                <td class="tdLabel140">
                                    Toll-free Phone
                                </td>
                                <td>
                                    <asp:TextBox ID="tbTollFreeNo" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140">
                                    Contact Mobile/Cell
                                </td>
                                <td class="tdLabel240">
                                    <asp:TextBox ID="tbContactMobile" runat="server" CssClass="text200" MaxLength="25"
                                        onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                </td>
                                <td class="tdLabel140">
                                    Business Fax
                                </td>
                                <td>
                                    <asp:TextBox ID="tbFax" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140">
                                    Business E-mail
                                </td>
                                <td class="tdLabel240">
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbBusinessEmail" runat="server" CssClass="text200" MaxLength="250"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revBusinessEmail" runat="server" ControlToValidate="tbBusinessEmail"
                                                    ValidationGroup="Save" ErrorMessage="Invalid E-mail Format" ValidationExpression="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
                                                    Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel140">
                                    Contact E-mail
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbContactEmail" runat="server" CssClass="text200" MaxLength="250"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revContactEmail" runat="server" ControlToValidate="tbContactEmail"
                                                    ValidationGroup="Save" ErrorMessage="Invalid E-mail Format" ValidationExpression="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
                                                    Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" valign="top">
                                    Web site
                                </td>
                                <td class="tdLabel240" valign="top">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbWebsite" runat="server" CssClass="text200" MaxLength="160"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" valign="top">
                                    Negotiated Price
                                </td>
                                <td class="pr_radtextbox_200" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <%--<asp:TextBox ID="tbNegotiatedPrice" runat="server" CssClass="text200" MaxLength="6"
                                                    onKeyPress="return fnAllowNumericAndChar(this,event,'.') " onBlur="return ValidateEmptyTextbox(this, event)"></asp:TextBox>--%>
                                                <telerik:RadNumericTextBox ID="tbNegotiatedPrice" runat="server" Type="Currency"
                                                    Culture="en-US" MaxLength="3" Value="0.00" NumberFormat-DecimalSeparator="."
                                                    ValidationGroup="Save" EnabledStyle-HorizontalAlign="Right">
                                                </telerik:RadNumericTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revNegotiatedPrice" runat="server" ControlToValidate="tbNegotiatedPrice"
                                                    ValidationGroup="Save" ErrorMessage="Invalid Format (000.00)" ValidationExpression="^[0-9]{0,3}(\.[0-9]{1,2})?$"
                                                    Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" valign="top">
                                    Exchange Rate
                                </td>
                                <td align="left" class="tdLabel240" valign="top">
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbExchangeRate" runat="server" OnTextChanged="ExchangeRate_TextChanged"
                                                    AutoPostBack="true" CssClass="text40" MaxLength="6"></asp:TextBox>
                                                <asp:HiddenField ID="hdnExchangeRateID" runat="server" />
                                                <asp:Button ID="btnSearchExchange" runat="server" OnClientClick="javascript:openWin('RadExchangeMasterPopup');return false;"
                                                    CssClass="browse-button" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator ID="cvExchangeRate" runat="server" ControlToValidate="tbExchangeRate"
                                                    ErrorMessage="Invalid Exchange Rate Code." Display="Dynamic" CssClass="alert-text"
                                                    ValidationGroup="Save"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel140" valign="top">
                                    Negotiated Terms
                                </td>
                                <td valign="top">
                                    <asp:TextBox ID="tbNegotiateTerm" runat="server" TextMode="MultiLine" Height="30px"
                                        CssClass="text190" MaxLength="800"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" valign="top">
                                    Sunday Work Hours
                                </td>
                                <td valign="top" class="tdLabel240">
                                    <asp:TextBox ID="tbSundayWorkHours" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                </td>
                                <td class="tdLabel140" valign="top">
                                    Monday Work Hours
                                </td>
                                <td valign="top">
                                    <asp:TextBox ID="tbMondayWorkHours" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" valign="top">
                                    Tuesday Work Hours
                                </td>
                                <td valign="top" class="tdLabel240">
                                    <asp:TextBox ID="tbTuesdayWorkHours" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                </td>
                                <td class="tdLabel140" valign="top">
                                    Wednesday Work Hours
                                </td>
                                <td valign="top">
                                    <asp:TextBox ID="tbWednesdayWorkHours" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" valign="top">
                                    Thursday Work Hours
                                </td>
                                <td valign="top" class="tdLabel240">
                                    <asp:TextBox ID="tbThursdayWorkHours" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                </td>
                                <td class="tdLabel140" valign="top">
                                    Friday Work Hours
                                </td>
                                <td valign="top">
                                    <asp:TextBox ID="tbFridayWorkHours" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" valign="top">
                                    Saturday Work Hours
                                </td>
                                <td valign="top" class="tdLabel240">
                                    <asp:TextBox ID="tbSaturdayWorkHours" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    Remarks
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="tbRemarks" runat="server" TextMode="MultiLine" CssClass="textarea-db-tbl"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <asp:Label ID="lbWarningMessage" Text="WARNING! This record is maintained by Universal.Information entered may be changed."
                            CssClass="alert-text" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <table class="pad-top-btm" cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td align="right">
                        <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                            <tr align="right">
                                <td>
                                    <asp:Button ID="btnSaveChanges" Text="Save" runat="server" ValidationGroup="Save"
                                        OnClick="SaveChanges_Click" CssClass="button" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancel" Text="Cancel" runat="server" OnClick="Cancel_Click" CssClass="button" />
                                    <asp:HiddenField ID="hdnSaveFlag" runat="server" />
                                    <asp:HiddenField ID="hdnIsUWa" runat="server" />
                                    <asp:HiddenField ID="hdnRedirect" runat="server" /> 
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
