﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="HotelCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Logistics.HotelCatalog"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" ValidateRequest="false" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<%@ Import Namespace="FlightPak.Web.Framework.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <link href="../../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
      
    </script>
    <script type="text/javascript">
        //this function is used to navigate to pop up screen's with the selected code
        function openWin(radWin) {
            var url = '';
            if (radWin == "RadCountryPopup") {
                url = '../Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('<%=tbCtry.ClientID%>').value;
            }
            else if (radWin == "RadMetroCityPopup") {
                url = '../Airports/MetroCityPopup.aspx?MetroCD=' + document.getElementById("<%=tbMetro.ClientID%>").value;
            }
            
            var oManager = $find("<%= RadWindowManager1.ClientID %>");
            var oWnd = oManager.open(url, radWin);
        }
        // To Delete record in a database,Confirming that it is not UWA record
        function deleterecord() {
            var masterTable = $find('<%= dgHotelCatalog.ClientID %>').get_masterTableView();
            var msg = 'Are you sure you want to delete this record?';
            
            var oManager = $find("<%= RadWindowManager1.ClientID %>");
            if (masterTable.get_selectedItems().length > 0) {
                var cell1 = masterTable.getCellByColumnUniqueName(masterTable.get_selectedItems()[0], "chkUWAID");
                if (cell1.innerHTML == "UWA") {
                    oManager.radalert('You Cant Delete UWA Record', 330, 100, "Delete", ""); 
                    return false;
                }
                else {
                    oManager.radconfirm(msg, callBackFn, 330, 100, '', 'Delete'); 
                    return false;
                }
            }
            else {
                //If no records are selected 
                oManager.radalert('Please select a record from the above table.', 330, 100, "Delete", ""); 
                return false;
            }
        }
        function callBackFn(confirmed) {
            if (confirmed) {
                var grid = $find('<%= dgHotelCatalog.ClientID %>');
                grid.get_masterTableView().fireCommand("DeleteSelected");
            }
        }
    </script>
    <script runat="server">
        // setting the properties for controls for navigation
        public string IcaoID
        {
            get
            {
                return tbAirportICAO.Text;
            }
        }
        public string address
        {
            get
            {
                return tbAddr1.Text;
            }
        }
        public string city
        {
            get
            {
                return tbCity.Text;
            }
        }
        public string state
        {
            get
            {
                return tbStateProv.Text;
            }
        }
        public string zipcode
        {
            get
            {
                return tbPostalCode.Text;
            }
        }
        //this event is used to assign  values of controls to the navigating url
        void lbtnAirportQueryString_Click(object sender, EventArgs e)
        {
            //string url = "../Airports/AirportCatalog.aspx?Screen=Airport&IcaoID=" + hdnAirportID.Value; Response.Redirect(url);
            string URL = "../Airports/AirportCatalog.aspx?Screen=Airport"; Response.Redirect(URL);
        }
        //Added for Map button
        void lbtnMapQueryString_Click(object sender, EventArgs e)
        {
            if (dgHotelCatalog.Items.Count > 0)
            {
                RedirectToPage(string.Format("{0}?address={1}&city={2}&state={3}&zipcode={4}&country={5}&redirect=true", WebConstants.URL.MapQuest, Microsoft.Security.Application.Encoder.HtmlEncode(tbAddr1.Text), Microsoft.Security.Application.Encoder.HtmlEncode(tbCity.Text), Microsoft.Security.Application.Encoder.HtmlEncode(tbStateProv.Text), Microsoft.Security.Application.Encoder.HtmlEncode(tbPostalCode.Text), Microsoft.Security.Application.Encoder.HtmlEncode(tbCtry.Text)));
            }


        }
        //this event is used to assign  values of controls to the navigating url

        public string CountryName
        {
            get
            {
                return tbAirportCountry.Text;
            }
        }
        public string AirportName
        {
            get
            {
                return tbAirport.Text;
            }
        }
        //this event is used to assign  values of controls to the navigating url
        void lbtnTransportQueryString_Click(object sender, EventArgs e)
        {
            RedirectToPage(String.Format("{0}?Screen=Airport&IcaoID={1}&CityName={2}&StateName={3}&CountryName={4}&AirportName={5}&AirportID={6}", WebConstants.URL.TransportCatalog, Microsoft.Security.Application.Encoder.UrlEncode(tbAirportICAO.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbAirportCity.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbAirportState.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbAirportCountry.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbAirport.Text), Microsoft.Security.Application.Encoder.UrlEncode(hdnAirportID.Value)));
        }
        //this event is used to assign  values of controls to the navigating url
        void lbtnCateringQueryString_Click(object sender, EventArgs e)
        {
            RedirectToPage(String.Format("{0}?Screen=Airport&IcaoID={1}&CityName={2}&StateName={3}&CountryName={4}&AirportName={5}&AirportID={6}", WebConstants.URL.CateringCatalog, Microsoft.Security.Application.Encoder.UrlEncode(tbAirportICAO.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbAirportCity.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbAirportState.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbAirportCountry.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbAirport.Text), Microsoft.Security.Application.Encoder.UrlEncode(hdnAirportID.Value)));
        }
        //this event is used to assign  values of controls to the navigating url
        //void lbtnHotelQueryString_Click(object sender, EventArgs e)
        //{
        //    string url = "../Logistics/HotelCatalog.aspx?Screen=Airport&IcaoID=" + tbAirportICAO.Text + "&CityName=" + tbCity.Text + "&StateName=" + tbStateProv.Text + "&CountryName=" + tbAirportCountry.Text + "&AirportName=" + tbAirport.Text + "&AirportID=" + hdnAirportID.Value; Response.Redirect(url);
        //}
        //this event is used to assign  values of controls to the navigating url
        void lbtnFboQueryString_Click(object sender, EventArgs e)
        {
            RedirectToPage(String.Format("{0}?Screen=Airport&IcaoID={1}&CityName={2}&StateName={3}&CountryName={4}&AirportName={5}&AirportID={6}", WebConstants.URL.FBOCatalog, Microsoft.Security.Application.Encoder.UrlEncode(tbAirportICAO.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbAirportCity.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbAirportState.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbAirportCountry.Text), Microsoft.Security.Application.Encoder.UrlEncode(tbAirport.Text), Microsoft.Security.Application.Encoder.UrlEncode(hdnAirportID.Value)));
        }               
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <table style="width: 100%;" cellpadding="0" cellspacing="0" runat="server" id="table1">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">
                        <asp:LinkButton ID="lbtnAirport" Text="Airport" runat="server" OnClick="lbtnAirportQueryString_Click"
                            OnClientClick="document.forms[0].target = '_self';"></asp:LinkButton>&nbsp;>
                        Hotel</span><span class="tab-nav-icons"><!--<a href="#" class="search-icon"></a><a href="#" class="save-icon">
                    </a><a href="#" class="print-icon"></a>--><a href="#" class="help-icon"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellspacing="0" cellpadding="0" class="sticky" runat="server"
        id="table2">
        <tr>
            <td class="tdLabel40">
                ICAO
            </td>
            <td class="tdLabel60">
                <asp:TextBox ID="tbAirportICAO" Enabled="false" CssClass="readonly-text40" runat="server"></asp:TextBox>
                <asp:HiddenField ID="hdnAirportID" runat="server" />
            </td>
            <td class="tdLabel35">
                City
            </td>
            <td class="tdLabel150">
                <asp:TextBox ID="tbAirportCity" Enabled="false" CssClass="readonly-text100" runat="server"></asp:TextBox>
            </td>
            <td class="tdLabel45">
                State
            </td>
            <td class="tdLabel150">
                <asp:TextBox ID="tbAirportState" Enabled="false" CssClass="readonly-text100" runat="server"></asp:TextBox>
            </td>
            <td class="tdLabel60">
                Country
            </td>
            <td class="tdLabel60">
                <asp:TextBox ID="tbAirportCountry" Enabled="false" CssClass="readonly-text30" runat="server"></asp:TextBox>
            </td>
            <td class="tdLabel50">
                Airport
            </td>
            <td>
                <asp:TextBox ID="tbAirport" Enabled="false" CssClass="readonly-text120" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" class="head-sub-menu" id="table4" runat="server">
        <tr>
            <td>
                <div class="tags_select">
                    <asp:LinkButton ID="lbtnFbo" Text="FBO" runat="server" OnClick="lbtnFboQueryString_Click"></asp:LinkButton>
                    <%--<asp:LinkButton ID="lbtnHotel" Text="Hotel" runat="server" OnClick="lbtnHotelQueryString_Click"></asp:LinkButton>--%>
                    <asp:LinkButton ID="lbtnTransport" Text="Transportation" runat="server" OnClick="lbtnTransportQueryString_Click"></asp:LinkButton>
                    <asp:LinkButton ID="lbtnCatering" Text="Catering" runat="server" OnClick="lbtnCateringQueryString_Click"></asp:LinkButton>
                </div>
            </td>
        </tr>
    </table>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="divExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgHotelCatalog">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            //this function is used to display the value of selected country code from popup
            function OnClientCloseCountryPopup(oWnd, args) {
                var combo = $find("<%= tbCtry.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCtry.ClientID%>").value = arg.CountryCD;
                        document.getElementById("<%=hdnCtry.ClientID%>").value = arg.CountryID;
                        document.getElementById("<%=cvCtry.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbCtry.ClientID%>").value = "";
                        document.getElementById("<%=hdnCtry.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            //this function is used to display the value of selected Payment code from popup
            function OnClientCloseExchangeRatePopup(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbExchangeRate.ClientID%>").value = arg.ExchangeRateCD;
                        document.getElementById("<%=cvExchangeRate.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbExchangeRate.ClientID%>").value = "";
                        document.getElementById("<%=cvExchangeRate.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }
            //this function is used to display the value of selected metro code from popup
            function OnClientCloseMetroCityPopup(oWnd, args) {
                var combo = $find("<%= tbMetro.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbMetro.ClientID%>").value = arg.MetroCD;
                        document.getElementById("<%=hdnMetro.ClientID%>").value = arg.MetroID;
                        document.getElementById("<%=cvMetro.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbMetro.ClientID%>").value = "";
                        document.getElementById("<%=hdnMetro.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            //this function is used to get dimension
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            //this function is used to get radwindow frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function LinkToMapQuest() {
                var ReturnValue = false;
                var grid = $find("<%=dgHotelCatalog.ClientID %>").get_masterTableView();
                var length = grid.get_selectedItems().length;
                var tbAddr1 = document.getElementById("<%=tbAddr1.ClientID%>").value;
                var tbCity = document.getElementById("<%=tbCity.ClientID%>").value;
                var tbStateProv = document.getElementById("<%=tbStateProv.ClientID%>").value;
                var tbPostalCode = document.getElementById("<%=tbPostalCode.ClientID%>").value;
                var tbCtry = document.getElementById("<%=tbCtry.ClientID%>").value;
                if (length > 0) {
                    var URL = "http://www.mapquest.com/maps?address=" + encodeURIComponent(tbAddr1) + "&city=" + tbCity + "&state=" + tbStateProv + "&zipcode=" + tbPostalCode + "&country=" + tbCtry + "&redirect=true";
                    window.open(URL);
                }
                return ReturnValue;
            }
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSaveFlag.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadCountryMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseCountryPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadMetroCityPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseMetroCityPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/MetroCityPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadExchangeMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseExchangeRatePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ExchangeRateMasterpopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radAddExchangeRate" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ExchangeRateMasterpopup.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <div id="divExternalForm" runat="server" class="ExternalForm">
        <table cellpadding="0" cellspacing="0" class="head-sub-menu" runat="server" id="table3">
            <tr>
                <td align="left" class="tdLabel120">
                    <asp:CheckBox ID="chkDisplayInctive" runat="server" Text="Display Inactive" OnCheckedChanged="chkDisplayInctive_CheckedChanged"
                        AutoPostBack="true" />
                </td>
                <td align="left" class="tdLabel100">
                    <asp:CheckBox ID="chkCrewOnly" runat="server" Text="Crew Only" OnCheckedChanged="chkDisplayInctive_CheckedChanged"
                        AutoPostBack="true" />
                </td>
                <td align="left">
                    <asp:CheckBox ID="chkPassengerOnly" runat="server" Text="PAX Only" OnCheckedChanged="chkDisplayInctive_CheckedChanged"
                        AutoPostBack="true" />
                </td>
                <td align="right">
                    <%--<asp:LinkButton runat="server" ID="lnkMap" OnClick="lbtnMapQueryString_Click" CssClass="map-icon"
                        ToolTip="View Map" OnClientClick="document.forms[0].target = '_blank';" Style="display: none;" />--%>
                    <asp:LinkButton runat="server" ID="lnkMapQuest" CssClass="map-icon" ToolTip="View Map"
                        OnClientClick="javascript:LinkToMapQuest();" />
                </td>
            </tr>
        </table>    
    
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <telerik:radgrid id="dgHotelCatalog" runat="server" AllowSorting="true" visible="true"
                OnItemCreated="dgHotelCatalog_ItemCreated" OnNeedDataSource="dgHotelCatalog_BindData"
                OnItemDataBound="dgHotelCatalog_ItemDataBound" OnItemCommand="dgHotelCatalog_ItemCommand"
                OnUpdateCommand="dgHotelCatalog_UpdateCommand" OnInsertCommand="dgHotelCatalog_InsertCommand"
                OnDeleteCommand="dgHotelCatalog_DeleteCommand" AutoGenerateColumns="false" PageSize="10"
                OnPreRender="dgHotelCatalog_PreRender" OnPageIndexChanged="dgHotelCatalog_PageIndexChanged"
                AllowPaging="true" OnSelectedIndexChanged="dgHotelCatalog_SelectedIndexChanged"
                AllowFilteringByColumn="true" Height="341px">
                <MasterTableView DataKeyNames="HotelID,AirportID,HotelCD,IsChoice,Name,PhoneNum,MilesFromICAO,HotelRating,FaxNum,Filter,
                    NegociatedRate,Addr1,Addr2,Addr3,CityName,MetroCD,StateName,PostalZipCD,
                    CountryCD,ContactName,Remarks,IsPassenger,IsCrew,IsUWAMaintained,LatitudeDegree,
                    LatitudeMinutes,LatitudeNorthSouth,LongitudeDegrees,LongitudeMinutes,
                    LongitudeEastWest,LastUpdUID,LastUpdTS,UpdateDT,RecordType,MinutesFromICAO,
                    SourceID,ControlNum,Website,UWAMaintFlag,UWAID,IsInActive,IsDeleted,UWAUpdates,IcaoID,MetroID,CountryID,ContactBusinessPhone,CellPhoneNum,ContactEmail,BusinessEmail,AltBusinessPhone,TollFreePhone,WebSite,ExchangeRateID,NegotiatedTerms,SundayWorkHours,MondayWorkHours,TuesdayWorkHours,WednesdayWorkHours,ThursdayWorkHours,FridayWorkHours,SaturdayWorkHours"
                    CommandItemDisplay="Bottom" ClientDataKeyNames="UWAID" Width="100%">
                    <Columns>
                        <telerik:GridBoundColumn DataField="HotelCD" HeaderText="Code" CurrentFilterFunction="StartsWith"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="60px" FilterControlWidth="30px"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsCrew" HeaderText="Crew" ShowFilterIcon="false"
                            CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" HeaderStyle-Width="50px"
                            AllowFiltering="false">
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridCheckBoxColumn DataField="IsPassenger" HeaderText="PAX" ShowFilterIcon="false"
                            CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" HeaderStyle-Width="40px"
                            AllowFiltering="false">
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridBoundColumn DataField="Name" HeaderText="Hotel Name" CurrentFilterFunction="StartsWith"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="150px" FilterDelay="500"
                            FilterControlWidth="130px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PhoneNum" HeaderText="Business Phone" CurrentFilterFunction="StartsWith"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="100px" FilterDelay="500"
                            FilterControlWidth="80px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="HotelRating" HeaderText="Rating" CurrentFilterFunction="StartsWith"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="70px" FilterControlWidth="50px"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="MilesFromICAO" HeaderText="Miles" CurrentFilterFunction="EqualTo"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="60px" FilterControlWidth="40px"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="IcaoID" HeaderText="From" CurrentFilterFunction="StartsWith"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="50px" FilterControlWidth="30px"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="MinutesFromICAO" HeaderText="Min" CurrentFilterFunction="EqualTo"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="40px" FilterControlWidth="20px"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="NegociatedRate" HeaderText="Negotiated Price" FilterDelay="500"
                            CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="false" ShowFilterIcon="false"
                            HeaderStyle-Width="80px" FilterControlWidth="60px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="MetroCD" HeaderText="Metro" CurrentFilterFunction="StartsWith"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="60px" FilterControlWidth="40px"
                            FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Filter" DataField="Filter" UniqueName="chkUWAID" FilterDelay="500"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                            HeaderStyle-Width="60px" FilterControlWidth="40px">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" ShowFilterIcon="false"
                            AutoPostBackOnFilter="true" AllowFiltering="false" HeaderStyle-Width="60px">
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridBoundColumn DataField="AirportID" HeaderText="AirportID" CurrentFilterFunction="EqualTo"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="UWAMaintFlag" HeaderText="UWAMaintFlag" CurrentFilterFunction="EqualTo"
                            Display="false" AllowFiltering="False" />
                        <telerik:GridBoundColumn DataField="HotelID" HeaderText="HotelID" CurrentFilterFunction="EqualTo"
                            Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LastUpdUID" HeaderText="LastUpdUID" CurrentFilterFunction="StartsWith"
                            Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LastUpdTS" HeaderText="LastUpdTS" CurrentFilterFunction="EqualTo"
                            Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="MetroID" HeaderText="MetroID" CurrentFilterFunction="EqualTo"
                            Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CountryID" HeaderText="CountryID" CurrentFilterFunction="EqualTo"
                            Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FaxNum" HeaderText="FaxNum" CurrentFilterFunction="StartsWith"
                            Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ContactName" HeaderText="ContactName" CurrentFilterFunction="StartsWith"
                            Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Addr1" HeaderText="Addr1" CurrentFilterFunction="StartsWith"
                            Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Addr2" HeaderText="Addr2" CurrentFilterFunction="StartsWith"
                            Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Addr3" HeaderText="Addr3" CurrentFilterFunction="StartsWith"
                            Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CityName" HeaderText="CityName" CurrentFilterFunction="StartsWith"
                            Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="StateName" HeaderText="StateName" CurrentFilterFunction="StartsWith"
                            Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PostalZipCD" HeaderText="PostalZipCD" CurrentFilterFunction="StartsWith"
                            Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CountryCD" HeaderText="CountryCD" CurrentFilterFunction="StartsWith"
                            Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LatitudeDegree" HeaderText="Latitude Degree"
                            CurrentFilterFunction="EqualTo" Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LatitudeMinutes" HeaderText="LatitudeMinutes"
                            CurrentFilterFunction="EqualTo" Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LongitudeEastWest" HeaderText="LongitudeEastWest"
                            CurrentFilterFunction="StartsWith" Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Remarks" HeaderText="Remarks" CurrentFilterFunction="StartsWith"
                            Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="WebSite" HeaderText="Web site" CurrentFilterFunction="StartsWith"
                            Display="false" AllowFiltering="False">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div style="padding: 5px 5px; float: left; clear: both;">
                            <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                Visible='<%# IsAuthorized(Permission.Database.AddHotel)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                Visible='<%# IsAuthorized(Permission.Database.EditHotel)%>' ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                            <asp:LinkButton ID="lnkCopy" OnClick="lnkCopy_Click" OnClientClick="javascript:return ProcessCopy();"
                                runat="server" ToolTip="Copy">
                    <img style="border:0px;vertical-align:middle;" alt="Copy" src="<%=ResolveClientUrl("~/App_Themes/Default/images/copy.png") %>"/></asp:LinkButton>
                            <asp:LinkButton ID="lbtnInitDelete" runat="server" OnClientClick="javascript:return deleterecord();"
                                Visible='<%# IsAuthorized(Permission.Database.DeleteHotel)%>' CommandName="DeleteSelected"
                                ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                        </div>
                        <div>
                            <asp:Label runat="server" ID="lbLastUpdatedUser" CssClass="last-updated-text"></asp:Label>
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:radgrid>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table class="border-box">
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td class="tdLabel100" align="left">
                                    <asp:CheckBox ID="chkCrewHotel" runat="server" />Crew Hotel
                                </td>
                                <td align="left" class="tdLabel130">
                                    <asp:CheckBox ID="chkPassengerHotel" runat="server" />Passenger Hotel
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkInactive" runat="server" />Inactive
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" align="left" valign="top">
                                    Hotel Code
                                </td>
                                <td class="tdLabel240" valign="top">
                                    <asp:TextBox ID="tbHotelCode" runat="server" CssClass="readonly-text80 inpt_non_edit"></asp:TextBox>
                                </td>
                                <td class="tdLabel130" align="left" valign="top">
                                    <span class="mnd_text">Hotel Name</span>
                                </td>
                                <td class="tdLabel200">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left">
                                                <asp:TextBox ID="tbName" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="Hotel Name is Required."
                                                    ControlToValidate="tbName" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" align="left">
                                    Address 1
                                </td>
                                <td class="tdLabel240">
                                    <asp:TextBox ID="tbAddr1" runat="server" CssClass="text200" MaxLength="40" onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                </td>
                                <td class="tdLabel130" align="left">
                                    Address 2
                                </td>
                                <td class="tdLabel200">
                                    <asp:TextBox ID="tbAddr2" runat="server" CssClass="text200" MaxLength="40" onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" align="left" valign="top">
                                    Address 3
                                </td>
                                <td class="tdLabel240" valign="top">
                                    <asp:TextBox ID="tbAddr3" runat="server" CssClass="text200" MaxLength="40" onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                </td>
                                <td class="tdLabel130" align="left" valign="top">
                                    Metro
                                </td>
                                <td class="tdLabel200">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="left">
                                                <asp:TextBox ID="tbMetro" runat="server" CssClass="text35" MaxLength="3" AutoPostBack="true"
                                                    OnTextChanged="tbMetroCD_TextChanged"></asp:TextBox>
                                                <asp:HiddenField ID="hdnMetro" runat="server" />
                                                <asp:Button ID="btnMetro" runat="server" OnClientClick="javascript:openWin('RadMetroCityPopup');return false;"
                                                    CssClass="browse-button" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator ID="cvMetro" runat="server" ControlToValidate="tbMetro" ErrorMessage="Invalid Metro Code."
                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" align="left">
                                    City
                                </td>
                                <td class="tdLabel240">
                                    <asp:TextBox ID="tbCity" runat="server" CssClass="text200" MaxLength="30"></asp:TextBox>
                                </td>
                                <td class="tdLabel130">
                                    State/Prov
                                </td>
                                <td class="tdLabel200" align="left">
                                    <asp:TextBox ID="tbStateProv" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                        onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" valign="top">
                                    Country
                                </td>
                                <td align="left" class="tdLabel240">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="left">
                                                <asp:TextBox ID="tbCtry" runat="server" CssClass="text35" MaxLength="3" AutoPostBack="true"
                                                    OnTextChanged="tbCountry_TextChanged"></asp:TextBox>
                                                <asp:HiddenField ID="hdnCtry" runat="server" />
                                                <asp:Button ID="btnCtry" runat="server" OnClientClick="javascript:openWin('RadCountryMasterPopup');return false;"
                                                    CssClass="browse-button" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:CustomValidator ID="cvCtry" runat="server" ControlToValidate="tbCtry" ErrorMessage="Invalid Country Code."
                                                    Display="Dynamic" CssClass="alert-text"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel130" align="left" valign="top">
                                    Postal
                                </td>
                                <td class="tdLabel200" valign="top">
                                    <asp:TextBox ID="tbPostalCode" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" align="left">
                                    Contact Name
                                </td>
                                <td class="tdLabel240" align="left">
                                    <asp:TextBox ID="tbContact" runat="server" CssClass="text200" MaxLength="30"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" align="left">
                                    Business Phone
                                </td>
                                <td class="tdLabel240" align="left">
                                    <asp:TextBox ID="tbPhone" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                </td>
                                <td class="tdLabel130" align="left">
                                    Alt Business Phone
                                </td>
                                <td>
                                    <asp:TextBox ID="tbAltbusinessPhone" runat="server" CssClass="text200" MaxLength="25"
                                        onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140">
                                    Contact Business Phone
                                </td>
                                <td class="tdLabel240">
                                    <asp:TextBox ID="tbcontactBusinessPhone" runat="server" CssClass="text200" MaxLength="25"
                                        onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                </td>
                                <td class="tdLabel130">
                                    Toll-free Phone
                                </td>
                                <td>
                                    <asp:TextBox ID="tbTollFreePhone" runat="server" CssClass="text200" MaxLength="25"
                                        onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" align="left">
                                    Contact Mobile/Cell
                                </td>
                                <td class="tdLabel240">
                                    <asp:TextBox ID="tbContactMobile" runat="server" CssClass="text200" MaxLength="25"
                                        onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                </td>
                                <td class="tdLabel130">
                                    Business Fax
                                </td>
                                <td>
                                    <asp:TextBox ID="tbFax" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" align="left">
                                    Business E-mail
                                </td>
                                <td class="tdLabel240">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbBusinessEmail" runat="server" CssClass="text200" MaxLength="250"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="tbBusinessEmail"
                                                    ValidationGroup="Save" ErrorMessage="Invalid E-mail Format" ValidationExpression="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
                                                    Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel130">
                                    Contact E-mail
                                </td>
                                <td class="tdLabel200" align="left">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbContactEmail" runat="server" CssClass="text200" MaxLength="250"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revContactEmail" runat="server" ControlToValidate="tbContactEmail"
                                                    ValidationGroup="Save" ErrorMessage="Invalid E-mail Format" ValidationExpression="^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$"
                                                    Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" align="left">
                                    Web site
                                </td>
                                <td class="tdLabel240" align="left">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbWebsite" runat="server" CssClass="text200" MaxLength="200"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <%--  <asp:RegularExpressionValidator ID="revWebsite" runat="server" ControlToValidate="tbWebsite"
                                                    ValidationGroup="Save" ErrorMessage="Invalid Web Site Format" ValidationExpression="^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}$"
                                                    Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" align="left" valign="top">
                                    Rating
                                </td>
                                <td class="tdLabel240" valign="top">
                                    <asp:TextBox ID="tbRating" runat="server" CssClass="text200" MaxLength="3"></asp:TextBox>
                                </td>
                                <td class="tdLabel130" align="left" valign="top">
                                    Negotiated Price
                                </td>
                                <td class="pr_radtextbox_200" align="left" valign="top">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left">
                                                <%--<asp:TextBox ID="tbNegotiatedPrice" runat="server" MaxLength="17" CssClass="text200"
                                                    onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>--%>
                                                <telerik:radnumerictextbox id="tbNegotiatedPrice" runat="server" type="Currency"
                                                    culture="en-US" maxlength="14" value="0.00" numberformat-decimalseparator="."
                                                    validationgroup="Save" enabledstyle-horizontalalign="Right">
                                                </telerik:radnumerictextbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <asp:RangeValidator ID="rvNegotiatedPrice" runat="server" ControlToValidate="tbLongMin"
                                                    Type="Double" MinimumValue="0" MaximumValue="99999999999999" ValidationGroup="Save"
                                                    CssClass="alert-text" ErrorMessage="0-99999999999999" Display="Dynamic"></asp:RangeValidator>
                                                <asp:RegularExpressionValidator ID="revNegotiatedPrice" runat="server" ControlToValidate="tbNegotiatedPrice"
                                                    ValidationGroup="Save" ErrorMessage="99999999999999.99" ValidationExpression="^[0-9]{0,14}(\.[0-9]{0,2})?$"
                                                    Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" align="left" valign="top">
                                    Latitude
                                </td>
                                <td class="tdLabel240" align="left" valign="top">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left" class="tdLabel45" valign="top">
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <asp:TextBox ID="tbLatDeg" runat="server" CssClass="text35" onKeyPress="return fnAllowNumeric(this, event)"
                                                                MaxLength="2" onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <asp:RangeValidator ID="rvLatDeg" runat="server" ControlToValidate="tbLatDeg" Type="Integer"
                                                                MinimumValue="0" MaximumValue="90" ValidationGroup="Save" CssClass="alert-text"
                                                                ErrorMessage="0-90" Display="Dynamic"></asp:RangeValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="left" class="tdLabel45" valign="top">
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <asp:TextBox ID="tbLatMin" runat="server" CssClass="text35" MaxLength="4" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <asp:RangeValidator ID="rvLatMin" runat="server" Type="Double" ControlToValidate="tbLatMin"
                                                                MinimumValue="0" MaximumValue="60" ErrorMessage="0-60" ValidationGroup="Save"
                                                                CssClass="alert-text" Display="Dynamic" SetFocusOnError="true"></asp:RangeValidator>
                                                            <asp:RegularExpressionValidator ID="revLatMin" runat="server" ControlToValidate="tbLatMin"
                                                                ValidationGroup="Save" ErrorMessage="60.0" ValidationExpression="^\d+(\.\d)$"
                                                                Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="left" class="tdLabel45" valign="top">
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <asp:TextBox ID="tbLatNS" runat="server" CssClass="text22" MaxLength="1" onKeyPress="return fnAllowAlpha(this, event)"
                                                                onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <asp:RegularExpressionValidator ID="revLatNS" runat="server" Display="Dynamic" ControlToValidate="tbLatNS"
                                                                ErrorMessage="N or S" ValidationExpression="[nN]|[sS]" CssClass="alert-text"
                                                                ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel130" align="left" valign="top">
                                    Longitude
                                </td>
                                <td class="tdLabel200" align="left" valign="top">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="tdLabel45" valign="top">
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <asp:TextBox ID="tbLongDeg" runat="server" MaxLength="3" CssClass="text35" onKeyPress="return fnAllowNumeric(this, event)"
                                                                onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <asp:RangeValidator ID="rvLongDeg" runat="server" ControlToValidate="tbLongDeg" Type="Integer"
                                                                MinimumValue="0" MaximumValue="180" ValidationGroup="Save" CssClass="alert-text"
                                                                ErrorMessage="0-180" Display="Dynamic"></asp:RangeValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="tdLabel45" valign="top">
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <asp:TextBox ID="tbLongMin" runat="server" MaxLength="4" CssClass="text35" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <asp:RangeValidator ID="rvLongMin" runat="server" ControlToValidate="tbLongMin" Type="Double"
                                                                MinimumValue="0" MaximumValue="60" ValidationGroup="Save" CssClass="alert-text"
                                                                ErrorMessage="0-60" Display="Dynamic"></asp:RangeValidator>
                                                            <asp:RegularExpressionValidator ID="revLongMin" runat="server" ControlToValidate="tbLongMin"
                                                                ValidationGroup="Save" ErrorMessage="60.0" ValidationExpression="^\d+(\.\d)$"
                                                                Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="tdLabel45" valign="top">
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <asp:TextBox ID="tbLongEW" runat="server" MaxLength="1" CssClass="text22" onKeyPress="return fnAllowAlpha(this, event)"
                                                                onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <asp:RegularExpressionValidator ID="revLongEW" runat="server" ControlToValidate="tbLongEW"
                                                                ValidationGroup="Save" ErrorMessage="E or W" ValidationExpression="[eE]|[wW]"
                                                                Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" align="left">
                                    Distance From ICAO
                                </td>
                                <td class="tdLabel90">
                                    <asp:TextBox ID="tbDistanceFromICAO" runat="server" CssClass="readonly-text70 inpt_non_edit"></asp:TextBox>
                                </td>
                                <td class="tdLabel50" align="left">
                                    Miles
                                </td>
                                <td class="tdLabel100" align="left">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbMiles" runat="server" CssClass="text35" MaxLength="6" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revMiles" runat="server" Display="Dynamic" ErrorMessage="0000.0"
                                                    ControlToValidate="tbMiles" CssClass="alert-text" ValidationExpression="^[0-9]{0,4}(\.[0-9]{0,1})?$"
                                                    ValidationGroup="Save"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel130">
                                    Minutes
                                </td>
                                <td class="tdLabel130" align="left">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbMinutes" runat="server" CssClass="text35" MaxLength="6" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RegularExpressionValidator ID="revMts" runat="server" Display="Dynamic" ErrorMessage="0000.0"
                                                    ControlToValidate="tbMinutes" CssClass="alert-text" ValidationExpression="^[0-9]{0,4}(\.[0-9]{0,1})?$"
                                                    ValidationGroup="Save"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" valign="top">
                                    Exchange Rate
                                </td>
                                <td align="left" class="tdLabel240" valign="top">
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbExchangeRate" runat="server" OnTextChanged="ExchangeRate_TextChanged"
                                                    AutoPostBack="true" CssClass="text40" MaxLength="6"></asp:TextBox>
                                                <asp:HiddenField ID="hdnExchangeRateID" runat="server" />
                                                <asp:Button ID="btnSearchExchange" runat="server" OnClientClick="javascript:openWin('RadExchangeMasterPopup');return false;"
                                                    CssClass="browse-button" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator ID="cvExchangeRate" runat="server" ControlToValidate="tbExchangeRate"
                                                    ErrorMessage="Invalid Exchange Rate Code." Display="Dynamic" CssClass="alert-text"
                                                    ValidationGroup="Save"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel130" valign="top">
                                    Negotiated Terms
                                </td>
                                <td valign="top">
                                    <asp:TextBox ID="tbNegotiateTerm" runat="server" TextMode="MultiLine" Height="30px"
                                        CssClass="text190" MaxLength="800"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" valign="top">
                                    Sunday Work Hours
                                </td>
                                <td valign="top" class="tdLabel240">
                                    <asp:TextBox ID="tbSundayWorkHours" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                </td>
                                <td class="tdLabel140" valign="top">
                                    Monday Work Hours
                                </td>
                                <td valign="top">
                                    <asp:TextBox ID="tbMondayWorkHours" runat="server" CssClass="text190" MaxLength="15"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" valign="top">
                                    Tuesday Work Hours
                                </td>
                                <td valign="top" class="tdLabel240">
                                    <asp:TextBox ID="tbTuesdayWorkHours" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                </td>
                                <td class="tdLabel140" valign="top">
                                    Wednesday Work Hours
                                </td>
                                <td valign="top">
                                    <asp:TextBox ID="tbWednesdayWorkHours" runat="server" CssClass="text190" MaxLength="15"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" valign="top">
                                    Thursday Work Hours
                                </td>
                                <td valign="top" class="tdLabel240">
                                    <asp:TextBox ID="tbThursdayWorkHours" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                </td>
                                <td class="tdLabel140" valign="top">
                                    Friday Work Hours
                                </td>
                                <td valign="top">
                                    <asp:TextBox ID="tbFridayWorkHours" runat="server" CssClass="text190" MaxLength="15"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel140" valign="top">
                                    Saturday Work Hours
                                </td>
                                <td valign="top" class="tdLabel240">
                                    <asp:TextBox ID="tbSaturdayWorkHours" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel130" align="left">
                                    Remarks
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="tbRemarks" runat="server" TextMode="MultiLine" CssClass="textarea-db-tbl"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <asp:Label ID="lbWarningMessage" Text="WARNING! This record is maintained by Universal.Information entered may be changed."
                            CssClass="alert-text" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr align="right">
                    <td>
                        <asp:Button ID="btnSaveChanges" Text="Save" runat="server" ValidationGroup="Save"
                            OnClick="SaveChanges_Click" CssClass="button" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" OnClick="btnCancel_Click"
                            CausesValidation="false" CssClass="button" />
                        <asp:HiddenField ID="hdnSaveFlag" runat="server" />
                        <asp:HiddenField ID="hdnIsUWa" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" /> 
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
