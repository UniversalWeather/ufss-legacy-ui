﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head runat="server">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" /> 
    <title>Catering</title>
    <style>

        .BackRed {
            color:red;
        }
        .grid-checkbox {
            padding-left:24px !important;
        }
    </style>
<link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
<link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
<script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
<script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="/Scripts/Common.js"></script>
    <script type="text/javascript">
        var selectedRowData = null;
        var deleteCatering = null;
        var deleteCateringName = null;
        var jqgridTableId = '#gridAirports';
        var selectedCateringCD = '';

        $(document).ready(function () {
            selectedCateringCD = $.trim(unescape(getQuerystring("CateringCD", "")));
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });
            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                if (rowData['CateringID'] == undefined) {
                    showMessageBox('Please select a catering.', popupTitle);
                } else {
                    returnToParent(rowData);
                }
                return false;
            });

            $("#recordAdd").click(function () {
                var widthDoc = $(document).width();
                //popupwindow("/Views/Settings/Logistics/CateringCatalog.aspx?IsPopup=Add&AirportID=" + getQuerystring("IcaoID", ""), popupTitle, widthDoc + 92, 798, jqgridTableId);
                openWinAddEdit("/Views/Settings/Logistics/CateringCatalog.aspx?FromCaterLookup=true&IsPopup=Add&AirportID=" + getQuerystring("IcaoID", ""), "rdAddPopupWindow", widthDoc + 92, 798, jqgridTableId);
                $(jqgridTableId).trigger('reloadGrid');
                return false;
            });

            $("#recordDelete").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                deleteCatering = rowData['CateringID'];
                deleteCateringName = rowData['CateringVendor'];
               
                if (deleteCatering != undefined && deleteCatering != null && deleteCatering != '' && deleteCatering != 0) {
                    showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                }
                else {
                    showMessageBox('Please select a catering.', popupTitle);
                }
                return false;
            });

            function confirmCallBackFn(arg) {
                if (arg) {
                    $.ajax({
                        type: 'GET',
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=Catering&CateringtId=' + deleteCatering,
                        contentType: 'text/html',
                        success: function(data) {
                            var message = "This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.";
                            DeleteApiResponse(jqgridTableId, data, message, popupTitle);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;                          
                            if (content.indexOf('404'))
                                showMessageBox("This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.", popupTitle);
                        }
                    });
                }
            }
            $("#recordEdit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                var airportId = rowData['AirportID'];
                var CateringID = rowData['CateringID'];
                var widthDoc = $(document).width();
                if (airportId != undefined && airportId != null && airportId != '' && airportId != 0) {
                    //popupwindow("/Views/Settings/Logistics/CateringCatalog.aspx?IsPopup=&AirportID=" + airportId + "&CateringID=" + CateringID, popupTitle, widthDoc + 92, 798, jqgridTableId);
                    openWinAddEdit("/Views/Settings/Logistics/CateringCatalog.aspx?FromCaterLookup=true&IsPopup=&AirportID=" + airportId + "&CateringID=" + CateringID, "rdEditPopupWindow", widthDoc + 92, 798, jqgridTableId);
                    $(jqgridTableId).trigger('reloadGrid');
                }
                else {
                    showMessageBox('Please select a catering.', popupTitle);
                }
                return false;
            });

            $("#chkInActive").change(function () {
                $(jqgridTableId).trigger('reloadGrid');
            });
            $("#chkChoice").change(function () {
                $(jqgridTableId).trigger('reloadGrid');
            });
            
            $(document).ready(function () {
                jQuery(jqgridTableId).jqGrid({
                    url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                    mtype: 'GET',
                    datatype: "json",
                    ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                    serializeGridData: function (postData) {
                        if (postData._search == undefined || postData._search == false) {
                            if (postData.filters === undefined) postData.filters = null;
                        }
                        postData.apiType = 'fss';
                        postData.method = 'catering';
                        postData.FetchActiveOnly = $("#chkInActive").prop('checked');
                        postData.FetchChoiceOnly = $("#chkChoice").prop('checked');
                        postData.CateringID = 0;
                       // postData.CateringCD = "_";
                        postData.sendCustomerId = "true";
                        postData.AirportID = getQuerystring("IcaoID","");

                        return postData;
                    },
                    height: 320,
                    width: 840,
                    viewrecords: true,
                    shrinkToFit: true,
                    rowNum: $("#rowNum").val(),
                    multiselect: false,
                    pager: "#pg_gridPager",
                    colNames: ['CateringID', 'AirportID', 'ContactName', 'ContactEmail', 'Code', 'Catering Service', 'Choice', 'Phone', 'Fax', 'Rate', 'Filter', 'Inactive'],
                    colModel: [
                        { name: 'CateringID', index: 'CateringID', key: true, hidden: true },
                        { name: 'AirportID', index: 'AirportID', hidden: true },
                         { name: 'ContactName', index: 'ContactName', hidden: true },
                        { name: 'ContactEmail', index: 'ContactEmail', hidden: true },
                        { name: 'CateringCD', index: 'CateringCD', width: 20 },
                        { name: 'CateringVendor', index: 'CateringVendor', width: 100 },
                        { name: 'IsChoice', index: 'IsChoice', width: 20, formatter: "checkbox",classes:"grid-checkbox" , formatoptions: { disabled: true }, search: false },
                        { name: 'PhoneNum', index: 'PhoneNum', width: 33 },
                        { name: 'FaxNum', index: 'FaxNum', width: 33 },
                        { name: 'NegotiatedRate', index: 'NegotiatedRate', width: 22, formatter: 'number', formatoptions: { decimalPlaces: 2 }, searchoptions: { sopt: ['eq'] } },
                        {
                            name: 'Filter', index: 'Filter', width: 25, cellattr: function (rowId, cellValue, rawObject, cm, rdata) {
                                if (cellValue == "UWA") {
                                    return 'class="BackRed"';
                                }
                            }
                        },
                         { name: 'IsInactive', index: 'IsInactive', width: 22, formatter: "checkbox", classes: "grid-checkbox", formatoptions: { disabled: true }, search: false }
                    ],
                    ondblClickRow: function (rowId) {
                        var rowData = jQuery(this).getRowData(rowId);
                        returnToParent(rowData);
                    },
                    loadComplete: function (data) {
                        var airportId = getUrlVars()["IcaoID"];
                        $.ajax({
                            type: 'GET',
                            url: '/Views/Utilities/GETApi.aspx?apiType=FSS&method=AirportByAirportId&airportId=' + airportId,
                            contentType: 'text/html',
                            success: function (rowData, textStatus, xhr) {
                                rowData = $.parseJSON($('<div/>').html(rowData).text());
                                
                                if (rowData != undefined && rowData != null) {
                                    var icao = rowData.IcaoID;
                                    var cityName = rowData.CityName;
                                    var stateName = rowData.StateName;
                                    var countryName = rowData.CountryName;
                                    var airportName = rowData.AirportName;
                                    if (icao != undefined && icao != null)
                                        $("#lbICAO").text(icao);
                                    if (cityName != undefined && cityName != null)
                                        $("#lbCity").text(cityName);
                                    if (stateName != undefined && stateName != null)
                                        $("#lbState").text(stateName);
                                    if (countryName != undefined && countryName != null)
                                        $("#lbCountry").text(countryName);
                                    if (airportName != undefined && airportName != null)
                                        $("#lbAirport").text(airportName);
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                //Check for any errors
                            }
                        });
                    },
                    onSelectRow: function (id) {
                        var rowData = $(this).getRowData(id);
                        var lastSel = rowData['CateringID'];//replace name with any column
                        selectedCateringCD = $.trim(rowData['CateringCD']);

                        if (id !== lastSel) {
                            $(this).find(".selected").removeClass('selected');
                            $('#results_table').jqGrid('resetSelection', lastSel, true);
                            $(this).find('.ui-state-highlight').addClass('selected');
                            lastSel = id;
                        }
                    },
                    afterInsertRow: function (rowid, rowdata, rowelem) {
                        if (rowdata.Filter == 'UWA') {
                            jQuery(jqgridTableId).setCell(rowid, 'Filter', '', 'red-text');
                        }

                        var lastSel = rowdata['CateringCD'];//replace name with any column
                        

                        if ($.trim(selectedCateringCD) == $.trim(lastSel) && selectedCateringCD != "") {
                            $(this).find(".selected").removeClass('selected');
                            $(this).find('.ui-state-highlight').addClass('selected');
                            $(jqgridTableId).jqGrid('setSelection', rowid);
                        }
                    }
                  
                });
                $("#pagesizebox").insertBefore('.ui-paging-info');
                $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });

            });

        });


        function returnToParent(rowData) {
            var oArg = new Object();
            oArg.CateringCD = rowData["CateringCD"];
            oArg.CateringVendor = rowData["CateringVendor"];
            oArg.PhoneNum = rowData["PhoneNum"];
            oArg.FaxNum = rowData["FaxNum"];
            oArg.NegotiatedRate = rowData["NegotiatedRate"];
            oArg.AirportID = rowData["AirportID"];
            oArg.CateringID = rowData["CateringID"];
            oArg.ContactName = rowData["ContactName"];
            oArg.ContactEmail = rowData["ContactEmail"];

            var oWnd = GetRadWindow();
            if (oArg) {
                oWnd.close(oArg);
            }
        }
        function Close() {
            GetRadWindow().Close();
        }

        function reloadPageSize() {
            var myGrid = $(jqgridTableId);
            var currentValue = $("#rowNum").val();
            myGrid.setGridParam({ rowNum: currentValue });
            myGrid.trigger('reloadGrid');
        }

        function rebindgrid()
        {
            $(jqgridTableId).setGridParam({ datatype: 'json', page: 1 }).trigger('reloadGrid');
            
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">

        <div class="divGridPanel">

            <div class="divGridPanel">
                <table class="box1">
                    <tr>
                         <td class="tdLabel30">
                            ICAO:
                        </td>
                        <td class="tdLabel40">
                            <span id="lbICAO"></span>
                        </td>
                        <td class="tdLabel40">
                            City:
                        </td>
                        <td class="tdLabel100">
                            <span id="lbCity"></span>
                        </td>
                        <td class="tdLabel40">
                            State:
                        </td>
                        <td class="tdLabel50">
                            <span id="lbState"></span>
                        </td>
                        <td class="tdLabel30">
                            Country:
                        </td>
                        <td class="tdLabel70">
                            <span id="lbCountry"></span>
                        </td>
                        <td class="tdLabel40">
                            Airport:
                        </td>
                        <td class="tdLabel150">
                            <span id="lbAirport"></span>
                        </td>
                    </tr>

                </table>
                 <div class="jqgrid">
                            <div>
                                <table class="box1">
                                    <tr>
                                        <td align="left">
                                            <div>
                                                <input type="checkbox" name="chkInActive" id="chkInActive" />
                                                Display Inactive 
                                                <input type="checkbox" name="chkChoice" id="chkChoice" />
                                                Display Choice Only 
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table id="gridAirports" style="width: 981px !important;" class="table table-striped table-hover table-bordered"></table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="grid_icon">
                                                <div role="group" id="pg_gridPager"></div>
                                                <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                                                <span class="Span">Page Size:</span>
                                                <input class="PageSize" id="rowNum" type="text" value="20" maxlength="5" />
                                                <input id="btnChange" class="btnChange" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(); return false;" />
                                            </div>
                                            <div style="padding: 5px 5px; text-align: right;">
                                                <input id="btnSubmit" class="button okButton" value="OK"  type="button"/>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
            </div>
        </div>
    </form>
</body>
</html>
