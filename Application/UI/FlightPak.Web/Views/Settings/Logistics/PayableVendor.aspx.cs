﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using System.Text.RegularExpressions;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.Views.Settings.Logistics
{
    public partial class PayableVendor : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        public List<string> lstPayableVendorMasterCodes = new List<string>();
        #region constants
        private const string ConstIsInactive = "IsInActive";
        #endregion
        private ExceptionManager exManager;
        private bool PayableVendorPageNavigated = false;
        private string strPayableVendorID = "";
        private string strPayableVendorCD = "";
        private bool _selectLastModified = false;

        private void CreateDictionayForImgUpload()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> DicImgage = new Dictionary<string, byte[]>();
                Session["DicImg"] = DicImgage;
                Dictionary<string, string> DicImageDelete = new Dictionary<string, string>();
                Session["DicImgDelete"] = DicImageDelete;
            }
        }
        protected void DeleteImage_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];
                        int count = DicImage.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                        if (count > 0)
                        {
                            DicImage.Remove(ddlImg.Text.Trim());
                            Session["DicImg"] = DicImage;
                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                            if (dicImgDelete.Count(D => D.Key.Equals(ddlImg.Text.Trim())) == 0)
                            {
                                dicImgDelete.Add(ddlImg.Text.Trim(), "");
                            }
                            Session["DicImgDelete"] = dicImgDelete;
                            ddlImg.Items.Clear();
                            ddlImg.Text = "";
                            tbImgName.Text = "";
                            int i = 0;
                            foreach (var DicItem in DicImage)
                            {
                                ddlImg.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                                i = i + 1;
                            }
                            if (ddlImg.Items.Count > 0)
                            {
                                Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                int Count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                                Session["Base64"] = null;   //added for image issue
                                if (Count > 0)
                                {
                                    //Ramesh: Set the URL for image file or link based on the file type
                                    SetURL(ddlImg.Text, dicImg[ddlImg.Text.Trim()]);

                                    ////start of modification for image issue
                                    //if (hdnBrowserName.Value == "Chrome")
                                    //{
                                    //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                    //}
                                    //else
                                    //{
                                    //    Session["Base64"] = dicImg[ddlImg.Text.Trim()];
                                    //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                    //}
                                    ////end of modification for image issue
                                }
                            }
                            else
                            {
                                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                                lnkFileName.NavigateUrl = "";
                            }
                            ImgPopup.ImageUrl = null;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }
        }
        private void LoadImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                System.IO.Stream MyStream;
                Int32 FileLen;
                Session["Base64"] = null;   //added for image issue
                if (fileUL.PostedFile != null)
                {
                    if (fileUL.PostedFile.ContentLength > 0)
                    {
                        //if (ddlImg.Items.Count >= 0)
                        //{
                            string FileName = fileUL.FileName;
                            FileLen = fileUL.PostedFile.ContentLength;
                            Byte[] Input = new Byte[FileLen];
                            MyStream = fileUL.FileContent;
                            MyStream.Read(Input, 0, FileLen);

                            //Ramesh: Set the URL for image file or link based on the file type
                            SetURL(FileName, Input);

                            ////start of modification for image issue
                            //if (hdnBrowserName.Value == "Chrome")
                            //{
                            //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                            //}
                            //else
                            //{
                            //    Session["Base64"] = Input;
                            //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                            //}
                            ////end of modification for image issue
                            Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];
                            //Commented for removing document name
                            //if (tbImgName.Text.Trim() != "")
                            //{
                            //    FileName = tbImgName.Text.Trim();
                            //}
                            int Count = DicImage.Count(D => D.Key.Equals(FileName));
                            if (Count > 0)
                            {
                                DicImage[FileName] = Input;
                            }
                            else
                            {
                                DicImage.Add(FileName, Input);
                                string encodedFileName = Microsoft.Security.Application.Encoder.HtmlEncode(FileName);
                                ddlImg.Items.Insert(ddlImg.Items.Count, new ListItem(encodedFileName, encodedFileName));

                                //Ramesh: Set the URL for image file or link based on the file type
                                SetURL(FileName, Input);
                            }
                            tbImgName.Text = "";
                            if (ddlImg.Items.Count != 0)
                            {
                                //ddlImg.SelectedIndex = ddlImg.Items.Count - 1;
                                ddlImg.SelectedValue = FileName;
                            }
                            btndeleteImage.Enabled = true;
                        //}
                    }
                }
            }
        }
        protected void ddlImg_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ddlImg.Text.Trim() == "")
                        {
                            imgFile.ImageUrl = "";
                            lnkFileName.NavigateUrl = "";
                        }
                        if (ddlImg.SelectedItem != null)
                        {
                            bool ImgFound = false;
                            Session["Base64"] = null;   //added for image issue
                            imgFile.ImageUrl = "";
                            lnkFileName.NavigateUrl = "";
                            using (FlightPakMasterService.MasterCatalogServiceClient ImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (Session["SelectedPayableVendorID"] != null)
                                {
                                    //removed tolower conversion for image issue
                                    var ReturnImg = ImgService.GetFileWarehouseList("PayableVendor", Convert.ToInt64(Session["SelectedPayableVendorID"].ToString())).EntityList.Where(x => x.UWAFileName == ddlImg.Text.Trim());
                                    foreach (FlightPakMasterService.FileWarehouse FWH in ReturnImg)
                                    {
                                        ImgFound = true;
                                        byte[] Picture = FWH.UWAFilePath;

                                        //Ramesh: Set the URL for image file or link based on the file type
                                        SetURL(FWH.UWAFileName, Picture);

                                        ////start of modification for image issue
                                        //if (hdnBrowserName.Value == "Chrome")
                                        //{
                                        //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Picture);
                                        //}
                                        //else
                                        //{
                                        //    Session["Base64"] = Picture;
                                        //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                        //}
                                        ////end of modification for image issue
                                    }
                                }
                                //tbImgName.Text = ddlImg.Text.Trim();   //Commented for removing document name
                                if (!ImgFound)
                                {
                                    Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                    int Count = dicImg.Count(D => D.Key.Equals(ddlImg.Text.Trim()));
                                    if (Count > 0)
                                    {
                                        //Ramesh: Set the URL for image file or link based on the file type
                                        SetURL(ddlImg.Text, dicImg[ddlImg.Text.Trim()]);

                                        ////start of modification for image issue
                                        //if (hdnBrowserName.Value == "Chrome")
                                        //{
                                        //    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlImg.Text.Trim()]);
                                        //}
                                        //else
                                        //{
                                        //    Session["Base64"] = dicImg[ddlImg.Text.Trim()];
                                        //    imgFile.ImageUrl = "../ImageHandler.ashx?cd=" + Guid.NewGuid();
                                        //}
                                        ////end of modification for image issue
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgPayableVendorCatalog, dgPayableVendorCatalog, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgPayableVendorCatalog.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewPayableVendor);
                            if (!IsAuthorized(Permission.Database.AddPayableVendor) && !IsAuthorized(Permission.Database.EditPayableVendor) && !IsAuthorized(Permission.Database.DeletePayableVendor))
                            {
                                lnkPayableVendorContacts.Enabled = false;
                            }
                            if (IsPopUp && !IsAdd)
                                Session["SelectedPayableVendorID"] = Request.QueryString["SelectedPayableVendorID"];
                            // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgPayableVendorCatalog.Rebind();
                                ReadOnlyForm();
                                EnableForm(false);
                                DisableLinks();
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                if (UserPrincipal.Identity._fpSettings._IsDeactivateHomeBaseFilter == false)
                                {
                                    chkSearchHomebaseOnly.Checked = true;
                                }
                                chkSearchActiveOnly.Checked = true;
                                DefaultSelection(true);
                                CreateDictionayForImgUpload();
                                Page.ClientScript.RegisterStartupScript(this.GetType(), "DetectBrowser", "GetBrowserName();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }
        }


        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }
        }

        /// <summary>
        /// Default selection of grid
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgPayableVendorCatalog.Rebind();
                    }
                    if (dgPayableVendorCatalog.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedPayableVendorID"] = null;
                        //}
                        if (Session["SelectedPayableVendorID"] == null)
                        {
                            dgPayableVendorCatalog.SelectedIndexes.Add(0);
                            if (!IsPopUp)
                                Session["SelectedPayableVendorID"] = dgPayableVendorCatalog.Items[0].GetDataKeyValue("VendorID").ToString();
                        }

                        if (dgPayableVendorCatalog.SelectedIndexes.Count == 0)
                            dgPayableVendorCatalog.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                        GridEnable(true, true, true);
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Highlight the selected item on edit
        /// </summary>
        private void SelectItem()
        {
            if (Session["SelectedPayableVendorID"] != null)
            {
                string ID = Session["SelectedPayableVendorID"].ToString();
                foreach (GridDataItem Item in dgPayableVendorCatalog.MasterTableView.Items)
                {
                    if (Item["VendorID"].Text.Trim() == ID)
                    {
                        Item.Selected = true;
                        break;
                    }
                }
                //if (dgPayableVendorCatalog.SelectedItems.Count == 0)
                //{
                //    DefaultSelection(false);
                //}
            }
            else
            {
                DefaultSelection(false);
            }
        }

        protected void PayableVendor_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (PayableVendorPageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgPayableVendorCatalog, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }
        }


        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgPayableVendorCatalog;
                        }
                        else if (e.Initiator.ID.IndexOf("btnSaveChangesTop", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgPayableVendorCatalog;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgPayableVendorCatalog_ItemCreated(object sender, GridItemEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEditButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }

        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.Validate(group);
                        // get the first validator that failed
                        var validator = GetValidators(group)
                        .OfType<BaseValidator>()
                        .FirstOrDefault(v => !v.IsValid);
                        // set the focus to the control
                        // that the validator targets
                        if (validator != null)
                        {
                            Control target = validator
                            .NamingContainer
                            .FindControl(validator.ControlToValidate);
                            if (target != null)
                            {
                                target.Focus();
                                IsEmptyCheck = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }
        }
        /// <summary>
        /// Bind Payable vendor catalog data into grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgPayableVendorCatalog_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objPayableVendorCatalogService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objPayableVendor = objPayableVendorCatalogService.GetPayableVendorInfo();
                            if (objPayableVendor.EntityList != null && objPayableVendor.EntityList.Count > 0)
                            {
                                dgPayableVendorCatalog.DataSource = objPayableVendor.EntityList.OrderBy(x => x.VendorCD);

                                Session["PayableVendor"] = objPayableVendor.EntityList;
                            }
                        }
                        if (!IsPopUp)
                            DoSearchFilter();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }
        }

        /// <summary>
        /// Method to display insert form
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    ClearForm();
                    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                    lnkFileName.NavigateUrl = "";
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Item Command for Payable vendor catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgPayableVendorCatalog_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                if (Session["SelectedPayableVendorID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.Vendor, Convert.ToInt64(Session["SelectedPayableVendorID"].ToString().Trim()));

                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.PayableVendor);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.PayableVendor);
                                            SelectItem();
                                            return;
                                        }
                                        pnlFilterForm.Enabled = false;
                                        SelectItem();
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        ddlImg.Enabled = true;
                                        if (imgFile.ImageUrl.Trim() != "" || lnkFileName.NavigateUrl.Trim() != "")
                                        {
                                            btndeleteImage.Enabled = true;
                                        }
                                        tbImgName.Enabled = true;
                                        tbName.Focus();
                                        SelectItem();
                                        DisableLinks();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgPayableVendorCatalog.SelectedIndexes.Clear();
                                //tbCode.ReadOnly = false;
                                //tbCode.BackColor = System.Drawing.Color.White;
                                pnlFilterForm.Enabled = false;
                                DisplayInsertForm();
                                if (UserPrincipal != null && UserPrincipal.Identity._airportICAOCd != null && UserPrincipal.Identity._airportICAOCd != string.Empty)
                                {
                                    tbHomeBase.Text = UserPrincipal.Identity._airportICAOCd;
                                    if (UserPrincipal != null && UserPrincipal.Identity._airportId != 0)
                                    {
                                        hdnHomeBaseID.Value = Convert.ToString(UserPrincipal.Identity._airportId);
                                    }
                                }
                                else
                                {
                                    tbHomeBase.Text = string.Empty;

                                }
                                chkActive.Checked = true;
                                GridEnable(true, false, false);

                                DisableLinks();
                                tbImgName.Enabled = true;
                                ddlImg.Enabled = true;
                                btndeleteImage.Enabled = true;
                                tbCode.Focus();
                                break;
                            case "Filter":
                                //foreach (GridColumn Column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }
        }

        /// <summary>
        /// To Disable Links during insert or edit mode
        /// </summary>
        private void DisableLinks()
        {
            lnkPayableVendorContacts.Enabled = false;
            lnkPayableVendorContacts.CssClass = "fleet_link_disable";
            chkSearchActiveOnly.Enabled = false;
            chkSearchHomebaseOnly.Enabled = false;
        }
        /// <summary>
        /// To Enable Links during insert or edit mode
        /// </summary>
        private void EnableLinks()
        {
            lnkPayableVendorContacts.Enabled = true;
            lnkPayableVendorContacts.CssClass = "fleet_link";
            chkSearchActiveOnly.Enabled = true;
            chkSearchHomebaseOnly.Enabled = true;
        }


        /// <summary>
        /// Update Command for Payable vendor Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgPayableVendorCatalog_UpdateCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedPayableVendorID"] != null)
                        {

                            e.Canceled = true;
                            bool IsValidate = true;
                            if (CheckAlreadyHomeBaseExist())
                            {
                                cvHomeBase.IsValid = false;
                                tbHomeBase.Focus();
                                IsValidate = false;

                            }
                            if (CheckAlreadyClosestIcaoExist())
                            {
                                cvClosestIcao.IsValid = false;
                                tbClosestIcao.Focus();
                                IsValidate = false;
                            }
                            if (CheckAlreadyBillCountryExist())
                            {
                                cvBillCountry.IsValid = false;
                                tbBillCountry.Focus();
                                IsValidate = false;
                            }
                            if (CheckAlreadyMetroCodeExist())
                            {
                                cvMetro.IsValid = false;
                                tbMetro.Focus();
                                IsValidate = false;
                            }
                            if (IsValidate)
                            {
                                e.Canceled = true;
                                using (FlightPakMasterService.MasterCatalogServiceClient objPayableVendorService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FlightPakMasterService.Vendor oVendor = new FlightPakMasterService.Vendor();
                                    oVendor = GetItems(oVendor);
                                    var objRetVal = objPayableVendorService.UpdatePayableVendorMaster(oVendor);

                                    //For Data Anotation
                                    if (objRetVal.ReturnFlag == true)
                                    {
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        ///////Update Method UnLock
                                        #region Image Upload in Edit Mode
                                        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            foreach (var DicItem in dicImg)
                                            {
                                                //objService = new FlightPakMasterService.MasterCatalogServiceClient();
                                                FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                                Service.FileWarehouseID = 0;
                                                Service.RecordType = "PayableVendor";
                                                Service.UWAFileName = DicItem.Key;
                                                Service.RecordID = Convert.ToInt64(oVendor.VendorID);
                                                Service.UWAWebpageName = "PayableVendor.aspx";
                                                Service.UWAFilePath = DicItem.Value;
                                                Service.IsDeleted = false;
                                                Service.FileWarehouseID = 0;
                                                objService.AddFWHType(Service);
                                            }
                                        }
                                        #endregion
                                        #region Image Upload in Delete
                                        using (FlightPakMasterService.MasterCatalogServiceClient objFWHTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            FlightPakMasterService.FileWarehouse objFWHType = new FlightPakMasterService.FileWarehouse();
                                            Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["DicImgDelete"];
                                            foreach (var DicItem in dicImgDelete)
                                            {
                                                objFWHType.UWAFileName = DicItem.Key;
                                                objFWHType.RecordID = Convert.ToInt64(oVendor.VendorID);
                                                objFWHType.RecordType = "PayableVendor";
                                                objFWHType.IsDeleted = true;
                                                objFWHTypeService.DeleteFWHType(objFWHType);
                                            }
                                        }
                                        #endregion
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.Vendor, Convert.ToInt64(Session["SelectedPayableVendorID"].ToString().Trim()));
                                        }
                                        GridEnable(true, true, true);
                                        dgPayableVendorCatalog.Rebind();
                                        SelectItem();
                                        ReadOnlyForm();

                                        ShowSuccessMessage();

                                        EnableLinks();
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.PayableVendor);
                                    }
                                }
                            }

                        }
                        if (IsPopUp)
                        {
                            //Clear session & close browser
                            Session.Remove("SelectedPayableVendorID");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPayableVendorPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }

        }


        protected void PayableVendor_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgPayableVendorCatalog.ClientSettings.Scrolling.ScrollTop = "0";
                        PayableVendorPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }
        }

        /// <summary>
        /// Insert Command for Payable Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgPayableVendorCatalog_InsertCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        if (CheckAlreadyExist())
                        {
                            cvCode.IsValid = false;
                            tbCode.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyHomeBaseExist())
                        {
                            cvHomeBase.IsValid = false;
                            tbHomeBase.Focus();
                            IsValidate = false;

                        }
                        if (CheckAlreadyClosestIcaoExist())
                        {
                            cvClosestIcao.IsValid = false;
                            tbClosestIcao.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyBillCountryExist())
                        {
                            cvBillCountry.IsValid = false;
                            tbBillCountry.Focus();
                            IsValidate = false;
                        }
                        if (CheckAlreadyMetroCodeExist())
                        {
                            cvMetro.IsValid = false;
                            tbMetro.Focus();
                            IsValidate = false;
                        }
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient objPayableVendorCatalogService = new MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.Vendor oVendor = new FlightPakMasterService.Vendor();
                                oVendor = GetItems(oVendor);
                                var Result = objPayableVendorCatalogService.AddPayableVendorMaster(oVendor);
                                #region Image Upload in Edit Mode
                                oVendor.VendorID = Result.EntityInfo.VendorID;
                                Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["DicImg"];
                                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    foreach (var DicItem in dicImg)
                                    {
                                        FlightPakMasterService.FileWarehouse Service = new FlightPakMasterService.FileWarehouse();
                                        Service.FileWarehouseID = 0;
                                        Service.RecordType = "PayableVendor";
                                        Service.UWAFileName = DicItem.Key;
                                        Service.RecordID = Convert.ToInt64(oVendor.VendorID);
                                        Service.UWAWebpageName = "PayableVendor.aspx";
                                        Service.UWAFilePath = DicItem.Value;
                                        Service.IsDeleted = false;
                                        Service.FileWarehouseID = 0;
                                        objService.AddFWHType(Service);
                                    }
                                }
                                #endregion
                            
                                if (Result.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                    EnableLinks();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(Result.ErrorMessage, ModuleNameConstants.Database.PayableVendor);
                                }
                            }
                            //dgPayableVendorCatalog.Rebind();
                            DefaultSelection(false);
                        }
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPayableVendorPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }

        }
        /// <summary>
        /// To check unique Code  on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null)
                        {
                            CheckAlreadyExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }

        }

        /// <summary>
        /// To check unique code already exists
        /// </summary>
        /// <returns></returns>
        private bool CheckAlreadyExist()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    using (FlightPakMasterService.MasterCatalogServiceClient objservice = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objservice.GetPayableVendorInfo().EntityList.Where(x => x.VendorCD.Trim().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim()));
                        if (objRetVal.Count() > 0 && objRetVal != null)
                        {
                            cvCode.IsValid = false;
                            tbCode.Focus();
                            returnVal = true;
                        }
                        else
                        {
                            tbName.Focus();
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return returnVal;

            }




        }

        /// <summary>
        /// To check unique Home Base  on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HomeBase_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbHomeBase.Text != null)
                        {
                            CheckAlreadyHomeBaseExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }

        }

        private bool CheckAlreadyHomeBaseExist()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbHomeBase.Text != string.Empty) && (tbHomeBase.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var RetValue = Service.GetAirportByAirportICaoID(tbHomeBase.Text).EntityList;
                            //   var RetValue = Service.GetAllAirportList().EntityList.Where(x => x.IcaoID.Trim().ToUpper().Equals(tbHomeBase.Text.ToString().ToUpper().Trim()));
                            if (RetValue.Count() == 0 || RetValue == null)
                            {
                                cvHomeBase.IsValid = false;
                                tbHomeBase.Focus();
                                RetVal = true;
                            }
                            else
                            {

                                hdnHomeBaseID.Value = RetValue[0].AirportID.ToString();
                                tbHomeBase.Text = RetValue[0].IcaoID;
                                //foreach (FlightPakMasterService.GetAllAirport cm in RetValue)
                                //{
                                //    hdnHomeBaseID.Value = cm.AirportID.ToString();
                                //}
                                tbClosestIcao.Focus();
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

                return RetVal;
            }


        }
        /// <summary>
        /// To check unique Airport on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClosestIcao_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbClosestIcao.Text != null)
                        {
                            CheckAlreadyClosestIcaoExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }

        }

        private bool CheckAlreadyClosestIcaoExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbClosestIcao.Text != string.Empty) && (tbClosestIcao.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var RetValue = Service.GetAirportByAirportICaoID(tbClosestIcao.Text).EntityList;
                            if (RetValue.Count() == 0 || RetValue == null)
                            {
                                cvClosestIcao.IsValid = false;
                                tbClosestIcao.Focus();
                                RetVal = true;
                            }

                            else
                            {
                                if (RetValue[0].AirportID != null)
                                {
                                    hdnAirportID.Value = RetValue[0].AirportID.ToString();
                                    tbClosestIcao.Text = RetValue[0].IcaoID;
                                }
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }

        /// <summary>
        /// To check unique Airport on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BillCountry_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbBillCountry.Text != null)
                        {
                            CheckAlreadyBillCountryExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }

        }

        private bool CheckAlreadyBillCountryExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbBillCountry.Text != string.Empty) && (tbBillCountry.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var RetValue = Service.GetCountryMasterList().EntityList.Where(x => x.CountryCD.Trim().ToUpper().Equals(tbBillCountry.Text.ToString().ToUpper().Trim()));
                            if (RetValue.Count() == 0 || RetValue == null)
                            {
                                cvBillCountry.IsValid = false;
                                tbBillCountry.Focus();
                                RetVal = true;
                            }
                            else
                            {
                                foreach (FlightPakMasterService.Country cm in RetValue)
                                {
                                    hdnCountryID.Value = cm.CountryID.ToString();
                                    tbBillCountry.Text = cm.CountryCD;
                                }
                                tbBillPostal.Focus();
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }
        /// <summary>
        /// To check unique Airport on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Metro_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbMetro.Text != null)
                        {
                            CheckAlreadyMetroCodeExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }
        }

        private bool CheckAlreadyMetroCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbMetro.Text != string.Empty) && (tbMetro.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var RetValue = Service.GetMetroCityList().EntityList.Where(x => x.MetroCD.Trim().ToUpper().Equals(tbMetro.Text.ToString().ToUpper().Trim()));
                            if (RetValue.Count() == 0 || RetValue == null)
                            {

                                cvMetro.IsValid = false;
                                tbMetro.Focus();

                                RetVal = true;
                            }
                            else
                            {

                                foreach (FlightPakMasterService.Metro cm in RetValue)
                                {
                                    hdnMetroID.Value = cm.MetroID.ToString();
                                    tbMetro.Text = cm.MetroCD;
                                }
                                tbBillAddr1.Focus();
                                RetVal = false;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
            else
            {
                string eventArgument = (this.Request["__EVENTARGUMENT"] == null) ? string.Empty : this.Request["__EVENTARGUMENT"];
                if (eventArgument == "LoadImage")
                {
                    LoadImage();
                }
            }
            if (!IsPostBack && IsPopUp)
            {
                //Hide Controls
                dgPayableVendorCatalog.Visible = false;
                chkSearchHomebaseOnly.Visible = false;
                chkSearchActiveOnly.Visible = false;
                if (IsAdd)
                {
                    (dgPayableVendorCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                }
                else
                {
                    (dgPayableVendorCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                }
            }
            
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgPayableVendorCatalog.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgPayableVendorCatalog.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgPayableVendorCatalog_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedPayableVendorID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient PayableVendorService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.Vendor PayableVendor = new FlightPakMasterService.Vendor();
                                string Code = Session["SelectedPayableVendorID"].ToString();
                                strPayableVendorCD = "";
                                strPayableVendorID = "";
                                foreach (GridDataItem Item in dgPayableVendorCatalog.MasterTableView.Items)
                                {
                                    if (Item["VendorID"].Text.Trim() == Code.Trim())
                                    {
                                        strPayableVendorCD = Item["VendorCD"].Text.Trim();
                                        strPayableVendorID = Item["VendorID"].Text.Trim();
                                        PayableVendor.VendorType = Item.GetDataKeyValue("VendorType").ToString();
                                        PayableVendor.CustomerID = Convert.ToInt64(Item.GetDataKeyValue("CustomerID").ToString());
                                        break;
                                    }
                                }
                                PayableVendor.VendorCD = strPayableVendorCD;
                                PayableVendor.VendorID = Convert.ToInt64(strPayableVendorID);
                                PayableVendor.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.Vendor, Convert.ToInt64(Session["SelectedPayableVendorID"].ToString()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.PayableVendor);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.PayableVendor);
                                        return;
                                    }
                                }
                                PayableVendorService.DeletePayableVendorMaster(PayableVendor);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                dgPayableVendorCatalog.Rebind();
                                DefaultSelection(false);
                                GridEnable(true, true, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer                           
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.Vendor, Convert.ToInt64(Session["SelectedPayableVendorID"].ToString()));
                    }
                }
            }
        }

        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgPayableVendorCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem Item = dgPayableVendorCatalog.SelectedItems[0] as GridDataItem;
                                pnlFilterForm.Enabled = true;
                                Session["SelectedPayableVendorID"] = Item["VendorID"].Text;
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                    }
                }
            }
        }

        /// <summary>
        /// Command Event Trigger for Save or Update Vendor Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgPayableVendorCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgPayableVendorCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                            pnlFilterForm.Enabled = true;
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPayableVendorPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }
        }

        /// <summary>
        /// Cancel Vendor Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedPayableVendorID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.Vendor, Convert.ToInt64(Session["SelectedPayableVendorID"].ToString().Trim()));
                                    //Session["SelectedPayableVendorID"] = null;
                                }
                                DefaultSelection(false);
                            }
                        }
                        pnlFilterForm.Enabled = true;
                        DefaultSelection(false);
                        EnableLinks();
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPayableVendorPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        private Vendor GetItems(Vendor objPayableVendorCatalog)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //FlightPakMasterService.Vendor objPayableVendorCatalog = new FlightPakMasterService.Vendor();

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    objPayableVendorCatalog.VendorCD = tbCode.Text.Trim();
                    objPayableVendorCatalog.Name = tbName.Text;
                    objPayableVendorCatalog.VendorType = "P";

                    if (hdnSave.Value == "Update")
                    {
                        objPayableVendorCatalog.VendorID = Convert.ToInt64(Session["SelectedPayableVendorID"].ToString());
                        if (!string.IsNullOrEmpty(hdnContactName.Value))
                        {
                            objPayableVendorCatalog.VendorContactName = hdnContactName.Value;
                        }
                    }
                    if (!string.IsNullOrEmpty(tbTaxId.Text))
                    {
                        objPayableVendorCatalog.TaxID = tbTaxId.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbTerms.Text))
                    {
                        objPayableVendorCatalog.Terms = tbTerms.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbHomeBase.Text))
                    {
                        objPayableVendorCatalog.HomebaseID = Convert.ToInt64(hdnHomeBaseID.Value);
                    }
                    if (!string.IsNullOrEmpty(tbClosestIcao.Text))
                    {
                        objPayableVendorCatalog.AirportID = Convert.ToInt64(hdnAirportID.Value);
                    }
                    objPayableVendorCatalog.IsInActive = chkActive.Checked;

                    //Upper Part
                    if (!string.IsNullOrEmpty(tbBillName.Text))
                    {
                        objPayableVendorCatalog.BillingName = tbBillName.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbBillAddr1.Text))
                    {
                        objPayableVendorCatalog.BillingAddr1 = tbBillAddr1.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbBillAddr2.Text))
                    {
                        objPayableVendorCatalog.BillingAddr2 = tbBillAddr2.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbBillCity.Text))
                    {
                        objPayableVendorCatalog.BillingCity = tbBillCity.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbBillState.Text))
                    {
                        objPayableVendorCatalog.BillingState = tbBillState.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbBillPostal.Text))
                    {
                        objPayableVendorCatalog.BillingZip = tbBillPostal.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbBillFax.Text))
                    {
                        objPayableVendorCatalog.BillingFaxNum = tbBillFax.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbBillPhone.Text))
                    {
                        objPayableVendorCatalog.BillingPhoneNum = tbBillPhone.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbEmailAddress.Text))
                    {
                        objPayableVendorCatalog.EmailID = tbEmailAddress.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbWebAddress.Text))
                    {
                        objPayableVendorCatalog.WebAddress = tbWebAddress.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbMetro.Text))
                    {
                        objPayableVendorCatalog.MetroID = Convert.ToInt64(hdnMetroID.Value);
                    }
                    if (!string.IsNullOrEmpty(tbBillCountry.Text))
                    {
                        objPayableVendorCatalog.CountryID = Convert.ToInt64(hdnCountryID.Value);
                    }
                    if (!string.IsNullOrEmpty(tbNotes.Text))
                    {
                        objPayableVendorCatalog.Notes = tbNotes.Text;
                    }
                    if (!string.IsNullOrEmpty(tbBillPhone.Text))
                    {
                        objPayableVendorCatalog.BusinessPhone = tbBillPhone.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbTollFreePhone.Text))
                    {
                        objPayableVendorCatalog.TollFreePhone = tbTollFreePhone.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbEmailAddress.Text))
                    {
                        objPayableVendorCatalog.BusinessEmail = tbEmailAddress.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbWebAddress.Text))
                    {
                        objPayableVendorCatalog.Website = tbWebAddress.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbBillingAddr3.Text))
                    {
                        objPayableVendorCatalog.BillingAddr3 = tbBillingAddr3.Text.Trim();
                    }
                    objPayableVendorCatalog.IsDeleted = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return objPayableVendorCatalog;
            }
        }


        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    //tbCode.ReadOnly = true;
                    //tbCode.BackColor = System.Drawing.Color.LightGray;
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    EnableForm(true);
                    tbCode.Enabled = false;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedPayableVendorID"] != null)
                    {
                        strPayableVendorID = "";
                        foreach (GridDataItem Item in dgPayableVendorCatalog.MasterTableView.Items)
                        {
                            if (Item["VendorID"].Text.Trim() == Session["SelectedPayableVendorID"].ToString().Trim())
                            {
                                if (Item.GetDataKeyValue("VendorCD") != null)
                                {
                                    tbCode.Text = Item.GetDataKeyValue("VendorCD").ToString().Trim();
                                }

                                if (Item.GetDataKeyValue("VendorContactName") != null)
                                {
                                    hdnContactName.Value = Item.GetDataKeyValue("VendorContactName").ToString().Trim();
                                }
                                else
                                {
                                    hdnContactName.Value = string.Empty;
                                }

                                if (Item.GetDataKeyValue("Name") != null)
                                {
                                    tbName.Text = Item.GetDataKeyValue("Name").ToString().Trim();
                                }
                                else
                                {
                                    tbName.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Terms") != null)
                                {
                                    tbTerms.Text = Item.GetDataKeyValue("Terms").ToString().Trim();
                                }
                                else
                                {
                                    tbTerms.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("TaxID") != null)
                                {
                                    tbTaxId.Text = Item.GetDataKeyValue("TaxID").ToString().Trim();
                                }
                                else
                                {
                                    tbTaxId.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("HomeBaseCD") != null)
                                {
                                    tbHomeBase.Text = Item.GetDataKeyValue("HomeBaseCD").ToString();
                                }
                                else
                                {
                                    tbHomeBase.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("ClosestICAO") != null)
                                {
                                    tbClosestIcao.Text = Item.GetDataKeyValue("ClosestICAO").ToString();
                                }
                                else
                                {
                                    tbClosestIcao.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue(ConstIsInactive) != null)
                                {
                                    chkActive.Checked = Convert.ToBoolean(Item.GetDataKeyValue(ConstIsInactive).ToString(), CultureInfo.CurrentCulture);
                                }
                                else
                                {
                                    chkActive.Checked = false;
                                }
                                // Upper Part Over
                                if (Item.GetDataKeyValue("BillingName") != null)
                                {
                                    tbBillName.Text = Item.GetDataKeyValue("BillingName").ToString().Trim();
                                }
                                else
                                {
                                    tbBillName.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("BillingAddr1") != null)
                                {
                                    tbBillAddr1.Text = Item.GetDataKeyValue("BillingAddr1").ToString().Trim();
                                }
                                else
                                {
                                    tbBillAddr1.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("BillingAddr2") != null)
                                {
                                    tbBillAddr2.Text = Item.GetDataKeyValue("BillingAddr2").ToString().Trim();
                                }
                                else
                                {
                                    tbBillAddr2.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("BillingCity") != null)
                                {
                                    tbBillCity.Text = Item.GetDataKeyValue("BillingCity").ToString();
                                }
                                else
                                {
                                    tbBillCity.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("BillingState") != null)
                                {
                                    tbBillState.Text = Item.GetDataKeyValue("BillingState").ToString().Trim();
                                }
                                else
                                {
                                    tbBillState.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("BillingZip") != null)
                                {
                                    tbBillPostal.Text = Item.GetDataKeyValue("BillingZip").ToString().Trim();
                                }
                                else
                                {
                                    tbBillPostal.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("NationalityCD") != null)
                                {
                                    tbBillCountry.Text = Item.GetDataKeyValue("NationalityCD").ToString();
                                }
                                else
                                {
                                    tbBillCountry.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("MetroCD") != null)
                                {
                                    tbMetro.Text = Item.GetDataKeyValue("MetroCD").ToString();
                                }
                                else
                                {
                                    tbMetro.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("BillingPhoneNUM") != null)
                                {
                                    tbBillPhone.Text = Item.GetDataKeyValue("BillingPhoneNUM").ToString();
                                }
                                else
                                {
                                    tbBillPhone.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("BillingFaxNum") != null)
                                {
                                    tbBillFax.Text = Item.GetDataKeyValue("BillingFaxNum").ToString();
                                }
                                else
                                {
                                    tbBillFax.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("EmailID") != null)
                                {
                                    tbEmailAddress.Text = Item.GetDataKeyValue("EmailID").ToString();
                                }
                                else
                                {
                                    tbEmailAddress.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("WebAddress") != null)
                                {
                                    tbWebAddress.Text = Item.GetDataKeyValue("WebAddress").ToString();
                                }
                                else
                                {
                                    tbWebAddress.Text = string.Empty;
                                }

                                if (Item.GetDataKeyValue("Notes") != null)
                                {
                                    tbNotes.Text = Item.GetDataKeyValue("Notes").ToString();
                                }
                                else
                                {
                                    tbNotes.Text = string.Empty;
                                }

                                
                                Label lbLastUpdatedUser;
                                lbLastUpdatedUser = (Label)dgPayableVendorCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                                if (Item.GetDataKeyValue("LastUpdUID") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                                }
                                else
                                {
                                    lbLastUpdatedUser.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LastUpdTS") != null)
                                {
                                    lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                                }
                                Item.Selected = true;



                                if ((Item.GetDataKeyValue("BusinessPhone") != null))
                                {
                                    tbBillPhone.Text = Item.GetDataKeyValue("BusinessPhone").ToString().Trim();
                                }
                                else
                                {
                                    tbBillPhone.Text = string.Empty;
                                }
                                if ((Item.GetDataKeyValue("TollFreePhone") != null))
                                {
                                    tbTollFreePhone.Text = Item.GetDataKeyValue("TollFreePhone").ToString().Trim();
                                }
                                else
                                {
                                    tbTollFreePhone.Text = string.Empty;
                                }
                                if ((Item.GetDataKeyValue("Website") != null))
                                {
                                    tbWebAddress.Text = Item.GetDataKeyValue("Website").ToString().Trim();
                                }
                                else
                                {
                                    tbWebAddress.Text = string.Empty;
                                }
                                if ((Item.GetDataKeyValue("BusinessEmail") != null))
                                {
                                    tbEmailAddress.Text = Item.GetDataKeyValue("BusinessEmail").ToString().Trim();
                                }
                                else
                                {
                                    tbEmailAddress.Text = string.Empty;
                                }
                                if ((Item.GetDataKeyValue("BillingAddr3") != null))
                                {
                                    tbBillingAddr3.Text = Item.GetDataKeyValue("BillingAddr3").ToString().Trim();
                                }
                                else
                                {
                                    tbBillingAddr3.Text = string.Empty;
                                }



                                if (Item.GetDataKeyValue("FirstName") != null && Item.GetDataKeyValue("VendorCD") != null)
                                {
                                    if (Item.GetDataKeyValue("FirstName").ToString().Trim() != string.Empty)
                                    {
                                        using (FlightPakMasterService.MasterCatalogServiceClient objVendorContactsService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            List<GetAllVendorContact> lstVendorContact = new List<GetAllVendorContact>();
                                            GetAllVendorContact objVendorContact = new GetAllVendorContact();

                                            objVendorContact.VendorID = Convert.ToInt64(Session["SelectedPayableVendorID"].ToString());
                                            objVendorContact.VendorType = "P";
                                            var objVen = objVendorContactsService.GetVendorContactMasterInfo(objVendorContact);

                                            if (objVen.ReturnFlag == true)
                                            {
                                                lstVendorContact = objVen.EntityList.ToList();
                                            }

                                            var results = (from code in lstVendorContact
                                                           where (code.IsChoice == true)
                                                           select code);


                                            foreach (var v in results)
                                            {
                                                if (v.FirstName != null)
                                                {
                                                    tbFirstName.Text = v.FirstName.ToString().Trim();
                                                }
                                                else
                                                {
                                                    tbFirstName.Text = string.Empty;
                                                }
                                                if (v.MiddleName != null)
                                                {
                                                    tbMiddleName.Text = v.MiddleName.ToString().Trim();
                                                }
                                                else
                                                {
                                                    tbMiddleName.Text = string.Empty;
                                                }
                                                if (v.LastName != null)
                                                {
                                                    tbLastName.Text = v.LastName.ToString().Trim();
                                                }
                                                else
                                                {
                                                    tbLastName.Text = string.Empty;
                                                }
                                                if (v.Title != null)
                                                {
                                                    tbTitle.Text = v.Title.ToString();
                                                }
                                                else
                                                {
                                                    tbTitle.Text = string.Empty;
                                                }
                                                if (v.Addr1 != null)
                                                {
                                                    tbAddr1.Text = v.Addr1.ToString();
                                                }
                                                else
                                                {
                                                    tbAddr1.Text = string.Empty;
                                                }
                                                if (v.Addr2 != null)
                                                {
                                                    tbAddr2.Text = v.Addr2.ToString();
                                                }
                                                else
                                                {
                                                    tbAddr2.Text = string.Empty;
                                                }
                                                if (v.CityName != null)
                                                {
                                                    tbCity.Text = v.CityName.ToString();
                                                }
                                                else
                                                {
                                                    tbCity.Text = string.Empty;
                                                }
                                                if (v.StateName != null)
                                                {
                                                    tbState.Text = v.StateName.ToString();
                                                }
                                                else
                                                {
                                                    tbState.Text = string.Empty;
                                                }
                                                if (v.PostalZipCD != null)
                                                {
                                                    tbPostal.Text = v.PostalZipCD.ToString();
                                                }
                                                else
                                                {
                                                    tbPostal.Text = string.Empty;
                                                }

                                                if (v.CountryID != null)
                                                {
                                                    tbCountry.Text = v.CountryCD.ToString();
                                                }
                                                else
                                                {
                                                    tbCountry.Text = string.Empty;
                                                }
                                                if (v.FaxNum != null)
                                                {
                                                    tbFax.Text = v.FaxNum.ToString();
                                                }
                                                else
                                                {
                                                    tbFax.Text = string.Empty;
                                                }
                                                if (v.EmailID != null)
                                                {
                                                    tbEmail.Text = v.EmailID.ToString();
                                                }
                                                else
                                                {
                                                    tbEmail.Text = string.Empty;
                                                }
                                                if (v.MoreInfo != null)
                                                {
                                                    tbAdditionalContact.Text = v.MoreInfo.ToString();
                                                }
                                                else
                                                {
                                                    tbAdditionalContact.Text = string.Empty;
                                                }

                                                if (v.BusinessPhone != null)
                                                {
                                                    tbBusinessPhone.Text = v.BusinessPhone.ToString();
                                                }
                                                else
                                                {
                                                    tbBusinessPhone.Text = string.Empty;
                                                }
                                                if (v.CellPhoneNum2 != null)
                                                {
                                                    tbCellPhoneNum2.Text = v.CellPhoneNum2.ToString();
                                                }
                                                else
                                                {
                                                    tbCellPhoneNum2.Text = string.Empty;
                                                }
                                                if (v.Website != null)
                                                {
                                                    tbWebsite.Text = v.Website.ToString();
                                                }
                                                else
                                                {
                                                    tbWebsite.Text = string.Empty;
                                                }
                                                if (v.BusinessEmail != null)
                                                {
                                                    tbMCBusinessEmail.Text = v.BusinessEmail.ToString();
                                                }
                                                else
                                                {
                                                    tbMCBusinessEmail.Text = string.Empty;
                                                }
                                                if (v.HomeFax != null)
                                                {
                                                    tbHomeFax.Text = v.HomeFax.ToString();
                                                }
                                                else
                                                {
                                                    tbHomeFax.Text = string.Empty;
                                                }
                                                if (v.OtherEmail != null)
                                                {
                                                    tbOtherEmail.Text = v.OtherEmail.ToString();
                                                }
                                                else
                                                {
                                                    tbOtherEmail.Text = string.Empty;
                                                }
                                                if (v.BusinessEmail != null)
                                                {
                                                    tbMCBusinessEmail.Text = v.BusinessEmail.ToString();
                                                }
                                                else
                                                {
                                                    tbMCBusinessEmail.Text = string.Empty;
                                                }
                                                if (v.PersonalEmail != null)
                                                {
                                                    tbPersonalEmail.Text = v.PersonalEmail.ToString();
                                                }
                                                else
                                                {
                                                    tbPersonalEmail.Text = string.Empty;
                                                }
                                                if (v.CellPhoneNum != null)
                                                {
                                                    tbCellPhoneNum.Text = v.CellPhoneNum.ToString();
                                                }
                                                else
                                                {
                                                    tbCellPhoneNum.Text = string.Empty;
                                                }
                                                if (v.OtherPhone != null)
                                                {
                                                    tbOtherPhone1.Text = v.OtherPhone.ToString();
                                                }
                                                else
                                                {
                                                    tbOtherPhone1.Text = string.Empty;
                                                }
                                                if (v.Addr3 != null)
                                                {
                                                    tbAddr3.Text = v.Addr3.ToString();
                                                }
                                                else
                                                {
                                                    tbAddr3.Text = string.Empty;
                                                }
                                                if (v.PhoneNum != null)
                                                {
                                                    tbPhone.Text = v.PhoneNum.ToString();
                                                }
                                                else
                                                {
                                                    tbPhone.Text = string.Empty;
                                                }

                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        tbFirstName.Text = string.Empty;
                                        tbMiddleName.Text = string.Empty;
                                        tbLastName.Text = string.Empty;
                                        tbTitle.Text = string.Empty;
                                        tbAddr1.Text = string.Empty;
                                        tbAddr2.Text = string.Empty;
                                        tbCity.Text = string.Empty;
                                        tbState.Text = string.Empty;
                                        tbPostal.Text = string.Empty;
                                        tbCountry.Text = string.Empty;
                                        tbFax.Text = string.Empty;
                                        tbEmail.Text = string.Empty;
                                        tbAdditionalContact.Text = string.Empty;
                                        tbBusinessPhone.Text = string.Empty;
                                        tbCellPhoneNum2.Text = string.Empty;
                                        tbWebsite.Text = string.Empty;
                                        tbMCBusinessEmail.Text = string.Empty;
                                        tbHomeFax.Text = string.Empty;
                                        tbOtherEmail.Text = string.Empty;
                                        tbMCBusinessEmail.Text = string.Empty;
                                        tbPersonalEmail.Text = string.Empty;
                                        tbCellPhoneNum.Text = string.Empty;
                                        tbOtherPhone1.Text = string.Empty;
                                        tbAddr3.Text = string.Empty;
                                        tbPhone.Text = string.Empty;
                                    }
                                }
                                else
                                {
                                    tbFirstName.Text = string.Empty;
                                    tbMiddleName.Text = string.Empty;
                                    tbLastName.Text = string.Empty;
                                    tbTitle.Text = string.Empty;
                                    tbAddr1.Text = string.Empty;
                                    tbAddr2.Text = string.Empty;
                                    tbCity.Text = string.Empty;
                                    tbState.Text = string.Empty;
                                    tbPostal.Text = string.Empty;
                                    tbCountry.Text = string.Empty;
                                    tbFax.Text = string.Empty;
                                    tbEmail.Text = string.Empty;
                                    tbAdditionalContact.Text = string.Empty;
                                    tbBusinessPhone.Text = string.Empty;
                                    tbCellPhoneNum2.Text = string.Empty;
                                    tbWebsite.Text = string.Empty;
                                    tbMCBusinessEmail.Text = string.Empty;
                                    tbHomeFax.Text = string.Empty;
                                    tbOtherEmail.Text = string.Empty;
                                    tbMCBusinessEmail.Text = string.Empty;
                                    tbPersonalEmail.Text = string.Empty;
                                    tbCellPhoneNum.Text = string.Empty;
                                    tbOtherPhone1.Text = string.Empty;
                                    tbAddr3.Text = string.Empty;
                                    tbPhone.Text = string.Empty;
                                }
                                #region Get Image from Database
                                CreateDictionayForImgUpload();
                                imgFile.ImageUrl = "";
                                ImgPopup.ImageUrl = null;
                                lnkFileName.NavigateUrl = "";
                                using (FlightPakMasterService.MasterCatalogServiceClient ImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var ReturnImage = ImgService.GetFileWarehouseList("PayableVendor", Convert.ToInt64(Session["SelectedPayableVendorID"].ToString())).EntityList;
                                    ddlImg.Items.Clear();
                                    ddlImg.Text = "";
                                    int i = 0;
                                    Session["Base64"] = null;   //added for image issue
                                    foreach (FlightPakMasterService.FileWarehouse FWH in ReturnImage)
                                    {
                                        Dictionary<string, byte[]> DicImage = (Dictionary<string, byte[]>)Session["DicImg"];
                                        string encodedFileName = Microsoft.Security.Application.Encoder.HtmlEncode(FWH.UWAFileName);
                                        ddlImg.Items.Insert(i, new ListItem(encodedFileName, encodedFileName));
                                        DicImage.Add(FWH.UWAFileName, FWH.UWAFilePath);
                                        if (i == 0)
                                        {
                                            byte[] picture = FWH.UWAFilePath;

                                            //Ramesh: Set the URL for image file or link based on the file type
                                            SetURL(FWH.UWAFileName, picture);

                                            ////start of modification for image issue
                                            //if (hdnBrowserName.Value == "Chrome")
                                            //{
                                            //    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("data:image/jpeg;base64," + Convert.ToBase64String(picture));
                                            //}
                                            //else
                                            //{
                                            //    Session["Base64"] = picture;
                                            //    imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../ImageHandler.ashx?cd=" + Guid.NewGuid());
                                            //}
                                            ////end of modification for image issue
                                        }
                                        i = i + 1;
                                    }
                                    if (ddlImg.Items.Count > 0)
                                    {
                                        ddlImg.SelectedIndex = 0;
                                    }
                                    else
                                    {
                                        imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("../../../App_Themes/Default/images/noimage.jpg");
                                        lnkFileName.NavigateUrl = "";
                                    }
                                }
                                #endregion

                                lbColumnName1.Text = "Payables Vendor Code";
                                lbColumnName2.Text = "Name";
                                lbColumnValue1.Text = Item["VendorCD"].Text;
                                lbColumnValue2.Text = Item["Name"].Text;
                            }

                        }

                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgPayableVendorCatalog.SelectedItems.Count == 0)
                    {
                        DefaultSelection(false);
                    }
                    if (dgPayableVendorCatalog.SelectedItems.Count > 0)
                    {
                        GridDataItem Item = dgPayableVendorCatalog.SelectedItems[0] as GridDataItem;
                        Label lbLastUpdatedUser = (Label)dgPayableVendorCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                        if (Item.GetDataKeyValue("LastUpdUID") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                        }
                        else
                        {
                            lbLastUpdatedUser.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("LastUpdTS") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                        }
                        LoadControlData();
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    
                  

                    hdnContactName.Value = string.Empty;
                    tbCode.Text = string.Empty;
                    tbName.Text = string.Empty;
                    tbTerms.Text = string.Empty;
                    tbTaxId.Text = string.Empty;
                    tbHomeBase.Text = string.Empty;
                    chkActive.Checked = false;
                    tbClosestIcao.Text = string.Empty;
                    //Upper Part
                    tbBillName.Text = string.Empty;
                    tbBillAddr1.Text = string.Empty;
                    tbBillAddr2.Text = string.Empty;
                    tbBillCity.Text = string.Empty;
                    tbBillState.Text = string.Empty;
                    tbBillCountry.Text = string.Empty;
                    tbMetro.Text = string.Empty;
                    tbBillPostal.Text = string.Empty;
                    tbBillPhone.Text = string.Empty;
                    tbBillFax.Text = string.Empty;
                    tbWebAddress.Text = string.Empty;
                    tbEmailAddress.Text = string.Empty;
                    tbNotes.Text = string.Empty;
                    // Clear Vendor Contact Fields
                    tbFirstName.Text = string.Empty;
                    tbMiddleName.Text = string.Empty;
                    tbLastName.Text = string.Empty;
                    tbTitle.Text = string.Empty;
                    tbAddr1.Text = string.Empty;
                    tbAddr2.Text = string.Empty;
                    tbCity.Text = string.Empty;
                    tbState.Text = string.Empty;
                    tbPostal.Text = string.Empty;
                    tbCountry.Text = string.Empty;
                    tbFax.Text = string.Empty;
                    tbEmail.Text = string.Empty;
                    tbAdditionalContact.Text = string.Empty;
                    hdnAirportID.Value = "";
                    hdnCountryID.Value = "";
                    hdnMetroID.Value = "";
                    hdnHomeBaseID.Value = "";

                    tbTollFreePhone.Text = string.Empty;
                    //tbBusinessEmail.Text = string.Empty;
                    tbWebsite.Text = string.Empty;
                    tbBillingAddr3.Text = string.Empty;
                    tbBusinessPhone.Text = string.Empty;
                    tbCellPhoneNum.Text = string.Empty;
                    tbHomeFax.Text = string.Empty;
                    tbCellPhoneNum2.Text = string.Empty;
                    tbOtherPhone1.Text = string.Empty;
                    tbMCBusinessEmail.Text = string.Empty;
                    tbPersonalEmail.Text = string.Empty;
                    tbOtherEmail.Text = string.Empty;

                    tbBusinessPhone.Text = string.Empty;
                    tbOtherPhone1.Text = string.Empty;
                    tbCellPhoneNum.Text = string.Empty;
                    tbCellPhoneNum2.Text = string.Empty;
                    tbMCBusinessEmail.Text = string.Empty;
                    tbPersonalEmail.Text = string.Empty;
                    tbOtherEmail.Text = string.Empty;
                    tbHomeFax.Text = string.Empty;
                    tbWebsite.Text = string.Empty;
                    tbAddr3.Text = string.Empty;

                    CreateDictionayForImgUpload();

                    ddlImg.Enabled = false;
                    imgFile.ImageUrl = "";
                    ImgPopup.ImageUrl = null;
                    lnkFileName.NavigateUrl = "";
                    ddlImg.Items.Clear();
                    ddlImg.Text = "";
                    tbNotes.Text = "";
                    tbImgName.Text = "";

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //if (enable == false)
                    //{
                    //    tbCode.ReadOnly = false;
                    //    tbCode.BackColor = System.Drawing.Color.White;
                    //}

                    ddlImg.Enabled = enable;
                    tbImgName.Enabled = enable;
                    btndeleteImage.Enabled = enable;
                    btndeleteImage.Visible = enable;
                    fileUL.Enabled = enable;


                    tbCode.Enabled = enable;
                    tbName.Enabled = enable;
                    tbTerms.Enabled = enable;
                    tbTaxId.Enabled = enable;
                    tbHomeBase.Enabled = enable;
                    chkActive.Enabled = enable;
                    tbClosestIcao.Enabled = enable;
                    btnBillCountry.Enabled = enable;
                    btnHomeBase.Enabled = enable;
                    btnClosestIcao.Enabled = enable;
                    btnMetro.Enabled = enable;
                    //Upper Part
                    tbBillName.Enabled = enable;
                    tbNotes.Enabled = enable;
                    tbBillAddr1.Enabled = enable;
                    tbBillAddr2.Enabled = enable;
                    tbBillCity.Enabled = enable;
                    tbBillState.Enabled = enable;
                    tbBillCountry.Enabled = enable;
                    tbBillPostal.Enabled = enable;
                    tbMetro.Enabled = enable;
                    tbEmailAddress.Enabled = enable;
                    tbWebAddress.Enabled = enable;
                    tbBillPhone.Enabled = enable;
                    tbBillFax.Enabled = enable;

                    // Enable / Disable Vendor Contact Fields
                    tbFirstName.Enabled = false;
                    tbMiddleName.Enabled = false;
                    tbLastName.Enabled = false;
                    tbTitle.Enabled = false;
                    tbAddr1.Enabled = false;
                    tbAddr2.Enabled = false;
                    tbCity.Enabled = false;
                    tbState.Enabled = false;
                    tbPostal.Enabled = false;
                    tbPhone.Enabled = false;
                    tbCountry.Enabled = false;
                    tbFax.Enabled = false;
                    tbEmail.Enabled = false;
                    tbAdditionalContact.Enabled = false;

                    btnSaveChanges.Visible = enable;
                    btnCancel.Visible = enable;
                    btnSaveChangesTop.Visible = enable;
                    btnCancelTop.Visible = enable;

                    tbTollFreePhone.Enabled = enable;
                    //tbBusinessEmail.Enabled = enable;
                    tbWebsite.Enabled = enable;
                    tbBillingAddr3.Enabled = enable;
                    tbBusinessPhone.Enabled = enable;
                    tbCellPhoneNum.Enabled = enable;
                    tbHomeFax.Enabled = enable;
                    tbCellPhoneNum2.Enabled = enable;
                    tbOtherPhone1.Enabled = enable;
                    tbMCBusinessEmail.Enabled = enable;
                    tbPersonalEmail.Enabled = enable;
                    tbOtherEmail.Enabled = enable;

                    tbBusinessPhone.Enabled = false;
                    tbOtherPhone1.Enabled = false;
                    tbCellPhoneNum.Enabled = false;
                    tbCellPhoneNum2.Enabled = false;
                    tbMCBusinessEmail.Enabled = false;
                    tbPersonalEmail.Enabled = false;
                    tbOtherEmail.Enabled = false;
                    tbHomeFax.Enabled = false;
                    tbWebsite.Enabled = false;
                    tbAddr3.Enabled = false;

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        private void GridEnable(bool Add, bool Edit, bool Delete)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtnInsertCtl = (LinkButton)dgPayableVendorCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                    LinkButton lbtnDelCtl = (LinkButton)dgPayableVendorCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                    LinkButton lbtnEditCtl = (LinkButton)dgPayableVendorCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                    if (IsAuthorized(Permission.Database.AddPayableVendor))
                    {
                        lbtnInsertCtl.Visible = true;
                        if (Add)
                        {
                            lbtnInsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtnInsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtnInsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeletePayableVendor))
                    {
                        lbtnDelCtl.Visible = true;
                        if (Delete)
                        {
                            lbtnDelCtl.Enabled = true;
                            lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            lbtnDelCtl.Enabled = false;
                            lbtnDelCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnDelCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditPayableVendor))
                    {
                        lbtnEditCtl.Visible = true;
                        if (Edit)
                        {
                            lbtnEditCtl.Enabled = true;
                            lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtnEditCtl.Enabled = false;
                            lbtnEditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnEditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void lnkPayableVendorContacts_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedPayableVendorID"] != null)
                        {
                            //GridDataItem Item = (GridDataItem)Session["SelectedItem"];
                            if (dgPayableVendorCatalog.Items.Count > 0)
                            {
                                string PayableVendorID = Session["SelectedPayableVendorID"].ToString();
                                    Session["SelectedVendorContactID"] = null;
                                    RedirectToPage(string.Format("{0}?Screen=PayableVendor&VendorID={1}&VendorType=P&VendorCode={2}&VendorName={3}", WebConstants.URL.VendorContacts, Microsoft.Security.Application.Encoder.HtmlEncode(PayableVendorID), Microsoft.Security.Application.Encoder.HtmlEncode(tbCode.Text), Microsoft.Security.Application.Encoder.HtmlEncode(tbName.Text)));

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }
        }

        #region "Search and Filter"
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DoSearchFilter();
                        DefaultSelection(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.PayableVendor);
                }
            }
        }

        public Dictionary<string, byte[]> DicImgs
        {
            get { return (Dictionary<string, byte[]>)Session["DicImg"]; }
            set { Session["DicImg"] = value; }
        }
        public Dictionary<string, string> DicImgsDelete
        {
            get { return (Dictionary<string, string>)Session["DicImgsDelete"]; }
            set { Session["DicImgsDelete"] = value; }
        }

        protected void DoSearchFilter()
        {
            using (FlightPakMasterService.MasterCatalogServiceClient objPayableVendorCatalogService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objPayableVendor = objPayableVendorCatalogService.GetPayableVendorInfo();

                List<FlightPakMasterService.GetAllVendor> lstAccount = new List<FlightPakMasterService.GetAllVendor>();
                if (Session["PayableVendor"] != null)
                {
                    lstAccount = (List<FlightPakMasterService.GetAllVendor>)Session["PayableVendor"];
                }
                if (lstAccount.Count != 0)
                {
                    if (chkSearchActiveOnly.Checked == true) { lstAccount = lstAccount.Where(x => x.IsInActive == true).ToList<GetAllVendor>(); }
                    if (chkSearchHomebaseOnly.Checked == true) { lstAccount = lstAccount.Where(x => x.HomeBaseCD == UserPrincipal.Identity._airportICAOCd).ToList<GetAllVendor>(); }
                    dgPayableVendorCatalog.DataSource = lstAccount.OrderBy(x => x.VendorCD);
                    //DefaultSelection(false);
                }
            }
        }
        #endregion

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgPayableVendorCatalog.SelectedIndexes.Clear();
                    chkSearchActiveOnly.Checked = false;
                    chkSearchHomebaseOnly.Checked = false;
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var PayableVendorValue = FPKMstService.GetPayableVendorInfo();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, PayableVendorValue);
            List<FlightPakMasterService.GetAllVendor> filteredList = GetFilteredList(PayableVendorValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.VendorID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgPayableVendorCatalog.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgPayableVendorCatalog.CurrentPageIndex = PageNumber;
            dgPayableVendorCatalog.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllVendor PayableVendorValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedPayableVendorID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = PayableVendorValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().VendorID;
                Session["SelectedPayableVendorID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllVendor> GetFilteredList(ReturnValueOfGetAllVendor PayableVendorValue)
        {
            List<FlightPakMasterService.GetAllVendor> filteredList = new List<FlightPakMasterService.GetAllVendor>();

            if (PayableVendorValue.ReturnFlag == true)
            {
                filteredList = PayableVendorValue.EntityList.OrderBy(x => x.VendorCD).ToList();
            }

            if (filteredList.Count > 0)
            {
                if (chkSearchActiveOnly.Checked == true) { filteredList = filteredList.Where(x => x.IsInActive == true).ToList<GetAllVendor>(); }
                if (chkSearchHomebaseOnly.Checked == true) { filteredList = filteredList.Where(x => x.HomeBaseCD == UserPrincipal.Identity._airportICAOCd).ToList<GetAllVendor>(); }
            }

            return filteredList;
        }

        /// <summary>
        /// Common method to set the URL for image and Download file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="filedata"></param>
        private void SetURL(string filename, byte[] filedata)
        {
            //Ramesh: Code to allow any file type for upload control.
            int iIndex = filename.Trim().IndexOf('.');
            //string strExtn = filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex);
            string strExtn = iIndex > 0 ? filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex) : FlightPak.Common.Utility.GetImageFormatExtension(filedata);  
            //Match regMatch = Regex.Match(strExtn, @"[^\s]+(?=.([jJ][pP][gG]|[gG][iI][fF]|[bB][mM][pP]|[jJ][pP][eE][gG]|[tT][iI][fF][fF]))", RegexOptions.IgnoreCase);
            string SupportedImageExtPatterns = System.Configuration.ConfigurationManager.AppSettings["SupportedImageExtPatterns"];
            Match regMatch = Regex.Match(strExtn, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
            if (regMatch.Success)
            {
                if (Request.UserAgent.Contains("Chrome"))
                {
                    hdnBrowserName.Value = "Chrome";
                }
                //start of modification for image issue
                if (hdnBrowserName.Value == "Chrome")
                {
                    imgFile.Visible = true;
                    imgFile.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(filedata);
                    lnkFileName.NavigateUrl = "";
                    lnkFileName.Visible = false;
                }
                else
                {
                    Session["Base64"] = filedata;
                    imgFile.Visible = true;
                    imgFile.ImageUrl = "~/Views/Settings/ImageHandler.ashx?cd=" + Guid.NewGuid();
                    lnkFileName.NavigateUrl = "";
                    lnkFileName.Visible = false;
                }
                //end of modification for image issue
            }
            else
            {
                imgFile.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                imgFile.Visible = false;
                lnkFileName.Visible = true;
                Session["Base64"] = filedata;
                lnkFileName.NavigateUrl = "~/Views/Settings/ImageHandler.ashx?cd=" + Guid.NewGuid() + "&filename=" + filename.Trim();
            }
        }
    }
}