﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;
using FlightPak.Common.Constants;

namespace FlightPak.Web.Views.Settings.Logistics
{
    /// <summary>
    /// Summary description for FuelFileInvalidRecordsHandler
    /// </summary>
    public class FuelFileInvalidRecordsHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            StringBuilder csvData=new StringBuilder();
            if (HttpContext.Current.Session[FuelFileConstants.InvalidFuelRecords] != null)
            {
                string filename = string.Empty;
                if (context.Request["filename"] != null)
                {
                    filename = context.Request.QueryString["filename"];
                }
                string[] csvColumnNameForErrorLogFile = { "e_AirportIdentifier", "e_FBOName", "e_GallonFrom", "e_GallonTO", "e_UnitPrice", "e_EffectiveDT", "e_InvalidRecordErrorMessage" };
                string[] reuqiredColumn = { "AirportIdentifier", "FBOName", "GallonFrom", "GallonTO", "UnitPrice", "EffectiveDT", "InvalidRecordErrorMessage" };
                var invalidRecordsList = (List<FlightPakMasterService.FlightpakFuel>)HttpContext.Current.Session[FuelFileConstants.InvalidFuelRecords];
                string csvHeaderline = csvColumnNameForErrorLogFile.Aggregate((current, next) => current + ", " + next);
                csvData.AppendLine(csvHeaderline.Remove(csvHeaderline.Length - 1));
                var values = new object[reuqiredColumn.Length];
                foreach (var item in invalidRecordsList)
                {
                    for (var i = 0; i < values.Length; i++)
                    {
                        string propValue = Convert.ToString(item.GetType().GetProperty(reuqiredColumn[i]).GetValue(item));
                        if (!string.IsNullOrEmpty(propValue) && (propValue.Contains(",") || propValue.Contains("&")))
                            values[i] = string.Format("\"{0}\"", propValue);
                        else
                            values[i] = propValue;
                    }
                    string data = values.Aggregate(string.Empty, (current, i) => current + (i + ","));
                    csvData.AppendLine(data.Remove(data.Length - 1));
                }
                context.Response.Clear();
                string xsltFileName = string.Format("UWA-{0}.csv", string.IsNullOrEmpty(filename) ? DateTime.Now.ToString("yyyy-MMM-dd-HHmmss") : filename);
                context.Response.ContentType = "text/csv";
                context.Response.AddHeader("content-disposition", "attachment; filename=" + xsltFileName);
                context.Response.Write(csvData);
                context.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}