﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VendorMasterPopUp.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Logistics.VendorMasterPopUp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Vendor</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgVendorCatalog.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgVendorCatalog.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;

                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "VendorID");
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "VendorCD");
                        var cell3 = MasterTable.getCellByColumnUniqueName(row, "Name");
                        if (i == 0) {

                            oArg.VendorID = cell1.innerHTML;
                            oArg.VendorCD = cell2.innerHTML + ",";
                            oArg.Name = cell3.innerHTML;
                        }
                        else {

                            oArg.VendorID += cell1.innerHTML;
                            oArg.VendorCD += cell2.innerHTML + ",";
                            oArg.Name += cell3.innerHTML;
                        }
                    }
                }
                else {

                    oArg.VendorID = "0";
                    oArg.VendorCD = "";
                    oArg.Name = "";
                }
                if (oArg.VendorCD != "") {
                    oArg.VendorCD = oArg.VendorCD.substring(0, oArg.VendorCD.length - 1)
                }
                else {
                    oArg.VendorCD = "";
                }
                oArg.Arg1 = oArg.VendorCD;
                oArg.CallingButton = "VendorCD";

                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgVendorCatalog">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgVendorCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnSearch">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgVendorCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                        <tr>
                            <td>
                                <div class="status-list">
                                    <span>
                                        <asp:CheckBox ID="chkSearchHomebaseOnly" runat="server" Text="Home Base Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged" /></span>
                                    <span>
                                        <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged" /></span>
                                </div>
                            </td>
                            <td class="tdLabel100">
                                <asp:Button ID="btnSearch" runat="server" Checked="false" Text="Search" CssClass="button"
                                    OnClick="Search_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadGrid ID="dgVendorCatalog" runat="server" AllowMultiRowSelection="true"
                        AllowSorting="true" OnNeedDataSource="dgVendorCatalog_BindData" AutoGenerateColumns="false"
                        Height="330px" AllowPaging="false" Width="800px" OnItemCommand="dgVendorCatalog_ItemCommand"
                        OnPreRender="dgVendorCatalog_PreRender">
                        <MasterTableView ClientDataKeyNames="VendorID,VendorCD,Name,VendorContactName,IsInActive,HomebaseID,FirstName,LastName"
                            CommandItemDisplay="None" AllowPaging="false">
                            <Columns>
                                <telerik:GridBoundColumn UniqueName="VendorID" HeaderText="VendorID" DataField="VendorID"
                                    Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="VendorCD" HeaderText="Code" CurrentFilterFunction="StartsWith"
                                    AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="100px"
                                    FilterControlWidth="80px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Name" HeaderText="Vendor Name" CurrentFilterFunction="StartsWith"
                                    AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="200px"
                                    FilterControlWidth="180px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FirstName" HeaderText="Contact First Name" CurrentFilterFunction="StartsWith"
                                    ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="160px"
                                    FilterControlWidth="140px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="LastName" HeaderText="Contact Last Name" CurrentFilterFunction="StartsWith"
                                    ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="160px"
                                    FilterControlWidth="140px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Active" CurrentFilterFunction="StartsWith"
                                    AutoPostBackOnFilter="true" ShowFilterIcon="false" HeaderStyle-Width="80px">
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridBoundColumn DataField="HomebaseCD" HeaderText="Home Base" CurrentFilterFunction="StartsWith"
                                    AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="100px" FilterDelay="500"
                                    FilterControlWidth="80px">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div style="padding: 5px 5px; text-align: right;">
                                    <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                                        Ok</button>
                                    <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                                </div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="returnToParent"  />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
