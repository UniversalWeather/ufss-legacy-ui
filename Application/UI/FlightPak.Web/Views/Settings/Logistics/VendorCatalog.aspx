﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    ValidateRequest="false" AutoEventWireup="true" CodeBehind="VendorCatalog.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Logistics.VendorCatalog" ClientIDMode="AutoID"
    MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript">
        function OnClientClick(strPanelToExpand) {

            var PanelBar1 = $find("<%= pnlNotes.ClientID %>");
            var PanelBar2 = $find("<%= pnlMainContact.ClientID %>");
            var PanelBar3 = $find("<%= pnlMaintenance.ClientID %>");
            PanelBar1.get_items().getItem(0).set_expanded(false);
            PanelBar2.get_items().getItem(0).set_expanded(false);
            PanelBar3.get_items().getItem(0).set_expanded(false);
            if (strPanelToExpand == "Notes") {
                PanelBar1.get_items().getItem(0).set_expanded(true);
            }
            else if (strPanelToExpand == "Main Contact") {
                PanelBar2.get_items().getItem(0).set_expanded(true);
            }
            return false;
        }      
    </script>
    <script type="text/javascript">
        function CheckTxtBox(control) {

            var txtCode = document.getElementById("<%=tbCode.ClientID%>").value;
            var txtName = document.getElementById("<%=tbName.ClientID%>").value;
            var txtBillName = document.getElementById("<%=tbBillName.ClientID%>").value;

            if (txtCode == "") {
                if (control == 'code') {
                    ValidatorEnable(document.getElementById('<%=rfvCode.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbCode.ClientID%>').focus()", 0);

                } return false;
            }

            if (txtName == "") {
                if (control == 'Name') {
                    ValidatorEnable(document.getElementById('<%=rfvName.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbName.ClientID%>').focus()", 0);

                } return false;
            }

            if (txtBillName == "") {
                if (control == 'BillName') {
                    ValidatorEnable(document.getElementById('<%=rfvBillName.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbBillName.ClientID%>').focus()", 0);

                } return false;
            }
        }
 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function openWin(url, value, radWin) {

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url + value, radWin);
            }

            function openHomeBase() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Airports/AirportMasterPopup.aspx?IcaoID=" + document.getElementById("<%=tbHomeBase.ClientID%>").value, "radAirportPopup");
                oWnd.add_close(OnClientCloseAirportMasterPopup);
            }
            function openClosestIcao() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Airports/AirportMasterPopup.aspx?IcaoID=" + document.getElementById("<%=tbClosestIcao.ClientID%>").value, "radAirportPopup");
                oWnd.add_close(OnClientCloseAirportMasterPopup1);
            }
            function openCountry() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Company/CountryMasterPopup.aspx?CountryCD=" + document.getElementById("<%=tbCountry.ClientID%>").value, "RadWindow2");

            }
            function openMetro() {
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open("../Airports/MetroCityPopup.aspx?MetroCD=" + document.getElementById("<%=tbMetro.ClientID%>").value, "RadWindow3");

            }

            function ConfirmClose(WinName) {

                var oManager = $find("<%= RadWindowManager1.ClientID %>"); //GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn); 
                }
            }
            function OnClientCloseAirportMasterPopup(oWnd, args) {
                var combo = $find("<%= tbHomeBase.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = arg.AirportID;
                    }
                    else {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = "";
                        document.getElementById("<%=cvHomeBase.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnHomeBaseID.ClientID%>").value = "";
                    }
                }
            }
            function OnClientCloseAirportMasterPopup1(oWnd, args) {
                var combo = $find("<%= tbClosestIcao.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClosestIcao.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=cvClosestIcao.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnAirportID.ClientID%>").value = arg.AirportID;

                    }
                    else {
                        document.getElementById("<%=tbClosestIcao.ClientID%>").value = "";
                        document.getElementById("<%=cvClosestIcao.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnAirportID.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }

            }
            function OnClientCloseCountryPopup(oWnd, args) {
                var combo = $find("<%= tbBillCountry.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbBillCountry.ClientID%>").value = arg.CountryCD;
                        document.getElementById("<%=cvBillCountry.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnCountryID.ClientID%>").value = arg.CountryID;
                    }
                    else {
                        document.getElementById("<%=tbBillCountry.ClientID%>").value = "";
                        document.getElementById("<%=cvBillCountry.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnCountryID.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseMetroCityPopup(oWnd, args) {
                var combo = $find("<%= tbMetro.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                //                    alert(arg.toString());
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbMetro.ClientID%>").value = arg.MetroCD;
                        document.getElementById("<%=cvMetro.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnMetroID.ClientID%>").value = arg.MetroID;
                    }
                    else {
                        document.getElementById("<%=tbMetro.ClientID%>").value = "";
                        document.getElementById("<%=cvMetro.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnMetroID.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function CheckLatitude() {
                var ReturnValue = true;
                var LatitudeDeg = document.getElementById("<%=tbLatitudeDeg.ClientID%>");
                var LatitudeMin = document.getElementById("<%=tbLatitudeMin.ClientID%>");
                var LatitudeNS = document.getElementById("<%=tbLatitudeNS.ClientID%>");

                var LongitudeDeg = document.getElementById("<%=tbLongitudeDeg.ClientID%>");
                var LongitudeMin = document.getElementById("<%=tbLongitudeMin.ClientID%>");
                var LongitudeEW = document.getElementById("<%=tbLongitudeEW.ClientID%>");

                if ((LatitudeDeg.value != "") || (LatitudeMin.value != "") || (LatitudeNS.value != "")) {
                    if ((LongitudeDeg.value == "") || (LongitudeEW.value == "")) {  //|| (LongitudeMin.value == "")
                        if (LatitudeDeg.value == "") {
                            LatitudeDeg.focus();
                            alert("Enter Latitude Degree");
                        }
                        else if (LatitudeNS.value == "") {
                            LatitudeNS.focus();
                            alert("Enter Latitude Direction (NS)");
                        }
                        else if (LongitudeDeg.value == "") {
                            LongitudeDeg.focus();
                            alert("Enter Longitude Degree");
                        }
                        //                        else if (LongitudeMin.value == "") {
                        //                            LongitudeMin.focus();
                        //                        }
                        else if (LongitudeEW.value == "") {
                            LongitudeEW.focus();
                            alert("Enter Longitude Direction (EW)");
                        }
                        ReturnValue = false;
                    }
                }
                if ((LongitudeDeg.value != "") || (LongitudeMin.value != "") || (LongitudeEW.value != "")) {
                    if ((LatitudeDeg.value == "") || (LatitudeNS.value == "")) {    //(LatitudeMin.value == "") ||
                        if (LongitudeDeg.value == "") {
                            LongitudeDeg.focus();
                            alert("Enter Longitude Degree");
                        }
                        else if (LongitudeEW.value == "") {
                            LongitudeEW.focus();
                            alert("Enter Longitude Direction (EW)");
                        }
                        else if (LatitudeDeg.value == "") {
                            LatitudeDeg.focus();
                            alert("Enter Latitude Degree");
                        }
                        //                        else if (LatitudeMin.value == "") {
                        //                            LatitudeMin.focus();
                        //                        }
                        else if (LatitudeNS.value == "") {
                            LatitudeNS.focus();
                            alert("Enter Latitude direction (NS)");
                        }
                        ReturnValue = false;
                    }
                }
                if (ReturnValue == true) {
                    document.getElementById("<%=btnSaveChanges_Dummy.ClientID%>").click();
                }
                return false;
            }

            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseAirportMasterPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings//Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <%--<telerik:RadWindow ID="RadWindow4" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseAirportMasterPopup1" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings//Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>--%>
            <telerik:RadWindow ID="RadWindow2" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseCountryPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow3" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseMetroCityPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/MetroCityPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadVendorPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseMetroCityPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/VendorContacts.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Vendor </span><span class="tab-nav-icons">
                        <!--<a href="#" class="search-icon">
                    </a><a href="#" class="save-icon"></a><a href="#" class="print-icon"></a>-->
                        <a href="../../Help/ViewHelp.aspx?Screen=VendorHelp" class="help-icon" target="_blank"
                            title="Help"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <table cellpadding="0" cellspacing="0" class="head-sub-menu">
            <tr>
                <td>
                    <div class="fleet_select">
                        <asp:LinkButton ID="lnkVendorContacts" CssClass="fleet_link" runat="server" Text="Contacts"
                            OnClick="lnkVendorContacts_Click"></asp:LinkButton></div>
                </td>
            </tr>
        </table>
        <asp:Panel ID="pnlFilterForm" runat="server">
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td>
                        <div class="status-list">
                            <span>
                                <asp:CheckBox ID="chkSearchHomebaseOnly" runat="server" Text="Home Base Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                    AutoPostBack="true" /></span><span>
                                        <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged"
                                            AutoPostBack="true" /></span>
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <telerik:RadGrid ID="dgVendorCatalog" runat="server" AllowSorting="true" OnNeedDataSource="dgVendorCatalog_BindData"
            OnItemCommand="dgVendorCatalog_ItemCommand" OnItemCreated="dgVendorCatalog_ItemCreated"
            OnPreRender="Vendor_PreRender" OnUpdateCommand="dgVendorCatalog_UpdateCommand"
            Height="341px" OnInsertCommand="dgVendorCatalog_InsertCommand" OnPageIndexChanged="Vendor_PageIndexChanged"
            OnDeleteCommand="dgVendorCatalog_DeleteCommand" AutoGenerateColumns="false" PageSize="10"
            AllowPaging="true" OnSelectedIndexChanged="dgVendorCatalog_SelectedIndexChanged"
            AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true">
            <MasterTableView ClientDataKeyNames="CustomerID,VendorID,VendorCD,Name,VendorType,IsApplicationFiled,IsApproved,IsDrugTest,IsInsuranceCERT,IsFAR135Approved,IsFAR135CERT,SITA,AdditionalInsurance,Contact,BillingName,BillingAddr1,BillingAddr2,ClosestICAO,NationalityCD,HomeBaseCD,MetroCD,BillingCity,BillingState,BillingZip,CountryID,MetroID,BillingPhoneNUM,BillingFaxNum,Notes,Credit,DiscountPercentage,TaxID,Terms,IsInActive,HomebaseID,LatitudeDegree,LatitudeMinutes,LatitudeNorthSouth,LongitudeDegree,LongitudeMinutes,LongitudeEastWest,AirportID,IsTaxExempt,DateAddedDT,EmailID,WebAddress,IsDeleted"
                DataKeyNames="CustomerID,VendorID,VendorCD,Name,VendorType,IsApplicationFiled,IsApproved,IsDrugTest,IsInsuranceCERT,IsFAR135Approved,IsFAR135CERT,AdditionalInsurance,Contact,BillingName,BillingAddr1,BillingAddr2,BillingCity,BillingState,MetroCD,ClosestICAO,HomeBaseCD,NationalityCD,BillingZip,CountryID,MetroID,BillingPhoneNUM,BillingFaxNum,Notes,Credit,DiscountPercentage,TaxID,Terms,IsInActive,HomebaseID,LatitudeDegree,LatitudeMinutes,LatitudeNorthSouth,LongitudeDegree,LongitudeMinutes,LongitudeEastWest,AirportID,LastUpdUID,LastUpdTS,IsTaxExempt,DateAddedDT,EmailID,WebAddress,IsDeleted
            ,TollFreePhone,BusinessEmail,Website,BillingAddr3,BusinessPhone,CellPhoneNum,HomeFax,CellPhoneNum2,OtherPhone,MCBusinessEmail,PersonalEmail,OtherEmail,FirstName,LastName,CertificateNumber"
                CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="VendorCD" HeaderText="Vendor Code" CurrentFilterFunction="StartsWith"
                        HeaderStyle-Width="100px" FilterControlWidth="80px" AutoPostBackOnFilter="false" FilterDelay="500"
                        ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Name" HeaderText="Vendor Name" CurrentFilterFunction="StartsWith"
                        AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="200px" FilterDelay="500"
                        FilterControlWidth="180px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FirstName" HeaderText="Contact First Name" CurrentFilterFunction="StartsWith"
                        ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="180px" FilterDelay="500"
                        FilterControlWidth="160px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LastName" HeaderText="Contact Last Name" CurrentFilterFunction="StartsWith"
                        ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="180px" FilterDelay="500"
                        FilterControlWidth="160px">
                    </telerik:GridBoundColumn>
                    <%-- <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Active" CurrentFilterFunction="EqualTo"
                    AutoPostBackOnFilter="true" ShowFilterIcon="false">
                </telerik:GridCheckBoxColumn>--%>
                    <telerik:GridBoundColumn DataField="HomebaseCD" HeaderText="Home Base" CurrentFilterFunction="StartsWith"
                        HeaderStyle-Width="100px" FilterControlWidth="80px" AutoPostBackOnFilter="false"
                        ShowFilterIcon="false" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="VendorID" HeaderText="Vendor Code" CurrentFilterFunction="EqualTo"
                        UniqueName="VendorID" Display="false" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; float: left; clear: both;">
                        <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                            Visible='<%# IsAuthorized(Permission.Database.AddVendorCatalog)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                            Visible='<%# IsAuthorized(Permission.Database.EditVendorCatalog)%>' ToolTip="Edit"
                            CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lbtnDelete" OnClientClick="javascript:return ProcessDelete();"
                            Visible='<%# IsAuthorized(Permission.Database.DeleteVendorCatalog)%>' runat="server"
                            CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                    </div>
                    <div>
                        <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings EnablePostBackOnRowClick="true">
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="right">
                        <table cellspacing="0" cellpadding="0" class="tblButtonArea-nw">
                            <tr>
                                <td>
                                    <asp:Button ID="btnMainContact" runat="server" CssClass="ui_nav" Text="Main Contact"
                                        OnClientClick="javascript:OnClientClick('Main Contact');return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnNotes" runat="server" CssClass="ui_nav" Text="Notes" OnClientClick="javascript:OnClientClick('Notes');return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnSaveChangesTop" runat="server" CssClass="button" Text="Save" ValidationGroup="save"
                                        OnClientClick="javascript:return CheckLatitude();" OnClick="SaveChanges_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancelTop" runat="server" CssClass="button" Text="Cancel" CausesValidation="false"
                                        OnClick="Cancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlMaintenance" Width="100%" ExpandAnimation-Type="none"
                ValidationGroup="save" CollapseAnimation-Type="none" runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Main Information" CssClass="PanelHeaderStyle">
                        <Items>
                            <telerik:RadPanelItem Value="Maintenance" runat="server">
                                <ContentTemplate>
                                    <table width="100%" class="box1">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="tdLabel100" valign="top">
                                                            <asp:CheckBox ID="chkTaxExempt" runat="server" Text="Tax Exempt" />
                                                        </td>
                                                        <td valign="top" align="left">
                                                            <asp:CheckBox ID="chkActive" runat="server" Text="Active" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_10">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            <span class="mnd_text">Vendor Code</span>
                                                        </td>
                                                        <td class="tdLabel174" valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCode" runat="server" CssClass="text40" MaxLength="5" ValidationGroup="save"
                                                                            OnTextChanged="Code_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique Vendor Code is Required"
                                                                            Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                                        <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="tbCode"
                                                                            ValidationGroup="save" Text="Vendor Code is Required" SetFocusOnError="true"
                                                                            Display="Dynamic" CssClass="alert-text"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel100" valign="top">
                                                            <span class="mnd_text">Vendor Name</span>
                                                        </td>
                                                        <td valign="top">
                                                            <table cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbName" runat="server" CssClass="text150" MaxLength="40" ValidationGroup="save"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="tbName"
                                                                            ValidationGroup="save" Text="Vendor Name is Required" SetFocusOnError="true"
                                                                            Display="Dynamic" CssClass="alert-text"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130">
                                                            Home Base
                                                        </td>
                                                        <td class="tdLabel174">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="tdLabel110">
                                                                        <asp:TextBox ID="tbHomeBase" runat="server" CssClass="text40" MaxLength="4" OnTextChanged="HomeBase_TextChanged"
                                                                            onBlur="return RemoveSpecialChars(this)" AutoPostBack="true">
                                                                        </asp:TextBox>
                                                                        <asp:Button ID="btnHomeBase" runat="server" CssClass="browse-button" OnClientClick="javascript:openHomeBase();return false;" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CustomValidator ID="cvHomeBase" runat="server" ControlToValidate="tbHomeBase"
                                                                            ErrorMessage="Invalid Home Base Code" Display="Dynamic" CssClass="alert-text"
                                                                            ValidationGroup="save"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel100">
                                                            Closest ICAO
                                                        </td>
                                                        <td class="tdLabel220">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="tdLabel110">
                                                                        <asp:TextBox ID="tbClosestIcao" runat="server" CssClass="text40" MaxLength="4" OnTextChanged="ClosestIcao_TextChanged"
                                                                            onBlur="return RemoveSpecialChars(this)" AutoPostBack="true">
                                                                        </asp:TextBox>
                                                                        <asp:Button ID="btnClosestIcao" runat="server" OnClientClick="javascript:openClosestIcao();return false;"
                                                                            CssClass="browse-button"></asp:Button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CustomValidator ID="cvClosestIcao" runat="server" ControlToValidate="tbClosestIcao"
                                                                            ErrorMessage="Invalid ClosestIcao Code" Display="Dynamic" CssClass="alert-text"
                                                                            ValidationGroup="save"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            <!--R2 change - As per Defect 4785 changed the label-->
                                                            Additional Insurance
                                                        </td>
                                                        <td class="tdLabel170" valign="top">
                                                            <asp:TextBox ID="tbAddlInsured" runat="server" CssClass="text150" MaxLength="60"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel140">
                                                            <asp:CheckBox ID="chkDrugTestApproval" runat="server" Text="Drug Test Approval" />
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chkInsCert" runat="server" Text="Insurance Certificate" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            Credit
                                                        </td>
                                                        <td class="tdLabel250" valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel130" valign="top">
                                                                        <asp:CheckBox ID="chkApplication" runat="server" Text="Application on File" />
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:CheckBox ID="chkApproval" runat="server" Text="Approval" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            Part 135 Ops
                                                        </td>
                                                        <td class="tdLabel450" valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel130" valign="top">
                                                                        <asp:CheckBox ID="chkApprov" runat="server" Text="Approval" />
                                                                    </td>
                                                                    <td class="tdLabel130" valign="top">
                                                                        <asp:CheckBox ID="chkCertificate" runat="server" Text="Certificate" AutoPostBack="true"
                                                                            OnCheckedChanged="chkCertificate_OnCheckedChanged" />
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="tbCertificate" runat="server" MaxLength="50" Visible="false"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_10">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <fieldset>
                                                    <legend>Billing Information</legend>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel120" valign="top">
                                                                            <span class="mnd_text">Name</span>
                                                                        </td>
                                                                        <td class="tdLabel240" valign="top">
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbBillName" runat="server" CssClass="text200" MaxLength="40" ValidationGroup="save"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RequiredFieldValidator ID="rfvBillName" runat="server" ErrorMessage="Name is Required"
                                                                                            Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbBillName" CssClass="alert-text"
                                                                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td class="tdLabel130" valign="top">
                                                                            Metro
                                                                        </td>
                                                                        <td class="tdLabel200" valign="top">
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td class="tdLabel110">
                                                                                        <asp:TextBox ID="tbMetro" runat="server" CssClass="text40" MaxLength="3" OnTextChanged="Metro_TextChanged"
                                                                                            onBlur="return RemoveSpecialChars(this)" AutoPostBack="true"></asp:TextBox>
                                                                                        <asp:Button ID="btnMetro" runat="server" OnClientClick="javascript:openMetro();return false;"
                                                                                            CssClass="browse-button"></asp:Button>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CustomValidator ID="cvMetro" runat="server" ControlToValidate="tbMetro" ErrorMessage="Invalid Metro Code"
                                                                                            Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel120" valign="top">
                                                                            Address 1
                                                                        </td>
                                                                        <td class="tdLabel240" valign="top">
                                                                            <asp:TextBox ID="tbBillAddr1" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                                        </td>
                                                                        <td class="tdLabel130" valign="top">
                                                                            Address 2
                                                                        </td>
                                                                        <td class="tdLabel200" valign="top">
                                                                            <asp:TextBox ID="tbBillAddr2" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                    <tr>
                                                                        <td class="tdLabel120">
                                                                            <asp:Label ID="lbBillingAddr3" runat="server" Text="Address Line 3" />
                                                                        </td>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbBillingAddr3" runat="server" MaxLength="40" CssClass="text200"
                                                                                            ValidationGroup="save" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel120" valign="top">
                                                                            City
                                                                        </td>
                                                                        <td class="tdLabel240" valign="top">
                                                                            <asp:TextBox ID="tbBillCity" runat="server" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                                        </td>
                                                                        <td class="tdLabel130" valign="top">
                                                                            State/Province
                                                                        </td>
                                                                        <td class="tdLabel200" valign="top">
                                                                            <asp:TextBox ID="tbBillState" runat="server" CssClass="text200" MaxLength="10"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel120" valign="top">
                                                                            Country
                                                                        </td>
                                                                        <td class="tdLabel240" valign="top">
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td class="tdLabel110">
                                                                                        <asp:TextBox ID="tbBillCountry" runat="server" CssClass="text50" MaxLength="3" ValidationGroup="save"
                                                                                            OnTextChanged="BillCountry_TextChanged" onBlur="return RemoveSpecialChars(this)"
                                                                                            AutoPostBack="true"></asp:TextBox>
                                                                                        <asp:Button ID="btnBillCountry" runat="server" OnClientClick="javascript:openCountry();return false;"
                                                                                            CssClass="browse-button"></asp:Button>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CustomValidator ID="cvBillCountry" runat="server" ControlToValidate="tbBillCountry"
                                                                                            ErrorMessage="Invalid Country Code" Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td class="tdLabel130" valign="top">
                                                                            Postal
                                                                        </td>
                                                                        <td class="tdLabel200" valign="top">
                                                                            <asp:TextBox ID="tbBillPostal" runat="server" CssClass="text200" MaxLength="15"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel120" valign="top">
                                                                            Latitude
                                                                        </td>
                                                                        <td class="tdLabel240" valign="top">
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td align="left" class="tdLabel50">
                                                                                        <asp:TextBox ID="tbLatitudeDeg" runat="server" CssClass="text35" MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)"
                                                                                            ValidationGroup="save"></asp:TextBox>
                                                                                    </td>
                                                                                    <td align="left" class="tdLabel50">
                                                                                        <asp:TextBox ID="tbLatitudeMin" runat="server" CssClass="text35" MaxLength="4" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                            ValidationGroup="save"></asp:TextBox>
                                                                                    </td>
                                                                                    <td align="left" class="tdLabel50">
                                                                                        <asp:TextBox ID="tbLatitudeNS" runat="server" CssClass="text20" MaxLength="1" onBlur="return RemoveSpecialChars(this)"
                                                                                            onKeyPress="return fnAllowAlpha(this, event)" ValidationGroup="save"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="ltd_int" valign="top">
                                                                                        Deg.
                                                                                    </td>
                                                                                    <td class="ltd_int" valign="top">
                                                                                        Min.
                                                                                    </td>
                                                                                    <td class="ltd_int" valign="top">
                                                                                        NS
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdLabel50" valign="top">
                                                                                        <asp:RangeValidator ID="rvLatitudeDeg" runat="server" ControlToValidate="tbLatitudeDeg"
                                                                                            Type="Integer" MinimumValue="0" MaximumValue="90" ValidationGroup="save" CssClass="alert-text"
                                                                                            ErrorMessage="0-90" Display="Dynamic"></asp:RangeValidator>
                                                                                    </td>
                                                                                    <td class="tdLabel50" valign="top">
                                                                                        <asp:RangeValidator ID="rvLatitudeMin" runat="server" Type="Double" ControlToValidate="tbLatitudeMin"
                                                                                            MinimumValue="0" MaximumValue="60" ErrorMessage="0-60" ValidationGroup="save"
                                                                                            CssClass="alert-text" Display="Dynamic" SetFocusOnError="true"></asp:RangeValidator>
                                                                                        <asp:RegularExpressionValidator ID="revLatitudeMin" runat="server" ControlToValidate="tbLatitudeMin"
                                                                                            ValidationGroup="save" ErrorMessage="00.0" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,1})?$"
                                                                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <asp:RegularExpressionValidator ID="revLatitudeNS" runat="server" Display="Dynamic"
                                                                                            ControlToValidate="tbLatitudeNS" ErrorMessage="N or S" ValidationExpression="[nN]|[sS]"
                                                                                            CssClass="alert-text" SetFocusOnError="true" ValidationGroup="save"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td class="tdLabel130" valign="top">
                                                                            Longitude
                                                                        </td>
                                                                        <td class="tdLabel200" valign="top">
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel50">
                                                                                        <asp:TextBox ID="tbLongitudeDeg" runat="server" MaxLength="3" CssClass="text35" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="tdLabel50">
                                                                                        <asp:TextBox ID="tbLongitudeMin" runat="server" MaxLength="4" CssClass="text35" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"></asp:TextBox>
                                                                                    </td>
                                                                                    <td class="tdLabel50">
                                                                                        <asp:TextBox ID="tbLongitudeEW" runat="server" MaxLength="1" CssClass="text20" onKeyPress="return fnAllowAlpha(this, event)"
                                                                                            onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="ltd_int" valign="top">
                                                                                        Deg.
                                                                                    </td>
                                                                                    <td valign="top" class="ltd_int">
                                                                                        Min.
                                                                                    </td>
                                                                                    <td valign="top" class="ltd_int">
                                                                                        EW
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdLabel40" valign="top">
                                                                                        <asp:RangeValidator ID="rvLongitudeDeg" runat="server" ControlToValidate="tbLongitudeDeg"
                                                                                            Type="double" MinimumValue="0" MaximumValue="180" ValidationGroup="save" CssClass="alert-text"
                                                                                            ErrorMessage="0-180" Display="Dynamic"></asp:RangeValidator>
                                                                                    </td>
                                                                                    <td class="tdLabel40" valign="top">
                                                                                        <asp:RangeValidator ID="rvLongitudeMin" runat="server" ControlToValidate="tbLongitudeMin"
                                                                                            Type="double" MinimumValue="0" MaximumValue="60" ValidationGroup="save" CssClass="alert-text"
                                                                                            ErrorMessage="0-60" Display="Dynamic"></asp:RangeValidator>
                                                                                        <asp:RegularExpressionValidator ID="revLongitudeMin" runat="server" ControlToValidate="tbLongitudeMin"
                                                                                            ValidationGroup="save" ErrorMessage="00.0" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,1})?$"
                                                                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <asp:RegularExpressionValidator ID="revLongitudeEW" runat="server" ControlToValidate="tbLongitudeEW"
                                                                                            ValidationGroup="save" ErrorMessage="E or W" ValidationExpression="[eE]|[wW]"
                                                                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel120">
                                                                            Business Phone
                                                                        </td>
                                                                        <td class="tdLabel240">
                                                                            <asp:TextBox ID="tbBillPhone" runat="server" MaxLength="25" CssClass="text200" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                        </td>
                                                                        <td class="tdLabel130">
                                                                            <asp:Label ID="lbTollFreePhone" runat="server" Text="Toll-free Phone" />
                                                                        </td>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbTollFreePhone" runat="server" MaxLength="25" CssClass="text200"
                                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)" ValidationGroup="save" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel120" valign="top">
                                                                            Business E-mail
                                                                        </td>
                                                                        <td class="tdLabel240" valign="top">
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbEmailAddress" runat="server" CssClass="text200" ValidationGroup="save"
                                                                                            MaxLength="40">
                                                                                        </asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="revEmail" runat="server" Display="Dynamic" ErrorMessage="Invalid Format"
                                                                                            ControlToValidate="tbEmailAddress" CssClass="alert-text" ValidationGroup="save"
                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td class="tdLabel130" valign="top">
                                                                            Web site
                                                                        </td>
                                                                        <td class="tdLabel200" valign="top">
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbWebAddress" runat="server" CssClass="text200" MaxLength="200"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%-- <asp:RegularExpressionValidator ID="revwebsite" runat="server" Display="Dynamic"
                                                                                            ErrorMessage="Invalid Format" ControlToValidate="tbWebAddress" CssClass="alert-text"
                                                                                            ValidationGroup="save" ValidationExpression="([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?"></asp:RegularExpressionValidator>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel120">
                                                                            Business Fax
                                                                        </td>
                                                                        <td class="tdLabel240">
                                                                            <asp:TextBox ID="tbBillFax" runat="server" CssClass="text200" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                        </td>
                                                                        <td class="tdLabel130">
                                                                            SITA/AFTN
                                                                        </td>
                                                                        <td class="tdLabel200">
                                                                            <asp:TextBox ID="tbSITANumber" runat="server" CssClass="text200" MaxLength="100"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td align="left">
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlMainContact" Width="100%" ExpandAnimation-Type="none"
                CollapseAnimation-Type="none" runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Main Contact Information" Expanded="false">
                        <Items>
                            <telerik:RadPanelItem>
                                <ContentTemplate>
                                    <table width="100%" class="box1">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            First Name
                                                        </td>
                                                        <td class="tdLabel150" valign="top">
                                                            <asp:TextBox ID="tbFirstName" runat="server" ReadOnly="true" CssClass="text100" MaxLength="40"
                                                                onKeyPress="return fnAllowAlpha(this, event)"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel80" valign="top">
                                                            Middle Name
                                                        </td>
                                                        <td class="tdLabel140" valign="top">
                                                            <asp:TextBox ID="tbMiddleName" runat="server" ReadOnly="true" CssClass="text100"
                                                                MaxLength="40" onKeyPress="return fnAllowAlpha(this, event)"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel70" valign="top">
                                                            Last Name
                                                        </td>
                                                        <td class="tdLabel140" valign="top">
                                                            <asp:TextBox ID="tbLastName" runat="server" ReadOnly="true" CssClass="text100" MaxLength="40"
                                                                onKeyPress="return fnAllowAlpha(this, event)"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_10">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            Title
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <asp:TextBox ID="tbTitle" runat="server" ReadOnly="true" CssClass="text200" MaxLength="40"
                                                                onKeyPress="return fnAllowAlpha(this, event)"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            Address 1
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <asp:TextBox ID="tbAddr1" runat="server" ReadOnly="true" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            Address 2
                                                        </td>
                                                        <td class="tdLabel200" valign="top">
                                                            <asp:TextBox ID="tbAddr2" runat="server" ReadOnly="true" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            Address Line 3
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <asp:TextBox ID="tbAddr3" runat="server" ReadOnly="true" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                        </td>
                                                        <td class="tdLabel200" valign="top">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            City
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <asp:TextBox ID="tbCity" runat="server" ReadOnly="true" CssClass="text200" MaxLength="40"
                                                                onKeyPress="return fnAllowAlpha(this, event)"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            State/Province
                                                        </td>
                                                        <td class="tdLabel200" valign="top">
                                                            <asp:TextBox ID="tbState" runat="server" ReadOnly="true" CssClass="text200" MaxLength="10"
                                                                onKeyPress="return fnAllowAlpha(this, event)"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            Country
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <asp:TextBox ID="tbCountry" runat="server" ReadOnly="true" CssClass="text50" MaxLength="3"
                                                                onKeyPress="return fnAllowAlpha(this, event)"></asp:TextBox>
                                                            <button id="btnCountry" class="browse-button">
                                                            </button>
                                                        </td>
                                                        <td class="tdLabel130" valign="top">
                                                            Postal
                                                        </td>
                                                        <td class="tdLabel200" valign="top">
                                                            <asp:TextBox ID="tbPostal" runat="server" ReadOnly="true" CssClass="text200" MaxLength="15"
                                                                onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130">
                                                            <asp:Label ID="lbBusinessPhone" runat="server" Text="Business Phone" />
                                                        </td>
                                                        <td class="tdLabel240">
                                                            <asp:TextBox ID="tbBusinessPhone" runat="server" MaxLength="25" CssClass="text200"
                                                                onKeyPress="return fnAllowPhoneFormat(this,event)" ValidationGroup="save" />
                                                        </td>
                                                        <td class="tdLabel130">
                                                            Home Phone
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbPhone" runat="server" ReadOnly="true" CssClass="text200" MaxLength="25"
                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130">
                                                            <asp:Label ID="lbOtherPhone" runat="server" Text="Other Phone"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbOtherPhone" runat="server" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                            MaxLength="25" CssClass="text200" ValidationGroup="save">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="tdLabel130">
                                                            <asp:Label ID="lbCellPhoneNum" runat="server" Text="Primary Mobile/Cell" />
                                                        </td>
                                                        <td class="tdLabel240">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCellPhoneNum" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                            runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130">
                                                            <asp:Label ID="lbCellPhoneNum2" runat="server" Text="Secondary Mobile/Cell" />
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCellPhoneNum2" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                            runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130" valign="top">
                                                            E-mail Address
                                                        </td>
                                                        <td class="tdLabel240" valign="top">
                                                            <asp:TextBox ID="tbEmail" runat="server" ReadOnly="true" CssClass="text200" MaxLength="40"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130">
                                                            <asp:Label ID="lbBusinessEmail" runat="server" Text="Business E-mail"></asp:Label>
                                                        </td>
                                                        <td class="tdLabel240">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbBusinessEmail" runat="server" MaxLength="250" CssClass="text200"
                                                                            ValidationGroup="save">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130">
                                                            <asp:Label ID="lbPersonalEmail" runat="server" Text="Personal E-mail" />
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbPersonalEmail" runat="server" MaxLength="25" CssClass="text200"
                                                                            ValidationGroup="save" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="tdLabel130">
                                                            <asp:Label ID="lbOtherEmail" runat="server" Text="Other E-mail" />
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbOtherEmail" runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130">
                                                           Business Fax
                                                        </td>
                                                        <td class="tdLabel240">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbFax" ReadOnly="true" runat="server" CssClass="text200" MaxLength="25"
                                                                            onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130">                                                            
                                                            <asp:Label ID="lbHomeFax" runat="server" Text="Home Fax" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbHomeFax" runat="server" MaxLength="25" CssClass="text200" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                ValidationGroup="save" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel130">
                                                            <asp:Label ID="lbWebsite" runat="server" Text="Web site" />
                                                        </td>
                                                        <td class="tdLabel240">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbWebsite" runat="server" MaxLength="25" CssClass="text200" ValidationGroup="save" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130">
                                                            <%--<asp:Label ID="lbMCBusinessEmail" runat="server" Text="Business E-mail" />--%>
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <%--<asp:TextBox ID="tbMCBusinessEmail" runat="server" MaxLength="25" CssClass="text200"
                                                                            ValidationGroup="save" />--%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_10">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td valign="top">
                                                            Additional Contact Information
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td valign="top">
                                                            <asp:TextBox ID="tbAdditionalContact" runat="server" ReadOnly="true" TextMode="MultiLine"
                                                                CssClass="textarea720x80"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td align="left">
                        <div class="nav-space">
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlNotes" Width="100%" ExpandAnimation-Type="none" CollapseAnimation-Type="none"
                runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Text="Notes" Expanded="false">
                        <Items>
                            <telerik:RadPanelItem>
                                <ContentTemplate>
                                    <table width="100%" cellspacing="0" class="note-box">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbNotes" runat="server" TextMode="MultiLine" CssClass="textarea-db">
                                                </asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <asp:Button ID="btnSaveChanges" Text="Save" runat="server" ValidationGroup="save"
                            OnClientClick="javascript:return CheckLatitude();" OnClick="SaveChanges_Click"
                            CssClass="button" />
                        <asp:Button ID="btnSaveChanges_Dummy" Text="Save" runat="server" ValidationGroup="save"
                            OnClick="SaveChanges_Click" CssClass="button" Style="display: none;" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnHomeBaseID" runat="server" />
                        <asp:HiddenField ID="hdnAirportID" runat="server" />
                        <asp:HiddenField ID="hdnCountryID" runat="server" />
                        <asp:HiddenField ID="hdnMetroID" runat="server" />
                        <asp:HiddenField ID="hdnContactName" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="button" CausesValidation="false"
                            OnClick="Cancel_Click" />
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
