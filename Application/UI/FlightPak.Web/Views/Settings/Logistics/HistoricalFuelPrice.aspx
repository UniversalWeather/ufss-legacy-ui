﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master" AutoEventWireup="true" CodeBehind="HistoricalFuelPrice.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Logistics.HistoricalFuelPrice" %>
<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
        <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <style type="text/css">
        .art-setting-PostContent .RadAjaxPanel .rgHeaderWrapper .rgHeaderDiv { margin-right:0 !important;}
        .marginTop{margin-top:5px !important}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadAjaxManager id="RadAjaxManager1" runat="server" >
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
             <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgFuelPrice">
                <UpdatedControls>                    
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="pnlFilter">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:radwindowmanager id="RadWindowManager1" runat="server">
    <Windows>
         <telerik:RadWindow ID="radSearchAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" OnClientClose="OnClientSearchAirportRadWindowClose" KeepInScreenBounds="true" Modal="true" Width="450px" Height="400px"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
         </telerik:RadWindow>
           <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" OnClientClose="OnClientAirportRadWindowClose" KeepInScreenBounds="true" Modal="true" Width="450px" Height="400px"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
         </telerik:RadWindow>
   </Windows>
</telerik:radwindowmanager>
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                    ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
            <ConfirmTemplate>
                <div class="rwDialogPopup radconfirm">
                    <div class="rwDialogText">
                        {1}
                    </div>
                    <div>
                        <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                        <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                        <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                    </div>
                </div>
            </ConfirmTemplate>
        </telerik:RadWindowManager>
    <telerik:radajaxloadingpanel id="RadAjaxLoadingPanel1" runat="server" skin="Sunset"></telerik:radajaxloadingpanel>
    <table width="100%" cellpadding="0" cellspacing="0" runat="server"
        id="table1">
        <tr>
            <td>
                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left">
                            <div class="tab-nav-top">
                                <span class="head-title">
                                    <a href="/Views/Settings/Logistics/FuelVendor.aspx?Screen=FuelVendor">Fuel Vendor</a>&nbsp;>Fuel Price
                                </span>
                                <span class="tab-nav-icons"><a href="#" class="help-icon"></a></span>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" width="100%" class="sticky" runat="server" id="table2">
            <tr>
                <td class="tdLabel80">
                    Fuel Vendor Code
                </td>
                <td class="tdLabel60">
                    <asp:TextBox ID="tbVendorCode" runat="server" Enabled="false" CssClass="readonly-text60"></asp:TextBox>
                    <asp:HiddenField ID="hdnVendorId" runat="server" />
                </td>
                <td class="tdLabel50">
                    Name
                </td>
                <td class="tdLabel100">
                    <asp:TextBox ID="tbVendorName" runat="server" Enabled="false" CssClass="readonly-text100"></asp:TextBox>
                </td>
                 <td class="tdLabel50">
                    Description
                </td>
                <td class="tdLabel100">
                    <asp:TextBox ID="tbDescription" runat="server" Enabled="false" CssClass="readonly-text100"></asp:TextBox>
                </td>
            </tr>
    </table>
         <table class="preflight-search-box" style="width:auto !important;padding:0 !important">
                <tr>
                    <td valign="top" width="50%">
                        <fieldset>
                            <legend>Search</legend>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="tdLabel80">Airport ICAO</td>
                                                <td class="tdLabel120">
                                                    <asp:TextBox ID="tbSearchIcao" runat="server" CssClass="text70" MaxLength="5" AutoPostBack="true" OnTextChanged="tbSearchIcao_OnTextChanged"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnSearchIcao" runat="server"/>
                                                    <asp:Button runat="server" ID="btnSearchAirportIcao" CssClass="browse-button" OnClientClick="javascript:openWin('SearchAirportRadWindow');return false;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                
                                                <td></td>
                                                <td>
                                                    <asp:CustomValidator ID="customValidatorSearchIcao" runat="server" CssClass="alert-text" Display="Dynamic" ControlToValidate="tbSearchIcao" ErrorMessage="Invalid Airport ICAO"></asp:CustomValidator>
                                                    <asp:Label ID="lblSearchIcaoDescription" CssClass="input_no_bg" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="tdLabel40">FBO</td>
                                                <td>
                                                    <asp:TextBox ID="tbSearchFbo" runat="server" CssClass="text150" MaxLength="10" AutoPostBack="true"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="tdLabel80">Effective Date</td>
                                                <td>
                                                    <asp:TextBox ID="tbSearchEffectiveDate" runat="server" CssClass="text80" MaxLength="10" onchange="parseDate(this, event);" onClick="showPopup(this, event);"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                   <td><asp:Button ID="btnSearch" Text="Search" runat="server" OnClick="btnSearch_OnClick" CssClass="button" /></td>
                                </tr>
                            </table>
                         </fieldset>
                    </td>
                </tr>
         </table>

        <telerik:radgrid id="dgFuelPrice" runat="server" CssClass="marginTop" AllowSorting="true"
                    OnNeedDataSource="dgFuelPrice_OnNeedDataSource" OnItemCreated="dgFuelPrice_OnItemCreated"
                    OnSelectedIndexChanged="dgFuelPrice_OnSelectedIndexChanged" OnPageIndexChanged="dgFuelPrice_OnPageIndexChanged"
                    OnItemCommand="dgFuelPrice_OnItemCommand" OnUpdateCommand="dgFuelPrice_OnUpdateCommand" 
                    OnInsertCommand="dgFuelPrice_OnInsertCommand" OnDeleteCommand="dgFuelPrice_OnDeleteCommand" OnInit="dgFuelPrice_OnInit"
                    autogeneratecolumns="True"  pagesize="20" allowpaging="true" 
                    pagerstyle-alwaysvisible="true" AllowCustomPaging="true"> 
                    <MasterTableView DataKeyNames="FlightpakFuelID,CustomerID,DepartureICAOID,DepartureICAO,FBOName,GallonFrom,GallonTO,EffectiveDT,UnitPrice,FuelVendorID,FromTable,AirportName,IsActive,FileUploadedOn"
                       ClientDataKeyNames="FlightpakFuelID,CustomerID,DepartureICAOID,DepartureICAO,FBOName,GallonFrom,GallonTO,EffectiveDT,UnitPrice,FuelVendorID,FromTable,AirportName,IsActive,FileUploadedOn"
                        CommandItemDisplay="Bottom">
                        <Columns>
                            <telerik:GridBoundColumn DataField="FlightpakFuelID" HeaderText="FlightpakFuelID" ShowFilterIcon="false" Display="false"/>
                            <telerik:GridBoundColumn DataField="DepartureICAOID" HeaderText="DepartureICAOID" ShowFilterIcon="false" Display="false"/>
                            <telerik:GridBoundColumn DataField="DepartureICAO" HeaderText="ICAO" AllowFiltering="false" AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="70px"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="FBOName" HeaderText="FBO" AllowFiltering="false" AutoPostBackOnFilter="false" ShowFilterIcon="false" HeaderStyle-Width="300px"></telerik:GridBoundColumn>
                            <telerik:GridCalculatedColumn DataFields="GallonFrom, GallonTO" ShowFilterIcon="False" HeaderText="Gallon Range" AllowFiltering="False" Expression='{0} + " - " + {1}' HeaderStyle-Width="90px"></telerik:GridCalculatedColumn>
                            <telerik:GridBoundColumn DataField="EffectiveDT" HeaderText="Effective Date" AllowFiltering="false" DataFormatString="{0:dd/MM/yyyy}" AutoPostBackOnFilter="false" HeaderStyle-Width="100px"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="UnitPrice" HeaderText="Price" DataFormatString="{0:$##.####}" AllowFiltering="false" AutoPostBackOnFilter="false" HeaderStyle-Width="100px"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="FileUploadedOn" HeaderText="File Uploaded On" AllowFiltering="false" DataFormatString="{0:dd/MM/yyyy}" AutoPostBackOnFilter="false" ></telerik:GridBoundColumn>
                        </Columns>
                        <CommandItemTemplate>
                            <div style="padding: 5px 5px; float: left; clear: both;">
                                <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid" Visible='<%# IsAuthorized(Permission.Database.AddHistoricalFuelFileData)%>' CommandName="InitInsert"></asp:LinkButton>
                                <asp:LinkButton ID="lnkInitEdit" runat="server" ToolTip="Edit" CommandName="Edit" Visible='<%# IsAuthorized(Permission.Database.EditHistoricalFuelFileData)%>'>
                                    <img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>"  />
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();" Visible='<%# IsAuthorized(Permission.Database.DeleteHistoricalFuelFileData)%>' runat="server" CommandName="DeleteSelected" ToolTip="Delete" >
                                    <img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" />
                                </asp:LinkButton>
                            </div>
                        </CommandItemTemplate>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
        </telerik:radgrid>
     <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.
                        </div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field
                        </div>
                    </td>
                </tr>
            </table>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <asp:HiddenField ID="hdnFlightpakFuelId" runat="server"/>
            <asp:HiddenField ID="hdnSave" runat="server"/>
            <asp:HiddenField ID="hdnIsSaved" runat="server"/>
            <asp:HiddenField ID="hdnFromTable" runat="server"/>
            <table width="100%" class="border-box" runat="server" id="tblFormInput">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel80">
                                    <asp:CheckBox runat="server" ID="chkIsActive" Text="IsActive" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel120" valign="top"><span class="mnd_text">Airport ICAO</span></td>
                                    <td class="tdLabel120" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbAirportIcao" runat="server" CssClass="text80" MaxLength="5" ValidationGroup="save" AutoPostBack="true" onKeyPress="return fnAllowAlphaNumeric(this,event)" OnTextChanged="tbAirportIcao_OnTextChanged"></asp:TextBox>
                                                    <asp:Button runat="server" ID="btnAirportIcao" CssClass="browse-button" OnClientClick="javascript:openWin('radAirportPopup');return false;" />
                                                    <asp:HiddenField ID="hdnAirportIcao" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CustomValidator ID="customValidatorAirportIcao" runat="server" ValidationGroup="save" CssClass="alert-text" Display="Dynamic" ControlToValidate="tbAirportIcao" ErrorMessage="Invalid Airport ICAO"></asp:CustomValidator>
                                                    <asp:Label ID="lblAirportIcaoDescription" CssClass="input_no_bg" runat="server" Text=""></asp:Label>
                                                    <asp:RequiredFieldValidator ID="rfvAirportIcao" runat="server" ErrorMessage="Airport ICAO is Required"
                                                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbAirportIcao" CssClass="alert-text"
                                                        ValidationGroup="save"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tdLabel80" valign="top"><span class="mnd_text">FBO Name</span></td>
                                    <td></td>
                                    <td class="tdLabel120" valign="top">
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbFboName" ValidationGroup="save" onKeyPress="return fnAllowAlphaNumeric(this,event)" runat="server" CssClass="text200"
                                                        MaxLength="25"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="rfvFboName" Display="Dynamic" runat="server" CssClass="alert-text"
                                                        ControlToValidate="tbFboName" ValidationGroup="save">FBO Name is Required.</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                 </tr>
                        </table>
                    </td>
                 </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                    <td class="tdLabel120" valign="top"><span class="mnd_text">Gallon From</span></td>
                                    <td class="tdLabel120" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbGallonFrom" runat="server" onKeyPress="return fnAllowNumeric(this,event)" CssClass="text60" MaxLength="5" ValidationGroup="save"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="rfvGallonFrom" runat="server" ErrorMessage="Gallon From is Required"
                                                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbGallonFrom" CssClass="alert-text"
                                                        ValidationGroup="save"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tdLabel80" valign="top"><span class="mnd_text">Gallon To</span></td>
                                    <td></td>
                                    <td class="tdLabel120" valign="top">
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbGallonTo" ValidationGroup="save" onKeyPress="return fnAllowNumeric(this,event)" runat="server" CssClass="text60"
                                                        MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="rfvGallonTo"  Display="Dynamic" runat="server" CssClass="alert-text"
                                                        ControlToValidate="tbGallonTo" ValidationGroup="save">Gallon To is Required.</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                 </tr>
                        </table>
                    </td>
                 </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                    <td class="tdLabel120" valign="top"><span class="mnd_text">Effective Date</span></td>
                                    <td class="tdLabel120" valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbEffectiveDate" runat="server" CssClass="text90"  onchange="parseDate(this, event);" onClick="showPopup(this, event);" ValidationGroup="save"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="rfvEffectiveDate" runat="server" ErrorMessage="Effective Date is Required"
                                                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbEffectiveDate" CssClass="alert-text"
                                                        ValidationGroup="save"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tdLabel80" valign="top"><span class="mnd_text">Unit Price</span></td>
                                    <td></td>
                                    <td class="tdLabel120" valign="top">
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="tbUnitPrice" ValidationGroup="save" onKeyPress="return fnAllowNumericAndChar(this,event,'.')" runat="server" CssClass="text60"
                                                        MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="rfvUnitPrice" Display="Dynamic" runat="server" CssClass="alert-text"
                                                        ControlToValidate="tbUnitPrice" ValidationGroup="save">Unit Price is Required.</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                 </tr>
                        </table>
                    </td>
                 </tr>
             </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnSave" OnClick="btnSave_OnClick" ValidationGroup="save" Text="Save" runat="server" CssClass="button" />
                                <asp:Button ID="btnCancel" OnClick="btnCancel_OnClick" CausesValidation="false" Text="Cancel" runat="server" CssClass="button" />
                            </td>
                        </tr>
                    </table>
        </asp:Panel>
    </div>
    <telerik:radcodeblock id="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            //This method is called to handle the onclick and onfocus client side events for the texbox
            function showPopup(sender, e) {
                //this is a reference to the texbox which raised the event
                //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html
                currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                //this gets a reference to the datepicker, which will be shown, to facilitate
                //the selection of a date
                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                //this variable is used to store a reference to the date picker, which is currently
                //active
                currentDatePicker = datePicker;

                //this method first parses the date, that the user entered or selected, and then
                //sets it as a selected date to the picker

                datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));


                //the code lines below show the calendar, which is used to select a date. The showPopup
                //function takes three arguments - the x and y coordinates where to show the calendar, as
                //well as its height, derived from the offsetHeight property of the textbox
                var position = datePicker.getElementPosition(currentTextBox);
                datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
            }

            //this function is used to parse the date entered or selected by the user
            function parseDate(sender, e) {
                if (currentDatePicker != null) {
                    var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                    var dateInput = currentDatePicker.get_dateInput();
                    var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                    sender.value = formattedDate;
                }
            }

            function hide() {
                var datePicker = $find("<%= RadDatePicker1.ClientID%>");
                datePicker.hidePopup();
                return true;
            }
            function dateSelected(sender, args) {

                if (currentTextBox != null) {
                    var step = "Date_TextChanged";
                    if (currentTextBox.value != args.get_newValue()) {
                        currentTextBox.value = args.get_newValue();
                        var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                }
            }

            function openWin(type) {
                var url = '';
                if (type == "SearchAirportRadWindow") {
                    var icaoId = IsNullOrEmpty($("#<%=tbSearchIcao.ClientID%>").val()) ? "" : $("#<%=tbSearchIcao.ClientID%>").val();
                    url = '../../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + icaoId;
                    var oWnd = radopen(url, 'radSearchAirportPopup');
                } else if (type == "radAirportPopup") {
                    var icaoId = IsNullOrEmpty($("#<%=tbAirportIcao.ClientID%>").val()) ? "" : $("#<%=tbAirportIcao.ClientID%>").val();
                    url = '../../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + icaoId;
                    var oWnd = radopen(url, 'radAirportPopup');
                }
            }

            function OnClientSearchAirportRadWindowClose(oWnd, args) {
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        $("#<%=tbSearchIcao.ClientID%>").val(arg.ICAO.toUpperCase());
                        window.__doPostBack("txtFuelVendor", "TextChanged");
                    }
                    else {
                        $("#<%=tbSearchIcao.ClientID%>").val("");
                        $("#<%=hdnAirportIcao.ClientID%>").val("");
                        $("#<%=lblSearchIcaoDescription.ClientID%>").val("");
                    }
                }
            }
            
            function OnClientAirportRadWindowClose(oWnd, args) {
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        $("#<%=tbAirportIcao.ClientID%>").val(arg.ICAO.toUpperCase());
                        window.__doPostBack("tbAirportIcao", "TextChanged");
                    }
                    else {
                        $("#<%=tbAirportIcao.ClientID%>").val("");
                        $("#<%=hdnAirportIcao.ClientID%>").val("");
                        $("#<%=lblAirportIcaoDescription.ClientID%>").val("");
                    }
                }
            }

            function pageLoad() {
                var isSaved = $("#<%=hdnIsSaved.ClientID%>").val();
                if (IsNullOrEmpty(isSaved) == false) {
                    ShowSuccessMessage();
                    $("#<%=hdnIsSaved.ClientID%>").val("");
                }
            }
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSave.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }
        </script>
    </telerik:radcodeblock>
</asp:Content>
