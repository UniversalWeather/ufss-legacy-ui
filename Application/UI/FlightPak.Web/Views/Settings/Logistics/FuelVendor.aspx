﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Framework/Masters/Settings.master"
    CodeBehind="FuelVendor.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Logistics.FuelVendor"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/UserControls/FuelVendor/FuelUploadWizard1.ascx" TagPrefix="UC" TagName="FuelUploadWizard1" %>
<%@ Register Src="~/UserControls/FuelVendor/FuelUploadWizard2.ascx" TagPrefix="UC" TagName="FuelUploadWizard2" %>
<%@ Register Src="~/UserControls/FuelVendor/FuelUploadWizard3.ascx" TagPrefix="UC" TagName="FuelUploadWizard3" %>
<%@ Register Src="~/UserControls/FuelVendor/FuelUploadWizard4.ascx" TagPrefix="UC" TagName="FuelUploadWizard4" %>
<%@ Register Src="~/UserControls/FuelVendor/FuelUploadWizard5.ascx" TagPrefix="UC" TagName="FuelUploadWizard5" %>
<%@ Register Src="~/UserControls/FuelVendor/FuelUploadWizard6.ascx" TagPrefix="UC" TagName="FuelUploadWizard6" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/Common.js")%>" ></script>
    <style type="text/css">
        .MakeItCenter > span {margin-left:22px;}
        .modal-backdrop{z-index:-1!important;position:relative!important;}
        .formatChanged {background: none repeat scroll 0 0 #ca5e61 !important;color: #fff;}
        .formatChangedText{color:#ca5e61!important;}
        .fd-zone {position: relative;overflow: hidden;width: 80%;height: 13em;margin: 0 auto;text-align: center;}        
        .fd-file {right: 212px;color: #a6a6a6;cursor: pointer;font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;font-size: 14px;height: 44px;left: 50%;margin: -3px 0 0 -54px !important;min-height: 40px;opacity: 0;z-index: 1;padding: 0;position: absolute;top: 40px;width: 122px;cursor: pointer;filter: alpha(opacity=0);font-family: sans-serif;}
        .fd-zone.over {border-color: maroon;background: #eee;}
        .progress-edit {height: 8px !important;border-radius: 0px !important;}
        body{background-color:white;height:auto;width:auto;}
         .art-setting-PostContent .RadAjaxPanel .rgHeaderWrapper .rgHeaderDiv { margin-right:0 !important;}
    </style>    
    <link href="<%= Page.ResolveUrl("~/Scripts/dragdrop/fss.css")%>" rel="stylesheet" />        
    <link href="<%= Page.ResolveUrl("~/Scripts/dragdrop/filedrop.css") %>" rel="stylesheet" />        
    <link href="<%= Page.ResolveUrl("~/Scripts/dragdrop/jquery-ui-theme-smoothness.css") %>" rel="stylesheet" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:radcodeblock id="RadCodeBlock1" runat="server">
    <script type="text/javascript">
            var currentLoadingPanel;
            var currentUpdatedControl;
            function RequestStart(sender, args) {
                currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                currentUpdatedControl = "<%=DivExternalForm.ClientID%>";
                //show the loading panel over the updated control
                currentLoadingPanel.hide(currentUpdatedControl);
                currentLoadingPanel.show(currentUpdatedControl);
            }
            function ResponseEnd() {
                //hide the loading panel and clean up the global variables
                if (currentLoadingPanel != null)
                    currentLoadingPanel.hide(currentUpdatedControl);
                currentUpdatedControl = null;
                currentLoadingPanel = null;
            }
            
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            function printImage() {
                var image = document.getElementById('<%=ImgPopup.ClientID%>');
                PrintWindow(image);
            }

            function CloseRadWindow() {
                var oWnd = $find("<%=RadwindowImagePopup.ClientID%>");
                document.getElementById('<%=ImgPopup.ClientID%>').src = null;
                oWnd.close();
            }

            function poponload() {
                myWindow = window.open("", "mywindow");
                var javascriptVariable = "<%= Microsoft.Security.Application.Encoder.HtmlEncode(StrErrUpload) %>";
                myWindow.document.write(String(javascriptVariable));
                return false;

            }
            function confirmCallBackFn(arg) {
            }

            
            $(document).ready(function UpdateConfirm() {
                $('#art-contentLayout_popup').css("width", "600px");
                $('#art-sheet').css("width", "620px");
                $('#art-sheet-body').css("width", "620px");
                $('#art-sheet-body').css("position", "fixed");
                $('#art-sheet-body').css("padding", "0px 0px 0px 5px");
                
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function prepareSearchInput(input) { input.value = input.value; }
            function openWin(radWin) {
                var url = '';

                if (radWin == "RadFuelFileUploadPopup") {
                    url = "FuelFileUploadWizard1.aspx";
                }

                if (url != "") {
                    var oWnd = radopen(url, radWin);
                }
            }

            function redirectFuelPrice() {
                var fuelVendorCode = $("#<%=tbCode.ClientID%>").val();
                var fuelVendorName = $("#<%=tbName.ClientID%>").val();
                var fuelVendorDescription = $("#<%=tbDescription.ClientID%>").val();
                var fuelVendorId = $("#<%=hdnFuelVendorID.ClientID%>").val();
                window.location.href = "/Views/Settings/Logistics/HistoricalFuelPrice.aspx?Screen=FuelVendor&fuelVendorCode=" + fuelVendorCode + "&fuelVendorName=" + fuelVendorName + "&fuelVendorDescription=" + fuelVendorDescription + "&fuelVendorID=" + fuelVendorId;
            }

        </script>
    </telerik:radcodeblock>
    <telerik:radajaxmanager id="RadAjaxManager1" runat="server" onajaxsettingcreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgFuelVendor">
                <UpdatedControls>                    
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="pnlFilter">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:radajaxmanager>
    <telerik:radwindowmanager id="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadwindowImagePopup" runat="server" VisibleOnPageLoad="false"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                Behaviors="close" Title="Fuel File Template" OnClientClose="CloseRadWindow">
                <ContentTemplate>
                    <div>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="float_printicon">
                                        <asp:ImageButton ID="ibtnPrint" runat="server" ImageUrl="~/App_Themes/Default/images/print.png"
                                            AlternateText="Print" OnClientClick="javascript:printImage();return false;" />
                                    </div>
                                    <asp:Image ID="ImgPopup" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
        
             <telerik:RadWindow ID="RadFuelFileUploadPopup" runat="server"  Height="500px" Width="600px"
                    ReloadOnShow="true" KeepInScreenBounds="true" Modal="false" Behaviors="close"
                    VisibleStatusbar="false">
                </telerik:RadWindow>
        </Windows>
    </telerik:radwindowmanager>
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <telerik:radajaxloadingpanel id="RadAjaxLoadingPanel1" runat="server" skin="Sunset"></telerik:radajaxloadingpanel>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="headerText">
                <div class="tab-nav-top">
                    <span class="head-title">Fuel Vendors</span> 
                    <span class="tab-nav-icons">
                        <a href="../../Help/ViewHelp.aspx?Screen=FuelVendorHelp"target="_blank" title="Help" class="help-icon"></a>
                    </span>
                </div>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" class="head-sub-menu" id="table2" runat="server">
        <tr>
            <td>
                <div class="tags_select" style="padding: 6px 0 6px 6px !important;">
                    <a href="javascript:void(0)" onclick="return redirectFuelPrice()">Fuel Price</a>
                </div>
            </td>
        </tr>
    </table>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <div id="LoadingSpinner" style="display: none;background: rgba(255, 255, 255, 0.5) ; position: absolute; width: 100%;text-align: center;z-index: 999;">
              <img src="/App_Themes/Default/images/loading.gif" style="position: absolute; left: 50%; top: 50%; margin-left: -32px; margin-top: -32px;" alt="Loading.."/>
        </div>
        <asp:Panel ID="pnlFilter" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" class="head-sub-menu">
                <tr>
                    <td>
                        <div>
                            Please note: Contact FSS Support to add fuel vendors not listed below.
                        </div>
                        <div class="status-list">
                            <span>
                                <asp:CheckBox ID="chkSearchActiveOnly" runat="server" Text="Active Only" OnCheckedChanged="FilterByCheckbox_OnCheckedChanged" AutoPostBack="true" />
                            </span>
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <telerik:radgrid id="dgFuelVendor" runat="server" allowsorting="true" 
                onselectedindexchanged="dgFuelVendor_SelectedIndexChanged"
                onneeddatasource="dgFuelVendor_BindData"
                onprerender="dgFuelVendor_PreRender"
                onpageindexchanged="dgFuelVendor_PageIndexChanged"
                autogeneratecolumns="false" pagesize="10" allowpaging="true" allowfilteringbycolumn="true"
                pagerstyle-alwaysvisible="true" height="341px">
                <MasterTableView DataKeyNames="FuelVendorID,VendorCD,VendorName,AirportID,CustomerID,VendorDescription,ServerInfo,BaseFileName,
                    FileLocation,FileTYPE,FldSep,IsHeaderRow,VendorDateFormat,LastUpdUID,LastUpdTS,IsDeleted,UWAUpdates,IsInActive,NoOfFuelRecordsInserted"
                    ClientDataKeyNames="VendorCD,VendorName,VendorDescription" CommandItemDisplay="Bottom">
                    <Columns>
                        <telerik:GridBoundColumn DataField="FuelVendorID" HeaderText="FuelVendorID" ShowFilterIcon="false"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="VendorCD" HeaderText="Code" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" HeaderStyle-Width="100px" FilterControlWidth="80px" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="VendorName" HeaderText="Name" AutoPostBackOnFilter="false" CurrentFilterFunction="StartsWith"
                            ShowFilterIcon="false" HeaderStyle-Width="250px" FilterControlWidth="230px" FilterDelay="500">
                        </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn DataField="LastUpdUID" HeaderText="File Uploaded By" AutoPostBackOnFilter="false" CurrentFilterFunction="StartsWith"
                            ShowFilterIcon="false" HeaderStyle-Width="110px" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn DataField="LastUpdTS" HeaderText="File Uploaded On" AutoPostBackOnFilter="false" CurrentFilterFunction="StartsWith"
                            ShowFilterIcon="false"  DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Width="110px" AllowFiltering="false" FilterDelay="500">
                        </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn DataField="NoOfFuelRecordsInserted" HeaderText="Saved Records"  CurrentFilterFunction="StartsWith"
                            ShowFilterIcon="false"  HeaderStyle-Width="100px" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  FooterStyle-HorizontalAlign="Center"  DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                           ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false" ItemStyle-CssClass="MakeItCenter">
                        </telerik:GridCheckBoxColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div>
                            <asp:Label ID="lbLastUpdatedUser" CssClass="last-updated-text" runat="server"></asp:Label>
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings EnablePostBackOnRowClick="true">
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:radgrid>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table width="100%" class="border-box" runat="server" id="tblFormInput">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel80">
                                    <asp:CheckBox runat="server" ID="chkInactive" Text="Inactive" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel120" valign="top">
                                    <span class="mnd_text">Fuel Vendor Code</span>
                                </td>
                                <td class="tdLabel120" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbCode" runat="server" CssClass="text50" MaxLength="5" ValidationGroup="save"></asp:TextBox>
                                                <asp:HiddenField ID="hdnFuelVendorID" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel80" valign="top">
                                    <span class="mnd_text">Name</span>
                                </td>
                                <td></td>
                                <td class="tdLabel120" valign="top">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbName" ValidationGroup="save" runat="server" CssClass="text200"
                                                    MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel120" valign="top">
                                    <span class="mnd_text">Description</span>
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbDescription" ValidationGroup="save" runat="server" CssClass="text400" MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvDescription" CssClass="alert-text" runat="server"
                                                    ControlToValidate="tbDescription" Display="Dynamic" ErrorMessage="Description is Required."
                                                    ValidationGroup="save"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="display: none;">
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel120" valign="top">
                                    Server Information
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbServerInformation" runat="server" CssClass="text400" MaxLength="200" onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>                       
                <tr style="display: none;">
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel120" valign="top">
                                    File Location
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbfilelocation" runat="server" CssClass="text400"></asp:TextBox>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="tdLabel120" valign="top">
                                    <asp:Label ID="lblFilename1" runat="server" Text="File Name"></asp:Label>
                                </td>
                                <td>  
                                    <asp:HiddenField ID="hdnIsFormatChanged" ClientIDMode="Static" Value="false" runat="server" />
                                    <asp:HiddenField ID="hdnIframeSuccess" ClientIDMode="Static" Value="" runat="server" />
                                    <asp:HiddenField ID="hdnMappedColumnsIDs" Value="" runat="server" />
                                    <asp:HiddenField ID="hdnIsColumnsMappedCorrect" Value="false" runat="server" />
                                    <asp:HiddenField ID="hdnUploadedFileName" runat="server" />
                                    <asp:Button runat="server" ID="btnOpenFuelUploadWizard" Visible='<%# IsAuthorized(Permission.Database.EditFuelVendor)%>' OnClientClick="return false;" class="button" data-toggle="modal" data-backdrop="static" data-target="#uploadWizardModal" ClientIDMode="Static" Text="Upload File" ></asp:Button>                                            
                                    <asp:Label ID="lblFilename" ClientIDMode="Static" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>                                            
            </table>
            <asp:HiddenField ID="hdnSave" runat="server" />
            <asp:HiddenField ID="hdnDate" runat="server" />
            <asp:HiddenField ID="hdnRedirect" runat="server" /> 
            <%-- <Triggers>
                    <asp:PostBackTrigger ControlID="btnSave" />
                    <asp:PostBackTrigger ControlID="tbCode" />
                    <asp:PostBackTrigger ControlID="btnCancel" />
                </Triggers>--%>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <UC:FuelUploadWizard1 runat="server" id="FuelUploadWizard1" />
        <UC:FuelUploadWizard2 runat="server" id="FuelUploadWizard2" />
        <UC:FuelUploadWizard3 runat="server" ID="FuelUploadWizard3" />
        <UC:FuelUploadWizard4 runat="server" ID="FuelUploadWizard4" />
        <UC:FuelUploadWizard5 runat="server" ID="FuelUploadWizard5" />
        <UC:FuelUploadWizard6 runat="server" ID="FuelUploadWizard6" />

        </asp:Panel>
       
        <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
            
            <script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
            <script  type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/jquery-ui.min.js") %>"></script>
            <script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/bootstrap/bootstrap.min.js")%>"></script>
            <script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/dragdrop/filedrop.js")%>"></script>            
            <script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/dragdrop/ie9.js")%>"></script>
            <script type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/dragdrop/modernizr-2.6.2.js")%>"></script>
            

            <script type="text/javascript">
                var zone;
                var isBackPress = 0;
                var lblMsgNoFileSelected = "No file uploaded.";
                var pMsgEmptyFileError = "Upload Fail. File is empty.";
                var pMsgWrongFileDataError = "Upload Fail. Please upload valid CSV data file.";
                var pMsgWrongFileTypeUploadError = "Upload Fail. Please upload CSV format file.";
                var pMsgFileSizeError = "Upload Fail. Please upload maximum 10 MB file size.";
                var pMsgFileNetworkError = "Upload Fail. File could not uploaded due to network failure.";
                var pMsgFileErrorDefault = "Uploading Fail. Please try one more time.";
                var pMsgFileErrorWrongBrowser = "Uploade Fail. Browser version does not support drag and drop. Please use 'Browse File' .";
                var isFileUploadSuccess = false;
                
                var options = { iframe: { url: "FuelFileUpload.aspx?ie=no" } };
                var isIE89 = false;
                var progress1 = 0;

                function pageLoad() {

                    if($telerik.isIE8){                        
                        isIE89 = true;
                    }
                    if ($telerik.isIE9) {
                        isIE89 = true;
                    }

                    if (isIE89==true) {
                        options = { iframe: { url: "FuelFileUpload.aspx?ie=ie89" } };
                        $("#zone").hide();
                        $("#spanMsg2").hide();
                    } else {
                        $("#IE8Zone").hide();                        
                    }
                    zoneIntialization();
                    modalIntialization();                   
                    
                    if ($("#<%=hdnUploadedFileName.ClientID%>").val().length < 1) {
                        $("#lblFilename").html(lblMsgNoFileSelected);
                    } else {
                        $("#lblFilename").html($("#<%=hdnUploadedFileName.ClientID%>").val());
                    }
                }

                /// This is used to call webmethod for making dataset for preview of file data
                function previewFuelUploadedFile(successCallback) {
                    // calling GetUploadedFileData of FuelFileUpload
                    $.ajax({
                        type: "POST",
                        url: "FuelFileUpload.aspx/GetUploadedFileData",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: successCallback,
                        error: function (e) { $("#divDataLoading").hide(); }
                    });
                }
                function OnSuccess(response) {
                    $("#uploadWizardModal").modal("hide");
                    $("#divDataLoading").hide();
                    var resp= JSON.parse(response.d);
                    var ischanged = resp.IsChanged;
                    if (ischanged) {
                        $("#uploadWizardFormatNotFound").modal("show");
                    } else {
                        saveCSVData();
                    }
                    if (ischanged == true) {                         
                        $("#pMsgIfChangedFormat").show();
                        $("#<%=hdnIsFormatChanged.ClientID%>").val("true");                         
                    } else {
                        $("#pMsgIfChangedFormat").hide();
                        $("#<%=hdnIsFormatChanged.ClientID%>").val("false");
                    }
                }

                function modalIntialization() {

                    // Set all modal popup as modal dialog by setting its backdrop to 'static' value.
                    //So that out side click will not close popup.
                    $('#uploadWizardModal').modal({
                        backdrop: 'static',
                        show: false
                    });
                    $('#uploadWizardModal2').modal({
                        backdrop: 'static',
                        show: false
                    });
                    $('#uploadWizardModalConfirmation').modal({
                        backdrop: 'static',
                        show: false
                    });
                    $('#uploadWizardCompleteModal').modal({
                        backdrop: 'static',
                        show: false
                    });
                    $('#uploadFieldMappingWizardModal').modal({
                        backdrop: 'static',
                        show: false
                    });

                    $('#uploadWizardModal').on('show.bs.modal', function (event) {
                        // At start of upload wizard, Set all elements at start point. progress bar, message etc..
                        if (isIE89) {
                            $("#zone").hide();
                            $("#IE8Zone").show();
                        } else {
                            $("#zone").show();
                            $("#IE8Zone").hide();
                        }
                        progress1 = 0;
                        $(".green-bar").css({ width: progress1 + "%" });
                        $("#spanProgressValue").html(progress1 + "%");
                        $("#pMsgIfChangedFormat").hide();

                        if (isBackPress == 1) {
                            $("#divMaintainHistoricalFuelData").show();
                            $("#divSuccess").show();
                            $("#divError").hide();
                            $("#spanMsg2").hide();
                            isFileUploadSuccess = true;
                        }
                        else {
                            $("#divMaintainHistoricalFuelData").hide();
                            $("#divSuccess").hide();
                            $("#divError").hide();
                            $("#spanMsg2").show();
                            isFileUploadSuccess = false;
                        }
                        isBackPress = 0;
                    });
                    $('#uploadWizardCompleteModal').on('hidden.bs.modal', function () {
                        rebindGrid();
                    });

                }
               
                function OpenMainWizard() {
                    // Error in reading file 
                    $("#zone").show();
                    isBackPress = 0;
                    $("#uploadWizardModal").modal("show");
                    $("#pMsgText").html(pMsgWrongFileDataError);
                    $("#divSuccess").hide();
                    $("#divError").show();
                    $("#spanMsg2").hide();
                    isFileUploadSuccess = false;
                }
                function OpenSuccessWizard(message) {
                    // Error in reading file 
                    isBackPress = 0;
                    $("#pSuccessMsg").html(message);
                    $("#uploadWizardCompleteModal").modal("show");                    
                    isFileUploadSuccess = false;
                }
                function progress() {
                    $("#spanProgressValue").html(progress1 + "%");
                    $(".green-bar").css({ width: progress1 + "%" });
                    if (progress1 > 99) {
                        $(".green-bar").css({ width: progress1 + "%" });
                        $("#spanProgressValue").html(progress1 + "%");
                        $("#spanMsg2").hide();
                        isFileUploadSuccess = true;
                    }
                }
                function zoneIntialization() {

                    // Attach FileDrop to an area ('zone' is an ID but you can also give a DOM node):
                    zone = new FileDrop('zone', options);
                   
                    if (isIE89) {
                        zone = new FileDrop('IE8Zone', options);

                        zone.event('iframeDone', function (xhr) {

                            $("#spanMsg2").show();
                            $("#divSuccess").hide();
                            $("#divError").hide();

                            $("#hdnIframeSuccess").val(xhr.responseText);
                            progress1 = 100;
                            var data = xhr.responseText;
                            data = JSON.parse(data);

                            if (data.ResponseCode == "1") {
                                $("#divSuccess").show();
                                $("#spanMsg2").hide();
                                isFileUploadSuccess = true;
                                $("#lblFilename").html(data.FileName);
                                $("#<%=hdnUploadedFileName.ClientID%>").val(data.FileName);
                            }
                            else {
                                $("#pMsgText").html(data.Error);
                                $("#divError").show();
                                $("#spanMsg2").hide();
                                isFileUploadSuccess = false;
                            }

                        });
                    }

                    $("#btnExit").click(function () {
                        progress1 = 0;
                        $("#spanProgressValue").html(progress1 + "%");
                        $(".green-bar").css({ width: progress1 + "%" });
                    });

                    $("#btnContinueWizard1").click(function () {
                        if (isIE89==true) {
                            var iframe = $("#hdnIframeSuccess").val();
                            if (iframe == "true") {
                                isFileUploadSuccess = true;
                            }
                        }
                        $("#zone").hide();
                        $("#IE8Zone").hide();
                        if (isFileUploadSuccess == true) {
                            $("#divDataLoading").show();
                            previewFuelUploadedFile(OnSuccess);
                        }
                    });

                    $("#btnBackPreviewModal").click(function () {
                        isBackPress = 1;
                        $("#uploadWizardModal2").modal("hide");
                        $("#uploadWizardModal").modal("show");

                    });

                    // Do something when a user chooses or drops a file:
                    zone.event('send', function (files) {
                        $("#spanMsg2").show();
                        $("#divSuccess").hide();
                        $("#divError").hide();
                        // Depending on browser support files (FileList) might contain multiple items.
                        files.each(function (file) {
                            var fileName = eval(file).name;
                            var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
                            if (fileNameExt != "csv") {
                                progress1 = 0;
                                isFileUploadSuccess = false;
                                $("#pMsgText").html(pMsgWrongFileTypeUploadError);
                                $("#divError").show();
                                $("#spanMsg2").hide();
                                $("#divSuccess").hide();
                                active.abort();
                            }

                            file.event('sendXHR', function () {
                                progress1 = 0;
                            });

                            // React on successful AJAX upload:
                            file.event('done', function (xhr) {
                                progress1 = 100;
                                var data = xhr.responseText;
                                data = JSON.parse(data);
                                if (data.ResponseCode == "1") {
                                    $("#divSuccess").children("div.validate-status").children("div.hasGlyph").children("p").empty().append(data.ResponseMessage);
                                    $("#divSuccess").show();
                                    $("#spanMsg2").hide();
                                    isFileUploadSuccess = true;
                                    $("#divMaintainHistoricalFuelData").show();
                                    $("#lblFilename").html(fileName);
                                    $("#<%=hdnUploadedFileName.ClientID%>").val(fileName);
                                }
                                else {
                                    $("#pMsgText").html(pMsgFileErrorDefault);
                                    $("#divError").show();
                                    $("#spanMsg2").hide();
                                    isFileUploadSuccess = false;
                                }
                            });
                            file.event('progress', function (sent, total) {
                                
                                progress1= Math.round(sent / total * 100);
                                progress();
                            });
                            // Send the file:
                            file.sendTo("FuelFileUpload.aspx");
                            file.event('error', function (e, xhr) {
                                
                                $("#pMsgText").html(pMsgFileErrorDefault);
                                $("#divError").show();
                                $("#spanMsg2").hide();
                                isFileUploadSuccess = false;
                                $("#lblFilename").html(lblMsgNoFileSelected);
                            });
                        });
                    });

                    zone.event('fileSetup', function (file) {
                        var fileSize = eval(file).size;
                        if (fileSize < 1) {
                            progress1 = 0;
                            isFileUploadSuccess = false;
                            $("#pMsgText").html(pMsgEmptyFileError);
                            $("#divError").show();
                            $("#divSuccess").hide();
                            $("#spanMsg2").hide();
                            active.abort();
                        } else {
                            progress1 = 2;
                            $(".green-bar").css({ width: progress1 + "%" });
                        }

                    });
                    // A bit of sugar - toggling multiple selection:
                    fd.addEvent(fd.byID('multiple'), 'change', function (e) {
                        zone.multiple(e.currentTarget || e.srcElement.checked);
                    });
                }

                function saveCSVData() {
                    var isMaintainHistoricalFuelData = $("[id$='IsMaintainHistoricalFuelData']").is(":checked");
                    RequestStart(null, null);
                    $.ajax({
                        async: true,
                        url: "/Views/Settings/Logistics/FuelFileUpload.aspx/SaveCSVData",
                        type: "POST",
                        data: JSON.stringify({"isMaintainHistoricalFuelData":isMaintainHistoricalFuelData}),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (response) {
                            response = response.d;
                            if (response.StatusCode == 200) {
                                OpenSuccessWizard(response.Result.Message);
                            } else {
                                OpenMainWizard();
                            }
                            ResponseEnd();
                        }
                    });
                }
                
                function rebindGrid() {
                    var masterTable = $find("<%= dgFuelVendor.ClientID %>").get_masterTableView();
                    masterTable.rebind();
                }

            </script>

        </telerik:RadCodeBlock>
    </div>
</asp:Content>
