﻿<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head id="Head1" runat="server">
<title>FBO</title>
<link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
<link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
<script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
<script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="/Scripts/Common.js"></script>

<script type="text/javascript">
    var selectedRowData = null;
    var jqgridTableId = '#gridFBOPopup';
    var deleteFBOId = null;
    var deletefboName = '';
    var selectedRowMultipleFBO = "";
    var isopenlookup = false;
    var ismultiSelect = false;
    var fboCD = [];
    $(document).ready(function () {
        ismultiSelect = getQuerystring("IsUIReports", "") == "" ? false : true;
        $.extend(jQuery.jgrid.defaults, {
            prmNames: {
                page: "page", rows: "size", order: "dir", sort: "sort"
            }
        });


        $("#chkDisplayInactive").change(function () {
            $(jqgridTableId).trigger('reloadGrid');
        });

        $("#chkDisplayChoice").change(function () {
            $(jqgridTableId).trigger('reloadGrid');
        });

        var selectedRowData = getQuerystring("FBOCD", "");
        selectedRowMultipleFBO = $.trim(decodeURI(getQuerystring("FBOCD", "")));
        jQuery("#hdnselectedfccd").val(selectedRowMultipleFBO);
        if (selectedRowData != "") {
            isopenlookup = true;
        }

        if (ismultiSelect) {
            if (selectedRowData.length < jQuery("#hdnselectedfccd").val().length) {
                selectedRowData = jQuery("#hdnselectedfccd").value;
            }
            fboID = selectedRowData.split(',');
        }


        $("#btnSubmit").click(function () {
            var grid = $(jqgridTableId);
            var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
            if ((selr != null && ismultiSelect == false) || (ismultiSelect == true && selectedRowMultipleFBO.length > 0)) {
                var rowData = $(jqgridTableId).getRowData(selr);
                returnToParent(rowData, 0);
            }
            else {
                showMessageBox('Please select a FBO.', popupTitle);
            }
            return false;
        });

        $("#recordAdd").click(function () {
            var widthDoc = $(document).width();
            popupwindow("/Views/Settings/Logistics/FBOCatalog.aspx?IsPopup=Add&AirportID=" + getUrlVars()["IcaoID"], popupTitle, 1030, 1254, jqgridTableId);
            return false;
        });

        $("#recordEdit").click(function () {
            var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
            var rowData = $(jqgridTableId).getRowData(selr);
            var fboid = rowData['FBOID'];
            var widthDoc = $(document).width();
            if (fboid != undefined && fboid != null && fboid != '' && fboid != 0) {
                popupwindow("/Views/Settings/Logistics/FBOCatalog.aspx?IsPopup=&AirportID=" + getUrlVars()["IcaoID"] + "&FBOId=" + fboid, popupTitle, 1030, 1254, jqgridTableId);
            }
            else {
                showMessageBox('Please select a FBO.', popupTitle);
            }
            return false;
        });

        $("#recordDelete").click(function () {
            var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
            var rowData = $(jqgridTableId).getRowData(selr);

            deleteFBOId = rowData['FBOID'];
            deletefboName = rowData['FBOVendor'];
            var widthDoc = $(document).width();
            if (deleteFBOId != undefined && deleteFBOId != null && deleteFBOId != '' && deleteFBOId != 0) {
                showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
            }
            else {
                showMessageBox('Please select a FBO.', popupTitle);
            }
            return false;
        });

        function confirmCallBackFn(arg) {
            if (arg) {
                if (deleteFBOId != undefined && deleteFBOId != null && deleteFBOId != '' && deleteFBOId != 0) {
                    $.ajax({
                        type: 'GET',
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=FBO&FBOId=' + deleteFBOId,
                        contentType: 'text/html',
                        success: function (data, textStatus, xhr) {
                            var message = "This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.";
                            DeleteApiResponse(jqgridTableId, data, message, popupTitle);
                        },
                        complete: function (xhr, textStatus) {
                            //Check for any statuses here as well
                            $(jqgridTableId).trigger('reloadGrid');
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            //Check for any errors
                            var content = jqXHR.responseText;                        
                            if (content.indexOf('404'))
                                showMessageBox("This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.", popupTitle);
                        }
                    });
                }
            }
        }


        jQuery(jqgridTableId).jqGrid({
            url: '/Views/Utilities/ApiCallerWithFilter.aspx',
            mtype: 'GET',
            datatype: "json",
            ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
            serializeGridData: function (postData) {
                if (postData._search == undefined || postData._search == false) {
                    if (postData.filters === undefined) postData.filters = null;
                }
                postData.apiType = 'fss';
                postData.method = 'FBOPopup';
                postData.airportId = getUrlVars()["IcaoID"];
                postData.showInactive = $("#chkDisplayInactive").is(':checked') ? true : false;
                postData.showChoiceOnly = $("#chkDisplayChoice").is(':checked') ? true : false;
                postData.fbocd = function () { if (ismultiSelect && selectedRowData.length > 0) { return fboCD[0]; } else { return selectedRowData; } };
                postData.markSelectedRecord = isopenlookup;
                isopenlookup = false;
                return postData;
            },
            height: 390,
            width: 1020,
            viewrecords: true,
            rowNum: $("#rowNum").val(),
            multiselect: ismultiSelect,
            autowidth: false,
            shrinkToFit: false,
            forceFit: false,
            pager: "#pg_gridPager",
            colNames: ['FBOID', 'Code', 'Name', 'Choice', 'AirportID', 'Phone1', 'Phone2', 'Frequency', 'Fax', 'Contact', 'Fuel Brand', 'Posted Price', 'Fuel Quantity', 'Payment Type',
                       'Remarks', 'Last Fuel Date', 'Last Fuel Price', 'Neg Price', 'Address1', 'Address2', 'City', 'State', 'Postal Code', 'Country', 'Filter', 'UWAID', 'HoursOfOperation', 'IsUVAirPNR', 'IsCrewCar', 'Active', 'UNICOM', 'ARINC', 'ContactEmail'],
            colModel: [
                { name: 'FBOID', index: 'FBOID', key: true, hidden: true },
                { name: 'FBOCD', index: 'FBOCD', width: 60 },
                { name: 'FBOVendor', index: 'FBOVendor', width: 250 },
				{ name: 'IsChoice', index: 'IsChoice', width: 50, formatter: "checkbox", formatoptions: { disabled: true }, search: false, align: 'center' },
                { name: 'AirportId', index: 'AirportID', key: true, hidden: true },
                { name: 'PhoneNUM1', index: 'PhoneNUM1', width: 85 },
                { name: 'PhoneNUM2', index: 'PhoneNUM2', width: 85 },
                { name: 'Frequency', index: 'Frequency', width: 70 },
				{ name: 'FaxNum', index: 'FaxNum', width: 85 },
				{ name: 'Contact', index: 'Contact', width: 100 },
				{ name: 'FuelBrand', index: 'FuelBrand', width: 70 },
				{ name: 'PostedPrice', index: 'PostedPrice', width: 70 },
				{ name: 'FuelQty', index: 'FuelQty', width: 75 },
                { name: 'PaymentType', index: 'PaymentType', width: 80 },
                { name: 'Remarks', index: 'Remarks', width: 100 },
				{ name: 'LastFuelDT', index: 'LastFuelDT', key: false, hidden: true },
				{ name: 'LastFuelPrice', index: 'LastFuelPrice', width: 55 },
                { name: 'NegotiatedFuelPrice', index: 'NegotiatedFuelPrice', width: 55 },
                { name: 'Addr1', index: 'Addr1', width: 100 },
				{ name: 'Addr2', index: 'Addr2', width: 100 },
				{ name: 'CityName', index: 'CityName', width: 80 },
                { name: 'StateName', index: 'StateName', width: 80 },
                { name: 'PostalZipCD', index: 'PostalZipCD', width: 70 },
                { name: 'CountryCD', index: 'CountryCD', width: 80 },
				{ name: 'Filter', index: 'Filter', width: 100 },
				{ name: 'UWAID', index: 'UWAID', key: true, hidden: true },
                { name: 'HoursOfOperation', index: 'HoursOfOperation', key: false, hidden: true },
				{ name: 'IsUVAirPNR', index: 'IsUVAirPNR', key: false, hidden: true },
                { name: 'IsCrewCar', index: 'IsCrewCar', key: false, hidden: true },
                {
                    name: 'IsInActive', index: 'IsInActive', search: false, align: 'center', width: 40, formatter: function (cellvalue, options, rowObject) {
                        var check = cellvalue == false ? "checked='checked'" : "";
                        return "<input type='checkbox' style='width:100%' disabled='disabled' "+ check+" readonly/>";
                    }
                },
                { name: 'UNICOM', index: 'UNICOM', key: false, hidden: true },
                { name: 'ARINC', index: 'ARINC', key: false, hidden: true },
                { name: 'ContactEmail', index: 'ContactEmail', key: false, hidden: true }
            ],
            ondblClickRow: function (rowId) {
                var rowData = jQuery(this).getRowData(rowId);
                returnToParent(rowData, 1);
            },
            loadComplete: function (data) {
                var airportId = getUrlVars()["IcaoID"];
                $.ajax({
                    type: 'GET',
                    url: '/Views/Utilities/GETApi.aspx?apiType=FSS&method=AirportByAirportId&airportId=' + airportId,
                    contentType: 'text/html',
                    success: function (rowData, textStatus, xhr) {
                        rowData = $.parseJSON($('<div/>').html(rowData).text());
                        if (rowData != undefined && rowData != null) {
                            var icao = rowData.IcaoID;
                            var cityName = rowData.CityName;
                            var stateName = rowData.StateName;
                            var countryName = rowData.CountryName;
                            var airportName = rowData.AirportName;
                            if (icao != undefined && icao != null)
                                $("#lbICAO").text(icao);
                            if (cityName != undefined && cityName != null)
                                $("#lbCity").text(cityName);
                            if (stateName != undefined && stateName != null)
                                $("#lbState").text(stateName);
                            if (countryName != undefined && countryName != null)
                                $("#lbCountry").text(countryName);
                            if (airportName != undefined && airportName != null)
                                $("#lbAirport").text(airportName);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //Check for any errors
                    }
                });
                JqgridCreateSelectAllOption(ismultiSelect, jqgridTableId);
            },
            onSelectRow: function (id, status) {
                var rowData = $(this).getRowData(id);
                selectedRowMultipleFBO = JqgridOnSelectRow(ismultiSelect, status, selectedRowMultipleFBO, rowData.FBOCD);
            },
            onSelectAll: function (id, status) {
                selectedRowMultipleFBO = JqgridSelectAll(selectedRowMultipleFBO, jqgridTableId, id, status, 'FBOCD');
                jQuery("#hdnselectedfccd").val(selectedRowMultipleFBO);
            },
            afterInsertRow: function (rowid, rowObject) {
                JqgridSelectAfterInsertRow(ismultiSelect, selectedRowMultipleFBO, rowid, rowObject.FBOCD, jqgridTableId);
            }
        });
        $("#pagesizebox").insertBefore('.ui-paging-info');
        $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
    });

    function reloadFBOPopup() {
        $(jqgridTableId).jqGrid().trigger('reloadGrid', [{ page: 1 }]);
    }

 </script>
 <script type="text/javascript">
     function returnToParent(rowData, one) {
         selectedRowMultipleFBO = jQuery.trim(selectedRowMultipleFBO);
         if (selectedRowMultipleFBO.lastIndexOf(",") == selectedRowMultipleFBO.length - 1)
             selectedRowMultipleFBO = selectedRowMultipleFBO.substr(0, selectedRowMultipleFBO.length - 1);


         var oArg = new Object();
         if (one == 0) {
             oArg.FBOCD = selectedRowMultipleFBO;
         }
         else {
             oArg.FBOCD = rowData["FBOCD"];
         }

         //create the argument that will be returned to the parent page

         oArg.FBOVendor = rowData["FBOVendor"];
         oArg.PhoneNUM1 = rowData["PhoneNUM1"];
         oArg.PhoneNUM2 = rowData["PhoneNUM2"];
         oArg.FaxNum = rowData["FaxNum"];
         oArg.CityName = rowData["CityName"];
         oArg.StateName = rowData["StateName"];
         oArg.PostalZipCD = rowData["PostalZipCD"];
         oArg.AirportID = rowData["AirportID"];
         oArg.FBOID = rowData["FBOID"];
         oArg.UNICOM = rowData["UNICOM"];
         oArg.ARINC = rowData["ARINC"];
         oArg.ContactEmail = rowData["ContactEmail"];

         var oWnd = GetRadWindow();
         if (oArg) {
             oWnd.close(oArg);
         }
     }
 </script>
<style type="text/css">
body{
    width: 1010px;
    height: 500px;
}
</style>
</head>
<body>
    <form id="form1">
        <input type="hidden" id="hdnselectedfccd" value=""/>
	    <div class="jqgrid">                         
            <div>
                <table class="box1">
                    <tr>
                        <td class="tdLabel30">
                            ICAO:
                        </td>
                        <td class="tdLabel40">
                            <span id="lbICAO"></span>
                        </td>
                        <td class="tdLabel40">
                            City:
                        </td>
                        <td class="tdLabel100">
                            <span id="lbCity"></span>
                        </td>
                        <td class="tdLabel40">
                            State:
                        </td>
                        <td class="tdLabel50">
                            <span id="lbState"></span>
                        </td>
                        <td class="tdLabel30">
                            Country:
                        </td>
                        <td class="tdLabel70">
                            <span id="lbCountry"></span>
                        </td>
                        <td class="tdLabel40">
                            Airport:
                        </td>
                        <td class="tdLabel150">
                            <span id="lbAirport"></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                             <input type="checkbox" id="chkDisplayInactive" name="chkDisplayInactive"/>Display Inactive
                        </td>
                        <td align="left" colspan="8">
                            <input type="checkbox" id="chkDisplayChoice" name="chkDisplayChoice" value="Display Choice Only" />Display Choice Only
                        </td>                       
                    </tr>
                    <tr>
                        <td colspan="10">
                            <table id="gridFBOPopup" class="table table-striped table-hover table-bordered"></table>                    
                        </td>
                    </tr>           
                    <tr>
                        <td colspan="10">
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager"></div>
                                <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#" ></a>
                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                                <div id="pagesizebox">
                                    <span>Page Size:</span>
                                    <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                    <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridTableId, $('#rowNum')); return false;" />
                                </div>
                            </div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK" type="button" />                                                                       
                            </div>
                        </td>
                    </tr>              
                </table>  
            </div>
        </div>
    </form>
</body>
</html>