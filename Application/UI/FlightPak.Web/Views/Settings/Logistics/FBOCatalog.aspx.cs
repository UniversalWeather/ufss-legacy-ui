﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using Telerik.Web.UI;
using System.Drawing;
using System.Collections;
using System.Text;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Logistics
{
    public partial class FBOCatalog : BaseSecuredPage
    {
        #region constants
        private const string IsChoice = "IsChoice";
        private const string IsInActive = "IsInActive";
        private const string IsUWAAirPartner = "IsUWAAirPartner";
        private const string IsCrewCar = "IsCrewCars";
        private bool IsCheckLookUp = true;
        public string DateFormat;
        private ExceptionManager exManager;
        private bool FboPageNavigated = false;
        private bool _selectLastModified = false;
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        {
                            // Grid Control could be ajaxified when the page is initially loaded.
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgFBO, dgFBO, RadAjaxLoadingPanel1);
                            // Store the clientID of the grid to reference it later on the client
                            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgFBO.ClientID));
                            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                            // Set Logged-in User Name
                            FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                            if (identity != null && identity.Identity._fpSettings._ApplicationDateFormat != null)
                            {
                                DateFormat = identity.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                            }
                            else
                            {
                                DateFormat = "MM/dd/yyyy";
                            }
                            ((RadDatePicker)ucDatePicker.FindControl("RadDatePicker1")).DateInput.DateFormat = DateFormat;
                            if (!IsPostBack)
                            {
                                CheckAutorization(Permission.Database.ViewFBO);
                                if (Request.QueryString["FBOId"] != null)
                                {
                                    Session["SelectedFBOID"] = Server.UrlDecode(Request.QueryString["FBOId"].ToString());
                                }
                                if (Request.QueryString["IcaoID"] != null)
                                {
                                    tbIcao.Text = Server.UrlDecode(Request.QueryString["IcaoID"].ToString());
                                }
                                if (Request.QueryString["CityName"] != null)
                                {
                                    tbFBOCity.Text = Server.UrlDecode(Request.QueryString["CityName"].ToString());
                                }
                                if (Request.QueryString["StateName"] != null)
                                {
                                    tbFBOState.Text = Server.UrlDecode(Request.QueryString["StateName"].ToString());
                                }
                                if (Request.QueryString["CountryName"] != null)
                                {
                                    tbFBOCountry.Text = Server.UrlDecode(Request.QueryString["CountryName"].ToString());
                                }
                                if (Request.QueryString["AirportName"] != null)
                                {
                                    tbAirport.Text = Server.UrlDecode(Request.QueryString["AirportName"].ToString());
                                }
                                if (Request.QueryString["AirportID"] != null)
                                {
                                    hdnAirportId.Value = Server.UrlDecode(Request.QueryString["AirportID"].ToString());
                                }
                                // Method to display first record in read only format
                                DefaultSelection(true);
                            }
                            if (IsPopUp)
                            {
                                dgFBO.AllowPaging = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }
        /// <summary>
        /// To Display first record as default
        /// </summary>        
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    // To select the first record as default
                    DateFormat = ((RadDatePicker)ucDatePicker.FindControl("RadDatePicker1")).DateInput.DateFormat;
                    if (!IsPopUp)
                    {
                        if (BindDataSwitch)
                        {
                            dgFBO.Rebind();
                        }
                        if (dgFBO.MasterTableView.Items.Count > 0)
                        {
                            //if (!IsPostBack)
                            //{
                            //    Session["SelectedFBOID"] = null;
                            //}
                            if (Session["SelectedFBOID"] == null)
                            {
                                dgFBO.SelectedIndexes.Add(0);
                                Session["SelectedFBOID"] = dgFBO.Items[0].GetDataKeyValue("FBOID").ToString();
                            }

                            if (dgFBO.SelectedIndexes.Count == 0)
                                dgFBO.SelectedIndexes.Add(0);

                            ReadOnlyForm();
                        }
                        else
                        {
                            ClearForm();
                            EnableForm(false);
                            //chkDisplayInctive.Enabled = false;
                            //chkDisplayChoiceOnly.Enabled = false;
                            //chkDisplayUVOnly.Enabled = false;
                        }
                        GridEnable(true, true, true, true);
                    }
                    if (IsViewOnly)
                    {
                        if (BindDataSwitch)
                        {
                            dgFBO.Rebind();
                        }
                        if (dgFBO.MasterTableView.Items.Count > 0)
                        {
                            dgFBO.SelectedIndexes.Add(0);
                            Session["SelectedFBOID"] = dgFBO.Items[0].GetDataKeyValue("FBOID").ToString();
                            ReadOnlyForm();
                        }
                        else
                        {
                            ClearForm();
                            EnableForm(false);
                            //chkDisplayInctive.Enabled = false;
                            //chkDisplayChoiceOnly.Enabled = false;
                            //chkDisplayUVOnly.Enabled = false;
                        }
                        GridEnable(true, true, true, true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

            }
        }
        /// <summary>
        /// Prerender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFBO_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (FboPageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgFBO, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }
        /// <summary>
        /// To highlight the selected item 
        /// </summary>        
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedFBOID"] != null)
                    {
                        //GridDataItem items = (GridDataItem)Session["SelectedItem"];
                        //string code = items.GetDataKeyValue("DelayTypeCD").ToString();
                        string ID = Convert.ToString(Session["SelectedFBOID"]).Trim();
                        foreach (GridDataItem item in dgFBO.MasterTableView.Items)
                        {
                            if (item["FBOID"].Text.Trim() == ID)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgFBO;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }
        /// <summary>
        /// Datagrid Item Created for Fuel Locator Insert and edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFBO_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnInitEdit = ((e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInitEdit, DivExternalForm, Page.Master.FindControl("RadAjaxLoadingPanel1") as RadAjaxLoadingPanel);
                            // To find the insert button in the grid
                            LinkButton lbtnInitInsert = ((e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInitInsert, DivExternalForm, Page.FindControl("RadAjaxLoadingPanel1") as RadAjaxLoadingPanel);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }
        /// <summary>
        /// To display UWA record in blue color in filter column grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFBO_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            string Color = System.Configuration.ConfigurationManager.AppSettings["UWAColor"];
                            GridDataItem DataItem = e.Item as GridDataItem;
                            GridColumn Column = dgFBO.MasterTableView.GetColumn("chkUWAID");
                            string UwaValue = DataItem["chkUWAID"].Text.Trim();
                            GridDataItem Item = (GridDataItem)e.Item;
                            // e.Item.Cells[6].Text = "UWA";
                            TableCell cell = (TableCell)Item["chkUWAID"];
                            if (UwaValue.ToUpper().Trim() == "UWA")
                            {
                                System.Drawing.ColorConverter ColourConvert = new ColorConverter();
                                cell.ForeColor = (System.Drawing.Color)ColourConvert.ConvertFromString(Color);
                                cell.Font.Bold = true;
                                // cell.Text = "UWA";
                            }
                            //else
                            //{
                            //   / cell.Text = "CUSTOM";
                            //}
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }
        /// <summary>
        /// Bind FBO Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFBO_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient FBOService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (IsViewOnly && IsPopUp && Session["SelectedAirportForFBOID"] != null)
                            {
                                hdnAirportId.Value = Session["SelectedAirportForFBOID"].ToString();
                            }
                            if (!string.IsNullOrEmpty(hdnAirportId.Value))
                            {
                                //if (IsPopUp && Session["SelectedAirportForFBOID"] != null)
                                //{
                                //    hdnAirportId.Value = Session["SelectedAirportForFBOID"].ToString();
                                //}
                                var FBOValue = FBOService.GetAllFBOByAirportID(Convert.ToInt64(hdnAirportId.Value));
                                Session["FBOCD"] = FBOValue.EntityList.ToList();
                                if (FBOValue.ReturnFlag == true)
                                {
                                    if (!IsPopUp)
                                    {
                                        if (chkDisplayInctive.Checked == true)
                                        {
                                            FBOValue.EntityList = FBOValue.EntityList.Where(x => x.IsDeleted.ToString().ToUpper().Trim().Equals("FALSE")).ToList();
                                        }
                                        else
                                        {
                                            FBOValue.EntityList = FBOValue.EntityList.Where(x => x.IsDeleted.ToString().ToUpper().Trim().Equals("FALSE") && (x.IsInActive.ToString().ToUpper().Trim() == "FALSE")).ToList();
                                        }
                                        if (chkDisplayChoiceOnly.Checked == true)
                                        {
                                            FBOValue.EntityList = FBOValue.EntityList.Where(x => x.IsChoice.ToString().ToUpper().Trim().Equals("TRUE")).ToList();
                                        }
                                        if (chkDisplayUVOnly.Checked == true)
                                        {
                                            FBOValue.EntityList = FBOValue.EntityList.Where(x => x.IsUWAAirPartner.ToString().ToUpper().Trim().Equals("TRUE")).ToList();
                                        }
                                    }
                                    else
                                    {
                                        FBOValue.EntityList = FBOValue.EntityList.ToList();
                                    }
                                    dgFBO.DataSource = FBOValue.EntityList.GroupBy(t => t.FBOVendor).Select(y => y.MinBy(ya => ya.FBOID)).ToList();
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }
        /// <summary>
        /// Item Command for FBO Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFBO_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                chkDisplayInctive.Enabled = false;
                                chkDisplayChoiceOnly.Enabled = false;
                                chkDisplayUVOnly.Enabled = false;
                                if (Session["SelectedFBOID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.FBO, Convert.ToInt64(Session["SelectedFBOID"].ToString().Trim()));

                                        Session["IsFboEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.FBO);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FBO);
                                            SelectItem();
                                            return;
                                        }
                                        DisplayEditForm();
                                        GridEnable(false, true, false, false);
                                        SelectItem();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgFBO.SelectedIndexes.Clear();
                                chkUVair.Checked = true;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbName);
                                // tbName.Focus();
                                DisplayInsertForm();
                                GridEnable(true, false, false, false);
                                chkDisplayInctive.Enabled = false;
                                chkDisplayChoiceOnly.Enabled = false;
                                chkDisplayUVOnly.Enabled = false;
                                break;
                            case "UpdateEdited":
                                dgFBO_UpdateCommand(sender, e);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    //column.CurrentFilterValue = string.Empty;
                                    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }
        /// <summary>
        /// To change the page index on paging        
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFBO_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgFBO.ClientSettings.Scrolling.ScrollTop = "0";
                        FboPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }
        /// <summary>
        /// Update Command for FBO Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFBO_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        CheckCountryExist();
                        CheckPaymenttype();
                        CheckExchangeRateExist();
                        if (IsCheckLookUp)
                        {
                            if (Session["SelectedFBOID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objFboService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var FBOValue = objFboService.UpdateFBO(GetItems());
                                    if (FBOValue.ReturnFlag == true)
                                    {
                                        ShowSuccessMessage();
                                        _selectLastModified = true;

                                        ///////Update Method UnLock
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.FBO, Convert.ToInt64(Session["SelectedFBOID"].ToString().Trim()));
                                            e.Item.OwnerTableView.Rebind();
                                            e.Item.Selected = true;
                                            //GridEnable(true, true, true, true);
                                            DefaultSelection(true);
                                            //chkDisplayInctive.Enabled = true;
                                            //chkDisplayChoiceOnly.Enabled = true;
                                            //chkDisplayUVOnly.Enabled = true;
                                            //GridEnable(true, true, true);
                                            //EnableForm(false);
                                            //tbFBOCode.ReadOnly = true;                                            
                                        }
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(FBOValue.ErrorMessage, ModuleNameConstants.Database.FBO);
                                    }
                                }
                            }
                        }
                        if (IsPopUp && (!IsViewOnly))
                        {
                            //Clear session & close browser
                            //Session["SelectedFBOID"] = null;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }
        /// <summary>
        /// Update Command for FBO Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFBO_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        CheckCountryExist();
                        CheckPaymenttype();
                        CheckExchangeRateExist();
                        if (IsCheckLookUp)
                        {
                            if (IsFBOExists())
                            {
                                
                                string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('FBO already exists', 360, 50, 'FBO');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                                
                            }
                            else
                            {
                                using (MasterCatalogServiceClient FBOService = new MasterCatalogServiceClient())
                                {
                                    var FBOValue = FBOService.AddFBO(GetItems());
                                    if (FBOValue.ReturnFlag == true)
                                    {
                                        ShowSuccessMessage();

                                        dgFBO.Rebind();
                                        DefaultSelection(false);
                                        //GridEnable(true, true, true);
                                        //EnableForm(false);
                                        //tbFBOCode.ReadOnly = true;
                                        //chkDisplayInctive.Enabled = true;
                                        //chkDisplayChoiceOnly.Enabled = true;
                                        //chkDisplayUVOnly.Enabled = true;
                                        _selectLastModified = true;
                                        if (IsPopUp && (!IsViewOnly))
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                                        }
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(FBOValue.ErrorMessage, ModuleNameConstants.Database.FBO);
                                    }
                                }
                            }
                        }
                      
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }
        /// <summary>
        /// Method to check if FBO Code already exists
        /// </summary>
        /// <returns></returns>
        private bool IsFBOExists()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    List<FlightPakMasterService.GetAllFBOByAirportID> lstFBO = new List<FlightPakMasterService.GetAllFBOByAirportID>();
                    if (Session["FBOCD"] != null)
                    {
                        lstFBO = (List<FlightPakMasterService.GetAllFBOByAirportID>)Session["FBOCD"];
                    }

                    var searchFBO = lstFBO.Where(x => x.FBOVendor.ToString().ToUpper().Trim().Equals(tbName.Text.ToString().ToUpper().Trim())).ToList();
                    if(searchFBO.Count>0)
                    {
                        ReturnVal = true;
                    }
                   
                }, FlightPak.Common.Constants.Policy.UILayer);

                return ReturnVal;
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgFBO_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                string Code = string.Empty;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedFBOID"] != null)
                        {
                            //GridDataItem Item = (GridDataItem)Session["SelectedItem"];
                            Code = Convert.ToString(Session["SelectedFBOID"]).Trim();
                            string AirportID = "", IsUWAMaintFlag = "";
                            foreach (GridDataItem Item in dgFBO.MasterTableView.Items)
                            {
                                if (Item["FBOID"].Text.Trim() == Code)
                                {
                                    AirportID = Item["AirportID"].Text.Trim();
                                    IsUWAMaintFlag = Item.GetDataKeyValue("IsUWAMaintFlag").ToString().Trim();
                                    break;
                                }
                            }
                            using (FlightPakMasterService.MasterCatalogServiceClient FBOService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.FBO FBOData = new FlightPakMasterService.FBO();
                                FBOData.AirportID = Convert.ToInt64(AirportID);
                                FBOData.FBOID = Convert.ToInt64(Code);
                                FBOData.IsDeleted = true;
                                if (!String.IsNullOrEmpty(IsUWAMaintFlag))
                                {
                                    FBOData.IsUWAMaintFlag = Convert.ToBoolean(IsUWAMaintFlag);
                                }
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.FBO, Convert.ToInt64(Code));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.FBO);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.FBO);
                                        return;
                                    }
                                }
                                FBOService.DeleteFBO(FBOData);
                                dgFBO.Rebind();
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                DefaultSelection(true);
                                //chkDisplayInctive.Enabled = true;
                                //chkDisplayChoiceOnly.Enabled = true;
                                //chkDisplayUVOnly.Enabled = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.FBO, Convert.ToInt64(Code));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgFBO_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            //GridDataItem Item = dgFBO.SelectedItems[0] as GridDataItem;
                            //Session["SelectedFBOID"] = Item["FBOID"].Text;
                            if (btnSaveChanges.Visible == false)
                            {
                                hdnSave.Value = "";
                                GridDataItem item = dgFBO.SelectedItems[0] as GridDataItem;
                                Session["SelectedFBOID"] = item["FBOID"].Text;
                                ReadOnlyForm();
                                GridEnable(true, true, true, true);
                            }

                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                    }
                }
            }
        }
        /// <summary>
        /// To Display all the fields
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    tbFBOCode.ReadOnly = true;
                    tbFBOCode.BackColor = System.Drawing.Color.White;
                    pnlExternalForm.Visible = true;
                    chkChoice.Checked = false;
                    chkInactive.Checked = false;
                    tbFBOCode.Text = string.Empty;
                    tbName.Text = string.Empty;
                    tbAddr1.Text = string.Empty;
                    tbAddr2.Text = string.Empty;
                    tbCity.Text = string.Empty;
                    tbState.Text = string.Empty;
                    tbPostal.Text = string.Empty;
                    tbCountry.Text = string.Empty;
                    tbPhone1.Text = string.Empty;
                    tbPhone2.Text = string.Empty;
                    tbTollFreePhone.Text = string.Empty;
                    tbFax.Text = string.Empty;
                    tbWebsite.Text = string.Empty;
                    tbEmail.Text = string.Empty;
                    tbContact.Text = string.Empty;
                    tbOpHrs.Text = string.Empty;
                    chkCrewCar.Checked = false;
                    chkUVair.Checked = false;
                    tbAirToGnd.Text = string.Empty;
                    tbSITA.Text = string.Empty;
                    tbUnicom.Text = string.Empty;
                    tbARINC.Text = string.Empty;
                    tbFuelBrand.Text = string.Empty;
                    tbNegPrice.Text = string.Empty;
                    tbFuelQuantity.Text = "000000000000.00";
                    tbNegPrice.Text = "00.0000";
                    tbPaymentType.Text = string.Empty;
                    tbLastFuelPrice.Text = "00000.0000";
                    tbPostedPrice.Text = "00000.0000";
                    hdnSave.Value = "Save";
                    ClearForm();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);

            }
        }
        /// <summary>
        /// To Clear the form
        /// </summary>        
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    chkChoice.Checked = false;
                    chkInactive.Checked = false;
                    tbName.Text = string.Empty;
                    tbAddr1.Text = string.Empty;
                    tbAddr2.Text = string.Empty;
                    tbCity.Text = string.Empty;
                    tbState.Text = string.Empty;
                    tbPostal.Text = string.Empty;
                    tbCountry.Text = string.Empty;
                    tbPhone1.Text = string.Empty;
                    tbPhone2.Text = string.Empty;
                    tbTollFreePhone.Text = string.Empty;
                    tbFax.Text = string.Empty;
                    tbWebsite.Text = string.Empty;
                    tbEmail.Text = string.Empty;
                    tbContact.Text = string.Empty;
                    tbOpHrs.Text = string.Empty;
                    chkCrewCar.Checked = false;
                    chkUVair.Checked = false;
                    tbAirToGnd.Text = string.Empty;
                    tbSITA.Text = string.Empty;
                    tbUnicom.Text = string.Empty;
                    tbARINC.Text = string.Empty;
                    tbFuelBrand.Text = string.Empty;
                    tbNegPrice.Text = string.Empty;
                    tbFuelQuantity.Text = "000000000000.00";
                    tbNegPrice.Text = "00.0000";
                    tbPaymentType.Text = string.Empty;
                    tbLastFuelPrice.Text = "00000.0000";
                    TextBox tbLastPurchase = (TextBox)ucLastPurchase.FindControl("tbDate");
                    tbLastPurchase.Text = string.Empty;
                    tbPostedPrice.Text = "00000.0000";
                    hdnCountryId.Value = string.Empty;
                    tbbusinessPhone.Text = string.Empty;
                    tbContactEmail.Text = string.Empty;
                    tbContactMobile.Text = string.Empty;
                    tbAddress3.Text = string.Empty;
                    tbNotes.Text = string.Empty;
                    hdnIsUWa.Value = string.Empty;
                    hdnExchangeRateID.Value = "";
                    tbExchangeRate.Text = "";
                    tbCustomField1.Text = "";
                    tbCustomField2.Text = "";
                    tbNegotiateTerm.Text = "";
                    tbSundayWorkHours.Text = "";
                    tbMondayWorkHours.Text = "";
                    tbTuesdayWorkHours.Text = "";
                    tbWednesdayWorkHours.Text = "";
                    tbThursdayWorkHours.Text = "";
                    tbFridayWorkHours.Text = "";
                    tbSaturdayWorkHours.Text = "";

                }, FlightPak.Common.Constants.Policy.UILayer);

            }
        }
        /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="Enable"></param>
        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbIcao.Enabled = Enable;
                    tbFBOCode.Enabled = false;
                    chkChoice.Enabled = Enable;
                    chkInactive.Enabled = Enable;
                    tbName.Enabled = Enable;
                    tbAddr1.Enabled = Enable;
                    tbAddr2.Enabled = Enable;
                    tbCity.Enabled = Enable;
                    tbState.Enabled = Enable;
                    tbPostal.Enabled = Enable;
                    tbCountry.Enabled = Enable;
                    tbPhone1.Enabled = Enable;
                    tbPhone2.Enabled = Enable;
                    tbTollFreePhone.Enabled = Enable;
                    tbFax.Enabled = Enable;
                    tbWebsite.Enabled = Enable;
                    tbEmail.Enabled = Enable;
                    tbContact.Enabled = Enable;
                    tbOpHrs.Enabled = Enable;
                    chkCrewCar.Enabled = Enable;
                    chkUVair.Enabled = Enable;
                    tbAirToGnd.Enabled = Enable;
                    tbSITA.Enabled = Enable;
                    tbUnicom.Enabled = Enable;
                    tbARINC.Enabled = Enable;
                    tbFuelBrand.Enabled = Enable;
                    tbNegPrice.Enabled = Enable;
                    tbFuelQuantity.Enabled = Enable;
                    tbPaymentType.Enabled = Enable;
                    tbLastFuelPrice.Enabled = Enable;
                    TextBox tbLastPurchase = (TextBox)ucLastPurchase.FindControl("tbDate");
                    tbLastPurchase.Enabled = Enable;
                    tbPostedPrice.Enabled = Enable;
                    btnCancel.Visible = Enable;
                    btnSaveChanges.Visible = Enable;
                    btnSaveChangesTop.Visible = Enable;
                    btnCancelTop.Visible = Enable;
                    btnCountry.Enabled = Enable;
                    btnPaymentType.Enabled = Enable;
                    tbbusinessPhone.Enabled = Enable;
                    tbContactEmail.Enabled = Enable;
                    tbContactMobile.Enabled = Enable;
                    tbAddress3.Enabled = Enable;
                    tbNotes.Enabled = Enable;
                    btnSearchExchange.Enabled = Enable;
                    //hdnExchangeRateID.Enabled = Enable;
                    tbExchangeRate.Enabled = Enable;
                    tbCustomField1.Enabled = Enable;
                    tbCustomField2.Enabled = Enable;
                    tbNegotiateTerm.Enabled = Enable;
                    tbSundayWorkHours.Enabled = Enable;
                    tbMondayWorkHours.Enabled = Enable;
                    tbTuesdayWorkHours.Enabled = Enable;
                    tbWednesdayWorkHours.Enabled = Enable;
                    tbThursdayWorkHours.Enabled = Enable;
                    tbFridayWorkHours.Enabled = Enable;
                    tbSaturdayWorkHours.Enabled = Enable;
                    //During Readonly mode the map should be disabled.
                    //if ((!string.IsNullOrEmpty(hdnSave.Value)) && (Enable == true))
                    //{
                    //    lnkMap.Enabled = true;
                    //}
                    //else
                    //{
                    //    lnkMap.Enabled = false;
                    //}
                    if ((hdnSave.Value == "Update") && (Enable == true))
                    {
                        chkDisplayInctive.Enabled = false;
                        chkDisplayChoiceOnly.Enabled = false;
                        chkDisplayUVOnly.Enabled = false;
                    }
                    else
                    {
                        chkDisplayInctive.Enabled = true;
                        chkDisplayChoiceOnly.Enabled = true;
                        chkDisplayUVOnly.Enabled = true;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbFBOCode.ReadOnly = true;
                    tbFBOCode.BackColor = System.Drawing.Color.LightGray;
                    pnlExternalForm.Visible = true;
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    dgFBO.Rebind();
                    string ID = Convert.ToString(Session["SelectedFBOID"]).Trim();
                    foreach (GridDataItem Item in dgFBO.MasterTableView.Items)
                    {
                        if (Item["FBOID"].Text.Trim() == ID.Trim())
                        {
                            Item.Selected = true;
                            break;
                        }
                    }
                    if (IsPopUp)
                    {
                        LoadControlData();
                    }
                    //  Item.Selected = true;
                    if (!string.IsNullOrEmpty(hdnIsUWa.Value))
                    {

                        if (hdnIsUWa.Value.ToUpper().Trim() == "TRUE")
                        {
                            DisplayUwaRecords(true);

                        }
                        else
                        {
                            EnableForm(true);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

            }
        }
        ///
        /// To display UWa Records
        /// 
        protected void DisplayUwaRecords(bool Enabled)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbIcao.Enabled = false;
                    chkChoice.Enabled = true;
                    chkInactive.Enabled = true;
                    tbName.Enabled = false;
                    tbAddr1.Enabled = true;
                    tbAddr2.Enabled = true;
                    tbCity.Enabled = true;
                    tbState.Enabled = true;
                    tbPostal.Enabled = true;
                    tbCountry.Enabled = false;
                    tbPhone1.Enabled = false;
                    tbPhone2.Enabled = true;
                    tbTollFreePhone.Enabled = true;
                    tbFax.Enabled = false;
                    tbWebsite.Enabled = true;
                    tbEmail.Enabled = true;
                    tbContact.Enabled = true;
                    tbOpHrs.Enabled = false;
                    chkCrewCar.Enabled = true;
                    chkUVair.Enabled = true;
                    tbAirToGnd.Enabled = false;
                    tbSITA.Enabled = false;
                    tbUnicom.Enabled = true;
                    tbARINC.Enabled = true;
                    tbFuelBrand.Enabled = false;
                    tbNegPrice.Enabled = true;
                    tbFuelQuantity.Enabled = true;
                    tbPaymentType.Enabled = true;
                    tbLastFuelPrice.Enabled = true;
                    TextBox tbLastPurchase = (TextBox)ucLastPurchase.FindControl("tbDate");
                    tbLastPurchase.Enabled = true;
                    tbPostedPrice.Enabled = true;
                    btnCancel.Visible = true;
                    btnSaveChanges.Visible = true;
                    btnSaveChangesTop.Visible = true;
                    btnCancelTop.Visible = true;
                    btnCountry.Enabled = false;
                    btnPaymentType.Enabled = true;
                    tbbusinessPhone.Enabled = true;
                    tbContactEmail.Enabled = true;
                    tbContactMobile.Enabled = true;
                    tbAddress3.Enabled = true;
                    tbNotes.Enabled = true;
                    tbCustomField1.Enabled = true;
                    tbCustomField2.Enabled = true;
                    if ((hdnSave.Value == "Update") && (Enabled == true))
                    {
                        chkDisplayInctive.Enabled = false;
                        chkDisplayChoiceOnly.Enabled = false;
                        chkDisplayUVOnly.Enabled = false;
                    }
                    else
                    {
                        chkDisplayInctive.Enabled = true;
                        chkDisplayChoiceOnly.Enabled = true;
                        chkDisplayUVOnly.Enabled = true;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

            }
        }
        /// <summary>
        /// To assign the data to controls
        /// </summary>
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    // GridDataItem Item = (GridDataItem)Session["SelectedItem"];
                    string Code = "";
                    if (Session["SelectedFBOID"] != null)
                    {
                        Code = Convert.ToString(Session["SelectedFBOID"]).Trim();
                    }
                    string FBOCD = "", CustomerID = "", AirportID = "", FBOVendor = "", Addr1 = "", Addr2 = "",
                        CityName = "", StateName = "", PostalZipCD = "", CountryCD = "", PhoneNUM1 = "",
                        PhoneNUM2 = "", TollFreePhoneNum = "", FaxNum = "", Website = "", EmailAddress = "",
                        Contact = "", HoursOfOperation = "", IsInActive = "", IsChoice = "", IsUWAAirPartner = "",
                        IsCrewCar = "", Frequency = "", SITA = "", UNICOM = "", ARINC = "", FuelBrand = "",
                        NegotiatedFuelPrice = "", FuelQty = "", PaymentType = "", LastFuelPrice = "",
                        PostedPrice = "", LastFuelDT = "", LastUpdUID = "", LastUpdTS = "", IsUWAMaintFlag = "", ExchangeRateID = "", CustomField1 = "", CustomField2 = "", NegotiatedTerms = "", SundayWorkHours = "", MondayWorkHours = "", TuesdayWorkHours = "", WednesdayWorkHours = "", ThursdayWorkHours = "", FridayWorkHours = "", SaturdayWorkHours = "";
                    foreach (GridDataItem Item in dgFBO.MasterTableView.Items)
                    {
                        if (Convert.ToString(Item["FBOID"].Text.Trim()) == Code)
                        {
                            FBOCD = Convert.ToString(Item["FBOCD"].Text.Trim()).Replace("&nbsp;", "");
                            CustomerID = Convert.ToString(Item["CustomerID"].Text.Trim()).Replace("&nbsp;", "");
                            AirportID = Convert.ToString(Item["AirportID"].Text.Trim()).Replace("&nbsp;", "");
                            FBOVendor = Convert.ToString(Item["FBOVendor"].Text.Trim()).Replace("&nbsp;", "");
                            Addr1 = Convert.ToString(Item["Addr1"].Text.Trim()).Replace("&nbsp;", "");
                            Addr2 = Convert.ToString(Item["Addr2"].Text.Trim()).Replace("&nbsp;", "");
                            CityName = Convert.ToString(Item["CityName"].Text.Trim()).Replace("&nbsp;", "");
                            StateName = Convert.ToString(Item["StateName"].Text.Trim()).Replace("&nbsp;", "");
                            PostalZipCD = Convert.ToString(Item["PostalZipCD"].Text.Trim()).Replace("&nbsp;", "");
                            CountryCD = Convert.ToString(Item["CountryCD"].Text.Trim()).Replace("&nbsp;", "");
                            PhoneNUM1 = Convert.ToString(Item["PhoneNUM1"].Text.Trim()).Replace("&nbsp;", "");
                            PhoneNUM2 = Convert.ToString(Item["PhoneNUM2"].Text.Trim()).Replace("&nbsp;", "");
                            TollFreePhoneNum = Convert.ToString(Item["TollFreePhoneNum"].Text.Trim()).Replace("&nbsp;", "");
                            FaxNum = Convert.ToString(Item["FaxNum"].Text.Trim()).Replace("&nbsp;", "");
                            Website = Convert.ToString(Item["Website"].Text.Trim()).Replace("&nbsp;", "");
                            EmailAddress = Convert.ToString(Item["EmailAddress"].Text.Trim()).Replace("&nbsp;", "");
                            Contact = Convert.ToString(Item["Contact"].Text.Trim()).Replace("&nbsp;", "");
                            HoursOfOperation = Convert.ToString(Item["HoursOfOperation"].Text.Trim()).Replace("&nbsp;", "");


                            if (Item.GetDataKeyValue("IsInActive") != null)
                            {
                                IsInActive = Item.GetDataKeyValue("IsInActive").ToString().Trim().Replace("&nbsp;", "");
                            }
                            if (Item.GetDataKeyValue("IsChoice") != null)
                            {
                                IsChoice = Item.GetDataKeyValue("IsChoice").ToString().Trim().Replace("&nbsp;", "");
                            }
                            if (Item.GetDataKeyValue("IsUWAAirPartner") != null)
                            {
                                IsUWAAirPartner = Item.GetDataKeyValue("IsUWAAirPartner").ToString().Trim().Replace("&nbsp;", "");
                            }
                            if (Item.GetDataKeyValue("IsCrewCar") != null)
                            {
                                IsCrewCar = Item.GetDataKeyValue("IsCrewCar").ToString().Trim().Replace("&nbsp;", "");
                            }
                            Frequency = Convert.ToString(Item["Frequency"].Text.Trim()).Replace("&nbsp;", "");
                            SITA = Convert.ToString(Item["SITA"].Text.Trim()).Replace("&nbsp;", "");
                            UNICOM = Convert.ToString(Item["UNICOM"].Text.Trim()).Replace("&nbsp;", "");
                            ARINC = Convert.ToString(Item["ARINC"].Text.Trim()).Replace("&nbsp;", "");
                            FuelBrand = Convert.ToString(Item["FuelBrand"].Text.Trim()).Replace("&nbsp;", "");
                            NegotiatedFuelPrice = Convert.ToString(Item["NegotiatedFuelPrice"].Text.Trim()).Replace("&nbsp;", "");
                            FuelQty = Convert.ToString(Item["FuelQty"].Text.Trim()).Replace("&nbsp;", "");
                            PaymentType = Convert.ToString(Item["PaymentType"].Text.Trim()).Replace("&nbsp;", "");
                            LastFuelPrice = Convert.ToString(Item["LastFuelPrice"].Text.Trim()).Replace("&nbsp;", "");
                            PostedPrice = Convert.ToString(Item["PostedPrice"].Text.Trim()).Replace("&nbsp;", "");
                            if (Item.GetDataKeyValue("Remarks") != null)
                            {
                                tbNotes.Text = Item.GetDataKeyValue("Remarks").ToString().Trim();
                            }
                            else
                            {
                                tbNotes.Text = string.Empty;
                            }
                            if ((Item.GetDataKeyValue("LastFuelDT") != null) && (Item.GetDataKeyValue("LastFuelDT") != "&nbsp;"))
                            {
                                if (!string.IsNullOrEmpty(DateFormat))
                                    LastFuelDT = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(Item["LastFuelDT"].Text)).Substring(0, 11);
                                else
                                    LastFuelDT = Item["LastFuelDT"].Text.Trim().Replace("&nbsp;", "");
                            }
                            else
                            {
                                LastFuelDT = Item["LastFuelDT"].Text.Trim().Replace("&nbsp;", "");
                            }

                            LastUpdUID = Convert.ToString(Item["LastUpdUID"].Text.Trim()).Replace("&nbsp;", "");
                            LastUpdTS = Convert.ToString(Item["LastUpdTS"].Text.Trim()).Replace("&nbsp;", "");
                            IsUWAMaintFlag = Item.GetDataKeyValue("IsUWAMaintFlag").ToString().Trim().Replace("&nbsp;", "");
                            if (Item.GetDataKeyValue("CountryID") != null)
                            {
                                hdnCountryId.Value = Item.GetDataKeyValue("CountryID").ToString().Trim();
                            }
                            if (Item.GetDataKeyValue("ContactCellPhoneNum") != null)
                            {
                                tbContactMobile.Text = Item.GetDataKeyValue("ContactCellPhoneNum").ToString().Trim();
                            }
                            if (Item.GetDataKeyValue("ContactBusinessPhone") != null)
                            {
                                tbbusinessPhone.Text = Item.GetDataKeyValue("ContactBusinessPhone").ToString().Trim();
                            }
                            if (Item.GetDataKeyValue("ContactEmail") != null)
                            {
                                tbContactEmail.Text = Item.GetDataKeyValue("ContactEmail").ToString().Trim();
                            }
                            if (Item.GetDataKeyValue("Addr3") != null)
                            {
                                tbAddress3.Text = Item.GetDataKeyValue("Addr3").ToString().Trim();
                            }
                            if (Item.GetDataKeyValue("Filter") != null)
                            {
                                if (Item.GetDataKeyValue("Filter").ToString().ToUpper().Trim() == "UWA")
                                {
                                    hdnIsUWa.Value = "True";
                                    lbWarningMessage.Visible = true;
                                }
                                else
                                {
                                    hdnIsUWa.Value = "False";
                                    lbWarningMessage.Visible = false;
                                }

                            }
                            else
                            {
                                hdnIsUWa.Value = "False";
                                lbWarningMessage.Visible = false;
                            }
                            if (Item.GetDataKeyValue("ExchangeRateID") != null)
                            {
                                ExchangeRateID = Convert.ToString(Item.GetDataKeyValue("ExchangeRateID").ToString());
                            }
                            if (Item.GetDataKeyValue("CustomField1") != null)
                            {
                                CustomField1 = Convert.ToString(Item.GetDataKeyValue("CustomField1").ToString());
                            }
                            if (Item.GetDataKeyValue("CustomField2") != null)
                            {
                                CustomField2 = Convert.ToString(Item.GetDataKeyValue("CustomField2").ToString());
                            }
                            if (Item.GetDataKeyValue("NegotiatedTerms") != null)
                            {
                                NegotiatedTerms = Convert.ToString(Item.GetDataKeyValue("NegotiatedTerms").ToString());
                            }
                            if (Item.GetDataKeyValue("SundayWorkHours") != null)
                            {
                                SundayWorkHours = Convert.ToString(Item.GetDataKeyValue("SundayWorkHours").ToString());
                            }
                            if (Item.GetDataKeyValue("MondayWorkHours") != null)
                            {
                                MondayWorkHours = Convert.ToString(Item.GetDataKeyValue("MondayWorkHours").ToString());
                            }
                            if (Item.GetDataKeyValue("TuesdayWorkHours") != null)
                            {
                                TuesdayWorkHours = Convert.ToString(Item.GetDataKeyValue("TuesdayWorkHours").ToString());
                            }
                            if (Item.GetDataKeyValue("WednesdayWorkHours") != null)
                            {
                                WednesdayWorkHours = Convert.ToString(Item.GetDataKeyValue("WednesdayWorkHours").ToString());
                            }
                            if (Item.GetDataKeyValue("ThursdayWorkHours") != null)
                            {
                                ThursdayWorkHours = Convert.ToString(Item.GetDataKeyValue("ThursdayWorkHours").ToString());
                            }
                            if (Item.GetDataKeyValue("FridayWorkHours") != null)
                            {
                                FridayWorkHours = Convert.ToString(Item.GetDataKeyValue("FridayWorkHours").ToString());
                            }
                            if (Item.GetDataKeyValue("SaturdayWorkHours") != null)
                            {
                                SaturdayWorkHours = Convert.ToString(Item.GetDataKeyValue("SaturdayWorkHours").ToString());
                            }

                            lbColumnName1.Text = "FBO Code";
                            lbColumnName2.Text = "FBO Name";
                            lbColumnValue1.Text = Item["FBOCD"].Text;
                            lbColumnValue2.Text = Item["FBOVendor"].Text;
                            Item.Selected = true;
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(FBOCD))
                    {
                        tbFBOCode.Text = FBOCD;
                    }
                    else
                    {
                        tbFBOCode.Text = string.Empty;
                    }
                    //if (!string.IsNullOrEmpty(AirportID))
                    //{
                    //    hdnAirportId.Value = AirportID;
                    //}
                    //else
                    //{
                    //    hdnAirportId.Value = string.Empty;
                    //}
                    if (!String.IsNullOrEmpty(FBOVendor))
                    {
                        tbName.Text = FBOVendor;
                    }
                    else
                    {
                        tbName.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(Addr1))
                    {
                        tbAddr1.Text = Addr1;
                    }
                    else
                    {
                        tbAddr1.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(Addr2))
                    {
                        tbAddr2.Text = Addr2;
                    }
                    else
                    {
                        tbAddr2.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(CityName))
                    {
                        tbCity.Text = CityName;
                    }
                    else
                    {
                        tbCity.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(StateName))
                    {
                        tbState.Text = StateName;
                    }
                    else
                    {
                        tbState.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(PostalZipCD))
                    {
                        tbPostal.Text = PostalZipCD;
                    }
                    else
                    {
                        tbPostal.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(CountryCD))
                    {
                        tbCountry.Text = CountryCD;
                    }
                    else
                    {
                        tbCountry.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(PhoneNUM1))
                    {
                        tbPhone1.Text = PhoneNUM1;
                    }
                    else
                    {
                        tbPhone1.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(PhoneNUM2))
                    {
                        tbPhone2.Text = PhoneNUM2;
                    }
                    else
                    {
                        tbPhone2.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(TollFreePhoneNum))
                    {
                        tbTollFreePhone.Text = TollFreePhoneNum;
                    }
                    else
                    {
                        tbTollFreePhone.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(FaxNum))
                    {
                        tbFax.Text = FaxNum;
                    }
                    else
                    {
                        tbFax.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(Website))
                    {
                        tbWebsite.Text = Website;
                    }
                    else
                    {
                        tbWebsite.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(EmailAddress))
                    {
                        tbEmail.Text = EmailAddress;
                    }
                    else
                    {
                        tbEmail.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(Contact))
                    {
                        tbContact.Text = Contact;
                    }
                    else
                    {
                        tbContact.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(HoursOfOperation))
                    {
                        tbOpHrs.Text = HoursOfOperation;
                    }
                    else
                    {
                        tbOpHrs.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(IsInActive))
                    {
                        chkInactive.Checked = Convert.ToBoolean(IsInActive);
                    }
                    else
                    {
                        chkInactive.Checked = false;
                    }
                    if (!string.IsNullOrEmpty(IsChoice))
                    {
                        chkChoice.Checked = Convert.ToBoolean(IsChoice);
                    }
                    else
                    {
                        chkChoice.Checked = false;
                    }
                    if (!string.IsNullOrEmpty(IsUWAAirPartner))
                    {
                        chkUVair.Checked = Convert.ToBoolean(IsUWAAirPartner);
                    }
                    else
                    {
                        chkUVair.Checked = false;
                    }
                    if (!string.IsNullOrEmpty(IsCrewCar))
                    {
                        chkCrewCar.Checked = Convert.ToBoolean(IsCrewCar);
                    }
                    else
                    {
                        chkCrewCar.Checked = false;
                    }
                    if (!string.IsNullOrEmpty(Frequency))
                    {
                        tbAirToGnd.Text = Frequency;
                    }
                    else
                    {
                        tbAirToGnd.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(SITA))
                    {
                        tbSITA.Text = SITA;
                    }
                    else
                    {
                        tbSITA.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(UNICOM))
                    {
                        tbUnicom.Text = UNICOM;
                    }
                    else
                    {
                        tbUnicom.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(ARINC))
                    {
                        tbARINC.Text = ARINC;
                    }
                    else
                    {
                        tbARINC.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(FuelBrand))
                    {
                        tbFuelBrand.Text = FuelBrand;
                    }
                    else
                    {
                        tbFuelBrand.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(NegotiatedFuelPrice))
                    {
                        tbNegPrice.Text = NegotiatedFuelPrice;
                    }
                    else
                    {
                        tbNegPrice.Text = "00.0000";
                    }
                    if (!string.IsNullOrEmpty(FuelQty))
                    {
                        tbFuelQuantity.Text = FuelQty;
                    }
                    else
                    {
                        tbFuelQuantity.Text = "000000000000.00";
                    }
                    if (!string.IsNullOrEmpty(PaymentType))
                    {
                        tbPaymentType.Text = PaymentType;
                    }
                    else
                    {
                        tbPaymentType.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(LastFuelPrice))
                    {
                        tbLastFuelPrice.Text = LastFuelPrice;
                    }
                    else
                    {
                        tbLastFuelPrice.Text = "00000.0000";
                    }
                    if (!string.IsNullOrEmpty(PostedPrice))
                    {
                        tbPostedPrice.Text = PostedPrice;
                    }
                    else
                    {
                        tbPostedPrice.Text = "00000.0000";
                    }
                    if (!string.IsNullOrEmpty(PostedPrice))
                    {
                        tbPostedPrice.Text = PostedPrice;
                    }
                    else
                    {
                        tbPostedPrice.Text = "00000.0000";
                    }
                    if (!string.IsNullOrEmpty(LastFuelDT))
                    {
                        TextBox tbLastPurchase = (TextBox)ucLastPurchase.FindControl("tbDate");
                        tbLastPurchase.Text = LastFuelDT;
                        //tbLastPurchase.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", LastFuelDT.Substring(0, 9));
                    }
                    else
                    {
                        TextBox tbLastPurchase = (TextBox)ucLastPurchase.FindControl("tbDate");
                        tbLastPurchase.Text = string.Empty;
                    }


                    if (!string.IsNullOrEmpty(ExchangeRateID))
                    {
                        hdnExchangeRateID.Value = ExchangeRateID;
                        if (hdnExchangeRateID.Value.ToString().Trim() != "")
                        {
                            CheckExchangeRate();
                        }
                        else
                        {
                            tbExchangeRate.Text = "";
                        }
                    }
                    if (!string.IsNullOrEmpty(CustomField1))
                    {
                        tbCustomField1.Text = CustomField1;
                    }
                    if (!string.IsNullOrEmpty(CustomField2))
                    {
                        tbCustomField2.Text = CustomField2;
                    }
                    if (!string.IsNullOrEmpty(NegotiatedTerms))
                    {
                        tbNegotiateTerm.Text = NegotiatedTerms;
                    }
                    if (!string.IsNullOrEmpty(SundayWorkHours))
                    {
                        tbSundayWorkHours.Text = SundayWorkHours;
                    }
                    if (!string.IsNullOrEmpty(MondayWorkHours))
                    {
                        tbMondayWorkHours.Text = MondayWorkHours;
                    }
                    if (!string.IsNullOrEmpty(TuesdayWorkHours))
                    {
                        tbTuesdayWorkHours.Text = TuesdayWorkHours;
                    }
                    if (!string.IsNullOrEmpty(WednesdayWorkHours))
                    {
                        tbWednesdayWorkHours.Text = WednesdayWorkHours;
                    }
                    if (!string.IsNullOrEmpty(ThursdayWorkHours))
                    {
                        tbThursdayWorkHours.Text = ThursdayWorkHours;
                    }
                    if (!string.IsNullOrEmpty(FridayWorkHours))
                    {
                        tbFridayWorkHours.Text = FridayWorkHours;
                    }
                    if (!string.IsNullOrEmpty(SaturdayWorkHours))
                    {
                        tbSaturdayWorkHours.Text = SaturdayWorkHours;
                    }

                    Label lbLastUpdatedUser;
                    lbLastUpdatedUser = (Label)dgFBO.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                    if (!string.IsNullOrEmpty(LastUpdUID))
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + LastUpdUID);
                    }
                    else
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    }
                    if (!string.IsNullOrEmpty(LastUpdTS))
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(LastUpdTS)));
                    }

                    divAdditionalInfo.InnerHtml = System.Web.HttpUtility.HtmlDecode(System.Web.HttpUtility.HtmlEncode(DisplayAdditionalInformation()));
                }, FlightPak.Common.Constants.Policy.UILayer);

            }
        }
        /// <summary>
        /// To make the form readonly
        /// </summary>        
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ClearForm();
                    LoadControlData();
                    tbFBOCode.ReadOnly = true;
                    tbFBOCode.BackColor = System.Drawing.Color.LightGray;
                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);

            }
        }
        /// <summary>
        /// Command Event Trigger for Save or Update Fuel Locator Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            (dgFBO.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgFBO.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }
        /// <summary>
        /// Cancel Fuel Locator Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedFBOID"] != null)
                        {
                            // Unlock the Record
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Database.FBO, Convert.ToInt64(Session["SelectedFBOID"].ToString().Trim()));
                            }
                            //Session.Remove("SelectedFBOID");
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radFBOCRUDPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }
                        //GridEnable(true, true, true);
                        DefaultSelection(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
            CheckIsWidget("PreInit");
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            if (IsPopUp || IsViewOnly)
                            {
                                //Set visible false to all controls.
                                //Container control visibility throwing some error
                                table1.Visible = false;
                                table2.Visible = false;
                                table3.Visible = false;
                                table4.Visible = false;
                                dgFBO.Visible = false;
                                if (IsViewOnly)
                                {
                                    dgFBO.Visible = true;
                                    //table2.Visible = true;
                                    table3.Visible = true;
                                    DefaultSelection(false);
                                }
                                if (!IsViewOnly)
                                {
                                    if (IsAdd)
                                    {
                                        (dgFBO.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                    }
                                    else
                                    {
                                        (dgFBO.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }

            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgFBO.SelectedIndexes.Clear();
                    //PreSelectItem(FPKMstService, false);
                    dgFBO.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="Add"></param>
        /// <param name="Edit"></param>
        /// <param name="Delete"></param>    
        private void GridEnable(bool Add, bool Edit, bool Delete, bool Copy)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((dgFBO.MasterTableView.GetItems(GridItemType.CommandItem)) != null)
                    {
                        LinkButton lbtnInitInsert = (LinkButton)dgFBO.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                        LinkButton lbtnInitDelete = (LinkButton)dgFBO.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitDelete");
                        LinkButton lbtnInitEdit = (LinkButton)dgFBO.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                        LinkButton lnkCopy = (LinkButton)dgFBO.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkCopy");
                        lnkCopy.Enabled = Copy;
                        if (IsAuthorized(Permission.Database.AddFBO))
                        {
                            lbtnInitInsert.Visible = true;
                            if (Add)
                                lbtnInitInsert.Enabled = true;
                            else
                                lbtnInitInsert.Enabled = false;
                        }
                        else
                        {
                            lbtnInitInsert.Visible = false;
                        }
                        if (IsAuthorized(Permission.Database.DeleteFBO))
                        {
                            lbtnInitDelete.Visible = true;
                            if (Delete)
                            {
                                lbtnInitDelete.Enabled = true;
                                lbtnInitDelete.OnClientClick = "javascript:return deleteFBORecord();";
                            }
                            else
                            {
                                lbtnInitDelete.Enabled = false;
                                lbtnInitDelete.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnInitDelete.Visible = false;
                        }
                        if (IsAuthorized(Permission.Database.EditFBO))
                        {
                            lbtnInitEdit.Visible = true;
                            if (Edit)
                            {
                                lbtnInitEdit.Enabled = true;
                                lbtnInitEdit.OnClientClick = "javascript:return ProcessUpdate();";
                            }
                            else
                            {
                                lbtnInitEdit.Enabled = false;
                                lbtnInitEdit.OnClientClick = string.Empty;
                            }
                        }
                        else
                        {
                            lbtnInitEdit.Visible = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <returns></returns>
        private FBO GetItemsForCopy()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string Code = string.Empty;
                FlightPakMasterService.FBO FBOService = new FlightPakMasterService.FBO();

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedFBOID"] != null)
                    {
                        Code = Session["SelectedFBOID"].ToString().Trim();
                    }
                    string IsUWAMaintFlag = "";
                    FBOService.AirportID = Convert.ToInt64(Request.QueryString["AirportID"].ToString());
                    //FBOService.FBOCD = tbFBOCode.Text.Trim();
                    FBOService.IsChoice = chkChoice.Checked;
                    FBOService.IsInActive = chkInactive.Checked;
                    FBOService.FBOVendor = tbName.Text.Trim();
                    FBOService.Addr1 = tbAddr1.Text.Trim();
                    FBOService.Addr2 = tbAddr2.Text.Trim();
                    FBOService.CityName = tbCity.Text.Trim();
                    FBOService.StateName = tbState.Text.Trim();
                    FBOService.PostalZipCD = tbPostal.Text.Trim();
                    if (!string.IsNullOrEmpty(hdnCountryId.Value))
                    {
                        FBOService.CountryID = Convert.ToInt64(hdnCountryId.Value.Trim());
                    }
                    FBOService.PhoneNUM1 = tbPhone1.Text.Trim();
                    FBOService.PhoneNUM2 = tbPhone2.Text.Trim();
                    FBOService.TollFreePhoneNum = tbTollFreePhone.Text.Trim();
                    FBOService.FaxNum = tbFax.Text.Trim();
                    FBOService.Website = tbWebsite.Text.Trim();
                    FBOService.EmailAddress = tbEmail.Text.Trim();
                    FBOService.Contact = tbContact.Text.Trim();
                    FBOService.PaymentType = tbPaymentType.Text.Trim();
                    FBOService.HoursOfOperation = tbOpHrs.Text.Trim();
                    FBOService.IsCrewCar = chkCrewCar.Checked;
                    FBOService.IsUWAAirPartner = chkUVair.Checked;
                    FBOService.SITA = tbSITA.Text.Trim();
                    FBOService.UNICOM = tbUnicom.Text.Trim();
                    FBOService.ARINC = tbARINC.Text.Trim();
                    FBOService.FuelBrand = tbFuelBrand.Text.Trim();
                    FBOService.Frequency = tbAirToGnd.Text.Trim();
                    if (!string.IsNullOrEmpty((((TextBox)ucLastPurchase.FindControl("tbDate")).Text)))
                    {
                        if (DateFormat != null)
                        {
                            FBOService.LastFuelDT = Convert.ToDateTime(FormatDate(((TextBox)ucLastPurchase.FindControl("tbDate")).Text.Trim(), DateFormat));
                        }
                        else
                        {
                            FBOService.LastFuelDT = Convert.ToDateTime(((TextBox)ucLastPurchase.FindControl("tbDate")).Text);
                        }

                    }
                    if (!string.IsNullOrEmpty(tbNegPrice.Text))
                    {
                        FBOService.NegotiatedFuelPrice = Convert.ToDecimal(tbNegPrice.Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        FBOService.NegotiatedFuelPrice = Convert.ToDecimal("00.00000");
                    }
                    if (!string.IsNullOrEmpty(tbFuelQuantity.Text))
                    {
                        FBOService.FuelQty = Convert.ToDecimal(tbFuelQuantity.Text);
                    }
                    else
                    {
                        FBOService.FuelQty = Convert.ToDecimal("000000000000.00");
                    }
                    if (!string.IsNullOrEmpty(tbLastFuelPrice.Text))
                    {
                        FBOService.LastFuelPrice = Convert.ToDecimal(tbLastFuelPrice.Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        FBOService.LastFuelPrice = Convert.ToDecimal("00000.0000");
                    }
                    if (!string.IsNullOrEmpty(tbPostedPrice.Text))
                    {
                        FBOService.PostedPrice = Convert.ToDecimal(tbPostedPrice.Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        FBOService.PostedPrice = Convert.ToDecimal("00000.0000");
                    }
                    if (hdnSave.Value == "Update")
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var FBOList = FPKMasterService.GetAllFBOByFBOID(Convert.ToInt64(Code)).EntityList.ToList();
                            if (FBOList.Count > 0)
                            {

                                GetAllFBOByFBOID FBOEntity = FBOList[0];
                                if (!string.IsNullOrEmpty(FBOEntity.UWAID))
                                    FBOService.UWAID = FBOEntity.UWAID.ToString().Trim();
                                if (FBOEntity.IsUWAMaintFlag != null)
                                {
                                    FBOService.IsUWAMaintFlag = FBOEntity.IsUWAMaintFlag;
                                }

                                FBOService.FBOID = -1;

                                if (FBOEntity.ControlNum != null)
                                {
                                    FBOService.ControlNum = FBOEntity.ControlNum;
                                }
                                if (FBOEntity.IsFPK != null)
                                {
                                    FBOService.IsFPK = FBOEntity.IsFPK;
                                }
                                if (FBOEntity.FPKUpdateDT != null)
                                {
                                    FBOService.FPKUpdateDT = FBOEntity.FPKUpdateDT;
                                }
                                if (FBOEntity.RecordType != null)
                                {
                                    FBOService.RecordType = FBOEntity.RecordType;
                                }
                                if (FBOEntity.SourceID != null)
                                {
                                    FBOService.SourceID = FBOEntity.SourceID;
                                }
                                if (FBOEntity.UpdateDT != null)
                                {
                                    FBOService.UpdateDT = FBOEntity.UpdateDT;
                                }
                                if (FBOEntity.IsPrimaryChoice != null)
                                {
                                    FBOService.IsPrimaryChoice = FBOEntity.IsPrimaryChoice;
                                }
                                if (FBOEntity.FixBaseOperatorID != null)
                                {
                                    FBOService.FixBaseOperatorID = FBOEntity.FixBaseOperatorID;
                                }
                                if (FBOEntity.ARFEECOM != null)
                                {
                                    FBOService.ARFEECOM = FBOEntity.ARFEECOM;
                                }
                                if (FBOEntity.AICS != null)
                                {
                                    FBOService.AICS = FBOEntity.AICS;
                                }
                                if (FBOEntity.ReceiptNum != null)
                                {
                                    FBOService.ReceiptNum = FBOEntity.ReceiptNum;
                                }
                                if (FBOEntity.HHEATED != null)
                                {
                                    FBOService.HHEATED = FBOEntity.HHEATED;
                                }
                                if (FBOEntity.HSJET != null)
                                {
                                    FBOService.HSJET = FBOEntity.HSJET;
                                }
                                if (FBOEntity.HMJET != null)
                                {
                                    FBOService.HMJET = FBOEntity.HMJET;
                                }
                                if (FBOEntity.HLJET != null)
                                {
                                    FBOService.HLJET = FBOEntity.HLJET;
                                }
                                if (FBOEntity.HCJET != null)
                                {
                                    FBOService.HCJET = FBOEntity.HCJET;
                                }
                                if (FBOEntity.FLSJETA != null)
                                {
                                    FBOService.FLSJETA = FBOEntity.FLSJETA;
                                }
                                if (FBOEntity.FLS100LL != null)
                                {
                                    FBOService.FLS100LL = FBOEntity.FLS100LL;
                                }
                                if (FBOEntity.FLS80 != null)
                                {
                                    FBOService.FLS80 = FBOEntity.FLS80;
                                }
                                if (FBOEntity.FSSJETA != null)
                                {
                                    FBOService.FSSJETA = FBOEntity.FSSJETA;
                                }
                                if (FBOEntity.FSS100LL != null)
                                {
                                    FBOService.FSS100LL = FBOEntity.FSS100LL;
                                }
                                if (FBOEntity.FSS80 != null)
                                {
                                    FBOService.FSS80 = FBOEntity.FSS80;
                                }
                                if (FBOEntity.FVOLDISCT != null)
                                {
                                    FBOService.FVOLDISCT = FBOEntity.FVOLDISCT;
                                }
                                if (FBOEntity.ARFEE != null)
                                {
                                    FBOService.ARFEE = FBOEntity.ARFEE;
                                }
                                if (FBOEntity.AHFEE != null)
                                {
                                    FBOService.AHFEE = FBOEntity.AHFEE;
                                }
                                if (FBOEntity.A24 != null)
                                {
                                    FBOService.A24 = FBOEntity.A24;
                                }
                                if (FBOEntity.AQT != null)
                                {
                                    FBOService.AQT = FBOEntity.AQT;
                                }
                                if (FBOEntity.ASEC != null)
                                {
                                    FBOService.ASEC = FBOEntity.ASEC;
                                }
                                if (FBOEntity.AddDeIcing != null)
                                {
                                    FBOService.AddDeIcing = FBOEntity.AddDeIcing;
                                }
                                if (FBOEntity.AOX != null)
                                {
                                    FBOService.AOX = FBOEntity.AOX;
                                }
                                if (FBOEntity.ANIT != null)
                                {
                                    FBOService.ANIT = FBOEntity.ANIT;
                                }
                                if (FBOEntity.ALAV != null)
                                {
                                    FBOService.ALAV = FBOEntity.ALAV;
                                }
                                if (FBOEntity.AGPU != null)
                                {
                                    FBOService.AGPU = FBOEntity.AGPU;
                                }
                                if (FBOEntity.AOWGR != null)
                                {
                                    FBOService.AOWGR = FBOEntity.AOWGR;
                                }
                                if (FBOEntity.AMMINOR != null)
                                {
                                    FBOService.AMMINOR = FBOEntity.AMMINOR;
                                }
                                if (FBOEntity.AMMAJOR != null)
                                {
                                    FBOService.AMMAJOR = FBOEntity.AMMAJOR;
                                }
                                if (FBOEntity.AMAV != null)
                                {
                                    FBOService.AMAV = FBOEntity.AMAV;
                                }
                                if (FBOEntity.AMAUTH != null)
                                {
                                    FBOService.AMAUTH = FBOEntity.AMAUTH;
                                }
                                if (FBOEntity.AICLEAN != null)
                                {
                                    FBOService.AICLEAN = FBOEntity.AICLEAN;
                                }
                                if (FBOEntity.AIIR != null)
                                {
                                    FBOService.AIIR = FBOEntity.AIIR;
                                }

                                if (FBOEntity.AEWAS != null)
                                {
                                    FBOService.AEWAS = FBOEntity.AEWAS;
                                }
                                if (FBOEntity.AEBRITE != null)
                                {
                                    FBOService.AEBRITE = FBOEntity.AEBRITE;
                                }
                                if (FBOEntity.AEPAINT != null)
                                {
                                    FBOService.AEPAINT = FBOEntity.AEPAINT;
                                }

                                if (FBOEntity.PFPR != null)
                                {
                                    FBOService.PFPR = FBOEntity.PFPR;
                                }
                                if (FBOEntity.PCT != null)
                                {
                                    FBOService.PCT = FBOEntity.PCT;
                                }
                                if (FBOEntity.PCC != null)
                                {
                                    FBOService.PCC = FBOEntity.PCC;
                                }
                                if (FBOEntity.PLOUNGE != null)
                                {
                                    FBOService.PLOUNGE = FBOEntity.PLOUNGE;
                                }
                                if (FBOEntity.PREC != null)
                                {
                                    FBOService.PREC = FBOEntity.PREC;
                                }
                                if (FBOEntity.PEXEC != null)
                                {
                                    FBOService.PEXEC = FBOEntity.PEXEC;
                                }
                                if (FBOEntity.PKIT != null)
                                {
                                    FBOService.PKIT = FBOEntity.PKIT;
                                }
                                if (FBOEntity.PLAUND != null)
                                {
                                    FBOService.PLAUND = FBOEntity.PLAUND;
                                }
                                if (FBOEntity.PSR != null)
                                {
                                    FBOService.PSR = FBOEntity.PSR;
                                }
                                if (FBOEntity.PSHOWER != null)
                                {
                                    FBOService.PSHOWER = FBOEntity.PSHOWER;
                                }
                                if (FBOEntity.PSUPP != null)
                                {
                                    FBOService.PSUPP = FBOEntity.PSUPP;
                                }
                                if (FBOEntity.PMEOS != null)
                                {
                                    FBOService.PMEOS = FBOEntity.PMEOS;
                                }

                                if (FBOEntity.RHERTZ != null)
                                {
                                    FBOService.RHERTZ = FBOEntity.RHERTZ;
                                }
                                if (FBOEntity.RAVIS != null)
                                {
                                    FBOService.RAVIS = FBOEntity.RAVIS;
                                }
                                if (FBOEntity.ROTHER != null)
                                {
                                    FBOService.ROTHER = FBOEntity.ROTHER;
                                }

                                if (FBOEntity.PPLIMO != null)
                                {
                                    FBOService.PPLIMO = FBOEntity.PPLIMO;
                                }
                                if (FBOEntity.PPTAXI != null)
                                {
                                    FBOService.PPTAXI = FBOEntity.PPTAXI;
                                }
                                if (FBOEntity.PPROS != null)
                                {
                                    FBOService.PPROS = FBOEntity.PPROS;
                                }
                                if (FBOEntity.PPRWD != null)
                                {
                                    FBOService.PPRWD = FBOEntity.PPRWD;
                                }
                                if (FBOEntity.PPCONC != null)
                                {
                                    FBOService.PPCONC = FBOEntity.PPCONC;
                                }
                                if (FBOEntity.PPCS != null)
                                {
                                    FBOService.PPCS = FBOEntity.PPCS;
                                }
                                if (FBOEntity.PPCR != null)
                                {
                                    FBOService.PPCR = FBOEntity.PPCR;
                                }
                                if (FBOEntity.PPLSR != null)
                                {
                                    FBOService.PPLSR = FBOEntity.PPLSR;
                                }
                                if (FBOEntity.PPSEC != null)
                                {
                                    FBOService.PPSEC = FBOEntity.PPSEC;
                                }
                                if (FBOEntity.PPCWIA != null)
                                {
                                    FBOService.PPCWIA = FBOEntity.PPCWIA;
                                }
                                if (FBOEntity.PPMS != null)
                                {
                                    FBOService.PPMS = FBOEntity.PPMS;
                                }
                                if (FBOEntity.PPFS != null)
                                {
                                    FBOService.PPFS = FBOEntity.PPFS;
                                }
                                if (FBOEntity.PPCA != null)
                                {
                                    FBOService.PPCA = FBOEntity.PPCA;
                                }
                                if (FBOEntity.PPGOLF != null)
                                {
                                    FBOService.PPGOLF = FBOEntity.PPGOLF;
                                }
                                if (FBOEntity.PPCOFFEE != null)
                                {
                                    FBOService.PPCOFFEE = FBOEntity.PPCOFFEE;
                                }
                                if (FBOEntity.PPSNACKS != null)
                                {
                                    FBOService.PPSNACKS = FBOEntity.PPSNACKS;
                                }
                                if (FBOEntity.PPNEWS != null)
                                {
                                    FBOService.PPNEWS = FBOEntity.PPNEWS;
                                }
                                if (FBOEntity.PPICE != null)
                                {
                                    FBOService.PPICE = FBOEntity.PPICE;
                                }

                                if (FBOEntity.PXMF != null)
                                {
                                    FBOService.PXMF = FBOEntity.PXMF;
                                }
                                if (FBOEntity.PXCOFFEE != null)
                                {
                                    FBOService.PXCOFFEE = FBOEntity.PXCOFFEE;
                                }
                                if (FBOEntity.PXBAR != null)
                                {
                                    FBOService.PXBAR = FBOEntity.PXBAR;
                                }
                                if (FBOEntity.PXCCC != null)
                                {
                                    FBOService.PXCCC = FBOEntity.PXCCC;
                                }
                                if (FBOEntity.PXVCC != null)
                                {
                                    FBOService.PXVCC = FBOEntity.PXVCC;
                                }
                                if (FBOEntity.PXLOUNGE != null)
                                {
                                    FBOService.PXLOUNGE = FBOEntity.PXLOUNGE;
                                }
                                if (FBOEntity.PXGIFT != null)
                                {
                                    FBOService.PXGIFT = FBOEntity.PXGIFT;
                                }

                                if (FBOEntity.OHF != null)
                                {
                                    FBOService.OHF = FBOEntity.OHF;
                                }
                                if (FBOEntity.OPCS != null)
                                {
                                    FBOService.OPCS = FBOEntity.OPCS;
                                }
                                if (FBOEntity.OTCS != null)
                                {
                                    FBOService.OTCS = FBOEntity.OTCS;
                                }
                                if (FBOEntity.OJCS != null)
                                {
                                    FBOService.OJCS = FBOEntity.OJCS;
                                }
                                if (FBOEntity.OHCS != null)
                                {
                                    FBOService.OHCS = FBOEntity.OHCS;
                                }
                                if (FBOEntity.OAPS != null)
                                {
                                    FBOService.OAPS = FBOEntity.OAPS;
                                }
                                if (FBOEntity.OCH != null)
                                {
                                    FBOService.OCH = FBOEntity.OCH;
                                }

                                if (FBOEntity.CreditCardVISA != null)
                                {
                                    FBOService.CreditCardVISA = FBOEntity.CreditCardVISA;
                                }
                                if (FBOEntity.CreditCardMasterCard != null)
                                {
                                    FBOService.CreditCardMasterCard = FBOEntity.CreditCardMasterCard;
                                }
                                if (FBOEntity.CreditCardAMEX != null)
                                {
                                    FBOService.CreditCardAMEX = FBOEntity.CreditCardAMEX;
                                }
                                if (FBOEntity.CreditCardDinerClub != null)
                                {
                                    FBOService.CreditCardDinerClub = FBOEntity.CreditCardDinerClub;
                                }
                                if (FBOEntity.CreditCardJCB != null)
                                {
                                    FBOService.CreditCardJCB = FBOEntity.CreditCardJCB;
                                }
                                if (FBOEntity.CreditCardMS != null)
                                {
                                    FBOService.CreditCardMS = FBOEntity.CreditCardMS;
                                }
                                if (FBOEntity.CreditCardAVCard != null)
                                {
                                    FBOService.CreditCardAVCard = FBOEntity.CreditCardAVCard;
                                }
                                if (FBOEntity.CreditCardUVAir != null)
                                {
                                    FBOService.CreditCardUVAir = FBOEntity.CreditCardUVAir;
                                }
                                if (FBOEntity.COMJET != null)
                                {
                                    FBOService.COMJET = FBOEntity.COMJET;
                                }
                                if (FBOEntity.PWPS != null)
                                {
                                    FBOService.PWPS = FBOEntity.PWPS;
                                }
                                if (FBOEntity.UWAUpdates != null)
                                {
                                    FBOService.UWAUpdates = FBOEntity.UWAUpdates;
                                }
                                if (FBOEntity.IsUVAirPNR != null)
                                {
                                    FBOService.IsUVAirPNR = FBOEntity.IsUVAirPNR;
                                }

                            }
                        }

                    }
                    else
                    {
                        FBOService.IsUWAMaintFlag = false;
                        FBOService.FBOID = 0;

                    }
                    if (!string.IsNullOrEmpty(tbbusinessPhone.Text))
                    {
                        FBOService.ContactBusinessPhone = tbbusinessPhone.Text;
                    }
                    else
                    {
                        FBOService.ContactBusinessPhone = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbContactMobile.Text))
                    {
                        FBOService.ContactCellPhoneNum = tbContactMobile.Text;
                    }
                    else
                    {
                        FBOService.ContactCellPhoneNum = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbContactEmail.Text))
                    {
                        FBOService.ContactEmail = tbContactEmail.Text;
                    }
                    else
                    {
                        FBOService.ContactEmail = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbAddress3.Text))
                    { //,F.CustomField1
                        FBOService.Addr3 = tbAddress3.Text;
                    }
                    else
                    {
                        FBOService.Addr3 = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbNotes.Text))
                    {
                        FBOService.Remarks = tbNotes.Text;
                    }
                    else
                    {
                        FBOService.Remarks = string.Empty;
                    }

                    //check
                    if (!string.IsNullOrEmpty(hdnExchangeRateID.Value))
                    {
                        FBOService.ExchangeRateID = Convert.ToInt64(hdnExchangeRateID.Value);
                    }
                    else
                    {
                        FBOService.Remarks = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbCustomField1.Text))
                    {
                        FBOService.CustomField1 = tbCustomField1.Text; //CustomField1
                    }
                    else
                    {
                        FBOService.CustomField1 = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbCustomField2.Text))
                    {
                        FBOService.CustomField2 = tbCustomField2.Text; // CustomField2
                    }
                    else
                    {
                        FBOService.CustomField2 = string.Empty;
                    }
                    //
                    if (!string.IsNullOrEmpty(tbNegotiateTerm.Text.Trim()))
                    {
                        FBOService.NegotiatedTerms = tbNegotiateTerm.Text.Trim(); //NegotiatedTerms
                    }
                    else
                    {
                        FBOService.NegotiatedTerms = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbSundayWorkHours.Text.Trim()))
                    {
                        FBOService.SundayWorkHours = tbSundayWorkHours.Text.Trim(); //SundayWorkHours
                    }
                    else
                    {
                        FBOService.SundayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbMondayWorkHours.Text.Trim()))
                    {
                        FBOService.MondayWorkHours = tbMondayWorkHours.Text; // MondayWorkHours
                    }
                    else
                    {
                        FBOService.MondayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbTuesdayWorkHours.Text.Trim()))
                    {
                        FBOService.TuesdayWorkHours = tbTuesdayWorkHours.Text.Trim(); //TuesdayWorkHours
                    }
                    else
                    {
                        FBOService.TuesdayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbWednesdayWorkHours.Text))
                    {
                        FBOService.WednesdayWorkHours = tbWednesdayWorkHours.Text; // WednesdayWorkHours
                    }
                    else
                    {
                        FBOService.WednesdayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbThursdayWorkHours.Text))
                    {
                        FBOService.ThursdayWorkHours = tbThursdayWorkHours.Text.Trim(); // ThursdayWorkHours
                    }
                    else
                    {
                        FBOService.ThursdayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbFridayWorkHours.Text.Trim()))
                    {
                        FBOService.FridayWorkHours = tbFridayWorkHours.Text.Trim(); //FridayWorkHours
                    }
                    else
                    {
                        FBOService.FridayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbSaturdayWorkHours.Text))
                    {
                        FBOService.SaturdayWorkHours = tbSaturdayWorkHours.Text; // SaturdayWorkHours
                    }
                    else
                    {
                        FBOService.SaturdayWorkHours = string.Empty;
                    }

                    FBOService.IsDeleted = false;
                    return FBOService;
                }, FlightPak.Common.Constants.Policy.UILayer);

                return FBOService;
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <returns></returns>
        private FBO GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string Code = string.Empty;
                FlightPakMasterService.FBO FBOService = new FlightPakMasterService.FBO();

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedFBOID"] != null)
                    {
                        Code = Session["SelectedFBOID"].ToString().Trim();
                    }
                    string IsUWAMaintFlag = "";
                    FBOService.AirportID = Convert.ToInt64(Request.QueryString["AirportID"].ToString());
                    FBOService.FBOCD = tbFBOCode.Text.Trim();
                    FBOService.IsChoice = chkChoice.Checked;
                    FBOService.IsInActive = chkInactive.Checked;
                    FBOService.FBOVendor = tbName.Text.Trim();
                    FBOService.Addr1 = tbAddr1.Text.Trim();
                    FBOService.Addr2 = tbAddr2.Text.Trim();
                    FBOService.CityName = tbCity.Text.Trim();
                    FBOService.StateName = tbState.Text.Trim();
                    FBOService.PostalZipCD = tbPostal.Text.Trim();
                    if (!string.IsNullOrEmpty(hdnCountryId.Value))
                    {
                        FBOService.CountryID = Convert.ToInt64(hdnCountryId.Value.Trim());
                    }
                    FBOService.PhoneNUM1 = tbPhone1.Text.Trim();
                    FBOService.PhoneNUM2 = tbPhone2.Text.Trim();
                    FBOService.TollFreePhoneNum = tbTollFreePhone.Text.Trim();
                    FBOService.FaxNum = tbFax.Text.Trim();
                    FBOService.Website = tbWebsite.Text.Trim();
                    FBOService.EmailAddress = tbEmail.Text.Trim();
                    FBOService.Contact = tbContact.Text.Trim();
                    FBOService.PaymentType = tbPaymentType.Text.Trim();
                    FBOService.HoursOfOperation = tbOpHrs.Text.Trim();
                    FBOService.IsCrewCar = chkCrewCar.Checked;
                    FBOService.IsUWAAirPartner = chkUVair.Checked;
                    FBOService.SITA = tbSITA.Text.Trim();
                    FBOService.UNICOM = tbUnicom.Text.Trim();
                    FBOService.ARINC = tbARINC.Text.Trim();
                    FBOService.FuelBrand = tbFuelBrand.Text.Trim();
                    FBOService.Frequency = tbAirToGnd.Text.Trim();
                    if (!string.IsNullOrEmpty((((TextBox)ucLastPurchase.FindControl("tbDate")).Text)))
                    {
                        if (DateFormat != null)
                        {
                            FBOService.LastFuelDT = Convert.ToDateTime(FormatDate(((TextBox)ucLastPurchase.FindControl("tbDate")).Text.Trim(), DateFormat));
                        }
                        else
                        {
                            FBOService.LastFuelDT = Convert.ToDateTime(((TextBox)ucLastPurchase.FindControl("tbDate")).Text);
                        }

                    }
                    if (!string.IsNullOrEmpty(tbNegPrice.Text))
                    {
                        FBOService.NegotiatedFuelPrice = Convert.ToDecimal(tbNegPrice.Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        FBOService.NegotiatedFuelPrice = Convert.ToDecimal("00.00000");
                    }
                    if (!string.IsNullOrEmpty(tbFuelQuantity.Text))
                    {
                        FBOService.FuelQty = Convert.ToDecimal(tbFuelQuantity.Text);
                    }
                    else
                    {
                        FBOService.FuelQty = Convert.ToDecimal("000000000000.00");
                    }
                    if (!string.IsNullOrEmpty(tbLastFuelPrice.Text))
                    {
                        FBOService.LastFuelPrice = Convert.ToDecimal(tbLastFuelPrice.Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        FBOService.LastFuelPrice = Convert.ToDecimal("00000.0000");
                    }
                    if (!string.IsNullOrEmpty(tbPostedPrice.Text))
                    {
                        FBOService.PostedPrice = Convert.ToDecimal(tbPostedPrice.Text, CultureInfo.CurrentCulture);
                    }
                    else
                    {
                        FBOService.PostedPrice = Convert.ToDecimal("00000.0000");
                    }
                    if (hdnSave.Value == "Update")
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var FBOList = FPKMasterService.GetAllFBOByFBOID(Convert.ToInt64(Code)).EntityList.ToList();
                            if (FBOList.Count > 0)
                            {

                                GetAllFBOByFBOID FBOEntity = FBOList[0];
                                if (!string.IsNullOrEmpty(FBOEntity.UWAID))
                                    FBOService.UWAID = FBOEntity.UWAID.ToString().Trim();
                                if (FBOEntity.IsUWAMaintFlag != null)
                                {
                                    FBOService.IsUWAMaintFlag = FBOEntity.IsUWAMaintFlag;
                                }
                                if (FBOEntity.FBOID != null)
                                {
                                    FBOService.FBOID = FBOEntity.FBOID;
                                }
                                if (FBOEntity.ControlNum != null)
                                {
                                    FBOService.ControlNum = FBOEntity.ControlNum;
                                }
                                if (FBOEntity.IsFPK != null)
                                {
                                    FBOService.IsFPK = FBOEntity.IsFPK;
                                }
                                if (FBOEntity.FPKUpdateDT != null)
                                {
                                    FBOService.FPKUpdateDT = FBOEntity.FPKUpdateDT;
                                }
                                if (FBOEntity.RecordType != null)
                                {
                                    FBOService.RecordType = FBOEntity.RecordType;
                                }
                                if (FBOEntity.SourceID != null)
                                {
                                    FBOService.SourceID = FBOEntity.SourceID;
                                }
                                if (FBOEntity.UpdateDT != null)
                                {
                                    FBOService.UpdateDT = FBOEntity.UpdateDT;
                                }
                                if (FBOEntity.IsPrimaryChoice != null)
                                {
                                    FBOService.IsPrimaryChoice = FBOEntity.IsPrimaryChoice;
                                }
                                if (FBOEntity.FixBaseOperatorID != null)
                                {
                                    FBOService.FixBaseOperatorID = FBOEntity.FixBaseOperatorID;
                                }
                                if (FBOEntity.ARFEECOM != null)
                                {
                                    FBOService.ARFEECOM = FBOEntity.ARFEECOM;
                                }
                                if (FBOEntity.AICS != null)
                                {
                                    FBOService.AICS = FBOEntity.AICS;
                                }
                                if (FBOEntity.ReceiptNum != null)
                                {
                                    FBOService.ReceiptNum = FBOEntity.ReceiptNum;
                                }
                                if (FBOEntity.HHEATED != null)
                                {
                                    FBOService.HHEATED = FBOEntity.HHEATED;
                                }
                                if (FBOEntity.HSJET != null)
                                {
                                    FBOService.HSJET = FBOEntity.HSJET;
                                }
                                if (FBOEntity.HMJET != null)
                                {
                                    FBOService.HMJET = FBOEntity.HMJET;
                                }
                                if (FBOEntity.HLJET != null)
                                {
                                    FBOService.HLJET = FBOEntity.HLJET;
                                }
                                if (FBOEntity.HCJET != null)
                                {
                                    FBOService.HCJET = FBOEntity.HCJET;
                                }
                                if (FBOEntity.FLSJETA != null)
                                {
                                    FBOService.FLSJETA = FBOEntity.FLSJETA;
                                }
                                if (FBOEntity.FLS100LL != null)
                                {
                                    FBOService.FLS100LL = FBOEntity.FLS100LL;
                                }
                                if (FBOEntity.FLS80 != null)
                                {
                                    FBOService.FLS80 = FBOEntity.FLS80;
                                }
                                if (FBOEntity.FSSJETA != null)
                                {
                                    FBOService.FSSJETA = FBOEntity.FSSJETA;
                                }
                                if (FBOEntity.FSS100LL != null)
                                {
                                    FBOService.FSS100LL = FBOEntity.FSS100LL;
                                }
                                if (FBOEntity.FSS80 != null)
                                {
                                    FBOService.FSS80 = FBOEntity.FSS80;
                                }
                                if (FBOEntity.FVOLDISCT != null)
                                {
                                    FBOService.FVOLDISCT = FBOEntity.FVOLDISCT;
                                }
                                if (FBOEntity.ARFEE != null)
                                {
                                    FBOService.ARFEE = FBOEntity.ARFEE;
                                }
                                if (FBOEntity.AHFEE != null)
                                {
                                    FBOService.AHFEE = FBOEntity.AHFEE;
                                }
                                if (FBOEntity.A24 != null)
                                {
                                    FBOService.A24 = FBOEntity.A24;
                                }
                                if (FBOEntity.AQT != null)
                                {
                                    FBOService.AQT = FBOEntity.AQT;
                                }
                                if (FBOEntity.ASEC != null)
                                {
                                    FBOService.ASEC = FBOEntity.ASEC;
                                }
                                if (FBOEntity.AddDeIcing != null)
                                {
                                    FBOService.AddDeIcing = FBOEntity.AddDeIcing;
                                }
                                if (FBOEntity.AOX != null)
                                {
                                    FBOService.AOX = FBOEntity.AOX;
                                }
                                if (FBOEntity.ANIT != null)
                                {
                                    FBOService.ANIT = FBOEntity.ANIT;
                                }
                                if (FBOEntity.ALAV != null)
                                {
                                    FBOService.ALAV = FBOEntity.ALAV;
                                }
                                if (FBOEntity.AGPU != null)
                                {
                                    FBOService.AGPU = FBOEntity.AGPU;
                                }
                                if (FBOEntity.AOWGR != null)
                                {
                                    FBOService.AOWGR = FBOEntity.AOWGR;
                                }
                                if (FBOEntity.AMMINOR != null)
                                {
                                    FBOService.AMMINOR = FBOEntity.AMMINOR;
                                }
                                if (FBOEntity.AMMAJOR != null)
                                {
                                    FBOService.AMMAJOR = FBOEntity.AMMAJOR;
                                }
                                if (FBOEntity.AMAV != null)
                                {
                                    FBOService.AMAV = FBOEntity.AMAV;
                                }
                                if (FBOEntity.AMAUTH != null)
                                {
                                    FBOService.AMAUTH = FBOEntity.AMAUTH;
                                }
                                if (FBOEntity.AICLEAN != null)
                                {
                                    FBOService.AICLEAN = FBOEntity.AICLEAN;
                                }
                                if (FBOEntity.AIIR != null)
                                {
                                    FBOService.AIIR = FBOEntity.AIIR;
                                }

                                if (FBOEntity.AEWAS != null)
                                {
                                    FBOService.AEWAS = FBOEntity.AEWAS;
                                }
                                if (FBOEntity.AEBRITE != null)
                                {
                                    FBOService.AEBRITE = FBOEntity.AEBRITE;
                                }
                                if (FBOEntity.AEPAINT != null)
                                {
                                    FBOService.AEPAINT = FBOEntity.AEPAINT;
                                }

                                if (FBOEntity.PFPR != null)
                                {
                                    FBOService.PFPR = FBOEntity.PFPR;
                                }
                                if (FBOEntity.PCT != null)
                                {
                                    FBOService.PCT = FBOEntity.PCT;
                                }
                                if (FBOEntity.PCC != null)
                                {
                                    FBOService.PCC = FBOEntity.PCC;
                                }
                                if (FBOEntity.PLOUNGE != null)
                                {
                                    FBOService.PLOUNGE = FBOEntity.PLOUNGE;
                                }
                                if (FBOEntity.PREC != null)
                                {
                                    FBOService.PREC = FBOEntity.PREC;
                                }
                                if (FBOEntity.PEXEC != null)
                                {
                                    FBOService.PEXEC = FBOEntity.PEXEC;
                                }
                                if (FBOEntity.PKIT != null)
                                {
                                    FBOService.PKIT = FBOEntity.PKIT;
                                }
                                if (FBOEntity.PLAUND != null)
                                {
                                    FBOService.PLAUND = FBOEntity.PLAUND;
                                }
                                if (FBOEntity.PSR != null)
                                {
                                    FBOService.PSR = FBOEntity.PSR;
                                }
                                if (FBOEntity.PSHOWER != null)
                                {
                                    FBOService.PSHOWER = FBOEntity.PSHOWER;
                                }
                                if (FBOEntity.PSUPP != null)
                                {
                                    FBOService.PSUPP = FBOEntity.PSUPP;
                                }
                                if (FBOEntity.PMEOS != null)
                                {
                                    FBOService.PMEOS = FBOEntity.PMEOS;
                                }

                                if (FBOEntity.RHERTZ != null)
                                {
                                    FBOService.RHERTZ = FBOEntity.RHERTZ;
                                }
                                if (FBOEntity.RAVIS != null)
                                {
                                    FBOService.RAVIS = FBOEntity.RAVIS;
                                }
                                if (FBOEntity.ROTHER != null)
                                {
                                    FBOService.ROTHER = FBOEntity.ROTHER;
                                }

                                if (FBOEntity.PPLIMO != null)
                                {
                                    FBOService.PPLIMO = FBOEntity.PPLIMO;
                                }
                                if (FBOEntity.PPTAXI != null)
                                {
                                    FBOService.PPTAXI = FBOEntity.PPTAXI;
                                }
                                if (FBOEntity.PPROS != null)
                                {
                                    FBOService.PPROS = FBOEntity.PPROS;
                                }
                                if (FBOEntity.PPRWD != null)
                                {
                                    FBOService.PPRWD = FBOEntity.PPRWD;
                                }
                                if (FBOEntity.PPCONC != null)
                                {
                                    FBOService.PPCONC = FBOEntity.PPCONC;
                                }
                                if (FBOEntity.PPCS != null)
                                {
                                    FBOService.PPCS = FBOEntity.PPCS;
                                }
                                if (FBOEntity.PPCR != null)
                                {
                                    FBOService.PPCR = FBOEntity.PPCR;
                                }
                                if (FBOEntity.PPLSR != null)
                                {
                                    FBOService.PPLSR = FBOEntity.PPLSR;
                                }
                                if (FBOEntity.PPSEC != null)
                                {
                                    FBOService.PPSEC = FBOEntity.PPSEC;
                                }
                                if (FBOEntity.PPCWIA != null)
                                {
                                    FBOService.PPCWIA = FBOEntity.PPCWIA;
                                }
                                if (FBOEntity.PPMS != null)
                                {
                                    FBOService.PPMS = FBOEntity.PPMS;
                                }
                                if (FBOEntity.PPFS != null)
                                {
                                    FBOService.PPFS = FBOEntity.PPFS;
                                }
                                if (FBOEntity.PPCA != null)
                                {
                                    FBOService.PPCA = FBOEntity.PPCA;
                                }
                                if (FBOEntity.PPGOLF != null)
                                {
                                    FBOService.PPGOLF = FBOEntity.PPGOLF;
                                }
                                if (FBOEntity.PPCOFFEE != null)
                                {
                                    FBOService.PPCOFFEE = FBOEntity.PPCOFFEE;
                                }
                                if (FBOEntity.PPSNACKS != null)
                                {
                                    FBOService.PPSNACKS = FBOEntity.PPSNACKS;
                                }
                                if (FBOEntity.PPNEWS != null)
                                {
                                    FBOService.PPNEWS = FBOEntity.PPNEWS;
                                }
                                if (FBOEntity.PPICE != null)
                                {
                                    FBOService.PPICE = FBOEntity.PPICE;
                                }

                                if (FBOEntity.PXMF != null)
                                {
                                    FBOService.PXMF = FBOEntity.PXMF;
                                }
                                if (FBOEntity.PXCOFFEE != null)
                                {
                                    FBOService.PXCOFFEE = FBOEntity.PXCOFFEE;
                                }
                                if (FBOEntity.PXBAR != null)
                                {
                                    FBOService.PXBAR = FBOEntity.PXBAR;
                                }
                                if (FBOEntity.PXCCC != null)
                                {
                                    FBOService.PXCCC = FBOEntity.PXCCC;
                                }
                                if (FBOEntity.PXVCC != null)
                                {
                                    FBOService.PXVCC = FBOEntity.PXVCC;
                                }
                                if (FBOEntity.PXLOUNGE != null)
                                {
                                    FBOService.PXLOUNGE = FBOEntity.PXLOUNGE;
                                }
                                if (FBOEntity.PXGIFT != null)
                                {
                                    FBOService.PXGIFT = FBOEntity.PXGIFT;
                                }

                                if (FBOEntity.OHF != null)
                                {
                                    FBOService.OHF = FBOEntity.OHF;
                                }
                                if (FBOEntity.OPCS != null)
                                {
                                    FBOService.OPCS = FBOEntity.OPCS;
                                }
                                if (FBOEntity.OTCS != null)
                                {
                                    FBOService.OTCS = FBOEntity.OTCS;
                                }
                                if (FBOEntity.OJCS != null)
                                {
                                    FBOService.OJCS = FBOEntity.OJCS;
                                }
                                if (FBOEntity.OHCS != null)
                                {
                                    FBOService.OHCS = FBOEntity.OHCS;
                                }
                                if (FBOEntity.OAPS != null)
                                {
                                    FBOService.OAPS = FBOEntity.OAPS;
                                }
                                if (FBOEntity.OCH != null)
                                {
                                    FBOService.OCH = FBOEntity.OCH;
                                }

                                if (FBOEntity.CreditCardVISA != null)
                                {
                                    FBOService.CreditCardVISA = FBOEntity.CreditCardVISA;
                                }
                                if (FBOEntity.CreditCardMasterCard != null)
                                {
                                    FBOService.CreditCardMasterCard = FBOEntity.CreditCardMasterCard;
                                }
                                if (FBOEntity.CreditCardAMEX != null)
                                {
                                    FBOService.CreditCardAMEX = FBOEntity.CreditCardAMEX;
                                }
                                if (FBOEntity.CreditCardDinerClub != null)
                                {
                                    FBOService.CreditCardDinerClub = FBOEntity.CreditCardDinerClub;
                                }
                                if (FBOEntity.CreditCardJCB != null)
                                {
                                    FBOService.CreditCardJCB = FBOEntity.CreditCardJCB;
                                }
                                if (FBOEntity.CreditCardMS != null)
                                {
                                    FBOService.CreditCardMS = FBOEntity.CreditCardMS;
                                }
                                if (FBOEntity.CreditCardAVCard != null)
                                {
                                    FBOService.CreditCardAVCard = FBOEntity.CreditCardAVCard;
                                }
                                if (FBOEntity.CreditCardUVAir != null)
                                {
                                    FBOService.CreditCardUVAir = FBOEntity.CreditCardUVAir;
                                }
                                if (FBOEntity.COMJET != null)
                                {
                                    FBOService.COMJET = FBOEntity.COMJET;
                                }
                                if (FBOEntity.PWPS != null)
                                {
                                    FBOService.PWPS = FBOEntity.PWPS;
                                }
                                if (FBOEntity.UWAUpdates != null)
                                {
                                    FBOService.UWAUpdates = FBOEntity.UWAUpdates;
                                }
                                if (FBOEntity.IsUVAirPNR != null)
                                {
                                    FBOService.IsUVAirPNR = FBOEntity.IsUVAirPNR;
                                }

                            }
                        }

                    }
                    else
                    {
                        FBOService.IsUWAMaintFlag = false;
                        FBOService.FBOID = 0;

                    }
                    if (!string.IsNullOrEmpty(tbbusinessPhone.Text))
                    {
                        FBOService.ContactBusinessPhone = tbbusinessPhone.Text;
                    }
                    else
                    {
                        FBOService.ContactBusinessPhone = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbContactMobile.Text))
                    {
                        FBOService.ContactCellPhoneNum = tbContactMobile.Text;
                    }
                    else
                    {
                        FBOService.ContactCellPhoneNum = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbContactEmail.Text))
                    {
                        FBOService.ContactEmail = tbContactEmail.Text;
                    }
                    else
                    {
                        FBOService.ContactEmail = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbAddress3.Text))
                    { //,F.CustomField1
                        FBOService.Addr3 = tbAddress3.Text;
                    }
                    else
                    {
                        FBOService.Addr3 = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbNotes.Text))
                    {
                        FBOService.Remarks = tbNotes.Text;
                    }
                    else
                    {
                        FBOService.Remarks = string.Empty;
                    }

                    //check
                    if (!string.IsNullOrEmpty(hdnExchangeRateID.Value))
                    {
                        FBOService.ExchangeRateID = Convert.ToInt64(hdnExchangeRateID.Value);
                    }
                    else
                    {
                        FBOService.ExchangeRateID = null;
                        //FBOService.Remarks = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbCustomField1.Text))
                    {
                        FBOService.CustomField1 = tbCustomField1.Text; //CustomField1
                    }
                    else
                    {
                        FBOService.CustomField1 = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbCustomField2.Text))
                    {
                        FBOService.CustomField2 = tbCustomField2.Text; // CustomField2
                    }
                    else
                    {
                        FBOService.CustomField2 = string.Empty;
                    }
                    //
                    if (!string.IsNullOrEmpty(tbNegotiateTerm.Text.Trim()))
                    {
                        FBOService.NegotiatedTerms = tbNegotiateTerm.Text.Trim(); //NegotiatedTerms
                    }
                    else
                    {
                        FBOService.NegotiatedTerms = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbSundayWorkHours.Text.Trim()))
                    {
                        FBOService.SundayWorkHours = tbSundayWorkHours.Text.Trim(); //SundayWorkHours
                    }
                    else
                    {
                        FBOService.SundayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbMondayWorkHours.Text.Trim()))
                    {
                        FBOService.MondayWorkHours = tbMondayWorkHours.Text; // MondayWorkHours
                    }
                    else
                    {
                        FBOService.MondayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbTuesdayWorkHours.Text.Trim()))
                    {
                        FBOService.TuesdayWorkHours = tbTuesdayWorkHours.Text.Trim(); //TuesdayWorkHours
                    }
                    else
                    {
                        FBOService.TuesdayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbWednesdayWorkHours.Text))
                    {
                        FBOService.WednesdayWorkHours = tbWednesdayWorkHours.Text; // WednesdayWorkHours
                    }
                    else
                    {
                        FBOService.WednesdayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbThursdayWorkHours.Text))
                    {
                        FBOService.ThursdayWorkHours = tbThursdayWorkHours.Text.Trim(); // ThursdayWorkHours
                    }
                    else
                    {
                        FBOService.ThursdayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbFridayWorkHours.Text.Trim()))
                    {
                        FBOService.FridayWorkHours = tbFridayWorkHours.Text.Trim(); //FridayWorkHours
                    }
                    else
                    {
                        FBOService.FridayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbSaturdayWorkHours.Text))
                    {
                        FBOService.SaturdayWorkHours = tbSaturdayWorkHours.Text; // SaturdayWorkHours
                    }
                    else
                    {
                        FBOService.SaturdayWorkHours = string.Empty;
                    }

                    FBOService.IsDeleted = false;
                    return FBOService;
                }, FlightPak.Common.Constants.Policy.UILayer);

                return FBOService;
            }
        }
        /// <summary>
        /// Method to check unique Country  Code 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CountryCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckCountryExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }

        /// <summary>
        /// Method to check unique Country  Code 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ExchangeRate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckExchangeRateExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }


        private void CheckExchangeRate()
        {
            using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var CountryValue = CountryService.GetExchangeRate().EntityList.Where(x => x.ExchangeRateID.ToString().Trim().ToUpper() == (hdnExchangeRateID.Value.ToString().Trim().ToUpper())).ToList();
                if (CountryValue.Count() > 0 && CountryValue != null)
                {
                    tbExchangeRate.Text = ((FlightPakMasterService.ExchangeRate)CountryValue[0]).ExchRateCD.ToString().Trim();
                    //RadAjaxManager.GetCurrent(Page).FocusControl(tbSaturdayWorkHours);
                    // tbPostal.Focus();
                }
                else
                {
                    tbExchangeRate.Text = "";
                }
            }
        }
        /// <summary>
        ///  Method to check unique Country  Code 
        /// </summary>
        private void CheckExchangeRateExist()
        {
            if (!string.IsNullOrEmpty(tbExchangeRate.Text))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var CountryValue = CountryService.GetExchangeRate().EntityList.Where(x => x.ExchRateCD.Trim().ToUpper() == (tbExchangeRate.Text.Trim().ToUpper())).ToList();
                    if (CountryValue.Count() > 0 && CountryValue != null)
                    {
                        hdnExchangeRateID.Value = ((FlightPakMasterService.ExchangeRate)CountryValue[0]).ExchangeRateID.ToString().Trim();
                        tbExchangeRate.Text = ((FlightPakMasterService.ExchangeRate)CountryValue[0]).ExchRateCD;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbSaturdayWorkHours);
                        // tbPostal.Focus();
                    }
                    else
                    {
                        cvExchangeRate.IsValid = false;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbExchangeRate);
                        //tbCountry.Focus();
                        IsCheckLookUp = false;
                    }
                }
            }
        }
        /// <summary>
        ///  Method to check unique Country  Code 
        /// </summary>
        private void CheckCountryExist()
        {
            if (!string.IsNullOrEmpty(tbCountry.Text))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var CountryValue = CountryService.GetCountryMasterList().EntityList.Where(x => x.CountryCD.Trim().ToUpper() == (tbCountry.Text.Trim().ToUpper())).ToList();
                    if (CountryValue.Count() > 0 && CountryValue != null)
                    {
                        hdnCountryId.Value = ((FlightPakMasterService.Country)CountryValue[0]).CountryID.ToString().Trim();
                        tbCountry.Text = ((FlightPakMasterService.Country)CountryValue[0]).CountryCD;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbPostal);
                        // tbPostal.Focus();
                    }
                    else
                    {
                        cvCountry.IsValid = false;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbCountry);
                        //tbCountry.Focus();
                        IsCheckLookUp = false;
                    }
                }
            }
        }
        /// <summary>
        /// Method to check unique Paymnet type  Code 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PaymentType_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckPaymenttype();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }

        /// <summary>
        /// To check payment ype
        /// </summary>

        private void CheckPaymenttype()
        {
            if (!string.IsNullOrEmpty(tbPaymentType.Text))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient PaymentService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var PaymentValue = PaymentService.GetPaymentType().EntityList.Where(x => x.PaymentTypeCD.Trim().ToUpper() == (tbPaymentType.Text.Trim().ToUpper())).ToList();
                    if (PaymentValue.Count() > 0 && PaymentValue != null)
                    {
                        hdnPayment.Value = ((FlightPakMasterService.PaymentType)PaymentValue[0]).PaymentTypeID.ToString().Trim();
                        tbPaymentType.Text = ((FlightPakMasterService.PaymentType)PaymentValue[0]).PaymentTypeCD;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbLastFuelPrice);
                        //tbLastFuelPrice.Focus();
                    }
                    else
                    {
                        cvPaymentType.IsValid = false;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbPaymentType);
                        // tbPaymentType.Focus();
                        IsCheckLookUp = false;
                    }
                }
            }
        }
        /// <summary>
        /// Method to display additional information
        /// </summary>
        /// <returns></returns>
        private string DisplayAdditionalInformation()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.MasterCatalogServiceClient FBOService = new FlightPakMasterService.MasterCatalogServiceClient();
                StringBuilder AdditionalInfo = new StringBuilder();
                StringBuilder FinalAdditionalInfo = new StringBuilder();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    List<GetAllFBOByAirportID> FBOData = new List<GetAllFBOByAirportID>();
                    string FBOID = Convert.ToString(Session["SelectedFBOID"]).Trim();
                    string Code = "";
                    foreach (GridDataItem Item in dgFBO.MasterTableView.Items)
                    {
                        if (Item["FBOID"].Text.Trim() == FBOID)
                        {
                            Code = Item["AirportID"].Text.Trim();
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(tbFBOCode.Text))
                        FBOData = FBOService.GetAllFBOByAirportID(Convert.ToInt64(Code)).EntityList.Where(x => x.FBOCD.Trim().ToUpper().Equals(tbFBOCode.Text.Trim().ToUpper())).ToList<GetAllFBOByAirportID>();
                    // FBOData = FBOService.GetAllFBOByAirportID(Convert.ToInt64(Code));
                    //   FBOData = FBOService.GetFBOInfo().EntityList.Where(x => x.FBOCDfb.Trim().ToUpper() == (tbFBOCode.Text.Trim().ToUpper()) && x.AirportID.ToString().Trim().ToUpper() == (Code.Trim().ToUpper())).ToList<GetAllFBO>();
                    string strComma = ", ";
                    string strNextLine = "<br>";
                    if (FBOData.Count != 0)
                    {
                        AdditionalInfo.Append("Transient Hangar " + strNextLine);
                        if (FBOData[0].HHEATED == "Y")
                        {
                            AdditionalInfo.Append("Heated" + strComma);
                        }
                        if (FBOData[0].HSJET == "Y")
                        {
                            AdditionalInfo.Append("Small Jet" + strComma);
                        }
                        if (FBOData[0].HMJET == "Y")
                        {
                            AdditionalInfo.Append("Mid-Sized Jet" + strComma);
                        }
                        if (FBOData[0].HLJET == "Y")
                        {
                            AdditionalInfo.Append("Large Jet" + strComma);
                        }
                        if (FBOData[0].HCJET == "Y")
                        {
                            AdditionalInfo.Append("Commercial Jet" + strComma);
                        }
                        if (AdditionalInfo.ToString().Contains(","))
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(',')));
                        }
                        else
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(strNextLine)));
                        }
                        AdditionalInfo = new StringBuilder();
                        AdditionalInfo.Append(strNextLine + "Fuel" + strNextLine);
                        if (FBOData[0].FLSJETA == "Y")
                        {
                            AdditionalInfo.Append("Line Service Jet A" + strComma);
                        }
                        if (FBOData[0].FLS100LL == "Y")
                        {
                            AdditionalInfo.Append("Line Service 100LL" + strComma);
                        }
                        if (FBOData[0].FLS80 == "Y")
                        {
                            AdditionalInfo.Append("Line Service 80" + strComma);
                        }
                        if (FBOData[0].FSSJETA == "Y")
                        {
                            AdditionalInfo.Append("Self Serve Jet A" + strComma);
                        }
                        if (FBOData[0].FSS100LL == "Y")
                        {
                            AdditionalInfo.Append("Self Serve 100LL" + strComma);
                        }
                        if (FBOData[0].FSS80 == "Y")
                        {
                            AdditionalInfo.Append("Self Serve 80" + strComma);
                        }
                        if (FBOData[0].FVOLDISCT == "Y")
                        {
                            AdditionalInfo.Append("Volume Discount" + strComma);
                        }
                        if (AdditionalInfo.ToString().Contains(","))
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(',')));
                        }
                        else
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(strNextLine)));
                        }
                        AdditionalInfo = new StringBuilder();
                        AdditionalInfo.Append(strNextLine + "Aircraft: " + strNextLine);
                        if (FBOData[0].ARFEE == "Y")
                        {
                            AdditionalInfo.Append("Ramp Fee" + strComma);
                        }
                        if (FBOData[0].AHFEE == "Y")
                        {
                            AdditionalInfo.Append("Handling Fee" + strComma);
                        }
                        if (FBOData[0].A24 == "Y")
                        {
                            AdditionalInfo.Append("24-Hour Service On Request" + strComma);
                        }
                        if (FBOData[0].AQT == "Y")
                        {
                            AdditionalInfo.Append("Quick Turns" + strComma);
                        }
                        if (FBOData[0].ASEC == "Y")
                        {
                            AdditionalInfo.Append("Security" + strComma);
                        }
                        if (FBOData[0].AddDeIcing == "Y")
                        {
                            AdditionalInfo.Append("De-Icing Service" + strComma);
                        }
                        if (FBOData[0].AOX == "Y")
                        {
                            AdditionalInfo.Append("Oxygen Service" + strComma);
                        }
                        if (FBOData[0].ANIT == "Y")
                        {
                            AdditionalInfo.Append("Nitrogen Service" + strComma);
                        }
                        if (FBOData[0].ALAV == "Y")
                        {
                            AdditionalInfo.Append("Lavatory Service" + strComma);
                        }
                        if (FBOData[0].AGPU == "Y")
                        {
                            AdditionalInfo.Append("Group Power Unit (GPU)" + strComma);
                        }
                        if (FBOData[0].AOWGR == "Y")
                        {
                            AdditionalInfo.Append("Over Water Gear Rental)" + strComma);
                        }
                        if (AdditionalInfo.ToString().Contains(","))
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(',')));
                        }
                        else
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(strNextLine)));
                        }
                        AdditionalInfo = new StringBuilder();
                        AdditionalInfo.Append(strNextLine + "Aircraft Maintenance: " + strNextLine);
                        if (FBOData[0].AMMINOR == "Y")
                        {
                            AdditionalInfo.Append("Minor" + strComma);
                        }
                        if (FBOData[0].AMMAJOR == "Y")
                        {
                            AdditionalInfo.Append("Major" + strComma);
                        }
                        if (FBOData[0].AMAV == "Y")
                        {
                            AdditionalInfo.Append("Avionics Repair" + strComma);
                        }
                        if (FBOData[0].AMAUTH == "Y")
                        {
                            AdditionalInfo.Append("Authorized Facility " + FBOData[0].AMAUTHCOM + strComma);
                        }
                        if (AdditionalInfo.ToString().Contains(","))
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(',')));
                        }
                        else
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(strNextLine)));
                        }
                        AdditionalInfo = new StringBuilder();
                        AdditionalInfo.Append(strNextLine + "Aircraft Interior: " + strNextLine);
                        if (FBOData[0].AICLEAN == "Y")
                        {
                            AdditionalInfo.Append("Cleaning" + strComma);
                        }
                        if (FBOData[0].AICLEAN == "Y")
                        {
                            AdditionalInfo.Append("Carpet Shampooing" + strComma);
                        }
                        if (FBOData[0].AIIR == "Y")
                        {
                            AdditionalInfo.Append("Interior Repair" + strComma);
                        }
                        if (AdditionalInfo.ToString().Contains(","))
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(',')));
                        }
                        else
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(strNextLine)));
                        }
                        AdditionalInfo = new StringBuilder();
                        AdditionalInfo.Append(strNextLine + "Aircraft Exterior: " + strNextLine);
                        if (FBOData[0].AEWAS == "Y")
                        {
                            AdditionalInfo.Append("Washing" + strComma);
                        }
                        if (FBOData[0].AEBRITE == "Y")
                        {
                            AdditionalInfo.Append("Bright" + strComma);
                        }
                        if (FBOData[0].AEPAINT == "Y")
                        {
                            AdditionalInfo.Append("Painting Facility" + strComma);
                        }
                        if (AdditionalInfo.ToString().Contains(","))
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(',')));
                        }
                        else
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(strNextLine)));
                        }
                        AdditionalInfo = new StringBuilder();
                        AdditionalInfo.Append(strNextLine + "Pilot: " + strNextLine);
                        if (FBOData[0].PWPS == "Y")
                        {
                            AdditionalInfo.Append("Weather Planning Service" + strComma);
                        }
                        if (FBOData[0].PFPR == "Y")
                        {
                            AdditionalInfo.Append("Flight Planning Room" + strComma);
                        }
                        if (FBOData[0].PCT == "Y")
                        {
                            AdditionalInfo.Append("Crew Transportation" + strComma);
                        }
                        if (FBOData[0].PCC == "Y")
                        {
                            AdditionalInfo.Append("Crew Cars" + strComma);
                        }
                        if (FBOData[0].PLOUNGE == "Y")
                        {
                            AdditionalInfo.Append("Lounge" + strComma);
                        }
                        if (FBOData[0].PREC == "Y")
                        {
                            AdditionalInfo.Append("Recreation Room" + strComma);
                        }
                        if (FBOData[0].PEXEC == "Y")
                        {
                            AdditionalInfo.Append("Exercise Room" + strComma);
                        }
                        if (FBOData[0].PKIT == "Y")
                        {
                            AdditionalInfo.Append("Kitchen" + strComma);
                        }
                        if (FBOData[0].PLAUND == "Y")
                        {
                            AdditionalInfo.Append("Laundry Service" + strComma);
                        }
                        if (FBOData[0].PSR == "Y")
                        {
                            AdditionalInfo.Append("Snooze/Sleep Room" + strComma);
                        }
                        if (FBOData[0].PSHOWER == "Y")
                        {
                            AdditionalInfo.Append("Shower Facility" + strComma);
                        }
                        if (FBOData[0].PSUPP == "Y")
                        {
                            AdditionalInfo.Append("Pilot Supplies" + strComma);
                        }
                        if (FBOData[0].PMEOS == "Y")
                        {
                            AdditionalInfo.Append("Medical Examiner On-Site");
                        }
                        if (AdditionalInfo.ToString().Contains(","))
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(',')));
                        }
                        else
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(strNextLine)));
                        }
                        AdditionalInfo = new StringBuilder();
                        AdditionalInfo.Append(strNextLine + "Rental Cars: " + strNextLine);
                        if (FBOData[0].RHERTZ == "Y")
                        {
                            AdditionalInfo.Append("Hertz" + strComma);
                        }
                        if (FBOData[0].RAVIS == "Y")
                        {
                            AdditionalInfo.Append("Avis" + strComma);
                        }
                        if (FBOData[0].ROTHER == "Y")
                        {
                            AdditionalInfo.Append("Other: " + FBOData[0].ROCOMM + strComma);
                        }
                        if (AdditionalInfo.ToString().Contains(","))
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(',')));
                        }
                        else
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(strNextLine)));
                        }
                        AdditionalInfo = new StringBuilder();
                        AdditionalInfo.Append(strNextLine + "Pilot/Passenger: " + strNextLine);
                        if (FBOData[0].PPLIMO == "Y")
                        {
                            AdditionalInfo.Append("Limousine Service" + strComma);
                        }
                        if (FBOData[0].PPTAXI == "Y")
                        {
                            AdditionalInfo.Append("Taxi Service" + strComma);
                        }
                        if (FBOData[0].PPROS == "Y")
                        {
                            AdditionalInfo.Append("Restaurant On-Site" + strComma);
                        }
                        if (FBOData[0].PPRWD == "Y")
                        {
                            AdditionalInfo.Append("Restaurant Walking Distance" + strComma);
                        }
                        if (FBOData[0].PPCONC == "Y")
                        {
                            AdditionalInfo.Append("Concierge Service" + strComma);
                        }
                        if (FBOData[0].PPCS == "Y")
                        {
                            AdditionalInfo.Append("Catering Service" + strComma);
                        }
                        if (FBOData[0].PPCR == "Y")
                        {
                            AdditionalInfo.Append("Catering Refrigeration" + strComma);
                        }
                        if (FBOData[0].PPLSR == "Y")
                        {
                            AdditionalInfo.Append("Locked Storage Room" + strComma);
                        }
                        if (FBOData[0].PPSEC == "Y")
                        {
                            AdditionalInfo.Append("Security" + strComma);
                        }
                        if (FBOData[0].PPCWIA == "Y")
                        {
                            AdditionalInfo.Append("Computer With Internet Access" + strComma);
                        }
                        if (FBOData[0].PPMS == "Y")
                        {
                            AdditionalInfo.Append("Modem Station" + strComma);
                        }
                        if (FBOData[0].PPFS == "Y")
                        {
                            AdditionalInfo.Append("Fax Service" + strComma);
                        }
                        if (FBOData[0].PPCA == "Y")
                        {
                            AdditionalInfo.Append("Copier Available" + strComma);
                        }
                        if (FBOData[0].PPGOLF == "Y")
                        {
                            AdditionalInfo.Append("Golf Near By" + strComma);
                        }
                        if (FBOData[0].PPCOFFEE == "Y")
                        {
                            AdditionalInfo.Append("Coffee" + strComma);
                        }
                        if (FBOData[0].PPSNACKS == "Y")
                        {
                            AdditionalInfo.Append("Snacks" + strComma);
                        }
                        if (FBOData[0].PPNEWS == "Y")
                        {
                            AdditionalInfo.Append("Newspapers" + strComma);
                        }
                        if (FBOData[0].PPICE == "Y")
                        {
                            AdditionalInfo.Append("Ice" + strComma);
                        }
                        if (AdditionalInfo.ToString().Contains(","))
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(',')));
                        }
                        else
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(strNextLine)));
                        }
                        AdditionalInfo = new StringBuilder();
                        AdditionalInfo.Append(strNextLine + "Passenger: " + strNextLine);
                        if (FBOData[0].PXMF == "Y")
                        {
                            AdditionalInfo.Append("Meeting Facility" + strComma);
                        }
                        if (FBOData[0].PXCOFFEE == "Y")
                        {
                            AdditionalInfo.Append("Coffee Area" + strComma);
                        }
                        if (FBOData[0].PXBAR == "Y")
                        {
                            AdditionalInfo.Append("Bar Area" + strComma);
                        }
                        if (FBOData[0].PXCCC == "Y")
                        {
                            AdditionalInfo.Append("Conference Call Capable" + strComma);
                        }
                        if (FBOData[0].PXVCC == "Y")
                        {
                            AdditionalInfo.Append("Video Conference Capable" + strComma);
                        }
                        if (FBOData[0].PXLOUNGE == "Y")
                        {
                            AdditionalInfo.Append("Lounge" + strComma);
                        }
                        if (FBOData[0].PXGIFT == "Y")
                        {
                            AdditionalInfo.Append("Gift Shop" + strComma);
                        }
                        if (AdditionalInfo.ToString().Contains(","))
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(',')));
                        }
                        else
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(strNextLine)));
                        }
                        AdditionalInfo = new StringBuilder();
                        AdditionalInfo.Append(strNextLine + "Other: " + strNextLine);
                        if (FBOData[0].OHF == "Y")
                        {
                            AdditionalInfo.Append("Helicopter Friendly" + strComma);
                        }
                        if (FBOData[0].OPCS == "Y")
                        {
                            AdditionalInfo.Append("Piston Charter Service" + strComma);
                        }
                        if (FBOData[0].OTCS == "Y")
                        {
                            AdditionalInfo.Append("Turboprop Charter Service" + strComma);
                        }
                        if (FBOData[0].OJCS == "Y")
                        {
                            AdditionalInfo.Append("Jet Charter Service" + strComma);
                        }
                        if (FBOData[0].OHCS == "Y")
                        {
                            AdditionalInfo.Append("Helicopter Charter Service" + strComma);
                        }
                        if (FBOData[0].OAPS == "Y")
                        {
                            AdditionalInfo.Append("Aerial Photography Service" + strComma);
                        }
                        if (FBOData[0].OCH == "Y")
                        {
                            AdditionalInfo.Append("Cargo Handling" + strComma);
                        }
                        if (AdditionalInfo.ToString().Contains(","))
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(',')));
                        }
                        else
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(strNextLine)));
                        }
                        AdditionalInfo = new StringBuilder();
                        AdditionalInfo.Append(strNextLine + "Credit Cards Accepted: " + strNextLine);
                        if (FBOData[0].CreditCardVISA == "Y")
                        {
                            AdditionalInfo.Append("Visa" + strComma);
                        }
                        if (FBOData[0].CreditCardMasterCard == "Y")
                        {
                            AdditionalInfo.Append("Mastercard" + strComma);
                        }
                        if (FBOData[0].CreditCardAMEX == "Y")
                        {
                            AdditionalInfo.Append("American Express" + strComma);
                        }
                        if (FBOData[0].CreditCardDinerClub == "Y")
                        {
                            AdditionalInfo.Append("Diners Club" + strComma);
                        }
                        if (FBOData[0].CreditCardJCB == "Y")
                        {
                            AdditionalInfo.Append("JCB" + strComma);
                        }
                        if (FBOData[0].CreditCardMS == "Y")
                        {
                            AdditionalInfo.Append("Multi Service" + strComma);
                        }
                        if (FBOData[0].CreditCardAVCard == "Y")
                        {
                            AdditionalInfo.Append("Avcard" + strComma);
                        }
                        if (FBOData[0].CreditCardUVAir == "Y")
                        {
                            AdditionalInfo.Append("Uvair" + strComma);
                        }
                        if (FBOData[0].COMJET == "Y")
                        {
                            AdditionalInfo.Append("Provide Service For Commercial Size Jet" + strComma);
                        }
                        if (AdditionalInfo.ToString().Contains(","))
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(',')));
                        }
                        else
                        {
                            FinalAdditionalInfo.Append(AdditionalInfo.ToString().Remove(AdditionalInfo.ToString().LastIndexOf(strNextLine)));
                        }
                    }
                    return FinalAdditionalInfo.ToString();
                }, FlightPak.Common.Constants.Policy.UILayer);

                return FinalAdditionalInfo.ToString();
            }
        }
        protected void lnkCopy_Click(object sender, EventArgs e)
        {
            CheckCountryExist();
            CheckPaymenttype();
            CheckExchangeRateExist();
            if (IsCheckLookUp)
            {
                if (IsFBOExists())
                {
                    
                    string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                        + @" oManager.radalert('FBO already exists', 360, 50, 'FBO');";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                }
                else
                {
                    using (MasterCatalogServiceClient FBOService = new MasterCatalogServiceClient())
                    {
                        var FBOValue = FBOService.AddFBO(GetItemsForCopy());
                        if (FBOValue.ReturnFlag == true)
                        {
                            ShowSuccessMessage();
                        }
                        dgFBO.Rebind();
                        SelectItem();
                        //chkDisplayInctive.Enabled = true;
                        //chkDisplayChoiceOnly.Enabled = true;
                        //chkDisplayUVOnly.Enabled = true;
                    }
                }
            }
        }
        /// <summary>
        /// To dispaly active and inactive records based on checkbox change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkDisplayInctive_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (btnSaveChanges.Visible == false)
                        {
                            SearchAndFilter(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }
        private void CheckIsWidget(string LoadType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["IsWidget"] != null)
                {
                    if (Session["IsWidget"].ToString() == "1")
                    {
                        if (LoadType == "PreInit")
                        {
                            this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");
                        }
                    }
                }
            }
        }

        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetAllFBOByAirportID> lstFBO = new List<FlightPakMasterService.GetAllFBOByAirportID>();
                if (Session["FBOCD"] != null)
                {
                    lstFBO = (List<FlightPakMasterService.GetAllFBOByAirportID>)Session["FBOCD"];
                }
                if (lstFBO.Count != 0)
                {
                    if (chkDisplayInctive.Checked == false) { lstFBO = lstFBO.Where(x => x.IsInActive == false).ToList<GetAllFBOByAirportID>(); }
                    if (chkDisplayChoiceOnly.Checked == true) { lstFBO = lstFBO.Where(x => x.IsChoice == true).ToList<GetAllFBOByAirportID>(); }
                    if (chkDisplayUVOnly.Checked == true) { lstFBO = lstFBO.Where(x => x.IsUWAAirPartner == true).ToList<GetAllFBOByAirportID>(); }
                    dgFBO.DataSource = lstFBO.GroupBy(t => t.FBOVendor).Select(y => y.First()); 
                    if (IsDataBind)
                    {
                        dgFBO.DataBind();
                    }
                }
                else
                {
                    chkDisplayChoiceOnly.Enabled = false;
                    chkDisplayInctive.Enabled = false;
                    chkDisplayUVOnly.Enabled = false;
                }
                LoadControlData();
                return false;
            }
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgFBO.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            if (IsViewOnly && IsPopUp && Session["SelectedAirportForFBOID"] != null)
            {
                hdnAirportId.Value = Session["SelectedAirportForFBOID"].ToString();
            }
            
            var FBOValue = FPKMstService.GetAllFBOByAirportID(Convert.ToInt64(hdnAirportId.Value));
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, FBOValue);
            List<FlightPakMasterService.GetAllFBOByAirportID> filteredList = GetFilteredList(FBOValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.FBOID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgFBO.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgFBO.CurrentPageIndex = PageNumber;
            dgFBO.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllFBOByAirportID FBOValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedFBOID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = FBOValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().FBOID;
                Session["SelectedFBOID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllFBOByAirportID> GetFilteredList(ReturnValueOfGetAllFBOByAirportID FBOValue)
        {
            List<FlightPakMasterService.GetAllFBOByAirportID> filteredList = new List<FlightPakMasterService.GetAllFBOByAirportID>();

            if (FBOValue.ReturnFlag)
            {
                filteredList = FBOValue.EntityList;
            }

            if (!IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (!chkDisplayInctive.Checked) { filteredList = filteredList.Where(x => x.IsInActive == false).ToList<GetAllFBOByAirportID>(); }
                    if (chkDisplayChoiceOnly.Checked) { filteredList = filteredList.Where(x => x.IsChoice == true).ToList<GetAllFBOByAirportID>(); }
                    if (chkDisplayUVOnly.Checked) { filteredList = filteredList.Where(x => x.IsUWAAirPartner == true).ToList<GetAllFBOByAirportID>(); }
                }
            }

            return filteredList;
        }
    }
}
