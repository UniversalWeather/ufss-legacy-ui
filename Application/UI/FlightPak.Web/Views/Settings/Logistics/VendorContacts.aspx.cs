﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Security.Policy;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Logistics
{
    public partial class VendorContacts : BaseSecuredPage
    {
        private bool IsEmptyCheck = true;
        public List<string> lstVendorContactCodes = new List<string>();
        #region constants
        private const string ConstIsInactive = "IsChoice";
        #endregion
        private ExceptionManager exManager;
        private bool VendorContactPageNavigated = false;
        private string strVendorContactID = "";
        private string strVendorContactCD = "";
        private Int64 strVendorID = 0;
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgVendorContacts, dgVendorContacts, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgVendorContacts.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            DefaultSelection(true);
                            tbVendorCode.Text = Server.UrlDecode(Request.QueryString["VendorCode"].ToString());
                            tbVendorContacts.Text = Server.UrlDecode(Request.QueryString["VendorName"].ToString());
                            tbVendorCode.Enabled = false;
                            tbVendorContacts.Enabled = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.VendorContact);
                }
            }

        }
        
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgVendorContacts.Rebind();
                    }
                    if (dgVendorContacts.MasterTableView.Items.Count > 0)
                    {
                        if (Session["SelectedVendorContactID"] == null)
                        {
                            dgVendorContacts.SelectedIndexes.Add(0);
                            Session["SelectedVendorContactID"] = dgVendorContacts.Items[0].GetDataKeyValue("VendorContactID").ToString();
                        }

                        if (dgVendorContacts.SelectedIndexes.Count == 0)
                            dgVendorContacts.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                    GridEnable(true, true, true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }


        /// <summary>
        /// Highlight the selected item on edit
        /// </summary>
        private void SelectItem()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedVendorContactID"] != null)
                    {
                        string ID = Session["SelectedVendorContactID"].ToString();
                        foreach (GridDataItem Item in dgVendorContacts.MasterTableView.Items)
                        {
                            if (Item["VendorContactID"].Text.Trim() == ID)
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);


            }

        }

        protected void VendorContact_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (VendorContactPageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgVendorContacts, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.VendorContact);
                }
            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgVendorContacts;
                        }
                        else if (e.Initiator.ID.IndexOf("btnSaveChangesTop", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgVendorContacts;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.VendorContact);
                }
            }
        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void dgVendorContacts_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEditButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.VendorContact);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.Validate(group);
                        // get the first validator that failed
                        var validator = GetValidators(group)
                        .OfType<BaseValidator>()
                        .FirstOrDefault(v => !v.IsValid);
                        // set the focus to the control
                        // that the validator targets
                        if (validator != null)
                        {
                            Control target = validator
                            .NamingContainer
                            .FindControl(validator.ControlToValidate);
                            if (target != null)
                            {
                                target.Focus();
                                IsEmptyCheck = false;
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.VendorContact);
                }
            }

        }
        /// <summary>
        /// Bind Vendor Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void dgVendorContacts_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objVendorContactsService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<GetAllVendorContact> lstVendorContact = new List<GetAllVendorContact>();
                            GetAllVendorContact objVendorContact = new GetAllVendorContact();
                            objVendorContact.VendorID = Convert.ToInt64(Server.UrlDecode(Request.QueryString["VendorID"].ToString()));
                            objVendorContact.VendorType = Request.QueryString["VendorType"].ToString();
                            var objVen = objVendorContactsService.GetVendorContactMasterInfo(objVendorContact);

                            if (objVen.ReturnFlag == true)
                            {
                                dgVendorContacts.DataSource = objVen.EntityList;
                            }
                            Session.Remove("VendorContactCD");
                            lstVendorContactCodes = new List<string>();
                            foreach (GetAllVendorContact VendorContactEntity in objVen.EntityList)
                            {
                                lstVendorContactCodes.Add(VendorContactEntity.VendorContactCD.ToString().Trim());
                            }

                            Session["VendorContactCD"] = lstVendorContact;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.VendorContact);
                }
            }
        }

        /// <summary>
        /// Item Command for Vendor Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void dgVendorContacts_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                if (Session["SelectedVendorContactID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.VendorContact, Convert.ToInt64(Session["SelectedVendorContactID"].ToString().Trim()));

                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.VendorContact);
                                            SelectItem();
                                            return;
                                        }
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        tbContactName.Focus();
                                        SelectItem();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgVendorContacts.SelectedIndexes.Clear();
                                //tbCode.ReadOnly = false;
                                //tbCode.BackColor = System.Drawing.Color.White;
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                tbContactName.Focus();
                                break;
                            case "Filter":
                                //foreach (GridColumn Column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.VendorContact);
                }
            }
        }

        protected void Country_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCountryCode.Text != null)
                        {
                            CheckAlreadyCountryExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.VendorContact);
                }
            }
        }

        private bool CheckAlreadyCountryExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool RetVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbCountryCode.Text != string.Empty) && (tbCountryCode.Text != null))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var RetValue = Service.GetCountryMasterList().EntityList.Where(x => x.CountryCD.Trim().ToUpper().Equals(tbCountryCode.Text.ToString().ToUpper().Trim()));
                            if (RetValue.Count() == 0 || RetValue == null)
                            {

                                cvCountry.IsValid = false;
                                tbCountryCode.Focus();

                                RetVal = true;
                            }

                            else
                            {
                                foreach (Country cm in RetValue)
                                {
                                    hdnCountryID.Value = cm.CountryID.ToString();
                                    tbCountryCode.Text = cm.CountryCD;
                                }
                                tbPostal.Focus();
                                RetVal = false;
                            }
                        }
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
                return RetVal;
            }

        }

        //protected void dgVendorContacts_ItemDataBound(object sender, GridCommandEventArgs e)
        //{
        //    if (e.Item is GridCommandItem)
        //    {
        //        //Added based on UWA requirement.
        //        LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
        //        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);

        //        LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
        //        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
        //    }
        //}

        /// <summary>
        /// Update Command for Delay Type Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>

        protected void dgVendorContacts_UpdateCommand(object source, GridCommandEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session["SelectedVendorContactID"] != null)
                        {

                            e.Canceled = true;
                            bool IsValidate = true;
                            if (CheckAlreadyCountryExist())
                            {
                                cvCountry.IsValid = false;
                                tbCountryCode.Focus();
                                IsValidate = false;
                            }
                            if (IsValidate)
                            {
                                e.Canceled = true;
                                using (FlightPakMasterService.MasterCatalogServiceClient VendorContactService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                   var objRetVal = VendorContactService.UpdateVendorContactMaster(GetItems());
                                    //For Data Anotation
                                   if (objRetVal.ReturnFlag == true)
                                   {
                                       e.Item.OwnerTableView.Rebind();
                                       e.Item.Selected = true;
                                       ///////Update Method UnLock
                                       using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                       {
                                           var returnValue = CommonService.UnLock(EntitySet.Database.VendorContact, Convert.ToInt64(Session["SelectedVendorContactID"].ToString().Trim()));
                                       }
                                       GridEnable(true, true, true);
                                       SelectItem();
                                       ReadOnlyForm();
                                       ShowSuccessMessage();
                                       _selectLastModified = true;
                                   }
                                   else
                                   {
                                       //For Data Anotation
                                       ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.VendorContact);
                                   }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.VendorContact);
                }
            }

        }

        protected void VendorContact_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgVendorContacts.ClientSettings.Scrolling.ScrollTop = "0";
                        VendorContactPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.VendorContact);
                }
            }
        }

        /// <summary>
        /// Insert Command for Vendor Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgVendorContacts_InsertCommand(object source, GridCommandEventArgs e)
        {
            //try
            //{

            //    e.Canceled = true;
            //    bool IsValidate = true;
            //    if (CheckAlreadyCountryExist())
            //    {
            //        cvCountry.IsValid = false;
            //        tbCountryCode.Focus();
            //        IsValidate = false;
            //    }
            //    if (IsValidate)
            //    {
            //        using (MasterCatalogServiceClient objVendorContactService = new MasterCatalogServiceClient())
            //        {
            //            var objRetVal = objVendorContactService.AddVendorContactMaster(GetItems(string.Empty));
            //            dgVendorContacts.Rebind();
            //            defaultSelection();
            //        }
            //    }


            //}
            //catch (System.NullReferenceException exc)
            //{
            //    e.Canceled = true;
            //    e.Item.OwnerTableView.IsItemInserted = false;
            //    throw exc;
            //}
            //catch (System.ArgumentOutOfRangeException exc)
            //{
            //    e.Canceled = true;
            //    e.Item.OwnerTableView.IsItemInserted = false;
            //    throw exc;
            //}
            //catch (System.InvalidCastException exc)
            //{
            //    e.Canceled = true;
            //    e.Item.OwnerTableView.IsItemInserted = false;
            //    throw exc;
            //}


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        if (CheckAlreadyCountryExist())
                        {
                            cvCountry.IsValid = false;
                            tbCountryCode.Focus();
                            IsValidate = false;
                        }
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient VendorContactService = new MasterCatalogServiceClient())
                            {
                                var objRetVal = VendorContactService.AddVendorContactMaster(GetItems());
                                //For Data Anotation
                                if (objRetVal.ReturnFlag == true)
                                {
                                    dgVendorContacts.Rebind();
                                    DefaultSelection(false);
                                    ShowSuccessMessage();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(objRetVal.ErrorMessage, ModuleNameConstants.Database.VendorContact);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.VendorContact);
                }
            }
        }

        //private bool checkAllReadyExist()
        //{
        //    bool returnVal = false;
        //    lstVendorContactCodes = (List<string>)Session["VendorContactCD"];
        //    if (lstVendorContactCodes != null && lstVendorContactCodes.Contains(tbCode.Text.ToString().ToLower().Trim()))
        //        return true;
        //    return returnVal;
        //}
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgVendorContacts_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedVendorContactID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient VendorContactService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.VendorContact VendorContact = new FlightPakMasterService.VendorContact();
                                string Code = Session["SelectedVendorContactID"].ToString();
                                strVendorContactCD = "";
                                strVendorContactID = "";
                                foreach (GridDataItem Item in dgVendorContacts.MasterTableView.Items)
                                {
                                    if (Item["VendorContactID"].Text.Trim() == Code.Trim())
                                    {
                                        strVendorContactCD = Item["VendorContactCD"].Text.Trim();
                                        strVendorContactID = Item["VendorContactID"].Text.Trim();
                                        strVendorID = Convert.ToInt64(Convert.ToString(Item.GetDataKeyValue("VendorID")));

                                        VendorContact.VendorType = Item.GetDataKeyValue("VendorType").ToString();


                                        VendorContact.CustomerID = Convert.ToInt64(Item.GetDataKeyValue("CustomerID").ToString());
                                        break;
                                    }
                                }
                                VendorContact.VendorContactCD = Convert.ToInt32(strVendorContactCD);
                                VendorContact.VendorContactID = Convert.ToInt64(strVendorContactID);
                                VendorContact.VendorID = strVendorID;
                                VendorContact.IsChoice = chkMainChoice.Checked;
                                VendorContact.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.VendorContact, Convert.ToInt64(Session["SelectedVendorContactID"].ToString()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.VendorContact);
                                        return;
                                    }
                                }
                                VendorContactService.DeleteVendorContactMaster(VendorContact);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                dgVendorContacts.Rebind();
                                DefaultSelection(false);
                                GridEnable(true, true, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.VendorContact);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer                           
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.VendorContact, Convert.ToInt64(Session["SelectedVendorContactID"].ToString()));
                    }
                }
            }
        }

        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgVendorContacts_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {

                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem Item = dgVendorContacts.SelectedItems[0] as GridDataItem;
                                if (Item.GetDataKeyValue("VendorContactID") != null)
                                {
                                    Session["SelectedVendorContactID"] = Item.GetDataKeyValue("VendorContactID").ToString();
                                    ReadOnlyForm();
                                    GridEnable(true, true, true);
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);

                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.VendorContact);
                    }
                }
            }
        }

        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>

        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgVendorContacts.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgVendorContacts.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.VendorContact);
                }
            }
        }

        /// <summary>
        /// Cancel Vendor Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            ////pnlExternalForm.Visible = false;
            //Session["SelectedItem"] = null;
            //GridEnable(true, true, true);
            ////btnSaveChanges.Visible = false;
            ////btnCancel.Visible = false;
            //ClearForm();
            //EnableForm(false);
            //tbCode.ReadOnly = true;
            //defaultSelection();



            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedVendorContactID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.VendorContact, Convert.ToInt64(Session["SelectedVendorContactID"].ToString().Trim()));
                                    //Session["SelectedPayableVendorID"] = null;
                                    DefaultSelection(false);
                                }
                            }
                        }
                        DefaultSelection(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.VendorContact);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void GridEnable(bool Add, bool Edit, bool Delete)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtnInsertCtl = (LinkButton)dgVendorContacts.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                    LinkButton lbtnDelCtl = (LinkButton)dgVendorContacts.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                    LinkButton lbtnEditCtl = (LinkButton)dgVendorContacts.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                    //if (IsAuthorized(Permission.Database.AddVendorContacts))
                    //{
                    //    lbtnInsertCtl.Visible = true;
                    if (Add)
                    {
                        lbtnInsertCtl.Enabled = true;
                    }
                    else
                    {
                        lbtnInsertCtl.Enabled = false;
                    }
                    //}
                    //else
                    //{
                    //    lbtnInsertCtl.Visible = false;
                    //}
                    //if (IsAuthorized(Permission.Database.DeleteVendorContacts))
                    //{
                    //    lbtnDelCtl.Visible = true;
                    if (Delete)
                    {
                        lbtnDelCtl.Enabled = true;
                        lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete();";
                    }
                    else
                    {
                        lbtnDelCtl.Enabled = false;
                        lbtnDelCtl.OnClientClick = string.Empty;
                    }
                    //}
                    //else
                    //{
                    //    lbtnDelCtl.Visible = false;
                    //}
                    //if (IsAuthorized(Permission.Database.EditVendorContacts))
                    //{
                    //    lbtnEditCtl.Visible = true;
                    if (Edit)
                    {
                        lbtnEditCtl.Enabled = true;
                        lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                    }
                    else
                    {
                        lbtnEditCtl.Enabled = false;
                        lbtnEditCtl.OnClientClick = string.Empty;
                    }
                    //}
                    //else
                    //{
                    //    lbtnEditCtl.Visible = false;
                    //}
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>

        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSave.Value = "Save";
                    ClearForm();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    chkMainChoice.Checked = false;
                    tbContactName.Text = string.Empty;
                    tbFirstName.Text = string.Empty;
                    tbMiddleName.Text = string.Empty;
                    tbLastName.Text = string.Empty;
                    tbTitle.Text = string.Empty;
                    tbAddr1.Text = string.Empty;
                    tbAddr2.Text = string.Empty;
                    tbCity.Text = string.Empty;
                    tbState.Text = string.Empty;
                    tbPostal.Text = string.Empty;
                    tbCountryCode.Text = string.Empty;
                    tbPhone.Text = string.Empty;
                    tbFax.Text = string.Empty;
                    tbEmailId.Text = string.Empty;
                    tbAdditionalContact.Text = string.Empty;
                    tbNotes.Text = string.Empty;

                    tbTollFreePhone.Text = string.Empty;
                    tbBusinessEmail.Text = string.Empty;
                    tbWebsite.Text = string.Empty;
                    tbAddr3.Text = string.Empty;
                    tbBusinessPhone.Text = string.Empty;
                    tbCellPhoneNum.Text = string.Empty;
                    tbHomeFax.Text = string.Empty;
                    tbCellPhoneNum2.Text = string.Empty;
                    tbOtherPhone.Text = string.Empty;
                    tbPersonalEmail.Text = string.Empty;
                    tbOtherEmail.Text = string.Empty;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Enabled = false;
                    chkMainChoice.Enabled = enable;
                    tbContactName.Enabled = enable;
                    tbFirstName.Enabled = enable;
                    tbMiddleName.Enabled = enable;
                    tbLastName.Enabled = enable;
                    tbTitle.Enabled = enable;
                    tbAddr1.Enabled = enable;
                    tbAddr2.Enabled = enable;
                    tbCity.Enabled = enable;
                    btnCountry.Enabled = enable;
                    tbState.Enabled = enable;
                    tbPostal.Enabled = enable;
                    tbCountryCode.Enabled = enable;
                    tbPhone.Enabled = enable;
                    tbFax.Enabled = enable;
                    tbEmailId.Enabled = enable;
                    tbAdditionalContact.Enabled = enable;
                    tbNotes.Enabled = enable;
                    btnSaveChanges.Visible = enable;
                    btnSaveChangesTop.Visible = enable;
                    btnCancel.Visible = enable;
                    btnCancelTop.Visible = enable;

                    tbTollFreePhone.Enabled = enable;
                    tbBusinessEmail.Enabled = enable;
                    tbWebsite.Enabled = enable;
                    tbAddr3.Enabled = enable;
                    tbBusinessPhone.Enabled = enable;
                    tbCellPhoneNum.Enabled = enable;
                    tbHomeFax.Enabled = enable;
                    tbCellPhoneNum2.Enabled = enable;
                    tbOtherPhone.Enabled = enable;
                    tbPersonalEmail.Enabled = enable;
                    tbOtherEmail.Enabled = enable;

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>

        protected void DisplayEditForm()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    //tbCode.ReadOnly = true;
                    //tbCode.BackColor = System.Drawing.Color.LightGray;
                    hdnSave.Value = "Update";
                    hdnRedirect.Value = "";
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }


            //GridDataItem item = (GridDataItem)Session["SelectedItem"];
            //tbCode.Text = item.GetDataKeyValue("VendorContactCD").ToString().Trim();
            //if (item.GetDataKeyValue("ContactName") != null)
            //{
            //    tbContactName.Text = item.GetDataKeyValue("ContactName").ToString().Trim();
            //}
            //else
            //{
            //    tbContactName.Text = string.Empty;
            //}
            //if (item.GetDataKeyValue(ConstIsInactive) != null)
            //{
            //    chkMainChoice.Checked = Convert.ToBoolean(item.GetDataKeyValue(ConstIsInactive).ToString(), CultureInfo.CurrentCulture);
            //}
            //else
            //{
            //    chkMainChoice.Checked = false;
            //}
            //if (item.GetDataKeyValue("FirstName") != null)
            //{
            //    tbFirstName.Text = item.GetDataKeyValue("FirstName").ToString();
            //}
            //else
            //{
            //    tbFirstName.Text = string.Empty;
            //}
            //if (item.GetDataKeyValue("LastName") != null)
            //{
            //    tbLastName.Text = item.GetDataKeyValue("LastName").ToString();
            //}
            //else
            //{
            //    tbContactName.Text = string.Empty;
            //}
            //if (item.GetDataKeyValue("MiddleName") != null)
            //{
            //    tbMiddleName.Text = item.GetDataKeyValue("MiddleName").ToString();
            //}
            //else
            //{
            //    tbMiddleName.Text = string.Empty;
            //}
            //if (item.GetDataKeyValue("Title") != null)
            //{
            //    tbTitle.Text = item.GetDataKeyValue("Title").ToString();
            //}
            //else
            //{
            //    tbTitle.Text = string.Empty;
            //}
            //if (item.GetDataKeyValue("Addr1") != null)
            //{
            //    tbAddr1.Text = item.GetDataKeyValue("Addr1").ToString();
            //}
            //else
            //{
            //    tbAddr1.Text = string.Empty;
            //}
            //if (item.GetDataKeyValue("Addr2") != null)
            //{
            //    tbAddr2.Text = item.GetDataKeyValue("Addr2").ToString();
            //}
            //else
            //{
            //    tbAddr2.Text = string.Empty;
            //}
            //if (item.GetDataKeyValue("CityName") != null)
            //{
            //    tbCity.Text = item.GetDataKeyValue("CityName").ToString();
            //}
            //else
            //{
            //    tbCity.Text = string.Empty;
            //}
            //if (item.GetDataKeyValue("StateName") != null)
            //{
            //    tbState.Text = item.GetDataKeyValue("StateName").ToString();
            //}
            //else
            //{
            //    tbState.Text = string.Empty;
            //}
            //if (item.GetDataKeyValue("PostalZipCD") != null)
            //{
            //    tbPostal.Text = item.GetDataKeyValue("PostalZipCD").ToString();
            //}
            //else
            //{
            //    tbPostal.Text = string.Empty;
            //}
            //if (item.GetDataKeyValue("CountryCD") != null)
            //{
            //    tbCountryCode.Text = item.GetDataKeyValue("CountryCD").ToString();
            //}
            //else
            //{
            //    tbCountryCode.Text = string.Empty;
            //}
            //if (item.GetDataKeyValue("PhoneNum") != null)
            //{
            //    tbPhone.Text = item.GetDataKeyValue("PhoneNum").ToString();
            //}
            //else
            //{
            //    tbPhone.Text = string.Empty;
            //}
            //if (item.GetDataKeyValue("FaxNum") != null)
            //{
            //    tbFax.Text = item.GetDataKeyValue("FaxNum").ToString();
            //}
            //else
            //{
            //    tbFax.Text = string.Empty;
            //}
            //if (item.GetDataKeyValue("EmailID") != null)
            //{
            //    tbEmailId.Text = item.GetDataKeyValue("EmailID").ToString();
            //}
            //else
            //{
            //    tbEmailId.Text = string.Empty;
            //}
            //if (item.GetDataKeyValue("MoreInfo") != null)
            //{
            //    tbAdditionalContact.Text = item.GetDataKeyValue("MoreInfo").ToString();
            //}
            //else
            //{
            //    tbAdditionalContact.Text = string.Empty;
            //}
            //tbCode.ReadOnly = true;
            //tbCode.BackColor = System.Drawing.Color.LightGray;
            //// pnlExternalForm.Visible = true;
            //hdnSave.Value = "Update";
            //btnCancel.Visible = true;
            //btnSaveChanges.Visible = true;
            ////  dgVendorContacts.Rebind();
            ////  item.Selected = true;
            //EnableForm(true);
        }

        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedVendorContactID"] != null)
                    {
                        strVendorContactID = "";
                        foreach (GridDataItem Item in dgVendorContacts.MasterTableView.Items)
                        {
                            if (Item["VendorContactID"].Text.Trim() == Session["SelectedVendorContactID"].ToString().Trim())
                            {

                                tbCode.Text = Item.GetDataKeyValue("VendorContactCD").ToString().Trim();
                                if (Item.GetDataKeyValue("ContactName") != null)
                                {
                                    tbContactName.Text = Item.GetDataKeyValue("ContactName").ToString().Trim();
                                }
                                else
                                {
                                    tbContactName.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("IsChoice") != null)
                                {
                                    chkMainChoice.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsChoice").ToString());
                                }
                                else
                                {
                                    chkMainChoice.Checked = false;
                                }
                                if (Item.GetDataKeyValue("FirstName") != null)
                                {
                                    tbFirstName.Text = Item.GetDataKeyValue("FirstName").ToString().Trim();
                                }
                                else
                                {
                                    tbFirstName.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("LastName") != null)
                                {
                                    tbLastName.Text = Item.GetDataKeyValue("LastName").ToString().Trim();
                                }
                                else
                                {
                                    tbLastName.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("MiddleName") != null)
                                {
                                    tbMiddleName.Text = Item.GetDataKeyValue("MiddleName").ToString().Trim();
                                }
                                else
                                {
                                    tbMiddleName.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Title") != null)
                                {
                                    tbTitle.Text = Item.GetDataKeyValue("Title").ToString().Trim();
                                }
                                else
                                {
                                    tbTitle.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Addr1") != null)
                                {
                                    tbAddr1.Text = Item.GetDataKeyValue("Addr1").ToString().Trim();
                                }
                                else
                                {
                                    tbAddr1.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Addr2") != null)
                                {
                                    tbAddr2.Text = Item.GetDataKeyValue("Addr2").ToString().Trim();
                                }
                                else
                                {
                                    tbAddr2.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("CityName") != null)
                                {
                                    tbCity.Text = Item.GetDataKeyValue("CityName").ToString().Trim();
                                }
                                else
                                {
                                    tbCity.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("StateName") != null)
                                {
                                    tbState.Text = Item.GetDataKeyValue("StateName").ToString().Trim();
                                }
                                else
                                {
                                    tbState.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("PostalZipCD") != null)
                                {
                                    tbPostal.Text = Item.GetDataKeyValue("PostalZipCD").ToString().Trim();
                                }
                                else
                                {
                                    tbPostal.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("CountryCD") != null)
                                {
                                    tbCountryCode.Text = Item.GetDataKeyValue("CountryCD").ToString().Trim();
                                }
                                else
                                {
                                    tbCountryCode.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("PhoneNum") != null)
                                {
                                    tbPhone.Text = Item.GetDataKeyValue("PhoneNum").ToString().Trim();
                                }
                                else
                                {
                                    tbPhone.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("FaxNum") != null)
                                {
                                    tbFax.Text = Item.GetDataKeyValue("FaxNum").ToString().Trim();
                                }
                                else
                                {
                                    tbFax.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("EmailID") != null)
                                {
                                    tbEmailId.Text = Item.GetDataKeyValue("EmailID").ToString().Trim();
                                }
                                else
                                {
                                    tbEmailId.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("MoreInfo") != null)
                                {
                                    tbAdditionalContact.Text = Item.GetDataKeyValue("MoreInfo").ToString().Trim();
                                }
                                else
                                {
                                    tbAdditionalContact.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Notes") != null)
                                {
                                    tbNotes.Text = Item.GetDataKeyValue("Notes").ToString().Trim();
                                }
                                else
                                {
                                    tbNotes.Text = string.Empty;
                                }

                                if (Item.GetDataKeyValue("TollFreePhone") != null)
                                {
                                    tbTollFreePhone.Text = Item.GetDataKeyValue("TollFreePhone").ToString().Trim();
                                }
                                else
                                {
                                    tbTollFreePhone.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("BusinessEmail") != null)
                                {
                                    tbBusinessEmail.Text = Item.GetDataKeyValue("BusinessEmail").ToString().Trim();
                                }
                                else
                                {
                                    tbBusinessEmail.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Website") != null)
                                {
                                    tbWebsite.Text = Item.GetDataKeyValue("Website").ToString().Trim();
                                }
                                else
                                {
                                    tbWebsite.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Addr3") != null)
                                {
                                    tbAddr3.Text = Item.GetDataKeyValue("Addr3").ToString().Trim();
                                }
                                else
                                {
                                    tbAddr3.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("BusinessPhone") != null)
                                {
                                    tbBusinessPhone.Text = Item.GetDataKeyValue("BusinessPhone").ToString().Trim();
                                }
                                else
                                {
                                    tbBusinessPhone.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("CellPhoneNum") != null)
                                {
                                    tbCellPhoneNum.Text = Item.GetDataKeyValue("CellPhoneNum").ToString().Trim();
                                }
                                else
                                {
                                    tbCellPhoneNum.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("HomeFax") != null)
                                {
                                    tbHomeFax.Text = Item.GetDataKeyValue("HomeFax").ToString().Trim();
                                }
                                else
                                {
                                    tbHomeFax.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("CellPhoneNum2") != null)
                                {
                                    tbCellPhoneNum2.Text = Item.GetDataKeyValue("CellPhoneNum2").ToString().Trim();
                                }
                                else
                                {
                                    tbCellPhoneNum2.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("OtherPhone") != null)
                                {
                                    tbOtherPhone.Text = Item.GetDataKeyValue("OtherPhone").ToString().Trim();
                                }
                                else
                                {
                                    tbOtherPhone.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("PersonalEmail") != null)
                                {
                                    tbPersonalEmail.Text = Item.GetDataKeyValue("PersonalEmail").ToString().Trim();
                                }
                                else
                                {
                                    tbPersonalEmail.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("OtherEmail") != null)
                                {
                                    tbOtherEmail.Text = Item.GetDataKeyValue("OtherEmail").ToString().Trim();
                                }
                                else
                                {
                                    tbOtherEmail.Text = string.Empty;
                                }

                                lbColumnName1.Text = "First Name";
                                lbColumnName2.Text = "Last Name";
                                lbColumnValue1.Text = Item["FirstName"].Text;
                                lbColumnValue2.Text = Item["LastName"].Text;
                                break;
                            }
                            
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    GridDataItem Item = dgVendorContacts.SelectedItems[0] as GridDataItem;
                    Label lbLastUpdatedUser = (Label)dgVendorContacts.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                    if (Item.GetDataKeyValue("LastUpdUID") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                    }
                    else
                    {
                        lbLastUpdatedUser.Text = string.Empty;
                    }
                    if (Item.GetDataKeyValue("LastUpdTS") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                    }
                    LoadControlData();
                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private VendorContact GetItems()
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.VendorContact VendorContactService = new FlightPakMasterService.VendorContact();

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {




                    if (hdnSave.Value == "Update")
                    {
                        if (Session["SelectedVendorContactID"] != null)
                            VendorContactService.VendorContactID = Convert.ToInt64(Session["SelectedVendorContactID"].ToString());
                    }
                    VendorContactService.VendorID = Convert.ToInt64(Request.QueryString["VendorID"].ToString());


                    VendorContactService.VendorType = Request.QueryString["VendorType"].ToString();

                    if (!string.IsNullOrEmpty(tbContactName.Text))
                    {
                        VendorContactService.ContactName = tbContactName.Text.Trim();
                    }

                    VendorContactService.IsChoice = chkMainChoice.Checked;

                    if (!string.IsNullOrEmpty(tbAddr1.Text))
                    {
                        VendorContactService.Addr1 = tbAddr1.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbAddr2.Text))
                    {
                        VendorContactService.Addr2 = tbAddr2.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbCity.Text))
                    {
                        VendorContactService.CityName = tbCity.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbState.Text))
                    {
                        VendorContactService.StateName = tbState.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbPostal.Text))
                    {
                        VendorContactService.PostalZipCD = tbPostal.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbFax.Text))
                    {
                        VendorContactService.FaxNum = tbFax.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbPhone.Text))
                    {
                        VendorContactService.PhoneNum = tbPhone.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbCountryCode.Text))
                    {
                        VendorContactService.CountryID = Convert.ToInt64(hdnCountryID.Value);

                    }
                    if (!string.IsNullOrEmpty(tbAdditionalContact.Text))
                    {
                        VendorContactService.MoreInfo = tbAdditionalContact.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbTitle.Text))
                    {
                        VendorContactService.Title = tbTitle.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbEmailId.Text))
                    {
                        VendorContactService.EmailID = tbEmailId.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbLastName.Text))
                    {
                        VendorContactService.LastName = tbLastName.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbMiddleName.Text))
                    {
                        VendorContactService.MiddleName = tbMiddleName.Text.Trim();
                    }
                    if (!string.IsNullOrEmpty(tbFirstName.Text))
                    {
                        VendorContactService.FirstName = tbFirstName.Text.Trim();
                    }

                    if (!string.IsNullOrEmpty(tbNotes.Text))
                    {
                        VendorContactService.Notes = tbNotes.Text;
                    }
                    VendorContactService.IsDeleted = false;

                    if (!string.IsNullOrEmpty(tbTollFreePhone.Text))
                    {
                        VendorContactService.TollFreePhone = tbTollFreePhone.Text;
                    }
                    if (!string.IsNullOrEmpty(tbBusinessEmail.Text))
                    {
                        VendorContactService.BusinessEmail = tbBusinessEmail.Text;
                    }
                    if (!string.IsNullOrEmpty(tbWebsite.Text))
                    {
                        VendorContactService.Website = tbWebsite.Text;
                    }
                    if (!string.IsNullOrEmpty(tbAddr3.Text))
                    {
                        VendorContactService.Addr3 = tbAddr3.Text;
                    }
                    if (!string.IsNullOrEmpty(tbBusinessPhone.Text))
                    {
                        VendorContactService.BusinessPhone = tbBusinessPhone.Text;
                    }
                    if (!string.IsNullOrEmpty(tbCellPhoneNum.Text))
                    {
                        VendorContactService.CellPhoneNum = tbCellPhoneNum.Text;
                    }
                    if (!string.IsNullOrEmpty(tbHomeFax.Text))
                    {
                        VendorContactService.HomeFax = tbHomeFax.Text;
                    }
                    if (!string.IsNullOrEmpty(tbCellPhoneNum2.Text))
                    {
                        VendorContactService.CellPhoneNum2 = tbCellPhoneNum2.Text;
                    }
                    if (!string.IsNullOrEmpty(tbOtherPhone.Text))
                    {
                        VendorContactService.OtherPhone = tbOtherPhone.Text;
                    }
                    if (!string.IsNullOrEmpty(tbPersonalEmail.Text))
                    {
                        VendorContactService.PersonalEmail = tbPersonalEmail.Text;
                    }
                    if (!string.IsNullOrEmpty(tbOtherEmail.Text))
                    {
                        VendorContactService.OtherEmail = tbOtherEmail.Text;
                    }

                    //break;
                    //    }
                    //}

                }, FlightPak.Common.Constants.Policy.UILayer);

                return VendorContactService;
            }

        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgVendorContacts.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            GetAllVendorContact vendorContact = new GetAllVendorContact();
            vendorContact.VendorID = Convert.ToInt64(Server.UrlDecode(Request.QueryString["VendorID"].ToString()));
            vendorContact.VendorType = Request.QueryString["VendorType"].ToString();
            var ContactValue = FPKMstService.GetVendorContactMasterInfo(vendorContact);

            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, ContactValue);
            List<FlightPakMasterService.GetAllVendorContact> filteredList = GetFilteredList(ContactValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.VendorContactID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgVendorContacts.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgVendorContacts.CurrentPageIndex = PageNumber;
            dgVendorContacts.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllVendorContact ContactValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedVendorContactID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = ContactValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().VendorContactID;
                Session["SelectedVendorContactID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllVendorContact> GetFilteredList(ReturnValueOfGetAllVendorContact ContactValue)
        {
            List<FlightPakMasterService.GetAllVendorContact> filteredList = new List<FlightPakMasterService.GetAllVendorContact>();

            if (ContactValue.ReturnFlag)
            {
                filteredList = ContactValue.EntityList;
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgVendorContacts.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgVendorContacts.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
    }

}
