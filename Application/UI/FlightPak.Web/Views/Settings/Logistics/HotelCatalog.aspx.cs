﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using Telerik.Web.UI;
using System.Drawing;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Web.Views.Settings.Logistics
{
    public partial class HotelCatalog : BaseSecuredPage
    {
        private bool IsValidateCustom = true;
        private List<string> LstHotelCode = new List<string>();
        private ExceptionManager exManager;
        private bool HotelPageNavigated = false;
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgHotelCatalog, dgHotelCatalog, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgHotelCatalog.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewHotel);
                            if (Request.QueryString["IcaoID"] != null)
                            {
                                tbAirportICAO.Text = Server.UrlDecode(Request.QueryString["IcaoID"].ToString());
                            }
                            if (Request.QueryString["CityName"] != null)
                            {
                                tbAirportCity.Text = Server.UrlDecode(Request.QueryString["CityName"].ToString());
                            }
                            if (Request.QueryString["StateName"] != null)
                            {
                                tbAirportState.Text = Server.UrlDecode(Request.QueryString["StateName"].ToString());
                            }
                            if (Request.QueryString["CountryName"] != null)
                            {
                                tbAirportCountry.Text = Server.UrlDecode(Request.QueryString["CountryName"].ToString());
                            }
                            if (Request.QueryString["AirportName"] != null)
                            {
                                tbAirport.Text = Server.UrlDecode(Request.QueryString["AirportName"].ToString());
                            }
                            if (Request.QueryString["IcaoID"] != null)
                            {
                                tbDistanceFromICAO.Text = Server.UrlDecode(Request.QueryString["IcaoID"].ToString());
                                tbDistanceFromICAO.ReadOnly = true;
                                tbDistanceFromICAO.BackColor = System.Drawing.Color.LightGray;
                            }
                            if (Request.QueryString["IcaoID"] != null)
                            {
                                tbHotelCode.Text = Server.UrlDecode(Request.QueryString["IcaoID"].ToString()) + "-";
                                tbHotelCode.ReadOnly = true;
                                tbHotelCode.BackColor = System.Drawing.Color.LightGray;
                            }
                            if (Request.QueryString["AirportID"] != null)
                            {
                                hdnAirportID.Value = Server.UrlDecode(Request.QueryString["AirportID"].ToString());
                            }
                            // Method to display first record in read only format
                            DefaultSelection(true);
                        }
                        if (IsPopUp)
                        {
                            dgHotelCatalog.AllowPaging = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
                }
            }
        }
        /// <summary>
        /// To Display first record as default
        /// </summary>       
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPopUp)
                        {
                            if (BindDataSwitch)
                            {
                                dgHotelCatalog.Rebind();
                            }
                            if (dgHotelCatalog.MasterTableView.Items.Count > 0)
                            {
                                //if (!IsPostBack)
                                //{
                                //    Session["SelectedHotelID"] = null;
                                //}
                                if (Session["SelectedHotelID"] == null)
                                {
                                    dgHotelCatalog.SelectedIndexes.Add(0);

                                    if (!IsPopUp)
                                    {
                                        Session["SelectedHotelID"] = dgHotelCatalog.Items[0].GetDataKeyValue("HotelID").ToString();
                                    }
                                }

                                if (dgHotelCatalog.SelectedIndexes.Count == 0)
                                    dgHotelCatalog.SelectedIndexes.Add(0);

                                ReadOnlyForm();
                            }
                            else
                            {
                                ClearForm();
                                EnableForm(false);
                                //chkDisplayInctive.Enabled = false;
                                //chkCrewOnly.Enabled = false;
                                //chkPassengerOnly.Enabled = false;
                            }
                            GridEnable(true, true, true, true);
                        }
                        if (IsViewOnly)
                        {
                            if (BindDataSwitch)
                            {
                                dgHotelCatalog.Rebind();
                            }
                            if (dgHotelCatalog.MasterTableView.Items.Count > 0)
                            {
                                if (!IsPostBack)
                                {
                                    Session["SelectedHotelID"] = null;
                                }
                                if (Session["SelectedHotelID"] == null)
                                {
                                    dgHotelCatalog.SelectedIndexes.Add(0);
                                    Session["SelectedHotelID"] = dgHotelCatalog.Items[0].GetDataKeyValue("HotelID").ToString();
                                }

                                if (dgHotelCatalog.SelectedIndexes.Count == 0)
                                    dgHotelCatalog.SelectedIndexes.Add(0);

                                ReadOnlyForm();

                            }
                            else
                            {
                                ClearForm();
                                EnableForm(false);
                                //chkDisplayInctive.Enabled = false;
                                //chkCrewOnly.Enabled = false;
                                //chkPassengerOnly.Enabled = false;
                            }
                            GridEnable(true, true, true, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
                }
            }
        }
        /// <summary>
        /// To change the page index on paging        
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgHotelCatalog_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgHotelCatalog.ClientSettings.Scrolling.ScrollTop = "0";
                        HotelPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgHotelCatalog_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveFlag.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (HotelPageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgHotelCatalog, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
                }
            }
        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        protected void dgHotelCatalog_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnInitEdit = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInitEdit, divExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInitInsert = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInitInsert, divExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
                }
            }
        }
        /// <summary>
        /// To display UWA record in blue color in filter column grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgHotelCatalog_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            string Color = System.Configuration.ConfigurationManager.AppSettings["UWAColor"];
                            GridDataItem DataItem = e.Item as GridDataItem;
                            GridColumn Column = dgHotelCatalog.MasterTableView.GetColumn("chkUWAID");
                            string UwaValue = DataItem["chkUWAID"].Text.Trim();
                            GridDataItem Item = (GridDataItem)e.Item;
                            // e.Item.Cells[6].Text = "UWA";
                            TableCell cell = (TableCell)Item["chkUWAID"];
                            if (UwaValue.ToUpper().Trim() == "UWA")
                            {
                                System.Drawing.ColorConverter ColourConvert = new ColorConverter();
                                cell.ForeColor = (System.Drawing.Color)ColourConvert.ConvertFromString(Color);
                                cell.Font.Bold = true;
                                //  cell.Text = "UWA";
                            }
                            else
                            {
                                // cell.Text = "CUSTOM";
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
                }
            }
        }
        /// <summary>
        /// Bind Hotel Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgHotelCatalog_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient HotelService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (IsPopUp && Session["SelectedAirportIDforHotelPopup"] != null)
                            {
                                hdnAirportID.Value = Session["SelectedAirportIDforHotelPopup"].ToString();
                            }
                            if (!string.IsNullOrEmpty(hdnAirportID.Value))
                            {
                                var HotelValue = HotelService.GetHotelsByAirportID(Convert.ToInt64(hdnAirportID.Value));
                                Session["HotelCD"] = HotelValue.EntityList.ToList();
                                if (HotelValue.ReturnFlag == true)
                                {
                                    if (!IsPopUp)
                                    {
                                        if (chkDisplayInctive.Checked == true)
                                        {
                                            dgHotelCatalog.DataSource = HotelValue.EntityList.Where(x => x.IsDeleted.ToString().ToUpper().Trim().Equals("FALSE")).ToList().GroupBy(t => t.Name).Select(y => y.First()).ToList();
                                        }
                                        else
                                        {
                                            dgHotelCatalog.DataSource = HotelValue.EntityList.Where(x => x.IsDeleted.ToString().ToUpper().Trim().Equals("FALSE") && x.IsInActive.ToString().ToUpper().Trim() == "FALSE").GroupBy(t => t.Name).Select(y => y.First()).ToList();
                                        }
                                        if (chkCrewOnly.Checked == true)
                                        {
                                            dgHotelCatalog.DataSource = HotelValue.EntityList.Where(x => x.IsCrew.ToString().ToUpper().Trim().Equals("TRUE")).ToList().GroupBy(t => t.Name).Select(y => y.First()).ToList();
                                        }
                                        if (chkPassengerOnly.Checked == true)
                                        {
                                            dgHotelCatalog.DataSource = HotelValue.EntityList.Where(x => x.IsPassenger.ToString().ToUpper().Trim().Equals("TRUE")).ToList().GroupBy(t => t.Name).Select(y => y.First()).ToList();
                                        }
                                    }
                                    else
                                    {
                                        dgHotelCatalog.DataSource = HotelValue.EntityList.GroupBy(t => t.Name).Select(y => y.First()).ToList();
                                    }
                                }
                                //Session["HotelCD"] = (List<GetHotelsByAirportID>)HotelValue.EntityList.ToList();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
                }
            }
        }
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedHotelID"] != null)
                    {
                        string ID = Convert.ToString(Session["SelectedHotelID"]).Trim();
                        foreach (GridDataItem Item in dgHotelCatalog.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("HotelID").ToString().Trim() == ID)
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Bind Hotel Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgHotelCatalog_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSaveFlag.Value = "Update";
                                if (Session["SelectedHotelID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.Hotel, Convert.ToInt64(Session["SelectedHotelID"].ToString().Trim()));
                                        Session["IsHotelEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Hotel);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Hotel);
                                            SelectItem();
                                            return;
                                        }
                                        DisplayEditForm();
                                        GridEnable(false, true, false, false);
                                        SelectItem();
                                    }
                                }
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbName);
                                //tbName.Focus();
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgHotelCatalog.SelectedIndexes.Clear();
                                tbHotelCode.ReadOnly = true;
                                tbHotelCode.BackColor = System.Drawing.Color.White;
                                DisplayInsertForm();
                                GridEnable(true, false, false, false);
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbName);
                                // tbName.Focus();
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
                }
            }
        }
        /// <summary>
        /// Update Command for Hotel Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgHotelCatalog_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        CheckCustomValidator();
                        CheckExchangeRateExist();
                        if (IsValidateCustom)
                        {
                            if (Session["SelectedHotelID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objHotelService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var HotelValue = objHotelService.UpdateHotel(GetItems());
                                    if (HotelValue.ReturnFlag == true)
                                    {
                                        ShowSuccessMessage();

                                        ///////Update Method UnLock
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.Hotel, Convert.ToInt64(Session["SelectedHotelID"].ToString().Trim()));
                                        }
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        GridEnable(true, true, true, true);
                                        DefaultSelection(true);
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(HotelValue.ErrorMessage, ModuleNameConstants.Database.Hotel);
                                    }
                                }
                            }
                        }
                        if (IsPopUp && (!IsViewOnly))
                        {
                            //Clear session & close browser
                            //Session["SelectedHotelID"] = null;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
                }
            }
        }
        /// <summary>
        /// Update Command for Hotel Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgHotelCatalog_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        CheckCustomValidator();
                        CheckExchangeRateExist();
                        if (IsValidateCustom)
                        {
                            if (IsHotelExists())
                            {

                                string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Hotel already exists', 360, 50, 'Hotel');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                            }
                            else
                            {
                                using (MasterCatalogServiceClient HotelCatalogService = new MasterCatalogServiceClient())
                                {
                                    var HotelValue = HotelCatalogService.AddHotel(GetItems());
                                    if (HotelValue.ReturnFlag == true)
                                    {
                                        ShowSuccessMessage();

                                        dgHotelCatalog.Rebind();
                                        DefaultSelection(false);
                                        GridEnable(true, true, true, true);
                                        _selectLastModified = true;
                                        if (IsPopUp && (!IsViewOnly))
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                                        }
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(HotelValue.ErrorMessage, ModuleNameConstants.Database.Hotel);
                                    }
                                }
                            }
                        }
                       
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
                }
            }
        }
        private bool IsHotelExists()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    List<FlightPakMasterService.GetHotelsByAirportID> lstHotel = new List<FlightPakMasterService.GetHotelsByAirportID>();
                    if (Session["HotelCD"] != null)
                    {
                        lstHotel = (List<FlightPakMasterService.GetHotelsByAirportID>)Session["HotelCD"];
                    }

                    var searchHotel = lstHotel.Where(x => x.Name.ToString().ToUpper().Trim().Equals(tbName.Text.ToString().ToUpper().Trim())).ToList();
                    if (searchHotel.Count > 0)
                    {
                        ReturnVal = true;
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);

                return ReturnVal;
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgHotelCatalog_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient HotelService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            FlightPakMasterService.Hotel HotelData = new FlightPakMasterService.Hotel();
                            string Code = Convert.ToString(Session["SelectedHotelID"]).Trim();
                            string AirportID = "", UWAMaintFlag = "";
                            foreach (GridDataItem Item in dgHotelCatalog.MasterTableView.Items)
                            {
                                if (Item["HotelID"].Text.Trim() == Code)
                                {
                                    AirportID = Item["AirportID"].Text.Trim().Replace("&nbsp;", "");
                                    UWAMaintFlag = Item["UWAMaintFlag"].Text.Trim().Replace("&nbsp;", "");
                                    break;
                                }
                            }
                            HotelData.AirportID = Convert.ToInt64(AirportID);
                            HotelData.HotelID = Convert.ToInt64(Code);
                            HotelData.IsDeleted = true;
                            if (!string.IsNullOrEmpty(UWAMaintFlag))
                            {
                                HotelData.UWAMaintFlag = Convert.ToBoolean(UWAMaintFlag);
                            }
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.Lock(EntitySet.Database.Hotel, Convert.ToInt64(Session["SelectedHotelID"].ToString().Trim()));
                                if (!returnValue.ReturnFlag)
                                {
                                    e.Item.Selected = true;
                                    DefaultSelection(false);
                                    if (IsPopUp)
                                        ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Hotel);
                                    else
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Hotel);
                                    return;
                                }
                            }
                            HotelService.DeleteHotel(HotelData);
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = true;
                            ClearForm();
                            DefaultSelection(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
                }
                finally
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.Hotel, Convert.ToInt64(Session["SelectedHotelID"].ToString().Trim()));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgHotelCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSaveFlag.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {

                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgHotelCatalog.SelectedItems[0] as GridDataItem;
                                Session["SelectedHotelID"] = item["HotelID"].Text;
                                ReadOnlyForm();
                                GridEnable(true, true, true, true);
                            }

                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
                    }
                }
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgHotelCatalog;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
                }
            }
        }
        /// <summary>
        /// To Display all the fields
        /// </summary>       
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnSaveFlag.Value = "Save";
                    tbHotelCode.ReadOnly = true;
                    tbHotelCode.BackColor = System.Drawing.Color.LightGray;
                    tbDistanceFromICAO.ReadOnly = true;
                    tbDistanceFromICAO.BackColor = System.Drawing.Color.LightGray;
                    ClearForm();
                    if (!IsPopUp)
                    {
                        tbHotelCode.Text = Request.QueryString["IcaoID"].ToString() + " - ";
                        tbDistanceFromICAO.Text = Request.QueryString["IcaoID"].ToString();
                    }
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Clear the form
        /// </summary>        
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbHotelCode.Text = string.Empty;
                    tbName.Text = string.Empty;
                    tbAddr1.Text = string.Empty;
                    tbAddr2.Text = string.Empty;
                    tbAddr3.Text = string.Empty;
                    tbCity.Text = string.Empty;
                    tbMetro.Text = string.Empty;
                    tbStateProv.Text = string.Empty;
                    tbPostalCode.Text = string.Empty;
                    tbCtry.Text = string.Empty;
                    tbLatDeg.Text = string.Empty;
                    tbLatMin.Text = string.Empty;
                    tbLatNS.Text = string.Empty;
                    tbLongDeg.Text = string.Empty;
                    tbLongMin.Text = string.Empty;
                    tbLongEW.Text = string.Empty;
                    tbPhone.Text = string.Empty;
                    tbFax.Text = string.Empty;
                    tbContact.Text = string.Empty;
                    tbRating.Text = string.Empty;
                    chkInactive.Checked = false;
                    chkCrewHotel.Checked = false;
                    chkPassengerHotel.Checked = false;
                    tbDistanceFromICAO.Text = string.Empty;
                    tbMiles.Text = string.Empty;
                    tbMinutes.Text = string.Empty;
                    tbNegotiatedPrice.Text = string.Empty;
                    tbWebsite.Text = string.Empty;
                    tbRemarks.Text = string.Empty;
                    hdnMetro.Value = string.Empty;
                    hdnCtry.Value = string.Empty;
                    tbcontactBusinessPhone.Text = string.Empty;
                    tbContactEmail.Text = string.Empty;
                    tbContactMobile.Text = string.Empty;
                    tbBusinessEmail.Text = string.Empty;
                    tbTollFreePhone.Text = string.Empty;
                    tbAltbusinessPhone.Text = string.Empty;
                    tbExchangeRate.Text = "";

                    tbNegotiateTerm.Text = "";
                    tbSundayWorkHours.Text = "";
                    tbMondayWorkHours.Text = "";
                    tbTuesdayWorkHours.Text = "";
                    tbWednesdayWorkHours.Text = "";
                    tbThursdayWorkHours.Text = "";
                    tbFridayWorkHours.Text = "";
                    tbSaturdayWorkHours.Text = "";
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="Enable"></param>
        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbName.Enabled = Enable;
                    tbHotelCode.Enabled = false;
                    tbDistanceFromICAO.Enabled = Enable;
                    tbAddr1.Enabled = Enable;
                    tbAddr2.Enabled = Enable;
                    tbAddr3.Enabled = Enable;
                    tbCity.Enabled = Enable;
                    tbMetro.Enabled = Enable;
                    tbStateProv.Enabled = Enable;
                    tbPostalCode.Enabled = Enable;
                    tbCtry.Enabled = Enable;
                    tbLatDeg.Enabled = Enable;
                    tbLatMin.Enabled = Enable;
                    tbLatNS.Enabled = Enable;
                    tbLongDeg.Enabled = Enable;
                    tbLongMin.Enabled = Enable;
                    tbLongEW.Enabled = Enable;
                    tbPhone.Enabled = Enable;
                    tbFax.Enabled = Enable;
                    btnSearchExchange.Enabled = Enable;
                    tbContact.Enabled = Enable;
                    tbRating.Enabled = Enable;
                    chkInactive.Enabled = Enable;
                    chkCrewHotel.Enabled = Enable;
                    chkPassengerHotel.Enabled = Enable;
                    tbMiles.Enabled = Enable;
                    tbMinutes.Enabled = Enable;
                    tbNegotiatedPrice.Enabled = Enable;
                    tbWebsite.Enabled = Enable;
                    tbRemarks.Enabled = Enable;
                    btnCancel.Visible = Enable;
                    btnSaveChanges.Visible = Enable;
                    btnCtry.Enabled = Enable;
                    btnMetro.Enabled = Enable;
                    tbcontactBusinessPhone.Enabled = Enable;
                    tbContactEmail.Enabled = Enable;
                    tbContactMobile.Enabled = Enable;
                    tbBusinessEmail.Enabled = Enable;
                    tbTollFreePhone.Enabled = Enable;
                    tbAltbusinessPhone.Enabled = Enable;
                    tbExchangeRate.Enabled = Enable;
                    tbNegotiateTerm.Enabled = Enable;
                    tbSundayWorkHours.Enabled = Enable;
                    tbMondayWorkHours.Enabled = Enable;
                    tbTuesdayWorkHours.Enabled = Enable;
                    tbWednesdayWorkHours.Enabled = Enable;
                    tbThursdayWorkHours.Enabled = Enable;
                    tbFridayWorkHours.Enabled = Enable;
                    tbSaturdayWorkHours.Enabled = Enable;
                    //chkDisplayInctive.Enabled = !(Enable);
                    //chkCrewOnly.Enabled = !(Enable);
                    //chkPassengerOnly.Enabled = !(Enable);

                    if ((hdnSaveFlag.Value == "Update") && (Enable == true))
                    {
                        chkDisplayInctive.Enabled = false;
                        chkCrewOnly.Enabled = false;
                        chkPassengerOnly.Enabled = false;
                    }
                    else
                    {
                        chkDisplayInctive.Enabled = true;
                        chkCrewOnly.Enabled = true;
                        chkPassengerOnly.Enabled = true;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //GridDataItem Item = (GridDataItem)Session["SelectedItem"];
                    if (IsPopUp)
                    {
                        LoadControlData();
                    }
                    tbHotelCode.ReadOnly = true;
                    tbHotelCode.BackColor = System.Drawing.Color.LightGray;
                    tbDistanceFromICAO.ReadOnly = true;
                    tbDistanceFromICAO.BackColor = System.Drawing.Color.LightGray;
                    hdnSaveFlag.Value = "Update";
                    hdnRedirect.Value = "";
                    EnableForm(true);
                    dgHotelCatalog.Rebind();
                    string ID = Convert.ToString(Session["SelectedHotelID"]).Trim();
                    foreach (GridDataItem Item in dgHotelCatalog.MasterTableView.Items)
                    {
                        if (Item["HotelID"].Text.Trim() == ID.Trim())
                        {
                            Item.Selected = true;
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(hdnIsUWa.Value))
                    {
                        if (hdnIsUWa.Value.ToUpper().Trim() == "TRUE")
                        {
                            DisplayUwaRecords(true);

                        }
                        else
                        {
                            EnableForm(true);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected void DisplayUwaRecords(bool Enabled)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbHotelCode.Enabled = false;
                    tbName.Enabled = false;
                    tbAddr1.Enabled = false;
                    tbAddr2.Enabled = false;
                    tbAddr3.Enabled = true;
                    tbCity.Enabled = false;
                    tbMetro.Enabled = true;
                    tbStateProv.Enabled = false;
                    tbPostalCode.Enabled = true;
                    tbCtry.Enabled = false;
                    tbLatDeg.Enabled = true;
                    tbLatMin.Enabled = true;
                    tbLatNS.Enabled = true;
                    tbLongDeg.Enabled = true;
                    tbLongMin.Enabled = true;
                    tbLongEW.Enabled = true;
                    tbPhone.Enabled = false;
                    tbFax.Enabled = false;
                    tbContact.Enabled = true;
                    tbRating.Enabled = true;
                    chkInactive.Enabled = true;
                    chkCrewHotel.Enabled = true;
                    chkPassengerHotel.Enabled = true;
                    tbMiles.Enabled = false;
                    tbMinutes.Enabled = true;
                    tbNegotiatedPrice.Enabled = true;
                    tbWebsite.Enabled = false;
                    tbDistanceFromICAO.Enabled = true;
                    tbRemarks.Enabled = true;
                    btnCancel.Visible = true;
                    btnSaveChanges.Visible = true;
                    btnCtry.Enabled = false;
                    btnMetro.Enabled = true;
                    tbcontactBusinessPhone.Enabled = true;
                    tbContactEmail.Enabled = true;
                    tbContactMobile.Enabled = true;
                    tbBusinessEmail.Enabled = true;
                    tbTollFreePhone.Enabled = true;
                    tbAltbusinessPhone.Enabled = true;
                    if ((hdnSaveFlag.Value == "Update") && (Enabled == true))
                    {
                        chkDisplayInctive.Enabled = false;
                        chkCrewOnly.Enabled = false;
                        chkPassengerOnly.Enabled = false;
                    }
                    else
                    {
                        chkDisplayInctive.Enabled = true;
                        chkCrewOnly.Enabled = true;
                        chkPassengerOnly.Enabled = true;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To make the form readonly
        /// </summary>        
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LoadControlData();
                    tbHotelCode.ReadOnly = true;
                    tbHotelCode.BackColor = System.Drawing.Color.LightGray;
                    tbDistanceFromICAO.ReadOnly = true;
                    tbDistanceFromICAO.BackColor = System.Drawing.Color.LightGray;
                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void lnkCopy_Click(object sender, EventArgs e)
        {

            CheckCustomValidator();
            CheckExchangeRateExist();
            if (IsValidateCustom)
            {
                if (IsHotelExists())
                {

                    string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                        + @" oManager.radalert('Hotel already exists', 360, 50, 'Hotel');";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                }
                else
                {
                    using (MasterCatalogServiceClient HotelCatalogService = new MasterCatalogServiceClient())
                    {
                        var HotelValue = HotelCatalogService.AddHotel(GetItemsForCopy());
                        if (HotelValue.ReturnFlag == true)
                        {
                            ShowSuccessMessage();
                        }
                        dgHotelCatalog.Rebind();
                        DefaultSelection(false);
                    }
                }
            }
        }
        /// <summary>
        /// For Inserting the values in db
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveFlag.Value == "Update")
                        {
                            (dgHotelCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgHotelCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);


                    //This if else block and hdnSave.Value - "" are implemented because LastUpdTS is setting null
                    //when user modifies a Hotel. It is updating when a Hotel is added
                    if (!_selectLastModified)
                        hdnSaveFlag.Value = "";

                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
                }
            }
        }
        /// <summary>
        /// Cancel Hotel Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedHotelID"] != null)
                        {
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Database.Hotel, Convert.ToInt64(Session["SelectedHotelID"].ToString().Trim()));
                            }
                            //Session.Remove("SelectedHotelID");
                        }
                        DefaultSelection(true);
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
                }
            }

            hdnSaveFlag.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// To find the control and make it visible false  
        /// </summary>
        /// <param name="Add"></param>
        /// <param name="Edit"></param>
        /// <param name="Delete"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete, bool Copy)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtnInsertCtl = (LinkButton)dgHotelCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                    LinkButton lbtnDelCtl = (LinkButton)dgHotelCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitDelete");
                    LinkButton lbtnEditCtl = (LinkButton)dgHotelCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                    LinkButton lnkCopy = (LinkButton)dgHotelCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkCopy");
                    lnkCopy.Enabled = Copy;
                    if (IsAuthorized(Permission.Database.AddHotel))
                    {
                        lbtnInsertCtl.Visible = true;
                        if (Add)
                        {
                            lbtnInsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtnInsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtnInsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteHotel))
                    {
                        lbtnDelCtl.Visible = true;
                        if (Delete)
                        {
                            lbtnDelCtl.Enabled = true;
                            lbtnDelCtl.OnClientClick = "javascript:return deleterecord();";
                        }
                        else
                        {
                            lbtnDelCtl.Enabled = false;
                            lbtnDelCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnDelCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditHotel))
                    {
                        lbtnEditCtl.Visible = true;
                        if (Edit)
                        {
                            lbtnEditCtl.Enabled = true;
                            lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtnEditCtl.Enabled = false;
                            lbtnEditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnEditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <returns></returns>
        private Hotel GetItemsForCopy()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string Code = string.Empty;
                FlightPakMasterService.Hotel HotelService = new FlightPakMasterService.Hotel();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //     GridDataItem Item = (GridDataItem)Session["SelectedItem"];
                    if (!string.IsNullOrEmpty(hdnAirportID.Value))
                    {
                        HotelService.AirportID = Convert.ToInt64(hdnAirportID.Value);
                    }
                    //HotelService.HotelCD = tbHotelCode.Text.Remove(0, tbHotelCode.Text.IndexOf("-") + 1).Trim();
                    HotelService.IsChoice = true;
                    HotelService.Name = tbName.Text;
                    HotelService.PhoneNum = tbPhone.Text;
                    if (!string.IsNullOrEmpty(tbMiles.Text))
                    {
                        HotelService.MilesFromICAO = Convert.ToDecimal(tbMiles.Text.Trim(), CultureInfo.CurrentCulture);
                    }
                    HotelService.HotelRating = tbRating.Text;
                    HotelService.FaxNum = tbFax.Text;
                    if (tbNegotiatedPrice.Text != string.Empty)
                    {
                        HotelService.NegociatedRate = Convert.ToDecimal(tbNegotiatedPrice.Text);
                    }
                    HotelService.Addr1 = tbAddr1.Text;
                    HotelService.Addr2 = tbAddr2.Text;
                    HotelService.Addr3 = tbAddr3.Text;
                    HotelService.CityName = tbCity.Text;
                    if (!string.IsNullOrEmpty(hdnMetro.Value))
                    {
                        HotelService.MetroID = Convert.ToInt64(hdnMetro.Value);
                    }
                    HotelService.StateName = tbStateProv.Text;
                    HotelService.PostalZipCD = tbPostalCode.Text;
                    if (!string.IsNullOrEmpty(hdnCtry.Value))
                    {
                        HotelService.CountryID = Convert.ToInt64(hdnCtry.Value);
                    }
                    HotelService.ContactName = tbContact.Text;
                    HotelService.Remarks = tbRemarks.Text;
                    HotelService.IsPassenger = chkPassengerHotel.Checked;
                    HotelService.IsCrew = chkCrewHotel.Checked;
                    if (tbLatDeg.Text != string.Empty)
                    {
                        HotelService.LatitudeDegree = Convert.ToDecimal(tbLatDeg.Text);
                    }
                    if (tbLatMin.Text != string.Empty)
                    {
                        HotelService.LatitudeMinutes = Convert.ToDecimal(tbLatMin.Text);
                    }
                    HotelService.LatitudeNorthSouth = tbLatNS.Text;
                    if (tbLongDeg.Text != string.Empty)
                    {
                        HotelService.LongitudeDegrees = Convert.ToDecimal(tbLongDeg.Text);
                    }
                    if (tbLongMin.Text != string.Empty)
                    {
                        HotelService.LongitudeMinutes = Convert.ToDecimal(tbLongMin.Text);
                    }
                    HotelService.LongitudeEastWest = tbLongEW.Text;
                    //  HotelService.UpdateDT = System.DateTime.Now;                  
                    if (tbMinutes.Text != string.Empty)
                    {
                        HotelService.MinutesFromICAO = Convert.ToDecimal(tbMinutes.Text);
                    }
                    HotelService.Website = tbWebsite.Text;




                    HotelService.IsUWAMaintained = false;
                    Code = "0";
                    HotelService.UWAID = string.Empty;
                    HotelService.RecordType = string.Empty;
                    HotelService.SourceID = string.Empty;
                    HotelService.ControlNum = string.Empty;

                    HotelService.IsInActive = chkInactive.Checked;
                    HotelService.IsDeleted = false;
                    if (tbTollFreePhone.Text != string.Empty)
                    {
                        HotelService.TollFreePhone = tbTollFreePhone.Text;
                    }
                    if (tbAltbusinessPhone.Text != string.Empty)
                    {
                        HotelService.AltBusinessPhone = tbAltbusinessPhone.Text;
                    }
                    if (tbBusinessEmail.Text != string.Empty)
                    {
                        HotelService.BusinessEmail = tbBusinessEmail.Text;
                    }
                    if (tbContactEmail.Text != string.Empty)
                    {
                        HotelService.ContactEmail = tbContactEmail.Text;
                    }
                    if (tbContactMobile.Text != string.Empty)
                    {
                        HotelService.CellPhoneNum = tbContactMobile.Text;
                    }
                    if (tbcontactBusinessPhone.Text != string.Empty)
                    {
                        HotelService.ContactBusinessPhone = tbcontactBusinessPhone.Text;
                    }

                    //New
                    if (hdnExchangeRateID.Value.ToString().Trim() != string.Empty)
                    {
                        HotelService.ExchangeRateID = Convert.ToInt64(hdnExchangeRateID.Value);
                    }
                    if (tbNegotiateTerm.Text.Trim() != string.Empty)
                    {
                        HotelService.NegotiatedTerms = tbNegotiateTerm.Text.Trim();
                    }
                    if (tbSundayWorkHours.Text.Trim() != string.Empty)
                    {
                        HotelService.SundayWorkHours = tbSundayWorkHours.Text;
                    }
                    if (tbTuesdayWorkHours.Text.Trim() != string.Empty)
                    {
                        HotelService.TuesdayWorkHours = tbTuesdayWorkHours.Text;
                    }
                    if (tbWednesdayWorkHours.Text.Trim() != string.Empty)
                    {
                        HotelService.WednesdayWorkHours = tbWednesdayWorkHours.Text;
                    }
                    if (tbThursdayWorkHours.Text.Trim() != string.Empty)
                    {
                        HotelService.ThursdayWorkHours = tbThursdayWorkHours.Text;
                    }
                    if (tbFridayWorkHours.Text.Trim() != string.Empty)
                    {
                        HotelService.FridayWorkHours = tbFridayWorkHours.Text;
                    }
                    if (tbSaturdayWorkHours.Text.Trim() != string.Empty)
                    {
                        HotelService.SaturdayWorkHours = tbSaturdayWorkHours.Text;
                    }
                    HotelService.HotelID = 0;
                    //if (tbcontactBusinessPhone.Text != string.Empty)
                    //{
                    //    HotelService.ContactBusinessPhone = tbcontactBusinessPhone.Text;
                    //}


                    return HotelService;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return HotelService;
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <returns></returns>
        private Hotel GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string Code = string.Empty;
                FlightPakMasterService.Hotel HotelService = new FlightPakMasterService.Hotel();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //     GridDataItem Item = (GridDataItem)Session["SelectedItem"];
                    if (!string.IsNullOrEmpty(hdnAirportID.Value))
                    {
                        HotelService.AirportID = Convert.ToInt64(hdnAirportID.Value);
                    }
                    HotelService.HotelCD = tbHotelCode.Text.Remove(0, tbHotelCode.Text.IndexOf("-") + 1).Trim();
                    HotelService.IsChoice = true;
                    HotelService.Name = tbName.Text;
                    HotelService.PhoneNum = tbPhone.Text;
                    if (!string.IsNullOrEmpty(tbMiles.Text))
                    {
                        HotelService.MilesFromICAO = Convert.ToDecimal(tbMiles.Text.Trim(), CultureInfo.CurrentCulture);
                    }
                    HotelService.HotelRating = tbRating.Text;
                    HotelService.FaxNum = tbFax.Text;
                    if (tbNegotiatedPrice.Text != string.Empty)
                    {
                        HotelService.NegociatedRate = Convert.ToDecimal(tbNegotiatedPrice.Text);
                    }
                    HotelService.Addr1 = tbAddr1.Text;
                    HotelService.Addr2 = tbAddr2.Text;
                    HotelService.Addr3 = tbAddr3.Text;
                    HotelService.CityName = tbCity.Text;
                    if (!string.IsNullOrEmpty(hdnMetro.Value))
                    {
                        HotelService.MetroID = Convert.ToInt64(hdnMetro.Value);
                    }
                    HotelService.StateName = tbStateProv.Text;
                    HotelService.PostalZipCD = tbPostalCode.Text;
                    if (!string.IsNullOrEmpty(hdnCtry.Value))
                    {
                        HotelService.CountryID = Convert.ToInt64(hdnCtry.Value);
                    }
                    HotelService.ContactName = tbContact.Text;
                    HotelService.Remarks = tbRemarks.Text;
                    HotelService.IsPassenger = chkPassengerHotel.Checked;
                    HotelService.IsCrew = chkCrewHotel.Checked;
                    if (tbLatDeg.Text != string.Empty)
                    {
                        HotelService.LatitudeDegree = Convert.ToDecimal(tbLatDeg.Text);
                    }
                    if (tbLatMin.Text != string.Empty)
                    {
                        HotelService.LatitudeMinutes = Convert.ToDecimal(tbLatMin.Text);
                    }
                    HotelService.LatitudeNorthSouth = tbLatNS.Text;
                    if (tbLongDeg.Text != string.Empty)
                    {
                        HotelService.LongitudeDegrees = Convert.ToDecimal(tbLongDeg.Text);
                    }
                    if (tbLongMin.Text != string.Empty)
                    {
                        HotelService.LongitudeMinutes = Convert.ToDecimal(tbLongMin.Text);
                    }
                    HotelService.LongitudeEastWest = tbLongEW.Text;
                    //  HotelService.UpdateDT = System.DateTime.Now;                  
                    if (tbMinutes.Text != string.Empty)
                    {
                        HotelService.MinutesFromICAO = Convert.ToDecimal(tbMinutes.Text);
                    }
                    HotelService.Website = tbWebsite.Text;
                    if (Session["SelectedHotelID"] != null)
                    {
                        Code = Convert.ToString(Session["SelectedHotelID"]).Trim();
                    }

                    if (hdnSaveFlag.Value == "Update")
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var HotelList = FPKMasterService.GetHotelByHotelID(Convert.ToInt64(Code)).EntityList.ToList();
                            if (HotelList.Count > 0)
                            {
                                GetHotelByHotelID HotelEntity = HotelList[0];
                                if (!string.IsNullOrEmpty(HotelEntity.UWAID))
                                    HotelService.UWAID = HotelEntity.UWAID.ToString().Trim();
                                if (HotelEntity.UWAMaintFlag != null)
                                {
                                    HotelService.UWAMaintFlag = HotelEntity.UWAMaintFlag;
                                }
                                if (HotelEntity.HotelID != null)
                                {
                                    HotelService.HotelID = HotelEntity.HotelID;
                                }
                                if (HotelEntity.UWAUpdates != null)
                                {
                                    HotelService.UWAUpdates = HotelEntity.UWAUpdates;
                                }
                                if (HotelEntity.RecordType != null)
                                {
                                    HotelService.RecordType = HotelEntity.RecordType;
                                }
                                if (HotelEntity.SourceID != null)
                                {
                                    HotelService.SourceID = HotelEntity.SourceID;
                                }
                                if (HotelEntity.ControlNum != null)
                                {
                                    HotelService.ControlNum = HotelEntity.ControlNum;
                                }
                                if (HotelEntity.IsUWAMaintained != null)
                                {
                                    HotelService.IsUWAMaintained = HotelEntity.IsUWAMaintained;
                                }

                            }
                        }

                    }
                    else
                    {
                        HotelService.IsUWAMaintained = false;
                        Code = "0";
                        HotelService.UWAID = string.Empty;
                        HotelService.RecordType = string.Empty;
                        HotelService.SourceID = string.Empty;
                        HotelService.ControlNum = string.Empty;
                    }
                    HotelService.IsInActive = chkInactive.Checked;
                    HotelService.IsDeleted = false;
                    if (tbTollFreePhone.Text != string.Empty)
                    {
                        HotelService.TollFreePhone = tbTollFreePhone.Text;
                    }
                    if (tbAltbusinessPhone.Text != string.Empty)
                    {
                        HotelService.AltBusinessPhone = tbAltbusinessPhone.Text;
                    }
                    if (tbBusinessEmail.Text != string.Empty)
                    {
                        HotelService.BusinessEmail = tbBusinessEmail.Text;
                    }
                    if (tbContactEmail.Text != string.Empty)
                    {
                        HotelService.ContactEmail = tbContactEmail.Text;
                    }
                    if (tbContactMobile.Text != string.Empty)
                    {
                        HotelService.CellPhoneNum = tbContactMobile.Text;
                    }
                    if (tbcontactBusinessPhone.Text != string.Empty)
                    {
                        HotelService.ContactBusinessPhone = tbcontactBusinessPhone.Text;
                    }

                    //New
                    if (hdnExchangeRateID.Value.ToString().Trim() != string.Empty)
                    {
                        HotelService.ExchangeRateID = Convert.ToInt64(hdnExchangeRateID.Value);
                    }
                    if (tbNegotiateTerm.Text.Trim() != string.Empty)
                    {
                        HotelService.NegotiatedTerms = tbNegotiateTerm.Text.Trim();
                    }
                    if (tbSundayWorkHours.Text.Trim() != string.Empty)
                    {
                        HotelService.SundayWorkHours = tbSundayWorkHours.Text;
                    }
                    if (tbTuesdayWorkHours.Text.Trim() != string.Empty)
                    {
                        HotelService.TuesdayWorkHours = tbTuesdayWorkHours.Text;
                    }
                    if (tbWednesdayWorkHours.Text.Trim() != string.Empty)
                    {
                        HotelService.WednesdayWorkHours = tbWednesdayWorkHours.Text;
                    }
                    if (tbThursdayWorkHours.Text.Trim() != string.Empty)
                    {
                        HotelService.ThursdayWorkHours = tbThursdayWorkHours.Text;
                    }
                    if (tbFridayWorkHours.Text.Trim() != string.Empty)
                    {
                        HotelService.FridayWorkHours = tbFridayWorkHours.Text;
                    }
                    if (tbSaturdayWorkHours.Text.Trim() != string.Empty)
                    {
                        HotelService.SaturdayWorkHours = tbSaturdayWorkHours.Text;
                    }
                    //if (tbcontactBusinessPhone.Text != string.Empty)
                    //{
                    //    HotelService.ContactBusinessPhone = tbcontactBusinessPhone.Text;
                    //}


                    return HotelService;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return HotelService;
            }
        }
        /// </summary>
        /// To assign the data to controls 
        /// </summary>        
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ClearForm();
                    //hdnSaveFlag.Value = "Update";
                    // GridDataItem Item = (GridDataItem)Session["SelectedItem"];
                    string Code = "";
                    if (Session["SelectedHotelID"] != null)
                    {
                        Code = Convert.ToString(Session["SelectedHotelID"]).Trim();
                    }
                    string LastUpdUID = "", LastUpdTS = "", IcaoID = "", IsInActive = "", IsCrew = "", MetroID = "", ExchangeRateID = "", NegotiatedTerms = "", SundayWorkHours = "", MondayWorkHours = "", TuesdayWorkHours = "", WednesdayWorkHours = "", ThursdayWorkHours = "", FridayWorkHours = "", SaturdayWorkHours = "",
                        CountryID = "", IsPassenger = "", Name = "", PhoneNum = "", FaxNum = "", ContactName = "",
                        HotelRating = "", MilesFromICAO = "", MinutesFromICAO = "", NegociatedRate = "", Addr1 = "",
                        Addr2 = "", Addr3 = "", CityName = "", MetroCD = "", StateName = "", PostalZipCD = "",
                        CountryCD = "", LatitudeDegree = "", LatitudeMinutes = "", LatitudeNorthSouth = "",
                        LongitudeDegrees = "", LongitudeMinutes = "", LongitudeEastWest = "", Remarks = "",
                        Website = "", UWAMaintFlag = "", HotelCD = "";
                    foreach (GridDataItem Item in dgHotelCatalog.MasterTableView.Items)
                    {
                        if (Convert.ToString(Item["HotelID"].Text.Trim()) == Code)
                        {
                            LastUpdUID = Convert.ToString(Item["LastUpdUID"].Text).Trim().Replace("&nbsp;", "");
                            LastUpdTS = Convert.ToString(Item["LastUpdTS"].Text).Trim().Replace("&nbsp;", "");
                            IcaoID = Convert.ToString(Item["IcaoID"].Text).Trim().Replace("&nbsp;", "");
                            if (Item.GetDataKeyValue("IsInActive") != null)
                            {
                                IsInActive = Item.GetDataKeyValue("IsInActive").ToString().Trim().Replace("&nbsp;", "");
                            }
                            if (Item.GetDataKeyValue("IsCrew") != null)
                            {
                                IsCrew = Item.GetDataKeyValue("IsCrew").ToString().Trim().Replace("&nbsp;", "");
                            }
                            MetroID = Convert.ToString(Item["MetroID"].Text).Trim().Replace("&nbsp;", "");
                            CountryID = Convert.ToString(Item["CountryID"].Text.Trim()).Replace("&nbsp;", "");
                            if (Item.GetDataKeyValue("IsPassenger") != null)
                            {
                                IsPassenger = Item.GetDataKeyValue("IsPassenger").ToString().Trim().Replace("&nbsp;", "");
                            }
                            Name = Convert.ToString(Item["Name"].Text.Trim()).Replace("&nbsp;", "");
                            PhoneNum = Convert.ToString(Item["PhoneNum"].Text.Trim()).Replace("&nbsp;", "");
                            FaxNum = Convert.ToString(Item["FaxNum"].Text.Trim()).Replace("&nbsp;", "");
                            ContactName = Convert.ToString(Item["ContactName"].Text.Trim()).Replace("&nbsp;", "");
                            HotelRating = Convert.ToString(Item["HotelRating"].Text.Trim()).Replace("&nbsp;", "");
                            MilesFromICAO = Convert.ToString(Item["MilesFromICAO"].Text.Trim()).Replace("&nbsp;", "");
                            MinutesFromICAO = Convert.ToString(Item["MinutesFromICAO"].Text.Trim()).Replace("&nbsp;", "");
                            NegociatedRate = Convert.ToString(Item["NegociatedRate"].Text.Trim()).Replace("&nbsp;", "");
                            Addr1 = Convert.ToString(Item["Addr1"].Text.Trim()).Replace("&nbsp;", "");
                            Addr2 = Convert.ToString(Item["Addr2"].Text.Trim()).Replace("&nbsp;", "");
                            Addr3 = Convert.ToString(Item["Addr3"].Text.Trim()).Replace("&nbsp;", "");
                            CityName = Convert.ToString(Item["CityName"].Text.Trim()).Replace("&nbsp;", "");
                            MetroCD = Convert.ToString(Item["MetroCD"].Text.Trim()).Replace("&nbsp;", "");
                            StateName = Convert.ToString(Item["StateName"].Text.Trim()).Replace("&nbsp;", "");
                            PostalZipCD = Convert.ToString(Item["PostalZipCD"].Text.Trim()).Replace("&nbsp;", "");
                            CountryCD = Convert.ToString(Item["CountryCD"].Text.Trim()).Replace("&nbsp;", "");
                            LatitudeDegree = Convert.ToString(Item["LatitudeDegree"].Text.Trim()).Replace("&nbsp;", "");
                            LatitudeMinutes = Convert.ToString(Item["LatitudeMinutes"].Text.Trim()).Replace("&nbsp;", "");
                            if (Item.GetDataKeyValue("TollFreePhone") != null)
                            {
                                tbTollFreePhone.Text = Item.GetDataKeyValue("TollFreePhone").ToString().Trim();
                            }
                            if (Item.GetDataKeyValue("AltBusinessPhone") != null)
                            {
                                tbAltbusinessPhone.Text = Item.GetDataKeyValue("AltBusinessPhone").ToString().Trim();
                            }
                            if (Item.GetDataKeyValue("BusinessEmail") != null)
                            {
                                tbBusinessEmail.Text = Item.GetDataKeyValue("BusinessEmail").ToString().Trim();
                            }
                            if (Item.GetDataKeyValue("ContactEmail") != null)
                            {
                                tbContactEmail.Text = Item.GetDataKeyValue("ContactEmail").ToString().Trim();
                            }
                            if (Item.GetDataKeyValue("CellPhoneNum") != null)
                            {
                                tbContactMobile.Text = Item.GetDataKeyValue("CellPhoneNum").ToString().Trim();
                            }
                            if (Item.GetDataKeyValue("ContactBusinessPhone") != null)
                            {
                                tbcontactBusinessPhone.Text = Item.GetDataKeyValue("ContactBusinessPhone").ToString().Trim();
                            }

                            if (Item.GetDataKeyValue("LatitudeNorthSouth") != null)
                            {
                                LatitudeNorthSouth = Item.GetDataKeyValue("LatitudeNorthSouth").ToString().Trim().Replace("&nbsp;", "");
                            }
                            if (Item.GetDataKeyValue("LongitudeDegrees") != null)
                            {
                                LongitudeDegrees = Item.GetDataKeyValue("LongitudeDegrees").ToString().Trim().Replace("&nbsp;", "");
                            }
                            if (Item.GetDataKeyValue("LongitudeMinutes") != null)
                            {
                                LongitudeMinutes = Item.GetDataKeyValue("LongitudeMinutes").ToString().Trim().Replace("&nbsp;", "");
                            }
                            if (Item.GetDataKeyValue("LongitudeEastWest") != null)
                            {
                                LongitudeEastWest = Item.GetDataKeyValue("LongitudeEastWest").ToString().Trim().Replace("&nbsp;", "");
                            }
                            if (Item.GetDataKeyValue("ExchangeRateID") != null)
                            {
                                ExchangeRateID = Convert.ToString(Item.GetDataKeyValue("ExchangeRateID").ToString());
                            }

                            if (Item.GetDataKeyValue("NegotiatedTerms") != null)
                            {
                                NegotiatedTerms = Convert.ToString(Item.GetDataKeyValue("NegotiatedTerms").ToString());
                            }
                            if (Item.GetDataKeyValue("SundayWorkHours") != null)
                            {
                                SundayWorkHours = Convert.ToString(Item.GetDataKeyValue("SundayWorkHours").ToString());
                            }
                            if (Item.GetDataKeyValue("MondayWorkHours") != null)
                            {
                                MondayWorkHours = Convert.ToString(Item.GetDataKeyValue("MondayWorkHours").ToString());
                            }
                            if (Item.GetDataKeyValue("TuesdayWorkHours") != null)
                            {
                                TuesdayWorkHours = Convert.ToString(Item.GetDataKeyValue("TuesdayWorkHours").ToString());
                            }
                            if (Item.GetDataKeyValue("WednesdayWorkHours") != null)
                            {
                                WednesdayWorkHours = Convert.ToString(Item.GetDataKeyValue("WednesdayWorkHours").ToString());
                            }
                            if (Item.GetDataKeyValue("ThursdayWorkHours") != null)
                            {
                                ThursdayWorkHours = Convert.ToString(Item.GetDataKeyValue("ThursdayWorkHours").ToString());
                            }
                            if (Item.GetDataKeyValue("FridayWorkHours") != null)
                            {
                                FridayWorkHours = Convert.ToString(Item.GetDataKeyValue("FridayWorkHours").ToString());
                            }
                            if (Item.GetDataKeyValue("SaturdayWorkHours") != null)
                            {
                                SaturdayWorkHours = Convert.ToString(Item.GetDataKeyValue("SaturdayWorkHours").ToString());
                            }
                            Remarks = Convert.ToString(Item["Remarks"].Text.Trim()).Replace("&nbsp;", "");
                            Website = Convert.ToString(Item["Website"].Text.Trim()).Replace("&nbsp;", "");
                            UWAMaintFlag = Convert.ToString(Item["chkUWAID"].Text.Trim()).Replace("&nbsp;", "");
                            HotelCD = Convert.ToString(Item["HotelCD"].Text.Trim()).Replace("&nbsp;", "");
                            if (Item.GetDataKeyValue("Filter") != null)
                            {
                                if (Item.GetDataKeyValue("Filter").ToString().ToUpper().Trim() == "UWA")
                                {
                                    hdnIsUWa.Value = "True";
                                    lbWarningMessage.Visible = true;
                                }
                                else
                                {
                                    hdnIsUWa.Value = "False";
                                    lbWarningMessage.Visible = false;
                                }

                            }
                            else
                            {
                                hdnIsUWa.Value = "False";
                                lbWarningMessage.Visible = false;
                            }

                            lbColumnName1.Text = "Hotel Code";
                            lbColumnName2.Text = "Hotel Name";
                            lbColumnValue1.Text = Item["HotelCD"].Text;
                            lbColumnValue2.Text = Item["Name"].Text;
                            Item.Selected = true;
                            break;
                        }
                    }
                    Label lbUser = (Label)dgHotelCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                    if (!string.IsNullOrEmpty(LastUpdUID))
                    {
                        lbUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + LastUpdUID);
                    }
                    else
                    {
                        lbUser.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    }
                    if (!string.IsNullOrEmpty(LastUpdTS))
                    {
                        lbUser.Text = System.Web.HttpUtility.HtmlEncode(lbUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(LastUpdTS)));
                    }
                    else
                    {
                        lbUser.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    }
                    if (!string.IsNullOrEmpty(IcaoID))
                    {
                        tbHotelCode.Text = IcaoID + " - " + HotelCD;
                        tbDistanceFromICAO.Text = IcaoID;
                    }
                    if (!string.IsNullOrEmpty(IsInActive))
                    {
                        chkInactive.Checked = Convert.ToBoolean(IsInActive);
                    }
                    else
                    {
                        chkInactive.Checked = false;
                    }
                    if (!string.IsNullOrEmpty(IsCrew))
                    {
                        chkCrewHotel.Checked = Convert.ToBoolean(IsCrew);
                    }
                    else
                    {
                        chkCrewHotel.Checked = false;
                    }
                    if (!string.IsNullOrEmpty(MetroID))
                    {
                        hdnMetro.Value = MetroID;
                    }
                    else
                    {
                        hdnMetro.Value = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(CountryID))
                    {
                        hdnCtry.Value = CountryID;
                    }
                    else
                    {
                        hdnCtry.Value = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(IsPassenger))
                    {
                        chkPassengerHotel.Checked = Convert.ToBoolean(IsPassenger);
                    }
                    else
                    {
                        chkPassengerHotel.Checked = false;
                    }
                    if (!string.IsNullOrEmpty(Name))
                    {
                        tbName.Text = Name;
                    }
                    else
                    {
                        tbName.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(PhoneNum))
                    {
                        tbPhone.Text = PhoneNum;
                    }
                    else
                    {
                        tbPhone.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(FaxNum))
                    {
                        tbFax.Text = FaxNum;
                    }
                    else
                    {
                        tbFax.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(ContactName))
                    {
                        tbContact.Text = ContactName;
                    }
                    else
                    {
                        tbContact.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(HotelRating))
                    {
                        tbRating.Text = HotelRating;
                    }
                    else
                    {
                        tbRating.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(MilesFromICAO))
                    {
                        tbMiles.Text = MilesFromICAO;
                    }
                    else
                    {
                        tbMiles.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(MinutesFromICAO))
                    {
                        tbMinutes.Text = MinutesFromICAO;
                    }
                    else
                    {
                        tbMinutes.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(NegociatedRate))
                    {
                        tbNegotiatedPrice.Text = NegociatedRate;
                    }
                    else
                    {
                        tbNegotiatedPrice.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(Addr1))
                    {
                        tbAddr1.Text = Addr1;
                    }
                    else
                    {
                        tbAddr1.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(Addr2))
                    {
                        tbAddr2.Text = Addr2;
                    }
                    else
                    {
                        tbAddr2.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(Addr3))
                    {
                        tbAddr3.Text = Addr3;
                    }
                    else
                    {
                        tbAddr3.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(CityName))
                    {
                        tbCity.Text = CityName;
                    }
                    else
                    {
                        tbCity.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(MetroCD))
                    {
                        tbMetro.Text = MetroCD;
                    }
                    else
                    {
                        tbMetro.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(StateName))
                    {
                        tbStateProv.Text = StateName;
                    }
                    else
                    {
                        tbStateProv.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(PostalZipCD))
                    {
                        tbPostalCode.Text = PostalZipCD;
                    }
                    else
                    {
                        tbPostalCode.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(CountryCD))
                    {
                        tbCtry.Text = CountryCD;
                    }
                    else
                    {
                        tbCtry.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(LatitudeDegree))
                    {
                        tbLatDeg.Text = LatitudeDegree;
                    }
                    else
                    {
                        tbLatDeg.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(LatitudeMinutes))
                    {
                        tbLatMin.Text = LatitudeMinutes;
                    }
                    else
                    {
                        tbLatMin.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(LatitudeNorthSouth))
                    {
                        tbLatNS.Text = LatitudeNorthSouth;
                    }
                    else
                    {
                        tbLatNS.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(LongitudeDegrees))
                    {
                        tbLongDeg.Text = LongitudeDegrees;
                    }
                    else
                    {
                        tbLongDeg.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(LongitudeMinutes))
                    {
                        tbLongMin.Text = LongitudeMinutes;
                    }
                    else
                    {
                        tbLongMin.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(LongitudeEastWest))
                    {
                        tbLongEW.Text = LongitudeEastWest;
                    }
                    else
                    {
                        tbLongEW.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(Remarks))
                    {
                        tbRemarks.Text = Remarks;
                    }
                    else
                    {
                        tbRemarks.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(Website))
                    {
                        tbWebsite.Text = Website;
                    }
                    else
                    {
                        tbWebsite.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(ExchangeRateID))
                    {
                        hdnExchangeRateID.Value = ExchangeRateID;
                        if (hdnExchangeRateID.Value.ToString().Trim() != "")
                        {
                            CheckExchangeRate();
                        }
                        else
                        {
                            tbExchangeRate.Text = "";
                        }
                    }

                    if (!string.IsNullOrEmpty(NegotiatedTerms))
                    {
                        tbNegotiateTerm.Text = NegotiatedTerms;
                    }
                    if (!string.IsNullOrEmpty(SundayWorkHours))
                    {
                        tbSundayWorkHours.Text = SundayWorkHours;
                    }
                    if (!string.IsNullOrEmpty(MondayWorkHours))
                    {
                        tbMondayWorkHours.Text = MondayWorkHours;
                    }
                    if (!string.IsNullOrEmpty(TuesdayWorkHours))
                    {
                        tbTuesdayWorkHours.Text = TuesdayWorkHours;
                    }
                    if (!string.IsNullOrEmpty(WednesdayWorkHours))
                    {
                        tbWednesdayWorkHours.Text = WednesdayWorkHours;
                    }
                    if (!string.IsNullOrEmpty(ThursdayWorkHours))
                    {
                        tbThursdayWorkHours.Text = ThursdayWorkHours;
                    }
                    if (!string.IsNullOrEmpty(FridayWorkHours))
                    {
                        tbFridayWorkHours.Text = FridayWorkHours;
                    }
                    if (!string.IsNullOrEmpty(SaturdayWorkHours))
                    {
                        tbSaturdayWorkHours.Text = SaturdayWorkHours;
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To check unique Metro Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbMetroCD_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAllReadyMetroExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
                }
            }
        }
        /// <summary>
        /// Method to check unique Metro Code
        /// </summary>
        /// <returns></returns>
        private bool CheckAllReadyMetroExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbMetro.Text != null) && (tbMetro.Text != string.Empty))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient MetroService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            //To check for unique metro code
                            var MetroValue = MetroService.GetMetroCityList().EntityList.Where(x => x.MetroCD.ToString().ToUpper().Trim().Equals(tbMetro.Text.ToUpper().Trim())).ToList();
                            if (MetroValue.Count() > 0 && MetroValue != null)
                            {
                                hdnMetro.Value = ((FlightPakMasterService.Metro)MetroValue[0]).MetroID.ToString().Trim();
                                tbMetro.Text = ((FlightPakMasterService.Metro)MetroValue[0]).MetroCD;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbCity);
                                //tbCity.Focus();
                            }
                            else
                            {
                                cvMetro.IsValid = false;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbMetro);
                                // tbMetro.Focus();
                                ReturnVal = true;
                            }
                        }
                    }
                    return ReturnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnVal;
            }
        }
        /// <summary>
        /// To check unique Country Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbCountry_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAllReadyCountryExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
                }
            }
        }
        /// <summary>
        /// Method to check unique Country  Code 
        /// </summary>
        /// <returns></returns>
        private bool CheckAllReadyCountryExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if ((tbCtry.Text != null) && (tbCtry.Text != string.Empty))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var CountryValue = CountryService.GetCountryMasterList().EntityList.Where(x => x.CountryCD.ToString().ToUpper().Trim().Equals(tbCtry.Text.ToString().ToUpper().Trim())).ToList();
                            if (CountryValue.Count() > 0 && CountryValue != null)
                            {

                                hdnCtry.Value = ((FlightPakMasterService.Country)CountryValue[0]).CountryID.ToString().ToUpper().Trim();
                                tbCtry.Text = ((FlightPakMasterService.Country)CountryValue[0]).CountryCD;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbPostalCode);
                                //tbPostalCode.Focus();
                            }
                            else
                            {
                                cvCtry.IsValid = false;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbCtry);
                                //tbCtry.Focus();
                                ReturnValue = false;
                            }
                        }
                    }
                    return ReturnValue;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnValue;
            }
        }
        /// <summary>
        /// Method to check custom Validator
        /// </summary>
        private void CheckCustomValidator()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (CheckAllReadyMetroExist())
                    {
                        cvMetro.IsValid = false;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbMetro);
                        //tbMetro.Focus();
                        IsValidateCustom = false;
                    }
                    if (CheckAllReadyCountryExist())
                    {
                        cvCtry.IsValid = false;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbCtry);
                        //tbCtry.Focus();
                        IsValidateCustom = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To bring active records basded on displayinactive checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkDisplayInctive_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //if (btnSaveChanges.Visible == false)
                        //{
                        //    using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                        //    {
                        //        var HotelVal = objService.GetHotelsByAirportID(Convert.ToInt64(hdnAirportID.Value));
                        //        if ((HotelVal.ReturnFlag == true) && (chkDisplayInctive.Checked == false))
                        //        {
                        //            dgHotelCatalog.DataSource = HotelVal.EntityList.Where(x => x.IsInActive == false);
                        //            DefaultSelection(true);
                        //        }
                        //        if ((HotelVal.ReturnFlag == true) && (chkDisplayInctive.Checked == true))
                        //        {
                        //            dgHotelCatalog.DataSource = HotelVal.EntityList;
                        //            DefaultSelection(true);
                        //        }
                        //    }
                        //}
                        //if (btnSaveChanges.Visible == false)
                        //{
                        SearchAndFilter(true);
                        //}
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
                }
            }
        }
        /// <summary>
        /// Method to check unique Country  Code 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ExchangeRate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckExchangeRateExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }


        private void CheckExchangeRate()
        {
            using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var CountryValue = CountryService.GetExchangeRate().EntityList.Where(x => x.ExchangeRateID.ToString().Trim().ToUpper() == (hdnExchangeRateID.Value.ToString().Trim().ToUpper())).ToList();
                if (CountryValue.Count() > 0 && CountryValue != null)
                {
                    tbExchangeRate.Text = ((FlightPakMasterService.ExchangeRate)CountryValue[0]).ExchRateCD.ToString().Trim();
                    //RadAjaxManager.GetCurrent(Page).FocusControl(tbSaturdayWorkHours);
                    // tbPostal.Focus();
                }
                else
                {
                    tbExchangeRate.Text = "";
                }
            }

        }
        /// <summary>
        ///  Method to check unique Country  Code 
        /// </summary>
        private void CheckExchangeRateExist()
        {
            if (!string.IsNullOrEmpty(tbExchangeRate.Text))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var CountryValue = CountryService.GetExchangeRate().EntityList.Where(x => x.ExchRateCD.Trim().ToUpper() == (tbExchangeRate.Text.Trim().ToUpper())).ToList();
                    if (CountryValue.Count() > 0 && CountryValue != null)
                    {
                        hdnExchangeRateID.Value = ((FlightPakMasterService.ExchangeRate)CountryValue[0]).ExchangeRateID.ToString().Trim();
                        tbExchangeRate.Text = ((FlightPakMasterService.ExchangeRate)CountryValue[0]).ExchRateCD;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbSaturdayWorkHours);
                        // tbPostal.Focus();
                    }
                    else
                    {
                        cvExchangeRate.IsValid = false;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbExchangeRate);
                        //tbCountry.Focus();
                        IsValidateCustom = false;
                    }
                }
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            if (IsPopUp || IsViewOnly)
                            {
                                //Set visible false to all controls.
                                //Container control visibility throwing some error
                                table1.Visible = false;
                                table2.Visible = false;
                                table3.Visible = false;
                                table4.Visible = false;
                                dgHotelCatalog.Visible = false;
                                if (IsViewOnly)
                                {
                                    dgHotelCatalog.Visible = true;
                                    //table2.Visible = true;
                                    table3.Visible = true;
                                    DefaultSelection(false);
                                }
                                if (!IsViewOnly)
                                {
                                    if (IsAdd)
                                    {
                                        (dgHotelCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                    }
                                    else
                                    {
                                        (dgHotelCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                    }
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }

            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgHotelCatalog.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgHotelCatalog.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
            CheckIsWidget("PreInit");
        }
        ///// <summary>
        ///// To display only crewonly records
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void chkCrewOnly_CheckedChanged(Object sender, EventArgs e)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient();
        //                if ((objService.GetHotelInfo().ReturnFlag == true) && (chkCrewOnly.Checked == false))
        //                {
        //                    dgHotelCatalog.DataSource = objService.GetHotelInfo().EntityList.Where(x => x.AirportID.ToString().ToUpper().Contains(hdnAirportID.Value.ToString().ToUpper().Trim()) && (x.IsDeleted.ToString().ToUpper().Equals("FALSE")) && (x.IsInActive == false) && (x.IsCrew.ToString().ToUpper().Equals("FALSE")));
        //                    DefaultSelection(true);
        //                }
        //                if ((objService.GetHotelInfo().ReturnFlag == true) && (chkCrewOnly.Checked == true))
        //                {
        //                    dgHotelCatalog.DataSource = objService.GetHotelInfo().EntityList.Where(x => x.AirportID.ToString().ToUpper().Contains(hdnAirportID.Value.ToString().ToUpper().Trim()) && (x.IsDeleted.ToString().ToUpper().Equals("FALSE")));
        //                    DefaultSelection(true);
        //                }
        //            }, FlightPak.Common.Constants.Policy.UILayer);
        //        }
        //        catch (Exception ex)
        //        {
        //            //The exception will be handled, logged and replaced by our custom exception. 
        //            ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
        //        }
        //    }
        //}
        ///// <summary>
        ///// To dispaly only passenger records
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void chkPassengerOnly_CheckedChanged(Object sender, EventArgs e)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient();
        //                if ((objService.GetHotelInfo().ReturnFlag == true) && (chkPassengerOnly.Checked == false))
        //                {
        //                    dgHotelCatalog.DataSource = objService.GetHotelInfo().EntityList.Where(x => x.AirportID.ToString().ToUpper().Contains(hdnAirportID.Value.ToString().ToUpper().Trim()) && (x.IsDeleted.ToString().ToUpper().Equals("FALSE")) && (x.IsInActive == false) && (x.IsPassenger.ToString().ToUpper().Equals("FALSE")));
        //                    DefaultSelection(true);
        //                }
        //                if ((objService.GetHotelInfo().ReturnFlag == true) && (chkDisplayInctive.Checked == true))
        //                {
        //                    dgHotelCatalog.DataSource = objService.GetHotelInfo().EntityList.Where(x => x.AirportID.ToString().ToUpper().Contains(hdnAirportID.Value.ToString().ToUpper().Trim()) && (x.IsDeleted.ToString().ToUpper().Equals("FALSE")));
        //                    DefaultSelection(true);
        //                }
        //            }, FlightPak.Common.Constants.Policy.UILayer);
        //        }
        //        catch (Exception ex)
        //        {
        //            //The exception will be handled, logged and replaced by our custom exception. 
        //            ProcessErrorMessage(ex, ModuleNameConstants.Database.Hotel);
        //        }
        //    }
        //}

        private void CheckIsWidget(string LoadType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["IsWidget"] != null)
                {
                    if (Session["IsWidget"].ToString() == "1")
                    {
                        if (LoadType == "PreInit")
                        {
                            this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");
                        }
                    }
                }
            }
        }

        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetHotelsByAirportID> lstHotel = new List<FlightPakMasterService.GetHotelsByAirportID>();
                if (Session["HotelCD"] != null)
                {
                    lstHotel = (List<FlightPakMasterService.GetHotelsByAirportID>)Session["HotelCD"];
                }
                if (lstHotel.Count != 0)
                {
                    if (chkDisplayInctive.Checked == false) { lstHotel = lstHotel.Where(x => x.IsInActive == false).ToList<GetHotelsByAirportID>(); }
                    if (chkCrewOnly.Checked == true) { lstHotel = lstHotel.Where(x => x.IsCrew == true).ToList<GetHotelsByAirportID>(); }
                    if (chkPassengerOnly.Checked == true) { lstHotel = lstHotel.Where(x => x.IsPassenger == true).ToList<GetHotelsByAirportID>(); }
                    dgHotelCatalog.DataSource = lstHotel.GroupBy(t => t.Name).Select(y => y.First()).ToList(); 
                    if (IsDataBind)
                    {
                        dgHotelCatalog.DataBind();
                    }
                }
                else
                {
                    chkCrewOnly.Enabled = false;
                    chkDisplayInctive.Enabled = false;
                    chkPassengerOnly.Enabled = false;
                }
                LoadControlData();
                return false;
            }
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
			
			   if (IsPopUp)
                {
                    if (Request.QueryString["HotelID"] != null)
                    {
                        Session["SelectedHotelID"] = Request.QueryString["HotelID"].Trim();
                    }

                    if (Request.QueryString["IcaoID"] != null)
                    {
                        Session["SelectedAirportIDforHotelPopup"] = Request.QueryString["IcaoID"].Trim();
                    }                  
                }

				
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgHotelCatalog.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, true);
                }
            }
        }

        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            if (IsPopUp && Session["SelectedAirportIDforHotelPopup"] != null)
            {
                hdnAirportID.Value = Session["SelectedAirportIDforHotelPopup"].ToString();
            }

            var HotelValue = FPKMstService.GetHotelsByAirportID(Convert.ToInt64(hdnAirportID.Value));
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, HotelValue);
            List<FlightPakMasterService.GetHotelsByAirportID> filteredList = GetFilteredList(HotelValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.HotelID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgHotelCatalog.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgHotelCatalog.CurrentPageIndex = PageNumber;
            dgHotelCatalog.SelectedIndexes.Add(ItemIndex);
        }

        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetHotelsByAirportID HotelValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedHotelID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                //This if else block and hdnSave.Value - "" are implemented because LastUpdTS is setting null
                //when user modifies a Hotel. It is updating when a Hotel is added
                if (hdnSaveFlag.Value == "Save")
                {
                    PrimaryKeyValue = HotelValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().HotelID;
                    Session["SelectedHotelID"] = PrimaryKeyValue;
                }
                else
                {
                    if (Session["SelectedCateringID"] != null)
                    PrimaryKeyValue = Convert.ToInt64(Session["SelectedHotelID"].ToString());
                }

                hdnSaveFlag.Value = "";
            }

            return PrimaryKeyValue;
        }

        private List<FlightPakMasterService.GetHotelsByAirportID> GetFilteredList(ReturnValueOfGetHotelsByAirportID HotelValue)
        {
            List<FlightPakMasterService.GetHotelsByAirportID> filteredList = new List<FlightPakMasterService.GetHotelsByAirportID>();

            if (HotelValue.ReturnFlag)
            {
                filteredList = HotelValue.EntityList.Where(x => x.IsDeleted == false).ToList();
            }

            if (!IsPopUp)
            {
                if (filteredList.Count > 0)
                {
                    if (chkDisplayInctive.Checked)
                    {
                        dgHotelCatalog.DataSource = HotelValue.EntityList.Where(x => x.IsDeleted.ToString().ToUpper().Trim().Equals("FALSE")).ToList().GroupBy(t => t.Name).Select(y => y.First()).ToList();
                    }
                    else
                    {
                        dgHotelCatalog.DataSource = HotelValue.EntityList.Where(x => x.IsDeleted.ToString().ToUpper().Trim().Equals("FALSE") && x.IsInActive.ToString().ToUpper().Trim() == "FALSE").ToList().GroupBy(t => t.Name).Select(y => y.First()).ToList();
                    }
                    if (chkCrewOnly.Checked)
                    {
                        dgHotelCatalog.DataSource = HotelValue.EntityList.Where(x => x.IsCrew.ToString().ToUpper().Trim().Equals("TRUE")).ToList().GroupBy(t => t.Name).Select(y => y.First()).ToList();
                    }
                    if (chkPassengerOnly.Checked)
                    {
                        dgHotelCatalog.DataSource = HotelValue.EntityList.Where(x => x.IsPassenger.ToString().ToUpper().Trim().Equals("TRUE")).ToList().GroupBy(t => t.Name).Select(y => y.First()).ToList();
                    }
                }
            }

            return filteredList;
        }
    }
}
