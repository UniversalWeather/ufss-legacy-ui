﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using Telerik.Web.UI;
using FlightPak.Web.Framework.Helpers;
using System.Drawing;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Web.Views.Settings.Logistics
{
    public partial class TransportCatalog : BaseSecuredPage
    {
        private List<string> TransportCodeList = new List<string>();
        private bool TransportPageNavigated = false;
        private bool IsValidateCustom = true;
        private ExceptionManager exManager;
        private bool _selectLastModified = false;

        //FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient();
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgTransportCatalog, dgTransportCatalog, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format("window['gridId'] = '{0}';", dgTransportCatalog.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            //Checking whether the querry string has values
                            CheckAutorization(Permission.Database.ViewTransport);
                            if (Request.QueryString["IcaoID"] != null)
                            {
                                tbICAO.Text = Server.UrlDecode(Request.QueryString["IcaoID"].ToString());
                                hdnICAO.Value = Server.UrlDecode(Request.QueryString["IcaoID"].ToString());
                            }
                            if (Request.QueryString["CityName"] != null)
                            {
                                tbCity.Text = Server.UrlDecode(Request.QueryString["CityName"].ToString());
                            }
                            if (Request.QueryString["StateName"] != null)
                            {
                                tbState.Text = Server.UrlDecode(Request.QueryString["StateName"].ToString());
                            }
                            if (Request.QueryString["CountryName"] != null)
                            {
                                tbCountry.Text = Server.UrlDecode(Request.QueryString["CountryName"].ToString());
                            }
                            if (Request.QueryString["AirportName"] != null)
                            {
                                tbAirport.Text = Server.UrlDecode(Request.QueryString["AirportName"].ToString());
                            }
                            if (Request.QueryString["AirportID"] != null)
                            {
                                hdnAirportId.Value = Server.UrlDecode(Request.QueryString["AirportID"].ToString());
                            }
                            DefaultSelection(true);
                        }
                        if (IsPopUp)
                        {
                            dgTransportCatalog.AllowPaging = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Transport);
                }
            }
        }
        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgTransportCatalog.Rebind();
                    }
                    if (dgTransportCatalog.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedTransportID"] = null;
                        //}
                        if (!String.IsNullOrEmpty(Request.QueryString["TransportId"]))
                        {
                            var transportId = Convert.ToInt64(Request.QueryString["TransportId"]);
                            for (int i = 0; i < dgTransportCatalog.Items.Count; i++)
                            {
                                if (dgTransportCatalog.Items[i].GetDataKeyValue("TransportID").ToString() == transportId.ToString())
                                {
                                    dgTransportCatalog.SelectedIndexes.Add(i);
                                    Session["SelectedTransportID"] = transportId.ToString();
                                    break;
                                }
                            }
                        }

                        if (Session["SelectedTransportID"] == null)
                        {
                            dgTransportCatalog.SelectedIndexes.Add(0);
                            Session["SelectedTransportID"] = dgTransportCatalog.Items[0].GetDataKeyValue("TransportID").ToString();
                        }

                        if (dgTransportCatalog.SelectedIndexes.Count == 0)
                            dgTransportCatalog.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                        //chkDisplayInctive.Enabled = false;
                        //chkDisplayChoiceOnly.Enabled = false;
                    }
                    GridEnable(true, true, true, true);
                    if (IsViewOnly)
                    {
                        if (BindDataSwitch)
                        {
                            dgTransportCatalog.Rebind();
                        }
                        if (dgTransportCatalog.MasterTableView.Items.Count > 0)
                        {
                            //if (!IsPostBack)
                            //{
                            //    Session["SelectedTransportID"] = null;
                            //}
                            if (Session["SelectedTransportID"] == null)
                            {
                                dgTransportCatalog.SelectedIndexes.Add(0);
                                Session["SelectedTransportID"] = dgTransportCatalog.Items[0].GetDataKeyValue("TransportID").ToString();
                            }

                            if (dgTransportCatalog.SelectedIndexes.Count == 0)
                                dgTransportCatalog.SelectedIndexes.Add(0);

                            ReadOnlyForm();
                        }
                        else
                        {
                            ClearForm();
                            EnableForm(false);
                            chkDisplayInctive.Enabled = false;
                            chkDisplayChoiceOnly.Enabled = false;
                        }
                        GridEnable(true, true, true, true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Prerender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgTransportCatalog_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveFlag.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (TransportPageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgTransportCatalog, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Transport);
                }
            }
        }
        /// <summary>
        /// To highlight the selected item which they have selected
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedTransportID"] != null)
                    {
                        string ID = Convert.ToString(Session["SelectedTransportID"]).Trim();
                        foreach (GridDataItem item in dgTransportCatalog.MasterTableView.Items)
                        {
                            if (item["TransportID"].Text.Trim() == ID)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To display UWA record in blue color in filter column grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgTransportCatalog_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            string Color = System.Configuration.ConfigurationManager.AppSettings["UWAColor"];
                            GridDataItem DataItem = e.Item as GridDataItem;
                            GridColumn Column = dgTransportCatalog.MasterTableView.GetColumn("chkUWAID");
                            if ((DataItem.GetDataKeyValue("PhoneNUM") == null) || (DataItem.GetDataKeyValue("PhoneNUM").ToString().Trim() == ""))
                            {

                                if ((DataItem.GetDataKeyValue("TollFreePhoneNum") != null) && (DataItem.GetDataKeyValue("TollFreePhoneNum").ToString().Trim() != ""))
                                {
                                    // GridColumn companyPhone = dgTransportCatalog.MasterTableView.GetColumn("ColCompanyPhone");

                                    GridDataItem ItemTemp = ((GridDataItem)e.Item);
                                    TableCell cellTemp = (TableCell)ItemTemp["PhoneNUM"];
                                    cellTemp.Text = DataItem["TollFreePhoneNum"].Text.ToString();
                                }

                            }
                            string UwaValue = DataItem["chkUWAID"].Text.Trim();
                            GridDataItem Item = (GridDataItem)e.Item;
                            TableCell cell = (TableCell)Item["chkUWAID"];
                            if (UwaValue.ToUpper().Trim() == "UWA")
                            {
                                System.Drawing.ColorConverter ColourConvert = new ColorConverter();
                                cell.ForeColor = (System.Drawing.Color)ColourConvert.ConvertFromString(Color);
                                cell.Font.Bold = true;
                                // cell.Text = "UWA";
                            }
                            //else
                            //{
                            //    cell.Text = "CUSTOM";
                            //}
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Transport);
                }
            }
        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgTransportCatalog_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnInitEdit = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInitEdit, divExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInitInsert = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInitInsert, divExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Transport);
                }
            }
        }
        /// <summary>
        /// To check unique Transport Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string CustNum = "";
                        string ID = Convert.ToString(Session["SelectedTransportID"]).Trim();
                        foreach (GridDataItem Item in dgTransportCatalog.MasterTableView.Items)
                        {
                            if (Item["TransportID"].Text.Trim() == ID.Trim())
                            {
                                if (Item.GetDataKeyValue("CustomerID") != null)
                                {
                                    CustNum = Item.GetDataKeyValue("CustomerID").ToString().Trim().Replace("&nbsp;", "");
                                }
                                break;
                            }
                        }
                        using (FlightPakMasterService.MasterCatalogServiceClient TransportService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var CodeValue = TransportService.GetTransportList().EntityList.Where(x => x.TransportCD.ToString().ToUpper().Equals(tbTransportCode.Text.ToString().ToUpper().Trim()) && x.CustomerID.ToString().ToUpper().Equals(CustNum.ToString().ToUpper()) && ((x.UWAMaintFlag.ToString().ToUpper().Trim() == "FALSE") || (!string.IsNullOrEmpty(x.UWAMaintFlag.ToString()))));
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Transport);
                }
            }
        }
        /// <summary>
        /// Bind Transport Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgTransportCatalog_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient TransportService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (IsPopUp && Session["SelectedAirportForTransportID"] != null)
                            {
                                hdnAirportId.Value = Session["SelectedAirportForTransportID"].ToString();
                            }
                            if (!string.IsNullOrEmpty(hdnAirportId.Value))
                            {
                                var TransportValue = TransportService.GetTransportByAirportID(Convert.ToInt64(hdnAirportId.Value));
                                Session["TransportCD"] = (List<GetTransportByAirportID>)TransportValue.EntityList.ToList();
                                if (TransportValue.ReturnFlag == true)
                                {
                                    if (!IsPopUp)
                                    {
                                        if (chkDisplayInctive.Checked == true)
                                        {
                                            //dgTransportCatalog.DataSource = TransportValue.EntityList;
                                        }
                                        else
                                        {
                                            //dgTransportCatalog.DataSource = TransportValue.EntityList.Where(x => x.IsInActive.ToString().ToUpper().Trim() == "FALSE");
                                            TransportValue.EntityList = TransportValue.EntityList.Where(x => x.IsInActive.ToString().ToUpper().Trim() == "FALSE").ToList();
                                        }

                                        if (chkDisplayChoiceOnly.Checked == true)
                                        {
                                            TransportValue.EntityList = TransportValue.EntityList.Where(x => x.IsChoice.ToString().ToUpper().Trim().Equals("TRUE")).ToList();
                                        }
                                    }
                                    else
                                    {
                                        TransportValue.EntityList = TransportValue.EntityList.ToList();
                                    }

                                    dgTransportCatalog.DataSource = TransportValue.EntityList.GroupBy(t => t.TransportationVendor).Select(y => y.First());
                                }
                                //else
                                //{
                                //    if ((TransportValue.ReturnFlag == true) && (chkInactive.Checked == true))
                                //    {
                                //        dgTransportCatalog.DataSource = TransportValue.EntityList.Where(x => x.AirportID.ToString().ToUpper().Trim().Contains(hdnAirportId.Value.ToString().ToUpper().Trim()) && (x.IsDeleted.ToString().ToUpper().Trim().Equals("FALSE")));

                                //    }
                                //}
                                // Session["TransportCD"] = (List<GetTransportByAirportID>)TransportValue.EntityList.ToList();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Transport);
                }
            }
        }
        /// <summary>
        /// Item Command for Transport Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgTransportCatalog_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSaveFlag.Value = "Update";
                                if (Session["SelectedTransportID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.Transport, Convert.ToInt64(Session["SelectedTransportID"].ToString().Trim()));
                                        Session["IsTransportEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Transport);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Transport);
                                            SelectItem();
                                            return;
                                        }
                                    }
                                    DisplayEditForm();
                                    GridEnable(false, true, false, false);
                                    chkDisplayInctive.Enabled = false;
                                    chkDisplayChoiceOnly.Enabled = false;
                                    SelectItem();
                                }
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbName);
                                // tbName.Focus();
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgTransportCatalog.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(true, false, false, false);
                                chkDisplayInctive.Enabled = false;
                                chkDisplayChoiceOnly.Enabled = false;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbName);
                                //tbName.Focus();                               
                                Session.Remove("SelectedTransportID");

                                break;
                            case "UpdateEdited":
                                dgTransportCatalog_UpdateCommand(sender, e);
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Transport);
                }
            }
        }
        /// <summary>
        /// To change the page index on paging        
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgTransportCatalog_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgTransportCatalog.ClientSettings.Scrolling.ScrollTop = "0";
                        TransportPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Transport);
                }
            }
        }
        /// <summary>
        /// Update Command for Transport Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgTransportCatalog_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAllReadyCountryExist();
                        CheckAllReadyMetroExist();
                        CheckExchangeRateExist();
                        if (IsValidateCustom)
                        {
                            if (Session["SelectedTransportID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objTransportService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var TransportVal = objTransportService.UpdateTransport(GetItems());
                                    if (TransportVal.ReturnFlag == true)
                                    {
                                        ShowSuccessMessage();

                                        ///////Update Method UnLock
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.Transport, Convert.ToInt64(Session["SelectedTransportID"].ToString().Trim()));
                                        }
                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        GridEnable(true, true, true, true);
                                        ClearForm();
                                        DefaultSelection(true);
                                        chkDisplayInctive.Enabled = true;
                                        chkDisplayChoiceOnly.Enabled = true;
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(TransportVal.ErrorMessage, ModuleNameConstants.Database.Transport);
                                    }
                                }
                            }
                        }
                        if (IsPopUp && (!IsViewOnly))
                        {
                            //Clear session & close browser
                            //Session["SelectedTransportID"] = null;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Transport);
                }
            }
        }
        /// <summary>
        /// Update Command for Transport Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgTransportCatalog_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        CheckAllReadyCountryExist();
                        CheckAllReadyMetroExist();
                        CheckExchangeRateExist();
                        if (IsValidateCustom)
                        {
                            if (IsTransportExists())
                            {

                                string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Transport already exists', 360, 50, 'Transport');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                            }
                            else
                            {
                                using (MasterCatalogServiceClient TransportService = new MasterCatalogServiceClient())
                                {
                                    var TransportVal = TransportService.AddTransport(GetItems());
                                    if (TransportVal.ReturnFlag == true)
                                    {
                                        ShowSuccessMessage();

                                        dgTransportCatalog.Rebind();
                                        ClearForm();
                                        DefaultSelection(false);
                                        chkDisplayInctive.Enabled = true;
                                        chkDisplayChoiceOnly.Enabled = true;
                                        _selectLastModified = true;
                                        if (IsPopUp && (!IsViewOnly))
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                                        }
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(TransportVal.ErrorMessage, ModuleNameConstants.Database.Transport);
                                    }
                                }
                            }
                        }
             

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Transport);
                }
            }
        }
        private bool IsTransportExists()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    List<FlightPakMasterService.GetTransportByAirportID> lstTransport = new List<FlightPakMasterService.GetTransportByAirportID>();
                    if (Session["TransportCD"] != null)
                    {
                        lstTransport = (List<FlightPakMasterService.GetTransportByAirportID>)Session["TransportCD"];
                    }

                    var searchTransport = lstTransport.Where(x => x.TransportationVendor.ToString().ToUpper().Trim().Equals(tbName.Text.ToString().ToUpper().Trim())).ToList();
                    if (searchTransport.Count > 0)
                    {
                        ReturnVal = true;
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);

                return ReturnVal;
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgTransportCatalog_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                string Code = string.Empty;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedTransportID"] != null)
                        {
                            Code = Convert.ToString(Session["SelectedTransportID"]).Trim();
                            string AirportID = "", IsUWAMaintFlag = "";
                            foreach (GridDataItem Item in dgTransportCatalog.MasterTableView.Items)
                            {
                                if (Item["TransportID"].Text.Trim() == Code)
                                {
                                    if (Item.GetDataKeyValue("AirportID") != null)
                                    {
                                        AirportID = Item.GetDataKeyValue("AirportID").ToString().Trim();
                                    }
                                    if (Item.GetDataKeyValue("UWAMaintFlag") != null)
                                    {
                                        IsUWAMaintFlag = Item.GetDataKeyValue("UWAMaintFlag").ToString().Trim();
                                    }
                                    break;
                                }
                            }
                            using (FlightPakMasterService.MasterCatalogServiceClient TransportService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.Transport TransportData = new FlightPakMasterService.Transport();
                                TransportData.AirportID = Convert.ToInt64(AirportID);
                                TransportData.TransportID = Convert.ToInt64(Code);
                                TransportData.IsDeleted = true;
                                if (!String.IsNullOrEmpty(IsUWAMaintFlag))
                                {
                                    TransportData.UWAMaintFlag = Convert.ToBoolean(IsUWAMaintFlag);
                                }
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.Transport, Convert.ToInt64(Code));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Transport);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Transport);
                                        return;
                                    }
                                }
                                TransportService.DeleteTransport(TransportData);
                                dgTransportCatalog.Rebind();
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                DefaultSelection(true);
                                chkDisplayInctive.Enabled = true;
                                chkDisplayChoiceOnly.Enabled = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Transport);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.Transport, Convert.ToInt64(Code));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgTransportCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSaveFlag.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            //GridDataItem Item = dgTransportCatalog.SelectedItems[0] as GridDataItem;
                            //Session["SelectedTransportID"] = Item["TransportID"].Text;
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgTransportCatalog.SelectedItems[0] as GridDataItem;
                                Session["SelectedTransportID"] = item["TransportID"].Text;
                                ReadOnlyForm();
                                GridEnable(true, true, true, true);
                            }

                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.Transport);
                    }
                }
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgTransportCatalog;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Transport);
                }
            }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbTransportCode.ReadOnly = true;
                    tbTransportCode.BackColor = System.Drawing.Color.White;
                    pnlExternalForm.Visible = true;
                    hdnSaveFlag.Value = "Save";
                    dgTransportCatalog.Rebind();
                    chkChoice.Checked = false;
                    tbName.Text = string.Empty;
                    tbPhone.Text = string.Empty;
                    tbFax.Text = string.Empty;
                    tbContact.Text = string.Empty;
                    tbNegotiatedPrice.Text = "000.0";
                    chkInactive.Checked = false;
                    tbTollFreeNo.Text = string.Empty;
                    tbWebsite.Text = string.Empty;
                    tbRemarks.Text = string.Empty;
                    ClearForm();
                    EnableForm(true);
                    btnCancel.Visible = true;
                    btnSaveChanges.Visible = true;
                    tbAltBusinessPhone.Text = string.Empty;
                    tbBusinessEmail.Text = string.Empty;
                    tbContactMobile.Text = string.Empty;
                    tbContactBusinessPhone.Text = string.Empty;
                    tbContactEmail.Text = string.Empty;
                    tbAddr1.Text = string.Empty;
                    tbAddr2.Text = string.Empty;
                    tbAddr3.Text = string.Empty;
                    tbTransportCountry.Text = string.Empty;
                    tbStateProvince.Text = string.Empty;
                    tbMetroCD.Text = string.Empty;
                    tbPostalCode.Text = string.Empty;
                    tbTransportCity.Text = string.Empty;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Clear form
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbTransportCode.Text = string.Empty;
                    tbName.Text = string.Empty;
                    tbPhone.Text = string.Empty;
                    tbFax.Text = string.Empty;
                    chkInactive.Checked = false;
                    tbNegotiatedPrice.Text = "000.0";
                    chkChoice.Checked = false;
                    tbTollFreeNo.Text = string.Empty;
                    tbWebsite.Text = string.Empty;
                    tbContact.Text = string.Empty;
                    tbRemarks.Text = string.Empty;
                    chkDisplayInctive.Checked = false;
                    chkDisplayChoiceOnly.Checked = false;
                    tbAltBusinessPhone.Text = string.Empty;
                    tbBusinessEmail.Text = string.Empty;
                    tbContactBusinessPhone.Text = string.Empty;
                    tbContactEmail.Text = string.Empty;
                    tbContactMobile.Text = string.Empty;
                    tbAddr1.Text = string.Empty;
                    tbAddr2.Text = string.Empty;
                    tbAddr3.Text = string.Empty;
                    tbTransportCountry.Text = string.Empty;
                    tbStateProvince.Text = string.Empty;
                    tbMetroCD.Text = string.Empty;
                    tbPostalCode.Text = string.Empty;
                    tbTransportCity.Text = string.Empty;
                    hdnCountryId.Value = string.Empty;
                    hdnMetroId.Value = string.Empty;
                    hdnExchangeRateID.Value = "";
                    tbExchangeRate.Text = "";
                    chkIsPassenger.Checked = false;
                    chkIsCrew.Checked = false;
                    tbNegotiateTerm.Text = "";
                    tbSundayWorkHours.Text = "";
                    tbMondayWorkHours.Text = "";
                    tbTuesdayWorkHours.Text = "";
                    tbWednesdayWorkHours.Text = "";
                    tbThursdayWorkHours.Text = "";
                    tbFridayWorkHours.Text = "";
                    tbSaturdayWorkHours.Text = "";

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Enable form
        /// </summary>
        /// <param name="Enable"></param>
        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbTransportCode.Enabled = false;
                    tbName.Enabled = Enable;
                    tbFax.Enabled = Enable;
                    chkChoice.Enabled = Enable;
                    tbNegotiatedPrice.Enabled = Enable;
                    tbPhone.Enabled = Enable;
                    tbTollFreeNo.Enabled = Enable;
                    tbWebsite.Enabled = Enable;
                    chkInactive.Enabled = Enable;
                    tbContact.Enabled = Enable;
                    tbRemarks.Enabled = Enable;
                    tbICAO.Enabled = Enable;
                    btnSaveChanges.Visible = Enable;
                    btnCancel.Visible = Enable;
                    tbAltBusinessPhone.Enabled = Enable;
                    tbBusinessEmail.Enabled = Enable;
                    tbContactBusinessPhone.Enabled = Enable;
                    tbContactEmail.Enabled = Enable;
                    tbAddr1.Enabled = Enable;
                    tbAddr2.Enabled = Enable;
                    tbAddr3.Enabled = Enable;
                    btnSearchExchange.Enabled = Enable;
                    tbTransportCountry.Enabled = Enable;
                    tbStateProvince.Enabled = Enable;
                    tbMetroCD.Enabled = Enable;
                    tbPostalCode.Enabled = Enable;
                    tbTransportCity.Enabled = Enable;
                    btnCountry.Enabled = Enable;
                    btnMetro.Enabled = Enable;
                    tbContactMobile.Enabled = Enable;
                    chkIsCrew.Enabled = Enable;
                    chkIsPassenger.Enabled = Enable;
                    tbExchangeRate.Enabled = Enable;
                    tbNegotiateTerm.Enabled = Enable;
                    tbSundayWorkHours.Enabled = Enable;
                    tbMondayWorkHours.Enabled = Enable;
                    tbTuesdayWorkHours.Enabled = Enable;
                    tbWednesdayWorkHours.Enabled = Enable;
                    tbThursdayWorkHours.Enabled = Enable;
                    tbFridayWorkHours.Enabled = Enable;
                    tbSaturdayWorkHours.Enabled = Enable;
                    //chkDisplayInctive.Enabled = !(Enable);
                    //chkDisplayChoiceOnly.Enabled = !(Enable);
                    if ((hdnSaveFlag.Value == "Update") && (Enable == true))
                    {
                        chkDisplayInctive.Enabled = false;
                        chkDisplayChoiceOnly.Enabled = false;
                    }
                    else
                    {
                        chkDisplayInctive.Enabled = true;
                        chkDisplayChoiceOnly.Enabled = true;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReadOnlyForm();
                    hdnSaveFlag.Value = "Update";
                    hdnRedirect.Value = "";
                    btnCancel.Visible = true;
                    btnSaveChanges.Visible = true;
                    dgTransportCatalog.Rebind();
                    if (!string.IsNullOrEmpty(hdnIsUWa.Value))
                    {
                        if (hdnIsUWa.Value.ToUpper().Trim() == "TRUE")
                        {
                            DisplayUwaRecords(true);

                        }
                        else
                        {
                            EnableForm(true);

                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected void DisplayUwaRecords(bool Enabled)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbTransportCode.Enabled = false;
                    tbName.Enabled = false;
                    tbFax.Enabled = false;
                    chkChoice.Enabled = true;
                    tbNegotiatedPrice.Enabled = true;
                    tbPhone.Enabled = false;
                    tbTollFreeNo.Enabled = false;
                    tbWebsite.Enabled = false;
                    chkInactive.Enabled = true;
                    tbContact.Enabled = true;
                    tbRemarks.Enabled = true;
                    tbICAO.Enabled = true;
                    btnSaveChanges.Visible = true;
                    btnCancel.Visible = true;
                    tbAltBusinessPhone.Enabled = true;
                    tbBusinessEmail.Enabled = true;
                    tbContactBusinessPhone.Enabled = true;
                    tbContactEmail.Enabled = true;
                    tbAddr1.Enabled = true;
                    tbAddr2.Enabled = true;
                    tbAddr3.Enabled = true;
                    tbTransportCountry.Enabled = true;
                    tbStateProvince.Enabled = true;
                    tbMetroCD.Enabled = true;
                    tbPostalCode.Enabled = true;
                    tbTransportCity.Enabled = true;
                    tbContactMobile.Enabled = true;
                    btnCountry.Enabled = true;
                    btnMetro.Enabled = true;
                    if ((hdnSaveFlag.Value == "Update") && (Enabled == true))
                    {
                        chkDisplayInctive.Enabled = false;
                        chkDisplayChoiceOnly.Enabled = false;                        
                    }
                    else
                    {
                        chkDisplayInctive.Enabled = true;
                        chkDisplayChoiceOnly.Enabled = true;                      
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Read Only form
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                    {
                        string Code = Convert.ToString(Session["SelectedTransportID"]).Trim();
                        string UWAMaintFlag = string.Empty;
                        string LastUpdUID = "", LastUpdTS = "";
                        foreach (GridDataItem Item in dgTransportCatalog.MasterTableView.Items)
                        {
                            if (Item["TransportID"].Text.Trim() == Code)
                            {
                                if (Item.GetDataKeyValue("TransportCD") != null)
                                {
                                    tbTransportCode.Text = Item.GetDataKeyValue("TransportCD").ToString().Trim().Replace("&nbsp;", "");
                                }
                                if (Item.GetDataKeyValue("TransportationVendor") != null)
                                {
                                    tbName.Text = Item.GetDataKeyValue("TransportationVendor").ToString().Trim().Replace("&nbsp;", "");
                                }
                                else
                                {
                                    tbName.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("IsChoice") != null)
                                {
                                    chkChoice.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsChoice").ToString().Trim().Replace("&nbsp;", ""));
                                }
                                else
                                {
                                    chkChoice.Checked = false;
                                }
                                if (Item.GetDataKeyValue("PhoneNUM") != null)
                                {
                                    tbPhone.Text = Item.GetDataKeyValue("PhoneNUM").ToString().Trim().Replace("&nbsp;", "");
                                }
                                else
                                {
                                    tbPhone.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("IsInActive") != null)
                                {
                                    chkInactive.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsInActive").ToString().Trim().Replace("&nbsp;", ""));
                                }
                                else
                                {
                                    chkInactive.Checked = false;
                                }
                                if (Item.GetDataKeyValue("FaxNUM") != null)
                                {
                                    tbFax.Text = Item.GetDataKeyValue("FaxNUM").ToString().Trim().Replace("&nbsp;", "");
                                }
                                else
                                {
                                    tbFax.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("ContactName") != null)
                                {
                                    tbContact.Text = Item.GetDataKeyValue("ContactName").ToString().Trim().Replace("&nbsp;", "");
                                }
                                else
                                {
                                    tbContact.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("NegotiatedRate") != null)
                                {
                                    tbNegotiatedPrice.Text = Item.GetDataKeyValue("NegotiatedRate").ToString().Trim().Replace("&nbsp;", "");
                                }
                                else
                                {
                                    tbNegotiatedPrice.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("TollFreePhoneNum") != null)
                                {
                                    tbTollFreeNo.Text = Item.GetDataKeyValue("TollFreePhoneNum").ToString().Trim().Replace("&nbsp;", "");
                                }
                                else
                                {
                                    tbTollFreeNo.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("WebSite") != null)
                                {
                                    tbWebsite.Text = Item.GetDataKeyValue("WebSite").ToString().Trim().Replace("&nbsp;", "");
                                }
                                else
                                {
                                    tbWebsite.Text = string.Empty;
                                }

                                if (Item.GetDataKeyValue("Remarks") != null)
                                {
                                    tbRemarks.Text = Item.GetDataKeyValue("Remarks").ToString().Trim().Replace("&nbsp;", "");
                                }
                                else
                                {
                                    tbRemarks.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("chkUWAID") != null)
                                {
                                    UWAMaintFlag = Item.GetDataKeyValue("chkUWAID").ToString().Trim().Replace("&nbsp;", "");
                                }
                                if (Item.GetDataKeyValue("AltBusinessPhone") != null)
                                {
                                    tbAltBusinessPhone.Text = Item.GetDataKeyValue("AltBusinessPhone").ToString().Trim();
                                }
                                else
                                {
                                    tbAltBusinessPhone.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("BusinessEmail") != null)
                                {
                                    tbBusinessEmail.Text = Item.GetDataKeyValue("BusinessEmail").ToString().Trim();
                                }
                                else
                                {
                                    tbBusinessEmail.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("CellPhoneNum") != null)
                                {
                                    tbContactMobile.Text = Item.GetDataKeyValue("CellPhoneNum").ToString().Trim();
                                }
                                else
                                {
                                    tbContactMobile.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("ContactBusinessPhone") != null)
                                {
                                    tbContactBusinessPhone.Text = Item.GetDataKeyValue("ContactBusinessPhone").ToString().Trim();
                                }
                                else
                                {
                                    tbContactBusinessPhone.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("ContactEmail") != null)
                                {
                                    tbContactEmail.Text = Item.GetDataKeyValue("ContactEmail").ToString().Trim();
                                }
                                else
                                {
                                    tbContactEmail.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Addr1") != null)
                                {
                                    tbAddr1.Text = Item.GetDataKeyValue("Addr1").ToString().Trim();
                                }
                                else
                                {
                                    tbAddr1.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Addr2") != null)
                                {
                                    tbAddr2.Text = Item.GetDataKeyValue("Addr2").ToString().Trim();
                                }
                                else
                                {
                                    tbAddr2.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Addr3") != null)
                                {
                                    tbAddr3.Text = Item.GetDataKeyValue("Addr3").ToString().Trim();
                                }
                                else
                                {
                                    tbAddr3.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("CountryCD") != null)
                                {
                                    tbTransportCountry.Text = Item.GetDataKeyValue("CountryCD").ToString().Trim();
                                }
                                else
                                {
                                    tbTransportCountry.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("StateProvince") != null)
                                {
                                    tbStateProvince.Text = Item.GetDataKeyValue("StateProvince").ToString().Trim();
                                }
                                else
                                {
                                    tbStateProvince.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("MetroCD") != null)
                                {
                                    tbMetroCD.Text = Item.GetDataKeyValue("MetroCD").ToString().Trim();
                                }
                                else
                                {
                                    tbMetroCD.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("PostalZipCD") != null)
                                {
                                    tbPostalCode.Text = Item.GetDataKeyValue("PostalZipCD").ToString().Trim();
                                }
                                else
                                {
                                    tbPostalCode.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("City") != null)
                                {
                                    tbTransportCity.Text = Item.GetDataKeyValue("City").ToString().Trim();
                                }
                                else
                                {
                                    tbTransportCity.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("Filter") != null)
                                {
                                    if (Item.GetDataKeyValue("Filter").ToString().ToUpper().Trim() == "UWA")
                                    {
                                        hdnIsUWa.Value = "True";
                                        lbWarningMessage.Visible = true;
                                    }
                                    else
                                    {
                                        hdnIsUWa.Value = "False";
                                        lbWarningMessage.Visible = false;
                                    }

                                }
                                else
                                {
                                    hdnIsUWa.Value = "False";
                                    lbWarningMessage.Visible = false;
                                }

                                if (Item.GetDataKeyValue("ExchangeRateID") != null)
                                {
                                    hdnExchangeRateID.Value = Convert.ToString(Item.GetDataKeyValue("ExchangeRateID").ToString());
                                    if (hdnExchangeRateID.Value.ToString().Trim() != "")
                                    {
                                        CheckExchangeRate();
                                    }
                                    else
                                    {
                                        tbExchangeRate.Text = "";
                                    }
                                }
                                else
                                {
                                    tbExchangeRate.Text = "";
                                }
                                if (Item.GetDataKeyValue("IsCrew") != null)
                                {
                                    chkIsCrew.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsCrew").ToString());
                                }
                                if (Item.GetDataKeyValue("IsPassenger") != null)
                                {
                                    chkIsPassenger.Checked = Convert.ToBoolean(Item.GetDataKeyValue("IsPassenger").ToString());
                                }
                                if (Item.GetDataKeyValue("NegotiatedTerms") != null)
                                {
                                    tbNegotiateTerm.Text = Convert.ToString(Item.GetDataKeyValue("NegotiatedTerms").ToString());
                                }
                                if (Item.GetDataKeyValue("SundayWorkHours") != null)
                                {
                                    tbSundayWorkHours.Text = Convert.ToString(Item.GetDataKeyValue("SundayWorkHours").ToString());
                                }
                                if (Item.GetDataKeyValue("MondayWorkHours") != null)
                                {
                                    tbMondayWorkHours.Text = Convert.ToString(Item.GetDataKeyValue("MondayWorkHours").ToString());
                                }
                                if (Item.GetDataKeyValue("TuesdayWorkHours") != null)
                                {
                                    tbTuesdayWorkHours.Text = Convert.ToString(Item.GetDataKeyValue("TuesdayWorkHours").ToString());
                                }
                                if (Item.GetDataKeyValue("WednesdayWorkHours") != null)
                                {
                                    tbWednesdayWorkHours.Text = Convert.ToString(Item.GetDataKeyValue("WednesdayWorkHours").ToString());
                                }
                                if (Item.GetDataKeyValue("ThursdayWorkHours") != null)
                                {
                                    tbThursdayWorkHours.Text = Convert.ToString(Item.GetDataKeyValue("ThursdayWorkHours").ToString());
                                }
                                if (Item.GetDataKeyValue("FridayWorkHours") != null)
                                {
                                    tbFridayWorkHours.Text = Convert.ToString(Item.GetDataKeyValue("FridayWorkHours").ToString());
                                }
                                if (Item.GetDataKeyValue("SaturdayWorkHours") != null)
                                {
                                    tbSaturdayWorkHours.Text = Convert.ToString(Item.GetDataKeyValue("SaturdayWorkHours").ToString());
                                }
                               
                                //New
                                if (Item.GetDataKeyValue("LastUpdUID") != null)
                                    LastUpdUID = Convert.ToString(Item.GetDataKeyValue("LastUpdUID"));//.ToString();
                                if (Item.GetDataKeyValue("LastUpdTS") != null)
                                    LastUpdTS = Convert.ToString(Item.GetDataKeyValue("LastUpdTS"));//.ToString();

                                lbColumnName1.Text = "Transport Code";
                                lbColumnName2.Text = "Company Name";
                                lbColumnValue1.Text = Item["TransportCD"].Text;
                                lbColumnValue2.Text = Item["TransportationVendor"].Text;
                                Item.Selected = true;
                                break;
                            }
                        }
                        if (!string.IsNullOrEmpty(UWAMaintFlag))
                        {
                            if (UWAMaintFlag.ToUpper().Trim() == "TRUE")
                            {

                                hdnIsUWa.Value = "True";

                            }
                        }

                        Label lbLastUpdatedUser;
                        lbLastUpdatedUser = (Label)dgTransportCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                        if (!string.IsNullOrEmpty(LastUpdUID))
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + LastUpdUID);
                        }
                        else
                        {
                            lbLastUpdatedUser.Text = string.Empty;
                        }
                        if (!string.IsNullOrEmpty(LastUpdTS))
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(LastUpdTS)));//To retrieve back the UTC date in Homebase format
                        }
                        tbTransportCode.ReadOnly = true;
                        tbTransportCode.BackColor = System.Drawing.Color.LightGray;
                        btnCancel.Visible = false;
                        btnSaveChanges.Visible = false;
                        EnableForm(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Command Event Trigger for Save or Update Transport Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveFlag.Value == "Update")
                        {
                            (dgTransportCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgTransportCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    
                    //This if else block and hdnSave.Value - "" are implemented because LastUpdTS is setting null
                    //when user modifies a Transport. It is updating when a Transport is added
                    if (!_selectLastModified)
                        hdnSaveFlag.Value = "";

                    //if (!String.IsNullOrEmpty(Request.QueryString["IsPopup"]))
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);

                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Transport);
                }
            }
        }
        /// <summary>
        /// Cancel Transport Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedTransportID"] != null)
                        {
                            // Unlock the Record
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Database.Transport, Convert.ToInt64(Session["SelectedTransportID"].ToString().Trim()));
                                //Session.Remove("SelectedTransportID");
                            }
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radTransportCodePopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }

                        if (!String.IsNullOrEmpty(Request.QueryString["IsPopup"]))
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radTransportCodePopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);

                        //GridEnable(true, true, true);
                        DefaultSelection(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Transport);
                }
            }

            hdnSaveFlag.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// Grid enable event
        /// </summary>
        /// <param name="Add"></param>
        /// <param name="Edit"></param>
        /// <param name="Delete"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete, bool Copy)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtnInitInsert = (LinkButton)dgTransportCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                    LinkButton lbtnInitDelete = (LinkButton)dgTransportCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitDelete");
                    LinkButton lbtnInitEdit = (LinkButton)dgTransportCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                    LinkButton lnkCopy = (LinkButton)dgTransportCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkCopy");
                    lnkCopy.Enabled = Copy;
                    if (IsAuthorized(Permission.Database.AddTransport))
                    {
                        lbtnInitInsert.Visible = true;
                        if (Add)
                            lbtnInitInsert.Enabled = true;
                        else
                            lbtnInitInsert.Enabled = false;
                    }
                    else
                    {
                        lbtnInitInsert.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteTransport))
                    {
                        lbtnInitDelete.Visible = true;
                        if (Delete)
                        {
                            lbtnInitDelete.Enabled = true;
                            lbtnInitDelete.OnClientClick = "javascript:return deleteRecord();";
                        }
                        else
                        {
                            lbtnInitDelete.Enabled = false;
                            lbtnInitDelete.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnInitDelete.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditTransport))
                    {
                        lbtnInitEdit.Visible = true;
                        if (Edit)
                        {
                            lbtnInitEdit.Enabled = true;
                            lbtnInitEdit.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtnInitEdit.Enabled = false;
                            lbtnInitEdit.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnInitEdit.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);

            }
        }

        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <returns></returns>
        private FlightPakMasterService.Transport GetItemsForCopy()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.Transport TransportService = new FlightPakMasterService.Transport();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    string Code = string.Empty;
                    if (Session["SelectedTransportID"] != null)
                    {
                        Code = Session["SelectedTransportID"].ToString().Trim();
                    }
                    string UWAMaintFlag = "";
                    // TransportService.TransportCD = tbTransportCode.Text;
                    TransportService.IsChoice = chkChoice.Checked;
                    TransportService.TransportationVendor = tbName.Text;
                    TransportService.PhoneNum = tbPhone.Text;
                    TransportService.FaxNum = tbFax.Text;
                    TransportService.ContactName = tbContact.Text;
                    TransportService.AltBusinessPhone = tbAltBusinessPhone.Text;
                    TransportService.BusinessEmail = tbBusinessEmail.Text;
                    TransportService.CellPhoneNum = tbContactMobile.Text;
                    TransportService.ContactBusinessPhone = tbContactBusinessPhone.Text;
                    TransportService.ContactEmail = tbContactEmail.Text;
                    TransportService.Addr1 = tbAddr1.Text;
                    TransportService.Addr2 = tbAddr2.Text;
                    TransportService.Addr3 = tbAddr3.Text;
                    if (!string.IsNullOrEmpty(hdnCountryId.Value))
                        TransportService.CountryID = Convert.ToInt64(hdnCountryId.Value);
                    TransportService.StateProvince = tbStateProvince.Text;
                    if (!string.IsNullOrEmpty(hdnMetroId.Value))
                        TransportService.MetroID = Convert.ToInt64(hdnMetroId.Value);
                    TransportService.City = tbTransportCity.Text;
                    TransportService.PostalZipCD = tbPostalCode.Text;
                    if (hdnSaveFlag.Value == "Update")
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var TransportList = FPKMasterService.GetTransportByTransportID(Convert.ToInt64(Code)).EntityList.ToList();
                            if (TransportList.Count > 0)
                            {
                                GetTransportByTransportID TransportEntity = TransportList[0];
                                if (TransportEntity.UWAMaintFlag != null)
                                {
                                    TransportService.UWAMaintFlag = TransportEntity.UWAMaintFlag;
                                }

                                TransportService.TransportID = -1;
                                
                                if (TransportEntity.UWAID != null)
                                {
                                    TransportService.UWAID = TransportEntity.UWAID;
                                }
                                if (TransportEntity.UWAUpdates != null)
                                {
                                    TransportService.UWAUpdates = TransportEntity.UWAUpdates;
                                }

                                if (TransportEntity.UpdateDT != null)
                                {
                                    TransportService.UpdateDT = TransportEntity.UpdateDT;
                                }
                                if (TransportEntity.RecordType != null)
                                {
                                    TransportService.RecordType = TransportEntity.RecordType;
                                }
                                if (TransportEntity.SourceID != null)
                                {
                                    TransportService.SourceID = TransportEntity.SourceID;
                                }
                                if (TransportEntity.ControlNum != null)
                                {
                                    TransportService.ControlNum = TransportEntity.ControlNum;
                                }

                            }
                        }

                    }
                    else
                    {
                        TransportService.UWAMaintFlag = false;
                        TransportService.TransportID = 0;
                        TransportService.UWAID = string.Empty;
                        TransportService.RecordType = string.Empty;
                        TransportService.SourceID = string.Empty;
                        TransportService.ControlNum = string.Empty;

                    }
                    if (tbNegotiatedPrice.Text != string.Empty)
                    {
                        TransportService.NegotiatedRate = Convert.ToDecimal(tbNegotiatedPrice.Text);
                    }
                    if (hdnAirportId.Value != null)
                    {
                        TransportService.AirportID = Convert.ToInt64(hdnAirportId.Value);
                    }

                    if (!string.IsNullOrEmpty(hdnExchangeRateID.Value))
                    {
                        TransportService.ExchangeRateID = Convert.ToInt64(hdnExchangeRateID.Value);
                    }
                    else
                    {
                        TransportService.ExchangeRateID = null;
                    }
                    TransportService.IsPassenger = chkIsPassenger.Checked;
                    TransportService.IsCrew = chkIsCrew.Checked;
                    if (!string.IsNullOrEmpty(tbNegotiateTerm.Text.Trim()))
                    {
                        TransportService.NegotiatedTerms = tbNegotiateTerm.Text.Trim(); //NegotiatedTerms
                    }
                    else
                    {
                        TransportService.NegotiatedTerms = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbSundayWorkHours.Text.Trim()))
                    {
                        TransportService.SundayWorkHours = tbSundayWorkHours.Text.Trim(); //SundayWorkHours
                    }
                    else
                    {
                        TransportService.SundayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbMondayWorkHours.Text.Trim()))
                    {
                        TransportService.MondayWorkHours = tbMondayWorkHours.Text; // MondayWorkHours
                    }
                    else
                    {
                        TransportService.MondayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbTuesdayWorkHours.Text.Trim()))
                    {
                        TransportService.TuesdayWorkHours = tbTuesdayWorkHours.Text.Trim(); //TuesdayWorkHours
                    }
                    else
                    {
                        TransportService.TuesdayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbWednesdayWorkHours.Text))
                    {
                        TransportService.WednesdayWorkHours = tbWednesdayWorkHours.Text; // WednesdayWorkHours
                    }
                    else
                    {
                        TransportService.WednesdayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbThursdayWorkHours.Text))
                    {
                        TransportService.ThursdayWorkHours = tbThursdayWorkHours.Text.Trim(); // ThursdayWorkHours
                    }
                    else
                    {
                        TransportService.ThursdayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbFridayWorkHours.Text.Trim()))
                    {
                        TransportService.FridayWorkHours = tbFridayWorkHours.Text.Trim(); //FridayWorkHours
                    }
                    else
                    {
                        TransportService.FridayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbSaturdayWorkHours.Text))
                    {
                        TransportService.SaturdayWorkHours = tbSaturdayWorkHours.Text; // SaturdayWorkHours
                    }
                    else
                    {
                        TransportService.SaturdayWorkHours = string.Empty;
                    }
                    TransportService.IsInActive = chkInactive.Checked;
                    TransportService.TollFreePhoneNum = tbTollFreeNo.Text;
                    TransportService.Website = tbWebsite.Text;
                    TransportService.Remarks = tbRemarks.Text;
                    TransportService.IsDeleted = false;
                    return TransportService;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return TransportService;
            }
        }

        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <returns></returns>
        private FlightPakMasterService.Transport GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.Transport TransportService = new FlightPakMasterService.Transport();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    string Code = string.Empty;
                    if (Session["SelectedTransportID"] != null)
                    {
                        Code = Session["SelectedTransportID"].ToString().Trim();
                    }
                    string UWAMaintFlag = "";
                    TransportService.TransportCD = tbTransportCode.Text;
                    TransportService.IsChoice = chkChoice.Checked;
                    TransportService.TransportationVendor = tbName.Text;
                    TransportService.PhoneNum = tbPhone.Text;
                    TransportService.FaxNum = tbFax.Text;
                    TransportService.ContactName = tbContact.Text;
                    TransportService.AltBusinessPhone = tbAltBusinessPhone.Text;
                    TransportService.BusinessEmail = tbBusinessEmail.Text;
                    TransportService.CellPhoneNum = tbContactMobile.Text;
                    TransportService.ContactBusinessPhone = tbContactBusinessPhone.Text;
                    TransportService.ContactEmail = tbContactEmail.Text;
                    TransportService.Addr1 = tbAddr1.Text;
                    TransportService.Addr2 = tbAddr2.Text;
                    TransportService.Addr3 = tbAddr3.Text;
                    if (!string.IsNullOrEmpty(hdnCountryId.Value))
                        TransportService.CountryID = Convert.ToInt64(hdnCountryId.Value);
                    TransportService.StateProvince = tbStateProvince.Text;
                    if (!string.IsNullOrEmpty(hdnMetroId.Value))
                        TransportService.MetroID = Convert.ToInt64(hdnMetroId.Value);
                    TransportService.City = tbTransportCity.Text;
                    TransportService.PostalZipCD = tbPostalCode.Text; 
                    if (hdnSaveFlag.Value == "Update")
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var TransportList = FPKMasterService.GetTransportByTransportID(Convert.ToInt64(Code)).EntityList.ToList();
                            if (TransportList.Count > 0)
                            {
                                GetTransportByTransportID TransportEntity = TransportList[0];
                                if (TransportEntity.UWAMaintFlag != null)
                                {
                                    TransportService.UWAMaintFlag = TransportEntity.UWAMaintFlag;
                                }
                                if (TransportEntity.TransportID != null)
                                {
                                    TransportService.TransportID = TransportEntity.TransportID;
                                }
                                if (TransportEntity.UWAID != null)
                                {
                                    TransportService.UWAID = TransportEntity.UWAID;
                                }
                                if (TransportEntity.UWAUpdates != null)
                                {
                                    TransportService.UWAUpdates = TransportEntity.UWAUpdates;
                                }

                                if (TransportEntity.UpdateDT != null)
                                {
                                    TransportService.UpdateDT = TransportEntity.UpdateDT;
                                }
                                if (TransportEntity.RecordType != null)
                                {
                                    TransportService.RecordType = TransportEntity.RecordType;
                                }
                                if (TransportEntity.SourceID != null)
                                {
                                    TransportService.SourceID = TransportEntity.SourceID;
                                }
                                if (TransportEntity.ControlNum != null)
                                {
                                    TransportService.ControlNum = TransportEntity.ControlNum;
                                }

                            }
                        }

                    }
                    else
                    {
                        TransportService.UWAMaintFlag = false;
                        TransportService.TransportID = 0;
                        TransportService.UWAID = string.Empty;
                        TransportService.RecordType = string.Empty;
                        TransportService.SourceID = string.Empty;
                        TransportService.ControlNum = string.Empty;

                    }
                    if (tbNegotiatedPrice.Text != string.Empty)
                    {
                        TransportService.NegotiatedRate = Convert.ToDecimal(tbNegotiatedPrice.Text);
                    }
                    if (hdnAirportId.Value != null)
                    {
                        TransportService.AirportID = Convert.ToInt64(hdnAirportId.Value);
                    }

                    if (!string.IsNullOrEmpty(hdnExchangeRateID.Value))
                    {
                        TransportService.ExchangeRateID = Convert.ToInt64(hdnExchangeRateID.Value);
                    }
                    else
                    {
                        TransportService.ExchangeRateID = null;
                    }
                    TransportService.IsPassenger = chkIsPassenger.Checked;
                    TransportService.IsCrew = chkIsCrew.Checked;
                    if (!string.IsNullOrEmpty(tbNegotiateTerm.Text.Trim()))
                    {
                        TransportService.NegotiatedTerms = tbNegotiateTerm.Text.Trim(); //NegotiatedTerms
                    }
                    else
                    {
                        TransportService.NegotiatedTerms = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbSundayWorkHours.Text.Trim()))
                    {
                        TransportService.SundayWorkHours = tbSundayWorkHours.Text.Trim(); //SundayWorkHours
                    }
                    else
                    {
                        TransportService.SundayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbMondayWorkHours.Text.Trim()))
                    {
                        TransportService.MondayWorkHours = tbMondayWorkHours.Text; // MondayWorkHours
                    }
                    else
                    {
                        TransportService.MondayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbTuesdayWorkHours.Text.Trim()))
                    {
                        TransportService.TuesdayWorkHours = tbTuesdayWorkHours.Text.Trim(); //TuesdayWorkHours
                    }
                    else
                    {
                        TransportService.TuesdayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbWednesdayWorkHours.Text))
                    {
                        TransportService.WednesdayWorkHours = tbWednesdayWorkHours.Text; // WednesdayWorkHours
                    }
                    else
                    {
                        TransportService.WednesdayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbThursdayWorkHours.Text))
                    {
                        TransportService.ThursdayWorkHours = tbThursdayWorkHours.Text.Trim(); // ThursdayWorkHours
                    }
                    else
                    {
                        TransportService.ThursdayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbFridayWorkHours.Text.Trim()))
                    {
                        TransportService.FridayWorkHours = tbFridayWorkHours.Text.Trim(); //FridayWorkHours
                    }
                    else
                    {
                        TransportService.FridayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbSaturdayWorkHours.Text))
                    {
                        TransportService.SaturdayWorkHours = tbSaturdayWorkHours.Text; // SaturdayWorkHours
                    }
                    else
                    {
                        TransportService.SaturdayWorkHours = string.Empty;
                    }
                    TransportService.IsInActive = chkInactive.Checked;
                    TransportService.TollFreePhoneNum = tbTollFreeNo.Text;
                    TransportService.Website = tbWebsite.Text;
                    TransportService.Remarks = tbRemarks.Text;
                    TransportService.IsDeleted = false;
                    return TransportService;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return TransportService;
            }
        }
        /// <summary>
        /// To check for unique Transport code
        /// </summary>
        /// <returns></returns>
        private bool checkAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    TransportCodeList = (List<string>)Session["CaterCode"];
                    if (TransportCodeList != null && TransportCodeList.Contains(tbTransportCode.Text.ToString().Trim()))
                    {
                        ReturnValue = true;
                    }
                    return ReturnValue;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnValue;
            }
        }
        /// <summary>
        /// Check changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkDisplayInctive_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (btnSaveChanges.Visible == false)
                        {
                            //using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                            //{
                            //    var TransportVal = objService.GetTransportByAirportID(Convert.ToInt64(hdnAirportId.Value));
                            //    if ((TransportVal.ReturnFlag == true) && (chkDisplayInctive.Checked == false))
                            //    {
                            //        dgTransportCatalog.DataSource = TransportVal.EntityList.Where(x => x.IsInActive.ToString().ToUpper().Trim() == "FALSE");
                            //        DefaultSelection(true);
                            //    }
                            //    if ((TransportVal.ReturnFlag == true) && (chkDisplayInctive.Checked == true))
                            //    {
                            //        dgTransportCatalog.DataSource = TransportVal.EntityList;
                            //        DefaultSelection(true);
                            //    }
                            //}
                            SearchAndFilter(true);
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Transport);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbTransportCountry_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAllReadyCountryExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {

                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// Method to check unique Country  Code 
        /// </summary>
        /// <returns></returns>
        private bool CheckAllReadyCountryExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                if ((tbTransportCountry.Text != null) && (tbTransportCountry.Text != string.Empty))
                {
                    //  FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient();
                    //To check for unique Country code
                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var CountryValue = FPKMasterService.GetCountryMasterList().EntityList.Where(x => x.CountryCD.ToString().ToUpper().Trim().Equals(tbTransportCountry.Text.ToString().ToUpper().Trim())).ToList();
                        if (CountryValue.Count() > 0 && CountryValue != null)
                        {

                            hdnCountryId.Value = ((FlightPakMasterService.Country)CountryValue[0]).CountryID.ToString().ToUpper().Trim();
                            tbTransportCountry.Text = ((FlightPakMasterService.Country)CountryValue[0]).CountryCD;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbTransportCountry);
                            //tbTransportCountry.Focus();
                        }
                        else
                        {
                            cvCountry.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbContact);
                            //tbContact.Focus();
                            ReturnValue = false;
                            IsValidateCustom = false;
                        }
                    }
                }
                return ReturnValue;
            }
        }
        /// <summary>
        /// To check unique Metro Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbMetroCD_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAllReadyMetroExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {

                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// Method to check unique Metro Code 
        /// </summary>
        /// <returns></returns>
        private bool CheckAllReadyMetroExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                if ((tbMetroCD.Text != null) && (tbMetroCD.Text != string.Empty))
                {
                    //FlightPakMasterService.MasterCatalogServiceClient MetroService = new FlightPakMasterService.MasterCatalogServiceClient();
                    //To check for unique metro code
                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var MetroValue = FPKMasterService.GetMetroCityList().EntityList.Where(x => x.MetroCD.ToString().ToUpper().Trim().Equals(tbMetroCD.Text.ToUpper().Trim())).ToList();
                        if (MetroValue.Count() > 0 && MetroValue != null)
                        {
                            hdnMetroId.Value = ((FlightPakMasterService.Metro)MetroValue[0]).MetroID.ToString().Trim();
                            tbMetroCD.Text = ((FlightPakMasterService.Metro)MetroValue[0]).MetroCD;
                        }
                        else
                        {
                            cvMetro.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbMetroCD);
                            // tbMetroCD.Focus();
                            ReturnValue = true;
                            IsValidateCustom = false;
                        }
                    }
                }
                return ReturnValue;
            }
        }
        /// <summary>
        /// Method to check unique Country  Code 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ExchangeRate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckExchangeRateExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Transport);
                }
            }
        }


        private void CheckExchangeRate()
        {
            using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var CountryValue = CountryService.GetExchangeRate().EntityList.Where(x => x.ExchangeRateID.ToString().Trim().ToUpper() == (hdnExchangeRateID.Value.ToString().Trim().ToUpper())).ToList();
                if (CountryValue.Count() > 0 && CountryValue != null)
                {
                    tbExchangeRate.Text = ((FlightPakMasterService.ExchangeRate)CountryValue[0]).ExchRateCD.ToString().Trim();
                    //RadAjaxManager.GetCurrent(Page).FocusControl(tbSaturdayWorkHours);
                    // tbPostal.Focus();
                }
                else
                {
                    tbExchangeRate.Text = "";
                }
            }
        }

        protected void lnkCopy_Click(object sender, EventArgs e)
        {
            CheckAllReadyCountryExist();
            CheckAllReadyMetroExist();
            CheckExchangeRateExist();
            if (IsValidateCustom)
            {
                if (IsTransportExists())
                {

                    string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                        + @" oManager.radalert('Transport already exists', 360, 50, 'Transport');";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                }
                else
                {
                    using (MasterCatalogServiceClient TransportService = new MasterCatalogServiceClient())
                    {
                        var TransportVal = TransportService.AddTransport(GetItemsForCopy());
                        if (TransportVal.ReturnFlag == true)
                        {
                            ShowSuccessMessage();
                        }
                        dgTransportCatalog.Rebind();
                        // ClearForm();
                        SelectItem();
                        chkDisplayInctive.Enabled = true;
                        chkDisplayChoiceOnly.Enabled = true;
                    }
                }
            }
        }
        /// <summary>
        ///  Method to check unique Country  Code 
        /// </summary>
        private void CheckExchangeRateExist()
        {
            if (!string.IsNullOrEmpty(tbExchangeRate.Text))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var CountryValue = CountryService.GetExchangeRate().EntityList.Where(x => x.ExchRateCD.Trim().ToUpper() == (tbExchangeRate.Text.Trim().ToUpper())).ToList();
                    if (CountryValue.Count() > 0 && CountryValue != null)
                    {
                        hdnExchangeRateID.Value = ((FlightPakMasterService.ExchangeRate)CountryValue[0]).ExchangeRateID.ToString().Trim();
                        tbExchangeRate.Text = ((FlightPakMasterService.ExchangeRate)CountryValue[0]).ExchRateCD;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbSaturdayWorkHours);
                        // tbPostal.Focus();
                    }
                    else
                    {
                        cvExchangeRate.IsValid = false;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbExchangeRate);
                        //tbCountry.Focus();
                        IsValidateCustom = false;
                    }
                }
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
            CheckIsWidget("PreInit");
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            if (IsPopUp || IsViewOnly)
                            {
                                //Set visible false to all controls.
                                //Container control visibility throwing some error
                                table1.Visible = false;
                                table2.Visible = false;
                                table3.Visible = false;
                                table4.Visible = false;
                                table5.Visible = false;
                                dgTransportCatalog.Visible = false;
                                if (IsViewOnly)
                                {
                                    dgTransportCatalog.Visible = true;
                                    table2.Visible = true;
                                    DefaultSelection(false);
                                }
                                if (!IsViewOnly)
                                {
                                    if (IsAdd)
                                    {
                                        (dgTransportCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                    }
                                    else
                                    {
                                        (dgTransportCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                    }
                                }

                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Transport);
                }
            }
            
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgTransportCatalog.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgTransportCatalog.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }

        private void CheckIsWidget(string LoadType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["IsWidget"] != null)
                {
                    if (Session["IsWidget"].ToString() == "1")
                    {
                        if (LoadType == "PreInit")
                        {
                            this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");
                        }                        
                    }
                }
            }
        }

        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetTransportByAirportID> lstTransport = new List<FlightPakMasterService.GetTransportByAirportID>();
                if (Session["TransportCD"] != null)
                {
                    lstTransport = (List<FlightPakMasterService.GetTransportByAirportID>)Session["TransportCD"];
                }
                if (lstTransport.Count != 0)
                {
                    if (chkDisplayInctive.Checked == false) { lstTransport = lstTransport.Where(x => x.IsInActive == false).ToList<GetTransportByAirportID>(); }
                    if (chkDisplayChoiceOnly.Checked == true) { lstTransport = lstTransport.Where(x => x.IsChoice == true).ToList<GetTransportByAirportID>(); }
                    dgTransportCatalog.DataSource = lstTransport.GroupBy(t => t.TransportationVendor).Select(y => y.First());                    
                    if (IsDataBind)
                    {
                        dgTransportCatalog.DataBind();
                    }
                }
                else
                {
                    chkDisplayChoiceOnly.Enabled = false;
                    chkDisplayInctive.Enabled = false;
                }
                ReadOnlyForm();
                return false;
            }
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgTransportCatalog.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            if (IsPopUp && Session["SelectedAirportForTransportID"] != null)
            {
                hdnAirportId.Value = Session["SelectedAirportForTransportID"].ToString();
            }

            var TransportValue = FPKMstService.GetTransportByAirportID(Convert.ToInt64(hdnAirportId.Value));
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, TransportValue);
            List<FlightPakMasterService.GetTransportByAirportID> filteredList = GetFilteredList(TransportValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.TransportID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgTransportCatalog.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgTransportCatalog.CurrentPageIndex = PageNumber;
            dgTransportCatalog.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetTransportByAirportID TransportValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedTransportID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                //This if else block and hdnSave.Value - "" are implemented because LastUpdTS is setting null
                //when user modifies a Hotel. It is updating when a Hotel is added
                if (hdnSaveFlag.Value == "Save")
                {
                    PrimaryKeyValue = TransportValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().TransportID;
                    Session["SelectedTransportID"] = PrimaryKeyValue;
                }
                else
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SelectedTransportID"].ToString());
                }

                hdnSaveFlag.Value = "";
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetTransportByAirportID> GetFilteredList(ReturnValueOfGetTransportByAirportID TransportValue)
        {
            List<FlightPakMasterService.GetTransportByAirportID> filteredList = new List<FlightPakMasterService.GetTransportByAirportID>();

            if (TransportValue.ReturnFlag)
            {
                filteredList = TransportValue.EntityList;
            }

            if (!IsPopUp)
            {
                if (!chkDisplayInctive.Checked) { filteredList = filteredList.Where(x => x.IsInActive.ToString().ToUpper().Trim() == "FALSE").ToList(); }
                if (chkDisplayChoiceOnly.Checked) { filteredList = filteredList.Where(x => x.IsChoice.ToString().ToUpper().Trim().Equals("TRUE")).ToList(); }
            }

            return filteredList;
        }
    }
}
