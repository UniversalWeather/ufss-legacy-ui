﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using Telerik.Web.UI;
using System.Drawing;
using System.Collections;
using System.Text;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Web.Views.Settings.Logistics
{
    public partial class CateringCatalog : BaseSecuredPage
    {
        #region constants
        private ExceptionManager exManager;
        private bool CateringPageNavigated = false;
        private string strCateringID = "";
        private bool IsValidateCustom = true;
        private bool _selectLastModified = false;
        //FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient();
        #endregion
        #region "Ëvents"
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["CateringID"] != null)
                Session["SelectedCateringID"] = Request.QueryString["CateringID"];
            if (Request.QueryString["AirportID"] != null)
                Session["SelectedAirportForCateringID"] = Request.QueryString["AirportID"];

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        {
                            // Grid Control could be ajaxified when the page is initially loaded.
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCateringCatalog, dgCateringCatalog, RadAjaxLoadingPanel1);
                            // Store the clientID of the grid to reference it later on the client
                            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format("window['gridId'] = '{0}';", dgCateringCatalog.ClientID));
                            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));
                            if (!IsPostBack)
                            {
                                CheckAutorization(Permission.Database.ViewCatering);
                                //Checking whether the querry string has values
                                if (Request.QueryString["IcaoID"] != null)
                                {
                                    tbICAO.Text = Server.UrlDecode(Request.QueryString["IcaoID"].ToString());
                                    //hdnAirportId.Value = Server.UrlDecode(Request.QueryString["IcaoID"].ToString());
                                }
                                if (Request.QueryString["CityName"] != null)
                                {
                                    tbCity.Text = Server.UrlDecode(Request.QueryString["CityName"].ToString());
                                }
                                if (Request.QueryString["StateName"] != null)
                                {
                                    tbState.Text = Server.UrlDecode(Request.QueryString["StateName"].ToString());
                                }
                                if (Request.QueryString["CountryName"] != null)
                                {
                                    tbCountry.Text = Server.UrlDecode(Request.QueryString["CountryName"].ToString());
                                }
                                if (Request.QueryString["AirportName"] != null)
                                {
                                    tbAirport.Text = Server.UrlDecode(Request.QueryString["AirportName"].ToString());
                                }
                                if (Request.QueryString["AirportID"] != null)
                                {
                                    hdnAirportId.Value = Server.UrlDecode(Request.QueryString["AirportID"].ToString());
                                }
                                DefaultSelection(true);
                            }
                            if (IsPopUp)
                            {
                                dgCateringCatalog.AllowPaging = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Catering);
                }
            }
        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCateringCatalog_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnEditButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, DivExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbntInsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbntInsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Catering);
                }
            }
        }
        /// <summary>
        /// To display UWA record in blue color in filter column grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCateringCatalog_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            string Color = System.Configuration.ConfigurationManager.AppSettings["UWAColor"];
                            GridDataItem DataItem = e.Item as GridDataItem;
                            GridColumn Column = dgCateringCatalog.MasterTableView.GetColumn("chkUWAID");
                            string UwaValue = DataItem["chkUWAID"].Text.Trim();
                            GridDataItem Item = (GridDataItem)e.Item;
                            TableCell cell = (TableCell)Item["chkUWAID"];
                            if (UwaValue.ToUpper().Trim() == "UWA")
                            {
                                System.Drawing.ColorConverter ColourConvert = new ColorConverter();
                                cell.ForeColor = (System.Drawing.Color)ColourConvert.ConvertFromString(Color);
                                cell.Font.Bold = true;
                                //cell.Text = "UWA";
                            }
                            //else
                            //{
                            //    cell.Text = "CUSTOM";
                            //}
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Catering);
                }
            }
        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCateringCatalog_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient CateringService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (IsPopUp && Session["SelectedAirportForCateringID"] != null)
                            {
                                hdnAirportId.Value = Session["SelectedAirportForCateringID"].ToString();
                            }
                            if (!string.IsNullOrEmpty(hdnAirportId.Value))
                            {
                                var CateringValue = CateringService.GetAllCateringByAirportID(Convert.ToInt64(hdnAirportId.Value));
                                Session["CateringCD"] = (List<GetAllCateringByAirportID>)CateringValue.EntityList.ToList();
                                if (CateringValue.ReturnFlag == true)
                                {
                                    if (!IsPopUp)
                                    {
                                        if (chkDisplayInctive.Checked == true)
                                        {
                                            CateringValue.EntityList = CateringValue.EntityList.Where(x => x.IsDeleted.ToString().ToUpper().Trim().Equals("FALSE")).ToList();
                                        }
                                        else
                                        {
                                            CateringValue.EntityList = CateringValue.EntityList.Where(x => x.IsDeleted.ToString().ToUpper().Trim().Equals("FALSE") && (x.IsInActive.ToString().ToUpper().Trim() == "FALSE")).ToList();
                                        }
                                        if (chkDisplayChoiceOnly.Checked == true)
                                        {
                                            CateringValue.EntityList = CateringValue.EntityList.Where(x => x.IsChoice.ToString().ToUpper().Trim().Equals("TRUE")).ToList();
                                        }
                                    }
                                    else
                                    {
                                        CateringValue.EntityList = CateringValue.EntityList.ToList();
                                    }
                                    dgCateringCatalog.DataSource = CateringValue.EntityList.GroupBy(t => t.CateringVendor).Select(y => y.First());  
                                }
                                //if (CateringValue.ReturnFlag == true)
                                //{
                                //    dgCateringCatalog.DataSource = CateringValue.EntityList.Where(x => x.AirportID.ToString().ToUpper().Trim().Equals(hdnAirportId.Value.ToString().ToUpper().Trim()) && (x.IsDeleted.ToString().ToUpper().Equals("FALSE")) && (x.IsInActive == false));
                                //}                                
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Catering);
                }
            }
        }
        /// <summary>
        /// Item Command for Catering Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCateringCatalog_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                hdnSaveFlag.Value = "Update";
                                if (Session["SelectedCateringID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.Catering, Convert.ToInt64(Session["SelectedCateringID"].ToString().Trim()));
                                        Session["IsCateringEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Catering);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Catering);
                                            SelectItem();
                                            return;
                                        }
                                        ClearForm();
                                        DisplayEditForm();
                                        GridEnable(false, true, false,false);
                                        chkDisplayInctive.Enabled = false;
                                        chkDisplayChoiceOnly.Enabled = false;
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbName);
                                        //tbName.Focus();
                                        SelectItem();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbName);
                                // tbName.Focus();
                                dgCateringCatalog.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                GridEnable(false, true, false, false);
                                chkDisplayInctive.Enabled = false;
                                chkDisplayChoiceOnly.Enabled = false;
                                break;
                            case "UpdateEdited":
                                dgCateringCatalog_UpdateCommand(sender, e);
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Catering);
                }
            }
        }
        /// <summary>
        /// To change the page index on paging        
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCateringCatalog_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCateringCatalog.ClientSettings.Scrolling.ScrollTop = "0";
                        CateringPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Catering);
                }
            }
        }
        /// <summary>
        /// Pre Render
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCateringCatalog_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveFlag.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (CateringPageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCateringCatalog, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Catering);
                }
            }
        }
        /// <summary>
        /// Update Command for Catering Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCateringCatalog_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAllReadyCountryExist();
                        CheckAllReadyMetroExist();
                        CheckExchangeRateExist();
                        if (IsValidateCustom)
                        {
                            if (Session["SelectedCateringID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objCateringService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var Result = objCateringService.UpdateCatering(GetItems());

                                    if (Result.ReturnFlag == true)
                                    {
                                        ///////Update Method UnLock
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.Catering, Convert.ToInt64(Session["SelectedCateringID"].ToString().Trim()));
                                        }

                                        //  tdSuccessMessage.InnerText = "Record saved successfully.";
                                        ShowSuccessMessage();

                                        e.Item.OwnerTableView.Rebind();
                                        e.Item.Selected = true;
                                        GridEnable(true, true, true, true);
                                        DefaultSelection(true);
                                        chkDisplayInctive.Enabled = true;
                                        chkDisplayChoiceOnly.Enabled = true;
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(Result.ErrorMessage, ModuleNameConstants.Database.Catering);
                                    }
                                    
                                }
                            }
                        }
                        if (IsPopUp && (!IsViewOnly))
                        {
                            if (Request.QueryString.HasKeys() && Request.QueryString["FromCaterLookup"] == "true")
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radCateringPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            else
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Catering);
                }
            }
        }
        /// <summary>
        /// Update Command for Catering Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCateringCatalog_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        CheckAllReadyCountryExist();
                        CheckAllReadyMetroExist();
                        CheckExchangeRateExist();
                        if (IsValidateCustom)
                        {
                            if (IsCateringExists())
                            {

                                string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                    + @" oManager.radalert('Caterer already exists', 360, 50, 'Catering');";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                            }
                            else
                            {
                                using (MasterCatalogServiceClient CateringService = new MasterCatalogServiceClient())
                                {
                                    var ReturnValue = CateringService.AddCatering(GetItems());
                                    if (ReturnValue.ReturnFlag == true)
                                    {
                                        //tdSuccessMessage.InnerText = "Record saved successfully.";
                                        ShowSuccessMessage();

                                        dgCateringCatalog.Rebind();
                                        GridEnable(true, true, true, true);
                                        DefaultSelection(false);
                                        chkDisplayInctive.Enabled = true;
                                        chkDisplayChoiceOnly.Enabled = true;
                                        _selectLastModified = true;
                                        if (Request.QueryString.HasKeys() && Request.QueryString["FromCaterLookup"] == "true")
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radCateringPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);                                        
                                        else if (IsPopUp && (!IsViewOnly))
                                        {
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();", true);
                                        }                                        
                                    }
                                    else
                                    {
                                        // For Data Anotation
                                        ProcessErrorMessage(ReturnValue.ErrorMessage, ModuleNameConstants.Database.Catering);
                                    }
                                }
                            }
                        }
                        
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Catering);
                }
            }
        }
        private bool IsCateringExists()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    List<FlightPakMasterService.GetAllCateringByAirportID> lstCatering = new List<FlightPakMasterService.GetAllCateringByAirportID>();
                    if (Session["CateringCD"] != null)
                    {
                        lstCatering = (List<FlightPakMasterService.GetAllCateringByAirportID>)Session["CateringCD"];
                    }

                    var searchCatering = lstCatering.Where(x => x.CateringVendor.ToString().ToUpper().Trim().Equals(tbName.Text.ToString().ToUpper().Trim())).ToList();
                    if (searchCatering.Count>0)
                    {
                        ReturnVal = true;
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);

                return ReturnVal;
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCateringCatalog_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedCateringID"] != null)
                        {
                            //GridDataItem Item = (GridDataItem)Session["SelectedItem"];
                            string Code = Convert.ToString(Session["SelectedCateringID"]).Trim();
                            string AirportID = "", CateringCD = "", UWAMaintFlag = "";
                            foreach (GridDataItem Item in dgCateringCatalog.MasterTableView.Items)
                            {
                                if (Item["CateringID"].Text.Trim() == Code)
                                {
                                    AirportID = Item.GetDataKeyValue("AirportID").ToString();
                                    //CateringCD = Item.GetDataKeyValue("CateringCD").ToString();
                                    UWAMaintFlag = Item.GetDataKeyValue("UWAMaintFlag").ToString();
                                    break;
                                }
                            }
                            using (FlightPakMasterService.MasterCatalogServiceClient CateringTypeService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.Catering CateringType = new FlightPakMasterService.Catering();
                                CateringType.AirportID = Convert.ToInt64(AirportID);
                                //CateringType.CateringCD = CateringCD;
                                // CateringType.UpdateDT = System.DateTime.Now;
                                CateringType.IsDeleted = true;
                                if (!string.IsNullOrEmpty(UWAMaintFlag))
                                {
                                    CateringType.UWAMaintFlag = Convert.ToBoolean(UWAMaintFlag);
                                }
                                if (!string.IsNullOrEmpty(Code))
                                {
                                    CateringType.CateringID = Convert.ToInt64(Code);
                                }
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.Catering, Convert.ToInt64(Session["SelectedCateringID"].ToString().Trim()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Catering);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Catering);
                                        return;
                                    }
                                }
                                CateringTypeService.DeleteCatering(CateringType);
                                DefaultSelection(false);
                                chkDisplayInctive.Enabled = true;
                                chkDisplayChoiceOnly.Enabled = true; 
                                dgCateringCatalog.Rebind();
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Catering);
                }
                finally
                {
                    if (Session["SelectedCateringID"] != null)
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var returnValue1 = CommonService.UnLock(EntitySet.Database.Catering, Convert.ToInt64(Session["SelectedCateringID"].ToString().Trim()));
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCateringCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSaveFlag.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            //GridDataItem GridItem = dgCateringCatalog.SelectedItems[0] as GridDataItem;
                            //Session["SelectedCateringID"] = GridItem["CateringID"].Text;
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgCateringCatalog.SelectedItems[0] as GridDataItem;
                                Session["SelectedCateringID"] = item["CateringID"].Text;
                                ReadOnlyForm();
                                GridEnable(true, true, true, true);
                            }
                            //if (Session["IsCateringEditLock"] == "True")
                            //{
                            //    // Unlock the Record
                            //    CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient();
                            //    var returnValue = CommonService.UnLock(EntitySet.Database.Catering, Convert.ToInt64(Session["SelectedItem"].ToString().Trim()));
                            //    Session["IsCateringEditLock"] = "False";
                            //}
                            //// Session["SelectedItem"] = (GridDataItem)dgCateringCatalog.SelectedItems[0];
                            ////GridDataItem Item = (GridDataItem)dgCateringCatalog.SelectedItems[0];
                            //GridDataItem Item = dgCateringCatalog.SelectedItems[0] as GridDataItem;
                            //Session["SelectedItem"] = Item["CateringID"].Text;
                            //Label lbUser = (Label)dgCateringCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                            //if (Item.GetDataKeyValue("LastUpdUID") != null)
                            //{
                            //    lbUser.Text = "Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString();
                            //}
                            //else
                            //{
                            //    lbUser.Text = string.Empty;
                            //}
                            //if (Item.GetDataKeyValue("LastUpdTS") != null)
                            //{
                            //    lbUser.Text = lbUser.Text + " Date: " + Item.GetDataKeyValue("LastUpdTS").ToString();
                            //}
                            //else
                            //{
                            //    lbUser.Text = string.Empty;
                            //}
                            //ReadOnlyForm();
                            //GridEnable(true, true, true);
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.Catering);
                    }
                }
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgCateringCatalog;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Catering);
                }
            }
        }

        protected void lnkCopy_Click(object sender, EventArgs e)
        {

            CheckAllReadyCountryExist();
            CheckAllReadyMetroExist();
            CheckExchangeRateExist();
            if (IsValidateCustom)
            {
                if (IsCateringExists())
                {

                    string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                        + @" oManager.radalert('Caterer already exists', 360, 50, 'Catering');";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                }
                else
                {
                    using (MasterCatalogServiceClient CateringService = new MasterCatalogServiceClient())
                    {
                        var ReturnValue = CateringService.AddCatering(GetItemsForCopy());
                        if (ReturnValue.ReturnFlag == true)
                        {
                            //tdSuccessMessage.InnerText = "Record copied successfully.";
                            ShowSuccessMessage();
                        }
                        dgCateringCatalog.Rebind();
                        GridEnable(true, true, true, true);
                        SelectItem();
                        chkDisplayInctive.Enabled = true;
                        chkDisplayChoiceOnly.Enabled = true;
                    }
                }
            }
        }

        /// <summary>
        /// Command Event Trigger for Save or Update catering Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveFlag.Value == "Update")
                        {
                            (dgCateringCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgCateringCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }                        
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    
                    //This if else block and hdnSaveFlag.Value - "" are implemented because LastUpdTS is setting date but without time
                    //when user adds a new Catering. It is not updating when a Catering is modified
                    if (!_selectLastModified)
                        hdnSaveFlag.Value = "";

                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Catering);
                }
            }
        }
        /// <summary>
        /// Cancel catering Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveFlag.Value == "Update")
                        {
                            if (Session["SelectedCateringID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.Catering, Convert.ToInt64(Session["SelectedCateringID"].ToString().Trim()));
                                }
                            }
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radCateringPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }

                        if (!String.IsNullOrEmpty(Request.QueryString["IsPopup"]))
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radCateringPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);

                        DefaultSelection(false);
                        //Session.Remove("SelectedCateringID");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Catering);
                }
            }

            hdnSaveFlag.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// To dispaly active and inactive records based on checkbox change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkDisplayInctive_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (btnSaveChanges.Visible == false)
                        {
                            //using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                            //{
                            //    var CateringVal = objService.GetAllCateringByAirportID(Convert.ToInt64(hdnAirportId.Value));
                            //    if ((CateringVal.ReturnFlag == true) && (chkDisplayInctive.Checked == false))
                            //    {
                            //        dgCateringCatalog.DataSource = CateringVal.EntityList.Where(x => x.IsDeleted.ToString().ToUpper().Equals("FALSE") && (x.IsInActive == false));
                            //        DefaultSelection(true);
                            //    }
                            //    if ((CateringVal.ReturnFlag == true) && (chkDisplayInctive.Checked == true))
                            //    {
                            //        dgCateringCatalog.DataSource = CateringVal.EntityList.Where(x => x.IsDeleted.ToString().ToUpper().Equals("FALSE"));
                            //        DefaultSelection(true);
                            //    }
                            //}
                            SearchAndFilter(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Catering);
                }
            }
        }
        /// <summary>
        /// To check unique country code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbCateringCountry_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAllReadyCountryExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// To check unique Metro Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbMetroCD_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAllReadyMetroExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        #endregion
        #region "Methods"
        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (!IsPopUp)
                    {
                        if (BindDataSwitch)
                        {
                            dgCateringCatalog.Rebind();
                        }
                        if (dgCateringCatalog.MasterTableView.Items.Count > 0)
                        {
                            //if (!IsPostBack)
                            //{
                            //    Session["SelectedCateringID"] = null;
                            //}
                            if (Session["SelectedCateringID"] == null)
                            {
                                dgCateringCatalog.SelectedIndexes.Add(0);
                                Session["SelectedCateringID"] = dgCateringCatalog.Items[0].GetDataKeyValue("CateringID").ToString();
                            }

                            if (dgCateringCatalog.SelectedIndexes.Count == 0)
                                dgCateringCatalog.SelectedIndexes.Add(0);

                            //ClearForm();
                            ReadOnlyForm();
                        }
                        else
                        {
                            ClearForm();
                            EnableForm(false);
                            //chkDisplayInctive.Enabled = false;
                            //chkDisplayChoiceOnly.Enabled = false;
                        }
                        GridEnable(true, true, true, true);
                    }
                    if (IsViewOnly)
                    {
                        if (BindDataSwitch)
                        {
                            dgCateringCatalog.Rebind();
                        }
                        if (dgCateringCatalog.MasterTableView.Items.Count > 0)
                        {
                            //if (!IsPostBack)
                            //{
                            //    Session["SelectedCateringID"] = null;
                            //}
                            if (Session["SelectedCateringID"] == null)
                            {
                                dgCateringCatalog.SelectedIndexes.Add(0);
                                Session["SelectedCateringID"] = dgCateringCatalog.Items[0].GetDataKeyValue("CateringID").ToString();
                            }

                            if (dgCateringCatalog.SelectedIndexes.Count == 0)
                                dgCateringCatalog.SelectedIndexes.Add(0);

                            //ClearForm();
                            ReadOnlyForm();
                        }
                        else
                        {
                            ClearForm();
                            EnableForm(false);
                            //chkDisplayInctive.Enabled = false;
                            //chkDisplayChoiceOnly.Enabled = false;
                        }
                        GridEnable(true, true, true, true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To select the selected item which they have selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedCateringID"] != null)
                    {
                        string ID = Convert.ToString(Session["SelectedCateringID"]).Trim();
                        foreach (GridDataItem Item in dgCateringCatalog.MasterTableView.Items)
                        {
                            if (Item["CateringID"].Text.Trim() == ID)
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Insert form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    pnlExternalForm.Visible = true;
                    hdnSaveFlag.Value = "Save";
                    dgCateringCatalog.Rebind();
                    chkChoice.Checked = false;
                    tbName.Text = string.Empty;
                    tbPhone.Text = string.Empty;
                    tbFax.Text = string.Empty;
                    tbContact.Text = string.Empty;
                    tbNegotiatedPrice.Text = "000.00";
                    chkInactive.Checked = false;
                    tbTollFreeNo.Text = string.Empty;
                    tbWebsite.Text = string.Empty;
                    tbRemarks.Text = string.Empty;
                    tbBusinessEmail.Text = string.Empty;
                    tbContactMobile.Text = string.Empty;
                    tbContactBusinessPhone.Text = string.Empty;
                    tbContactEmail.Text = string.Empty;
                    tbAddr1.Text = string.Empty;
                    tbAddr2.Text = string.Empty;
                    tbAddr3.Text = string.Empty;
                    tbCateringCountry.Text = string.Empty;
                    tbStateProvince.Text = string.Empty;
                    tbMetroCD.Text = string.Empty;
                    tbPostalCode.Text = string.Empty;
                    tbCateringCity.Text = string.Empty;
                    ClearForm();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Clear form
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCaterCode.Text = string.Empty;
                    tbName.Text = string.Empty;
                    tbPhone.Text = string.Empty;
                    tbFax.Text = string.Empty;
                    chkInactive.Checked = false;
                    tbNegotiatedPrice.Text = "0.00";
                    chkChoice.Checked = false;
                    tbTollFreeNo.Text = string.Empty;
                    tbWebsite.Text = string.Empty;
                    tbContact.Text = string.Empty;
                    tbRemarks.Text = string.Empty;
                    tbBusinessEmail.Text = string.Empty;
                    tbContactMobile.Text = string.Empty;
                    tbContactBusinessPhone.Text = string.Empty;
                    tbContactEmail.Text = string.Empty;
                    tbAddr1.Text = string.Empty;
                    tbAddr2.Text = string.Empty;
                    tbAddr3.Text = string.Empty;
                    tbCateringCountry.Text = string.Empty;
                    tbStateProvince.Text = string.Empty;
                    tbMetroCD.Text = string.Empty;
                    tbPostalCode.Text = string.Empty;
                    tbCateringCity.Text = string.Empty;
                    hdnIsUWa.Value = string.Empty;
                    hdnCountryId.Value = string.Empty;
                    hdnMetroId.Value = string.Empty;
                    hdnExchangeRateID.Value = "";
                    tbExchangeRate.Text = "";

                    tbNegotiateTerm.Text = "";
                    tbSundayWorkHours.Text = "";
                    tbMondayWorkHours.Text = "";
                    tbTuesdayWorkHours.Text = "";
                    tbWednesdayWorkHours.Text = "";
                    tbThursdayWorkHours.Text = "";
                    tbFridayWorkHours.Text = "";
                    tbSaturdayWorkHours.Text = "";
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Dispaly UWA Records
        /// 
        /// </summary>
        protected void DisplayUwaRecords(bool Enabled)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCaterCode.Enabled = false;
                    tbName.Enabled = false;
                    tbFax.Enabled = false;
                    chkChoice.Enabled = true;
                    tbNegotiatedPrice.Enabled = true;
                    tbPhone.Enabled = false;
                    tbTollFreeNo.Enabled = false;
                    tbWebsite.Enabled = false;
                    chkInactive.Enabled = true;
                    tbContact.Enabled = true;
                    tbRemarks.Enabled = true;
                    tbICAO.Enabled = true;
                    btnCancel.Visible = true;
                    btnSaveChanges.Visible = true;
                    tbBusinessEmail.Enabled = true;
                    tbContactMobile.Enabled = true;
                    tbContactBusinessPhone.Enabled = true;
                    tbContactEmail.Enabled = true;
                    tbAddr1.Enabled = true;
                    tbAddr2.Enabled = true;
                    tbAddr3.Enabled = true;
                    tbCateringCity.Enabled = true;
                    tbStateProvince.Enabled = true;
                    tbMetroCD.Enabled = true;
                    tbPostalCode.Enabled = true;
                    tbCateringCountry.Enabled = true;
                    btnCountry.Enabled = true;
                    btnMetro.Enabled = true;
                    if ((hdnSaveFlag.Value == "Update") && (Enabled == true))
                    {
                        chkDisplayInctive.Enabled = false;
                        chkDisplayChoiceOnly.Enabled = false;
                    }
                    else
                    {
                        chkDisplayInctive.Enabled = true;
                        chkDisplayChoiceOnly.Enabled = true;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Enable form
        /// </summary>
        /// <param name="Enable"></param>
        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCaterCode.Enabled = false;
                    tbName.Enabled = Enable;
                    tbFax.Enabled = Enable;
                    chkChoice.Enabled = Enable;
                    tbNegotiatedPrice.Enabled = Enable;
                    tbPhone.Enabled = Enable;
                    tbTollFreeNo.Enabled = Enable;
                    tbWebsite.Enabled = Enable;
                    chkInactive.Enabled = Enable;
                    tbContact.Enabled = Enable;
                    tbRemarks.Enabled = Enable;
                    tbICAO.Enabled = Enable;
                    btnCancel.Visible = Enable;
                    btnSaveChanges.Visible = Enable;
                    tbBusinessEmail.Enabled = Enable;
                    tbContactMobile.Enabled = Enable;
                    tbContactBusinessPhone.Enabled = Enable;
                    tbContactEmail.Enabled = Enable;
                    tbAddr1.Enabled = Enable;
                    tbAddr2.Enabled = Enable;
                    tbAddr3.Enabled = Enable;
                    tbCateringCity.Enabled = Enable;
                    tbStateProvince.Enabled = Enable;
                    tbMetroCD.Enabled = Enable;
                    tbPostalCode.Enabled = Enable;
                    tbCateringCountry.Enabled = Enable;
                    btnCountry.Enabled = Enable;
                    btnMetro.Enabled = Enable;
                    tbExchangeRate.Enabled = Enable;
                    tbNegotiateTerm.Enabled = Enable;
                    tbSundayWorkHours.Enabled = Enable;
                    tbMondayWorkHours.Enabled = Enable;
                    tbTuesdayWorkHours.Enabled = Enable;
                    tbWednesdayWorkHours.Enabled = Enable;
                    tbThursdayWorkHours.Enabled = Enable;
                    tbFridayWorkHours.Enabled = Enable;
                    tbSaturdayWorkHours.Enabled = Enable;
                    btnSearchExchange.Enabled = Enable;
                    //chkDisplayInctive.Enabled = !(Enable);
                    //chkDisplayChoiceOnly.Enabled = !(Enable);   
                    if ((hdnSaveFlag.Value == "Update") && (Enable == true))
                    {
                        chkDisplayInctive.Enabled = false;
                        chkDisplayChoiceOnly.Enabled = false;
                    }
                    else
                    {
                        chkDisplayInctive.Enabled = true;
                        chkDisplayChoiceOnly.Enabled = true;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReadOnlyForm();
                    hdnSaveFlag.Value = "Update";
                    hdnRedirect.Value = "";
                    dgCateringCatalog.Rebind();
                    if (!string.IsNullOrEmpty(hdnIsUWa.Value))
                    {
                        if (hdnIsUWa.Value.ToUpper().Trim() == "TRUE")
                        {
                            DisplayUwaRecords(true);
                        }
                        else
                        {
                            EnableForm(true);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Read Only form
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    string Code = "";
                    if (Session["SelectedCateringID"] != null)
                    {
                        Code = Convert.ToString(Session["SelectedCateringID"]).Trim();
                    }
                    string CateringCD = "", CateringVendor = "", IsChoice = "", PhoneNUM = "", IsInActive = "", FaxNUM = "",
                        ContactName = "", NegotiatedRate = "", TollFreePhoneNum = "",
                        WebSite = "", IcaoID = "", Remarks = "", LastUpdUID = "", LastUpdTS = "", UWAMaintFlag = "", ExchangeRateID = "", CustomField1 = "", CustomField2 = "", NegotiatedTerms = "", SundayWorkHours = "", MondayWorkHours = "", TuesdayWorkHours = "", WednesdayWorkHours = "", ThursdayWorkHours = "", FridayWorkHours = "", SaturdayWorkHours = "";
                    foreach (GridDataItem Item in dgCateringCatalog.MasterTableView.Items)
                    {
                        if (Item["CateringID"].Text.Trim() == Code)
                        {
                            CateringCD = Convert.ToString(Item.GetDataKeyValue("CateringCD"));
                            CateringVendor = Convert.ToString(Item.GetDataKeyValue("CateringVendor"));
                            IsChoice = Convert.ToString(Item.GetDataKeyValue("IsChoice"));
                            PhoneNUM = Convert.ToString(Item.GetDataKeyValue("PhoneNUM"));
                            IsInActive = Convert.ToString(Item.GetDataKeyValue("IsInActive"));
                            FaxNUM = Convert.ToString(Item.GetDataKeyValue("FaxNUM"));
                            ContactName = Convert.ToString(Item.GetDataKeyValue("ContactName"));
                            NegotiatedRate = Convert.ToString(Item.GetDataKeyValue("NegotiatedRate"));
                            TollFreePhoneNum = Convert.ToString(Item.GetDataKeyValue("TollFreePhoneNum"));
                            WebSite = Convert.ToString(Item.GetDataKeyValue("WebSite"));
                            if (Item.GetDataKeyValue("Filter") != null)
                            {
                                if (Item.GetDataKeyValue("Filter").ToString().ToUpper().Trim() == "UWA")
                                {
                                    hdnIsUWa.Value = "True";
                                    lbWarningMessage.Visible = true;
                                }
                                else
                                {
                                    hdnIsUWa.Value = "False";
                                    lbWarningMessage.Visible = false;
                                }

                            }
                            else
                            {
                                hdnIsUWa.Value = "False";
                                lbWarningMessage.Visible = false;
                            }
                            if (Item.GetDataKeyValue("BusinessEmail") != null)
                            {
                                tbBusinessEmail.Text = Item.GetDataKeyValue("BusinessEmail").ToString().Trim();
                            }
                            else
                            {
                                tbBusinessEmail.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("ContactBusinessPhone") != null)
                            {
                                tbContactBusinessPhone.Text = Item.GetDataKeyValue("ContactBusinessPhone").ToString().Trim();
                            }
                            else
                            {
                                tbContactBusinessPhone.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("CellPhoneNum") != null)
                            {
                                tbContactMobile.Text = Item.GetDataKeyValue("CellPhoneNum").ToString().Trim();
                            }
                            else
                            {
                                tbContactMobile.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("ContactEmail") != null)
                            {
                                tbContactEmail.Text = Item.GetDataKeyValue("ContactEmail").ToString().Trim();
                            }
                            else
                            {
                                tbContactEmail.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("Addr1") != null)
                            {
                                tbAddr1.Text = Item.GetDataKeyValue("Addr1").ToString().Trim();
                            }
                            else
                            {
                                tbAddr1.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("Addr2") != null)
                            {
                                tbAddr2.Text = Item.GetDataKeyValue("Addr2").ToString().Trim();
                            }
                            else
                            {
                                tbAddr2.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("Addr3") != null)
                            {
                                tbAddr3.Text = Item.GetDataKeyValue("Addr3").ToString().Trim();
                            }
                            else
                            {
                                tbAddr3.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("City") != null)
                            {
                                tbCateringCity.Text = Item.GetDataKeyValue("City").ToString().Trim();
                            }
                            else
                            {
                                tbCateringCity.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("StateProvince") != null)
                            {
                                tbStateProvince.Text = Item.GetDataKeyValue("StateProvince").ToString().Trim();
                            }
                            else
                            {
                                tbStateProvince.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("CountryCD") != null)
                            {
                                tbCateringCountry.Text = Item.GetDataKeyValue("CountryCD").ToString().Trim();
                            }
                            else
                            {
                                tbCateringCountry.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("MetroCD") != null)
                            {
                                tbMetroCD.Text = Item.GetDataKeyValue("MetroCD").ToString().Trim();
                            }
                            else
                            {
                                tbMetroCD.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("PostalZipCD") != null)
                            {
                                tbPostalCode.Text = Item.GetDataKeyValue("PostalZipCD").ToString().Trim();
                            }
                            else
                            {
                                tbPostalCode.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("ExchangeRateID") != null)
                            {
                                ExchangeRateID = Convert.ToString(Item.GetDataKeyValue("ExchangeRateID").ToString());
                            }

                            if (Item.GetDataKeyValue("NegotiatedTerms") != null)
                            {
                                NegotiatedTerms = Convert.ToString(Item.GetDataKeyValue("NegotiatedTerms").ToString());
                            }
                            if (Item.GetDataKeyValue("SundayWorkHours") != null)
                            {
                                SundayWorkHours = Convert.ToString(Item.GetDataKeyValue("SundayWorkHours").ToString());
                            }
                            if (Item.GetDataKeyValue("MondayWorkHours") != null)
                            {
                                MondayWorkHours = Convert.ToString(Item.GetDataKeyValue("MondayWorkHours").ToString());
                            }
                            if (Item.GetDataKeyValue("TuesdayWorkHours") != null)
                            {
                                TuesdayWorkHours = Convert.ToString(Item.GetDataKeyValue("TuesdayWorkHours").ToString());
                            }
                            if (Item.GetDataKeyValue("WednesdayWorkHours") != null)
                            {
                                WednesdayWorkHours = Convert.ToString(Item.GetDataKeyValue("WednesdayWorkHours").ToString());
                            }
                            if (Item.GetDataKeyValue("ThursdayWorkHours") != null)
                            {
                                ThursdayWorkHours = Convert.ToString(Item.GetDataKeyValue("ThursdayWorkHours").ToString());
                            }
                            if (Item.GetDataKeyValue("FridayWorkHours") != null)
                            {
                                FridayWorkHours = Convert.ToString(Item.GetDataKeyValue("FridayWorkHours").ToString());
                            }
                            if (Item.GetDataKeyValue("SaturdayWorkHours") != null)
                            {
                                SaturdayWorkHours = Convert.ToString(Item.GetDataKeyValue("SaturdayWorkHours").ToString());
                            }
                            Remarks = Convert.ToString(Item.GetDataKeyValue("Remarks"));
                            LastUpdUID = Convert.ToString(Item.GetDataKeyValue("LastUpdUID"));
                            LastUpdTS = Convert.ToString(Item.GetDataKeyValue("LastUpdTS"));
                            if (Item.GetDataKeyValue("UWAMaintFlag") != null)
                            {
                                UWAMaintFlag = Item.GetDataKeyValue("UWAMaintFlag").ToString();
                            }
                            if (!string.IsNullOrEmpty(UWAMaintFlag))
                            {
                                if (UWAMaintFlag.Trim().ToUpper() == "TRUE")
                                {
                                    hdnIsUWa.Value = "True";
                                }
                                else
                                {
                                    hdnIsUWa.Value = "False";
                                }
                            }
                            if (!string.IsNullOrEmpty(ExchangeRateID))
                            {
                                hdnExchangeRateID.Value = ExchangeRateID;
                                if (hdnExchangeRateID.Value.ToString().Trim() != "")
                                {
                                    CheckExchangeRate();
                                }
                                else
                                {
                                    tbExchangeRate.Text = "";
                                }
                            }

                            if (!string.IsNullOrEmpty(NegotiatedTerms))
                            {
                                tbNegotiateTerm.Text = NegotiatedTerms;
                            }
                            if (!string.IsNullOrEmpty(SundayWorkHours))
                            {
                                tbSundayWorkHours.Text = SundayWorkHours;
                            }
                            if (!string.IsNullOrEmpty(MondayWorkHours))
                            {
                                tbMondayWorkHours.Text = MondayWorkHours;
                            }
                            if (!string.IsNullOrEmpty(TuesdayWorkHours))
                            {
                                tbTuesdayWorkHours.Text = TuesdayWorkHours;
                            }
                            if (!string.IsNullOrEmpty(WednesdayWorkHours))
                            {
                                tbWednesdayWorkHours.Text = WednesdayWorkHours;
                            }
                            if (!string.IsNullOrEmpty(ThursdayWorkHours))
                            {
                                tbThursdayWorkHours.Text = ThursdayWorkHours;
                            }
                            if (!string.IsNullOrEmpty(FridayWorkHours))
                            {
                                tbFridayWorkHours.Text = FridayWorkHours;
                            }
                            if (!string.IsNullOrEmpty(SaturdayWorkHours))
                            {
                                tbSaturdayWorkHours.Text = SaturdayWorkHours;
                            }
                            Label lbLastUpdatedUser;
                            lbLastUpdatedUser = (Label)dgCateringCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                            if (!string.IsNullOrEmpty(LastUpdUID))
                            {
                                lbLastUpdatedUser.Text = HttpUtility.HtmlEncode("Last Updated User: " + LastUpdUID);
                            }
                            else
                            {
                                lbLastUpdatedUser.Text = string.Empty;
                            }
                            if (!string.IsNullOrEmpty(LastUpdTS))
                            {
                                lbLastUpdatedUser.Text = HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(LastUpdTS)));//To retrieve back the UTC date in Homebase format
                            }

                            lbColumnName1.Text = "Catering Code";
                            lbColumnName2.Text = "Company Name";
                            lbColumnValue1.Text = Item["CateringCD"].Text;
                            lbColumnValue2.Text = Item["CateringVendor"].Text;
                            Item.Selected = true;
                            EnableForm(false);
                            break;
                        }
                    }
                    tbCaterCode.Text = CateringCD;
                    if (!string.IsNullOrEmpty(CateringVendor))
                    {
                        tbName.Text = CateringVendor;
                    }
                    else
                    {
                        tbName.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(IsChoice))
                    {
                        chkChoice.Checked = Convert.ToBoolean(IsChoice);
                    }
                    else
                    {
                        chkChoice.Checked = false;
                    }
                    if (!string.IsNullOrEmpty(PhoneNUM))
                    {
                        tbPhone.Text = PhoneNUM;
                    }
                    else
                    {
                        tbPhone.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(IsInActive))
                    {
                        chkInactive.Checked = Convert.ToBoolean(IsInActive);
                    }
                    else
                    {
                        chkInactive.Checked = false;
                    }
                    if (!string.IsNullOrEmpty(FaxNUM))
                    {
                        tbFax.Text = FaxNUM;
                    }
                    else
                    {
                        tbFax.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(ContactName))
                    {
                        tbContact.Text = ContactName;
                    }
                    else
                    {
                        tbContact.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(NegotiatedRate))
                    {
                        tbNegotiatedPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(NegotiatedRate), 2));
                    }
                    else
                    {
                        tbNegotiatedPrice.Text = "0.00";
                    }
                    if (!string.IsNullOrEmpty(TollFreePhoneNum))
                    {
                        tbTollFreeNo.Text = TollFreePhoneNum;
                    }
                    else
                    {
                        tbTollFreeNo.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(WebSite))
                    {
                        tbWebsite.Text = WebSite;
                    }
                    else
                    {
                        tbWebsite.Text = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(Remarks))
                    {
                        tbRemarks.Text = Remarks;
                    }
                    else
                    {
                        tbRemarks.Text = string.Empty;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Grid enable event
        /// </summary>
        /// <param name="Add"></param>
        /// <param name="Edit"></param>
        /// <param name="Delete"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete,bool Copy)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton lbtnInsertCtl = (LinkButton)dgCateringCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                    LinkButton lbtnDeleteCtl = (LinkButton)dgCateringCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                    LinkButton lbtnEditCtl = (LinkButton)dgCateringCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                    LinkButton lnkCopy = (LinkButton)dgCateringCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkCopy");
                    lnkCopy.Enabled = Copy;
                    if (IsAuthorized(Permission.Database.AddCatering))
                    {
                        lbtnInsertCtl.Visible = true;
                        if (Add)
                        {
                            lbtnInsertCtl.Enabled = true;
                        }
                        else
                        {
                            lbtnInsertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        lbtnInsertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.DeleteCatering))
                    {
                        lbtnDeleteCtl.Visible = true;
                        if (Delete)
                        {
                            lbtnDeleteCtl.Enabled = true;
                            lbtnDeleteCtl.OnClientClick = "javascript:return deleterecord();";
                        }
                        else
                        {
                            lbtnDeleteCtl.Enabled = false;
                            lbtnDeleteCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnDeleteCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.Database.EditCatering))
                    {
                        lbtnEditCtl.Visible = true;
                        if (Edit)
                        {
                            lbtnEditCtl.Enabled = true;
                            lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            lbtnEditCtl.Enabled = false;
                            lbtnEditCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        lbtnEditCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private Catering GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.Catering CateringService = new FlightPakMasterService.Catering();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    string Code = string.Empty;
                    if (Session["SelectedCateringID"] != null)
                    {
                        Code = Convert.ToString(Session["SelectedCateringID"]).Trim();
                    }
                    string UWAMaintFlag = "", CateringID = "";
                    CateringService.AirportID = Convert.ToInt64(Request.QueryString["AirportID"].ToString());
                    CateringService.CateringCD = Convert.ToString(tbCaterCode.Text);
                    CateringService.IsChoice = chkChoice.Checked;
                    CateringService.CateringVendor = Convert.ToString(tbName.Text);
                    CateringService.PhoneNum = Convert.ToString(tbPhone.Text);
                    CateringService.FaxNum = Convert.ToString(tbFax.Text);
                    CateringService.ContactName = Convert.ToString(tbContact.Text);
                    if (hdnSaveFlag.Value == "Update")
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var CateringList = FPKMasterService.GetAllCateringByCateringID(Convert.ToInt64(Code)).EntityList.ToList();
                            if (CateringList.Count > 0)
                            {
                                GetAllCateringByCateringID CateringEntity = CateringList[0];
                                if (CateringEntity.UWAID != null)
                                    CateringService.UWAID = CateringEntity.UWAID;
                                if (CateringEntity.UWAMaintFlag != null)
                                {
                                    CateringService.UWAMaintFlag = CateringEntity.UWAMaintFlag;
                                }
                                if (CateringEntity.UpdateDT != null)
                                {
                                    CateringService.UpdateDT = CateringEntity.UpdateDT;
                                }
                                if (CateringEntity.RecordType != null)
                                {
                                    CateringService.RecordType = CateringEntity.RecordType;
                                }
                                if (CateringEntity.SourceID != null)
                                {
                                    CateringService.SourceID = CateringEntity.SourceID;
                                }
                                if (CateringEntity.ControlNum != null)
                                {
                                    CateringService.ControlNum = CateringEntity.ControlNum;
                                }
                                if (CateringEntity.UWAUpdates != null)
                                {
                                    CateringService.UWAUpdates = CateringEntity.UWAUpdates;
                                }
                                CateringService.CateringID = Convert.ToInt64(Code);
                            }
                        }
                    }
                    else
                    {
                        CateringService.UWAMaintFlag = false;
                        CateringService.CateringID = 0;
                        CateringService.UWAID = string.Empty;
                    }
                    if (tbNegotiatedPrice.Text != string.Empty)
                    {
                        CateringService.NegotiatedRate = Convert.ToDecimal(tbNegotiatedPrice.Text);
                    }
                    else
                    {
                        CateringService.NegotiatedRate = 0;
                    }
                    CateringService.IsInActive = chkInactive.Checked;
                    CateringService.TollFreePhoneNum = Convert.ToString(tbTollFreeNo.Text);
                    CateringService.WebSite = Convert.ToString(tbWebsite.Text);
                    CateringService.Remarks = Convert.ToString(tbRemarks.Text);
                    CateringService.IsDeleted = false;
                    CateringService.BusinessEmail = Convert.ToString(tbBusinessEmail.Text);
                    CateringService.CellPhoneNum = Convert.ToString(tbContactMobile.Text);
                    CateringService.ContactBusinessPhone = Convert.ToString(tbContactBusinessPhone.Text);
                    CateringService.ContactEmail = Convert.ToString(tbContactEmail.Text);
                    CateringService.Addr1 = Convert.ToString(tbAddr1.Text);
                    CateringService.Addr2 = Convert.ToString(tbAddr2.Text);
                    CateringService.Addr3 = Convert.ToString(tbAddr3.Text);
                    CateringService.City = Convert.ToString(tbCateringCity.Text);
                    CateringService.PostalZipCD = tbPostalCode.Text;
                    if (!string.IsNullOrEmpty(hdnExchangeRateID.Value))
                    {
                        CateringService.ExchangeRateID = Convert.ToInt64(hdnExchangeRateID.Value);
                    }
                    else
                    {
                        CateringService.ExchangeRateID = null;
                    }
                    //if (!string.IsNullOrEmpty(tbCustomField1.Text))
                    //{
                    //    CateringService.CustomField1 = tbCustomField1.Text; //CustomField1
                    //}
                    //else
                    //{
                    //    CateringService.CustomField1 = string.Empty;
                    //}
                    //if (!string.IsNullOrEmpty(tbCustomField2.Text))
                    //{
                    //    CateringService.CustomField2 = tbCustomField2.Text; // CustomField2
                    //}
                    //else
                    //{
                    //    CateringService.CustomField2 = string.Empty;
                    //}
                    //
                    if (!string.IsNullOrEmpty(tbNegotiateTerm.Text.Trim()))
                    {
                        CateringService.NegotiatedTerms = tbNegotiateTerm.Text.Trim(); //NegotiatedTerms
                    }
                    else
                    {
                        CateringService.NegotiatedTerms = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbSundayWorkHours.Text.Trim()))
                    {
                        CateringService.SundayWorkHours = tbSundayWorkHours.Text.Trim(); //SundayWorkHours
                    }
                    else
                    {
                        CateringService.SundayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbMondayWorkHours.Text.Trim()))
                    {
                        CateringService.MondayWorkHours = tbMondayWorkHours.Text; // MondayWorkHours
                    }
                    else
                    {
                        CateringService.MondayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbTuesdayWorkHours.Text.Trim()))
                    {
                        CateringService.TuesdayWorkHours = tbTuesdayWorkHours.Text.Trim(); //TuesdayWorkHours
                    }
                    else
                    {
                        CateringService.TuesdayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbWednesdayWorkHours.Text))
                    {
                        CateringService.WednesdayWorkHours = tbWednesdayWorkHours.Text; // WednesdayWorkHours
                    }
                    else
                    {
                        CateringService.WednesdayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbThursdayWorkHours.Text))
                    {
                        CateringService.ThursdayWorkHours = tbThursdayWorkHours.Text.Trim(); // ThursdayWorkHours
                    }
                    else
                    {
                        CateringService.ThursdayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbFridayWorkHours.Text.Trim()))
                    {
                        CateringService.FridayWorkHours = tbFridayWorkHours.Text.Trim(); //FridayWorkHours
                    }
                    else
                    {
                        CateringService.FridayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbSaturdayWorkHours.Text))
                    {
                        CateringService.SaturdayWorkHours = tbSaturdayWorkHours.Text; // SaturdayWorkHours
                    }
                    else
                    {
                        CateringService.SaturdayWorkHours = string.Empty;
                    }

                    CateringService.StateProvince = Convert.ToString(tbStateProvince.Text);
                    if (!string.IsNullOrEmpty(hdnCountryId.Value))
                        CateringService.CountryID = Convert.ToInt64(hdnCountryId.Value);
                    if (!string.IsNullOrEmpty(hdnMetroId.Value))
                        CateringService.MetroID = Convert.ToInt64(hdnMetroId.Value);
                    return CateringService;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return CateringService;
            }
        }
        /// <summary>
        /// To check for unique Transport code
        /// </summary>
        /// <returns></returns>
        private bool checkAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnVal = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    List<string> lstCaterCode = (List<string>)Session["CaterCode"];
                    if (lstCaterCode != null && lstCaterCode.Contains(tbCaterCode.Text.ToString().Trim()))
                    {
                        ReturnVal = true;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ReturnVal;
            }
        }
        /// <summary>
        /// Method to check unique Country  Code 
        /// </summary>
        /// <returns></returns>
        private bool CheckAllReadyCountryExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                if ((tbCateringCountry.Text != null) && (tbCateringCountry.Text != string.Empty))
                {
                    //To check for unique Country code
                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var CountryValue = FPKMasterService.GetCountryMasterList().EntityList.Where(x => x.CountryCD.ToString().ToUpper().Trim().Equals(tbCateringCountry.Text.ToString().ToUpper().Trim())).ToList();
                        if (CountryValue.Count() > 0 && CountryValue != null)
                        {
                            hdnCountryId.Value = ((FlightPakMasterService.Country)CountryValue[0]).CountryID.ToString().ToUpper().Trim();
                            tbCateringCountry.Text = ((FlightPakMasterService.Country)CountryValue[0]).CountryCD;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbContact);
                            //tbContact.Focus();
                        }
                        else
                        {
                            cvCountry.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCateringCountry);
                            //tbCateringCountry.Focus();
                            ReturnValue = false;
                            IsValidateCustom = false;
                        }
                    }
                }
                return ReturnValue;
            }
        }
        /// <summary>
        /// Method to check unique Metro Code 
        /// </summary>
        /// <returns></returns>
        private bool CheckAllReadyMetroExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                if ((tbMetroCD.Text != null) && (tbMetroCD.Text != string.Empty))
                {
                    //To check for unique metro code
                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var MetroValue = FPKMasterService.GetMetroCityList().EntityList.Where(x => x.MetroCD.ToString().ToUpper().Trim().Equals(tbMetroCD.Text.ToUpper().Trim())).ToList();
                        if (MetroValue.Count() > 0 && MetroValue != null)
                        {
                            hdnMetroId.Value = ((FlightPakMasterService.Metro)MetroValue[0]).MetroID.ToString().Trim();
                            tbMetroCD.Text = ((FlightPakMasterService.Metro)MetroValue[0]).MetroCD;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCateringCountry);
                            //tbCateringCountry.Focus();
                        }
                        else
                        {
                            cvMetro.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbMetroCD);
                            // tbMetroCD.Focus();
                            ReturnValue = true;
                            IsValidateCustom = false;
                        }
                    }
                }
                return ReturnValue;
            }
        }

        /// <summary>
        /// Method to check unique Country  Code 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ExchangeRate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckExchangeRateExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }


        private void CheckExchangeRate()
        {
            using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var CountryValue = CountryService.GetExchangeRate().EntityList.Where(x => x.ExchangeRateID.ToString().Trim().ToUpper() == (hdnExchangeRateID.Value.ToString().Trim().ToUpper())).ToList();
                if (CountryValue.Count() > 0 && CountryValue != null)
                {
                    tbExchangeRate.Text = ((FlightPakMasterService.ExchangeRate)CountryValue[0]).ExchRateCD.ToString().Trim();
                    //RadAjaxManager.GetCurrent(Page).FocusControl(tbSaturdayWorkHours);
                    // tbPostal.Focus();
                }
                else
                {
                    tbExchangeRate.Text = "";
                }
            }
        }
        /// <summary>
        ///  Method to check unique Country  Code 
        /// </summary>
        private void CheckExchangeRateExist()
        {
            if (!string.IsNullOrEmpty(tbExchangeRate.Text))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var CountryValue = CountryService.GetExchangeRate().EntityList.Where(x => x.ExchRateCD.Trim().ToUpper() == (tbExchangeRate.Text.Trim().ToUpper())).ToList();
                    if (CountryValue.Count() > 0 && CountryValue != null)
                    {
                        hdnExchangeRateID.Value = ((FlightPakMasterService.ExchangeRate)CountryValue[0]).ExchangeRateID.ToString().Trim();
                        tbExchangeRate.Text = ((FlightPakMasterService.ExchangeRate)CountryValue[0]).ExchRateCD;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbSaturdayWorkHours);
                        // tbPostal.Focus();
                    }
                    else
                    {
                        cvExchangeRate.IsValid = false;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbExchangeRate);
                        //tbCountry.Focus();
                        IsValidateCustom = false;
                    }
                }
            }
        }


        private Catering GetItemsForCopy()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.Catering CateringService = new FlightPakMasterService.Catering();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    string Code = string.Empty;
                    if (Session["SelectedCateringID"] != null)
                    {
                        Code = Convert.ToString(Session["SelectedCateringID"]).Trim();
                    }
                    CateringService.AirportID = Convert.ToInt64(Request.QueryString["AirportID"].ToString());
                    //CateringService.CateringCD = Convert.ToString(tbCaterCode.Text);
                    CateringService.IsChoice = chkChoice.Checked;
                    CateringService.CateringVendor = Convert.ToString(tbName.Text);
                    CateringService.PhoneNum = Convert.ToString(tbPhone.Text);
                    CateringService.FaxNum = Convert.ToString(tbFax.Text);
                    CateringService.ContactName = Convert.ToString(tbContact.Text);
                    if (hdnSaveFlag.Value == "Update")
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var CateringList = FPKMasterService.GetAllCateringByCateringID(Convert.ToInt64(Code)).EntityList.ToList();
                            if (CateringList.Count > 0)
                            {
                                GetAllCateringByCateringID CateringEntity = CateringList[0];
                                if (CateringEntity.UWAID != null)
                                    CateringService.UWAID = CateringEntity.UWAID;
                                if (CateringEntity.UWAMaintFlag != null)
                                {
                                    CateringService.UWAMaintFlag = CateringEntity.UWAMaintFlag;
                                }
                                if (CateringEntity.UpdateDT != null)
                                {
                                    CateringService.UpdateDT = CateringEntity.UpdateDT;
                                }
                                if (CateringEntity.RecordType != null)
                                {
                                    CateringService.RecordType = CateringEntity.RecordType;
                                }
                                if (CateringEntity.SourceID != null)
                                {
                                    CateringService.SourceID = CateringEntity.SourceID;
                                }
                                if (CateringEntity.ControlNum != null)
                                {
                                    CateringService.ControlNum = CateringEntity.ControlNum;
                                }
                                if (CateringEntity.UWAUpdates != null)
                                {
                                    CateringService.UWAUpdates = CateringEntity.UWAUpdates;
                                }
                                CateringService.CateringID = -1;
                            }
                        }
                    }
                    else
                    {
                        CateringService.UWAMaintFlag = false;
                        CateringService.CateringID = 0;
                        CateringService.UWAID = string.Empty;
                    }
                    if (tbNegotiatedPrice.Text != string.Empty)
                    {
                        CateringService.NegotiatedRate = Convert.ToDecimal(tbNegotiatedPrice.Text);
                    }
                    CateringService.IsInActive = chkInactive.Checked;
                    CateringService.TollFreePhoneNum = Convert.ToString(tbTollFreeNo.Text);
                    CateringService.WebSite = Convert.ToString(tbWebsite.Text);
                    CateringService.Remarks = Convert.ToString(tbRemarks.Text);
                    CateringService.IsDeleted = false;
                    CateringService.BusinessEmail = Convert.ToString(tbBusinessEmail.Text);
                    CateringService.CellPhoneNum = Convert.ToString(tbContactMobile.Text);
                    CateringService.ContactBusinessPhone = Convert.ToString(tbContactBusinessPhone.Text);
                    CateringService.ContactEmail = Convert.ToString(tbContactEmail.Text);
                    CateringService.Addr1 = Convert.ToString(tbAddr1.Text);
                    CateringService.Addr2 = Convert.ToString(tbAddr2.Text);
                    CateringService.Addr3 = Convert.ToString(tbAddr3.Text);
                    CateringService.City = Convert.ToString(tbCateringCity.Text);
                    if (!string.IsNullOrEmpty(hdnExchangeRateID.Value))
                    {
                        CateringService.ExchangeRateID = Convert.ToInt64(hdnExchangeRateID.Value);
                    }
                    else
                    {
                        CateringService.ExchangeRateID = null;
                    }
                    //
                    if (!string.IsNullOrEmpty(tbNegotiateTerm.Text.Trim()))
                    {
                        CateringService.NegotiatedTerms = tbNegotiateTerm.Text.Trim(); //NegotiatedTerms
                    }
                    else
                    {
                        CateringService.NegotiatedTerms = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbSundayWorkHours.Text.Trim()))
                    {
                        CateringService.SundayWorkHours = tbSundayWorkHours.Text.Trim(); //SundayWorkHours
                    }
                    else
                    {
                        CateringService.SundayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbMondayWorkHours.Text.Trim()))
                    {
                        CateringService.MondayWorkHours = tbMondayWorkHours.Text; // MondayWorkHours
                    }
                    else
                    {
                        CateringService.MondayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbTuesdayWorkHours.Text.Trim()))
                    {
                        CateringService.TuesdayWorkHours = tbTuesdayWorkHours.Text.Trim(); //TuesdayWorkHours
                    }
                    else
                    {
                        CateringService.TuesdayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbWednesdayWorkHours.Text))
                    {
                        CateringService.WednesdayWorkHours = tbWednesdayWorkHours.Text; // WednesdayWorkHours
                    }
                    else
                    {
                        CateringService.WednesdayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbThursdayWorkHours.Text))
                    {
                        CateringService.ThursdayWorkHours = tbThursdayWorkHours.Text.Trim(); // ThursdayWorkHours
                    }
                    else
                    {
                        CateringService.ThursdayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbFridayWorkHours.Text.Trim()))
                    {
                        CateringService.FridayWorkHours = tbFridayWorkHours.Text.Trim(); //FridayWorkHours
                    }
                    else
                    {
                        CateringService.FridayWorkHours = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(tbSaturdayWorkHours.Text))
                    {
                        CateringService.SaturdayWorkHours = tbSaturdayWorkHours.Text; // SaturdayWorkHours
                    }
                    else
                    {
                        CateringService.SaturdayWorkHours = string.Empty;
                    }

                    CateringService.StateProvince = Convert.ToString(tbStateProvince.Text);
                    if (!string.IsNullOrEmpty(hdnCountryId.Value))
                        CateringService.CountryID = Convert.ToInt64(hdnCountryId.Value);
                    if (!string.IsNullOrEmpty(hdnMetroId.Value))
                        CateringService.MetroID = Convert.ToInt64(hdnMetroId.Value);
                    return CateringService;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return CateringService;
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
            CheckIsWidget("PreInit");
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            if (IsPopUp)
                            {
                                //Set visible false to all controls.
                                //Container control visibility throwing some error
                                table1.Visible = false;
                                table2.Visible = false;
                                table3.Visible = false;
                                table4.Visible = false;
                                table5.Visible = false;
                                dgCateringCatalog.Visible = false;
                                if (IsViewOnly)
                                {
                                    dgCateringCatalog.Visible = true;
                                    table2.Visible = true;
                                    DefaultSelection(false);
                                }
                                if (!IsViewOnly)
                                {
                                    if (IsAdd)
                                    {
                                        (dgCateringCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                                    }
                                    else
                                    {
                                        (dgCateringCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                                    }
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Catering);
                }
            }
            
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgCateringCatalog.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgCateringCatalog.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
        private void CheckIsWidget(string LoadType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["IsWidget"] != null)
                {
                    if (Session["IsWidget"].ToString() == "1")
                    {
                        if (LoadType == "PreInit")
                        {
                            this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");
                        }
                    }
                }
            }
        }
        
        private bool SearchAndFilter(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.GetAllCateringByAirportID> lstCatering = new List<FlightPakMasterService.GetAllCateringByAirportID>();
                if (Session["CateringCD"] != null)
                {
                    lstCatering = (List<FlightPakMasterService.GetAllCateringByAirportID>)Session["CateringCD"];
                }
                if (lstCatering.Count != 0)
                {
                    if (chkDisplayInctive.Checked == false) { lstCatering = lstCatering.Where(x => x.IsInActive == false).ToList<GetAllCateringByAirportID>(); }
                    if (chkDisplayChoiceOnly.Checked == true) { lstCatering = lstCatering.Where(x => x.IsChoice == true).ToList<GetAllCateringByAirportID>(); }
                    dgCateringCatalog.DataSource = lstCatering.GroupBy(t => t.CateringVendor).Select(y => y.First());  
                    if (IsDataBind)
                    {
                        dgCateringCatalog.DataBind();
                    }
                }
                else
                {
                    chkDisplayChoiceOnly.Enabled = false;
                    chkDisplayInctive.Enabled = false;
                }
                ReadOnlyForm();
                return false;
            }
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgCateringCatalog.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            if (IsPopUp && Session["SelectedAirportForCateringID"] != null)
            {
                hdnAirportId.Value = Session["SelectedAirportForCateringID"].ToString();
            }

            var CateringValue = FPKMstService.GetAllCateringByAirportID(Convert.ToInt64(hdnAirportId.Value));
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, CateringValue);
            List<FlightPakMasterService.GetAllCateringByAirportID> filteredList = GetFilteredList(CateringValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.CateringID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgCateringCatalog.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgCateringCatalog.CurrentPageIndex = PageNumber;
            dgCateringCatalog.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfGetAllCateringByAirportID CateringValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedCateringID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                //This if else block and hdnSaveFlag.Value - "" are implemented because LastUpdTS is setting date but without time
                //when user adds a new Catering. It is not updating when a Catering is modified
                if (hdnSaveFlag.Value == "Save")
                {
                    PrimaryKeyValue = CateringValue.EntityList.OrderByDescending(x => x.CateringID).FirstOrDefault().CateringID;
                    Session["SelectedCateringID"] = PrimaryKeyValue;
                }
                else
                {
                    if(Session["SelectedCateringID"] != null)
                      PrimaryKeyValue = Convert.ToInt64(Session["SelectedCateringID"].ToString());
                }

                hdnSaveFlag.Value = "";
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.GetAllCateringByAirportID> GetFilteredList(ReturnValueOfGetAllCateringByAirportID CateringValue)
        {
            List<FlightPakMasterService.GetAllCateringByAirportID> filteredList = new List<FlightPakMasterService.GetAllCateringByAirportID>();

            if (CateringValue.ReturnFlag)
            {
                filteredList = CateringValue.EntityList;
            }

            if (!IsPopUp)
            {
                if (chkDisplayInctive.Checked)
                {
                    filteredList = filteredList.Where(x => x.IsDeleted.ToString().ToUpper().Trim().Equals("FALSE")).ToList();
                }
                else
                {
                    filteredList = filteredList.Where(x => x.IsDeleted.ToString().ToUpper().Trim().Equals("FALSE") && (x.IsInActive.ToString().ToUpper().Trim() == "FALSE")).ToList();
                }
                if (chkDisplayChoiceOnly.Checked) { filteredList = filteredList.Where(x => x.IsChoice.ToString().ToUpper().Trim().Equals("TRUE")).ToList(); }
            }

            return filteredList;
        }
        #endregion
    }
}
