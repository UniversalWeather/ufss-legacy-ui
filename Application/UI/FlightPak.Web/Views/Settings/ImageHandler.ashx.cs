﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Text.RegularExpressions;

namespace FlightPak.Web.Views.Settings
{
    /// <summary>
    /// Summary description for ImageHandler
    /// </summary>
    public class ImageHandler : IHttpHandler, IRequiresSessionState
    {
        String SupportedExtnPatterns = "^(.TXT)$";
        string strExtn="";
        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");

            HttpResponse r = context.Response;
            if (context.Request.QueryString["filename"] != null)
            {
                r.Clear();
                int iIndex = context.Request.QueryString["filename"].IndexOf('.');
                strExtn = context.Request.QueryString["filename"].Substring(iIndex, context.Request.QueryString["filename"].Length - iIndex);                
                Match regMatch = Regex.Match(strExtn, SupportedExtnPatterns, RegexOptions.IgnoreCase);
                if (regMatch.Success)
                {
                    r.ContentType = "text/plain";
                }
                else
                {
                    r.ContentType = "application/octet-stream";
                }
                //Fix-Flaw Id 215- Build-Nov 22, 2014-
                r.AddHeader("Content-Disposition", "attachment;filename=\"" + Microsoft.Security.Application.Encoder.HtmlEncode(context.Request.QueryString["filename"]) + "\"");                
            }
            else
            {
                r.ContentType = "image/jpeg";
            }

            if (context.Request.QueryString["CqQuoteReportBase64"] == null && context.Request.QueryString["CqInvoiceReportBase64"] == null && context.Session["Base64"] != null && context.Request.QueryString["CQBase64"] == null && context.Request.QueryString["CQQuoteOnFileBase64"] == null)
            {
                r.Clear();
                if (r.ContentType == "image/jpeg")
                {
                    r.BinaryWrite(GetFormatedString((byte[])context.Session["Base64"]));
                }
                else
                {
                    r.OutputStream.Write(GetFormatedString((byte[])context.Session["Base64"]), 0, ((byte[])context.Session["Base64"]).Length - 1);
                }                            
            }
            else
            {
                if ((context.Session["Base64"] == null) && (context.Session["Base641"] != null))
                {
                    r.Clear();
                    if (r.ContentType == "image/jpeg")
                    {
                        r.BinaryWrite(GetFormatedString((byte[])context.Session["Base641"]));                     
                    }
                    else
                    {
                        r.OutputStream.Write(GetFormatedString((byte[])context.Session["Base641"]), 0, ((byte[])context.Session["Base641"]).Length - 1);
                    }                    
                }                               
                if ((context.Session["CQBase64"] == null) && (context.Session["CQBase641"] != null))
                {
                    r.Clear();
                    if (r.ContentType == "image/jpeg")
                    {
                        r.BinaryWrite(GetFormatedString((byte[])context.Session["CQBase641"]));
                    }
                    else
                    {
                        r.OutputStream.Write(GetFormatedString((byte[])context.Session["CQBase641"]), 0, ((byte[])context.Session["CQBase641"]).Length - 1);
                    }                    
                }
                if (context.Session["CQBase64"] != null)
                {
                    r.Clear();
                    if (r.ContentType == "image/jpeg")
                    {
                        r.BinaryWrite(GetFormatedString((byte[])context.Session["CQBase64"]));
                    }
                    else
                    {
                        r.OutputStream.Write(GetFormatedString((byte[])context.Session["CQBase64"]), 0, ((byte[])context.Session["CQBase64"]).Length - 1);
                    }                                        
                }
                if (context.Session["CQQuoteOnFileBase64"] != null)
                {
                    r.Clear();
                    if (r.ContentType == "image/jpeg")
                    {
                        r.BinaryWrite(GetFormatedString((byte[])context.Session["CQQuoteOnFileBase64"]));
                    }
                    else
                    {
                        r.OutputStream.Write(GetFormatedString((byte[])context.Session["CQQuoteOnFileBase64"]), 0, ((byte[])context.Session["CQQuoteOnFileBase64"]).Length - 1);
                    }                                       
                }
                if (context.Request.QueryString["CqQuoteReportBase64"] != null && context.Session["CqQuoteReportBase64"] != null)
                {
                    r.Clear();
                    if (r.ContentType == "image/jpeg")
                    {                       
                        r.BinaryWrite(GetFormatedString((byte[])context.Session["CqQuoteReportBase64"]));
                    }
                    else
                    {                       
                        r.OutputStream.Write(GetFormatedString((byte[])context.Session["CqQuoteReportBase64"]), 0, ((byte[])context.Session["CqQuoteReportBase64"]).Length - 1);
                    }                    
                }
                if (context.Request.QueryString["CqInvoiceReportBase64"] != null && context.Session["CqInvoiceReportBase64"] != null)
                {
                    r.Clear();
                    if (r.ContentType == "image/jpeg")
                    {
                        r.BinaryWrite(GetFormatedString((byte[])context.Session["CqInvoiceReportBase64"]));
                    }
                    else
                    {
                        r.OutputStream.Write(GetFormatedString((byte[])context.Session["CqInvoiceReportBase64"]), 0, ((byte[])context.Session["CqInvoiceReportBase64"]).Length - 1);
                    }                    
                }
            }
            r.Flush();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public static byte[] GetFormatedString(byte[] stream)
        {
            string str = Microsoft.Security.Application.Encoder.HtmlEncode(Convert.ToBase64String(stream));
            return Convert.FromBase64String(str);
        }
    }
}