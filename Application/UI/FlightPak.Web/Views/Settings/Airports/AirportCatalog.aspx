﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="AirportCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Settings.Airports.AirportCatalog"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true" ValidateRequest="false" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<%@ Register TagPrefix="uc" TagName="DatePicker" Src="~/UserControls/UCDate.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript" language="javascript">
        //this function is used to expand the panel on click
        function onClientPanelClick(strPanelToExpand) {
            var panelBar1 = $find("<%= pnlbarnotes.ClientID %>");
            var panelBar2 = $find("<%= pnlbarMaintainance.ClientID %>");
            var panelBar3 = $find("<%= pnlbarAlerts.ClientID %>");
            var panelBar4 = $find("<%= pnlbarrunways.ClientID %>");
            panelBar1.get_items().getItem(0).set_expanded(false);
            panelBar2.get_items().getItem(0).set_expanded(false);
            panelBar3.get_items().getItem(0).set_expanded(false);
            panelBar4.get_items().getItem(0).set_expanded(false);
            if (strPanelToExpand == "Runways") {
                panelBar4.get_items().getItem(0).set_expanded(true);
            }
            else if (strPanelToExpand == "Alerts") {
                panelBar3.get_items().getItem(0).set_expanded(true);
            }
            else if (strPanelToExpand == "Notes") {
                panelBar1.get_items().getItem(0).set_expanded(true);
            }
            return false;
        } 

        //this function is used to display the value of selected Payment code from popup
        function OnClientCloseExchangeRatePopup(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg !== null) {
                if (arg) {
                    document.getElementById("<%=tbExchangeRate.ClientID%>").value = arg.ExchangeRateCD;
                    document.getElementById("<%=cvExchangeRate.ClientID%>").innerHTML = "";
                }
                else {
                    document.getElementById("<%=tbExchangeRate.ClientID%>").value = "";
                    document.getElementById("<%=cvExchangeRate.ClientID%>").innerHTML = "";
                    combo.clearSelection();
                }
            }
        }

        //this function is used to navigate to pop up screen's with the selected code
        function openWin(radWin) {
            var url = '';
            if (radWin == "RadCountryMasterPopup") {
                url = '../Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('<%=tbCountry.ClientID%>').value;
            }
            else if (radWin == "RadDSTRegionPopup") {
                url = '../Airports/DSTRegionPopup.aspx?DSTRegionCD=' + document.getElementById('<%=tbRegion.ClientID%>').value;
            }
            else if (radWin == "RadMetroCityPopup") {
                url = '../Airports/MetroCityPopup.aspx?MetroCD=' + document.getElementById('<%=tbMetroCD.ClientID%>').value;
            }
            else if (radWin == "RadExportData") {
                url = "../../Reports/ExportReportInformation.aspx?Report=RptDBAirportExport&icaoid=" + document.getElementById('<%=tbICAO.ClientID%>').value + "&UserCD=UC";
            }
            var oManager = $find("<%= RadWindowManager1.ClientID %>");
            var oWnd = oManager.open(url, radWin);
        }
        // To Delete record in a database,Confirming that it is not UWA record
        function deleteRecord() {
            var masterTable = $find('<%= dgAirportCatalog.ClientID %>').get_masterTableView();
            var msg = 'Are you sure you want to delete this record?';
            
            var oManager = $find("<%= RadWindowManager1.ClientID %>");
            if (masterTable.get_selectedItems().length > 0) {
                var cell1 = masterTable.getCellByColumnUniqueName(masterTable.get_selectedItems()[0], "chkUWAID");
                if (cell1.innerHTML == "UWA") {
                    oManager.radalert('You Cant Delete UWA Record', 330, 100, "Delete", "");
                    return false;
                }
                else {
                    oManager.radconfirm(msg, callBackFn, 330, 100, '', 'Delete');
                    return false;
                }
            }
            else {
                //If no records are selected 
                oManager.radalert('Please select a record from the above table.', 330, 100, "Delete", "");
                return false;
            }
        }
        function callBackFn(confirmed) {
            if (confirmed) {
                var grid = $find('<%= dgAirportCatalog.ClientID %>');
                grid.get_masterTableView().fireCommand("DeleteSelected");
            }
        }
        //this function is called while adding an UWA record as customer record
        function confirmCallBackFn(arg) {
            if (arg == false) {
                document.getElementById('<%=btnCancelTop.ClientID%>').click();
            }
            else {
                return false;
            }
        }
        //this function is used to replace default value when textbox is empty
        function validateEmptyAircraftTextbox(ctrlID, e) {
            if (ctrlID.value == "") {
                ctrlID.value = "0.00";
            }
        }
        //this function is used to replace default value when textbox is empty
        function validateEmptyLandingBiasTextbox(ctrlID, e) {
            if (ctrlID.value == "") {
                ctrlID.value = "0.0";
            }
        }
        //this function is used to replace default value when textbox is empty
        function validateEmptyLatitudeTextbox(ctrlID, e) {
            if (ctrlID.value == "") {
                ctrlID.value = "0";
            }
        }
        
        function ShowReports(radWin, ReportFormat, ReportName) {
            document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
            document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
            if (ReportFormat == "EXPORT") {
                url = "../../Reports/ExportReportInformation.aspx";
                
                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, 'RadExportData');
                return true;
            }
            else {
                return true;
            }
        }
        function OnClientCloseExportReport(oWnd, args) {
            document.getElementById("<%=btnShowReports.ClientID%>").click();
        }
    </script>
    <script runat="server">
        // setting the properties for controls for navigation
        public string IcaoID
        {
            get
            {
                return tbICAO.Text;
            }
        }
        public string CityName
        {
            get
            {
                return tbCity.Text;
            }
        }
        public string StateName
        {
            get
            {
                return tbStateProvince.Text;
            }
        }
        public string CountryName
        {
            get
            {
                return tbCountry.Text;
            }
        }
        public string AirportName
        {
            get
            {
                return tbAirport.Text;
            }
        }
        //this event is used to assign  values of controls to the navigating url
        void lbtnTransportQueryString_Click(object sender, EventArgs e)
        {
            Session["SelectedTransportID"] = null;
            RedirectFromAirport("TransportCatalog.aspx");
        }
        //this event is used to assign  values of controls to the navigating url
        void lbtnCateringQueryString_Click(object sender, EventArgs e)
        {
            Session["SelectedCateringID"] = null;
            RedirectFromAirport("CateringCatalog.aspx");
        }
        //this event is used to assign  values of controls to the navigating url
        void lbtnHotelQueryString_Click(object sender, EventArgs e)
        {
            Session["SelectedHotelID"] = null;
            RedirectFromAirport("HotelCatalog.aspx");
        }
        //this event is used to assign  values of controls to the navigating url
        void lbtnFboQueryString_Click(object sender, EventArgs e)
        {
            Session["SelectedFBOID"] = null;
            RedirectFromAirport("FBOCatalog.aspx");
        }
        //this event is used to assign  values of controls to the navigating url
        void lbtnFuelLocatorQueryString_Click(object sender, EventArgs e)
        {            
            Session["SelectedFuelLocatorID"] = null;
            RedirectFromAirport("FuelLocator.aspx");
        }       
        
        void RedirectFromAirport(string strRedirectPage)
        {
            string strIcao = Microsoft.Security.Application.Encoder.UrlEncode(tbICAO.Text.Trim());
            string strCity = Microsoft.Security.Application.Encoder.UrlEncode(tbCity.Text.Trim());
            string strStateProvince = Microsoft.Security.Application.Encoder.UrlEncode(tbStateProvince.Text.Trim());
            string strCountry = Microsoft.Security.Application.Encoder.UrlEncode(tbCountry.Text.Trim());
            string strAirportName = Microsoft.Security.Application.Encoder.UrlEncode(tbAirport.Text.Trim());
            string strAirportId = Microsoft.Security.Application.Encoder.UrlEncode(hdnAirportId.Value);
            RedirectToPage(string.Format("/views/Settings/Logistics/{0}?Screen=Airport&IcaoID={1}&CityName={2}&StateName={3}&CountryName={4}&AirportName={5}&AirportID={6}", strRedirectPage, strIcao, strCity, strStateProvince, strCountry, strAirportName, strAirportId));
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgAirportCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="divInactive" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancelTop">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgAirportCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="divInactive" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSaveTop">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgAirportCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="divInactive" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgAirportCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="divInactive" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgAirportCatalog">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgAirportCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="divInactive" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="chkDisplayInctive">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgAirportCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="divInactive" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbRunway">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgAirportCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="divInactive" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbICAO">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="tbIATA">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbIATA" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbRegion">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgAirportCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="divInactive" />
                    <telerik:AjaxUpdatedControl ControlID="tbRegion" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbUTC" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="cvRegion" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="ucStartDate" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="ucFinishedDate" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="rmtbStartTime" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="rmtbFinishTime" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="chkSummerTime" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbCountry">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbCountry" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbCountryDesc" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbApMgr" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="cvCountry" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbMetroCD">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbMetroCD" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbNotes" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="cvMetro" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbExchangeRate">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbExchangeRate" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbNotes" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="cvExchangeRate" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDstTime">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgAirportCatalog" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="divInactive" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadCountryMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="onClientCloseCountryPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CountryMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadDSTRegionPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="onClientCloseRegionPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/DSTRegionPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadMetroCityPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="onClientCloseMetroCityPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/MetroCityPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadExchangeMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseExchangeRatePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ExchangeRateMasterpopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadExportData" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Export Report Information" KeepInScreenBounds="true" Height="540px" Width="880px"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
 
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            // this function is used to the refresh the currency grid
            function refreshGrid(arg) {
                $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest('Rebind');
            }
            ///To validate hours and minute
            function validateHourAndMinute(oSrc, args) {

                var array = args.Value.split(":");
                if (array[0] > 23 && array[1] > 59) {
                    args.IsValid = false;
                    oSrc.innerText = "Hour entry must be 0-23; Minute entry must be 0-59";
                }
                else if ((array[0] > 23)) {
                    args.IsValid = false;
                    oSrc.innerText = "Hour entry must be 0-23";
                }
                else {
                    if (array[1] > 59) {
                        args.IsValid = false;
                        oSrc.innerText = " Minute entry must be 0-59";
                    }
                }
            }
            // this function is used to close the pop up
            function confirmClose(WinName) {
                var oManager = $find("<%= RadWindowManager1.ClientID %>"); //GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    oManager.radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }
            //this function is used to display the value of selected country code from popup
            function onClientCloseCountryPopup(oWnd, args) {
                var combo = $find("<%= tbCountry.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCountry.ClientID%>").value = arg.CountryCD;
                        document.getElementById("<%=tbCountryDesc.ClientID%>").value = arg.CountryName;
                        document.getElementById("<%=hdnCountryId.ClientID%>").value = arg.CountryID;
                        document.getElementById("<%=cvCountry.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbCountry.ClientID%>").value = "";
                        document.getElementById("<%=tbCountryDesc.ClientID%>").value = "";
                        document.getElementById("<%=hdnCountryId.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            //this function is used to display the value of selected Region code from popup
            function onClientCloseRegionPopup(oWnd, args) {
                var combo = $find("<%= tbRegion.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbRegion.ClientID%>").value = arg.DSTRegionCD;
                        document.getElementById("<%=hdnDstID.ClientID%>").value = arg.DSTRegionID;
                        document.getElementById("<%=cvRegion.ClientID%>").innerHTML = "";

                        //                        if ((arg.StartDT != "&nbsp;") && (arg.StartDT != "undefined")) {
                        //                            document.getElementById("<%=hdnDstStartDate.ClientID%>").value = arg.StartDT;
                        //                            if (arg.StartDT.indexOf("AM") > -1) {
                        //                                if (arg.StartDT.substr(11, 2) == "12") {
                        //                                    document.getElementById("<%=hdnStartTime.ClientID%>").value = "00:" + arg.StartDT.substr(14, 2);
                        //                                }
                        //                                else {
                        //                                    document.getElementById("<%=hdnStartTime.ClientID%>").value = arg.StartDT.substr(11, 2) + ":" + arg.StartDT.substr(14, 2);
                        //                                }
                        //                            }
                        //                            else if (arg.StartDT.indexOf("PM") > -1) {
                        //                                if (arg.StartDT.substr(11, 2) == "12") {
                        //                                    document.getElementById("<%=hdnStartTime.ClientID%>").value = "12:" + arg.StartDT.substr(14, 2);
                        //                                }
                        //                                else {
                        //                                    document.getElementById("<%=hdnStartTime.ClientID%>").value = parseInt(arg.StartDT.substr(11, 2)) + 12 + ":" + arg.StartDT.substr(14, 2);
                        //                                }
                        //                            }
                        //                            else {
                        //                                document.getElementById("<%=hdnStartTime.ClientID%>").value = arg.StartDT.substr(11, 2) + ":" + arg.StartDT.substr(14, 2);
                        //                            }
                        //                        }
                        //                        if ((arg.EndDT != "&nbsp;") && (arg.EndDT != "undefined")) {
                        //                            document.getElementById("<%=hdnDstEndDate.ClientID%>").value = arg.EndDT;

                        //                            if (arg.EndDT.indexOf("AM") > -1) {
                        //                                if (arg.EndDT.substr(11, 2) == "12") {
                        //                                    document.getElementById("<%=hdnEndTime.ClientID%>").value = "00:" + arg.EndDT.substr(14, 2);
                        //                                }
                        //                                else {
                        //                                    document.getElementById("<%=hdnEndTime.ClientID%>").value = arg.EndDT.substr(11, 2) + ":" + arg.EndDT.substr(14, 2);
                        //                                }
                        //                            }
                        //                            else if (arg.EndDT.indexOf("PM") > -1) {
                        //                                if (arg.EndDT.substr(11, 2) == "12") {
                        //                                    document.getElementById("<%=hdnEndTime.ClientID%>").value = "12:" + arg.EndDT.substr(14, 2);
                        //                                }
                        //                                else {
                        //                                    document.getElementById("<%=hdnEndTime.ClientID%>").value = parseInt(arg.EndDT.substr(11, 2)) + 12 + ":" + arg.EndDT.substr(14, 2);
                        //                                }
                        //                            }
                        //                            else {
                        //                                document.getElementById("<%=hdnEndTime.ClientID%>").value = arg.EndDT.substr(11, 2) + ":" + arg.EndDT.substr(14, 2);
                        //                            }
                        //                        }

                        document.getElementById('<% =btnDstTime.ClientID %>').click();
                        if ((arg.OffSet != "&nbsp;") && (arg.OffSet != "undefined")) {
                            document.getElementById("<%=tbUTC.ClientID%>").value = arg.OffSet;
                        }
                    }
                    else {
                        document.getElementById("<%=tbRegion.ClientID%>").value = "";
                        document.getElementById("<%=hdnDstID.ClientID%>").value = "";
                        document.getElementById("<%=hdnDstStartDate.ClientID%>").value = null;
                        document.getElementById("<%=hdnDstEndDate.ClientID%>").value = null;
                        document.getElementById("<%=hdnStartTime.ClientID%>").value = null;
                        document.getElementById("<%=hdnEndTime.ClientID%>").value = null;
                        document.getElementById("<%=tbUTC.ClientID%>").value = null;
                        combo.clearSelection();
                    }
                }
            }
            //this function is used to display the value of selected metro code from popup
            function onClientCloseMetroCityPopup(oWnd, args) {
                var combo = $find("<%= tbMetroCD.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbMetroCD.ClientID%>").value = arg.MetroCD;
                        document.getElementById("<%=hdnMetroId.ClientID%>").value = arg.MetroID;
                        document.getElementById("<%=cvMetro.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbMetroCD.ClientID%>").value = "";
                        document.getElementById("<%=hdnMetroId.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            //this function is used to get dimension
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            //this function is used to get radwindow frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) {
                    oWindow = window.radWindow;
                }
                else if (window.frameElement.radWindow) {
                    oWindow = window.frameElement.radWindow;
                }
                return oWindow;
            }

            function openReport(radWin, param) {
                var url = '';
                if (radWin == "Airport") {
                    url = '../../../../Views/Reports/ExportReportInformation.aspx?Report=RptDBAirport&P1=' + document.getElementById('<%=tbICAO.ClientID%>').value;
                }

                var oManager = $find("<%= RadWindowManager1.ClientID %>");
                var oWnd = oManager.open(url, "RadExportData");
            }


            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });

            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
            return false;
        }
        function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <table style="width: 100%;" cellpadding="0" cellspacing="0" id="table1" runat="server">
        <tr style="display: none;">
            <td>
                <uc:DatePicker ID="ucDatePicker" runat="server"></uc:DatePicker>
            </td>
        </tr>
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Airport</span> <span class="tab-nav-icons">
                        <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:ShowReports('','PDF','RptDBAirport');"
                            OnClick="btnShowReports_OnClick" title="Preview Report" class="search-icon"></asp:LinkButton>
                        <asp:LinkButton ID="lbtnSaveReports" runat="server" title="Export Report" OnClientClick="openReport('Airport');return false;"
                            class="save-icon"></asp:LinkButton>
                        <asp:Button ID="btnShowReports" runat="server" OnClick="btnShowReports_OnClick" CssClass="button-disable"
                            Style="display: none;" />
                        <a href="../../Help/ViewHelp.aspx?Screen=AirportHelp" target="_blank" title="Help"
                            class="help-icon"></a>
                        <asp:HiddenField ID="hdnReportName" runat="server" />
                        <asp:HiddenField ID="hdnReportFormat" runat="server" />
                        <asp:HiddenField ID="hdnReportParameters" runat="server" />
                    </span>
                </div>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" class="head-sub-menu" id="table2" runat="server">
        <tr>
            <td>
                <div class="tags_select">
                    <asp:LinkButton ID="lbtnFbo" Text="FBO" runat="server" OnClick="lbtnFboQueryString_Click"></asp:LinkButton><asp:LinkButton
                        ID="lbtnHotel" Text="Hotel" runat="server" OnClick="lbtnHotelQueryString_Click"></asp:LinkButton>
                    <asp:LinkButton ID="lbtnTransport" Text="Transportation" runat="server" OnClick="lbtnTransportQueryString_Click"></asp:LinkButton>
                    <asp:LinkButton ID="lbtnCatering" Text="Catering" runat="server" OnClick="lbtnCateringQueryString_Click"></asp:LinkButton>
                    <asp:LinkButton ID="lbtnLocator" Text="Locator" runat="server" OnClick="lbtnFuelLocatorQueryString_Click"
                        Visible="false" Enabled="false"></asp:LinkButton>
                </div>
            </td>
        </tr>
    </table>
    <div id="divInactive" runat="server">
        <table cellpadding="0" cellspacing="0" class="head-sub-menu">
            <tr>
                <td align="left" class="tdLabel150">
                    <div class="status-list">
                        <span>
                            <asp:CheckBox ID="chkDisplayInctive" runat="server" Text="Display Inactive" OnCheckedChanged="chkDisplayInctive_CheckedChanged"
                                AutoPostBack="true" />
                        </span>
                    </div>
                </td>
                <%--<td>
                    <div class="status-list">
                        <span>Display Only Country :</span> <span>
                            <asp:TextBox ID="tbCountryOnly" CssClass="text60" runat="server" OnTextChanged="tbCountryOnly_TextChanged"
                                AutoPostBack="true" />
                            <asp:Button runat="server" ID="btnCountryOnly" CssClass="browse-button" OnClientClick="javascript:openWin('RadCountryMasterPopup');return false;" />
                        </span>
                    </div>
                </td>--%>
                <td class="tdLabel110">Minimum Runway:
                </td>
                <td>
                     <asp:TextBox ID="tbRunway" CssClass="text60" runat="server" onKeyPress="return fnAllowNumeric(this, event)"
                        OnTextChanged="tbRunway_TextChanged" AutoPostBack="true" pattern="[0-9]*" />                    
                </td>
            </tr>
            <%--<tr>
                <td>
                </td>
                <td>
                    <asp:CustomValidator ID="cvCountryOnly" runat="server" ControlToValidate="tbCountryOnly"
                        ErrorMessage="Invalid Country Code." Display="Dynamic" CssClass="alert-text"
                        SetFocusOnError="true"></asp:CustomValidator>
                </td>
                <td>
                </td>
            </tr>--%>
        </table>
    </div>
    <telerik:RadGrid ID="dgAirportCatalog" CssClass="airport-catalog-grid" runat="server" AllowMultiRowSelection="true" AllowSorting="true" OnNeedDataSource="dgAirportCatalog_BindData"
        OnItemCreated="dgAirportCatalog_ItemCreated" Visible="true" OnItemCommand="dgAirportCatalog_ItemCommand"
        OnItemDataBound="dgAirportCatalog_ItemDataBound" OnUpdateCommand="dgAirportCatalog_UpdateCommand"
        OnInsertCommand="dgAirportCatalog_InsertCommand" OnDeleteCommand="dgAirportCatalog_DeleteCommand"
        OnSelectedIndexChanged="dgAirportCatalog_SelectedIndexChanged" onRowDataBound="dgAirportCatalog_RowDataBound"
        Height="358px" AutoGenerateColumns="false" PageSize="10" AllowPaging="true" AllowFilteringByColumn="False"
        OnPageIndexChanged="dgAirportCatalog_PageIndexChanged" OnPreRender="dgAirportCatalog_PreRender"
        PagerStyle-AlwaysVisible="true" Skin="Office2010Silver" AllowCustomPaging="true">
        <MasterTableView DataKeyNames="AirportID,CustomerID,IcaoID,CityName,StateName,CountryID,CountryCD,AirportName,IsInActive,LastUpdUID,LastUpdTS,IsWorldClock,UWAID,Iata,MaxRunway,Filter,CountryName"
            CommandItemDisplay="Bottom" ClientDataKeyNames="IsWorldClock,UWAID">
            <Columns>
                <telerik:GridBoundColumn DataField="IcaoID" HeaderText="ICAO" AutoPostBackOnFilter="false"
                    HeaderStyle-Width="60px" FilterControlWidth="40px" ShowFilterIcon="false"
                    FilterDelay="1500" CurrentFilterFunction="StartsWith">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Iata" HeaderText="IATA" AutoPostBackOnFilter="false" FilterDelay="1500"
                    HeaderStyle-Width="60px" FilterControlWidth="40px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CityName" HeaderText="City" AutoPostBackOnFilter="false" FilterDelay="1500"
                    HeaderStyle-Width="140px" FilterControlWidth="120px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="StateName" HeaderText="State" AutoPostBackOnFilter="false" FilterDelay="1500"
                    HeaderStyle-Width="140px" FilterControlWidth="120px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CountryCD" HeaderText="Country Code" AutoPostBackOnFilter="false"
                    HeaderStyle-Width="60px" FilterControlWidth="40px" ShowFilterIcon="false"
                    FilterDelay="1500" CurrentFilterFunction="StartsWith">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CountryName" HeaderText="Country" AutoPostBackOnFilter="false" FilterDelay="1500"
                    HeaderStyle-Width="140px" FilterControlWidth="120px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="AirportName" HeaderText="Airport" AutoPostBackOnFilter="false" FilterDelay="1500"
                    HeaderStyle-Width="140px" FilterControlWidth="120px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith">
                </telerik:GridBoundColumn>
                <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" ShowFilterIcon="false"
                    HeaderStyle-Width="60px" AllowFiltering="false"
                    AutoPostBackOnFilter="true">
                </telerik:GridCheckBoxColumn>
                <telerik:GridBoundColumn HeaderText="Runway" DataField="MaxRunway" AllowFiltering="false"
                    HeaderStyle-Width="60px" ShowFilterIcon="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="OffsetToGMT" HeaderText="UTC +/-" AutoPostBackOnFilter="false" FilterDelay="1500"
                    HeaderStyle-Width="50px" FilterControlWidth="30px" ShowFilterIcon="false">
                </telerik:GridBoundColumn>
                <telerik:GridCheckBoxColumn DataField="IsEntryPort" HeaderText="Port of Entry" ShowFilterIcon="false"
                    HeaderStyle-Width="50px" FilterControlWidth="30px" AllowFiltering="false"
                    AutoPostBackOnFilter="true">
                </telerik:GridCheckBoxColumn>
                <telerik:GridBoundColumn HeaderText="Filter" DataField="Filter" UniqueName="chkUWAID" FilterDelay="1500"
                    HeaderStyle-Width="80px" FilterControlWidth="60px"
                    AutoPostBackOnFilter="false" ShowFilterIcon="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="AirportID" HeaderText="AirportID"
                    Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CustomerID" HeaderText="CustomerID"
                    Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="LastUpdUID" HeaderText="LastUpdUID"
                    Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="LastUpdTS" HeaderText="LastUpdTS"
                    Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="UWAID" HeaderText="UWAID"
                    Display="false">
                </telerik:GridBoundColumn>
            </Columns>
            <CommandItemTemplate>
                <div class="grid_icon">
                    <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                        CssClass="add-icon-grid" Visible='<%# IsAuthorized(Permission.Database.AddAirport)%>'></asp:LinkButton>
                    <asp:LinkButton ID="lbtnInitEdit" runat="server" ToolTip="Edit" CommandName="Edit"
                        CssClass="edit-icon-grid" Visible='<%# IsAuthorized(Permission.Database.EditAirport)%>'
                        OnClientClick="javascript:return ProcessUpdate();"></asp:LinkButton>
                    <asp:LinkButton ID="lbtnInitDelete" OnClientClick="javascript:return deleteRecord();"
                        runat="server" CommandName="DeleteSelected" CssClass="delete-icon-grid" ToolTip="Delete"
                        Visible='<%# IsAuthorized(Permission.Database.DeleteAirport)%>'></asp:LinkButton>
                </div>
                <div>
                    <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                </div>
            </CommandItemTemplate>
        </MasterTableView>
        <ClientSettings EnablePostBackOnRowClick="true">
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        <GroupingSettings CaseSensitive="false" />
        <PagerStyle Font-Size="8px" PageSizeLabelText="Size" CssClass="gridPager"  />
    </telerik:RadGrid>
    <div id="divExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.
                        </div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field
                        </div>
                    </td>
                </tr>
            </table>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="right">
                        <table cellpadding="0" cellspacing="0" class="tblButtonArea-nw">
                            <tr>
                                <td>
                                    <asp:Label ID="lbScript" runat="server" Text="" Visible="false"></asp:Label>
                                    <asp:Button ID="btnRunways" runat="server" CssClass="ui_nav" Text="Runways" OnClientClick="javascript:onClientPanelClick('Runways');return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnAlerts" runat="server" CssClass="ui_nav" Text="Alerts" OnClientClick="javascript:onClientPanelClick('Alerts');return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnNotes" runat="server" CssClass="ui_nav" Text="Notes" OnClientClick="javascript:onClientPanelClick('Notes');return false;" />
                                </td>
                                <td>
                                    <asp:Button ID="btnSaveTop" runat="server" CssClass="button" Text="Save" OnClick="SaveChanges_Click"
                                        ValidationGroup="Save" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancelTop" runat="server" CssClass="button" Text="Cancel" OnClick="Cancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlbarMaintainance" Width="100%" ExpandAnimation-Type="none"
                CollapseAnimation-Type="none" runat="server"  >
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Main Information" CssClass="PanelHeaderStyle">
                        <Items>
                            <telerik:RadPanelItem Value="Maintenance" runat="server">
                                <ContentTemplate>
                                    <table width="100%" class="box1">
                                        <tr>
                                            <td valign="top">
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel80">
                                                            <asp:CheckBox ID="chkInactive" runat="server" />
                                                            Inactive
                                                        </td>
                                                        <td class="tdLabel75">
                                                            <asp:CheckBox ID="chkAirport" runat="server" />
                                                            Airport
                                                        </td>
                                                        <td class="tdLabel80">
                                                            <asp:CheckBox ID="chkHeliport" runat="server" />
                                                            Heliport
                                                        </td>
                                                        <td class="tdLabel89">
                                                            <asp:CheckBox ID="chkSeaplane" runat="server" />
                                                            Seaplane
                                                        </td>
                                                        <td class="tdLabel70">
                                                            <asp:CheckBox ID="chkIsPublic" runat="server" />
                                                            Public
                                                        </td>
                                                        <td class="tdLabel75">
                                                            <asp:CheckBox ID="chkIsPrivate" runat="server" />
                                                            Private
                                                        </td>
                                                        <td class="tdLabel75">
                                                            <asp:CheckBox ID="chkIsMilitary" runat="server" />
                                                            Military
                                                        </td>
                                                        <td class="tdLabel65">
                                                            <asp:CheckBox ID="chkRural" runat="server" />
                                                            Rural
                                                        </td>
                                                        <td class="tdLabel85">
                                                            <asp:CheckBox ID="chkUSTax" runat="server" />
                                                            U.S. Tax
                                                        </td>
                                                        <td class="tdLabel102" nowrap="nowrap">
                                                            <asp:CheckBox ID="chkWorldClock" runat="server" />
                                                            World Clock
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tblspace_10">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="tdLabel100" valign="top">
                                                            <span class="mnd_text">ICAO</span>
                                                        </td>
                                                        <td valign="top" class="tdLabel310">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="tdLabel100">
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbICAO" runat="server" CssClass="text50" MaxLength="4" OnTextChanged="tbICAO_TextChanged"
                                                                                        AutoPostBack="true"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td class="tdLabel50">
                                                                        IATA
                                                                    </td>
                                                                    <td class="tdLabel150">
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td class="tdLabel60">
                                                                                    <asp:TextBox ID="tbIATA" runat="server" CssClass="text50" MaxLength="3"></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkEUETS" runat="server" />EU-ETS
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="rfvICAO" runat="server" ControlToValidate="tbICAO"
                                                                            ValidationGroup="Save" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">ICAO is Required.</asp:RequiredFieldValidator>
                                                                        <asp:CustomValidator ID="cvICAO" runat="server" ControlToValidate="tbICAO" ErrorMessage="Unique Code is Required ."
                                                                            Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel120" valign="top">
                                                            Airport Name
                                                        </td>
                                                        <td valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbAirport" runat="server" CssClass="text180" MaxLength="25"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td valign="top" class="tdLabel100">
                                                            City
                                                        <td class="tdLabel310">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCity" runat="server" CssClass="text150" MaxLength="25" onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel120" valign="top">
                                                            <asp:Label ID="lbStateProvince" runat="server" Text="State/Province"></asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbStateProvince" runat="server" CssClass="text180" MaxLength="25"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="tdLabel100" valign="top">
                                                            Country
                                                        </td>
                                                        <td class="tdLabel310" valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel110">
                                                                        <asp:TextBox ID="tbCountry" runat="server" CssClass="text50" MaxLength="3" AutoPostBack="true"
                                                                            OnTextChanged="tbCountry_TextChanged"></asp:TextBox>
                                                                        <asp:Button runat="server" ID="btnCountry" CssClass="browse-button" OnClientClick="javascript:openWin('RadCountryMasterPopup');return false;" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CustomValidator ID="cvCountry" runat="server" ControlToValidate="tbCountry"
                                                                            ErrorMessage="Invalid Country Code." Display="Dynamic" CssClass="alert-text"
                                                                            ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel120" valign="top">
                                                            Country Description
                                                        </td>
                                                        <td align="left">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <asp:TextBox ID="tbCountryDesc" runat="server" CssClass="text180" MaxLength="60"></asp:TextBox></tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="tdLabel100">
                                                            Airport Manager
                                                        </td>
                                                        <td class="tdLabel310">
                                                            <asp:TextBox ID="tbApMgr" runat="server" CssClass="text150" MaxLength="60"></asp:TextBox>
                                                        </td>
                                                        <td class="tdLabel120">
                                                            Manager Phone
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="tbMgrPhn" runat="server" CssClass="text180" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%">
                                                <fieldset>
                                                    <legend>Airport Information</legend>
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td colspan="4">
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td align="left" valign="top" class="tdLabel90">
                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td valign="top">
                                                                                                    <span class="mnd_text">Latitude</span>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td class="tdLabel140" valign="top">
                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td align="left" class="tdLabel45">
                                                                                                    <asp:TextBox ID="tbLatitudeDeg" runat="server" CssClass="text35" MaxLength="2" onBlur="return validateEmptyLatitudeTextbox(this, event)"
                                                                                                        onKeyPress="return fnAllowNumeric(this, event)" ValidationGroup="Save" pattern="[0-9]*"></asp:TextBox>
                                                                                                </td>
                                                                                                <td align="left" class="tdLabel45">
                                                                                                    <asp:TextBox ID="tbLatitudeMin" runat="server" CssClass="text35" MaxLength="4" onBlur="return validateEmptyLandingBiasTextbox(this, event)"
                                                                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'.')" ValidationGroup="Save" pattern="[0-9]*"></asp:TextBox>
                                                                                                </td>
                                                                                                <td align="left" class="tdLabel45">
                                                                                                    <asp:TextBox ID="tbLatitudeNS" runat="server" CssClass="text20" MaxLength="1" onBlur="return RemoveSpecialChars(this)"
                                                                                                        onKeyPress="return fnAllowAlpha(this, event)" ValidationGroup="Save"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="ltd_int">
                                                                                                    Deg.
                                                                                                </td>
                                                                                                <td class="ltd_int">
                                                                                                    Min/Sec.
                                                                                                </td>
                                                                                                <td class="ltd_int">
                                                                                                    NS
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="tdLabel50" valign="top">
                                                                                                    <asp:RangeValidator ID="rvLatitudeDeg" runat="server" ControlToValidate="tbLatitudeDeg"
                                                                                                        Type="Integer" MinimumValue="0" MaximumValue="90" ValidationGroup="Save" CssClass="alert-text"
                                                                                                        ErrorMessage="0-90" Display="Dynamic"></asp:RangeValidator>
                                                                                                </td>
                                                                                                <td class="tdLabel40">
                                                                                                    <asp:RangeValidator ID="rvLatitudeMin" runat="server" Type="Double" ControlToValidate="tbLatitudeMin"
                                                                                                        MinimumValue="0" MaximumValue="60" ErrorMessage="0-60" ValidationGroup="Save"
                                                                                                        CssClass="alert-text" Display="Dynamic" SetFocusOnError="true"></asp:RangeValidator>
                                                                                                    <asp:RegularExpressionValidator ID="revLatitudeMin" runat="server" ControlToValidate="tbLatitudeMin"
                                                                                                        ValidationGroup="Save" ErrorMessage="00.0" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,1})?$"
                                                                                                        Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                                </td>
                                                                                                <td valign="top">
                                                                                                    <asp:RegularExpressionValidator ID="revLatitudeNS" runat="server" Display="Dynamic"
                                                                                                        ControlToValidate="tbLatitudeNS" ErrorMessage="N or S" ValidationExpression="[nN]|[sS]"
                                                                                                        CssClass="alert-text" SetFocusOnError="true" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="3">
                                                                                                    <asp:RequiredFieldValidator ID="rfvLatitudeDeg" runat="server" ControlToValidate="tbLatitudeDeg"
                                                                                                        ValidationGroup="Save" Display="Dynamic" SetFocusOnError="true" CssClass="alert-text">Latitude Deg. is Required.</asp:RequiredFieldValidator>
                                                                                                    <asp:RequiredFieldValidator ID="rfvLatitudeMin" runat="server" ControlToValidate="tbLatitudeMin"
                                                                                                        ValidationGroup="Save" Display="Dynamic" SetFocusOnError="true" CssClass="alert-text">Latitude Min. is Required.</asp:RequiredFieldValidator>
                                                                                                    <asp:RequiredFieldValidator ID="rfvLatitudeNS" runat="server" ControlToValidate="tbLatitudeNS"
                                                                                                        ValidationGroup="Save" Display="Dynamic" SetFocusOnError="true" CssClass="alert-text">Latitude NS is Required.</asp:RequiredFieldValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <div class="tblspace_3">
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td valign="top" class="tdLabel90">
                                                                                                    Magnetic var.
                                                                                                </td>
                                                                                                <td class="tdLabel45">
                                                                                                    <asp:TextBox ID="tbMagnecticVar" runat="server" CssClass="text35" MaxLength="4" onBlur="return validateEmptyLandingBiasTextbox(this, event)"
                                                                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'.')" pattern="[0-9]*"></asp:TextBox>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="tbVarEW" runat="server" CssClass="text20" MaxLength="1" onKeyPress="return fnAllowAlpha(this,event)"
                                                                                                        onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                </td>
                                                                                                <td class="tdLabel37">
                                                                                                    <asp:RegularExpressionValidator ID="revMagnecticVar" runat="server" ControlToValidate="tbMagnecticVar"
                                                                                                        ValidationGroup="Save" ErrorMessage="00.0" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,1})?$"
                                                                                                        Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:RegularExpressionValidator>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:RegularExpressionValidator ID="revMagnecticVarEW" runat="server" ControlToValidate="tbVarEW"
                                                                                                        ValidationGroup="Save" ErrorMessage="E or W" ValidationExpression="[eE]|[wW]"
                                                                                                        Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:RegularExpressionValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" valign="top">
                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <span class="mnd_text">Longitude</span>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td align="left" valign="top">
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td class="tdLabel50">
                                                                                                    <asp:TextBox ID="tbLongitudeDeg" runat="server" MaxLength="3" CssClass="text35" onKeyPress="return fnAllowNumeric(this, event)"
                                                                                                        onBlur="return validateEmptyLatitudeTextbox(this, event)" pattern="[0-9]*"></asp:TextBox>
                                                                                                </td>
                                                                                                <td class="tdLabel45">
                                                                                                    <asp:TextBox ID="tbLongitudeMin" runat="server" MaxLength="5" CssClass="text35" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                                        onBlur="return validateEmptyLandingBiasTextbox(this, event)" pattern="[0-9]*"></asp:TextBox>
                                                                                                </td>
                                                                                                <td class="tdLabel45">
                                                                                                    <asp:TextBox ID="tbLongitudeEW" runat="server" MaxLength="1" CssClass="text20" onKeyPress="return fnAllowAlpha(this, event)"
                                                                                                        onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr style="padding-top: 0px; padding-bottom: 0px; margin-top: 0px;">
                                                                                                <td class="ltd_int">
                                                                                                    Deg.
                                                                                                </td>
                                                                                                <td valign="top" class="ltd_int">
                                                                                                    Min/Sec.
                                                                                                </td>
                                                                                                <td valign="top" class="ltd_int">
                                                                                                    EW
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="tdLabel40" valign="top">
                                                                                                    <asp:RangeValidator ID="rvLongitudeDeg" runat="server" ControlToValidate="tbLongitudeDeg"
                                                                                                        Type="double" MinimumValue="0" MaximumValue="180" ValidationGroup="Save" CssClass="alert-text"
                                                                                                        ErrorMessage="0-180" Display="Dynamic"></asp:RangeValidator>
                                                                                                </td>
                                                                                                <td class="tdLabel40">
                                                                                                    <asp:RangeValidator ID="rvLongitudeMin" runat="server" ControlToValidate="tbLongitudeMin"
                                                                                                        Type="double" MinimumValue="0" MaximumValue="60" ValidationGroup="Save" CssClass="alert-text"
                                                                                                        ErrorMessage="0-60" Display="Dynamic"></asp:RangeValidator>
                                                                                                    <asp:RegularExpressionValidator ID="revLongitudeMin" runat="server" ControlToValidate="tbLongitudeMin"
                                                                                                        ValidationGroup="Save" ErrorMessage="00.0" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,1})?$"
                                                                                                        Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                                </td>
                                                                                                <td valign="top">
                                                                                                    <asp:RegularExpressionValidator ID="revLongitudeEW" runat="server" ControlToValidate="tbLongitudeEW"
                                                                                                        ValidationGroup="Save" ErrorMessage="E or W" ValidationExpression="[eE]|[wW]"
                                                                                                        Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td valign="top" colspan="3">
                                                                                                    <asp:RequiredFieldValidator ID="rfvLongitudeDeg" runat="server" ControlToValidate="tbLongitudeDeg"
                                                                                                        ValidationGroup="Save" Display="Dynamic" SetFocusOnError="true" CssClass="alert-text">Longitude Deg. is Required.</asp:RequiredFieldValidator>
                                                                                                    <asp:RequiredFieldValidator ID="rfvLongitudeMin" runat="server" ControlToValidate="tbLongitudeMin"
                                                                                                        ValidationGroup="Save" Display="Dynamic" SetFocusOnError="true" CssClass="alert-text">Longitude Min. is Required.</asp:RequiredFieldValidator>
                                                                                                    <asp:RequiredFieldValidator ID="rfvLongitudeEW" runat="server" ControlToValidate="tbLongitudeEW"
                                                                                                        ValidationGroup="Save" Display="Dynamic" SetFocusOnError="true" CssClass="alert-text">Longitude EW is Required.</asp:RequiredFieldValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <div class="tblspace_3">
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td class="tdLabel90">
                                                                                                    Elevation
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="tbElevation" runat="server" CssClass="text35" MaxLength="5" onKeyPress="return fnAllowNumeric(this, event)"
                                                                                                        onBlur="return RemoveSpecialChars(this)" pattern="[0-9]*"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td valign="top">
                                                                            <fieldset>
                                                                                <legend>Aircraft Type Landing Fee</legend>
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td class="tdLabel80" valign="top">
                                                                                            Small
                                                                                        </td>
                                                                                        <td class="tdLabel70" valign="top">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbSmall" runat="server" CssClass="text50" MaxLength="20" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                                            onBlur="return validateEmptyAircraftTextbox(this, event)" pattern="[0-9]*"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:RegularExpressionValidator ID="revSmall" runat="server" ControlToValidate="tbSmall"
                                                                                                            ValidationGroup="Save" ErrorMessage="0000000.00" ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$"
                                                                                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td class="tdLabel50" valign="top">
                                                                                            Medium
                                                                                        </td>
                                                                                        <td valign="top">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbMedium" runat="server" CssClass="text50" MaxLength="20" onBlur="return validateEmptyAircraftTextbox(this, event)"
                                                                                                            onKeyPress="return fnAllowNumericAndChar(this, event,'.')" pattern="[0-9]*" ></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:RegularExpressionValidator ID="revMedium" runat="server" ControlToValidate="tbMedium"
                                                                                                            ValidationGroup="Save" ErrorMessage="0000000.00" ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$"
                                                                                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tdLabel80" valign="top">
                                                                                            Large
                                                                                        </td>
                                                                                        <td class="tdLabel90" valign="top">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbLarge" runat="server" CssClass="text50" MaxLength="20" onBlur="return validateEmptyAircraftTextbox(this, event)"
                                                                                                            onKeyPress="return fnAllowNumericAndChar(this, event,'.')" pattern="[0-9]*"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:RegularExpressionValidator ID="revLarge" runat="server" ControlToValidate="tbLarge"
                                                                                                            ValidationGroup="Save" ErrorMessage="0000000.00" ValidationExpression="^[0-9]{0,7}(\.[0-9]{1,2})?$"
                                                                                                            Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                </table>
                                                                            </fieldset>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" colspan="2">
                                                                            <div class="tblspace_10">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td valign="top" width="80%">
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td align="left" valign="top" class="tdLabel90">
                                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                UTC+/-
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td valign="top" class="tdLabel150">
                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td align="left" class="padright_10">
                                                                                                                <asp:TextBox ID="tbUTC" runat="server" CssClass="text35" MaxLength="6" onBlur="return validateEmptyAircraftTextbox(this, event)"
                                                                                                                    onKeyPress="return fnAllowNumericAndChar(this, event,'-,.')" pattern="[0-9]*"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="tbUTCDate" runat="server" CssClass="inpt_non_edit"> </asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:RangeValidator ID="rvUTC" runat="server" ControlToValidate="tbUTC" ValidationGroup="Save"
                                                                                                                    CssClass="alert-text" Text="-14 to 14" Display="Dynamic" Type="Double" MaximumValue="14"
                                                                                                                    MinimumValue="-14"></asp:RangeValidator>
                                                                                                                <asp:CustomValidator ID="cvUTC" runat="server" ControlToValidate="tbUTC" ErrorMessage="UTC Offset Must Be Unique"
                                                                                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:CustomValidator ID="cvUTCDate" runat="server" ControlToValidate="tbUTC" ErrorMessage="Enter Valid Date"
                                                                                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td valign="top">
                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td class="tdLabel120">
                                                                                                                <span class="mnd_text">DST Region</span>
                                                                                                            </td>
                                                                                                            <td class="tdLabel90">
                                                                                                                <asp:TextBox ID="tbRegion" runat="server" CssClass="text35" MaxLength="5" OnTextChanged="tbRegion_TextChanged"
                                                                                                                    AutoPostBack="true"></asp:TextBox>
                                                                                                                <asp:Button ID="btnRegion" runat="server" OnClientClick="javascript:openWin('RadDSTRegionPopup');return false;"
                                                                                                                    CssClass="browse-button" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td colspan="2">
                                                                                                                <asp:RequiredFieldValidator ID="rfvRegion" runat="server" ControlToValidate="tbRegion"
                                                                                                                    ValidationGroup="Save" Display="Dynamic" SetFocusOnError="true" CssClass="alert-text">Region is Required.</asp:RequiredFieldValidator>
                                                                                                                <asp:CustomValidator ID="cvRegion" runat="server" ControlToValidate="tbRegion" ErrorMessage="Invalid Region Code."
                                                                                                                    Display="Dynamic" ValidationGroup="Save" SetFocusOnError="true" CssClass="alert-text"></asp:CustomValidator>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    Summer Time
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:CheckBox ID="chkSummerTime" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td valign="top">
                                                                                                    UTC Start
                                                                                                </td>
                                                                                                <td valign="top" colspan="2">
                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td valign="top">
                                                                                                                <table cellspacing="0" cellpadding="0">
                                                                                                                    <tr>
                                                                                                                        <td class="padright_10">
                                                                                                                            <uc:DatePicker ID="ucStartDate" runat="server" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                            <td valign="top" width="100%">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td valign="top" class="tdLabel85">
                                                                                                                            <telerik:RadMaskedTextBox ID="rmtbStartTime" runat="server" SelectionOnFocus="SelectAll"
                                                                                                                                Mask="<0..99>:<0..99>" CssClass="RadMaskedTextBox35 mask-textbox">
                                                                                                                            </telerik:RadMaskedTextBox>
                                                                                                                        </td>
                                                                                                                        <td class="tdLabel120">
                                                                                                                            Wind Zone
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <asp:TextBox ID="tbWindZone" runat="server" MaxLength="5" CssClass="text35  inpt_non_edit"
                                                                                                                                onKeyPress="return fnAllowNumeric(this, event)" onBlur="return RemoveSpecialChars(this)" pattern="[0-9]*" ></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td colspan="3">
                                                                                                                            <asp:CustomValidator ID="reqtbStart" runat="server" ValidationGroup="Save" ControlToValidate="rmtbStartTime"
                                                                                                                                CssClass="alert-text" Display="Dynamic" ClientValidationFunction="validateHourAndMinute"
                                                                                                                                ErrorMessage="">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td valign="top">
                                                                                                    UTC Finish
                                                                                                </td>
                                                                                                <td colspan="2" valign="top">
                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td class="padright_10" valign="top">
                                                                                                                <table cellspacing="0" cellpadding="0">
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <uc:DatePicker ID="ucFinishedDate" runat="server" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                            <td valign="top">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td valign="top">
                                                                                                                            <telerik:RadMaskedTextBox ID="rmtbFinishTime" runat="server" SelectionOnFocus="SelectAll"
                                                                                                                                Mask="<0..99>:<0..99>" CssClass="RadMaskedTextBox35 mask-textbox">
                                                                                                                            </telerik:RadMaskedTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <asp:CustomValidator ID="cvtbFinishTime" runat="server" ValidationGroup="Save" ControlToValidate="rmtbFinishTime"
                                                                                                                                CssClass="alert-text" Display="Dynamic" ClientValidationFunction="validateHourAndMinute"
                                                                                                                                ErrorMessage="">
                                                                                                                            </asp:CustomValidator>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td valign="top" width="20%">
                                                                                        <fieldset>
                                                                                            <legend>Bias</legend>
                                                                                            <table width="100%">
                                                                                                <tr>
                                                                                                    <td align="left" class="tdLabel70" valign="top">
                                                                                                        Takeoff
                                                                                                    </td>
                                                                                                    <td valign="top">
                                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="tbTakeoffBias" runat="server" CssClass="text35" MaxLength="4" onBlur="return validateEmptyLandingBiasTextbox(this, event)"
                                                                                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'.')" Text="0.0" pattern="[0-9]*"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:RegularExpressionValidator ID="revTakeoffBias" runat="server" ControlToValidate="tbTakeoffBias"
                                                                                                                        ValidationGroup="Save" ErrorMessage="00.0" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,1})?$"
                                                                                                                        Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="left" class="tdLabel70" valign="top">
                                                                                                        Landing
                                                                                                    </td>
                                                                                                    <td valign="top">
                                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="tbLandingBias" runat="server" CssClass="text35" MaxLength="4" onBlur="return validateEmptyLandingBiasTextbox(this, event)"
                                                                                                                        onKeyPress="return fnAllowNumericAndChar(this, event,'.')" Text="0.0" pattern="[0-9]*"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:RegularExpressionValidator ID="revLandingBias" runat="server" ControlToValidate="tbLandingBias"
                                                                                                                        ValidationGroup="Save" ErrorMessage="00.0" ValidationExpression="^[0-9]{0,2}(\.[0-9]{1,1})?$"
                                                                                                                        Display="Dynamic" CssClass="alert-text"></asp:RegularExpressionValidator>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </fieldset>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <fieldset>
                                                                    <legend>Runway Information</legend>
                                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td width="50%">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                        <td>
                                                                                            Runway ID
                                                                                        </td>
                                                                                        <td>
                                                                                            Length
                                                                                        </td>
                                                                                        <td>
                                                                                            Width
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            1.
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbRunwayId1" runat="server" CssClass="text80" MaxLength="20"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbRunway1Length" runat="server" CssClass="text80" MaxLength="5"
                                                                                                onKeyPress="return fnAllowNumeric(this, event)" onBlur="return RemoveSpecialChars(this)" pattern="[0-9]*"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbRunway1Width" runat="server" CssClass="text80" MaxLength="5" onKeyPress="return fnAllowNumeric(this, event)"
                                                                                                onBlur="return RemoveSpecialChars(this)" pattern="[0-9]*"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            2.
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbRunwayId2" runat="server" CssClass="text80" MaxLength="20"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbRunway2Length" runat="server" CssClass="text80" MaxLength="5"
                                                                                                onKeyPress="return fnAllowNumeric(this, event)" onBlur="return RemoveSpecialChars(this)" pattern="[0-9]*"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbRunway2Width" runat="server" CssClass="text80" MaxLength="5" onKeyPress="return fnAllowNumeric(this, event)"
                                                                                                onBlur="return RemoveSpecialChars(this)" pattern="[0-9]*"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                        <td>
                                                                                            Runway ID
                                                                                        </td>
                                                                                        <td>
                                                                                            Length
                                                                                        </td>
                                                                                        <td>
                                                                                            Width
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            3.
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbRunwayId3" runat="server" CssClass="text80" MaxLength="20"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbRunway3Length" runat="server" CssClass="text80" MaxLength="5"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbRunway3Width" runat="server" CssClass="text80" MaxLength="5"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            4.
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbRunwayId4" runat="server" CssClass="text80" MaxLength="20"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbRunway4Length" runat="server" CssClass="text80" MaxLength="5"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbRunway4Width" runat="server" CssClass="text80" MaxLength="5"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <div class="tblspace_10">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                Airport Information
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="tbAirportInformation" runat="server" CssClass="textarea215x50" TextMode="MultiLine"
                                                                    onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel100" valign="top" colspan="2">
                                                                            FBO of Choice
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="tbFBOofchoice" runat="server" CssClass="textarea215x50" TextMode="MultiLine"
                                                                                Enabled="true" ReadOnly="true" Text="No Fbo Selected"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td valign="top" style="width: 40%;">
                                                            <fieldset>
                                                                <legend>Customs and FSS</legend>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <asp:CheckBox ID="chkCustoms" runat="server" />Customs on Airport
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdLabel150">
                                                                                        Location
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbLocation" runat="server" CssClass="text150" MaxLength="100"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdLabel150">
                                                                                        Customs Phone
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbCustomsPhn" runat="server" CssClass="text150" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                            onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Immigration Phone
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbImmPhn" runat="server" CssClass="text150" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                            onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        Agriculture Phone
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbAgPhn" runat="server" CssClass="text150" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"
                                                                                            onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        Hours of Operation
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbOpHours" runat="server" MaxLength="25" CssClass="text150"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <asp:CheckBox ID="chkEntry" runat="server" />Port of Entry
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdLabel150">
                                                                                        Entry Type
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbEntryType" runat="server" CssClass="text150" MaxLength="6"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdLabel150">
                                                                                        PPR
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbPRR" runat="server" CssClass="text150" MaxLength="7"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <asp:CheckBox ID="chkSlots" runat="server" />Slots
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdLabel150">
                                                                                        FSS Name
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbFSSName" runat="server" CssClass="text150" MaxLength="25"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tdLabel150">
                                                                                        FSS Phone
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbFSSPhn" runat="server" CssClass="text150" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                        <td valign="top" width="60%">
                                                            <fieldset>
                                                                <legend>Frequency and Phone Numbers</legend>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td class="tdLabel70">
                                                                            Tower
                                                                        </td>
                                                                        <td class="tdLabel90">
                                                                            <asp:TextBox ID="tbTower" runat="server" CssClass="text50" MaxLength="7"></asp:TextBox>
                                                                        </td>
                                                                        <td class="tdLabel100">
                                                                            ARINC
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="tbARINC" runat="server" CssClass="text50" MaxLength="7"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            ATIS
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="tbATIS" runat="server" CssClass="text50" MaxLength="7"></asp:TextBox>
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:TextBox ID="tbATISPhone" runat="server" CssClass="text183" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Ground
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="tbGround" runat="server" CssClass="text50" MaxLength="7"></asp:TextBox>
                                                                        </td>
                                                                        <td align="left">
                                                                            Approach
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="tbApproach" runat="server" CssClass="text50" MaxLength="7"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Departure
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="tbDepart" runat="server" CssClass="text50" MaxLength="7"></asp:TextBox>
                                                                        </td>
                                                                        <td align="left">
                                                                            UNICOM
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="tbUnicom" runat="server" CssClass="text50" MaxLength="10"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tdLabel130">
                                                                            Clearance Delivery 1
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="tbClrDel1" runat="server" CssClass="text50" MaxLength="10"></asp:TextBox>
                                                                        </td>
                                                                        <td align="left" class="tdLabel130">
                                                                            Clearance Delivery 2
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="tbClrDel2" runat="server" CssClass="text50" MaxLength="10"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            ASOS
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="tbASOS" runat="server" CssClass="text50" MaxLength="10"></asp:TextBox>
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:TextBox ID="tbASOSPhone" runat="server" CssClass="text183" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            AWOS
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="tbAWOS" runat="server" CssClass="text50" MaxLength="10"></asp:TextBox>
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:TextBox ID="tbAWOSPhone" runat="server" CssClass="text183" MaxLength="25" onKeyPress="return fnAllowPhoneFormat(this,event)"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            AWOS Type
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="tbAwosType" runat="server" CssClass="text50" MaxLength="1"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Custom 1
                                                                        </td>
                                                                        <td valign="top">
                                                                            <asp:TextBox ID="tbFREQ1" runat="server" CssClass="text50" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" MaxLength="7"  pattern="[0-9]*"></asp:TextBox>
                                                                        </td>
                                                                        <td valign="top" colspan="2">
                                                                            <asp:TextBox ID="tbLabel1FREQ" runat="server" CssClass="text183" MaxLength="10"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td colspan="2">
                                                                         <asp:RegularExpressionValidator ID="revFREQ1" runat="server" ControlToValidate="tbFREQ1"
                                                                            EnableClientScript="True" CssClass="alert-text" ValidationGroup="Save" ErrorMessage="Expected Format(000.000)"
                                                                            Display="Dynamic" ValidationExpression="^[0-9]{0,12}(\.[0-9]{1,3})?$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Custom 2
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="tbFREQ2" runat="server" CssClass="text50" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" MaxLength="7" pattern="[0-9]*"></asp:TextBox>
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:TextBox ID="tbLabel2FREQ" runat="server" CssClass="text183" MaxLength="10"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td colspan="2">
                                                                         <asp:RegularExpressionValidator ID="revFREQ2" runat="server" ControlToValidate="tbFREQ2"
                                                                            EnableClientScript="True" CssClass="alert-text" ValidationGroup="Save" ErrorMessage="Expected Format(000.000)"
                                                                            Display="Dynamic" ValidationExpression="^[0-9]{0,12}(\.[0-9]{1,3})?$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Metro
                                                                        </td>
                                                                        <td valign="top" class="tdLabel200" colspan="3">
                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        <asp:TextBox ID="tbMetroCD" runat="server" CssClass="text50" MaxLength="3" AutoPostBack="true"
                                                                                            OnTextChanged="tbMetroCD_TextChanged"></asp:TextBox>
                                                                                        <asp:Button ID="btnMetro" runat="server" OnClientClick="javascript:openWin('RadMetroCityPopup');return false;"
                                                                                            CssClass="browse-button" />
                                                                                        </button>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CustomValidator ID="cvMetro" runat="server" ControlToValidate="tbMetroCD" ErrorMessage="Invalid Metro Code."
                                                                                            Display="Dynamic" class="alert-text" ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="tblspace_20">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tblspace_10">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="tdLabel170" valign="top">
                                                            Exchange Rate
                                                        </td>
                                                        <td align="left" class="tdLabel200" valign="top">
                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                <tr>
                                                                    <td class="tdLabel80">
                                                                        <asp:TextBox ID="tbExchangeRate" runat="server" OnTextChanged="ExchangeRate_TextChanged"
                                                                            AutoPostBack="true" CssClass="text40" MaxLength="6"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdnExchangeRateID" runat="server" />
                                                                        <asp:Button ID="btnSearchExchange" runat="server" OnClientClick="javascript:openWin('RadExchangeMasterPopup');return false;"
                                                                            CssClass="browse-button" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CustomValidator ID="cvExchangeRate" runat="server" ControlToValidate="tbExchangeRate"
                                                                            ErrorMessage="Invalid Exchange Rate Code." Display="Dynamic" CssClass="alert-text"
                                                                            ValidationGroup="Save"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel100" valign="top">
                                                            Custom Notes
                                                        </td>
                                                        <td valign="top">
                                                            <asp:TextBox ID="tbNegotiateTerm" runat="server" CssClass="textarea250x50" TextMode="MultiLine"
                                                                MaxLength="800"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <fieldset>
                                                    <legend>Airport Hours of Operation</legend>
                                                    <table>
                                                      <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel130" valign="top">
                                                                        Sunday
                                                                    </td>
                                                                    <td valign="top" class="tdLabel180">
                                                                        <asp:TextBox ID="tbSundayWorkHours" runat="server" CssClass="text150" MaxLength="15"></asp:TextBox>
                                                                    </td>
                                                                    <td class="tdLabel160" valign="top">
                                                                        Monday
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="tbMondayWorkHours" runat="server" CssClass="text150" MaxLength="15"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel130" valign="top">
                                                                        Tuesday
                                                                    </td>
                                                                    <td valign="top" class="tdLabel180">
                                                                        <asp:TextBox ID="tbTuesdayWorkHours" runat="server" CssClass="text150" MaxLength="15"></asp:TextBox>
                                                                    </td>
                                                                    <td class="tdLabel160" valign="top">
                                                                        Wednesday
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="tbWednesdayWorkHours" runat="server" CssClass="text150" MaxLength="15"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel130" valign="top">
                                                                        Thursday
                                                                    </td>
                                                                    <td valign="top" class="tdLabel180">
                                                                        <asp:TextBox ID="tbThursdayWorkHours" runat="server" CssClass="text150" MaxLength="15"></asp:TextBox>
                                                                    </td>
                                                                    <td class="tdLabel160" valign="top">
                                                                        Friday
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="tbFridayWorkHours" runat="server" CssClass="text150" MaxLength="15"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel130" valign="top">
                                                                        Saturday
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:TextBox ID="tbSaturdayWorkHours" runat="server" CssClass="text150" MaxLength="15"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_5">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                        </tr>
                                        
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlbarnotes" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                runat="server" o>
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Notes" CssClass="PanelHeaderStyle">
                        <Items>
                            <telerik:RadPanelItem Value="Notes" runat="server">
                                <ContentTemplate>
                                    <table width="100%" cellspacing="0" class="note-box">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbNotes" TextMode="MultiLine" runat="server" CssClass="textarea-db"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlbarAlerts" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Alerts" CssClass="PanelHeaderStyle">
                        <Items>
                            <telerik:RadPanelItem Value="Alerts" runat="server">
                                <ContentTemplate>
                                    <table width="100%" cellspacing="0" class="note-box">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="tbAlerts" TextMode="MultiLine" runat="server" CssClass="textarea-db "
                                                    onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadPanelBar ID="pnlbarrunways" Width="100%" ExpandAnimation-Type="None"
                CollapseAnimation-Type="None" runat="server">
                <Items>
                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Runways" CssClass="PanelHeaderStyle">
                        <Items>
                            <telerik:RadPanelItem Value="Runways" runat="server">
                                <ContentTemplate>
                                    <telerik:RadGrid ID="dgRunways" runat="server" AllowSorting="true" OnNeedDataSource="dgRunways_BindData"
                                        AutoGenerateColumns="false" PageSize="10" AllowPaging="true" AllowFilteringByColumn="true"
                                        PagerStyle-AlwaysVisible="true" Height="341px" Width="765px">
                                        <MasterTableView DataKeyNames="RunwayLength,RunwayWidth,Surface,Lights,InstLandSysType,NavigationAids">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="UWARunwayID" HeaderText="Runway ID" HeaderStyle-Width="80px"
                                                    FilterControlWidth="60px" ShowFilterIcon="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="RunwayLength" HeaderText="Length" HeaderStyle-Width="80px"
                                                    FilterControlWidth="60px" ShowFilterIcon="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="RunwayWidth" HeaderText="Width" HeaderStyle-Width="60px"
                                                    FilterControlWidth="40px" ShowFilterIcon="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Surface" HeaderText="Surface" HeaderStyle-Width="80px"
                                                    FilterControlWidth="60px" ShowFilterIcon="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Lights" HeaderText="Lights" HeaderStyle-Width="60px"
                                                    FilterControlWidth="40px" ShowFilterIcon="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="InstLandSysType" HeaderText="ILS Type" HeaderStyle-Width="80px"
                                                    FilterControlWidth="60px" ShowFilterIcon="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="NavigationAids" HeaderText="Nav. Aids" ShowFilterIcon="false"
                                                    HeaderStyle-Width="325px" FilterControlWidth="305px">
                                                </telerik:GridBoundColumn>
                                            </Columns>
                                            <CommandItemTemplate>
                                            </CommandItemTemplate>
                                        </MasterTableView>
                                        <ClientSettings>
                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                            <Selecting AllowRowSelect="true" />
                                        </ClientSettings>
                                    </telerik:RadGrid>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelBar>
            <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td align="right">
                        <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                            <tr align="right">
                                <td>
                                    <asp:Button ID="btnSaveChanges" Text="Save" runat="server" OnClick="SaveChanges_Click"
                                        CssClass="button" ValidationGroup="Save" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancel" Text="Cancel" runat="server" CausesValidation="false"
                                        OnClick="Cancel_Click" CssClass="button" />
                                </td>
                                <asp:HiddenField ID="hdnSave" runat="server" />
                                <asp:HiddenField ID="hdnStartDate" runat="server" />
                                <asp:HiddenField ID="hdnFinishedDate" runat="server" />
                                <asp:HiddenField ID="hdnUTCDate" runat="server" />
                                <asp:HiddenField ID="hdnDate" runat="server" />
                                <asp:HiddenField ID="hdnStartTime" runat="server" />
                                <asp:HiddenField ID="hdnEndTime" runat="server" />
                                <asp:HiddenField ID="hdnDstStartDate" runat="server" />
                                <asp:Button ID="btnDstTime" runat="server" OnClick="btnDstTime_OnClick" CssClass="buttonfalse" />
                                <asp:HiddenField ID="hdnDstEndDate" runat="server" />
                                <asp:HiddenField ID="hdnAirportId" runat="server" />
                                <asp:HiddenField ID="hdnDstID" runat="server" />
                                <asp:HiddenField ID="hdnCountryId" runat="server" />
                                <asp:HiddenField ID="hdnMetroId" runat="server" />
                                <asp:HiddenField ID="hdnRedirect" runat="server" />
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
            <script type="text/javascript">
                function onClientClose(obj, event) {
                    event.set_cancel(true);
                    $("#" + "<%=btnCancelTop.ClientID%>").click();
                    obj.remove_beforeClose(onClientClose);
                }
                $(document).ready(function () {
                    
                    var rdAddWindow = GetRadWindow();
                    if (rdAddWindow != null && rdAddWindow != undefined)
                    {
                        console.log("<%=btnCancelTop.Visible %>");
                        rdAddWindow.add_beforeClose(onClientClose);
                        
                    }
                });
            </script>        

        </asp:Panel>
    </div>
</asp:Content>
