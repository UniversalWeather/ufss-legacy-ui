﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.GridHelpers;
using Telerik.Web.UI;
using System.Drawing;
using System.Text;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.Airports
{
    public partial class AirportCatalog : BaseSecuredPage
    {
        #region "Variables"
        private bool IsValidateCustom = true;
        private bool IsUpdate = true;
        private bool IsEmptyCheck = true;
        private bool IsUwa = true;
        private bool IsRunway = true;
        private ExceptionManager exManager;
        string DateFormat;
        string StartDateTime;
        string EndDateTime;
        private bool IsDisplayInctive;
        private bool AirportPageNavigated = false;
        private string Custnum = string.Empty;
        string SearchSessionName = "SearchItemPrimaryKeyValue", SelectedItemSessionName = "SelectedAirportRow";
        private bool _selectLastModified = false;
        #endregion
        #region "Methods"
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgAirportCatalog, dgAirportCatalog, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgAirportCatalog.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));
                        // Set Logged-in User Name
                        lbtnReports.Visible = lbtnSaveReports.Visible = IsAuthorized(Permission.DatabaseReports.ViewAirportReport);
                        if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                            ((RadDatePicker)ucDatePicker.FindControl("RadDatePicker1")).DateInput.DateFormat = DateFormat;
                        }
                        if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._CustomerID != null)
                        {
                            Custnum = UserPrincipal.Identity._fpSettings._CustomerID.ToString().Trim();
                        }
                        if (Request.QueryString["AirportID"] != null)
                        {
                            Session["SelectedAirportID"] = Request.QueryString["AirportID"].ToString();
                        }                       
                        CheckIsWidget("PageLoad");
                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Database.ViewAirport);
                            //if (!IsPopUp && Session[SearchSessionName] == null && Session[SelectedItemSessionName] != null)
                            //{ LoadOptimized(); }
                            if (Session[SearchSessionName] != null)
                            {
                                Session["SelectedAirportID"] = Session[SearchSessionName];
                                dgAirportCatalog.Rebind();
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                            else if (Session[SelectedItemSessionName] != null)
                            {
                                //Session["SelectedAirportRow"] = Session[SelectedItemSessionName];
                                dgAirportCatalog.Rebind();
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                            }
                        }
                        //
                        Session.Remove("SearchItemPrimaryKeyValue");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack && IsPopUp)
                        {
                            //Set visible false to all controls.
                            //Container control visibility throwing some error
                            table1.Visible = false;
                            table2.Visible = false;
                            divInactive.Visible = false;
                            dgAirportCatalog.Visible = false;
                            if (IsAdd)
                            {
                                (dgAirportCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.InitInsertCommandName, string.Empty);
                            }
                            else if (IsViewOnly)
                            {
                                if (Session["SelectedAirportID"] != null)
                                {
                                    ReadOnlyForm();
                                }
                            }
                            else
                            {
                                (dgAirportCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.EditCommandName, string.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FleetProfile);
                }
            }

            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    if (Session["SelectedAirportID"] != null)
                    {
                        Session[SelectedItemSessionName] = Session["SelectedAirportID"];
                        SelectItem(SelectedItemSessionName, chkDisplayInctive.Checked);
                    }
                }
            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPopUp)
            {
                // Set Master Page
                this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");

            }
            CheckIsWidget("PreInit");
        }
        /// <summary>
        /// To Select first 300 records during page Load
        /// </summary>
        private void LoadOptimized()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["SelectedAirportID"] != null)
                {
                    Session["SelectedAirportRow"] = Session["SelectedAirportID"];
                    dgAirportCatalog.Rebind();
                    if (Session["SelectedAirportRow"] != null)
                    {
                        SelectItem(SelectedItemSessionName, chkDisplayInctive.Checked);
                    }
                }
                else
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        IsDisplayInctive = chkDisplayInctive.Checked;
                        var AirportValue = FPKMasterService.GetAllAirportListForGridLimit(300, IsDisplayInctive);
                        var AirportCount = FPKMasterService.GetAllAirportListForGridCount(IsDisplayInctive).EntityList.ToList();
                        if (AirportValue.ReturnFlag == true)
                        {
                            dgAirportCatalog.AllowCustomPaging = true;
                            dgAirportCatalog.VirtualItemCount = AirportCount[0].RecordCount.Value;
                            dgAirportCatalog.DataSource = AirportValue.EntityList.ToList();
                            dgAirportCatalog.DataBind();
                            dgAirportCatalog.SelectedIndexes.Add(0);
                            Session["SelectedAirportID"] = dgAirportCatalog.Items[0].GetDataKeyValue("AirportID").ToString();
                        }
                        if (dgAirportCatalog.MasterTableView.Items.Count > 0)
                        {
                            ReadOnlyForm();
                        }
                        else
                        {
                            ClearForm();
                            EnableForm(false);
                        }
                        GridEnable(true, true, true);
                    }
                }
            }
        }
        /// <summary>
        /// To Display first record as default
        /// </summary>
        private void DefaultSelectGrid(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Checking whether the ICAO ID is there in querry string
                //if (Session["SelectedAirportID"] != null)
                //{
                //    Session["SelectedAirportRow"] = Session["SelectedAirportID"];
                //    dgAirportCatalog.Rebind();
                //    if (Session["SelectedAirportRow"] != null)
                //    {
                //        SelectItem(SelectedItemSessionName, chkDisplayInctive.Checked);
                //    }
                //}
                //else
                //{
                    if (BindDataSwitch)
                    {
                        dgAirportCatalog.Rebind();
                    }
                    if (dgAirportCatalog.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedAirportID"] = null;
                        //}
                        if (Session["SelectedAirportID"] == null)
                        {
                            
                            if (!IsPopUp)
                                Session["SelectedAirportID"] = UserPrincipal.Identity._airportId;
                        }

                        ReadOnlyForm();
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                    
                //}

                EnableForm(false);
                GridEnable(true, true, true);

                if (Session["SelectedAirportID"] != null)
                    hdnAirportId.Value = Session["SelectedAirportID"].ToString().Trim();

            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.Validate(group);
                        // get the first validator that failed
                        var validator = GetValidators(group)
                            .OfType<BaseValidator>()
                            .FirstOrDefault(v => !v.IsValid);
                        // set the focus to the control
                        // that the validator targets
                        if (validator != null)
                        {
                            Control target = validator
                                .NamingContainer
                                .FindControl(validator.ControlToValidate);
                            if (target != null)
                            {
                                target.Focus();
                                IsEmptyCheck = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// Method to check custom Validator
        /// </summary>
        private void CheckCustomValidator()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((CheckAllReadyICAOExist()) && (IsUpdate))
                {
                    cvICAO.IsValid = false;
                    RadAjaxManager.GetCurrent(Page).FocusControl(tbICAO);
                    //tbICAO.Focus();
                    IsValidateCustom = false;
                }
                if (!CheckAllReadyICAOExist())
                {
                    RadAjaxManager.GetCurrent(Page).FocusControl(tbIATA);
                    //tbIATA.Focus();
                }
                if (CheckAllReadyRegionExist(false))
                {
                    cvRegion.IsValid = false;
                    RadAjaxManager.GetCurrent(Page).FocusControl(tbRegion);
                    // tbRegion.Focus();
                    IsValidateCustom = false;
                }
                if (CheckAllReadyMetroExist())
                {
                    cvMetro.IsValid = false;
                    RadAjaxManager.GetCurrent(Page).FocusControl(tbMetroCD);
                    // tbMetroCD.Focus();
                    IsValidateCustom = false;
                }
                if (CheckAllReadyCountryExist())
                {
                    cvCountry.IsValid = false;
                    RadAjaxManager.GetCurrent(Page).FocusControl(tbCountry);
                    // tbCountry.Focus();
                    IsValidateCustom = false;
                }
            }
        }
        /// <summary>
        /// Method to check unique Country  Code 
        /// </summary>
        /// <returns></returns>
        private bool CheckAllReadyCountryExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                if ((tbCountry.Text != null) && (tbCountry.Text != string.Empty))
                {
                    //To check for unique Country code
                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var CountryValue = FPKMasterService.GetCountryMasterList().EntityList.Where(x => x.CountryCD != null && x.CountryCD.ToString().ToUpper().Trim().Equals(tbCountry.Text.ToString().ToUpper().Trim())).ToList();
                        if (CountryValue.Count() > 0 && CountryValue != null)
                        {
                            if (!string.IsNullOrEmpty(((FlightPakMasterService.Country)CountryValue[0]).CountryName))
                            {
                                tbCountryDesc.Text = ((FlightPakMasterService.Country)CountryValue[0]).CountryName.ToUpper().Trim();
                                hdnCountryId.Value = ((FlightPakMasterService.Country)CountryValue[0]).CountryID.ToString().ToUpper().Trim();
                                tbCountry.Text = ((FlightPakMasterService.Country)CountryValue[0]).CountryCD;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbApMgr);
                            }
                            //tbApMgr.Focus();
                        }
                        else
                        {
                            cvCountry.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCountry);
                            //tbCountry.Focus();
                            ReturnValue = false;
                        }
                    }
                }
                return ReturnValue;
            }
        }
        /// <summary>
        /// Method to check unique Region Code 
        /// </summary>
        /// <returns></returns>
        private bool CheckAllReadyRegionExist(Boolean val)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                //System.Decimal Offset = 0;
                if (tbRegion.Text != null)
                {
                    // To check for unique region code
                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var DSTValue = FPKMasterService.GetDSTRegionList().EntityList.Where(x => x.DSTRegionCD.ToString().ToUpper().Trim().Equals(tbRegion.Text.ToUpper().Trim())).ToList();
                        if (DSTValue.Count() > 0 && DSTValue != null)
                        {
                            tbUTC.Text = ((FlightPakMasterService.DSTRegion)DSTValue[0]).OffSet.ToString().Trim();
                            hdnDstID.Value = ((FlightPakMasterService.DSTRegion)DSTValue[0]).DSTRegionID.ToString().Trim();
                            hdnDstStartDate.Value = ((FlightPakMasterService.DSTRegion)DSTValue[0]).StartDT.ToString().Trim();
                            hdnDstEndDate.Value = ((FlightPakMasterService.DSTRegion)DSTValue[0]).EndDT.ToString().Trim();
                            tbRegion.Text = ((FlightPakMasterService.DSTRegion)DSTValue[0]).DSTRegionCD;
                            if (!String.IsNullOrEmpty(hdnDstStartDate.Value))
                            {
                                DateTime dtStart = DateTime.Parse(hdnDstStartDate.Value);
                                hdnStartTime.Value = dtStart.ToString("HH:mm");
                                if (val)
                                    chkSummerTime.Checked = true;
                            }
                            else
                            {
                                hdnStartTime.Value = string.Empty;
                                if (val)
                                    chkSummerTime.Checked = false;
                            }

                            if (!String.IsNullOrEmpty(hdnDstEndDate.Value))
                            {
                                DateTime dtEnd = DateTime.Parse(hdnDstEndDate.Value);
                                hdnEndTime.Value = dtEnd.ToString("HH:mm");
                            }
                            else
                            { hdnEndTime.Value = string.Empty; }

                            if (!string.IsNullOrEmpty(hdnDstStartDate.Value))
                            {
                                TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
                                if (!string.IsNullOrEmpty(DateFormat))
                                    tbStartDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(hdnDstStartDate.Value)).Substring(0, 11).Trim(); //hdnDstStartDate.Value.ToString().Substring(0, 11); //FormatDate(hdnDstStartDate.ToString().Substring(0, 11), DateFormat).ToString();
                                else
                                    tbStartDate.Text = hdnDstStartDate.Value.ToString().Substring(0, 11).Trim();
                            }
                            if (!string.IsNullOrEmpty(hdnDstEndDate.Value))
                            {
                                TextBox tbEndDate = (TextBox)ucFinishedDate.FindControl("tbDate");
                                if (!string.IsNullOrEmpty(DateFormat))
                                    tbEndDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(hdnDstEndDate.Value)).Substring(0, 11).Trim(); //FormatDate(hdnDstEndDate.Value.ToString().Substring(0, 11).Trim(), DateFormat).ToString();//hdnDstEndDate.Value.ToString().Substring(0, 11); 
                                else
                                    tbEndDate.Text = hdnDstEndDate.Value.ToString().Substring(0, 11).Trim();
                            }
                            if (!string.IsNullOrEmpty(hdnStartTime.Value))
                            {
                                //rmtbStartTime.TextWithLiterals = hdnStartTime.Value;
                                rmtbStartTime.Text = hdnStartTime.Value;
                            }
                            else { rmtbStartTime.TextWithLiterals = "00:00"; }
                            if (!string.IsNullOrEmpty(hdnEndTime.Value))
                            {
                                //rmtbFinishTime.TextWithLiterals = hdnEndTime.Value;
                                rmtbFinishTime.Text = hdnEndTime.Value;
                            }
                            else { rmtbFinishTime.TextWithLiterals = "00:00"; }

                            RadAjaxManager.GetCurrent(Page).FocusControl(chkSummerTime);
                            //chkSummerTime.Focus();
                        }
                        else
                        {
                            cvRegion.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbRegion);
                            //tbRegion.Focus();
                            ReturnValue = true;
                        }
                    }
                }
                return ReturnValue;
            }
        }
        /// <summary>
        /// Method to check unique Metro Code 
        /// </summary>
        /// <returns></returns>
        private bool CheckAllReadyMetroExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = false;
                if ((tbMetroCD.Text != null) && (tbMetroCD.Text != string.Empty))
                {
                    //To check for unique metro code
                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var MetroValue = FPKMasterService.GetMetroCityList().EntityList.Where(x => x.MetroCD.ToString().ToUpper().Trim().Equals(tbMetroCD.Text.ToUpper().Trim())).ToList();
                        if (MetroValue.Count() > 0 && MetroValue != null)
                        {
                            hdnMetroId.Value = ((FlightPakMasterService.Metro)MetroValue[0]).MetroID.ToString().Trim();
                            tbMetroCD.Text = ((FlightPakMasterService.Metro)MetroValue[0]).MetroCD;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbNotes);
                            //tbNotes.Focus();
                        }
                        else
                        {
                            cvMetro.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbMetroCD);
                            //tbMetroCD.Focus();
                            ReturnValue = true;
                        }
                    }
                }
                return ReturnValue;
            }
        }
        /// <summary>
        /// To check the unique code of ICAO
        /// </summary>
        /// <returns></returns>
        private bool CheckAllReadyICAOExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnVal = false;
                //No meaning to have this block 
                //Doing extra checks to ommit this block
                if (!IsAdd)
                {
                    string Code = Session["SelectedAirportID"].ToString().Trim();
                    string CustomerID = "";
                    foreach (GridDataItem Item in dgAirportCatalog.MasterTableView.Items)
                    {
                        if (Item["AirportID"].Text.Trim() == Code)
                        {
                            CustomerID = Item["CustomerID"].Text.Trim();
                            break;
                        }
                    }
                }
                using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var AirportResults = FPKMasterService.GetAirportByAirportICaoID(tbICAO.Text.ToString().Trim()).EntityList.Where(x => x.CustomerID.ToString().Trim().Equals(Custnum)).ToList();
                    if (AirportResults.Count() > 0 && AirportResults != null)
                    {
                        ReturnVal = true;
                    }
                    else
                    {
                        ReturnVal = false;
                    }
                    return ReturnVal;
                }
            }
        }
        /// <summary>
        /// To FormatDateTime
        /// </summary>
        /// <param name="Date"></param>
        /// <param name="Format"></param>
        /// <returns></returns>
        private DateTime FormatDateTime(string Date, string Format)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Date, Format))
            {
                string[] DateAndTime = Date.Split(' ');
                string[] SplitDate = new string[3];
                string[] SplitFormat = new string[3];
                string[] TimeFormat = { "hh", "mm", "ss" };
                string[] SplitTime = new string[3];
                if (DateFormat.Contains("/"))
                {
                    SplitFormat = Format.Split('/');
                    SplitTime = DateAndTime[1].Split(':');
                }
                else if (DateFormat.Contains("-"))
                {
                    SplitFormat = Format.Split('-');
                    SplitTime = DateAndTime[1].Split(':');
                }
                else if (DateFormat.Contains("."))
                {
                    SplitFormat = Format.Split('.');
                    SplitTime = DateAndTime[1].Split(':');
                }
                if (DateAndTime[0].Contains("/"))
                {
                    SplitDate = DateAndTime[0].Split('/');
                    SplitTime = DateAndTime[1].Split(':');
                }
                else if (DateAndTime[0].Contains("-"))
                {
                    SplitDate = DateAndTime[0].Split('-');
                    SplitTime = DateAndTime[1].Split(':');
                }
                else if (DateAndTime[0].Contains("."))
                {
                    SplitDate = DateAndTime[0].Split('.');
                    SplitTime = DateAndTime[1].Split(':');
                }
                int dd = 0, mm = 0, yyyy = 0, hh = 0, MM = 0, ss = 0;
                for (int Index = 0; Index < SplitFormat.Count(); Index++)
                {
                    if (SplitFormat[Index].ToLower().Contains("d"))
                    {
                        dd = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("m"))
                    {
                        mm = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("y"))
                    {
                        yyyy = Convert.ToInt16(SplitDate[Index]);
                    }
                }
                for (int Index = 0; Index < TimeFormat.Count(); Index++)
                {
                    if (TimeFormat[Index].ToLower().Contains("hh"))
                    {
                        hh = Convert.ToInt32(SplitTime[Index]);
                    }
                    if (TimeFormat[Index].ToLower().Contains("mm"))
                    {
                        MM = Convert.ToInt32(SplitTime[Index]);
                    }
                    if (TimeFormat[Index].ToLower().Contains("ss"))
                    {
                        ss = Convert.ToInt32(SplitTime[Index]);
                    }
                }
                return new DateTime(yyyy, mm, dd, hh, MM, ss);
            }
        }
        /// <summary>
        /// To Display all the fields
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                hdnSave.Value = "Save";
                ClearForm();
                EnableForm(true);
            }
        }
        /// <summary>
        /// To select the selected item which they selected during navigating to transport,hotel,catering
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void SelectItem()
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
        //    {
        //        if (Session["SelectedAirportRow"] != null)
        //        {
        //            //bool FoundItem = false;
        //            //int CurrentPageIndex = dgAirportCatalog.CurrentPageIndex; // the current page index of the Grid
        //            ////loop through the items of all item page from the current to the last
        //            //for (int i = CurrentPageIndex; i < dgAirportCatalog.PageCount; i++)
        //            //{
        //            //    foreach (GridDataItem Item in dgAirportCatalog.Items)
        //            //    {
        //            //        if (Item["AirportID"].Text.Trim().ToUpper() == Session["SelectedAirportRow"].ToString().Trim().ToUpper())
        //            //        {
        //            //            Item.Selected = true;
        //            //            GridDataItem item = dgAirportCatalog.SelectedItems[0] as GridDataItem;
        //            //            Session["SelectedAirportID"] = item["AirportID"].Text;
        //            //            ReadOnlyForm();
        //            //            GridEnable(true, true, true);
        //            //            FoundItem = true;
        //            //            break;
        //            //        }
        //            //    }
        //            //    if (FoundItem)
        //            //    {
        //            //        //in order selection to be applied
        //            //        break;
        //            //    }
        //            //    else
        //            //    {
        //            //        dgAirportCatalog.CurrentPageIndex = i + 1;
        //            //        dgAirportCatalog.Rebind();
        //            //    }
        //            //}

        //            //Performance Fix - Vishwa
        //            dgAirportCatalog.AllowPaging = false;
        //            dgAirportCatalog.Rebind();
        //            foreach (GridDataItem Item in dgAirportCatalog.Items)
        //            {
        //                if (Item["AirportID"].Text.Trim().ToUpper() == Session["SelectedAirportRow"].ToString().Trim().ToUpper())
        //                {
        //                    Item.Selected = true;
        //                    GridDataItem item = dgAirportCatalog.SelectedItems[0] as GridDataItem;
        //                    Session["SelectedAirportID"] = item["AirportID"].Text;
        //                    ReadOnlyForm();
        //                    GridEnable(true, true, true);
        //                    break;
        //                }
        //            }
        //            dgAirportCatalog.AllowPaging = true;
        //            dgAirportCatalog.Rebind();
        //        }
        //    }
        //}
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private Airport GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.Airport AirportService = new FlightPakMasterService.Airport();
                AirportService.IcaoID = tbICAO.Text.Trim();
                AirportService.Iata = tbIATA.Text.Trim();
                AirportService.AirportName = tbAirport.Text.Trim();
                AirportService.IsEUETS = chkEUETS.Checked;
                AirportService.CityName = tbCity.Text.Trim();
                AirportService.StateName = tbStateProvince.Text.Trim();
                if (!string.IsNullOrEmpty(hdnCountryId.Value))
                    AirportService.CountryID = Convert.ToInt64(hdnCountryId.Value.Trim());
                AirportService.CountryName = tbCountryDesc.Text.Trim();
                AirportService.AirportManager = tbApMgr.Text.Trim();
                AirportService.AirportManagerPhoneNum = tbMgrPhn.Text.Trim();
                AirportService.IsUSTax = chkUSTax.Checked;
                AirportService.IsRural = chkRural.Checked;
                AirportService.IsAirport = chkAirport.Checked;
                AirportService.IsHeliport = chkHeliport.Checked;
                AirportService.IsSeaPlane = chkSeaplane.Checked;
                AirportService.IsWorldClock = chkWorldClock.Checked;
                if (hdnSave.Value == "Update")
                {
                    Int64 Code = 0;
                    if (Session["SelectedAirportID"] != null)
                        Code = Convert.ToInt64(Session["SelectedAirportID"]);

                    string CustomerID = "", IcaoID = "", UWAID = "";
                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var AirportList = FPKMasterService.GetAirportByAirportIDFBO(Code).EntityList.ToList();
                        GetAirportByAirportIDFBO AirportCatalogEntity = AirportList[0];
                        if (AirportList.Count > 0)
                        {
                            if ((!string.IsNullOrEmpty(AirportCatalogEntity.UWAID)) && (AirportCatalogEntity.UWAID != "&nbsp;"))
                            {
                                AirportService.UWAID = AirportCatalogEntity.UWAID.ToString().Trim();
                                if (AirportCatalogEntity.LongestRunway != null)
                                {
                                    IsRunway = false;
                                    AirportService.LongestRunway = AirportCatalogEntity.LongestRunway;
                                }
                            }
                            else
                            {
                                AirportService.UWAID = AirportCatalogEntity.UWAID;
                                IsRunway = true;
                            }
                            if (AirportCatalogEntity.CustomerID != null)
                                CustomerID = AirportCatalogEntity.CustomerID.ToString();
                            if (!string.IsNullOrEmpty(AirportCatalogEntity.IcaoID))
                                IcaoID = AirportCatalogEntity.IcaoID.ToString().Trim();
                            AirportService.AirportID = Code;
                            if (AirportCatalogEntity.FBOCnt != null)
                                AirportService.FBOCnt = AirportCatalogEntity.FBOCnt;
                            if (AirportCatalogEntity.HotelCnt != null)
                                AirportService.HotelCnt = AirportCatalogEntity.HotelCnt;
                            if (AirportCatalogEntity.CateringCnt != null)
                                AirportService.CateringCnt = AirportCatalogEntity.CateringCnt;
                            if (AirportCatalogEntity.TransportationCnt != null)
                                AirportService.TransportationCnt = AirportCatalogEntity.TransportationCnt;
                            if (AirportCatalogEntity.IsFlightPakFlag != null)
                                AirportService.IsFlightPakFlag = AirportCatalogEntity.IsFlightPakFlag;
                            if (AirportCatalogEntity.AlternateDestination1 != null)
                                AirportService.AlternateDestination1 = AirportCatalogEntity.AlternateDestination1;
                            if (AirportCatalogEntity.AlternateDestination2 != null)
                                AirportService.AlternateDestination2 = AirportCatalogEntity.AlternateDestination2;
                            if (AirportCatalogEntity.AlternateDestination3 != null)
                                AirportService.AlternateDestination3 = AirportCatalogEntity.AlternateDestination3;
                            if (AirportCatalogEntity.RecordType != null)
                                AirportService.RecordType = AirportCatalogEntity.RecordType;
                            if (AirportCatalogEntity.NewICAO != null)
                                AirportService.NewICAO = AirportCatalogEntity.NewICAO;
                            if (AirportCatalogEntity.PreviousICAO != null)
                                AirportService.PreviousICAO = AirportCatalogEntity.PreviousICAO;
                            if (AirportCatalogEntity.FAA != null)
                                AirportService.FAA = AirportCatalogEntity.FAA;
                            if (AirportCatalogEntity.IsFIX != null)
                                AirportService.IsFIX = AirportCatalogEntity.IsFIX;
                            if (AirportCatalogEntity.UWAUpdates != null)
                                AirportService.UWAUpdates = AirportCatalogEntity.UWAUpdates;
                            if (AirportCatalogEntity.FlightPakUPD != null)
                                AirportService.FlightPakUPD = AirportCatalogEntity.FlightPakUPD;
                            if (AirportCatalogEntity.WidthRunway != null)
                                AirportService.WidthRunway = AirportCatalogEntity.WidthRunway;



                        }
                    }
                }
                else
                {
                    AirportService.UWAID = "";

                    AirportService.AirportID = 0;
                }
                AirportService.IsInActive = chkInactive.Checked;
                if (!string.IsNullOrEmpty(tbRunway1Length.Text))
                {
                    AirportService.LengthWidthRunway = Convert.ToDecimal(tbRunway1Length.Text.Trim(), CultureInfo.CurrentCulture);
                }
                if (!string.IsNullOrEmpty(tbRunway1Width.Text))
                {
                    AirportService.WidthLengthRunway = Convert.ToDecimal(tbRunway1Width.Text.Trim(), CultureInfo.CurrentCulture);
                }
                if (!string.IsNullOrEmpty(tbRunway2Length.Text))
                {
                    AirportService.Runway2Length = Convert.ToDecimal(tbRunway2Length.Text.Trim(), CultureInfo.CurrentCulture);
                }
                if (!string.IsNullOrEmpty(tbRunway2Width.Text))
                {
                    AirportService.Runway2Width = Convert.ToDecimal(tbRunway2Width.Text.Trim(), CultureInfo.CurrentCulture);
                }
                if (!string.IsNullOrEmpty(tbRunway3Length.Text))
                {
                    AirportService.Runway3Length = Convert.ToDecimal(tbRunway3Length.Text.Trim(), CultureInfo.CurrentCulture);
                }
                if (!string.IsNullOrEmpty(tbRunway3Width.Text))
                {
                    AirportService.Runway3Width = Convert.ToDecimal(tbRunway3Width.Text.Trim(), CultureInfo.CurrentCulture);
                }
                if (!string.IsNullOrEmpty(tbRunway4Length.Text))
                {
                    AirportService.Runway4Length = Convert.ToDecimal(tbRunway4Length.Text.Trim(), CultureInfo.CurrentCulture);
                }
                if (!string.IsNullOrEmpty(tbRunway4Width.Text))
                {
                    AirportService.Runway4Width = Convert.ToDecimal(tbRunway4Width.Text.Trim(), CultureInfo.CurrentCulture);
                }
                if (!string.IsNullOrEmpty(tbRunwayId1.Text))
                {
                    AirportService.RunwayID1 = tbRunwayId1.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbRunwayId2.Text))
                {
                    AirportService.RunwayID2 = tbRunwayId2.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbRunwayId3.Text))
                {
                    AirportService.RunwayID3 = tbRunwayId3.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbRunwayId4.Text))
                {
                    AirportService.RunwayID4 = tbRunwayId4.Text.Trim();
                }
                if (!string.IsNullOrEmpty(tbTakeoffBias.Text))
                {
                    AirportService.TakeoffBIAS = Convert.ToDecimal(tbTakeoffBias.Text.Trim(), CultureInfo.CurrentCulture);
                }
                if (!string.IsNullOrEmpty(tbLandingBias.Text))
                {
                    AirportService.LandingBIAS = Convert.ToDecimal(tbLandingBias.Text.Trim(), CultureInfo.CurrentCulture);
                }
                if (!string.IsNullOrEmpty(tbLatitudeDeg.Text))
                {
                    AirportService.LatitudeDegree = Convert.ToDecimal(tbLatitudeDeg.Text.Trim(), CultureInfo.CurrentCulture);
                }
                if (!string.IsNullOrEmpty(tbLatitudeMin.Text))
                {
                    AirportService.LatitudeMinutes = Convert.ToDecimal(tbLatitudeMin.Text.Trim(), CultureInfo.CurrentCulture);
                }
                AirportService.LatitudeNorthSouth = tbLatitudeNS.Text.Trim();
                if (!string.IsNullOrEmpty(tbMagnecticVar.Text))
                {
                    AirportService.MagneticVariance = Convert.ToDecimal(tbMagnecticVar.Text.Trim(), CultureInfo.CurrentCulture);
                }
                AirportService.VarianceEastWest = tbVarEW.Text.Trim();
                if (!string.IsNullOrEmpty(tbLongitudeMin.Text))
                {
                    AirportService.LongitudeMinutes = Convert.ToDecimal(tbLongitudeMin.Text.Trim(), CultureInfo.CurrentCulture);
                }
                AirportService.LongitudeEastWest = tbLongitudeEW.Text.Trim();
                if (!string.IsNullOrEmpty(tbLongitudeDeg.Text))
                {
                    AirportService.LongitudeDegrees = Convert.ToDecimal(tbLongitudeDeg.Text.Trim(), CultureInfo.CurrentCulture);
                }
                AirportService.IsMilitary = chkIsMilitary.Checked;
                AirportService.IsPublic = chkIsPublic.Checked;
                AirportService.IsPrivate = chkIsPrivate.Checked;

                if (hdnExchangeRateID.Value.ToString().Trim() != string.Empty)
                {
                    AirportService.ExchangeRateID = Convert.ToInt64(hdnExchangeRateID.Value);
                }
                if (tbNegotiateTerm.Text.Trim() != string.Empty)
                {
                    AirportService.CustomNotes = tbNegotiateTerm.Text.Trim();
                }
                if (tbSundayWorkHours.Text.Trim() != string.Empty)
                {
                    AirportService.CustomsSundayWorkHours = tbSundayWorkHours.Text;
                }
                if (tbTuesdayWorkHours.Text.Trim() != string.Empty)
                {
                    AirportService.CustomsTuesdayWorkHours = tbTuesdayWorkHours.Text;
                }
                if (tbWednesdayWorkHours.Text.Trim() != string.Empty)
                {
                    AirportService.CustomsWednesdayWorkHours = tbWednesdayWorkHours.Text;
                }
                if (tbThursdayWorkHours.Text.Trim() != string.Empty)
                {
                    AirportService.CustomsThursdayWorkHours = tbThursdayWorkHours.Text;
                }
                if (tbFridayWorkHours.Text.Trim() != string.Empty)
                {
                    AirportService.CustomsFridayWorkHours = tbFridayWorkHours.Text;
                }
                if (tbSaturdayWorkHours.Text.Trim() != string.Empty)
                {
                    AirportService.CustomsSaturdayWorkHours = tbSaturdayWorkHours.Text;
                }
                if (!string.IsNullOrEmpty(tbElevation.Text))
                {
                    AirportService.Elevation = Convert.ToDecimal(tbElevation.Text.Trim(), CultureInfo.CurrentCulture);
                }
                AirportService.AirportInfo = tbAirportInformation.Text.Trim();
                if (!string.IsNullOrEmpty(tbSmall.Text))
                {
                    AirportService.LandingFeeSmall = Convert.ToDecimal(tbSmall.Text.Trim(), CultureInfo.CurrentCulture);
                }
                if (!string.IsNullOrEmpty(tbMedium.Text))
                {
                    AirportService.LandingFeeMedium = Convert.ToDecimal(tbMedium.Text.Trim(), CultureInfo.CurrentCulture);
                }
                if (!string.IsNullOrEmpty(tbLarge.Text))
                {
                    AirportService.LandingFeeLarge = Convert.ToDecimal(tbLarge.Text.Trim(), CultureInfo.CurrentCulture);
                }
                if (!string.IsNullOrEmpty(tbUTC.Text))
                {
                    AirportService.OffsetToGMT = Convert.ToDecimal(tbUTC.Text.Trim(), CultureInfo.CurrentCulture);
                }
                if (!string.IsNullOrEmpty(hdnUTCDate.Value))
                {
                    AirportService.UpdateDT = Convert.ToDateTime(hdnUTCDate.Value, CultureInfo.CurrentCulture);
                }
                if ((!string.IsNullOrEmpty(StartDateTime)) && (!string.IsNullOrEmpty(DateFormat)) && (!string.IsNullOrEmpty(hdnStartDate.Value)))
                {
                    AirportService.DayLightSavingStartDT = Convert.ToDateTime(FormatDate(StartDateTime.Trim(), DateFormat));
                }
                else
                {
                    if ((!string.IsNullOrEmpty(StartDateTime)) && (string.IsNullOrEmpty(DateFormat)) && (!string.IsNullOrEmpty(hdnStartDate.Value)))
                    {
                        AirportService.DayLightSavingStartDT = Convert.ToDateTime(StartDateTime);
                    }
                }
                if (rmtbStartTime.Text != "0000")
                {
                    AirportService.DaylLightSavingStartTM = hdnStartTime.Value;
                }
                AirportService.IsDayLightSaving = chkSummerTime.Checked;
                AirportService.DSTRegionID = Convert.ToInt64(hdnDstID.Value.Trim());
                if (!string.IsNullOrEmpty(tbWindZone.Text))
                {
                    AirportService.WindZone = Convert.ToDecimal(tbWindZone.Text.Trim(), CultureInfo.CurrentCulture);
                }
                if ((!string.IsNullOrEmpty(EndDateTime)) && (!string.IsNullOrEmpty(DateFormat)) && (!string.IsNullOrEmpty(hdnFinishedDate.Value)))
                {
                    AirportService.DayLightSavingEndDT = Convert.ToDateTime(FormatDate(EndDateTime.Trim(), DateFormat));
                }
                else
                {
                    if ((!string.IsNullOrEmpty(EndDateTime)) && (string.IsNullOrEmpty(DateFormat)) && (!string.IsNullOrEmpty(hdnFinishedDate.Value)))
                    {
                        AirportService.DayLightSavingEndDT = Convert.ToDateTime(hdnFinishedDate.Value);
                    }
                }
                if (rmtbFinishTime.Text != "0000")
                {
                    AirportService.DayLightSavingEndTM = hdnEndTime.Value;
                }
                AirportService.IsCustomAvailable = chkCustoms.Checked;
                AirportService.CustomLocation = tbLocation.Text.Trim();
                AirportService.ImmigrationPhoneNum = tbImmPhn.Text.Trim();
                AirportService.CustomPhoneNum = tbCustomsPhn.Text.Trim();
                AirportService.AgPhoneNum = tbAgPhn.Text.Trim();
                AirportService.IsEntryPort = chkEntry.Checked;
                AirportService.PortEntryType = tbEntryType.Text.Trim();
                AirportService.PPR = tbPRR.Text.Trim();
                AirportService.HoursOfOperation = tbOpHours.Text.Trim();
                AirportService.IsSlots = chkSlots.Checked;
                AirportService.FssName = tbFSSName.Text.Trim();
                AirportService.FssPhone = tbFSSPhn.Text.Trim();
                AirportService.TowerFreq = tbTower.Text.Trim();
                AirportService.ARINCFreq = tbARINC.Text.Trim();
                AirportService.ATISFreq = tbATIS.Text.Trim();
                AirportService.AtisPhoneNum = tbATISPhone.Text.Trim();
                AirportService.GroundFreq = tbGround.Text.Trim();
                AirportService.ApproachFreq = tbApproach.Text.Trim();
                AirportService.DepartFreq = tbDepart.Text.Trim();
                AirportService.Unicom = tbUnicom.Text.Trim();
                AirportService.Clearence1DEL = tbClrDel1.Text.Trim();
                AirportService.Clearence2DEL = tbClrDel2.Text.Trim();
                AirportService.ASOS = tbASOS.Text.Trim();
                AirportService.AsosPhoneNum = tbASOSPhone.Text.Trim();
                AirportService.AWOS = tbAWOS.Text.Trim();
                AirportService.AwosPhoneNum = tbAWOSPhone.Text.Trim();
                AirportService.AwosType = tbAwosType.Text.Trim();
                AirportService.Label1Freq = tbLabel1FREQ.Text.Trim();
                AirportService.Freq1 = tbFREQ1.Text.Trim();
                if (!string.IsNullOrEmpty(hdnMetroId.Value))
                {
                    AirportService.MetroID = Convert.ToInt64(hdnMetroId.Value.Trim());
                }
                AirportService.Label2Freq = tbLabel2FREQ.Text.Trim();
                AirportService.Freq2 = tbFREQ2.Text.Trim();
                AirportService.GeneralNotes = tbNotes.Text.Trim();
                AirportService.Alerts = tbAlerts.Text.Trim();
                AirportService.GeneralNotes = tbNotes.Text.Trim();
                AirportService.IsDeleted = false;
                AirportService.IsDayLightSaving = chkSummerTime.Checked;
                //Preflight calculation are made based on this LongestRunway column, Dont change this code
                if (IsRunway == true)
                {
                    if ((!string.IsNullOrEmpty(tbRunway1Length.Text)) && (!string.IsNullOrEmpty(tbRunway2Length.Text)) && (!string.IsNullOrEmpty(tbRunway3Length.Text)) && (!string.IsNullOrEmpty(tbRunway4Length.Text)))
                    {
                        decimal Runway1Length = Convert.ToDecimal(tbRunway1Length.Text);
                        decimal Runway2Length = Convert.ToDecimal(tbRunway2Length.Text);
                        decimal Runway3Length = Convert.ToDecimal(tbRunway3Length.Text);
                        decimal Runway4Length = Convert.ToDecimal(tbRunway4Length.Text);
                        var result = (System.Math.Max(System.Math.Max(Runway1Length, Runway2Length), Runway3Length));
                        if (result > Runway4Length)
                        {
                            AirportService.LongestRunway = result;
                        }
                        else
                        {
                            AirportService.LongestRunway = Runway4Length;
                        }
                    }
                }
                return AirportService;
            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                LoadControlData();
                hdnSave.Value = "Update";
                hdnRedirect.Value = "";
                if (IsUwa)
                {
                    DisplayUwaRecords(true);
                }
                else
                {
                    EnableForm(true);
                }
            }
        }
        /// <summary>
        /// Disaply UWA Records
        /// </summary>
        protected void DisplayUwaRecords(bool Enabled)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbTakeoffBias.Enabled = true;
                tbLandingBias.Enabled = true;
                tbNotes.Enabled = true;
                tbAlerts.Enabled = true;
                tbMetroCD.Enabled = true;
                tbRegion.Enabled = true;
                tbFSSPhn.Enabled = true;
                tbICAO.Enabled = false;
                chkWorldClock.Enabled = true;
                chkCustoms.Enabled = true;
                tbIATA.Enabled = false;
                tbAirport.Enabled = false;
                chkEUETS.Enabled = false;
                tbCity.Enabled = false;
                tbStateProvince.Enabled = false;
                tbCountry.Enabled = false;
                tbCountryDesc.Enabled = false;
                tbApMgr.Enabled = false;
                tbMgrPhn.Enabled = false;
                chkUSTax.Enabled = true;
                chkRural.Enabled = false;
                chkAirport.Enabled = false;
                chkHeliport.Enabled = false;
                chkSeaplane.Enabled = false;
                chkInactive.Enabled = false;
                tbRunwayId1.Enabled = false;
                tbRunwayId2.Enabled = false;
                tbRunwayId3.Enabled = false;
                tbRunwayId4.Enabled = false;
                tbRunway1Length.Enabled = false;
                tbRunway1Width.Enabled = false;
                tbRunway2Length.Enabled = false;
                tbRunway2Width.Enabled = false;
                tbRunway3Length.Enabled = false;
                tbRunway3Width.Enabled = false;
                tbRunway4Length.Enabled = false;
                tbRunway4Width.Enabled = false;
                tbLatitudeDeg.Enabled = false;
                tbLatitudeMin.Enabled = false;
                tbLatitudeNS.Enabled = false;
                tbMagnecticVar.Enabled = false;
                tbVarEW.Enabled = false;
                tbLongitudeMin.Enabled = false;
                tbLongitudeEW.Enabled = false;
                tbLongitudeDeg.Enabled = false;
                tbElevation.Enabled = false;
                //tbAirportInformation.Enabled = false;
                tbSmall.Enabled = true;
                tbMedium.Enabled = true;
                tbLarge.Enabled = true;
                chkSummerTime.Enabled = false;
                tbWindZone.Enabled = true;
                tbLocation.Enabled = false;
                tbImmPhn.Enabled = false;
                tbCustomsPhn.Enabled = false;
                tbAgPhn.Enabled = false;
                chkEntry.Enabled = false;
                tbEntryType.Enabled = false;
                tbPRR.Enabled = false;
                tbOpHours.Enabled = false;
                chkSlots.Enabled = false;
                tbFSSName.Enabled = false;
                tbTower.Enabled = false;
                tbARINC.Enabled = false;
                tbATIS.Enabled = false;
                tbATISPhone.Enabled = false;
                tbGround.Enabled = false;
                tbApproach.Enabled = false;
                tbDepart.Enabled = false;
                tbUnicom.Enabled = false;
                tbClrDel1.Enabled = false;
                tbClrDel2.Enabled = false;
                tbASOS.Enabled = false;
                tbASOSPhone.Enabled = false;
                tbAWOS.Enabled = false;
                tbAWOSPhone.Enabled = false;
                tbAwosType.Enabled = false;
                tbLabel1FREQ.Enabled = true;
                tbFREQ1.Enabled = true;
                tbLabel2FREQ.Enabled = true;
                tbFREQ2.Enabled = true;
                TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
                tbStartDate.Enabled = false;
                rmtbStartTime.Enabled = false;
                TextBox tbFinishedDate = (TextBox)ucFinishedDate.FindControl("tbDate");
                tbFinishedDate.Enabled = false;
                rmtbFinishTime.Enabled = false;
                tbUTCDate.Enabled = false;
                tbUTC.Enabled = false;
                btnCountry.Enabled = false;
                btnRegion.Enabled = true;
                btnMetro.Enabled = true;
                btnSaveChanges.Visible = true;
                btnCancel.Visible = true;
                btnSaveTop.Visible = true;
                btnCancelTop.Visible = true;
                if ((hdnSave.Value == "Update") && (Enabled == true))
                {
                    chkDisplayInctive.Enabled = false;
                }
                else
                {
                    chkDisplayInctive.Enabled = true;
                }
                tbFBOofchoice.Enabled = true;
                tbAirportInformation.Enabled = true;
                tbAirportInformation.ReadOnly = true;
            }
        }
        /// <summary>
        /// To make the form readonly
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                EnableForm(false);
                LoadControlData();

            }
        }
        /// <summary>
        /// To Clear the form
        /// </summary>        
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                lbColumnName1.Text = "ICAO ID";
                lbColumnValue1.Text = string.Empty;
                lbColumnName2.Text = "Airport Name";
                lbColumnValue2.Text = string.Empty;
                tbICAO.Text = string.Empty;
                tbIATA.Text = string.Empty;
                tbAirport.Text = string.Empty;
                chkEUETS.Checked = false;
                tbCity.Text = string.Empty;
                tbStateProvince.Text = string.Empty;
                tbCountry.Text = string.Empty;
                tbCountryDesc.Text = string.Empty;
                tbApMgr.Text = string.Empty;
                tbMgrPhn.Text = string.Empty;
                chkUSTax.Checked = false;
                chkRural.Checked = false;
                chkAirport.Checked = false;
                chkHeliport.Checked = false;
                chkSeaplane.Checked = false;
                chkWorldClock.Checked = false;
                chkInactive.Checked = false;
                tbRunwayId1.Text = string.Empty;
                tbRunwayId2.Text = string.Empty;
                tbRunwayId3.Text = string.Empty;
                tbRunwayId4.Text = string.Empty;
                tbRunway1Length.Text = "0";
                tbRunway1Width.Text = "0";
                tbRunway2Length.Text = "0";
                tbRunway2Width.Text = "0";
                tbRunway3Length.Text = "0";
                tbRunway3Width.Text = "0";
                tbRunway4Length.Text = "0";
                tbRunway4Width.Text = "0";
                tbTakeoffBias.Text = "0.0";
                tbLandingBias.Text = "0.0";
                tbLatitudeDeg.Text = "0";
                tbLatitudeMin.Text = "0.0";
                tbLatitudeNS.Text = string.Empty;
                tbMagnecticVar.Text = "0.0";
                tbVarEW.Text = string.Empty;
                tbLongitudeMin.Text = "0.0";
                tbLongitudeEW.Text = string.Empty;
                tbLongitudeDeg.Text = "0";
                tbElevation.Text = "0";
                tbAirportInformation.Text = string.Empty;
                tbFBOofchoice.Text = string.Empty;
                tbSmall.Text = "0.00";
                tbMedium.Text = "0.00";
                tbLarge.Text = "0.00";
                chkSummerTime.Checked = false;
                tbRegion.Text = string.Empty;
                tbWindZone.Text = string.Empty;
                chkCustoms.Checked = false;
                tbLocation.Text = string.Empty;
                tbImmPhn.Text = string.Empty;
                tbCustomsPhn.Text = string.Empty;
                tbAgPhn.Text = string.Empty;
                chkEntry.Checked = false;
                tbEntryType.Text = string.Empty;
                tbPRR.Text = string.Empty;
                tbOpHours.Text = string.Empty;
                chkSlots.Checked = false;
                tbFSSName.Text = string.Empty;
                tbFSSPhn.Text = string.Empty;
                tbTower.Text = string.Empty;
                tbARINC.Text = string.Empty;
                tbATIS.Text = string.Empty;
                tbATISPhone.Text = string.Empty;
                tbGround.Text = string.Empty;
                tbApproach.Text = string.Empty;
                tbDepart.Text = string.Empty;
                tbUnicom.Text = string.Empty;
                tbClrDel1.Text = string.Empty;
                tbClrDel2.Text = string.Empty;
                tbASOS.Text = string.Empty;
                tbASOSPhone.Text = string.Empty;
                tbAWOS.Text = string.Empty;
                tbAWOSPhone.Text = string.Empty;
                tbAwosType.Text = string.Empty;
                tbLabel1FREQ.Text = string.Empty;
                tbFREQ1.Text = string.Empty;
                tbMetroCD.Text = string.Empty;
                tbLabel2FREQ.Text = string.Empty;
                tbFREQ2.Text = string.Empty;
                tbNotes.Text = string.Empty;
                tbAlerts.Text = string.Empty;
                tbNotes.Text = string.Empty;
                (((TextBox)ucStartDate.FindControl("tbDate")).Text) = string.Empty;
                rmtbStartTime.Text = "0000";
                (((TextBox)ucFinishedDate.FindControl("tbDate")).Text) = string.Empty;
                rmtbFinishTime.Text = "0000";
                tbUTCDate.Text = string.Empty;
                tbUTC.Text = "0.00";
                hdnCountryId.Value = string.Empty;
                hdnMetroId.Value = string.Empty;

                chkIsPrivate.Checked = false;

                chkIsPublic.Checked = false;

                chkIsMilitary.Checked = false;

                tbExchangeRate.Text = "";

                tbNegotiateTerm.Text = "";

                tbSundayWorkHours.Text = "";
                tbMondayWorkHours.Text = "";
                tbTuesdayWorkHours.Text = "";

                tbWednesdayWorkHours.Text = "";

                tbThursdayWorkHours.Text = "";

                tbFridayWorkHours.Text = "";

                tbSaturdayWorkHours.Text = "";
            }
        }
        /// <summary>
        /// To Enable the form
        /// </summary>
        /// <param name="Enable"></param>
        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                if (Enable == true && hdnSave.Value == "Save")
                {
                    tbICAO.Enabled = true;
                }
                else
                {
                    tbICAO.Enabled = false;
                }
                tbIATA.Enabled = Enable;
                tbAirport.Enabled = Enable;
                chkEUETS.Enabled = Enable;
                tbCity.Enabled = Enable;
                tbStateProvince.Enabled = Enable;
                tbCountry.Enabled = Enable;
                tbCountryDesc.Enabled = Enable;
                tbApMgr.Enabled = Enable;
                tbMgrPhn.Enabled = Enable;
                chkUSTax.Enabled = Enable;
                chkRural.Enabled = Enable;
                chkAirport.Enabled = Enable;
                chkHeliport.Enabled = Enable;
                chkSeaplane.Enabled = Enable;
                chkWorldClock.Enabled = Enable;
                chkInactive.Enabled = Enable;
                tbTakeoffBias.Enabled = Enable;
                tbRunwayId1.Enabled = Enable;
                tbRunwayId2.Enabled = Enable;
                tbRunwayId3.Enabled = Enable;
                tbRunwayId4.Enabled = Enable;
                tbRunway1Length.Enabled = Enable;
                tbRunway1Width.Enabled = Enable;
                tbRunway2Length.Enabled = Enable;
                tbRunway2Width.Enabled = Enable;
                tbRunway3Length.Enabled = Enable;
                tbRunway3Width.Enabled = Enable;
                tbRunway4Length.Enabled = Enable;
                tbRunway4Width.Enabled = Enable;
                tbLandingBias.Enabled = Enable;
                tbLatitudeDeg.Enabled = Enable;
                tbLatitudeMin.Enabled = Enable;
                tbLatitudeNS.Enabled = Enable;
                tbMagnecticVar.Enabled = Enable;
                tbVarEW.Enabled = Enable;
                tbLongitudeMin.Enabled = Enable;
                tbLongitudeEW.Enabled = Enable;
                tbLongitudeDeg.Enabled = Enable;
                tbElevation.Enabled = Enable;
                //                tbAirportInformation.Enabled = Enable;
                tbSmall.Enabled = Enable;
                tbMedium.Enabled = Enable;
                tbLarge.Enabled = Enable;
                chkSummerTime.Enabled = Enable;
                tbRegion.Enabled = Enable;
                tbWindZone.Enabled = Enable;
                chkCustoms.Enabled = Enable;
                tbLocation.Enabled = Enable;
                tbImmPhn.Enabled = Enable;
                tbCustomsPhn.Enabled = Enable;
                tbAgPhn.Enabled = Enable;
                chkEntry.Enabled = Enable;
                tbEntryType.Enabled = Enable;
                tbPRR.Enabled = Enable;
                tbOpHours.Enabled = Enable;
                chkSlots.Enabled = Enable;
                tbFSSName.Enabled = Enable;
                tbFSSPhn.Enabled = Enable;
                tbTower.Enabled = Enable;
                tbARINC.Enabled = Enable;
                tbATIS.Enabled = Enable;
                tbATISPhone.Enabled = Enable;
                tbGround.Enabled = Enable;
                tbApproach.Enabled = Enable;
                tbDepart.Enabled = Enable;
                tbUnicom.Enabled = Enable;
                tbClrDel1.Enabled = Enable;
                tbClrDel2.Enabled = Enable;
                tbASOS.Enabled = Enable;
                tbASOSPhone.Enabled = Enable;
                tbAWOS.Enabled = Enable;
                tbAWOSPhone.Enabled = Enable;
                tbAwosType.Enabled = Enable;
                tbLabel1FREQ.Enabled = Enable;
                tbFREQ1.Enabled = Enable;
                tbMetroCD.Enabled = Enable;
                tbLabel2FREQ.Enabled = Enable;
                tbFREQ2.Enabled = Enable;
                tbNotes.Enabled = Enable;
                tbAlerts.Enabled = Enable;
                tbNotes.Enabled = Enable;
                TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
                tbStartDate.Enabled = Enable;
                rmtbStartTime.Enabled = Enable;
                TextBox tbFinishedDate = (TextBox)ucFinishedDate.FindControl("tbDate");
                tbFinishedDate.Enabled = Enable;
                rmtbFinishTime.Enabled = Enable;
                tbUTCDate.Enabled = Enable;
                tbUTC.Enabled = Enable;
                btnCountry.Enabled = Enable;
                btnRegion.Enabled = Enable;
                btnMetro.Enabled = Enable;
                btnSaveChanges.Visible = Enable;
                btnCancel.Visible = Enable;
                btnSaveTop.Visible = Enable;
                btnCancelTop.Visible = Enable;
                tbUTCDate.Enabled = false;
                chkIsPrivate.Enabled = Enable;
                chkIsPublic.Enabled = Enable;
                chkIsMilitary.Enabled = Enable;
                btnSearchExchange.Enabled = Enable;
                tbExchangeRate.Enabled = Enable;
                tbNegotiateTerm.Enabled = Enable;
                tbSundayWorkHours.Enabled = Enable;
                tbMondayWorkHours.Enabled = Enable;
                tbTuesdayWorkHours.Enabled = Enable;
                tbWednesdayWorkHours.Enabled = Enable;
                tbThursdayWorkHours.Enabled = Enable;
                tbFridayWorkHours.Enabled = Enable;
                tbSaturdayWorkHours.Enabled = Enable;

                if ((hdnSave.Value == "Update") && (Enable == true))
                {
                    chkDisplayInctive.Enabled = false;
                }
                else
                {
                    chkDisplayInctive.Enabled = true;
                }
                tbFBOofchoice.Enabled = true;
                tbAirportInformation.Enabled = true;
                tbAirportInformation.ReadOnly = !Enable;
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="Add"></param>
        /// <param name="Edit"></param>
        /// <param name="Delete"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                LinkButton lbtnInsertCtl = (LinkButton)dgAirportCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                LinkButton lbtnDelCtl = (LinkButton)dgAirportCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitDelete");
                LinkButton lbtnEditCtl = (LinkButton)dgAirportCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
                if (IsAuthorized(Permission.Database.AddAirport))
                {
                    lbtnInsertCtl.Visible = true;
                    if (Add)
                    {
                        lbtnInsertCtl.Enabled = true;
                    }
                    else
                    {
                        lbtnInsertCtl.Enabled = false;
                    }
                }
                else
                {
                    lbtnInsertCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.DeleteAirport))
                {
                    lbtnDelCtl.Visible = true;
                    if (Delete)
                    {
                        lbtnDelCtl.Enabled = true;
                        lbtnDelCtl.OnClientClick = "javascript:return deleteRecord();";
                    }
                    else
                    {
                        lbtnDelCtl.Enabled = false;
                        lbtnDelCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    lbtnDelCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.EditAirport))
                {
                    lbtnEditCtl.Visible = true;
                    if (Edit)
                    {
                        lbtnEditCtl.Enabled = true;
                        lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                    }
                    else
                    {
                        lbtnEditCtl.Enabled = false;
                        lbtnEditCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    lbtnEditCtl.Visible = false;
                }
            }
        }
        /// <summary>
        /// To assign the data to controls 
        /// </summary>
        protected void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Int64 AirportCode = 0;
                string Code = "";
                if (Session["SelectedAirportID"] != null)
                {
                    AirportCode = Convert.ToInt64(Session["SelectedAirportID"].ToString().Trim());
                    Code = Session["SelectedAirportID"].ToString().Trim();
                    hdnAirportId.Value = Code.ToString();
                }
                string IcaoID = "", UWAID = "", LastUpdUID = "", LastUpdTS = "";
                Int64 CustomerID = 0;
                foreach (GridDataItem Item in dgAirportCatalog.MasterTableView.Items)
                {
                    if (Item["AirportID"].Text.Trim() == Code)
                    {
                        if (Item.GetDataKeyValue("CustomerID") != null)
                            CustomerID = Convert.ToInt64(Item["CustomerID"].Text.Trim());
                        if (Item.GetDataKeyValue("IcaoID") != null)
                            IcaoID = Item["IcaoID"].Text.Trim();
                        if (Item.GetDataKeyValue("UWAID") != null)
                            UWAID = Item.GetDataKeyValue("UWAID").ToString().Trim();
                        if (Item.GetDataKeyValue("LastUpdUID") != null)
                            LastUpdUID = Item["LastUpdUID"].Text.Trim();
                        if (Item.GetDataKeyValue("LastUpdTS") != null)
                            LastUpdTS = Item["LastUpdTS"].Text.Trim();

                        
                        
                        break;
                    }
                }
                Label lbUser = (Label)dgAirportCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                if (!string.IsNullOrEmpty(LastUpdUID))
                {

                    lbUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + LastUpdUID);
                }
                else
                {
                    lbUser.Text = string.Empty;
                }
                if (!string.IsNullOrEmpty(LastUpdTS))
                {

                    lbUser.Text = System.Web.HttpUtility.HtmlEncode(lbUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(LastUpdTS)));
                }
                tbICAO.Text = IcaoID;
                //To get values from DB to display in fields               
                using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var AirportList = FPKMasterService.GetAirportByAirportIDFBO(AirportCode).EntityList.ToList();
                    if (AirportList.Count > 0)
                    {
                        GetAirportByAirportIDFBO AirportCatalogEntity = AirportList[0];
                        if (AirportCatalogEntity.IcaoID != null)
                        {
                            tbICAO.Text = AirportCatalogEntity.IcaoID.Trim();
                            lbColumnName1.Text = "ICAO ID";
                            string icaoString = AirportCatalogEntity.IcaoID.Trim();
                            lbColumnValue1.Text = Microsoft.Security.Application.Encoder.HtmlEncode(icaoString);
                        }
                        else
                        {
                            tbICAO.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.AirportID != null)
                        {
                            hdnAirportId.Value = AirportCatalogEntity.AirportID.ToString();
                        }
                        if (AirportCatalogEntity.Iata != null)
                        {
                            tbIATA.Text = AirportCatalogEntity.Iata.Trim();
                        }
                        else
                        {
                            tbIATA.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.AirportName != null)
                        {
                            string airportName = AirportCatalogEntity.AirportName.Trim();
                            tbAirport.Text = airportName;
                            lbColumnName2.Text = "Airport Name";

                            lbColumnValue2.Text = Microsoft.Security.Application.Encoder.HtmlEncode(airportName);
                        }
                        else
                        {
                            tbAirport.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.IsEUETS != null)
                        {
                            chkEUETS.Checked = Convert.ToBoolean(AirportCatalogEntity.IsEUETS.ToString(), CultureInfo.CurrentCulture);
                        }
                        if (AirportCatalogEntity.CityName != null)
                        {
                            tbCity.Text = AirportCatalogEntity.CityName.Trim();
                        }
                        else
                        {
                            tbCity.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.StateName != null)
                        {
                            tbStateProvince.Text = AirportCatalogEntity.StateName;
                        }
                        else
                        {
                            tbStateProvince.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.CountryCD != null)
                        {
                            tbCountry.Text = AirportCatalogEntity.CountryCD.ToString();
                        }
                        else
                        {
                            tbCountry.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.CountryName != null)
                        {
                            tbCountryDesc.Text = AirportCatalogEntity.CountryName.ToString();
                        }
                        else
                        {
                            tbCountryDesc.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.AirportManager != null)
                        {
                            tbApMgr.Text = AirportCatalogEntity.AirportManager.ToString();
                        }
                        else
                        {
                            tbApMgr.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.AirportManagerPhoneNum != null)
                        {
                            tbMgrPhn.Text = AirportCatalogEntity.AirportManagerPhoneNum.ToString();
                        }
                        else
                        {
                            tbMgrPhn.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.IsUSTax != null)
                        {
                            chkUSTax.Checked = Convert.ToBoolean(AirportCatalogEntity.IsUSTax.ToString().Trim(), CultureInfo.CurrentCulture);
                        }
                        if (AirportCatalogEntity.IsRural != null)
                        {
                            chkRural.Checked = Convert.ToBoolean(AirportCatalogEntity.IsRural.ToString().Trim(), CultureInfo.CurrentCulture);
                        }
                        if (AirportCatalogEntity.IsAirport != null)
                        {
                            chkAirport.Checked = Convert.ToBoolean(AirportCatalogEntity.IsAirport.ToString().Trim(), CultureInfo.CurrentCulture);
                        }
                        if (AirportCatalogEntity.IsHeliport != null)
                        {
                            chkHeliport.Checked = Convert.ToBoolean(AirportCatalogEntity.IsHeliport.ToString().Trim(), CultureInfo.CurrentCulture);
                        }
                        if (AirportCatalogEntity.IsSeaPlane != null)
                        {
                            chkSeaplane.Checked = Convert.ToBoolean(AirportCatalogEntity.IsSeaPlane.ToString().Trim(), CultureInfo.CurrentCulture);
                        }
                        else 
                        { 
                            chkSeaplane.Checked = false; 
                        }
                        if (AirportCatalogEntity.IsWorldClock != null)
                        {
                            chkWorldClock.Checked = Convert.ToBoolean(AirportCatalogEntity.IsWorldClock.ToString().Trim(), CultureInfo.CurrentCulture);
                        }
                        if (AirportCatalogEntity.IsInActive != null)
                        {
                            chkInactive.Checked = Convert.ToBoolean(AirportCatalogEntity.IsInActive.ToString().Trim(), CultureInfo.CurrentCulture);
                        }
                        //Changing as per Anoop
                        if (AirportCatalogEntity.WidthLengthRunway != null)
                        {
                            tbRunway1Width.Text = Convert.ToString(AirportCatalogEntity.WidthLengthRunway, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbRunway1Width.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.LengthWidthRunway != null)
                        {
                            tbRunway1Length.Text = Convert.ToString(AirportCatalogEntity.LengthWidthRunway, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbRunway1Length.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.RunwayID1 != null)
                        {
                            tbRunwayId1.Text = Convert.ToString(AirportCatalogEntity.RunwayID1, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbRunwayId1.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.RunwayID2 != null)
                        {
                            tbRunwayId2.Text = Convert.ToString(AirportCatalogEntity.RunwayID2, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbRunwayId2.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.RunwayID3 != null)
                        {
                            tbRunwayId3.Text = Convert.ToString(AirportCatalogEntity.RunwayID3, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbRunwayId3.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.RunwayID4 != null)
                        {
                            tbRunwayId4.Text = Convert.ToString(AirportCatalogEntity.RunwayID4, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbRunwayId4.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.Runway2Length != null)
                        {
                            tbRunway2Length.Text = Convert.ToString(AirportCatalogEntity.Runway2Length, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbRunway2Length.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.Runway2Width != null)
                        {
                            tbRunway2Width.Text = Convert.ToString(AirportCatalogEntity.Runway2Width, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbRunway2Width.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.Runway3Length != null)
                        {
                            tbRunway3Length.Text = Convert.ToString(AirportCatalogEntity.Runway3Length, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbRunway3Length.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.Runway3Width != null)
                        {
                            tbRunway3Width.Text = Convert.ToString(AirportCatalogEntity.Runway3Width, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbRunway3Width.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.Runway4Length != null)
                        {
                            tbRunway4Length.Text = Convert.ToString(AirportCatalogEntity.Runway4Length, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbRunway4Length.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.Runway4Width != null)
                        {
                            tbRunway4Width.Text = Convert.ToString(AirportCatalogEntity.Runway4Width, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbRunway4Width.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.TakeoffBIAS != null)
                        {
                            tbTakeoffBias.Text = Convert.ToString(AirportCatalogEntity.TakeoffBIAS, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbTakeoffBias.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.LandingBIAS != null)
                        {
                            tbLandingBias.Text = Convert.ToString(AirportCatalogEntity.LandingBIAS, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbLandingBias.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.LatitudeDegree != null)
                        {
                            tbLatitudeDeg.Text = Convert.ToString(AirportCatalogEntity.LatitudeDegree, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbLatitudeDeg.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.LatitudeMinutes != null)
                        {
                            tbLatitudeMin.Text = Convert.ToString(AirportCatalogEntity.LatitudeMinutes, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbLatitudeMin.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.LatitudeNorthSouth != null)
                        {
                            tbLatitudeNS.Text = AirportCatalogEntity.LatitudeNorthSouth;
                        }
                        if (AirportCatalogEntity.MagneticVariance != null)
                        {
                            tbMagnecticVar.Text = Convert.ToString(AirportCatalogEntity.MagneticVariance, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbMagnecticVar.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.VarianceEastWest != null)
                        {
                            tbVarEW.Text = AirportCatalogEntity.VarianceEastWest.Trim();
                        }
                        else
                        {
                            tbVarEW.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.LongitudeMinutes != null)
                        {
                            tbLongitudeMin.Text = Convert.ToString(AirportCatalogEntity.LongitudeMinutes, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbLongitudeMin.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.LongitudeEastWest != null)
                        {
                            tbLongitudeEW.Text = AirportCatalogEntity.LongitudeEastWest.Trim();
                        }
                        else
                        {
                            tbLongitudeEW.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.LongitudeDegrees != null)
                        {
                            tbLongitudeDeg.Text = Convert.ToString(AirportCatalogEntity.LongitudeDegrees, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbLongitudeDeg.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.Elevation != null)
                        {
                            tbElevation.Text = Convert.ToString(AirportCatalogEntity.Elevation, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbElevation.Text = string.Empty;
                        }
                        tbAirportInformation.Text = AirportCatalogEntity.AirportInfo;
                        if (AirportCatalogEntity.LandingFeeSmall != null)
                        {
                            tbSmall.Text = Convert.ToString(AirportCatalogEntity.LandingFeeSmall, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbSmall.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.LandingFeeMedium != null)
                        {
                            tbMedium.Text = Convert.ToString(AirportCatalogEntity.LandingFeeMedium, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbMedium.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.LandingFeeLarge != null)
                        {
                            tbLarge.Text = Convert.ToString(AirportCatalogEntity.LandingFeeLarge, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbLarge.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.OffsetToGMT != null)
                        {
                            tbUTC.Text = Convert.ToString(AirportCatalogEntity.OffsetToGMT, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbUTC.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.LastUpdTS != null)
                        {
                            if (!string.IsNullOrEmpty(DateFormat))
                                tbUTCDate.Text = "Updt:" + String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(AirportCatalogEntity.LastUpdTS)).Substring(0, 11);
                            else
                                tbUTCDate.Text = "Updt:" + AirportCatalogEntity.LastUpdTS.ToString().Substring(0, 11);
                        }
                        else
                        {
                            tbUTCDate.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.IsDayLightSaving != null)
                        {
                            chkSummerTime.Checked = Convert.ToBoolean(AirportCatalogEntity.IsDayLightSaving.ToString(), CultureInfo.CurrentCulture);
                        }
                        if (AirportCatalogEntity.DSTRegionCD != null)
                        {
                            tbRegion.Text = AirportCatalogEntity.DSTRegionCD.ToString().Trim();
                        }
                        else
                        {
                            tbRegion.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.WindZone != null)
                        {
                            tbWindZone.Text = Convert.ToString(AirportCatalogEntity.WindZone, CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            tbWindZone.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.DayLightSavingStartDT != null)
                        {
                            TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
                            if (!string.IsNullOrEmpty(DateFormat))
                                tbStartDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(AirportCatalogEntity.DayLightSavingStartDT)).Substring(0, 11);
                            else
                                tbStartDate.Text = AirportCatalogEntity.DayLightSavingStartDT.ToString().Substring(0, 11);
                        }
                        else
                        {
                            TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
                            tbStartDate.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.DaylLightSavingStartTM != null)
                        {
                            string starttm = AirportCatalogEntity.DaylLightSavingStartTM;
                            rmtbStartTime.Text = starttm.Substring(0, 2) + ":" + starttm.Substring(3, 2);
                        }
                        else
                        {
                            rmtbStartTime.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.DayLightSavingEndDT != null)
                        {
                            TextBox tbEndDate = (TextBox)ucFinishedDate.FindControl("tbDate");
                            if (!string.IsNullOrEmpty(DateFormat))
                                tbEndDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(AirportCatalogEntity.DayLightSavingEndDT)).Substring(0, 11);
                            else
                                tbEndDate.Text = AirportCatalogEntity.DayLightSavingEndDT.ToString().Substring(0, 11);
                        }
                        else
                        {
                            TextBox tbEndDate = (TextBox)ucFinishedDate.FindControl("tbDate");
                            tbEndDate.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.DayLightSavingEndTM != null)
                        {
                            string endtm = AirportCatalogEntity.DayLightSavingEndTM;
                            rmtbFinishTime.Text = endtm.Substring(0, 2) + ":" + endtm.Substring(3, 2);
                        }
                        else
                        {
                            rmtbFinishTime.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.IsCustomAvailable != null)
                        {
                            chkCustoms.Checked = Convert.ToBoolean(AirportCatalogEntity.IsCustomAvailable.ToString(), CultureInfo.CurrentCulture);
                        }
                        if (!string.IsNullOrEmpty(AirportCatalogEntity.CustomLocation))
                            tbLocation.Text = AirportCatalogEntity.CustomLocation;
                        else
                            tbLocation.Text = string.Empty;
                        if (!string.IsNullOrEmpty(AirportCatalogEntity.ImmigrationPhoneNum))
                            tbImmPhn.Text = AirportCatalogEntity.ImmigrationPhoneNum;
                        else
                            tbImmPhn.Text = string.Empty;
                        if (!string.IsNullOrEmpty(AirportCatalogEntity.CustomPhoneNum))
                            tbCustomsPhn.Text = AirportCatalogEntity.CustomPhoneNum;
                        else
                            tbCustomsPhn.Text = string.Empty;
                        if (!string.IsNullOrEmpty(AirportCatalogEntity.AgPhoneNum))
                            tbAgPhn.Text = AirportCatalogEntity.AgPhoneNum;
                        else
                            tbAgPhn.Text = string.Empty;
                        if (AirportCatalogEntity.IsEntryPort != null)
                        {
                            chkEntry.Checked = Convert.ToBoolean(AirportCatalogEntity.IsEntryPort.ToString(), CultureInfo.CurrentCulture);
                        }
                        if (!string.IsNullOrEmpty(AirportCatalogEntity.PortEntryType))
                            tbEntryType.Text = AirportCatalogEntity.PortEntryType;
                        else
                            tbEntryType.Text = string.Empty;
                        if (!string.IsNullOrEmpty(AirportCatalogEntity.PPR))
                            tbPRR.Text = AirportCatalogEntity.PPR.Trim();
                        else
                            tbPRR.Text = string.Empty;
                        if (!string.IsNullOrEmpty(AirportCatalogEntity.HoursOfOperation))
                            tbOpHours.Text = AirportCatalogEntity.HoursOfOperation.Trim();
                        else
                            tbOpHours.Text = string.Empty;
                        if (AirportCatalogEntity.IsSlots != null)
                        {
                            chkSlots.Checked = Convert.ToBoolean(AirportCatalogEntity.IsSlots.ToString(), CultureInfo.CurrentCulture);
                        }
                        if (AirportCatalogEntity.FssName != null)
                        {
                            tbFSSName.Text = AirportCatalogEntity.FssName.Trim();
                        }
                        else
                        {
                            tbFSSName.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.FssPhone != null)
                        {
                            tbFSSPhn.Text = AirportCatalogEntity.FssPhone;
                        }
                        else
                        {
                            tbFSSPhn.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.TowerFreq != null)
                        {
                            tbTower.Text = AirportCatalogEntity.TowerFreq.Trim();
                        }
                        else
                        {
                            tbTower.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.ARINCFreq != null)
                        {
                            tbARINC.Text = AirportCatalogEntity.ARINCFreq.Trim();
                        }
                        else
                        {
                            tbARINC.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.ATISFreq != null)
                        {
                            tbATIS.Text = AirportCatalogEntity.ATISFreq.Trim();
                        }
                        else
                        {
                            tbATIS.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.AtisPhoneNum != null)
                        {
                            tbATISPhone.Text = AirportCatalogEntity.AtisPhoneNum.Trim();
                        }
                        else
                        {
                            tbATISPhone.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.GroundFreq != null)
                        {
                            tbGround.Text = AirportCatalogEntity.GroundFreq.Trim();
                        }
                        else
                        {
                            tbGround.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.ApproachFreq != null)
                        {
                            tbApproach.Text = AirportCatalogEntity.ApproachFreq.Trim();
                        }
                        else
                        {
                            tbApproach.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.DepartFreq != null)
                        {
                            tbDepart.Text = AirportCatalogEntity.DepartFreq.Trim();
                        }
                        else
                        {
                            tbDepart.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.Unicom != null)
                        {
                            tbUnicom.Text = AirportCatalogEntity.Unicom.Trim();
                        }
                        else
                        {
                            tbUnicom.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.Clearence1DEL != null)
                        {
                            tbClrDel1.Text = AirportCatalogEntity.Clearence1DEL.Trim();
                        }
                        else
                        {
                            tbClrDel1.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.Clearence2DEL != null)
                        {
                            tbClrDel2.Text = AirportCatalogEntity.Clearence2DEL.Trim();
                        }
                        else
                        {
                            tbClrDel2.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.ASOS != null)
                        {
                            tbASOS.Text = AirportCatalogEntity.ASOS.Trim();
                        }
                        else
                        {
                            tbASOS.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.AsosPhoneNum != null)
                        {
                            tbASOSPhone.Text = AirportCatalogEntity.AsosPhoneNum.Trim();
                        }
                        else
                        {
                            tbASOSPhone.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.AWOS != null)
                        {
                            tbAWOS.Text = AirportCatalogEntity.AWOS.Trim();
                        }
                        else
                        {
                            tbAWOS.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.AwosPhoneNum != null)
                        {
                            tbAWOSPhone.Text = AirportCatalogEntity.AwosPhoneNum.Trim();
                        }
                        else
                        {
                            tbAWOSPhone.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.AwosType != null)
                        {
                            tbAwosType.Text = AirportCatalogEntity.AwosType.Trim();
                        }
                        else
                        {
                            tbAwosType.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.Label1Freq != null)
                        {
                            tbLabel1FREQ.Text = AirportCatalogEntity.Label1Freq.Trim();
                        }
                        else
                        {
                            tbLabel1FREQ.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.Freq1 != null)
                        {
                            tbFREQ1.Text = AirportCatalogEntity.Freq1.Trim();
                        }
                        else
                        {
                            tbFREQ1.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.MetroCD != null)
                        {
                            tbMetroCD.Text = AirportCatalogEntity.MetroCD.ToString().Trim();
                        }
                        else
                        {
                            tbMetroCD.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.Label2Freq != null)
                        {
                            tbLabel2FREQ.Text = AirportCatalogEntity.Label2Freq.Trim();
                        }
                        else
                        {
                            tbLabel2FREQ.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.Freq2 != null)
                        {
                            tbFREQ2.Text = AirportCatalogEntity.Freq2.Trim();
                        }
                        else
                        {
                            tbFREQ2.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.GeneralNotes != null)
                        {
                            tbNotes.Text = AirportCatalogEntity.GeneralNotes.Trim();
                        }
                        else
                        {
                            tbNotes.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.MetroID != null)
                        {
                            hdnMetroId.Value = AirportCatalogEntity.MetroID.ToString().Trim();
                        }
                        if (AirportCatalogEntity.DSTRegionID != null)
                        {
                            hdnDstID.Value = AirportCatalogEntity.DSTRegionID.ToString().Trim();
                        }
                        if (AirportCatalogEntity.CountryID != null)
                        {
                            hdnCountryId.Value = AirportCatalogEntity.CountryID.ToString().Trim();
                        }
                        if ((string.IsNullOrEmpty(AirportCatalogEntity.UWAID)) || (AirportCatalogEntity.UWAID.Trim() == "&nbsp;") || (AirportCatalogEntity.UWAID.Trim() == ""))
                        {
                            IsUwa = false;
                        }
                        else
                        {
                            IsUwa = true;
                        }
                        if (AirportCatalogEntity.Alerts != null)
                        {
                            tbAlerts.Text = AirportCatalogEntity.Alerts.Trim();
                        }
                        else
                        {
                            tbAlerts.Text = string.Empty;
                        }
                        if ((tbLatitudeDeg.Text != string.Empty) && (tbLatitudeNS.Text != string.Empty) && (tbLongitudeEW.Text != string.Empty) && (tbLongitudeDeg.Text != string.Empty))
                        {
                            tbWindZone.Text = AirportCatalogEntity.WindZone.ToString().Trim();
                        }
                        else
                        {
                            tbWindZone.Text = string.Empty;
                        }
                        if (AirportCatalogEntity.MetroID != null)
                        {
                            hdnMetroId.Value = AirportCatalogEntity.MetroID.ToString().Trim();
                        }
                        if (AirportCatalogEntity.DSTRegionID != null)
                        {
                            hdnDstID.Value = AirportCatalogEntity.DSTRegionID.ToString().Trim();
                        }
                        if (AirportCatalogEntity.CountryID != null)
                        {
                            hdnCountryId.Value = AirportCatalogEntity.CountryID.ToString().Trim();
                        }
                        if (AirportCatalogEntity.IsMilitary == true)
                        {
                            chkIsMilitary.Checked = true;
                        }
                        else
                        {
                            chkIsMilitary.Checked = false;
                        }
                        if (AirportCatalogEntity.IsPrivate == true)
                        {
                            chkIsPrivate.Checked = true;
                        }
                        else
                        {
                            chkIsPrivate.Checked = false;
                        }
                        if (AirportCatalogEntity.IsPublic == true)
                        {
                            chkIsPublic.Checked = true;
                        }
                        else
                        {
                            chkIsPublic.Checked = false;
                        }
                        if (AirportCatalogEntity.ExchangeRateID != null && AirportCatalogEntity.ExchangeRateID.ToString().Trim() != string.Empty)
                        {
                            hdnExchangeRateID.Value = Convert.ToString(AirportCatalogEntity.ExchangeRateID);
                            if (hdnExchangeRateID.Value.ToString().Trim() != "")
                            {
                                CheckExchangeRate();
                            }
                            else
                            {
                                tbExchangeRate.Text = "";
                            }
                        }
                        else
                        {
                            tbExchangeRate.Text = "";
                        }

                        if (AirportCatalogEntity.CustomNotes != null && AirportCatalogEntity.CustomNotes.ToString().Trim() != string.Empty)
                        {
                            tbNegotiateTerm.Text = AirportCatalogEntity.CustomNotes;
                        }
                        else
                        {
                            tbNegotiateTerm.Text = "";
                        }
                        if (AirportCatalogEntity.CustomsSundayWorkHours != null && AirportCatalogEntity.CustomsSundayWorkHours.ToString().Trim() != string.Empty)
                        {
                            tbSundayWorkHours.Text = AirportCatalogEntity.CustomsSundayWorkHours;
                        }
                        else
                        {
                            tbSundayWorkHours.Text = "";
                        }
                        if (AirportCatalogEntity.CustomsTuesdayWorkHours != null && AirportCatalogEntity.CustomsTuesdayWorkHours.ToString().Trim() != string.Empty)
                        {
                            tbTuesdayWorkHours.Text = AirportCatalogEntity.CustomsTuesdayWorkHours;
                        }
                        else
                        {
                            tbTuesdayWorkHours.Text = "";
                        }
                        if (AirportCatalogEntity.CustomsWednesdayWorkHours != null && AirportCatalogEntity.CustomsWednesdayWorkHours.ToString().Trim() != string.Empty)
                        {
                            tbWednesdayWorkHours.Text = AirportCatalogEntity.CustomsWednesdayWorkHours;
                        }
                        else
                        {
                            tbWednesdayWorkHours.Text = "";
                        }
                        if (AirportCatalogEntity.CustomsThursdayWorkHours != null && AirportCatalogEntity.CustomsThursdayWorkHours.ToString().Trim() != string.Empty)
                        {
                            tbThursdayWorkHours.Text = AirportCatalogEntity.CustomsThursdayWorkHours;
                        }
                        else
                        {
                            tbThursdayWorkHours.Text = "";
                        }
                        if (AirportCatalogEntity.CustomsFridayWorkHours != null && AirportCatalogEntity.CustomsFridayWorkHours.ToString().Trim() != string.Empty)
                        {
                            tbFridayWorkHours.Text = AirportCatalogEntity.CustomsFridayWorkHours;
                        }
                        else
                        {
                            tbFridayWorkHours.Text = "";
                        }
                        if (AirportCatalogEntity.CustomsSaturdayWorkHours != null && AirportCatalogEntity.CustomsSaturdayWorkHours.ToString().Trim() != string.Empty)
                        {
                            tbSaturdayWorkHours.Text = AirportCatalogEntity.CustomsSaturdayWorkHours;
                        }
                        else
                        {
                            tbSaturdayWorkHours.Text = "";
                        }
                        GetFBOFields(ref AirportCatalogEntity);
                        if (IsUwa)
                        {
                            pnlbarrunways.Visible = true;
                            dgRunways.Rebind();
                            btnRunways.Visible = true;
                        }
                        else
                        {
                            pnlbarrunways.Visible = false;
                            btnRunways.Visible = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// To select item based on Airport Id
        /// </summary>
        private void SelectedItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["SelectedAirportID"] != null)
                {
                    string ID = Session["SelectedAirportID"].ToString();
                    foreach (GridDataItem Item in dgAirportCatalog.MasterTableView.Items)
                    {
                        if (Item["AirportID"].Text.Trim() == ID.Trim())
                        {
                            Item.Selected = true;
                            break;
                        }
                    }
                }
                else
                {
                    DefaultSelectGrid(false);
                }
            }
        }
        /// <summary>
        /// To get Fbo values based on airport id
        /// </summary>
        /// <param name="AirportCatalogEntity"></param>
        private void GetFBOFields(ref GetAirportByAirportIDFBO AirportCatalogEntity)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                StringBuilder FBOInfo = new StringBuilder();
                string strNextLine = "\r\n";
                if (AirportCatalogEntity.FBOName != null)
                {
                    FBOInfo.Append("Name: " + AirportCatalogEntity.FBOName + strNextLine);
                }
                if (AirportCatalogEntity.FBOPhoneNum1 != null)
                {
                    FBOInfo.Append("Phone: " + AirportCatalogEntity.FBOPhoneNum1 + strNextLine);
                }
                if (AirportCatalogEntity.FBOFaxNum != null)
                {
                    FBOInfo.Append("Fax: " + AirportCatalogEntity.FBOFaxNum + strNextLine);
                }
                if (AirportCatalogEntity.FBOContact != null)
                {
                    FBOInfo.Append("Contact: " + AirportCatalogEntity.FBOContact + strNextLine);
                }
                if (AirportCatalogEntity.FBOFrequency != null)
                {
                    FBOInfo.Append("Freq: " + AirportCatalogEntity.FBOFrequency + strNextLine);
                }
                if (AirportCatalogEntity.FBOAddr1 != null)
                {
                    FBOInfo.Append("Addr.#1: " + AirportCatalogEntity.FBOAddr1 + strNextLine);
                }
                if (AirportCatalogEntity.FBOAddr2 != null)
                {
                    FBOInfo.Append("Addr.#2: " + AirportCatalogEntity.FBOAddr2 + strNextLine);
                }
                if (AirportCatalogEntity.FBOCityName != null)
                {
                    FBOInfo.Append("City: " + AirportCatalogEntity.FBOCityName + strNextLine);
                }
                if (AirportCatalogEntity.FBOStateName != null)
                {
                    FBOInfo.Append("State/Prov: " + AirportCatalogEntity.FBOStateName + strNextLine);
                }
                if (AirportCatalogEntity.FBOPostalZipCD != null)
                {
                    FBOInfo.Append("Postal Code: " + AirportCatalogEntity.FBOPostalZipCD + strNextLine);
                }
                if (AirportCatalogEntity.FBOIsCrewCar != null)
                {
                    FBOInfo.Append("Crew Car: " + AirportCatalogEntity.FBOIsCrewCar + strNextLine);
                }
                if (AirportCatalogEntity.FBOCreditCardUVAir != null)
                {
                    FBOInfo.Append("UVair: " + AirportCatalogEntity.FBOCreditCardUVAir + strNextLine);
                }
                if (AirportCatalogEntity.FBOFuelBrand != null)
                {
                    FBOInfo.Append("Fuel Brand: " + AirportCatalogEntity.FBOFuelBrand + strNextLine);
                }
                if (AirportCatalogEntity.FBOLastFuelDT != null)
                {
                    FBOInfo.Append("Last Purchase Date: " + AirportCatalogEntity.FBOLastFuelDT + strNextLine);
                }
                if (AirportCatalogEntity.FBOLastFuelPrice != null)
                {
                    FBOInfo.Append("Last Fuel Price: " + AirportCatalogEntity.FBOLastFuelPrice + strNextLine);
                }
                if (AirportCatalogEntity.FBONegotiatedFuelPrice != null)
                {
                    FBOInfo.Append("Negotiated Price: " + AirportCatalogEntity.FBONegotiatedFuelPrice + strNextLine);
                }
                tbFBOofchoice.Text = FBOInfo.ToString();
            }
        }
        /// <summary>
        /// Method to check unique Country  Code 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ExchangeRate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckExchangeRateExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }


        protected void tbRunway_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgAirportCatalog.Rebind();
                        DefaultSelectGrid(true);
                        if (dgAirportCatalog.Items.Count == 0)
                        {
                            ClearForm();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }


        //protected void tbCountryOnly_TextChanged(object sender, EventArgs e)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                if (CheckCountryExist())
        //                {
        //                    dgAirportCatalog.Rebind();
        //                    DefaultSelectGrid(true);
        //                    if (dgAirportCatalog.Items.Count == 0)
        //                    {
        //                        ClearForm();
        //                    }
        //                }
        //            }, FlightPak.Common.Constants.Policy.UILayer);
        //        }
        //        catch (Exception ex)
        //        {
        //            //The exception will be handled, logged and replaced by our custom exception. 
        //            ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
        //        }
        //    }
        //}

        private void CheckExchangeRate()
        {
            using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var CountryValue = CountryService.GetExchangeRate().EntityList.Where(x => x.ExchangeRateID.ToString().Trim().ToUpper() == (hdnExchangeRateID.Value.ToString().Trim().ToUpper())).ToList();
                if (CountryValue.Count() > 0 && CountryValue != null)
                {
                    tbExchangeRate.Text = ((FlightPakMasterService.ExchangeRate)CountryValue[0]).ExchRateCD.ToString().Trim();
                    //RadAjaxManager.GetCurrent(Page).FocusControl(tbSaturdayWorkHours);
                    // tbPostal.Focus();
                }
                else
                {
                    tbExchangeRate.Text = "";
                }
            }

        }
        /// <summary>
        ///  Method to check unique Country  Code 
        /// </summary>
        private void CheckExchangeRateExist()
        {
            if (!string.IsNullOrEmpty(tbExchangeRate.Text))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var CountryValue = CountryService.GetExchangeRate().EntityList.Where(x => x.ExchRateCD.Trim().ToUpper() == (tbExchangeRate.Text.Trim().ToUpper())).ToList();
                    if (CountryValue.Count() > 0 && CountryValue != null)
                    {
                        hdnExchangeRateID.Value = ((FlightPakMasterService.ExchangeRate)CountryValue[0]).ExchangeRateID.ToString().Trim();
                        tbExchangeRate.Text = ((FlightPakMasterService.ExchangeRate)CountryValue[0]).ExchRateCD;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbSaturdayWorkHours);
                        // tbPostal.Focus();
                    }
                    else
                    {
                        cvExchangeRate.IsValid = false;
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbExchangeRate);
                        //tbCountry.Focus();
                        IsValidateCustom = false;
                    }
                }
            }
        }
        private void CheckIsWidget(string LoadType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(LoadType))
            {
                if (Request.QueryString["IsWidget"] != null)
                {
                    if (Request.QueryString["IsWidget"].ToString() == "1")
                    {
                        Session["IsWidget"] = "1";
                        CheckAutorization(Permission.Utilities.ViewAirport);
                    }
                    else
                    {
                        Session["IsWidget"] = "0";
                    }
                }
                if (Session["IsWidget"] != null)
                {
                    if (Session["IsWidget"].ToString() == "1")
                    {
                        if (LoadType == "PreInit")
                        {
                            this.MasterPageFile = ResolveClientUrl("~/Framework/Masters/Popup.Master");
                        }
                        if (LoadType == "PageLoad")
                        {
                            lbtnReports.Visible = true;
                            lbtnSaveReports.Visible = true;
                            //lbtnSaveReports.Visible = false;
                        }
                    }
                }
            }
        }
        #endregion
        #region "Events"
        /// <summary>
        /// To check unique ICAO Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbICAO_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool CheckUWARecord = true;
                        if (Session["SelectedAirportID"] != null)
                        {
                            string Code = Session["SelectedAirportID"].ToString().Trim();
                            string CustomerID = "";
                            foreach (GridDataItem Item in dgAirportCatalog.MasterTableView.Items)
                            {
                                if (Item["AirportID"].Text.Trim() == Code)
                                {
                                    CustomerID = Item["CustomerID"].Text.Trim();
                                    break;
                                }
                            }
                            if (tbICAO.Text != null)
                            {
                                //To check for unique Icao code
                                using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var CustomerValue = FPKMasterService.GetAirportByAirportICaoID(tbICAO.Text).EntityList.Where(x => x.CustomerID.ToString().ToUpper().Equals(CustomerID.ToString().ToUpper()) && ((x.UWAID == "") || (x.UWAID == null) || (x.UWAID == "&nbsp;")));
                                    if (CustomerValue.Count() > 0 && CustomerValue != null)
                                    {
                                        //Enable custom validator
                                        cvICAO.IsValid = false;
                                        CheckUWARecord = false;
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbIATA);
                                        //tbIATA.Focus();
                                    }
                                    if (CheckUWARecord)
                                    {
                                        // To check whether uwa record exist or not
                                        var UWAValue = FPKMasterService.GetAirportByAirportICaoID(tbICAO.Text).EntityList.Where(x => ((x.UWAID != "") || (x.UWAID != null) || (x.UWAID != "&nbsp;")));
                                        if (UWAValue.Count() > 0 && UWAValue != null)
                                        {
                                            RadWindowManager1.RadConfirm("UWA record found, do you want to create unique ICAO??", "confirmCallBackFn", 500, 30, null, "Confirm");
                                        }
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// To check unique Country Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbCountry_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAllReadyCountryExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// To check unique Region Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbRegion_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAllReadyRegionExist(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// To check unique Metro Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbMetroCD_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAllReadyMetroExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAirportCatalog_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton lbtnInitEdit = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInitEdit, divExternalForm, RadAjaxLoadingPanel1);
                            // To find the insert button in the grid
                            LinkButton lbtnInitInsert = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInitInsert, divExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// Bind Airport Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAirportCatalog_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindAirportCatalogGrid(chkDisplayInctive.Checked);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }

        private void BindAirportCatalogGrid(bool DisplayInactive)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                bool IsDisplayInctive = DisplayInactive;
                if (IsPopUp)
                    IsDisplayInctive = true;

                TelerikPaging telerikPaging = TelerikGridHelper.GetPaging(dgAirportCatalog.CurrentPageIndex, dgAirportCatalog.PageSize);
                string strSortOrder = TelerikGridHelper.GetSortExpressionString(dgAirportCatalog.MasterTableView.SortExpressions);

                decimal MinimumRunWay = 0;

                if (!string.IsNullOrEmpty(tbRunway.Text))
                    MinimumRunWay = Convert.ToDecimal(tbRunway.Text);

                string IcaoID = dgAirportCatalog.MasterTableView.GetColumn("IcaoID").CurrentFilterValue;
                string Iata = dgAirportCatalog.MasterTableView.GetColumn("Iata").CurrentFilterValue;
                string CityName = dgAirportCatalog.MasterTableView.GetColumn("CityName").CurrentFilterValue;
                string StateName = dgAirportCatalog.MasterTableView.GetColumn("StateName").CurrentFilterValue;
                string CountryCD = dgAirportCatalog.MasterTableView.GetColumn("CountryCD").CurrentFilterValue;
                string CountryName = dgAirportCatalog.MasterTableView.GetColumn("CountryName").CurrentFilterValue;
                string AirportName = dgAirportCatalog.MasterTableView.GetColumn("AirportName").CurrentFilterValue;
                string OffsetToGMT = dgAirportCatalog.MasterTableView.GetColumn("OffsetToGMT").CurrentFilterValue;
                string Filter = dgAirportCatalog.MasterTableView.GetColumn("chkUWAID").CurrentFilterValue;

                var AirportValueResult = FPKMstService.GetAllAirportListForGridCustomPaging(
                    IsDisplayInctive, MinimumRunWay, telerikPaging.Start, telerikPaging.End, strSortOrder,
                    IcaoID, Iata, CityName, StateName, CountryCD, CountryName, AirportName, OffsetToGMT,
                    Filter);
                var AirportValue = AirportValueResult.EntityList;

                if (AirportValueResult.ReturnFlag)
                {
                    if (AirportValue != null && AirportValue.Count > 0)
                    {
                        var AirportTotal = AirportValue.FirstOrDefault();

                        if (AirportTotal != null)
                        {
                            dgAirportCatalog.AllowCustomPaging = true;
                            dgAirportCatalog.VirtualItemCount = Convert.ToInt32(AirportTotal.TotalCount);
                        }
                    }

                    dgAirportCatalog.DataSource = AirportValue;
                }
            }
        }

        /// <summary>
        /// Bind Runway Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgRunways_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(hdnAirportId.Value))
                        {
                            Int64 AirportId = Convert.ToInt64(hdnAirportId.Value.ToString().Trim());
                            using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var RunwayValue = FPKMasterService.GetRunwayByAirportID(AirportId);
                                if (RunwayValue.ReturnFlag == true)
                                {
                                    dgRunways.DataSource = RunwayValue.EntityList;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session[SearchSessionName] != null)
                {
                    SelectItem(SearchSessionName, true);
                }
                else if (Session["SelectedAirportID"] != null)
                {
                    Session["SelectedAirportRow"] = Session["SelectedAirportID"];
                    //dgAirportCatalog.Rebind();
                    if (Session["SelectedAirportRow"] != null)
                    {
                        SelectItem(SelectedItemSessionName, chkDisplayInctive.Checked);
                    }
                }
                if (Session["SelectedAirportID"] == null)
                {
                    Session["SelectedAirportID"] = UserPrincipal.Identity._airportId;
                    Session["SelectedAirportRow"] = Session["SelectedAirportID"];
                    SelectItem(SelectedItemSessionName, chkDisplayInctive.Checked);
                }
            }
        }

        private void SelectItem(string sessionName, bool isInactive)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                chkDisplayInctive.Checked = isInactive;
                Int64 PrimaryKeyValue = Convert.ToInt64(Session[sessionName]);
                var AirportValue = FPKMstService.GetAirportPositionAndCount(isInactive, PrimaryKeyValue, _selectLastModified);
                int FullItemIndex = 0;
                if (AirportValue != null && AirportValue.EntityList != null && AirportValue.EntityList.Count > 0)
                {
                    var Airport = AirportValue.EntityList.FirstOrDefault();
                    Session["SelectedAirportID"] = Airport.AirportId;
                    Session["SelectedAirportRow"] = Session["SelectedAirportID"];
                    FullItemIndex = (Convert.ToInt32(Airport.RecordNumber) - 1);
                }

                int PageSize = dgAirportCatalog.PageSize;
                int PageNumber = FullItemIndex/PageSize;
                int ItemIndex = FullItemIndex%PageSize;

                dgAirportCatalog.SelectedIndexes.Clear();
                dgAirportCatalog.CurrentPageIndex = PageNumber;

                dgAirportCatalog.SelectedIndexes.Add(new int[] {ItemIndex});
            }
        }

        /// <summary>
        /// To change the page index on paging        
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgAirportCatalog_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgAirportCatalog.ClientSettings.Scrolling.ScrollTop = "0";
                        AirportPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// Pre Render
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAirportCatalog_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectedItem();
                        }
                        else if (AirportPageNavigated)
                        {
                            SelectedItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgAirportCatalog, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// Bind Airport Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAirportCatalog_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                ClearForm();
                                e.Canceled = true;
                                e.Item.Selected = true;
                                chkDisplayInctive.Enabled = false;
                                if (Session["SelectedAirportID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.Airport, Convert.ToInt64(Session["SelectedAirportID"].ToString().Trim()));
                                        Session["IsAirportEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Airport);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Airport);
                                            SelectedItem();
                                            return;
                                        }
                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        SelectedItem();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgAirportCatalog.SelectedIndexes.Clear();
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbICAO);
                                //tbICAO.Focus();
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                chkDisplayInctive.Enabled = false;
                                pnlbarrunways.Visible = false;
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //if (column.CurrentFilterValue.ToUpper() == "CUSTOM")
                                //{
                                //    column.CurrentFilterValue = null;
                                //}
                                //else
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //}
                                ////column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// Rebind the popup values in the corresponding grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.Argument)
                        {
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// Update Command for Airport Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgAirportCatalog_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        IsUpdate = false;
                        CheckCustomValidator();
                        CheckExchangeRateExist();
                        //checking for custom validator
                        if (IsValidateCustom)
                        {
                            if (Session["SelectedAirportID"] != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var ReturnValue = FPKMasterService.UpdateAirport(GetItems());
                                    if (ReturnValue.ReturnFlag == true)
                                    {
                                        ///////Update Method UnLock
                                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                        {
                                            var returnValue = CommonService.UnLock(EntitySet.Database.Airport, Convert.ToInt64(Session["SelectedAirportID"].ToString().Trim()));
                                            e.Item.OwnerTableView.Rebind();
                                            e.Item.Selected = true;
                                            GridEnable(true, true, true);
                                            dgAirportCatalog.Rebind();
                                            //Session.Remove("SelectedAirportID");
                                            DefaultSelectGrid(true);
                                        }

                                        ShowSuccessMessage();

                                        hdnDate.Value = string.Empty;
                                        hdnEndTime.Value = string.Empty;
                                        hdnFinishedDate.Value = string.Empty;
                                        hdnStartDate.Value = string.Empty;
                                        hdnStartTime.Value = string.Empty;
                                        hdnUTCDate.Value = string.Empty;
                                        hdnDstEndDate.Value = string.Empty;
                                        hdnDstStartDate.Value = string.Empty;
                                        _selectLastModified = true;
                                    }
                                    else
                                    {
                                        //For Data Anotation
                                        ProcessErrorMessage(ReturnValue.ErrorMessage, ModuleNameConstants.Database.Airport);
                                    }
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            //Clear session & close browser
                            Session.Remove("SelectedAirportID");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.remove_beforeClose(onClientClose); oWnd.close();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// Update Command for Airport  Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgAirportCatalog_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        CheckCustomValidator();
                        CheckExchangeRateExist();
                        if (IsValidateCustom)
                        {
                            using (MasterCatalogServiceClient AirportCatalogService = new MasterCatalogServiceClient())
                            {
                                var AirportValue = AirportCatalogService.AddAirport(GetItems());
                                if (AirportValue.ReturnFlag == true)
                                {
                                    dgAirportCatalog.Rebind();
                                    Session.Remove("SelectedAirportID");
                                    DefaultSelectGrid(true);

                                    ShowSuccessMessage();

                                    GridEnable(true, true, true);
                                    hdnDate.Value = string.Empty;
                                    hdnEndTime.Value = string.Empty;
                                    hdnFinishedDate.Value = string.Empty;
                                    hdnStartDate.Value = string.Empty;
                                    hdnStartTime.Value = string.Empty;
                                    hdnUTCDate.Value = string.Empty;
                                    hdnDstEndDate.Value = string.Empty;
                                    hdnDstStartDate.Value = string.Empty;
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(AirportValue.ErrorMessage, ModuleNameConstants.Database.Airport);
                                }
                            }
                        }
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.remove_beforeClose(onClientClose); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radAirportPopup');", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgAirportCatalog_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPakMasterService.Airport AirportData = new FlightPakMasterService.Airport();
                        if (Session["SelectedAirportID"] != null)
                        {
                            string Code = Session["SelectedAirportID"].ToString().Trim();
                            string CustomerID = "", IcaoID = "", UWAID = "";
                            foreach (GridDataItem Item in dgAirportCatalog.MasterTableView.Items)
                            {
                                if (Item["AirportID"].Text.Trim() == Code)
                                {
                                    if (Item.GetDataKeyValue("CustomerID") != null)
                                    {
                                        CustomerID = Item.GetDataKeyValue("CustomerID").ToString().Trim();
                                    }
                                    if (Item.GetDataKeyValue("IcaoID") != null)
                                    {
                                        IcaoID = Item.GetDataKeyValue("IcaoID").ToString().Trim();
                                    }
                                    if (Item.GetDataKeyValue("UWAID") != null)
                                    {
                                        UWAID = Item.GetDataKeyValue("UWAID").ToString().Trim();
                                    }
                                    break;
                                }
                            }
                            AirportData.IcaoID = IcaoID;
                            AirportData.IsDeleted = true;
                            if (!string.IsNullOrEmpty(UWAID))
                            {
                                AirportData.UWAID = UWAID;
                            }
                            AirportData.AirportID = Convert.ToInt64(Session["SelectedAirportID"].ToString().Trim());
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.Lock(EntitySet.Database.Airport, Convert.ToInt64(Session["SelectedAirportID"].ToString().Trim()));
                                if (!returnValue.ReturnFlag)
                                {
                                    e.Item.Selected = true;
                                    DefaultSelectGrid(false);
                                    if (IsPopUp)
                                        ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Airport);
                                    else
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Airport);
                                    return;
                                }
                                using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FPKMasterService.DeleteAirport(AirportData);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    dgAirportCatalog.Rebind();
                                    if (dgAirportCatalog.MasterTableView.Items.Count > 0)
                                    {
                                        dgAirportCatalog.SelectedIndexes.Add(0);
                                        Session["SelectedAirportID"] = dgAirportCatalog.Items[0].GetDataKeyValue("AirportID").ToString();
                                        ReadOnlyForm();
                                    }
                                    else
                                    {
                                        ClearForm();
                                        EnableForm(false);
                                    }
                                    GridEnable(true, true, true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessageNew(ex, ModuleNameConstants.Database.Airport);
                }
                finally
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.Airport, Convert.ToInt64(Session["SelectedAirportID"].ToString().Trim()));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAirportCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectedItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgAirportCatalog.SelectedItems[0] as GridDataItem;
                                Session["SelectedAirportID"] = item.GetDataKeyValue("AirportID").ToString().Trim();
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                                Session["PageIndex"] = dgAirportCatalog.CurrentPageIndex;
                                Session["ItemIndex"] = item.ItemIndex;
                                hdnAirportId.Value = item.GetDataKeyValue("AirportID").ToString().Trim();
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                    }
                }
            }
        }
        /// <summary>
        /// To display UWA record in blue color in filter column grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAirportCatalog_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            string Color = System.Configuration.ConfigurationManager.AppSettings["UWAColor"];
                            GridDataItem DataItem = e.Item as GridDataItem;
                            GridColumn Column = dgAirportCatalog.MasterTableView.GetColumn("chkUWAID");
                            string UwaValue = DataItem["chkUWAID"].Text.Trim();
                            GridDataItem Item = (GridDataItem)e.Item;
                            TableCell cell = (TableCell)Item["chkUWAID"];
                            if (UwaValue.ToUpper().Trim() == "UWA")
                            {
                                System.Drawing.ColorConverter ColourConvert = new ColorConverter();
                                cell.ForeColor = (System.Drawing.Color)ColourConvert.ConvertFromString(Color);
                                cell.Font.Bold = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// For Inserting the values in db
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            bool IsValidDateCheck = true;
                            string StartTime = rmtbStartTime.Text.Trim();
                            string StartHrs = StartTime.Substring(0, 2);
                            string StartMts = StartTime.Substring(2, 2);
                            string StartSec = "00";
                            string FinishTime = rmtbFinishTime.Text.Trim();
                            string FinishHrs = FinishTime.Substring(0, 2);
                            string FinishMts = FinishTime.Substring(2, 2);
                            string FinishSec = "00";
                            hdnStartDate.Value = (((TextBox)ucStartDate.FindControl("tbDate")).Text);
                            hdnFinishedDate.Value = (((TextBox)ucFinishedDate.FindControl("tbDate")).Text);
                            hdnUTCDate.Value = System.DateTime.Now.ToString();
                            hdnStartTime.Value = string.Format("{0}:{1}:{2}", StartHrs, StartMts, StartSec, CultureInfo.InvariantCulture);
                            hdnEndTime.Value = string.Format("{0}:{1}:{2}", FinishHrs, FinishMts, FinishSec, CultureInfo.InvariantCulture);
                            StartDateTime = string.Format("{0} {1}", hdnStartDate.Value, hdnStartTime.Value, CultureInfo.InvariantCulture);
                            EndDateTime = string.Format("{0} {1}", hdnFinishedDate.Value, hdnEndTime.Value, CultureInfo.InvariantCulture);
                            DateTime DateStart = new DateTime();
                            DateTime DateEnd = new DateTime();
                            //if ((string.IsNullOrEmpty(tbICAO.Text)) && (string.IsNullOrEmpty(tbIATA.Text)))
                            //{
                            //    string alertMsg = "radalert('Enter ICAO Code or IATA Code', 360, 50, 'Airport Catalog');";
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            //    IsValidDateCheck = false;
                            //}
                            if ((!string.IsNullOrEmpty(DateFormat)) && (!string.IsNullOrEmpty(StartDateTime)) && (!string.IsNullOrEmpty(EndDateTime)) && (!string.IsNullOrEmpty(hdnStartDate.Value)) && (!string.IsNullOrEmpty(EndDateTime)))
                            {
                                DateStart = FormatDateTime(StartDateTime.Trim(), DateFormat);
                                DateEnd = FormatDateTime(EndDateTime.Trim(), DateFormat);
                            }
                            if ((IsValidDateCheck) && (!string.IsNullOrEmpty(StartDateTime)) && (StartDateTime != "") && (EndDateTime != "") && (!string.IsNullOrEmpty(EndDateTime)) && (!string.IsNullOrEmpty(hdnStartDate.Value)) && (!string.IsNullOrEmpty(hdnFinishedDate.Value)))
                            {
                                if ((DateStart) > (DateEnd))
                                {
                                    string AlertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                        + " oManager.radalert('Start Date and Time should be lesser than End Date and Time', 360, 50, 'Airport Catalog');";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                                    IsValidDateCheck = false;
                                }
                            }

                            if (IsValidDateCheck)
                            {
                                if (hdnSave.Value == "Update")
                                {
                                    (dgAirportCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                                }
                                else
                                {
                                    (dgAirportCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// Cancel Airport Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedAirportID"] != null)
                        {
                            // Unlock the Record
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Database.Airport, Convert.ToInt64(Session["SelectedAirportID"].ToString().Trim()));
                                //Session.Remove("SelectedAirportID");
                            }
                        }
                        DefaultSelectGrid(true);
                        
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.remove_beforeClose(onClientClose); oWnd.close(); ", true);
                            
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgAirportCatalog;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// To assign values of start and end date based on region cd
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDstTime_OnClick(object sender, EventArgs e)
        {
            //string Dates = hdnDstStartDate.Value.ToString().Substring(0, 11).Trim();
            TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
            if (!string.IsNullOrEmpty(hdnDstStartDate.Value))
            {
                if (!string.IsNullOrEmpty(DateFormat))
                    tbStartDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(hdnDstStartDate.Value.Trim())).Substring(0, 11); //hdnDstStartDate.Value.ToString().Substring(0, 11); //FormatDate(hdnDstStartDate.ToString().Substring(0, 11), DateFormat).ToString();
                else
                    tbStartDate.Text = hdnDstStartDate.Value.ToString().Substring(0, 11).Trim();

            }
            else
            {
                tbStartDate.Text = string.Empty;

            }
            TextBox tbEndDate = (TextBox)ucFinishedDate.FindControl("tbDate");
            if (!string.IsNullOrEmpty(hdnDstEndDate.Value))
            {
                if (!string.IsNullOrEmpty(DateFormat))
                    tbEndDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(hdnDstEndDate.Value.Trim())).Substring(0, 11); //FormatDate(hdnDstEndDate.Value.ToString().Substring(0, 11).Trim(), DateFormat).ToString();//hdnDstEndDate.Value.ToString().Substring(0, 11); 
                else
                    tbEndDate.Text = hdnDstEndDate.Value.ToString().Substring(0, 11);
            }
            else
            {
                tbEndDate.Text = string.Empty;
            }
        }
        /// <summary>
        /// To display inactive records on checkbox change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkDisplayInctive_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (btnSaveChanges.Visible == false)
                        {
                            IsDisplayInctive = chkDisplayInctive.Checked;
                            BindAirportCatalogGrid(IsDisplayInctive);
                            dgAirportCatalog.CurrentPageIndex = 0;
                            dgAirportCatalog.Rebind();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        #endregion
        #region "Reports"
        /// <summary>
        /// To show reports
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";IcaoID=" + tbICAO.Text;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        #endregion

        private void Page_Unload(object sender, System.EventArgs e)
        {
            ////if (!IsViewOnly)
            ////{
            //    string stTime = rmtbStartTime.Text;
            //    string endTime = rmtbFinishTime.Text;
            ////}
        }
        //private void Page_Init(object sender, System.EventArgs e)
        //{

        //    if (!string.IsNullOrEmpty(Request.QueryString["IcaoID"]))
        //    {
        //        IcaoID = Request.QueryString["IcaoID"].ToString().ToUpper().Trim();


        //        FlightPakMasterService.MasterCatalogServiceClient AirportService = new FlightPakMasterService.MasterCatalogServiceClient();

        //        var ReturnValue = AirportService.GetAllAirportListForGrid(false);

        //        if (ReturnValue.ReturnFlag == true)
        //        {
        //            int iIndex = 0;
        //            int FullItemIndex = 0;
        //            foreach (FlightPakMasterService.GetAllAirportListForGrid Item in ReturnValue.EntityList)
        //            {
        //                if (IcaoID.ToUpper() == Item.IcaoID.Trim().ToUpper())
        //                {
        //                    FullItemIndex = iIndex;
        //                    break;
        //                }
        //                iIndex++;
        //            }

        //            int PageSize = dgAirport.PageSize;
        //            int PageNumber = FullItemIndex / PageSize;
        //            int ItemIndex = FullItemIndex % PageSize;

        //            dgAirport.CurrentPageIndex = PageNumber;

        //            int[] iItemIndexes;
        //            iItemIndexes = new int[1];
        //            iItemIndexes[0] = ItemIndex;
        //            dgAirport.SelectedIndexes.Add(iItemIndexes);

        //        }
        //    }
        //}

        /// <summary>
        /// Method to check unique Country  Code 
        /// </summary>
        /// <returns></returns>
        //private bool CheckCountryExist()
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
        //    {
        //        bool ReturnValue = false;
        //        if ((tbCountryOnly.Text != null) && (tbCountryOnly.Text != string.Empty))
        //        {
        //            //To check for unique Country code
        //            using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
        //            {
        //                var CountryValue = FPKMasterService.GetCountryMasterList().EntityList.Where(x => x.CountryCD != null && x.CountryCD.ToString().ToUpper().Trim().Equals(tbCountryOnly.Text.ToString().ToUpper().Trim())).ToList();
        //                if (CountryValue.Count() > 0 && CountryValue != null)
        //                {
        //                   //if (!string.IsNullOrEmpty(((FlightPakMasterService.Country)CountryValue[0]).CountryName))
        //                      //  tbCountryDesc.Text = ((FlightPakMasterService.Country)CountryValue[0]).CountryName.ToUpper().Trim();
        //                    //hdnCountryId.Value = ((FlightPakMasterService.Country)CountryValue[0]).CountryID.ToString().ToUpper().Trim();
        //                   // RadAjaxManager.GetCurrent(Page).FocusControl(tbApMgr);
        //                    //tbApMgr.Focus();
        //                    ReturnValue = true;
        //                }
        //                else
        //                {
        //                    cvCountryOnly.IsValid = false;
        //                    RadAjaxManager.GetCurrent(Page).FocusControl(tbCountryOnly);
        //                    //tbCountry.Focus();
        //                    ReturnValue = false;
        //                }
        //            }
        //        }
        //        return ReturnValue;
        //    }
        //}
    }
}
