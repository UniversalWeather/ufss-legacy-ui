﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using Telerik.Web.UI;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class DaylightSavingTimeRegion : BaseSecuredPage
    {
        private ExceptionManager exManager;
        private bool IsEmptyCheck = true;
        private bool DSTPageNavigated = false;
        private string strDstID = "";
        string DateFormat;
        private List<string> DSTRegionCode = new List<string>();
        private bool _selectLastModified = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        // Set Logged-in User Name
                        FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                        if (identity != null && identity.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            DateFormat = identity.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                        }
                        else
                        {
                            DateFormat = "MM/dd/yyyy";
                        }
                        ((RadDatePicker)ucDatePicker.FindControl("RadDatePicker1")).DateInput.DateFormat = DateFormat;
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgDaylightSavingTimeRegion, dgDaylightSavingTimeRegion, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgDaylightSavingTimeRegion.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewFuelLocator);
                            // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgDaylightSavingTimeRegion.Rebind();
                                ReadOnlyForm();                                
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }
        }
        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgDaylightSavingTimeRegion.Rebind();
                    }
                    if (dgDaylightSavingTimeRegion.MasterTableView.Items.Count > 0)
                    {
                        //if (!IsPostBack)
                        //{
                        //    Session["SelectedDSTRegionID"] = null;
                        //}
                        if (Session["SelectedDSTRegionID"] == null)
                        {
                            dgDaylightSavingTimeRegion.SelectedIndexes.Add(0);
                            Session["SelectedDSTRegionID"] = dgDaylightSavingTimeRegion.Items[0].GetDataKeyValue("DSTRegionID").ToString();
                        }

                        if (dgDaylightSavingTimeRegion.SelectedIndexes.Count == 0)
                            dgDaylightSavingTimeRegion.SelectedIndexes.Add(0);

                        ReadOnlyForm();
                    }
                    else
                    {
                        ClearForm();
                        EnableForm(false);
                    }
                    //GridEnable(true, true, true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To change the page index on paging        
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDaylightSavingTimeRegion_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgDaylightSavingTimeRegion.ClientSettings.Scrolling.ScrollTop = "0";
                        DSTPageNavigated = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }
        }
        /// <summary>
        /// PreRender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDaylightSavingTimeRegion_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (DSTPageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgDaylightSavingTimeRegion, Page.Session);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }
        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(group))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    base.Validate(group);
                    // get the first validator that failed
                    var validator = GetValidators(group)
                    .OfType<BaseValidator>()
                    .FirstOrDefault(v => !v.IsValid);
                    // set the focus to the control
                    // that the validator targets
                    if (validator != null)
                    {
                        Control target = validator
                        .NamingContainer
                        .FindControl(validator.ControlToValidate);
                        if (target != null)
                        {
                            target.Focus();
                            IsEmptyCheck = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To highlight the selected item 
        /// </summary>        
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedDSTRegionID"] != null)
                    {
                        //GridDataItem items = (GridDataItem)Session["SelectedItem"];
                        //string code = items.GetDataKeyValue("DelayTypeCD").ToString();
                        string ID = Session["SelectedDSTRegionID"].ToString();
                        foreach (GridDataItem item in dgDaylightSavingTimeRegion.MasterTableView.Items)
                        {
                            if (item.GetDataKeyValue("DSTRegionID").ToString().Trim() == ID)
                            {
                                item.Selected = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Datagrid Item Created for DayLightSavingTimeRegion Insert and edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDaylightSavingTimeRegion_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }
        }
        /// <summary>
        /// Bind DayLightSavingTimeRegion Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDaylightSavingTimeRegion_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objDSTService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objDstVal = objDSTService.GetDSTRegionList();
                            if (objDstVal.ReturnFlag == true)
                            {
                                dgDaylightSavingTimeRegion.DataSource = objDstVal.EntityList;
                            }
                            Session["DstCD"] = objDstVal.EntityList.ToList();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }
        }
        /// <summary>
        /// Datagrid Item Command for DayLightSavingTimeRegion Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDaylightSavingTimeRegion_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                hdnRedirect.Value = "";
                                if (Session["SelectedDSTRegionID"] != null)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.DSTRegion, Convert.ToInt64(Session["SelectedDSTRegionID"].ToString().Trim()));
                                        Session["IsDstEditLock"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.DSTRegion);
                                            SelectItem();
                                            return;
                                        }
                                        DisplayEditForm();
                                        tbCode.ReadOnly = true;
                                        // GridEnable(false, true, false);
                                        tbDescription.Focus();
                                        SelectItem();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgDaylightSavingTimeRegion.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                // GridEnable(true, false, false);
                                dgDaylightSavingTimeRegion.Rebind();
                                break;
                            case "UpdateEdited":
                                dgDaylightSavingTimeRegion_UpdateCommand(sender, e);
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }
        }
        /// <summary>
        /// Update Command for updating the values of DayLightSavingTimeRegion  in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDaylightSavingTimeRegion_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedDSTRegionID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var RetVal = objDstService.UpdateDSTRegion(GetItems());
                                ///////Update Method UnLock
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.DSTRegion, Convert.ToInt64(Session["SelectedDSTRegionID"].ToString().Trim()));
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    //  GridEnable(true, true, true);
                                    DefaultSelection(true);
                                }
                                if (RetVal.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                    _selectLastModified = true;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }
        }
        /// <summary>
        /// DaylightSavingTimeRegion Insert Command for inserting value in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDaylightSavingTimeRegion_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (CheckAllReadyExist())
                        {
                            cvDstCode.IsValid = false;
                            tbCode.Focus();
                        }
                        else
                        {
                            using (MasterCatalogServiceClient objDSTRegionService = new MasterCatalogServiceClient())
                            {
                                var RetVal = objDSTRegionService.AddDSTRegion(GetItems());
                                dgDaylightSavingTimeRegion.Rebind();
                                DefaultSelection(false);

                                if (RetVal.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                    _selectLastModified = true;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }
        }
        /// <summary>
        /// Save and Update DayLightSavingTimeRegion Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsEmptyCheck)
                        {
                            bool IsValidDateCheck = true;
                            string StartDateTime;
                            string EndDateTime;
                            hdnStartDate.Value = (((TextBox)ucStartDate.FindControl("tbDate")).Text);
                            hdnFinishedDate.Value = (((TextBox)ucFinishedDate.FindControl("tbDate")).Text);
                            StartDateTime = rmtbStartTime.TextWithLiterals;
                            EndDateTime = rmtbFinishTime.TextWithLiterals;
                            hdnStartDate.Value = string.Format("{0} {1}", hdnStartDate.Value, StartDateTime, CultureInfo.InvariantCulture);
                            hdnFinishedDate.Value = string.Format("{0} {1}", hdnFinishedDate.Value, EndDateTime, CultureInfo.InvariantCulture);
                            if ((IsValidDateCheck) && (!string.IsNullOrEmpty(hdnStartDate.Value)) && (StartDateTime != "") && (EndDateTime != "") && (!string.IsNullOrEmpty(hdnFinishedDate.Value)))
                            {
                                DateTime DateSource = DateTime.MinValue;
                                IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                DateSource = DateTime.Parse((hdnStartDate.Value).ToString(yyyymmddFormat), System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal);

                                DateTime DateSource1 = DateTime.MinValue;
                                IFormatProvider yyyymmddFormat1 = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                DateSource1 = DateTime.Parse((hdnFinishedDate.Value).ToString(yyyymmddFormat), System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal);

                                // if ((Convert.ToDateTime(hdnStartDate.Value, CultureInfo.InvariantCulture)) > (Convert.ToDateTime(hdnFinishedDate.Value, CultureInfo.InvariantCulture)))
                                if (DateSource > DateSource1)
                                {
                                    
                                    string alertMsg = @"var oManager = $find('" + RadWindowManager1.ClientID + "');"
                                        + @" oManager.radalert('Start date  should be lesser than end date', 360, 50, 'DaylightSavingTimeRegion');";
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                    IsValidDateCheck = false;
                                }
                            }
                            if (IsValidDateCheck)
                            {
                                if (hdnSave.Value == "Update")
                                {
                                    (dgDaylightSavingTimeRegion.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                                }
                                else
                                {
                                    (dgDaylightSavingTimeRegion.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }
        }
        /// <summary>
        /// Cancel DayLightSavingTimeRegion  Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedDSTRegionID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.DSTRegion, Convert.ToInt64(Session["SelectedDSTRegionID"].ToString().Trim()));
                                }
                            }
                        }
                        DefaultSelection(false);
                        //Session.Remove("SelectedDSTRegionID");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }
        /// <summary>
        ///  DaylightSavingTimeRegion Delete Command for deleting value in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDaylightSavingTimeRegion_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["SelectedDSTRegionID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.DSTRegion DstType = new FlightPakMasterService.DSTRegion();
                                string Code = Session["SelectedDSTRegionID"].ToString();
                                strDstID = "";
                                foreach (GridDataItem Item in dgDaylightSavingTimeRegion.MasterTableView.Items)
                                {
                                    if (Item.GetDataKeyValue("DSTRegionID").ToString().Trim() == Code.Trim())
                                    {
                                        if (Item.GetDataKeyValue("DSTRegionCD") != null)
                                        {
                                            DstType.DSTRegionCD = Item.GetDataKeyValue("DSTRegionID").ToString().Trim();
                                        }
                                        if (Item.GetDataKeyValue("DSTRegionID") != null)
                                        {
                                            strDstID = Item.GetDataKeyValue("DSTRegionID").ToString().Trim();
                                        }
                                        break;
                                    }
                                }
                                DstType.DSTRegionID = Convert.ToInt64(strDstID);
                                DstType.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.DSTRegion, Convert.ToInt64(strDstID));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.DSTRegion);
                                        return;
                                    }
                                    objDstService.DeleteDSTRegion(DstType);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    DefaultSelection(true);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelLocator);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        //Unlock should happen from Service Layer
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.DSTRegion, Convert.ToInt64(strDstID));
                    }
                }
            }
        }
        /// <summary>
        /// Bind Selected Item in the Session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDaylightSavingTimeRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgDaylightSavingTimeRegion.SelectedItems[0] as GridDataItem;
                                if (item.GetDataKeyValue("DSTRegionID") != null)
                                {
                                    Session["SelectedDSTRegionID"] = item.GetDataKeyValue("DSTRegionID").ToString().Trim();
                                }
                                if (btnSaveChanges.Visible == false)
                                {
                                    ReadOnlyForm();
                                }
                                //GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                    }
                }
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgDaylightSavingTimeRegion;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion
       );
                }
            }
        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private DSTRegion GetItems()
        {
            FlightPakMasterService.DSTRegion DstServiceType = null;
            decimal checkforDecimal;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.DSTRegion objDSTRegion = new FlightPakMasterService.DSTRegion();
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    DstServiceType = new FlightPakMasterService.DSTRegion();
                    DstServiceType.DSTRegionCD = tbCode.Text;
                    DstServiceType.DSTRegionName = tbDescription.Text;
                    DstServiceType.CityList = tbCityList.Text.Trim();
                    if (hdnSave.Value == "Update")
                    {
                        //GridDataItem Item = (GridDataItem)dgMetroCity.SelectedItems[0];
                        if (Session["SelectedDSTRegionID"] != null)
                        {
                            DstServiceType.DSTRegionID = Convert.ToInt64(Session["SelectedDSTRegionID"].ToString());
                        }
                    }
                    else
                    {
                        DstServiceType.DSTRegionID = 0;
                    }
                    if (!string.IsNullOrEmpty(hdnStartDate.Value))
                    {
                        DstServiceType.StartDT = Convert.ToDateTime(hdnStartDate.Value);
                    }
                    if (!string.IsNullOrEmpty(hdnFinishedDate.Value))
                    {
                        DstServiceType.EndDT = Convert.ToDateTime(hdnFinishedDate.Value);
                    }
                    if (tbutcoffset.Text != null && tbutcoffset.Text != string.Empty)
                        DstServiceType.OffSet = decimal.TryParse(tbutcoffset.Text.Substring(0, tbutcoffset.Text.IndexOf(".")) + "." + tbutcoffset.Text.Substring(tbutcoffset.Text.IndexOf(".") + 1, 2), out checkforDecimal) ? checkforDecimal : 0;
                    DstServiceType.IsDeleted = false;
                    return DstServiceType;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return DstServiceType;
            }
        }
        protected void tbCode_textchange(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    CheckAllReadyExist();
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// Display Insert  form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.ReadOnly = false;
                    tbCode.BackColor = System.Drawing.Color.White;
                    hdnSave.Value = "Save";
                    ClearForm();
                    dgDaylightSavingTimeRegion.Rebind();
                    EnableForm(true);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To check whether the code is unique
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private bool CheckAllReadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnval = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    List<DSTRegion> DSTRegionList = new List<DSTRegion>();
                    DSTRegionList = ((List<DSTRegion>)Session["DstCD"]).Where(x => x.DSTRegionCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim())).ToList<DSTRegion>();
                    if (DSTRegionList.Count != 0)
                    {
                        returnval = true;
                        cvDstCode.IsValid = false;
                        tbCode.Focus();
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
                return returnval;

            }
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["SelectedDSTRegionID"] != null)
                    {
                        strDstID = "";
                        foreach (GridDataItem Item in dgDaylightSavingTimeRegion.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("DSTRegionID").ToString().Trim() == Session["SelectedDSTRegionID"].ToString().Trim())
                            {
                                if (Item.GetDataKeyValue("DSTRegionCD") != null)
                                {
                                    tbCode.Text = Item.GetDataKeyValue("DSTRegionCD").ToString().Trim();
                                }
                                if (Item.GetDataKeyValue("DSTRegionID") != null)
                                {
                                    strDstID = Item.GetDataKeyValue("DSTRegionID").ToString().Trim();
                                }
                                if (Item.GetDataKeyValue("DSTRegionName") != null)
                                {
                                    tbDescription.Text = Item.GetDataKeyValue("DSTRegionName").ToString().Trim();
                                }
                                else
                                {
                                    tbDescription.Text = string.Empty;
                                }
                                if ((Item.GetDataKeyValue("StartDT") != null) && (!string.IsNullOrEmpty(DateFormat)))
                                {
                                    string startDate = Item.GetDataKeyValue("StartDT").ToString();
                                    TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
                                    tbStartDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(startDate)).Substring(0, 11);// String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", startDate).Substring(0, 10);
                                    rmtbStartTime.Text = startDate.Substring(11, 2) + ":" + startDate.Substring(13, 2);
                                }
                                else
                                {
                                    TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
                                    tbStartDate.Text = string.Empty;
                                    rmtbStartTime.Text = string.Empty;
                                }
                                if ((Item.GetDataKeyValue("StartDT") != null) && (string.IsNullOrEmpty(DateFormat)))
                                {
                                    string startDate = Item.GetDataKeyValue("StartDT").ToString();
                                    TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
                                    tbStartDate.Text = Convert.ToDateTime(startDate).ToString().Substring(0, 11);// String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", startDate).Substring(0, 10);
                                    rmtbStartTime.Text = startDate.Substring(11, 2) + ":" + startDate.Substring(13, 2);
                                }
                                else
                                {
                                    TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
                                    tbStartDate.Text = string.Empty;
                                    rmtbStartTime.Text = string.Empty;
                                }
                                if ((Item.GetDataKeyValue("EndDT") != null) && (!string.IsNullOrEmpty(DateFormat)))
                                {
                                    string enddate = Item.GetDataKeyValue("EndDT").ToString();
                                    TextBox tbEndDate = (TextBox)ucFinishedDate.FindControl("tbDate");
                                    tbEndDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(enddate)).Substring(0, 11);//String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", enddate).Substring(0, 10);
                                    rmtbFinishTime.Text = enddate.Substring(11, 2) + ":" + enddate.Substring(13, 2);
                                }
                                else
                                {
                                    TextBox tbEndDate = (TextBox)ucFinishedDate.FindControl("tbDate");
                                    tbEndDate.Text = string.Empty;
                                    rmtbFinishTime.Text = string.Empty;
                                }
                                if ((Item.GetDataKeyValue("EndDT") != null) && (string.IsNullOrEmpty(DateFormat)))
                                {
                                    string enddate = Item.GetDataKeyValue("EndDT").ToString();
                                    TextBox tbEndDate = (TextBox)ucFinishedDate.FindControl("tbDate");
                                    tbEndDate.Text = Convert.ToDateTime(enddate).ToString().Substring(0, 11);//String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", enddate).Substring(0, 10);
                                    rmtbFinishTime.Text = enddate.Substring(11, 2) + ":" + enddate.Substring(13, 2);
                                }
                                else
                                {
                                    TextBox tbEndDate = (TextBox)ucFinishedDate.FindControl("tbDate");
                                    tbEndDate.Text = string.Empty;
                                    rmtbFinishTime.Text = string.Empty;
                                }
                                if (Item.GetDataKeyValue("OffSet") != null)
                                {
                                    tbutcoffset.Text = Item.GetDataKeyValue("OffSet").ToString();
                                }
                                else
                                    tbutcoffset.Text = "00.00";
                                if (Item.GetDataKeyValue("CityList") != null)
                                {
                                    tbCityList.Text = Item.GetDataKeyValue("CityList").ToString();
                                }
                                else
                                    tbCityList.Text = string.Empty;
                                break;
                            }
                        }
                        EnableForm(true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// to Enable the form based on condition
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableForm(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Enabled = enable;
                    tbDescription.Enabled = enable;
                    TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
                    tbStartDate.Enabled = enable;
                    rmtbStartTime.Enabled = enable;
                    TextBox tbFinishedDate = (TextBox)ucFinishedDate.FindControl("tbDate");
                    tbFinishedDate.Enabled = enable;
                    rmtbFinishTime.Enabled = enable;
                    tbCityList.Enabled = enable;
                    tbutcoffset.Enabled = enable;
                    btnCancel.Visible = enable;
                    btnSaveChanges.Visible = enable;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To clear the form
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbCode.Text = string.Empty;
                    tbDescription.Text = string.Empty;
                    tbCityList.Text = string.Empty;
                    (((TextBox)ucStartDate.FindControl("tbDate")).Text) = string.Empty;
                    rmtbStartTime.Text = "0000";
                    (((TextBox)ucFinishedDate.FindControl("tbDate")).Text) = string.Empty;
                    rmtbFinishTime.Text = "0000";
                    tbutcoffset.Text = "00.00";
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To display as read only
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    GridDataItem Item = dgDaylightSavingTimeRegion.SelectedItems[0] as GridDataItem;
                    tbCode.Text = Convert.ToString(Item.GetDataKeyValue("DSTRegionCD"));//.ToString();
                    if (Item.GetDataKeyValue("DSTRegionName") != null)
                    {
                        tbDescription.Text = Item.GetDataKeyValue("DSTRegionName").ToString();
                    }
                    else
                    {
                        tbDescription.Text = string.Empty;
                    }
                    //Start Date
                    if ((Item.GetDataKeyValue("StartDT") != null) && (!string.IsNullOrEmpty(DateFormat)))
                    {
                        string startDate = Item.GetDataKeyValue("StartDT").ToString();

                        TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
                        string[] startDateTime = startDate.Split(' ');

                        if (startDate.Contains("AM") || startDate.Contains("PM"))
                        {
                            //tbStartDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(startDate)).Substring(0, 11).Trim();// String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", startDate).Substring(0, 10);
                            tbStartDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(startDateTime[0].ToString())).Trim();

                            if (startDateTime[1].Contains("AM"))
                            {
                                if (startDateTime[2].Substring(0, 2) == "12")
                                {
                                    rmtbStartTime.TextWithLiterals = "00:" + startDateTime[2].Substring(3, 2);
                                }
                                else
                                {
                                    rmtbStartTime.TextWithLiterals = startDateTime[2].Substring(0, 2) + ":" + startDateTime[2].Substring(3, 2);
                                }
                            }
                            else if (startDateTime[1].Contains("PM"))
                            {
                                if (startDateTime[2].Substring(0, 2) == "12")
                                {
                                    rmtbStartTime.TextWithLiterals = "12:" + startDateTime[2].Substring(3, 2);
                                }
                                else
                                {
                                    rmtbStartTime.TextWithLiterals = Convert.ToString(Convert.ToInt32(startDateTime[2].Substring(0, 2)) + 12) + ":" + startDateTime[2].Substring(3, 2);
                                }
                            }
                            else if (startDateTime[2].Contains("AM"))
                            {
                                if (startDateTime[1].Substring(0, 2) == "12")
                                {
                                    rmtbStartTime.TextWithLiterals = "00:" + startDateTime[1].Substring(3, 2);
                                }
                                else
                                {
                                    rmtbStartTime.TextWithLiterals = startDateTime[1].Substring(0, 2) + ":" + startDateTime[1].Substring(3, 2);
                                }
                            }
                            else if (startDateTime[2].Contains("PM"))
                            {
                                if (startDateTime[1].Substring(0, 2) == "12")
                                {
                                    rmtbStartTime.TextWithLiterals = "12:" + startDateTime[1].Substring(3, 2);
                                }
                                else
                                {
                                    rmtbStartTime.TextWithLiterals = Convert.ToString(Convert.ToInt32(startDateTime[1].Substring(0, 2)) + 12) + ":" + startDateTime[1].Substring(3, 2);
                                }
                            }
                        }
                        else
                        {
                            tbStartDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(startDateTime[0].ToString())).Trim();
                            rmtbStartTime.TextWithLiterals = startDateTime[1].Substring(0, 2) + ":" + startDateTime[1].Substring(3, 2);
                        }
                    }
                    else
                    {
                        TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
                        tbStartDate.Text = string.Empty;
                        rmtbStartTime.Text = "00:00";
                    }

                    //End Date           
                    if ((Item.GetDataKeyValue("EndDT") != null) && (!string.IsNullOrEmpty(DateFormat)))
                    {
                        string endDate = Item.GetDataKeyValue("EndDT").ToString();

                        TextBox tbEndDate = (TextBox)ucFinishedDate.FindControl("tbDate");
                        string[] endDateTime = endDate.Split(' ');

                        if (endDate.Contains("AM") || endDate.Contains("PM"))
                        {
                            //tbEndDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(endDate)).Substring(0, 11).Trim();//String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", enddate).Substring(0, 10);                
                            tbEndDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(endDateTime[0].ToString())).Trim();

                            if (endDateTime[1].Contains("AM"))
                            {
                                if (endDateTime[2].Substring(0, 2) == "12")
                                {
                                    rmtbFinishTime.TextWithLiterals = "00:" + endDateTime[2].Substring(3, 2);
                                }
                                else
                                {
                                    rmtbFinishTime.TextWithLiterals = endDateTime[2].Substring(0, 2) + ":" + endDateTime[2].Substring(3, 2);
                                }
                            }
                            else if (endDateTime[1].Contains("PM"))
                            {
                                if (endDateTime[2].Substring(0, 2) == "12")
                                {
                                    rmtbFinishTime.TextWithLiterals = "12:" + endDateTime[2].Substring(3, 2);
                                }
                                else
                                {
                                    rmtbFinishTime.TextWithLiterals = Convert.ToString(Convert.ToInt32(endDateTime[2].Substring(0, 2)) + 12) + ":" + endDateTime[2].Substring(3, 2);
                                }
                            }
                            else if (endDateTime[2].Contains("AM"))
                            {
                                if (endDateTime[1].Substring(0, 2) == "12")
                                {
                                    rmtbFinishTime.TextWithLiterals = "00:" + endDateTime[1].Substring(3, 2);
                                }
                                else
                                {
                                    rmtbFinishTime.TextWithLiterals = endDateTime[1].Substring(0, 2) + ":" + endDateTime[1].Substring(3, 2);
                                }
                            }
                            else if (endDateTime[2].Contains("PM"))
                            {
                                if (endDateTime[1].Substring(0, 2) == "12")
                                {
                                    rmtbFinishTime.TextWithLiterals = "12:" + endDateTime[1].Substring(3, 2);
                                }
                                else
                                {
                                    rmtbFinishTime.TextWithLiterals = Convert.ToString(Convert.ToInt32(endDateTime[1].Substring(0, 2)) + 12) + ":" + endDateTime[1].Substring(3, 2);
                                }
                            }
                        }
                        else
                        {
                            tbEndDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(endDateTime[0].ToString())).Trim();
                            rmtbFinishTime.TextWithLiterals = endDateTime[1].Substring(0, 2) + ":" + endDateTime[1].Substring(3, 2);
                        }
                    }
                    else
                    {
                        TextBox tbEndDate = (TextBox)ucFinishedDate.FindControl("tbDate");
                        tbEndDate.Text = string.Empty;
                        rmtbFinishTime.Text = "00:00";
                    }
          
                    if (Item.GetDataKeyValue("OffSet") != null)
                    {
                        tbutcoffset.Text = Item.GetDataKeyValue("OffSet").ToString();
                    }
                    else
                        tbutcoffset.Text = "00.00";
                    if (Item.GetDataKeyValue("CityList") != null)
                    {
                        tbCityList.Text = Item.GetDataKeyValue("CityList").ToString();
                    }
                    else
                        tbCityList.Text = string.Empty;
                    Label lbLastUpdatedUser;
                    lbLastUpdatedUser = (Label)dgDaylightSavingTimeRegion.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                    if (Item.GetDataKeyValue("LastUpdUID") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Convert.ToString(Item.GetDataKeyValue("LastUpdUID")));//.ToString();
                    }
                    else
                    {
                        lbLastUpdatedUser.Text = string.Empty;
                    }
                    if (Item.GetDataKeyValue("LastUpdTS") != null)
                    {
                        //To retrieve back the UTC date in Homebase format
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString())));
                    }
                    lbColumnName1.Text = "DST Region Code";
                    lbColumnName2.Text = "Description";
                    lbColumnValue1.Text = Item["DSTRegionCD"].Text;
                    lbColumnValue2.Text = Item["DSTRegionName"].Text;
                    EnableForm(false);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void GridEnable(bool add, bool edit, bool delete)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
        //    {
        //        //Handle methods throguh exception manager with return flag
        //        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //        exManager.Process(() =>
        //        {
        //            LinkButton lbtninsertCtl, lbtndelCtl, lbtneditCtl;
        //            lbtninsertCtl = (LinkButton)dgDaylightSavingTimeRegion.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
        //            lbtndelCtl = (LinkButton)dgDaylightSavingTimeRegion.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
        //            lbtneditCtl = (LinkButton)dgDaylightSavingTimeRegion.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
        //            if (IsAuthorized(Permission.Database.AddDaylightSavingTimeRegion))
        //            {
        //                lbtninsertCtl.Visible = true;
        //                if (add)
        //                {
        //                    lbtninsertCtl.Enabled = true;
        //                }
        //                else
        //                {
        //                    lbtninsertCtl.Enabled = false;
        //                }
        //            }
        //            else
        //            {
        //                lbtninsertCtl.Visible = false;
        //            }
        //            if (IsAuthorized(Permission.Database.DeleteDaylightSavingTimeRegion))
        //            {
        //                lbtndelCtl.Visible = true;
        //                if (delete)
        //                {
        //                    lbtndelCtl.Enabled = true;
        //                    lbtndelCtl.OnClientClick = "javascript:return ProcessDelete();";
        //                }
        //                else
        //                {
        //                    lbtndelCtl.Enabled = false;
        //                    lbtndelCtl.OnClientClick = string.Empty;
        //                }
        //            }
        //            else
        //            {
        //                lbtndelCtl.Visible = false;
        //            }
        //            if (IsAuthorized(Permission.Database.EditDaylightSavingTimeRegion))
        //            {
        //                lbtneditCtl.Visible = true;
        //                if (edit)
        //                {
        //                    lbtneditCtl.Enabled = true;
        //                    lbtneditCtl.OnClientClick = "javascript:return ProcessUpdate();";
        //                }
        //                else
        //                {
        //                    lbtneditCtl.Enabled = false;
        //                    lbtneditCtl.OnClientClick = string.Empty;
        //                }
        //            }
        //            else
        //            {
        //                lbtneditCtl.Visible = false;
        //            }
        //        }, FlightPak.Common.Constants.Policy.UILayer);
        //    }
        //}

        
        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgDaylightSavingTimeRegion.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var DSTRegionValue = FPKMstService.GetDSTRegionList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, DSTRegionValue);
            List<FlightPakMasterService.DSTRegion> filteredList = GetFilteredList(DSTRegionValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.DSTRegionID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgDaylightSavingTimeRegion.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgDaylightSavingTimeRegion.CurrentPageIndex = PageNumber;
            dgDaylightSavingTimeRegion.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfDSTRegion DSTRegionValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedDSTRegionID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = DSTRegionValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().DSTRegionID;
                Session["SelectedDSTRegionID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.DSTRegion> GetFilteredList(ReturnValueOfDSTRegion DSTRegionValue)
        {
            List<FlightPakMasterService.DSTRegion> filteredList = new List<FlightPakMasterService.DSTRegion>();

            if (DSTRegionValue.ReturnFlag)
            {
                filteredList = DSTRegionValue.EntityList;
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgDaylightSavingTimeRegion.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgDaylightSavingTimeRegion.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
    }
}