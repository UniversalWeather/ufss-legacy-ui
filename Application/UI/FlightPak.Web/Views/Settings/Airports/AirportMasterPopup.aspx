﻿﻿<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!--<![endif]-->
<head id="Head1" runat="server">
    <title>Airport</title>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <script type="text/javascript">
        var selectedRowData = null;
        var deleteAirportId = null;
        var deleteAirportName = null;
        var jqgridTableId = '#gridAirports';
        var selectedRowMultipleAirport = "";
        var isopenlookup = false;
        var ismultiSelect = false;
        var icaoID = [];
        $(document).ready(function () {
            ismultiSelect = getQuerystring("IsUIReports", "") == "" ? false : true;
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

            var selectedRowData = getQuerystring("IcaoID", "");
            selectedRowMultipleAirport = $.trim(decodeURI(getQuerystring("IcaoID", "")));
            jQuery("#hdnselectedfccd").val(selectedRowMultipleAirport);
            if (selectedRowData != "") {
                isopenlookup = true;
            }

            if (ismultiSelect) {
                if (selectedRowData.length < jQuery("#hdnselectedfccd").val().length) {
                    selectedRowData = jQuery("#hdnselectedfccd").value;
                }
                icaoID = selectedRowData.split(',');
            }

            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                if ((selr != null && ismultiSelect == false) || (ismultiSelect == true && selectedRowMultipleAirport.length > 0)) {
                    var rowData = $(jqgridTableId).getRowData(selr);
                    returnToParent(rowData, 0);
                }
                else {
                    showMessageBox('Please select an airport.', popupTitle);
                }
                return false;
            });

            $("#recordAdd").click(function () {
                var widthDoc = $(document).width(); var heightDoc = $(document).height();
                openWinAddEdit("/Views/Settings/Airports/AirportCatalog.aspx?IsPopup=Add", "rdAddpopupwindow", widthDoc + 92, heightDoc+ 50 , jqgridTableId);
                return false;
            });

            $("#recordDelete").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                deleteAirportId = rowData['AirportId'];
                deleteAirportName = rowData['AirportName'];
                var widthDoc = $(document).width();
                if (deleteAirportId != undefined && deleteAirportId != null && deleteAirportId != '' && deleteAirportId != 0) {
                    showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                }
                else {
                    showMessageBox('Please select an airport.', popupTitle);
                }
                return false;
            });

            function confirmCallBackFn(arg) {
                if (arg) {
                    $.ajax({
                        type: 'GET',
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=Airport&airportId=' + deleteAirportId,
                        contentType: 'text/html',
                        success: function (data) {
                            var message = "This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.";
                            DeleteApiResponse(jqgridTableId, data, message, popupTitle);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var content = jqXHR.responseText;
                            if (content.indexOf('404'))
                                showMessageBox("This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.", popupTitle);
                        }
                    });
                }
            }
            $("#recordEdit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                var airportId = rowData['AirportId'];
                var widthDoc = $(document).width();
                if (airportId != undefined && airportId != null && airportId != '' && airportId != 0) {
                    popupwindow("/Views/Settings/Airports/AirportCatalog.aspx?IsPopup=&AirportID=" + airportId, popupTitle, widthDoc + 92, 798, jqgridTableId);
                }
                else {
                    showMessageBox('Please select an airport.', popupTitle);
                }
                return false;
            });

            $("#chkInActive").change(function () {
                $(jqgridTableId).trigger('reloadGrid');

            });

            jQuery(jqgridTableId).jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }

                    postData.apiType = 'fss';
                    postData.method = 'airports';
                    postData.showInactive = $("#chkInActive").prop('checked');
                    postData.icaoId = function () { if (ismultiSelect && selectedRowData.length > 0) { return icaoID[0]; } else { return selectedRowData; } };
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;
                    return postData;

                },
                height: 390,
                width: 1020,
                viewrecords: true,
                rowNum: $("#rowNum").val(),
                multiselect: ismultiSelect,
                pager: "#pg_gridPager",
                colNames: ['AirportId', 'ICAO', 'IATA', 'City', 'State', 'Country', 'Airport', 'Inactive', 'Runway', 'UTC +/-', 'Entry Point'],
                colModel: [
                    { name: 'AirportId', index: 'AirportId', key: true, hidden: true },
                    { name: 'IcaoID', index: 'IcaoID', width: 62 },
                    { name: 'IATA', index: 'IATA', width: 65 },
                    { name: 'CityName', index: 'CityName', width: 190 },
                    { name: 'StateName', index: 'StateName', width: 140 },
                    { name: 'CountryName', index: 'CountryName', width: 115 },
                    { name: 'AirportName', index: 'AirportName', width: 200 },
                    { name: 'IsInactive', index: 'IsInactive', width: 60, formatter: "checkbox", align: 'center', formatoptions: { disabled: true }, search: false },
                    { name: 'LongestRunway', index: 'LongestRunway', width: 65, searchoptions: { sopt: ['ge'] } },
                    { name: 'OffsetToGMT', index: 'OffsetToGMT', width: 60, formatter: 'number', formatoptions: { decimalPlaces: 2 }, searchoptions: { sopt: ['eq'] } },
                    { name: 'IsEntryPort', index: 'IsEntryPort', width: 40, formatter: "checkbox", align: 'center', formatoptions: { disabled: true }, search: false }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData, 1);
                },
                onSelectRow: function (id, status) {
                    var rowData = $(this).getRowData(id);
                    var lastSel = rowData['IcaoID'];//replace name with any column
                    selectedRowMultipleAirport = JqgridOnSelectRow(ismultiSelect, status, selectedRowMultipleAirport, rowData.IcaoID);
                },
                onSelectAll: function (id, status) {
                    selectedRowMultipleAirport = JqgridSelectAll(selectedRowMultipleAirport, jqgridTableId, id, status, 'IcaoID');
                    jQuery("#hdnselectedfccd").val(selectedRowMultipleAirport);
                },
                afterInsertRow: function (rowid, rowObject) {
                    JqgridSelectAfterInsertRow(ismultiSelect, selectedRowMultipleAirport, rowid, rowObject.IcaoID, jqgridTableId);
                },
                loadComplete: function (rowData) {
                    JqgridCreateSelectAllOption(ismultiSelect, jqgridTableId);
                }
            });

            $("#pagesizebox").insertBefore('.ui-paging-info');
            $(jqgridTableId).jqGrid('filterToolbar', {
                defaultSearch: 'bw', searchOnEnter: false, stringResult: true,
                beforeSearch: function () {
                    if ($("#gs_IcaoID").val().length > 4) {
                        $("#gs_IcaoID").val('');
                        showMessageBox("ICAO should not exceed 4 characters.", popupTitle);
                        return true;
                    }

                    if ($("#gs_IATA").val().length > 6) {
                        $("#gs_IATA").val('');
                        showMessageBox("IATA should not exceed 6 characters.", popupTitle);
                        return true;
                    }

                    if ($("#gs_CityName").val().length > 50) {
                        $("#gs_CityName").val('');
                        showMessageBox("City should not exceed 50 characters.", popupTitle);
                        return true;
                    }

                    if ($("#gs_StateName").val().length > 50) {
                        $("#gs_StateName").val('');
                        showMessageBox("State should not exceed 50 characters.", popupTitle);
                        return true;
                    }

                    if ($("#gs_CountryName").val().length > 50) {
                        $("#gs_CountryName").val('');
                        showMessageBox("Country should not exceed 50 characters.", popupTitle);
                        return true;
                    }

                    if ($("#gs_AirportName").val().length > 50) {
                        $("#gs_AirportName").val('');
                        showMessageBox("Airport should not exceed 50 characters.", popupTitle);
                        return true;
                    }

                    var regex = /^\d*(\.\d{1})?\d{0,1}$/;
                    if (!$("#gs_LongestRunway").val().match(regex)) {
                        $("#gs_LongestRunway").val('');
                        showMessageBox("Incorrect Runway. Please enter number/decimal input.", popupTitle);
                        return true;
                    }
                    else if ($("#gs_LongestRunway").val().length > 6) {
                        $("#gs_LongestRunway").val('');
                        showMessageBox("Runway should not exceed 6 digits.", popupTitle);
                        return true;

                    }

                    var reg = /^\d*(\-\d{1})?(\+\d{1})?(\d+(\.\d{1,2}))?\d{0,1}$/;
                    if (!$("#gs_OffsetToGMT").val().match(reg)) {
                        $("#gs_OffsetToGMT").val('');
                        showMessageBox("Incorrect UTC. Please enter UTC +/- number/decimal input.", popupTitle);
                        return true;
                    }
                    else if ($("#gs_OffsetToGMT").val().length > 6) {
                        $("#gs_OffsetToGMT").val('');
                        showMessageBox("UTC should not exceed 6 digits.", popupTitle);
                        return true;
                    }
                }
            });
        });

        function returnToParent(rowData, one) {
            selectedRowMultipleAirport = jQuery.trim(selectedRowMultipleAirport);
            if (selectedRowMultipleAirport.lastIndexOf(",") == selectedRowMultipleAirport.length - 1)
                selectedRowMultipleAirport = selectedRowMultipleAirport.substr(0, selectedRowMultipleAirport.length - 1);


            var oArg = new Object();
            if (one == 0) {
                oArg.ICAO = selectedRowMultipleAirport;
            }
            else {
                oArg.ICAO = rowData["IcaoID"];
            }

            oArg.AirportName = rowData["AirportName"];
            oArg.AirportID = rowData["AirportId"];
            oArg.CountryName = rowData["CountryName"];
            oArg.Iata = rowData["IATA"];
            oArg.CityName = rowData["City"];
            oArg.Arg1 = oArg.ICAO;
            oArg.CallingButton = "IcaoID";
            var oWnd = GetRadWindow();
            if (oArg) {
                oWnd.close(oArg);
            }
        }

    </script>

    <style type="text/css">
</style>
</head>

<body>
    <form id="form1" runat="server">

        <div class="jqgrid">
            <input type="hidden" id="hdnselectedfccd" value="" />
            <div>
                <table class="box1">
                    <tr>
                        <td align="left">
                            <div>
                                <input type="checkbox" name="chkInActive" id="chkInActive" />
                                Display Inactive 
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="gridAirports" style="width: 981px !important;" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">
                                <div role="group" id="pg_gridPager"></div>
                                <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                                <div id="pagesizebox">
                                    <span>Page Size:</span>
                                    <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                    <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridTableId, $('#rowNum')); return false;" />
                                </div>
                            </div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK" type="button" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
