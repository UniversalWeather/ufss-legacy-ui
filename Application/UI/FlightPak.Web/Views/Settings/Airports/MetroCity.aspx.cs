﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Telerik.Web.UI;
//For Tracing and Exceptioon Handling
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Settings.People
{
    public partial class MetroCity : BaseSecuredPage
    {
        private List<string> MetroCodes = new List<string>();
        private bool MetroCityPageNavigated = false;
        private ExceptionManager exManager;
        private string strMetroCD = "";
        private string strMetroID = "";
        private bool _selectLastModified = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgMetroCatalog, dgMetroCatalog, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgMetroCatalog.ClientID));
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));

                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            CheckAutorization(Permission.Database.ViewMetro);
                            // Method to display first record in read only format   
                            if (Session["SearchItemPrimaryKeyValue"] != null)
                            {
                                dgMetroCatalog.Rebind();
                                ReadOnlyForm();
                                GridEnable(true, true, true);
                                Session.Remove("SearchItemPrimaryKeyValue");
                            }
                            else
                            {
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgMetroCatalog;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }


        }

        /// <summary>
        /// 
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                if (BindDataSwitch)
                {
                    dgMetroCatalog.Rebind();
                }
                if (dgMetroCatalog.MasterTableView.Items.Count > 0)
                {
                    //if (!IsPostBack)
                    //{
                    //    Session["SelectedMetroID"] = null;
                    //}
                    if (Session["SelectedMetroID"] == null)
                    {
                        dgMetroCatalog.SelectedIndexes.Add(0);
                        Session["SelectedMetroID"] = dgMetroCatalog.Items[0].GetDataKeyValue("MetroID").ToString();
                    }

                    if (dgMetroCatalog.SelectedIndexes.Count == 0)
                        dgMetroCatalog.SelectedIndexes.Add(0);

                    ReadOnlyForm();
                    GridEnable(true, true, true);
                }
                else
                {
                    ClearForm();
                    EnableForm(false);
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                if (Session["SelectedMetroID"] != null)
                {

                    string ID = Session["SelectedMetroID"].ToString();
                    foreach (GridDataItem Item in dgMetroCatalog.MasterTableView.Items)
                    {
                        if (Item["MetroID"].Text.Trim() == ID.Trim())
                        {
                            Item.Selected = true;
                            break;
                        }
                    }
                }
                else
                {
                    DefaultSelection(false);
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void MetroCity_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            LinkButton lbtnEditButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, divExternalForm, RadAjaxLoadingPanel1);
                            LinkButton lbtnInsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, divExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void MetroCity_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                {

                    //   CacheService.CacheServiceClient CService = new CacheService.CacheServiceClient();
                    //     var newMetroCity = CService.GetListCacheMetro();

                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            var MetroCity = Service.GetMetroCityList();
                            if (MetroCity.ReturnFlag == true)
                            {
                                dgMetroCatalog.DataSource = MetroCity.EntityList;
                            }
                            Session.Remove("MetroCD");
                            MetroCodes = new List<string>();
                            foreach (Metro MetroCatalogEntity in MetroCity.EntityList)
                            {
                                MetroCodes.Add(MetroCatalogEntity.MetroCD.ToLower().Trim());
                            }
                            Session["MetroCD"] = MetroCodes;
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void MetroCity_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;

                                if (Session["SelectedMetroID"] != null)
                                {
                                    //Lock will happen from UI                                
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Database.Metro, Convert.ToInt64(Session["SelectedMetroID"].ToString().Trim()));

                                        if (!returnValue.ReturnFlag)
                                        {
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Metro);
                                            SelectItem();
                                            return;
                                        }

                                        DisplayEditForm();
                                        GridEnable(false, true, false);
                                        tbCode.Enabled = false;
                                        tbDescription.Focus();
                                        SelectItem();
                                    }
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                DisplayInsertForm();
                                GridEnable(true, false, false);
                                dgMetroCatalog.SelectedIndexes.Clear();
                                tbCode.Focus();
                                break;
                            case "Filter":
                                //foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                //{
                                //    //column.CurrentFilterValue = string.Empty;
                                //    //column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                //}
		                        Pair filterPair = (Pair)e.CommandArgument;
		                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void MetroCity_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;

                        if (Session["SelectedMetroID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient MetroService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var RetVal = MetroService.UpdateMetroCity(GetItems());
                                if (RetVal.ReturnFlag == true)
                                {
                                    //Unlock should happen from Service Layer
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Database.Metro, Convert.ToInt64(Session["SelectedMetroID"].ToString().Trim()));
                                    }
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    //dgMetroCatalog.Rebind();
                                    GridEnable(true, true, true);
                                    SelectItem();
                                    ReadOnlyForm();
                                    ShowSuccessMessage();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(RetVal.ErrorMessage, ModuleNameConstants.Database.Metro);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void MetroCity_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidate = true;
                        if (CheckAlreadyExist())
                        {
                            cvCode.IsValid = false;
                            tbCode.Focus();
                            IsValidate = false;
                        }
                        if (IsValidate)
                        {
                            using (MasterCatalogServiceClient MetroService = new MasterCatalogServiceClient())
                            {
                                var ReturnValue = MetroService.AddMetroCity(GetItems());
                                if (ReturnValue.ReturnFlag == true)
                                {
                                    dgMetroCatalog.Rebind();
                                    DefaultSelection(false);

                                    ShowSuccessMessage();
                                    _selectLastModified = true;
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(ReturnValue.ErrorMessage, ModuleNameConstants.Database.Metro);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void MetroCity_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (Session["SelectedMetroID"] != null)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient MetroService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.Metro Metro = new FlightPakMasterService.Metro();

                                string Code = Session["SelectedMetroID"].ToString();
                                strMetroCD = "";
                                strMetroID = "";
                                foreach (GridDataItem Item in dgMetroCatalog.MasterTableView.Items)
                                {
                                    if (Item["MetroID"].Text.Trim() == Code.Trim())
                                    {
                                        strMetroCD = Item["MetroCD"].Text.Trim();
                                        strMetroID = Item["MetroID"].Text.Trim();
                                        break;
                                    }
                                }

                                Metro.MetroCD = strMetroCD;
                                Metro.MetroID = Convert.ToInt64(strMetroID);
                                Metro.IsDeleted = true;
                                //Lock will happen from UI
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Database.Metro, Convert.ToInt64(strMetroID));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelection(false);
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstants.Database.Metro);
                                        return;
                                    }
                                    MetroService.DeleteMetroCity(Metro);

                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = false;
                                    DefaultSelection(false);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.Metro, Convert.ToInt64(strMetroID));
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void MetroCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnRedirect.Value = "";

            if (hdnSave.Value == "Update")
            {
                SelectItem();

                string confirmMessage = "Do you want to save changes to the current record?";
                rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
            }
            else
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            if (btnSaveChanges.Visible == false)
                            {
                                GridDataItem item = dgMetroCatalog.SelectedItems[0] as GridDataItem;
                                Session["SelectedMetroID"] = item["MetroID"].Text;
                                //dgMetroCatalog.Items[0].GetDataKeyValue("MetroID").ToString();
                                if (btnSaveChanges.Visible == false)
                                {
                                    ReadOnlyForm();
                                }
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void MetroCity_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        dgMetroCatalog.Rebind();
                        GridEnable(true, true, true);
                        MetroCityPageNavigated = true;
                        dgMetroCatalog.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void MetroCity_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //GridEnable(true, true, true);
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (MetroCityPageNavigated)
                        {
                            SelectItem();
                        }
                        RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgMetroCatalog, Page.Session);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Add"></param>
        /// <param name="Edit"></param>
        /// <param name="Delete"></param>
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {

                LinkButton lbtnInsertCtl = (LinkButton)dgMetroCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                LinkButton lbtnDelCtl = (LinkButton)dgMetroCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                LinkButton lbtnEditCtl = (LinkButton)dgMetroCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");

                if (IsAuthorized(Permission.Database.AddMetro))
                {
                    lbtnInsertCtl.Visible = true;
                    if (Add)
                    {
                        lbtnInsertCtl.Enabled = true;
                    }
                    else
                    {
                        lbtnInsertCtl.Enabled = false;
                    }
                }
                else
                {
                    lbtnInsertCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.DeleteMetro))
                {
                    lbtnDelCtl.Visible = true;
                    if (Delete)
                    {
                        lbtnDelCtl.Enabled = true;
                        lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete();";
                    }
                    else
                    {
                        lbtnDelCtl.Enabled = false;
                        lbtnDelCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    lbtnDelCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Database.EditMetro))
                {
                    lbtnEditCtl.Visible = true;
                    if (Edit)
                    {
                        lbtnEditCtl.Enabled = true;
                        lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
                    }
                    else
                    {
                        lbtnEditCtl.Enabled = false;
                        lbtnEditCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    lbtnEditCtl.Visible = false;
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void DisplayInsertForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                hdnSave.Value = "Save";
                ClearForm();
                EnableForm(true);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                LoadControlData();
                pnlExternalForm.Visible = true;
                hdnSave.Value = "Update";
                hdnRedirect.Value = "";
                EnableForm(true);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Enable"></param>
        protected void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                tbCode.Enabled = Enable;
                tbDescription.Enabled = Enable;
                btnSaveChanges.Visible = Enable;
                btnCancel.Visible = Enable;

            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbCode.Text = string.Empty;
                tbDescription.Text = string.Empty;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgMetroCatalog.SelectedItems.Count > 0)
                {
                    GridDataItem Item = dgMetroCatalog.SelectedItems[0] as GridDataItem;
                    Label lbLastUpdatedUser = (Label)dgMetroCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                    if (Item.GetDataKeyValue("LastUpdUID") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                    }
                    else
                    {
                        lbLastUpdatedUser.Text = string.Empty;
                    }
                    if (Item.GetDataKeyValue("LastUpdTS") != null)
                    {
                        //To retrieve back the UTC date in Homebase format
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                    }
                    LoadControlData();
                    EnableForm(false);
                }
                else
                {
                    DefaultSelection(true);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["SelectedMetroID"] != null)
                {
                    strMetroCD = "";
                    strMetroID = "";
                    string strMetroName = "";
                    foreach (GridDataItem Item in dgMetroCatalog.MasterTableView.Items)
                    {
                        if (Item["MetroID"].Text.Trim() == Session["SelectedMetroID"].ToString().Trim())
                        {
                            strMetroCD = Item["MetroCD"].Text.Trim();
                            strMetroID = Item["MetroID"].Text.Trim();
                            if (Item.GetDataKeyValue("MetroName") != null)
                            {
                                strMetroName = Item.GetDataKeyValue("MetroName").ToString().Trim();
                            }

                            lbColumnName1.Text = "Metro Code";
                            lbColumnName2.Text = "Description";
                            lbColumnValue1.Text = Item["MetroCD"].Text;
                            lbColumnValue2.Text = Item["MetroName"].Text;
                            break;
                        }
                    }
                    tbCode.Text = strMetroCD;
                    if (strMetroName != null)
                    {
                        tbDescription.Text = strMetroName;
                    }
                    else
                    {
                        tbDescription.Text = string.Empty;
                    }
                }

            }
        }

        /// <summary>
        /// Save and Update Metro Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            (dgMetroCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgMetroCatalog.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    hdnSave.Value = "";
                    if (!string.IsNullOrEmpty(hdnRedirect.Value))
                    {
                        Response.Redirect(hdnRedirect.Value, false);
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }
        }

        /// <summary>
        /// Cancel Metro Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["SelectedMetroID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySet.Database.Metro, Convert.ToInt64(Session["SelectedMetroID"].ToString().Trim()));
                                }
                                //Session["IsEditLockMetro"] = "False";
                            }
                        }
                        DefaultSelection(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

            hdnSave.Value = "";
            if (!string.IsNullOrEmpty(hdnRedirect.Value))
            {
                Response.Redirect(hdnRedirect.Value, false);
            }
        }

        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private Metro GetItems()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FlightPakMasterService.Metro MetroServiceType = null;

                MetroServiceType = new FlightPakMasterService.Metro();

                if (hdnSave.Value == "Update")
                {
                    //GridDataItem Item = (GridDataItem)dgMetroCatalog.SelectedItems[0];
                    if (Session["SelectedMetroID"] != null)
                    {
                        MetroServiceType.MetroID = Convert.ToInt64(Session["SelectedMetroID"].ToString().Trim());
                    }
                }
                else
                {
                    MetroServiceType.MetroID = 0;
                }
                MetroServiceType.MetroCD = tbCode.Text.Trim();
                MetroServiceType.MetroName = tbDescription.Text.Trim();
                MetroServiceType.IsDeleted = false;
                //pnlExternalForm.Visible = false

                return MetroServiceType;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (tbCode.Text != null)
                        {
                            if (CheckAlreadyExist())
                            {
                                //tbCode.Focus();
                                RadAjaxManager1.FocusControl(tbCode.ClientID);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbCode.ClientID + "');", true);
                            }
                            else
                            {
                                //tbDescription.Focus();
                                RadAjaxManager1.FocusControl(tbDescription.ClientID);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetFocus", "SetFocus('" + tbDescription.ClientID + "');", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool CheckAlreadyExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnFlag = false;

                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var ReturnValue = Service.GetMetroCityList().EntityList.Where(x => x.MetroCD.Trim().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim()));
                    if (ReturnValue.Count() > 0 && ReturnValue != null)
                    {
                        cvCode.IsValid = false;
                        tbCode.Focus();
                        ReturnFlag = true;
                    }
                }
                return ReturnFlag;
            }
        }

        
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    dgMetroCatalog.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, true);
                }
            }
        }
        
        private void PreSelectItem(FlightPakMasterService.MasterCatalogServiceClient FPKMstService, bool isFromSearch)
        {
            int iIndex = 0;
            int FullItemIndex = 0;

            var MetroValue = FPKMstService.GetMetroCityList();
            Int64 PrimaryKeyValue = CheckOriginForPreSelection(isFromSearch, MetroValue);
            List<FlightPakMasterService.Metro> filteredList = GetFilteredList(MetroValue);

            foreach (var Item in filteredList)
            {
                if (PrimaryKeyValue == Item.MetroID)
                {
                    FullItemIndex = iIndex;
                    break;
                }

                iIndex++;
            }

            int PageSize = dgMetroCatalog.PageSize;
            int PageNumber = FullItemIndex / PageSize;
            int ItemIndex = FullItemIndex % PageSize;

            dgMetroCatalog.CurrentPageIndex = PageNumber;
            dgMetroCatalog.SelectedIndexes.Add(ItemIndex);
        }
        
        private Int64 CheckOriginForPreSelection(bool fromSearch, ReturnValueOfMetro MetroValue)
        {
            Int64 PrimaryKeyValue = 0;

            if (fromSearch)
            {
                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    PrimaryKeyValue = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]);
                    Session["SelectedMetroID"] = Session["SearchItemPrimaryKeyValue"];
                }
            }
            else
            {
                PrimaryKeyValue = MetroValue.EntityList.OrderByDescending(x => x.LastUpdTS).FirstOrDefault().MetroID;
                Session["SelectedMetroID"] = PrimaryKeyValue;
            }

            return PrimaryKeyValue;
        }
        
        private List<FlightPakMasterService.Metro> GetFilteredList(ReturnValueOfMetro MetroValue)
        {
            List<FlightPakMasterService.Metro> filteredList = new List<FlightPakMasterService.Metro>();

            if (MetroValue.ReturnFlag)
            {
                filteredList = MetroValue.EntityList;
            }

            return filteredList;
        }
        
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FPKMstService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (_selectLastModified)
                {
                    dgMetroCatalog.SelectedIndexes.Clear();
                    PreSelectItem(FPKMstService, false);
                    dgMetroCatalog.Rebind();
                    SelectItem();
                    ReadOnlyForm();
                }
            }
        }
    }
}