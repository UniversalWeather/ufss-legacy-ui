﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MetroCityPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Airports.MetroCityPopup" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Metro City</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgMetroCity.ClientID %>");

            //this function is used to set the frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            //this function is used to select the value of selected Metro code 
              function returnToParent()
               {
                //create the argument that will be returned to the parent page             
                oArg = new Object();
                grid = $find("<%= dgMetroCity.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++)
                 {
                
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "MetroCD");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "MetroName");
                    var cell3 = MasterTable.getCellByColumnUniqueName(row, "MetroID");
                    
                }
                if (selectedRows.length > 0)
                 {
                    oArg.MetroCD = cell1.innerHTML;
                    oArg.MetroName = cell2.innerHTML;
                    oArg.MetroID = cell3.innerHTML;
                  
                }
                else 
                {
                    oArg.MetroCD = "";
                    oArg.MetroName = "";
                    oArg.MetroID = "";
                }


                var oWnd = GetRadWindow();
                if (oArg) 
                {
                    oWnd.close(oArg);
                }
            }
            //this function is used to close the window 
            function Close() {
                GetRadWindow().Close();
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgMetroCity">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgMetroCity" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgMetroCity" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgMetroCity" runat="server" AllowMultiRowSelection="true" AllowSorting="true"
            OnNeedDataSource="dgMetroCity_BindData" AutoGenerateColumns="false" Height="348px"
            PageSize="10" AllowPaging="true" Width="600px" OnItemCommand="dgMetroCity_ItemCommand"
            OnPreRender="dgMetroCity_PreRender">
            <MasterTableView DataKeyNames="MetroID,MetroCD,MetroName,LastUpdUID,LastUpdTS" CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="MetroCD" HeaderText="Metro Code" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px" FilterControlWidth="80px"
                        ShowFilterIcon="false" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="MetroName" HeaderText="Description" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="StartsWith" ShowFilterIcon="false" HeaderStyle-Width="500px"
                        FilterControlWidth="480px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="MetroID" HeaderText="MetroID" UniqueName="MetroID" Display="false"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="EqualTo" ShowFilterIcon="false"
                        FilterDelay="500">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; text-align: right;">
                        <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                            Ok</button>                       
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnRowDblClick = "returnToParent"  />
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
             <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
    </div>
    </form>
</body>
</html>
