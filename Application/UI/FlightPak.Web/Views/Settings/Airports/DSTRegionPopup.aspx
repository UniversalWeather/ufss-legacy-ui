﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DSTRegionPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Airports.DSTRegionPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Daylight Saving Time Region</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshGrid(arg);
            }
            var oArg = new Object();
            var grid = $find("<%= dgRegion.ClientID %>");

            //this function is used to set the frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            //this function is used to select the value of selected Region code 
            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgRegion.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "DSTRegionCD");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "DSTRegionName");
                    var cell3 = MasterTable.getCellByColumnUniqueName(row, "OffSet");
                    var cell4 = MasterTable.getCellByColumnUniqueName(row, "StartDT");
                    var cell5 = MasterTable.getCellByColumnUniqueName(row, "EndDT");
                    var cell6 = MasterTable.getCellByColumnUniqueName(row, "DSTRegionID");
                }
                if (selectedRows.length > 0) {
                    oArg.DSTRegionCD = cell1.innerHTML;
                    oArg.DSTRegionName = cell2.innerHTML;
                    oArg.OffSet = cell3.innerHTML;
                    oArg.StartDT = cell4.innerHTML;
                    oArg.EndDT = cell5.innerHTML;
                    oArg.DSTRegionID = cell6.innerHTML;

                }
                else {
                    oArg.DSTRegionCD = "";
                    oArg.DSTRegionName = "";
                    oArg.OffSet = "";
                    oArg.StartDT = "";
                    oArg.EndDT = "";
                    oArg.DSTRegionID = "";
                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            //this function is used to close the window 
            function Close() {
                GetRadWindow().Close();
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgRegion">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgRegion" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgRegion" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgRegion" runat="server" AllowMultiRowSelection="true" AllowSorting="true"
            OnNeedDataSource="dgRegion_BindData" AutoGenerateColumns="false" Height="341px"
            Width="840px" PageSize="10" AllowPaging="true" AllowFilteringByColumn="true"
            OnItemCommand="dgRegion_ItemCommand" OnPreRender="dgRegion_PreRender" >
            <MasterTableView DataKeyNames="DSTRegionID,DSTRegionCD,DSTRegionName,LastUpdUID,LastUpdTS,OffSet,StartDT,EndDT"
                CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="DSTRegionCD" HeaderText="DST Code" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="StartsWith" ShowFilterIcon="false" HeaderStyle-Width="80px"
                        FilterControlWidth="60px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DSTRegionName" HeaderText="Description" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="StartsWith" ShowFilterIcon="false" HeaderStyle-Width="340px"
                        FilterControlWidth="320px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="OffSet" HeaderText="Offset" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="EqualTo" ShowFilterIcon="false" HeaderStyle-Width="80px"
                        FilterControlWidth="60px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="StartDT" HeaderText="Start Date" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="EqualTo" ShowFilterIcon="false" HeaderStyle-Width="160px"
                        FilterControlWidth="140px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EndDT" HeaderText="End Date" AutoPostBackOnFilter="false"
                        CurrentFilterFunction="EqualTo" ShowFilterIcon="false" HeaderStyle-Width="160px"
                        FilterControlWidth="140px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DSTRegionID" HeaderText="Region ID" UniqueName="DSTRegionID"
                        Display="false" AutoPostBackOnFilter="false" CurrentFilterFunction="EqualTo" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; text-align: right;">
                        <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                            Ok</button>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnRowDblClick = "returnToParent" />
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
    </div>
    </form>
</body>
</html>
