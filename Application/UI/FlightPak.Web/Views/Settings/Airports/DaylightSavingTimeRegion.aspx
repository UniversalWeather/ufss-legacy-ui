﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="DaylightSavingTimeRegion.aspx.cs" Inherits="FlightPak.Web.Views.Settings.People.DaylightSavingTimeRegion"
    ClientIDMode="AutoID" MaintainScrollPositionOnPostback="true"  %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<%@ Register TagPrefix="uc" TagName="DatePicker" Src="~/UserControls/UCDate.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
 <style type="text/css"  >
        .rgDataDiv {
            overflow:hidden !important;
        }
    </style>
    <script type="text/javascript" src="../../../Scripts/Common.js">
    </script>
    <script type="text/javascript">
        function CheckTxtBox(control) {


            var txtCode = document.getElementById("<%=tbCode.ClientID%>").value;
            var txtDesc = document.getElementById("<%=tbDescription.ClientID%>").value;

            if (txtCode == "") {
                if (control == 'code') {
                    ValidatorEnable(document.getElementById('<%=rfvCode.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbCode.ClientID%>').focus()", 0);
                } return false;
            }

            if (txtDesc == "") {
                if (control == 'desc') {
                    ValidatorEnable(document.getElementById('<%=rfvDescription.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbDescription.ClientID%>').focus()", 0);
                } return false;
            }
        }
 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            
            $(document).ready(function UpdateConfirm() {
                $("a").click(function (event) {
                    var redirectUrl = $(this).attr("href");
                    if (redirectUrl !== "#" && document.getElementById('<%=hdnSave.ClientID%>').value === "Update") {

                        var msg = "Do you want to save changes to the current record?";
                        document.getElementById('<%=hdnRedirect.ClientID%>').value = redirectUrl;

                        var oManager = $find('<%=rwmUpdateConfirmation.ClientID%>');
                        oManager.radconfirm(msg, UpdateConfirmCallBackFn, 330, 110, null, "Confirmation!");

                        //for blocking redirection
                        return false;
                    }
                });
            });
            
            function UpdateConfirmCallBackFn(arg) {
                if (arg === 1) {
                    document.getElementById('<%=btnSaveChanges.ClientID%>').click();
                }
                else if (arg === 2) {
                    document.getElementById('<%=btnCancel.ClientID%>').click();
                }
                //for blocking redirection
                return false;
            }

            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgDaylightSavingTimeRegion" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="clrFilters">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgDaylightSavingTimeRegion">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgDaylightSavingTimeRegion" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
    
    <telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">DST Region</span> <span class="tab-nav-icons">
                        <!-- <a href="#" class="search-icon"></a><a href="#" class="save-icon"></a><a href="#"
                            class="print-icon"></a> -->
                        <a href="../../Help/ViewHelp.aspx?Screen=DSTRegionHelp" target="_blank" title="Help" class="help-icon"></a></span>
                </div>
            </td>
        </tr>
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
        <tr style="display: none;">
            <td>
                <uc:DatePicker ID="ucDatePicker" runat="server"></uc:DatePicker>
            </td>
        </tr>
    </table>
    <telerik:RadGrid ID="dgDaylightSavingTimeRegion" runat="server" AllowSorting="true"
        OnItemCreated="dgDaylightSavingTimeRegion_ItemCreated" Visible="true" OnNeedDataSource="dgDaylightSavingTimeRegion_BindData"
        OnItemCommand="dgDaylightSavingTimeRegion_ItemCommand" OnUpdateCommand="dgDaylightSavingTimeRegion_UpdateCommand"
        OnInsertCommand="dgDaylightSavingTimeRegion_InsertCommand" OnDeleteCommand="dgDaylightSavingTimeRegion_DeleteCommand"
        AutoGenerateColumns="false" PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgDaylightSavingTimeRegion_SelectedIndexChanged"
        Height="341px" AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true"
        OnPreRender="dgDaylightSavingTimeRegion_PreRender" OnPageIndexChanged="dgDaylightSavingTimeRegion_PageIndexChanged">
        <MasterTableView DataKeyNames="DSTRegionID,DSTRegionCD,DSTRegionName,StartDT,EndDT,OffSet,CityList,LastUpdUID,LastUpdTS"
            CommandItemDisplay="Bottom">
            <Columns>
                <telerik:GridBoundColumn DataField="DSTRegionCD" HeaderText="DST Code" AllowFiltering="true"
                    CurrentFilterFunction="StartsWith" ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500"
                    HeaderStyle-Width="80px" FilterControlWidth="60px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DSTRegionName" HeaderText="Description" AllowFiltering="true"
                    CurrentFilterFunction="StartsWith" ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500"
                    HeaderStyle-Width="300px" FilterControlWidth="280px">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CityList" HeaderText="Area/City" AllowFiltering="true"
                    CurrentFilterFunction="StartsWith" ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500"
                    HeaderStyle-Width="380px" FilterControlWidth="360px">
                </telerik:GridBoundColumn>
            </Columns>
            <CommandItemTemplate>
                <div class="grid_icon">
                    <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                        CssClass="add-icon-grid" Visible="false"></asp:LinkButton>
                    <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                        CssClass="edit-icon-grid" ToolTip="Edit" CommandName="Edit" Visible="false"></asp:LinkButton>
                    <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                        CssClass="delete-icon-grid" runat="server" CommandName="DeleteSelected" ToolTip="Delete"
                        Visible="false"></asp:LinkButton>
                </div>
                <div>
                    <asp:Label runat="server" ID="lbLastUpdatedUser" CssClass="last-updated-text"></asp:Label>
                </div>
            </CommandItemTemplate>
        </MasterTableView>
        <ClientSettings EnablePostBackOnRowClick="true">
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        <GroupingSettings CaseSensitive="false" />
    </telerik:RadGrid>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="True">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdLabel160">
                        <div id="tdSuccessMessage" class="success_msg">
                            Record saved successfully.</div>
                    </td>
                    <td align="right">
                        <div class="mandatory">
                            <span>Bold</span> Indicates required field</div>
                    </td>
                </tr>
            </table>
            <table class="border-box">
                <tr>
                    <td valign="top">
                        <span class="mnd_text">DST Code</span>
                    </td>
                    <td class="tdtext100" valign="top">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="tbCode" runat="server" MaxLength="3" CssClass="text50" OnTextChanged="tbCode_textchange"
                                        ValidationGroup="Save" AutoPostBack="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvCode" runat="server" ValidationGroup="Save" ControlToValidate="tbCode"
                                                    Display="Dynamic" CssClass="alert-text" SetFocusOnError="true" ErrorMessage="Code is Required"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator ID="cvDstCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique DST Code is Required"
                                                    Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" class="tdLabel100">
                        <span class="mnd_text">Description</span>
                    </td>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="tbDescription" runat="server" MaxLength="40" CssClass="text180"
                                        ValidationGroup="Save"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ValidationGroup="Save"
                                        ControlToValidate="tbDescription" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                        ErrorMessage="Description is Required"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel100" valign="top">
                        <asp:Label ID="lbStart" runat="server" Text="Start"></asp:Label>
                    </td>
                    <td class="text160">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="padright_5">
                                    <uc:DatePicker ID="ucStartDate" runat="server" />
                                </td>
                                <td valign="top" class="tdLabel100 mask-textbox span">
                                    <telerik:RadMaskedTextBox ID="rmtbStartTime" runat="server" SelectionOnFocus="SelectAll"
                                        Mask="<0..23>:<0..59>" CssClass="RadMaskedTextBox35 mask-textbox">
                                    </telerik:RadMaskedTextBox>
                                </td>
                            </tr>
                        </table>
                        <%--  <telerik:RadMaskedTextBox ID="tbStart" runat="server" SelectionOnFocus="SelectAll"
                            Mask="<0..12>/<0..31>/<0..9999> <0..23>:<0..59>" CssClass="text100">
                        </telerik:RadMaskedTextBox>--%>
                    </td>
                    <td class="tdLabel60" valign="top">
                        <asp:Label ID="lbEnd" runat="server" Text="End"></asp:Label>
                    </td>
                    <td>
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="padright_5">
                                    <uc:DatePicker ID="ucFinishedDate" runat="server" />
                                </td>
                                <td valign="top" class="tdLabel100 mask-textbox span">
                                    <telerik:RadMaskedTextBox ID="rmtbFinishTime" runat="server" SelectionOnFocus="SelectAll"
                                        Mask="<0..23>:<0..59>" CssClass="RadMaskedTextBox35 mask-textbox">
                                    </telerik:RadMaskedTextBox>
                                </td>
                            </tr>
                        </table>
                        <%--<telerik:RadMaskedTextBox ID="tbEnd" runat="server" SelectionOnFocus="SelectAll"
                            Mask="<0..12>/<0..31>/<0..9999> <0..23>:<0..59>" CssClass="text100">
                        </telerik:RadMaskedTextBox>--%>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel100" valign="top">
                        <asp:Label ID="lbUTCOffset" runat="server" Text="UTC Offset"></asp:Label>
                    </td>
                    <td class="tdLabel120">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="tbutcoffset" runat="server" MaxLength="6" CssClass="text60" Text="00.00"
                                        onKeyPress="return fnAllowNumericAndChar(this, event,'.+-')">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RangeValidator ID="rvUTC" runat="server" ControlToValidate="tbutcoffset" ValidationGroup="Save"
                                        CssClass="alert-text" Text="Values should be between -14 to 14" Display="Dynamic"
                                        Type="Double" MaximumValue="14" MinimumValue="-14"></asp:RangeValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RegularExpressionValidator ID="revutcoffset" runat="server" Display="Dynamic"
                                        ErrorMessage="Invalid Format" ControlToValidate="tbutcoffset" CssClass="alert-text"
                                        ValidationExpression="^[-+]?\d+(\.\d\d)$" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="tdLabel100" valign="top">
                        <asp:Label ID="lbCityList" runat="server" Text="City List"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="tbCityList" runat="server" MaxLength="60" CssClass="text180" onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                <tr>
                    <td>
                        <asp:Button ID="btnSaveChanges" Text="Save" runat="server" OnClick="SaveChanges_Click"
                            ValidationGroup="Save" CssClass="button" />
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" OnClick="Cancel_Click" CssClass="button"
                            CausesValidation="false" />
                        <asp:HiddenField ID="hdnSave" runat="server" />
                        <asp:HiddenField ID="hdnStartDate" runat="server" />
                        <asp:HiddenField ID="hdnFinishedDate" runat="server" />
                        <asp:HiddenField ID="hdnRedirect" runat="server" /> 
                    </td>
                </tr>
            </table>
            <div id="toolbar" class="scr_esp_down_db">
                <div class="floating_main_db">
                    <div class="floating_bar_db">
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName1"></asp:Label></span>
                        <span class="padright_20">
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue1"></asp:Label></span>
                        <span class="padright_10">
                            <asp:Label runat="server" CssClass="bar_info" ID="lbColumnName2"></asp:Label></span>
                        <span>
                            <asp:Label runat="server" CssClass="floatinfo" ID="lbColumnValue2"></asp:Label></span>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="../../../Scripts/scroll_toolbar.js"></script>
            <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
        </asp:Panel>
    </div>
</asp:Content>
