﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Settings.SystemAdministration
{
    public partial class ChangeAirportICAO : BaseSecuredPage
    {
        // Declarations
        private ExceptionManager exManager;
        public bool IsLock = false;
        public bool NoLoggedinuser = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        #region Authorization
                        if (!UserPrincipal.Identity._isSysAdmin)
                            Response.Redirect(ResolveUrl("~/Views/ErrorPages/AccessDenied.aspx"));
                        #endregion

                        //Check for logged users and locked records
                        if (!IsPostBack)
                        {
                            CheckLoggedUsersAndRedirect();

                            if (NoLoggedinuser)
                                checkLockRecordsAndRedirect();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }

        }
        #region "NFR Logged Users"
        private bool CheckNoLoggedUsers()
        {
            using (CommonService.CommonServiceClient objComsvc = new CommonService.CommonServiceClient())
            {
                bool returnVal = true;
                List<FlightPak.Web.CommonService.GetAllLoggedUsersByCustomerId> loggedInUsers = new List<CommonService.GetAllLoggedUsersByCustomerId>();
                var objRetVal = objComsvc.GetAllLoggedInUsers(false);
                if (objRetVal.ReturnFlag)
                {
                    loggedInUsers = objRetVal.EntityList;
                    if (loggedInUsers != null && loggedInUsers.Count > 0)
                    {
                        returnVal = false;
                        NoLoggedinuser = false;
                    }
                }
                return returnVal;
            }
        }

        private void CheckLoggedUsersAndRedirect()
        {
            if (!CheckNoLoggedUsers())
                //PP23012013
                RadWindowManager1.RadConfirm("All other users MUST be out of Flight Scheduling Software System. ICAO Change Utility cannot be run right now. Other users may be currently logged in. Please check the Logged-In Users screen.", "confirmCancelCallBackFn", 400, 150, null, ModuleNameConstants.SysAdmin.SystemAdmin);
        }

        #endregion

        #region "NFR Locked Records"
        private bool checkLockRecords()
        {
            using (CommonService.CommonServiceClient common = new CommonService.CommonServiceClient())
            {
                bool returnLockVal = true;
                List<FlightPak.Web.CommonService.GetAllLocks> lockedRecords = new List<CommonService.GetAllLocks>();
                var LockData = common.GetAllLocks();
                if (LockData.ReturnFlag == true)
                {
                    lockedRecords = LockData.EntityList;
                    if (lockedRecords != null && lockedRecords.Count > 0)
                    {
                        returnLockVal = false;
                    }
                }
                return returnLockVal;
            }
        }

        private void checkLockRecordsAndRedirect()
        {
            if (!checkLockRecords())
            {
                RadWindowManager1.RadAlert("Records are being used by another user...Please use record lock manager screen to unlock.", 300, 150, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackFn");
                //PP23012013(Deleted IsLock = true;)
            }
        }

        #endregion

        protected void btnYes_Click(object sender, EventArgs e)
        {
            //Redirection for logged users and locked records
            Response.Redirect("LockManager.aspx"); //PP23012013
        }

        //PP23012013
        protected void btnCancelYes_Click(object sender, EventArgs e)
        {
            Response.Redirect("LoggedInUsers.aspx");
        }

        protected void btnCancelNo_Click(object sender, EventArgs e)
        {
            //Redirection for logged users and locked records

            //Response.Redirect("../../Home.aspx");
            Response.Redirect(ResolveUrl("~/Views/Settings/Default.aspx?Screen=ChangeAirportICAO"));  
        }
        //PP23012013

        /// <summary>
        /// To validate Old Airport ICAO 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OldICAO_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                lbcvOldICAO.Text = string.Empty;
                tbOldICAO.Text = tbOldICAO.Text.ToUpper();
                if (!string.IsNullOrEmpty(tbOldICAO.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                        var objRetVal = objDstsvc.GetAirportByAirportICaoID(tbOldICAO.Text.ToUpper().Trim());
                        if (objRetVal.ReturnFlag)
                        {
                            AirportLists = objRetVal.EntityList.ToList();
                            if (AirportLists != null && AirportLists.Count > 0)
                            {
                                if (string.IsNullOrEmpty(AirportLists[0].UWAID) || AirportLists[0].UWAID == "&nbsp;" || AirportLists[0].UWAID == "")
                                {
                                    hdnOldAirportID.Value = ((FlightPak.Web.FlightPakMasterService.GetAllAirport)AirportLists[0]).AirportID.ToString();
                                    if (!string.IsNullOrEmpty(AirportLists[0].AirportName))
                                        lbOldICAO.Text = System.Web.HttpUtility.HtmlEncode(((FlightPak.Web.FlightPakMasterService.GetAllAirport)AirportLists[0]).AirportName.ToString());
                                    lbcvOldICAO.Text = "";
                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("This airport is not a User added airport. Only User added airports can be changed with this procedure", 350, 100, "System Messages", "alertCallBackOldICAOFn", null);
                                    lbcvOldICAO.Text = "";
                                    lbOldICAO.Text = "";
                                    tbOldICAO.Text = "";
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbOldICAO);
                                }
                            }
                            else
                            {
                                lbcvOldICAO.Text = System.Web.HttpUtility.HtmlEncode("Invalid Old ICAO.");
                                lbOldICAO.Text = "";
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbOldICAO);
                            }
                        }
                        else
                        {
                            lbcvOldICAO.Text = System.Web.HttpUtility.HtmlEncode("Invalid Old ICAO.");
                            lbOldICAO.Text = "";
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbOldICAO);
                        }
                    }
                }
                else
                {
                    hdnOldAirportID.Value = "";
                    lbOldICAO.Text = "";
                }
            }
        }

        /// <summary>
        /// To validate New Airport ICAO doesnot exist in Database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NewICAO_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                tbNewICAO.Text = tbNewICAO.Text.ToUpper();
                if (!string.IsNullOrEmpty(tbNewICAO.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                        var objRetVal = objDstsvc.GetAirportByAirportICaoID(tbNewICAO.Text);
                        if (objRetVal.ReturnFlag)
                        {
                            AirportLists = objRetVal.EntityList;
                            if (AirportLists != null && AirportLists.Count > 0)
                            {
                                RadWindowManager1.RadAlert("WARNING- You have selected a New ICAO that already exists in the database. This operation is NOT allowed. It would make two airports have the same ICAO. Select another ICAO?", 350, 100, "System Messages", "alertCallBackNewICAOFn", null);
                                tbNewICAO.Text = "";
                            }
                            else
                            {

                            }
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Change button OnClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnChange_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(hdnOldAirportID.Value))
            {
                RadWindowManager1.RadAlert("Old ICAO Is Required.", 300, 100, "System Messages", "alertCallBackOldICAOFn", null);
            }
            if (string.IsNullOrEmpty(tbNewICAO.Text) && !string.IsNullOrEmpty(hdnOldAirportID.Value))
            {
                RadWindowManager1.RadAlert("New ICAO Is Required.", 300, 100, "System Messages", "alertCallBackNewICAOFn", null);
            }

            if (!string.IsNullOrEmpty(tbNewICAO.Text) && !string.IsNullOrEmpty(hdnOldAirportID.Value))
            {
                RadWindowManager1.RadConfirm("WARNING - Are you sure you want to change ICAO?", "confirmCallBackChangeAccountFn", 300, 100, null, ModuleNameConstants.SysAdmin.SystemAdmin);
            }
        }

        protected void ChangeICAONo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbNewICAO);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        protected void ChangeICAOYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Check for logged users and locked records
                        if (CheckNoLoggedUsers() && checkLockRecords())
                        {
                            using (CommonService.CommonServiceClient CommonSvc = new CommonService.CommonServiceClient())
                            {

                                CommonSvc.LockCustomer();
                                var objRetVal = CommonSvc.ChangeAirport(tbOldICAO.Text, tbNewICAO.Text);
                                CommonSvc.UnlockCustomer();
                                if (objRetVal != null && objRetVal)
                                {
                                    RadWindowManager1.RadAlert("All Icaos Changed", 250, 100, "System Messages", "alertCallBackOldICAOFn", null);
                                    Clearform();
                                }
                            }

                        }
                        else
                        {
                            if (!NoLoggedinuser)
                            {
                                //PP23012013
                                RadWindowManager1.RadConfirm("All other users MUST be out of Flight Scheduling Software System. Icao Number Change Utility cannot be run right now. Other users may be currently logged in. Please check the Logged-In Users screen.", "confirmCancelCallBackFn", 400, 150, null, ModuleNameConstants.SysAdmin.SystemAdmin);
                            }
                            else
                            {
                                IsLock = true;
                                RadWindowManager1.RadAlert("Records are being used by another user...Please use record lock manager screen to unlock.", 300, 150, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackFn");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        /// <summary>
        /// Change button OnClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("../../Home.aspx");
        }

        protected void Clearform()
        {
            tbOldICAO.Text = string.Empty;
            lbcvOldICAO.Text = string.Empty;
            hdnOldAirportID.Value = "";
            tbNewICAO.Text = string.Empty;
            lbOldICAO.Text = string.Empty;
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {

        }
    }
}