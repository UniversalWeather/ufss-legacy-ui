﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Telerik.Web.UI;
using FlightPak.Common;

namespace FlightPak.Web.Views.Settings.SystemAdministration
{
    public partial class ChangeTailNumber : BaseSecuredPage
    {
        private ExceptionManager exManager;
        public bool IsLock = false;
        public bool NoLoggedinuser = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        #region Authorization
                        if (!UserPrincipal.Identity._isSysAdmin)
                            Response.Redirect(ResolveUrl("~/Views/ErrorPages/AccessDenied.aspx"));
                        #endregion

                        if (!IsPostBack)
                        {
                            //Check for logged users and locked records
                            CheckLoggedUsersAndRedirect();

                            if (NoLoggedinuser)
                                checkLockRecordsAndRedirect();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }


        #region "NFR Logged Users"
        private bool CheckNoLoggedUsers()
        {
            using (CommonService.CommonServiceClient objComsvc = new CommonService.CommonServiceClient())
            {
                bool returnVal = true;
                List<FlightPak.Web.CommonService.GetAllLoggedUsersByCustomerId> loggedInUsers = new List<CommonService.GetAllLoggedUsersByCustomerId>();
                var objRetVal = objComsvc.GetAllLoggedInUsers(false);
                if (objRetVal.ReturnFlag)
                {
                    loggedInUsers = objRetVal.EntityList;
                    if (loggedInUsers != null && loggedInUsers.Count > 0)
                    {
                        returnVal = false;
                        NoLoggedinuser = false;
                    }
                }
                return returnVal;
            }
        }

        private void CheckLoggedUsersAndRedirect()
        {
            if (!CheckNoLoggedUsers())
                //PP23012013
                RadWindowManager1.RadConfirm("All other users MUST be out of Flight Scheduling Software System. Tail No. Change Utility cannot be run right now. Other users may be currently logged in. Please check the Logged-In Users screen.", "confirmCancelCallBackFn", 400, 150, null, ModuleNameConstants.SysAdmin.SystemAdmin);
        }

        #endregion

        #region "NFR Locked Records"
        private bool checkLockRecords()
        {
            using (CommonService.CommonServiceClient common = new CommonService.CommonServiceClient())
            {
                bool returnLockVal = true;
                List<FlightPak.Web.CommonService.GetAllLocks> lockedRecords = new List<CommonService.GetAllLocks>();
                var LockData = common.GetAllLocks();
                if (LockData.ReturnFlag == true)
                {
                    lockedRecords = LockData.EntityList;
                    if (lockedRecords != null && lockedRecords.Count > 0)
                    {
                        returnLockVal = false;
                    }
                }
                return returnLockVal;
            }
        }

        private void checkLockRecordsAndRedirect()
        {
            if (!checkLockRecords())
            {
                RadWindowManager1.RadAlert("Records are being used by another user...Please use record lock manager screen to unlock.", 300, 150, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackFn");
                //PP23012013(Deleted IsLock = true;)
            }
        }

        #endregion

        protected void btnYesLogin_Click(object sender, EventArgs e)
        {
            //Redirection for logged users and locked records
            Response.Redirect("LockManager.aspx"); //PP23012013
        }

        //PP23012013
        protected void btnCancelYes_Click(object sender, EventArgs e)
        {
            Response.Redirect("LoggedInUsers.aspx");
        }

        protected void btnCancelNo_Click(object sender, EventArgs e)
        {
            //Redirection for logged users and locked records

            //Response.Redirect("../../Home.aspx");
            Response.Redirect(ResolveUrl("~/Views/Settings/Default.aspx?Screen=ChangeTail"));
        }
        //PP23012013

        /// <summary>
        /// To validate Old Tail Number 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OldTailNumber_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbcvOldTailNumber.Text = string.Empty;
                        if (!string.IsNullOrEmpty(tbOldTailNumber.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.GetAllFleetForPopup> FleetLists = new List<FlightPak.Web.FlightPakMasterService.GetAllFleetForPopup>();
                                var objRetVal = objDstsvc.GetAllFleetForPopupList();
                                if (objRetVal.ReturnFlag)
                                {
                                    FleetLists = objRetVal.EntityList.Where(x => x.TailNum.ToString().ToUpper().Trim().Equals(tbOldTailNumber.Text.ToUpper().Trim())).ToList();
                                    if (FleetLists != null && FleetLists.Count > 0)
                                    {
                                        hdnOldFleetID.Value = ((FlightPak.Web.FlightPakMasterService.GetAllFleetForPopup)FleetLists[0]).FleetID.ToString();
                                        lbcvOldTailNumber.Text = "";
                                    }
                                    else
                                    {
                                        lbcvOldTailNumber.Text = System.Web.HttpUtility.HtmlEncode("Invalid Tail No.");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbOldTailNumber);
                                        hdnOldFleetID.Value = "";
                                    }
                                }
                                else
                                {
                                    lbcvOldTailNumber.Text = System.Web.HttpUtility.HtmlEncode("Invalid Tail No.");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbOldTailNumber);
                                    hdnOldFleetID.Value = "";
                                }
                            }
                        }
                        else
                        {
                            hdnOldFleetID.Value = "";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
        /// <summary>
        /// To validate that entered New Tail Number does not match in database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NewTailNumber_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbNewTailNumber.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.GetAllFleetForPopup> FleetLists = new List<FlightPak.Web.FlightPakMasterService.GetAllFleetForPopup>();
                                var objRetVal = objDstsvc.GetAllFleetForPopupList();
                                if (objRetVal.ReturnFlag)
                                {
                                    FleetLists = objRetVal.EntityList.Where(x => x.TailNum.ToString().ToUpper().Trim().Equals(tbNewTailNumber.Text.ToUpper().Trim())).ToList();
                                    if (FleetLists != null && FleetLists.Count > 0)
                                    {
                                        RadWindowManager1.RadAlert("WARNING- You have selected a New Tail No. that already exists in the database. This operation is NOT allowed. It would make two aircraft have the same Tail No.. Select another Tail No.", 350, 100, "System Messages", "alertCallBackNewTailFn", null);
                                        tbNewTailNumber.Text = "";
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
        /// <summary>
        /// Change button OnClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnChange_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (string.IsNullOrEmpty(hdnOldFleetID.Value))
                        {
                            RadWindowManager1.RadAlert("Old Tail No. Is Required.", 300, 100, "System Messages", "alertCallBackOldTailFn", null);
                        }
                        if (string.IsNullOrEmpty(tbNewTailNumber.Text) && !string.IsNullOrEmpty(hdnOldFleetID.Value))
                        {
                            RadWindowManager1.RadAlert("New Tail No. Is Required.", 300, 100, "System Messages", "alertCallBackNewTailFn", null);
                        }
                        if (!string.IsNullOrEmpty(tbOldTailNumber.Text.Trim()) && !string.IsNullOrEmpty(tbNewTailNumber.Text.Trim()) && tbOldTailNumber.Text.Trim() == tbNewTailNumber.Text.Trim())
                        {
                            RadWindowManager1.RadAlert("WARNING- You have selected a New Tail No. that already exists in the database. This operation is NOT allowed. It would make two aircraft have the same Tail No.. Select another Tail No.", 350, 100, "System Messages", "alertCallBackNewTailFn", null);
                            tbNewTailNumber.Text = "";
                        }
                        if (!string.IsNullOrEmpty(hdnOldFleetID.Value) && !string.IsNullOrEmpty(tbNewTailNumber.Text.Trim()))
                        {
                            if (string.IsNullOrEmpty(tbEffecTailChangeDate.Text))
                            {
                                RadWindowManager1.RadConfirm("Are you sure you want to change Tail No. " + tbOldTailNumber.Text + " To " + tbNewTailNumber.Text + " throughout FlightPak?", "confirmCallBackChangeTailThrouFn", 300, 100, null, "Flight Scheduling Software System");
                            }
                            else
                            {
                                RadWindowManager1.RadConfirm("Are you sure you want to change Tail No. " + tbOldTailNumber.Text + " To " + tbNewTailNumber.Text + " Effective Date " + tbEffecTailChangeDate.Text + "?", "confirmCallBackChangeTailFn", 300, 100, null, "Flight Scheduling Software System");
                            }


                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
        /// <summary>
        /// Change button OnClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Response.Redirect("../../Home.aspx");

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }

                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
        /// <summary>
        /// To Return bool value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Check for logged users and locked records
                        if (CheckNoLoggedUsers() && checkLockRecords())
                        {
                            using (CommonService.CommonServiceClient CommonSvc = new CommonService.CommonServiceClient())
                            {
                                Int64 oldFleetID = 0;
                                DateTime? effectiveDateTime = null;
                                if (!string.IsNullOrEmpty(hdnOldFleetID.Value))
                                    oldFleetID = Convert.ToInt64(hdnOldFleetID.Value.Trim());
                                if (!string.IsNullOrEmpty(tbEffecTailChangeDate.Text))
                                    effectiveDateTime = Convert.ToDateTime(tbEffecTailChangeDate.Text);

                                CommonSvc.LockCustomer();
                                bool result = CommonSvc.ChangeFleet(oldFleetID, tbNewTailNumber.Text, effectiveDateTime);
                                CommonSvc.UnlockCustomer();
                                if (result)
                                {
                                    RadWindowManager1.RadAlert("All Tail Numbers Changed from " + tbOldTailNumber.Text + " To " + tbNewTailNumber.Text + " Effective Date " + tbEffecTailChangeDate.Text, 300, 100, "System Messages", "alertCallBackOldTailFn", null);
                                    Clearform();                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("Transaction failed.Try after sometime ", 300, 100, "System Messages", "alertCallBackOldTailFn", null);
                                }

                            }
                        }
                        else
                        {
                            if (!NoLoggedinuser)
                            {
                                //PP23012013
                                RadWindowManager1.RadConfirm("All other users MUST be out of Flight Scheduling Software System. Tail No. Change Utility cannot be run right now. Other users may be currently logged in. Please check the Logged-In Users screen.", "confirmCancelCallBackFn", 400, 150, null, ModuleNameConstants.SysAdmin.SystemAdmin);
                            }
                            else
                            {
                                IsLock = true;
                                RadWindowManager1.RadAlert("Records are being used by another user...Please use record lock manager screen to unlock.", 300, 150, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackFn");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }
        }
        /// <summary>
        /// To focus back to tail number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbNewTailNumber);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }

                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
        /// <summary>
        /// To clear form
        /// </summary>
        protected void Clearform()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbOldTailNumber.Text = string.Empty;
                        lbcvOldTailNumber.Text = string.Empty;
                        hdnOldFleetID.Value = "";
                        tbNewTailNumber.Text = string.Empty;
                        tbEffecTailChangeDate.Text = string.Empty;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }

                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
        /// <summary>
        /// To Return bool value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnthroughYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Check for logged users and locked records
                        if (CheckNoLoggedUsers() && checkLockRecords())
                        {
                            using (CommonService.CommonServiceClient CommonSvc = new CommonService.CommonServiceClient())
                            {
                                Int64 oldFleetID = 0;
                                DateTime? effectiveDateTime = null;
                                if (!string.IsNullOrEmpty(hdnOldFleetID.Value))
                                    oldFleetID = Convert.ToInt64(hdnOldFleetID.Value.Trim());
                                if (!string.IsNullOrEmpty(tbEffecTailChangeDate.Text))
                                    effectiveDateTime = Convert.ToDateTime(tbEffecTailChangeDate.Text);

                                CommonSvc.LockCustomer();
                                bool result = CommonSvc.ChangeFleet(oldFleetID, tbNewTailNumber.Text, effectiveDateTime);
                                CommonSvc.UnlockCustomer();
                                if (result)
                                {
                                    RadWindowManager1.RadAlert("All Tail Numbers Changed from " + tbOldTailNumber.Text + " To " + tbNewTailNumber.Text + " throughout Flight Scheduling Software", 300, 100, "System Messages", "alertCallBackOldTailFn", null);
                                    Clearform();
                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("Transaction failed.Try after sometime ", 300, 100, "System Messages", "alertCallBackOldTailFn", null);
                                }

                            }
                        }
                        else
                        {
                            if (!NoLoggedinuser)
                            {
                                //PP23012013
                                RadWindowManager1.RadConfirm("All other users MUST be out of Flight Scheduling Software System. Tail No. Change Utility cannot be run right now. Other users may be currently logged in. Please check the Logged-In Users screen.", "confirmCancelCallBackFn", 400, 150, null, ModuleNameConstants.SysAdmin.SystemAdmin);
                            }
                            else
                            {
                                IsLock = true;
                                RadWindowManager1.RadAlert("Records are being used by another user...Please use record lock manager screen to unlock.", 300, 150, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackFn");
                            }
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
        /// <summary>
        /// To focus to control tail number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnthroughNo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbNewTailNumber);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
        /// <summary>
        /// Ajax request
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
    }
}
