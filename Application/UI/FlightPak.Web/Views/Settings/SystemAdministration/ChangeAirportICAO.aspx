﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="ChangeAirportICAO.aspx.cs" Inherits="FlightPak.Web.Views.Settings.SystemAdministration.ChangeAirportICAO" %>

<asp:Content ID="Content2" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js">
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<%=ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript">

            function alertCallBackOldICAOFn(arg) {
                document.getElementById("<%=tbOldICAO.ClientID%>").focus();
            }

            function alertCallBackNewICAOFn(arg) {
                document.getElementById("<%=tbNewICAO.ClientID%>").focus();
            }

            function alertCallBackFn(arg) {
                document.getElementById('<%=btnYes.ClientID%>').click();
            }


            function confirmCallBackChangeAccountFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=ChangeICAOYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=ChangeICAONo.ClientID%>').click();
                }
            }

            function confirmCancelCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnCancelYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnCancelNo.ClientID%>').click();
                }
            }

        </script>
        <script type="text/javascript">
            function openWin(radWin) {

                var url = '';

                if (radWin == "radAirportPopup") {
                    url = "../../Settings/Airports/AirportMasterPopup.aspx?FromPage=" + "systemAdmin";
                }
                if (url != '') {
                    var oWnd = radopen(url, radWin);
                }
            }

            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

            function OnClientOldICAOClose(oWnd, args) {
                var combo = $find("<%= tbOldICAO.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%= tbOldICAO.ClientID%>").value = arg.ICAO.toUpperCase();
                        document.getElementById("<%= hdnOldAirportID.ClientID%>").innerHTML = arg.AirportID;
                        document.getElementById("<%= lbOldICAO.ClientID%>").innerHTML = arg.AirportName;
                        if (arg.ICAO != null)
                            document.getElementById("<%=lbcvOldICAO.ClientID%>").innerHTML = "";
                        var step = "tbOldICAO_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%= tbOldICAO.ClientID%>").value = "";
                        document.getElementById("<%= hdnOldAirportID.ClientID%>").innerHTML = "";
                        document.getElementById("<%= lbOldICAO.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function RequestStart(sender, args) {
                currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                currentUpdatedControl = "<%= DivExternalForm.ClientID %>";
                //show the loading panel over the updated control
                currentLoadingPanel.show(currentUpdatedControl);
            }
            function ResponseEnd() {
                //hide the loading panel and clean up the global variables
                if (currentLoadingPanel != null)
                    currentLoadingPanel.hide(currentUpdatedControl);
                currentUpdatedControl = null;
                currentLoadingPanel = null;
            }

        </script>
    </telerik:RadCodeBlock>
    <%--<telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableCdn="true">
    </telerik:RadScriptManager>--%>
    <telerik:RadAjaxManager ID="RadAjaxManager1" OnAjaxRequest="RadAjaxManager1_AjaxRequest"
        runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnChange">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbOldICAO">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbNewICAO">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
                </telerik:RadWindow>            
                <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientOldICAOClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <table width="100%">
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="tblspace_12" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="mnd_text padleft_10" colspan="2">
                                Select Airport ICAO To Change:
                            </td>
                        </tr>
                        <tr>
                            <td class="nav-3" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table class="border-box">
                                    <tr>
                                        <td>
                                            Old
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbOldICAO" MaxLength="4" runat="server" CssClass="text60" OnTextChanged="OldICAO_TextChanged"
                                                AutoPostBack="true">
                                            </asp:TextBox>
                                            <asp:HiddenField ID="hdnOldAirportID" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnOldICAO" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('radAirportPopup');return false;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td colspan="2" align="left">
                                            <asp:Label ID="lbOldICAO" runat="server" CssClass="input_no_bg" Visible="true">
                                            </asp:Label>
                                            <asp:Label ID="lbcvOldICAO" runat="server" CssClass="alert-text" Visible="true">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            New
                                        </td>
                                        <td colspan="2">
                                            <!--onBlur="return RemoveSpecialChars(this)"-->
                                            <asp:TextBox ID="tbNewICAO" MaxLength="4" runat="server" CssClass="text60" AutoPostBack="true"
                                                OnTextChanged="NewICAO_TextChanged">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="nav-3">
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2">
                                <asp:Button ID="btnChange" Text="Change" runat="server" CssClass="button" OnClick="btnChange_Click" />
                                <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="button" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table id="tblHidden" style="display: none;">
            <tr>
                <td>
                    <asp:Button ID="ChangeICAOYes" runat="server" Text="Button" OnClick="ChangeICAOYes_Click" />
                    <asp:Button ID="ChangeICAONo" runat="server" Text="Button" OnClick="ChangeICAONo_Click" />
                    <asp:Button ID="btnYes" runat="server" Text="Button" OnClick="btnYes_Click" />
                    <asp:Button ID="btnCancelYes" runat="server" Text="Button" OnClick="btnCancelYes_Click" />
                    <asp:Button ID="btnCancelNo" runat="server" Text="Button" OnClick="btnCancelNo_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
