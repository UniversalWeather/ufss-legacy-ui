﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="LockManager.aspx.cs" Inherits="FlightPak.Web.Views.Settings.SystemAdministration.LockManager" %>

<asp:Content ID="Content2" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js">
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <script type="text/javascript">
        var TotalChkBx;
        var Counter;

        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= this.gvLock.Rows.Count %>');

            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }

        function HeaderClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl =
       document.getElementById('<%= this.gvLock.ClientID %>');
            var TargetChildControl = "chkBxSelect";

            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");

            //Checked/Unchecked all the checkBoxes in side the GridView.
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' &&
                Inputs[n].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[n].checked = CheckBox.checked;

            //Reset Counter
            Counter = CheckBox.checked ? TotalChkBx : 0;
        }

        function ChildClick(CheckBox, HCheckBox) {
            //get target control.
            var HeaderCheckBox = document.getElementById(HCheckBox);

            //Modifiy Counter; 
            if (CheckBox.checked && Counter < TotalChkBx)
                Counter++;
            else if (Counter > 0)
                Counter--;

            //Change state of the header CheckBox.
            if (Counter < TotalChkBx)
                HeaderCheckBox.checked = false;
            else if (Counter == TotalChkBx)
                HeaderCheckBox.checked = true;
        }
    </script>
    <%--<telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableCdn="true">
    </telerik:RadScriptManager>--%>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    </telerik:RadWindowManager>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div class="tab-nav-top">
                    <span class="head-title">Record Lock Manager</span> <span class="tab-nav-icons">
                    </span>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="padding: 0px 0px 10px 0px;">
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:PlaceHolder ID="Report" runat="server"></asp:PlaceHolder>
                <asp:GridView DataKeyNames="FPLockID" ID="gvLock" runat="server" AutoGenerateColumns="False"
                    CssClass="GridViewStyle" GridLines="None" EmptyDataText="No Records Found." OnRowCommand="gvLock_RowCommand"
                    OnRowDataBound="gvLock_RowDataBound" OnRowDeleting="gvLock_RowDeleting" Width="100%">
                    <FooterStyle CssClass="GridViewFooterStyle" />
                    <RowStyle CssClass="GridViewRowStyle" />
                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                    <PagerStyle CssClass="GridViewPagerStyle" />
                    <AlternatingRowStyle CssClass="GridViewAlternatingRowStyle" />
                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                    <Columns>
                        <asp:BoundField DataField="FPLockID" HeaderText="Lock ID" />
                        <asp:BoundField DataField="customerID" HeaderText="Customer ID" Visible="false" />
                        <asp:BoundField DataField="EntityType" HeaderText="Entity Type" />
                        <asp:BoundField DataField="EntityPrimaryKey" HeaderText="Entity Primary Key" />
                        <asp:BoundField DataField="LockReqUID" Visible="false" HeaderText="Locked By" />
                        <asp:BoundField DataField="Fullname" HeaderText="Locked By" />
                        <asp:BoundField DataField="EmailID" HeaderText="E-mail" />
                        <asp:BoundField DataField="PhoneNum" HeaderText="Phone" />
                        <asp:BoundField DataField="LockReqStartTime" HeaderText="Locked On" />
                        <asp:TemplateField HeaderText="Select" HeaderStyle-Width="21%">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkBxSelect" runat="server" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkBxHeader" onclick="javascript:HeaderClick(this);" runat="server" />
                                <asp:LinkButton ID="lnkDeleteSelected" CommandArgument='<%# Eval("FPLockID") %>'
                                    CommandName="DeleteSelected" runat="server" Font-Bold="true" ForeColor="White">
         Unlock Selected</asp:LinkButton>
                            </HeaderTemplate>
                        </asp:TemplateField>
                        <%--<asp:TemplateField HeaderText="Select">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" CommandArgument='<%# Eval("FPLockID") %>' CommandName="Delete"
                                    runat="server">
         Delete Lock</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
