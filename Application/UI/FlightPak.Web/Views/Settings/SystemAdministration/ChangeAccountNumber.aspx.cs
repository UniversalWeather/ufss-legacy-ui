﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views
{
    public partial class ChangeAccountNumber : BaseSecuredPage
    {
        // Declarations
        private ExceptionManager exManager;
        public bool IsLock = false;
        public bool NoLoggedinuser = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        #region Authorization
                        if (!UserPrincipal.Identity._isSysAdmin)
                            Response.Redirect(ResolveUrl("~/Views/ErrorPages/AccessDenied.aspx"));
                        #endregion

                        //Check for logged users and locked records
                        if (!IsPostBack)
                        {
                            CheckLoggedUsersAndRedirect();

                            if (NoLoggedinuser)
                                CheckLockRecordsAndRedirect();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }


        #region "NFR Logged Users"
        private bool CheckNoLoggedUsers()
        {
            using (CommonService.CommonServiceClient objComsvc = new CommonService.CommonServiceClient())
            {
                bool returnVal = true;
                List<FlightPak.Web.CommonService.GetAllLoggedUsersByCustomerId> loggedInUsers = new List<CommonService.GetAllLoggedUsersByCustomerId>();
                var objRetVal = objComsvc.GetAllLoggedInUsers(false);
                if (objRetVal.ReturnFlag)
                {
                    loggedInUsers = objRetVal.EntityList;
                    if (loggedInUsers != null && loggedInUsers.Count > 0)
                    {
                        returnVal = false;
                        NoLoggedinuser = false;
                    }
                }
                return returnVal;
            }
        }

        private void CheckLoggedUsersAndRedirect()
        {
            if (!CheckNoLoggedUsers())
                //PP23012013
                RadWindowManager1.RadConfirm("All other users MUST be out of Flight Scheduling Software System. Account No. Change Utility cannot be run right now. Other users may be currently logged in. Please check the Logged-In Users screen.", "confirmCancelCallBackFn", 400, 150, null, ModuleNameConstants.SysAdmin.SystemAdmin);
        }

        #endregion

        #region "NFR Locked Records"
        private bool CheckLockRecords()
        {
            using (CommonService.CommonServiceClient common = new CommonService.CommonServiceClient())
            {
                bool returnLockVal = true;
                List<FlightPak.Web.CommonService.GetAllLocks> lockedRecords = new List<CommonService.GetAllLocks>();
                var LockData = common.GetAllLocks();
                if (LockData.ReturnFlag == true)
                {
                    lockedRecords = LockData.EntityList;
                    if (lockedRecords != null && lockedRecords.Count > 0)
                    {
                        returnLockVal = false;
                    }
                }
                return returnLockVal;
            }
        }

        private void CheckLockRecordsAndRedirect()
        {
            if (!CheckLockRecords())
            {
                RadWindowManager1.RadAlert("Records are being used by another user...Please use record lock manager screen to unlock.", 300, 150, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackFn");
                //PP23012013(Deleted IsLock = true;)
            }
        }

        #endregion

        /// <summary>
        /// To Calidate Old Account Number on Text Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OldAccountNo_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbOldAccount.Text = string.Empty;
                        lbcvOldAccount.Text = string.Empty;
                        if (!string.IsNullOrEmpty(tbOldAccount.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.GetAccounts> AccountLists = new List<FlightPak.Web.FlightPakMasterService.GetAccounts>();
                                var objRetVal = objDstsvc.GetAllAccountList();
                                if (objRetVal.ReturnFlag)
                                {
                                    AccountLists = objRetVal.EntityList.Where(x => x.AccountNum.ToString().ToUpper().Trim().Equals(tbOldAccount.Text.ToUpper().Trim())).ToList();
                                    if (AccountLists != null && AccountLists.Count > 0)
                                    {
                                        lbOldAccount.Text = System.Web.HttpUtility.HtmlEncode(((FlightPak.Web.FlightPakMasterService.GetAccounts)AccountLists[0]).AccountDescription);
                                        hdnOldAccount.Value = ((FlightPak.Web.FlightPakMasterService.GetAccounts)AccountLists[0]).AccountID.ToString();
                                        lbcvOldAccount.Text = string.Empty;
                                    }
                                    else
                                    {
                                        lbcvOldAccount.Text = System.Web.HttpUtility.HtmlEncode("Invalid Account Number.");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbOldAccount);
                                        hdnOldAccount.Value = string.Empty;
                                    }
                                }
                                else
                                {
                                    lbcvOldAccount.Text = System.Web.HttpUtility.HtmlEncode("Invalid Account Number.");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbOldAccount);
                                    hdnOldAccount.Value = string.Empty;
                                }
                            }
                        }
                        else
                        {
                            lbOldAccount.Text = string.Empty;
                            hdnOldAccount.Value = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        /// <summary>
        /// To Validate New Account Number on Text Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NewAccountNo_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbNewAccount.Text = string.Empty;
                        lbcvNewAccount.Text = string.Empty;
                        if (!string.IsNullOrEmpty(tbNewAccount.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.GetAccounts> AccountLists = new List<FlightPak.Web.FlightPakMasterService.GetAccounts>();
                                var objRetVal = objDstsvc.GetAllAccountList();
                                if (objRetVal.ReturnFlag)
                                {
                                    AccountLists = objRetVal.EntityList.Where(x => x.AccountNum.ToString().ToUpper().Trim().Equals(tbNewAccount.Text.ToUpper().Trim())).ToList();
                                    if (AccountLists != null && AccountLists.Count > 0)
                                    {
                                        lbNewAccount.Text = System.Web.HttpUtility.HtmlEncode(((FlightPak.Web.FlightPakMasterService.GetAccounts)AccountLists[0]).AccountDescription);
                                        hdnNewAccount.Value = ((FlightPak.Web.FlightPakMasterService.GetAccounts)AccountLists[0]).AccountID.ToString();
                                        lbcvNewAccount.Text = string.Empty;
                                    }
                                    else
                                    {
                                        lbcvNewAccount.Text = System.Web.HttpUtility.HtmlEncode("Invalid Account Number.");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbNewAccount);
                                        hdnNewAccount.Value = string.Empty;
                                    }
                                }
                                else
                                {
                                    lbcvNewAccount.Text = System.Web.HttpUtility.HtmlEncode("Invalid Account Number.");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbNewAccount);
                                    hdnNewAccount.Value = string.Empty;
                                }
                            }
                        }
                        else
                        {
                            lbNewAccount.Text = string.Empty;
                            hdnNewAccount.Value = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        /// <summary>
        /// Change Button Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Change_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (string.IsNullOrEmpty(hdnOldAccount.Value))
                        {
                            RadWindowManager1.RadAlert("Old Account Number Is Required.", 300, 100, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackOldAccFn", null);
                        }
                        else if (string.IsNullOrEmpty(hdnNewAccount.Value) && !string.IsNullOrEmpty(hdnOldAccount.Value))
                        {
                            RadWindowManager1.RadAlert("New Account Number Is Required.", 300, 100, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackNewAccFn", null);
                        }
                        else if (!string.IsNullOrEmpty(hdnNewAccount.Value) && !string.IsNullOrEmpty(hdnOldAccount.Value) && hdnNewAccount.Value == hdnOldAccount.Value)
                        {
                            RadWindowManager1.RadAlert("Old And New Account Numbers Cannot Be The Same.", 350, 100, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackOldAccFn", null);
                        }
                        else if (!string.IsNullOrEmpty(hdnOldAccount.Value) && !string.IsNullOrEmpty(hdnNewAccount.Value) && hdnNewAccount.Value != hdnOldAccount.Value)
                        {
                            RadWindowManager1.RadConfirm("WARNING - Are you sure you want to change Account No?", "confirmCallBackChangeAccountFn", 300, 100, null, ModuleNameConstants.SysAdmin.SystemAdmin);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        protected void ChangeAccountNo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbNewAccount);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        protected void ChangeAccountYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Check for logged users and locked records
                        if (CheckNoLoggedUsers() && CheckLockRecords())
                        {
                            using (CommonService.CommonServiceClient CommonSvc = new CommonService.CommonServiceClient())
                            {
                                bool result = false;
                                long oldAccID = Convert.ToInt64(hdnOldAccount.Value);
                                long newAccID = Convert.ToInt64(hdnNewAccount.Value);
                                string oldAccNum = tbOldAccount.Text;
                                string newAccNum = tbNewAccount.Text;

                                CommonSvc.LockCustomer();
                                result = CommonSvc.ChangeAccountNumber(oldAccID, newAccID, oldAccNum, newAccNum);
                                CommonSvc.UnlockCustomer();
                                if (result)
                                {
                                    RadWindowManager1.RadAlert("All Account Numbers Changed.", 300, 100, ModuleNameConstants.SysAdmin.SystemAdmin, null, null);
                                    Clearform();
                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("Error in Changing Account Numbers!", 300, 100, ModuleNameConstants.SysAdmin.SystemAdmin, null, null);
                                }
                            }
                        }
                        else
                        {
                            if (!NoLoggedinuser)
                            {
                                //PP23012013
                                RadWindowManager1.RadConfirm("All other users MUST be out of Flight Scheduling Software System. Account No. Change Utility cannot be run right now. Other users may be currently logged in. Please check the Logged-In Users screen.", "confirmCancelCallBackFn", 400, 150, null, ModuleNameConstants.SysAdmin.SystemAdmin);
                            }
                            else
                            {
                                IsLock = true;
                                RadWindowManager1.RadAlert("Records are being used by another user...Please use record lock manager screen to unlock.", 300, 150, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackFn");
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        /// <summary>
        /// Cancel Button Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Response.Redirect("../../Home.aspx");

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        /// <summary>
        /// Method to Clear Form
        /// </summary>
        protected void Clearform()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbOldAccount.Text = string.Empty;
                lbOldAccount.Text = string.Empty;
                lbcvOldAccount.Text = string.Empty;
                hdnOldAccount.Value = string.Empty;
                tbNewAccount.Text = string.Empty;
                lbNewAccount.Text = string.Empty;
                lbcvNewAccount.Text = string.Empty;
                hdnNewAccount.Value = string.Empty;
                RadAjaxManager.GetCurrent(Page).FocusControl(tbOldAccount);
            }
        }

        /// <summary>
        /// Ajax Request Events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {

            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            //Redirection for logged users and locked records
            Response.Redirect("LockManager.aspx"); //PP23012013
        }

        //PP23012013
        protected void btnCancelYes_Click(object sender, EventArgs e)
        {
            Response.Redirect("LoggedInUsers.aspx");
        }

        protected void btnCancelNo_Click(object sender, EventArgs e)
        {
            //Redirection for logged users and locked records

            //Response.Redirect("../../Home.aspx");
            Response.Redirect(ResolveUrl("~/Views/Settings/Default.aspx?Screen=ChangeAccount"));
        }
        //PP23012013
    }
}
