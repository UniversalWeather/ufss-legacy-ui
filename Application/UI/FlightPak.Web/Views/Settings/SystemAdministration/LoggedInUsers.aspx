﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="LoggedInUsers.aspx.cs" Inherits="FlightPak.Web.Views.Settings.SystemAdministration.LoggedInUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js">
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <script type="text/javascript">
        var TotalChkBx;
        var Counter;

        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= this.gvLoggedInUsers.Rows.Count %>');

            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }

        function HeaderClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl =
       document.getElementById('<%= this.gvLoggedInUsers.ClientID %>');
            var TargetChildControl = "chkBxSelect";

            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");

            //Checked/Unchecked all the checkBoxes in side the GridView.
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' &&
                Inputs[n].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[n].checked = CheckBox.checked;

            //Reset Counter
            Counter = CheckBox.checked ? TotalChkBx : 0;
        }

        function ChildClick(CheckBox, HCheckBox) {
            //get target control.
            var HeaderCheckBox = document.getElementById(HCheckBox);

            //Modifiy Counter; 
            if (CheckBox.checked && Counter < TotalChkBx)
                Counter++;
            else if (Counter > 0)
                Counter--;

            //Change state of the header CheckBox.
            if (Counter < TotalChkBx)
                HeaderCheckBox.checked = false;
            else if (Counter == TotalChkBx)
                HeaderCheckBox.checked = true;
        }
    </script>
    <%--<telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableCdn="true">
    </telerik:RadScriptManager>--%>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    </telerik:RadWindowManager>
    
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <div class="tab-nav-top">
                        <span class="head-title">Logged In Users</span> <span class="tab-nav-icons"></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="padding: 0px 0px 10px 0px;">
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:PlaceHolder ID="Report" runat="server"></asp:PlaceHolder>
                    <asp:GridView DataKeyNames="UserName" ID="gvLoggedInUsers" runat="server" AutoGenerateColumns="False"
                        CssClass="GridViewStyle" GridLines="None" EmptyDataText="No Records Found." OnRowCommand="gvLoggedInUsers_RowCommand"
                        OnRowDataBound="gvLoggedInUsers_RowDataBound" OnRowDeleting="gvLoggedInUsers_RowDeleting"
                        Width="100%">
                        <FooterStyle CssClass="GridViewFooterStyle" />
                        <RowStyle CssClass="GridViewRowStyle" />
                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                        <PagerStyle CssClass="GridViewPagerStyle" />
                        <AlternatingRowStyle CssClass="GridViewAlternatingRowStyle" />
                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                        <Columns>
                            <asp:TemplateField HeaderText="Select" HeaderStyle-Width="25%">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkBxSelect" runat="server" Visible='<%# Eval("UserName").ToString().ToUpper().Trim() != base.UserPrincipal.Identity._name.ToString().ToUpper().Trim() %>' />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                <ItemStyle HorizontalAlign="left" VerticalAlign="Middle" />
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkBxHeader" onclick="javascript:HeaderClick(this);" runat="server" />
                                  
                                    <asp:LinkButton ID="lnkDeleteSelected" CommandArgument='<%# Eval("UserName") %>'
                                        CommandName="DeleteSelected" runat="server" Font-Bold="true" ForeColor="White" Font-Size="8px">Logoff User</asp:LinkButton>
                                  
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="UserName" HeaderText="User Name" HeaderStyle-Width="12%" />
                            <asp:TemplateField HeaderText="Full Name" HeaderStyle-Width="12%">
                                <ItemTemplate>
                                    <asp:Label ID="lbName" runat="server" Text='<%# Eval("FirstName") + " " + Eval("MiddleName")+ " " + Eval("LastName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="EmailID" HeaderText="E-mail ID" HeaderStyle-Width="10%" />
                            <asp:BoundField DataField="PhoneNum" HeaderText="Phone" HeaderStyle-Width="10%" />
                            <asp:BoundField DataField="LoginStartTime" HeaderText="Logged In (UTC)" HeaderStyle-Width="10%" DataFormatString="{0:MM/dd/yyyy HH:mm:ss}" />
                            <asp:BoundField DataField="LoginEndTIme" HeaderText="Last Log Out (UTC)" HeaderStyle-Width="10%" DataFormatString="{0:MM/dd/yyyy HH:mm:ss}" />
                            <asp:BoundField DataField="UserGroup" HeaderText="User Group" HeaderStyle-Width="15%" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    
</asp:Content>
