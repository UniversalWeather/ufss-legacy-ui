﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="ChangeCustomerCode.aspx.cs" Inherits="FlightPak.Web.Views.Settings.SystemAdministration.ChangeCustomerCode" %>

<asp:Content ID="Content2" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js">
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<%=ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript">

            function alertCallBackOldCQCusFn(arg) {
                document.getElementById("<%=tbOldCustomerCode.ClientID%>").focus();
            }

            function alertCallBackNewCQCusFn(arg) {
                document.getElementById("<%=tbNewCustomerCode.ClientID%>").focus();
            }

            function rebindgrid() {

            }

            function confirmCallBackChangeCQCusFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnNo.ClientID%>').click();
                }
            }

            function confirmCallBackChangeCQCusThrouFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnthroughYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnthroughNo.ClientID%>').click();
                }
            }
            function alertCallBackFn(arg) {
                document.getElementById('<%=btnYesLogin.ClientID%>').click();
            }
            function confirmCancelCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnCancelYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnCancelNo.ClientID%>').click();
                }
            }
            var currentTextBox = null;
            var currentDatePicker = null;

            //This method is called to handle the onclick and onfocus client side events for the texbox
            function showPopup(sender, e) {
                currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                //this gets a reference to the datepicker, which will be shown, to facilitate
                //the selection of a date
                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                //this variable is used to store a reference to the date picker, which is currently
                //active
                currentDatePicker = datePicker;

                //this method first parses the date, that the user entered or selected, and then
                //sets it as a selected date to the picker

                datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));


                //the code lines below show the calendar, which is used to select a date. The showPopup
                //function takes three arguments - the x and y coordinates where to show the calendar, as
                //well as its height, derived from the offsetHeight property of the textbox
                var position = datePicker.getElementPosition(currentTextBox);
                datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
            }

            //this handler is used to set the text of the TextBox to the value of selected from the popup
            function dateSelected(sender, args) {

                if (currentTextBox != null) {
                    var step = "Date_TextChanged";
                    if (currentTextBox.value != args.get_newValue()) {
                        currentTextBox.value = args.get_newValue();
                        var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                }
            }

            function hide() {
                var datePicker = $find("<%= RadDatePicker1.ClientID%>");
                datePicker.hidePopup();
                return true;
            }


            function tbDate_OnKeyDown(sender, event) {
                var step = "Date_TextChanged";
                if (event.keyCode == 9) {
                    parseDate(sender, event);
                    var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                    datePicker.hidePopup();
                    var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                    if (step) {
                        ajaxManager.ajaxRequest(step);
                    }
                    return true;
                }
            }

            //this function is used to parse the date entered or selected by the user
            function parseDate(sender, e) {
                if (currentDatePicker != null) {
                    var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                    var dateInput = currentDatePicker.get_dateInput();
                    var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                    sender.value = formattedDate;
                }
            }

            function ValidateDate(control) {
                var MinDate = new Date("01/01/1900");
                var MaxDate = new Date("12/31/2100");
                var SelectedDate;
                if (control.value != "") {
                    SelectedDate = new Date(control.value);
                    if ((SelectedDate < MinDate) || (SelectedDate > MaxDate)) {
                        alert("Please enter/select Date between 01/01/1900 and 12/31/2100");
                        control.value = "";
                    }
                }
                return false;
            }

        </script>
        <script type="text/javascript">
            function openWin(radWin) {

                var url = '';

                if (radWin == "radCQCPopups") {
                    url = "../../CharterQuote/CharterQuoteCustomerPopup.aspx?fromPage=" + "SystemTools";
                }
                if (url != '') {
                    var oWnd = radopen(url, radWin);
                }
            }

            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

            function OnClientOldCqCustomerCodeClose(oWnd, args) {
                var combo = $find("<%= tbOldCustomerCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%= tbOldCustomerCode.ClientID%>").value = arg.CQCustomerCD;
                        document.getElementById("<%= hdnOldCQCustomerID.ClientID%>").innerHTML = arg.CQCustomerID;
                        if (arg.FleetID != null)
                            document.getElementById("<%=lbcvOldCustomerCode.ClientID%>").innerHTML = "";
                        var step = "OldCustomerCode_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%= tbOldCustomerCode.ClientID%>").value = "";
                        document.getElementById("<%= hdnOldCQCustomerID.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function RequestStart(sender, args) {
                currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                currentUpdatedControl = "<%= DivExternalForm.ClientID %>";
                //show the loading panel over the updated control
                currentLoadingPanel.show(currentUpdatedControl);
            }
            function ResponseEnd() {
                //hide the loading panel and clean up the global variables
                if (currentLoadingPanel != null)
                    currentLoadingPanel.hide(currentUpdatedControl);
                currentUpdatedControl = null;
                currentLoadingPanel = null;
            }

        </script>
    </telerik:RadCodeBlock>
    <%--<telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableCdn="true">
    </telerik:RadScriptManager>--%>
    <telerik:RadAjaxManager ID="RadAjaxManager1" OnAjaxRequest="RadAjaxManager1_AjaxRequest"
        runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnChange">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbOldCustomerCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbNewCustomerCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                    ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="radCQCPopups" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientOldCqCustomerCodeClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radCQCPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <table width="100%">
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="tblspace_12">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="mnd_text padleft_10">
                                Select CQ Customer Code To Change:
                            </td>
                        </tr>
                        <tr>
                            <td class="nav-3">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="border-box">
                                    <tr>
                                        <td>
                                            Old
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbOldCustomerCode" runat="server" CssClass="text60" OnTextChanged="OldCustomerCode_TextChanged"
                                                MaxLength="5" AutoPostBack="true">
                                            </asp:TextBox>
                                            <asp:HiddenField ID="hdnOldCQCustomerID" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnOldCustomerCode" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('radCQCPopups');return false;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td colspan="2">
                                            <asp:Label ID="lbcvOldCustomerCode" runat="server" CssClass="alert-text" Visible="true">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            New
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox ID="tbNewCustomerCode" runat="server" CssClass="text60" AutoPostBack="true"
                                                MaxLength="5" OnTextChanged="NewCustomerCode_TextChanged">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Effective Customer Code Change Date
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox ID="tbEffecCustomerChangeDate" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                onclick="showPopup(this, event);" AutoPostBack="true" onkeydown="return tbDate_OnKeyDown(this, event);"
                                                CssClass="text60" runat="server" onchange="parseDate(this, event);javascript:return ValidateDate(this);"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="nav-3">
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnChange" Text="Change" runat="server" CssClass="button" OnClick="btnChange_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="button" OnClick="btnCancel_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table id="tblHidden" style="display: none;">
                        <tr>
                            <td>
                                <asp:Button ID="btnYes" runat="server" Text="Button" OnClick="btnYes_Click" />
                                <asp:Button ID="btnNo" runat="server" Text="Button" OnClick="btnNo_Click" />
                            </td>
                        </tr>
                    </table>
                    <table id="Table1" style="display: none;">
                        <tr>
                            <td>
                                <asp:Button ID="btnthroughYes" runat="server" Text="Button" OnClick="btnthroughYes_Click" />
                                <asp:Button ID="btnthroughNo" runat="server" Text="Button" OnClick="btnthroughNo_Click" />
                                <asp:Button ID="btnYesLogin" runat="server" Text="Button" OnClick="btnYesLogin_Click" />
                                <asp:Button ID="btnCancelYes" runat="server" Text="Button" OnClick="btnCancelYes_Click" />
                                <asp:Button ID="btnCancelNo" runat="server" Text="Button" OnClick="btnCancelNo_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
