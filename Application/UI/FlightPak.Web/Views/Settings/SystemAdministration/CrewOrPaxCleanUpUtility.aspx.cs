﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Telerik.Web.UI;
using System.IO;
using System.Text;
using FlightPak.Web.CommonService;
using FlightPak.Common;

namespace FlightPak.Web.Views.Settings.SystemAdministration
{
    public partial class CrewOrPaxCleanUpUtility : BaseSecuredPage
    {
        private ExceptionManager exManager;
        string DateFormat, UserName;
        public RadDatePicker DatePicker { get { return this.RadDatePicker1; } }
        public bool IsLock = false;
        public bool NoLoggedinuser = true;
        /// <summary>
        /// To set the application date format for date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {


                        RadDatePicker1.DateInput.DateFormat = "MM/dd/yyyy";
                        RadDatePicker1.DateInput.DisplayDateFormat = "MM/dd/yyyy";
                        DatePicker.DateInput.DateFormat = "MM/dd/yyyy";
                        DatePicker.DateInput.DisplayDateFormat = "MM/dd/yyyy";


                        if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            RadDatePicker1.DateInput.DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            RadDatePicker1.DateInput.DisplayDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            DatePicker.DateInput.DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            DatePicker.DateInput.DisplayDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        #region Authorization
                        if (!UserPrincipal.Identity._isSysAdmin)
                            Response.Redirect(ResolveUrl("~/Views/ErrorPages/AccessDenied.aspx"));
                        #endregion
                        //Check for logged users and locked records
                        if (!IsPostBack)
                        {
                            CheckLoggedUsersAndRedirect();
                            if (NoLoggedinuser)
                                checkLockRecordsAndRedirect();
                        }
                      
                        if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                            UserName = UserPrincipal.Identity._name;
                        }                       

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }




        }
        #region "NFR Logged Users"
        private bool CheckNoLoggedUsers()
        {
            using (CommonService.CommonServiceClient objComsvc = new CommonService.CommonServiceClient())
            {
                bool returnVal = true;
                List<FlightPak.Web.CommonService.GetAllLoggedUsersByCustomerId> loggedInUsers = new List<CommonService.GetAllLoggedUsersByCustomerId>();
                var objRetVal = objComsvc.GetAllLoggedInUsers(false);
                if (objRetVal.ReturnFlag)
                {
                    loggedInUsers = objRetVal.EntityList;
                    if (loggedInUsers != null && loggedInUsers.Count > 0)
                    {
                        returnVal = false;
                        NoLoggedinuser = false;
                    }
                }
                return returnVal;
            }
        }

        private void CheckLoggedUsersAndRedirect()
        {
            if (!CheckNoLoggedUsers())
                //PP23012013
                RadWindowManager1.RadConfirm("All other users MUST be out of Flight Scheduling Software System. Crew/Pax Table Clean-up Utility cannot be run right now. Other users may be currently logged in. Please check the Logged-In Users screen.", "confirmCancelCallBackFn", 400, 150, null, ModuleNameConstants.SysAdmin.SystemAdmin);
        }

        #endregion

        #region "NFR Locked Records"
        private bool checkLockRecords()
        {
            using (CommonService.CommonServiceClient common = new CommonService.CommonServiceClient())
            {
                bool returnLockVal = true;
                List<FlightPak.Web.CommonService.GetAllLocks> lockedRecords = new List<CommonService.GetAllLocks>();
                var LockData = common.GetAllLocks();
                if (LockData.ReturnFlag == true)
                {
                    lockedRecords = LockData.EntityList;
                    if (lockedRecords != null && lockedRecords.Count > 0)
                    {
                        returnLockVal = false;
                    }
                }
                return returnLockVal;
            }
        }

        private void checkLockRecordsAndRedirect()
        {
            if (!checkLockRecords())
            {
                RadWindowManager1.RadAlert("Records are being used by another user...Please use record lock manager screen to unlock.", 300, 150, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackFn");
                //PP23012013(Deleted IsLock = true;)
            }
        }

        #endregion

        protected void btnYes_Click(object sender, EventArgs e)
        {
            //Redirection for logged users and locked records
            Response.Redirect("LockManager.aspx"); //PP23012013
        }

        //PP23012013
        protected void btnCancelYes_Click(object sender, EventArgs e)
        {
            Response.Redirect("LoggedInUsers.aspx");
        }

        protected void btnCancelNo_Click(object sender, EventArgs e)
        {
            //Redirection for logged users and locked records

            //Response.Redirect("../../Home.aspx");
            Response.Redirect(ResolveUrl("~/Views/Settings/Default.aspx?Screen=CleanUPUtility"));
        }
        //PP23012013

        /// <summary>
        /// To Generate Report for Records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnGenerateReport_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbReportDate.Text))
                        {
                            using (CommonService.CommonServiceClient objGeneratecmc = new CommonService.CommonServiceClient())
                            {
                                DateTime reportDate = System.DateTime.Now;
                                bool showInactive;
                                if (!string.IsNullOrEmpty(tbReportDate.Text))
                                {
                                    reportDate = Convert.ToDateTime(FormatDate(tbReportDate.Text.Trim(), DateFormat));
                                }
                                string sortBy = radlstSortBy.SelectedValue.ToString();
                                if (chkInactive.Checked == true)
                                {
                                    showInactive = true;
                                }
                                else
                                {
                                    showInactive = false;
                                }
                                //Check for logged users and locked records
                                if (CheckNoLoggedUsers() && checkLockRecords())
                                {
                                    //objGeneratecmc.LockCustomer();
                                    //var resultCrewPAXCleanUpList = objGeneratecmc.GetCrewPAXCleanUpList(sortBy, showInactive, reportDate);
                                    //objGeneratecmc.UnlockCustomer();
                                    Session["TabSelect"] = "Db";
                                    Session["REPORT"] = "RptDBSysAdmnCrewPaxCleanUpUtility";
                                    Session["FORMAT"] = "PDF";
                                    Session["PARAMETER"] = "UserCD=" + UserName.Trim() + ";OrderBy=" + sortBy + ";ShowInactive=" + showInactive + ";ReportDate=" + reportDate;
                                    Response.Redirect("../../Reports/ReportRenderView.aspx");

                                }
                                else
                                {
                                    //PP23012013
                                    RadWindowManager1.RadConfirm("All other users MUST be out of Flight Scheduling Software System. Crew/PAX Table Clean-up Utility cannot be run right now. Other users may be currently logged in. Please check the Logged-In Users screen.", "confirmCancelCallBackFn", 400, 150, null, ModuleNameConstants.SysAdmin.SystemAdmin);
                                }
                            }
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Please select Report Date.", 250, 100, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackCleanUp", null);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
        /// <summary>
        /// To Inactivate Records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnInactivateRecords_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbReportDate.Text))
                        {
                            using (CommonService.CommonServiceClient objGeneratecmc = new CommonService.CommonServiceClient())
                            {

                               
                                DateTime reportDate = System.DateTime.Now;
                                bool showInactive;
                                if (!string.IsNullOrEmpty(tbReportDate.Text))
                                {
                                    reportDate = Convert.ToDateTime(FormatDate(tbReportDate.Text.Trim(), DateFormat));
                                }
                                string sortBy = radlstSortBy.SelectedValue.ToString();
                                if (chkInactive.Checked == true)
                                {
                                    showInactive = true;
                                }
                                else
                                {
                                    showInactive = false;
                                }
                                //Check for logged users and locked records
                                if (CheckNoLoggedUsers() && checkLockRecords())
                                {
                                    if (hdnInactivate.Value != "yes")
                                    {
                                        objGeneratecmc.LockCustomer();
                                        var resultCrewPAXCleanUpList = objGeneratecmc.GetCrewPAXCleanUpList(sortBy, false, reportDate);
                                        //var resultInactivateCrewPAXForCleanUp = objGeneratecmc.GetInactivateCrewPAXForCleanUp(sortBy, showInactive, reportDate).EntityList;
                                        objGeneratecmc.UnlockCustomer();
                                        if (resultCrewPAXCleanUpList.EntityList.Count() > 0)
                                        {
                                            RadWindowManager1.RadConfirm("Are you sure you want to mark Crew/Pax Inactive who have no duty/have not flown after " + reportDate.ToShortDateString(), "confirmCallBackFn", 500, 30, null, ModuleNameConstants.SysAdmin.SystemAdmin);
                                        }
                                        else
                                        {
                                            RadWindowManager1.RadAlert("All Crew members and Passengers have duty/have flown after " + reportDate.ToShortDateString(), 350, 100, ModuleNameConstants.SysAdmin.SystemAdmin, "", null);
                                        }
                                    }
                                    if (hdnInactivate.Value == "yes")
                                    {
                                        hdnInactivate.Value = string.Empty;
                                        objGeneratecmc.LockCustomer();
                                        var resultInactivateCrewPAXForCleanUp = objGeneratecmc.GetInactivateCrewPAXForCleanUp(sortBy, showInactive, reportDate).EntityList;
                                        objGeneratecmc.UnlockCustomer();
                                        int crewForCleanUpCount = resultInactivateCrewPAXForCleanUp.Where(x => x.CrewOrPax.ToUpper().Trim().Equals("C")).Count();
                                        int paxForCleanUpCount = resultInactivateCrewPAXForCleanUp.Where(x => x.CrewOrPax.ToUpper().Trim().Equals("P")).Count();
                                        if (crewForCleanUpCount == 0 && paxForCleanUpCount == 0)
                                        {
                                            RadWindowManager1.RadAlert("All Crew members and Passengers have duty/have flown after " + reportDate.ToShortDateString(), 350, 100, "SystemAdmin", "", null);
                                        }
                                        else
                                        {
                                            string name = string.Empty;
                                            if (UserPrincipal != null && UserPrincipal.Identity._name != null)
                                            {
                                                name = UserPrincipal.Identity._name;
                                            }
                                            name = "USER:" + name;
                                            string date = System.DateTime.UtcNow.ToShortDateString();
                                            string dateTime = System.DateTime.UtcNow.ToLocalTime().ToShortTimeString();
                                            date = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(date));
                                            var dateSeparator = string.Empty;
                                            var dotSeparator = date.IndexOf(".");
                                            var slashSeparator = date.IndexOf("/");
                                            var hyphenSeparator = date.IndexOf("-");
                                            if (dotSeparator != -1) { dateSeparator = "."; }
                                            else if (slashSeparator != -1) { dateSeparator = "/"; }
                                            else if (hyphenSeparator != 1) { dateSeparator = "-"; }
                                            string reportdate = "REPORT DATE:" + date;
                                            string header = "--------------------------------------------------------------------------------";
                                            string emptystring = "                                                                                ";
                                            int nameandreportlength = name.Length + reportdate.Length;
                                            string userName = name + emptystring.Substring(0, header.Length - nameandreportlength) + reportdate;
                                            Response.ContentType = "text/plain";
                                            Response.AddHeader("Content-Disposition", "attachment; filename=" + Server.UrlEncode("CREWPAX_CLEANUP_" + date.Replace(dateSeparator, "") + dateTime.Replace(":", "")));
                                            dateTime = date + " " + dateTime;
                                            StringBuilder PaxList = new StringBuilder();
                                            using (StreamWriter writer = new StreamWriter(Response.OutputStream, Encoding.UTF8))
                                            {
                                                writer.WriteLine(header);
                                                writer.WriteLine("                    CREW/PAX CLEAN UP UITILITY REPORT                           ");
                                                string encodedUserName = Microsoft.Security.Application.Encoder.HtmlEncode(userName);
                                                writer.WriteLine(encodedUserName);
                                                writer.WriteLine(header);

                                                if (crewForCleanUpCount == 0)
                                                {
                                                    writer.WriteLine("");
                                                    writer.WriteLine(" No Crew found to be inactivated.");
                                                }
                                                else
                                                {
                                                    writer.WriteLine("Crew Members:");
                                                    foreach (GetInactivateCrewPAXForCleanUp CrewPAX in resultInactivateCrewPAXForCleanUp)
                                                    {
                                                        if (CrewPAX.CrewOrPax.ToUpper().Trim() == "C")
                                                        {
                                                            string messageToDisplay = Microsoft.Security.Application.Encoder.HtmlEncode("            " + CrewPAX.CrewOrPAXCD + "                       " + CrewPAX.CrewOrPaxName);
                                                            writer.WriteLine(messageToDisplay);
                                                        }
                                                    }
                                                    writer.WriteLine(header);
                                                    writer.WriteLine("Total No. Of Crew Members marked Inactive :" + crewForCleanUpCount);

                                                }
                                                if (paxForCleanUpCount == 0)
                                                {
                                                    writer.WriteLine("");
                                                    writer.WriteLine(" No Passengers found to be inactivated.");
                                                    writer.WriteLine(header);
                                                }
                                                else
                                                {
                                                    writer.WriteLine("Pax Members:");
                                                    foreach (GetInactivateCrewPAXForCleanUp CrewPAX in resultInactivateCrewPAXForCleanUp)
                                                    {
                                                        if (CrewPAX.CrewOrPax.ToUpper().Trim() == "P")
                                                        {
                                                            string messageToDisplay = Microsoft.Security.Application.Encoder.HtmlEncode("            " + CrewPAX.CrewOrPAXCD + "                       " + CrewPAX.CrewOrPaxName);
                                                            writer.WriteLine(messageToDisplay);
                                                        }
                                                    }
                                                    writer.WriteLine(header);
                                                    writer.WriteLine("Total No. Of Pax Members marked Inactive :" + paxForCleanUpCount);

                                                }
                                                writer.WriteLine("");
                                                string encodedDate = Microsoft.Security.Application.Encoder.HtmlEncode(" DATETIME:" + dateTime);
                                                writer.WriteLine(encodedDate);


                                            }                                          
                                            Response.End();
                                            
                                        }
                                    }
                                }
                                else
                                {
                                    if (!NoLoggedinuser)
                                        RadWindowManager1.RadAlert("All other users MUST be out of Flight Scheduling Software System.Crew/Pax Table Clean-up  Utility cannot be run right now.Other users may be currently logged in. Please check the Logged-In Users screen.", 400, 150, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackFn");
                                    else
                                    {
                                        IsLock = true;
                                        RadWindowManager1.RadAlert("Records are being used by another user...Please use record lock manager screen to unlock.", 300, 150, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackFn");
                                    }  
                                }

                            }
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Please select Report Date.", 250, 100, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackCleanUp", null);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        /// <summary>
        /// Ajax Request Events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {

        }

        //protected void btnInactivated_Click(object sender, EventArgs e)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                 var resultInactivateCrewPAXForCleanUp = objGeneratecmc.GetInactivateCrewPAXForCleanUp(sortBy, showInactive, reportDate).EntityList;
        //                            objGeneratecmc.UnlockCustomer();
        //                            int crewForCleanUpCount = resultInactivateCrewPAXForCleanUp.Where(x => x.CrewOrPax.ToUpper().Trim().Equals("C")).Count();
        //                            int paxForCleanUpCount = resultInactivateCrewPAXForCleanUp.Where(x => x.CrewOrPax.ToUpper().Trim().Equals("P")).Count();
        //                            if (crewForCleanUpCount == 0 && paxForCleanUpCount == 0)
        //                            {
        //                                RadWindowManager1.RadAlert("All Crew members and Passengers have duty/have flown after " + reportDate.ToShortDateString(), 350, 100, "SystemAdmin", "", null);
        //                            }
        //                            else
        //                            {
        //                                string name = string.Empty;
        //                                if (UserPrincipal != null && UserPrincipal.Identity._name != null)
        //                                {
        //                                    name = UserPrincipal.Identity._name;
        //                                }
        //                                name = "USER:" + name;
        //                                string date = System.DateTime.UtcNow.ToShortDateString();
        //                                string dateTime = System.DateTime.UtcNow.ToLocalTime().ToShortTimeString();
        //                                date = String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(date));
        //                                var dateSeparator = string.Empty;
        //                                var dotSeparator = date.IndexOf(".");
        //                                var slashSeparator = date.IndexOf("/");
        //                                var hyphenSeparator = date.IndexOf("-");
        //                                if (dotSeparator != -1) { dateSeparator = "."; }
        //                                else if (slashSeparator != -1) { dateSeparator = "/"; }
        //                                else if (hyphenSeparator != 1) { dateSeparator = "-"; }
        //                                string reportdate = "REPORT DATE:" + date;
        //                                string header = "--------------------------------------------------------------------------------";
        //                                string emptystring = "                                                                                ";
        //                                int nameandreportlength = name.Length + reportdate.Length;
        //                                string userName = name + emptystring.Substring(0, header.Length - nameandreportlength) + reportdate;
        //                                Response.ContentType = "text/plain";
        //                                Response.AddHeader("Content-Disposition", "attachment; filename=" + Server.UrlEncode("CREWPAX_CLEANUP_" + date.Replace(dateSeparator, "") + dateTime.Replace(":", "")));
        //                                dateTime = date + " " + dateTime;
        //                                StringBuilder PaxList = new StringBuilder();
        //                                using (StreamWriter writer = new StreamWriter(Response.OutputStream, Encoding.UTF8))
        //                                {
        //                                    writer.WriteLine(header);
        //                                    writer.WriteLine("                    CREW/PAX CLEAN UP UITILITY REPORT                           ");
        //                                    writer.WriteLine(userName);
        //                                    writer.WriteLine(header);

        //                                    if (crewForCleanUpCount == 0)
        //                                    {
        //                                        writer.WriteLine("");
        //                                        writer.WriteLine(" No Crew found to be inactivated.");
        //                                    }
        //                                    else
        //                                    {
        //                                        writer.WriteLine("Crew Members:");
        //                                        foreach (GetInactivateCrewPAXForCleanUp CrewPAX in resultInactivateCrewPAXForCleanUp)
        //                                        {

        //                                            if (CrewPAX.CrewOrPax.ToUpper().Trim() == "C")
        //                                            {

        //                                                writer.WriteLine("            " + CrewPAX.CrewOrPAXCD + "                       " + CrewPAX.CrewOrPaxName);

        //                                            }

        //                                        }
        //                                        writer.WriteLine(header);
        //                                        writer.WriteLine("Total No. Of Crew Members marked Inactive :" + crewForCleanUpCount);

        //                                    }
        //                                    if (paxForCleanUpCount == 0)
        //                                    {
        //                                        writer.WriteLine("");
        //                                        writer.WriteLine(" No Passengers found to be inactivated.");
        //                                        writer.WriteLine(header);
        //                                    }
        //                                    else
        //                                    {
        //                                        writer.WriteLine("Pax Members:");
        //                                        foreach (GetInactivateCrewPAXForCleanUp CrewPAX in resultInactivateCrewPAXForCleanUp)
        //                                        {

        //                                            if (CrewPAX.CrewOrPax.ToUpper().Trim() == "P")
        //                                            {

        //                                                writer.WriteLine("            " + CrewPAX.CrewOrPAXCD + "                       " + CrewPAX.CrewOrPaxName);

        //                                            }

        //                                        }
        //                                        writer.WriteLine(header);
        //                                        writer.WriteLine("Total No. Of Pax Members marked Inactive :" + paxForCleanUpCount);

        //                                    }
        //                                    writer.WriteLine("");
        //                                    writer.WriteLine(" DATETIME:" + dateTime);


        //                                }
        //                                Response.End();
        //                            }
        //                        }
        //            }, FlightPak.Common.Constants.Policy.UILayer);
        //        }
        //        catch (Exception ex)
        //        {
        //            //The exception will be handled, logged and replaced by our custom exception. 
        //            ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
        //        }
        //    }
        }
    }
