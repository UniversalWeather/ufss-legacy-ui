﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace FlightPak.Web.Views.Settings.SystemAdministration
{
    public partial class UserPreferenceWidgetSettings : BaseSecuredPage
    {
        private ExceptionManager exManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        #region Authorization
                        if (!UserPrincipal.Identity._isSysAdmin)
                            Response.Redirect(ResolveUrl("~/Views/ErrorPages/AccessDenied.aspx"));
                        #endregion

                        if (!Page.IsPostBack)
                        {
                            LoadData();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        private void LoadData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (CommonService.CommonServiceClient commonsvc = new CommonService.CommonServiceClient())
                {
                    var userData = commonsvc.GetUserPreferenceList(100036);
                    if (userData.ReturnFlag == true)
                    {
                        gvSettings.DataSource = userData.EntityList.ToList();
                        gvSettings.DataBind();
                    }
                }
            }
        }

        protected void Settings_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Row.RowType == DataControlRowType.DataRow)
                        {
                            CheckBox chk = (CheckBox)e.Row.FindControl("chkSelect");
                            bool keyValue = Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "KeyValue"));
                            chk.Checked = keyValue;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        protected void Save_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        using (CommonService.CommonServiceClient commonsvc = new CommonService.CommonServiceClient())
                        {
                            if (gvSettings.Rows.Count > 0)
                            {
                                commonsvc.DeleteUserPreference();

                                for (int i = 0; i < gvSettings.Rows.Count; i++)
                                {
                                    string _keyName = gvSettings.Rows[i].Cells[0].Text.Trim();
                                    CheckBox chkSelect = (CheckBox)gvSettings.Rows[i].Cells[1].FindControl("chkSelect");
                                    string _keyValue = chkSelect.Checked.ToString();

                                    commonsvc.AddUserPreference("Utility", string.Empty, _keyName, _keyValue);
                                }

                                RadWindowManager1.RadAlert("Utility Preferences Saved Successfully.<br>You will need to re login to see the changes.", 350, 100, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackFn");                                                                                               
                            }
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
    }
}