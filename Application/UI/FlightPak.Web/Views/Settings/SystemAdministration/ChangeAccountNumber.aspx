﻿<%@ Page Language="C#" MasterPageFile="~/Framework/Masters/Settings.master" AutoEventWireup="true"
    CodeBehind="ChangeAccountNumber.aspx.cs" Inherits="FlightPak.Web.Views.ChangeAccountNumber" %>

<asp:Content ID="Content2" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js">
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<%=ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript">
           
            function alertCallBackOldAccFn(arg) {
                document.getElementById("<%=tbOldAccount.ClientID%>").focus();
            }

            function alertCallBackNewAccFn(arg) {
                document.getElementById("<%=tbNewAccount.ClientID%>").focus();
            }

            function alertCallBackFn(arg) {
                document.getElementById('<%=btnYes.ClientID%>').click();
            }

            function confirmCallBackChangeAccountFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=ChangeAccountYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=ChangeAccountNo.ClientID%>').click();
                }
            }

            function confirmCancelCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnCancelYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnCancelNo.ClientID%>').click();
                }
            }

        </script>
        <script type="text/javascript">
//            function openWin(radWin) {

//                var url = '';

//                if (radWin == "radOldAccountMasterPopup") {
//                    url = "../../Settings/Company/AccountMasterPopup.aspx?fromPage=" + "SystemTools";
//                }
//                if (radWin == "radNewAccountMasterPopup") {
//                    url = "../../Settings/Company/AccountMasterPopup.aspx?fromPage=" + "SystemTools";
//                }
//                if (url != '') {
//                    var oWnd = radopen(url, radWin);
//                }
//            }
            var Popup = '';
            function openWinAccount(type) {
                var url = '';
                Popup = type;
                if (type == "OLD") {

                    url = "../../Settings/Company/AccountMasterPopup.aspx?fromPage=SystemTools";
                    var oWnd = radopen(url, 'radAccountMasterPopup');
                    oWnd.add_close(RadPopupClose);
                }
                else {

                    url = "../../Settings/Company/AccountMasterPopup.aspx?fromPage=SystemTools";
                    var oWnd = radopen(url, 'radAccountMasterPopup');
                    oWnd.add_close(RadPopupClose);
                }

            }

            function RadPopupClose(oWnd, args) {
                if (Popup == 'OLD') {
                    var combo = $find("<%= tbOldAccount.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%= tbOldAccount.ClientID%>").value = arg.AccountNum;
                            document.getElementById("<%= lbOldAccount.ClientID%>").innerHTML = arg.AccountDescription;
                            document.getElementById("<%= hdnOldAccount.ClientID%>").innerHTML = arg.AccountID;
                            if (arg.AccountID != null)
                                document.getElementById("<%=lbcvOldAccount.ClientID%>").innerHTML = "";
                            var step = "OldAccountNo_TextChanged";
                            var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }
                        else {
                            document.getElementById("<%= tbOldAccount.ClientID%>").value = "";
                            document.getElementById("<%= lbOldAccount.ClientID%>").innerHTML = "";
                            document.getElementById("<%= hdnOldAccount.ClientID%>").innerHTML = "";
                            combo.clearSelection();
                        }
                    }
                   
                }
                if (Popup == 'NEW') {
                    var combo = $find("<%= tbNewAccount.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%= tbNewAccount.ClientID%>").value = arg.AccountNum;
                            document.getElementById("<%= lbNewAccount.ClientID%>").innerHTML = arg.AccountDescription;
                            document.getElementById("<%= hdnNewAccount.ClientID%>").innerHTML = arg.AccountID;
                            if (arg.AccountID != null)
                                document.getElementById("<%=lbcvNewAccount.ClientID%>").innerHTML = "";
                            var step = "NewAccountNo_TextChanged";
                            var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }
                        else {
                            document.getElementById("<%= tbOldAccount.ClientID%>").value = "";
                            document.getElementById("<%= lbNewAccount.ClientID%>").innerHTML = "";
                            document.getElementById("<%= hdnNewAccount.ClientID%>").innerHTML = "";
                            combo.clearSelection();
                        }
                    }
                }
            }


            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

//            function OnClientOldAccClose(oWnd, args) {
//                var combo = $find("<%= tbOldAccount.ClientID %>");
//                //get the transferred arguments
//                var arg = args.get_argument();
//                if (arg !== null) {
//                    if (arg) {
//                        document.getElementById("<%= tbOldAccount.ClientID%>").value = arg.AccountNum;
//                        document.getElementById("<%= lbOldAccount.ClientID%>").innerHTML = arg.AccountDescription;
//                        document.getElementById("<%= hdnOldAccount.ClientID%>").innerHTML = arg.AccountID;
//                        if (arg.AccountID != null)
//                            document.getElementById("<%=lbcvOldAccount.ClientID%>").innerHTML = "";
//                        var step = "OldAccountNo_TextChanged";
//                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
//                        if (step) {
//                            ajaxManager.ajaxRequest(step);
//                        }
//                    }
//                    else {
//                        document.getElementById("<%= tbOldAccount.ClientID%>").value = "";
//                        document.getElementById("<%= lbOldAccount.ClientID%>").innerHTML = "";
//                        document.getElementById("<%= hdnOldAccount.ClientID%>").innerHTML = "";
//                        combo.clearSelection();
//                    }
//                }
//            }

//            function OnClientNewAccClose(oWnd, args) {
//                var combo = $find("<%= tbNewAccount.ClientID %>");
//                //get the transferred arguments
//                var arg = args.get_argument();
//                if (arg !== null) {
//                    if (arg) {
//                        document.getElementById("<%= tbNewAccount.ClientID%>").value = arg.AccountNum;
//                        document.getElementById("<%= lbNewAccount.ClientID%>").innerHTML = arg.AccountDescription;
//                        document.getElementById("<%= hdnNewAccount.ClientID%>").innerHTML = arg.AccountID;
//                        if (arg.AccountID != null)
//                            document.getElementById("<%=lbcvNewAccount.ClientID%>").innerHTML = "";
//                        var step = "NewAccountNo_TextChanged";
//                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
//                        if (step) {
//                            ajaxManager.ajaxRequest(step);
//                        }
//                    }
//                    else {
//                        document.getElementById("<%= tbOldAccount.ClientID%>").value = "";
//                        document.getElementById("<%= lbNewAccount.ClientID%>").innerHTML = "";
//                        document.getElementById("<%= hdnNewAccount.ClientID%>").innerHTML = "";
//                        combo.clearSelection();
//                    }
//                }
//            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function RequestStart(sender, args) {
                currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                currentUpdatedControl = "<%= DivExternalForm.ClientID %>";
                //show the loading panel over the updated control
                currentLoadingPanel.show(currentUpdatedControl);
            }
            function ResponseEnd() {
                //hide the loading panel and clean up the global variables
                if (currentLoadingPanel != null)
                    currentLoadingPanel.hide(currentUpdatedControl);
                currentUpdatedControl = null;
                currentLoadingPanel = null;
            }

        </script>
    </telerik:RadCodeBlock>
    <%--<telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableCdn="true">
    </telerik:RadScriptManager>--%>
    <telerik:RadAjaxManager ID="RadAjaxManager1" OnAjaxRequest="RadAjaxManager1_AjaxRequest"
        runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnChange">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbOldAccountNo">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbNewAccountNo">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="radAccountMasterCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radAccountMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
                </telerik:RadWindow>               
            </Windows>
        </telerik:RadWindowManager>
       
            <table width="100%">
                <tr>
                    <td align="center">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="tblspace_12" colspan="2">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="mnd_text padleft_10" colspan="2">
                                    Select Account Nos. To Change:
                                </td>
                            </tr>
                            <tr>
                                <td class="nav-3" colspan="2">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table class="border-box">
                                        <tr>
                                            <td>
                                                Old
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbOldAccount" runat="server" CssClass="text340" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                    OnTextChanged="OldAccountNo_TextChanged" AutoPostBack="true" onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                                <asp:HiddenField ID="hdnOldAccount" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnOldAccount" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinAccount('OLD');return false;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td colspan="2" align="left">
                                                <asp:Label ID="lbOldAccount" runat="server" CssClass="input_no_bg" Visible="true"></asp:Label>
                                                <asp:Label ID="lbcvOldAccount" runat="server" CssClass="alert-text" Visible="true"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                New
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbNewAccount" runat="server" CssClass="text340" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                    OnTextChanged="NewAccountNo_TextChanged" AutoPostBack="true" onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                                <asp:HiddenField ID="hdnNewAccount" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnNewAccount" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinAccount('NEW');return false;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td colspan="2" align="left">
                                                <asp:Label ID="lbNewAccount" runat="server" CssClass="input_no_bg" Visible="true"></asp:Label>
                                                <asp:Label ID="lbcvNewAccount" runat="server" CssClass="alert-text" Visible="true"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="nav-3">
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="2">
                                    <asp:Button ID="btnChange" Text="Change" runat="server" CssClass="button" OnClick="Change_Click" />
                                    <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="button" OnClick="Cancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table id="tblHidden" style="display: none;">
                <tr>
                    <td>
                        <asp:Button ID="ChangeAccountYes" runat="server" Text="Button" OnClick="ChangeAccountYes_Click" />
                        <asp:Button ID="ChangeAccountNo" runat="server" Text="Button" OnClick="ChangeAccountNo_Click" />
                        <asp:Button ID="btnYes" runat="server" Text="Button" OnClick="btnYes_Click" />
                        <asp:Button ID="btnCancelYes" runat="server" Text="Button" OnClick="btnCancelYes_Click" />
                        <asp:Button ID="btnCancelNo" runat="server" Text="Button" OnClick="btnCancelNo_Click" />
                    </td>
                </tr>
            </table>
     
    </div>
</asp:Content>
