﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace FlightPak.Web.Views.Settings.SystemAdministration
{
    public partial class LoggedInUsers : BaseSecuredPage
    {
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        #region Authorization
                        if (!UserPrincipal.Identity._isSysAdmin)
                            Response.Redirect(ResolveUrl("~/Views/ErrorPages/AccessDenied.aspx"));
                        #endregion

                        if (!Page.IsPostBack)
                        {
                            LoadData();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        private void LoadData()
        {
            using (CommonService.CommonServiceClient commonsvc = new CommonService.CommonServiceClient())
            {
                var userData = commonsvc.GetAllLoggedInUsers(true);
                if (userData.ReturnFlag == true)
                {
                    gvLoggedInUsers.DataSource = userData.EntityList.ToList();
                    gvLoggedInUsers.DataBind();
                }
            }
        }


        protected void gvLoggedInUsers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Row.RowType == DataControlRowType.Header)
                        {
                            LinkButton lnkSelected = (LinkButton)e.Row.FindControl("lnkDeleteSelected");
                            lnkSelected.Attributes.Add("onclick", "javascript:return " +
                            "confirm('Are you sure you want to logoff selected users." +
                            DataBinder.Eval(e.Row.DataItem, "0") + "')");
                        }



                        if (e.Row.RowType == DataControlRowType.DataRow &&
               (e.Row.RowState == DataControlRowState.Normal ||
                e.Row.RowState == DataControlRowState.Alternate))
                        {
                            CheckBox chkBxSelect = (CheckBox)e.Row.Cells[1].FindControl("chkBxSelect");
                            CheckBox chkBxHeader = (CheckBox)this.gvLoggedInUsers.HeaderRow.FindControl("chkBxHeader");
                            chkBxSelect.Attributes["onclick"] = string.Format
                                                                   (
                                                                      "javascript:ChildClick(this,'{0}');",
                                                                      chkBxHeader.ClientID
                                                                   );
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        protected void gvLoggedInUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.CommandName == "Delete")
                        {
                            string username = e.CommandArgument.ToString();
                            LogOffUser(username);
                        }
                        if (e.CommandName == "DeleteSelected")
                        {
                            foreach (GridViewRow row in gvLoggedInUsers.Rows)
                            {
                                CheckBox checkbox = (CheckBox)row.FindControl("chkBxSelect");

                                //Check if the checkbox is checked.
                                //value in the HtmlInputCheckBox's Value property is set as the //value of the delete command's parameter.
                                if (checkbox.Checked)
                                {
                                    // Retreive the Employee ID
                                    string username = gvLoggedInUsers.DataKeys[row.RowIndex].Value.ToString();
                                    using (CommonService.CommonServiceClient common = new CommonService.CommonServiceClient())
                                    {
                                        common.LogOutAllUsers(username);
                                    }
                                }
                            }
                            LoadData();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }

        }

        protected void gvLoggedInUsers_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //  int FPLockID = (int)gvLock.DataKeys[e.RowIndex].Value;
            //DeleteRecordByID(FPLockID);
        }

        private void LogOffUser(string username)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(username))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CommonService.CommonServiceClient common = new CommonService.CommonServiceClient())
                        {
                            common.LogOutAllUsers(username);
                            LoadData();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
    }
}