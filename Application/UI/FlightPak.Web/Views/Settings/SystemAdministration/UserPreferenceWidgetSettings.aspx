﻿<%@ Page Language="C#" MasterPageFile="~/Framework/Masters/Site.Master" AutoEventWireup="true"
    CodeBehind="UserPreferenceWidgetSettings.aspx.cs" Inherits="FlightPak.Web.Views.Settings.SystemAdministration.UserPreferenceWidgetSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function alertCallBackFn(arg) { if (arg == true) { __doPostBack(); } }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableCdn="true">
    </telerik:RadScriptManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true">
    </telerik:RadWindowManager>
    <div class="account-body">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <div class="tab-nav-top">
                        <span class="head-title">Widget Settings</span> <span class="tab-nav-icons"></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="padding: 0px 0px 10px 0px;">
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:GridView ID="gvSettings" runat="server" AutoGenerateColumns="False" CssClass="GridViewStyle"
                        GridLines="None" EmptyDataText="No Records Found." OnRowDataBound="Settings_OnRowDataBound"
                        Width="100%">
                        <FooterStyle CssClass="GridViewFooterStyle" />
                        <RowStyle CssClass="GridViewRowStyle" />
                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                        <PagerStyle CssClass="GridViewPagerStyle" />
                        <AlternatingRowStyle CssClass="GridViewAlternatingRowStyle" />
                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                        <Columns>
                            <asp:BoundField DataField="KeyName" HeaderText="Widgets" HeaderStyle-Width="90%" />
                            <asp:TemplateField HeaderText="Show" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Button ID="btnSave" runat="server" Text="SAVE" CssClass="button" OnClick="Save_OnClick" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
