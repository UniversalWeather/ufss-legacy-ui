﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Settings.SystemAdministration
{
    public partial class ChangeCrewCode : BaseSecuredPage
    {
        // Declarations
        private ExceptionManager exManager;
        public bool IsLock = false;
        public bool NoLoggedinuser = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        #region Authorization
                        if (!UserPrincipal.Identity._isSysAdmin)
                            Response.Redirect(ResolveUrl("~/Views/ErrorPages/AccessDenied.aspx"));
                        #endregion


                        //Check for logged users and locked records
                        if (!IsPostBack)
                        {
                            CheckLoggedUsersAndRedirect();

                            if (NoLoggedinuser)
                                CheckLockRecordsAndRedirect();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        /// <summary>
        /// To validate Old Crew Code 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OldCrewCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbOldCrewCode.Text = string.Empty;
                        lbcvOldCrewCode.Text = string.Empty;
                        tbOldCrewCode.Text = tbOldCrewCode.Text.ToUpper();
                        if (!string.IsNullOrEmpty(tbOldCrewCode.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.GetAllCrew> CrewCodeLists = new List<FlightPak.Web.FlightPakMasterService.GetAllCrew>();
                                var objRetVal = objDstsvc.GetCrewList();
                                if (objRetVal.ReturnFlag)
                                {
                                    CrewCodeLists = objRetVal.EntityList.Where(x => x.CrewCD.ToString().ToUpper().Trim().Equals(tbOldCrewCode.Text.ToUpper().Trim())).ToList();
                                    if (CrewCodeLists != null && CrewCodeLists.Count > 0)
                                    {
                                        lbOldCrewCode.Text = System.Web.HttpUtility.HtmlEncode(((FlightPak.Web.FlightPakMasterService.GetAllCrew)CrewCodeLists[0]).CrewName);
                                        hdnOldCrewID.Value = ((FlightPak.Web.FlightPakMasterService.GetAllCrew)CrewCodeLists[0]).CrewID.ToString();
                                        lbcvOldCrewCode.Text = "";
                                    }
                                    else
                                    {
                                        lbcvOldCrewCode.Text = System.Web.HttpUtility.HtmlEncode("Invalid Crew Code.");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbOldCrewCode);
                                        hdnOldCrewID.Value = "";
                                    }
                                }
                                else
                                {
                                    lbcvOldCrewCode.Text = System.Web.HttpUtility.HtmlEncode("Invalid Crew Code.");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbOldCrewCode);
                                    hdnOldCrewID.Value = "";
                                }
                            }
                        }
                        else
                        {
                            lbOldCrewCode.Text = "";
                            hdnOldCrewID.Value = "";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        /// <summary>
        /// To validate New Crew Code 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NewCrewCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbNewCrewCode.Text = string.Empty;
                        lbcvNewCrewCode.Text = string.Empty;
                        tbNewCrewCode.Text = tbNewCrewCode.Text.ToUpper();
                        if (!string.IsNullOrEmpty(tbNewCrewCode.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.GetAllCrew> CrewCodeLists = new List<FlightPak.Web.FlightPakMasterService.GetAllCrew>();
                                var objRetVal = objDstsvc.GetCrewList();
                                if (objRetVal.ReturnFlag)
                                {
                                    CrewCodeLists = objRetVal.EntityList.Where(x => x.CrewCD.ToString().ToUpper().Trim().Equals(tbNewCrewCode.Text.ToUpper().Trim())).ToList();
                                    if (CrewCodeLists != null && CrewCodeLists.Count > 0)
                                    {
                                        lbNewCrewCode.Text = System.Web.HttpUtility.HtmlEncode(((FlightPak.Web.FlightPakMasterService.GetAllCrew)CrewCodeLists[0]).CrewName);
                                        hdnNewCrewID.Value = ((FlightPak.Web.FlightPakMasterService.GetAllCrew)CrewCodeLists[0]).CrewID.ToString();
                                        lbcvNewCrewCode.Text = string.Empty;
                                    }
                                    else
                                    {
                                        //lbcvNewCrewCode.Text = System.Web.HttpUtility.HtmlEncode("Invalid Crew Code.");
                                        //RadAjaxManager.GetCurrent(Page).FocusControl(tbNewCrewCode);
                                        hdnNewCrewID.Value = "";
                                    }
                                }
                                else
                                {
                                    //lbcvNewCrewCode.Text = System.Web.HttpUtility.HtmlEncode("Invalid Crew Code.");
                                    //RadAjaxManager.GetCurrent(Page).FocusControl(tbNewCrewCode);
                                    hdnNewCrewID.Value = "";
                                }
                            }
                        }
                        else
                        {
                            lbNewCrewCode.Text = "";
                            hdnNewCrewID.Value = "";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        /// <summary>
        /// Change button OnClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Change_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (string.IsNullOrEmpty(hdnOldCrewID.Value))
                        {
                            RadWindowManager1.RadAlert("Old Crew Code Is Required.", 300, 100, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackOldCrewFn", null);
                        }
                        else if (string.IsNullOrEmpty(tbNewCrewCode.Text.Trim()))
                        {
                            RadWindowManager1.RadAlert("New Crew Code Is Required.", 300, 100, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackNewCrewFn", null);
                        }
                        else if (!string.IsNullOrEmpty(tbNewCrewCode.Text.Trim()) && !string.IsNullOrEmpty(tbOldCrewCode.Text.Trim()) && tbNewCrewCode.Text.Trim().ToLower() == tbOldCrewCode.Text.Trim().ToLower())
                        {
                            RadWindowManager1.RadAlert("Old And New Crew Codes Cannot Be The Same.", 350, 100, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackOldCrewFn", null);
                        }
                        else if (!string.IsNullOrEmpty(hdnNewCrewID.Value) && !string.IsNullOrEmpty(hdnOldCrewID.Value))
                        {
                            RadWindowManager1.RadConfirm("WARNING- You have selected a New Crew code that already exists in the database. Performing this operation will merge all the Old Crew data. Are you sure you want to do this?", "confirmCallBackChangeCrewFn", 300, 100, null, ModuleNameConstants.SysAdmin.SystemAdmin);
                        }
                        else if (!string.IsNullOrEmpty(tbNewCrewCode.Text) && !string.IsNullOrEmpty(hdnOldCrewID.Value))
                        {
                            RadWindowManager1.RadConfirm("WARNING - Are you sure you want to change Crew code?", "confirmCallBackChangeCrewFn", 300, 100, null, ModuleNameConstants.SysAdmin.SystemAdmin);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        /// <summary>
        /// Change button OnClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Response.Redirect("../../Home.aspx");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        protected void Yes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Check for logged users and locked records
                        if (CheckNoLoggedUsers() && CheckLockRecords())
                        {
                            using (CommonService.CommonServiceClient CommonSvc = new CommonService.CommonServiceClient())
                            {
                                bool result = false;
                                long oldCrewID = Convert.ToInt64(hdnOldCrewID.Value);
                                long newCrewID = 0;
                                string crewCD = string.Empty;

                                if (!string.IsNullOrEmpty(hdnNewCrewID.Value))
                                    newCrewID = Convert.ToInt64(hdnNewCrewID.Value);
                                if (!string.IsNullOrEmpty(tbNewCrewCode.Text))
                                    crewCD = tbNewCrewCode.Text.Trim();

                                result = CommonSvc.ChangeCrew(oldCrewID, newCrewID, crewCD);

                                if (result)
                                {
                                    RadWindowManager1.RadAlert("All Crew Codes Changed.", 300, 100, ModuleNameConstants.SysAdmin.SystemAdmin, null, null);
                                    Clearform();
                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("Error in Changing Crew Codes!", 300, 100, ModuleNameConstants.SysAdmin.SystemAdmin, null, null);
                                }
                            }
                        }
                        else
                        {
                            if (!NoLoggedinuser)
                            {
                                //PP23012013
                                RadWindowManager1.RadConfirm("All other users MUST be out of Flight Scheduling Software System. Crew Code Change Utility cannot be run right now. Other users may be currently logged in. Please check the Logged-In Users screen.", "confirmCancelCallBackFn", 400, 150, null, ModuleNameConstants.SysAdmin.SystemAdmin);
                            }
                            else
                            {
                                IsLock = true;
                                RadWindowManager1.RadAlert("Records are being used by another user...Please use record lock manager screen to unlock.", 300, 150, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackFn");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        protected void No_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbNewCrewCode);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        /// <summary>
        /// Method to Clear Form
        /// </summary>
        protected void Clearform()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbOldCrewCode.Text = string.Empty;
                lbOldCrewCode.Text = string.Empty;
                lbcvOldCrewCode.Text = string.Empty;
                hdnOldCrewID.Value = string.Empty;
                tbNewCrewCode.Text = string.Empty;
                lbNewCrewCode.Text = string.Empty;
                lbcvNewCrewCode.Text = string.Empty;
                hdnNewCrewID.Value = string.Empty;
            }
        }

        /// <summary>
        /// Ajax Request Events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {

            }
        }

        #region "NFR Logged Users"
        private bool CheckNoLoggedUsers()
        {
            using (CommonService.CommonServiceClient objComsvc = new CommonService.CommonServiceClient())
            {
                bool returnVal = true;
                List<FlightPak.Web.CommonService.GetAllLoggedUsersByCustomerId> loggedInUsers = new List<CommonService.GetAllLoggedUsersByCustomerId>();
                var objRetVal = objComsvc.GetAllLoggedInUsers(false);
                if (objRetVal.ReturnFlag)
                {
                    loggedInUsers = objRetVal.EntityList;
                    if (loggedInUsers.Count > 0)
                    {
                        returnVal = false;
                        NoLoggedinuser = false;
                    }
                }
                return returnVal;
            }
        }

        private void CheckLoggedUsersAndRedirect()
        {
            if (!CheckNoLoggedUsers())
                //PP23012013
                RadWindowManager1.RadConfirm("All other users MUST be out of Flight Scheduling Software System. Crew Code Change Utility cannot be run right now. Other users may be currently logged in. Please check the Logged-In Users screen.", "confirmCancelCallBackFn", 400, 150, null, ModuleNameConstants.SysAdmin.SystemAdmin);
        }

        protected void AlertYes_Click(object sender, EventArgs e)
        {
            //Redirection for logged users and locked records
            //PP23012013
            Response.Redirect("LockManager.aspx");
        }
        #endregion

        #region "NFR Locked Records"
        private bool CheckLockRecords()
        {
            using (CommonService.CommonServiceClient common = new CommonService.CommonServiceClient())
            {
                bool returnLockVal = true;
                List<FlightPak.Web.CommonService.GetAllLocks> lockedRecords = new List<CommonService.GetAllLocks>();
                var LockData = common.GetAllLocks();
                if (LockData.ReturnFlag == true)
                {
                    lockedRecords = LockData.EntityList;
                    if (lockedRecords != null && lockedRecords.Count > 0)
                    {
                        returnLockVal = false;
                    }
                }
                return returnLockVal;
            }
        }

        private void CheckLockRecordsAndRedirect()
        {
            if (!CheckLockRecords())
            {
                //PP23012013(Deleted IsLock = true;)
                RadWindowManager1.RadAlert("Records are being used by another user...Please use record lock manager screen to unlock.", 300, 150, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackFn");
            }
        }

        #endregion

        //PP23012013
        protected void btnCancelYes_Click(object sender, EventArgs e)
        {
            Response.Redirect("LoggedInUsers.aspx");
        }

        protected void btnCancelNo_Click(object sender, EventArgs e)
        {
            //Redirection for logged users and locked records

            //Response.Redirect("../../Home.aspx");
            Response.Redirect(ResolveUrl("~/Views/Settings/Default.aspx?Screen=ChangeCrewCode"));
        }
        //PP23012013
    }
}