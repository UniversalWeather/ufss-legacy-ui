﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Settings.SystemAdministration
{
    public partial class ChangePassengerCode : BaseSecuredPage
    {
        // Declarations
        private ExceptionManager exManager;
        public bool IsLock = false;
        public bool NoLoggedinuser = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        #region Authorization
                        if (!UserPrincipal.Identity._isSysAdmin)
                            Response.Redirect(ResolveUrl("~/Views/ErrorPages/AccessDenied.aspx"));
                        #endregion

                        //Check for logged users and locked records
                        if (!IsPostBack)
                        {
                            CheckLoggedUsersAndRedirect();

                            if (NoLoggedinuser)
                                CheckLockRecordsAndRedirect();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        /// <summary>
        /// To validate Old PassengerCode 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OldPassengerCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbOldPassengerCode.Text = string.Empty;
                        lbcvOldPassengerCode.Text = string.Empty;
                        tbOldPassengerCode.Text = tbOldPassengerCode.Text.ToUpper();
                        hdnOldPassengerID.Value = "";
                        if (!string.IsNullOrEmpty(tbOldPassengerCode.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.GetAllPassenger> PassengerCodeLists = new List<FlightPak.Web.FlightPakMasterService.GetAllPassenger>();
                                var objRetVal = objDstsvc.GetPassengerList();
                                if (objRetVal.ReturnFlag)
                                {
                                    PassengerCodeLists = objRetVal.EntityList.Where(x => x.PassengerRequestorCD.ToString().ToUpper().Trim().Equals(tbOldPassengerCode.Text.ToUpper().Trim())).ToList();
                                    if (PassengerCodeLists != null && PassengerCodeLists.Count > 0)
                                    {
                                        lbOldPassengerCode.Text = System.Web.HttpUtility.HtmlEncode(((FlightPak.Web.FlightPakMasterService.GetAllPassenger)PassengerCodeLists[0]).PassengerName);
                                        hdnOldPassengerID.Value = ((FlightPak.Web.FlightPakMasterService.GetAllPassenger)PassengerCodeLists[0]).PassengerRequestorID.ToString();
                                        lbcvOldPassengerCode.Text = "";
                                    }
                                    else
                                    {
                                        lbcvOldPassengerCode.Text = System.Web.HttpUtility.HtmlEncode("Invalid Passenger Code.");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbOldPassengerCode);
                                        hdnOldPassengerID.Value = "";
                                    }
                                }
                                else
                                {
                                    lbcvOldPassengerCode.Text = System.Web.HttpUtility.HtmlEncode("Invalid Passenger Code.");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbOldPassengerCode);
                                    hdnOldPassengerID.Value = "";
                                }
                            }
                        }
                        else
                        {
                            lbOldPassengerCode.Text = "";
                            hdnOldPassengerID.Value = "";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        /// <summary>
        /// To validate New PassengerCode 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NewPassengerCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbNewPassengerCode.Text = string.Empty;
                        lbcvNewPassengerCode.Text = string.Empty;
                        tbNewPassengerCode.Text = tbNewPassengerCode.Text.ToUpper();
                        hdnNewPassengerID.Value = "";
                        if (!string.IsNullOrEmpty(tbNewPassengerCode.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.GetAllPassenger> PassengerCodeLists = new List<FlightPak.Web.FlightPakMasterService.GetAllPassenger>();
                                var objRetVal = objDstsvc.GetPassengerList();
                                if (objRetVal.ReturnFlag)
                                {
                                    PassengerCodeLists = objRetVal.EntityList.Where(x => x.PassengerRequestorCD.ToString().ToUpper().Trim().Equals(tbNewPassengerCode.Text.ToUpper().Trim())).ToList();
                                    if (PassengerCodeLists != null && PassengerCodeLists.Count > 0)
                                    {
                                        lbNewPassengerCode.Text = System.Web.HttpUtility.HtmlEncode(((FlightPak.Web.FlightPakMasterService.GetAllPassenger)PassengerCodeLists[0]).PassengerName);
                                        hdnNewPassengerID.Value = ((FlightPak.Web.FlightPakMasterService.GetAllPassenger)PassengerCodeLists[0]).PassengerRequestorID.ToString();
                                        lbcvNewPassengerCode.Text = "";
                                    }
                                    else
                                    {
                                        //lbcvNewPassengerCode.Text = System.Web.HttpUtility.HtmlEncode("Invalid Passenger Code.");
                                        //RadAjaxManager.GetCurrent(Page).FocusControl(tbNewPassengerCode);
                                        hdnNewPassengerID.Value = "";
                                    }
                                }
                                else
                                {
                                    //lbcvNewPassengerCode.Text = System.Web.HttpUtility.HtmlEncode("Invalid Passenger Code.");
                                    //RadAjaxManager.GetCurrent(Page).FocusControl(tbNewPassengerCode);
                                    hdnNewPassengerID.Value = "";
                                }
                            }
                        }
                        else
                        {
                            lbNewPassengerCode.Text = "";
                            hdnNewPassengerID.Value = "";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        /// <summary>
        /// Change button OnClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Change_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (string.IsNullOrEmpty(hdnOldPassengerID.Value))
                        {
                            RadWindowManager1.RadAlert("Old Passenger Code Is Required.", 300, 100, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackOldPasscFn", null);
                        }
                        else if (string.IsNullOrEmpty(tbNewPassengerCode.Text.Trim()))
                        {
                            RadWindowManager1.RadAlert("New Passenger Code Is Required.", 300, 100, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackNewPassFn", null);
                        }
                        else if (!string.IsNullOrEmpty(tbNewPassengerCode.Text.Trim()) && !string.IsNullOrEmpty(tbOldPassengerCode.Text.Trim()) && tbNewPassengerCode.Text.Trim().ToLower() == tbOldPassengerCode.Text.Trim().ToLower())
                        {
                            RadWindowManager1.RadAlert("Old And New Passenger Codes Cannot Be The Same.", 350, 100, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackOldPasscFn", null);
                        }
                        else if (!string.IsNullOrEmpty(hdnNewPassengerID.Value) && !string.IsNullOrEmpty(hdnOldPassengerID.Value))
                        {
                            RadWindowManager1.RadConfirm("WARNING- You have selected a New passenger code that already exists in the database. Performing this operation will merge all the Old passenger data. Are you sure you want to do this?", "confirmCallBackChangePassFn", 300, 100, null, ModuleNameConstants.SysAdmin.SystemAdmin);
                        }
                        else if (!string.IsNullOrEmpty(tbNewPassengerCode.Text) && !string.IsNullOrEmpty(hdnOldPassengerID.Value))
                        {
                            RadWindowManager1.RadConfirm("WARNING - Are you sure you want to change Passenger code?", "confirmCallBackChangePassFn", 300, 100, null, ModuleNameConstants.SysAdmin.SystemAdmin);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        /// <summary>
        /// Change button OnClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Response.Redirect("../../Home.aspx");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        protected void Yes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Check for logged users and locked records
                        if (CheckNoLoggedUsers() && CheckLockRecords())
                        {
                            using (CommonService.CommonServiceClient CommonSvc = new CommonService.CommonServiceClient())
                            {
                                bool result = false;
                                long oldPaxID = Convert.ToInt64(hdnOldPassengerID.Value);
                                long newPaxID = 0;
                                string paxCD = string.Empty;

                                if (!string.IsNullOrEmpty(hdnNewPassengerID.Value))
                                    newPaxID = Convert.ToInt64(hdnNewPassengerID.Value);

                                if (!string.IsNullOrEmpty(tbNewPassengerCode.Text))
                                    paxCD = tbNewPassengerCode.Text.Trim();

                                result = CommonSvc.ChangePassenger(oldPaxID, newPaxID, paxCD);

                                if (result)
                                {
                                    RadWindowManager1.RadAlert("All Passenger Codes Changed.", 300, 100, ModuleNameConstants.SysAdmin.SystemAdmin, null, null);
                                    Clearform();
                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("Error in Changing Passenger Codes!", 300, 100, ModuleNameConstants.SysAdmin.SystemAdmin, null, null);
                                }
                            }
                        }
                        else
                        {
                            if (!NoLoggedinuser)
                            {
                                //PP23012013
                                RadWindowManager1.RadConfirm("All other users MUST be out of Flight Scheduling Software System. Passenger Number Change Utility cannot be run right now. Other users may be currently logged in. Please check the Logged-In Users screen.", "confirmCancelCallBackFn", 400, 150, null, ModuleNameConstants.SysAdmin.SystemAdmin);
                            }
                            else
                            {
                                IsLock = true;
                                RadWindowManager1.RadAlert("Records are being used by another user...Please use record lock manager screen to unlock.", 300, 150, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackFn");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        protected void No_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbNewPassengerCode);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }

        /// <summary>
        /// Method to Clear Form
        /// </summary>
        protected void Clearform()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbOldPassengerCode.Text = string.Empty;
                lbOldPassengerCode.Text = string.Empty;
                lbcvOldPassengerCode.Text = string.Empty;
                hdnOldPassengerID.Value = string.Empty;
                tbNewPassengerCode.Text = string.Empty;
                lbNewPassengerCode.Text = string.Empty;
                lbcvNewPassengerCode.Text = string.Empty;
                hdnNewPassengerID.Value = string.Empty;
            }
        }

        /// <summary>
        /// Ajax Request Events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {

            }
        }

        #region NFR Methods
        private bool CheckNoLoggedUsers()
        {
            using (CommonService.CommonServiceClient objComsvc = new CommonService.CommonServiceClient())
            {
                bool returnVal = true;
                List<FlightPak.Web.CommonService.GetAllLoggedUsersByCustomerId> loggedInUsers = new List<CommonService.GetAllLoggedUsersByCustomerId>();
                var objRetVal = objComsvc.GetAllLoggedInUsers(false);
                if (objRetVal.ReturnFlag)
                {
                    loggedInUsers = objRetVal.EntityList;
                    if (loggedInUsers.Count > 0)
                    {
                        returnVal = false;
                        NoLoggedinuser = false;
                    }
                }
                return returnVal;
            }
        }

        private void CheckLoggedUsersAndRedirect()
        {
            if (!CheckNoLoggedUsers())
                //PP23012013
                RadWindowManager1.RadConfirm("All other users MUST be out of Flight Scheduling Software System. Passenger Number Change Utility cannot be run right now. Other users may be currently logged in. Please check the Logged-In Users screen.", "confirmCancelCallBackFn", 400, 150, null, ModuleNameConstants.SysAdmin.SystemAdmin);
        }

        protected void AlertYes_Click(object sender, EventArgs e)
        {
            //Redirection for logged users and locked records
            //PP23012013
            Response.Redirect("LockManager.aspx");
        }
        #endregion

        #region "NFR Locked Records"
        private bool CheckLockRecords()
        {
            using (CommonService.CommonServiceClient common = new CommonService.CommonServiceClient())
            {
                bool returnLockVal = true;
                List<FlightPak.Web.CommonService.GetAllLocks> lockedRecords = new List<CommonService.GetAllLocks>();
                var LockData = common.GetAllLocks();
                if (LockData.ReturnFlag == true)
                {
                    lockedRecords = LockData.EntityList;
                    if (lockedRecords != null && lockedRecords.Count > 0)
                    {
                        returnLockVal = false;
                    }
                }
                return returnLockVal;
            }
        }

        private void CheckLockRecordsAndRedirect()
        {
            if (!CheckLockRecords())
            {
                //PP23012013(Deleted IsLock = true;)
                RadWindowManager1.RadAlert("Records are being used by another user...Please use record lock manager screen to unlock.", 300, 150, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackFn");
            }
        }

        #endregion

        //PP23012013
        protected void btnCancelYes_Click(object sender, EventArgs e)
        {
            Response.Redirect("LoggedInUsers.aspx");
        }

        protected void btnCancelNo_Click(object sender, EventArgs e)
        {
            //Redirection for logged users and locked records

            //Response.Redirect("../../Home.aspx");
            Response.Redirect(ResolveUrl("~/Views/Settings/Default.aspx?Screen=ChangePassenger")); 
        }
        //PP23012013
    }
}