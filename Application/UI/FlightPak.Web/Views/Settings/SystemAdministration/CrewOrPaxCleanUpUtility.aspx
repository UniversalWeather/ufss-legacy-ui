﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Framework/Masters/Settings.master"
    CodeBehind="CrewOrPaxCleanUpUtility.aspx.cs" Inherits="FlightPak.Web.Views.Settings.SystemAdministration.CrewOrPaxCleanUpUtility" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<%@ Register TagPrefix="uc" TagName="DatePicker" Src="~/UserControls/UCDate.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js">
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<%=ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript">

            var currentTextBox = null;
            var currentDatePicker = null;

            //This method is called to handle the onclick and onfocus client side events for the texbox
            function showPopup(sender, e) {
                currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                //this gets a reference to the datepicker, which will be shown, to facilitate
                //the selection of a date
                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                //this variable is used to store a reference to the date picker, which is currently
                //active
                currentDatePicker = datePicker;

                //this method first parses the date, that the user entered or selected, and then
                //sets it as a selected date to the picker

                datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));


                //the code lines below show the calendar, which is used to select a date. The showPopup
                //function takes three arguments - the x and y coordinates where to show the calendar, as
                //well as its height, derived from the offsetHeight property of the textbox
                var position = datePicker.getElementPosition(currentTextBox);
                datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
            }

            //this handler is used to set the text of the TextBox to the value of selected from the popup
            function dateSelected(sender, args) {

                if (currentTextBox != null) {
                    var step = "Date_TextChanged";
                    if (currentTextBox.value != args.get_newValue()) {
                        currentTextBox.value = args.get_newValue();
                        var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                }
            }

            function hide() {
                var datePicker = $find("<%= RadDatePicker1.ClientID%>");
                datePicker.hidePopup();
                return true;
            }


            function tbDate_OnKeyDown(sender, event) {
                var step = "Date_TextChanged";
                if (event.keyCode == 9) {
                    parseDate(sender, event);
                    var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                    datePicker.hidePopup();
                    var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                    if (step) {
                        ajaxManager.ajaxRequest(step);
                    }
                    return true;
                }
            }

            //this function is used to parse the date entered or selected by the user
            function parseDate(sender, e) {
                if (currentDatePicker != null) {
                    var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                    var dateInput = currentDatePicker.get_dateInput();
                    var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                    sender.value = formattedDate;
                }
            }

            // this function is used to validate the date in tbDate textbox...
            function ontbDateKeyPress(sender, e) {

                // find whether formattedDate contains . or - or / etc.. and pass them to functionAllowNumericKeyChar() respectively...
                var dateInput = ($find("<%= RadDatePicker1.ClientID %>")).get_dateInput();

                var dateSeparator = null;
                var dotSeparator = dateInput.get_displayDateFormat().indexOf('.');
                var slashSeparator = dateInput.get_displayDateFormat().indexOf('/');
                var hyphenSeparator = dateInput.get_displayDateFormat().indexOf('-');
                if (dotSeparator != -1) { dateSeparator = '.'; }
                else if (slashSeparator != -1) { dateSeparator = '/'; }
                else if (hyphenSeparator != 1) { dateSeparator = '-'; }
                else {
                    alert("Invalid Date");
                }
                // allow dot, slash and hypen characters... if any other character, throw alert
                return fnAllowNumericAndChar($find("<%=tbReportDate.ClientID %>"), e, dateSeparator);
            }
            function ValidateDateEmpty() {
                // var objLastPurchase = document.forms[0].ctl00$ctl00$MainContent$SettingBodyContent$ucReportDate$tbDate;

                var ReportDate = document.getElementById("<%= tbReportDate.ClientID %>").value

                if (ReportDate == null || ReportDate == "") {
                    radalert('Please select Report Date', 330, 100, "Report Date", "alertCallBackCleanUp");
                    return false;
                }
            }

            function alertCallBackCleanUp(arg) {
                document.getElementById("<%=tbReportDate.ClientID%>").focus();
            }

            function ValidateDate(control) {
                var MinDate = new Date("01/01/1900");
                var MaxDate = new Date("12/31/2100");
                var SelectedDate;
                if (control.value != "") {
                    SelectedDate = new Date(control.value);
                    if ((SelectedDate < MinDate) || (SelectedDate > MaxDate)) {
                        alert("Please enter/select Date between 01/01/1900 and 12/31/2100");
                        control.value = "";
                    }
                }
                return false;
            }
            function alertCallBackFn(arg) {
                document.getElementById('<%=btnYes.ClientID%>').click();
            }
            function confirmCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById("<%= hdnInactivate.ClientID%>").value = 'yes';
                    document.getElementById('<%=btnInactivateRecords.ClientID%>').click();
                }
            }
            function confirmCancelCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnCancelYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnCancelNo.ClientID%>').click();
                }
            }
        </script>
    </telerik:RadCodeBlock>
    <%--<telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableCdn="true">
    </telerik:RadScriptManager>--%>
    <telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                    ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <table width="100%">
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="tblspace_12" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="mnd_text padleft_10" colspan="2">
                                Crew/Pax Table Clean-up
                            </td>
                        </tr>
                        <tr>
                            <td class="tblspace_10">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table class="border-box">
                                    <tr>
                                        <td>
                                            Report Date
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbReportDate" onclick="showPopup(this, event);" AutoPostBack="true"
                                                onkeydown="return tbDate_OnKeyDown(this, event);" onKeyPress="return ontbDateKeyPress(this, event);"
                                                CssClass="text80" runat="server" onchange="parseDate(this, event);javascript:return ValidateDate(this);"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Sort By
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="radlstSortBy" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="Code" Selected="True">Code</asp:ListItem>
                                                <asp:ListItem Value="Name"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td align="left">
                                            <asp:CheckBox ID="chkInactive" runat="server" Text="Show Inactive" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="nav-6">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="tblspace_5">
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2">
                                <%--<asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                        OnClientClick="javascript:return ValidateDateEmpty();" OnClick="btnGenerateReport_Click">
                                    </asp:Button>
                                    <asp:Button ID="btnInactivateRecords" runat="server" CssClass="button" Text="Inactivate Records"
                                        OnClientClick="javascript:return ValidateDateEmpty();" OnClick="btnInactivateRecords_Click">
                                    </asp:Button>--%>
                                <asp:Button ID="btnGenerateReport" runat="server" CssClass="button" Text="Generate Report"
                                    OnClick="btnGenerateReport_Click"></asp:Button>
                                <asp:Button ID="btnInactivateRecords" runat="server" CssClass="button" Text="Inactivate Records"
                                    OnClick="btnInactivateRecords_Click"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table id="tblHidden" style="display: none;">
            <tr>
                <td>
                    <asp:Button ID="btnYes" runat="server" Text="Button" OnClick="btnYes_Click" />
                    <asp:HiddenField ID="hdnInactivate" runat="server" />
                    <asp:Button ID="btnCancelYes" runat="server" Text="Button" OnClick="btnCancelYes_Click" />
                    <asp:Button ID="btnCancelNo" runat="server" Text="Button" OnClick="btnCancelNo_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
