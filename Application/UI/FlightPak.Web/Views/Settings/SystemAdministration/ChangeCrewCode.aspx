﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="ChangeCrewCode.aspx.cs" Inherits="FlightPak.Web.Views.Settings.SystemAdministration.ChangeCrewCode" %>

<asp:Content ID="Content2" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js">
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<%=ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript">

            function alertCallBackOldCrewFn(arg) {
                document.getElementById("<%=tbOldCrewCode.ClientID%>").focus();
            }

            function alertCallBackNewCrewFn(arg) {
                document.getElementById("<%=tbNewCrewCode.ClientID%>").focus();
            }

            function confirmCallBackChangeCrewFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnNo.ClientID%>').click();
                }
            }

            function alertCallBackFn(arg) {
                document.getElementById('<%=btnAlertYes.ClientID%>').click();
            }

            function confirmCancelCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnCancelYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnCancelNo.ClientID%>').click();
                }
            }
        </script>
        <script type="text/javascript">
            var Popup = '';
            function openWinCrew(type) {
                var url = '';
                Popup = type;
                if (type == "OLD") {

                    url = "../../Settings/People/CrewRosterPopup.aspx?fromPage=SystemTools";
                    var oWnd = radopen(url, 'radCrewRosterPopup');
                    oWnd.add_close(RadPopupClose);
                }
                else {

                    url = "../../Settings/People/CrewRosterPopup.aspx?fromPage=SystemTools";
                    var oWnd = radopen(url, 'radCrewRosterPopup');
                    oWnd.add_close(RadPopupClose);
                }

            }
//            function openWin(radWin) {

//                var url = '';

//                if (radWin == "radOldCrewCodePopup") {
//                    url = "../../Settings/People/CrewRosterPopup.aspx?fromPage=SystemTools";
//                }
//                if (radWin == "radNewCrewCodePopup") {
//                    url = "../../Settings/People/CrewRosterPopup.aspx?fromPage=SystemTools";
//                }
//                if (url != '') {
//                    var oWnd = radopen(url, radWin);
//                }
//            }

            function RadPopupClose(oWnd, args) {
                if (Popup == 'OLD') {
                    var combo = $find("<%= tbOldCrewCode.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%= tbOldCrewCode.ClientID%>").value = arg.CrewCD;
                            document.getElementById("<%= lbOldCrewCode.ClientID%>").innerHTML = arg.CrewName;
                            document.getElementById("<%= hdnOldCrewID.ClientID%>").innerHTML = arg.CrewID;
                            if (arg.CrewID != null)
                                document.getElementById("<%=lbcvOldCrewCode.ClientID%>").innerHTML = "";
                            var step = "OldCrewCode_TextChanged";
                            var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }
                        else {
                            document.getElementById("<%= tbOldCrewCode.ClientID%>").value = "";
                            document.getElementById("<%= lbOldCrewCode.ClientID%>").innerHTML = "";
                            document.getElementById("<%= hdnOldCrewID.ClientID%>").innerHTML = "";
                            combo.clearSelection();
                        }
                    }

                }
                if (Popup == 'NEW') {
                    var combo = $find("<%= tbNewCrewCode.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%= tbNewCrewCode.ClientID%>").value = arg.CrewCD;
                            document.getElementById("<%= lbNewCrewCode.ClientID%>").innerHTML = arg.CrewName;
                            document.getElementById("<%= hdnNewCrewID.ClientID%>").innerHTML = arg.CrewID;
                            if (arg.CrewID != null)
                                document.getElementById("<%=lbcvNewCrewCode.ClientID%>").innerHTML = "";
                            var step = "NewCrewCode_TextChanged";
                            var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }
                        else {
                            document.getElementById("<%= tbNewCrewCode.ClientID%>").value = "";
                            document.getElementById("<%= lbNewCrewCode.ClientID%>").innerHTML = "";
                            document.getElementById("<%= hdnNewCrewID.ClientID%>").innerHTML = "";
                            combo.clearSelection();
                        }
                    }
                }
            }
            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

//            function OnClientOldCrewCodeClose(oWnd, args) {
//                var combo = $find("<%= tbOldCrewCode.ClientID %>");
//                //get the transferred arguments
//                var arg = args.get_argument();
//                if (arg !== null) {
//                    if (arg) {
//                        document.getElementById("<%= tbOldCrewCode.ClientID%>").value = arg.CrewCD;
//                        document.getElementById("<%= lbOldCrewCode.ClientID%>").innerHTML = arg.CrewName;
//                        document.getElementById("<%= hdnOldCrewID.ClientID%>").innerHTML = arg.CrewID;
//                        if (arg.CrewID != null)
//                            document.getElementById("<%=lbcvOldCrewCode.ClientID%>").innerHTML = "";
//                        var step = "OldCrewCode_TextChanged";
//                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
//                        if (step) {
//                            ajaxManager.ajaxRequest(step);
//                        }
//                    }
//                    else {
//                        document.getElementById("<%= tbOldCrewCode.ClientID%>").value = "";
//                        document.getElementById("<%= lbOldCrewCode.ClientID%>").innerHTML = "";
//                        document.getElementById("<%= hdnOldCrewID.ClientID%>").innerHTML = "";
//                        combo.clearSelection();
//                    }
//                }
//            }

//            function OnClientNewCrewCodeClose(oWnd, args) {
//                var combo = $find("<%= tbNewCrewCode.ClientID %>");
//                //get the transferred arguments
//                var arg = args.get_argument();
//                if (arg !== null) {
//                    if (arg) {
//                        document.getElementById("<%= tbNewCrewCode.ClientID%>").value = arg.CrewCD;
//                        document.getElementById("<%= lbNewCrewCode.ClientID%>").innerHTML = arg.CrewName;
//                        document.getElementById("<%= hdnNewCrewID.ClientID%>").innerHTML = arg.CrewID;
//                        if (arg.CrewID != null)
//                            document.getElementById("<%=lbcvNewCrewCode.ClientID%>").innerHTML = "";
//                        var step = "NewCrewCode_TextChanged";
//                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
//                        if (step) {
//                            ajaxManager.ajaxRequest(step);
//                        }
//                    }
//                    else {
//                        document.getElementById("<%= tbNewCrewCode.ClientID%>").value = "";
//                        document.getElementById("<%= lbNewCrewCode.ClientID%>").innerHTML = "";
//                        document.getElementById("<%= hdnNewCrewID.ClientID%>").innerHTML = "";
//                        combo.clearSelection();
//                    }
//                }
//            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function RequestStart(sender, args) {
                currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                currentUpdatedControl = "<%= DivExternalForm.ClientID %>";
                //show the loading panel over the updated control
                currentLoadingPanel.show(currentUpdatedControl);
            }
            function ResponseEnd() {
                //hide the loading panel and clean up the global variables
                if (currentLoadingPanel != null)
                    currentLoadingPanel.hide(currentUpdatedControl);
                currentUpdatedControl = null;
                currentLoadingPanel = null;
            }

        </script>
    </telerik:RadCodeBlock>
    <%-- <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableCdn="true">
    </telerik:RadScriptManager>--%>
    <telerik:RadAjaxManager ID="RadAjaxManager1" OnAjaxRequest="RadAjaxManager1_AjaxRequest"
        runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnChange">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbOldCrewCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbNewCrewCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="radCrewRosterCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewRosterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radCrewRosterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewRosterPopup.aspx">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <table width="100%">
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="tblspace_12" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="mnd_text padleft_10" colspan="2">
                                Select Crew Code To Change:
                            </td>
                        </tr>
                        <tr>
                            <td class="nav-3" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table class="border-box">
                                    <tr>
                                        <td>
                                            Old
                                        </td>
                                        <td class="tdLabel60">
                                            <asp:TextBox ID="tbOldCrewCode" runat="server" MaxLength="5" CssClass="text50" OnTextChanged="OldCrewCode_TextChanged"
                                                AutoPostBack="true">
                                            </asp:TextBox>
                                            <asp:HiddenField ID="hdnOldCrewID" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnOldCrewCode" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinCrew('OLD');return false;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td colspan="2" align="left">
                                            <asp:Label ID="lbOldCrewCode" runat="server" CssClass="input_no_bg" Visible="true"></asp:Label>
                                            <asp:Label ID="lbcvOldCrewCode" runat="server" CssClass="alert-text" Visible="true">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            New
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbNewCrewCode" runat="server" MaxLength="5" CssClass="text50" AutoPostBack="true"
                                                OnTextChanged="NewCrewCode_TextChanged">
                                            </asp:TextBox>
                                            <asp:HiddenField ID="hdnNewCrewID" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnNewCrewCode" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinCrew('NEW');return false;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td colspan="2" align="left">
                                            <asp:Label ID="lbNewCrewCode" runat="server" CssClass="input_no_bg" Visible="true"></asp:Label>
                                            <asp:Label ID="lbcvNewCrewCode" runat="server" CssClass="alert-text" Visible="true">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="nav-3">
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2">
                                <asp:Button ID="btnChange" Text="Change" runat="server" CssClass="button" OnClick="Change_Click" />
                                <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="button" OnClick="Cancel_Click" />
                            </td>
                        </tr>
                    </table>
                    <table id="tblHidden" style="display: none;">
                        <tr>
                            <td>
                                <asp:Button ID="btnYes" runat="server" Text="Button" OnClick="Yes_Click" />
                                <asp:Button ID="btnNo" runat="server" Text="Button" OnClick="No_Click" />
                                <asp:Button ID="btnAlertYes" runat="server" Text="Button" OnClick="AlertYes_Click" />
                                <asp:Button ID="btnCancelYes" runat="server" Text="Button" OnClick="btnCancelYes_Click" />
                                <asp:Button ID="btnCancelNo" runat="server" Text="Button" OnClick="btnCancelNo_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
