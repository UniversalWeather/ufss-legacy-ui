﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Telerik.Web.UI;
using FlightPak.Common;


namespace FlightPak.Web.Views.Settings.SystemAdministration
{
    public partial class ChangeCustomerCode : BaseSecuredPage
    {
        private ExceptionManager exManager;
        public bool IsLock = false;
        public bool NoLoggedinuser = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        #region Authorization
                        if (!UserPrincipal.Identity._isSysAdmin)
                            Response.Redirect(ResolveUrl("~/Views/ErrorPages/AccessDenied.aspx"));
                        #endregion

                        if (!IsPostBack)
                        {
                            //Check for logged users and locked records
                            CheckLoggedUsersAndRedirect();

                            if (NoLoggedinuser)
                                checkLockRecordsAndRedirect();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }


        #region "NFR Logged Users"
        private bool CheckNoLoggedUsers()
        {
            using (CommonService.CommonServiceClient objComsvc = new CommonService.CommonServiceClient())
            {
                bool returnVal = true;
                List<FlightPak.Web.CommonService.GetAllLoggedUsersByCustomerId> loggedInUsers = new List<CommonService.GetAllLoggedUsersByCustomerId>();
                var objRetVal = objComsvc.GetAllLoggedInUsers(false);
                if (objRetVal.ReturnFlag)
                {
                    loggedInUsers = objRetVal.EntityList;
                    if (loggedInUsers != null && loggedInUsers.Count > 0)
                    {
                        returnVal = false;
                        NoLoggedinuser = false;
                    }
                }
                return returnVal;
            }
        }

        private void CheckLoggedUsersAndRedirect()
        {
            if (!CheckNoLoggedUsers())
                RadWindowManager1.RadConfirm("All other users MUST be out of Flight Scheduling Software System. CQCustomer Code Change Utility cannot be run right now. Other users may be currently logged in. Please check the Logged-In Users screen.", "confirmCancelCallBackFn", 400, 150, null, ModuleNameConstants.SysAdmin.SystemAdmin);
        }

        #endregion

        #region "NFR Locked Records"
        private bool checkLockRecords()
        {
            using (CommonService.CommonServiceClient common = new CommonService.CommonServiceClient())
            {
                bool returnLockVal = true;
                List<FlightPak.Web.CommonService.GetAllLocks> lockedRecords = new List<CommonService.GetAllLocks>();
                var LockData = common.GetAllLocks();
                if (LockData.ReturnFlag == true)
                {
                    lockedRecords = LockData.EntityList;
                    if (lockedRecords != null && lockedRecords.Count > 0)
                    {
                        returnLockVal = false;
                    }
                }
                return returnLockVal;
            }
        }

        private void checkLockRecordsAndRedirect()
        {
            if (!checkLockRecords())
            {
                RadWindowManager1.RadAlert("Records are being used by another user...Please use record lock manager screen to unlock.", 300, 150, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackFn");
            }
        }

        #endregion

        protected void btnYesLogin_Click(object sender, EventArgs e)
        {
            //Redirection for logged users and locked records
            Response.Redirect("LockManager.aspx");
        }

        protected void btnCancelYes_Click(object sender, EventArgs e)
        {
            Response.Redirect("LoggedInUsers.aspx");
        }

        protected void btnCancelNo_Click(object sender, EventArgs e)
        {
            //Redirection for logged users and locked records
            //Response.Redirect("../../Home.aspx");
            Response.Redirect(ResolveUrl("~/Views/Settings/Default.aspx?Screen=ChangeCQCustomer"));
        }

        /// <summary>
        /// To validate Old cqcustomer code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OldCustomerCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbcvOldCustomerCode.Text = string.Empty;
                        if (!string.IsNullOrEmpty(tbOldCustomerCode.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPak.Web.FlightPakMasterService.CQCustomer objCQCustomer = new FlightPak.Web.FlightPakMasterService.CQCustomer();
                                objCQCustomer.CQCustomerCD = tbOldCustomerCode.Text.Trim();
                                var RetValue = objMasterService.GetCQCustomerByCQCustomerCD(objCQCustomer).EntityList;
                                if (RetValue != null && RetValue.Count > 0)
                                {
                                    if (RetValue[0].CQCustomerID != null && RetValue[0].CQCustomerID != 0)
                                    {
                                        hdnOldCQCustomerID.Value = RetValue[0].CQCustomerID.ToString();
                                        lbcvOldCustomerCode.Text = "";
                                    }
                                    else
                                    {
                                        lbcvOldCustomerCode.Text = System.Web.HttpUtility.HtmlEncode("Invalid Customer Code");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbOldCustomerCode);
                                        hdnOldCQCustomerID.Value = "";
                                    }
                                }
                                else
                                {
                                    lbcvOldCustomerCode.Text = System.Web.HttpUtility.HtmlEncode("Invalid Customer Code");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbOldCustomerCode);
                                    hdnOldCQCustomerID.Value = "";
                                }
                            }
                        }
                        else
                        {
                            hdnOldCQCustomerID.Value = "";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
        /// <summary>
        /// To validate that entered New customer code does not match in database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NewCustomerCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbNewCustomerCode.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPak.Web.FlightPakMasterService.CQCustomer objCQCustomer = new FlightPak.Web.FlightPakMasterService.CQCustomer();
                                objCQCustomer.CQCustomerCD = tbNewCustomerCode.Text.Trim();
                                var RetValue = objMasterService.GetCQCustomerByCQCustomerCD(objCQCustomer).EntityList;
                                if (RetValue != null && RetValue.Count > 0)
                                {
                                    RadWindowManager1.RadAlert("WARNING- You have selected a New Customer Code. that already exists in the database. This operation is NOT allowed. It would make two CQCustomer have the same Customer code.. Select another Customer code", 350, 100, "System Messages", "alertCallBackNewCQCusFn", null);
                                    tbNewCustomerCode.Text = "";
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
        /// <summary>
        /// Change button OnClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnChange_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (string.IsNullOrEmpty(hdnOldCQCustomerID.Value))
                        {
                            RadWindowManager1.RadAlert("Old Customer Code Is Required.", 300, 100, "System Messages", "alertCallBackOldCQCusFn", null);
                        }
                        if (string.IsNullOrEmpty(tbNewCustomerCode.Text) && !string.IsNullOrEmpty(hdnOldCQCustomerID.Value))
                        {
                            RadWindowManager1.RadAlert("New Customer Code Is Required.", 300, 100, "System Messages", "alertCallBackNewCQCusFn", null);
                        }
                        if (!string.IsNullOrEmpty(tbOldCustomerCode.Text.Trim()) && !string.IsNullOrEmpty(tbNewCustomerCode.Text.Trim()) && tbOldCustomerCode.Text.Trim() == tbNewCustomerCode.Text.Trim())
                        {
                            RadWindowManager1.RadAlert("WARNING- You have selected a New Customer Code that already exists in the database. This operation is NOT allowed. It would make two CQCustomer have the same Customer Code.. Select another Customer Code", 350, 100, "System Messages", "alertCallBackNewCQCusFn", null);
                            tbNewCustomerCode.Text = "";
                        }
                        if (!string.IsNullOrEmpty(hdnOldCQCustomerID.Value) && !string.IsNullOrEmpty(tbNewCustomerCode.Text.Trim()))
                        {
                            if (string.IsNullOrEmpty(tbEffecCustomerChangeDate.Text))
                            {
                                RadWindowManager1.RadConfirm("Are you sure you want to change Customer Code " + tbOldCustomerCode.Text + " To " + tbNewCustomerCode.Text + " throughout FlightPak?", "confirmCallBackChangeCQCusThrouFn", 300, 100, null, "FSS System");
                            }
                            else
                            {
                                RadWindowManager1.RadConfirm("Are you sure you want to change Customer Code " + tbOldCustomerCode.Text + " To " + tbNewCustomerCode.Text + " w.e.f. " + tbEffecCustomerChangeDate.Text + "?", "confirmCallBackChangeCQCusFn", 300, 100, null, "Flight Scheduling Software System");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
        /// <summary>
        /// Change button OnClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Response.Redirect("../../Home.aspx");

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }

                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
        /// <summary>
        /// To Return bool value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Check for logged users and locked records
                        if (CheckNoLoggedUsers() && checkLockRecords())
                        {
                            using (CommonService.CommonServiceClient CommonSvc = new CommonService.CommonServiceClient())
                            {
                                Int64 oldCQCustomerID = 0;
                                DateTime? effectiveDateTime = null;
                                if (!string.IsNullOrEmpty(hdnOldCQCustomerID.Value))
                                    oldCQCustomerID = Convert.ToInt64(hdnOldCQCustomerID.Value.Trim());
                                if (!string.IsNullOrEmpty(tbEffecCustomerChangeDate.Text))
                                    effectiveDateTime = Convert.ToDateTime(tbEffecCustomerChangeDate.Text);

                                CommonSvc.LockCustomer();
                                bool result = CommonSvc.ChangeCQCustomerCD(oldCQCustomerID, tbNewCustomerCode.Text, effectiveDateTime);                                
                                CommonSvc.UnlockCustomer();
                                if (result)
                                {
                                    RadWindowManager1.RadAlert("All CQCustomer codes Changed from " + tbOldCustomerCode.Text + " To " + tbNewCustomerCode.Text + " w.e.f. " + tbEffecCustomerChangeDate.Text, 300, 100, "System Messages", "alertCallBackOldCQCusFn", null);
                                    Clearform();
                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("Transaction failed.Try after sometime ", 300, 100, "System Messages", "alertCallBackOldCQCusFn", null);
                                }

                            }
                        }
                        else
                        {
                            if (!NoLoggedinuser)
                            {
                                RadWindowManager1.RadConfirm("All other users MUST be out of Flight Scheduling Software System. CQCustomer Code Change Utility cannot be run right now. Other users may be currently logged in. Please check the Logged-In Users screen.", "confirmCancelCallBackFn", 400, 150, null, ModuleNameConstants.SysAdmin.SystemAdmin);
                            }
                            else
                            {
                                IsLock = true;
                                RadWindowManager1.RadAlert("Records are being used by another user...Please use record lock manager screen to unlock.", 300, 150, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackFn");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DelayType);
                }
            }
        }
        /// <summary>
        /// To focus back to customer code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbNewCustomerCode);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }

                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
        /// <summary>
        /// To clear form
        /// </summary>
        protected void Clearform()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbOldCustomerCode.Text = string.Empty;
                        lbcvOldCustomerCode.Text = string.Empty;
                        hdnOldCQCustomerID.Value = "";
                        tbNewCustomerCode.Text = string.Empty;
                        tbEffecCustomerChangeDate.Text = string.Empty;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }

                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
        /// <summary>
        /// To Return bool value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnthroughYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Check for logged users and locked records
                        if (CheckNoLoggedUsers() && checkLockRecords())
                        {
                            using (CommonService.CommonServiceClient CommonSvc = new CommonService.CommonServiceClient())
                            {
                                Int64 oldCQCustomerID = 0;
                                DateTime? effectiveDateTime = null;
                                if (!string.IsNullOrEmpty(hdnOldCQCustomerID.Value))
                                    oldCQCustomerID = Convert.ToInt64(hdnOldCQCustomerID.Value.Trim());
                                if (!string.IsNullOrEmpty(tbEffecCustomerChangeDate.Text))
                                    effectiveDateTime = Convert.ToDateTime(tbEffecCustomerChangeDate.Text);

                                CommonSvc.LockCustomer();
                                bool result = CommonSvc.ChangeCQCustomerCD(oldCQCustomerID, tbNewCustomerCode.Text, effectiveDateTime);                                                   
                                CommonSvc.UnlockCustomer();
                                if (result)
                                {
                                    RadWindowManager1.RadAlert("All CQCustomer codes Changed from " + tbOldCustomerCode.Text + " To " + tbNewCustomerCode.Text + " throughout Flight Scheduling Software", 300, 100, "System Messages", "alertCallBackOldCQCusFn", null);
                                    Clearform();
                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("Transaction failed.Try after sometime ", 300, 100, "System Messages", "alertCallBackOldCQCusFn", null);
                                }

                            }
                        }
                        else
                        {
                            if (!NoLoggedinuser)
                            {
                                //PP23012013
                                RadWindowManager1.RadConfirm("All other users MUST be out of Flight Scheduling Software System. CQCustomer Code Change Utility cannot be run right now. Other users may be currently logged in. Please check the Logged-In Users screen.", "confirmCancelCallBackFn", 400, 150, null, ModuleNameConstants.SysAdmin.SystemAdmin);
                            }
                            else
                            {
                                IsLock = true;
                                RadWindowManager1.RadAlert("Records are being used by another user...Please use record lock manager screen to unlock.", 300, 150, ModuleNameConstants.SysAdmin.SystemAdmin, "alertCallBackFn");
                            }
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
        /// <summary>
        /// To focus to control cqcustomer code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnthroughNo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbNewCustomerCode);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
        /// <summary>
        /// Ajax request
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.SysAdmin.SystemAdmin);
                }
            }
        }
    }
}