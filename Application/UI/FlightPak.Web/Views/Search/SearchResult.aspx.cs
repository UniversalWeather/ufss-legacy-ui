﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using FlightPak.Common;
using Telerik.Web.UI;

namespace FlightPak.Web.Views.Search
{
    public partial class SearchResult : BaseSecuredPage
    {
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            tdSearchHeader.InnerHtml = System.Web.HttpUtility.HtmlEncode("Search results for \"" + SearchString + "\"");

                            //if (!(SearchString.Length >= 3))
                            //{
                            //    RadWindowManager1.RadAlert("Search string length should be minimum of 3 characters", 360, 50, ModuleNameConstants.Search.BasicSearch, "");
                            //}

                            //var searchCombo = this.Master.Master.FindControl("RadSearchCombo") as RadComboBox;
                            //searchCombo.SelectedValue = Category.ToString();
                            //var siteMaster = new Framework.Masters.Site();
                            //siteMaster.ModuleSelectedValue = Category.ToString();


                            //var searchTextBox = this.Master.Master.FindControl("SearchTextBox") as TextBox;
                            //searchTextBox.Text = SearchString;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Search.BasicSearch);
                }
            }
        }

        protected void ShowSearch_BindData(object sender, RadListViewNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (SearchService.SearchServiceClient searchService = new SearchService.SearchServiceClient())
                        {
                            //if ((SearchString.Length >= 3))
                            ShowSearch.DataSource = searchService.SimpleSearch(SearchString, Category, 0, 0).EntityList;

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Search.BasicSearch);
                }
            }
        }
        protected void ShowSearch_ItemCommand(Object Sender, RadListViewCommandEventArgs e)
        {
            //Label2.Text = "The " + ((Button)e.CommandSource).Text + " button has just been clicked; <br />";
            //Session["SearchItemPrimaryKey"] = e.Item.DataItem[""];
            string pageName = string.Empty;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string[] argument = e.CommandArgument.ToString().Split(',');
                        Session["SearchItemPrimaryKeyValue"] = argument[1];

                        TryResolveUrl(argument[0], out pageName);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Search.BasicSearch);
                }
            }
            Response.Redirect(pageName);


        }

        public string SearchString
        {
            get
            {
                if (Request.QueryString["q"] != null)
                {
                    return Request.QueryString["q"].ToString();
                }
                return string.Empty;
            }

        }
        public string Category
        {
            get
            {
                if (Request.QueryString["c"] != null)
                {
                    return Request.QueryString["c"].ToString();
                }
                return string.Empty;
            }
        }
    }
}