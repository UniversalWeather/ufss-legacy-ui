﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Authentication.Master"
    AutoEventWireup="true" CodeBehind="SearchResult.aspx.cs" Inherits="FlightPak.Web.Views.Search.SearchResult" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    </telerik:RadWindowManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ShowSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ShowSearch" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" MinDisplayTime="0" />
    <div id="searchZone" class="account-body">
        <table width="100%">
            <tr>
                <td runat="server" id="tdSearchHeader" style="font-size: 20px; font-weight: bolder;"
                    align="center">
                </td>
            </tr>
            <tr>
                <td>
                    <%-- <asp:Repeater ID="ShowSearch" runat="server" OnItemCommand="ShowSearch_ItemCommand">
                    <HeaderTemplate>
                        <table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:LinkButton ID="ResultRedirection" runat="server" Text='<%# Eval("PageVirtualPath") %>'
                                    CommandArgument='<%# Eval("PageVirtualPath") + "," + Eval("PrimaryKeyValue")%>'></asp:LinkButton>
                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%# DataBinder.Eval(Container.DataItem, "ColumnValue")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                        
                    </FooterTemplate>--%>
                    <telerik:RadListView ID="ShowSearch" Width="97%" AllowPaging="True" runat="server"
                        Skin="Metro" OnItemCommand="ShowSearch_ItemCommand" ItemPlaceholderID="SearchHolder"
                        OnNeedDataSource="ShowSearch_BindData">
                        <LayoutTemplate>
                            <table cellpadding="0" cellspacing="0" width="100%;" style="clear: both;">
                                <tr>
                                    <td>
                                        <telerik:RadDataPager ID="radTopPager" runat="server" PagedControlID="ShowSearch"
                                            PageSize="10" Skin="Metro">
                                            <Fields>
                                                <telerik:RadDataPagerButtonField FieldType="FirstPrev" />
                                                <telerik:RadDataPagerButtonField FieldType="Numeric" PageButtonCount="5" />
                                                <telerik:RadDataPagerButtonField FieldType="NextLast" />
                                                <telerik:RadDataPagerTemplatePageField>
                                                    <PagerTemplate>
                                                        <div style="float: right">
                                                            <b>Items
                                                                <asp:Label runat="server" ID="CurrentPageLabel" Text="<%# System.Web.HttpUtility.HtmlEncode(Container.Owner.StartRowIndex+1) %>" />
                                                                to
                                                                <asp:Label runat="server" ID="TotalPagesLabel" Text="<%# System.Web.HttpUtility.HtmlEncode(Container.Owner.TotalRowCount > (Container.Owner.StartRowIndex+Container.Owner.PageSize) ? Container.Owner.StartRowIndex+Container.Owner.PageSize : Container.Owner.TotalRowCount) %>" />
                                                                of
                                                                <asp:Label runat="server" ID="TotalItemsLabel" Text="<%# System.Web.HttpUtility.HtmlEncode(Container.Owner.TotalRowCount)%>" />
                                                                <br />
                                                            </b>
                                                        </div>
                                                    </PagerTemplate>
                                                </telerik:RadDataPagerTemplatePageField>
                                            </Fields>
                                        </telerik:RadDataPager>
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" width="100%;" class="box_searchitems" style="clear: both;">
                                <tr>
                                    <td>
                                        <asp:Panel ID="SearchHolder" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" width="100%;" style="clear: both;">
                                <tr>
                                    <td>
                                        <telerik:RadDataPager ID="SearchDataPager" runat="server" PagedControlID="ShowSearch"
                                            PageSize="10" Skin="Metro">
                                            <Fields>
                                                <telerik:RadDataPagerButtonField FieldType="FirstPrev" />
                                                <telerik:RadDataPagerButtonField FieldType="Numeric" PageButtonCount="5" />
                                                <telerik:RadDataPagerButtonField FieldType="NextLast" />
                                                <telerik:RadDataPagerTemplatePageField>
                                                    <PagerTemplate>
                                                        <div style="float: right">
                                                            <b>Items
                                                                <asp:Label runat="server" ID="CurrentPageLabel" Text="<%# System.Web.HttpUtility.HtmlEncode(Container.Owner.StartRowIndex+1) %>" />
                                                                to
                                                                <asp:Label runat="server" ID="TotalPagesLabel" Text="<%# System.Web.HttpUtility.HtmlEncode(Container.Owner.TotalRowCount > (Container.Owner.StartRowIndex+Container.Owner.PageSize) ? Container.Owner.StartRowIndex+Container.Owner.PageSize : Container.Owner.TotalRowCount) %>" />
                                                                of
                                                                <asp:Label runat="server" ID="TotalItemsLabel" Text="<%# System.Web.HttpUtility.HtmlEncode(Container.Owner.TotalRowCount)%>" />
                                                                <br />
                                                            </b>
                                                        </div>
                                                    </PagerTemplate>
                                                </telerik:RadDataPagerTemplatePageField>
                                            </Fields>
                                        </telerik:RadDataPager>
                                    </td>
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <div style="float: left; margin-left: 20px">
                                <table width="100%">
                                    <tr style="height: 10px">
                                    </tr>
                                    <tr>
                                        <td title='<%# DataBinder.Eval(Container.DataItem, "ColumnValue")%>'>
                                            <%# TruncateStringByLineBreak(DataBinder.Eval(Container.DataItem, "ColumnValue").ToString())%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="400px">
                                            <asp:LinkButton ID="ResultRedirection" runat="server" Text='View Record' CommandArgument='<%# Eval("PageVirtualPath") + "," + Eval("PrimaryKeyValue")%>'></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr style="height: 10px">
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <fieldset style="width: 900px">
                                <legend>Search</legend>No records found.
                            </fieldset>
                        </EmptyDataTemplate>
                    </telerik:RadListView>
                </td>
            </tr>
        </table>
    </div>
    <telerik:RadToolTipManager ID="RadToolTipManager1" runat="server" RelativeTo="Element"
        Position="MiddleRight" AutoTooltipify="true" ContentScrolling="Default" AutoCloseDelay="300000"
        Width="250" Height="10" ToolTipZoneID="searchZone">
    </telerik:RadToolTipManager>
</asp:Content>
