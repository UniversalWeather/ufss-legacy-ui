﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//Included
using Telerik.Web.UI;
using System.Globalization;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Text.RegularExpressions;
using FlightPak.Web.Framework.Helpers;
using System.Text;
using System.Drawing;
namespace FlightPak.Web.Views.Utilities
{
    public partial class ItineraryPlanning : BaseSecuredPage
    {
        private ExceptionManager exManager;
        private string DateFormat;
        private bool IsSaveSuccessItineraryPlan = false;
        private bool IsSaveSuccessItineraryPlanLegs = false;
        private Int64 Identity;
        private string CurrencyType = string.Empty;
        private string CurrencyFormat = string.Empty;
        private string TenthMinFormat = string.Empty;
        private string DateValidationError = string.Empty;
        private string ResetTime = "00:00";
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.


                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgItineraryPlanning, dgItineraryPlanning, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgItineraryPlanning, pnlbarItineraryPlanLegs, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(pnlbarItineraryPlanLegs, pnlbarItineraryPlanLegs, RadAjaxLoadingPanel1);

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgItineraryPlanningLegs, dgItineraryPlanningLegs, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgItineraryPlanningLegs, pnlbarItineraryPlanLegs, RadAjaxLoadingPanel1);
                        
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSaveAll, dgItineraryPlanning, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSaveAll, pnlbarItineraryPlanLegs, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancelAll, dgItineraryPlanning, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancelAll, pnlbarItineraryPlanLegs, RadAjaxLoadingPanel1);


                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId1'] = '{0}';", dgItineraryPlanning.ClientID));

                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgItineraryPlanningLegs, dgItineraryPlanningLegs, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client

                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId2'] = '{0}';", dgItineraryPlanningLegs.ClientID));
                        SetDefaultData();
                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Utilities.ViewItineraryPlan);
                            if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                            {
                                DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                                RadDatePicker1.DateInput.DateFormat = DateFormat;
                            }
                            else
                            {
                                DateFormat = "MM/dd/yyyy";
                                RadDatePicker1.DateInput.DateFormat = DateFormat;
                            }
                            DefaultSelectionItineraryPlan(true);
                            DefaultSelectionItineraryPlanLegs(true);
                            Session["StandardFlightpakConversion"] = LoadStandardFPConversion();
                        }
                        //added for datetime formatting
                        else
                        {
                            if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                            {
                                DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                                RadDatePicker1.DateInput.DateFormat = DateFormat;
                            }
                            else
                            {
                                DateFormat = "MM/dd/yyyy";
                                RadDatePicker1.DateInput.DateFormat = DateFormat;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlan);
                }
            }
        }
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = DivExternalForm;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlan);
                }
            }
        }

        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";ItineraryNumber=" + tbIPFileNumber.Text;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
            }
        }
        private void SetDefaultData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((UserPrincipal.Identity != null) && (UserPrincipal.Identity._fpSettings != null) && (UserPrincipal.Identity._fpSettings._CurrencySymbol != null))
                {
                    CurrencyType = UserPrincipal.Identity._fpSettings._CurrencySymbol;
                }
                else
                {
                    CurrencyType = "$ ";
                }
                CurrencyFormat = "#########0.00";
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                {
                    TenthMinFormat = "##0.0";
                    regIPLTOBias.ValidationExpression = "^[0-9]{0,3}\\.?[0-9]{0,1}$";
                    regIPLTOBias.ErrorMessage = "Invalid Format. Required Format is NNN.N";
                    regIPLLandBias.ValidationExpression = "^[0-9]{0,3}\\.?[0-9]{0,1}$";
                    regIPLLandBias.ErrorMessage = "Invalid Format. Required Format is NNN.N";
                    hdnTenthMin.Value = "1";
                }
                else if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                {
                    regIPLTOBias.ValidationExpression = "^[0-9]{0,3}\\:?[0-9]{0,2}$";
                    regIPLTOBias.ErrorMessage = "Invalid Format. Required Format is NNN:NN";
                    regIPLLandBias.ValidationExpression = "^[0-9]{0,3}\\:?[0-9]{0,2}$";
                    regIPLLandBias.ErrorMessage = "Invalid Format. Required Format is NNN:NN";
                    TenthMinFormat = "#0:00";
                    hdnTenthMin.Value = "2";
                }
                #region "Kilometer/Miles"
                if (UserPrincipal.Identity._fpSettings._IsKilometer == null ? false : (bool)UserPrincipal.Identity._fpSettings._IsKilometer)
                    lbIPLMiles.Text = "Kilometer";
                else
                    lbIPLMiles.Text = "Miles(N)";
                #endregion
            }
        }
        #region "Itinerary Plan"
        private void DefaultSelectionItineraryPlan(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgItineraryPlanning.Rebind();
                    }
                    if (dgItineraryPlanning.MasterTableView.Items.Count > 0)
                    {
                        dgItineraryPlanning.SelectedIndexes.Add(0);
                        Session["ItineraryPlanID"] = dgItineraryPlanning.Items[0].GetDataKeyValue("ItineraryPlanID").ToString();
                        LoadControlDataItineraryPlan();
                        EnableFormItineraryPlan(false);
                        GridEnableItineraryPlan(true, true, true);
                    }
                    else
                    {
                        Session["ItineraryPlanID"] = null;
                        ClearFormItineraryPlan();
                        EnableFormItineraryPlan(false);
                        GridEnableItineraryPlan(true, true, true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void DefaultSelectItemFormItineraryPlan()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["ItineraryPlanID"] != null)
                    {
                        string ID = Session["ItineraryPlanID"].ToString();
                        foreach (GridDataItem Item in dgItineraryPlanning.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("ItineraryPlanID").ToString() == ID.Trim())
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                        if (dgItineraryPlanning.SelectedItems.Count == 0)
                        {
                            DefaultSelectionItineraryPlan(false);
                        }
                    }
                    else
                    {
                        DefaultSelectionItineraryPlan(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void GridEnableItineraryPlan(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                LinkButton insertCtl, editCtl, delCtl;
                insertCtl = (LinkButton)dgItineraryPlanning.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                editCtl = (LinkButton)dgItineraryPlanning.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                delCtl = (LinkButton)dgItineraryPlanning.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                if (IsAuthorized(Permission.Utilities.AddItineraryPlan))
                {
                    insertCtl.Visible = true;
                    if (Add)
                        insertCtl.Enabled = true;
                    else
                        insertCtl.Enabled = false;
                }
                else
                {
                    insertCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Utilities.EditItineraryPlan))
                {
                    editCtl.Visible = true;
                    if (Edit)
                    {
                        editCtl.Enabled = true;
                        editCtl.OnClientClick = "javascript:return ProcessUpdateItineraryPlan();";
                    }
                    else
                    {
                        editCtl.Enabled = false;
                        editCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    editCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Utilities.DeleteItineraryPlan))
                {
                    delCtl.Visible = true;
                    if (Delete)
                    {
                        delCtl.Enabled = true;
                        delCtl.OnClientClick = "javascript:return ProcessDeleteItineraryPlan();";
                    }
                    else
                    {
                        delCtl.Enabled = false;
                        delCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    delCtl.Visible = false;
                }
            }
        }
        private void EnableFormItineraryPlan(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                tbIPFileNumber.Enabled = false;
                tbIPTailNumber.Enabled = Enable;
                btnIPTailNumber.Enabled = Enable;
                tbIPTypeCode.Enabled = Enable;
                btnBrowseTypeCode.Enabled = Enable;
                ddlIPQuarter.Enabled = Enable;
                tbIPYear.Enabled = Enable;
                tbIPClientCode.Enabled = Enable;
                btnIPClientCode.Enabled = Enable;
                tbIPHomebase.Enabled = Enable;
                btnIPHomebase.Enabled = Enable;
                tbIPDescription.Enabled = Enable;
                btnSaveItineraryPlans.Enabled = Enable;
                btnCancelItineraryPlans.Enabled = Enable;
                btnSaveAll.Visible = Enable;
                btnCancelAll.Visible = Enable;
                btnSaveAll.ValidationGroup = "SaveItineraryPlan";
                if (dgItineraryPlanning.Items.Count != 0)
                {
                    btnIPLTransfer.Enabled = true;
                }
                else
                {
                    btnIPLTransfer.Enabled = false;
                }
                btnIPLAlerts.Enabled = !(Enable);
                btnIPLTransfer.Enabled = !(Enable);
            }
        }
        private void ClearFormItineraryPlan()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                hdnSaveItineraryPlans.Value = string.Empty;
                hdnIPItineraryPlanID.Value = string.Empty;
                tbIPFileNumber.Text = string.Empty;
                tbIPTailNumber.Text = string.Empty;
                hdnIPTailNumber.Value = string.Empty;
                tbIPTypeCode.Text = string.Empty;
                hdnIPTypeCodeID.Value = string.Empty;
                ddlIPQuarter.SelectedIndex = 0;
                tbIPYear.Text = string.Empty;
                tbIPYear.Text = DateTime.UtcNow.Year.ToString();
                tbIPClientCode.Text = string.Empty;
                hdnIPClientID.Value = string.Empty;
                tbIPHomebase.Text = string.Empty;
                hdnIPHomebase.Value = string.Empty;
                if (UserPrincipal.Identity._airportId != null)
                {
                    hdnIPHomebaseAirportID.Value = UserPrincipal.Identity._airportId.ToString().Trim();
                }
                tbIPDescription.Text = string.Empty;
                Identity = 0;
            }
        }
        private void LoadControlDataItineraryPlan()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgItineraryPlanning.SelectedItems.Count != 0)
                {
                    GridDataItem Item = dgItineraryPlanning.SelectedItems[0] as GridDataItem;
                    if (Item.GetDataKeyValue("ItineraryPlanID").ToString().Trim().ToUpper() == Session["ItineraryPlanID"].ToString().Trim().ToUpper())
                    {
                        if (Item.GetDataKeyValue("ItineraryPlanID") != null)
                        {
                            hdnIPItineraryPlanID.Value = Item.GetDataKeyValue("ItineraryPlanID").ToString().Trim();
                        }
                        else
                        {
                            hdnIPItineraryPlanID.Value = string.Empty;
                        }
                        if (Item.GetDataKeyValue("IntinearyNUM") != null)
                        {
                            tbIPFileNumber.Text = Item.GetDataKeyValue("IntinearyNUM").ToString().Trim();
                        }
                        else
                        {
                            tbIPFileNumber.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("TailNum") != null)
                        {
                            tbIPTailNumber.Text = Item.GetDataKeyValue("TailNum").ToString().Trim();
                        }
                        else
                        {
                            tbIPTailNumber.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("FleetID") != null)
                        {
                            hdnIPTailNumber.Value = Item.GetDataKeyValue("FleetID").ToString().Trim();
                        }
                        else
                        {
                            hdnIPTailNumber.Value = string.Empty;
                        }
                        if (Item.GetDataKeyValue("AircraftCD") != null)
                        {
                            tbIPTypeCode.Text = Item.GetDataKeyValue("AircraftCD").ToString().Trim();
                        }
                        else
                        {
                            tbIPTypeCode.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("AircraftID") != null)
                        {
                            hdnIPTypeCodeID.Value = Item.GetDataKeyValue("AircraftID").ToString().Trim();
                        }
                        else
                        {
                            hdnIPTypeCodeID.Value = string.Empty;
                        }
                        if (Item.GetDataKeyValue("ItineraryPlanQuarter") != null)
                        {
                            ddlIPQuarter.SelectedValue = Item.GetDataKeyValue("ItineraryPlanQuarter").ToString().Trim();
                        }
                        else
                        {
                            ddlIPQuarter.SelectedIndex = 0;
                        }
                        if (Item.GetDataKeyValue("ItineraryPlanQuarterYear") != null)
                        {
                            tbIPYear.Text = Item.GetDataKeyValue("ItineraryPlanQuarterYear").ToString().Trim();
                        }
                        else
                        {
                            tbIPYear.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("ClientCD") != null)
                        {
                            tbIPClientCode.Text = Item.GetDataKeyValue("ClientCD").ToString().Trim();
                        }
                        else
                        {
                            tbIPClientCode.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("ClientID") != null)
                        {
                            hdnIPClientID.Value = Item.GetDataKeyValue("ClientID").ToString().Trim();
                        }
                        else
                        {
                            hdnIPClientID.Value = string.Empty;
                        }
                        if (Item.GetDataKeyValue("IcaoID") != null)
                        {
                            tbIPHomebase.Text = Item.GetDataKeyValue("IcaoID").ToString().Trim();
                        }
                        else
                        {
                            tbIPHomebase.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("HomebaseID") != null)
                        {
                            hdnIPHomebase.Value = Item.GetDataKeyValue("HomebaseID").ToString().Trim();
                        }
                        else
                        {
                            hdnIPHomebase.Value = string.Empty;
                        }
                        tbIPHomebase_OnTextChanged(tbIPHomebase, EventArgs.Empty);
                        if (Item.GetDataKeyValue("ItineraryPlanDescription") != null)
                        {
                            tbIPDescription.Text = Item.GetDataKeyValue("ItineraryPlanDescription").ToString().Trim();
                        }
                        else
                        {
                            tbIPDescription.Text = string.Empty;
                        }
                        Label lbLastUpdatedUser;
                        lbLastUpdatedUser = (Label)dgItineraryPlanning.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                        if (Item.GetDataKeyValue("LastUpdUID") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                        }
                        else
                        {
                            lbLastUpdatedUser.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("LastUpdTS") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString())));
                        }
                    }
                }
            }
        }
        private UtilitiesService.ItineraryPlan GetItemsItineraryPlan(UtilitiesService.ItineraryPlan oItineraryPlan)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oItineraryPlan))
            {
                if (hdnSaveItineraryPlans.Value == "Update")
                {
                    if (!string.IsNullOrEmpty(hdnIPItineraryPlanID.Value))
                    {
                        oItineraryPlan.ItineraryPlanID = Convert.ToInt64(hdnIPItineraryPlanID.Value);
                    }
                }
                if (!string.IsNullOrEmpty(tbIPFileNumber.Text))
                {
                    oItineraryPlan.IntinearyNUM = Convert.ToInt32(tbIPFileNumber.Text);
                }
                else
                {
                    oItineraryPlan.IntinearyNUM = -1;
                }
                if (!string.IsNullOrEmpty(hdnIPTailNumber.Value))
                {
                    oItineraryPlan.FleetID = Convert.ToInt64(hdnIPTailNumber.Value);
                }
                if (!string.IsNullOrEmpty(hdnIPTypeCodeID.Value))
                {
                    oItineraryPlan.AircraftID = Convert.ToInt64(hdnIPTypeCodeID.Value);
                }
                oItineraryPlan.ItineraryPlanQuarter = Convert.ToInt32(ddlIPQuarter.SelectedValue);
                if (!string.IsNullOrEmpty(tbIPYear.Text))
                {
                    oItineraryPlan.ItineraryPlanQuarterYear = tbIPYear.Text;
                }
                if (!string.IsNullOrEmpty(hdnIPClientID.Value))
                {
                    oItineraryPlan.ClientID = Convert.ToInt64(hdnIPClientID.Value);
                }
                if (!string.IsNullOrEmpty(hdnIPHomebase.Value))
                {
                    oItineraryPlan.HomebaseID = Convert.ToInt64(hdnIPHomebase.Value);
                }
                if (!string.IsNullOrEmpty(tbIPDescription.Text))
                {
                    oItineraryPlan.ItineraryPlanDescription = tbIPDescription.Text;
                }
                oItineraryPlan.IsDeleted = false;
                return oItineraryPlan;
            }
        }
        protected void tbIPTypeCode_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbIPTypeCode.Text))
                        {
                            if (CheckAircraftExist(tbIPTypeCode, hdnIPTypeCodeID) == false)
                            {
                                cvIPTypeCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbIPTypeCode.ClientID);
                            }
                        }
                        else
                        {
                            //rfvIPTypeCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbIPTypeCode.ClientID);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlan);
                }
            }
        }
        private bool CheckAircraftExist(TextBox TxtBx, HiddenField HdnFld)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (!string.IsNullOrEmpty(TxtBx.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objMasterService.GetAircraftList().EntityList.Where(x => x.AircraftCD.Trim().ToUpper() == TxtBx.Text.Trim().ToUpper()).ToList();
                        if (objRetVal.Count() != 0)
                        {
                            List<FlightPakMasterService.GetAllAircraft> AircraftList = new List<FlightPakMasterService.GetAllAircraft>();
                            AircraftList = (List<FlightPakMasterService.GetAllAircraft>)objRetVal.ToList();
                            HdnFld.Value = AircraftList[0].AircraftID.ToString();
                            ReturnValue = true;
                        }
                        else
                        {
                            HdnFld.Value = string.Empty;
                            ReturnValue = false;
                        }
                    }
                }
                return ReturnValue;
            }
        }
        protected void tbIPTailNumber_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbIPTailNumber.Text))
                        {
                            if (CheckTailNumberExist(tbIPTailNumber, hdnIPTailNumber) == false)
                            {
                                cvIPTailNumber.IsValid = false;
                                RadAjaxManager1.FocusControl(tbIPTailNumber.ClientID);
                            }
                        }
                        else
                        {
                            hdnIPTailNumber.Value = string.Empty;
                            RadAjaxManager1.FocusControl(tbIPTailNumber.ClientID);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlan);
                }
            }
        }
        private bool CheckTailNumberExist(TextBox TxtBx, HiddenField HdnFld)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (!string.IsNullOrEmpty(TxtBx.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objMasterService.GetFleetProfileList().EntityList.Where(x => x.TailNum.ToString().ToUpper().Trim().Equals(TxtBx.Text.ToUpper().Trim())).ToList();
                        if (objRetVal.Count() > 0 && (objRetVal != null && objRetVal[0] != null ))
                        {
                            //List<FlightPakMasterService.FleetByTailNumberResult> FleetList = new List<FlightPakMasterService.FleetByTailNumberResult>();
                            //FleetList = (List<FlightPakMasterService.FleetByTailNumberResult>)objRetVal.ToList();
                            HdnFld.Value = Convert.ToString(objRetVal[0].FleetID);
                            tbIPTypeCode.Text = "";
                            hdnIPTypeCodeID.Value = Convert.ToString(objRetVal[0].AircraftID);
                            if (objRetVal[0].AircraftID != null && objRetVal[0].AircraftID.ToString().Trim() != string.Empty)
                            {
                                var objRetValAircraft = objMasterService.GetAircraftByAircraftID(Convert.ToInt64(objRetVal[0].AircraftID)).EntityList.ToList();
                                if (objRetValAircraft != null && objRetValAircraft.Count > 0)
                                {
                                    tbIPTypeCode.Text = Convert.ToString(objRetValAircraft[0].AircraftCD.ToUpper());
                                }
                            }
                            ReturnValue = true;
                        }
                        else
                        {
                            HdnFld.Value = string.Empty;
                            ReturnValue = false;
                            //tbIPTypeCode.Text = "";
                        }
                    }
                }
                return ReturnValue;
            }
        }
        protected void tbIPClientCode_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbIPClientCode.Text))
                        {
                            if (CheckClientExist(tbIPClientCode, hdnIPClientID) == false)
                            {
                                cvIPClientCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbIPClientCode.ClientID);
                            }
                        }
                        else
                        {
                            hdnIPClientID.Value = string.Empty;
                            RadAjaxManager1.FocusControl(tbIPClientCode.ClientID);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlan);
                }
            }
        }
        private bool CheckClientExist(TextBox TxtBx, HiddenField HdnFld)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (!string.IsNullOrEmpty(TxtBx.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objMasterService.GetClientByClientCD(TxtBx.Text).EntityList;
                        if (objRetVal.Count() != 0)
                        {
                            List<FlightPakMasterService.ClientByClientCDResult> ClientList = new List<FlightPakMasterService.ClientByClientCDResult>();
                            ClientList = (List<FlightPakMasterService.ClientByClientCDResult>)objRetVal.ToList();
                            HdnFld.Value = ClientList[0].ClientID.ToString();
                            ReturnValue = true;
                        }
                        else
                        {
                            HdnFld.Value = string.Empty;
                            ReturnValue = false;
                        }
                    }
                }
                return ReturnValue;
            }
        }
        protected void tbIPHomebase_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbIPHomebase.Text))
                        {
                            if (CheckHomebaseExist(tbIPHomebase, hdnIPHomebase) == false)
                            {
                                cvIPHomebase.IsValid = false;
                                RadAjaxManager1.FocusControl(tbIPHomebase.ClientID);
                            }
                        }
                        else
                        {
                            hdnIPHomebase.Value = string.Empty;
                            RadAjaxManager1.FocusControl(tbIPHomebase.ClientID);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlan);
                }
            }
        }
        private bool CheckHomebaseExist(TextBox TxtBx, HiddenField HdnFld)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (!string.IsNullOrEmpty(TxtBx.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objMasterService.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD != null && x.HomebaseCD.Trim().ToUpper() == (tbIPHomebase.Text.Trim().ToUpper())).ToList();
                        if (objRetVal.Count() != 0)
                        {
                            List<FlightPakMasterService.GetAllCompanyMaster> AirportList = new List<FlightPakMasterService.GetAllCompanyMaster>();
                            AirportList = (List<FlightPakMasterService.GetAllCompanyMaster>)objRetVal.ToList();
                            HdnFld.Value = AirportList[0].HomebaseID.ToString();
                            hdnIPHomebaseAirportID.Value = AirportList[0].HomebaseAirportID.ToString();
                            ReturnValue = true;
                        }
                        else
                        {
                            HdnFld.Value = string.Empty;
                            ReturnValue = false;
                        }
                    }
                }
                return ReturnValue;
            }
        }
        #region "dgItineraryPlanning grid events"
        protected void dgItineraryPlanning_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UtilitiesService.ItineraryPlan oGetItineraryPlan = new UtilitiesService.ItineraryPlan();
                        oGetItineraryPlan.ItineraryPlanID = -1;
                        oGetItineraryPlan.IsDeleted = false;
                        List<UtilitiesService.GetItineraryPlan> GetItineraryPlanList = new List<UtilitiesService.GetItineraryPlan>();
                        using (UtilitiesService.UtilitiesServiceClient UtilitiesService = new UtilitiesService.UtilitiesServiceClient())
                        {
                            var GetItineraryPlanInfo = UtilitiesService.GetItineraryPlan(oGetItineraryPlan);
                            if (GetItineraryPlanInfo.ReturnFlag == true)
                            {
                                GetItineraryPlanList = GetItineraryPlanInfo.EntityList;
                            }
                            dgItineraryPlanning.DataSource = GetItineraryPlanList;
                            Session["ItineraryPlanList"] = GetItineraryPlanList;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlan);
                }
            }
        }
        protected void dgItineraryPlanning_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (btnSaveAll.Visible == false)
                        {
                            GridDataItem item = dgItineraryPlanning.SelectedItems[0] as GridDataItem;
                            Session["ItineraryPlanID"] = item.GetDataKeyValue("ItineraryPlanID").ToString();
                            LoadControlDataItineraryPlan();
                            EnableFormItineraryPlan(false);
                            GridEnableItineraryPlan(true, true, true);
                            DefaultSelectionItineraryPlanLegs(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlan);
                }
            }
        }
        protected void dgItineraryPlanning_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgItineraryPlanning.SelectedIndexes.Clear();
                                GridEnableItineraryPlan(false, false, false);
                                ClearFormItineraryPlan();
                                GetDefaultData(true);
                                EnableFormItineraryPlan(true);
                                hdnSaveItineraryPlans.Value = "Save";
                                Session["ItineraryPlanID"] = null;
                                Bind_ItineraryPlanLegs(true);
                                ClearFormItineraryPlanLegs();
                                DefaultMonth();
                                RadAjaxManager1.FocusControl(tbIPYear.ClientID);
                                break;
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    if (Session["ItineraryPlanID"] != null)
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Utilities.ItineraryPlan, Convert.ToInt64(Session["ItineraryPlanID"].ToString()));
                                        Session["IsEditLockAirportPair"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Utilities.ItineraryPlan);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Utilities.ItineraryPlan);
                                            return;
                                        }
                                    }
                                }
                                GridEnableItineraryPlan(false, false, false);
                                EnableFormItineraryPlan(true);
                                LoadControlDataItineraryPlan();
                                hdnSaveItineraryPlans.Value = "Update";
                                RadAjaxManager1.FocusControl(tbIPDescription.ClientID);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    column.CurrentFilterValue = string.Empty;
                                    column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlan);
                }
            }
        }

        public void DefaultMonth()
        {
            int Month = DateTime.UtcNow.Month;
            if (Month == 12 || Month == 1 || Month == 2)
            {
                ddlIPQuarter.SelectedValue = "1";
            }
            else if (Month == 3 || Month == 4 || Month == 5)
            {
                ddlIPQuarter.SelectedValue = "2";
            }
            else if (Month == 6 || Month == 7 || Month == 8)
            {
                ddlIPQuarter.SelectedValue = "3";
            }
            else if (Month == 9 || Month == 10 || Month == 11)
            {
                ddlIPQuarter.SelectedValue = "4";
            }
        }

        protected void dgItineraryPlanning_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidateCustom = true;
                        if (tbIPTypeCode.Text.Trim() == string.Empty && tbIPTailNumber.Text.Trim() == string.Empty)
                        {
                            string AlertMsg = "radalert('Aircraft Type Code or Tail No. is required', 360, 50, 'Itinerary Planning');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                            IsValidateCustom = false;
                        }
                        //IsValidateCustom = ValidateItineraryPlan();
                        if (IsValidateCustom == true)
                        {
                            UtilitiesService.ItineraryPlan oItineraryPlan = new UtilitiesService.ItineraryPlan();
                            UtilitiesService.ItineraryPlanDetail oItineraryPlanDetail = new UtilitiesService.ItineraryPlanDetail();
                            oItineraryPlan.ItineraryPlanDetails = new List<UtilitiesService.ItineraryPlanDetail>();
                            using (UtilitiesService.UtilitiesServiceClient UtilitiesService = new UtilitiesService.UtilitiesServiceClient())
                            {
                                oItineraryPlan = GetItemsItineraryPlan(oItineraryPlan);
                                if ((hdnSaveItineraryPlanLegs.Value == "Save") || (hdnSaveItineraryPlanLegs.Value == "Update"))
                                {
                                    CalculateAll(false, true, true, true, true, true, true, null);
                                    oItineraryPlanDetail = new UtilitiesService.ItineraryPlanDetail();
                                    oItineraryPlanDetail = GetItemsItineraryPlanLegs(oItineraryPlanDetail);
                                    oItineraryPlan.ItineraryPlanDetails.Add(oItineraryPlanDetail);
                                }
                                var Result = UtilitiesService.AddItineraryPlan(oItineraryPlan);
                                hdnIPItineraryPlanID.Value = Result.EntityInfo.ItineraryPlanID.ToString();
                                if (Result.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                    IsSaveSuccessItineraryPlan = true;
                                }
                            }
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPaxInfoPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlan);
                }
                finally
                {
                }
            }
        }
        protected void dgItineraryPlanning_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidateCustom = true;
                        if (tbIPTypeCode.Text.Trim() == string.Empty && tbIPTailNumber.Text.Trim() == string.Empty)
                        {
                            string AlertMsg = "radalert('Aircraft Type Code or Tail No. is required', 360, 50, 'Itinerary Planning');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                            IsValidateCustom = false;
                        }
                        //IsValidateCustom = ValidateItineraryPlan();
                        if (IsValidateCustom == true)
                        {
                            UtilitiesService.ItineraryPlan oItineraryPlan = new UtilitiesService.ItineraryPlan();
                            UtilitiesService.ItineraryPlanDetail oItineraryPlanDetail = new UtilitiesService.ItineraryPlanDetail();
                            oItineraryPlan.ItineraryPlanDetails = new List<UtilitiesService.ItineraryPlanDetail>();
                            using (UtilitiesService.UtilitiesServiceClient UtilitiesService = new UtilitiesService.UtilitiesServiceClient())
                            {
                                oItineraryPlan = GetItemsItineraryPlan(oItineraryPlan);
                                hdnIPItineraryPlanID.Value = oItineraryPlan.ItineraryPlanID.ToString();
                                if ((hdnSaveItineraryPlanLegs.Value == "Save") || (hdnSaveItineraryPlanLegs.Value == "Update"))
                                {
                                    CalculateAll(false, true, true, true, true, true, true, null);
                                    oItineraryPlanDetail = new UtilitiesService.ItineraryPlanDetail();
                                    oItineraryPlanDetail = GetItemsItineraryPlanLegs(oItineraryPlanDetail);
                                    oItineraryPlan.ItineraryPlanDetails.Add(oItineraryPlanDetail);
                                }
                                var Result = UtilitiesService.UpdateItineraryPlan(oItineraryPlan);
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    if (Session["ItineraryPlanID"] != null)
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Utilities.ItineraryPlan, Convert.ToInt64(Session["ItineraryPlanID"].ToString()));
                                        Session["IsEditLockAirportPair"] = "False";
                                    }
                                }
                                if (Result.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                    IsSaveSuccessItineraryPlan = true;
                                }
                            }
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = true;
                            if (IsPopUp)
                            {
                                //Clear session & close browser
                                Session["ItineraryPlanID"] = null;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPaxInfoPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlan);
                }
            }
        }
        protected void dgItineraryPlanning_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            e.Canceled = true;
                            UtilitiesService.ItineraryPlan oItineraryPlan = new UtilitiesService.ItineraryPlan();
                            GridDataItem Item = dgItineraryPlanning.SelectedItems[0] as GridDataItem;
                            oItineraryPlan.ItineraryPlanID = Convert.ToInt64(Item.GetDataKeyValue("ItineraryPlanID").ToString().Trim());
                            oItineraryPlan.IsDeleted = true;
                            using (UtilitiesService.UtilitiesServiceClient UtilitiesService = new UtilitiesService.UtilitiesServiceClient())
                            {
                                if (Session["ItineraryPlanID"] != null)
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Utilities.ItineraryPlan, Convert.ToInt64(Session["ItineraryPlanID"].ToString()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelectionItineraryPlan(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Utilities.ItineraryPlan);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Utilities.ItineraryPlan);
                                        return;
                                    }
                                }
                                UtilitiesService.DeleteItineraryPlan(oItineraryPlan);
                            }
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = true;
                            DefaultSelectionItineraryPlan(true);
                            DefaultSelectionItineraryPlanLegs(true);
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlan);
                    }
                    finally
                    {
                        if (Session["ItineraryPlanID"] != null)
                        {
                            var returnValue1 = CommonService.UnLock(EntitySet.Utilities.ItineraryPlan, Convert.ToInt64(Session["ItineraryPlanID"].ToString()));
                        }
                    }
                }
            }
        }
        protected void dgItineraryPlanning_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem dataItem = e.Item as GridDataItem;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlan);
                }
            }
        }
        #endregion
        protected void btnIPLTransfer_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgItineraryPlanning.SelectedItems.Count != 0)
                        {
                            GridDataItem Item = dgItineraryPlanning.SelectedItems[0] as GridDataItem;
                            UtilitiesService.ItineraryPlan oItineraryPlan = new UtilitiesService.ItineraryPlan();
                            if (Item.GetDataKeyValue("ItineraryPlanID") != null)
                            {
                                oItineraryPlan.ItineraryPlanID = Convert.ToInt64(Item.GetDataKeyValue("ItineraryPlanID").ToString().Trim());
                            }
                            using (UtilitiesService.UtilitiesServiceClient objService = new UtilitiesService.UtilitiesServiceClient())
                            {
                                var Result = objService.UpdateItineraryPlanTransfer(oItineraryPlan);
                                if (Result.ReturnFlag == true)
                                {
                                    RadWindowManager1.RadAlert("Itinerary Plan Transferred To Preflight With A Worksheet Status.", 400, 50, "Itinerary Plan.", "TransferCallBackfn");
                                }
                            }
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Please select a record from the above table.", 400, 50, "Itinerary Plan.", "TransferCallBackfn");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlanLegs);
                }
            }
        }
        protected void btnSaveItineraryPlans_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveItineraryPlans.Value == "Update")
                        {
                            (dgItineraryPlanning.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgItineraryPlanning.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlan);
                }
            }
        }
        protected void btnCancelItineraryPlans_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["ItineraryPlanID"] != null)
                        {
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Utilities.ItineraryPlan, Convert.ToInt64(Session["ItineraryPlanID"].ToString().Trim()));
                            }
                        }
                        Session["ItineraryPlanID"] = null;
                        DefaultSelectionItineraryPlan(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlan);
                }
            }
        }
        protected void btnSaveAll_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((hdnSaveItineraryPlans.Value == "Update") || (hdnSaveItineraryPlans.Value == "Save"))
                        {
                            IsSaveSuccessItineraryPlan = false;
                            if (ValidateItineraryPlan() == false)
                            {
                                return;
                            }
                        }
                        if ((hdnSaveItineraryPlanLegs.Value == "Update") || (hdnSaveItineraryPlanLegs.Value == "Save"))
                        {
                            IsSaveSuccessItineraryPlanLegs = false;
                            if (ValidateItineraryPlanLegs() == false)
                            {
                                return;
                            }
                        }
                        if (hdnSaveItineraryPlans.Value == "Save")
                        {
                            (dgItineraryPlanning.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            if (IsSaveSuccessItineraryPlan == true)
                            {
                                GridEnableItineraryPlan(true, true, true);
                                EnableFormItineraryPlan(false);
                                dgItineraryPlanning.Rebind();
                                DefaultSelectItemFormItineraryPlan();
                                GridEnableItineraryPlanLegs(true, true, true);
                                EnableFormItineraryPlanLegs(false);
                                dgItineraryPlanningLegs.Rebind();
                                DefaultSelectItemFormItineraryPlanLegs();
                                hdnSaveItineraryPlans.Value = string.Empty;
                                hdnSaveItineraryPlanLegs.Value = string.Empty;
                            }
                        }
                        else if (hdnSaveItineraryPlans.Value == "Update")
                        {
                            (dgItineraryPlanning.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            if (IsSaveSuccessItineraryPlan == true)
                            {
                                GridEnableItineraryPlan(true, true, true);
                                EnableFormItineraryPlan(false);
                                dgItineraryPlanning.Rebind();
                                DefaultSelectItemFormItineraryPlan();
                                GridEnableItineraryPlanLegs(true, true, true);
                                EnableFormItineraryPlanLegs(false);
                                dgItineraryPlanningLegs.Rebind();
                                DefaultSelectItemFormItineraryPlanLegs();
                                hdnSaveItineraryPlans.Value = string.Empty;
                                hdnSaveItineraryPlanLegs.Value = string.Empty;
                            }
                        }
                        if ((hdnSaveItineraryPlans.Value == string.Empty) && (hdnSaveItineraryPlanLegs.Value == "Save"))
                        {
                            (dgItineraryPlanningLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            if (IsSaveSuccessItineraryPlanLegs == true)
                            {
                                GridEnableItineraryPlanLegs(true, true, true);
                                EnableFormItineraryPlanLegs(false);
                                dgItineraryPlanningLegs.Rebind();
                                DefaultSelectItemFormItineraryPlanLegs();
                                hdnSaveItineraryPlans.Value = string.Empty;
                                hdnSaveItineraryPlanLegs.Value = string.Empty;
                            }
                        }
                        else if ((hdnSaveItineraryPlans.Value == string.Empty) && (hdnSaveItineraryPlanLegs.Value == "Update"))
                        {
                            (dgItineraryPlanningLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            if (IsSaveSuccessItineraryPlanLegs == true)
                            {
                                GridEnableItineraryPlanLegs(true, true, true);
                                EnableFormItineraryPlanLegs(false);
                                dgItineraryPlanningLegs.Rebind();
                                DefaultSelectItemFormItineraryPlanLegs();
                                hdnSaveItineraryPlans.Value = string.Empty;
                                hdnSaveItineraryPlanLegs.Value = string.Empty;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlan);
                }
            }
        }
        protected void btnCancelAll_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //if ((hdnSaveItineraryPlans.Value == "Update") || (hdnSaveItineraryPlans.Value == "Save"))
                        //{
                        if (Session["ItineraryPlanID"] != null)
                        {
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Utilities.ItineraryPlan, Convert.ToInt64(Session["ItineraryPlanID"].ToString().Trim()));
                            }
                        }
                        Session["ItineraryPlanID"] = null;
                        ClearFormItineraryPlan();
                        dgItineraryPlanning.SelectedIndexes.Clear();
                        DefaultSelectionItineraryPlan(true);
                        //}
                        //if ((hdnSaveItineraryPlanLegs.Value == "Update") || (hdnSaveItineraryPlanLegs.Value == "Save"))
                        //{
                        if (Session["ItineraryPlanDetailID"] != null)
                        {
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Utilities.ItineraryPlanLeg, Convert.ToInt64(Session["ItineraryPlanDetailID"].ToString().Trim()));
                            }
                        }
                        Session["ItineraryPlanDetailID"] = null;
                        ClearFormItineraryPlanLegs();
                        dgItineraryPlanningLegs.SelectedIndexes.Clear();
                        DefaultSelectionItineraryPlanLegs(true);
                        //}
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlan);
                }
            }
        }
        private bool ValidateItineraryPlan()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool IsValidateCustom = true;
                if ((string.IsNullOrEmpty(tbIPYear.Text)) && (IsValidateCustom == true))
                {
                    rfvIPYear.IsValid = false;
                    RadAjaxManager1.FocusControl(tbIPYear.ClientID);
                    IsValidateCustom = false;
                }
                if ((string.IsNullOrEmpty(tbIPTypeCode.Text)) && (IsValidateCustom == true))
                {
                    //rfvIPTypeCode.IsValid = false;
                    RadAjaxManager1.FocusControl(tbIPTypeCode.ClientID);
                    IsValidateCustom = false;
                }
                if ((CheckAircraftExist(tbIPTypeCode, hdnIPTypeCodeID) == false) && (IsValidateCustom == true))
                {
                    cvIPTypeCode.IsValid = false;
                    RadAjaxManager1.FocusControl(tbIPTypeCode.ClientID);
                    IsValidateCustom = false;
                }
                if ((CheckTailNumberExist(tbIPTailNumber, hdnIPTailNumber) == false) && (IsValidateCustom == true))
                {
                    cvIPTailNumber.IsValid = false;
                    RadAjaxManager1.FocusControl(tbIPTailNumber.ClientID);
                    IsValidateCustom = false;
                }
                if ((CheckClientExist(tbIPClientCode, hdnIPClientID) == false) && (IsValidateCustom == true))
                {
                    cvIPClientCode.IsValid = false;
                    RadAjaxManager1.FocusControl(tbIPClientCode.ClientID);
                    IsValidateCustom = false;
                }
                if ((CheckHomebaseExist(tbIPHomebase, hdnIPHomebase) == false) && (IsValidateCustom == true))
                {
                    cvIPHomebase.IsValid = false;
                    RadAjaxManager1.FocusControl(tbIPHomebase.ClientID);
                    IsValidateCustom = false;
                }
                if (tbIPTypeCode.Text.Trim() == string.Empty && tbIPTailNumber.Text.Trim() == string.Empty)
                {
                    string AlertMsg = "radalert('Type Code or Tail No. is required', 360, 50, 'Itinerary Planning');";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
                    IsValidateCustom = false;
                }
                return IsValidateCustom;
            }
        }
        private bool ValidateItineraryPlanLegs()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool IsValidateCustom = true;
                if ((string.IsNullOrEmpty(tbIPLDeparture.Text)) && (IsValidateCustom == true))
                {
                    rfvIPLDeparture.IsValid = false;
                    RadAjaxManager1.FocusControl(tbIPLDeparture.ClientID);
                    IsValidateCustom = false;
                }
                if ((string.IsNullOrEmpty(tbIPLArrive.Text)) && (IsValidateCustom == true))
                {
                    rfvIPLArrive.IsValid = false;
                    RadAjaxManager1.FocusControl(tbIPLArrive.ClientID);
                    IsValidateCustom = false;
                }
                if ((CheckAirportICAOExist(tbIPLDeparture, hdnIPLDepartureID, lbAPLDepartDesc) == false) && (IsValidateCustom == true))
                {
                    cvIPLDeparture.IsValid = false;
                    RadAjaxManager1.FocusControl(tbIPLDeparture.ClientID);
                    IsValidateCustom = false;
                }
                if ((CheckAirportICAOExist(tbIPLArrive, hdnIPLArriveID, lbAPLAirriveDesc) == false) && (IsValidateCustom == true))
                {
                    cvIPLArrive.IsValid = false;
                    RadAjaxManager1.FocusControl(tbIPLArrive.ClientID);
                    IsValidateCustom = false;
                }
                string DDate;
                string ADate;
                DateValidationError = string.Empty;
                if (IsValidateCustom == true)
                {
                    DDate = tbDepartLocalDate.Text;
                    ADate = tbArriveLocalDate.Text;
                    IsValidateCustom = ValidateDate(DDate, ADate, "Local");
                    if (IsValidateCustom == true)
                    {
                        IsValidateCustom = ValidateTime(DDate, tbDepartLocalTime.Text, ADate, tbArriveLocalTime.Text, "Local");
                    }
                }
                if (IsValidateCustom == true)
                {
                    DDate = tbDepartUTCDate.Text;
                    ADate = tbArriveUTCDate.Text;
                    IsValidateCustom = ValidateDate(DDate, ADate, "UTC");
                    if (IsValidateCustom == true)
                    {
                        IsValidateCustom = ValidateTime(DDate, tbDepartUTCTime.Text, ADate, tbArriveUTCTime.Text, "UTC");
                    }
                }
                if (IsValidateCustom == true)
                {
                    DDate = tbDepartHomeDate.Text;
                    ADate = tbArriveHomeDate.Text;
                    IsValidateCustom = ValidateDate(DDate, ADate, "Home");
                    if (IsValidateCustom == true)
                    {
                        IsValidateCustom = ValidateTime(DDate, tbDepartHomeTime.Text, ADate, tbArriveHomeTime.Text, "Home");
                    }
                }
                if (IsValidateCustom == true)
                {
                    string ErrorString = CheckAircraftCapability();
                    if (!string.IsNullOrEmpty(ErrorString.ToString().Trim()))
                    {
                        IsValidateCustom = true;
                        string alertMsg = "radalert('" + ErrorString + "', 360, 50, 'Itinerary Plan Legs');";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                    }
                }

                return IsValidateCustom;
            }
        }
        private bool ValidateDate(string DepartDate, string ArriveDate, string DateType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                //if (string.IsNullOrEmpty(DateFormat))
                //{
                //    DateFormat = "MM/dd/yyyy";
                //}
                //IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                //DateTime DDate = new DateTime();
                //DateTime ADate = new DateTime();
                //if (!(string.IsNullOrEmpty(DepartDate)))
                //{
                //    if (DateFormat != null)
                //    {
                //        DDate = Convert.ToDateTime(FormatDate(DepartDate, DateFormat, false));
                //    }
                //    else
                //    {
                //        DDate = Convert.ToDateTime(DepartDate);
                //    }
                //}
                //if (!(string.IsNullOrEmpty(ArriveDate)))
                //{
                //    if (DateFormat != null)
                //    {
                //        ADate = Convert.ToDateTime(FormatDate(ArriveDate, DateFormat, false));
                //    }
                //    else
                //    {
                //        ADate = Convert.ToDateTime(ArriveDate);
                //    }
                //}
                //if (!(String.IsNullOrEmpty(DepartDate)) && !(String.IsNullOrEmpty(ArriveDate)))
                //{
                //    if (DDate > ADate)
                //    {
                //        string alertMsg = "radalert('Departure " + DateType + " Date should be before Arrival " + DateType + " Date.', 360, 50, 'Itinerary Plan legs');";
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                //        ReturnValue = false;
                //    }
                //}
                //foreach (GridDataItem Item in dgItineraryPlanningLegs.Items)
                //{
                //    if (Item.GetDataKeyValue("LegNUM") != null)
                //    {
                //        if (!string.IsNullOrEmpty(hdnInsertLegNum.Value.Trim()))
                //        {
                //            if (Convert.ToInt32(hdnInsertLegNum.Value.Trim()) > Convert.ToInt32(Item.GetDataKeyValue("LegNUM").ToString().Trim()))
                //            {
                //                if (Item.GetDataKeyValue("ArrivalLocal") != null)
                //                {
                //                    if (DDate > Convert.ToDateTime(Item.GetDataKeyValue("ArrivalLocal").ToString().Trim()))
                //                    {
                //                        DateValidationError = "Warning - Current Leg Departure Date Overlaps Previous Leg Arrival Date.";
                //                        //string alertMsg = "radalert('Warning - Current Leg Departure Date Overlaps Previous Leg Arrival Date.', 360, 50, 'Itinerary Plan legs');";
                //                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                //                        break;
                //                        //ReturnValue = false;
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}
                return ReturnValue;
            }
        }
        private bool ValidateTime(string DepartDate, string DepartTime, string ArriveDate, string ArriveTime, string DateType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartDate, DepartTime, ArriveDate, ArriveTime, DateType))
            {
                bool ReturnValue = true;
                string[] SplitDDate = new string[3];
                string[] SplitADate = new string[3];
                if (!string.IsNullOrEmpty(DepartTime))
                {
                    SplitDDate = DepartTime.Split(':');
                }
                if (!string.IsNullOrEmpty(ArriveTime))
                {
                    SplitADate = ArriveTime.Split(':');
                }
                if (SplitDDate != null && SplitDDate.Length > 1)
                {
                    if (!string.IsNullOrEmpty(SplitDDate[0]))
                    {
                        if (Convert.ToInt32(SplitDDate[0]) > 23)
                        {
                            string alertMsg = "radalert('Departs " + DateType + " Time Hour should be between 0 and 23.', 360, 50, 'Itinerary Plan legs');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return ReturnValue = false;
                        }
                    }

                    if (!string.IsNullOrEmpty(SplitDDate[1]))
                    {
                        if (Convert.ToInt32(SplitDDate[1]) > 59)
                        {
                            string alertMsg = "radalert('Departs " + DateType + " Time Minute should be between 0 and 59.', 360, 50, 'Itinerary Plan legs');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return ReturnValue = false;
                        }
                    }
                }
                if (SplitADate != null && SplitADate.Length > 1)
                {
                    if (!string.IsNullOrEmpty(SplitADate[0]))
                    {
                        if (Convert.ToInt32(SplitADate[0]) > 23)
                        {
                            string alertMsg = "radalert('Arrive " + DateType + " Time Hour should be between 0 and 23.', 360, 50, 'Itinerary Plan legs');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return ReturnValue = false;
                        }
                    }
                    if (!string.IsNullOrEmpty(SplitADate[1]))
                    {
                        if (Convert.ToInt32(SplitADate[1]) > 59)
                        {
                            string alertMsg = "radalert('Arrive " + DateType + " Time Minute should be between 0 and 59.', 360, 50, 'Itinerary Plan legs');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                            return ReturnValue = false;
                        }
                    }
                }

                //if (DepartDate == ArriveDate)
                //{
                //    if ((!string.IsNullOrEmpty(SplitDDate[0])) && (!string.IsNullOrEmpty(SplitADate[0])))
                //    {
                //        if (Convert.ToInt32(SplitDDate[0]) > Convert.ToInt32(SplitADate[0]))
                //        {
                //            string alertMsg = "radalert('Departure " + DateType + " Time should be before Arrival " + DateType + " Time.', 360, 50, 'Itinerary Plan legs');";
                //            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                //            return ReturnValue = false;
                //        }
                //        if (Convert.ToInt32(SplitDDate[0]) == Convert.ToInt32(SplitADate[0]))
                //        {
                //            if (((!string.IsNullOrEmpty(SplitDDate[1])) && (!string.IsNullOrEmpty(SplitADate[1]))) && ReturnValue == true)
                //            {
                //                if (Convert.ToInt32(SplitDDate[1]) > Convert.ToInt32(SplitADate[1]))
                //                {
                //                    string alertMsg = "radalert('Departure " + DateType + " Time should be before Arrival " + DateType + " Time.', 360, 50, 'Itinerary Plan legs');";
                //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                //                    return ReturnValue = false;
                //                }
                //            }
                //        }
                //    }
                //}
                if (DateType == "Local")
                {
                    if (string.IsNullOrEmpty(DateValidationError))
                    {
                        foreach (GridDataItem Item in dgItineraryPlanningLegs.Items)
                        {
                            if (Item.GetDataKeyValue("LegNUM") != null)
                            {
                                if (!string.IsNullOrEmpty(hdnInsertLegNum.Value.Trim()))
                                {
                                    //if (Convert.ToInt32(hdnInsertLegNum.Value.Trim()) > Convert.ToInt32(Item.GetDataKeyValue("LegNUM").ToString().Trim()))
                                    if ((Convert.ToInt32(hdnInsertLegNum.Value.Trim()) - 1) == Convert.ToInt32(Item.GetDataKeyValue("LegNUM").ToString().Trim()))
                                    {
                                        if (Item.GetDataKeyValue("ArrivalLocal") != null)
                                        {
                                            DepartDate = FormatDate(DepartDate, DateFormat, false);
                                            if (Convert.ToDateTime(string.Format("{0} {1}", DepartDate, DepartTime, CultureInfo.InvariantCulture)) < Convert.ToDateTime(Item.GetDataKeyValue("ArrivalLocal").ToString().Trim()))
                                            {
                                                DateValidationError = "Warning - Current Leg Departure Date Time Overlaps Previous Leg Arrival Date Time.";
                                                //string alertMsg = "radalert('Warning - Current Leg Departure Date Overlaps Previous Leg Arrival Date.', 360, 50, 'Itinerary Plan legs');";
                                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                                break;
                                                //ReturnValue = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return ReturnValue;
            }
        }
        private void GetDefaultData(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (UserPrincipal != null && UserPrincipal.Identity._clientId != null && UserPrincipal.Identity._clientId != 0)
                {
                    if (!string.IsNullOrEmpty(tbIPClientCode.Text))
                    {
                        if (tbIPClientCode.Text.Trim() == UserPrincipal.Identity._clientCd.ToString().Trim())
                        {
                            tbIPClientCode.Enabled = false;
                        }
                        else
                        {
                            tbIPClientCode.Enabled = true;
                        }
                    }
                    tbIPClientCode.Text = UserPrincipal.Identity._clientCd.ToString().Trim();
                    hdnIPClientID.Value = UserPrincipal.Identity._clientId.ToString().Trim();
                    btnIPClientCode.Enabled = false;
                }
                else
                {
                    tbIPClientCode.Enabled = Enable;
                    btnIPClientCode.Enabled = Enable;
                }
                if (UserPrincipal != null && UserPrincipal.Identity._airportICAOCd != null && UserPrincipal.Identity._airportId != 0)
                {
                    tbIPHomebase.Text = UserPrincipal.Identity._airportICAOCd.ToString().Trim();
                    hdnIPHomebase.Value = UserPrincipal.Identity._airportId.ToString().Trim();
                    hdnIPHomebaseAirportID.Value = UserPrincipal.Identity._airportId.ToString().Trim();
                }
            }
        }
        #endregion
        #region "Itinerary Plan Legs
        private void DefaultSelectionItineraryPlanLegs(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgItineraryPlanningLegs.Rebind();
                    }
                    if (dgItineraryPlanningLegs.MasterTableView.Items.Count > 0)
                    {
                        dgItineraryPlanningLegs.SelectedIndexes.Add(0);
                        Session["ItineraryPlanDetailID"] = dgItineraryPlanningLegs.Items[0].GetDataKeyValue("ItineraryPlanDetailID").ToString();
                        LoadControlDataItineraryPlanLegs();
                        EnableFormItineraryPlanLegs(false);
                        GridEnableItineraryPlanLegs(true, true, true);
                    }
                    else
                    {
                        Session["ItineraryPlanDetailID"] = null;
                        ClearFormItineraryPlanLegs();
                        EnableFormItineraryPlanLegs(false);
                        GridEnableItineraryPlanLegs(true, true, true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void DefaultSelectItemFormItineraryPlanLegs()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["ItineraryPlanDetailID"] != null)
                    {
                        string ID = Session["ItineraryPlanDetailID"].ToString();
                        foreach (GridDataItem Item in dgItineraryPlanningLegs.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("ItineraryPlanDetailID").ToString() == ID.Trim())
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                        if (dgItineraryPlanningLegs.SelectedItems.Count == 0)
                        {
                            DefaultSelectionItineraryPlanLegs(false);
                        }
                    }
                    else
                    {
                        DefaultSelectionItineraryPlanLegs(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void GridEnableItineraryPlanLegs(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                LinkButton insertCtl, editCtl, delCtl, insertAddCtl;
                insertCtl = (LinkButton)dgItineraryPlanningLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                insertAddCtl = (LinkButton)dgItineraryPlanningLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitAdd");
                editCtl = (LinkButton)dgItineraryPlanningLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                delCtl = (LinkButton)dgItineraryPlanningLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                if (IsAuthorized(Permission.Utilities.AddItineraryPlanLegs))
                {
                    insertCtl.Visible = true;
                    if (Add && dgItineraryPlanning.Items.Count > 0 && dgItineraryPlanningLegs.Items.Count > 0)
                        insertCtl.Enabled = true;
                    else
                        insertCtl.Enabled = false;
                }
                else
                {
                    insertCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Utilities.AddItineraryPlanLegs))
                {
                    insertAddCtl.Visible = true;
                    if (Add && dgItineraryPlanning.Items.Count > 0)
                        insertAddCtl.Enabled = true;
                    else
                        insertAddCtl.Enabled = false;
                }
                else
                {
                    insertAddCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Utilities.EditItineraryPlanLegs))
                {
                    editCtl.Visible = true;
                    if (Edit && dgItineraryPlanning.Items.Count > 0)
                    {
                        editCtl.Enabled = true;
                        editCtl.OnClientClick = "javascript:return ProcessUpdateItineraryPlanLegs();";
                    }
                    else
                    {
                        editCtl.Enabled = false;
                        editCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    editCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Utilities.DeleteItineraryPlanLegs))
                {
                    delCtl.Visible = true;
                    if (Delete && dgItineraryPlanning.Items.Count > 0)
                    {
                        delCtl.Enabled = true;
                        delCtl.OnClientClick = "javascript:return ProcessDeleteItineraryPlanLegs();";
                    }
                    else
                    {
                        delCtl.Enabled = false;
                        delCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    delCtl.Visible = false;
                }
            }
        }
        private void EnableFormItineraryPlanLegs(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                //tbIPLDeparture.Enabled = Enable;                
                //tbIPLDeparture.ForeColor = System.Drawing.Color.Black;

                // Fix for UW-1165(8179)
                tbIPLDeparture.ReadOnly = !Enable;                
                
                //tbIPLArrive.Enabled = Enable;
                //tbIPLArrive.ForeColor = System.Drawing.Color.Black;
                
                // Fix for UW-1165(8179)
                tbIPLArrive.ReadOnly = !Enable;

                btnBrowseIPLDeparture.Enabled = Enable;
                btnBrowseIPLArrive.Enabled = Enable;
                tbIPLPaxNumber.Enabled = Enable;
                tbIPLTotalCost.Enabled = false;
                tbIPLMiles.Enabled = Enable;
                tbIPLTOBias.Enabled = Enable;
                imgbtnIPLTOBias.Enabled = Enable;
                tbIPLTAS.Enabled = Enable;
                rblistIPLWindReliability.Enabled = Enable;
                tbIPLPower.Enabled = Enable;
                btnBrowseIPLPower.Enabled = Enable;
                tbIPLLandBias.Enabled = Enable;
                btnBrowseIPLLandBias.Enabled = Enable;
                tbIPLWinds.Enabled = Enable;
                tbIPLEte.Enabled = false;
                tbIPLCost.Enabled = false;
                tbDepartLocalDate.Enabled = Enable;
                tbDepartLocalTime.Enabled = Enable;
                tbDepartUTCDate.Enabled = Enable;
                tbDepartUTCTime.Enabled = Enable;
                tbDepartHomeDate.Enabled = Enable;
                tbDepartHomeTime.Enabled = Enable;
                tbArriveLocalDate.Enabled = Enable;
                tbArriveLocalTime.Enabled = Enable;
                tbArriveUTCDate.Enabled = Enable;
                tbArriveUTCTime.Enabled = Enable;
                tbArriveHomeDate.Enabled = Enable;
                tbArriveHomeTime.Enabled = Enable;
                btnSaveItineraryPlanLegs.Visible = Enable;
                btnCancelItineraryPlanLegs.Visible = Enable;
                btnSaveAll.Visible = Enable;
                btnCancelAll.Visible = Enable;
                btnSaveAll.ValidationGroup = "SaveItineraryPlanLegs";
                if (dgItineraryPlanning.Items.Count != 0)
                {
                    btnIPLTransfer.Enabled = true;
                }
                else
                {
                    btnIPLTransfer.Enabled = false;
                }
                btnIPLAlerts.Enabled = !(Enable);
                btnIPLTransfer.Enabled = !(Enable);
            }
        }
        private void ClearFormItineraryPlanLegs()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                hdnSaveItineraryPlanLegs.Value = string.Empty;
                if (UserPrincipal.Identity._airportId != null)
                {
                    hdnIPHomebaseAirportID.Value = UserPrincipal.Identity._airportId.ToString().Trim();
                }
                tbIPLDeparture.Text = string.Empty;
                hdnIPLDepartureID.Value = string.Empty;
                lbAPLDepartDesc.Text = string.Empty;
                tbIPLArrive.Text = string.Empty;
                hdnIPLArriveID.Value = string.Empty;
                lbAPLAirriveDesc.Text = string.Empty;
                tbIPLPaxNumber.Text = string.Empty;
                tbIPLTotalCost.Text = string.Empty;
                tbIPLTotalCost.Text = CurrencyType + "0.00";
                tbIPLMiles.Text = string.Empty;
                hdnIPLMiles.Value = string.Empty;
                tbIPLTOBias.Text = string.Empty;
                tbIPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", "0.00");
                tbIPLTAS.Text = string.Empty;
                rblistIPLWindReliability.SelectedIndex = 0;
                tbIPLPower.Text = string.Empty;
                tbIPLLandBias.Text = string.Empty;
                tbIPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", "0.00");
                tbIPLWinds.Text = string.Empty;
                tbIPLEte.Text = string.Empty;
                tbIPLEte.Text = string.Format("{0:" + TenthMinFormat + "}", "0.00");
                tbIPLCost.Text = string.Empty;
                tbIPLCost.Text = CurrencyType + string.Format("{0:" + TenthMinFormat + "}", "0.00");
                DateTime dtToday = DateTime.Today;
                if (string.IsNullOrEmpty(DateFormat))
                {
                    DateFormat = "MM/dd/yyyy";
                }
                tbDepartLocalDate.Text = string.Empty;
                //if (DateFormat != null)
                //{
                //    tbDepartLocalDate.Text = String.Format("{0:" + DateFormat + "}", dtToday);
                //}
                tbDepartLocalTime.Text = ResetTime;
                tbDepartUTCDate.Text = string.Empty;
                //if (DateFormat != null)
                //{
                //    tbDepartUTCDate.Text = String.Format("{0:" + DateFormat + "}", dtToday);
                //}
                tbDepartUTCTime.Text = ResetTime;
                tbDepartHomeDate.Text = string.Empty;
                //if (DateFormat != null)
                //{
                //    tbDepartHomeDate.Text = String.Format("{0:" + DateFormat + "}", dtToday);
                //}
                tbDepartHomeTime.Text = ResetTime;
                tbArriveLocalDate.Text = string.Empty;
                //if (DateFormat != null)
                //{
                //    tbArriveLocalDate.Text = String.Format("{0:" + DateFormat + "}", dtToday);
                //}
                tbArriveLocalTime.Text = ResetTime;
                tbArriveUTCDate.Text = string.Empty;
                //if (DateFormat != null)
                //{
                //    tbArriveUTCDate.Text = String.Format("{0:" + DateFormat + "}", dtToday);
                //}
                tbArriveUTCTime.Text = ResetTime;
                tbArriveHomeDate.Text = string.Empty;
                //if (DateFormat != null)
                //{
                //    tbArriveHomeDate.Text = String.Format("{0:" + DateFormat + "}", dtToday);
                //}
                tbArriveHomeTime.Text = ResetTime;
                hdnInsertLegNum.Value = string.Empty;
            }
        }
        private void LoadControlDataItineraryPlanLegs()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgItineraryPlanningLegs.SelectedItems.Count != 0)
                {
                    GridDataItem Item = dgItineraryPlanningLegs.SelectedItems[0] as GridDataItem;
                    if (Item.GetDataKeyValue("ItineraryPlanDetailID").ToString().Trim().ToUpper() == Session["ItineraryPlanDetailID"].ToString().Trim().ToUpper())
                    {
                        if (Item.GetDataKeyValue("ItineraryPlanDetailID") != null)
                        {
                            hdnItineraryPlanLegsID.Value = Item.GetDataKeyValue("ItineraryPlanDetailID").ToString().Trim();
                        }
                        else
                        {
                            hdnItineraryPlanLegsID.Value = string.Empty;
                        }
                        if (Item.GetDataKeyValue("DAirportIcaoID") != null)
                        {
                            tbIPLDeparture.Text = Item.GetDataKeyValue("DAirportIcaoID").ToString().Trim();
                        }
                        else
                        {
                            tbIPLDeparture.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("DAirportID") != null)
                        {
                            hdnIPLDepartureID.Value = Item.GetDataKeyValue("DAirportID").ToString().Trim();
                        }
                        else
                        {
                            hdnIPLDepartureID.Value = string.Empty;
                        }
                        CheckAirportICAOExist(tbIPLDeparture, hdnIPLDepartureID, lbAPLDepartDesc);
                        if (Item.GetDataKeyValue("AAirportIcaoID") != null)
                        {
                            tbIPLArrive.Text = Item.GetDataKeyValue("AAirportIcaoID").ToString().Trim();
                        }
                        else
                        {
                            tbIPLArrive.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("AAirportID") != null)
                        {
                            hdnIPLArriveID.Value = Item.GetDataKeyValue("AAirportID").ToString().Trim();
                        }
                        else
                        {
                            hdnIPLArriveID.Value = string.Empty;
                        }
                        CheckAirportICAOExist(tbIPLArrive, hdnIPLArriveID, lbAPLAirriveDesc);
                        if (Item.GetDataKeyValue("PassengerNUM") != null)
                        {
                            tbIPLPaxNumber.Text = Item.GetDataKeyValue("PassengerNUM").ToString().Trim();
                        }
                        else
                        {
                            tbIPLPaxNumber.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("Distance") != null)
                        {
                            tbIPLMiles.Text = Item.GetDataKeyValue("Distance").ToString().Trim();
                            hdnIPLMiles.Value = Item.GetDataKeyValue("Distance").ToString().Trim();
                        }
                        else
                        {
                            tbIPLMiles.Text = string.Empty;
                            hdnIPLMiles.Value = string.Empty;
                        }
                        //if (Item.GetDataKeyValue("Distance") != null)
                        //{
                        //    tbIPLMiles.Text = Item.GetDataKeyValue("Distance").ToString().Trim();
                        //}
                        //else
                        //{
                        //    tbIPLMiles.Text = string.Empty;
                        //}
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                        {
                            if (Item.GetDataKeyValue("TakeoffBIAS") != null)
                            {
                                tbIPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", Math.Round(Convert.ToDecimal(Item.GetDataKeyValue("TakeoffBIAS").ToString().Trim()), 1)).Trim();
                            }
                            else
                            {
                                tbIPLTOBias.Text = string.Empty;
                            }
                        }
                        else
                        {
                            if (Item.GetDataKeyValue("TakeoffBIAS") != null)
                            {
                                tbIPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", ConvertTenthsToMins(Item.GetDataKeyValue("TakeoffBIAS").ToString().Trim())).Trim();
                            }
                            else
                            {
                                tbIPLTOBias.Text = string.Empty;
                            }
                        }
                        if (Item.GetDataKeyValue("TrueAirSpeed") != null)
                        {
                            tbIPLTAS.Text = Item.GetDataKeyValue("TrueAirSpeed").ToString().Trim();
                        }
                        else
                        {
                            tbIPLTAS.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("PowerSetting") != null)
                        {
                            tbIPLPower.Text = Item.GetDataKeyValue("PowerSetting").ToString().Trim();
                        }
                        if (Item.GetDataKeyValue("WindReliability") != null)
                        {
                            rblistIPLWindReliability.SelectedValue = Item.GetDataKeyValue("WindReliability").ToString().Trim();
                        }
                        else
                        {
                            rblistIPLWindReliability.SelectedIndex = 0;
                        }
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                        {
                            if (Item.GetDataKeyValue("LandingBias") != null)
                            {
                                tbIPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", Math.Round(Convert.ToDecimal(Item.GetDataKeyValue("LandingBias").ToString().Trim()), 1)).Trim();
                            }
                            else
                            {
                                tbIPLLandBias.Text = string.Empty;
                            }
                        }
                        else
                        {
                            if (Item.GetDataKeyValue("LandingBias") != null)
                            {
                                tbIPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", ConvertTenthsToMins(Item.GetDataKeyValue("LandingBias").ToString().Trim())).Trim();
                            }
                            else
                            {
                                tbIPLLandBias.Text = string.Empty;
                            }
                        }
                        if (Item.GetDataKeyValue("WindsBoeingTable") != null)
                        {
                            tbIPLWinds.Text = Item.GetDataKeyValue("WindsBoeingTable").ToString().Trim();
                        }
                        else
                        {
                            tbIPLWinds.Text = string.Empty;
                        }
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                        {
                            if (Item.GetDataKeyValue("ElapseTM") != null)
                            {
                                tbIPLEte.Text = string.Format("{0:" + TenthMinFormat + "}", Math.Round(Convert.ToDecimal(Item.GetDataKeyValue("ElapseTM").ToString().Trim()), 1));
                            }
                            else
                            {
                                tbIPLEte.Text = string.Empty;
                            }
                        }
                        else
                        {
                            if (Item.GetDataKeyValue("ElapseTM") != null)
                            {
                                tbIPLEte.Text = string.Format("{0:" + TenthMinFormat + "}", ConvertTenthsToMins(Item.GetDataKeyValue("ElapseTM").ToString().Trim()));
                            }
                            else
                            {
                                tbIPLEte.Text = string.Empty;
                            }
                        }
                        if (Item.GetDataKeyValue("Cost") != null)
                        {
                            tbIPLCost.Text = CurrencyType + string.Format("{0:" + CurrencyFormat + "}", Math.Round(Convert.ToDecimal(Item.GetDataKeyValue("Cost").ToString().Trim()), 2));
                        }
                        else
                        {
                            tbIPLCost.Text = string.Empty;
                            tbIPLCost.Text = CurrencyType + string.Format("{0:" + CurrencyFormat + "}", "0.00");
                        }
                        tbIPLTotalCost.Text = "0";
                        tbIPLTotalCost.Text = CurrencyType + string.Format("{0:" + CurrencyFormat + "}", "0.00");
                        CalculateTotalCost();
                        string Date = string.Empty, Time = string.Empty;
                        if (Item.GetDataKeyValue("DepartureLocal") != null)
                        {
                            Date = string.Empty;
                            Time = string.Empty;
                            SplitDateAndTime(Item.GetDataKeyValue("DepartureLocal").ToString(), ref Date, ref Time);
                            if (DateFormat != null)
                            {
                                tbDepartLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Date);
                                //FormatDate(Date, DateFormat);
                                //String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Date));
                            }
                            else
                            {
                                tbDepartLocalDate.Text = Date;
                            }
                            if (!string.IsNullOrEmpty(Time))
                            {
                                tbDepartLocalTime.Text = Time;
                            }
                            else
                            {
                                tbDepartLocalTime.Text = ResetTime;
                            }
                        }
                        else
                        {
                            tbDepartLocalDate.Text = string.Empty;
                            tbDepartLocalTime.Text = ResetTime;
                        }
                        if (Item.GetDataKeyValue("DepartureGMT") != null)
                        {
                            Date = string.Empty;
                            Time = string.Empty;
                            SplitDateAndTime(Item.GetDataKeyValue("DepartureGMT").ToString(), ref Date, ref Time);
                            if (DateFormat != null)
                            {
                                tbDepartUTCDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Date);
                                //FormatDate(Date, DateFormat);
                                //String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Date));
                            }
                            else
                            {
                                tbDepartUTCDate.Text = Date;
                            }
                            if (!string.IsNullOrEmpty(Time))
                            {
                                tbDepartUTCTime.Text = Time;
                            }
                            else
                            {
                                tbDepartUTCTime.Text = ResetTime;
                            }
                        }
                        else
                        {
                            tbDepartUTCDate.Text = string.Empty;
                            tbDepartUTCTime.Text = ResetTime;
                        }
                        if (Item.GetDataKeyValue("DepartureHome") != null)
                        {
                            Date = string.Empty;
                            Time = string.Empty;
                            SplitDateAndTime(Item.GetDataKeyValue("DepartureHome").ToString(), ref Date, ref Time);
                            if (DateFormat != null)
                            {
                                tbDepartHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Date);
                                //FormatDate(Date, DateFormat);
                                //String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Date));
                            }
                            else
                            {
                                tbDepartHomeDate.Text = Date;
                            }
                            if (!string.IsNullOrEmpty(Time))
                            {
                                tbDepartHomeTime.Text = Time;
                            }
                            else
                            {
                                tbDepartHomeTime.Text = ResetTime;
                            }
                        }
                        else
                        {
                            tbDepartHomeDate.Text = string.Empty;
                            tbDepartHomeTime.Text = ResetTime;
                        }
                        if (Item.GetDataKeyValue("ArrivalLocal") != null)
                        {
                            Date = string.Empty;
                            Time = string.Empty;
                            SplitDateAndTime(Item.GetDataKeyValue("ArrivalLocal").ToString(), ref Date, ref Time);
                            if (DateFormat != null)
                            {
                                tbArriveLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Date);
                                //FormatDate(Date, DateFormat);
                                //String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Date));
                            }
                            else
                            {
                                tbArriveLocalDate.Text = Date;
                            }
                            if (!string.IsNullOrEmpty(Time))
                            {
                                tbArriveLocalTime.Text = Time;
                            }
                            else
                            {
                                tbArriveLocalTime.Text = ResetTime;
                            }
                        }
                        else
                        {
                            tbArriveLocalDate.Text = string.Empty;
                            tbArriveLocalTime.Text = ResetTime;
                        }
                        if (Item.GetDataKeyValue("ArrivalGMT") != null)
                        {
                            Date = string.Empty;
                            Time = string.Empty;
                            SplitDateAndTime(Item.GetDataKeyValue("ArrivalGMT").ToString(), ref Date, ref Time);
                            if (DateFormat != null)
                            {
                                tbArriveUTCDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Date);
                                //FormatDate(Date, DateFormat);
                                //String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Date));
                            }
                            else
                            {
                                tbArriveUTCDate.Text = Date;
                            }
                            if (!string.IsNullOrEmpty(Time))
                            {
                                tbArriveUTCTime.Text = Time;
                            }
                            else
                            {
                                tbArriveUTCTime.Text = ResetTime;
                            }
                        }
                        else
                        {
                            tbArriveUTCDate.Text = string.Empty;
                            tbArriveUTCTime.Text = ResetTime;
                        }
                        if (Item.GetDataKeyValue("ArrivalHome") != null)
                        {
                            Date = string.Empty;
                            Time = string.Empty;
                            SplitDateAndTime(Item.GetDataKeyValue("ArrivalHome").ToString(), ref Date, ref Time);
                            if (DateFormat != null)
                            {
                                tbArriveHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Date);
                                //FormatDate(Date, DateFormat);
                                //String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Date));
                            }
                            else
                            {
                                tbArriveHomeDate.Text = Date;
                            }
                            if (!string.IsNullOrEmpty(Time))
                            {
                                tbArriveHomeTime.Text = Time;
                            }
                            else
                            {
                                tbArriveHomeTime.Text = ResetTime;
                            }
                        }
                        else
                        {
                            tbArriveHomeDate.Text = string.Empty;
                            tbArriveHomeTime.Text = ResetTime;
                        }
                        Label lbLastUpdatedUser;
                        lbLastUpdatedUser = (Label)dgItineraryPlanningLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                        if (Item.GetDataKeyValue("LastUpdUID") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                        }
                        else
                        {
                            lbLastUpdatedUser.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("LastUpdTS") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString())));
                        }
                    }
                }
                else
                {
                }
            }
        }
        private UtilitiesService.ItineraryPlanDetail GetItemsItineraryPlanLegs(UtilitiesService.ItineraryPlanDetail oItineraryPlanDetail)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oItineraryPlanDetail))
            {
                if (hdnSaveItineraryPlanLegs.Value == "Update")
                {
                    if (!string.IsNullOrEmpty(hdnItineraryPlanLegsID.Value))
                    {
                        oItineraryPlanDetail.ItineraryPlanDetailID = Convert.ToInt64(hdnItineraryPlanLegsID.Value);
                    }
                }
                else
                {
                    Identity = Identity - 1;
                    oItineraryPlanDetail.ItineraryPlanDetailID = Identity;
                }
                if (!string.IsNullOrEmpty(hdnIPItineraryPlanID.Value))
                {
                    oItineraryPlanDetail.ItineraryPlanID = Convert.ToInt64(hdnIPItineraryPlanID.Value);
                }
                if (!string.IsNullOrEmpty(hdnIPLDepartureID.Value))
                {
                    oItineraryPlanDetail.DAirportID = Convert.ToInt64(hdnIPLDepartureID.Value);
                }
                if (!string.IsNullOrEmpty(hdnIPLArriveID.Value))
                {
                    oItineraryPlanDetail.AAirportID = Convert.ToInt64(hdnIPLArriveID.Value);
                }
                if (!string.IsNullOrEmpty(tbIPLPaxNumber.Text))
                {
                    oItineraryPlanDetail.PassengerNUM = Convert.ToInt64(tbIPLPaxNumber.Text);
                }
                if (!string.IsNullOrEmpty(hdnInsertLegNum.Value))
                {
                    oItineraryPlanDetail.LegNUM = Convert.ToInt32(hdnInsertLegNum.Value);
                }
                //if (!string.IsNullOrEmpty(tbIPLMiles.Text))
                //{
                //    oItineraryPlanDetail.Distance = Convert.ToDecimal(tbIPLMiles.Text);
                //}
                if (!string.IsNullOrEmpty(hdnIPLMiles.Value))
                {
                    oItineraryPlanDetail.Distance = Convert.ToDecimal(hdnIPLMiles.Value);
                }
                if (!string.IsNullOrEmpty(tbIPLTOBias.Text))
                {
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                    {
                        oItineraryPlanDetail.TakeoffBIAS = Convert.ToDecimal(tbIPLTOBias.Text);
                    }
                    else
                    {
                        oItineraryPlanDetail.TakeoffBIAS = Convert.ToDecimal(ConvertMinToTenths(tbIPLTOBias.Text, true, string.Empty));
                    }
                }
                if (!string.IsNullOrEmpty(tbIPLTAS.Text))
                {
                    oItineraryPlanDetail.TrueAirSpeed = Convert.ToDecimal(tbIPLTAS.Text);
                }
                oItineraryPlanDetail.WindReliability = Convert.ToInt32(rblistIPLWindReliability.SelectedValue);
                if (!string.IsNullOrEmpty(tbIPLPower.Text))
                {
                    oItineraryPlanDetail.PowerSetting = tbIPLPower.Text;
                }
                if (!string.IsNullOrEmpty(tbIPLPower.Text))
                {
                    oItineraryPlanDetail.PowerSetting = tbIPLPower.Text;
                }
                if (!string.IsNullOrEmpty(tbIPLLandBias.Text))
                {
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                    {
                        oItineraryPlanDetail.LandingBias = Convert.ToDecimal(tbIPLLandBias.Text);
                    }
                    else
                    {
                        oItineraryPlanDetail.LandingBias = Convert.ToDecimal(ConvertMinToTenths(tbIPLLandBias.Text, true, string.Empty));
                    }
                }
                if (!string.IsNullOrEmpty(tbIPLWinds.Text))
                {
                    oItineraryPlanDetail.WindsBoeingTable = Convert.ToDecimal(tbIPLWinds.Text);
                }
                if (!string.IsNullOrEmpty(tbIPLEte.Text))
                {
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                    {
                        oItineraryPlanDetail.ElapseTM = Convert.ToDecimal(tbIPLEte.Text);
                    }
                    else
                    {
                        oItineraryPlanDetail.ElapseTM = Convert.ToDecimal(ConvertMinToTenths(tbIPLEte.Text, true, string.Empty));
                    }
                }
                if (!string.IsNullOrEmpty(tbIPLCost.Text))
                {
                    oItineraryPlanDetail.Cost = Convert.ToDecimal(tbIPLCost.Text.Replace(CurrencyType, string.Empty));
                }
                if (string.IsNullOrEmpty(DateFormat))
                {
                    DateFormat = "MM/dd/yyyy";
                }
                DateTime DateAndTime;
                string DateString = string.Empty;
                string Date = string.Empty, Time = string.Empty;
                if (!string.IsNullOrEmpty(tbDepartLocalDate.Text))
                {
                    DateString = string.Empty; Date = string.Empty; Time = string.Empty;
                    Date = tbDepartLocalDate.Text;
                    Time = tbDepartLocalTime.Text;
                    DateString = string.Format("{0} {1}", Date, Time, CultureInfo.InvariantCulture);
                    DateAndTime = Convert.ToDateTime(FormatDate(DateString, DateFormat, true));
                    oItineraryPlanDetail.DepartureLocal = DateAndTime;
                    //oItineraryPlanDetail.DepartureLocal = Convert.ToDateTime(FormatDate(tbDepartLocalDate.Text, DateFormat));                    
                }
                if (!string.IsNullOrEmpty(tbDepartUTCDate.Text))
                {
                    DateString = string.Empty; Date = string.Empty; Time = string.Empty;
                    Date = tbDepartUTCDate.Text;
                    Time = tbDepartUTCTime.Text;
                    DateString = string.Format("{0} {1}", Date, Time, CultureInfo.InvariantCulture);
                    DateAndTime = Convert.ToDateTime(FormatDate(DateString, DateFormat, true));
                    oItineraryPlanDetail.DepartureGMT = DateAndTime;
                    //oItineraryPlanDetail.DepartureGMT = Convert.ToDateTime(FormatDate(tbDepartUTCDate.Text, DateFormat));
                }
                if (!string.IsNullOrEmpty(tbDepartHomeDate.Text))
                {
                    DateString = string.Empty; Date = string.Empty; Time = string.Empty;
                    Date = tbDepartHomeDate.Text;
                    Time = tbDepartHomeTime.Text;
                    DateString = string.Format("{0} {1}", Date, Time, CultureInfo.InvariantCulture);
                    DateAndTime = Convert.ToDateTime(FormatDate(DateString, DateFormat, true));
                    oItineraryPlanDetail.DepartureHome = DateAndTime;
                    //oItineraryPlanDetail.DepartureHome = Convert.ToDateTime(FormatDate(tbDepartHomeDate.Text, DateFormat));
                }
                if (hdnIsDepartureConfirmed.Value == "True")
                {
                    oItineraryPlanDetail.IsDepartureConfirmed = true;
                    oItineraryPlanDetail.IsArrivalConfirmation = false;
                }
                else
                {
                    oItineraryPlanDetail.IsDepartureConfirmed = false;
                    oItineraryPlanDetail.IsArrivalConfirmation = true;
                }
                if (!string.IsNullOrEmpty(tbArriveLocalDate.Text))
                {
                    DateString = string.Empty; Date = string.Empty; Time = string.Empty;
                    Date = tbArriveLocalDate.Text;
                    Time = tbArriveLocalTime.Text;
                    DateString = string.Format("{0} {1}", Date, Time, CultureInfo.InvariantCulture);
                    DateAndTime = Convert.ToDateTime(FormatDate(DateString, DateFormat, true));
                    oItineraryPlanDetail.ArrivalLocal = DateAndTime;
                    //oItineraryPlanDetail.ArrivalLocal = Convert.ToDateTime(FormatDate(tbArriveLocalDate.Text, DateFormat));
                }
                if (!string.IsNullOrEmpty(tbArriveUTCDate.Text))
                {
                    DateString = string.Empty; Date = string.Empty; Time = string.Empty;
                    Date = tbArriveUTCDate.Text;
                    Time = tbArriveUTCTime.Text;
                    DateString = string.Format("{0} {1}", Date, Time, CultureInfo.InvariantCulture);
                    DateAndTime = Convert.ToDateTime(FormatDate(DateString, DateFormat, true));
                    oItineraryPlanDetail.ArrivalGMT = DateAndTime;
                    //oItineraryPlanDetail.ArrivalGMT = Convert.ToDateTime(FormatDate(tbArriveUTCDate.Text, DateFormat));
                }
                if (!string.IsNullOrEmpty(tbArriveHomeDate.Text))
                {
                    DateString = string.Empty; Date = string.Empty; Time = string.Empty;
                    Date = tbArriveHomeDate.Text;
                    Time = tbArriveHomeTime.Text;
                    DateString = string.Format("{0} {1}", Date, Time, CultureInfo.InvariantCulture);
                    DateAndTime = Convert.ToDateTime(FormatDate(DateString, DateFormat, true));
                    oItineraryPlanDetail.ArrivalHome = DateAndTime;
                    //oItineraryPlanDetail.ArrivalHome = Convert.ToDateTime(FormatDate(tbArriveHomeDate.Text, DateFormat)); ;
                }
                oItineraryPlanDetail.IsDeleted = false;
                return oItineraryPlanDetail;
            }
        }
        private void lnkInitInsert_OnClick()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgItineraryPlanningLegs.SelectedItems.Count != 0)
                {
                    int SelectedIndex = 0;
                    GridDataItem Item;
                    SelectedIndex = dgItineraryPlanningLegs.SelectedItems[0].RowIndex;
                    SelectedIndex = (SelectedIndex / 2) - 1;
                    if (SelectedIndex == 0)
                    {
                        Item = dgItineraryPlanningLegs.Items[SelectedIndex] as GridDataItem;
                        if (Item.GetDataKeyValue("DAirportIcaoID") != null)
                        {
                            tbIPLArrive.Text = Item.GetDataKeyValue("DAirportIcaoID").ToString();
                            CheckAirportICAOExist(tbIPLArrive, hdnIPLArriveID, lbAPLAirriveDesc);
                        }
                        if (Item.GetDataKeyValue("LegNUM") != null)
                        {
                            hdnInsertLegNum.Value = Item.GetDataKeyValue("LegNUM").ToString();
                        }
                        else
                        {
                            hdnInsertLegNum.Value = "0";
                        }
                        LoadDateTimeDepartureToArrival(Item);
                    }
                    else
                    {
                        Item = dgItineraryPlanningLegs.Items[SelectedIndex - 1] as GridDataItem;
                        if (Item.GetDataKeyValue("AAirportIcaoID") != null)
                        {
                            tbIPLDeparture.Text = Item.GetDataKeyValue("AAirportIcaoID").ToString();
                            CheckAirportICAOExist(tbIPLDeparture, hdnIPLDepartureID, lbAPLDepartDesc);
                        }
                        LoadDateTimeArrivalToDeparture(Item);
                        Item = dgItineraryPlanningLegs.Items[SelectedIndex] as GridDataItem;
                        if (Item.GetDataKeyValue("LegNUM") != null)
                        {
                            hdnInsertLegNum.Value = Item.GetDataKeyValue("LegNUM").ToString();
                        }
                        else
                        {
                            hdnInsertLegNum.Value = "0";
                        }
                    }
                }
                else
                {
                    string alertMsg = "radalert('You cannot insert legs. Please add before you insert legs.', 360, 50, 'Itinerary Plan legs');";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                    return;
                }
            }
        }
        private void lnkInitAdd_OnClick()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgItineraryPlanningLegs.Items.Count != 0)
                {
                    GridDataItem Item = dgItineraryPlanningLegs.Items[dgItineraryPlanningLegs.Items.Count - 1] as GridDataItem;
                    if (Item.GetDataKeyValue("AAirportIcaoID") != null)
                    {
                        tbIPLDeparture.Text = Item.GetDataKeyValue("AAirportIcaoID").ToString();
                        CheckAirportICAOExist(tbIPLDeparture, hdnIPLDepartureID, lbAPLDepartDesc);
                    }
                    LoadDateTimeArrivalToDeparture(Item);
                    if (Item.GetDataKeyValue("LegNUM") != null)
                    {
                        hdnInsertLegNum.Value = (Convert.ToInt32(Item.GetDataKeyValue("LegNUM").ToString()) + 1).ToString();
                    }
                }
            }
        }
        private void lnkInitEdit_OnClick()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgItineraryPlanningLegs.Items.Count != 0)
                {
                    GridDataItem Item = dgItineraryPlanningLegs.SelectedItems[0] as GridDataItem;
                    if (Item.GetDataKeyValue("LegNUM") != null)
                    {
                        hdnInsertLegNum.Value = Item.GetDataKeyValue("LegNUM").ToString();
                    }
                    else
                    {
                        hdnInsertLegNum.Value = "0";
                    }
                }
            }
        }
        protected void btnSaveItineraryPlanLegs_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveItineraryPlanLegs.Value == "Update")
                        {
                            (dgItineraryPlanningLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgItineraryPlanningLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlanLegs);
                }
            }
        }
        protected void btnCancelItineraryPlanLegs_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["ItineraryPlanDetailID"] != null)
                        {
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Utilities.ItineraryPlanLeg, Convert.ToInt64(Session["ItineraryPlanDetailID"].ToString().Trim()));
                            }
                        }
                        Session["ItineraryPlanDetailID"] = null;
                        DefaultSelectionItineraryPlanLegs(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlanLegs);
                }
            }
        }
        protected void tbIPLDeparture_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbAPLDepartDesc.Text = string.Empty;
                        //cvIPLDeparture.Text = string.Empty;
                        if (!string.IsNullOrEmpty(tbIPLDeparture.Text))
                        {
                            if (CheckAirportICAOExist(tbIPLDeparture, hdnIPLDepartureID, lbAPLDepartDesc) == false)
                            {
                                cvIPLDeparture.IsValid = false;
                                RadAjaxManager1.FocusControl(tbIPLDeparture.ClientID);
                                return;
                            }
                            else
                            {
                                if ((hdnSaveItineraryPlanLegs.Value == "Save") && (tbDepartLocalDate.Text.Trim() == string.Empty))
                                {
                                    LoadDefaultDate(DateTime.Now.ToString(), tbDepartLocalDate, tbDepartLocalTime);
                                }
                                CalculateAll(true, true, true, true, true, true, true, null);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlanLegs);
                }
            }
        }
        protected void tbIPLArrive_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbAPLAirriveDesc.Text = string.Empty;
                        //cvIPLArrive.Text = string.Empty;
                        if (!string.IsNullOrEmpty(tbIPLArrive.Text))
                        {
                            if (CheckAirportICAOExist(tbIPLArrive, hdnIPLArriveID, lbAPLAirriveDesc) == false)
                            {
                                cvIPLArrive.IsValid = false;
                                RadAjaxManager1.FocusControl(tbIPLArrive.ClientID);
                                return;
                            }
                            else
                            {
                                if ((hdnSaveItineraryPlanLegs.Value == "Save") && (tbDepartLocalDate.Text.Trim() == string.Empty))
                                {
                                    LoadDefaultDate(DateTime.Now.ToString(), tbDepartLocalDate, tbDepartLocalTime);
                                }
                                CalculateAll(true, true, true, true, true, true, true, null);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlanLegs);
                }
            }
        }
        private bool CheckAirportICAOExist(TextBox TxtBx, HiddenField HdnFld, Label lbAirportDesc)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TxtBx, HdnFld))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool ReturnValue = true;
                    if (!string.IsNullOrEmpty(TxtBx.Text))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objMasterService.GetAirportByAirportICaoID(TxtBx.Text.Trim().ToUpper().ToString()).EntityList;
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.GetAllAirport> AirportList = new List<FlightPakMasterService.GetAllAirport>();
                                AirportList = (List<FlightPakMasterService.GetAllAirport>)objRetVal.ToList();
                                HdnFld.Value = AirportList[0].AirportID.ToString();
                                lbAirportDesc.Text = System.Web.HttpUtility.HtmlEncode(AirportList[0].AirportName.ToString());
                                string builder = string.Empty;
                                builder = "ICAO : " + (AirportList[0].IcaoID != null ? AirportList[0].IcaoID : string.Empty)
                                    + "\n" + "City : " + (AirportList[0].CityName != null ? AirportList[0].CityName : string.Empty)
                                    + "\n" + "State/Province : " + (AirportList[0].StateName != null ? AirportList[0].StateName : string.Empty)
                                    + "\n" + "Country : " + (AirportList[0].CountryName != null ? AirportList[0].CountryName : string.Empty)
                                    + "\n" + "Airport : " + (AirportList[0].AirportName != null ? AirportList[0].AirportName : string.Empty)
                                    + "\n" + "DST Region : " + (AirportList[0].DSTRegionCD != null ? AirportList[0].DSTRegionCD : string.Empty)
                                    + "\n" + "UTC+/- : " + (AirportList[0].OffsetToGMT != null ? AirportList[0].OffsetToGMT.ToString() : string.Empty)
                                    + "\n" + "Longest Runway : " + (AirportList[0].LongestRunway != null ? AirportList[0].LongestRunway.ToString() : string.Empty)
                                    + "\n" + "IATA : " + (AirportList[0].Iata != null ? AirportList[0].Iata.ToString() : string.Empty);
                                lbAirportDesc.ToolTip = builder;

                                // Fix for UW-1165(8179)
                                string tooltipStr = string.Empty;
                                if (!string.IsNullOrEmpty(AirportList[0].Alerts))
                                {
                                    //TxtBx.ToolTip = AirportList[0].Alerts.ToString();
                                    string alertStr = "Alerts : \n";
                                    alertStr += AirportList[0].Alerts;
                                    tooltipStr = alertStr;
                                    TxtBx.ForeColor = Color.Red;
                                }
                                else
                                {
                                    TxtBx.ForeColor = Color.Black;
                                }

                                if (!string.IsNullOrEmpty(AirportList[0].GeneralNotes))
                                {
                                    string noteStr = string.Empty;
                                    if (!string.IsNullOrEmpty(tooltipStr))
                                        noteStr = "\n\nNotes : \n";
                                    else
                                        noteStr = "Notes : \n";
                                    noteStr += AirportList[0].GeneralNotes;
                                    tooltipStr += noteStr;
                                }
                                TxtBx.ToolTip = tooltipStr;
                                ReturnValue = true;
                            }
                            else
                            {
                                HdnFld.Value = string.Empty;
                                lbAirportDesc.Text = string.Empty;
                                ReturnValue = false;
                            }
                        }
                    }
                    return ReturnValue;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #region "dgItineraryPlanningLegs grid events"
        private void Bind_ItineraryPlanLegs(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                UtilitiesService.ItineraryPlanDetail oItineraryPlanDetail = new UtilitiesService.ItineraryPlanDetail();
                if (Session["ItineraryPlanID"] != null)
                {
                    oItineraryPlanDetail.ItineraryPlanID = Convert.ToInt64(Session["ItineraryPlanID"].ToString());
                }
                oItineraryPlanDetail.IsDeleted = false;
                List<UtilitiesService.GetItineraryPlanDetailByItineraryPlanID> GetItineraryPlanDetailList = new List<UtilitiesService.GetItineraryPlanDetailByItineraryPlanID>();
                using (UtilitiesService.UtilitiesServiceClient UtilitiesService = new UtilitiesService.UtilitiesServiceClient())
                {
                    var GetItineraryPlanDetailInfo = UtilitiesService.GetItineraryPlanDetailByItineraryPlanID(oItineraryPlanDetail);
                    if (GetItineraryPlanDetailInfo.ReturnFlag == true)
                    {
                        GetItineraryPlanDetailList = GetItineraryPlanDetailInfo.EntityList.OrderBy(x => x.LegNUM).ToList();
                    }
                    dgItineraryPlanningLegs.DataSource = GetItineraryPlanDetailList;
                    if (IsDataBind == true)
                    {
                        dgItineraryPlanningLegs.DataBind();
                    }
                    Session["ItineraryPlanDetailList"] = GetItineraryPlanDetailList;
                }
            }
        }
        protected void dgItineraryPlanningLegs_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Bind_ItineraryPlanLegs(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlanLegs);
                }
            }
        }
        protected void dgItineraryPlanningLegs_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (btnSaveAll.Visible == false)
                        {
                            GridDataItem item = dgItineraryPlanningLegs.SelectedItems[0] as GridDataItem;
                            Session["ItineraryPlanDetailID"] = item.GetDataKeyValue("ItineraryPlanDetailID").ToString();
                            LoadControlDataItineraryPlanLegs();
                            EnableFormItineraryPlanLegs(false);
                            GridEnableItineraryPlanLegs(true, true, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlanLegs);
                }
            }
        }
        protected void dgItineraryPlanningLegs_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                string Mode = hdnSaveItineraryPlanLegs.Value;
                                e.Canceled = true;
                                GridEnableItineraryPlanLegs(false, false, false);
                                ClearFormItineraryPlanLegs();
                                EnableFormItineraryPlanLegs(true);
                                PopulateTypeCodeData(true, true, 0);
                                if (Mode == "Insert")
                                {
                                    lnkInitInsert_OnClick();
                                }
                                else if (Mode == "Add")
                                {
                                    lnkInitAdd_OnClick();
                                }
                                DefaultInputSetting();
                                dgItineraryPlanningLegs.SelectedIndexes.Clear();
                                hdnSaveItineraryPlanLegs.Value = "Save";
                                RadAjaxManager1.FocusControl(tbIPLDeparture.ClientID);
                                break;
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    if (Session["ItineraryPlanDetailID"] != null)
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Utilities.ItineraryPlanLeg, Convert.ToInt64(Session["ItineraryPlanDetailID"].ToString()));
                                        Session["IsEditLockAirportPairLegs"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Utilities.ItineraryPlanLegs);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Utilities.ItineraryPlanLegs);
                                            return;
                                        }
                                    }
                                }
                                GridEnableItineraryPlanLegs(false, false, false);
                                EnableFormItineraryPlanLegs(true);
                                LoadControlDataItineraryPlanLegs();
                                lnkInitEdit_OnClick();
                                e.Item.Selected = true;
                                hdnSaveItineraryPlanLegs.Value = "Update";
                                RadAjaxManager1.FocusControl(tbIPLDeparture.ClientID);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    column.CurrentFilterValue = string.Empty;
                                    column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlanLegs);
                }
            }
        }
        protected void dgItineraryPlanningLegs_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidateCustom = true;
                        //IsValidateCustom = ValidateItineraryPlanLegs();
                        if (IsValidateCustom == true)
                        {
                            UtilitiesService.ItineraryPlanDetail oItineraryPlanDetail = new UtilitiesService.ItineraryPlanDetail();
                            using (UtilitiesService.UtilitiesServiceClient UtilitiesService = new UtilitiesService.UtilitiesServiceClient())
                            {
                                CalculateAll(false, true, true, true, true, true, true, null);
                                oItineraryPlanDetail = GetItemsItineraryPlanLegs(oItineraryPlanDetail);
                                var Result = UtilitiesService.AddItineraryPlanDetail(oItineraryPlanDetail);
                                if (Result.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                    IsSaveSuccessItineraryPlanLegs = true;
                                    if (Result.EntityInfo.ItineraryPlanDetailID != null)
                                    {
                                        Session["ItineraryPlanDetailID"] = Result.EntityInfo.ItineraryPlanDetailID.ToString();
                                    }
                                }
                            }
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPaxInfoPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlanLegs);
                }
                finally
                {
                }
            }
        }
        protected void dgItineraryPlanningLegs_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidateCustom = true;
                        //IsValidateCustom = ValidateItineraryPlanLegs();
                        if (IsValidateCustom == true)
                        {
                            UtilitiesService.ItineraryPlanDetail oItineraryPlanDetail = new UtilitiesService.ItineraryPlanDetail();
                            using (UtilitiesService.UtilitiesServiceClient UtilitiesService = new UtilitiesService.UtilitiesServiceClient())
                            {
                                CalculateAll(false, true, true, true, true, true, true, null);
                                oItineraryPlanDetail = GetItemsItineraryPlanLegs(oItineraryPlanDetail);
                                var Result = UtilitiesService.UpdateItineraryPlanDetail(oItineraryPlanDetail);
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    if (Session["ItineraryPlanDetailID"] != null)
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Utilities.ItineraryPlanLeg, Convert.ToInt64(Session["ItineraryPlanDetailID"].ToString()));
                                        Session["IsEditLockAirportPairLegs"] = "False";
                                    }
                                }
                                if (Result.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                    IsSaveSuccessItineraryPlanLegs = true;
                                }
                            }
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = true;
                            if (IsPopUp)
                            {
                                //Clear session & close browser
                                Session["ItineraryPlanDetailID"] = null;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPaxInfoPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlanLegs);
                }
            }
        }
        protected void dgItineraryPlanningLegs_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            e.Canceled = true;
                            UtilitiesService.ItineraryPlanDetail oItineraryPlanDetail = new UtilitiesService.ItineraryPlanDetail();
                            GridDataItem Item = dgItineraryPlanningLegs.SelectedItems[0] as GridDataItem;
                            oItineraryPlanDetail.ItineraryPlanDetailID = Convert.ToInt64(Item.GetDataKeyValue("ItineraryPlanDetailID").ToString().Trim());
                            oItineraryPlanDetail.IsDeleted = true;
                            using (UtilitiesService.UtilitiesServiceClient UtilitiesService = new UtilitiesService.UtilitiesServiceClient())
                            {
                                if (Session["ItineraryPlanDetailID"] != null)
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Utilities.ItineraryPlanLeg, Convert.ToInt64(Session["ItineraryPlanDetailID"].ToString()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelectionItineraryPlanLegs(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Utilities.ItineraryPlanLegs);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Utilities.ItineraryPlanLegs);
                                        return;
                                    }
                                }
                                UtilitiesService.DeleteItineraryPlanDetail(oItineraryPlanDetail);
                            }
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = true;
                            DefaultSelectionItineraryPlanLegs(true);
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlanLegs);
                    }
                    finally
                    {
                        if (Session["ItineraryPlanDetailID"] != null)
                        {
                            var returnValue1 = CommonService.UnLock(EntitySet.Utilities.ItineraryPlanLeg, Convert.ToInt64(Session["ItineraryPlanDetailID"].ToString()));
                        }
                    }
                }
            }
        }
        protected void dgItineraryPlanningLegs_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem dataItem = e.Item as GridDataItem;
                            //added for datetime formatting
                            if (dataItem.GetDataKeyValue("DepartureLocal") != null)
                            {
                                dataItem["DepartureLocal"].Text = Microsoft.Security.Application.Encoder.HtmlEncode(Convert12To24Hrs(dataItem.GetDataKeyValue("DepartureLocal").ToString()));
                            }
                            if (dataItem.GetDataKeyValue("ArrivalLocal") != null)
                            {
                                dataItem["ArrivalLocal"].Text = Microsoft.Security.Application.Encoder.HtmlEncode(Convert12To24Hrs(dataItem.GetDataKeyValue("ArrivalLocal").ToString()));
                            }                           
                            if (dataItem["ElapseTM"] != null && !string.IsNullOrEmpty(dataItem["ElapseTM"].Text) && dataItem["ElapseTM"].Text != "&nbsp;")
                            {
                                if (UserPrincipal.Identity._fpSettings != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                    dataItem["ElapseTM"].Text = System.Web.HttpUtility.HtmlEncode(ConvertTenthsToMins(Math.Round(Convert.ToDecimal(dataItem["ElapseTM"].Text), 3).ToString()));
                                else
                                    dataItem["ElapseTM"].Text = System.Web.HttpUtility.HtmlEncode(Math.Round(Convert.ToDecimal(dataItem["ElapseTM"].Text), 1).ToString());
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlanLegs);
                }
            }
        }
        #endregion
        protected void tbIPLMiles_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbIPLMiles.Text))
                        {
                            decimal Miles = 0;
                            hdnIPLMiles.Value = "0";
                            if (decimal.TryParse(tbIPLMiles.Text, out Miles))
                            {
                                hdnIPLMiles.Value = ConvertToMilesBasedOnCompanyProfile(Miles).ToString();
                            }
                            CalculateAll(false, true, true, true, true, true, true, null);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        protected void tbIPLPower_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbIPLPower.Text))
                        {
                            PopulateTypeCodeData(true, false, 0);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlanLegs);
                }
            }
        }
        protected void tbIPLTOBias_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbIPLTOBias.Text))
                        {
                            CalculateAll(false, true, true, true, true, true, true, null);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        protected void tbIPLLandBias_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbIPLLandBias.Text))
                        {
                            CalculateAll(false, true, true, true, true, true, true, null);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        protected void tbIPLTAS_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbIPLTAS.Text))
                        {
                            CalculateAll(false, true, true, true, true, true, true, null);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        protected void rblistIPLWindReliability_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CalculateAll(false, true, true, true, true, true, true, null);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.ItineraryPlanLegs);
                }
            }
        }
        private void CalculateDistance()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((!string.IsNullOrEmpty(tbIPLDeparture.Text)) &&
                    (!string.IsNullOrEmpty(hdnIPLDepartureID.Value)) &&
                    (!string.IsNullOrEmpty(tbIPLArrive.Text)) &&
                    (!string.IsNullOrEmpty(hdnIPLArriveID.Value)))
                {
                    using (CalculationService.CalculationServiceClient CalculationService = new CalculationService.CalculationServiceClient())
                    {
                        double Miles = 0;
                        Miles = Math.Round(CalculationService.GetDistance(Convert.ToInt64(hdnIPLDepartureID.Value), Convert.ToInt64(hdnIPLArriveID.Value)), 2);
                        tbIPLMiles.Text = CalculationService.GetDistance(Convert.ToInt64(hdnIPLDepartureID.Value), Convert.ToInt64(hdnIPLArriveID.Value)).ToString();
                        hdnIPLMiles.Value = Miles.ToString();
                    }
                }
                else
                {
                    tbIPLMiles.Text = "0";
                    hdnIPLMiles.Value = "0";
                }
            }
        }
        private void CalculateWind()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((!string.IsNullOrEmpty(tbIPLDeparture.Text)) &&
                    (!string.IsNullOrEmpty(hdnIPLDepartureID.Value)) &&
                    (!string.IsNullOrEmpty(tbIPLArrive.Text)) &&
                    (!string.IsNullOrEmpty(hdnIPLArriveID.Value)) &&
                    (!string.IsNullOrEmpty(hdnIPTypeCodeID.Value)))
                {
                    using (CalculationService.CalculationServiceClient CalculationService = new CalculationService.CalculationServiceClient())
                    {
                        tbIPLWinds.Text = Math.Round(CalculationService.GetWind(Convert.ToInt64(hdnIPLDepartureID.Value), Convert.ToInt64(hdnIPLArriveID.Value), Convert.ToInt32(rblistIPLWindReliability.SelectedValue), Convert.ToInt64(hdnIPTypeCodeID.Value), ddlIPQuarter.SelectedValue), 2).ToString();
                    }
                }
                else
                {
                    tbIPLWinds.Text = "0";
                }
            }
        }
        private void PopulateTypeCodeData(bool IsGetFromDB, bool IsForUpdate, Int64 MultiTypeCodeID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((!string.IsNullOrEmpty(tbIPTypeCode.Text)) && (!string.IsNullOrEmpty(hdnIPTypeCodeID.Value)))
                {
                    List<FlightPakMasterService.Aircraft> lstAircraft = new List<FlightPakMasterService.Aircraft>();
                    using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objAircraft = objService.GetAircraftByAircraftID(Convert.ToInt64(hdnIPTypeCodeID.Value)).EntityList;
                        if (objAircraft.Count() != 0)
                        {
                            lstAircraft = (List<FlightPakMasterService.Aircraft>)objAircraft;
                        }
                    }
                    if (lstAircraft.Count != 0)
                    {
                        if (IsGetFromDB == true)
                        {
                            if (IsForUpdate)
                            {
                                if (lstAircraft[0].PowerSetting != null)
                                {
                                    tbIPLPower.Text = lstAircraft[0].PowerSetting.ToString();
                                }
                            }
                            if (tbIPLPower.Text == "3")
                            {
                                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                                {
                                    if (lstAircraft[0].PowerSettings3TakeOffBias != null)
                                    {
                                        tbIPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", Math.Round(Convert.ToDecimal(lstAircraft[0].PowerSettings3TakeOffBias.Value.ToString()), 1)).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings3LandingBias != null)
                                    {
                                        tbIPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", Math.Round(Convert.ToDecimal(lstAircraft[0].PowerSettings3LandingBias.Value.ToString()), 1)).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings3TrueAirSpeed != null)
                                    {
                                        tbIPLTAS.Text = lstAircraft[0].PowerSettings3TrueAirSpeed.Value.ToString();
                                    }
                                }
                                else
                                {
                                    if (lstAircraft[0].PowerSettings3TakeOffBias != null)
                                    {
                                        tbIPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", ConvertTenthsToMins(lstAircraft[0].PowerSettings3TakeOffBias.Value.ToString())).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings3LandingBias != null)
                                    {
                                        tbIPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", ConvertTenthsToMins(lstAircraft[0].PowerSettings3LandingBias.Value.ToString())).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings3TrueAirSpeed != null)
                                    {
                                        tbIPLTAS.Text = lstAircraft[0].PowerSettings3TrueAirSpeed.Value.ToString();
                                    }
                                }
                            }
                            else if (tbIPLPower.Text == "2")
                            {
                                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                                {
                                    if (lstAircraft[0].PowerSettings2TakeOffBias != null)
                                    {
                                        tbIPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", Math.Round(Convert.ToDecimal(lstAircraft[0].PowerSettings2TakeOffBias.Value.ToString()), 1)).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings2LandingBias != null)
                                    {
                                        tbIPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", Math.Round(Convert.ToDecimal(lstAircraft[0].PowerSettings2LandingBias.Value.ToString()), 1)).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings2TrueAirSpeed != null)
                                    {
                                        tbIPLTAS.Text = lstAircraft[0].PowerSettings2TrueAirSpeed.Value.ToString();
                                    }
                                }
                                else
                                {
                                    if (lstAircraft[0].PowerSettings2TakeOffBias != null)
                                    {
                                        tbIPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", ConvertTenthsToMins(lstAircraft[0].PowerSettings2TakeOffBias.Value.ToString())).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings2LandingBias != null)
                                    {
                                        tbIPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", ConvertTenthsToMins(lstAircraft[0].PowerSettings2LandingBias.Value.ToString())).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings2TrueAirSpeed != null)
                                    {
                                        tbIPLTAS.Text = lstAircraft[0].PowerSettings2TrueAirSpeed.Value.ToString();
                                    }
                                }
                            }
                            else //if (lstAircraft[0].PowerSetting == "1")
                            {
                                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                                {
                                    if (lstAircraft[0].PowerSettings1TakeOffBias != null)
                                    {
                                        tbIPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", Math.Round(Convert.ToDecimal(lstAircraft[0].PowerSettings1TakeOffBias.Value.ToString()), 1)).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings1LandingBias != null)
                                    {
                                        tbIPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", Math.Round(Convert.ToDecimal(lstAircraft[0].PowerSettings1LandingBias.Value.ToString()), 1)).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings1TrueAirSpeed != null)
                                    {
                                        tbIPLTAS.Text = lstAircraft[0].PowerSettings1TrueAirSpeed.Value.ToString();
                                    }
                                }
                                else
                                {
                                    if (lstAircraft[0].PowerSettings1TakeOffBias != null)
                                    {
                                        tbIPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", ConvertTenthsToMins(lstAircraft[0].PowerSettings1TakeOffBias.Value.ToString())).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings1LandingBias != null)
                                    {
                                        tbIPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", ConvertTenthsToMins(lstAircraft[0].PowerSettings1LandingBias.Value.ToString())).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings1TrueAirSpeed != null)
                                    {
                                        tbIPLTAS.Text = lstAircraft[0].PowerSettings1TrueAirSpeed.Value.ToString();
                                    }
                                }
                            }
                        }
                        else
                        {
                            lstAircraft[0].PowerSetting = tbIPLPower.Text.Trim();
                        }
                    }
                    else
                    {
                        tbIPLPower.Text = "0";
                        tbIPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", "0.0").Trim();
                        tbIPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", "0.0").Trim();
                        tbIPLTAS.Text = "0";
                    }
                }
            }
        }
        private void CalculateETE()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((!string.IsNullOrEmpty(tbIPLWinds.Text)) &&
                    (!string.IsNullOrEmpty(tbIPLLandBias.Text)) &&
                    (!string.IsNullOrEmpty(tbIPLTAS.Text)) &&
                    (!string.IsNullOrEmpty(tbIPLTOBias.Text)) &&
                    (!string.IsNullOrEmpty(hdnIPLMiles.Value)))
                {
                    using (CalculationService.CalculationServiceClient CalculationService = new CalculationService.CalculationServiceClient())
                    {
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                        {
                            tbIPLEte.Text = Math.Round(CalculationService.GetIcaoEte(Convert.ToDouble(tbIPLWinds.Text), Convert.ToDouble(tbIPLLandBias.Text), Convert.ToDouble(tbIPLTAS.Text), Convert.ToDouble(tbIPLTOBias.Text), Convert.ToDouble(hdnIPLMiles.Value), Convert.ToDateTime(DateTime.Today)), 2).ToString();
                        }
                        else
                        {
                            string TOBias = ConvertMinToTenths(tbIPLTOBias.Text, true, string.Empty);
                            string LandBias = ConvertMinToTenths(tbIPLLandBias.Text, true, string.Empty);
                            tbIPLEte.Text = Math.Round(CalculationService.GetIcaoEte(Convert.ToDouble(tbIPLWinds.Text), Convert.ToDouble(LandBias), Convert.ToDouble(tbIPLTAS.Text), Convert.ToDouble(TOBias), Convert.ToDouble(hdnIPLMiles.Value), Convert.ToDateTime(DateTime.Today)), 2).ToString();
                            tbIPLEte.Text = ConvertTenthsToMins(tbIPLEte.Text);
                        }
                    }
                }
                else
                {
                    tbIPLEte.Text = string.Format("{0:" + TenthMinFormat + "}", "0.00");
                }
                tbIPLEte.Text = string.Format("{0:" + TenthMinFormat + "}", tbIPLEte.Text).Trim();
            }
        }
        private void CalculateCost()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(hdnIPTypeCodeID.Value))
                {
                    decimal lnChrg_Rate = 0.0M;
                    decimal lnCost = 0.0M;
                    decimal LegDistance = 0;
                    decimal LegETE = 0;
                    decimal LegFlightCost = 0;
                    string lcChrg_Unit = string.Empty;
                    Int64 AircraftID = Convert.ToInt64(hdnIPTypeCodeID.Value);
                    if (!string.IsNullOrEmpty(hdnIPLMiles.Value))
                    {
                        LegDistance = Convert.ToDecimal(hdnIPLMiles.Value);
                    }
                    if (!string.IsNullOrEmpty(tbIPLEte.Text))
                    {
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                        {
                            LegETE = Convert.ToDecimal(tbIPLEte.Text);
                        }
                        else
                        {
                            LegETE = Convert.ToDecimal(ConvertMinToTenths(tbIPLEte.Text, true, string.Empty));
                        }
                    }
                    FlightPakMasterService.Aircraft Aircraft = GetAircraft(AircraftID);
                    if (Aircraft != null)
                    {
                        lnChrg_Rate = Aircraft.ChargeRate == null ? 0.0M : (decimal)Aircraft.ChargeRate;
                        lcChrg_Unit = Aircraft.ChargeUnit;
                    }
                    switch (lcChrg_Unit)
                    {
                        case "N":
                            {
                                lnCost = (LegDistance == null ? 0 : (decimal)LegDistance) * lnChrg_Rate;
                                break;
                            }
                        case "K":
                            {
                                lnCost = (LegDistance == null ? 0 : (decimal)LegDistance) * lnChrg_Rate * 1.852M;
                                break;
                            }
                        case "S":
                            {
                                lnCost = (LegDistance == null ? 0 : (decimal)LegDistance) * lnChrg_Rate * 1.1508M;
                                break;
                            }
                        case "H":
                            {
                                lnCost = (LegETE == null ? 0 : (decimal)LegETE) * lnChrg_Rate;
                                break;
                            }
                        default: lnCost = 0.0M; break;
                    }
                    LegFlightCost = lnCost;
                    tbIPLCost.Text = Math.Round(LegFlightCost, 2).ToString();
                    tbIPLCost.Text = CurrencyType + string.Format("{0:" + CurrencyFormat + "}", tbIPLCost.Text);
                }
                else
                {
                    tbIPLCost.Text = "0";
                    tbIPLCost.Text = CurrencyType + string.Format("{0:" + CurrencyFormat + "}", "0.00");
                }
            }
        }
        private void CalculateTotalCost()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Decimal TotalFlightCost = 0;
                tbIPLTotalCost.Text = string.Empty;
                foreach (GridDataItem Item in dgItineraryPlanningLegs.Items)
                {
                    if (Item.GetDataKeyValue("Cost") != null)
                    {
                        TotalFlightCost = TotalFlightCost + Convert.ToDecimal(Item.GetDataKeyValue("Cost"));
                    }
                }
                tbIPLTotalCost.Text = CurrencyType + string.Format("{0:" + CurrencyFormat + "}", Math.Round(TotalFlightCost, 2).ToString());
            }
        }
        private FlightPak.Web.FlightPakMasterService.Aircraft GetAircraft(Int64 AircraftID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AircraftID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.Aircraft retAircraft = new FlightPakMasterService.Aircraft();
                    var objPowerSetting = objDstsvc.GetAircraftByAircraftID(AircraftID);
                    if (objPowerSetting.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = objPowerSetting.EntityList;
                        if (AircraftList != null && AircraftList.Count > 0)
                            retAircraft = AircraftList[0];
                        else
                            retAircraft = null;
                    }
                    return retAircraft;
                }
            }
        }
        private void CalculateAll(bool CalcDistance, bool CalcWind, bool CalcETE, bool CalcCost, bool CalcTotalCost, bool CalcDate, bool IsDepartConfirm, DateTime? ChgDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (CalcDistance)
                {
                    CalculateDistance();
                }

                if (CalcWind)
                {
                    CalculateWind();
                }
                if (CalcETE)
                {
                    CalculateETE();
                }
                if (CalcDate)
                {
                    CalculateDateTime(IsDepartConfirm, ChgDate);
                    if (ChgDate != null)
                    {
                        CalculateWind();
                        CalculateETE();
                        CalculateDateTime(IsDepartConfirm, ChgDate);
                    }
                }
                if (CalcCost)
                {
                    CalculateCost();
                }
                if (CalcTotalCost)
                {
                    CalculateTotalCost();
                }
            }
        }
        private string CheckAircraftCapability()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                StringBuilder ErrorString = new StringBuilder();
                string BreakLine = "<br>";
                string Error1 = CheckAircraftCapabilityLegDistance();
                string Error2 = CheckAircraftCapabilityLegTime();
                string Error3 = ValidateDefaultDateRange();
                if (!string.IsNullOrEmpty(Error1))
                {
                    ErrorString.Append(Error1 + BreakLine);
                }
                if (!string.IsNullOrEmpty(Error2))
                {
                    ErrorString.Append(Error2 + BreakLine);
                }
                if (!string.IsNullOrEmpty(Error3))
                {
                    ErrorString.Append(Error3 + BreakLine);
                }
                if (!string.IsNullOrEmpty(DateValidationError))
                {
                    ErrorString.Append(DateValidationError + BreakLine);
                }
                return ErrorString.ToString();
            }
        }
        private string CheckAircraftCapabilityLegDistance()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                string ReturnString = string.Empty;
                Int64 AircraftID;
                double LegDistance = 0;
                double aircrPS1 = 0.0, aircrPS2 = 0.0, aircrPS3 = 0.0;
                string[] TypeCodeList = new string[100];
                if (!string.IsNullOrEmpty(hdnIPLMiles.Value))
                {
                    LegDistance = Convert.ToDouble(hdnIPLMiles.Value);
                }
                using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    AircraftID = Convert.ToInt64(hdnIPTypeCodeID.Value);
                    var objRetAircraftList = objMasterService.GetAircraftByAircraftID(AircraftID).EntityList;
                    if (objRetAircraftList[0].PowerSettings1TrueAirSpeed != null && objRetAircraftList[0].PowerSettings1HourRange != null)
                        aircrPS1 = ((double)(objRetAircraftList[0].PowerSettings1TrueAirSpeed)) * ((double)(objRetAircraftList[0].PowerSettings1HourRange));
                    if (objRetAircraftList[0].PowerSettings2TrueAirSpeed != null && objRetAircraftList[0].PowerSettings2HourRange != null)
                        aircrPS2 = ((double)(objRetAircraftList[0].PowerSettings2TrueAirSpeed)) * ((double)(objRetAircraftList[0].PowerSettings2HourRange));
                    if (objRetAircraftList[0].PowerSettings3TrueAirSpeed != null && objRetAircraftList[0].PowerSettings3HourRange != null)
                        aircrPS3 = ((double)(objRetAircraftList[0].PowerSettings3TrueAirSpeed)) * ((double)(objRetAircraftList[0].PowerSettings3HourRange));
                    if (objRetAircraftList[0].PowerSetting == "1" && aircrPS1 < LegDistance)
                    {
                        ReturnValue = false;
                    }
                    else if (objRetAircraftList[0].PowerSetting == "2" && aircrPS2 < LegDistance)
                    {
                        ReturnValue = false;
                    }
                    else if (objRetAircraftList[0].PowerSetting == "3" && aircrPS3 < LegDistance)
                    {
                        ReturnValue = false;
                    }
                }
                if (ReturnValue == false)
                {
                    ReturnString = "Warning - Leg Distance Beyond Aircraft Capability.";
                }
                return ReturnString;
            }
        }
        private string CheckAircraftCapabilityLegTime()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                string ReturnString = string.Empty;
                Int64 AircraftID;
                double FlightHours = 0;
                string[] TypeCodeList = new string[100];
                if (!string.IsNullOrEmpty(tbIPLEte.Text))
                {
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                    {
                        FlightHours = Convert.ToDouble(tbIPLEte.Text);
                    }
                    else
                    {
                        FlightHours = Convert.ToDouble(ConvertMinToTenths(tbIPLEte.Text, true, string.Empty));
                    }
                }
                using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    AircraftID = Convert.ToInt64(hdnIPTypeCodeID.Value);
                    var objRetAircraftList = objMasterService.GetAircraftByAircraftID(AircraftID).EntityList;
                    if (objRetAircraftList[0].PowerSetting == "1" && (double)objRetAircraftList[0].PowerSettings1HourRange < FlightHours)
                    {
                        ReturnValue = false;
                    }
                    else if (objRetAircraftList[0].PowerSetting == "2" && (double)objRetAircraftList[0].PowerSettings1HourRange < FlightHours)
                    {
                        ReturnValue = false;
                    }
                    else if (objRetAircraftList[0].PowerSetting == "3" && (double)objRetAircraftList[0].PowerSettings1HourRange < FlightHours)
                    {
                        ReturnValue = false;
                    }
                }
                if (ReturnValue == false)
                {
                    ReturnString = "Warning - Leg Time Aloft Beyond Aircraft Capability.";
                }
                return ReturnString;
            }
        }
        private void DefaultInputSetting()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgItineraryPlanningLegs.Items.Count == 0)
                {
                    if (!string.IsNullOrEmpty(tbIPHomebase.Text.Trim()))
                    {
                        tbIPLDeparture.Text = tbIPHomebase.Text;
                        tbIPLDeparture_OnTextChanged(tbIPLDeparture, EventArgs.Empty);
                    }
                    else
                    {
                        if ((UserPrincipal.Identity != null) && (UserPrincipal.Identity._airportICAOCd != null))
                        {
                            tbIPLDeparture.Text = UserPrincipal.Identity._airportICAOCd.ToString();
                            tbIPLDeparture_OnTextChanged(tbIPLDeparture, EventArgs.Empty);
                        }
                    }
                    tbDepartHomeTime.Text = ResetTime;
                    tbDepartLocalTime.Text = ResetTime;
                    tbDepartUTCTime.Text = ResetTime;
                }
            }
        }
        #endregion
        /* need to implement yet
        Warning - Leg Distance Beyond Aircraft Capability.
        Warning - Leg Time Aloft Beyond Aircraft Capability.         
        */
        //added for datetime formatting (modified)
        private void SplitDateAndTimeOLD(string DateTimeValue, ref string Date, ref string Time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Date = DateTimeValue.Substring(0, DateTimeValue.IndexOf(' '));
                Time = DateTimeValue.Substring(DateTimeValue.IndexOf(' '), DateTimeValue.Length - DateTimeValue.IndexOf(' '));
            }
        }
        private string JoinDateAndTime(string Date, string Time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                return Date + " " + Time;
            }
        }
        protected decimal ConvertToKilomenterBasedOnCompanyProfile(decimal Miles)
        {
            if (UserPrincipal.Identity._fpSettings._IsKilometer == null ? false : (bool)UserPrincipal.Identity._fpSettings._IsKilometer)
                return Math.Round(Miles * 1.852M, 0);
            else
                return Miles;
        }
        protected decimal ConvertToMilesBasedOnCompanyProfile(decimal Kilometers)
        {
            if (UserPrincipal.Identity._fpSettings._IsKilometer == null ? false : (bool)UserPrincipal.Identity._fpSettings._IsKilometer)
                return Math.Round(Kilometers / 1.852M, 0);
            else
                return Kilometers;
        }

        protected void dgCustom_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgItineraryPlanning.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }

        protected void dgItineraryPlanningLegs_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgItineraryPlanningLegs.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }


        #region "Mintues/Tenths Conversion"
        public List<StandardFlightpakConversion> LoadStandardFPConversion()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<StandardFlightpakConversion> conversionList = new List<StandardFlightpakConversion>();
                // Set Standard Conversions
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.0M, StartMinutes = 0, EndMinutes = 5, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.1M, StartMinutes = 6, EndMinutes = 11, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.2M, StartMinutes = 12, EndMinutes = 17, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.3M, StartMinutes = 18, EndMinutes = 23, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.4M, StartMinutes = 24, EndMinutes = 29, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.5M, StartMinutes = 30, EndMinutes = 35, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.6M, StartMinutes = 36, EndMinutes = 41, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.7M, StartMinutes = 42, EndMinutes = 47, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.8M, StartMinutes = 48, EndMinutes = 53, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.9M, StartMinutes = 54, EndMinutes = 59, ConversionType = 1 });
                // Set Flightpak Conversions
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.0M, StartMinutes = 0, EndMinutes = 2, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.1M, StartMinutes = 3, EndMinutes = 8, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.2M, StartMinutes = 9, EndMinutes = 14, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.3M, StartMinutes = 15, EndMinutes = 21, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.4M, StartMinutes = 22, EndMinutes = 27, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.5M, StartMinutes = 28, EndMinutes = 32, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.6M, StartMinutes = 33, EndMinutes = 39, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.7M, StartMinutes = 40, EndMinutes = 44, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.8M, StartMinutes = 45, EndMinutes = 50, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.9M, StartMinutes = 51, EndMinutes = 57, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 1.0M, StartMinutes = 58, EndMinutes = 59, ConversionType = 2 });
                return conversionList;
            }
        }
        /// <summary>
        /// Method to Get Tenths Conversion Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <param name="isTenths">Pass Boolean Value</param>
        /// <param name="conversionType">Pass Conversion Type</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        protected string ConvertMinToTenths(String time, Boolean isTenths, String conversionType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time, isTenths, conversionType))
            {
                string result = "0.0";

                if (time.IndexOf(":") != -1)
                {
                    string[] timeArray = time.Split(':');
                    decimal hour = Convert.ToDecimal(timeArray[0]);
                    decimal minute = Convert.ToDecimal(timeArray[1]);

                    if (isTenths && (conversionType == "1" || conversionType == "2")) // Standard and Flightpak Conversion
                    {
                        if (Session["StandardFlightpakConversion"] != null)
                        {
                            List<StandardFlightpakConversion> StandardFlightpakConversionList = (List<StandardFlightpakConversion>)Session["StandardFlightpakConversion"];
                            var standardList = StandardFlightpakConversionList.ToList().Where(x => minute >= x.StartMinutes && minute <= x.EndMinutes && x.ConversionType.ToString() == conversionType).ToList();

                            if (standardList.Count > 0)
                            {
                                result = Convert.ToString(hour + standardList[0].Tenths);
                            }
                        }
                    }
                    //else if (isTenths && conversionType == "3") // Tenths - Min Conversion : Others
                    //{
                    //    List<GetPOHomeBaseSetting> homeBaseSetting = (List<GetPOHomeBaseSetting>)Session[Master.POHomeBaseKey];
                    //    List<GetPOTenthsConversion> tenthConvList = homeBaseSetting[0].TenthConversions.ToList().Where(x => minute >= x.StartMinimum && minute <= x.EndMinutes).ToList();

                    //    if (tenthConvList.Count > 0)
                    //    {
                    //        result = Convert.ToString(hour + tenthConvList[0].Tenths);
                    //    }
                    //}
                    else
                    {
                        decimal decimalOfMin = 0;
                        if (minute > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = minute / 60;

                        result = Convert.ToString(hour + decimalOfMin);
                    }
                }
                return result;
            }
        }
        /// <summary>
        /// Method to Get Minutes from decimal Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        protected string ConvertTenthsToMins(String time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time))
            {

                string result = ResetTime;
                int val;
                if (!string.IsNullOrEmpty(time))
                {
                    if (time.IndexOf(".") < 0)

                        time = time + ".0";

                    if (time.IndexOf(".") != -1)
                    {
                        string[] timeArray = time.Split('.');
                        decimal hour = Convert.ToDecimal(timeArray[0]);
                        decimal decimal_min = Convert.ToDecimal(String.Concat("0.", timeArray[1]));
                        decimal decimalOfMin = 0;
                        if (decimal_min > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = (decimal_min * 60 / 100);
                        decimal finalval = Math.Round(hour + decimalOfMin, 2);
                        result = Convert.ToString(finalval).Replace(".", ":");
                        if (result.IndexOf(":") < 0)
                            result = result + ":00";

                        timeArray = result.Split(':');
                        if (timeArray[0].Length == 1)
                        {
                            result = "0" + result;
                        }
                        if (timeArray[1].Length == 1)
                        {
                            result = result + "0";
                        }
                    }
                    else if (int.TryParse(time, out val))
                    {
                        result = time;
                    }
                }

                return result;
            }
        }
        #endregion

        #region "Date Events"
        protected void tbDepartLocalDate_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ValidateDateFormat(tbDepartLocalDate);
                        if (!string.IsNullOrEmpty(hdnIPLDepartureID.Value))
                        {
                            if (!string.IsNullOrEmpty(tbDepartLocalDate.Text))
                            {
                                hdnIsDepartureConfirmed.Value = "True";
                                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                {


                                    DateTime dt = new DateTime();
                                    DateTime ldGmtDep = new DateTime();
                                    //tbDepartLocalTime.Text = "00:00";
                                    string StartTime = tbDepartLocalTime.Text.Trim();

                                    int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                    int StartMts = Convert.ToInt16(StartTime.Substring(3, 2));
                                    bool calculate = true;
                                    if (StartHrs > 23)
                                    {
                                        RadWindowManager1.RadAlert("Hour entry must be between 0 and 23", 330, 100, "Itinerary Plan Legs", null);
                                        calculate = false;
                                        tbDepartLocalTime.Text = ResetTime;
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartLocalTime);

                                    }
                                    else if (StartMts > 59)
                                    {
                                        RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, "Itinerary Plan Legs", null);
                                        calculate = false;
                                        tbDepartLocalTime.Text = ResetTime;
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartLocalTime);
                                    }

                                    if (calculate)
                                    {
                                        dt = DateTime.ParseExact(tbDepartLocalDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);



                                        dt = dt.AddHours(StartHrs);
                                        dt = dt.AddMinutes(StartMts);
                                        //GMTDept
                                        ldGmtDep = objDstsvc.GetGMT(Convert.ToInt64(hdnIPLDepartureID.Value), dt, true, false);

                                        string startHrs = ResetTime + ldGmtDep.Hour.ToString();
                                        string startMins = ResetTime + ldGmtDep.Minute.ToString();
                                        tbDepartUTCDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", ldGmtDep);
                                        tbDepartUTCTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);


                                        CalculateAll(false, true, true, true, true, true, true, dt);
                                        if (sender is TextBox)
                                        {
                                            if (((TextBox)sender).ID.ToLower() == "tbDepartLocalDate")
                                                RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartLocalDate);

                                            if (((TextBox)sender).ID.ToLower() == "tbDepartUTCTime")
                                                RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartLocalTime);
                                        }
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightLeg);
                }
            }

        }

        protected void tbDepartUTCDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ValidateDateFormat(tbDepartUTCDate);
                        if (!string.IsNullOrEmpty(hdnIPLDepartureID.Value))
                        {
                            if (!string.IsNullOrEmpty(tbDepartUTCDate.Text))
                            {
                                hdnIsDepartureConfirmed.Value = "True";
                                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                {
                                    DateTime dt = new DateTime();
                                    DateTime ldGmtDep = new DateTime();
                                    string StartTime = tbDepartUTCTime.Text.Trim();
                                    int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                    int StartMts = Convert.ToInt16(StartTime.Substring(3, 2));
                                    bool calculate = true;
                                    if (StartHrs > 23)
                                    {
                                        RadWindowManager1.RadAlert("Hour entry must be between 0 and 23", 330, 100, "Itinerary Plan Legs", null);
                                        calculate = false;
                                        tbDepartUTCTime.Text = ResetTime;
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartUTCTime);
                                    }
                                    else if (StartMts > 59)
                                    {
                                        RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, "Itinerary Plan Legs", null);
                                        calculate = false;
                                        tbDepartUTCTime.Text = ResetTime;
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartUTCTime);
                                    }

                                    if (calculate)
                                    {
                                        dt = DateTime.ParseExact(tbDepartUTCDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);

                                        dt = dt.AddHours(StartHrs);
                                        dt = dt.AddMinutes(StartMts);
                                        //GMTDept
                                        ldGmtDep = objDstsvc.GetGMT(Convert.ToInt64(hdnIPLDepartureID.Value), dt, true, true);

                                        string startHrs = ResetTime + ldGmtDep.Hour.ToString();
                                        string startMins = ResetTime + ldGmtDep.Minute.ToString();
                                        tbDepartUTCDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", ldGmtDep);
                                        tbDepartUTCTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);


                                        CalculateAll(false, true, true, true, true, true, true, dt);

                                        if (sender is TextBox)
                                        {
                                            if (((TextBox)sender).ID.ToLower() == "tbDepartUTCDate")
                                            {
                                                RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartUTCTime);
                                            }
                                            if (((TextBox)sender).ID.ToLower() == "tbDepartUTCTime")
                                            {
                                                RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartUTCTime);
                                            }
                                        }


                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightLeg);
                }
            }

        }

        protected void tbDepartHomeDate_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ValidateDateFormat(tbDepartHomeDate);
                        if (!string.IsNullOrEmpty(hdnIPHomebaseAirportID.Value))
                        {
                            if (!string.IsNullOrEmpty(tbDepartHomeDate.Text))
                            {
                                hdnIsDepartureConfirmed.Value = "True";
                                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                {
                                    if (!string.IsNullOrEmpty(hdnIPHomebaseAirportID.Value))
                                    {
                                        DateTime dt = new DateTime();
                                        DateTime ldGmtDep = new DateTime();
                                        string StartTime = tbDepartHomeTime.Text.Trim();
                                        int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                        int StartMts = Convert.ToInt16(StartTime.Substring(3, 2));
                                        bool calculate = true;
                                        if (StartHrs > 23)
                                        {
                                            RadWindowManager1.RadAlert("Hour entry must be between 0 and 23", 330, 100, "Itinerary Plan Legs", null);
                                            calculate = false;
                                            tbDepartHomeTime.Text = ResetTime;
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartHomeTime);
                                        }
                                        else if (StartMts > 59)
                                        {
                                            RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, "Itinerary Plan Legs", null);
                                            calculate = false;
                                            tbDepartHomeTime.Text = ResetTime;
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartHomeTime);
                                        }

                                        if (calculate)
                                        {
                                            dt = DateTime.ParseExact(tbDepartHomeDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);


                                            dt = dt.AddHours(StartHrs);
                                            dt = dt.AddMinutes(StartMts);
                                            //GMTDept
                                            ldGmtDep = objDstsvc.GetGMT(Convert.ToInt64(hdnIPHomebaseAirportID.Value), dt, true, false);

                                            string startHrs = ResetTime + ldGmtDep.Hour.ToString();
                                            string startMins = ResetTime + ldGmtDep.Minute.ToString();
                                            tbDepartUTCDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", ldGmtDep);
                                            tbDepartUTCTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);


                                            CalculateAll(false, true, true, true, true, true, true, dt);

                                            if (sender is TextBox)
                                            {
                                                if (((TextBox)sender).ID.ToLower() == "tbDepartHomeDate")
                                                {
                                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartHomeTime);
                                                }
                                                if (((TextBox)sender).ID.ToLower() == "tbDepartHomeTime")
                                                {
                                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartHomeTime);
                                                }
                                            }


                                        }
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightLeg);
                }
            }

        }

        protected void tbArriveLocalDate_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ValidateDateFormat(tbArriveLocalDate);
                        if (!string.IsNullOrEmpty(hdnIPLArriveID.Value))
                        {
                            if (!string.IsNullOrEmpty(tbArriveLocalDate.Text))
                            {
                                hdnIsDepartureConfirmed.Value = "False";
                                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                {
                                    DateTime dt = new DateTime();
                                    DateTime ldGmtArr = new DateTime();
                                    string StartTime = tbArriveLocalTime.Text.Trim();
                                    int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                    int StartMts = Convert.ToInt16(StartTime.Substring(3, 2));
                                    bool calculate = true;
                                    if (StartHrs > 23)
                                    {
                                        RadWindowManager1.RadAlert("Hour entry must be between 0 and 23", 330, 100, "Itinerary Plan Legs", null);
                                        calculate = false;
                                        tbArriveLocalTime.Text = ResetTime;
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveLocalTime);
                                    }
                                    if (StartMts > 59)
                                    {
                                        RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, "Itinerary Plan Legs", null);
                                        calculate = false;
                                        tbArriveLocalTime.Text = ResetTime;
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveLocalTime);
                                    }

                                    if (calculate)
                                    {
                                        dt = DateTime.ParseExact(tbArriveLocalDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);

                                        dt = dt.AddHours(StartHrs);
                                        dt = dt.AddMinutes(StartMts);
                                        //GMTDept
                                        ldGmtArr = objDstsvc.GetGMT(Convert.ToInt64(hdnIPLArriveID.Value), dt, true, false);

                                        string startHrs = ResetTime + ldGmtArr.Hour.ToString();
                                        string startMins = ResetTime + ldGmtArr.Minute.ToString();
                                        tbArriveUTCDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", ldGmtArr);
                                        tbArriveUTCTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                        CalculateAll(false, true, true, true, true, true, false, dt);

                                        if (sender is TextBox)
                                        {
                                            if (((TextBox)sender).ID.ToLower() == "tbArriveLocalDate")
                                            {
                                                RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveLocalTime);
                                            }
                                            if (((TextBox)sender).ID.ToLower() == "tbArriveLocalTime")
                                            {
                                                RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveLocalTime);
                                            }
                                        }


                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightLeg);
                }
            }

        }

        protected void tbArriveUTCDate_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ValidateDateFormat(tbArriveUTCDate);
                        if (!string.IsNullOrEmpty(hdnIPLArriveID.Value))
                        {
                            if (!string.IsNullOrEmpty(tbArriveUTCDate.Text))
                            {
                                hdnIsDepartureConfirmed.Value = "False";
                                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                {
                                    DateTime dt = new DateTime();
                                    DateTime ldGmtArr = new DateTime();
                                    string StartTime = tbArriveUTCTime.Text.Trim();
                                    int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                    int StartMts = Convert.ToInt16(StartTime.Substring(3, 2));
                                    bool calculate = true;
                                    if (StartHrs > 23)
                                    {
                                        RadWindowManager1.RadAlert("Hour entry must be between 0 and 23", 330, 100, "Itinerary Plan Legs", null);
                                        calculate = false;
                                        tbArriveUTCTime.Text = ResetTime;
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveUTCTime);
                                    }
                                    if (StartMts > 59)
                                    {
                                        RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, "Itinerary Plan Legs", null);
                                        calculate = false;
                                        tbArriveUTCTime.Text = ResetTime;
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveUTCTime);
                                    }

                                    if (calculate)
                                    {
                                        dt = DateTime.ParseExact(tbArriveUTCDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);

                                        dt = dt.AddHours(StartHrs);
                                        dt = dt.AddMinutes(StartMts);
                                        //GMTDept
                                        ldGmtArr = objDstsvc.GetGMT(Convert.ToInt64(hdnIPLArriveID.Value), dt, true, true);

                                        string startHrs = ResetTime + ldGmtArr.Hour.ToString();
                                        string startMins = ResetTime + ldGmtArr.Minute.ToString();
                                        tbArriveUTCDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", ldGmtArr);
                                        tbArriveUTCTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                        CalculateAll(false, true, true, true, true, true, false, dt);

                                        if (sender is TextBox)
                                        {
                                            if (((TextBox)sender).ID.ToLower() == "tbArriveUTCDate")
                                            {
                                                RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveUTCTime);
                                            }
                                            if (((TextBox)sender).ID.ToLower() == "tbArriveUTCTime")
                                            {
                                                RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveUTCTime);
                                            }
                                        }


                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightLeg);
                }
            }

        }

        protected void tbArriveHomeDate_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ValidateDateFormat(tbArriveHomeDate);
                        if (!string.IsNullOrEmpty(hdnIPHomebaseAirportID.Value))
                        {
                            if (!string.IsNullOrEmpty(tbArriveHomeDate.Text))
                            {
                                hdnIsDepartureConfirmed.Value = "False";
                                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                {
                                    DateTime dt = new DateTime();
                                    DateTime ldGmtArr = new DateTime();
                                    string StartTime = tbArriveUTCTime.Text.Trim();
                                    int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                    int StartMts = Convert.ToInt16(StartTime.Substring(3, 2));
                                    bool calculate = true;
                                    if (StartHrs > 23)
                                    {
                                        RadWindowManager1.RadAlert("Hour entry must be between 0 and 23", 330, 100, "Itinerary Plan Legs", null);
                                        calculate = false;
                                        tbArriveUTCTime.Text = ResetTime;
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveUTCTime);

                                    }
                                    else if (StartMts > 59)
                                    {
                                        RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, "Itinerary Plan Legs", null);
                                        calculate = false;
                                        tbArriveUTCTime.Text = ResetTime;
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveUTCTime);
                                    }

                                    if (calculate)
                                    {
                                        dt = DateTime.ParseExact(tbArriveHomeDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);

                                        dt = dt.AddHours(StartHrs);
                                        dt = dt.AddMinutes(StartMts);
                                        //GMTDept
                                        ldGmtArr = objDstsvc.GetGMT(Convert.ToInt64(hdnIPHomebaseAirportID.Value), dt, true, false);

                                        string startHrs = ResetTime + ldGmtArr.Hour.ToString();
                                        string startMins = ResetTime + ldGmtArr.Minute.ToString();
                                        tbArriveUTCDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", ldGmtArr);
                                        tbArriveUTCTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                        CalculateAll(false, true, true, true, true, true, false, dt);

                                        if (sender is TextBox)
                                        {
                                            if (((TextBox)sender).ID.ToLower() == "tbArriveHomeDate")
                                            {
                                                RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveHomeTime);
                                            }
                                            if (((TextBox)sender).ID.ToLower() == "tbArriveHomeTime")
                                            {
                                                RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveHomeTime);
                                            }
                                        }


                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightLeg);
                }
            }

        }

        private void CalculateDateTime(bool isDepartureConfirmed, DateTime? ChangedDate)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isDepartureConfirmed))
            {
                double x10, x11;
                int DeptUTCHrst, DeptUTCMtstr, Hrst, Mtstr, ArrivalUTChrs, ArrivalUTCmts;
                DateTime Arrivaldt;
                //try
                //{

                if (ChangedDate == null)
                {
                    if (!string.IsNullOrEmpty(tbDepartLocalDate.Text))
                    {
                        using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                        {
                            DateTime dt = new DateTime();
                            //string StartTime = tbDepartLocalTime.Text.Trim();
                            string StartTime = ResetTime;
                            int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                            int StartMts = Convert.ToInt16(StartTime.Substring(3, 2));
                            if (StartHrs > 23)
                            {
                                RadWindowManager1.RadConfirm("Hour entry must be between 0 and 23", "confirmCrewRemoveCallBackFn", 330, 100, null, "Confirmation!");
                            }
                            else if (StartMts > 59)
                            {
                                RadWindowManager1.RadConfirm("Minute entry must be between 0 and 59", "confirmCrewRemoveCallBackFn", 330, 100, null, "Confirmation!");
                            }
                            dt = DateTime.ParseExact(tbDepartLocalDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                            dt = dt.AddHours(StartHrs);
                            dt = dt.AddMinutes(StartMts);
                            ChangedDate = dt;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(tbArriveLocalDate.Text))
                        {
                            using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                            {
                                DateTime dt = new DateTime();
                                string StartTime = tbArriveLocalTime.Text.Trim();
                                int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                int StartMts = Convert.ToInt16(StartTime.Substring(3, 2));

                                dt = DateTime.ParseExact(tbArriveLocalDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                dt = dt.AddHours(StartHrs);
                                dt = dt.AddMinutes(StartMts);
                                ChangedDate = dt;
                                isDepartureConfirmed = false;
                            }
                        }
                    }
                }


                if (ChangedDate != null)
                {


                    DateTime EstDepartDate = (DateTime)ChangedDate;
                    //if (Trip.EstDepartureDT != null)
                    //{
                    //    EstDepartDate = (DateTime)Trip.EstDepartureDT;
                    //}



                    try
                    {

                        //(!string.IsNullOrEmpty(hdnIPHomebaseAirportID.Value) && hdnIPHomebaseAirportID.Value != "0")&& 

                        if ((!string.IsNullOrEmpty(hdnIPLDepartureID.Value) && hdnIPLDepartureID.Value != "0")
                            && (!string.IsNullOrEmpty(hdnIPLArriveID.Value) && hdnIPLArriveID.Value != "0")
                            )
                        {

                            if (!string.IsNullOrEmpty(tbIPLEte.Text))
                            {
                                string ETEstr = "0";
                                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                {
                                    ETEstr = ConvertMinToTenths(tbIPLEte.Text, true, "1");
                                }
                                else
                                    ETEstr = tbIPLEte.Text;


                                double tripleg_elp_time = RoundElpTime(Convert.ToDouble(ETEstr));

                                x10 = (tripleg_elp_time * 60 * 60);
                                x11 = (tripleg_elp_time * 60 * 60);
                            }
                            else
                            {
                                x10 = 0;
                                x11 = 0;
                            }
                            DateTime DeptUTCdt = DateTime.MinValue;
                            DateTime ArrUTCdt = DateTime.MinValue;

                            if (isDepartureConfirmed)
                            {
                                //DeptUTCDate
                                if (!string.IsNullOrEmpty(tbDepartUTCTime.Text))
                                {
                                    string DeptUTCTimestr = tbDepartUTCTime.Text.Trim();
                                    DeptUTCHrst = Convert.ToInt16(DeptUTCTimestr.Substring(0, 2));
                                    DeptUTCMtstr = Convert.ToInt16(DeptUTCTimestr.Substring(3, 2));
                                    if (!string.IsNullOrEmpty(tbDepartUTCDate.Text))
                                    {
                                        DeptUTCdt = DateTime.ParseExact(tbDepartUTCDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                        DeptUTCdt = DeptUTCdt.AddHours(DeptUTCHrst);
                                        DeptUTCdt = DeptUTCdt.AddMinutes(DeptUTCMtstr);
                                    }
                                }
                                if (!string.IsNullOrWhiteSpace(this.tbIPLArrive.Text))
                                {
                                    string Timestr = tbDepartUTCTime.Text.Trim();
                                    Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                    Mtstr = Convert.ToInt16(Timestr.Substring(3, 2));

                                    Arrivaldt = DateTime.ParseExact(tbDepartUTCDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    Arrivaldt = Arrivaldt.AddHours(Hrst);
                                    Arrivaldt = Arrivaldt.AddMinutes(Mtstr);
                                    Arrivaldt = Arrivaldt.AddSeconds(x10);
                                    ArrUTCdt = Arrivaldt;

                                    tbArriveUTCDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", Arrivaldt);
                                    string startHrs = ResetTime + Arrivaldt.Hour.ToString();
                                    string startMins = ResetTime + Arrivaldt.Minute.ToString();
                                    tbArriveUTCTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                }
                                else
                                {
                                    tbArriveUTCDate.Text = string.Empty;
                                    tbArriveUTCTime.Text = string.Empty;
                                }
                            }

                            else
                            {
                                //Arrival utc date
                                string ArrUTCTimestr = tbArriveUTCTime.Text.Trim();
                                int ArrUTCHrst = Convert.ToInt16(ArrUTCTimestr.Substring(0, 2));
                                int ArrUTCMtstr = Convert.ToInt16(ArrUTCTimestr.Substring(3, 2));
                                ArrUTCdt = DateTime.ParseExact(tbArriveUTCDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                ArrUTCdt = ArrUTCdt.AddHours(ArrUTCHrst);
                                ArrUTCdt = ArrUTCdt.AddMinutes(ArrUTCMtstr);

                                if (!string.IsNullOrWhiteSpace(this.tbIPLDeparture.Text))
                                {
                                    // tbDepartUTCDate.Text = Convert.ToString(Convert.ToDateTime(this.tbArriveUTCDate.Text).AddSeconds(-(x11)));

                                    string Timestr = tbArriveUTCTime.Text.Trim();
                                    ArrivalUTChrs = Convert.ToInt16(Timestr.Substring(0, 2));
                                    ArrivalUTCmts = Convert.ToInt16(Timestr.Substring(3, 2));
                                    DateTime dt = DateTime.ParseExact(tbArriveUTCDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    dt = dt.AddHours(ArrivalUTChrs);
                                    dt = dt.AddMinutes(ArrivalUTCmts);
                                    dt = dt.AddSeconds(-(x11));
                                    DeptUTCdt = dt;
                                    tbDepartUTCDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", dt);
                                    string startHrs = ResetTime + dt.Hour.ToString();
                                    string startMins = ResetTime + dt.Minute.ToString();
                                    tbDepartUTCTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                }
                                else
                                {
                                    tbDepartUTCDate.Text = string.Empty; //DepartsUTC
                                    tbDepartUTCTime.Text = "";
                                }
                            }




                            using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                            {
                                DateTime ldLocDep = DateTime.MinValue;
                                DateTime ldLocArr = DateTime.MinValue;
                                DateTime ldHomDep = DateTime.MinValue;
                                DateTime ldHomArr = DateTime.MinValue;
                                if (!string.IsNullOrEmpty(hdnIPLDepartureID.Value))
                                {
                                    ldLocDep = objDstsvc.GetGMT(Convert.ToInt64(hdnIPLDepartureID.Value), DeptUTCdt, false, false);
                                }

                                if (!string.IsNullOrEmpty(hdnIPHomebaseAirportID.Value))
                                {
                                    ldHomDep = objDstsvc.GetGMT(Convert.ToInt64(hdnIPHomebaseAirportID.Value), DeptUTCdt, false, false);
                                }

                                if (!string.IsNullOrEmpty(hdnIPLArriveID.Value))
                                {
                                    ldLocArr = objDstsvc.GetGMT(Convert.ToInt64(hdnIPLArriveID.Value), ArrUTCdt, false, false);
                                }

                                if (!string.IsNullOrEmpty(hdnIPHomebaseAirportID.Value))
                                {
                                    ldHomArr = objDstsvc.GetGMT(Convert.ToInt64(hdnIPHomebaseAirportID.Value), ArrUTCdt, false, false);
                                }

                                //DateTime ltBlank = EstDepartDate;

                                if (!string.IsNullOrEmpty(this.tbIPLDeparture.Text))
                                {
                                    // DateTime Localdt = ldLocDep;
                                    string startHrs = ResetTime + ldLocDep.Hour.ToString();
                                    string startMins = ResetTime + ldLocDep.Minute.ToString();
                                    tbDepartLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", ldLocDep);
                                    tbDepartLocalTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);


                                    if (!string.IsNullOrEmpty(hdnIPHomebaseAirportID.Value))
                                    {
                                        // DateTime Homedt = ldHomDep;
                                        string HomedtstartHrs = ResetTime + ldHomDep.Hour.ToString();
                                        string HomedtstartMins = ResetTime + ldHomDep.Minute.ToString();
                                        tbDepartHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", ldHomDep);
                                        tbDepartHomeTime.Text = HomedtstartHrs.Substring(HomedtstartHrs.Length - 2, 2) + ":" + HomedtstartMins.Substring(HomedtstartMins.Length - 2, 2);
                                    }
                                }
                                else
                                {
                                    DateTime Localdt = EstDepartDate;
                                    string startHrs = ResetTime + Localdt.Hour.ToString();
                                    string startMins = ResetTime + Localdt.Minute.ToString();

                                    tbDepartLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", EstDepartDate);
                                    tbDepartLocalTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                    tbDepartUTCDate.Text = tbDepartLocalDate.Text;
                                    tbDepartUTCTime.Text = tbDepartLocalTime.Text;

                                    if (!string.IsNullOrEmpty(hdnIPHomebaseAirportID.Value))
                                    {
                                        tbDepartHomeDate.Text = tbDepartLocalDate.Text;
                                        tbDepartHomeTime.Text = tbDepartLocalTime.Text;
                                    }
                                }

                                if (!string.IsNullOrEmpty(this.tbIPLArrive.Text))
                                {
                                    // DateTime dt = ldLocArr;
                                    string startHrs = ResetTime + ldLocArr.Hour.ToString();
                                    string startMins = ResetTime + ldLocArr.Minute.ToString();
                                    tbArriveLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", ldLocArr);
                                    tbArriveLocalTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);


                                    if (!string.IsNullOrEmpty(hdnIPHomebaseAirportID.Value))
                                    {
                                        // DateTime HomeArrivaldt = ldHomArr;
                                        string HomeArrivalstartHrs = ResetTime + ldHomArr.Hour.ToString();
                                        string HomeArrivalstartMins = ResetTime + ldHomArr.Minute.ToString();
                                        tbArriveHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", ldHomArr);
                                        tbArriveHomeTime.Text = HomeArrivalstartHrs.Substring(HomeArrivalstartHrs.Length - 2, 2) + ":" + HomeArrivalstartMins.Substring(HomeArrivalstartMins.Length - 2, 2);
                                    }
                                }
                                else
                                {
                                    DateTime Localdt = EstDepartDate;
                                    string startHrs = ResetTime + Localdt.Hour.ToString();
                                    string startMins = ResetTime + Localdt.Minute.ToString();

                                    tbArriveLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", EstDepartDate);
                                    tbArriveLocalTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                    tbArriveUTCDate.Text = tbArriveLocalDate.Text;
                                    tbArriveUTCTime.Text = tbArriveLocalTime.Text;

                                    if (!string.IsNullOrEmpty(hdnIPHomebaseAirportID.Value))
                                    {
                                        tbArriveHomeDate.Text = tbArriveLocalDate.Text;
                                        tbArriveHomeTime.Text = tbDepartLocalTime.Text;
                                    }

                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        //Manually handled
                        if (!string.IsNullOrEmpty(this.tbArriveLocalDate.Text))
                        {
                            tbArriveLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", EstDepartDate);
                            tbArriveLocalTime.Text = ResetTime;
                        }
                        if (!string.IsNullOrEmpty(hdnIPHomebaseAirportID.Value))
                        {

                            if (!string.IsNullOrEmpty(tbArriveHomeDate.Text))
                            {
                                tbArriveHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", EstDepartDate);
                                tbArriveHomeTime.Text = ResetTime;
                            }
                        }

                        if (!string.IsNullOrEmpty(this.tbArriveUTCDate.Text))
                        {
                            tbArriveUTCDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", EstDepartDate);
                            tbArriveUTCTime.Text = ResetTime;
                        }

                        if (!string.IsNullOrEmpty(this.tbDepartLocalDate.Text))
                        {
                            tbDepartLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", EstDepartDate);
                            tbDepartLocalTime.Text = ResetTime;
                        }

                        if (!string.IsNullOrEmpty(this.tbDepartUTCDate.Text))
                        {
                            tbDepartUTCDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", EstDepartDate);
                            tbDepartUTCTime.Text = ResetTime;
                        }
                        if (!string.IsNullOrEmpty(hdnIPHomebaseAirportID.Value))
                        {
                            if (!string.IsNullOrEmpty(this.tbDepartHomeDate.Text))
                            {
                                tbDepartHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", EstDepartDate);
                                tbDepartHomeTime.Text = ResetTime;
                            }
                        }
                    }

                }

            }

        }

        protected double RoundElpTime(double lnElp_Time)
        {
            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
            {

                decimal ElapseTMRounding = 0;

                //lnEteRound=(double)CompanyLists[0].ElapseTMRounding~lnRound;
                double lnElp_Min = 0.0;
                double lnEteRound = 0.0;

                if (UserPrincipal.Identity._fpSettings._ElapseTMRounding != null)
                    ElapseTMRounding = (decimal)UserPrincipal.Identity._fpSettings._ElapseTMRounding;

                if (ElapseTMRounding == 1)
                    lnEteRound = 0;
                else if (ElapseTMRounding == 2)
                    lnEteRound = 5;
                else if (ElapseTMRounding == 3)
                    lnEteRound = 10;
                else if (ElapseTMRounding == 4)
                    lnEteRound = 15;
                else if (ElapseTMRounding == 5)
                    lnEteRound = 30;
                else if (ElapseTMRounding == 6)
                    lnEteRound = 60;

                if (lnEteRound > 0)
                {

                    lnElp_Min = (lnElp_Time - Math.Floor(lnElp_Time)) * 60;
                    //lnElp_Min = lnElp_Min % lnEteRound;
                    if (((lnElp_Min % lnEteRound) / lnEteRound) <= 0.5)
                        lnElp_Min = lnElp_Min - (lnElp_Min % lnEteRound);
                    else
                        lnElp_Min = lnElp_Min + (lnEteRound - (lnElp_Min % lnEteRound));


                    if (lnElp_Min > 0 && lnElp_Min < 60)
                        lnElp_Time = Math.Floor(lnElp_Time) + lnElp_Min / 60;

                    if (lnElp_Min == 0)
                        lnElp_Time = Math.Floor(lnElp_Time);


                    if (lnElp_Min == 60)
                        lnElp_Time = Math.Ceiling(lnElp_Time);

                }
            }
            return lnElp_Time;
        }

        private string ValidateDefaultDateRange()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<UtilitiesService.GetItineraryPlanDetailByItineraryPlanID> ItineraryPlanDetailList = new List<UtilitiesService.GetItineraryPlanDetailByItineraryPlanID>();
                ItineraryPlanDetailList = (List<UtilitiesService.GetItineraryPlanDetailByItineraryPlanID>)Session["ItineraryPlanDetailList"];
                StringBuilder ErrorString = new StringBuilder();
                if (ItineraryPlanDetailList.Count != 0)
                {
                    for (int i = 0; i < ItineraryPlanDetailList.Count - 1; i++)
                    {
                        if (ItineraryPlanDetailList[i].ArrivalLocal != null && ItineraryPlanDetailList[i + 1].DepartureLocal != null)
                        {
                            if (ItineraryPlanDetailList[i].ArrivalLocal < ItineraryPlanDetailList[i + 1].DepartureLocal)
                            {
                                if (UserPrincipal.Identity._fpSettings._TripsheetDTWarning != null)
                                {
                                    if ((double)UserPrincipal.Identity._fpSettings._TripsheetDTWarning > 0)
                                    {
                                        TimeSpan span = ((DateTime)ItineraryPlanDetailList[i + 1].DepartureLocal).Subtract((DateTime)ItineraryPlanDetailList[i].ArrivalLocal);
                                        if (((span.Days * 24 * 60) + (span.Hours * 60) + (span.Minutes)) > (double)UserPrincipal.Identity._fpSettings._TripsheetDTWarning * 24 * 60)
                                        {
                                            ErrorString.Append("Warning - CurrentLeg and Prev Leg Exceeds Default System Range");
                                            break;
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
                return ErrorString.ToString();
            }
        }

        //added for datetime formatting
        private string Convert12To24Hrs(string DateAndTime)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                return string.Format("{0:" + DateFormat + " HH:mm}", Convert.ToDateTime(DateAndTime));
            }
        }

        //added for datetime formatting
        private void SplitDateAndTime(string DateTimeValue, ref string Date, ref string Time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DateTimeValue = Convert12To24Hrs(DateTimeValue);
                Date = DateTimeValue.Substring(0, DateTimeValue.IndexOf(' ')).Trim();
                Time = DateTimeValue.Substring(DateTimeValue.IndexOf(' '), DateTimeValue.Length - DateTimeValue.IndexOf(' ')).Trim();
            }
        }

        public string FormatDate(string Date, string Format, bool AppendTime)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Date, Format))
            {
                string[] DateAndTime = Date.Split(' ');
                string[] SplitDate = new string[3];
                string[] SplitFormat = new string[3];
                if (Format.Contains("/"))
                {
                    SplitFormat = Format.Split('/');
                }
                else if (Format.Contains("-"))
                {
                    SplitFormat = Format.Split('-');
                }
                else if (Format.Contains("."))
                {
                    SplitFormat = Format.Split('.');
                }
                if (DateAndTime[0].Contains("/"))
                {
                    SplitDate = DateAndTime[0].Split('/');
                }
                else if (DateAndTime[0].Contains("-"))
                {
                    SplitDate = DateAndTime[0].Split('-');
                }
                else if (DateAndTime[0].Contains("."))
                {
                    SplitDate = DateAndTime[0].Split('.');
                }
                int dd = 0, mm = 0, yyyy = 0;
                for (int Index = 0; Index < SplitFormat.Count(); Index++)
                {
                    if (SplitFormat[Index].ToLower().Contains("d"))
                    {
                        dd = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("m"))
                    {
                        mm = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("y"))
                    {
                        yyyy = Convert.ToInt16(SplitDate[Index]);
                    }
                }
                //return new DateTime(yyyy, mm, dd);
                if (AppendTime == true)
                {
                    return (yyyy + "/" + mm + "/" + dd + ' ' + DateAndTime[1]);
                }
                else
                {
                    return (yyyy + "/" + mm + "/" + dd);
                }
            }
        }

        private void LoadDefaultDate(string DateValue, TextBox TxtDate, TextBox TxtTime)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string Date = string.Empty, Time = string.Empty;
                if (DateValue != null)
                {
                    Date = string.Empty;
                    Time = string.Empty;
                    SplitDateAndTime(DateValue, ref Date, ref Time);
                    if (DateFormat != null)
                    {
                        TxtDate.Text = String.Format("{0:" + DateFormat + "}", Date);
                        //FormatDate(Date, DateFormat);
                        //String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Date));
                    }
                    else
                    {
                        TxtDate.Text = Date;
                    }
                    //if (!string.IsNullOrEmpty(Time))
                    //{
                    //    TxtTime.Text = Time;
                    //}
                    //else
                    //{
                    TxtTime.Text = ResetTime;
                    //}
                }
                else
                {
                    TxtDate.Text = string.Empty;
                    TxtTime.Text = ResetTime;
                }
                tbDepartLocalDate_TextChanged(TxtDate, EventArgs.Empty);
            }
        }

        private bool ValidateDateFormat(TextBox TxtDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TxtDate))
            {
                bool ReturnValue = true;
                if (!string.IsNullOrEmpty(TxtDate.Text))
                {
                    DateTime DDate = new DateTime();
                    if (DateFormat != null)
                    {
                        DDate = Convert.ToDateTime(FormatDate(TxtDate.Text, DateFormat, false));
                    }
                    else
                    {
                        DDate = Convert.ToDateTime(TxtDate.Text);
                    }
                    if ((DDate.Year < 1900) || (DDate.Year > 2100))
                    {
                        string alertMsg = "radalert('Please enter / select Date between 01/01/1900 and 12/31/2100', 360, 50, 'Itinerary Plan legs');";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        TxtDate.Text = string.Empty;
                        TxtDate.Focus();
                        ReturnValue = false;
                    }
                }
                return ReturnValue;
            }
        }

        private void LoadDateTimeArrivalToDeparture(GridDataItem Item)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Item))
            {
                string Date = string.Empty;
                string Time = string.Empty;
                string DateAndTime = string.Empty;
                if (Item.GetDataKeyValue("ArrivalLocal") != null)
                {
                    Date = string.Empty;
                    Time = string.Empty;
                    DateAndTime = Item.GetDataKeyValue("ArrivalLocal").ToString();
                    DateAndTime = AddGroundTime(DateAndTime);
                    SplitDateAndTime(DateAndTime, ref Date, ref Time);
                    if (DateFormat != null)
                    {
                        tbDepartLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Date);
                        //FormatDate(Date, DateFormat);
                        //String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Date));
                    }
                    else
                    {
                        tbDepartLocalDate.Text = Date;
                    }
                    if (!string.IsNullOrEmpty(Time))
                    {
                        tbDepartLocalTime.Text = Time;
                    }
                    else
                    {
                        tbDepartLocalTime.Text = ResetTime;
                    }
                }
                else
                {
                    tbDepartLocalDate.Text = string.Empty;
                    tbDepartLocalTime.Text = ResetTime;
                }
                if (Item.GetDataKeyValue("ArrivalGMT") != null)
                {
                    Date = string.Empty;
                    Time = string.Empty;
                    DateAndTime = Item.GetDataKeyValue("ArrivalGMT").ToString();
                    DateAndTime = AddGroundTime(DateAndTime);
                    SplitDateAndTime(DateAndTime, ref Date, ref Time);
                    if (DateFormat != null)
                    {
                        tbDepartUTCDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Date);
                        //FormatDate(Date, DateFormat);
                        //String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Date));
                    }
                    else
                    {
                        tbDepartUTCDate.Text = Date;
                    }
                    if (!string.IsNullOrEmpty(Time))
                    {
                        tbDepartUTCTime.Text = Time;
                    }
                    else
                    {
                        tbDepartUTCTime.Text = ResetTime;
                    }
                }
                else
                {
                    tbDepartUTCDate.Text = string.Empty;
                    tbDepartUTCTime.Text = ResetTime;
                }
                if (Item.GetDataKeyValue("ArrivalHome") != null)
                {
                    Date = string.Empty;
                    Time = string.Empty;
                    DateAndTime = Item.GetDataKeyValue("ArrivalHome").ToString();
                    DateAndTime = AddGroundTime(DateAndTime);
                    SplitDateAndTime(DateAndTime, ref Date, ref Time);
                    if (DateFormat != null)
                    {
                        tbDepartHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Date);
                        //FormatDate(Date, DateFormat);
                        //String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Date));
                    }
                    else
                    {
                        tbDepartHomeDate.Text = Date;
                    }
                    if (!string.IsNullOrEmpty(Time))
                    {
                        tbDepartHomeTime.Text = Time;
                    }
                    else
                    {
                        tbDepartHomeTime.Text = ResetTime;
                    }
                }
                else
                {
                    tbDepartHomeDate.Text = string.Empty;
                    tbDepartHomeTime.Text = ResetTime;
                }
            }
        }

        private void LoadDateTimeDepartureToArrival(GridDataItem Item)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Item))
            {
                string Date = string.Empty;
                string Time = string.Empty;
                string DateAndTime = string.Empty;
                if (Item.GetDataKeyValue("DepartureLocal") != null)
                {
                    Date = string.Empty;
                    Time = string.Empty;
                    DateAndTime = Item.GetDataKeyValue("DepartureLocal").ToString();
                    DateAndTime = AddGroundTime(DateAndTime);
                    SplitDateAndTime(DateAndTime, ref Date, ref Time);
                    if (DateFormat != null)
                    {
                        tbArriveLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Date);
                        //FormatDate(Date, DateFormat);
                        //String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Date));
                    }
                    else
                    {
                        tbArriveLocalDate.Text = Date;
                    }
                    if (!string.IsNullOrEmpty(Time))
                    {
                        tbArriveLocalTime.Text = Time;
                    }
                    else
                    {
                        tbArriveLocalTime.Text = ResetTime;
                    }
                }
                else
                {
                    tbArriveLocalDate.Text = string.Empty;
                    tbArriveLocalTime.Text = ResetTime;
                }
                if (Item.GetDataKeyValue("DepartureGMT") != null)
                {
                    Date = string.Empty;
                    Time = string.Empty;
                    DateAndTime = Item.GetDataKeyValue("DepartureGMT").ToString();
                    DateAndTime = AddGroundTime(DateAndTime);
                    SplitDateAndTime(DateAndTime, ref Date, ref Time);
                    if (DateFormat != null)
                    {
                        tbArriveUTCDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Date);
                        //FormatDate(Date, DateFormat);
                        //String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Date));
                    }
                    else
                    {
                        tbArriveUTCDate.Text = Date;
                    }
                    if (!string.IsNullOrEmpty(Time))
                    {
                        tbArriveUTCTime.Text = Time;
                    }
                    else
                    {
                        tbArriveUTCTime.Text = ResetTime;
                    }
                }
                else
                {
                    tbArriveUTCDate.Text = string.Empty;
                    tbArriveUTCTime.Text = ResetTime;
                }
                if (Item.GetDataKeyValue("DepartureHome") != null)
                {
                    Date = string.Empty;
                    Time = string.Empty;
                    DateAndTime = Item.GetDataKeyValue("DepartureHome").ToString();
                    DateAndTime = AddGroundTime(DateAndTime);
                    SplitDateAndTime(DateAndTime, ref Date, ref Time);
                    if (DateFormat != null)
                    {
                        tbArriveHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Date);
                        //FormatDate(Date, DateFormat);
                        //String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Date));
                    }
                    else
                    {
                        tbArriveHomeDate.Text = Date;
                    }
                    if (!string.IsNullOrEmpty(Time))
                    {
                        tbArriveHomeTime.Text = Time;
                    }
                    else
                    {
                        tbArriveHomeTime.Text = ResetTime;
                    }
                }
                else
                {
                    tbArriveHomeDate.Text = string.Empty;
                    tbArriveHomeTime.Text = ResetTime;
                }
            }
        }

        private void LoadDateTimePreviousDepartureToCurrentDeparture(GridDataItem Item)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Item))
            {
                string Date = string.Empty;
                string Time = string.Empty;
                string DateAndTime = string.Empty;
                if (Item.GetDataKeyValue("DepartureLocal") != null)
                {
                    Date = string.Empty;
                    Time = string.Empty;
                    DateAndTime = Item.GetDataKeyValue("DepartureLocal").ToString();
                    DateAndTime = AddGroundTime(DateAndTime);
                    SplitDateAndTime(DateAndTime, ref Date, ref Time);
                    if (DateFormat != null)
                    {
                        tbDepartLocalDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Date);
                        //FormatDate(Date, DateFormat);
                        //String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Date));
                    }
                    else
                    {
                        tbDepartLocalDate.Text = Date;
                    }
                    if (!string.IsNullOrEmpty(Time))
                    {
                        tbDepartLocalTime.Text = Time;
                    }
                    else
                    {
                        tbDepartLocalTime.Text = ResetTime;
                    }
                }
                else
                {
                    tbDepartLocalDate.Text = string.Empty;
                    tbDepartLocalTime.Text = ResetTime;
                }
                if (Item.GetDataKeyValue("DepartureGMT") != null)
                {
                    Date = string.Empty;
                    Time = string.Empty;
                    DateAndTime = Item.GetDataKeyValue("DepartureGMT").ToString();
                    DateAndTime = AddGroundTime(DateAndTime);
                    SplitDateAndTime(DateAndTime, ref Date, ref Time);
                    if (DateFormat != null)
                    {
                        tbDepartUTCDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Date);
                        //FormatDate(Date, DateFormat);
                        //String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Date));
                    }
                    else
                    {
                        tbDepartUTCDate.Text = Date;
                    }
                    if (!string.IsNullOrEmpty(Time))
                    {
                        tbDepartUTCTime.Text = Time;
                    }
                    else
                    {
                        tbDepartUTCTime.Text = ResetTime;
                    }
                }
                else
                {
                    tbDepartUTCDate.Text = string.Empty;
                    tbDepartUTCTime.Text = ResetTime;
                }
                if (Item.GetDataKeyValue("DepartureHome") != null)
                {
                    Date = string.Empty;
                    Time = string.Empty;
                    DateAndTime = Item.GetDataKeyValue("DepartureHome").ToString();
                    DateAndTime = AddGroundTime(DateAndTime);
                    SplitDateAndTime(DateAndTime, ref Date, ref Time);
                    if (DateFormat != null)
                    {
                        tbDepartHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", Date);
                        //FormatDate(Date, DateFormat);
                        //String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(Date));
                    }
                    else
                    {
                        tbDepartHomeDate.Text = Date;
                    }
                    if (!string.IsNullOrEmpty(Time))
                    {
                        tbDepartHomeTime.Text = Time;
                    }
                    else
                    {
                        tbDepartHomeTime.Text = ResetTime;
                    }
                }
                else
                {
                    tbDepartHomeDate.Text = string.Empty;
                    tbDepartHomeTime.Text = ResetTime;
                }
            }
        }

        private string AddGroundTime(string DateAndTime)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DateAndTime))
            {
                string GroundHour = string.Empty;
                double dblGroundHour = 0;
                if ((UserPrincipal != null) && (UserPrincipal.Identity != null) && (UserPrincipal.Identity._fpSettings != null) && (UserPrincipal.Identity._fpSettings._GroundTM != null))
                {
                    GroundHour = UserPrincipal.Identity._fpSettings._GroundTM.Value.ToString();
                    dblGroundHour = Convert.ToDouble(GroundHour);
                }
                else
                {
                    GroundHour = "0";
                    dblGroundHour = Convert.ToDouble(GroundHour);
                }
                DateTime dt;
                try
                {
                    dt = Convert.ToDateTime(DateAndTime);
                    dt = dt.AddHours(dblGroundHour);
                    DateAndTime = dt.ToString();
                    return DateAndTime;
                }
                catch (Exception ex)
                {
                    return DateAndTime;
                }
            }
        }
        #endregion
    }
}