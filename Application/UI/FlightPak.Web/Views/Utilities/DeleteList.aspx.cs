﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Configuration;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using Newtonsoft.Json;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.Views.Utilities
{
    public partial class PostApi : System.Web.UI.Page
    {

        public readonly string strApiUrl = "";
        readonly string result;
        private ExceptionManager exManager;

        public PostApi()
        {
            String coreApiUrl = ConfigurationManager.AppSettings["CoreApiServer"];
            if (String.IsNullOrEmpty(coreApiUrl))
                coreApiUrl = "http://core-dev.universalweather.rdn/";
            
            strApiUrl = string.Format("{0}/api/v1/{1}", coreApiUrl, "{0}/{1}");
        }

        public PostApi(string dataObject)
        {
            result = dataObject;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            exManager.Process(() =>
            {
                PostApi postApiObj;
                String apiName = this.Request["apiName"];
                String apiType = this.Request["apiType"];
                String apiMethod = this.Request["method"];
                String requestData = null;
                
                if (Request.QueryString.Count == 4)
                    requestData = Request.QueryString[3];
                
                String strUrl = String.Format(strApiUrl, apiName, apiMethod);
                String accessToken = FlightPak.Web.Framework.Helpers.CoreApiManager.GetApiAccessToken();
                try
                {
                    using (var webClient = new WebClient())
                    {
                        webClient.Headers.Add("Content-Type", "application/json");
                        webClient.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + accessToken);

                        postApiObj = new PostApi(webClient.UploadString(String.Format(strApiUrl, apiName, apiMethod), apiType, requestData));
                    }
                }               
                catch (WebException ex)
                {
                    postApiObj = new PostApi(string.Format("Failed to execute api {0} for {1}", apiName, apiMethod));
                    if (((System.Net.HttpWebResponse)(ex.Response)).StatusCode == HttpStatusCode.Unauthorized)
                    {
                        Dictionary<string, object> result = new Dictionary<string, object>();
                        result.Add("StatusCode", 401);
                        postApiObj = new PostApi(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    }
                    Logger.Write(String.Format("Url:{0}-Token:{1}-Exception:{2}", Microsoft.Security.Application.Encoder.HtmlEncode(strUrl), Microsoft.Security.Application.Encoder.HtmlEncode(accessToken), Microsoft.Security.Application.Encoder.HtmlEncode(ex.ToString())), WebConstants.FlightPakException);
                }
                catch (Exception ex)
                {
                    postApiObj = new PostApi(string.Format("Failed to execute api {0} for {1}", apiName, apiMethod));
                    Logger.Write(String.Format("Url:{0}-Token:{1}-Exception:{2}", Microsoft.Security.Application.Encoder.HtmlEncode(strUrl), Microsoft.Security.Application.Encoder.HtmlEncode(accessToken), Microsoft.Security.Application.Encoder.HtmlEncode(ex.ToString())), WebConstants.FlightPakException);
                }
                Response.Write(Microsoft.Security.Application.Encoder.HtmlEncode(postApiObj.result));
                Response.End();

            }, FlightPak.Common.Constants.Policy.UILayer);
        }
    }
}