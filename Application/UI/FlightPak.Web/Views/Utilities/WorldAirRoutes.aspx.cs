﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Web.Views.Utilities
{
    public partial class WorldAirRoutes : BaseSecuredPage
    {
        private string ModuleNameConstant = ModuleNameConstants.Utilities.WorldWinds;
        private bool IsPopup = false;
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Utilities.ViewWolrdWinds);
                            BindData();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }

        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";Departure=" + tbDeparture.Text + ";Arrival=" + tbArrive.Text;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }

        private void BindData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(Request.QueryString["DepartureAirportID"]))
                {
                    IsPopup = true;
                    hdnDepartureID.Value = Request.QueryString["DepartureAirportID"].ToString();
                    GetAirportData(hdnDepartureID.Value, "D");
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ArriveAirportID"]))
                {
                    IsPopup = true;
                    hdnArriveID.Value = Request.QueryString["ArriveAirportID"].ToString();
                    GetAirportData(hdnArriveID.Value, "A");
                }
                if ((!string.IsNullOrEmpty(hdnDepartureID.Value)) && (!string.IsNullOrEmpty(hdnArriveID.Value)))
                {
                    GetWindAirRoutes(Convert.ToInt64(hdnDepartureID.Value), Convert.ToInt64(hdnArriveID.Value));
                }
                else
                {
                    GetWindAirRoutes(0, 0);
                }
            }
        }

        private void GetAirportData(string sAirportID, string IcaoType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sAirportID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = objMasterService.GetAirportByAirportID(Convert.ToInt64(sAirportID)).EntityList;
                    if (objRetVal.Count != 0)
                    {
                        if (IcaoType == "D")
                        {
                            tbDeparture.Text = objRetVal[0].IcaoID;
                            tbDepartureName.Text = objRetVal[0].CityName;
                        }
                        if (IcaoType == "A")
                        {
                            tbArrive.Text = objRetVal[0].IcaoID;
                            tbArriveName.Text = objRetVal[0].CityName;
                        }
                    }
                    else
                    {
                        ClearForm(IcaoType);
                    }
                }
            }
        }

        private void ClearForm(string IcaoType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (IcaoType == "D")
                {
                    tbDeparture.Text = string.Empty;
                    tbDepartureName.Text = string.Empty;
                    hdnDepartureID.Value = string.Empty;
                }
                if (IcaoType == "A")
                {
                    tbArrive.Text = string.Empty;
                    tbArriveName.Text = string.Empty;
                    hdnArriveID.Value = string.Empty;
                }
                tbZoneZone.Text = string.Empty;
                tbModelArrivalCity.Text = string.Empty;
                tbModelDepartureCity.Text = string.Empty;
            }
        }

        public void GetWindAirRoutes(Int64 DepartureAirportID, Int64 ArriavalAirportID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartureAirportID, ArriavalAirportID))
            {
                DataTable dtAircraft = new DataTable();
                dtAircraft.Clear();
                dtAircraft.Columns.Add("FlightLevelID");
                dtAircraft.Columns.Add("FlightLevel");
                dtAircraft.Columns.Add("DirectRouteQtr1");
                dtAircraft.Columns.Add("DirectRouteQtr2");
                dtAircraft.Columns.Add("DirectRouteQtr3");
                dtAircraft.Columns.Add("DirectRouteQtr4");
                dtAircraft.Columns.Add("ReturnRouteQtr1");
                dtAircraft.Columns.Add("ReturnRouteQtr2");
                dtAircraft.Columns.Add("ReturnRouteQtr3");
                dtAircraft.Columns.Add("ReturnRouteQtr4");
                dtAircraft.Columns.Add("StdDeviationQtr1");
                dtAircraft.Columns.Add("StdDeviationQtr2");
                dtAircraft.Columns.Add("StdDeviationQtr3");
                dtAircraft.Columns.Add("StdDeviationQtr4");

                using (UtilitiesService.UtilitiesServiceClient objService = new UtilitiesService.UtilitiesServiceClient())
                {
                    var objRetVal = objService.GetWorldWindAirRoutes(DepartureAirportID, ArriavalAirportID).EntityList;
                    if (objRetVal.Count != 0)
                    {
                        tbZoneZone.Text = string.Empty;
                        if (objRetVal[0].DepartureZoneAirport != null)
                        {
                            tbZoneZone.Text = objRetVal[0].DepartureZoneAirport.ToString();
                        }
                        if (objRetVal[0].ArrivalZoneAirport != null)
                        {
                            tbZoneZone.Text = tbZoneZone.Text + "/ " + objRetVal[0].ArrivalZoneAirport.ToString();
                        }
                        if (objRetVal[0].DepartureCity != null)
                        {
                            tbModelDepartureCity.Text = objRetVal[0].DepartureCity;
                        }
                        if (objRetVal[0].ArrivalCity != null)
                        {
                            tbModelArrivalCity.Text = objRetVal[0].ArrivalCity;
                        }                        
                        for (int rindex = 4; rindex > 1; rindex--)
                        {
                            DataRow drAircraft = dtAircraft.NewRow();
                            drAircraft["FlightLevel"] = string.Format("{0:" + "####,###" + "}", (rindex * 10000));
                            for (int cindex = 0; cindex < 12; cindex++)
                            {
                                if (objRetVal[cindex].Altitude == rindex.ToString())
                                {
                                    if (objRetVal[cindex].WorldWindQuarter == "1")
                                    {
                                        drAircraft["DirectRouteQtr1"] = objRetVal[cindex].Direction;
                                        drAircraft["ReturnRouteQtr1"] = objRetVal[cindex].ReturnDirection;
                                        drAircraft["StdDeviationQtr1"] = objRetVal[cindex].WorldWindStandard;
                                    }
                                    if (objRetVal[cindex].WorldWindQuarter == "2")
                                    {
                                        drAircraft["DirectRouteQtr2"] = objRetVal[cindex].Direction;
                                        drAircraft["ReturnRouteQtr2"] = objRetVal[cindex].ReturnDirection;
                                        drAircraft["StdDeviationQtr2"] = objRetVal[cindex].WorldWindStandard;
                                    }
                                    if (objRetVal[cindex].WorldWindQuarter == "3")
                                    {
                                        drAircraft["DirectRouteQtr3"] = objRetVal[cindex].Direction;
                                        drAircraft["ReturnRouteQtr3"] = objRetVal[cindex].ReturnDirection;
                                        drAircraft["StdDeviationQtr3"] = objRetVal[cindex].WorldWindStandard;
                                    }
                                    if (objRetVal[cindex].WorldWindQuarter == "4")
                                    {
                                        drAircraft["DirectRouteQtr4"] = objRetVal[cindex].Direction;
                                        drAircraft["ReturnRouteQtr4"] = objRetVal[cindex].ReturnDirection;
                                        drAircraft["StdDeviationQtr4"] = objRetVal[cindex].WorldWindStandard;
                                    }
                                }
                                if (objRetVal[cindex].Altitude == rindex.ToString())
                                {
                                    if (objRetVal[cindex].WorldWindQuarter == "1")
                                    {
                                        drAircraft["DirectRouteQtr1"] = objRetVal[cindex].Direction;
                                        drAircraft["ReturnRouteQtr1"] = objRetVal[cindex].ReturnDirection;
                                        drAircraft["StdDeviationQtr1"] = objRetVal[cindex].WorldWindStandard;
                                    }
                                    if (objRetVal[cindex].WorldWindQuarter == "2")
                                    {
                                        drAircraft["DirectRouteQtr2"] = objRetVal[cindex].Direction;
                                        drAircraft["ReturnRouteQtr2"] = objRetVal[cindex].ReturnDirection;
                                        drAircraft["StdDeviationQtr2"] = objRetVal[cindex].WorldWindStandard;
                                    }
                                    if (objRetVal[cindex].WorldWindQuarter == "3")
                                    {
                                        drAircraft["DirectRouteQtr3"] = objRetVal[cindex].Direction;
                                        drAircraft["ReturnRouteQtr3"] = objRetVal[cindex].ReturnDirection;
                                        drAircraft["StdDeviationQtr3"] = objRetVal[cindex].WorldWindStandard;
                                    }
                                    if (objRetVal[cindex].WorldWindQuarter == "4")
                                    {
                                        drAircraft["DirectRouteQtr4"] = objRetVal[cindex].Direction;
                                        drAircraft["ReturnRouteQtr4"] = objRetVal[cindex].ReturnDirection;
                                        drAircraft["StdDeviationQtr4"] = objRetVal[cindex].WorldWindStandard;
                                    }
                                }
                                if (objRetVal[cindex].Altitude == rindex.ToString())
                                {
                                    if (objRetVal[cindex].WorldWindQuarter == "1")
                                    {
                                        drAircraft["DirectRouteQtr1"] = objRetVal[cindex].Direction;
                                        drAircraft["ReturnRouteQtr1"] = objRetVal[cindex].ReturnDirection;
                                        drAircraft["StdDeviationQtr1"] = objRetVal[cindex].WorldWindStandard;
                                    }
                                    if (objRetVal[cindex].WorldWindQuarter == "2")
                                    {
                                        drAircraft["DirectRouteQtr2"] = objRetVal[cindex].Direction;
                                        drAircraft["ReturnRouteQtr2"] = objRetVal[cindex].ReturnDirection;
                                        drAircraft["StdDeviationQtr2"] = objRetVal[cindex].WorldWindStandard;
                                    }
                                    if (objRetVal[cindex].WorldWindQuarter == "3")
                                    {
                                        drAircraft["DirectRouteQtr3"] = objRetVal[cindex].Direction;
                                        drAircraft["ReturnRouteQtr3"] = objRetVal[cindex].ReturnDirection;
                                        drAircraft["StdDeviationQtr3"] = objRetVal[cindex].WorldWindStandard;
                                    }
                                    if (objRetVal[cindex].WorldWindQuarter == "4")
                                    {
                                        drAircraft["DirectRouteQtr4"] = objRetVal[cindex].Direction;
                                        drAircraft["ReturnRouteQtr4"] = objRetVal[cindex].ReturnDirection;
                                        drAircraft["StdDeviationQtr4"] = objRetVal[cindex].WorldWindStandard;
                                    }
                                }
                            }
                            dtAircraft.Rows.Add(drAircraft);
                        }
                    }
                    else
                    {
                        tbZoneZone.Text = string.Empty;
                        tbModelDepartureCity.Text = string.Empty;
                        tbModelArrivalCity.Text = string.Empty;
                        for (int rindex = 4; rindex > 1; rindex--)
                        {
                            string Zero = "0";
                            DataRow drAircraft = dtAircraft.NewRow();
                            drAircraft["FlightLevel"] = string.Format("{0:" + "####,###" + "}", (rindex * 10000));
                            for (int cindex = 1; cindex <= 4; cindex++)
                            {
                                if (rindex.ToString() == "4")
                                {
                                    if (cindex == 1)
                                    {
                                        drAircraft["DirectRouteQtr1"] = Zero;
                                        drAircraft["ReturnRouteQtr1"] = Zero;
                                        drAircraft["StdDeviationQtr1"] = Zero;
                                    }
                                    if (cindex == 2)
                                    {
                                        drAircraft["DirectRouteQtr2"] = Zero;
                                        drAircraft["ReturnRouteQtr2"] = Zero;
                                        drAircraft["StdDeviationQtr2"] = Zero;
                                    }
                                    if (cindex == 3)
                                    {
                                        drAircraft["DirectRouteQtr3"] = Zero;
                                        drAircraft["ReturnRouteQtr3"] = Zero;
                                        drAircraft["StdDeviationQtr3"] = Zero;
                                    }
                                    if (cindex == 4)
                                    {
                                        drAircraft["DirectRouteQtr4"] = Zero;
                                        drAircraft["ReturnRouteQtr4"] = Zero;
                                        drAircraft["StdDeviationQtr4"] = Zero;
                                    }
                                }
                                if (rindex.ToString() == "3")
                                {
                                    if (cindex == 1)
                                    {
                                        drAircraft["DirectRouteQtr1"] = Zero;
                                        drAircraft["ReturnRouteQtr1"] = Zero;
                                        drAircraft["StdDeviationQtr1"] = Zero;
                                    }
                                    if (cindex == 2)
                                    {
                                        drAircraft["DirectRouteQtr2"] = Zero;
                                        drAircraft["ReturnRouteQtr2"] = Zero;
                                        drAircraft["StdDeviationQtr2"] = Zero;
                                    }
                                    if (cindex == 3)
                                    {
                                        drAircraft["DirectRouteQtr3"] = Zero;
                                        drAircraft["ReturnRouteQtr3"] = Zero;
                                        drAircraft["StdDeviationQtr3"] = Zero;
                                    }
                                    if (cindex == 4)
                                    {
                                        drAircraft["DirectRouteQtr4"] = Zero;
                                        drAircraft["ReturnRouteQtr4"] = Zero;
                                        drAircraft["StdDeviationQtr4"] = Zero;
                                    }
                                }
                                if (rindex.ToString() == "2")
                                {
                                    if (cindex == 1)
                                    {
                                        drAircraft["DirectRouteQtr1"] = Zero;
                                        drAircraft["ReturnRouteQtr1"] = Zero;
                                        drAircraft["StdDeviationQtr1"] = Zero;
                                    }
                                    if (cindex == 2)
                                    {
                                        drAircraft["DirectRouteQtr2"] = Zero;
                                        drAircraft["ReturnRouteQtr2"] = Zero;
                                        drAircraft["StdDeviationQtr2"] = Zero;
                                    }
                                    if (cindex == 3)
                                    {
                                        drAircraft["DirectRouteQtr3"] = Zero;
                                        drAircraft["ReturnRouteQtr3"] = Zero;
                                        drAircraft["StdDeviationQtr3"] = Zero;
                                    }
                                    if (cindex == 4)
                                    {
                                        drAircraft["DirectRouteQtr4"] = Zero;
                                        drAircraft["ReturnRouteQtr4"] = Zero;
                                        drAircraft["StdDeviationQtr4"] = Zero;
                                    }
                                }
                            }
                            dtAircraft.Rows.Add(drAircraft);
                        }
                        if (IsPopup == true)
                        {
                            RadWindowManager1.RadAlert("Airport Icao Pairs Not Found In The World Air Routes Table.", 400, 50, "Winds on the World Air Routes", "");
                        }
                        else
                        {
                            if (DepartureAirportID != 0 && ArriavalAirportID != 0)
                            {
                                RadWindowManager1.RadAlert("Airport Icao Pairs Not Found In The World Air Routes Table.", 400, 50, "Winds on the World Air Routes", "");
                            }
                        }
                    }
                    dgAircraftCatalog.DataSource = dtAircraft;
                    dgAircraftCatalog.DataBind();
                }
            }
        }

        protected void btnGetWindAirRoutes_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((!string.IsNullOrEmpty(hdnDepartureID.Value)) && (!string.IsNullOrEmpty(hdnArriveID.Value)))
                        {
                            GetWindAirRoutes(Convert.ToInt64(hdnDepartureID.Value), Convert.ToInt64(hdnArriveID.Value));
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }

        protected void tbDeparture_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbDeparture.Text))
                        {
                            if (CheckAirportICAOExist(tbDeparture, hdnDepartureID) == false)
                            {
                                cvDeparture.IsValid = false;
                                RadAjaxManager1.FocusControl(tbDeparture.ClientID);
                                ClearForm("D");
                                GetWindAirRoutes(0, 0);
                                return;
                            }
                            if ((!string.IsNullOrEmpty(hdnDepartureID.Value)))
                            {
                                GetAirportData(hdnDepartureID.Value, "D");
                            }
                            if ((!string.IsNullOrEmpty(hdnArriveID.Value)))
                            {
                                GetAirportData(hdnArriveID.Value, "A");
                            }
                            if ((!string.IsNullOrEmpty(hdnDepartureID.Value)) && (!string.IsNullOrEmpty(hdnArriveID.Value)))
                            {
                                GetWindAirRoutes(Convert.ToInt64(hdnDepartureID.Value), Convert.ToInt64(hdnArriveID.Value));
                            }
                        }
                        else
                        {
                            ClearForm("D");
                            GetWindAirRoutes(0, 0);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }

        protected void tbArrive_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbArrive.Text))
                        {
                            if (CheckAirportICAOExist(tbArrive, hdnArriveID) == false)
                            {
                                cvArrive.IsValid = false;
                                RadAjaxManager1.FocusControl(tbArrive.ClientID);
                                ClearForm("A");
                                GetWindAirRoutes(0, 0);
                                return;
                            }
                            if ((!string.IsNullOrEmpty(hdnDepartureID.Value)))
                            {
                                GetAirportData(hdnDepartureID.Value, "D");
                            }
                            if ((!string.IsNullOrEmpty(hdnArriveID.Value)))
                            {
                                GetAirportData(hdnArriveID.Value, "A");
                            }
                            if ((!string.IsNullOrEmpty(hdnDepartureID.Value)) && (!string.IsNullOrEmpty(hdnArriveID.Value)))
                            {
                                GetWindAirRoutes(Convert.ToInt64(hdnDepartureID.Value), Convert.ToInt64(hdnArriveID.Value));
                            }
                        }
                        else
                        {
                            ClearForm("A");
                            GetWindAirRoutes(0, 0);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }

        private bool CheckAirportICAOExist(TextBox TxtBx, HiddenField HdnFld)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TxtBx, HdnFld))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool ReturnValue = true;
                    if (!string.IsNullOrEmpty(TxtBx.Text))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objMasterService.GetAirportByAirportICaoID(TxtBx.Text.Trim().ToUpper().ToString()).EntityList;
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.GetAllAirport> AirportList = new List<FlightPakMasterService.GetAllAirport>();
                                AirportList = (List<FlightPakMasterService.GetAllAirport>)objRetVal.ToList();
                                HdnFld.Value = AirportList[0].AirportID.ToString();
                                if (TxtBx.ID.Contains("tbDeparture"))
                                {
                                    tbDepartureName.Text = AirportList[0].CityName;
                                }
                                else if (TxtBx.ID.Contains("tbArrive"))
                                {
                                    tbArriveName.Text = AirportList[0].CityName;
                                }
                                ReturnValue = true;
                            }
                            else
                            {
                                HdnFld.Value = string.Empty;
                                ReturnValue = false;
                            }
                        }
                    }
                    return ReturnValue;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void MetroCity_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgAircraftCatalog.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = divContainer;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
    }
}
