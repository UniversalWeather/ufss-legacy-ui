﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecentActivities.aspx.cs"
    Inherits="FlightPak.Web.Views.Utilities.RecentActivities" %>

<%@ Import Namespace="System.Globalization" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Recent Activities</title>
    <base target="_self" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1WC" runat="server">
        <script type="text/javascript">
            var allDocks = [];
            function DockInit(dock, args) {
                allDocks[allDocks.length] = dock;
            }
            function DockCommand(dock, args) {
                var commandName = args.command.get_name();
                if (commandName = "ExpandCollapse") {
                    if (dock.get_collapsed() == false) {
                        for (var i = 0; i < allDocks.length; i++) {
                            if (allDocks[i] != dock) {
                                allDocks[i].set_collapsed(true);
                            }

                        }
                    }
                }
            }
            function closeandredirectPreflight(args) {
                GetRadWindow().BrowserWindow.location.href = args;
                GetRadWindow().close();       //closes the window     
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            } 
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1WC" runat="server" Skin="Sunset" />
    <telerik:RadAjaxManager ID="RadAjaxManager1WC" runat="server">
        <AjaxSettings>
            <%--<telerik:AjaxSetting AjaxControlID="dgWorldClock">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgWorldClock" LoadingPanelID="RadAjaxLoadingPanel1WC" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <%--  <telerik:RadWindowManager ID="RadWindowManager1WC" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadAirportPopupWC" runat="server" OnClientResize="GetDimensions"
                OnClientClose="OnClientCloseAirportPopup" AutoSize="false" KeepInScreenBounds="true"
                Height="425px" Width="1000px" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="AirportWorldClockPopup.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>--%>
    <div style="margin: 0 auto; width: 980px;">
        <table width="100%" cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left">
                                <div class="tab-nav-top">
                                    <span class="head-title">Recent Activities</span> <span class="tab-nav-icons"></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="tblspace_10">
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <telerik:RadPanelBar ID="pnlbarMaintainance" Width="100%" ExpandAnimation-Type="none"
                                    CollapseAnimation-Type="none" runat="server">
                                    <Items>
                                        <telerik:RadPanelItem runat="server" Expanded="false" Text="Preflight" CssClass="PanelHeaderStyle">
                                            <Items>
                                                <telerik:RadPanelItem>
                                                    <ContentTemplate>
                                                        <telerik:RadGrid ID="dgPreflight" runat="server" Visible="true" OnNeedDataSource="dgPreflight_BindData"
                                                            Width="960px" Height="300px" AutoGenerateColumns="false" AllowFilteringByColumn="false" OnPageIndexChanged="dgPreflight_PageIndexChanged"
                                                            AllowSorting="true" PagerStyle-AlwaysVisible="true" Skin="Office2010Silver" AllowCustomPaging="false">
                                                            <MasterTableView AllowPaging="false" CommandItemDisplay="None" DataKeyNames="TripID,TripNUM,EstDepartureDT,PassengerRequestorCD,TripDescription,TripStatus,FleetTailNum,LastUpdTS"
                                                                ClientDataKeyNames="TripId">
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="TripNUM" HeaderText="Trip No." AllowFiltering="false"
                                                                        UniqueName="TripNUM" HeaderStyle-Width="75px" />
                                                                    <telerik:GridBoundColumn DataField="EstDepartureDT" HeaderText="Departure Date" AllowFiltering="false"
                                                                        HeaderStyle-Width="75px" DataFormatString="{0:MM/dd/yy}" />
                                                                    <telerik:GridBoundColumn DataField="PassengerRequestorCD" HeaderText="Requestor"
                                                                        AllowFiltering="false" HeaderStyle-Width="100px" />
                                                                    <telerik:GridBoundColumn DataField="TripDescription" HeaderText="Trip Description"
                                                                        AllowFiltering="false" HeaderStyle-Width="250px">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="TripStatus" HeaderText="Status" HeaderStyle-Width="35px"
                                                                        AllowFiltering="false" />
                                                                    <telerik:GridBoundColumn DataField="FleetTailNum" HeaderText="Tail No." HeaderStyle-Width="75px"
                                                                        AllowFiltering="false" />
                                                                    <telerik:GridBoundColumn DataField="LastUpdUID" HeaderText="Last Modified User" HeaderStyle-Width="75px"
                                                                        AllowFiltering="false" />
                                                                    <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Last Modified Time"
                                                                        HeaderStyle-Width="150px">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="PreflightDate" runat="server" Text='<%#  System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm }", GetDate(Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "LastUpdTS"))))) %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn AllowFiltering="false" HeaderStyle-Width="50px">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="PreflightView" runat="server" CommandName="PreflightView" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TripId") %>'
                                                                                OnCommand="PreflightView_ItemCommand" Text="View"></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                </Columns>
                                                                <CommandItemTemplate>
                                                                </CommandItemTemplate>
                                                            </MasterTableView>
                                                            <ClientSettings EnablePostBackOnRowClick="false">
                                                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                    </ContentTemplate>
                                                </telerik:RadPanelItem>
                                            </Items>
                                        </telerik:RadPanelItem>
                                        <telerik:RadPanelItem runat="server" Expanded="false" Text="Postflight" CssClass="PanelHeaderStyle">
                                            <Items>
                                                <telerik:RadPanelItem>
                                                    <ContentTemplate>
                                                        <telerik:RadGrid ID="dgPostFlight" runat="server" Visible="true" OnNeedDataSource="dgPostFlight_BindData"
                                                            Width="960px" Height="300px" AutoGenerateColumns="false" AllowFilteringByColumn="false" OnPageIndexChanged="dgPostFlight_PageIndexChanged"
                                                            AllowSorting="true" PagerStyle-AlwaysVisible="true" Skin="Office2010Silver" AllowCustomPaging="false">
                                                            <MasterTableView AllowPaging="false" CommandItemDisplay="Bottom" DataKeyNames="POLogID,LogNum,TripNum,DispatchNum,TailNum,EstDepartureDT,Requestor,POMainDescription,LastUpdTS"
                                                                ClientDataKeyNames="POLogID">
                                                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="LogNum" HeaderText="Log No." AllowFiltering="false"
                                                                        HeaderStyle-Width="75px" />
                                                                    <telerik:GridBoundColumn DataField="TripNum" HeaderText="Trip No." AllowFiltering="false"
                                                                        HeaderStyle-Width="75px" />
                                                                    <telerik:GridBoundColumn DataField="DispatchNum" HeaderText="Dispatch No." AllowFiltering="false"
                                                                        HeaderStyle-Width="75px" />
                                                                    <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail No." AllowFiltering="false"
                                                                        HeaderStyle-Width="75px" />
                                                                    <telerik:GridBoundColumn DataField="EstDepartureDT" HeaderText="Departure Date" AllowFiltering="false"
                                                                        HeaderStyle-Width="75px" DataFormatString="{0:MM/dd/yy}" />
                                                                    <telerik:GridBoundColumn DataField="Requestor" HeaderText="Requestor" AllowFiltering="false"
                                                                        HeaderStyle-Width="100px" />
                                                                    <telerik:GridBoundColumn DataField="POMainDescription" HeaderText="Trip Description"
                                                                        AllowFiltering="false" HeaderStyle-Width="200px">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="LastUpdUID" HeaderText="Last Modified User" HeaderStyle-Width="75px"
                                                                        AllowFiltering="false" />
                                                                    <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Last Modified Time"
                                                                        HeaderStyle-Width="150px">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="PostflightDate" runat="server" Text='<%#  System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm }", GetDate(Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "LastUpdTS"))))) %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                    <telerik:GridTemplateColumn AllowFiltering="false">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="PostflightView" runat="server" CommandName="PostflightView" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "POLogID") %>'
                                                                                OnCommand="PostflightView_ItemCommand" Text="View"></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                </Columns>
                                                            </MasterTableView>
                                                            <ClientSettings EnablePostBackOnRowClick="false">
                                                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                    </ContentTemplate>
                                                </telerik:RadPanelItem>
                                            </Items>
                                        </telerik:RadPanelItem>                                      
                                        <telerik:RadPanelItem runat="server" Expanded="false" Text="Charter Quote" CssClass="PanelHeaderStyle">
                                            <Items>
                                                <telerik:RadPanelItem>
                                                    <ContentTemplate>
                                                        <telerik:RadGrid ID="dgCharterQuote" runat="server" Visible="true" OnNeedDataSource="dgCharterQuote_BindData"
                                                            Width="960px" Height="300px" AutoGenerateColumns="false" AllowFilteringByColumn="false" OnPageIndexChanged="dgCharterQuote_PageIndexChanged"
                                                            AllowSorting="true" PagerStyle-AlwaysVisible="true" Skin="Office2010Silver" AllowCustomPaging="false">
                                                            <MasterTableView AllowPaging="false" CommandItemDisplay="None" DataKeyNames="CQFileID">
                                                                <Columns>
                                                                    <%--<telerik:GridBoundColumn DataField="QuoteNum" HeaderText="Trip No." AllowFiltering="false"
                                                                        HeaderStyle-Width="100px" />
                                                                    <telerik:GridBoundColumn DataField="Requestor" HeaderText="Requestor" AllowFiltering="false"
                                                                        HeaderStyle-Width="200px" />
                                                                    <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail Num." AllowFiltering="false"
                                                                        HeaderStyle-Width="100px">
                                                                    </telerik:GridBoundColumn>--%>
                                                                    <telerik:GridBoundColumn DataField="FileNUM" HeaderText="File" HeaderStyle-Width="43px"
                                                                        AllowFiltering="false">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="EstDepartureDT" HeaderText="Departure Date" HeaderStyle-Width="100px"
                                                                        DataFormatString="{0:MM/dd/yy}" AllowFiltering="false">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="CQCustomerName" HeaderText="Customer" HeaderStyle-Width="100px"
                                                                        AllowFiltering="false">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="CQFileDescription" HeaderText="Trip Description"
                                                                        HeaderStyle-Width="100px" AllowFiltering="false">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="Alert" HeaderText="Alert" HeaderStyle-Width="40px"
                                                                        AllowFiltering="false">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="IsFinancialWarning" HeaderText="Financial Warning"
                                                                        HeaderStyle-Width="120px" AllowFiltering="false">
                                                                    </telerik:GridBoundColumn>
                                                                    <%--telerik:GridBoundColumn DataField="FileExcep" HeaderText="Exceptions" HeaderStyle-Width="80px"
                                                                        ItemStyle-HorizontalAlign="Center" ItemStyle-ForeColor="Red" AllowFiltering="false">
                                                                    </telerik:GridBoundColumn>--%>
                                                                    <telerik:GridBoundColumn DataField="HomebaseCD" HeaderText="Home" HeaderStyle-Width="50px"
                                                                        AllowFiltering="false">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="CQFileID" HeaderText="CQFileID" Display="false"
                                                                        AllowFiltering="false">
                                                                    </telerik:GridBoundColumn>
                                                                    <%--<telerik:GridBoundColumn DataField="LeadSourceCD" HeaderText="Lead Source" CurrentFilterFunction="Contains"
                                                                        HeaderStyle-Width="90px" ShowFilterIcon="false">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="SalesPersonCD" HeaderText="Sales Person" CurrentFilterFunction="Contains"
                                                                        HeaderStyle-Width="90px" ShowFilterIcon="false">
                                                                    </telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="CustomerType" HeaderText="Customer Type" CurrentFilterFunction="Contains"
                                                                        HeaderStyle-Width="110px" ShowFilterIcon="false">
                                                                    </telerik:GridBoundColumn>--%>
                                                                    <telerik:GridBoundColumn DataField="LastUpdUID" HeaderText="Last Modified User" HeaderStyle-Width="150px"
                                                                        AllowFiltering="false" />
                                                                    <telerik:GridBoundColumn DataField="LastUpdTS" HeaderText="Last Modified" HeaderStyle-Width="150px"
                                                                        AllowFiltering="false" DataFormatString="{0:MM/dd/yy}" />
                                                                    <telerik:GridTemplateColumn AllowFiltering="false">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="CORView" runat="server" CommandName="CORView" Text="View" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CQFileID") %>'
                                                                                OnCommand="CORView_ItemCommand"></asp:LinkButton><%--OnCommand="PostflightView_ItemCommand"--%>
                                                                        </ItemTemplate>
                                                                    </telerik:GridTemplateColumn>
                                                                </Columns>
                                                                <CommandItemTemplate>
                                                                </CommandItemTemplate>
                                                            </MasterTableView>
                                                            <ClientSettings EnablePostBackOnRowClick="false">
                                                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                    </ContentTemplate>
                                                </telerik:RadPanelItem>
                                            </Items>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelBar>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
