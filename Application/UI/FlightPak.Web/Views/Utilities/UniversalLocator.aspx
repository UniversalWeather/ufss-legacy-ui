﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UniversalLocator.aspx.cs"
    Inherits="FlightPak.Web.Views.Utilities.UniversalLocator" %>
<%@ Import Namespace="System.Web.Optimization" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Locator</title>
    <link type="text/css" rel="stylesheet" href="../../Scripts/jquery.alerts.css" />
    <script type="text/javascript" src="../../Scripts/Common.js"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="../../Scripts/jquery.alerts.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <%=Styles.Render("~/bundles/StyleFleetProfileCatalogSelect2") %>
        <%=Scripts.Render("~/bundles/scriptFleetProfileCatalogSelect2") %>   
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function pageLoad() {
                $(document).ready(function () {
                    var postData = new Object();
                    postData["apiType"] = 'fss';
                    postData["method"] = 'countries';
                    postData["countrycd"] = '';
                    postData["markSelectedRecord"] = false;

                    //Country Selectors for all four tabs
                    applyCustomSelect('.tbCountryAutoComplete', '#tbCountryName', '#hdnCountry', formatSelectionAirport);
                    applyCustomSelect('.tbHotelCountryAutoComplete', '#tbHotelCountryName', '#hdnHotelCountry', formateHotelCountrySelection);
                    applyCustomSelect('.tbVendorCountryAutoComplete', '#tbVendorCountryName', '#hdnVendorCountry', formateVendorCountrySelection);
                    applyCustomSelect('.tbCustomCountryAutoComplete', '#tbCustomCountryName', '#hdnCustomCountry', formateCustomCountrySelection);

                    function applyCustomSelect(textFieldClassName, textHiddenID, valueHiddenID, formatSelection) {
                        $(textFieldClassName).createSelect2AutoComplete({
                            idField: "CountryID",
                            textField: "CountryName",
                            textHiddenId: $(textHiddenID),
                            valueHiddenId: $(valueHiddenID),
                            postData: postData,
                            searchFieldName: "CountryName",
                            formatFunction: formateCountry,
                            extraParaNames: "CountryCD",
                            formatSelection: formatSelection
                        });
                    }
                });
                function formateCountry(state) {
                    if (!state.id) { return state.text; }
                    var $state = $('<div class="img-thumbnail flag flag-icon-background flag-icon-' + state.CountryCD.toLowerCase() + '" title="' + state.CountryCD.toLowerCase() + '" id="' + state.CountryCD.toLowerCase() + '"><span style="margin-left: 25px;display: inline-block;height: 14px;overflow: hidden;width:110px">' + state.text + '</span></div>');
                    return $state;
                }
                //For Airports Country
                function formatSelectionAirport(state) {
                    if (!state.id) { return state.text; }
                    if (state.CountryCD != null)
                        $("#hdnCountryCode").val(state.CountryCD.toLowerCase());
                    if ($("#hdnCountryCode").val() != "") {
                        var $state = $('<div class="img-thumbnail flag flag-icon-background flag-icon-' + $("#hdnCountryCode").val().toLowerCase() + '" title="' + $("#hdnCountryCode").val() + '" id="' + $("#hdnCountryCode").val() + '"><span style="margin-left: 25px;display: inline-block;height: 25px;overflow: hidden;width:74px;">' + state.text + '</span></div>');
                        return $state;
                    }
                    else
                        return state.text;
                }
                //For Hotel Country
                function formateHotelCountrySelection(state) {
                    if (!state.id) { return state.text; }
                    if (state.CountryCD != null)
                        $("#hdnHotelCountryCode").val(state.CountryCD.toLowerCase());
                    if ($("#hdnHotelCountryCode").val() != "") {
                        var $state = $('<div class="img-thumbnail flag flag-icon-background flag-icon-' + $("#hdnHotelCountryCode").val().toLowerCase() + '" title="' + $("#hdnHotelCountryCode").val() + '" id="' + $("#hdnHotelCountryCode").val() + '"><span style="margin-left: 25px;display: inline-block;height: 25px;overflow: hidden;width:74px;">' + state.text + '</span></div>');
                        return $state;
                    }
                    else
                        return state.text;
                }
                //For Vendor Country
                function formateVendorCountrySelection(state) {
                    if (!state.id) { return state.text; }
                    if (state.CountryCD != null)
                        $("#hdnVendorCountryCode").val(state.CountryCD.toLowerCase());
                    if ($("#hdnVendorCountryCode").val() != "") {
                        var $state = $('<div class="img-thumbnail flag flag-icon-background flag-icon-' + $("#hdnVendorCountryCode").val().toLowerCase() + '" title="' + $("#hdnVendorCountryCode").val() + '" id="' + $("#hdnVendorCountryCode").val() + '"><span style="margin-left: 25px;display: inline-block;height: 25px;overflow: hidden;width:74px;">' + state.text + '</span></div>');
                        return $state;
                    }
                    else
                        return state.text;
                }
                //For Custom Country
                function formateCustomCountrySelection(state) {
                    if (!state.id) { return state.text; }
                    if (state.CountryCD != null)
                        $("#hdnCustomCountryCode").val(state.CountryCD.toLowerCase());
                    if ($("#hdnCustomCountryCode").val() != "") {
                        var $state = $('<div class="img-thumbnail flag flag-icon-background flag-icon-' + $("#hdnCustomCountryCode").val().toLowerCase() + '" title="' + $("#hdnCustomCountryCode").val() + '" id="' + $("#hdnCustomCountryCode").val() + '"><span style="margin-left: 25px;display: inline-block;height: 25px;overflow: hidden;width:74px;">' + state.text + '</span></div>');
                        return $state;
                    }
                    else
                        return state.text;
                }
            }
</script>
        <script type="text/javascript">
            function openWin(radWin) {
                var url = '';
                if (radWin == "RadMetroCityPopup") {
                    url = '../Settings/Airports/MetroCityPopup.aspx?MetroCD=' + document.getElementById('<%=tbMetroCD.ClientID%>').value;
                }
                else if (radWin == "radOldICAOPopup") {
                    url = '../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbAirportICAO.ClientID%>').value;
                }
                if (radWin == "radHotelOldICAOPopup") {
                    url = '../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbHotelAirportIcao.ClientID%>').value;
                }
                else if (radWin == "RadHotelMetroCityPopup") {
                    url = '../Settings/Airports/MetroCityPopup.aspx?MetroCD=' + document.getElementById('<%=tbHotelMetro.ClientID%>').value;
                }               
                else if (radWin == "RadAicraftTypePopup") {
                    url = "../Settings/Fleet/AircraftPopup.aspx?AircraftCD=" + document.getElementById('<%=tbAircraftType.ClientID%>').value + "&tailNoAirCraftID=" + document.getElementById("<%=hdnAircraftType.ClientID%>").value;

                }
                else if (radWin == "RadVendorMetroCityPopup") {
                    url = '../Settings/Airports/MetroCityPopup.aspx?MetroCD=' + document.getElementById('<%=tbVendorMetro.ClientID%>').value;

                }               
                else if (radWin == "radVendorICAOPopup") {
                    url = '../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbVendorAirportIcao.ClientID%>').value;

                }
                else if (radWin == "RadVendorMetroCityPopup") {
                    url = '../Settings/Airports/MetroCityPopup.aspx?MetroCD=' + document.getElementById('<%=tbVendorMetro.ClientID%>').value;

                }                
                else if (radWin == "radCustomICAOPopup") {
                    url = '../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbCustomIcao.ClientID%>').value;
                }                
                else if (radWin == "radCustomMetroPopup") {
                    url = '../Settings/Airports/MetroCityPopup.aspx?MetroCD=' + document.getElementById('<%=tbCustomMetro.ClientID%>').value;

                }
                else if (radWin == "radOldIATAPopup") {
                    url = '../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbCustomIcao.ClientID%>').value;
                }
                else if (radWin == "radHotelIATAPopup") {
                    url = '../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbCustomIcao.ClientID%>').value;
                }
                else if (radWin == "radVendorIATAPopup") {
                    url = '../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbCustomIcao.ClientID%>').value;
                }
                else if (radWin == "radCustomIATAPopup") {
                    url = '../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbCustomIcao.ClientID%>').value;
                }
                var oWnd = radopen(url, radWin);
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            // this function is used to display the value of selected aircrafttype code from popup
            function onClientCloseAicraftTypePopup(oWnd, args) {
                //get the transferred arguments
                var combo = $find("<%= tbAircraftType.ClientID %>");
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {

                        document.getElementById("<%= tbAircraftType.ClientID%>").value = arg.AircraftCD;
                        document.getElementById("<%= hdnAircraftType.ClientID%>").value = arg.AircraftID;
                        document.getElementById("<%=cvAircraftType.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbAircraftType.ClientID%>").value = "";
                        document.getElementById("<%=cvAircraftType.ClientID%>").innerHTML = "";
                    }


                }

            }

            function OnClientOldICAOClose(oWnd, args) {
                var combo = $find("<%= tbAirportICAO.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {

                        document.getElementById("<%= tbAirportICAO.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%= hdnAirportICAOID.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%= cvAirportICAO.ClientID%>").innerHTML = "";

                    }

                    else {
                        document.getElementById("<%= tbAirportICAO.ClientID%>").value = "";
                        document.getElementById("<%= hdnAirportICAOID.ClientID%>").value = "";
                        document.getElementById("<%= cvAirportICAO.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }

            }

            //this function is used to display the value of selected metro code from popup
            function onClientCloseMetroCityPopup(oWnd, args) {
                var combo = $find("<%= tbMetroCD.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbMetroCD.ClientID%>").value = arg.MetroCD;
                        document.getElementById("<%=hdnMetroId.ClientID%>").value = arg.MetroID;
                        document.getElementById("<%=cvMetro.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbMetroCD.ClientID%>").value = "";
                        document.getElementById("<%=hdnMetroId.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientHotelOldICAOClose(oWnd, args) {
                var combo = $find("<%= tbHotelAirportIcao.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {

                        document.getElementById("<%= tbHotelAirportIcao.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%= hdnHotelAirportIcao.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%= cvHotelAirportIcao.ClientID%>").innerHTML = "";

                    }

                    else {
                        document.getElementById("<%= tbHotelAirportIcao.ClientID%>").value = "";
                        document.getElementById("<%= hdnHotelAirportIcao.ClientID%>").value = "";
                        document.getElementById("<%= cvHotelAirportIcao.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }

            }

            //this function is used to display the value of selected metro code from popup
            function onClientCloseHotelMetroCityPopup(oWnd, args) {
                var combo = $find("<%= tbHotelMetro.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbHotelMetro.ClientID%>").value = arg.MetroCD;
                        document.getElementById("<%=hdnHotelMetro.ClientID%>").value = arg.MetroID;
                        document.getElementById("<%=cvHotelMetro.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbHotelMetro.ClientID%>").value = "";
                        document.getElementById("<%=hdnHotelMetro.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientVendorOldICAOClose(oWnd, args) {
                var combo = $find("<%= tbVendorAirportIcao.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {

                        document.getElementById("<%= tbVendorAirportIcao.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%= hdnVendorIcao.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%= cvVendorIcao.ClientID%>").innerHTML = "";

                    }

                    else {
                        document.getElementById("<%= tbVendorAirportIcao.ClientID%>").value = "";
                        document.getElementById("<%= hdnVendorIcao.ClientID%>").value = "";
                        document.getElementById("<%= cvVendorIcao.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }

            }

            //this function is used to display the value of selected metro code from popup
            function onClientCloseVendorMetroCityPopup(oWnd, args) {
                var combo = $find("<%= tbVendorMetro.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbVendorMetro.ClientID%>").value = arg.MetroCD;
                        document.getElementById("<%=hdnVendorMetro.ClientID%>").value = arg.MetroID;
                        document.getElementById("<%=cvVendorMetro.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbVendorMetro.ClientID%>").value = "";
                        document.getElementById("<%=hdnVendorMetro.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCustomOldICAOClose(oWnd, args) {
                var combo = $find("<%= tbCustomIcao.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {

                        document.getElementById("<%= tbCustomIcao.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%= hdnCustomIcao.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%= cvCustomIcao.ClientID%>").innerHTML = "";

                    }

                    else {
                        document.getElementById("<%= tbCustomIcao.ClientID%>").value = "";
                        document.getElementById("<%= hdnCustomIcao.ClientID%>").value = "";
                        document.getElementById("<%= cvCustomIcao.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }

            }

            //this function is used to display the value of selected metro code from popup
            function onClientCloseCustomMetroCityPopup(oWnd, args) {
                var combo = $find("<%= tbCustomMetro.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCustomMetro.ClientID%>").value = arg.MetroCD;
                        document.getElementById("<%=hdnCustomMetro.ClientID%>").value = arg.MetroID;
                        document.getElementById("<%=cvCustomMetro.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbCustomMetro.ClientID%>").value = "";
                        document.getElementById("<%=hdnCustomMetro.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientAirportIataClose(oWnd, args) {
                var combo = $find("<%= tbIata.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {

                        document.getElementById("<%= tbIata.ClientID%>").value = arg.Iata;
                        document.getElementById("<%= hdnIata.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%= cvIata.ClientID%>").innerHTML = "";

                    }

                    else {
                        document.getElementById("<%= tbIata.ClientID%>").value = "";
                        document.getElementById("<%= hdnIata.ClientID%>").value = "";
                        document.getElementById("<%= cvIata.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }

            }
            function OnClientHotelIataClose(oWnd, args) {
                var combo = $find("<%= tbHotelIata.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {

                        document.getElementById("<%= tbHotelIata.ClientID%>").value = arg.Iata;
                        document.getElementById("<%= hdnHotelIata.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%= cvHotelIata.ClientID%>").innerHTML = "";

                    }

                    else {
                        document.getElementById("<%= tbHotelIata.ClientID%>").value = "";
                        document.getElementById("<%= hdnHotelIata.ClientID%>").value = "";
                        document.getElementById("<%= cvHotelIata.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }

            }
            function OnClientVendorIataClose(oWnd, args) {
                var combo = $find("<%= tbVendorIata.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {

                        document.getElementById("<%= tbVendorIata.ClientID%>").value = arg.Iata;
                        document.getElementById("<%= hdnVendorIata.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%= cvVendorIata.ClientID%>").innerHTML = "";

                    }

                    else {
                        document.getElementById("<%= tbVendorIata.ClientID%>").value = "";
                        document.getElementById("<%= hdnVendorIata.ClientID%>").value = "";
                        document.getElementById("<%= cvVendorIata.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }

            }
            function OnClientCustomIataClose(oWnd, args) {
                var combo = $find("<%= tbCustomIata.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {

                        document.getElementById("<%= tbCustomIata.ClientID%>").value = arg.Iata;
                        document.getElementById("<%= hdnCustomIata.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%= cvCustomIata.ClientID%>").innerHTML = "";

                    }

                    else {
                        document.getElementById("<%= tbCustomIata.ClientID%>").value = "";
                        document.getElementById("<%= hdnCustomIcao.ClientID%>").value = "";
                        document.getElementById("<%= cvCustomIata.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }

            }
            function alertCallBackAirportIcaoFn(arg) {
                document.getElementById("<%=tbAirportICAO.ClientID%>").focus();
            }
            function alertCallBackAirportCountryFn(arg) {
                document.getElementById("<%=tbCountry.ClientID%>").focus();
            }
            function alertCallBackAirportStateFn(arg) {
                document.getElementById("<%=tbState.ClientID%>").focus();
            }
            function alertCallBackHotelIcaoFn(arg) {
                document.getElementById("<%=tbHotelAirportIcao.ClientID%>").focus();
            }
            function alertCallBackHotelCountryFn(arg) {
                document.getElementById("<%=tbHotelCountry.ClientID%>").focus();
            }
            function alertCallBackHotelStateFn(arg) {
                document.getElementById("<%=tbHotelState.ClientID%>").focus();
            }
            function alertCallBackVendorIcaoFn(arg) {
                document.getElementById("<%=tbVendorAirportIcao.ClientID%>").focus();
            }
            function alertCallBackVendorCountryFn(arg) {
                document.getElementById("<%=tbVendorCountry.ClientID%>").focus();
            }
            function alertCallBackVendorStateFn(arg) {
                document.getElementById("<%=tbVendorState.ClientID%>").focus();
            }
            function alertCallBackCustomIcaoFn(arg) {
                document.getElementById("<%=tbCustomIcao.ClientID%>").focus();
            }
            function alertCallBackCustomCountryFn(arg) {
                document.getElementById("<%=tbCustomCountry.ClientID%>").focus();
            }
            function alertCallBackCustomStateFn(arg) {
                document.getElementById("<%=tbCustomState.ClientID%>").focus();
            }

            function LinkToMapQuest() {
                var ReturnValue = false;
                var tbAddr1 = '';
                var tbCity = document.getElementById("<%=hdnCity.ClientID%>").value;
                var tbState = document.getElementById("<%=hdnState.ClientID%>").value;
                var tbPostal = '';
                var tbCountry = document.getElementById("<%=hdnCountrs.ClientID%>").value;
                var Latitude = document.getElementById("<%=hdnLatitude.ClientID%>").value;
                var Longitude = document.getElementById("<%=hdnLongitude.ClientID%>").value;
                var URL = "http://www.mapquest.com";
                if (Latitude == '' || Latitude == "&nbsp;" || parseFloat(Latitude) == 0.0 || Longitude == '' || Latitude == "&nbsp;" || parseFloat(Longitude) == 0.0) {
                    URL += "/maps?address=" + encodeURIComponent(tbAddr1) + "&city=" + tbCity + "&state=" + tbState + "&zipcode=" + tbPostal + "&country=" + tbCountry + "&redirect=true";
                }
                else {
                    URL += "/?q=" + Latitude + "," + Longitude;
                }
                window.open(URL);
                return ReturnValue;
            }

            function openReport(radWin) {
                var url = '';
                var AirportId = '';
                var Runway = '';
                var Miles = '';
                var Country = '';

                var ReportName_Tab = document.getElementById('RadTabLocator');

                if (ReportName_Tab.control.get_selectedTab().get_value() == "1") {
                    ReportName = "RptLocatorAirport"

                    if (document.getElementById("<%=hdnAirportICAOID.ClientID%>").value != null && document.getElementById("<%=hdnAirportICAOID.ClientID%>").value != '') {
                        AirportId = document.getElementById("<%=hdnAirportICAOID.ClientID%>").value;
                    }

                    if (AirportId == '' && (document.getElementById("<%=hdnIata.ClientID%>").value != null && document.getElementById("<%=hdnIata.ClientID%>").value != '')) {
                        AirportId = document.getElementById("<%=hdnIata.ClientID%>").value;
                    }

                    if (document.getElementById("<%=tbRunway.ClientID%>").value == '') {
                        Runway = '0';
                    }

                    if (document.getElementById("<%=tbMiles.ClientID%>").value == '') {
                        Miles = '0';
                    }

                    if (document.getElementById("<%=hdnCountry.ClientID%>").value == '') {
                        Country = '0';
                    }

                    url = '../Reports/ExportReportInformation.aspx?Report=' + ReportName + '&P1=' + document.getElementById("<%=tbAirportICAO.ClientID%>").value + '&P2=' + document.getElementById("<%=tbCountry.ClientID%>").value + '&P3=' + Runway + '&P4=' + document.getElementById("<%=tbCity.ClientID%>").value + '&P5=' + document.getElementById("<%=tbMetroCD.ClientID%>").value + '&P6=' + document.getElementById("<%=tbIata.ClientID%>").value + '&P7=' + Miles + '&P8=' + document.getElementById("<%=tbState.ClientID%>").value + '&P9=' + document.getElementById("<%=tbName.ClientID%>").value + '&P10=' + document.getElementById("chkHeliport").checked + '&P11=' + document.getElementById("chkApofEntry").checked + '&P12=' + document.getElementById("chkIncludeInactivateAirport").checked + '&P13=' + Country + '&P14=' + AirportId;
                }
                else if (ReportName_Tab.control.get_selectedTab().get_value() == "2") {
                    ReportName = "RptLocatorHotel"

                    if (document.getElementById("<%=hdnHotelAirportIcao.ClientID%>").value != null && document.getElementById("<%=hdnHotelAirportIcao.ClientID%>").value != '') {
                        AirportId = document.getElementById("<%=hdnHotelAirportIcao.ClientID%>").value;
                    }

                    if (AirportId == '' && (document.getElementById("<%=hdnHotelIata.ClientID%>").value != null && document.getElementById("<%=hdnHotelIata.ClientID%>").value != '')) {
                        AirportId = document.getElementById("<%=hdnHotelIata.ClientID%>").value;
                    }

                    if (document.getElementById("<%=tbHotelMilesFrom.ClientID%>").value == '') {
                        Miles = '0';
                    }

                    if (document.getElementById("<%=hdnHotelCountry.ClientID%>").value == '') {
                        Country = '0';
                    }

                    url = '../Reports/ExportReportInformation.aspx?Report=' + ReportName + '&P1=' + document.getElementById("<%=tbHotelAirportIcao.ClientID%>").value + '&P2=' + document.getElementById("<%=tbHotelCountry.ClientID%>").value + '&P3=' + Miles + '&P4=' + document.getElementById("<%=tbHotelCity.ClientID%>").value + '&P5=' + document.getElementById("<%=tbHotelMetro.ClientID%>").value + '&P6=' + document.getElementById("<%=tbHotelIata.ClientID%>").value + '&P7=' + document.getElementById("<%=tbHotelState.ClientID%>").value + '&P8=' + document.getElementById("<%=tbHotelName.ClientID%>").value + '&P9=' + Country + '&P10=' + AirportId;
                }
                else if (ReportName_Tab.control.get_selectedTab().get_value() == "3") {

                    ReportName = "RptLocatorVendor"

                    if (document.getElementById("<%=hdnVendorIcao.ClientID%>").value != null && document.getElementById("<%=hdnVendorIcao.ClientID%>").value != '') {
                        AirportId = document.getElementById("<%=hdnVendorIcao.ClientID%>").value;
                    }

                    if (AirportId == '' && (document.getElementById("<%=hdnVendorIata.ClientID%>").value != null && document.getElementById("<%=hdnVendorIata.ClientID%>").value != '')) {
                        AirportId = document.getElementById("<%=hdnVendorIata.ClientID%>").value;
                    }

                    if (document.getElementById("<%=tbVendorMiles.ClientID%>").value == '') {
                        Miles = '0';
                    }

                    url = '../Reports/ExportReportInformation.aspx?Report=' + ReportName + '&P1=' + document.getElementById("<%=tbVendorAirportIcao.ClientID%>").value + '&P2=' + document.getElementById("<%=tbVendorCountry.ClientID%>").value + '&P3=' + Miles + '&P4=' + document.getElementById("<%=tbVendorCity.ClientID%>").value + '&P5=' + document.getElementById("<%=tbVendorMetro.ClientID%>").value + '&P6=' + document.getElementById("<%=tbAircraftType.ClientID%>").value + '&P7=' + document.getElementById("<%=tbVendorIata.ClientID%>").value + '&P8=' + document.getElementById("<%=tbVendorState.ClientID%>").value + '&P9=' + document.getElementById("<%=tbVendorName.ClientID%>").value + '&P10=' + AirportId;
                }
                else if (ReportName_Tab.control.get_selectedTab().get_value() == "4") {
                    ReportName = "RptLocatorCustom"

                    if (document.getElementById("<%=hdnCustomIcao.ClientID%>").value != null && document.getElementById("<%=hdnCustomIcao.ClientID%>").value != '') {
                        AirportId = document.getElementById("<%=hdnCustomIcao.ClientID%>").value;
                    }

                    if (AirportId == '' && (document.getElementById("<%=hdnCustomIata.ClientID%>").value != null && document.getElementById("<%=hdnCustomIata.ClientID%>").value != '')) {
                        AirportId = document.getElementById("<%=hdnCustomIata.ClientID%>").value;
                    }

                    if (document.getElementById("<%=tbCustomMiles.ClientID%>").value == '') {
                        Miles = '0';
                    }
                    url = '../Reports/ExportReportInformation.aspx?Report=' + ReportName + '&P1=' + document.getElementById("<%=tbCustomIcao.ClientID%>").value + '&P2=' + document.getElementById("<%=tbCustomCountry.ClientID%>").value + '&P3=' + Miles + '&P4=' + document.getElementById("<%=tbCustomCity.ClientID%>").value + '&P5=' + document.getElementById("<%=tbCustomMetro.ClientID%>").value + '&P6=' + document.getElementById("<%=tbVendorIata.ClientID%>").value + '&P7=' + document.getElementById("<%=tbCustomState.ClientID%>").value + '&P8=' + document.getElementById("<%=tbCustomName.ClientID%>").value + '&P9=' + AirportId;
                }

                var oWnd = radopen(url, "RadExportData");
            }

            function ShowReports(radWin, ReportFormat) {
                var ReportName_Tab = document.getElementById('RadTabLocator');

                if (ReportName_Tab.control.get_selectedTab().get_value() == "1") {
                    if (ReportFormat == 'PDF') {
                        ReportName = "RptLocatorAirport"
                    }
                    else {
                        ReportName = "RptLocatorAirportExport"
                    }
                }
                else if (ReportName_Tab.control.get_selectedTab().get_value() == "2") {
                    if (ReportFormat == 'PDF') {
                        ReportName = "RptLocatorHotel"
                    }
                    else {
                        ReportName = "RptLocatorHotelExport"
                    }
                }
                else if (ReportName_Tab.control.get_selectedTab().get_value() == "3") {
                    if (ReportFormat == 'PDF') {
                        ReportName = "RptLocatorVendor"
                    }
                    else {
                        ReportName = "RptLocatorVendorExport"
                    }
                }
                else if (ReportName_Tab.control.get_selectedTab().get_value() == "4") {
                    if (ReportFormat == 'PDF') {
                        ReportName = "RptLocatorCustom"
                    }
                    else {
                        ReportName = "RptLocatorCustomExport"
                    }
                }

                document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
                document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
                return true;
                //Commented by Ramesh, defect# 6429  since only excel option is allowed, not displaying export choices.
                //                if (ReportFormat == "EXPORT") {
                //                    url = "../Reports/ExportReportInformation.aspx";
                //                    var oWnd = radopen(url, 'RadExportData');
                //                    return true;
                //                }
                //                else {
                //                    return true;
                //                }
            }

        </script>
    </telerik:RadCodeBlock>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableCdn="true">
    </telerik:RadScriptManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadExportData" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Export Report Information" KeepInScreenBounds="true" Height="540px" Width="880px"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadAicraftTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="onClientCloseAicraftTypePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/AircraftTypePopUp.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radOldICAOPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientOldICAOClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>        
        <Windows>
            <telerik:RadWindow ID="RadMetroCityPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="onClientCloseMetroCityPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/MetroCityPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radHotelOldICAOPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHotelOldICAOClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadHotelMetroCityPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="onClientCloseHotelMetroCityPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/MetroCityPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadVendorMetroCityPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="onClientCloseVendorMetroCityPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/MetroCityPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radVendorICAOPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientVendorOldICAOClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radCustomMetroPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="onClientCloseCustomMetroCityPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/MetroCityPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radCustomICAOPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCustomOldICAOClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radOldIATAPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientAirportIataClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radHotelIATAPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHotelIataClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radVendorIATAPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientVendorIataClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radCustomIATAPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCustomIataClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <AjaxSettings>
            <%--<telerik:AjaxSetting AjaxControlID="Div1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Div1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Div1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divIcons" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div style="margin: 0 auto; width: 980px;">
        <table cellpadding="0" cellspacing="0" width="100%" class="box1">
            <tr>
                <td valign="top">
                    <div id="Div1" runat="server" class="ExternalForm">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left">
                                    <div id="divIcons" runat="server" class="tab-nav-top">
                                        <span class="head-title">Locator</span> <span class="tab-nav-icons">
                                            <asp:LinkButton runat="server" ID="lnkMapQuest" ToolTip="View Map" CssClass="map-icon"
                                                OnClientClick="javascript:LinkToMapQuest();" />
                                            <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:ShowReports('','PDF');"
                                                title="Preview Report" OnClick="btnShowReports_OnClick" CssClass="search-icon"></asp:LinkButton>
                                            <%-- <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="javascript:ShowReports('','Excel');"
                                                title="Preview Excel" OnClick="btnShowReports_OnClick" CssClass="save-icon"></asp:LinkButton>--%>
                                            <asp:LinkButton ID="lbtnSaveReports" runat="server" OnClientClick="javascript:openReport('Locator');return false;"
                                                title="Export Report" CssClass="save-icon"></asp:LinkButton>
                                            <asp:Button ID="btnShowReports" runat="server" CssClass="button-disable" Style="display: none;" />
                                            <a href="#" title="Help" class="help-icon"></a></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="tblspace_10">
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div id="DivExternalForm" runat="server" class="ExternalForm">
                            <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
                                <div class="exampleWrapper">
                                    <telerik:RadTabStrip ID="RadTabLocator" runat="server" Skin="Simple" SelectedIndex="0"
                                        OnTabClick="rtCorpRequest_Clicked" MultiPageID="RadMultiPage1" Align="Justify"
                                        ReorderTabsOnSelect="true" Width="400px">
                                        <Tabs>
                                            <telerik:RadTab Value="1" Text="Airports">
                                            </telerik:RadTab>
                                            <telerik:RadTab Value="2" Text="Hotels">
                                            </telerik:RadTab>
                                            <telerik:RadTab Value="3" Text="Vendors">
                                            </telerik:RadTab>
                                            <telerik:RadTab Value="4" Text="Custom">
                                            </telerik:RadTab>
                                        </Tabs>
                                    </telerik:RadTabStrip>
                                    <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" CssClass="pageView"
                                        Width="100%">
                                        <telerik:RadPageView ID="RadPageAirport" runat="server">
                                            <div id="divAirportsForm" runat="server" class="ExternalForm">
                                                <asp:Panel ID="pnlAirportsForm" runat="server" Visible="true">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <telerik:RadSplitter ID="RadSplitter1" runat="server" PanesBorderSize="0" Width="100%"
                                                                    Skin="Windows7">
                                                                    <telerik:RadPane ID="LeftPane" runat="server" Scrolling="None" Width="70%" Style="background: red!important;">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <telerik:RadGrid ID="dgAirportLocator" runat="server" AllowSorting="true" Visible="true"
                                                                                        OnPreRender="RadGrid1_PreRender" OnItemCommand="AirportLocator_ItemCommand" OnPageIndexChanged="MetroCity_PageIndexChanged"
                                                                                        Width="100%" Height="350px" OnSortCommand="dgAirportLocator_SortCommand" OnNeedDataSource="dgAirportLocator_BindData"
                                                                                        OnSelectedIndexChanged="dgAirportLocator_SelectedIndexChanged" AutoGenerateColumns="false"
                                                                                        MasterTableView-NoMasterRecordsText="No records to display." AllowPaging="false"
                                                                                        AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true">
                                                                                        <GroupingSettings CaseSensitive="false" />
                                                                                        <MasterTableView NoMasterRecordsText="" CommandItemDisplay="Bottom" AllowPaging="false"
                                                                                            AllowMultiColumnSorting="false">
                                                                                            <Columns>
                                                                                                <telerik:GridBoundColumn DataField="AirportName" HeaderText="Airport Name" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="240px" FilterControlWidth="220px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="IcaoID" HeaderText="ICAO" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="60px" FilterControlWidth="40px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="City" HeaderText="City" CurrentFilterFunction="Contains"
                                                                                                    HeaderStyle-Width="160px" FilterControlWidth="140px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="State" HeaderText="State" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="80px" FilterControlWidth="60px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="CountryCD" HeaderText="Country" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="60px" FilterControlWidth="40px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="Miles" HeaderText="Driving Miles" CurrentFilterFunction="Contains"
                                                                                                    HeaderStyle-Width="60px" FilterControlWidth="40px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="RunwayLength" HeaderText="Runway" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="60px" CurrentFilterFunction="Contains" AllowFiltering="false" ShowFilterIcon="false">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridCheckBoxColumn DataField="IsHeliport" HeaderText="Heliport" AutoPostBackOnFilter="true"
                                                                                                    AllowFiltering="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                                                                                                    HeaderStyle-Width="60px">
                                                                                                </telerik:GridCheckBoxColumn>
                                                                                                 <telerik:GridCheckBoxColumn DataField="IsEntryPort" HeaderText="Port <br> of <br> Entry" AutoPostBackOnFilter="true"
                                                                                                    AllowFiltering="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                                                                                                    HeaderStyle-Width="60px">
                                                                                                </telerik:GridCheckBoxColumn>
                                                                                                <telerik:GridBoundColumn DataField="OrigLatitude" HeaderText="Latitude" Display="false"
                                                                                                    AutoPostBackOnFilter="true" HeaderStyle-Width="60px" FilterControlWidth="40px"
                                                                                                    ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="OrigLongitude" HeaderText="Longitude" Display="false"
                                                                                                    AutoPostBackOnFilter="true" HeaderStyle-Width="60px" FilterControlWidth="40px"
                                                                                                    ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                            </Columns>
                                                                                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                                        </MasterTableView>
                                                                                        <ClientSettings EnablePostBackOnRowClick="true">
                                                                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                                            <Selecting AllowRowSelect="true" />
                                                                                        </ClientSettings>
                                                                                        <GroupingSettings CaseSensitive="false" />
                                                                                    </telerik:RadGrid>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <%--<td align="left" style="display: none">
                                                                                            <a href="#" class="map-icon"></a>
                                                                                        </td>--%>
                                                                                            <td align="left" class="tdLabel80">
                                                                                                <asp:Button ID="btnResetAll" CssClass="button" runat="server" Text="Reset All" OnClick="btnResetAll_Click" />
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <asp:Label ID="lbAirportText" runat="server" CssClass="alert-text"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </telerik:RadPane>
                                                                    <telerik:RadSplitBar ID="RadSplitBar1" runat="server" CollapseMode="Backward" />
                                                                    <telerik:RadPane ID="RightPane" runat="server" Width="30%" Scrolling="Y">
                                                                        <div class="ul_splitter_header">
                                                                            <span>Search Criteria</span></div>
                                                                        <div class="ul_search_area">
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        ICAO
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbAirportICAO" runat="server" CssClass="text80" MaxLength="4" AutoPostBack="true"
                                                                                            OnTextChanged="tbAirportICAO_TextChanged"></asp:TextBox>
                                                                                        <asp:Button ID="btnAirportICAO" OnClientClick="javascript:openWin('radOldICAOPopup');return false;"
                                                                                            CssClass="browse-button" runat="server" />
                                                                                        <asp:HiddenField ID="hdnAirportICAOID" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:CustomValidator ID="cvAirportICAO" runat="server" ControlToValidate="tbAirportICAO"
                                                                                            ErrorMessage="Invalid ICAO Code." Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        IATA
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbIata" runat="server" CssClass="text80" MaxLength="3" AutoPostBack="true"
                                                                                            OnTextChanged="tbIata_TextChanged"></asp:TextBox>
                                                                                        <asp:Button ID="btnIata" OnClientClick="javascript:openWin('radOldIATAPopup');return false;"
                                                                                            CssClass="browse-button" runat="server" />
                                                                                        <asp:HiddenField ID="hdnIata" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:CustomValidator ID="cvIata" runat="server" ControlToValidate="tbIata" ErrorMessage="Invalid IATA Code."
                                                                                            Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Country
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="hidden" runat="server" clientidmode="Static" id="hdnCountryCode" />
                                                                                        <asp:TextBox runat="server" CssClass="tbCountryAutoComplete" Style="width: 156px" ID="tbCountry" />
                                                                                        <asp:HiddenField runat="server" ID="tbCountryName"  ClientIDMode="Static"/>
                                                                                        <asp:HiddenField ID="hdnCountry" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:CustomValidator ID="cvCountry" runat="server" ControlToValidate="tbCountry"
                                                                                            ErrorMessage="Invalid Country Code" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Minimum Runway 
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbRunway" runat="server" MaxLength="5" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        City
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbCity" runat="server" CssClass="text150" MaxLength="30"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Metro
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <asp:TextBox ID="tbMetroCD" runat="server" CssClass="text50" MaxLength="3" AutoPostBack="true"
                                                                                            OnTextChanged="tbMetroCD_TextChanged"></asp:TextBox>
                                                                                        <asp:Button ID="btnMetro" runat="server" OnClientClick="javascript:openWin('RadMetroCityPopup');return false;"
                                                                                            CssClass="browse-button" />
                                                                                        <asp:HiddenField ID="hdnMetroId" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:CustomValidator ID="cvMetro" runat="server" ControlToValidate="tbMetroCD" ErrorMessage="Invalid Metro Code."
                                                                                            Display="Dynamic" class="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Miles From
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbMiles" runat="server" CssClass="text80" MaxLength="4" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        State/Prov
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbState" runat="server" CssClass="text150" MaxLength="10"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Name
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbName" runat="server" CssClass="text150" MaxLength="40"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <div class="nav-space">
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkHeliport" runat="server" Text="Heliport" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkApofEntry" runat="server" Text="Port of Entry" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <asp:CheckBox ID="chkIncludeInactivateAirport" runat="server" Text="Include Inactive Airports" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <div class="nav-space">
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" align="center">
                                                                                        <asp:Button ID="btnReset" CssClass="button" runat="server" Text="Reset" OnClick="btnReset_Click" />
                                                                                        <asp:Button ID="btnSearch" CssClass="button" runat="server" Text="Search" OnClick="btnSearch_Click" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </telerik:RadPane>
                                                                </telerik:RadSplitter>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </div>
                                        </telerik:RadPageView>
                                        <telerik:RadPageView ID="RadPageHotel" runat="server">
                                            <div id="divHotelForm" runat="server" class="ExternalForm">
                                                <asp:Panel ID="pnlHotelForm" runat="server" Visible="true">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <telerik:RadSplitter ID="RadSplitter2" runat="server" PanesBorderSize="0" Width="100%"
                                                                    Skin="Windows7">
                                                                    <telerik:RadPane ID="RadPane1" runat="server" Scrolling="None" Width="70%">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <telerik:RadGrid ID="dgHotel" runat="server" Visible="true" AutoGenerateColumns="false"
                                                                                        OnPageIndexChanged="dgHotel_PageIndexChanged" Width="100%" Height="350px" OnSelectedIndexChanged="dgHotel_SelectedIndexChanged"
                                                                                        OnSortCommand="dgHotel_SortCommand" AllowPaging="false" AllowFilteringByColumn="true"
                                                                                        OnNeedDataSource="dgHotel_BindData" PagerStyle-AlwaysVisible="true" AllowSorting="true"
                                                                                        MasterTableView-NoMasterRecordsText="No records to display.">
                                                                                        <GroupingSettings CaseSensitive="false" />
                                                                                        <MasterTableView NoMasterRecordsText="" CommandItemDisplay="Bottom" AllowPaging="false"
                                                                                            AllowMultiColumnSorting="false">
                                                                                            <Columns>
                                                                                                <telerik:GridBoundColumn DataField="Name" HeaderText="Hotel Name" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="220px" FilterControlWidth="200px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="IcaoID" HeaderText="ICAO" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="60px" FilterControlWidth="40px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="CityName" HeaderText="City" CurrentFilterFunction="Contains"
                                                                                                    HeaderStyle-Width="140px" FilterControlWidth="120px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="StateName" HeaderText="State" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="140px" FilterControlWidth="120px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="CountryCD" HeaderText="Country" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="60px" FilterControlWidth="40px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="Miles" HeaderText="Driving Miles" CurrentFilterFunction="Contains"
                                                                                                    HeaderStyle-Width="60px" FilterControlWidth="40px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="HotelRating" HeaderText="Rating" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="50px" FilterControlWidth="30px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="NegociatedRate" HeaderText="Neg.Rate" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="80px" FilterControlWidth="60px" ShowFilterIcon="false" CurrentFilterFunction="EqualTo">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="PhoneNum" HeaderText="Business Phone" CurrentFilterFunction="EqualTo"
                                                                                                    HeaderStyle-Width="110px" FilterControlWidth="90px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="OrigLatitude" HeaderText="Latitude" Display="false"
                                                                                                    AutoPostBackOnFilter="true" HeaderStyle-Width="60px" FilterControlWidth="40px"
                                                                                                    ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="OrigLongitude" HeaderText="Longitude" Display="false"
                                                                                                    AutoPostBackOnFilter="true" HeaderStyle-Width="60px" FilterControlWidth="40px"
                                                                                                    ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                            </Columns>
                                                                                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                                        </MasterTableView>
                                                                                        <ClientSettings EnablePostBackOnRowClick="true">
                                                                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                                            <Selecting AllowRowSelect="true" />
                                                                                        </ClientSettings>
                                                                                        <GroupingSettings CaseSensitive="false" />
                                                                                    </telerik:RadGrid>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <%--<td align="left" style="display: none">
                                                                                            <a href="#" class="map-icon"></a>
                                                                                        </td>--%>
                                                                                            <td align="left">
                                                                                                <asp:Button ID="btnHotelResetAlll" CssClass="button" runat="server" Text="Reset All"
                                                                                                    OnClick="btnHotelResetAlll_Click" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </telerik:RadPane>
                                                                    <telerik:RadSplitBar ID="RadSplitBar2" runat="server" CollapseMode="Backward" />
                                                                    <telerik:RadPane ID="RadPane2" runat="server" Width="30%" Scrolling="None">
                                                                        <div class="ul_splitter_header">
                                                                            <span>Search Criteria</span></div>
                                                                        <div class="ul_search_area">
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        ICAO
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbHotelAirportIcao" runat="server" CssClass="text80" MaxLength="4"
                                                                                            AutoPostBack="true" OnTextChanged="tbHotelAirportIcao_TextChanged"></asp:TextBox>
                                                                                        <asp:Button ID="btnHotelIcao" OnClientClick="javascript:openWin('radHotelOldICAOPopup');return false;"
                                                                                            CssClass="browse-button" runat="server" />
                                                                                        <asp:HiddenField ID="hdnHotelAirportIcao" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:CustomValidator ID="cvHotelAirportIcao" runat="server" ControlToValidate="tbHotelAirportIcao"
                                                                                            ErrorMessage="Invalid ICAO Code." Display="Dynamic" CssClass="alert-text" ValidationGroup="SaveHotel"
                                                                                            SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        IATA
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbHotelIata" runat="server" CssClass="text80" MaxLength="3" AutoPostBack="true"
                                                                                            OnTextChanged="tbHotelIata_TextChanged"></asp:TextBox>
                                                                                        <asp:Button ID="btnHotelIata" OnClientClick="javascript:openWin('radHotelIATAPopup');return false;"
                                                                                            CssClass="browse-button" runat="server" />
                                                                                        <asp:HiddenField ID="hdnHotelIata" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:CustomValidator ID="cvHotelIata" runat="server" ControlToValidate="tbHotelIata"
                                                                                            ErrorMessage="Invalid IATA Code." Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Country
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="hidden" runat="server" clientidmode="Static" id="hdnHotelCountryCode" />
                                                                                        <asp:TextBox runat="server" CssClass="tbHotelCountryAutoComplete" Style="width: 156px" ID="tbHotelCountry" />
                                                                                        <asp:HiddenField runat="server" ID="tbHotelCountryName"  ClientIDMode="Static"/>                                                                                       
                                                                                        <asp:HiddenField ID="hdnHotelCountry" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CustomValidator ID="cvHotelCountry" runat="server" ControlToValidate="tbHotelCountry"
                                                                                            ErrorMessage="Invalid Country Code" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"
                                                                                            SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        City
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbHotelCity" runat="server" CssClass="text150" MaxLength="30"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Metro
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <asp:TextBox ID="tbHotelMetro" runat="server" CssClass="text50" MaxLength="3" AutoPostBack="true"
                                                                                            OnTextChanged="tbHotelMetro_TextChanged"></asp:TextBox>
                                                                                        <asp:Button ID="btn" runat="server" OnClientClick="javascript:openWin('RadHotelMetroCityPopup');return false;"
                                                                                            CssClass="browse-button" />
                                                                                        <asp:HiddenField ID="hdnHotelMetro" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:CustomValidator ID="cvHotelMetro" runat="server" ControlToValidate="tbHotelMetro"
                                                                                            ErrorMessage="Invalid Metro Code." Display="Dynamic" class="alert-text" ValidationGroup="Save"
                                                                                            SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Miles From
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbHotelMilesFrom" runat="server" CssClass="text80" MaxLength="4"
                                                                                            onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        State/Prov
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbHotelState" runat="server" CssClass="text150" MaxLength="10"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Name
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbHotelName" runat="server" CssClass="text150" MaxLength="40"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="nav-space">
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" align="center">
                                                                                        <asp:Button ID="btnHotelReset" CssClass="button" runat="server" Text="Reset" OnClick="btnHotelReset_Click" />
                                                                                        <asp:Button ID="btnHotelSearch" CssClass="button" runat="server" Text="Search" OnClick="btnHotelSearch_Click" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </telerik:RadPane>
                                                                </telerik:RadSplitter>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </div>
                                        </telerik:RadPageView>
                                        <telerik:RadPageView ID="RadPageVendor" runat="server">
                                            <div id="divVendorForm" runat="server" class="ExternalForm">
                                                <asp:Panel ID="pnlVendorForm" runat="server" Visible="true">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <telerik:RadSplitter ID="RadSplitter3" runat="server" PanesBorderSize="0" Width="100%"
                                                                    Skin="Windows7">
                                                                    <telerik:RadPane ID="RadPane3" runat="server" Scrolling="None" Width="70%">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <telerik:RadGrid ID="dgVendors" runat="server" AllowSorting="true" Visible="true"
                                                                                        OnPageIndexChanged="dgVendors_PageIndexChanged" Width="100%" Height="350px" OnSelectedIndexChanged="dgVendors_SelectedIndexChanged"
                                                                                        OnSortCommand="dgVendors_SortCommand" OnNeedDataSource="dgVendors_BindData" AutoGenerateColumns="false"
                                                                                        MasterTableView-NoMasterRecordsText="No records to display." AllowPaging="false"
                                                                                        AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" AllowMultiColumnSorting="true">
                                                                                        <GroupingSettings CaseSensitive="false" />
                                                                                        <MasterTableView NoMasterRecordsText="" CommandItemDisplay="Bottom" AllowPaging="false"
                                                                                            AllowMultiColumnSorting="false">
                                                                                            <Columns>
                                                                                                <telerik:GridBoundColumn DataField="Name" HeaderText="Vendor Name" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="240px" FilterControlWidth="220px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="IcaoID" HeaderText="ICAO" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="60px" FilterControlWidth="40px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="BillingCity" HeaderText="City" CurrentFilterFunction="Contains"
                                                                                                    HeaderStyle-Width="160px" FilterControlWidth="140px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="BillingState" HeaderText="State" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="160px" FilterControlWidth="140px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="CountryCD" HeaderText="Country" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="60px" FilterControlWidth="40px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="Miles" HeaderText="Driving Miles" CurrentFilterFunction="Contains"
                                                                                                    HeaderStyle-Width="60px" FilterControlWidth="40px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="AircraftCD" HeaderText="Aircraft" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="80px" FilterControlWidth="60px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="BusinessPhone" HeaderText="Business Phone" CurrentFilterFunction="EqualTo"
                                                                                                    HeaderStyle-Width="110px" FilterControlWidth="90px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="OrigLatitude" HeaderText="Latitude" Display="false"
                                                                                                    AutoPostBackOnFilter="true" HeaderStyle-Width="60px" FilterControlWidth="40px"
                                                                                                    ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="OrigLongitude" HeaderText="Longitude" Display="false"
                                                                                                    AutoPostBackOnFilter="true" HeaderStyle-Width="60px" FilterControlWidth="40px"
                                                                                                    ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                            </Columns>
                                                                                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                                        </MasterTableView>
                                                                                        <ClientSettings EnablePostBackOnRowClick="true">
                                                                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                                            <Selecting AllowRowSelect="true" />
                                                                                        </ClientSettings>
                                                                                        <GroupingSettings CaseSensitive="false" />
                                                                                    </telerik:RadGrid>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <%--<td align="left" style="display: none">
                                                                                            <a href="#" class="map-icon"></a>
                                                                                        </td>--%>
                                                                                            <td align="left">
                                                                                                <asp:Button ID="btnVendorResetAll" CssClass="button" runat="server" Text="Reset All"
                                                                                                    OnClick="btnVendorResetAll_Click" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </telerik:RadPane>
                                                                    <telerik:RadSplitBar ID="RadSplitBar3" runat="server" CollapseMode="Backward" />
                                                                    <telerik:RadPane ID="RadPane4" runat="server" Width="30%" Scrolling="None">
                                                                        <div class="ul_splitter_header">
                                                                            <span>Search Criteria</span></div>
                                                                        <div class="ul_search_area">
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        ICAO
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbVendorAirportIcao" runat="server" CssClass="text80" MaxLength="4"
                                                                                            AutoPostBack="true" OnTextChanged="tbVendorAirportIcao_TextChanged"></asp:TextBox>
                                                                                        <asp:Button ID="btnVendorICao" OnClientClick="javascript:openWin('radVendorICAOPopup');return false;"
                                                                                            CssClass="browse-button" runat="server" />
                                                                                        <asp:HiddenField ID="hdnVendorIcao" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <asp:CustomValidator ID="cvVendorIcao" runat="server" ControlToValidate="tbVendorAirportIcao"
                                                                                            ErrorMessage="Invalid ICAO Code." Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        IATA
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbVendorIata" runat="server" CssClass="text80" MaxLength="3" AutoPostBack="true"
                                                                                            OnTextChanged="tbVendorIata_TextChanged"></asp:TextBox>
                                                                                        <asp:Button ID="btnVendorIata" OnClientClick="javascript:openWin('radVendorIATAPopup');return false;"
                                                                                            CssClass="browse-button" runat="server" />
                                                                                        <asp:HiddenField ID="hdnVendorIata" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <asp:CustomValidator ID="cvVendorIata" runat="server" ControlToValidate="tbVendorIata"
                                                                                            ErrorMessage="Invalid IATA Code." Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Country
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="hidden" runat="server" clientidmode="Static" id="hdnVendorCountryCode" />
                                                                                        <asp:TextBox runat="server" CssClass="tbVendorCountryAutoComplete" Style="width: 156px" ID="tbVendorCountry" />
                                                                                        <asp:HiddenField runat="server" ID="tbVendorCountryName"  ClientIDMode="Static"/>
                                                                                        <asp:HiddenField ID="hdnVendorCountry" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <asp:CustomValidator ID="cvVendorCountry" runat="server" ControlToValidate="tbVendorCountry"
                                                                                            ErrorMessage="Invalid Country Code" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"
                                                                                            SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        City
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbVendorCity" runat="server" CssClass="text150" MaxLength="30"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Metro
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbVendorMetro" runat="server" CssClass="text80" MaxLength="3" AutoPostBack="true"
                                                                                            OnTextChanged="tbVendorMetro_TextChanged"></asp:TextBox>
                                                                                        <asp:Button ID="btnVendorMetro" runat="server" OnClientClick="javascript:openWin('RadVendorMetroCityPopup');return false;"
                                                                                            CssClass="browse-button" />
                                                                                        <asp:HiddenField ID="hdnVendorMetro" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <asp:CustomValidator ID="cvVendorMetro" runat="server" ControlToValidate="tbVendorMetro"
                                                                                            ErrorMessage="Invalid Metro Code." Display="Dynamic" class="alert-text" ValidationGroup="Save"
                                                                                            SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Miles From
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbVendorMiles" runat="server" CssClass="text80" MaxLength="4" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        State/Prov
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbVendorState" runat="server" CssClass="text150" MaxLength="10"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Name
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbVendorName" runat="server" CssClass="text150" MaxLength="40"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Aircraft
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbAircraftType" runat="server" AutoPostBack="true" CssClass="text70"
                                                                                            MaxLength="10" OnTextChanged="AircraftType_TextChanged">
                                                                                        </asp:TextBox>
                                                                                        <asp:Button ID="btnAircraftType" OnClientClick="javascript:openWin('RadAicraftTypePopup');return false;"
                                                                                            CssClass="browse-button" runat="server" />
                                                                                        <asp:HiddenField ID="hdnAircraftType" runat="server" />
                                                                                        <asp:CustomValidator ID="cvAircraftType" runat="server" ControlToValidate="tbAircraftType"
                                                                                            ErrorMessage="Invalid Aircraft Type Code" Display="Dynamic" CssClass="alert-text"
                                                                                            SetFocusOnError="true" ValidationGroup="Save"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <div class="nav-space">
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" align="center">
                                                                                        <asp:Button ID="btnVendorReset" CssClass="button" runat="server" Text="Reset" OnClick="btnVendorReset_Click" />
                                                                                        <asp:Button ID="btnvendorSearch" CssClass="button" runat="server" Text="Search" OnClick="btnvendorSearch_Click" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </telerik:RadPane>
                                                                </telerik:RadSplitter>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </div>
                                        </telerik:RadPageView>
                                        <telerik:RadPageView ID="RadPageCustom" runat="server">
                                            <div id="divCustomForm" runat="server" class="ExternalForm">
                                                <asp:Panel ID="pnlCustomForm" runat="server" Visible="true">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <telerik:RadSplitter ID="RadSplitter4" runat="server" PanesBorderSize="0" Width="100%"
                                                                    Skin="Windows7">
                                                                    <telerik:RadPane ID="RadPane5" runat="server" Scrolling="None" Width="70%">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <telerik:RadGrid ID="dgCustom" runat="server" AllowSorting="true" Visible="true"
                                                                                        OnPageIndexChanged="dgCustom_PageIndexChanged" Width="100%" Height="350px" OnSelectedIndexChanged="dgCustom_SelectedIndexChanged"
                                                                                        OnSortCommand="dgCustom_SortCommand" OnNeedDataSource="dgCustom_BindData" AutoGenerateColumns="false"
                                                                                        MasterTableView-NoMasterRecordsText="No records to display." AllowPaging="false"
                                                                                        AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" AllowMultiColumnSorting="true">
                                                                                        <GroupingSettings CaseSensitive="false" />
                                                                                        <MasterTableView NoMasterRecordsText="" CommandItemDisplay="Bottom" AllowPaging="false"
                                                                                            AllowMultiColumnSorting="false">
                                                                                            <Columns>
                                                                                                <telerik:GridBoundColumn DataField="Name" HeaderText="Name" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="260px" FilterControlWidth="240px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="IcaoID" HeaderText="ICAO" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="60px" FilterControlWidth="40px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="CustomCity" HeaderText="City" CurrentFilterFunction="Contains"
                                                                                                    HeaderStyle-Width="180px" FilterControlWidth="160px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="CustomState" HeaderText="State" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="180px" FilterControlWidth="160px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="CountryCD" HeaderText="Country" AutoPostBackOnFilter="true"
                                                                                                    HeaderStyle-Width="60px" FilterControlWidth="40px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="Miles" HeaderText="Driving Miles" CurrentFilterFunction="Contains"
                                                                                                    HeaderStyle-Width="80px" FilterControlWidth="60px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="PhoneNum" HeaderText="Phone" CurrentFilterFunction="EqualTo"
                                                                                                    HeaderStyle-Width="100px" FilterControlWidth="80px" ShowFilterIcon="false" AutoPostBackOnFilter="true">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="OrigLatitude" HeaderText="Latitude" Display="false"
                                                                                                    AutoPostBackOnFilter="true" HeaderStyle-Width="60px" FilterControlWidth="40px"
                                                                                                    ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn DataField="OrigLongitude" HeaderText="Longitude" Display="false"
                                                                                                    AutoPostBackOnFilter="true" HeaderStyle-Width="60px" FilterControlWidth="40px"
                                                                                                    ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                                                                </telerik:GridBoundColumn>
                                                                                            </Columns>
                                                                                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                                        </MasterTableView>
                                                                                        <ClientSettings EnablePostBackOnRowClick="true">
                                                                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                                            <Selecting AllowRowSelect="true" />
                                                                                        </ClientSettings>
                                                                                        <GroupingSettings CaseSensitive="false" />
                                                                                    </telerik:RadGrid>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <%--<td align="left" style="display: none">
                                                                                            <a href="#" class="map-icon"></a>
                                                                                        </td>--%>
                                                                                            <td align="left">
                                                                                                <asp:Button ID="btnCustomLocatorReset" CssClass="button" runat="server" Text="Reset All"
                                                                                                    OnClick="btnCustomLocatorReset_Click" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </telerik:RadPane>
                                                                    <telerik:RadSplitBar ID="RadSplitBar4" runat="server" CollapseMode="Backward" />
                                                                    <telerik:RadPane ID="RadPane6" runat="server" Width="30%" Scrolling="None">
                                                                        <div class="ul_splitter_header">
                                                                            <span>Search Criteria</span></div>
                                                                        <div class="ul_search_area">
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        ICAO
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbCustomIcao" runat="server" CssClass="text80" MaxLength="4" AutoPostBack="true"
                                                                                            OnTextChanged="tbCustomIcao_TextChanged"></asp:TextBox>
                                                                                        <asp:Button ID="btnCustomIcao" CssClass="browse-button" runat="server" OnClientClick="javascript:openWin('radCustomICAOPopup');return false;" />
                                                                                        <asp:HiddenField ID="hdnCustomIcao" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:CustomValidator ID="cvCustomIcao" runat="server" ControlToValidate="tbCustomIcao"
                                                                                            ErrorMessage="Invalid ICAO Code." Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        IATA
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbCustomIata" runat="server" CssClass="text80" MaxLength="3" AutoPostBack="true"
                                                                                            OnTextChanged="tbCustomIata_TextChanged"></asp:TextBox>
                                                                                        <asp:Button ID="Button1" OnClientClick="javascript:openWin('radCustomIATAPopup');return false;"
                                                                                            CssClass="browse-button" runat="server" />
                                                                                        <asp:HiddenField ID="hdnCustomIata" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:CustomValidator ID="cvCustomIata" runat="server" ControlToValidate="tbCustomIata"
                                                                                            ErrorMessage="Invalid IATA Code." Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Country
                                                                                    </td>
                                                                                    <td>
                                                                                        
                                                                                        <asp:TextBox runat="server" CssClass="tbCustomCountryAutoComplete" Style="width: 156px" ID="tbCustomCountry" />
                                                                                        <asp:HiddenField runat="server" ID="tbCustomCountryName"  ClientIDMode="Static"/>
                                                                                        <asp:HiddenField ID="HiddenField2" runat="server" />
                                                                                        <asp:HiddenField ID="hdnCustomCountry" runat="server" />
                                                                                        <input type="hidden" runat="server" clientidmode="Static" id="hdnCustomCountryCode" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:CustomValidator ID="cvCustomCountry" runat="server" ControlToValidate="tbCustomCountry"
                                                                                            ErrorMessage="Invalid Country Code" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        City
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbCustomCity" runat="server" CssClass="text150" MaxLength="30"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Metro
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <asp:TextBox ID="tbCustomMetro" runat="server" CssClass="text50" MaxLength="3" AutoPostBack="true"
                                                                                            OnTextChanged="tbCustomMetro_TextChanged"></asp:TextBox>
                                                                                        <asp:Button ID="Button3" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('radCustomMetroPopup');return false;" />
                                                                                        <asp:HiddenField ID="hdnCustomMetro" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:CustomValidator ID="cvCustomMetro" runat="server" ControlToValidate="tbCustomMetro"
                                                                                            ErrorMessage="Invalid Metro Code." Display="Dynamic" class="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Miles From
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbCustomMiles" runat="server" CssClass="text80" MaxLength="4" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        State/Prov
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbCustomState" runat="server" CssClass="text150" MaxLength="10"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Name
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbCustomName" runat="server" CssClass="text150" MaxLength="40"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="nav-space">
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" align="center">
                                                                                        <asp:Button ID="btnCustomReset" CssClass="button" runat="server" Text="Reset" OnClick="btnCustomReset_Click" />
                                                                                        <asp:Button ID="btnCustomSearch" CssClass="button" runat="server" Text="Search" OnClick="btnCustomSearch_Click" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </telerik:RadPane>
                                                                </telerik:RadSplitter>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </div>
                                        </telerik:RadPageView>
                                    </telerik:RadMultiPage>
                                    <asp:HiddenField ID="hdnaddress" runat="server" />
                                    <asp:HiddenField ID="hdnCity" runat="server" />
                                    <asp:HiddenField ID="hdnState" runat="server" />
                                    <asp:HiddenField ID="hdnZipCode" runat="server" />
                                    <asp:HiddenField ID="hdnCountrs" runat="server" />
                                    <asp:HiddenField ID="hdnReportName" runat="server" />
                                    <asp:HiddenField ID="hdnReportFormat" runat="server" />
                                    <asp:HiddenField ID="hdnReportParameters" runat="server" />
                                    <asp:HiddenField ID="hdnLatitude" runat="server" />
                                    <asp:HiddenField ID="hdnLongitude" runat="server" />
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
