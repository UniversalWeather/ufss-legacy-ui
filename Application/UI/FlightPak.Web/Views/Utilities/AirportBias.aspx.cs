﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Web.Views.Utilities
{
    public partial class AirportBias : BaseSecuredPage
    {
        private string ModuleNameConstant = "Airport Bias";
        private ExceptionManager exManager;
        private Int64 AirportID;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindAirportBias();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }

        private void BindAirportBias()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<GetAllAirport> AirportList = new List<GetAllAirport>();
                if (Request.QueryString["AirportID"] != null)
                {
                    AirportID = Convert.ToInt64(Request.QueryString["AirportID"].ToString());
                    using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var ObjRetVal = ObjService.GetAirportByAirportID(AirportID);
                        if (ObjRetVal.ReturnFlag == true)
                        {
                            AirportList = ObjRetVal.EntityList;
                        }
                    }
                }
                if (AirportList.Count != 0)
                {
                    tbICAO.Text = System.Web.HttpUtility.HtmlEncode(AirportList[0].IcaoID);
                    tbCity.Text = System.Web.HttpUtility.HtmlEncode(AirportList[0].CityName);
                    tbState.Text = System.Web.HttpUtility.HtmlEncode(AirportList[0].StateName);
                    tbCountry.Text = System.Web.HttpUtility.HtmlEncode(AirportList[0].CountryCD);
                    tbAirport.Text = System.Web.HttpUtility.HtmlEncode(AirportList[0].CountryName);
                    if (AirportList[0].TakeoffBIAS != null)
                    {
                        tbTakeoffBias.Text = System.Web.HttpUtility.HtmlEncode(AirportList[0].TakeoffBIAS.Value.ToString());
                    }
                    else
                    {
                        tbTakeoffBias.Text = System.Web.HttpUtility.HtmlEncode("0:00");
                    }
                    if (AirportList[0].LandingBIAS != null)
                    {
                        tbLandingBias.Text = System.Web.HttpUtility.HtmlEncode(AirportList[0].LandingBIAS.Value.ToString());
                    }
                    else
                    {
                        tbLandingBias.Text = System.Web.HttpUtility.HtmlEncode("0:00");
                    }
                }
            }
        }

    }
}
