﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PowerSettingPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Utilities.PowerSettingPopup" ClientIDMode="AutoID" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Aircraft Power Settings</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshGrid(arg);
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgAircraftCatalog.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                if (selectedRows.length == 0) {
                    radalert("Please select the power setting.", 400, 100, 'Aircraft Power Settings', "");
                    args.cancel = true;
                    return false;
                }
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "PowerSettingID");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "PowerSettings");
                    var cell3 = MasterTable.getCellByColumnUniqueName(row, "TrueAirSpeed");
                    var cell4 = MasterTable.getCellByColumnUniqueName(row, "HourRange");
                    var cell5 = MasterTable.getCellByColumnUniqueName(row, "TakeOffBias");
                    var cell6 = MasterTable.getCellByColumnUniqueName(row, "LandingBias");
                    if (selectedRows.length > 0) {
                        oArg.PowerSettingID = cell1.innerHTML;
                        oArg.PowerSettings = cell2.innerHTML;
                        oArg.TrueAirSpeed = cell3.innerHTML;
                        oArg.HourRange = cell4.innerHTML;
                        oArg.TakeOffBias = cell5.innerHTML;
                        oArg.LandingBias = cell6.innerHTML;
                    }
                    else {
                        oArg.PowerSettingID = "";
                        oArg.PowerSettings = "";
                        oArg.TrueAirSpeed = "";
                        oArg.HourRange = "";
                        oArg.TakeOffBias = "";
                        oArg.LandingBias = "";
                    }
                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }
            function ClosePopup() {
                GetRadWindow().close();
                return false;
            }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table width="100%" class="box1">
            <tr>
                <td>
                    Aircraft:
                    <asp:Label ID="lblAircrafCDtValue" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="preflight-custom-grid">
                    <telerik:RadGrid ID="dgAircraftCatalog" runat="server" OnNeedDataSource="dgAircraftCatalog_BindData"
                        AllowSorting="true" AutoGenerateColumns="false" PagerStyle-AlwaysVisible="false"
                        PageSize="5" Width="100%">
                        <MasterTableView DataKeyNames="PowerSettingID,PowerSettings,TrueAirSpeed,HourRange,TakeOffBias,LandingBias"
                            CommandItemDisplay="None" AllowSorting="false" AllowFilteringByColumn="false"
                            ShowFooter="false" AllowPaging="false">
                            <Columns>
                                <telerik:GridBoundColumn DataField="PowerSettingID" HeaderText="PowerSettingID" UniqueName="PowerSettingID"
                                    CurrentFilterFunction="Contains" FilterDelay="4000" ShowFilterIcon="false" Display="false" />
                                <telerik:GridBoundColumn DataField="PowerSettings" HeaderText="PowerSetting" UniqueName="PowerSettings"
                                    CurrentFilterFunction="Contains" FilterDelay="4000" ShowFilterIcon="false" />
                                <telerik:GridBoundColumn DataField="TrueAirSpeed" HeaderText="TAS" UniqueName="TrueAirSpeed"
                                    CurrentFilterFunction="Contains" FilterDelay="4000" ShowFilterIcon="false" />
                                <telerik:GridBoundColumn DataField="HourRange" HeaderText="Hr.Range" UniqueName="HourRange"
                                    CurrentFilterFunction="Contains" FilterDelay="4000" ShowFilterIcon="false" />
                                <telerik:GridBoundColumn DataField="TakeOffBias" HeaderText="TOBias" UniqueName="TakeOffBias"
                                    CurrentFilterFunction="Contains" FilterDelay="4000" ShowFilterIcon="false" />
                                <telerik:GridBoundColumn DataField="LandingBias" HeaderText="LandBias" UniqueName="LandingBias"
                                    CurrentFilterFunction="Contains" FilterDelay="4000" ShowFilterIcon="false" />
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="returnToParent" />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
            <tr>
                <td align="right" valign="bottom">
                    <asp:Button ID="btPowerSelect" runat="server" CssClass="button" Text="Select" OnClientClick="returnToParent(); return false;" />
                    <asp:Button ID="btPowerCancel" runat="server" CssClass="button" Text="Cancel" OnClientClick="ClosePopup();" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
