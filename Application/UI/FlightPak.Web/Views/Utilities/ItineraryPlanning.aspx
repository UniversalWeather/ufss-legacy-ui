﻿<%@ Page Title="Itinerary Plan" Language="C#" AutoEventWireup="true" CodeBehind="ItineraryPlanning.aspx.cs"
    Inherits="FlightPak.Web.Views.Utilities.ItineraryPlanning" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Itinerary Plan</title>
    <link type="text/css" rel="stylesheet" href="../../Scripts/jquery.alerts.css" />
    <script type="text/javascript" src="../../Scripts/Common.js"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="../../Scripts/jquery.alerts.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function ShowSuccessMessage() {
                $(document).ready(function () {
                    $("#tdSuccessMessage").css("display", "inline");
                    $('#tdSuccessMessage').delay(60000).fadeOut(60000);
                });
            }
        </script>
        <script type="text/javascript">
            function openWin(radWin) {
                var url = '';
                if (radWin == "radTypeCodePopup") {
                    url = '../Settings/Fleet/AircraftPopup.aspx?AircraftCD=' + document.getElementById('<%=tbIPTypeCode.ClientID%>').value;
                }
                else if (radWin == "radTailNumberPopup") {
                    url = '../Settings/Fleet/FleetProfilePopup.aspx?TailNumber=' + document.getElementById('<%=tbIPTailNumber.ClientID%>').value;
                }
                else if (radWin == "radClientPopup") {
                    url = '../Settings/Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('<%=tbIPClientCode.ClientID%>').value;
                }
                else if (radWin == "radHomebasePopup") {
                    url = '../Settings/Company/CompanyMasterPopup.aspx?HomeBase=' + document.getElementById('<%=tbIPHomebase.ClientID%>').value;
                }
                else if (radWin == "radAirportDeparturePopup") {
                    url = '../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbIPLDeparture.ClientID%>').value;
                }
                else if (radWin == "radAirportArrivePopup") {
                    url = '../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbIPLArrive.ClientID%>').value;
                }
                else if (radWin == "radTOBiasPopup") {
                    url = 'AirportBias.aspx?AirportID=' + document.getElementById('<%=hdnIPLDepartureID.ClientID%>').value;
                }
                else if (radWin == "radLandBiasPopup") {
                    url = 'AirportBias.aspx?AirportID=' + document.getElementById('<%=hdnIPLArriveID.ClientID%>').value;
                }
                else if (radWin == "radPowerPopup") {
                    url = 'PowerSettingPopup.aspx?AircraftID=' + document.getElementById('<%=hdnIPTypeCodeID.ClientID%>').value;
                }
                var oWnd = radopen(url, radWin);
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }



            //            function ShowReports(radWin, ReportFormat, ReportName) {
            //                document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
            //                document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
            //                if (ReportFormat == "EXPORT") {
            //                    url = "";
            //                    var oWnd = radopen(url, 'RadExportData');
            //                    return true;
            //                }
            //                else {
            //                    return true;
            //                }
            //            }
            function ShowReports(radWin, ReportFormat, ReportName) {
                document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
                document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
                if (ReportFormat == "EXPORT") {
                    url = "../../../Views/Reports/ExportReportInformation.aspx";
                    var oWnd = radopen(url, 'RadExportData');
                    return true;
                }
                else {
                    return true;
                }
            }

            function openReport(radWin, param) {
                var url = '';
                if (radWin == "ItineraryPlanning") {
                    url = '../../../../Views/Reports/ExportReportInformation.aspx?Report=RptUTItineraryPlanning&P1=' + document.getElementById('<%=tbIPFileNumber.ClientID%>').value;
                }

                var oWnd = radopen(url, "RadExportData");
            }

            function OnClientCloseTypeCodePopup(oWnd, args) {
                var combo = $find("<%= tbIPTypeCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbIPTypeCode.ClientID%>").value = arg.AircraftCD;
                        document.getElementById("<%=hdnIPTypeCodeID.ClientID%>").value = arg.AircraftID;
                        document.getElementById("<%=cvIPTypeCode.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbIPTypeCode.ClientID%>").value = "";
                        document.getElementById("<%=hdnIPTypeCodeID.ClientID%>").value = "";
                        document.getElementById("<%=cvIPTypeCode.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseTailNumberPopup(oWnd, args) {
                var combo = $find("<%= tbIPTailNumber.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbIPTypeCode.ClientID%>").value = arg.AirCraftTypeCode;
                        document.getElementById("<%=tbIPTailNumber.ClientID%>").value = arg.TailNum;
                        document.getElementById("<%=hdnIPTailNumber.ClientID%>").value = arg.FleetId;
                        document.getElementById("<%=cvIPTailNumber.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbIPTailNumber.ClientID%>").value = "";
                        document.getElementById("<%=hdnIPTailNumber.ClientID%>").value = "";
                        document.getElementById("<%=cvIPTailNumber.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseClientPopup(oWnd, args) {
                var combo = $find("<%= tbIPClientCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbIPClientCode.ClientID%>").value = htmlDecode(arg.ClientCD);
                        document.getElementById("<%=hdnIPClientID.ClientID%>").value = arg.ClientID;
                        document.getElementById("<%=cvIPClientCode.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbIPClientCode.ClientID%>").value = "";
                        document.getElementById("<%=hdnIPClientID.ClientID%>").value = "";
                        document.getElementById("<%=cvIPClientCode.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseHomebasePopup(oWnd, args) {
                var combo = $find("<%= tbIPHomebase.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbIPHomebase.ClientID%>").value = arg.HomeBase;
                        document.getElementById("<%=hdnIPHomebase.ClientID%>").value = arg.HomebaseID;
                        document.getElementById("<%=cvIPHomebase.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbIPHomebase.ClientID%>").value = "";
                        document.getElementById("<%=hdnIPHomebase.ClientID%>").value = "";
                        document.getElementById("<%=cvIPHomebase.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
                var step = "tbIPHomebase_OnTextChanged";
                var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                if (step) {
                    ajaxManager.ajaxRequest(step);
                }
            }

            function OnClientCloseDeparturePopup(oWnd, args) {
                var combo = $find("<%= tbIPLDeparture.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbIPLDeparture.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=hdnIPLDepartureID.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=cvIPLDeparture.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbIPLDeparture.ClientID%>").value = "";
                        document.getElementById("<%=hdnIPLDepartureID.ClientID%>").value = "";
                        document.getElementById("<%=cvIPLDeparture.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
                var step = "tbIPLArrive_OnTextChanged";
                var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                if (step) {
                    ajaxManager.ajaxRequest(step);
                }
            }

            function OnClientCloseArrivePopup(oWnd, args) {
                var combo = $find("<%= tbIPLArrive.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbIPLArrive.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=hdnIPLArriveID.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=cvIPLArrive.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbIPLArrive.ClientID%>").value = "";
                        document.getElementById("<%=hdnIPLArriveID.ClientID%>").value = "";
                        document.getElementById("<%=cvIPLArrive.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
                var step = "tbIPLDeparture_OnTextChanged";
                var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                if (step) {
                    ajaxManager.ajaxRequest(step);
                }
            }

            function OnClientCloseTOBiasPopup(oWnd, args) {
                var combo = $find("<%= tbIPLTOBias.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                    }
                    else {
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseLandBiasPopup(oWnd, args) {
                var combo = $find("<%= tbIPLLandBias.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                    }
                    else {
                        combo.clearSelection();
                    }
                }
            }
            function OnClientClosePowerPopup(oWnd, args) {
                var combo = $find("<%= tbIPLPower.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbIPLPower.ClientID%>").value = arg.PowerSettingID;
                        document.getElementById("<%=tbIPLTOBias.ClientID%>").value = arg.TakeOffBias;
                        document.getElementById("<%=tbIPLLandBias.ClientID%>").value = arg.LandingBias;
                        document.getElementById("<%=tbIPLTAS.ClientID%>").value = arg.TrueAirSpeed;
                    }
                    else {
                        combo.clearSelection();
                    }
                }
                var step = "tbIPLPower_OnTextChanged";
                var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                if (step) {
                    ajaxManager.ajaxRequest(step);
                }
            }

            function ItineraryPlancallBackFn(confirmed) {
                if (confirmed) {
                    var grid = $find(window['gridId1']);
                    grid.get_masterTableView().fireCommand("DeleteSelected");
                }
            }

            function ProcessUpdateItineraryPlan() {
                var grid = $find(window['gridId1']);
                if (grid.get_masterTableView().get_selectedItems().length == 0) {
                    radalert('Please select a record from the above table.', 330, 100, "Edit", "");
                    return false;
                }
            }

            function ProcessDeleteItineraryPlan(customMsg) {
                //get reference to the client grid object using the global window variable set in the user control's Page_Load server handler
                var grid = $find(window['gridId1']);
                var msg = 'Are you sure you want to delete this record?';
                if (customMsg != null) {
                    msg = customMsg;
                }
                if (grid.get_masterTableView().get_selectedItems().length > 0) {

                    radconfirm(msg, ItineraryPlancallBackFn, 330, 100, '', 'Delete');
                    return false;
                }
                else {
                    radalert('Please select a record from the above table.', 330, 100, "Delete", "");
                    return false;
                }
            }

            function ItineraryPlanLegscallBackFn(confirmed) {
                if (confirmed) {
                    var grid = $find(window['gridId2']);
                    grid.get_masterTableView().fireCommand("DeleteSelected");
                }
            }

            function ProcessInsertItineraryPlanLegs(Mode) {
                document.getElementById("<%=hdnSaveItineraryPlanLegs.ClientID %>").value = Mode;
            }

            function ProcessUpdateItineraryPlanLegs() {
                var grid = $find(window['gridId2']);
                if (grid.get_masterTableView().get_selectedItems().length == 0) {
                    radalert('Please select a record from the above table.', 330, 100, "Edit", "");
                    return false;
                }
            }

            function ProcessDeleteItineraryPlanLegs(customMsg) {
                //get reference to the client grid object using the global window variable set in the user control's Page_Load server handler
                var grid = $find(window['gridId2']);
                var msg = 'Are you sure you want to delete this record?';
                if (customMsg != null) {
                    msg = customMsg;
                }
                if (grid.get_masterTableView().get_selectedItems().length > 0) {

                    radconfirm(msg, ItineraryPlanLegscallBackFn, 330, 100, '', 'Delete');
                    return false;
                }
                else {
                    radalert(' Please select a record from the above table.', 330, 100, "Delete", "");
                    return false;
                }
            }

            function CheckPowerSetting(ctrl, args) {
                var ReturnValue = false;
                var PowerSettingCallBackFn = Function.createDelegate(ctrl, function (shouldSubmit) {
                    return ReturnValue;
                });
                if ((ctrl.value >= 1) && (ctrl.value <= 3)) {
                    ReturnValue = true;
                }
                else {
                    radalert("Power Setting must be '1', '2', or '3'.", 360, 50, "Itinerary Plan", PowerSettingCallBackFn);
                    ctrl.value = "1";
                    ReturnValue = false;
                }
                return ReturnValue;
            }

            function TransferCallBackfn() {
                return false;
            }

            function ShowAirportInfo() {
                var grid = $find("<%=dgItineraryPlanningLegs.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var MasterSelectedItem = MasterTable.get_selectedItems();
                if (MasterSelectedItem.length != 0) {
                    var AirportID = MasterSelectedItem[0].getDataKeyValue('AAirportID');
                    if (AirportID != "") {
                        url = "../Transactions/Preflight/PreflightAirportInfo.aspx?AirportID=" + AirportID;
                        window.radopen(url, "radAirportInfoPage");
                    }
                }
                return false;
            }
            function BiasValidation_onchange(sender, args) {
                var ReturnValue = true;
                var TitleName = "Itinerary Plan Legs";
                var BiasValue = sender.value;
                var CallBackFn = Function.createDelegate(sender, function (shouldSubmit) {
                    sender.focus();
                    return ReturnValue;
                });
                var SplitBias;
                if (document.getElementById("<%=hdnTenthMin.ClientID%>").value == "2") {
                    if (BiasValue != "") {
                        BiasValue = BiasValue.replace(".", ":");
                        sender.value = BiasValue.replace(".", ":");
                        if (BiasValue.indexOf(":") != -1) {
                            SplitBias = BiasValue.split(":");
                            if (SplitBias[1] != "") {
                                if ((parseInt(SplitBias[1]) >= 0) && (parseInt(SplitBias[1]) <= 59)) {
                                }
                                else {
                                    ReturnValue = false;
                                    radalert("You entered: " + SplitBias[1] + ". Minute entry must be between 0 and 59.", 360, 50, TitleName, CallBackFn);
                                    sender.value = 0 + ":00";
                                    args.cancel = true;
                                }
                            } else {
                                sender.value = SplitBias[0] + ":00";
                            }
                            if (SplitBias[1] == "") {
                                sender.value = BiasValue + "00";
                            }
                        }
                        else {
                            sender.value = BiasValue + ":00";
                        }
                    }
                }
                else {
                    if (BiasValue != "") {
                        BiasValue = BiasValue.replace(":", "");
                        sender.value = sender.value.replace(":", "");
                        if (BiasValue > 99) {
                            ReturnValue = false;
                            radalert("You entered: " + BiasValue + ". Invalid format. Required Format is NN.N", 360, 50, TitleName, CallBackFn);
                            sender.value = "0" + ".0";
                            args.cancel = true;
                        }
                        else {
                            if (BiasValue.indexOf(".") != -1) {
                                SplitBias = BiasValue.split(".");
                                if (SplitBias[0] != "") {
                                    if (SplitBias[0].length > 2) {
                                        ReturnValue = false;
                                        radalert("You entered: " + SplitBias[0] + ". Invalid format. Required Format is NN.N", 360, 50, TitleName, CallBackFn);
                                        sender.value = SplitBias[0] + ".0";
                                        args.cancel = true;
                                    }
                                }
                                if (SplitBias[1] != "") {
                                    if (SplitBias[1].length > 1) {
                                        ReturnValue = false;
                                        radalert("You entered: " + SplitBias[1] + ". Invalid format. Required Format is NN.N", 360, 50, TitleName, CallBackFn);
                                        sender.value = SplitBias[0] + ".0";
                                        args.cancel = true;
                                    }
                                    else {
                                        if ((parseInt(SplitBias[1]) >= 0) && (parseInt(SplitBias[1]) <= 9)) {
                                        }
                                        else {
                                            ReturnValue = false;
                                            radalert("You entered: " + SplitBias[1] + ". Invalid format. Required Format is NN.N", 360, 50, TitleName, CallBackFn);
                                            sender.value = SplitBias[0] + ".0";
                                            args.cancel = true;
                                        }
                                    }
                                } else {
                                    sender.value = SplitBias[0] + ".0";
                                }
                            }

                            else {
                                sender.value = BiasValue + ".0";
                            }
                        }
                    }
                }
                return ReturnValue;
            }
            function ValidateTimeFormat(sender, args, eventname) {
                var ReturnValue = true;
                var TitleName = "Itinerary Plan Legs";
                var CallBackFn = Function.createDelegate(sender, function (shouldSubmit) {
                    sender.focus();
                    return ReturnValue;
                });
                var Time = sender.value;
                var SplitTime;
                if (Time.indexOf(":") != -1) {
                    SplitTime = Time.split(":");
                    if (SplitTime[0].length == 2) {
                        if ((parseInt(SplitTime[0]) < 0) || (parseInt(SplitTime[0]) > 23)) {
                            ReturnValue = false;
                            radalert("You entered: " + SplitTime[0] + ". Invalid format. Hour must be between 0 and 23", 360, 50, TitleName, CallBackFn);
                            sender.value = "00:00";
                            args.cancel = true;
                            return false;
                        }
                    }
                    else {
                        ReturnValue = false;
                        radalert("You entered: " + SplitTime[0] + ". Invalid format. Required Format is HH:MM", 360, 50, TitleName, CallBackFn);
                        sender.value = "00:00";
                        args.cancel = true;
                        return false;
                    }
                    if (SplitTime[1].length == 2) {
                        if ((parseInt(SplitTime[1]) < 0) || (parseInt(SplitTime[1]) > 59)) {
                            ReturnValue = false;
                            radalert("You entered: " + SplitTime[1] + ". Invalid format. Hour must be between 0 and 59", 360, 50, TitleName, CallBackFn);
                            sender.value = "00:00";
                            args.cancel = true;
                            return false;
                        }
                    }
                    else {
                        ReturnValue = false;
                        radalert("You entered: " + SplitTime[0] + ". Invalid format. Required Format is HH:MM", 360, 50, TitleName, CallBackFn);
                        sender.value = "00:00";
                        args.cancel = true;
                        return false;
                    }
                }
                else {
                    if (Time.length == 2) {
                        if ((parseInt(Time) < 0) || (parseInt(Time) > 23)) {
                            ReturnValue = false;
                            radalert("You entered: " + Time + ". Invalid format. Hour must be between 0 and 23", 360, 50, TitleName, CallBackFn);
                            sender.value = "00:00";
                            args.cancel = true;
                            return false;
                        }
                        sender.value = Time + ":00";
                    }
                    else {
                        ReturnValue = false;
                        radalert("You entered: " + Time + ". Invalid format. Required Format is HH:MM", 360, 50, TitleName, CallBackFn);
                        sender.value = "00:00";
                        args.cancel = true;
                        return false;
                    }
                }
                if ((sender.value.length == 5) && (sender.value.indexOf(":") != -1)) {
                    var step = eventname;
                    var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                    if (step) {
                        ajaxManager.ajaxRequest(step);
                    }
                }
                return ReturnValue;
            }
            function SetTimeFormat(sender, args) {
                var ReturnValue = true;
                var TimeValue = sender.value;
                var SplitTime;
                //if ((event.keyCode == 8) || (event.keyCode == 46)) {
                TimeValue = TimeValue.replace(/^\s+|\s+$/g, '')
                if (TimeValue == "") {
                    sender.value = "00:00";
                }
                else {
                    SetTimeMask(args.keyCode, sender, "##:##", "");
                }
                //}                    
                return ReturnValue;
            }
            //onKeyUp="SetTimeFormat(event.keyCode, this, '##:##','');"
            function SetTimeMask(key, textbox, Mask, next) {
                // key == 9 ||
                if (key == 8 || key == 13 || (key >= 16 && key <= 18) || key == 20 || key == 27 || (key >= 33 && key <= 40) || key == 45 || key == 46 || key == 93 || key == 144 || key == 145)
                    return;
                Raw = '';
                for (i = 0; i < textbox.value.length; i++)
                    Raw += isNaN(parseInt(textbox.value.charAt(i))) ? '' : textbox.value.charAt(i);
                var Formatted = '';
                for (i = 0; i < Mask.length; i++) {
                    if (Mask.charAt(i) != '#')
                        Formatted += Mask.charAt(i);
                    else {
                        Formatted += Raw.charAt(0);
                        Raw = Raw.substring(1, Raw.length);
                    }
                    if ((Raw.length == 0) && (Mask.charAt(i + 1) == '#')) break;
                }
                textbox.value = Formatted;
                //                if (Formatted.length == Mask.length) {
                //                    document.getElementsByName(next)[0].focus();
                //                    document.getElementsByName(next)[0].select();
                //                }
            }
        </script>
        <script type="text/javascript">
            var currentTextBox = null;
            var currentDatePicker = null;

            //This method is called to handle the onclick and onfocus client side events for the texbox
            function showPopup(sender, e) {
                //this is a reference to the texbox which raised the event
                //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);



                //this gets a reference to the datepicker, which will be shown, to facilitate
                //the selection of a date
                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                //this variable is used to store a reference to the date picker, which is currently
                //active
                currentDatePicker = datePicker;

                //this method first parses the date, that the user entered or selected, and then
                //sets it as a selected date to the picker

                datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));


                //the code lines below show the calendar, which is used to select a date. The showPopup
                //function takes three arguments - the x and y coordinates where to show the calendar, as
                //well as its height, derived from the offsetHeight property of the textbox
                var position = datePicker.getElementPosition(currentTextBox);
                datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
            }

            //this handler is used to set the text of the TextBox to the value of selected from the popup
            function dateSelected(sender, args) {

                if (currentTextBox != null) {
                    var step = "Date_TextChanged";
                    if (currentTextBox.value != args.get_newValue()) {
                        currentTextBox.value = args.get_newValue();
                        var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                }
            }

            function hide() {


                //                if (control.value != "") {
                //                    if (control.value.length > 7) {
                //                        parseDate(control, event);
                //                    }
                //                    else {
                //                        alert("Invalid Date");
                //                        control.value = "";
                //                        return false;
                //                    }
                //                }
                var datePicker = $find("<%= RadDatePicker1.ClientID%>");
                datePicker.hidePopup();
                return true;
            }


            function tbDate_OnKeyDown(sender, event) {
                var step = "Date_TextChanged";
                if (event.keyCode == 9) {
                    parseDate(sender, event);
                    var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                    datePicker.hidePopup();
                    var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                    if (step) {
                        ajaxManager.ajaxRequest(step);
                    }
                    return true;
                }
            }

            //this function is used to parse the date entered or selected by the user
            function parseDate(sender, e) {
                if (currentDatePicker != null) {
                    var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                    var dateInput = currentDatePicker.get_dateInput();
                    var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                    sender.value = formattedDate;
                }
            }

            function ValidateDate(sender, args) {
                var MinDate = new Date("01/01/1900");
                var MaxDate = new Date("12/31/2100");
                var SelectedDate;
                var ReturnValue = false;
                var CallBackFn = Function.createDelegate(sender, function (shouldSubmit) {
                    sender.value = "";
                    sender.focus();
                    return ReturnValue;
                });
                if (sender.value != "") {
                    SelectedDate = new Date(sender.value);
                    var nYear = SelectedDate.getFullYear();
                    if ((SelectedDate < MinDate) || (SelectedDate > MaxDate)) {
                        ReturnValue = false;
                        radalert("Please enter / select Date between 01/01/1900 and 12/31/2100", 400, 100, "Date");
                        args.cancel = true;
                        sender.value = "";
                    }
                }
                return ReturnValue;
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableCdn="true">
    </telerik:RadScriptManager>
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                    ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="radTypeCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseTypeCodePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/AircraftPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radTailNumberPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseTailNumberPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radClientPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseClientPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radHomebasePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseHomebasePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportDeparturePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseDeparturePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportArrivePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseArrivePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radTOBiasPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseTOBiasPopup" AutoSize="false" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Utilities/AirportBias.aspx"
                Width="600px" Height="180px">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radLandBiasPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseLandBiasPopup" AutoSize="false" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Utilities/AirportBias.aspx"
                Width="600px" Height="180px">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPowerPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClosePowerPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Utilities/PowerSettingPopup.aspx"
                Height="190px" Width="400px" BackColor="#DADADA">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportInfoPage" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" AutoSize="true" Modal="true" ReloadOnShow="false" Behaviors="Close"
                VisibleStatusbar="false" Title="Airport Information">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadExportData" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Export Report Information" KeepInScreenBounds="true" Height="540px" Width="880px"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div style="margin: 0 auto; width: 980px;">
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left">
                                <div class="tab-nav-top">
                                    <span class="head-title">Itinerary Planning</span> <span class="tab-nav-icons">
                                        <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:ShowReports('','PDF','RptUTItineraryPlanning');"
                                            OnClick="btnShowReports_OnClick" title="Preview Report" class="search-icon"></asp:LinkButton>
                                        <asp:LinkButton ID="lbtnSaveReports" runat="server" title="Export Report" OnClientClick="javascript:openReport('ItineraryPlanning');return false;"
                                            class="save-icon"></asp:LinkButton><%--OnClientClick="javascript:ShowReports('','EXPORT','RptDBAccountsExport');return false;"--%>
                                        <asp:Button ID="btnShowReports" runat="server" CssClass="button-disable" Style="display: none;" />
                                        <a href="#" title="Help" class="help-icon"></a></span>
                                    <asp:HiddenField ID="hdnReportName" runat="server" />
                                    <asp:HiddenField ID="hdnReportFormat" runat="server" />
                                    <asp:HiddenField ID="hdnReportParameters" runat="server" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div id="DivExternalForm" runat="server" class="ExternalForm">
                        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" class="tdLabel160">
                                        <div id="tdSuccessMessage" class="success_msg">
                                            Record saved successfully.</div>
                                    </td>
                                    <td align="right">
                                        <div class="mandatory">
                                            <span>Bold</span> Indicates required field</div>
                                    </td>
                                </tr>
                            </table>
                            <telerik:RadPanelBar ID="RadPanelBarItineraryPlan" Width="100%" ExpandAnimation-Type="None"
                                CollapseAnimation-Type="none" runat="server">
                                <Items>
                                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Itinerary Planning" CssClass="PanelHeaderStyle">
                                        <Items>
                                            <telerik:RadPanelItem>
                                                <ContentTemplate>
                                                    <telerik:RadGrid ID="dgItineraryPlanning" runat="server" AllowSorting="true" AutoGenerateColumns="false"
                                                        OnPageIndexChanged="dgCustom_PageIndexChanged" AllowPaging="true" AllowFilteringByColumn="true"
                                                        PagerStyle-AlwaysVisible="true" OnNeedDataSource="dgItineraryPlanning_BindData"
                                                        CssClass="grid_auto" OnItemCommand="dgItineraryPlanning_ItemCommand" OnInsertCommand="dgItineraryPlanning_InsertCommand"
                                                        OnUpdateCommand="dgItineraryPlanning_UpdateCommand" OnSelectedIndexChanged="dgItineraryPlanning_SelectedIndexChanged"
                                                        OnDeleteCommand="dgItineraryPlanning_DeleteCommand" OnItemDataBound="dgItineraryPlanning_ItemDataBound">
                                                        <MasterTableView ClientDataKeyNames="ItineraryPlanID" DataKeyNames="ItineraryPlanID ,CustomerID ,IntinearyNUM ,ItineraryPlanQuarter ,ItineraryPlanQuarterYear 
                                                        ,AircraftID ,FleetID ,ItineraryPlanDescription ,ClientID ,HomebaseID ,LastUpdUID ,LastUpdTS ,IsDeleted , AircraftCD, AircraftDescription
                                                        ,TailNum , TypeDescription, ClientCD, ClientDescription, IcaoID, BaseDescription"
                                                            CommandItemDisplay="Bottom">
                                                            <Columns>
                                                                <telerik:GridBoundColumn DataField="IntinearyNUM" HeaderText="File No." UniqueName="IntinearyNUM"
                                                                    CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                    HeaderStyle-Width="100px" />
                                                                <telerik:GridBoundColumn DataField="ItineraryPlanQuarter" HeaderText="Wind Period"
                                                                    UniqueName="ItineraryPlanQuarter" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                                                    AutoPostBackOnFilter="true" HeaderStyle-Width="100px" />
                                                                <telerik:GridBoundColumn DataField="ItineraryPlanQuarterYear" HeaderText="Year" UniqueName="ItineraryPlanQuarterYear"
                                                                    CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                    HeaderStyle-Width="100px" />
                                                                <telerik:GridBoundColumn DataField="AircraftCD" HeaderText="Aircraft Type" UniqueName="AircraftCD"
                                                                    CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                    HeaderStyle-Width="100px" />
                                                                <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail No." UniqueName="TailNum"
                                                                    CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                    HeaderStyle-Width="100px" />
                                                                <telerik:GridBoundColumn DataField="ItineraryPlanDescription" HeaderText="Description"
                                                                    UniqueName="ItineraryPlanDescription" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                                                    AutoPostBackOnFilter="true" HeaderStyle-Width="100px" />
                                                            </Columns>
                                                            <CommandItemTemplate>
                                                                <div class="grid_icon">
                                                                    <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                                                        CommandName="InitInsert"> </asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdateItineraryPlan();"
                                                                        ToolTip="Edit" CssClass="edit-icon-grid" CommandName="Edit"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDeleteItineraryPlan();"
                                                                        CommandName="DeleteSelected" CssClass="delete-icon-grid" ToolTip="Delete"></asp:LinkButton>
                                                                </div>
                                                                <div>
                                                                    <asp:Label ID="lbLastUpdatedUser" runat="server" class="last-updated-text"></asp:Label>
                                                                </div>
                                                            </CommandItemTemplate>
                                                        </MasterTableView>
                                                        <ClientSettings EnablePostBackOnRowClick="true">
                                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                            <Selecting AllowRowSelect="true" />
                                                        </ClientSettings>
                                                        <GroupingSettings CaseSensitive="false" />
                                                    </telerik:RadGrid>
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <div class="tblspace_10">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table class="border-box">
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel100" valign="top">
                                                                            <span class="mnd_text">File No.</span>
                                                                        </td>
                                                                        <td class="tdLabel130" valign="top">
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbIPFileNumber" runat="server" CssClass="text80" MaxLength="6"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdnIPItineraryPlanID" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td class="tdLabel80" valign="top">
                                                                            <span>Type Code</span>
                                                                        </td>
                                                                        <td class="tdLabel130" valign="top">
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbIPTypeCode" runat="server" CssClass="text80" MaxLength="10" AutoPostBack="true"
                                                                                            OnTextChanged="tbIPTypeCode_OnTextChanged" TabIndex="2"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdnIPTypeCodeID" runat="server" />
                                                                                        <asp:Button ID="btnBrowseTypeCode" OnClientClick="javascript:openWin('radTypeCodePopup');return false;"
                                                                                            CssClass="browse-button" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <%--<asp:RequiredFieldValidator ID="rfvIPTypeCode" runat="server" ErrorMessage="Aircraft Type Code is Required."
                                                                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbIPTypeCode" ValidationGroup="SaveItineraryPlan">
                                                                                        </asp:RequiredFieldValidator>--%>
                                                                                        <asp:CustomValidator ID="cvIPTypeCode" runat="server" ControlToValidate="tbIPTypeCode"
                                                                                            ErrorMessage="Invalid Aircraft Type Code." Display="Dynamic" CssClass="alert-text"
                                                                                            ValidationGroup="SaveItineraryPlan" SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td class="tdLabel80" valign="top">
                                                                            <span>Tail No.</span>
                                                                        </td>
                                                                        <td class="tdLabel130" valign="top">
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbIPTailNumber" runat="server" CssClass="text80" MaxLength="6" AutoPostBack="true"
                                                                                            OnTextChanged="tbIPTailNumber_OnTextChanged" TabIndex="3"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdnIPTailNumber" runat="server" />
                                                                                        <asp:Button ID="btnIPTailNumber" OnClientClick="javascript:openWin('radTailNumberPopup');return false;"
                                                                                            CssClass="browse-button" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CustomValidator ID="cvIPTailNumber" runat="server" ControlToValidate="tbIPTailNumber"
                                                                                            ErrorMessage="Invalid Tail Number." Display="Dynamic" CssClass="alert-text" ValidationGroup="SaveItineraryPlan"
                                                                                            SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td class="tdLabel80" valign="top">
                                                                            <span>Wind Period</span>
                                                                        </td>
                                                                        <td valign="top">
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="ddlIPQuarter" runat="server" CssClass="text130" ClientIDMode="Static"
                                                                                            TabIndex="4">
                                                                                            <asp:ListItem Text="1st (Dec, Jan, Feb)" Value="1" Selected="True" />
                                                                                            <asp:ListItem Text="2nd (Mar, Apr, May)" Value="2" />
                                                                                            <asp:ListItem Text="3rd (Jun, Jul, Aug)" Value="3" />
                                                                                            <asp:ListItem Text="4th (Sep, Oct, Nov)" Value="4" />
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel100" valign="top">
                                                                            <span class="mnd_text">Year</span>
                                                                        </td>
                                                                        <td class="tdLabel130" valign="top">
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbIPYear" runat="server" CssClass="text80" MaxLength="4" TabIndex="1"
                                                                                            onKeyPress="return fnAllowNumeric(this, event);"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RequiredFieldValidator ID="rfvIPYear" runat="server" ErrorMessage="Year is Required.."
                                                                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbIPYear" ValidationGroup="SaveItineraryPlan">
                                                                                        </asp:RequiredFieldValidator>
                                                                                        <asp:RangeValidator ID="rngIPYear" runat="server" ControlToValidate="tbIPYear" Type="Integer"
                                                                                            MinimumValue="1900" MaximumValue="2100" ValidationGroup="SaveItineraryPlan" CssClass="alert-text"
                                                                                            SetFocusOnError="true" ErrorMessage="Year should be between 1900 and 2100." Display="Dynamic" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td class="tdLabel80" valign="top">
                                                                            <span>Client</span>
                                                                        </td>
                                                                        <td class="tdLabel130" valign="top">
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbIPClientCode" runat="server" CssClass="text80" MaxLength="5" AutoPostBack="true"
                                                                                            OnTextChanged="tbIPClientCode_OnTextChanged" TabIndex="5"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdnIPClientID" runat="server" />
                                                                                        <asp:Button ID="btnIPClientCode" OnClientClick="javascript:openWin('radClientPopup');return false;"
                                                                                            CssClass="browse-button" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CustomValidator ID="cvIPClientCode" runat="server" ControlToValidate="tbIPClientCode"
                                                                                            ErrorMessage="Invalid Client Code." Display="Dynamic" CssClass="alert-text" ValidationGroup="SaveItineraryPlan"
                                                                                            SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td class="tdLabel80" valign="top">
                                                                            <span>Home Base</span>
                                                                        </td>
                                                                        <td class="tdLabel130" valign="top">
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbIPHomebase" runat="server" CssClass="text80" MaxLength="6" AutoPostBack="true"
                                                                                            OnTextChanged="tbIPHomebase_OnTextChanged" TabIndex="6"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdnIPHomebase" runat="server" />
                                                                                        <asp:HiddenField ID="hdnIPHomebaseAirportID" runat="server" />
                                                                                        <asp:Button ID="btnIPHomebase" OnClientClick="javascript:openWin('radHomebasePopup');return false;"
                                                                                            CssClass="browse-button" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CustomValidator ID="cvIPHomebase" runat="server" ControlToValidate="tbIPHomebase"
                                                                                            ErrorMessage="Invalid Homebase." Display="Dynamic" CssClass="alert-text" ValidationGroup="SaveItineraryPlan"
                                                                                            SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td class="tdLabel80" valign="top">
                                                                            <span class="mnd_text">Description</span>
                                                                        </td>
                                                                        <td valign="top">
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="tbIPDescription" runat="server" Width="125px" MaxLength="40" TabIndex="7"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RequiredFieldValidator ID="rfvIPDescription" runat="server" ErrorMessage="Description Required.."
                                                                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbIPDescription" ValidationGroup="SaveItineraryPlan">
                                                                                        </asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <div class="tblspace_10">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </telerik:RadPanelItem>
                                        </Items>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                            <asp:Panel ID="pnlItenaryPlanDetail" runat="server">
                                <table cellspacing="0" cellpadding="0" class="tblButtonArea" style="display: none;">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSaveItineraryPlans" runat="server" CssClass="button" Text="Save"
                                                ValidationGroup="SaveItineraryPlan" OnClick="btnSaveItineraryPlans_OnClick" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCancelItineraryPlans" runat="server" CssClass="button" Text="Cancel"
                                                CausesValidation="false" OnClick="btnCancelItineraryPlans_OnClick" />
                                            <asp:HiddenField ID="hdnSaveItineraryPlans" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <telerik:RadPanelBar ID="pnlbarItineraryPlanLegs" Width="100%" ExpandAnimation-Type="None"
                                CollapseAnimation-Type="none" runat="server">
                                <Items>
                                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Itinerary Planning Legs"
                                        CssClass="PanelHeaderStyle">
                                        <Items>
                                            <telerik:RadPanelItem>
                                                <ContentTemplate>
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td valign="top">
                                                                <telerik:RadGrid ID="dgItineraryPlanningLegs" runat="server" AllowSorting="true"
                                                                    CssClass="grid_auto" OnPageIndexChanged="dgItineraryPlanningLegs_PageIndexChanged"
                                                                    AutoGenerateColumns="false" AllowPaging="false" AllowFilteringByColumn="true"
                                                                    PagerStyle-AlwaysVisible="true" Width="968px" OnNeedDataSource="dgItineraryPlanningLegs_BindData"
                                                                    OnItemCommand="dgItineraryPlanningLegs_ItemCommand" OnInsertCommand="dgItineraryPlanningLegs_InsertCommand"
                                                                    OnUpdateCommand="dgItineraryPlanningLegs_UpdateCommand" OnSelectedIndexChanged="dgItineraryPlanningLegs_SelectedIndexChanged"
                                                                    OnDeleteCommand="dgItineraryPlanningLegs_DeleteCommand" OnItemDataBound="dgItineraryPlanningLegs_ItemDataBound">
                                                                    <MasterTableView ClientDataKeyNames="ItineraryPlanDetailID,AAirportID" DataKeyNames="ItineraryPlanDetailID,CustomerID,ItineraryPlanID,IntinearyNUM,LegNUM,
                                                                                DAirportID,AAirportID,Distance,PowerSetting,TakeoffBIAS,LandingBias,TrueAirSpeed,WindsBoeingTable,ElapseTM,DepartureLocal,
                                                                                DepartureGMT,DepartureHome,ArrivalLocal,ArrivalGMT,ArrivalHome,IsDepartureConfirmed,IsArrivalConfirmation,PassengerNUM,
                                                                                Cost,WindReliability,LegID,LastUpdUID,LastUpdTS,IsDeleted,DAirportIcaoID,DAirportCityName,DAirportCountryName,
                                                                                AAirportIcaoID,AAirportCityName,AAirportCountryName,DAirportAlerts,AAirportAlerts"
                                                                        CommandItemDisplay="Bottom" AllowPaging="false" ShowFooter="false">
                                                                        <Columns>
                                                                            <telerik:GridBoundColumn DataField="LegNUM" HeaderText="Leg" UniqueName="LegNUM"
                                                                                CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                                HeaderStyle-Width="40px" FilterControlWidth="20px" />
                                                                            <telerik:GridBoundColumn DataField="DAirportIcaoID" HeaderText="Departure ICAO" UniqueName="DAirportIcaoID"
                                                                                CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                                HeaderStyle-Width="100px" />
                                                                            <telerik:GridBoundColumn DataField="AAirportIcaoID" HeaderText="Arrival ICAO" UniqueName="AAirportIcaoID"
                                                                                CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                                HeaderStyle-Width="100px" />
                                                                            <telerik:GridBoundColumn DataField="ElapseTM" HeaderText="ETE" UniqueName="ElapseTM"
                                                                                CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                                HeaderStyle-Width="80px" FilterControlWidth="60px" />
                                                                            <telerik:GridCheckBoxColumn DataField="IsDepartureConfirmed" HeaderText="TA" UniqueName="IsDepartureConfirmed"
                                                                                CurrentFilterFunction="NoFilter" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                                                                                HeaderStyle-Width="40px" AllowFiltering="false" />
                                                                            <telerik:GridBoundColumn DataField="DepartureLocal" HeaderText="Departure Date/Time"
                                                                                UniqueName="DepartureLocal" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                                                                AutoPostBackOnFilter="true" HeaderStyle-Width="120px" FilterControlWidth="100px" />
                                                                            <telerik:GridCheckBoxColumn DataField="IsArrivalConfirmation" HeaderText="TA" UniqueName="IsArrivalConfirmation"
                                                                                CurrentFilterFunction="NoFilter" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                                                                                HeaderStyle-Width="40px" AllowFiltering="false" />
                                                                            <telerik:GridBoundColumn DataField="ArrivalLocal" HeaderText="Arrival Date/Time"
                                                                                UniqueName="ArrivalLocal" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                                                                AutoPostBackOnFilter="true" HeaderStyle-Width="120px" FilterControlWidth="100px" />
                                                                            <telerik:GridBoundColumn DataField="PassengerNUM" HeaderText="PAX" UniqueName="PassengerNUM"
                                                                                CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                                HeaderStyle-Width="60px" FilterControlWidth="40px" />
                                                                            <telerik:GridBoundColumn DataField="Cost" HeaderText="Cost" UniqueName="Cost" CurrentFilterFunction="Contains"
                                                                                ShowFilterIcon="false" AutoPostBackOnFilter="true" HeaderStyle-Width="100px" />
                                                                        </Columns>
                                                                        <CommandItemTemplate>
                                                                            <div class="grid_icon">
                                                                                <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Insert" CssClass="insert-icon-grid"
                                                                                    CommandName="InitInsert" OnClientClick="javascript:return ProcessInsertItineraryPlanLegs('Insert');"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkInitAdd" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                                                                    CommandName="InitInsert" OnClientClick="javascript:return ProcessInsertItineraryPlanLegs('Add');"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdateItineraryPlanLegs();"
                                                                                    ToolTip="Edit" CssClass="edit-icon-grid" CommandName="Edit"></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDeleteItineraryPlanLegs();"
                                                                                    CommandName="DeleteSelected" CssClass="delete-icon-grid" ToolTip="Delete"></asp:LinkButton>
                                                                            </div>
                                                                            <div>
                                                                                <asp:Label ID="lbLastUpdatedUser" runat="server" class="last-updated-text"></asp:Label>
                                                                            </div>
                                                                        </CommandItemTemplate>
                                                                    </MasterTableView>
                                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                        <Selecting AllowRowSelect="true" />
                                                                    </ClientSettings>
                                                                    <GroupingSettings CaseSensitive="false" />
                                                                </telerik:RadGrid>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="tblspace_10">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table class="border-box">
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel100" valign="top">
                                                                                        <span class="mnd_text">Departure</span>
                                                                                    </td>
                                                                                    <td class="tdLabel130" valign="top">
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="tbIPLDeparture" runat="server" CssClass="text80" MaxLength="4" AutoPostBack="true"
                                                                                                        OnTextChanged="tbIPLDeparture_OnTextChanged"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hdnIPLDepartureID" runat="server" />
                                                                                                    <asp:Button ID="btnBrowseIPLDeparture" OnClientClick="javascript:openWin('radAirportDeparturePopup');return false;"
                                                                                                        CssClass="browse-button" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Label ID="lbAPLDepartDesc" CssClass="input_no_bg" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:RequiredFieldValidator ID="rfvIPLDeparture" runat="server" ErrorMessage="Departure Airport ICAO Id is Required."
                                                                                                        Display="Dynamic" CssClass="alert-text" ControlToValidate="tbIPLDeparture" ValidationGroup="SaveItineraryPlanLegs">
                                                                                                    </asp:RequiredFieldValidator>
                                                                                                    <asp:CustomValidator ID="cvIPLDeparture" runat="server" ControlToValidate="tbIPLDeparture"
                                                                                                        ErrorMessage="Invalid Departure Airport ICAO Id." Display="Dynamic" CssClass="alert-text"
                                                                                                        ValidationGroup="SaveItineraryPlanLegs" SetFocusOnError="true"></asp:CustomValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td class="tdLabel80" valign="top">
                                                                                        <span class="mnd_text">Arrival</span>
                                                                                    </td>
                                                                                    <td class="tdLabel130" valign="top">
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="tbIPLArrive" runat="server" CssClass="text80" MaxLength="4" AutoPostBack="true"
                                                                                                        OnTextChanged="tbIPLArrive_OnTextChanged"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hdnIPLArriveID" runat="server" />
                                                                                                    <asp:Button ID="btnBrowseIPLArrive" OnClientClick="javascript:openWin('radAirportArrivePopup');return false;"
                                                                                                        CssClass="browse-button" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Label ID="lbAPLAirriveDesc" CssClass="input_no_bg" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:RequiredFieldValidator ID="rfvIPLArrive" runat="server" ErrorMessage="Arrive Airport ICAO Id is Required."
                                                                                                        Display="Dynamic" CssClass="alert-text" ControlToValidate="tbIPLArrive" ValidationGroup="SaveItineraryPlanLegs">
                                                                                                    </asp:RequiredFieldValidator>
                                                                                                    <asp:CustomValidator ID="cvIPLArrive" runat="server" ControlToValidate="tbIPLArrive"
                                                                                                        ErrorMessage="Invalid Arrive Airport ICAO Id." Display="Dynamic" CssClass="alert-text"
                                                                                                        ValidationGroup="SaveItineraryPlanLegs" SetFocusOnError="true"></asp:CustomValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td class="tdLabel80" valign="top">
                                                                                        <span>PAX No.</span>
                                                                                    </td>
                                                                                    <td valign="top" class="tdLabel130">
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="tbIPLPaxNumber" runat="server" CssClass="text80" MaxLength="3" onKeyPress="return fnAllowNumeric(this, event);"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td class="tdLabel80" valign="top">
                                                                                        <span>Total Cost</span>
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <asp:TextBox ID="tbIPLTotalCost" runat="server" CssClass="text80" MaxLength="4"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="tblspace_10">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <fieldset>
                                                                                <legend>Distance & Time</legend>
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td valign="top" class="tdLabel90">
                                                                                                        <span>
                                                                                                            <asp:HiddenField ID="hdnIPLMiles" runat="server" />
                                                                                                            <asp:Label ID="lbIPLMiles" runat="server" Text="Miles(N)" /></span>
                                                                                                    </td>
                                                                                                    <td valign="top" class="tdLabel130">
                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="tbIPLMiles" runat="server" CssClass="text80" MaxLength="5" onKeyPress="return fnAllowNumeric(this, event);"
                                                                                                                        AutoPostBack="true" OnTextChanged="tbIPLMiles_OnTextChanged"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                    <td valign="top" class="tdLabel80">
                                                                                                        <span>T/O Bias</span>
                                                                                                    </td>
                                                                                                    <td valign="top" class="tdLabel130">
                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="tbIPLTOBias" runat="server" CssClass="text80" MaxLength="6" AutoPostBack="true"
                                                                                                                        onKeyPress="return fnAllowNumericAndChar(this, event, '.:');" onblur="javascript:BiasValidation_onchange(this, event);"
                                                                                                                        OnTextChanged="tbIPLTOBias_OnTextChanged"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:Button ID="imgbtnIPLTOBias" ToolTip="" runat="server" OnClientClick="javascript:openWin('radTOBiasPopup');return false;"
                                                                                                                        CssClass="calc-icon" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td colspan="2">
                                                                                                                    <asp:RegularExpressionValidator ID="regIPLTOBias" runat="server" ErrorMessage="Invalid Format"
                                                                                                                        Display="Dynamic" CssClass="alert-text" ControlToValidate="tbIPLTOBias" ValidationGroup="SaveItineraryPlanLegs"
                                                                                                                        ValidationExpression="^[0-9]{0,3}\.?[0-9]{0,2}$" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                    <td valign="top" class="tdLabel80">
                                                                                                        <span>TAS</span>
                                                                                                    </td>
                                                                                                    <td valign="top" class="tdLabel100">
                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="tbIPLTAS" runat="server" CssClass="text80" MaxLength="5" AutoPostBack="true"
                                                                                                                        onKeyPress="return fnAllowNumeric(this, event);" OnTextChanged="tbIPLTAS_OnTextChanged"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <span>Wind Reliability</span>
                                                                                                    </td>
                                                                                                    <td align="left">
                                                                                                        <asp:RadioButtonList ID="rblistIPLWindReliability" runat="server" RepeatDirection="Horizontal"
                                                                                                            AutoPostBack="true" OnSelectedIndexChanged="rblistIPLWindReliability_OnSelectedIndexChanged">
                                                                                                            <asp:ListItem Value="1">50%</asp:ListItem>
                                                                                                            <asp:ListItem Value="2">75%</asp:ListItem>
                                                                                                            <asp:ListItem Value="3" Selected="True">85%</asp:ListItem>
                                                                                                        </asp:RadioButtonList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                <tr>
                                                                                                    <td valign="top" class="tdLabel90">
                                                                                                        <span>Power</span>
                                                                                                    </td>
                                                                                                    <td valign="top" class="tdLabel130">
                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="tbIPLPower" runat="server" CssClass="text80" MaxLength="1" AutoPostBack="true"
                                                                                                                        onKeyPress="return fnAllowNumeric(this, event);" OnTextChanged="tbIPLPower_OnTextChanged"
                                                                                                                        onblur="return CheckPowerSetting(this, event);"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:Button ID="btnBrowseIPLPower" OnClientClick="javascript:openWin('radPowerPopup');return false;"
                                                                                                                        CssClass="browse-button" runat="server" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                    <td valign="top" class="tdLabel80">
                                                                                                        <span>Landing Bias</span>
                                                                                                    </td>
                                                                                                    <td valign="top" class="tdLabel130">
                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="tbIPLLandBias" runat="server" CssClass="text80" MaxLength="6" AutoPostBack="true"
                                                                                                                        onKeyPress="return fnAllowNumericAndChar(this, event, '.:');" onblur="javascript:BiasValidation_onchange(this, event);"
                                                                                                                        OnTextChanged="tbIPLLandBias_OnTextChanged"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:Button ID="btnBrowseIPLLandBias" ToolTip="" runat="server" OnClientClick="javascript:openWin('radLandBiasPopup');return false;"
                                                                                                                        CssClass="calc-icon" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td colspan="2">
                                                                                                                    <asp:RegularExpressionValidator ID="regIPLLandBias" runat="server" ErrorMessage="Invalid Format"
                                                                                                                        Display="Dynamic" CssClass="alert-text" ControlToValidate="tbIPLLandBias" ValidationGroup="SaveItineraryPlanLegs"
                                                                                                                        ValidationExpression="^[0-9]{0,3}\.?[0-9]{0,2}$" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                    <td valign="top" class="tdLabel80">
                                                                                                        Winds
                                                                                                    </td>
                                                                                                    <td valign="top" class="tdLabel100">
                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="tbIPLWinds" runat="server" CssClass="text80" MaxLength="4" onKeyPress="return fnAllowNumeric(this, event);"
                                                                                                                        AutoPostBack="true" OnTextChanged="rblistIPLWindReliability_OnSelectedIndexChanged"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                    <td class="tdLabel40" valign="top">
                                                                                                        <span>ETE</span>
                                                                                                    </td>
                                                                                                    <td class="tdLabel130" valign="top">
                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="tbIPLEte" runat="server" CssClass="text80" MaxLength="8" AutoPostBack="true"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                    <td class="tdLabel40" valign="top">
                                                                                                        <span>Cost</span>
                                                                                                    </td>
                                                                                                    <td valign="top">
                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="tbIPLCost" runat="server" CssClass="text80" MaxLength="10" AutoPostBack="true"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </fieldset>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table width="100%" style="height: 60px;">
                                                                                            <tr>
                                                                                                <td valign="bottom">
                                                                                                    Departure:
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td valign="middle">
                                                                                                    Arrival:
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td>
                                                                                        <fieldset>
                                                                                            <legend>Local</legend>
                                                                                            <table width="80%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbDepartLocalDate" CssClass="text70" OnTextChanged="tbDepartLocalDate_TextChanged"
                                                                                                            onkeydown="return tbDate_OnKeyDown(this, event);" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                            onclick="showPopup(this, event);" onchange="parseDate(this, event); ValidateDate(this, event);"
                                                                                                            runat="server"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbDepartLocalTime" runat="server" MaxLength="5" CssClass="text90"
                                                                                                            OnTextChanged="tbDepartLocalDate_TextChanged" AutoPostBack="true" onblur="ValidateTimeFormat(this,event,'tbDepartLocalDate_TextChanged')"
                                                                                                            onKeyPress="return fnAllowNumericAndChar(this, event,':')" onkeyup="return SetTimeFormat(this, event);"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <asp:HiddenField ID="hdnIsDepartureConfirmed" runat="server" />
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbArriveLocalDate" CssClass="text70" OnTextChanged="tbArriveLocalDate_TextChanged"
                                                                                                            onkeydown="return tbDate_OnKeyDown(this, event);" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                            onclick="showPopup(this, event);" onchange="parseDate(this, event); ValidateDate(this, event);"
                                                                                                            runat="server"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbArriveLocalTime" runat="server" MaxLength="5" CssClass="text90"
                                                                                                            OnTextChanged="tbArriveLocalDate_TextChanged" AutoPostBack="true" onblur="ValidateTimeFormat(this,event,'tbArriveLocalDate_TextChanged')"
                                                                                                            onKeyPress="return fnAllowNumericAndChar(this, event,':')" onkeyup="return SetTimeFormat(this, event);"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </fieldset>
                                                                                    </td>
                                                                                    <td>
                                                                                        <fieldset>
                                                                                            <legend>UTC</legend>
                                                                                            <table width="80%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbDepartUTCDate" CssClass="text70" OnTextChanged="tbDepartUTCDate_TextChanged"
                                                                                                            onkeydown="return tbDate_OnKeyDown(this, event);" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                            onclick="showPopup(this, event);" onchange="parseDate(this, event); ValidateDate(this, event);"
                                                                                                            runat="server"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbDepartUTCTime" runat="server" MaxLength="5" CssClass="text90"
                                                                                                            OnTextChanged="tbDepartUTCDate_TextChanged" AutoPostBack="true" onblur="ValidateTimeFormat(this,event,'tbDepartUTCDate_TextChanged')"
                                                                                                            onKeyPress="return fnAllowNumericAndChar(this, event,':')" onkeyup="return SetTimeFormat(this, event);"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbArriveUTCDate" CssClass="text70" OnTextChanged="tbArriveUTCDate_TextChanged"
                                                                                                            onkeydown="return tbDate_OnKeyDown(this, event);" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                            onclick="showPopup(this, event);" onchange="parseDate(this, event); ValidateDate(this, event);"
                                                                                                            runat="server"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbArriveUTCTime" runat="server" MaxLength="5" CssClass="text90"
                                                                                                            OnTextChanged="tbArriveUTCDate_TextChanged" AutoPostBack="true" onblur="ValidateTimeFormat(this,event,'tbArriveUTCDate_TextChanged')"
                                                                                                            onKeyPress="return fnAllowNumericAndChar(this, event,':')" onkeyup="return SetTimeFormat(this, event);"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </fieldset>
                                                                                    </td>
                                                                                    <td>
                                                                                        <fieldset>
                                                                                            <legend>Home</legend>
                                                                                            <table width="80%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbDepartHomeDate" CssClass="text70" OnTextChanged="tbDepartHomeDate_TextChanged"
                                                                                                            onkeydown="return tbDate_OnKeyDown(this, event);" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                            onclick="showPopup(this, event);" onchange="parseDate(this, event); ValidateDate(this, event);"
                                                                                                            runat="server"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbDepartHomeTime" runat="server" MaxLength="5" CssClass="text90"
                                                                                                            OnTextChanged="tbDepartHomeDate_TextChanged" AutoPostBack="true" onblur="ValidateTimeFormat(this,event,'tbDepartHomeDate_TextChanged')"
                                                                                                            onKeyPress="return fnAllowNumericAndChar(this, event,':')" onkeyup="return SetTimeFormat(this, event);"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbArriveHomeDate" CssClass="text70" OnTextChanged="tbArriveHomeDate_TextChanged"
                                                                                                            onkeydown="return tbDate_OnKeyDown(this, event);" AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                                                            onclick="showPopup(this, event);" onchange="parseDate(this, event); ValidateDate(this, event);"
                                                                                                            runat="server"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="tbArriveHomeTime" runat="server" MaxLength="5" CssClass="text90"
                                                                                                            OnTextChanged="tbArriveHomeDate_TextChanged" AutoPostBack="true" onblur="ValidateTimeFormat(this,event,'tbArriveHomeDate_TextChanged')"
                                                                                                            onKeyPress="return fnAllowNumericAndChar(this, event,':')" onkeyup="return SetTimeFormat(this, event);"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </fieldset>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0" class="tblButtonArea" width="100%">
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Button ID="btnIPLTransfer" CssClass="button" Text="Transfer" runat="server"
                                                                                OnClick="btnIPLTransfer_OnClick" />
                                                                            <asp:Button ID="btnIPLAlerts" CssClass="button" Text="Alerts" runat="server" OnClientClick="javascript:ShowAirportInfo(); return false;" />
                                                                        </td>
                                                                        <td align="right">
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Button ID="btnSaveAll" runat="server" CssClass="button" Text="Save" ValidationGroup="SaveItineraryPlan"
                                                                                            OnClick="btnSaveAll_OnClick" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Button ID="btnCancelAll" runat="server" CssClass="button" Text="Cancel" CausesValidation="false"
                                                                                            OnClick="btnCancelAll_OnClick" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td align="right" style="display: none;">
                                                                            <asp:Button ID="btnSaveItineraryPlanLegs" CssClass="button" Text="Save" runat="server"
                                                                                ValidationGroup="SaveItineraryPlanLegs" OnClick="btnSaveItineraryPlanLegs_OnClick" />
                                                                            <asp:Button ID="btnCancelItineraryPlanLegs" Text="Cancel" CssClass="button" runat="server"
                                                                                CausesValidation="false" OnClick="btnCancelItineraryPlanLegs_OnClick" />
                                                                            <asp:HiddenField ID="hdnSaveItineraryPlanLegs" runat="server" />
                                                                            <asp:HiddenField ID="hdnItineraryPlanLegsID" runat="server" />
                                                                            <asp:HiddenField ID="hdnTenthMin" runat="server" />
                                                                            <asp:HiddenField ID="hdnInsertLegNum" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </telerik:RadPanelItem>
                                        </Items>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                        </asp:Panel>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
