﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Web.Views.Utilities
{
    public partial class PowerSettingPopup : BaseSecuredPage
    {
        private ExceptionManager exManager;
        private Int64 AircraftID;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }

        /// <summary>
        /// Method to Bind Grid Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgAircraftCatalog_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Request.QueryString["AircraftID"] != null)
                        {
                            AircraftID = Convert.ToInt64(Request.QueryString["AircraftID"].ToString());
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var ObjRetVal = ObjService.GetAircraftByAircraftID(AircraftID);
                                if (ObjRetVal.ReturnFlag == true)
                                {
                                    dgAircraftCatalog.DataSource = GirdBindPowerSettings(ObjRetVal.EntityList);
                                }

                            }
                        }                        
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Aircraft);
                }
            }
        }
        
        private DataTable GirdBindPowerSettings(List<Aircraft> AircraftList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AircraftList))
            {
                DataTable dtAircraft = new DataTable();
                if (AircraftList.Count != 0)
                {
                    lblAircrafCDtValue.Text = System.Web.HttpUtility.HtmlEncode(AircraftList[0].AircraftCD);
                    dtAircraft.Clear();
                    dtAircraft.Columns.Add("PowerSettingID");
                    dtAircraft.Columns.Add("PowerSettings");
                    dtAircraft.Columns.Add("TrueAirSpeed");
                    dtAircraft.Columns.Add("HourRange");
                    dtAircraft.Columns.Add("TakeOffBias");
                    dtAircraft.Columns.Add("LandingBias");
                    for (int index = 0; index < 3; index++)
                    {
                        DataRow drAircraft = dtAircraft.NewRow();
                        drAircraft["PowerSettingID"] = index + 1;
                        if (index == 0)
                        {
                            drAircraft["PowerSettings"] = AircraftList[0].PowerSettings1Description;
                            drAircraft["TrueAirSpeed"] = AircraftList[0].PowerSettings1TrueAirSpeed;
                            drAircraft["HourRange"] = AircraftList[0].PowerSettings1HourRange;
                            drAircraft["TakeOffBias"] = AircraftList[0].PowerSettings1TakeOffBias;
                            drAircraft["LandingBias"] = AircraftList[0].PowerSettings1LandingBias;
                        }
                        else if (index == 1)
                        {
                            drAircraft["PowerSettings"] = AircraftList[0].PowerSettings2Description;
                            drAircraft["TrueAirSpeed"] = AircraftList[0].PowerSettings2TrueAirSpeed;
                            drAircraft["HourRange"] = AircraftList[0].PowerSettings2HourRange;
                            drAircraft["TakeOffBias"] = AircraftList[0].PowerSettings2TakeOffBias;
                            drAircraft["LandingBias"] = AircraftList[0].PowerSettings2LandingBias;
                        }
                        else if (index == 2)
                        {
                            drAircraft["PowerSettings"] = AircraftList[0].PowerSettings3Description;
                            drAircraft["TrueAirSpeed"] = AircraftList[0].PowerSettings3TrueAirSpeed;
                            drAircraft["HourRange"] = AircraftList[0].PowerSettings3HourRange;
                            drAircraft["TakeOffBias"] = AircraftList[0].PowerSettings3TakeOffBias;
                            drAircraft["LandingBias"] = AircraftList[0].PowerSettings3LandingBias;
                        }
                        dtAircraft.Rows.Add(drAircraft);
                    }
                }
                return dtAircraft;
            }
        }
        
    }
}
