﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using Telerik.Web.UI;
using System.IO;
using System.Data;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Text.RegularExpressions;
using FlightPak.Web.Framework.Prinicipal;
#endregion

namespace FlightPak.Web.Views.Utilities
{
    public partial class UVTripPlanner : BaseSecuredPage
    {
        private ExceptionManager exManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckAutorization(Permission.Utilities.ViewUVTripPlanner);
        }

        protected void chkDisplayUVMaintained_CheckedChanged(object sender, EventArgs e)
        {
            DoSearchfilter(true);
        }

        protected void RetrieveSearch_Click(object sender, EventArgs e)
        {

        }

        protected void MetroCity_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgAirport.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }

        //protected void lnkIcaoID_Click(object sender, EventArgs e)
        //{
        //    if (((LinkButton)sender) != null)
        //    {
        //        string TextboxID = ((LinkButton)sender).ID;
                
        //        string Url = "https://www.google.com";
        //        //string Url = "https://secure.universalweather.com/regusers/publictools/uvtripplanner/index.html?id=10d70a06-83ee-11dc-8314-0800200c9a66&icao=" + ((LinkButton)sender).Text;
        //        Response.Write("<script type='text/javascript'>window.open('" + Url + "','_blank');</script>");
        //    }
        //}

        protected void dgAirport_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //if (e.Item is GridDataItem)
            //{
            //    GridDataItem item = (GridDataItem)e.Item;
            //    string val1 = item.GetDataKeyValue("IcaoID").ToString();
            //    HyperLink hLink = (HyperLink)item["IcaoID"].Controls[0];
            //    hLink.NavigateUrl = "https://secure.universalweather.com/regusers/publictools/uvtripplanner/index.html?id=10d70a06-83ee-11dc-8314-0800200c9a66&icao=" + val1 ;
            //    hLink.ForeColor = System.Drawing.Color.DarkBlue;
                
            //}
        } 
        
        protected void chkDisplayInactive_CheckedChanged(object sender, EventArgs e)
        {
            DoSearchfilter(true);
        }
        
        
        protected void dgAirport_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPakMasterService.MasterCatalogServiceClient AirportService = new FlightPakMasterService.MasterCatalogServiceClient();

                        var ReturnValue = AirportService.GetAllAirportListForGrid(false);

                        if (ReturnValue.ReturnFlag == true)
                        {

                            dgAirport.DataSource = ReturnValue.EntityList;
                            Session["AirportGridPopUp"] = ReturnValue.EntityList;
                        }
                        DoSearchfilter(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Airport);
                }
            }

        }

        public bool DoSearchfilter(bool IsDatabind)
        {

            List<GetAllAirportListForGrid> FilterCrew = new List<GetAllAirportListForGrid>();
            FilterCrew = (List<GetAllAirportListForGrid>)Session["AirportGridPopUp"];

            if (chkDisplayUVMaintained.Checked)
            {
                FilterCrew = FilterCrew.Where(x => (x.Filter != null) && (x.Filter.ToString().ToUpper() == "UWA")).ToList();
            }
            if (chkDisplayInactive.Checked)
            {
                FilterCrew = FilterCrew.Where(x => (x.IsInActive == false)).ToList();
            }
            dgAirport.DataSource = FilterCrew;
            if (IsDatabind)
            {
                dgAirport.DataBind();
            }

            return false;
        }
    }
}