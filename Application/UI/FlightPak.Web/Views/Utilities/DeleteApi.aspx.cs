﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Configuration;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.Views.Utilities
{
    public partial class DeleteApi : System.Web.UI.Page
    {
        public readonly string strApiUrl = "";
        readonly string result;
        private ExceptionManager exManager;

        public DeleteApi()
        {
            String coreApiUrl = ConfigurationManager.AppSettings["CoreApiServer"];
            if (String.IsNullOrEmpty(coreApiUrl))
                coreApiUrl = "http://core-dev.universalweather.rdn/";

            strApiUrl = string.Format("{0}/api/v1/{1}", coreApiUrl, "{0}/{1}{2}");
        }

        public DeleteApi(string dataObject)
        {
            result = dataObject;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            exManager.Process(() =>
            {
                DeleteApi deleteApiObj= default(DeleteApi);
                String apiType = this.Request["apiType"];
                String apiMethod = this.Request["method"];
                StringBuilder sbParams = new StringBuilder();
                sbParams.Append("?");
                foreach (String key in Request.QueryString)
                {
                    if (key.ToLower() != "apitype" && key.ToLower() != "method")
                        sbParams.AppendFormat("{0}={1}&", key, Request.Params[key]);
                }

                String queryStringParams = sbParams.ToString();
                if (queryStringParams.EndsWith("&"))
                    queryStringParams = queryStringParams.Remove(queryStringParams.Length - 1);

                String strUrl = "";
                if (!string.IsNullOrEmpty(strApiUrl) && !string.IsNullOrWhiteSpace(strApiUrl))
                {
                    strUrl = String.Format(strApiUrl, apiType, apiMethod, queryStringParams);
                }
                String accessToken = FlightPak.Web.Framework.Helpers.CoreApiManager.GetApiAccessToken();
                try
                {
                    using (var webClient = new WebClient())
                    {
                        webClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                        webClient.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + accessToken);

                        if (!string.IsNullOrEmpty(strUrl) && !string.IsNullOrWhiteSpace(strUrl))
                        {
                            var response = webClient.UploadValues(strUrl, "DELETE", new NameValueCollection());
                            deleteApiObj = new DeleteApi(Encoding.UTF8.GetString(response));
                        }
                    }
                }

                catch (WebException ex)
                {
                    deleteApiObj = new DeleteApi(((HttpWebResponse)ex.Response).StatusCode.ToString());
                    if (((System.Net.HttpWebResponse)(ex.Response)).StatusCode == HttpStatusCode.Unauthorized)
                    {
                        Dictionary<string, object> result = new Dictionary<string, object>();
                        result.Add("StatusCode", 401);
                        deleteApiObj = new DeleteApi(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                        Response.ContentType = "text/html";
                    }                    
                    Logger.Write(String.Format("Url:{0}-Token:{1}-Exception:{2}", Microsoft.Security.Application.Encoder.HtmlEncode(strUrl), Microsoft.Security.Application.Encoder.HtmlEncode(accessToken), Microsoft.Security.Application.Encoder.HtmlEncode(ex.ToString())), WebConstants.FlightPakException);                  
                }
                catch (Exception ex)
                {
                    deleteApiObj = new DeleteApi(string.Format("Failed to execute api {0} for {1}", apiMethod, apiType));
                    Logger.Write(String.Format("Url:{0}-Token:{1}-Exception:{2}", Microsoft.Security.Application.Encoder.HtmlEncode(strUrl), Microsoft.Security.Application.Encoder.HtmlEncode(accessToken), Microsoft.Security.Application.Encoder.HtmlEncode(ex.ToString())), WebConstants.FlightPakException);
                }

                Response.Write(Microsoft.Security.Application.Encoder.HtmlEncode(deleteApiObj.result));
                Response.End();
            }, FlightPak.Common.Constants.Policy.UILayer);
        }
    }
}