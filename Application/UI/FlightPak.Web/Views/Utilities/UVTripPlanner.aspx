﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UVTripPlanner.aspx.cs"
    EnableEventValidation="true" Inherits="FlightPak.Web.Views.Utilities.UVTripPlanner" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>UVTripPlanner</title>
    <style type="text/css">
        .rgAltRow, .rgRow
        {
            cursor: hand !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="radScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript" src="../../../Scripts/Common.js"></script>
        <script type="text/javascript">

            function openWin() {

                var grid = $find("<%=dgAirport.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell = MasterTable.getCellByColumnUniqueName(row, "IcaoId")
                    //here cell.innerHTML holds the value of the cell    
                }
                var url = '<% =ResolveClientUrl("https://www.google.com") %>';
                window.open(url, 'UVTripPlanner', 'width=750,height=565,toolbar=no, location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes,top=' + (screen.height - 565) / 2 + ',left=' + (screen.width - 750) / 2);
                return false;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgAirport.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "IcaoID");
                }
                var URL = 'https://secure.universalweather.com/regusers/publictools/uvtripplanner/index.html?id=10d70a06-83ee-11dc-8314-0800200c9a66&icao=' + cell1.innerHTML;

                window.open(URL, '_blank');
            }


            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadCountryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dgAirport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="chkDisplayUVMaintained">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgAirport" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="chkDisplayInactive">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgAirport" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <%-- <telerik:RadTabStrip ID="rtPreflightAPIS" runat="server" Skin="Simple" ReorderTabsOnSelect="true"
                                    AutoPostBack="true" MultiPageID="RadMultiPage1" SelectedIndex="0" Align="Justify"
                                    Style="float: inherit">
                                    <Tabs>
                                        <telerik:RadTab Value="Search" Text="FlightPak Airports" Selected="true">
                                        </telerik:RadTab>
                                    </Tabs>
                                </telerik:RadTabStrip>--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0">
                        <telerik:RadPageView ID="RadPageView1" runat="server">
                            <telerik:RadPanelItem Value="Check">
                                <ContentTemplate>
                                    <table class="border-box">
                                        <tr>
                                            <td valign="top" width="50%">
                                                <table>
                                                    <tr>
                                                        <td class="tdLabel210">
                                                            <asp:CheckBox ID="chkDisplayUVMaintained" Text="Display UV Maintained Only" AutoPostBack="true"
                                                                OnCheckedChanged="chkDisplayUVMaintained_CheckedChanged" runat="server" />
                                                        </td>
                                                        <td class="tdLabel125">
                                                            <asp:CheckBox ID="chkDisplayInactive" Text="Display Inactive" AutoPostBack="true"
                                                                OnCheckedChanged="chkDisplayInactive_CheckedChanged" runat="server" />
                                                        </td>
                                                        <td width="10%">
                                                            <%--<asp:Button ID="btnSearch" Style="display: none" runat="server" Text="Search" CssClass="button"
                                                                 OnClick="RetrieveSearch_Click" />--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="preflight-apis">
                                                <telerik:RadGrid ID="dgAirport" runat="server" AllowMultiRowSelection="true" AllowSorting="true"
                                                    OnPageIndexChanged="MetroCity_PageIndexChanged" OnNeedDataSource="dgAirport_BindData"
                                                    AutoGenerateColumns="false" Height="341px" OnItemDataBound="dgAirport_ItemDataBound"
                                                    AllowPaging="true" Width="900px">
                                                    <MasterTableView DataKeyNames="AirportID,CustomerID,IcaoID,CityName,StateName,CountryID,CountryCD,OffsetToGMT,IsEntryPort,AirportName,IsInActive,LastUpdUID,LastUpdTS,IsWorldClock,UWAID,Iata,MaxRunway"
                                                        CommandItemDisplay="Bottom" ClientDataKeyNames="IsWorldClock,UWAID">
                                                        <Columns>
                                                            <%--<telerik:GridHyperLinkColumn Target="_new" HeaderText="ICAO" DataTextField="IcaoID"
                                                                ItemStyle-Font-Underline="true" ItemStyle-ForeColor="DarkBlue" ShowFilterIcon="false"
                                                                CurrentFilterFunction="Contains" HeaderStyle-Width="100px" AutoPostBackOnFilter="true"
                                                                UniqueName="IcaoID">
                                                            </telerik:GridHyperLinkColumn>--%>
                                                            <telerik:GridBoundColumn DataField="IcaoID" HeaderText="ICAO" AutoPostBackOnFilter="true"
                                                                HeaderStyle-Width="100px" FilterControlWidth="80px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Iata" HeaderText="IATA" AutoPostBackOnFilter="true"
                                                                HeaderStyle-Width="100px" FilterControlWidth="80px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="AirportName" HeaderText="Airport Name" AutoPostBackOnFilter="true"
                                                                HeaderStyle-Width="220px" FilterControlWidth="200px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="CityName" HeaderText="City" AutoPostBackOnFilter="true"
                                                                HeaderStyle-Width="190px" FilterControlWidth="170px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="StateName" HeaderText="State" AutoPostBackOnFilter="true"
                                                                HeaderStyle-Width="190px" FilterControlWidth="170px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="CountryCD" HeaderText="Country" AutoPostBackOnFilter="true"
                                                                HeaderStyle-Width="100px" FilterControlWidth="80px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                                                            </telerik:GridBoundColumn>
                                                        </Columns>
                                                    </MasterTableView>
                                                    <ClientSettings EnablePostBackOnRowClick="false">
                                                        <ClientEvents OnRowDblClick="returnToParent" />
                                                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                        <Selecting AllowRowSelect="true" />
                                                    </ClientSettings>
                                                    <GroupingSettings CaseSensitive="false" />
                                                </telerik:RadGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </telerik:RadPageView>
                    </telerik:RadMultiPage>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
