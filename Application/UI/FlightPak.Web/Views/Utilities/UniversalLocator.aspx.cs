﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//Included
using Telerik.Web.UI;
using System.Globalization;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Web.Views.Utilities
{
    public partial class UniversalLocator : BaseSecuredPage
    {
        private ExceptionManager exManager;
        bool Count_Check = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        this.dgHotel.MasterTableView.AllowMultiColumnSorting = false;
                        this.dgAirportLocator.MasterTableView.AllowMultiColumnSorting = false;
                        this.dgVendors.MasterTableView.AllowMultiColumnSorting = false;
                        this.dgCustom.MasterTableView.AllowMultiColumnSorting = false;
                        if (!IsPostBack)
                        {
                            //To disable Report icons on page load
                            EnableReportIcon(false);

                            ApplyPermissions();
                            EmptyGrid();
                            MapVisibility();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
            }
        }

        protected void MetroCity_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgAirportLocator.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }

        protected void dgHotel_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgHotel.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }

        protected void dgVendors_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgVendors.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }

        protected void dgCustom_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCustom.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }


        /// <summary>
        /// To Enable reports icon after every search
        /// </summary>
        private void EnableReportIcon(bool Enable)
        {
            if (Enable == true)
            {
                lbtnReports.Enabled = Enable;
                lbtnReports.OnClientClick = "javascript:ShowReports('','PDF');";
                lbtnReports.CssClass = "search-icon";
                lbtnReports.Style.Add("cursor", "pointer");

                lbtnSaveReports.Enabled = Enable;
                //lbtnSaveReports.OnClientClick = "javascript:ShowReports('','Excel');";
                lbtnSaveReports.OnClientClick = "javascript:openReport('Locator');return false;";
                lbtnSaveReports.CssClass = "save-icon";
                lbtnSaveReports.Style.Add("cursor", "pointer");
            }
            else
            {
                //To disable Report icons on page load
                lbtnReports.Enabled = Enable;
                lbtnReports.OnClientClick = "";
                lbtnReports.CssClass = "search-icon";
                lbtnReports.Style.Add("cursor", "default");

                lbtnSaveReports.Enabled = Enable;
                lbtnSaveReports.OnClientClick = "";
                lbtnSaveReports.CssClass = "save-icon";
                lbtnSaveReports.Style.Add("cursor", "default");
            }
        }

        /// <summary>
        /// To Refresh the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //To refresh the form
                        e.Updated = DivExternalForm;
                        e.Updated = divIcons;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
            }
        }

        public void MapVisibility()
        {
            if (RadTabLocator.SelectedTab.Value == "1")
            {
                if (dgAirportLocator.Items.Count > 0 && dgAirportLocator.SelectedItems.Count > 0)
                {
                    lnkMapQuest.Enabled = true;
                    lnkMapQuest.OnClientClick = "javascript:LinkToMapQuest();";
                }
                else
                {
                    lnkMapQuest.Enabled = false;
                    lnkMapQuest.OnClientClick = "";
                }
            }
            if (RadTabLocator.SelectedTab.Value == "2")
            {
                if (dgHotel.Items.Count > 0 && dgHotel.SelectedItems.Count > 0)
                {
                    lnkMapQuest.Enabled = true;
                    lnkMapQuest.OnClientClick = "javascript:LinkToMapQuest();";
                }
                else
                {
                    lnkMapQuest.Enabled = false;
                    lnkMapQuest.OnClientClick = "";
                }
            }
            if (RadTabLocator.SelectedTab.Value == "3")
            {
                if (dgVendors.Items.Count > 0 && dgVendors.SelectedItems.Count > 0)
                {
                    lnkMapQuest.Enabled = true;
                    lnkMapQuest.OnClientClick = "javascript:LinkToMapQuest();";
                }
                else
                {
                    lnkMapQuest.Enabled = false;
                    lnkMapQuest.OnClientClick = "";
                }
            }
            if (RadTabLocator.SelectedTab.Value == "4")
            {
                if (dgCustom.Items.Count > 0 && dgCustom.SelectedItems.Count > 0)
                {
                    lnkMapQuest.Enabled = true;
                    lnkMapQuest.OnClientClick = "javascript:LinkToMapQuest();";
                }
                else
                {
                    lnkMapQuest.Enabled = false;
                    lnkMapQuest.OnClientClick = "";
                }
            }
        }

        protected void rtCorpRequest_Clicked(object sender, RadTabStripEventArgs e)
        {
            MapVisibility();
            ClearForm();
        }

        #region "Reports"
        /// <summary>
        /// To show reports
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["FORMAT"] = hdnReportFormat.Value;

                        if (RadTabLocator.SelectedTab.Value == "1")
                        {
                            //hdnReportName.Value = "RptLocatorAirport";
                            // Int64 AirportId = 0;

                            // if (!string.IsNullOrEmpty(hdnAirportICAOID.Value))
                            // {
                            //     AirportId = Convert.ToInt64(hdnAirportICAOID.Value);
                            //}
                            //if ((AirportId == 0) && (!string.IsNullOrEmpty(hdnIata.Value)))
                            //{
                            //    AirportId = Convert.ToInt64(hdnIata.Value);
                            //}

                            string AirportId = "";
                            if (!string.IsNullOrEmpty(hdnAirportICAOID.Value))
                            {
                                AirportId = hdnAirportICAOID.Value;
                            }
                            if ((AirportId == "") && (!string.IsNullOrEmpty(hdnIata.Value)))
                            {
                                AirportId = hdnIata.Value;
                            }
                            if (tbRunway.Text == "")
                            {
                                tbRunway.Text = "0";
                            }
                            if (tbMiles.Text == "")
                            {
                                tbMiles.Text = "0";
                            }
                            if (hdnCountry.Value == "")
                            {
                                hdnCountry.Value = "0";
                            }

                            Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";IcaoID=" + tbAirportICAO.Text + ";CountryCD=" + tbCountry.Text + ";Runway=" + Convert.ToInt32(tbRunway.Text) + ";CityName=" + tbCity.Text + ";MetroCD=" + tbMetroCD.Text + ";IATA=" + tbIata.Text + ";MilesFrom=" + Convert.ToInt32(tbMiles.Text) + ";StateName=" + tbState.Text + ";AirportName=" + tbName.Text + ";IsHeliport=" + chkHeliport.Checked + ";IsEntryPort=" + chkApofEntry.Checked + ";IsInActive=" + chkIncludeInactivateAirport.Checked + ";CountryID=" + hdnCountry.Value + ";AirportID=" + AirportId;
                        }
                        else if (RadTabLocator.SelectedTab.Value == "2")
                        {
                            //hdnReportName.Value = "RptLocatorHotel";
                            //Int64 AirportId = 0;
                            //if (!string.IsNullOrEmpty(hdnHotelAirportIcao.Value))
                            //{
                            //    AirportId = Convert.ToInt64(hdnHotelAirportIcao.Value);
                            //}
                            //if ((AirportId == 0) && (!string.IsNullOrEmpty(hdnHotelIata.Value)))
                            //{
                            //    AirportId = Convert.ToInt64(hdnHotelIata.Value);
                            //}
                            string AirportId = "";
                            if (!string.IsNullOrEmpty(hdnHotelAirportIcao.Value))
                            {
                                AirportId = hdnHotelAirportIcao.Value;
                            }
                            if ((AirportId == "") && (!string.IsNullOrEmpty(hdnHotelIata.Value)))
                            {
                                AirportId = hdnHotelIata.Value;
                            }
                            if (tbHotelMilesFrom.Text == "")
                            {
                                tbHotelMilesFrom.Text = "0";
                            }
                            if (hdnHotelCountry.Value == "")
                            {
                                hdnHotelCountry.Value = "0";
                            }
                            Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";IcaoID=" + tbHotelAirportIcao.Text + ";CountryCD=" + tbHotelCountry.Text + ";MilesFrom=" + Convert.ToInt32(tbHotelMilesFrom.Text) + ";CityName=" + tbHotelCity.Text + ";MetroCD=" + tbHotelMetro.Text + ";IATA=" + tbHotelIata.Text + ";StateName=" + tbHotelState.Text + ";AirportName=" + tbHotelName.Text + ";CountryID=" + hdnHotelCountry.Value + ";AirportID=" + AirportId;
                        }
                        else if (RadTabLocator.SelectedTab.Value == "3")
                        {
                            //hdnReportName.Value = "RptLocatorVendor";

                            if (tbVendorMiles.Text == "")
                            {
                                tbVendorMiles.Text = "0";
                            }
                            //Int64 AirportId = 0;
                            //if (!string.IsNullOrEmpty(hdnVendorIcao.Value))
                            //{
                            //    AirportId = Convert.ToInt64(hdnVendorIcao.Value);
                            //}
                            //if ((AirportId == 0) && (!string.IsNullOrEmpty(hdnVendorIata.Value)))
                            //{
                            //    AirportId = Convert.ToInt64(hdnVendorIata.Value);
                            //}

                            string AirportId = "";
                            if (!string.IsNullOrEmpty(hdnVendorIcao.Value))
                            {
                                AirportId = hdnVendorIcao.Value;
                            }
                            if ((AirportId == "") && (!string.IsNullOrEmpty(hdnVendorIata.Value)))
                            {
                                AirportId = hdnVendorIata.Value;
                            }
                            Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";IcaoID=" + tbVendorAirportIcao.Text + ";CountryCD=" + tbVendorCountry.Text + ";MilesFrom=" + Convert.ToInt32(tbVendorMiles.Text) + ";CityName=" + tbVendorCity.Text + ";MetroCD=" + tbVendorMetro.Text + ";AircraftType=" + tbAircraftType.Text + ";IATA=" + tbVendorIata.Text + ";StateName=" + tbVendorState.Text + ";AirportName=" + tbVendorName.Text + ";AirportID=" + AirportId;
                        }
                        else
                        {
                            //hdnReportName.Value = "RptLocatorCustom";

                            if (tbCustomMiles.Text == "")
                            {
                                tbCustomMiles.Text = "0";
                            }
                            //Int64 AirportId = 0;
                            //if (!string.IsNullOrEmpty(hdnCustomIcao.Value))
                            //{
                            //    AirportId = Convert.ToInt64(hdnCustomIcao.Value);
                            //}
                            //if ((AirportId == 0) && (!string.IsNullOrEmpty(hdnCustomIata.Value)))
                            //{
                            //    AirportId = Convert.ToInt64(hdnCustomIata.Value);
                            //}
                            string AirportId = "";
                            if (!string.IsNullOrEmpty(hdnCustomIcao.Value))
                            {
                                AirportId = hdnCustomIcao.Value;
                            }
                            if ((AirportId == "") && (!string.IsNullOrEmpty(hdnCustomIata.Value)))
                            {
                                AirportId = hdnCustomIata.Value;
                            }
                            Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";IcaoID=" + tbCustomIcao.Text + ";CountryCD=" + tbCustomCountry.Text + ";MilesFrom=" + Convert.ToInt32(tbCustomMiles.Text) + ";CityName=" + tbCustomCity.Text + ";MetroCD=" + tbCustomMetro.Text + ";IATA=" + tbCustomIata.Text + ";StateName=" + tbCustomState.Text + ";AirportName=" + tbCustomName.Text + ";AirportID=" + AirportId;
                        }

                        Session["REPORT"] = hdnReportName.Value;
                        //Commented by Ramesh, both PDF or Excel will be redirected to the same page.
                        //if (hdnReportFormat.Value == "PDF")
                        //{
                        Response.Redirect(@"..\Reports\ReportRenderView.aspx");
                        //}                        
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        #endregion

        /// <summary>
        /// For Ajax Request
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {


                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
            }
        }
        /// <summary>
        /// To Empty Grids
        /// </summary>
        private void EmptyGrid()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        EmptyAirportHiddenValue();
                        EmptyHiddenHotelValue();
                        EmptyVendorHiddenValue();
                        EmptyCustomHiddenValues();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.Locator);
                }
            }
        }


        /// <summary>
        /// Method to Apply Permissions
        /// </summary>
        private void ApplyPermissions()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                // Check for Page Level Permissions
                CheckAutorization(Permission.Utilities.ViewLocator);
            }
        }
        #region Airports

        protected void AirportLocator_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    string resolvedurl = string.Empty;
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case "Filter":
                                if (!((string.IsNullOrEmpty(tbAirportICAO.Text)) && (string.IsNullOrEmpty(tbIata.Text)) && (string.IsNullOrEmpty(tbCountry.Text)) && (string.IsNullOrEmpty(tbRunway.Text)) && (string.IsNullOrEmpty(tbCity.Text)) && (string.IsNullOrEmpty(tbMetroCD.Text)) && (string.IsNullOrEmpty(tbMiles.Text)) && (string.IsNullOrEmpty(tbState.Text)) && (string.IsNullOrEmpty(tbName.Text)) && (chkHeliport.Checked == false) && (chkApofEntry.Checked == false) && (chkIncludeInactivateAirport.Checked == false)))
                                {
                                    Count_Check = true;
                                }
                                int Count = dgAirportLocator.Items.Count;
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }

        public int GridCount(RadGrid Grid)
        {
            int CurrentPageIndex = Grid.CurrentPageIndex;
            int TotalCount = 0;
            int Pagescount = Grid.PageCount;
            for (int P_C = 0; P_C <= Pagescount - 1; P_C++)
            {
                Grid.CurrentPageIndex = P_C;
                TotalCount += Grid.Items.Count;
            }
            Grid.CurrentPageIndex = CurrentPageIndex;
            return TotalCount;
        }

        protected void RadGrid1_PreRender(object sender, EventArgs e)
        {
            if (Count_Check)
            {
                //dgAirportLocator.MasterTableView.AllowPaging = false;
                //dgAirportLocator.Rebind();
                int Count = dgAirportLocator.Items.Count;

                //int Count = GridCount(dgAirportLocator);
                if ((!string.IsNullOrEmpty(tbAirportICAO.Text)) && (!string.IsNullOrEmpty(tbMiles.Text)))
                {
                    if (Count <= 1)
                        lbAirportText.Text = System.Web.HttpUtility.HtmlEncode("Miles are calculated from ICAO: " + tbAirportICAO.Text.ToUpper().Trim() + " (" + Count + " Airport found)");
                    else
                        lbAirportText.Text = System.Web.HttpUtility.HtmlEncode("Miles are calculated from ICAO: " + tbAirportICAO.Text.ToUpper().Trim() + " (" + Count + " Airports found)");
                }
                else
                {
                    if (Count <= 1)
                        lbAirportText.Text = System.Web.HttpUtility.HtmlEncode("Miles are calculated from middle of the city (" + Count + " Airport found)");
                    else
                        lbAirportText.Text = System.Web.HttpUtility.HtmlEncode("Miles are calculated from middle of the city (" + Count + " Airports found)");
                }
                //lbAirportText.Text = System.Web.HttpUtility.HtmlEncode("Miles are calculated from middle of the city (" + dgAirportLocator.Items.Count + " Airports found)");
                //dgAirportLocator.MasterTableView.AllowPaging = true;
                //dgAirportLocator.Rebind();
            }
        }

        public void MilesCalculation(RadGrid dgAirportLocator)
        {
            string Count = Convert.ToString(dgAirportLocator.VirtualItemCount);
            lbAirportText.Text = System.Web.HttpUtility.HtmlEncode("Miles are calculated from middle of the city (" + Count + " Airports found)");
        }

        protected void dgAirportLocator_SortCommand(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            //Apply custom sorting

            GridSortExpression sortExpr = new GridSortExpression();
            switch (e.OldSortOrder)
            {
                case GridSortOrder.None:
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = GridSortOrder.Descending;

                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                    break;
                case GridSortOrder.Ascending:
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = dgAirportLocator.MasterTableView.AllowNaturalSort ? GridSortOrder.None : GridSortOrder.Descending;
                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                    break;
                case GridSortOrder.Descending:
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = GridSortOrder.Ascending;

                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                    break;

            }
            e.Canceled = true;
            dgAirportLocator.Rebind();

        }
        /// <summary>
        /// To validate Old Airport ICAO 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbAirportICAO_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (!string.IsNullOrEmpty(tbAirportICAO.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                        var objRetVal = objDstsvc.GetAirportByAirportICaoID(tbAirportICAO.Text.ToUpper().Trim());
                        if (objRetVal.ReturnFlag)
                        {
                            AirportLists = objRetVal.EntityList.ToList();
                            if (AirportLists != null && AirportLists.Count > 0)
                            {
                                if (AirportLists[0].AirportID != null)
                                    hdnAirportICAOID.Value = ((FlightPak.Web.FlightPakMasterService.GetAllAirport)AirportLists[0]).AirportID.ToString();
                                cvAirportICAO.IsValid = true;
                            }
                            else
                            {
                                tbAirportICAO.Text = string.Empty;
                                cvAirportICAO.IsValid = false;
                                hdnAirportICAOID.Value = string.Empty;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbAirportICAO);
                            }
                        }
                    }
                }
                else
                {
                    hdnAirportICAOID.Value = string.Empty;
                }
            }
        }
        /// <summary>
        /// To check unique Country Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbCountry_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbCountry.Text != null) && (tbCountry.Text != string.Empty))
                {
                    //To check for unique Country code
                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var CountryValue = FPKMasterService.GetCountryMasterList().EntityList.Where(x => x.CountryCD != null && x.CountryCD.ToString().ToUpper().Trim().Equals(tbCountry.Text.ToString().ToUpper().Trim())).ToList();
                        if (CountryValue.Count() > 0 && CountryValue != null)
                        {
                            if (!string.IsNullOrEmpty(((FlightPakMasterService.Country)CountryValue[0]).CountryName)) ;
                            hdnCountry.Value = ((FlightPakMasterService.Country)CountryValue[0]).CountryID.ToString().ToUpper().Trim();
                            cvCountry.IsValid = true;
                        }
                        else
                        {
                            tbCountry.Text = string.Empty;
                            cvCountry.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCountry);
                            hdnCountry.Value = string.Empty;
                        }
                    }
                }
                else
                {
                    hdnCountry.Value = string.Empty;
                }
            }
        }
        /// <summary>
        /// To check unique Metro Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbMetroCD_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbMetroCD.Text != null) && (tbMetroCD.Text != string.Empty))
                {
                    //To check for unique metro code
                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var MetroValue = FPKMasterService.GetMetroCityList().EntityList.Where(x => x.MetroCD.ToString().ToUpper().Trim().Equals(tbMetroCD.Text.ToUpper().Trim())).ToList();
                        if (MetroValue.Count() > 0 && MetroValue != null)
                        {
                            hdnMetroId.Value = ((FlightPakMasterService.Metro)MetroValue[0]).MetroID.ToString().Trim();
                            cvMetro.IsValid = true;
                        }
                        else
                        {
                            tbMetroCD.Text = string.Empty;
                            cvMetro.IsValid = false;
                            hdnMetroId.Value = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbMetroCD);
                        }
                    }
                }
                else
                {
                    hdnMetroId.Value = string.Empty;
                }
            }
        }
        /// <summary>
        /// To validate Old Airport ICAO 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbIata_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (!string.IsNullOrEmpty(tbIata.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllAirportListForGrid> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirportListForGrid>();
                        var objRetVal = objDstsvc.GetAllAirportListForGrid(true).EntityList.Where(x => x.Iata != null && x.Iata.ToUpper().Trim().Equals(tbIata.Text.ToUpper().Trim()));
                        if (objRetVal != null && objRetVal.Count() > 0)
                        {
                            AirportLists = objRetVal.ToList();
                            if (AirportLists != null && AirportLists.Count > 0)
                            {
                                if (AirportLists[0].AirportID != null)
                                    hdnIata.Value = ((FlightPak.Web.FlightPakMasterService.GetAllAirportListForGrid)AirportLists[0]).AirportID.ToString();
                                cvIata.IsValid = true;
                            }
                            else
                            {
                                tbIata.Text = string.Empty;
                                cvIata.IsValid = false;
                                hdnIata.Value = string.Empty;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbIata);
                            }
                        }
                        else
                        {
                            tbHotelIata.Text = string.Empty;
                            cvHotelIata.IsValid = false;
                            hdnHotelIata.Value = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbHotelIata);
                        }
                    }
                }
                else
                {
                    hdnHotelIata.Value = string.Empty;
                }
            }
        }
        /// <summary>
        /// To Reset the Airport grid and controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReset_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgAirportLocator.DataSource = string.Empty;
                        dgAirportLocator.DataBind();
                        //EmptyAirportHiddenValue();
                        EmptyGrid();
                        EnableReportIcon(false);
                        MapVisibility();
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbAirportICAO);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.Locator);
                }
            }
        }
        /// <summary>
        /// To fetch records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbAirportText.Text = string.Empty;
                        //dgAirportLocator.MasterTableView.FilterExpression = string.Empty;
                        foreach (GridColumn column in dgAirportLocator.MasterTableView.OwnerGrid.Columns)
                        {
                            column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                            column.CurrentFilterValue = string.Empty;
                        }
                        dgAirportLocator.MasterTableView.FilterExpression = string.Empty;
                        dgAirportLocator.DataBind();

                        dgAirportLocator.DataSource = new string[] { };
                        dgAirportLocator.MasterTableView.Rebind();
                        if (CheckAirportValidation())
                        {
                            BindAirportData(true);
                            MapVisibility();
                            EnableReportIcon(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage("Timed out. Please refine your search", ModuleNameConstants.Utilities.Locator);
                }
            }

        }
        /// <summary>
        /// To validate the validations
        /// </summary>
        /// <returns></returns>
        private bool CheckAirportValidation()
        {
            bool returnValue = true;
            if ((string.IsNullOrEmpty(tbAirportICAO.Text)) && (string.IsNullOrEmpty(tbIata.Text)) && (string.IsNullOrEmpty(tbCountry.Text)) && (string.IsNullOrEmpty(tbRunway.Text)) && (string.IsNullOrEmpty(tbCity.Text)) && (string.IsNullOrEmpty(tbMetroCD.Text)) && (string.IsNullOrEmpty(tbMiles.Text)) && (string.IsNullOrEmpty(tbState.Text))
                   && (string.IsNullOrEmpty(tbName.Text)) && (chkHeliport.Checked == false) && (chkApofEntry.Checked == false) && (chkIncludeInactivateAirport.Checked == false))
            {
                returnValue = false;
                string msgToDisplay = "Search Criteria must be entered for the Airport Locator";
                RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackAirportIcaoFn");
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbMiles.Text)) && (string.IsNullOrEmpty(tbAirportICAO.Text)) && (string.IsNullOrEmpty(tbCity.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "The Miles From search criteria must be used in conjunction with either the ICAO or the City field";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackAirportIcaoFn");
                }
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbMiles.Text)) && (!string.IsNullOrEmpty(tbAirportICAO.Text)) && (!string.IsNullOrEmpty(tbCity.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "Miles From search criteria must be calculated using either the ICAO or the City field. Both fields cannot contain a value. Please remove one of them";
                    RadWindowManager1.RadAlert(msgToDisplay, 600, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackAirportIcaoFn");
                }
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbMiles.Text)) && (!string.IsNullOrEmpty(tbCity.Text)) && (string.IsNullOrEmpty(tbCountry.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "Please qualify your City by entering the Country in which it is located";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackAirportCountryFn");
                }
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbMiles.Text)) && (!string.IsNullOrEmpty(tbCity.Text)) && (!string.IsNullOrEmpty(tbCountry.Text)) && (tbCountry.Text.ToUpper().Trim() == "US") && (string.IsNullOrEmpty(tbState.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "Please enter the State in which the City is located";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackAirportStateFn");
                }
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbMiles.Text)) && (!string.IsNullOrEmpty(tbCity.Text)) && (!string.IsNullOrEmpty(tbCountry.Text)) && (tbCountry.Text.ToUpper().Trim() == "CA") && (string.IsNullOrEmpty(tbState.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "Please enter the Province in which the City is located";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackAirportStateFn");
                }
            }
            return returnValue;
        }
        /// <summary>
        /// To clear all the values and fields
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnResetAll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        EmptyGrid();
                        EnableReportIcon(false);
                        MapVisibility();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.Locator);
                }
            }
        }
        /// <summary>
        /// To get records
        /// </summary>
        /// <param name="isBind"></param>
        private void BindAirportData(bool isBind)
        {
            using (UtilitiesService.UtilitiesServiceClient objService = new UtilitiesService.UtilitiesServiceClient())
            {
                Int64 AirportId = 0;
                if (!string.IsNullOrEmpty(hdnAirportICAOID.Value))
                {
                    AirportId = Convert.ToInt64(hdnAirportICAOID.Value);
                }
                if ((AirportId == 0) && (!string.IsNullOrEmpty(hdnIata.Value)))
                {
                    AirportId = Convert.ToInt64(hdnIata.Value);
                }
                Int64 CountryId = 0;
                if (!string.IsNullOrEmpty(hdnCountry.Value))
                {
                    CountryId = Convert.ToInt64(hdnCountry.Value);
                }
                Int64 MetroId = 0;
                if (!string.IsNullOrEmpty(hdnMetroId.Value))
                {
                    MetroId = Convert.ToInt64(hdnMetroId.Value);
                }
                Int32 miles = 0;
                if (!string.IsNullOrEmpty(tbMiles.Text))
                {
                    miles = Convert.ToInt32(tbMiles.Text);
                }
                decimal runway = 0;
                if (!string.IsNullOrEmpty(tbRunway.Text))
                {
                    runway = Convert.ToDecimal(tbRunway.Text);
                }
                lbAirportText.Text = string.Empty;

                var objAirport = objService.GetAllLocatorAirport(AirportId, tbAirportICAO.Text, tbIata.Text, tbCountry.Text, CountryId, runway, tbCity.Text, MetroId, tbMetroCD.Text, miles, tbState.Text, tbName.Text, Convert.ToBoolean(chkHeliport.Checked), Convert.ToBoolean(chkApofEntry.Checked), Convert.ToBoolean(chkIncludeInactivateAirport.Checked));
                if (objAirport != null && objAirport.EntityList != null && objAirport.EntityList.Count() > 0)
                {
                    var objResult = objAirport.EntityList;
                    dgAirportLocator.DataSource = objResult;

                    if (isBind)
                    {
                        dgAirportLocator.DataBind();
                        if (objResult != null && objResult.Count() > 0)
                        {
                            if ((!string.IsNullOrEmpty(tbAirportICAO.Text)) && (!string.IsNullOrEmpty(tbMiles.Text)))
                            {
                                if (objResult.Count <= 1)
                                    lbAirportText.Text = System.Web.HttpUtility.HtmlEncode("Miles are calculated from ICAO: " + tbAirportICAO.Text.ToUpper().Trim() + " (" + objResult.Count + " Airport found)");
                                else
                                    lbAirportText.Text = System.Web.HttpUtility.HtmlEncode("Miles are calculated from ICAO: " + tbAirportICAO.Text.ToUpper().Trim() + " (" + objResult.Count + " Airports found)");
                            }
                            else
                            {
                                if (objResult.Count <= 1)
                                    lbAirportText.Text = System.Web.HttpUtility.HtmlEncode("Miles are calculated from middle of the city (" + objResult.Count + " Airport found)");
                                else
                                    lbAirportText.Text = System.Web.HttpUtility.HtmlEncode("Miles are calculated from middle of the city (" + objResult.Count + " Airports found)");
                            }
                        }

                    }

                }
                else
                {
                    dgAirportLocator.DataSource = string.Empty;
                    string msgToDisplay = "No airports meet your search criteria. Please try again";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "");
                }
                //}
                //catch (Exception ex)
                //{
                //    string msgToDisplay = " Timed out. Please refine your search";
                //    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "");
                //}
            }
        }
        /// <summary>
        /// Bind Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgAirportLocator_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindAirportData(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage("Timed out. Please refine your search", ModuleNameConstants.Utilities.Locator);
                }
            }
        }
        /// <summary>
        /// To Empty hidden value
        /// </summary>
        private void EmptyAirportHiddenValue()
        {
            hdnAirportICAOID.Value = string.Empty;
            hdnCountry.Value = string.Empty;
            hdnIata.Value = string.Empty;
            hdnMetroId.Value = string.Empty;
            tbAirportICAO.Text = string.Empty;
            tbIata.Text = string.Empty;
            tbCountry.Text = string.Empty;
            tbRunway.Text = string.Empty;
            tbCity.Text = string.Empty;
            tbMetroCD.Text = string.Empty;
            tbMiles.Text = string.Empty;
            tbState.Text = string.Empty;
            tbName.Text = string.Empty;
            chkHeliport.Checked = false;
            chkApofEntry.Checked = false;
            chkIncludeInactivateAirport.Checked = false;
            lbAirportText.Text = string.Empty;
            dgAirportLocator.DataSource = string.Empty;


            foreach (GridColumn column in dgAirportLocator.MasterTableView.OwnerGrid.Columns)
            {
                column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                column.CurrentFilterValue = string.Empty;
            }
            dgAirportLocator.MasterTableView.FilterExpression = string.Empty;
            dgAirportLocator.DataBind();
        }

        protected void dgAirportLocator_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgAirportLocator.Items.Count > 0 && dgAirportLocator.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = dgAirportLocator.SelectedItems[0] as GridDataItem;
                            hdnCity.Value = Convert.ToString(Item["City"].Text);
                            hdnState.Value = Convert.ToString(Item["State"].Text);
                            hdnCountrs.Value = Convert.ToString(Item["CountryCD"].Text);
                            hdnLatitude.Value = Convert.ToString(Item["OrigLatitude"].Text);
                            hdnLongitude.Value = Convert.ToString(Item["OrigLongitude"].Text);

                            MapVisibility();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }


        #endregion
        #region Hotels
        /// <summary>
        /// Bind Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgHotel_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindHotelData(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }

        protected void dgHotel_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgHotel.Items.Count > 0 && dgHotel.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = dgHotel.SelectedItems[0] as GridDataItem;
                            hdnCity.Value = Item["CityName"].Text;
                            hdnState.Value = Item["StateName"].Text;
                            hdnCountrs.Value = Item["CountryCD"].Text;
                            hdnLatitude.Value = Item["OrigLatitude"].Text;
                            hdnLongitude.Value = Item["OrigLongitude"].Text;

                            MapVisibility();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }

        protected void dgHotel_SortCommand(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            //Apply custom sorting

            GridSortExpression sortExpr = new GridSortExpression();
            switch (e.OldSortOrder)
            {
                case GridSortOrder.None:
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = GridSortOrder.Descending;

                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                    break;
                case GridSortOrder.Ascending:
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = dgHotel.MasterTableView.AllowNaturalSort ? GridSortOrder.None : GridSortOrder.Descending;
                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                    break;
                case GridSortOrder.Descending:
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = GridSortOrder.Ascending;

                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                    break;

            }
            e.Canceled = true;
            dgHotel.Rebind();

        }


        /// <summary>
        /// To validate Old Airport ICAO 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbHotelAirportIcao_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (!string.IsNullOrEmpty(tbHotelAirportIcao.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                        var objRetVal = objDstsvc.GetAirportByAirportICaoID(tbHotelAirportIcao.Text.ToUpper().Trim());
                        if (objRetVal.ReturnFlag)
                        {
                            AirportLists = objRetVal.EntityList.ToList();
                            if (AirportLists != null && AirportLists.Count > 0)
                            {
                                if (AirportLists[0].AirportID != null)
                                    hdnHotelAirportIcao.Value = ((FlightPak.Web.FlightPakMasterService.GetAllAirport)AirportLists[0]).AirportID.ToString();
                                cvHotelAirportIcao.IsValid = true;
                            }
                            else
                            {
                                tbHotelAirportIcao.Text = string.Empty;
                                cvHotelAirportIcao.IsValid = false;
                                hdnHotelAirportIcao.Value = string.Empty;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbHotelAirportIcao);
                            }
                        }
                    }
                }
                else
                {
                    hdnHotelAirportIcao.Value = string.Empty;
                }
            }
        }
        /// <summary>
        /// To check unique Country Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbHotelCountry_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(tbHotelCountry.Text))
                {
                    //To check for unique Country code
                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var CountryValue = FPKMasterService.GetCountryMasterList().EntityList.Where(x => x.CountryCD != null && x.CountryCD.ToString().ToUpper().Trim().Equals(tbHotelCountry.Text.ToString().ToUpper().Trim())).ToList();
                        if (CountryValue.Count() > 0 && CountryValue != null)
                        {
                            if (!string.IsNullOrEmpty(((FlightPakMasterService.Country)CountryValue[0]).CountryName)) ;
                            hdnHotelCountry.Value = ((FlightPakMasterService.Country)CountryValue[0]).CountryID.ToString().ToUpper().Trim();
                            cvHotelCountry.IsValid = true;
                        }
                        else
                        {
                            tbHotelCountry.Text = string.Empty;
                            hdnHotelCountry.Value = string.Empty;
                            cvHotelCountry.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbHotelCountry);
                        }
                    }
                }
                else
                {
                    hdnHotelCountry.Value = string.Empty;
                }
            }
        }
        /// <summary>
        /// To check unique Metro Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbHotelMetro_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(tbHotelMetro.Text))
                {
                    //To check for unique metro code
                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var MetroValue = FPKMasterService.GetMetroCityList().EntityList.Where(x => x.MetroCD.ToString().ToUpper().Trim().Equals(tbHotelMetro.Text.ToUpper().Trim())).ToList();
                        if (MetroValue.Count() > 0 && MetroValue != null)
                        {
                            hdnHotelMetro.Value = ((FlightPakMasterService.Metro)MetroValue[0]).MetroID.ToString().Trim();
                            cvHotelMetro.IsValid = true;
                        }
                        else
                        {
                            hdnHotelMetro.Value = string.Empty;
                            tbHotelMetro.Text = string.Empty;
                            cvHotelMetro.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbHotelMetro);
                        }
                    }
                }
                else
                {
                    hdnHotelMetro.Value = string.Empty;
                }
            }
        }
        /// <summary>
        /// To validate Old Airport ICAO 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbHotelIata_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (!string.IsNullOrEmpty(tbHotelIata.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllAirportListForGrid> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirportListForGrid>();
                        var objRetVal = objDstsvc.GetAllAirportListForGrid(true).EntityList.Where(x => x.Iata != null && x.Iata.ToUpper().Trim().Equals(tbHotelIata.Text.ToUpper().Trim()));
                        if (objRetVal != null && objRetVal.Count() > 0)
                        {
                            AirportLists = objRetVal.ToList();
                            if (AirportLists != null && AirportLists.Count > 0)
                            {
                                if (AirportLists[0].AirportID != null)
                                    hdnHotelIata.Value = ((FlightPak.Web.FlightPakMasterService.GetAllAirportListForGrid)AirportLists[0]).AirportID.ToString();
                                cvHotelIata.IsValid = true;
                            }
                            else
                            {
                                tbHotelIata.Text = string.Empty;
                                cvHotelIata.IsValid = false;
                                hdnHotelIata.Value = string.Empty;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbHotelIata);
                            }
                        }
                        else
                        {
                            tbHotelIata.Text = string.Empty;
                            cvHotelIata.IsValid = false;
                            hdnHotelIata.Value = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbHotelIata);
                        }
                    }
                }
                else
                {
                    hdnHotelIata.Value = string.Empty;
                }
            }
        }
        /// <summary>
        /// To Empty the hotel grid and controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnHotelReset_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        EmptyHiddenHotelValue();
                        EnableReportIcon(false);
                        MapVisibility();
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbHotelAirportIcao);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.Locator);
                }
            }
        }
        /// <summary>
        /// To fetch the value of hotel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnHotelSearch_Click(object sender, EventArgs e)
        {
            // lbho.Text = string.Empty;
            //dgAirportLocator.MasterTableView.FilterExpression = string.Empty;



            dgHotel.DataSource = new string[] { };
            dgHotel.MasterTableView.Rebind();
            dgHotel.DataSource = string.Empty;

            foreach (GridColumn column in dgHotel.MasterTableView.OwnerGrid.Columns)
            {
                column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                column.CurrentFilterValue = string.Empty;
            }
            dgHotel.MasterTableView.FilterExpression = string.Empty;


            dgHotel.DataBind();
            if (CheckHotelValidation())
            {
                BindHotelData(true);
                MapVisibility();
                EnableReportIcon(true);
            }
        }
        /// <summary>
        /// To Validate the validation for hotel
        /// </summary>
        /// <returns></returns>
        private bool CheckHotelValidation()
        {
            bool returnValue = true;
            if ((string.IsNullOrEmpty(tbHotelAirportIcao.Text)) && (string.IsNullOrEmpty(tbHotelIata.Text)) && (string.IsNullOrEmpty(tbHotelCountry.Text)) && (string.IsNullOrEmpty(tbHotelCity.Text)) && (string.IsNullOrEmpty(tbHotelMetro.Text)) && (string.IsNullOrEmpty(tbHotelMilesFrom.Text)) && (string.IsNullOrEmpty(tbHotelState.Text)) && (string.IsNullOrEmpty(tbHotelName.Text)))
            {
                returnValue = false;
                string msgToDisplay = "Search Criteria must be entered for the Hotel Locator";
                RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackHotelIcaoFn");
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbHotelMilesFrom.Text)) && (string.IsNullOrEmpty(tbHotelAirportIcao.Text)) && (string.IsNullOrEmpty(tbHotelCity.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "The Miles From search criteria must be used in conjunction with either the ICAO or the City field";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackHotelIcaoFn");
                }
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbHotelMilesFrom.Text)) && (!string.IsNullOrEmpty(tbHotelAirportIcao.Text)) && (!string.IsNullOrEmpty(tbHotelCity.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "Miles From search criteria must be calculated using either the ICAO or the City field. Both fields cannot contain a value. Please remove one of them";
                    RadWindowManager1.RadAlert(msgToDisplay, 600, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackHotelIcaoFn");
                }
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbHotelMilesFrom.Text)) && (!string.IsNullOrEmpty(tbHotelCity.Text)) && (string.IsNullOrEmpty(tbHotelCountry.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "Please qualify your City by entering the Country in which it is located";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackHotelCountryFn");
                }
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbHotelMilesFrom.Text)) && (!string.IsNullOrEmpty(tbHotelCity.Text)) && (!string.IsNullOrEmpty(tbHotelCountry.Text)) && (tbHotelCountry.Text.ToUpper().Trim() == "US") && (string.IsNullOrEmpty(tbHotelState.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "Please enter the State in which the City is located";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackHotelStateFn");
                }
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbHotelMilesFrom.Text)) && (!string.IsNullOrEmpty(tbHotelCity.Text)) && (!string.IsNullOrEmpty(tbHotelCountry.Text)) && (tbHotelCountry.Text.ToUpper().Trim() == "CA") && (string.IsNullOrEmpty(tbHotelState.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "Please enter the Province in which the City is located";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackHotelStateFn");
                }
            }
            return returnValue;
        }
        /// <summary>
        /// To Clear all the values of grid and controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnHotelResetAlll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        EmptyGrid();
                        EnableReportIcon(false);
                        MapVisibility();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.Locator);
                }
            }
        }
        /// <summary>
        /// To bind hotel data
        /// </summary>
        /// <param name="isBind"></param>
        private void BindHotelData(bool isBind)
        {
            using (UtilitiesService.UtilitiesServiceClient objService = new UtilitiesService.UtilitiesServiceClient())
            {
                Int64 AirportId = 0;
                if (!string.IsNullOrEmpty(hdnHotelAirportIcao.Value))
                {
                    AirportId = Convert.ToInt64(hdnHotelAirportIcao.Value);
                }
                if ((AirportId == 0) && (!string.IsNullOrEmpty(hdnHotelIata.Value)))
                {
                    AirportId = Convert.ToInt64(hdnHotelIata.Value);
                }
                Int64 CountryId = 0;
                if (!string.IsNullOrEmpty(hdnHotelCountry.Value))
                {
                    CountryId = Convert.ToInt64(hdnHotelCountry.Value);
                }
                Int64 MetroId = 0;
                if (!string.IsNullOrEmpty(hdnHotelMetro.Value))
                {
                    MetroId = Convert.ToInt64(hdnHotelMetro.Value);
                }
                Int32 miles = 0;
                if (!string.IsNullOrEmpty(tbHotelMilesFrom.Text))
                {
                    miles = Convert.ToInt32(tbHotelMilesFrom.Text);
                }

                var objHotel = objService.GetAllLocatorHotel(AirportId, tbHotelAirportIcao.Text, tbHotelIata.Text, tbHotelState.Text, tbHotelCity.Text, tbHotelCountry.Text, CountryId, MetroId, tbHotelMetro.Text, tbHotelName.Text, miles);
                if (objHotel != null && objHotel.EntityList.Count() > 0)
                {
                    if (AirportId != 0)
                    {
                        dgHotel.DataSource = objHotel.EntityList;
                    }
                    else
                    {
                        dgHotel.DataSource = objHotel.EntityList.Where(x => x.AirportID == 0).ToList();
                    }
                    if (isBind)
                        dgHotel.DataBind();
                }
                else
                {
                    string msgToDisplay = "No hotels meet your search criteria. Please try again";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "");
                }
            }
        }
        /// <summary>
        /// To Empty hotel value and grid
        /// </summary>
        private void EmptyHiddenHotelValue()
        {
            tbHotelAirportIcao.Text = string.Empty;
            tbHotelIata.Text = string.Empty;
            tbHotelCountry.Text = string.Empty;
            tbHotelCity.Text = string.Empty;
            tbHotelMetro.Text = string.Empty;
            tbHotelMilesFrom.Text = string.Empty;
            tbHotelState.Text = string.Empty;
            tbHotelName.Text = string.Empty;
            foreach (GridColumn column in dgHotel.MasterTableView.OwnerGrid.Columns)
            {
                column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                column.CurrentFilterValue = string.Empty;
            }
            dgHotel.MasterTableView.FilterExpression = string.Empty;
            dgHotel.DataBind();

            dgHotel.DataSource = string.Empty;
            dgHotel.DataBind();
            hdnHotelAirportIcao.Value = string.Empty;
            hdnHotelCountry.Value = string.Empty;
            hdnHotelIata.Value = string.Empty;
            hdnHotelMetro.Value = string.Empty;
        }
        #endregion
        #region Vendors

        protected void dgVendors_SortCommand(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            //Apply custom sorting

            GridSortExpression sortExpr = new GridSortExpression();
            switch (e.OldSortOrder)
            {
                case GridSortOrder.None:
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = GridSortOrder.Descending;

                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                    break;
                case GridSortOrder.Ascending:
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = dgVendors.MasterTableView.AllowNaturalSort ? GridSortOrder.None : GridSortOrder.Descending;
                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                    break;
                case GridSortOrder.Descending:
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = GridSortOrder.Ascending;

                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                    break;

            }
            e.Canceled = true;
            dgVendors.Rebind();

        }

        protected void dgVendors_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgVendors.Items.Count > 0 && dgVendors.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = dgVendors.SelectedItems[0] as GridDataItem;
                            hdnCity.Value = Item["BillingCity"].Text;
                            hdnState.Value = Item["BillingState"].Text;
                            hdnCountrs.Value = Item["CountryCD"].Text;
                            hdnLatitude.Value = Item["OrigLatitude"].Text;
                            hdnLongitude.Value = Item["OrigLongitude"].Text;

                            MapVisibility();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }
        /// <summary>
        /// Bind Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgVendors_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindVendorData(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// To get unique Aircraft
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AircraftType_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbAircraftType.Text.Trim()))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var AircraftServiceValue = FPKMasterService.GetAircraftList().EntityList.Where(x => x.AircraftCD.ToString().ToUpper().Trim().Equals(tbAircraftType.Text.ToUpper().Trim())).ToList();
                                if (AircraftServiceValue != null && AircraftServiceValue.Count > 0)
                                {
                                    hdnAircraftType.Value = ((FlightPakMasterService.GetAllAircraft)AircraftServiceValue[0]).AircraftID.ToString().Trim();
                                    cvAircraftType.IsValid = true;
                                }
                                else
                                {
                                    tbAircraftType.Text = string.Empty;
                                    hdnAircraftType.Value = string.Empty;
                                    cvAircraftType.IsValid = false;
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbAircraftType);
                                }
                            }
                        }
                        else
                        {
                            hdnAircraftType.Value = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Crew);
                }
            }
        }
        /// <summary>
        /// To validate Old Airport ICAO 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbVendorAirportIcao_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (!string.IsNullOrEmpty(tbVendorAirportIcao.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                        var objRetVal = objDstsvc.GetAirportByAirportICaoID(tbVendorAirportIcao.Text.ToUpper().Trim());
                        if (objRetVal.ReturnFlag)
                        {
                            AirportLists = objRetVal.EntityList.ToList();
                            if (AirportLists != null && AirportLists.Count > 0)
                            {
                                if (AirportLists[0].AirportID != null)
                                    cvVendorIcao.IsValid = true;
                                hdnVendorIcao.Value = ((FlightPak.Web.FlightPakMasterService.GetAllAirport)AirportLists[0]).AirportID.ToString();
                            }
                            else
                            {
                                cvVendorIcao.IsValid = false;
                                hdnVendorIcao.Value = string.Empty;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbVendorAirportIcao);
                            }
                        }
                    }
                }
                else
                {
                    hdnVendorIcao.Value = string.Empty;
                }
            }
        }
        /// <summary>
        /// To check unique Country Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbVendorCountry_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(tbVendorCountry.Text))
                {
                    //To check for unique Country code
                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var CountryValue = FPKMasterService.GetCountryMasterList().EntityList.Where(x => x.CountryCD != null && x.CountryCD.ToString().ToUpper().Trim().Equals(tbVendorCountry.Text.ToString().ToUpper().Trim())).ToList();
                        if (CountryValue.Count() > 0 && CountryValue != null)
                        {
                            if (!string.IsNullOrEmpty(((FlightPakMasterService.Country)CountryValue[0]).CountryName)) ;
                            hdnVendorCountry.Value = ((FlightPakMasterService.Country)CountryValue[0]).CountryID.ToString().ToUpper().Trim();
                            cvVendorCountry.IsValid = true;
                        }
                        else
                        {
                            tbVendorCountry.Text = string.Empty;
                            hdnVendorCountry.Value = string.Empty;
                            cvVendorCountry.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbVendorCountry);
                        }
                    }
                }
                else
                {
                    hdnVendorCountry.Value = string.Empty;
                }
            }
        }
        /// <summary>
        /// To check unique Metro Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbVendorMetro_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(tbVendorMetro.Text))
                {
                    //To check for unique metro code
                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var MetroValue = FPKMasterService.GetMetroCityList().EntityList.Where(x => x.MetroCD.ToString().ToUpper().Trim().Equals(tbVendorMetro.Text.ToUpper().Trim())).ToList();
                        if (MetroValue.Count() > 0 && MetroValue != null)
                        {
                            hdnVendorMetro.Value = ((FlightPakMasterService.Metro)MetroValue[0]).MetroID.ToString().Trim();
                            cvVendorMetro.IsValid = true;
                        }
                        else
                        {
                            tbVendorMetro.Text = string.Empty;
                            hdnVendorMetro.Value = string.Empty;
                            cvVendorMetro.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbVendorMetro);
                        }
                    }
                }
                else
                {
                    hdnVendorMetro.Value = string.Empty;
                }
            }
        }
        /// <summary>
        /// To validate Old Airport ICAO 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbVendorIata_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (!string.IsNullOrEmpty(tbVendorIata.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllAirportListForGrid> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirportListForGrid>();
                        var objRetVal = objDstsvc.GetAllAirportListForGrid(true).EntityList.Where(x => x.Iata != null && x.Iata.ToUpper().Trim().Equals(tbVendorIata.Text.ToUpper().Trim()));
                        if (objRetVal != null && objRetVal.Count() > 0)
                        {
                            AirportLists = objRetVal.ToList();
                            if (AirportLists != null && AirportLists.Count > 0)
                            {
                                if (AirportLists[0].AirportID != null)
                                    hdnVendorIata.Value = ((FlightPak.Web.FlightPakMasterService.GetAllAirportListForGrid)AirportLists[0]).AirportID.ToString();
                                cvVendorIata.IsValid = true;
                            }
                            else
                            {
                                tbVendorIata.Text = string.Empty;
                                cvVendorIata.IsValid = false;
                                hdnVendorIata.Value = string.Empty;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbVendorIata);
                            }
                        }
                        else
                        {
                            tbVendorIata.Text = string.Empty;
                            cvVendorIata.IsValid = false;
                            hdnVendorIata.Value = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbVendorIata);
                        }
                    }
                }
                else
                {
                    hdnVendorIata.Value = string.Empty;
                }
            }
        }
        /// <summary>
        /// To bind vendor data
        /// </summary>
        /// <param name="isBind"></param>
        private void BindVendorData(bool isBind)
        {
            using (UtilitiesService.UtilitiesServiceClient objService = new UtilitiesService.UtilitiesServiceClient())
            {
                Int64 AirportId = 0;
                if (!string.IsNullOrEmpty(hdnVendorIcao.Value))
                {
                    AirportId = Convert.ToInt64(hdnVendorIcao.Value);
                }
                if ((AirportId == 0) && (!string.IsNullOrEmpty(hdnVendorIata.Value)))
                {
                    AirportId = Convert.ToInt64(hdnVendorIata.Value);
                }
                Int64 CountryId = 0;
                if (!string.IsNullOrEmpty(hdnVendorCountry.Value))
                {
                    CountryId = Convert.ToInt64(hdnVendorCountry.Value);
                }
                Int64 MetroId = 0;
                if (!string.IsNullOrEmpty(hdnVendorMetro.Value))
                {
                    MetroId = Convert.ToInt64(hdnVendorMetro.Value);
                }
                Int32 miles = 0;
                if (!string.IsNullOrEmpty(tbVendorMiles.Text))
                {
                    miles = Convert.ToInt32(tbVendorMiles.Text);
                }
                Int64 AircraftId = 0;
                if (!string.IsNullOrEmpty(hdnAircraftType.Value))
                {
                    AircraftId = Convert.ToInt64(hdnAircraftType.Value);
                }
                var objVendor = objService.GetAllLocatorVendor(AirportId, tbVendorAirportIcao.Text, tbVendorIata.Text, tbVendorState.Text, tbVendorCity.Text, tbVendorCountry.Text, CountryId, MetroId, tbVendorMetro.Text, tbVendorName.Text, miles, AircraftId, tbAircraftType.Text);
                if (objVendor != null && objVendor.EntityList.Count() > 0)
                {
                    if (AirportId != 0)
                    {
                        dgVendors.DataSource = objVendor.EntityList;
                    }
                    else
                    {
                        dgVendors.DataSource = objVendor.EntityList.Where(x => x.AirportID == 0).ToList();
                    }
                    if (isBind)
                        dgVendors.DataBind();
                }
                else
                {
                    string msgToDisplay = "No vendors meet your search criteria. Please try again";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "");
                }
            }
        }
        /// <summary>
        /// To reset vendor grid and controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnVendorReset_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        EmptyVendorHiddenValue();
                        EnableReportIcon(false);
                        MapVisibility();
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbVendorAirportIcao);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.Locator);
                }
            }
        }
        /// <summary>
        /// To fetch vendor values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnvendorSearch_Click(object sender, EventArgs e)
        {
            dgVendors.DataSource = new string[] { };
            dgVendors.MasterTableView.Rebind();
            dgVendors.DataSource = string.Empty;

            foreach (GridColumn column in dgVendors.MasterTableView.OwnerGrid.Columns)
            {
                column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                column.CurrentFilterValue = string.Empty;
            }
            dgVendors.MasterTableView.FilterExpression = string.Empty;


            dgVendors.DataBind();
            if (CheckVendorValidation())
            {
                BindVendorData(true);
                MapVisibility();
                EnableReportIcon(true);
            }
        }
        /// <summary>
        /// To check validation of vendor
        /// </summary>
        /// <returns></returns>
        private bool CheckVendorValidation()
        {
            bool returnValue = true;
            if ((string.IsNullOrEmpty(tbVendorAirportIcao.Text)) && (string.IsNullOrEmpty(tbVendorIata.Text)) && (string.IsNullOrEmpty(tbVendorCountry.Text)) && (string.IsNullOrEmpty(tbVendorCity.Text)) && (string.IsNullOrEmpty(tbVendorMetro.Text)) && (string.IsNullOrEmpty(tbVendorMiles.Text)) && (string.IsNullOrEmpty(tbVendorState.Text)) && (string.IsNullOrEmpty(tbVendorName.Text)) && (string.IsNullOrEmpty(tbAircraftType.Text)))
            {
                returnValue = false;
                string msgToDisplay = "Search Criteria must be entered for the Vendor Locator";
                RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackVendorIcaoFn");
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbVendorMiles.Text)) && (string.IsNullOrEmpty(tbVendorAirportIcao.Text)) && (string.IsNullOrEmpty(tbVendorCity.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "The Miles From search criteria must be used in conjunction with either the ICAO or the City field";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackVendorIcaoFn");
                }
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbVendorMiles.Text)) && (!string.IsNullOrEmpty(tbVendorAirportIcao.Text)) && (!string.IsNullOrEmpty(tbVendorCity.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "Miles From search criteria must be calculated using either the ICAO or the City field. Both fields cannot contain a value. Please remove one of them";
                    RadWindowManager1.RadAlert(msgToDisplay, 600, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackVendorIcaoFn");
                }
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbVendorMiles.Text)) && (!string.IsNullOrEmpty(tbVendorCity.Text)) && (string.IsNullOrEmpty(tbVendorCountry.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "Please qualify your City by entering the Country in which it is located";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackVendorCountryFn");
                }
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbVendorMiles.Text)) && (!string.IsNullOrEmpty(tbVendorCity.Text)) && (!string.IsNullOrEmpty(tbVendorCountry.Text)) && (tbVendorCountry.Text.ToUpper().Trim() == "US") && (string.IsNullOrEmpty(tbVendorState.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "Please enter the State in which the City is located";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackVendorStateFn");
                }
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbVendorMiles.Text)) && (!string.IsNullOrEmpty(tbVendorCity.Text)) && (!string.IsNullOrEmpty(tbVendorCountry.Text)) && (tbVendorCountry.Text.ToUpper().Trim() == "CA") && (string.IsNullOrEmpty(tbVendorState.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "Please enter the Province in which the City is located";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackVendorStateFn");
                }
            }
            return returnValue;
        }
        /// <summary>
        /// To Empty all the grid and controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnVendorResetAll_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        EmptyGrid();
                        EnableReportIcon(false);
                        MapVisibility();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.Locator);
                }
            }
        }
        /// <summary>
        /// To Empty hidden value of vendor
        /// </summary>
        private void EmptyVendorHiddenValue()
        {
            tbVendorAirportIcao.Text = string.Empty;
            tbVendorIata.Text = string.Empty;
            tbVendorCountry.Text = string.Empty;
            tbVendorCity.Text = string.Empty;
            tbVendorMetro.Text = string.Empty;
            tbVendorMiles.Text = string.Empty;
            tbVendorState.Text = string.Empty;
            tbVendorName.Text = string.Empty;
            tbAircraftType.Text = string.Empty;

            foreach (GridColumn column in dgVendors.MasterTableView.OwnerGrid.Columns)
            {
                column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                column.CurrentFilterValue = string.Empty;
            }
            dgVendors.MasterTableView.FilterExpression = string.Empty;
            dgVendors.DataBind();


            dgVendors.DataSource = string.Empty;
            dgVendors.DataBind();
            hdnAircraftType.Value = string.Empty;
            hdnVendorCountry.Value = string.Empty;
            hdnVendorIata.Value = string.Empty;
            hdnVendorIcao.Value = string.Empty;
            hdnVendorMetro.Value = string.Empty;
        }
        #endregion
        #region Custom
        protected void dgCustom_SortCommand(object source, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            //Apply custom sorting

            GridSortExpression sortExpr = new GridSortExpression();
            switch (e.OldSortOrder)
            {
                case GridSortOrder.None:
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = GridSortOrder.Descending;

                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                    break;
                case GridSortOrder.Ascending:
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = dgCustom.MasterTableView.AllowNaturalSort ? GridSortOrder.None : GridSortOrder.Descending;
                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                    break;
                case GridSortOrder.Descending:
                    sortExpr.FieldName = e.SortExpression;
                    sortExpr.SortOrder = GridSortOrder.Ascending;

                    e.Item.OwnerTableView.SortExpressions.AddSortExpression(sortExpr);
                    break;

            }
            e.Canceled = true;
            dgCustom.Rebind();

        }

        public void ClearForm()
        {
            hdnCity.Value = "";
            hdnState.Value = "";
            hdnCountrs.Value = "";

            //To disable Report icons on tab change
            EnableReportIcon(false);
        }
        protected void dgCustom_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCustom.Items.Count > 0 && dgCustom.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = dgCustom.SelectedItems[0] as GridDataItem;
                            hdnCity.Value = Item["CustomCity"].Text;
                            hdnState.Value = Item["CustomState"].Text;
                            hdnCountrs.Value = Item["CountryCD"].Text;
                            hdnLatitude.Value = Item["OrigLatitude"].Text;
                            hdnLongitude.Value = Item["OrigLongitude"].Text;

                            MapVisibility();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FBO);
                }
            }
        }

        /// <summary>
        /// Bind Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgCustom_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindCustomData(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// To validate Old Airport ICAO 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbCustomIcao_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (!string.IsNullOrEmpty(tbCustomIcao.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                        var objRetVal = objDstsvc.GetAirportByAirportICaoID(tbCustomIcao.Text.ToUpper().Trim());
                        if (objRetVal.ReturnFlag)
                        {
                            AirportLists = objRetVal.EntityList.ToList();
                            if (AirportLists != null && AirportLists.Count > 0)
                            {
                                if (AirportLists[0].AirportID != null)
                                    hdnCustomIcao.Value = ((FlightPak.Web.FlightPakMasterService.GetAllAirport)AirportLists[0]).AirportID.ToString();
                                cvCustomIcao.IsValid = true;
                            }
                            else
                            {
                                tbCustomIcao.Text = string.Empty;
                                cvCustomIcao.IsValid = false;
                                hdnCustomIcao.Value = string.Empty;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbCustomIcao);
                            }
                        }
                    }
                }
                else
                {
                    hdnCustomIcao.Value = string.Empty;
                }
            }
        }
        /// <summary>
        /// To check unique Country Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbCustomCountry_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(tbCustomCountry.Text))
                {
                    //To check for unique Country code
                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var CountryValue = FPKMasterService.GetCountryMasterList().EntityList.Where(x => x.CountryCD != null && x.CountryCD.ToString().ToUpper().Trim().Equals(tbCustomCountry.Text.ToString().ToUpper().Trim())).ToList();
                        if (CountryValue.Count() > 0 && CountryValue != null)
                        {
                            if (!string.IsNullOrEmpty(((FlightPakMasterService.Country)CountryValue[0]).CountryName)) ;
                            hdnCustomCountry.Value = ((FlightPakMasterService.Country)CountryValue[0]).CountryID.ToString().ToUpper().Trim();
                            cvCustomCountry.IsValid = true;
                        }
                        else
                        {
                            tbCustomCountry.Text = string.Empty;
                            cvCustomCountry.IsValid = false;
                            hdnCustomCountry.Value = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCustomCountry);
                        }
                    }
                }
                else
                {
                    hdnCustomCountry.Value = string.Empty;
                }
            }
        }
        /// <summary>
        /// To check unique Metro Code on Tab Out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbCustomMetro_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(tbCustomMetro.Text))
                {
                    //To check for unique metro code
                    using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var MetroValue = FPKMasterService.GetMetroCityList().EntityList.Where(x => x.MetroCD.ToString().ToUpper().Trim().Equals(tbCustomMetro.Text.ToUpper().Trim())).ToList();
                        if (MetroValue.Count() > 0 && MetroValue != null)
                        {
                            hdnCustomMetro.Value = ((FlightPakMasterService.Metro)MetroValue[0]).MetroID.ToString().Trim();
                            cvCustomMetro.IsValid = true;
                        }
                        else
                        {
                            tbCustomMetro.Text = string.Empty;
                            hdnCustomMetro.Value = string.Empty;
                            cvCustomMetro.IsValid = false;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCustomMetro);
                        }
                    }
                }
                else
                {
                    hdnCustomMetro.Value = string.Empty;
                }
            }
        }
        /// <summary>
        /// To reset custom controls and grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCustomReset_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        EmptyCustomHiddenValues();
                        EnableReportIcon(false);
                        MapVisibility();
                        RadAjaxManager.GetCurrent(Page).FocusControl(tbCustomIcao);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.Locator);
                }
            }
        }
        /// <summary>
        /// To fetch custom address records
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCustomSearch_Click(object sender, EventArgs e)
        {
            dgCustom.DataSource = new string[] { };
            dgCustom.MasterTableView.Rebind();
            dgCustom.DataSource = string.Empty;

            foreach (GridColumn column in dgCustom.MasterTableView.OwnerGrid.Columns)
            {
                column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                column.CurrentFilterValue = string.Empty;
            }
            dgCustom.MasterTableView.FilterExpression = string.Empty;
            dgCustom.DataBind();
            if (CheckCustomValidation())
            {
                BindCustomData(true);
                MapVisibility();
                EnableReportIcon(true);
            }
        }
        /// <summary>
        /// To validate the validation of custom address
        /// </summary>
        /// <returns></returns>
        private bool CheckCustomValidation()
        {
            bool returnValue = true;
            if ((string.IsNullOrEmpty(tbCustomIcao.Text)) && (string.IsNullOrEmpty(tbCustomIata.Text)) && (string.IsNullOrEmpty(tbCustomCity.Text)) && (string.IsNullOrEmpty(tbCustomMetro.Text)) && (string.IsNullOrEmpty(tbCustomMiles.Text)) && (string.IsNullOrEmpty(tbCustomState.Text)) && (string.IsNullOrEmpty(tbCustomName.Text)) && (string.IsNullOrEmpty(tbCustomCountry.Text)))
            {
                returnValue = false;
                string msgToDisplay = "Search Criteria must be entered for the Custom Locator";
                RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackCustomIcaoFn");
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbCustomMiles.Text)) && (string.IsNullOrEmpty(tbCustomIcao.Text)) && (string.IsNullOrEmpty(tbCustomCity.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "The Miles From search criteria must be used in conjunction with either the ICAO or the City field";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackCustomIcaoFn");
                }
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbCustomMiles.Text)) && (!string.IsNullOrEmpty(tbCustomIcao.Text)) && (!string.IsNullOrEmpty(tbCustomCity.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "Miles From search criteria must be calculated using either the ICAO or the City field. Both fields cannot contain a value. Please remove one of them";
                    RadWindowManager1.RadAlert(msgToDisplay, 600, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackCustomIcaoFn");
                }
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbCustomMiles.Text)) && (!string.IsNullOrEmpty(tbCustomCity.Text)) && (string.IsNullOrEmpty(tbCustomCountry.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "Please qualify your City by entering the Country in which it is located";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackCustomCountryFn");
                }
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbCustomMiles.Text)) && (!string.IsNullOrEmpty(tbCustomCity.Text)) && (!string.IsNullOrEmpty(tbCustomCountry.Text)) && (tbCustomCountry.Text.ToUpper().Trim() == "US") && (string.IsNullOrEmpty(tbCustomState.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "Please enter the State in which the City is located";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackCustomStateFn");
                }
            }
            if (returnValue)
            {
                if ((!string.IsNullOrEmpty(tbCustomMiles.Text)) && (!string.IsNullOrEmpty(tbCustomCity.Text)) && (!string.IsNullOrEmpty(tbCustomCountry.Text)) && (tbCustomCountry.Text.ToUpper().Trim() == "CA") && (string.IsNullOrEmpty(tbCustomState.Text)))
                {
                    returnValue = false;
                    string msgToDisplay = "Please enter the Province in which the City is located";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "alertCallBackCustomStateFn");
                }
            }
            return returnValue;
        }
        /// <summary>
        /// To bind custom data
        /// </summary>
        /// <param name="isBind"></param>
        private void BindCustomData(bool isBind)
        {
            using (UtilitiesService.UtilitiesServiceClient objService = new UtilitiesService.UtilitiesServiceClient())
            {
                Int64 AirportId = 0;
                if (!string.IsNullOrEmpty(hdnCustomIcao.Value))
                {
                    AirportId = Convert.ToInt64(hdnCustomIcao.Value);
                }
                if ((AirportId == 0) && (!string.IsNullOrEmpty(hdnCustomIata.Value)))
                {
                    AirportId = Convert.ToInt64(hdnCustomIata.Value);
                }
                Int64 CountryId = 0;
                if (!string.IsNullOrEmpty(hdnCustomCountry.Value))
                {
                    CountryId = Convert.ToInt64(hdnCustomCountry.Value);
                }
                Int64 MetroId = 0;
                if (!string.IsNullOrEmpty(hdnCustomMetro.Value))
                {
                    MetroId = Convert.ToInt64(hdnCustomMetro.Value);
                }
                Int32 miles = 0;
                if (!string.IsNullOrEmpty(tbCustomMiles.Text))
                {
                    miles = Convert.ToInt32(tbCustomMiles.Text);
                }
                var objCustom = objService.GetAllLocatorCustom(AirportId, tbCustomIcao.Text, tbCustomIata.Text, tbCustomState.Text, tbCustomCity.Text, tbCustomCountry.Text, CountryId, MetroId, tbCustomMetro.Text, tbCustomName.Text, miles);
                if (objCustom != null && objCustom.EntityList.Count() > 0)
                {
                    if (AirportId != 0)
                    {
                        dgCustom.DataSource = objCustom.EntityList;
                    }
                    else
                    {
                        dgCustom.DataSource = objCustom.EntityList.Where(x => x.AirportID == 0).ToList();
                    }

                    if (isBind)
                    {

                        dgCustom.DataBind();
                    }

                }
                else
                {
                    string msgToDisplay = "No customs meet your search criteria. Please try again";
                    RadWindowManager1.RadAlert(msgToDisplay, 400, 100, ModuleNameConstants.Utilities.Locator, "");
                }
            }
        }
        /// <summary>
        /// To Reset all the grid and controls 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCustomLocatorReset_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        EmptyGrid();
                        EnableReportIcon(false);
                        MapVisibility();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.Locator);
                }
            }
        }
        /// <summary>
        /// To validate Old Airport ICAO 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbCustomIata_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (!string.IsNullOrEmpty(tbCustomIata.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllAirportListForGrid> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirportListForGrid>();
                        var objRetVal = objDstsvc.GetAllAirportListForGrid(true).EntityList.Where(x => x.Iata != null && x.Iata.ToUpper().Trim().Equals(tbCustomIata.Text.ToUpper().Trim()));
                        if (objRetVal != null && objRetVal.Count() > 0)
                        {
                            AirportLists = objRetVal.ToList();
                            if (AirportLists != null && AirportLists.Count > 0)
                            {
                                if (AirportLists[0].AirportID != null)
                                    hdnCustomIata.Value = ((FlightPak.Web.FlightPakMasterService.GetAllAirportListForGrid)AirportLists[0]).AirportID.ToString();
                                cvCustomIata.IsValid = true;
                            }
                            else
                            {
                                tbCustomIata.Text = string.Empty;
                                cvCustomIata.IsValid = false;
                                hdnCustomIata.Value = string.Empty;
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbCustomIata);
                            }
                        }
                        else
                        {
                            hdnCustomIata.Value = string.Empty;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// To Empty hidden values of custom address
        /// </summary>
        private void EmptyCustomHiddenValues()
        {
            tbCustomIcao.Text = string.Empty;
            tbCustomIata.Text = string.Empty;
            tbCustomCity.Text = string.Empty;
            tbCustomMetro.Text = string.Empty;
            tbCustomMiles.Text = string.Empty;
            tbCustomState.Text = string.Empty;
            tbCustomName.Text = string.Empty;
            tbCustomCountry.Text = string.Empty;
            foreach (GridColumn column in dgCustom.MasterTableView.OwnerGrid.Columns)
            {
                column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                column.CurrentFilterValue = string.Empty;
            }
            dgCustom.MasterTableView.FilterExpression = string.Empty;
            dgCustom.DataBind();

            dgCustom.DataSource = string.Empty;
            dgCustom.DataBind();
            hdnCustomCountry.Value = string.Empty;
            hdnCustomIata.Value = string.Empty;
            hdnCustomIcao.Value = string.Empty;
            hdnCustomMetro.Value = string.Empty;
        }
        #endregion

    }
}