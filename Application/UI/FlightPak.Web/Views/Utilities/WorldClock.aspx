﻿<%@ Page Title="World Clock" Language="C#" AutoEventWireup="true" CodeBehind="WorldClock.aspx.cs"
    Inherits="FlightPak.Web.Views.Utilities.WorldClock" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>World Clock</title>
    <link href="../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link media="screen" rel="stylesheet" type="text/css" href="../../Scripts/epiclock/stylesheet/jquery.epiclock.css" />
    <link media="screen" rel="stylesheet" type="text/css" href="../../Scripts/epiclock/renderers/retro/epiclock.retro.css" />
    <link media="screen" rel="stylesheet" type="text/css" href="../../Scripts/epiclock/renderers/retro-countdown/epiclock.retro-countdown.css" />
    <script type="text/javascript" src="../../Scripts/epiclock/javascript/jquery.min.js"></script>
    <script type="text/javascript" src="../../Scripts/epiclock/javascript/jquery.dateformat.js"></script>
    <script type="text/javascript" src="../../Scripts/epiclock/javascript/jquery.epiclock.js"></script>
    <script type="text/javascript" src="../../Scripts/epiclock/renderers/retro/epiclock.retro.js"></script>
    <script type="text/javascript" src="../../Scripts/epiclock/renderers/retro-countdown/epiclock.retro-countdown.js"></script>
    <script type="text/javascript" src="../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1WC" runat="server">
        <script type="text/javascript">
            function ShowAirportPopup(radWin) {
                var url;
                url = 'AirportWorldClockPopup.aspx';
                var grid = $find("<%=dgWorldClock.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var MasterSelectedItem = MasterTable.get_selectedItems();
                if (MasterSelectedItem.length != 0) {
                    var AirportID = MasterSelectedItem[0].getDataKeyValue('AirportID');
                    url = url + "?AirportID=" + AirportID;
                }
                var oWnd = radopen(url, radWin);
                return false;
            }
            function OnClientCloseAirportPopup(oWnd, args) {
                var arg = args.get_argument();
                RefreshGrid();
                return false;
            }
            function RefreshGrid() {
                var masterTable = $find("<%= dgWorldClock.ClientID %>").get_masterTableView();
                masterTable.rebind();
            }
            //this function is used to get dimension
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            //this function is used to get radwindow frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) {
                    oWindow = window.radWindow;
                }
                else if (window.frameElement.radWindow) {
                    oWindow = window.frameElement.radWindow;
                }
                return oWindow;
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1WC" runat="server" Skin="Sunset" />
    <telerik:RadAjaxManager ID="RadAjaxManager1WC" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dgWorldClock">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgWorldClock" LoadingPanelID="RadAjaxLoadingPanel1WC" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1WC" runat="server">
        <Windows>
            <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResize="GetDimensions" Height="470px" Width="1000px"
                OnClientClose="OnClientCloseAirportPopup" AutoSize="False" KeepInScreenBounds="true"
                 Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="AirportWorldClockPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" OnClientResize="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFBOCRUDPopup" runat="server" OnClientResize="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radHotelCRUDPopup" runat="server" OnClientResize="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radTransportCRUDPopup" runat="server" OnClientResize="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCateringCRUDPopup" runat="server" OnClientResize="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div style="margin: 0 auto; width: 980px;">
        <table width="100%" cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left">
                                <div class="tab-nav-top">
                                    <span class="head-title">World Clock</span> <span class="tab-nav-icons"></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="tblspace_10">
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellpadding="0" cellspacing="0" class="box1">
                        <tr>
                            <td>
                                <div id="WorldClockContainer" runat="server">
                                    <div id="divWCHeader" runat="server">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    UTC :
                                                </td>
                                                <td>
                                                    <dd id="utc-clock">
                                                    </dd>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnAddWorldClock" runat="server" CssClass="ui_nav" Text="Add World Clock"
                                                        OnClientClick="javascript:return ShowAirportPopup('radAirportPopup');return false;" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnDeleteWorldClock" runat="server" CssClass="ui_nav" Text="Delete World Clock"
                                                        OnClick="btnDeleteWorldClock_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <telerik:RadGrid ID="dgWorldClock" runat="server" AllowSorting="true" Visible="true"
                                        OnPageIndexChanged="MetroCity_PageIndexChanged" OnNeedDataSource="dgWorldClock_BindData"
                                        OnItemDataBound="dgWorldClock_ItemDataBound" Width="970px" Height="460px" AutoGenerateColumns="false"
                                        AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" Skin="Office2010Silver"
                                        AllowCustomPaging="false">
                                        <MasterTableView AllowPaging="false" NoDetailRecordsText="No records found in grid - Click the Select button to add records"
                                            CommandItemDisplay="Bottom" DataKeyNames="AirportID,CustomerID,IcaoID,CityName,StateName,CountryID,AirportName,IsWorldClock,DayLightSavingStartDT,DayLightSavingEndDT,DaylLightSavingStartTM,DayLightSavingEndTM"
                                            ClientDataKeyNames="AirportID,IsWorldClock">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="IcaoID" HeaderText="ICAO" UniqueName="ICAO" AutoPostBackOnFilter="true"
                                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="100px" />
                                                <telerik:GridBoundColumn DataField="CityName" HeaderText="City" AutoPostBackOnFilter="true"
                                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="130px" />
                                                <telerik:GridBoundColumn DataField="GeneralNotes" HeaderText="Local Time" ShowFilterIcon="false"
                                                    AllowFiltering="false" HeaderStyle-Width="200px" />
                                                <telerik:GridCheckBoxColumn DataField="IsDayLightSaving" HeaderText="DST On/Off"
                                                    AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="NoFilter"
                                                    AllowFiltering="false" HeaderStyle-Width="100px">
                                                </telerik:GridCheckBoxColumn>
                                                <telerik:GridBoundColumn DataField="StateName" HeaderText="State/ Prov" AutoPostBackOnFilter="true"
                                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="100px" />
                                                <telerik:GridBoundColumn DataField="CountryName" HeaderText="Country" AutoPostBackOnFilter="true"
                                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="150px" />
                                                <telerik:GridBoundColumn DataField="AirportName" HeaderText="Airport Name" AutoPostBackOnFilter="true"
                                                    ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="150px" />
                                            </Columns>
                                            <CommandItemTemplate>
                                            </CommandItemTemplate>
                                        </MasterTableView>
                                        <ClientSettings EnablePostBackOnRowClick="false">
                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                            <Selecting AllowRowSelect="true" />
                                        </ClientSettings>
                                        <GroupingSettings CaseSensitive="false" />
                                    </telerik:RadGrid>
                                </div>
                                <asp:HiddenField ID="hdnIsPopup" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
