﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using System.Drawing;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;


namespace FlightPak.Web.Views.Settings.Airports
{
    public partial class AirportWorldClockPopup : BaseSecuredPage
    {
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        private void Page_Init(object sender, System.EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["AirportID"]))
                        {
                            Int64 AirportID;
                            AirportID = Convert.ToInt64(Request.QueryString["AirportID"].ToString().ToUpper().Trim());
                            FlightPakMasterService.MasterCatalogServiceClient AirportService = new FlightPakMasterService.MasterCatalogServiceClient();
                            var ReturnValue = AirportService.GetAllAirportListForGrid(false);
                            if (ReturnValue.ReturnFlag == true)
                            {
                                int iIndex = 0;
                                int FullItemIndex = 0;
                                foreach (FlightPakMasterService.GetAllAirportListForGrid Item in ReturnValue.EntityList)
                                {
                                    if (AirportID.ToString().Trim().ToUpper() == Item.AirportID.ToString().Trim().ToUpper())
                                    {
                                        FullItemIndex = iIndex;
                                        break;
                                    }
                                    iIndex++;
                                }
                                int PageSize = dgAirport.PageSize;
                                int PageNumber = FullItemIndex / PageSize;
                                int ItemIndex = FullItemIndex % PageSize;
                                dgAirport.CurrentPageIndex = PageNumber;
                                int[] iItemIndexes;
                                iItemIndexes = new int[1];
                                iItemIndexes[0] = ItemIndex;
                                dgAirport.SelectedIndexes.Add(iItemIndexes);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgAirport_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        AirportWorldClock_BindData(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        private void AirportWorldClock_BindData(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (FlightPakMasterService.MasterCatalogServiceClient AirportService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    bool DisplayInactive = chkDisplayInactive.Checked;
                    bool DisplayEntryPoint = chkDisplayEntryPoint.Checked;
                    List<GetAllAirportListForGrid> GridAirportList = new List<GetAllAirportListForGrid>();
                    
                    if (DisplayEntryPoint == true)
                    {
                        GridAirportList = AirportService.GetAllAirportListForGrid(DisplayInactive).EntityList.Where(x => x.IsEntryPort != null &&
                                                                                                                         x.IsEntryPort == DisplayEntryPoint).ToList();
                    }
                    else
                    {
                        GridAirportList = AirportService.GetAllAirportListForGrid(DisplayInactive).EntityList;
                    }

                    if (GridAirportList.Count != 0)
                    {
                        dgAirport.DataSource = GridAirportList;
                    }
                    
                    if (IsDataBind)
                    {
                        dgAirport.DataBind();
                    }
                }
            }
        }
        private void FilterAirportWorldClock_BindData(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Request.QueryString["AirportID"] != null)
                {
                    foreach (GridDataItem Item in dgAirport.MasterTableView.Items)
                    {
                        if (Item.GetDataKeyValue("AirportID").ToString() == Request.QueryString["AirportID"].ToString())
                        {
                            Item.Selected = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgAirport;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (UtilitiesService.UtilitiesServiceClient objService1 = new UtilitiesService.UtilitiesServiceClient())
                        {
                            long AirportID = 0;
                            if (dgAirport.SelectedItems.Count > 0)
                            {
                                GridDataItem Item = dgAirport.SelectedItems[0] as GridDataItem;
                                AirportID = Convert.ToInt64(Item.GetDataKeyValue("AirportID").ToString().Trim());
                                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                                {
                                    CalculationService.CalculationServiceClient objCalculationServiceClient = new CalculationService.CalculationServiceClient();
                                    UtilitiesService.UtilitiesServiceClient objService = new UtilitiesService.UtilitiesServiceClient();
                                    var objAirport = objService.GetAllAirportForWorldClock().EntityList.Where(x => x.IsDeleted == false && x.IsWorldClock == true && x.IcaoID.ToString().Trim() == Item.GetDataKeyValue("IcaoID").ToString().Trim()).ToList();
                                    if (objAirport.Count > 0)
                                    {
                                        RadWindowManager1.RadAlert("Warning, Airport Code Already Exists in the World Clock.", 330, 100, "World Clock", null);
                                    }
                                    else
                                    {
                                        objService1.AddWorldClock(AirportID, true);
                                        Page.ClientScript.RegisterStartupScript(this.GetType(), "JQScript", "Close();", true);
                                    }
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.WorldClock);
                }
            }
        }

        protected void dgAirport_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    string resolvedurl = string.Empty;
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                GridDataItem item = dgAirport.SelectedItems[0] as GridDataItem;
                                Session["SelectedAirportID"] = item["AirportID"].Text;
                                TryResolveUrl("/Views/Settings/Airports/AirportCatalog.aspx?IsPopup=", out resolvedurl);
                                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radAirportCRUDPopup');", true);
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                TryResolveUrl("/Views/Settings/Airports/AirportCatalog.aspx?IsPopup=Add", out resolvedurl);
                                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radAirportCRUDPopup');", true);
                                break;
                            default:
                                break;
                        }
                        if (e.CommandName != null)
                        {
                            if ((Convert.ToString(e.CommandName) == "FBOView") || (Convert.ToString(e.CommandName) == "HotelView") || (Convert.ToString(e.CommandName) == "TransportView") || (Convert.ToString(e.CommandName) == "CateringView"))
                            {
                                if (dgAirport != null && dgAirport.Items != null && dgAirport.Items.Count > 0 && dgAirport.SelectedItems.Count > 0)
                                {
                                    if (e.CommandName != null)
                                    {
                                        if (Convert.ToString(e.CommandName) == "FBOView")
                                        {
                                            e.Canceled = true;
                                            e.Item.Selected = true;

                                            GridDataItem item = dgAirport.SelectedItems[0] as GridDataItem;
                                            string ICAO = item.GetDataKeyValue("AirportID").ToString();
                                            Session["SelectedAirportForFBOID"] = ICAO;
                                            TryResolveUrl("/Views/Settings/Logistics/FBOCatalog.aspx?AirportID=" + ICAO + "&&IsPopup=View", out resolvedurl);
                                            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radFBOCRUDPopup');", true);
                                        }
                                        if (Convert.ToString(e.CommandName) == "HotelView")
                                        {
                                            e.Canceled = true;
                                            e.Item.Selected = true;
                                            GridDataItem item = dgAirport.SelectedItems[0] as GridDataItem;
                                            string ICAO = item.GetDataKeyValue("AirportID").ToString();
                                            Session["SelectedAirportIDforHotelPopup"] = ICAO;
                                            TryResolveUrl("/Views/Settings/Logistics/HotelCatalog.aspx?AirportID=" + ICAO + "&&IsPopup=View", out resolvedurl);
                                            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radHotelCRUDPopup');", true);
                                        }
                                        if (Convert.ToString(e.CommandName) == "TransportView")
                                        {
                                            e.Canceled = true;
                                            e.Item.Selected = true;
                                            GridDataItem item = dgAirport.SelectedItems[0] as GridDataItem;
                                            string ICAO = item.GetDataKeyValue("AirportID").ToString();
                                            Session["SelectedAirportForTransportID"] = ICAO;
                                            TryResolveUrl("/Views/Settings/Logistics/TransportCatalog.aspx?AirportID=" + ICAO + "&&IsPopup=View", out resolvedurl);
                                            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radTransportCRUDPopup');", true);
                                        }
                                        if (Convert.ToString(e.CommandName) == "CateringView")
                                        {
                                            e.Canceled = true;
                                            e.Item.Selected = true;
                                            GridDataItem item = dgAirport.SelectedItems[0] as GridDataItem;
                                            string ICAO = item.GetDataKeyValue("AirportID").ToString();
                                            Session["SelectedAirportForCateringID"] = ICAO;
                                            TryResolveUrl("/Views/Settings/Logistics/CateringCatalog.aspx?AirportID=" + ICAO + "&&IsPopup=View", out resolvedurl);
                                            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radCateringCRUDPopup');", true);
                                        }
                                    }
                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("Please select a record from the above table.", 330, 100, "World Clock", null);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Account);
                }
            }
        }

        protected void dgAirport_DeleteCommand(object source, GridCommandEventArgs e)
        {
            string SelectedAirportID = string.Empty;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridDataItem item = dgAirport.SelectedItems[0] as GridDataItem;
                        SelectedAirportID = item["AirportID"].Text;
                        FlightPakMasterService.Airport AirportData = new FlightPakMasterService.Airport();
                        if (!string.IsNullOrEmpty(SelectedAirportID))
                        {
                            string Code = SelectedAirportID;
                            string CustomerID = "", IcaoID = "", UWAID = "";
                            foreach (GridDataItem Item in dgAirport.MasterTableView.Items)
                            {
                                if (Item["AirportID"].Text.Trim() == Code)
                                {
                                    if (Item.GetDataKeyValue("CustomerID") != null)
                                    {
                                        CustomerID = Item.GetDataKeyValue("CustomerID").ToString().Trim();
                                    }
                                    if (Item.GetDataKeyValue("IcaoID") != null)
                                    {
                                        IcaoID = Item.GetDataKeyValue("IcaoID").ToString().Trim();
                                    }
                                    if (Item.GetDataKeyValue("UWAID") != null)
                                    {
                                        UWAID = Item.GetDataKeyValue("UWAID").ToString().Trim();
                                    }
                                    break;
                                }
                            }
                            AirportData.IcaoID = IcaoID;
                            AirportData.IsDeleted = true;
                            if (!string.IsNullOrEmpty(UWAID))
                            {
                                AirportData.UWAID = UWAID;
                            }
                            AirportData.AirportID = Convert.ToInt64(SelectedAirportID);
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.Lock(EntitySet.Database.Airport, Convert.ToInt64(SelectedAirportID));
                                if (!returnValue.ReturnFlag)
                                {
                                    e.Item.Selected = true;
                                    ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Database.Airport);
                                    return;
                                }
                                using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FPKMasterService.DeleteAirport(AirportData);
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    dgAirport.Rebind();
                                    if (dgAirport.MasterTableView.Items.Count > 0)
                                    {
                                        dgAirport.SelectedIndexes.Add(0);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Airport);
                }
                finally
                {
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.Airport, Convert.ToInt64(SelectedAirportID));
                    }
                }
            }
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        AirportWorldClock_BindData(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.WorldClock);
                }
            }
        }
        protected void dgAirport_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridDataItem Item = dgAirport.SelectedItems[0] as GridDataItem;
                        Session["WCAirportID"] = Item["AirportID"].Text;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.WorldClock);
                }
            }
        }
        protected void chkDisplayInactive_CheckChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        AirportWorldClock_BindData(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.WorldClock);
                }
            }
        }
        protected void chkDisplayEntryPoint_CheckChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        AirportWorldClock_BindData(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.WorldClock);
                }
            }
        }

        protected void MetroCity_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgAirport.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }
    }
}
