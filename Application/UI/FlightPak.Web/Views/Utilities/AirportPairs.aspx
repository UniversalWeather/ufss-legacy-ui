﻿<%@ Page Title="Airport Pairs" Language="C#" AutoEventWireup="true" CodeBehind="AirportPairs.aspx.cs"
    Inherits="FlightPak.Web.Views.Utilities.AirportPairs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Airport Pairs</title>
    <link type="text/css" rel="stylesheet" href="../../Scripts/jquery.alerts.css" />
    <script type="text/javascript" src="../../Scripts/Common.js"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="../../Scripts/jquery.alerts.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableCdn="true">
    </telerik:RadScriptManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function ShowSuccessMessage() {
                $(document).ready(function () {
                    $("#tdSuccessMessage").css("display", "inline");
                    $('#tdSuccessMessage').delay(60000).fadeOut(60000);
                });
            }
        </script>
        <script type="text/javascript">
            function openWin(radWin) {
                var url = '';
                if (radWin == "radAircraftPopup") {
                    url = '../Settings/Fleet/AircraftPopup.aspx?AircraftCD=' + document.getElementById('<%=tbAPAircraft.ClientID%>').value;
                }
                else if (radWin == "radTypeCodePopup") {
                    if (document.getElementById('<%=hdnSaveAirportPairLegs.ClientID%>').value == "Update") {
                        url = '../Settings/Fleet/AircraftPopup.aspx?AircraftCD=' + document.getElementById('<%=tbAPLTypeCode.ClientID%>').value;
                    }
                    else {
                        url = '../Settings/Fleet/AircraftPopup.aspx?AircraftCD=';
                    }
                }
                else if (radWin == "radAirportDeparturePopup") {
                    url = '../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbAPLDeparture.ClientID%>').value;
                }
                else if (radWin == "radAirportArrivePopup") {
                    url = '../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbAPLArrive.ClientID%>').value;
                }
                else if (radWin == "radPowerPopup") {
                    url = 'PowerSettingPopup.aspx?AircraftID=' + document.getElementById('<%=hdnAPLTypeCodeID.ClientID%>').value;
                }
                else if (radWin == "radTOBiasPopup") {
                    url = 'AirportBias.aspx?AirportID=' + document.getElementById('<%=hdnAPLDepartureID.ClientID%>').value;
                }
                else if (radWin == "radLandBiasPopup") {
                    url = 'AirportBias.aspx?AirportID=' + document.getElementById('<%=hdnAPLArriveID.ClientID%>').value;
                }
                else if (radWin == "radWorldAirRoutePopup") {
                    url = 'WorldAirRoutes.aspx?DepartureAirportID=' + document.getElementById('<%=hdnAPLDepartureID.ClientID%>').value + '&ArriveAirportID=' + document.getElementById('<%=hdnAPLArriveID.ClientID%>').value;
                    window.open(url, 'WorldWinds', 'width=700,height=500,toolbar=no, location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes,top=' + (screen.height - 500) / 2 + ',left=' + (screen.width - 700) / 2);
                    return false;
                }
                var oWnd = radopen(url, radWin);
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function OnClientCloseAircraftPopup(oWnd, args) {
                var combo = $find("<%= tbAPAircraft.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbAPAircraft.ClientID%>").value = arg.AircraftCD;
                        document.getElementById("<%=hdnAPAircraftID.ClientID%>").value = arg.AircraftID;
                        document.getElementById("<%=cvAPAircraft.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbAPAircraft.ClientID%>").value = "";
                        document.getElementById("<%=hdnAPAircraftID.ClientID%>").value = "";
                        document.getElementById("<%=cvAPAircraft.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseTypeCode(oWnd, args) {
                var combo = $find("<%= tbAPLTypeCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=hdnAPLMultiTypeCodeID.ClientID%>").value = arg.AircraftID;
                        if (arg.AircraftCD.indexOf(",") != -1) {
                            arg.AircraftCD = arg.AircraftCD.substr(0, arg.AircraftCD.indexOf(","));
                        }
                        if (arg.AircraftID.indexOf(",") != -1) {
                            arg.AircraftID = arg.AircraftID.substr(0, arg.AircraftID.indexOf(","))
                        }
                        document.getElementById("<%=tbAPLTypeCode.ClientID%>").value = arg.AircraftCD;
                        document.getElementById("<%=hdnAPLTypeCodeID.ClientID%>").value = arg.AircraftID;
                        document.getElementById("<%=cvAPLTypeCode.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbAPLTypeCode.ClientID%>").setAttribute('title', "");
                        //document.getElementById("<%=lbAPLMultiTypeCodeCD.ClientID%>").innerText = "";
                        document.getElementById("<%=tbAPLTypeCode.ClientID%>").value = "";
                        document.getElementById("<%=hdnAPLTypeCodeID.ClientID%>").value = "";
                        document.getElementById("<%=cvAPLTypeCode.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnAPLMultiTypeCodeID.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
                var step = "tbAPLTypeCode_OnTextChanged";
                var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                if (step) {
                    ajaxManager.ajaxRequest(step);
                }
            }

            function OnClientCloseAirportDeparture(oWnd, args) {
                var combo = $find("<%= tbAPLDeparture.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbAPLDeparture.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=hdnAPLDepartureID.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=cvAPLDeparture.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbAPLDeparture.ClientID%>").value = "";
                        document.getElementById("<%=hdnAPLDepartureID.ClientID%>").value = "";
                        document.getElementById("<%=cvAPLDeparture.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
                var step = "tbAPLDeparture_OnTextChanged";
                var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                if (step) {
                    ajaxManager.ajaxRequest(step);
                }
            }


            function ShowReports(radWin, ReportFormat, ReportName) {
                document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
                document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
                if (ReportFormat == "EXPORT") {
                    url = "../../../Views/Reports/ExportReportInformation.aspx?Report=RptWeeklyCrew";
                    var oWnd = radopen(url, 'RadExportData');
                    return true;
                }
                else {
                    return true;
                }
            }

            function openReport(radWin) {
                var url = '';
                if (radWin == "AirportPairs") {
                    url = '../../../../Views/Reports/ExportReportInformation.aspx?Report=RptUTAirportPairs&P1=' + document.getElementById('<%=tbAPFileNumber.ClientID%>').value;
                }

                var oWnd = radopen(url, "RadExportData");
            }

            function OnClientCloseAirportArrive(oWnd, args) {
                var combo = $find("<%= tbAPLArrive.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbAPLArrive.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=hdnAPLArriveID.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=cvAPLArrive.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbAPLArrive.ClientID%>").value = "";
                        document.getElementById("<%=hdnAPLArriveID.ClientID%>").value = "";
                        document.getElementById("<%=cvAPLArrive.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
                var step = "tbAPLArrive_OnTextChanged";
                var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                if (step) {
                    ajaxManager.ajaxRequest(step);
                }
            }

            function OnClientCloseTOBiasPopup(oWnd, args) {
                var combo = $find("<%= tbAPLTOBias.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                    }
                    else {
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseLandBiasPopup(oWnd, args) {
                var combo = $find("<%= tbAPLLandBias.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                    }
                    else {
                        combo.clearSelection();
                    }
                }
            }
            function OnClientClosePowerPopup(oWnd, args) {
                var combo = $find("<%= tbAPLPower.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbAPLPower.ClientID%>").value = arg.PowerSettingID;
                        document.getElementById("<%=tbAPLTOBias.ClientID%>").value = arg.TakeOffBias;
                        document.getElementById("<%=tbAPLLandBias.ClientID%>").value = arg.LandingBias;
                        document.getElementById("<%=tbAPLTAS.ClientID%>").value = arg.TrueAirSpeed;
                    }
                    else {
                        combo.clearSelection();
                    }
                }
                var step = "tbAPLPower_OnTextChanged";
                var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                if (step) {
                    ajaxManager.ajaxRequest(step);
                }
            }
            function OnClientCloseWorldAirRoutePopup(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                    }
                    else {
                    }
                }
            }

            function AirportPaircallBackFn(confirmed) {
                if (confirmed) {
                    var grid = $find(window['gridId1']);
                    grid.get_masterTableView().fireCommand("DeleteSelected");
                }
            }
            function ProcessUpdateAirportPair() {
                var grid = $find(window['gridId1']);
                if (grid.get_masterTableView().get_selectedItems().length == 0) {
                    radalert('Please select a record from the above table.', 330, 100, "Edit", "");
                    return false;
                }
            }

            function ProcessDeleteAirportPair(customMsg) {
                //get reference to the client grid object using the global window variable set in the user control's Page_Load server handler
                var grid = $find(window['gridId1']);
                var msg = 'Are you sure you want to delete this record?';
                if (customMsg != null) {
                    msg = customMsg;
                }
                if (grid.get_masterTableView().get_selectedItems().length > 0) {

                    radconfirm(msg, AirportPaircallBackFn, 330, 100, '', 'Delete');
                    return false;
                }
                else {
                    radalert('Please select a record from the above table.', 330, 100, "Delete", "");
                    return false;
                }
            }
            function AirportPairLegscallBackFn(confirmed) {
                if (confirmed) {
                    var grid = $find(window['gridId2']);
                    grid.get_masterTableView().fireCommand("DeleteSelected");
                }
            }
            function ProcessUpdateAirportPairLegs() {
                var grid = $find(window['gridId2']);
                if (grid.get_masterTableView().get_selectedItems().length == 0) {
                    radalert('Please select a record from the above table.', 330, 100, "Edit", "");
                    return false;
                }
            }

            function ProcessDeleteAirportPairLegs(customMsg) {
                //get reference to the client grid object using the global window variable set in the user control's Page_Load server handler
                var grid = $find(window['gridId2']);
                var msg = 'Are you sure you want to delete this record?';
                if (customMsg != null) {
                    msg = customMsg;
                }
                if (grid.get_masterTableView().get_selectedItems().length > 0) {

                    radconfirm(msg, AirportPairLegscallBackFn, 330, 100, '', 'Delete');
                    return false;
                }
                else {
                    radalert('Please select a record from the above table.', 330, 100, "Delete", "");
                    return false;
                }
            }

            function CheckPowerSetting(ctrl, args) {
                var ReturnValue = false;
                var PowerSettingCallBackFn = Function.createDelegate(ctrl, function (shouldSubmit) {
                    return ReturnValue;
                });
                if ((ctrl.value >= 1) && (ctrl.value <= 3)) {
                    ReturnValue = true;
                }
                else {
                    radalert("Power Setting must be '1', '2', or '3'.", 360, 50, "Airport Pairs", PowerSettingCallBackFn);
                    ctrl.value = "1";
                    ReturnValue = false;
                }
                return ReturnValue;
            }

            function GridCreated(sender, args) {
                var scrollArea = sender.GridDataDiv;
                var parent = $get("DivExternalForm1");
                var gridHeader = sender.GridHeaderDiv;
                scrollArea.style.height = parent.clientHeight - gridHeader.clientHeight + "px";
            }

            function ShowAirportInfo() {
                var grid = $find("<%=dgAirportPairLegs.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var MasterSelectedItem = MasterTable.get_selectedItems();
                if (MasterSelectedItem.length != 0) {
                    var AirportID = MasterSelectedItem[0].getDataKeyValue('AAirportID');
                    url = "../Transactions/Preflight/PreflightAirportInfo.aspx?AirportID=" + AirportID;
                    window.radopen(url, "radAirportInfoPage");
                }
                return false;
            }

            function BiasValidation_onchange(sender, args) {
                var ReturnValue = true;
                var TitleName = "Airport Pair Legs";
                var BiasValue = sender.value;
                var CallBackFn = Function.createDelegate(sender, function (shouldSubmit) {
                    sender.focus();
                    return ReturnValue;
                });
                var SplitBias;
                if (document.getElementById("<%=hdnTenthMin.ClientID%>").value == "2") {
                    if (BiasValue != "") {
                        BiasValue = BiasValue.replace(".", ":");
                        sender.value = BiasValue.replace(".", ":");
                        if (BiasValue.indexOf(":") != -1) {
                            SplitBias = BiasValue.split(":");
                            if (SplitBias[1] != "") {
                                if ((parseInt(SplitBias[1]) >= 0) && (parseInt(SplitBias[1]) <= 59)) {
                                }
                                else {
                                    ReturnValue = false;
                                    radalert("You entered: " + SplitBias[1] + ". Minute entry must be between 0 and 59.", 360, 50, TitleName, CallBackFn);
                                    sender.value = 0 + ":00";
                                    args.cancel = true;
                                }
                            } else {
                                sender.value = SplitBias[0] + ":00";
                            }
                            if (SplitBias[1] == "") {
                                sender.value = BiasValue + "00";
                            }
                        }
                        else {
                            sender.value = BiasValue + ":00";
                        }
                    }
                }
                else {
                    if (BiasValue != "") {
                        BiasValue = BiasValue.replace(":", "");
                        sender.value = sender.value.replace(":", "");
                        if (BiasValue > 99) {
                            ReturnValue = false;
                            radalert("You entered: " + BiasValue + ". Invalid format. Required Format is NN.N", 360, 50, TitleName, CallBackFn);
                            sender.value = "0" + ".0";
                            args.cancel = true;
                        }
                        else {
                            if (BiasValue.indexOf(".") != -1) {
                                SplitBias = BiasValue.split(".");
                                if (SplitBias[0] != "") {
                                    if (SplitBias[0].length > 2) {
                                        ReturnValue = false;
                                        radalert("You entered: " + SplitBias[0] + ". Invalid format. Required Format is NN.N", 360, 50, TitleName, CallBackFn);
                                        sender.value = SplitBias[0] + ".0";
                                        args.cancel = true;
                                    }
                                }
                                if (SplitBias[1] != "") {
                                    if (SplitBias[1].length > 1) {
                                        ReturnValue = false;
                                        radalert("You entered: " + SplitBias[1] + ". Invalid format. Required Format is NN.N", 360, 50, TitleName, CallBackFn);
                                        sender.value = SplitBias[0] + ".0";
                                        args.cancel = true;
                                    }
                                    else {
                                        if ((parseInt(SplitBias[1]) >= 0) && (parseInt(SplitBias[1]) <= 9)) {
                                        }
                                        else {
                                            ReturnValue = false;
                                            radalert("You entered: " + SplitBias[1] + ". Invalid format. Required Format is NN.N", 360, 50, TitleName, CallBackFn);
                                            sender.value = SplitBias[0] + ".0";
                                            args.cancel = true;
                                        }
                                    }
                                } else {
                                    sender.value = SplitBias[0] + ".0";
                                }
                            }

                            else {
                                sender.value = BiasValue + ".0";
                            }
                        }
                    }
                }
                return ReturnValue;
            }
            
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="radAircraftPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseAircraftPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/AircraftPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radTypeCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseTypeCode" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/AircraftPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportDeparturePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseAirportDeparture" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airport/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportArrivePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseAirportArrive" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airport/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radTOBiasPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseTOBiasPopup" AutoSize="false" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Utilities/AirportBias.aspx"
                Width="600px" Height="180px">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPowerPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClosePowerPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Utilities/PowerSettingPopup.aspx"
                Height="190px" Width="400px" BackColor="#DADADA">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radLandBiasPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseLandBiasPopup" AutoSize="false" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Utilities/AirportBias.aspx"
                Width="600px" Height="180px">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radWorldAirRoutePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseWorldAirRoutePopup" AutoSize="false" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Utilities/WorldAirRoutes.aspx"
                Width="700px" Height="250px">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportInfoPage" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" AutoSize="true" Modal="true" ReloadOnShow="false" Behaviors="Close"
                VisibleStatusbar="false" Title="Airport Information">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadExportData" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Export Report Information" KeepInScreenBounds="true" Height="540px" Width="880px"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadShowReport" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Export Report Information" KeepInScreenBounds="true" Height="550px" Width="700px"
                Modal="true" Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <div style="margin: 0 auto; width: 980px;">
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td valign="top">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left">
                                <div class="tab-nav-top">
                                    <span class="head-title">Airport Pairs</span> <span class="tab-nav-icons">
                                        <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:ShowReports('','PDF','RptUTAirportPairs');"
                                            title="Preview Report" OnClick="btnShowReports_OnClick" class="search-icon"></asp:LinkButton>
                                        <asp:LinkButton ID="lbtnSaveReports" runat="server" title="Export Report" OnClientClick="javascript:openReport('AirportPairs');return false;"
                                            class="save-icon"></asp:LinkButton>
                                        <%--OnClientClick="javascript:ShowReports('','EXPORT','RptUTAirportPairs');return false;"--%>
                                        <asp:Button ID="btnShowReports" runat="server" CssClass="button-disable" Style="display: none;" />
                                        <a href="#" title="Help" class="help-icon"></a></span>
                                    <asp:HiddenField ID="hdnReportName" runat="server" />
                                    <asp:HiddenField ID="hdnReportFormat" runat="server" />
                                    <asp:HiddenField ID="hdnReportParameters" runat="server" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div id="DivExternalForm" runat="server" class="ExternalForm">
                        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" class="tdLabel160">
                                        <div id="tdSuccessMessage" class="success_msg">
                                            Record saved successfully.</div>
                                    </td>
                                    <td align="right">
                                        <div class="mandatory">
                                            <span>Bold</span> Indicates required field</div>
                                    </td>
                                </tr>
                            </table>
                            <telerik:RadPanelBar ID="RadPanelBarAirportPairs" Width="100%" ExpandAnimation-Type="None"
                                CollapseAnimation-Type="none" runat="server">
                                <Items>
                                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Airport Pairs" CssClass="PanelHeaderStyle">
                                        <Items>
                                            <telerik:RadPanelItem>
                                                <ContentTemplate>
                                                    <telerik:RadGrid ID="dgAirportPairs" runat="server" AllowSorting="true" AutoGenerateColumns="false"
                                                        CssClass="grid_auto" OnPageIndexChanged="MetroCity_PageIndexChanged" AllowPaging="true"
                                                        AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" OnNeedDataSource="dgAirportPairs_BindData"
                                                        OnItemCommand="dgAirportPairs_ItemCommand" OnInsertCommand="dgAirportPairs_InsertCommand"
                                                        OnUpdateCommand="dgAirportPairs_UpdateCommand" OnSelectedIndexChanged="dgAirportPairs_SelectedIndexChanged"
                                                        OnDeleteCommand="dgAirportPairs_DeleteCommand" OnItemDataBound="dgAirportPairs_ItemDataBound">
                                                        <MasterTableView ClientDataKeyNames="AirportPairID" DataKeyNames="AirportPairID,CustomerID,AirportPairNumber,AircraftID,WindQTR,AirportPairDescription,
                    LastUpdUID,LastUpdTS,IsDeleted,AircraftCD,AircraftDescription" CommandItemDisplay="Bottom">
                                                            <Columns>
                                                                <telerik:GridBoundColumn DataField="AirportPairNumber" HeaderText="File No." UniqueName="AirportPairNumber"
                                                                    HeaderStyle-Width="150px" CurrentFilterFunction="EqualTo" DataType="System.Int64"
                                                                    ShowFilterIcon="false" AutoPostBackOnFilter="true" />
                                                                <telerik:GridBoundColumn DataField="AircraftCD" HeaderText="Aircraft Type" UniqueName="AircraftCD"
                                                                    CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                    HeaderStyle-Width="200px" />
                                                                <telerik:GridBoundColumn DataField="WindQTR" HeaderText="Wind Period" UniqueName="WindQTR"
                                                                    CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                    HeaderStyle-Width="200px" />
                                                                <telerik:GridBoundColumn DataField="AirportPairDescription" UniqueName="AirportPairDescription"
                                                                    HeaderText="Description" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                                                    AutoPostBackOnFilter="true" />
                                                            </Columns>
                                                            <CommandItemTemplate>
                                                                <div class="grid_icon">
                                                                    <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                                                        CommandName="InitInsert"> </asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdateAirportPair();"
                                                                        ToolTip="Edit" CssClass="edit-icon-grid" CommandName="Edit"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDeleteAirportPair();"
                                                                        CommandName="DeleteSelected" CssClass="delete-icon-grid" ToolTip="Delete"></asp:LinkButton>
                                                                </div>
                                                                <div>
                                                                    <asp:Label ID="lbLastUpdatedUser" runat="server" class="last-updated-text"></asp:Label>
                                                                </div>
                                                            </CommandItemTemplate>
                                                        </MasterTableView>
                                                        <ClientSettings EnablePostBackOnRowClick="true">
                                                            <Scrolling AllowScroll="true" UseStaticHeaders="false" />
                                                            <Selecting AllowRowSelect="true" />
                                                        </ClientSettings>
                                                        <GroupingSettings CaseSensitive="false" />
                                                    </telerik:RadGrid>
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <div class="tblspace_10">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table class="border-box">
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td class="tdLabel100" valign="top">
                                                                                        <span class="mnd_text">File No.</span>
                                                                                    </td>
                                                                                    <td class="tdLabel130" valign="top">
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="tbAPFileNumber" runat="server" CssClass="text80" MaxLength="4"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hdnAPAirportPairID" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td class="tdLabel100" valign="top">
                                                                                        <span class="mnd_text">Aircraft Type</span>
                                                                                    </td>
                                                                                    <td class="tdLabel130" valign="top">
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="tbAPAircraft" runat="server" CssClass="text80" MaxLength="10" AutoPostBack="true"
                                                                                                        OnTextChanged="tbAPAircraft_OnTextChanged"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hdnAPAircraftID" runat="server" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Button ID="btnBrowseAircraft" OnClientClick="javascript:openWin('radAircraftPopup');return false;"
                                                                                                        CssClass="browse-button" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:RequiredFieldValidator ID="rfvAPAircraft" runat="server" ErrorMessage="Aircraft Type Code is Required."
                                                                                                        Display="Dynamic" CssClass="alert-text" ControlToValidate="tbAPAircraft" ValidationGroup="SaveAirportPair">
                                                                                                    </asp:RequiredFieldValidator>
                                                                                                    <asp:CustomValidator ID="cvAPAircraft" runat="server" ControlToValidate="tbAPAircraft"
                                                                                                        ErrorMessage="Invalid Aircraft Type Code." Display="Dynamic" CssClass="alert-text"
                                                                                                        ValidationGroup="SaveAirportPair" SetFocusOnError="true"></asp:CustomValidator>
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td class="tdLabel80" valign="top">
                                                                                        <span class="mnd_text">Description</span>
                                                                                    </td>
                                                                                    <td valign="top" class="tdLabel160">
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:TextBox ID="tbAPDescription" runat="server" CssClass="text130" MaxLength="40"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:RequiredFieldValidator ID="rfvAPDescription" runat="server" ErrorMessage="Description is Required."
                                                                                                        Display="Dynamic" CssClass="alert-text" ControlToValidate="tbAPDescription" ValidationGroup="SaveAirportPair">
                                                                                                    </asp:RequiredFieldValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td class="tdLabel80" valign="top">
                                                                                        <span>Wind Period</span>
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:DropDownList ID="ddlAPQuarter" runat="server" CssClass="text130" ClientIDMode="Static">
                                                                                                        <asp:ListItem Text="1st (Dec, Jan, Feb)" Value="1" />
                                                                                                        <asp:ListItem Text="2nd (Mar, Apr, May)" Value="2" />
                                                                                                        <asp:ListItem Text="3rd (Jun, Jul, Aug)" Value="3" />
                                                                                                        <asp:ListItem Text="4th (Sep, Oct, Nov)" Value="4" />
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <div class="tblspace_10">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </telerik:RadPanelItem>
                                        </Items>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                            <table cellspacing="0" cellpadding="0" class="tblButtonArea" style="display: none;">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSaveAirportPairs" runat="server" CssClass="button" Text="Save"
                                            ValidationGroup="SaveAirportPair" OnClick="btnSaveAirportPairs_OnClick" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnCancelAirportPairs" runat="server" CssClass="button" Text="Cancel"
                                            CausesValidation="false" OnClick="btnCancelAirportPairs_OnClick" />
                                        <asp:HiddenField ID="hdnSaveAirportPairs" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td>
                                        <div class="tblspace_10">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <telerik:RadPanelBar ID="pnlbarAirportPairLegs" Width="100%" ExpandAnimation-Type="None"
                                CollapseAnimation-Type="none" runat="server">
                                <Items>
                                    <telerik:RadPanelItem runat="server" Expanded="true" Text="Legs Worksheet" CssClass="PanelHeaderStyle">
                                        <Items>
                                            <telerik:RadPanelItem>
                                                <ContentTemplate>
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <telerik:RadGrid ID="dgAirportPairLegs" runat="server" AllowSorting="true" AutoGenerateColumns="false"
                                                                                OnPageIndexChanged="AirportPairLegs_PageIndexChanged" AllowPaging="true" CssClass="grid_auto"
                                                                                AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" Width="968px" OnNeedDataSource="dgAirportPairLegs_BindData"
                                                                                OnItemCommand="dgAirportPairLegs_ItemCommand" OnInsertCommand="dgAirportPairLegs_InsertCommand"
                                                                                OnUpdateCommand="dgAirportPairLegs_UpdateCommand" OnSelectedIndexChanged="dgAirportPairLegs_SelectedIndexChanged"
                                                                                OnDeleteCommand="dgAirportPairLegs_DeleteCommand" OnItemDataBound="dgAirportPairLegs_ItemDataBound">
                                                                                <MasterTableView AllowPaging="false" ShowFooter="false" ClientDataKeyNames="AirportPairDetailID,AAirportID"
                                                                                    DataKeyNames="AirportPairDetailID,CustomerID,AirportPairID,LegID,
                                                                    AircraftID,QTR,DAirportID,AAirportID,Distance,PowerSetting,TakeoffBIAS,LandingBIAS,TrueAirSpeed,Winds,
                                                                    ElapseTM,Cost,WindReliability,LastUpdUID,LastUpdTS,IsDeleted,AirportPairNumber,AirportPairDescription,
                                                                    AirportPairAircraftID,AircraftCD,AircraftDescription,DAirportIcaoID,DAirportCityName,DAirportCountryName,
                                                                    AAirportIcaoID,AAirportCityName,AAirportCountryName,DAirportAlerts,AAirportAlerts" CommandItemDisplay="Bottom">
                                                                                    <Columns>
                                                                                        <telerik:GridBoundColumn DataField="AircraftCD" HeaderText="Type Code" UniqueName="AircraftCD"
                                                                                            CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                                            HeaderStyle-Width="80px" />
                                                                                        <telerik:GridBoundColumn DataField="QTR" HeaderText="Wind Period" UniqueName="QTR"
                                                                                            CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                                            HeaderStyle-Width="50px" FilterControlWidth="30px" />
                                                                                        <telerik:GridBoundColumn DataField="DAirportIcaoID" HeaderText="Departure" UniqueName="DAirportIcaoID"
                                                                                            CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                                            HeaderStyle-Width="70px" FilterControlWidth="50px" />
                                                                                        <telerik:GridBoundColumn DataField="AAirportIcaoID" HeaderText="Arrival" UniqueName="AAirportIcaoID"
                                                                                            CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                                            HeaderStyle-Width="60px" FilterControlWidth="40px" />
                                                                                        <telerik:GridBoundColumn DataField="Distance" HeaderText="Miles" UniqueName="Distance"
                                                                                            CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                                            HeaderStyle-Width="60px" FilterControlWidth="40px" />
                                                                                        <telerik:GridBoundColumn DataField="PowerSetting" HeaderText="Power" UniqueName="PowerSetting"
                                                                                            CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                                            HeaderStyle-Width="50px" FilterControlWidth="30px" />
                                                                                        <telerik:GridBoundColumn DataField="TakeoffBIAS" HeaderText="T/O Bias" UniqueName="TakeoffBIAS"
                                                                                            CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                                            HeaderStyle-Width="90px" FilterControlWidth="70px" />
                                                                                        <telerik:GridBoundColumn DataField="LandingBIAS" HeaderText="Landing Bias" UniqueName="LandingBIAS"
                                                                                            CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                                            HeaderStyle-Width="90px" FilterControlWidth="70px" />
                                                                                        <telerik:GridBoundColumn DataField="TrueAirSpeed" HeaderText="TAS" UniqueName="TrueAirSpeed"
                                                                                            CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                                            HeaderStyle-Width="60px" FilterControlWidth="40px" />
                                                                                        <telerik:GridBoundColumn DataField="Winds" HeaderText="Winds" UniqueName="Winds"
                                                                                            CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                                            HeaderStyle-Width="70px" FilterControlWidth="40px" />
                                                                                        <telerik:GridBoundColumn DataField="ElapseTM" HeaderText="ETE" UniqueName="ElapseTM"
                                                                                            CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                                                                            HeaderStyle-Width="100px" />
                                                                                        <telerik:GridBoundColumn DataField="Cost" HeaderText="Cost" UniqueName="Cost" CurrentFilterFunction="Contains"
                                                                                            ShowFilterIcon="false" AutoPostBackOnFilter="true" HeaderStyle-Width="100px" />
                                                                                    </Columns>
                                                                                    <CommandItemTemplate>
                                                                                        <div class="grid_icon">
                                                                                            <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                                                                                CommandName="InitInsert"></asp:LinkButton>
                                                                                            <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdateAirportPairLegs();"
                                                                                                ToolTip="Edit" CssClass="edit-icon-grid" CommandName="Edit"></asp:LinkButton>
                                                                                            <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDeleteAirportPairLegs();"
                                                                                                CommandName="DeleteSelected" CssClass="delete-icon-grid" ToolTip="Delete"></asp:LinkButton>
                                                                                        </div>
                                                                                        <div>
                                                                                            <asp:Label ID="lbLastUpdatedUser" runat="server" class="last-updated-text"></asp:Label>
                                                                                        </div>
                                                                                    </CommandItemTemplate>
                                                                                </MasterTableView>
                                                                                <ClientSettings EnablePostBackOnRowClick="true">
                                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                                    <Selecting AllowRowSelect="true" />
                                                                                </ClientSettings>
                                                                                <GroupingSettings CaseSensitive="false" />
                                                                            </telerik:RadGrid>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="tblspace_10">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table class="border-box">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td class="tdLabel100" valign="top">
                                                                                                    <span class="mnd_text">Type Code</span>
                                                                                                </td>
                                                                                                <td class="tdLabel130" valign="top">
                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="tbAPLTypeCode" runat="server" CssClass="text80" MaxLength="10" AutoPostBack="true"
                                                                                                                    OnTextChanged="tbAPLTypeCode_OnTextChanged"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="hdnAPLTypeCodeID" runat="server" />
                                                                                                                <asp:HiddenField ID="hdnAPLMultiTypeCodeID" runat="server" />
                                                                                                                <asp:Button ID="btnBrowseAPLTypeCode" OnClientClick="javascript:openWin('radTypeCodePopup');return false;"
                                                                                                                    CssClass="browse-button" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="lbAPLMultiTypeCodeCD" CssClass="input_no_bg" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:RequiredFieldValidator ID="rfvAPLTypeCode" runat="server" ErrorMessage="Type Code is Required."
                                                                                                                    Display="Dynamic" CssClass="alert-text" ControlToValidate="tbAPLTypeCode" ValidationGroup="SaveAirportPairLegs">
                                                                                                                </asp:RequiredFieldValidator>
                                                                                                                <asp:CustomValidator ID="cvAPLTypeCode" runat="server" ControlToValidate="tbAPLTypeCode"
                                                                                                                    ErrorMessage="Invalid Aircraft Type Code." Display="Dynamic" CssClass="alert-text"
                                                                                                                    ValidationGroup="SaveAirportPairLegs" SetFocusOnError="true"></asp:CustomValidator>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td class="tdLabel80" valign="top">
                                                                                                    <span class="mnd_text">File No.</span>
                                                                                                </td>
                                                                                                <td class="tdLabel130" valign="top">
                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="tbAPLFileNumber" runat="server" CssClass="text80" MaxLength="4"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="hdnAPLAirportPairLegID" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td class="tdLabel80" valign="top">
                                                                                                    <span class="mnd_text">Description</span>
                                                                                                </td>
                                                                                                <td valign="top" class="tdLabel160">
                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="tbAPLDescription" runat="server" Width="125px" MaxLength="40"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td class="tdLabel70" valign="top">
                                                                                                    <span>Total Cost</span>
                                                                                                </td>
                                                                                                <td valign="top">
                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="tbAPLTotalCost" runat="server" CssClass="text80" MaxLength="4"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="tblspace_10">
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td class="tdLabel100" valign="top">
                                                                                                    <span class="mnd_text">Departure</span>
                                                                                                </td>
                                                                                                <td class="tdLabel130" valign="top">
                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="tbAPLDeparture" runat="server" CssClass="text80" MaxLength="4" AutoPostBack="true"
                                                                                                                    OnTextChanged="tbAPLDeparture_OnTextChanged"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="hdnAPLDepartureID" runat="server" />
                                                                                                                <asp:Button ID="btnBrowseAPLDeparture" OnClientClick="javascript:openWin('radAirportDeparturePopup');return false;"
                                                                                                                    CssClass="browse-button" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="lbAPLDepartDesc" CssClass="input_no_bg" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:RequiredFieldValidator ID="rfvAPLDeparture" runat="server" ErrorMessage="Departure Airport ICAO Id is Required."
                                                                                                                    Display="Dynamic" CssClass="alert-text" ControlToValidate="tbAPLDeparture" ValidationGroup="SaveAirportPairLegs">
                                                                                                                </asp:RequiredFieldValidator>
                                                                                                                <asp:CustomValidator ID="cvAPLDeparture" runat="server" ControlToValidate="tbAPLDeparture"
                                                                                                                    ErrorMessage="Invalid Departure Airport ICAO Id." Display="Dynamic" CssClass="alert-text"
                                                                                                                    ValidationGroup="SaveAirportPairLegs" SetFocusOnError="true"></asp:CustomValidator>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td class="tdLabel80" valign="top">
                                                                                                    <span class="mnd_text">Arrival</span>
                                                                                                </td>
                                                                                                <td class="tdLabel130" valign="top">
                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="tbAPLArrive" runat="server" CssClass="text80" MaxLength="4" AutoPostBack="true"
                                                                                                                    OnTextChanged="tbAPLArrive_OnTextChanged"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="hdnAPLArriveID" runat="server" />
                                                                                                                <asp:Button ID="btnBrowseAPLArrive" OnClientClick="javascript:openWin('radAirportArrivePopup');return false;"
                                                                                                                    CssClass="browse-button" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:Label ID="lbAPLAirriveDesc" CssClass="input_no_bg" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:RequiredFieldValidator ID="rfvAPLArrive" runat="server" ErrorMessage="Arrive Airport ICAO Id is Required."
                                                                                                                    Display="Dynamic" CssClass="alert-text" ControlToValidate="tbAPLArrive" ValidationGroup="SaveAirportPairLegs">
                                                                                                                </asp:RequiredFieldValidator>
                                                                                                                <asp:CustomValidator ID="cvAPLArrive" runat="server" ControlToValidate="tbAPLArrive"
                                                                                                                    ErrorMessage="Invalid Arrive Airport ICAO Id." Display="Dynamic" CssClass="alert-text"
                                                                                                                    ValidationGroup="SaveAirportPairLegs" SetFocusOnError="true"></asp:CustomValidator>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td class="tdLabel80" valign="top">
                                                                                                    <span class="mnd_text">Wind Period</span>
                                                                                                </td>
                                                                                                <td valign="top">
                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:DropDownList ID="ddlAPLQuarter" runat="server" AutoPostBack="true" CssClass="text130"
                                                                                                                    ClientIDMode="Static" OnSelectedIndexChanged="ddlAPLQuarter_OnSelectedIndexChanged">
                                                                                                                    <asp:ListItem Text="1st (Dec, Jan, Feb)" Value="1" Selected="True" />
                                                                                                                    <asp:ListItem Text="2nd (Mar, Apr, May)" Value="2" />
                                                                                                                    <asp:ListItem Text="3rd (Jun, Jul, Aug)" Value="3" />
                                                                                                                    <asp:ListItem Text="4th (Sep, Oct, Nov)" Value="4" />
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="tblspace_10">
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <fieldset>
                                                                                            <legend>Distance & Time</legend>
                                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                            <tr>
                                                                                                                <td valign="top" class="tdLabel90">
                                                                                                                    <span class="mnd_text">
                                                                                                                        <asp:Label ID="lbAPLMiles" runat="server" Text="Miles(N)" /></span>
                                                                                                                </td>
                                                                                                                <td valign="top" class="tdLabel130">
                                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <asp:HiddenField runat="server" ID="hdAPLMiles" />
                                                                                                                                <asp:TextBox ID="tbAPLMiles" runat="server" CssClass="text80" MaxLength="5" onKeyPress="return fnAllowNumeric(this, event);"
                                                                                                                                    AutoPostBack="true" OnTextChanged="tbAPLMiles_OnTextChanged"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                                <td valign="top" class="tdLabel80">
                                                                                                                    <span class="mnd_text">T/O Bias</span>
                                                                                                                </td>
                                                                                                                <td valign="top" class="tdLabel130">
                                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <asp:TextBox ID="tbAPLTOBias" runat="server" CssClass="text80" MaxLength="6" AutoPostBack="true"
                                                                                                                                    onKeyPress="return fnAllowNumericAndChar(this, event, '.:');" onblur="javascript:return BiasValidation_onchange(this, event);"
                                                                                                                                    OnTextChanged="tbAPLTOBias_OnTextChanged"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                            <td>
                                                                                                                                <asp:Button ID="imgbtnAPLTOBias" ToolTip="" runat="server" OnClientClick="javascript:openWin('radTOBiasPopup');return false;"
                                                                                                                                    CssClass="calc-icon" />
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td colspan="2">
                                                                                                                                <asp:RegularExpressionValidator ID="regAPLTOBias" runat="server" ErrorMessage="Invalid Format"
                                                                                                                                    Display="Dynamic" CssClass="alert-text" ControlToValidate="tbAPLTOBias" ValidationGroup="SaveAirportPairLegs"
                                                                                                                                    ValidationExpression="^[0-9]{0,3}\.?[0-9]{0,2}$" />
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                                <td valign="top" class="tdLabel80">
                                                                                                                    <span>TAS</span>
                                                                                                                </td>
                                                                                                                <td valign="top" class="tdLabel100">
                                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <asp:TextBox ID="tbAPLTAS" runat="server" CssClass="text80" MaxLength="5" AutoPostBack="true"
                                                                                                                                    onKeyPress="return fnAllowNumeric(this, event);" OnTextChanged="tbAPLTAS_OnTextChanged"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <span>Wind Reliability</span>
                                                                                                                </td>
                                                                                                                <td align="left" valign="top">
                                                                                                                    <asp:RadioButtonList ID="rblistAPLWindReliability" runat="server" RepeatDirection="Horizontal"
                                                                                                                        AutoPostBack="true" OnSelectedIndexChanged="rblistAPLWindReliability_OnSelectedIndexChanged">
                                                                                                                        <asp:ListItem Value="1">50%</asp:ListItem>
                                                                                                                        <asp:ListItem Value="2">75%</asp:ListItem>
                                                                                                                        <asp:ListItem Value="3" Selected="True">85%</asp:ListItem>
                                                                                                                    </asp:RadioButtonList>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                            <tr>
                                                                                                                <td valign="top" class="tdLabel90">
                                                                                                                    <span class="mnd_text">Power</span>
                                                                                                                </td>
                                                                                                                <td valign="top" class="tdLabel130">
                                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <asp:TextBox ID="tbAPLPower" runat="server" CssClass="text80" MaxLength="1" AutoPostBack="true"
                                                                                                                                    onKeyPress="return fnAllowNumeric(this, event);" OnTextChanged="tbAPLPower_OnTextChanged"
                                                                                                                                    onblur="return CheckPowerSetting(this, event);"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                            <td>
                                                                                                                                <asp:Button ID="btnBrowseAPLPower" OnClientClick="javascript:openWin('radPowerPopup');return false;"
                                                                                                                                    CssClass="browse-button" runat="server" />
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                                <td valign="top" class="tdLabel80">
                                                                                                                    <span class="mnd_text">Landing Bias</span>
                                                                                                                </td>
                                                                                                                <td valign="top" class="tdLabel130">
                                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <asp:TextBox ID="tbAPLLandBias" runat="server" CssClass="text80" MaxLength="6" AutoPostBack="true"
                                                                                                                                    onKeyPress="return fnAllowNumericAndChar(this, event, '.:');" onblur="javascript:return BiasValidation_onchange(this, event);"
                                                                                                                                    OnTextChanged="tbAPLLandBias_OnTextChanged"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                            <td>
                                                                                                                                <asp:Button ID="btnBrowseAPLLandBias" ToolTip="" runat="server" OnClientClick="javascript:openWin('radLandBiasPopup');return false;"
                                                                                                                                    CssClass="calc-icon" />
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <asp:RegularExpressionValidator ID="regAPLLandBias" runat="server" ErrorMessage="Invalid Format"
                                                                                                                                    Display="Dynamic" CssClass="alert-text" ControlToValidate="tbAPLLandBias" ValidationGroup="SaveAirportPairLegs"
                                                                                                                                    ValidationExpression="^[0-9]{0,3}\.?[0-9]{0,2}$" />
                                                                                                                            </td>
                                                                                                                            <td>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                                <td valign="top" class="tdLabel80">
                                                                                                                    Winds
                                                                                                                </td>
                                                                                                                <td valign="top" class="tdLabel100">
                                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <asp:TextBox ID="tbAPLWinds" runat="server" CssClass="text80" MaxLength="4" onKeyPress="return fnAllowNumeric(this, event);"
                                                                                                                                    AutoPostBack="true" OnTextChanged="rblistAPLWindReliability_OnSelectedIndexChanged"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                                <td class="tdLabel40" valign="top">
                                                                                                                    <span class="mnd_text">ETE</span>
                                                                                                                </td>
                                                                                                                <td class="tdLabel130" valign="top">
                                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <asp:TextBox ID="tbAPLEte" runat="server" CssClass="text80" MaxLength="8" AutoPostBack="true"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                                <td class="tdLabel40" valign="top">
                                                                                                                    <span>Cost</span>
                                                                                                                </td>
                                                                                                                <td valign="top">
                                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <asp:TextBox ID="tbAPLCost" runat="server" CssClass="text80" MaxLength="10" AutoPostBack="true"></asp:TextBox>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </fieldset>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table cellspacing="0" cellpadding="0" class="tblButtonArea" width="100%">
                                                                                <tr>
                                                                                    <td align="left">
                                                                                        <asp:Button ID="btnAPLWinds" CssClass="button" Text="Winds" runat="server" OnClientClick="javascript:openWin('radWorldAirRoutePopup');return false;" />
                                                                                        <asp:Button ID="btnIPLAlerts" CssClass="button" Text="Alerts" runat="server" OnClientClick="javascript:ShowAirportInfo(); return false;"
                                                                                            Style="display: none;" />
                                                                                    </td>
                                                                                    <td align="right">
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Button ID="btnSaveAll" runat="server" CssClass="button" Text="Save" ValidationGroup="SaveAirportPair"
                                                                                                        OnClick="btnSaveAll_OnClick" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:Button ID="btnCancelAll" runat="server" CssClass="button" Text="Cancel" CausesValidation="false"
                                                                                                        OnClick="btnCancelAll_OnClick" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td align="right" style="display: none;">
                                                                                        <asp:Button ID="btnSaveAirportPairLegs" CssClass="button" Text="Save" runat="server"
                                                                                            ValidationGroup="SaveAirportPairLegs" OnClick="btnSaveAirportPairLegs_OnClick" />
                                                                                        <asp:Button ID="btnCancelAirportPairLegs" Text="Cancel" CssClass="button" runat="server"
                                                                                            CausesValidation="false" OnClick="btnCancelAirportPairLegs_OnClick" />
                                                                                        <asp:HiddenField ID="hdnSaveAirportPairLegs" runat="server" />
                                                                                        <asp:HiddenField ID="hdnTenthMin" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </telerik:RadPanelItem>
                                        </Items>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelBar>
                        </asp:Panel>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
