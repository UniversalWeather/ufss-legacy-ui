﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AirportBias.aspx.cs" Inherits="FlightPak.Web.Views.Utilities.AirportBias"
    ClientIDMode="AutoID" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Airport Bias</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">                       
        </script>
    </telerik:RadCodeBlock>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <table class="box1">
        <tr>
            <td class="tdLabel80">
                ICAO
            </td>
            <td class="tdLabel120">
                City
            </td>
            <td class="tdLabel80">
                State
            </td>
            <td class="tdLabel150">
                Country
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="tbICAO" runat="server" />
            </td>
            <td>
                <asp:Label ID="tbCity" runat="server" />
            </td>
            <td>
                <asp:Label ID="tbState" runat="server" />
            </td>
            <td>
                <asp:Label ID="tbCountry" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                Airport
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="tbAirport" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <div class="nav-space">
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <table cellpadding="0" width="100%" cellspacing="0" class="preflight-box-noedit">
                    <tr>
                        <td class="tdLabel100">
                            Takeoff Bias
                        </td>
                        <td class="tdLabel40">
                            <asp:Label ID="tbTakeoffBias" runat="server" />
                        </td>
                        <td class="tdLabel100">
                            Landing Bias
                        </td>
                        <td>
                            <asp:Label ID="tbLandingBias" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
