﻿using FlightPak.Web.GridHelpers;
using FlightPak.Web.GridHelpers.CoreApiFormatters;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using FlightPak.Web.Framework.Constants;


namespace FlightPak.Web.Views.Utilities
{
    public partial class ApiCallerWithFilter : BaseSecuredPage
    {
        readonly string responseString;
        readonly String coreApiUrl = "";
        public ApiCallerWithFilter()
        {
            coreApiUrl = ConfigurationManager.AppSettings["CoreApiServer"];
            if (String.IsNullOrEmpty(coreApiUrl))
                coreApiUrl = "http://core-dev.universalweather.rdn/";
        }

        public ApiCallerWithFilter(string dataObject)
        {
            responseString = dataObject;
        }

        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            exManager.Process(() =>
            {
                ApiCallerWithFilter apiCallerWithFilterObj;
                String apiType = this.Request["apiType"];
                String apiMethod = this.Request["method"];
                String addCustomerIdParam = this.Request["sendCustomerId"];
                String addIcaoIdParam = this.Request["sendIcaoId"];
                String sort = this.Request["sort"];
                String dir = this.Request["dir"];
                StringBuilder sbParams = new StringBuilder();
                sbParams.Append("?");
                foreach (String key in Request.QueryString)
                {

                    if (key.ToLower() != "filters" && key.ToLower() != "sort" && key.ToLower() != "dir" && key.ToLower() != "apitype" && key.ToLower() != "method" && key.ToLower() != "sendcustomerid" && key.ToLower() != "sendicaoid")
                        sbParams.AppendFormat("{0}={1}&", key, Request.Params[key]);

                    else if (key.ToLower() == "filters")
                    {
                        //This will be changed
                        String strFilter = Request.Params[key];
                        List<Filter> listFilters = jqGridFilterHelper.GetCoreApiCompatibleFilters(strFilter);
                        foreach (var filter in listFilters)
                        {
                            if (apiMethod == "EmergencyContactList" && filter.Field == "FirstName")
                                filter.Field = "lfsname";
                        }
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        String formattedGridFilter = serializer.Serialize(listFilters);
                        string filters = serializer.Serialize(listFilters);           
                        sbParams.AppendFormat("{0}={1}&", key, filters);
                  
                    }
                }

                JavaScriptSerializer jsserializer = new JavaScriptSerializer();

                IDictionary<string, string> orders = null;
                if (!String.IsNullOrEmpty(sort))
                {
                    orders = new Dictionary<string, string> { { sort, dir } };
                    string orderStr = jsserializer.Serialize(orders);
                    sbParams.AppendFormat("orders={0}&", orderStr);
                }
                else
                    sbParams.Append("orders=&");

                if (!String.IsNullOrEmpty(addCustomerIdParam) && (addCustomerIdParam.ToLower() == "true" || addCustomerIdParam.ToLower() == "yes"))
                {
                    long customerId = (UserPrincipal != null && UserPrincipal.Identity != null) ? UserPrincipal.Identity._customerID : 0;
                    sbParams.AppendFormat("CustomerID={0}&", customerId.ToString());
                }
                if (addIcaoIdParam != null && (addIcaoIdParam.ToLower() == "true" || addIcaoIdParam.ToLower() == "yes"))
                {
                    string icaoId = (UserPrincipal != null && UserPrincipal.Identity != null) ? UserPrincipal.Identity._airportICAOCd : string.Empty;
                    sbParams.AppendFormat("icaoID={0}&", icaoId);
                }
                else if (addIcaoIdParam != null && (addIcaoIdParam.ToLower() == "false" || addIcaoIdParam.ToLower() == "no"))
                {

                    sbParams.AppendFormat("icaoID={0}&", "");
                }
                String queryStringParams = sbParams.ToString();
                if (queryStringParams.EndsWith("&"))
                    queryStringParams = queryStringParams.Remove(queryStringParams.Length - 1);



                string encodedApiUrl = Microsoft.Security.Application.Encoder.HtmlEncode(coreApiUrl);
                string encodedApiType = Microsoft.Security.Application.Encoder.HtmlEncode(apiType);
                string encodedApiMethod = Microsoft.Security.Application.Encoder.HtmlEncode(apiMethod);
                string encodedUrl = string.Format(encodedApiUrl + "/api/v1/{0}/{1}{2}", encodedApiType, encodedApiMethod, queryStringParams);
                WebRequest webRequest = HttpWebRequest.Create(encodedUrl);
                webRequest.Method = "GET";
                webRequest.ContentType = "application/x-www-form-urlencoded";
                String accessToken = FlightPak.Web.Framework.Helpers.CoreApiManager.GetApiAccessToken();
                String encodedHeader = string.Format("Bearer {0}",  Microsoft.Security.Application.Encoder.HtmlEncode(accessToken));
                webRequest.Headers.Add(HttpRequestHeader.Authorization, encodedHeader);

                try
                {
                    using (var webResponse = webRequest.GetResponse())
                    {
                        var responseStream = webResponse.GetResponseStream();

                        using (StreamReader srResponse = new StreamReader(responseStream))
                        {
                            apiCallerWithFilterObj = new ApiCallerWithFilter(srResponse.ReadToEnd());
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (((System.Net.HttpWebResponse)(((System.Net.WebException)(ex)).Response)).StatusCode == HttpStatusCode.Unauthorized)
                    {
                        Dictionary<string, object> result = new Dictionary<string, object>();
                        result.Add("StatusCode", 401);                        
                        apiCallerWithFilterObj = new ApiCallerWithFilter(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    }
                    else
                    {
                        apiCallerWithFilterObj = new ApiCallerWithFilter(string.Format("Failed to execute api {0} for {1}", apiMethod, apiType));                        
                    }
                    string encodedLog = Microsoft.Security.Application.Encoder.HtmlEncode(String.Format("Url:{0}-Token:{1}-Exception:{2}", encodedApiUrl, accessToken, ex.ToString()));
                    Logger.Write(encodedLog, WebConstants.FlightPakException);
                }
                Response.Write(Microsoft.Security.Application.Encoder.HtmlEncode(apiCallerWithFilterObj.responseString).Replace("&quot;", @""""));
                Response.End();
            }, FlightPak.Common.Constants.Policy.UILayer);
        }
    }
}