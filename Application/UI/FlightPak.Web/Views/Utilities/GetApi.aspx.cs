﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.Views.Utilities
{
    public partial class GetApi : BaseSecuredPage
    {
        public readonly string strApiUrl = "";
        private ExceptionManager exManager;
        private CoreApiManager coreApiManager;
        readonly string responseString;

        public GetApi()
        {
            String coreApiUrl = ConfigurationManager.AppSettings["CoreApiServer"];
            if (String.IsNullOrEmpty(coreApiUrl))
                coreApiUrl = "http://core-dev.universalweather.rdn/";
            
            strApiUrl = string.Format("{0}/api/v1/{1}", coreApiUrl, "{0}/{1}{2}");
        }

        public GetApi(string dataObject)
        {
            responseString = dataObject;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            exManager.Process(() =>
            {
                GetApi getApiObj;
                String apiType = this.Request["apiType"];
                String apiMethod = this.Request["method"];
                StringBuilder sbParams = new StringBuilder();
                sbParams.Append("?");
                foreach (String key in Request.QueryString)
                {
                    if (key.ToLower() != "apitype" && key.ToLower() != "method")
                        sbParams.AppendFormat("{0}={1}&", key, Request.Params[key]);
                }

                String queryStringParams = sbParams.ToString();
                if (queryStringParams.EndsWith("&"))
                    queryStringParams = queryStringParams.Remove(queryStringParams.Length - 1);
                
                String strUrl = String.Format(strApiUrl, apiType, apiMethod, queryStringParams);

                WebRequest webRequest = default(WebRequest);
                if (!string.IsNullOrEmpty(strUrl) && !string.IsNullOrWhiteSpace(strUrl))
                {
                    webRequest=HttpWebRequest.Create(strUrl);
                }
                String accessToken = CoreApiManager.GetApiAccessToken();
                webRequest.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + accessToken);

                try
                {
                    using (var webResponse = webRequest.GetResponse())
                    {
                        var responseStream = webResponse.GetResponseStream();

                        using (StreamReader srResponse = new StreamReader(responseStream))
                        {
                            getApiObj = new GetApi(srResponse.ReadToEnd());
                        }
                    }
                }
                catch(WebException webEx)
                {
                    getApiObj = new GetApi(string.Format("Failed to execute api {0} for {1}", apiMethod, apiType));
                    if (((System.Net.HttpWebResponse)(webEx.Response)).StatusCode == HttpStatusCode.Unauthorized)
                    {
                        Dictionary<string, object> result = new Dictionary<string, object>();
                        result.Add("StatusCode", 401);
                        getApiObj = new GetApi(Newtonsoft.Json.JsonConvert.SerializeObject(result));                        
                    }
                    Logger.Write(String.Format("Url:{0}-Token:{1}-Exception:{2}", Microsoft.Security.Application.Encoder.UrlEncode(strUrl), Microsoft.Security.Application.Encoder.HtmlEncode(accessToken), Microsoft.Security.Application.Encoder.HtmlEncode(webEx.ToString())), WebConstants.FlightPakException);
                }
                catch (Exception ex)
                {                   
                    getApiObj = new GetApi(string.Format("Failed to execute api {0} for {1}", apiMethod, apiType));                    
                    Logger.Write(String.Format("Url:{0}-Token:{1}-Exception:{2}", Microsoft.Security.Application.Encoder.UrlEncode(strUrl), Microsoft.Security.Application.Encoder.HtmlEncode(accessToken), Microsoft.Security.Application.Encoder.HtmlEncode(ex.ToString())), WebConstants.FlightPakException);
                }                
                Response.Write(Microsoft.Security.Application.Encoder.HtmlEncode(getApiObj.responseString));                
                Response.End();
            }, FlightPak.Common.Constants.Policy.UILayer);
        }
    }
}