﻿<%@ Page Title="Bookmarks" Language="C#" AutoEventWireup="true" CodeBehind="Bookmarks.aspx.cs"
    Inherits="FlightPak.Web.Views.Utilities.Bookmarks" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Bookmarks</title>
    <link type="text/css" rel="stylesheet" href="../../Scripts/jquery.alerts.css" />
    <script type="text/javascript" src="../../Scripts/Common.js"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="../../Scripts/jquery.alerts.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableCdn="true" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function ShowSuccessMessage() {
                $(document).ready(function () {
                    $("#tdSuccessMessage").css("display", "inline");
                    $('#tdSuccessMessage').delay(60000).fadeOut(60000);
                });
            }
        </script>
        <script type="text/javascript">
            function RefreshGrid() {
                var masterTable = $find("<%= dgBookmarks.ClientID %>").get_masterTableView();
                masterTable.rebind();
            }
            //this function is used to get dimension
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            //this function is used to get radwindow frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) {
                    oWindow = window.radWindow;
                }
                else if (window.frameElement.radWindow) {
                    oWindow = window.frameElement.radWindow;
                }
                return oWindow;
            }
            function NavigateToURL() {
                //create the argument that will be returned to the parent page
                var oArg = new Object();
                grid = $find("<%= dgBookmarks.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "KeyName");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "KeyValue");
                    oArg.KeyName = cell1.innerHTML;
                    oArg.KeyValue = cell2.innerHTML;
                    window.open(oArg.KeyValue); //, oArg.KeyName, 'width=998,height=550,toolbar=no, location=no,directories=no,status=no,menubar=no,scrollbars=yes,copyhistory=no,resizable=yes,top=' + (screen.height - 550) / 2 + ',left=' + (screen.width - 998) / 2);
                    break;
                }
            }
            function ValidateURL(sender, e) {
                var ReturnValue = true;
                var CtrlValue = document.getElementById("<%=tbBookmarkURL.ClientID%>").value;
                if ((CtrlValue.indexOf("http://") != -1) || (CtrlValue.indexOf("https://") != -1)) {
                }
                else {
                    sender.innerText = "Invalid URL format. Include 'http://' before the url.";
                    e.IsValid = false;
                    ReturnValue = false;
                }
                return ReturnValue;
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" class="dgpExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table width="100%" cellpadding="0" cellspacing="0" class="box1">
                <tr>
                    <td>
                        <telerik:RadGrid ID="dgBookmarks" runat="server" AllowSorting="true" OnNeedDataSource="dgBookmarks_BindData"
                            OnPageIndexChanged="dgCustom_PageIndexChanged" OnItemCommand="dgBookmarks_ItemCommand"
                            OnUpdateCommand="dgBookmarks_UpdateCommand" OnInsertCommand="dgBookmarks_InsertCommand"
                            OnDeleteCommand="dgBookmarks_DeleteCommand" OnSelectedIndexChanged="dgBookmarks_SelectedIndexChanged"
                            AutoGenerateColumns="false" PageSize="10" Height="341px" AllowPaging="true" AllowFilteringByColumn="true"
                            PagerStyle-AlwaysVisible="true" CssClass="bk_url">
                            <MasterTableView DataKeyNames="UserPreferenceID,CustomerID,UserName,CategoryName,SubCategoryName,KeyName,KeyValue,LastUpdTS"
                                ClientDataKeyNames="" CommandItemDisplay="Bottom">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="KeyName" HeaderText="Bookmark Name" UniqueName="KeyName"
                                        CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                        FilterControlWidth="200px" HeaderStyle-Width="150px" />
                                    <%--<telerik:GridHyperLinkColumn DataField="KeyValue" HeaderText="URL" UniqueName="KeyValue"
                                        CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                        HeaderStyle-Width="400px" />--%>
                                    <telerik:GridHyperLinkColumn DataNavigateUrlFields="KeyValue" UniqueName="KeyValue"
                                        HeaderText="URL" DataTextField="KeyValue" HeaderStyle-Width="400px" FilterControlWidth="300px"
                                        CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                        Target="_new" />
                                </Columns>
                                <CommandItemTemplate>
                                    <div style="padding: 5px 5px; float: left; clear: both;">
                                        <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                                        <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                            ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                                            runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                                    </div>
                                    <div>
                                        <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                                    </div>
                                </CommandItemTemplate>
                            </MasterTableView>
                            <ClientSettings EnablePostBackOnRowClick="true">
                                <%--<ClientEvents OnRowDblClick="NavigateToURL" />--%>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                <Selecting AllowRowSelect="true" />
                            </ClientSettings>
                            <GroupingSettings CaseSensitive="false" />
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td align="left" class="tdLabel160">
                                    <div id="tdSuccessMessage" class="success_msg">
                                        Record saved successfully.</div>
                                </td>
                                <td align="right">
                                    <div class="mandatory">
                                        <span>Bold</span> Indicates required field</div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="tdLabel100 mnd_text">
                                    Bookmark Name
                                </td>
                                <td>
                                    <asp:TextBox ID="tbBookmarkName" runat="server" CssClass="text300" MaxLength="40"
                                        ValidationGroup="save"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvBookmarkName" runat="server" ErrorMessage="Bookmark name is Required"
                                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbBookmarkName" CssClass="alert-text"
                                        ValidationGroup="save"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="mnd_text">
                                    URL
                                </td>
                                <td>
                                    <asp:TextBox ID="tbBookmarkURL" runat="server" CssClass="text400" MaxLength="255"
                                        ValidationGroup="save"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvBookmarkURL" runat="server" ErrorMessage="Bookmark URL is Required"
                                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbBookmarkURL" CssClass="alert-text"
                                        ValidationGroup="save"></asp:RequiredFieldValidator>
                                    <%--<asp:RegularExpressionValidator ID="regBookmarkURL" runat="server" Display="Dynamic"
                                        ErrorMessage="Invalid URL Format" ControlToValidate="tbBookmarkURL" CssClass="alert-text"
                                        ValidationGroup="save" ValidationExpression=""></asp:RegularExpressionValidator>--%>
                                    <%--<asp:CustomValidator ID="cvBookmarkURL" runat="server" ValidationGroup="save" ControlToValidate="tbBookmarkURL"
                                        Display="Dynamic" CssClass="alert-text" SetFocusOnError="true" ErrorMessage="Invalid URL Format"
                                        ClientValidationFunction="ValidateURL"></asp:CustomValidator>--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" class="tblButtonArea-nw">
                            <tr>
                                <td>
                                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="save"
                                        OnClick="btnSave_Click" />
                                    <asp:HiddenField ID="hdnSave" runat="server" />
                                    <asp:HiddenField ID="hdnUserPreferenceID" runat="server" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="false"
                                        OnClick="btnCancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
