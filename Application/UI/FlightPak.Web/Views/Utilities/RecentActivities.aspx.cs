﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.ViewModels;
using Telerik.Web.UI;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.PreflightService;
using FlightPak.Web.PostflightService;
using FlightPak.Web.CorporateRequestService;
using FlightPak.Web.CharterQuoteService;
using FlightPak.Web.Views.Transactions.Preflight;

namespace FlightPak.Web.Views.Utilities
{
    public partial class RecentActivities : BaseSecuredPage
    {
        int MaxRecordCount = 10;
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void dgPreflight_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CommonService.CommonServiceClient objCommonService = new CommonService.CommonServiceClient())
                        {
                            var objDstVal = objCommonService.GetRecentActivityPreflight(MaxRecordCount);
                            if (objDstVal.ReturnFlag == true)
                            {
                                dgPreflight.DataSource = objDstVal.EntityList;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.RecentActivities);
                }
            }
        }

        protected void dgPostFlight_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CommonService.CommonServiceClient objCommonService = new CommonService.CommonServiceClient())
                        {
                            var objDstVal = objCommonService.GetRecentActivityPostflight(MaxRecordCount);
                            if (objDstVal.ReturnFlag == true)
                            {
                                dgPostFlight.DataSource = objDstVal.EntityList;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.RecentActivities);
                }
            }
        }

        protected void dgCharterQuote_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CommonService.CommonServiceClient objCommonService = new CommonService.CommonServiceClient())
                        {
                            var objDstVal = objCommonService.GetRecentActivityCharterQuote(MaxRecordCount);
                            if (objDstVal.ReturnFlag == true)
                            {
                                dgCharterQuote.DataSource = objDstVal.EntityList;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.RecentActivities);
                }
            }
        }

        protected void PreflightView_ItemCommand(Object Sender, CommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        long TripID = Convert.ToInt64(e.CommandArgument.ToString());
                        PreflightService.PreflightMain Trip = null;
                        using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                        {
                            Trip = new PreflightService.PreflightMain();
                            var objRetVal = PrefSvc.GetTrip(TripID);
                            if (objRetVal.ReturnFlag)
                            {
                                PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
                                Trip = objRetVal.EntityList[0];

                                //SetTripModeToNoChange(ref Trip);
                                Trip.Mode = TripActionMode.NoChange;
                                Trip.State = FlightPak.Web.PreflightService.TripEntityState.NoChange;
                                Session["CurrentPreFlightTrip"] = Trip;

                                var ObjretVal = PrefSvc.GetTripExceptionList(Trip.TripID);
                                if (ObjretVal.ReturnFlag)
                                {
                                    Session["PreflightException"] = ObjretVal.EntityList;
                                }
                                else
                                    Session["PreflightException"] = null;
                                Trip.AllowTripToNavigate = true;
                                Trip.AllowTripToSave = true;
                                pfViewModel = PreflightTripManager.InjectPreflightMainToPreflightTripViewModel(Trip, pfViewModel);
                                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
                                HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentTrip] = Trip;
                                //InjectScript.Text = "<script type='text/javascript'>CloseAndRebind('navigateToInserted')</script>";
                            }
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Search.BasicSearch);
                }
            }
            string pageName = string.Empty;
            TryResolveUrl("Views/Transactions/Preflight/PreflightMain.aspx", out pageName);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "closeandredirectPreflight('" + pageName + "');", true);
        }

        protected void PostflightView_ItemCommand(Object Sender, CommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        long TripID = Convert.ToInt64(e.CommandArgument.ToString());
                        PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                        using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
                        {
                            Trip = new PreflightService.PreflightMain();
                            // Clear the Existing Session Values
                            Session.Remove("POSTFLIGHTMAIN");
                            Session.Remove("HOMEBASEKEY");
                            Session.Remove("FLEETRATEKEY");
                            Session.Remove("SIFLRATEKEY");
                            Session.Remove("FLEETPROFILEKEY");
                            Session.Remove("POEXCEPTION");
                            Session.Remove("POPAXINFO");
                            Session.Remove("POCREWINFO");
                            Session.Remove("POSTFLIGHTMAIN_RETAIN");

                            PostflightService.PostflightMain obj = new PostflightService.PostflightMain();
                            obj.POLogID = TripID;
                            var result = objService.GetTrip(obj);

                            if (result.ReturnFlag == true)
                            {
                                Session["POSTFLIGHTMAIN"] = (PostflightService.PostflightMain)result.EntityInfo;

                                // Bind the Saved Exceptions into the Session
                                if (result.EntityInfo.PostflightTripExceptions != null && result.EntityInfo.PostflightTripExceptions.Count > 0)
                                    Session["POEXCEPTION"] = result.EntityInfo.PostflightTripExceptions;
                            }
                        }



                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Search.BasicSearch);
                }
            }
            string pageName = string.Empty;
            TryResolveUrl("Views/Transactions/Postflight/PostflightMain.aspx", out pageName);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "closeandredirectPreflight('" + pageName + "');", true);
        }

        protected void CRView_ItemCommand(object sender, CommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        long CRMainID = Convert.ToInt64(e.CommandArgument.ToString());
                        using (CorporateRequestServiceClient CorpSvc = new CorporateRequestServiceClient())
                        {
                            var objRetVal = CorpSvc.GetRequest(CRMainID);
                            if (objRetVal.ReturnFlag)
                            {
                                CorporateRequestService.CRMain CorpRequest = new CorporateRequestService.CRMain();
                                CorpRequest = objRetVal.EntityList[0];
                                CorpRequest.Mode = CorporateRequestTripActionMode.NoChange;
                                CorpRequest.State = CorporateRequestTripEntityState.NoChange;
                                CorpRequest.AllowTripToNavigate = true;
                                CorpRequest.AllowTripToSave = true;
                                Session["CurrentCorporateRequest"] = CorpRequest;
                                var ObjretVal = CorpSvc.GetRequestExceptionList(CorpRequest.CRMainID);
                                if (ObjretVal.ReturnFlag)
                                {
                                    Session["CorporateRequestException"] = ObjretVal.EntityList;
                                }
                                else
                                    Session["CorporateRequestException"] = null;


                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Search.BasicSearch);
                }
            }
            string pageName = string.Empty;
            TryResolveUrl("Views/Transactions/CorporateRequest/CorporateRequestMain.aspx", out pageName);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "closeandredirectPreflight('" + pageName + "');", true);
        }
        protected void CORView_ItemCommand(object Sender, CommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        long FileID = Convert.ToInt64(e.CommandArgument.ToString());
                        CharterQuoteService.CQFile FileReq = new CharterQuoteService.CQFile();
                        using (CharterQuoteServiceClient CqSvc = new CharterQuoteServiceClient())
                        {
                            var objRetVal = CqSvc.GetCQRequestByID(FileID, false);
                            if (objRetVal.ReturnFlag)
                            {
                                FileReq = objRetVal.EntityList[0];
                                //SetTripModeToNoChange(ref Trip);
                                FileReq.Mode = CQRequestActionMode.NoChange;
                                FileReq.State = CQRequestEntityState.NoChange;


                                Session["CQFILE"] = FileReq;

                                ////sujitha waiting for exceptions
                                //var ObjretVal = CqSvc.GetTripExceptionList(Trip.TripID);
                                //FileReq.AllowTripToNavigate = true;
                                //FileReq.AllowTripToSave = true;
                                
                                //InjectScript.Text = "<script type='text/javascript'>CloseAndRebind('navigateToInserted')</script>";
                            }
                        }



                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Search.BasicSearch);
                }
            }
            string pageName = string.Empty;
            TryResolveUrl("Views/Transactions/CharterQuote/CharterQuoteRequest.aspx", out pageName);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "closeandredirectPreflight('" + pageName + "');", true);
        }

        /// <summary>
        /// Added to move the Scroll to Top
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgPreflight_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgPreflight.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }

        /// <summary>
        /// Added to move the Scroll to Top
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgPostFlight_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgPostFlight.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }

        /// <summary>
        /// Added to move the Scroll to Top
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCharterQuote_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCharterQuote.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }
    }
}