﻿<%@ Page Title="Winds on the World Air Routes" Language="C#" AutoEventWireup="true"
    CodeBehind="WorldAirRoutes.aspx.cs" Inherits="FlightPak.Web.Views.Utilities.WorldAirRoutes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Winds on the World Air Routes</title>
    <link type="text/css" rel="stylesheet" href="../../Scripts/jquery.alerts.css" />
    <script type="text/javascript" src="../../Scripts/Common.js"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="../../Scripts/jquery.alerts.js"></script>
    <style type="text/css">
        #dgAircraftCatalog{
            width:960px !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableCdn="true">
    </telerik:RadScriptManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function openWin(radWin) {
                var url = '';
                if (radWin == "radAirportDeparturePopup") {
                    url = '../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbDeparture.ClientID%>').value;
                }
                else if (radWin == "radAirportArrivePopup") {
                    url = '../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbArrive.ClientID%>').value;
                }
                var oWnd = radopen(url, radWin);
            }
            function OnClientCloseAirportDeparture(oWnd, args) {
                var combo = $find("<%= tbDeparture.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbDeparture.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=hdnDepartureID.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=cvDeparture.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbDeparture.ClientID%>").value = "";
                        document.getElementById("<%=hdnDepartureID.ClientID%>").value = "";
                        document.getElementById("<%=cvDeparture.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
                document.getElementById("<%=tbDeparture.ClientID%>").fireEvent("onchange");
                document.getElementById("<%=btnGetWindAirRoutes.ClientID%>").fireEvent("onclick");
            }

            function OnClientCloseAirportArrive(oWnd, args) {
                var combo = $find("<%= tbArrive.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbArrive.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=hdnArriveID.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=cvArrive.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbArrive.ClientID%>").value = "";
                        document.getElementById("<%=hdnArriveID.ClientID%>").value = "";
                        document.getElementById("<%=cvArrive.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
                document.getElementById("<%=tbArrive.ClientID%>").fireEvent("onchange");
                document.getElementById("<%=btnGetWindAirRoutes.ClientID%>").fireEvent("onclick");
            }

            function ShowReports(radWin, ReportFormat, ReportName) {
                document.getElementById("<%=hdnReportName.ClientID%>").value = ReportName;
                document.getElementById("<%=hdnReportFormat.ClientID%>").value = ReportFormat;
                if (ReportFormat == "EXPORT") {
                    url = "../../../Views/Reports/ExportReportInformation.aspx";
                    var oWnd = radopen(url, 'RadExportData');
                    return true;
                }
                else {
                    return true;
                }
            }

            function openReport(radWin, param) {
                var url = '';
                if (radWin == "WorldAirRoutes") {
                    url = '../../../../Views/Reports/ExportReportInformation.aspx?Report=RptUTWindsOnWorldAirRoutes&P1=' + document.getElementById('<%=tbDeparture.ClientID%>').value + '&P2=' + document.getElementById('<%=tbArrive.ClientID%>').value;                    
                }

                var oWnd = radopen(url, "RadExportData");
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                return oWindow;
            }
            function ClosePopup() {
                GetRadWindow().close();
                return false;
            }
            function AlertCallBackfn() {
                ClosePopup();
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="radAirportDeparturePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseAirportDeparture" AutoSize="true" KeepInScreenBounds="false"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radAirportArrivePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseAirportArrive" AutoSize="true" KeepInScreenBounds="false"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
           <telerik:RadWindow ID="RadExportData" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Export Report Information" KeepInScreenBounds="true" Height="540px" Width="880px"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="divContainer">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divContainer" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divContainer" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    
        <div style="margin: 0 auto; width: 980px;">
            <table width="100%" cellpadding="0" cellspacing="0" class="box1">
                <tr>
                    <td>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left">
                                    <div class="tab-nav-top">
                                        <span class="head-title">Winds on the World Air Routes</span> <span class="tab-nav-icons">
                                            <asp:LinkButton ID="lbtnReports" runat="server" OnClientClick="javascript:ShowReports('','PDF','RptUTWindsOnWorldAirRoutes');"
                                                title="Preview Report" OnClick="btnShowReports_OnClick" class="search-icon"></asp:LinkButton>
                                            <asp:LinkButton ID="lbtnSaveReports" runat="server" title="Export Report"  OnClientClick="javascript:openReport('WorldAirRoutes');return false;"
                                                class="save-icon"></asp:LinkButton> <%--OnClientClick="javascript:ShowReports('','EXPORT','RptDBAccountsExport');return false;"--%>
                                            <asp:Button ID="btnShowReports" runat="server" CssClass="button-disable" Style="display: none;" />
                                            <a href="#" title="Help" class="help-icon"></a></span>
                                        <asp:HiddenField ID="hdnReportName" runat="server" />
                                        <asp:HiddenField ID="hdnReportFormat" runat="server" />
                                        <asp:HiddenField ID="hdnReportParameters" runat="server" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="tblspace_10">
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div id="divContainer" runat="server" class="divGridPanel">
                            <table>
                            <tr>
                                <td class="tdLabel120">
                                    Departure:
                                </td>
                                <td class="tdLabel140">
                                    <asp:TextBox ID="tbDeparture" runat="server" CssClass="text80" MaxLength="4" AutoPostBack="true"
                                        OnTextChanged="tbDeparture_OnTextChanged"></asp:TextBox>
                                    <asp:HiddenField ID="hdnDepartureID" runat="server" />
                                    <asp:Button ID="btnBrowseDeparture" OnClientClick="javascript:openWin('radAirportDeparturePopup');return false;"
                                        CssClass="browse-button" runat="server" />
                                </td>
                                <td>
                                    <asp:TextBox ID="tbDepartureName" runat="server" CssClass="text150" ReadOnly="true"
                                        Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td colspan="2">
                                    <asp:CustomValidator ID="cvDeparture" runat="server" ControlToValidate="tbDeparture"
                                        ErrorMessage="Invalid Departure Airport ICAO Id." Display="Dynamic" CssClass="alert-text"
                                        SetFocusOnError="true"></asp:CustomValidator>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Arrival:
                                </td>
                                <td class="tdLabel140">
                                    <asp:TextBox ID="tbArrive" runat="server" CssClass="text80" MaxLength="4" AutoPostBack="true"
                                        OnTextChanged="tbArrive_OnTextChanged"></asp:TextBox>
                                    <asp:HiddenField ID="hdnArriveID" runat="server" />
                                    <asp:Button ID="btnBrowseArrive" OnClientClick="javascript:openWin('radAirportArrivePopup');return false;"
                                        CssClass="browse-button" runat="server" />
                                </td>
                                <td>
                                    <asp:TextBox ID="tbArriveName" runat="server" CssClass="text150" ReadOnly="true"
                                        Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td colspan="2">
                                    <asp:CustomValidator ID="cvArrive" runat="server" ControlToValidate="tbArrive" ErrorMessage="Invalid Arrive Airport ICAO Id."
                                        Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel120">
                                    Zone/Zone
                                </td>
                                <td>
                                    Model Departure City
                                </td>
                                <td>
                                    Model Arrival City
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="tbZoneZone" runat="server" CssClass="text80" ReadOnly="true" Enabled="false"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="tbModelDepartureCity" runat="server" CssClass="text150" ReadOnly="true"
                                        Enabled="false"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="tbModelArrivalCity" runat="server" CssClass="text150" ReadOnly="true"
                                        Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                            <table>
                            <tr>
                                <td class="preflight-custom-grid">
                                    <telerik:RadGrid ID="dgAircraftCatalog" runat="server" AllowSorting="true" AutoGenerateColumns="false" OnPageIndexChanged="MetroCity_PageIndexChanged"
                                        PagerStyle-AlwaysVisible="false" PageSize="5">
                                        <MasterTableView DataKeyNames="" CommandItemDisplay="None" AllowSorting="false" AllowFilteringByColumn="false"
                                            ShowFooter="false" AllowPaging="false">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="FlightLevel" HeaderText="Flight Level" UniqueName="FlightLevel"
                                                    CurrentFilterFunction="Contains" FilterDelay="4000" ShowFilterIcon="false" HeaderStyle-Width="8%" />
                                                <telerik:GridTemplateColumn HeaderText="Direct Route" CurrentFilterFunction="Contains"
                                                    FilterDelay="4000" ShowFilterIcon="false" HeaderStyle-Width="30%">
                                                    <HeaderTemplate>
                                                        <table width="100%">
                                                            <tr>
                                                                <td colspan="4" align="center">
                                                                    Direct Route
                                                                </td>
                                                            </tr>
                                                            <tr align="center">
                                                                <td>
                                                                    <asp:Label ID="lbDRQtr1" runat="server" Text="Dec-Feb" ToolTip="(Dec, Jan, Feb)"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbDRQtr2" runat="server" Text="Mar-May" ToolTip="(Mar, Apr, May)"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbDRQtr3" runat="server" Text="Jun-Aug" ToolTip="(Jun, Jul, Aug)"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbDRQtr4" runat="server" Text="Sep-Nov" ToolTip="(Sep, Oct, Nov)"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table width="100%">
                                                            <tr align="center">
                                                                <td style="border: none;">
                                                                    <asp:TextBox ID="tbDRQ1" runat="server" ReadOnly="true" Text='<%# Eval("DirectRouteQtr1") != null ? Eval("DirectRouteQtr1") : false %>'
                                                                        Width="20" />
                                                                </td>
                                                                <td style="border: none;">
                                                                    <asp:TextBox ID="tbDRQ2" runat="server" ReadOnly="true" Text='<%# Eval("DirectRouteQtr2") != null ? Eval("DirectRouteQtr2") : false %>'
                                                                        Width="20" />
                                                                </td>
                                                                <td style="border: none;">
                                                                    <asp:TextBox ID="tbDRQ3" runat="server" ReadOnly="true" Text='<%# Eval("DirectRouteQtr3") != null ? Eval("DirectRouteQtr3") : false %>'
                                                                        Width="20" />
                                                                </td>
                                                                <td style="border: none;">
                                                                    <asp:TextBox ID="tbDRQ4" runat="server" ReadOnly="true" Text='<%# Eval("DirectRouteQtr4") != null ? Eval("DirectRouteQtr4") : false %>'
                                                                        Width="20" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Return Route" CurrentFilterFunction="Contains"
                                                    FilterDelay="4000" ShowFilterIcon="false" HeaderStyle-Width="30%">
                                                    <HeaderTemplate>
                                                        <table width="100%">
                                                            <tr>
                                                                <td colspan="4" align="center">
                                                                    Return Route
                                                                </td>
                                                            </tr>
                                                            <tr align="center">
                                                                <td>
                                                                    <asp:Label ID="lbRRQtr1" runat="server" Text="Dec-Feb" ToolTip="(Dec, Jan, Feb)"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbRRQtr2" runat="server" Text="Mar-May" ToolTip="(Mar, Apr, May)"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbRRQtr3" runat="server" Text="Jun-Aug" ToolTip="(Jun, Jul, Aug)"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbRRQtr4" runat="server" Text="Sep-Nov" ToolTip="(Sep, Oct, Nov)"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table width="100%">
                                                            <tr align="center">
                                                                <td style="border: none;">
                                                                    <asp:TextBox ID="tbRRQ1" runat="server" ReadOnly="true" Text='<%# Eval("ReturnRouteQtr1") != null ? Eval("ReturnRouteQtr1") : false %>'
                                                                        Width="20" />
                                                                </td>
                                                                <td style="border: none;">
                                                                    <asp:TextBox ID="tbRRQ2" runat="server" ReadOnly="true" Text='<%# Eval("ReturnRouteQtr2") != null ? Eval("ReturnRouteQtr2") : false %>'
                                                                        Width="20" />
                                                                </td>
                                                                <td style="border: none;">
                                                                    <asp:TextBox ID="tbRRQ3" runat="server" ReadOnly="true" Text='<%# Eval("ReturnRouteQtr3") != null ? Eval("ReturnRouteQtr3") : false %>'
                                                                        Width="20" />
                                                                </td>
                                                                <td style="border: none;">
                                                                    <asp:TextBox ID="tbRRQ4" runat="server" ReadOnly="true" Text='<%# Eval("ReturnRouteQtr4") != null ? Eval("ReturnRouteQtr4") : false %>'
                                                                        Width="20" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Standard Deviation" CurrentFilterFunction="Contains"
                                                    FilterDelay="4000" ShowFilterIcon="false" HeaderStyle-Width="30%">
                                                    <HeaderTemplate>
                                                        <table width="100%">
                                                            <tr>
                                                                <td colspan="4" align="center">
                                                                    Standard Deviation
                                                                </td>
                                                            </tr>
                                                            <tr align="center">
                                                                <td>
                                                                    <asp:Label ID="lbSDQtr1" runat="server" Text="Dec-Feb" ToolTip="(Dec, Jan, Feb)"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbSDQtr2" runat="server" Text="Mar-May" ToolTip="(Mar, Apr, May)"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbSDQtr3" runat="server" Text="Jun-Aug" ToolTip="(Jun, Jul, Aug)"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lbSDQtr4" runat="server" Text="Sep-Nov" ToolTip="(Sep, Oct, Nov)"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table width="100%">
                                                            <tr align="center">
                                                                <td style="border: none;">
                                                                    <asp:TextBox ID="tbSDQ1" runat="server" ReadOnly="true" Text='<%# Eval("StdDeviationQtr1") != null ? Eval("StdDeviationQtr1") : false %>'
                                                                        Width="20" />
                                                                </td>
                                                                <td style="border: none;">
                                                                    <asp:TextBox ID="tbSDQ2" runat="server" ReadOnly="true" Text='<%# Eval("StdDeviationQtr2") != null ? Eval("StdDeviationQtr2") : false %>'
                                                                        Width="20" />
                                                                </td>
                                                                <td style="border: none;">
                                                                    <asp:TextBox ID="tbSDQ3" runat="server" ReadOnly="true" Text='<%# Eval("StdDeviationQtr3") != null ? Eval("StdDeviationQtr3") : false %>'
                                                                        Width="20" />
                                                                </td>
                                                                <td style="border: none;">
                                                                    <asp:TextBox ID="tbSDQ4" runat="server" ReadOnly="true" Text='<%# Eval("StdDeviationQtr4") != null ? Eval("StdDeviationQtr4") : false %>'
                                                                        Width="20" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                        </MasterTableView>
                                        <ClientSettings>
                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                            <Selecting AllowRowSelect="true" />
                                        </ClientSettings>
                                        <GroupingSettings CaseSensitive="false" />
                                    </telerik:RadGrid>
                                </td>
                            </tr>
                        </table>
                            <asp:Button ID="btnGetWindAirRoutes" runat="server" OnClick="btnGetWindAirRoutes_OnClick"
                            Style="display: none;" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    
    </form>
</body>
</html>
