﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.CommonService;
using System.Data;
using System.Text;
using System.Collections;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Web.Views.Utilities
{
    public partial class Bookmarks : BaseSecuredPage
    {
        #region "VARIABLE DECLARATIONS"
        private ExceptionManager exManager;
        private string ModuleNameConstant = ModuleNameConstants.Utilities.Bookmark;
        private string ViewBookmark = Permission.Utilities.ViewBookmark;
        private string AddBookmark = Permission.Utilities.AddBookmark;
        private string EditBookmark = Permission.Utilities.EditBookmark;
        private string DeleteBookmark = Permission.Utilities.DeleteBookmark;
        private string EntitySetBookmark = EntitySet.Utilities.Bookmark;
        #endregion "VARIABLE DECLARATIONS"
        #region "PAGE EVENTS"
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HaveModuleAccess(ModuleId.Favorites))
            {
                Response.Redirect("~/Views/Home.aspx?m=Bookmarks");
            }

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgBookmarks.ClientID));
                        if (!IsPostBack)
                        {
                            //CheckAutorization(ViewBookmark);
                            CheckAutorization(Permission.Utilities.ViewBookmark);
                            DefaultSelection(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        #endregion "PAGE EVENTS"
        #region "AJAX EVENTS"
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSave", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgBookmarks;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        #endregion "AJAX EVENTS"
        #region "USER DEFINED METHODS"
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(BindDataSwitch))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgBookmarks.Rebind();
                    }
                    if (dgBookmarks.MasterTableView.Items.Count > 0)
                    {
                        dgBookmarks.SelectedIndexes.Add(0);
                        Session["UserPreferenceID"] = dgBookmarks.Items[0].GetDataKeyValue("UserPreferenceID").ToString();
                        LoadControlData();
                        EnableForm(false);
                        GridEnable(true, true, true);
                    }
                    else
                    {
                        Session["UserPreferenceID"] = null;
                        ClearForm();
                        EnableForm(false);
                        GridEnable(true, true, true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["UserPreferenceID"] != null)
                    {
                        string ID = Session["UserPreferenceID"].ToString();
                        foreach (GridDataItem Item in dgBookmarks.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("UserPreferenceID").ToString() == ID.Trim())
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                        if (dgBookmarks.SelectedItems.Count == 0)
                        {
                            DefaultSelection(false);
                        }
                    }
                    else
                    {
                        DefaultSelection(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                LinkButton insertCtl, editCtl, delCtl;
                insertCtl = (LinkButton)dgBookmarks.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                editCtl = (LinkButton)dgBookmarks.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                delCtl = (LinkButton)dgBookmarks.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                //if (IsAuthorized(AddBookmark))
                //{
                    insertCtl.Visible = true;
                    if (Add)
                        insertCtl.Enabled = true;
                    else
                        insertCtl.Enabled = false;
                //}
                //else
                //{
                //    insertCtl.Visible = false;
                //}
                //if (IsAuthorized(EditBookmark))
                //{
                    editCtl.Visible = true;
                    if (Edit)
                    {
                        editCtl.Enabled = true;
                        editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                    }
                    else
                    {
                        editCtl.Enabled = false;
                        editCtl.OnClientClick = string.Empty;
                    }
                //}
                //else
                //{
                //    editCtl.Visible = false;
                //}
                //if (IsAuthorized(DeleteBookmark))
                //{
                    delCtl.Visible = true;
                    if (Delete)
                    {
                        delCtl.Enabled = true;
                        delCtl.OnClientClick = "javascript:return ProcessDelete();";
                    }
                    else
                    {
                        delCtl.Enabled = false;
                        delCtl.OnClientClick = string.Empty;
                    }
                //}
                //else
                //{
                //    delCtl.Visible = false;
                //}
            }
        }
        private void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                tbBookmarkName.Enabled = Enable;
                tbBookmarkURL.Enabled = Enable;
                btnSave.Visible = Enable;
                btnCancel.Visible = Enable;
            }
        }
        private void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string EmptyString = string.Empty;
                hdnSave.Value = EmptyString;
                tbBookmarkName.Text = EmptyString;
                tbBookmarkURL.Text = "";
            }
        }
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                ClearForm();
                if (dgBookmarks.SelectedItems.Count != 0)
                {
                    GridDataItem item = dgBookmarks.SelectedItems[0] as GridDataItem;
                    if (item.GetDataKeyValue("UserPreferenceID") != null)
                    {
                        hdnUserPreferenceID.Value = item.GetDataKeyValue("UserPreferenceID").ToString();
                    }
                    if (item.GetDataKeyValue("KeyName") != null)
                    {
                        tbBookmarkName.Text = item.GetDataKeyValue("KeyName").ToString();
                    }
                    if (item.GetDataKeyValue("KeyValue") != null)
                    {
                        tbBookmarkURL.Text = item.GetDataKeyValue("KeyValue").ToString();
                    }
                    Label lbLastUpdatedUser;
                    lbLastUpdatedUser = (Label)dgBookmarks.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                    if (item.GetDataKeyValue("UserName") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + item.GetDataKeyValue("UserName").ToString());
                    }
                    else
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    }
                    if (item.GetDataKeyValue("LastUpdTS") != null)
                    {
                        lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(item.GetDataKeyValue("LastUpdTS").ToString())));
                    }
                }


            }
        }
        private UserPreference GetItems(UserPreference oUserPreference)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oUserPreference))
            {
                if (hdnSave.Value == "Update")
                {
                    if (!string.IsNullOrEmpty(hdnUserPreferenceID.Value))
                    {
                        oUserPreference.UserPreferenceID = Convert.ToInt64(hdnUserPreferenceID.Value);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(hdnUserPreferenceID.Value))
                    {
                        oUserPreference.UserPreferenceID = 0;
                    }
                }
                if (!string.IsNullOrEmpty(tbBookmarkName.Text))
                {
                    oUserPreference.KeyName = tbBookmarkName.Text;
                }
                if (!string.IsNullOrEmpty(tbBookmarkName.Text))
                {
                    string URL = tbBookmarkURL.Text.Trim();
                    if (URL.ToUpper().StartsWith("HTTPS") || URL.ToUpper().StartsWith("HTTP") || URL.ToUpper().StartsWith("//"))
                    {
                        oUserPreference.KeyValue = URL;
                    }
                    else
                    {
                        oUserPreference.KeyValue = "http://" + URL;
                    }
                    

                }
                oUserPreference.CategoryName = "Bookmark";
                oUserPreference.CustomerID = 10000;
                oUserPreference.LastUpdTS = DateTime.UtcNow;
                oUserPreference.SubCategoryName = string.Empty;
                oUserPreference.UserName = UserPrincipal.Identity._name;
                return oUserPreference;
            }
        }
        #endregion "USER DEFINED METHODS"
        #region "GRID EVENTS"
        private void BindData(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(IsDataBind))
            {
                CommonService.UserPreference oUserPreference = new CommonService.UserPreference();
                oUserPreference.UserPreferenceID = 0;
                oUserPreference.CustomerID = UserPrincipal.Identity._customerID;
                oUserPreference.CategoryName = "Bookmark";
                oUserPreference.SubCategoryName = "";
                oUserPreference.UserName = "";
                oUserPreference.KeyName = "";
                oUserPreference.KeyValue = "";
                oUserPreference.LastUpdTS = DateTime.UtcNow;
                List<CommonService.GetBookmark> GetBookmarkList = new List<CommonService.GetBookmark>();
                using (CommonService.CommonServiceClient Service = new CommonService.CommonServiceClient())
                {
                    var GetBookmarkInfo = Service.GetBookmark(oUserPreference);
                    if (GetBookmarkInfo.ReturnFlag == true)
                    {
                        GetBookmarkList = GetBookmarkInfo.EntityList;
                    }
                    dgBookmarks.DataSource = GetBookmarkList;
                    if (IsDataBind)
                    {
                        dgBookmarks.DataBind();
                    }
                    Session["GetBookmarkList"] = GetBookmarkList;
                }
            }
        }
        protected void dgBookmarks_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindData(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void dgBookmarks_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (btnSave.Visible == false)
                        {
                            GridDataItem item = dgBookmarks.SelectedItems[0] as GridDataItem;
                            Session["UserPreferenceID"] = item.GetDataKeyValue("UserPreferenceID").ToString();
                            LoadControlData();
                            EnableForm(false);
                            GridEnable(true, true, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void dgBookmarks_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgBookmarks.SelectedIndexes.Clear();
                                GridEnable(false, false, false);
                                ClearForm();
                                hdnSave.Value = "Save";
                                EnableForm(true);
                                Session["UserPreferenceID"] = null;
                                RadAjaxManager1.FocusControl(tbBookmarkName.ClientID);
                                break;
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                //e.Item.Selected = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    if (Session["UserPreferenceID"] != null)
                                    {
                                        var returnValue = CommonService.Lock(EntitySetBookmark, Convert.ToInt64(Session["UserPreferenceID"].ToString()));
                                        Session["IsEditLockFeeSchedule"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstant);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstant);
                                            return;
                                        }
                                    }
                                }
                                GridEnable(false, false, false);
                                LoadControlData();
                                hdnSave.Value = "Update";
                                EnableForm(true);
                                RadAjaxManager1.FocusControl(tbBookmarkName.ClientID);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    column.CurrentFilterValue = string.Empty;
                                    column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void dgBookmarks_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidateCustom = true;
                        IsValidateCustom = ValidateAll();
                        if (IsValidateCustom == true)
                        {
                            UserPreference oUserPreference = new UserPreference();
                            using (CommonService.CommonServiceClient Service = new CommonServiceClient())
                            {
                                oUserPreference = GetItems(oUserPreference);
                                var Result = Service.AddBookmark(oUserPreference);
                                if (Result.ReturnFlag == true)
                                {
                                    Session["UserPreferenceID"] = "0";
                                    ShowSuccessMessage();
                                    dgBookmarks.Rebind();
                                    EnableForm(false);
                                    SelectItem();
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(Result.ErrorMessage, ModuleNameConstant);
                                }
                            }
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPaxInfoPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
                finally
                {
                }
            }
        }
        protected void dgBookmarks_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidateCustom = true;
                        IsValidateCustom = ValidateAll();
                        if (IsValidateCustom == true)
                        {
                            UserPreference oUserPreference = new UserPreference();
                            using (CommonService.CommonServiceClient Service = new CommonServiceClient())
                            {
                                oUserPreference = GetItems(oUserPreference);
                                var Result = Service.AddBookmark(oUserPreference);
                                if (Result.ReturnFlag == true)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        if (Session["UserPreferenceID"] != null)
                                        {
                                            var returnValue = CommonService.UnLock(EntitySetBookmark, Convert.ToInt64(Session["UserPreferenceID"].ToString()));
                                            Session["IsEditLockFeeSchedule"] = "False";
                                        }
                                    }
                                    ShowSuccessMessage();
                                    e.Item.OwnerTableView.Rebind();
                                    e.Item.Selected = true;
                                    dgBookmarks.Rebind();
                                    EnableForm(false);
                                    SelectItem();
                                }
                                else
                                {
                                    //For Data Anotation
                                    ProcessErrorMessage(Result.ErrorMessage, ModuleNameConstant);
                                }
                            }
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPaxInfoPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
                finally
                {
                }
            }
        }
        protected void dgBookmarks_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["UserPreferenceID"] != null)
                        {
                            UserPreference oUserPreference = new UserPreference();
                            hdnSave.Value = "Update";
                            oUserPreference = GetItems(oUserPreference);                            
                            oUserPreference.KeyName = "-1";
                            oUserPreference.KeyValue = "-1"; 
                            //Lock will happen from UI
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.Lock(EntitySetBookmark, oUserPreference.UserPreferenceID);
                                if (!returnValue.ReturnFlag)
                                {
                                    e.Item.Selected = true;
                                    DefaultSelection(false);
                                    if (IsPopUp)
                                        ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstant);
                                    else
                                        ShowAlert(returnValue.LockMessage, ModuleNameConstant);
                                    return;
                                }
                                CommonService.AddBookmark(oUserPreference);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
                finally
                {
                    // For Positive and negative cases need to unlock the record
                    // The same thing should be done for Edit
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        if (Session["UserPreferenceID"] != null)
                        {
                            //Unlock should happen from Service Layer
                            var returnValue1 = CommonService.UnLock(EntitySetBookmark, Convert.ToInt64(Session["UserPreferenceID"].ToString()));
                        }
                    }
                }
            }
        }
        protected void dgBookmarks_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem dataItem = e.Item as GridDataItem;                            
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        #endregion "GRID EVENTS"
        #region "CONTROL EVENTS"
        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            (dgBookmarks.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgBookmarks.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            if (Session["UserPreferenceID"] != null)
                            {
                                //Unlock should happen from Service Layer
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.UnLock(EntitySetBookmark, Convert.ToInt64(Session["UserPreferenceID"].ToString().Trim()));
                                }
                            }
                        }
                        DefaultSelection(false);
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radFeeGroupPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void FilterByCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindData(true);
                        DefaultSelection(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }        
        #endregion "CONTROL EVENTS"
        #region "VALIDATIONS"
        private bool ValidateAll()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool IsValidateCustom = true;
                if (hdnSave.Value == "Save")
                {
                    if ((CheckKeyNameUnique() == false) && (IsValidateCustom == true))
                    {
                        IsValidateCustom = false;
                    }
                }
                return IsValidateCustom;
            }
        }


        protected void dgCustom_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgBookmarks.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }


        private bool CheckKeyNameUnique()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (!string.IsNullOrEmpty(tbBookmarkName.Text))
                {
                    UserPreference oUserPreference = new UserPreference();
                    oUserPreference.UserPreferenceID = 0;
                    oUserPreference.CustomerID = UserPrincipal.Identity._customerID;
                    oUserPreference.CategoryName = "Bookmark";
                    oUserPreference.SubCategoryName = "";
                    oUserPreference.UserName = "";
                    oUserPreference.KeyName = tbBookmarkName.Text;
                    oUserPreference.KeyValue = tbBookmarkURL.Text;
                    oUserPreference.LastUpdTS = DateTime.UtcNow;
                    List<UserPreference> GetUserPreferenceList = new List<UserPreference>();
                    using (CommonServiceClient Service = new CommonServiceClient())
                    {
                        var GetUserPreferenceInfo = Service.GetBookmark(oUserPreference).EntityList.Where(x => x.KeyName == oUserPreference.KeyName).ToList();
                        if (GetUserPreferenceInfo.Count > 0)
                        {
                            ReturnValue = false;
                            string alertMsg = "radalert('Bookmark Name Must Be Unique.', 360, 50, '" + ModuleNameConstant + "');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }
                    }
                }
                return ReturnValue;
            }
        }
        #endregion "VALIDATIONS"
    }
}