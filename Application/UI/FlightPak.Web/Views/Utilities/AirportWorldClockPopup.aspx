﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AirportWorldClockPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.Airports.AirportWorldClockPopup" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Airport</title>
    <script type="text/javascript" src="../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgAirport.ClientID %>");
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgAirport.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            //this function is used to get dimension
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            function Close() {
                GetRadWindow().Close();
            }

            function rebindgrid() {
                var masterTable = $find("<%= dgAirport.ClientID %>").get_masterTableView();
                masterTable.rebind();
                masterTable.clearSelectedItems();
                masterTable.selectItem(masterTable.get_dataItems()[0].get_element());
            }

            function GetGridId() {
                return $find("<%= dgAirport.ClientID %>");
            }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgAirport">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAirport" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAirport" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAirport" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkDisplayInactive">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAirport" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkDisplayEntryPoint">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAirport" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResize="GetDimensions"
                    AutoSize="false" KeepInScreenBounds="true" Height="475px" Width="1000px" Modal="true"
                    Behaviors="close" VisibleStatusbar="false">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" OnClientResize="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radFBOCRUDPopup" runat="server" OnClientResize="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radHotelCRUDPopup" runat="server" OnClientResize="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radTransportCRUDPopup" runat="server" OnClientResize="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radCateringCRUDPopup" runat="server" OnClientResize="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td class="tdLabel120">
                    <asp:CheckBox ID="chkDisplayInactive" AutoPostBack="true" OnCheckedChanged="chkDisplayInactive_CheckChanged"
                        runat="server" Checked="false" Text="Display Inactive" />
                </td>
                <td>
                    <asp:CheckBox ID="chkDisplayEntryPoint" AutoPostBack="true" OnCheckedChanged="chkDisplayEntryPoint_CheckChanged"
                        runat="server" Checked="false" Text="AOE(Airport of Entry)" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="dgAirport" runat="server" AllowMultiRowSelection="True" AllowSorting="True"
            OnItemCommand="dgAirport_ItemCommand" OnPageIndexChanged="MetroCity_PageIndexChanged"
            OnDeleteCommand="dgAirport_DeleteCommand" OnNeedDataSource="dgAirport_BindData"
            AutoGenerateColumns="False" Height="341px" AllowPaging="True" Width="980px" AllowFilteringByColumn="True" CellSpacing="0" GridLines="None">
            <MasterTableView DataKeyNames="AirportID,CustomerID,IcaoID,CityName,StateName,CountryID,CountryCD,CountryName,OffsetToGMT,IsEntryPort,AirportName,IsInActive,LastUpdUID,LastUpdTS,IsWorldClock,UWAID,Iata,MaxRunway"
                CommandItemDisplay="Bottom" ClientDataKeyNames="IsWorldClock,UWAID" PageSize="10">
                <Columns>
                    <telerik:GridBoundColumn DataField="IcaoID" HeaderText="ICAO" AutoPostBackOnFilter="true"
                        HeaderStyle-Width="100px" ShowFilterIcon="false" CurrentFilterFunction="Contains" />
                    <telerik:GridBoundColumn DataField="CityName" HeaderText="City" AutoPostBackOnFilter="true"
                        UniqueName="CityName" HeaderStyle-Width="150px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="StateName" HeaderText="State" AutoPostBackOnFilter="true"
                        UniqueName="StateName" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                        FilterControlWidth="80px" HeaderStyle-Width="100px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CountryName" HeaderText="Country" AutoPostBackOnFilter="true"
                        UniqueName="CountryName" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                        HeaderStyle-Width="100px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AirportName" HeaderText="Airport" AutoPostBackOnFilter="true"
                        UniqueName="AirportName" HeaderStyle-Width="150px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                    </telerik:GridBoundColumn>
                    <telerik:GridCheckBoxColumn DataField="IsInActive" HeaderText="Inactive" ShowFilterIcon="false"
                        UniqueName="IsInActive" AllowFiltering="false" HeaderStyle-Width="60px" CurrentFilterFunction="EqualTo"
                        AutoPostBackOnFilter="true">
                    </telerik:GridCheckBoxColumn>
                    
                    <telerik:GridBoundColumn HeaderText="Runway" DataField="MaxRunway" CurrentFilterFunction="GreaterThanOrEqualTo"
                        UniqueName="MaxRunway" AutoPostBackOnFilter="true" ShowFilterIcon="false" HeaderStyle-Width="100px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="OffsetToGMT" HeaderText="UTC +/-" AutoPostBackOnFilter="true"
                        UniqueName="OffsetToGMT" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                        HeaderStyle-Width="100px">
                    </telerik:GridBoundColumn>
                    <telerik:GridCheckBoxColumn DataField="IsEntryPort" HeaderText="AOE"
                        ShowFilterIcon="false" UniqueName="IsEntryPort" HeaderStyle-Width="80px" CurrentFilterFunction="Contains"
                        AllowFiltering="false" AutoPostBackOnFilter="false">
                    </telerik:GridCheckBoxColumn>
                    <telerik:GridBoundColumn DataField="AirportID" HeaderText="AirportID" UniqueName="AirportID"
                        Display="false" AllowFiltering="false" />
                </Columns>
                <CommandItemTemplate>
                    <div class="grid_bottom_box">
                        <div class="grid_icon">
                            <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                CommandName="InitInsert" Visible='<%#  IsUIReports && ShowCRUD && IsAuthorized(Permission.Database.AddAirport)%>'></asp:LinkButton>
                            <%-- --%>
                            <asp:LinkButton ID="lnkInitEdit" runat="server" ToolTip="Edit" CssClass="edit-icon-grid"
                                CommandName="Edit" Visible='<%#  IsUIReports && ShowCRUD && IsAuthorized(Permission.Database.EditAirport)%>'
                                OnClientClick="return ProcessUpdatePopup(GetGridId());"></asp:LinkButton>
                            <%-- --%>
                            <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDeletePopup('dgAirport', 'radAirportPopup');"
                                CssClass="delete-icon-grid" ToolTip="Delete" CommandName="DeleteSelected" Visible='<%# IsUIReports && ShowCRUD && IsAuthorized(Permission.Database.DeleteAirport)%>'></asp:LinkButton>
                            <%-- CommandName="DeleteSelected" --%>
                        </div>
                        <div class="grid_buttons">
                            <asp:LinkButton ID="FBOView" runat="server" ToolTip="FBO" CssClass="button" CommandName="FBOView"
                                Text="FBO" Visible='<%#  IsUIReports && ShowCRUD %>'></asp:LinkButton>
                            <asp:LinkButton ID="HotelView" runat="server" ToolTip="Hotel" CssClass="button" CommandName="HotelView"
                                Text="Hotel" Visible='<%#  IsUIReports && ShowCRUD %>'></asp:LinkButton>
                            <asp:LinkButton ID="TransportView" runat="server" ToolTip="Transportation" CssClass="button"
                                Text="Transportation" CommandName="TransportView" Visible='<%#  IsUIReports && ShowCRUD %>'></asp:LinkButton>
                            <asp:LinkButton ID="CateringView" runat="server" ToolTip="Catering" CssClass="button"
                                Text="Catering" CommandName="CateringView" Visible='<%#  IsUIReports && ShowCRUD %>'></asp:LinkButton>
                        </div>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
        <div style="padding: 5px 5px; text-align: right;">
            <asp:Button ID="btnSaveChanges" Text="OK" runat="server" CssClass="button" OnClick="SaveChanges_Click" />
            <asp:Button ID="btnClose" Text="Close" runat="server" CssClass="button" OnClientClick="javascript:return returnToParent(); return false;"
                Style="display: none;" />
        </div>
    </div>
    </form>
</body>
</html>
