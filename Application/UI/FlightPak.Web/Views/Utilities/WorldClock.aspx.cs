﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Base;
using Telerik.Web.UI;
using System.Text;
namespace FlightPak.Web.Views.Utilities
{
    public partial class WorldClock : BaseSecuredPage
    {
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgWorldClock.MasterTableView.NoMasterRecordsText = "No records found in grid - Click the Select button to add records";
                        hdnIsPopup.Value = "1";
                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Utilities.ViewWolrdClock);
                            hdnIsPopup.Value = "0";
                            ShowWorldClock(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.WorldClock);
                }
            }
        }
        protected void dgWorldClock_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ShowWorldClock(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.WorldClock);
                }
            }
        }
        protected void dgWorldClock_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                StringBuilder strToolTip = new StringBuilder();
                if (e.Item is GridDataItem)
                {
                    string Date = string.Empty;
                    string Time = string.Empty;
                    GridDataItem item = (GridDataItem)e.Item;
                    strToolTip = new StringBuilder();
                    if (item.GetDataKeyValue("DayLightSavingStartDT") != null)
                    {
                        SplitDateAndTime(item.GetDataKeyValue("DayLightSavingStartDT").ToString(), ref Date, ref Time);
                        strToolTip.AppendLine("Day Light Saving Start Date : " + Date);
                    }
                    else
                    {
                        strToolTip.AppendLine("Day Light Saving Start Date : " + "Not available");
                    }
                    if (item.GetDataKeyValue("DaylLightSavingStartTM") != null)
                    {
                        strToolTip.AppendLine("Day Light Saving Start Time : " + item.GetDataKeyValue("DaylLightSavingStartTM").ToString());
                    }
                    else
                    {
                        strToolTip.AppendLine("Day Light Saving Start Time : " + "Not available");
                    }
                    if (item.GetDataKeyValue("DayLightSavingEndDT") != null)
                    {
                        SplitDateAndTime(item.GetDataKeyValue("DayLightSavingEndDT").ToString(), ref Date, ref Time);
                        strToolTip.AppendLine("Day Light Saving End Date : " + Date);
                    }
                    else
                    {
                        strToolTip.AppendLine("Day Light Saving End Date : " + "Not available");
                    }
                    if (item.GetDataKeyValue("DayLightSavingEndTM") != null)
                    {
                        strToolTip.AppendLine("Day Light Saving End Time : " + item.GetDataKeyValue("DayLightSavingEndTM").ToString());
                    }
                    else
                    {
                        strToolTip.AppendLine("Day Light Saving End Time : " + "Not available");
                    }
                    item.ToolTip = strToolTip.ToString();
                }
            }
        }
        /// <summary>
        /// Method to generate and dislpay world clock
        /// </summary>
        private void ShowWorldClock(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(IsDataBind))
            {
                CalculationService.CalculationServiceClient objCalculationServiceClient = new CalculationService.CalculationServiceClient();
                UtilitiesService.UtilitiesServiceClient objService = new UtilitiesService.UtilitiesServiceClient();
                var objAirport = objService.GetAllAirportForWorldClock().EntityList.Where(x => x.IsDeleted == false && x.IsWorldClock == true).ToList();
                List<UtilitiesService.GetAllAirportForWorldClock> lstAirport = new List<UtilitiesService.GetAllAirportForWorldClock>();
                System.Text.StringBuilder JQScript = new System.Text.StringBuilder();
                JQScript.AppendLine("<script type=\"text/javascript\"> $(function () {");
                //if (hdnIsPopup.Value == "0")
                //{
                    JQScript.AppendLine("$('#utc-clock').epiclock({mode: jQuery.epiclock.modes.clock, time: '" + DateTime.UtcNow.ToLocalTime().ToString() + "', utc: true,format: 'm/d/Y H:i:s'});");
                //}
                if (objAirport.Count != 0)
                {
                    lstAirport = objAirport;
                    DateTime CountryTime;
                    for (int Index = 0; Index < lstAirport.Count; Index++)
                    {
                        CountryTime = objCalculationServiceClient.GetGMT(lstAirport[Index].AirportID, DateTime.UtcNow, false, false);
                        //if (lstAirport[Index].OffsetToGMT != null)
                        //{
                        //    CountryTime = DateTime.UtcNow.ToLocalTime().AddHours(Convert.ToDouble(lstAirport[Index].OffsetToGMT));
                        //}
                        //JQScript.AppendLine("$('#clock-retro" + Index.ToString() + "').epiclock({mode: jQuery.epiclock.modes.explicit, time: '" + CountryTime.ToString() + "', utc: true,format: 'm/d/Y H:i:s', renderer: 'retro'});");   //for animating worldclock
                        JQScript.AppendLine("$('#clock-retro" + Index.ToString() + "').epiclock({mode: jQuery.epiclock.modes.explicit, time: '" + CountryTime.ToString() + "', utc: false,format: 'm/d/Y H:i:s'});");
                        lstAirport[Index].GeneralNotes = "<dd id='clock-retro" + Index + "'></dd>";
                    }
                }
                JQScript.AppendLine("});</" + "script>");
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "JQScript", JQScript.ToString(), false);  //commented
                ScriptManager.RegisterStartupScript(this, this.GetType(), "JQScript", JQScript.ToString(), false);
                dgWorldClock.DataSource = lstAirport;
                if (IsDataBind)
                {
                    dgWorldClock.DataBind();
                }
            }
        }
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgWorldClock;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.WorldClock);
                }
            }
        }
        private void SplitDateAndTime(string DateTimeValue, ref string Date, ref string Time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Date = DateTimeValue.Substring(0, DateTimeValue.IndexOf(' '));
                Time = DateTimeValue.Substring(DateTimeValue.IndexOf(' '), DateTimeValue.Length - DateTimeValue.IndexOf(' '));
            }
        }

        protected void btnDeleteWorldClock_Click(object sender, EventArgs e)
        {
            if (dgWorldClock.Items.Count > 0)
            {
                if (dgWorldClock.SelectedItems.Count > 0)
                {
                    GridDataItem Item = (GridDataItem)dgWorldClock.SelectedItems[0];
                    if (Item.GetDataKeyValue("AirportID") != null)
                    {
                        using (UtilitiesService.UtilitiesServiceClient objService1 = new UtilitiesService.UtilitiesServiceClient())
                        {
                            long AirportID = 0;
                            AirportID = Convert.ToInt64(Item.GetDataKeyValue("AirportID").ToString());
                            CalculationService.CalculationServiceClient objCalculationServiceClient = new CalculationService.CalculationServiceClient();
                            UtilitiesService.UtilitiesServiceClient objService = new UtilitiesService.UtilitiesServiceClient();
                            var objAirport = objService.GetAllAirportForWorldClock().EntityList.Where(x => x.IsDeleted == false && x.IsWorldClock == true && x.IcaoID.ToString().Trim() == Item.GetDataKeyValue("IcaoID").ToString().Trim()).ToList();
                            objService1.DeleteWorldClock(AirportID);
                            dgWorldClock.Rebind();
                        }
                    }
                }
                else
                {
                    RadWindowManager1WC.RadAlert("Please select a record from the above table.", 330, 100, "World Clock", null);
                    dgWorldClock.Rebind();
                }
            }
        }

        protected void MetroCity_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgWorldClock.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }
    }
}
