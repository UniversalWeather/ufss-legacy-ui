﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightPak.Web.Views.Utilities
{
    public partial class UserPrincipalCaller : BaseSecuredPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var json = new JavaScriptSerializer().Serialize(UserPrincipal.Identity);
            Response.Write(json);
            Response.End();
        }
    }
}