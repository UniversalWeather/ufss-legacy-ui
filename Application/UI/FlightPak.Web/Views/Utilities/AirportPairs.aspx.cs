﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//Included
using Telerik.Web.UI;
using System.Globalization;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Text.RegularExpressions;
using FlightPak.Web.Framework.Helpers;
using System.Text;
namespace FlightPak.Web.Views.Utilities
{
    public partial class AirportPairs : BaseSecuredPage
    {
        private ExceptionManager exManager;
        private bool IsSaveSuccessAirportPair = false;
        private bool IsSaveSuccessAirportPairLegs = false;
        private Int64 Identity;
        private string CurrencyType = string.Empty;
        private string CurrencyFormat = string.Empty;
        private string TenthMinFormat = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgAirportPairs, dgAirportPairs, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgAirportPairs, pnlbarAirportPairLegs, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgAirportPairLegs, dgAirportPairLegs, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgAirportPairLegs, pnlbarAirportPairLegs, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgAirportPairLegs, pnlbarAirportPairLegs, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSaveAll, dgAirportPairs, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSaveAll, pnlbarAirportPairLegs, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancelAll, dgAirportPairs, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancelAll, pnlbarAirportPairLegs, RadAjaxLoadingPanel1);

                        // Grid Control could be ajaxified when the page is initially loaded.

                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId1'] = '{0}';", dgAirportPairs.ClientID));
                        // Grid Control could be ajaxified when the page is initially loaded.

                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId2'] = '{0}';", dgAirportPairLegs.ClientID));
                        SetDefaultData();
                        if (!IsPostBack)
                        {
                            CheckAutorization(Permission.Utilities.ViewAirportPairs);
                            DefaultSelectionAirportPairs(true);
                            DefaultSelectionAirportPairLegs(true);
                            Session["StandardFlightpakConversion"] = LoadStandardFPConversion();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
            }
        }
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = DivExternalForm;
                        //if (e.Initiator.ID.IndexOf("btnSaveAll", StringComparison.Ordinal) > -1)
                        //{
                        //    e.Updated = DivExternalForm;
                        //}
                        //if (e.Initiator.ID.IndexOf("btnCancelAll", StringComparison.Ordinal) > -1)
                        //{
                        //    e.Updated = DivExternalForm;
                        //}
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
            }
        }

        protected void MetroCity_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgAirportPairLegs.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }

        protected void AirportPairLegs_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgAirportPairLegs.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }


        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["REPORT"] = hdnReportName.Value;
                        Session["FORMAT"] = hdnReportFormat.Value;
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";AirportPairNumber=" + tbAPFileNumber.Text;
                        if (hdnReportFormat.Value == "PDF")
                        {
                            Response.Redirect(@"..\Reports\ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
            }
        }
        private void SetDefaultData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((UserPrincipal.Identity != null) && (UserPrincipal.Identity._fpSettings != null) && (UserPrincipal.Identity._fpSettings._CurrencySymbol != null))
                {
                    CurrencyType = UserPrincipal.Identity._fpSettings._CurrencySymbol;
                }
                else
                {
                    CurrencyType = "$ ";
                }
                CurrencyFormat = "#########0.00";
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                {
                    TenthMinFormat = "##0.0";
                    regAPLTOBias.ValidationExpression = "^[0-9]{0,3}\\.?[0-9]{0,1}$";
                    regAPLTOBias.ErrorMessage = "Invalid Format. Required Format is NNN.N";
                    regAPLLandBias.ValidationExpression = "^[0-9]{0,3}\\.?[0-9]{0,1}$";
                    regAPLLandBias.ErrorMessage = "Invalid Format. Required Format is NNN.N";
                    hdnTenthMin.Value = "1";
                }
                else if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                {
                    TenthMinFormat = "#0:00";
                    regAPLTOBias.ValidationExpression = "^[0-9]{0,3}\\:?[0-9]{0,2}$";
                    regAPLTOBias.ErrorMessage = "Invalid Format. Required Format is NNN:NN";
                    regAPLLandBias.ValidationExpression = "^[0-9]{0,3}\\:?[0-9]{0,2}$";
                    regAPLLandBias.ErrorMessage = "Invalid Format. Required Format is NNN:NN";
                    hdnTenthMin.Value = "2";
                }
                #region "Kilometer/Miles"
                if (UserPrincipal.Identity._fpSettings._IsKilometer == null ? false : (bool)UserPrincipal.Identity._fpSettings._IsKilometer)
                    lbAPLMiles.Text = "Kilometers";
                else
                    lbAPLMiles.Text = "Miles(N)";
                #endregion
            }
        }
        #region "Airport Pairs Worksheet"
        private void DefaultSelectionAirportPairs(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgAirportPairs.Rebind();
                    }
                    if (dgAirportPairs.MasterTableView.Items.Count > 0)
                    {
                        dgAirportPairs.SelectedIndexes.Add(0);
                        Session["AirportPairID"] = dgAirportPairs.Items[0].GetDataKeyValue("AirportPairID").ToString();
                        LoadControlDataAirportPairs();
                        EnableFormAirportPairs(false);
                        GridEnableAirportPairs(true, true, true);
                    }
                    else
                    {
                        Session["AirportPairID"] = null;
                        ClearFormAirportPairs();
                        EnableFormAirportPairs(false);
                        GridEnableAirportPairs(true, true, true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void DefaultSelectItemAirportPair()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["AirportPairID"] != null)
                    {
                        string ID = Session["AirportPairID"].ToString();
                        foreach (GridDataItem Item in dgAirportPairs.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("AirportPairID").ToString() == ID.Trim())
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                        if (dgAirportPairs.SelectedItems.Count == 0)
                        {
                            DefaultSelectionAirportPairs(false);
                        }
                    }
                    else
                    {
                        DefaultSelectionAirportPairs(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void GridEnableAirportPairs(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                LinkButton insertCtl, editCtl, delCtl;
                insertCtl = (LinkButton)dgAirportPairs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                editCtl = (LinkButton)dgAirportPairs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                delCtl = (LinkButton)dgAirportPairs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                if (IsAuthorized(Permission.Utilities.AddAirportPairs))
                {
                    insertCtl.Visible = true;
                    if (Add)
                        insertCtl.Enabled = true;
                    else
                        insertCtl.Enabled = false;
                }
                else
                {
                    insertCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Utilities.EditAirportPairs))
                {
                    editCtl.Visible = true;
                    if (Edit)
                    {
                        editCtl.Enabled = true;
                        editCtl.OnClientClick = "javascript:return ProcessUpdateAirportPair();";
                    }
                    else
                    {
                        editCtl.Enabled = false;
                        editCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    editCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Utilities.DeleteAirportPairs))
                {
                    delCtl.Visible = true;
                    if (Delete)
                    {
                        delCtl.Enabled = true;
                        delCtl.OnClientClick = "javascript:return ProcessDeleteAirportPair();";
                    }
                    else
                    {
                        delCtl.Enabled = false;
                        delCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    delCtl.Visible = false;
                }
            }
        }
        private void EnableFormAirportPairs(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                tbAPFileNumber.Enabled = false;
                ddlAPQuarter.Enabled = Enable;
                tbAPAircraft.Enabled = Enable;
                btnBrowseAircraft.Enabled = Enable;
                tbAPDescription.Enabled = Enable;
                btnSaveAirportPairs.Visible = Enable;
                btnCancelAirportPairs.Visible = Enable;
                btnSaveAll.Visible = Enable;
                btnCancelAll.Visible = Enable;
                btnSaveAll.ValidationGroup = "SaveAirportPair";
            }
        }
        private void ClearFormAirportPairs()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                hdnSaveAirportPairs.Value = string.Empty;
                hdnAPAirportPairID.Value = string.Empty;
                tbAPFileNumber.Text = string.Empty;
                ddlAPQuarter.SelectedIndex = 0;
                tbAPAircraft.Text = string.Empty;
                hdnAPAircraftID.Value = string.Empty;
                tbAPDescription.Text = string.Empty;
                Identity = 0;
            }
        }
        private void LoadControlDataAirportPairs()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgAirportPairs.SelectedItems.Count != 0)
                {
                    GridDataItem Item = dgAirportPairs.SelectedItems[0] as GridDataItem;
                    if (Item.GetDataKeyValue("AirportPairID").ToString().Trim().ToUpper() == Session["AirportPairID"].ToString().Trim().ToUpper())
                    {
                        if (Item.GetDataKeyValue("AirportPairID") != null)
                        {
                            hdnAPAirportPairID.Value = Item.GetDataKeyValue("AirportPairID").ToString().Trim();
                        }
                        else
                        {
                            hdnAPAirportPairID.Value = string.Empty;
                        }
                        if (Item.GetDataKeyValue("AirportPairNumber") != null)
                        {
                            tbAPFileNumber.Text = Item.GetDataKeyValue("AirportPairNumber").ToString().Trim();
                        }
                        else
                        {
                            tbAPFileNumber.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("WindQTR") != null)
                        {
                            ddlAPQuarter.SelectedValue = Item.GetDataKeyValue("WindQTR").ToString().Trim();
                        }
                        else
                        {
                            ddlAPQuarter.SelectedIndex = 0;
                        }
                        if (Item.GetDataKeyValue("AircraftCD") != null)
                        {
                            tbAPAircraft.Text = Item.GetDataKeyValue("AircraftCD").ToString().Trim();
                        }
                        else
                        {
                            tbAPAircraft.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("AircraftID") != null)
                        {
                            hdnAPAircraftID.Value = Item.GetDataKeyValue("AircraftID").ToString().Trim();
                        }
                        else
                        {
                            hdnAPAircraftID.Value = string.Empty;
                        }
                        if (Item.GetDataKeyValue("AirportPairDescription") != null)
                        {
                            tbAPDescription.Text = Item.GetDataKeyValue("AirportPairDescription").ToString().Trim();
                        }
                        else
                        {
                            tbAPDescription.Text = string.Empty;
                        }
                        Label lbLastUpdatedUser;
                        lbLastUpdatedUser = (Label)dgAirportPairs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                        if (Item.GetDataKeyValue("LastUpdUID") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                        }
                        else
                        {
                            lbLastUpdatedUser.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("LastUpdTS") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString())));
                        }
                    }
                }
            }
        }
        private UtilitiesService.AirportPair GetItemsAirportPairs(UtilitiesService.AirportPair oAirportPairs)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oAirportPairs))
            {
                if (hdnSaveAirportPairs.Value == "Update")
                {
                    if (!string.IsNullOrEmpty(hdnAPAirportPairID.Value))
                    {
                        oAirportPairs.AirportPairID = Convert.ToInt64(hdnAPAirportPairID.Value);
                    }
                }
                if (!string.IsNullOrEmpty(tbAPFileNumber.Text))
                {
                    oAirportPairs.AirportPairNumber = Convert.ToInt32(tbAPFileNumber.Text);
                }
                else
                {
                    oAirportPairs.AirportPairNumber = -1;
                }
                if (!string.IsNullOrEmpty(ddlAPQuarter.Text))
                {
                    oAirportPairs.WindQTR = Convert.ToInt32(ddlAPQuarter.SelectedValue);
                }
                if (!string.IsNullOrEmpty(hdnAPAircraftID.Value))
                {
                    oAirportPairs.AircraftID = Convert.ToInt64(hdnAPAircraftID.Value);
                }
                if (!string.IsNullOrEmpty(tbAPDescription.Text))
                {
                    oAirportPairs.AirportPairDescription = tbAPDescription.Text;
                }
                oAirportPairs.IsDeleted = false;
                return oAirportPairs;
            }
        }
        protected void tbAPAircraft_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbAPAircraft.Text))
                        {
                            if (CheckAircraftExist(tbAPAircraft, hdnAPAircraftID) == false)
                            {
                                cvAPAircraft.IsValid = false;
                                RadAjaxManager1.FocusControl(tbAPAircraft.ClientID);
                            }
                        }
                        else
                        {
                            rfvAPAircraft.IsValid = false;
                            RadAjaxManager1.FocusControl(tbAPAircraft.ClientID);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
            }
        }
        private bool CheckAircraftExist(TextBox TxtBx, HiddenField HdnFld)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                if (!string.IsNullOrEmpty(TxtBx.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objMasterService.GetAircraftList().EntityList.Where(x => x.AircraftCD.Trim().ToUpper() == TxtBx.Text.Trim().ToUpper()).ToList();
                        if (objRetVal.Count() != 0)
                        {
                            List<FlightPakMasterService.GetAllAircraft> AircraftList = new List<FlightPakMasterService.GetAllAircraft>();
                            AircraftList = (List<FlightPakMasterService.GetAllAircraft>)objRetVal.ToList();
                            HdnFld.Value = AircraftList[0].AircraftID.ToString();
                            ReturnValue = true;
                        }
                        else
                        {
                            HdnFld.Value = string.Empty;
                            ReturnValue = false;
                        }
                    }
                }
                return ReturnValue;
            }
        }
        #region "dgAirportPairs grid events"
        protected void dgAirportPairs_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UtilitiesService.AirportPair oGetAirportPairs = new UtilitiesService.AirportPair();
                        oGetAirportPairs.AirportPairID = -1;
                        oGetAirportPairs.IsDeleted = false;
                        List<UtilitiesService.GetAirportPair> GetAirportPairsList = new List<UtilitiesService.GetAirportPair>();
                        using (UtilitiesService.UtilitiesServiceClient UtilitiesService = new UtilitiesService.UtilitiesServiceClient())
                        {
                            var GetAirportPairsInfo = UtilitiesService.GetAirportPairs(oGetAirportPairs);
                            if (GetAirportPairsInfo.ReturnFlag == true)
                            {
                                GetAirportPairsList = GetAirportPairsInfo.EntityList;
                            }
                            dgAirportPairs.DataSource = GetAirportPairsList;
                            Session["AirportPairsList"] = GetAirportPairsList;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
            }
        }
        protected void dgAirportPairs_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (btnSaveAll.Visible == false)
                        {
                            GridDataItem item = dgAirportPairs.SelectedItems[0] as GridDataItem;
                            Session["AirportPairID"] = item.GetDataKeyValue("AirportPairID").ToString();
                            LoadControlDataAirportPairs();
                            EnableFormAirportPairs(false);
                            GridEnableAirportPairs(true, true, true);
                            DefaultSelectionAirportPairLegs(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
            }
        }
        protected void dgAirportPairs_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgAirportPairs.SelectedIndexes.Clear();
                                GridEnableAirportPairs(false, false, false);
                                ClearFormAirportPairs();
                                EnableFormAirportPairs(true);
                                hdnSaveAirportPairs.Value = "Save";
                                Session["AirportPairID"] = null;
                                Bind_AirportPairLegs(true);
                                ClearFormAirportPairLegs();
                                DefaultMonth();
                                RadAjaxManager1.FocusControl(tbAPAircraft.ClientID);
                                break;
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    if (Session["AirportPairID"] != null)
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Utilities.AirportPair, Convert.ToInt64(Session["AirportPairID"].ToString()));
                                        Session["IsEditLockAirportPair"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Utilities.AirportPairs);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Utilities.AirportPairs);
                                            return;
                                        }
                                    }
                                }
                                GridEnableAirportPairs(false, false, false);
                                EnableFormAirportPairs(true);
                                LoadControlDataAirportPairs();
                                hdnSaveAirportPairs.Value = "Update";
                                RadAjaxManager1.FocusControl(tbAPAircraft.ClientID);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    column.CurrentFilterValue = string.Empty;
                                    column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
            }
        }

        public void DefaulMonth()
        {
            int Month = DateTime.UtcNow.Month;
            if (Month == 12 || Month == 1 || Month == 2)
            {
                ddlAPLQuarter.SelectedValue = "1";
            }
            else if (Month == 3 || Month == 4 || Month == 5)
            {
                ddlAPLQuarter.SelectedValue = "2";
            }
            else if (Month == 6 || Month == 7 || Month == 8)
            {
                ddlAPLQuarter.SelectedValue = "3";
            }
            else if (Month == 9 || Month == 10 || Month == 11)
            {
                ddlAPLQuarter.SelectedValue = "4";
            }
        }

        public void DefaultMonth()
        {
            int Month = DateTime.UtcNow.Month;
            if (Month == 12 || Month == 1 || Month == 2)
            {
                ddlAPQuarter.SelectedValue = "1";
            }
            else if (Month == 3 || Month == 4 || Month == 5)
            {
                ddlAPQuarter.SelectedValue = "2";
            }
            else if (Month == 6 || Month == 7 || Month == 8)
            {
                ddlAPQuarter.SelectedValue = "3";
            }
            else if (Month == 9 || Month == 10 || Month == 11)
            {
                ddlAPQuarter.SelectedValue = "4";
            }
        }
       
        protected void dgAirportPairs_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidateCustom = true;
                        //IsValidateCustom = ValidateAirportPair();
                        if (IsValidateCustom == true)
                        {
                            UtilitiesService.AirportPair oAirportPairs = new UtilitiesService.AirportPair();
                            UtilitiesService.AirportPairDetail oAirportPairDetail = new UtilitiesService.AirportPairDetail();
                            oAirportPairs.AirportPairDetails = new List<UtilitiesService.AirportPairDetail>();
                            using (UtilitiesService.UtilitiesServiceClient UtilitiesService = new UtilitiesService.UtilitiesServiceClient())
                            {
                                oAirportPairs = GetItemsAirportPairs(oAirportPairs);
                                if ((hdnSaveAirportPairLegs.Value == "Save") || (hdnSaveAirportPairLegs.Value == "Update"))
                                {
                                    string[] TypeCodeList = new string[100];
                                    if (!string.IsNullOrEmpty(hdnAPLMultiTypeCodeID.Value))
                                    {
                                        TypeCodeList = hdnAPLMultiTypeCodeID.Value.Split(',');
                                        for (int Index = 0; Index < TypeCodeList.Length; Index++)
                                        {
                                            if (!string.IsNullOrEmpty(TypeCodeList[Index]))
                                            {
                                                hdnAPLTypeCodeID.Value = TypeCodeList[Index].Trim();
                                                if (Index == 0)
                                                {
                                                    PopulateTypeCodeData(false, false, 0);
                                                    CalculateAll(false, true, true, true, true);
                                                }
                                                else
                                                {
                                                    PopulateTypeCodeData(true, true, 0);
                                                    CalculateAll(true, true, true, true, true);
                                                }
                                                oAirportPairDetail = new UtilitiesService.AirportPairDetail();
                                                oAirportPairDetail = GetItemsAirportPairLegs(oAirportPairDetail);
                                                oAirportPairs.AirportPairDetails.Add(oAirportPairDetail);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        PopulateTypeCodeData(false, false, 0);
                                        CalculateAll(false, true, true, true, true);
                                        oAirportPairDetail = new UtilitiesService.AirportPairDetail();
                                        oAirportPairDetail = GetItemsAirportPairLegs(oAirportPairDetail);
                                        oAirportPairs.AirportPairDetails.Add(oAirportPairDetail);
                                    }
                                }
                                var Result = UtilitiesService.AddAirportPairs(oAirportPairs);
                                hdnAPAirportPairID.Value = Result.EntityInfo.AirportPairID.ToString();
                                if (Result.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                    IsSaveSuccessAirportPair = true;
                                }
                            }
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPaxInfoPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
                finally
                {
                }
            }
        }
        protected void dgAirportPairs_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidateCustom = true;
                        //IsValidateCustom = ValidateAirportPair();
                        if (IsValidateCustom == true)
                        {
                            UtilitiesService.AirportPair oAirportPairs = new UtilitiesService.AirportPair();
                            UtilitiesService.AirportPairDetail oAirportPairDetail = new UtilitiesService.AirportPairDetail();
                            oAirportPairs.AirportPairDetails = new List<UtilitiesService.AirportPairDetail>();
                            using (UtilitiesService.UtilitiesServiceClient UtilitiesService = new UtilitiesService.UtilitiesServiceClient())
                            {
                                oAirportPairs = GetItemsAirportPairs(oAirportPairs);
                                hdnAPAirportPairID.Value = oAirportPairs.AirportPairID.ToString();
                                if ((hdnSaveAirportPairLegs.Value == "Save") || (hdnSaveAirportPairLegs.Value == "Update"))
                                {
                                    string[] TypeCodeList = new string[100];
                                    if (!string.IsNullOrEmpty(hdnAPLMultiTypeCodeID.Value))
                                    {
                                        TypeCodeList = hdnAPLMultiTypeCodeID.Value.Split(',');
                                        for (int Index = 0; Index < TypeCodeList.Length; Index++)
                                        {
                                            if (!string.IsNullOrEmpty(TypeCodeList[Index]))
                                            {
                                                hdnAPLTypeCodeID.Value = TypeCodeList[Index].Trim();
                                                if (Index == 0)
                                                {
                                                    PopulateTypeCodeData(false, false, 0);
                                                    CalculateAll(false, true, true, true, true);
                                                }
                                                else
                                                {
                                                    PopulateTypeCodeData(true, true, 0);
                                                    CalculateAll(true, true, true, true, true);
                                                }
                                                oAirportPairDetail = new UtilitiesService.AirportPairDetail();
                                                oAirportPairDetail = GetItemsAirportPairLegs(oAirportPairDetail);
                                                oAirportPairs.AirportPairDetails.Add(oAirportPairDetail);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        PopulateTypeCodeData(false, false, 0);
                                        CalculateAll(false, true, true, true, true);
                                        oAirportPairDetail = new UtilitiesService.AirportPairDetail();
                                        oAirportPairDetail = GetItemsAirportPairLegs(oAirportPairDetail);
                                        oAirportPairs.AirportPairDetails.Add(oAirportPairDetail);
                                    }
                                }
                                var Result = UtilitiesService.UpdateAirportPairs(oAirportPairs);
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    if (Session["AirportPairID"] != null)
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Utilities.AirportPair, Convert.ToInt64(Session["AirportPairID"].ToString()));
                                        Session["IsEditLockAirportPair"] = "False";
                                    }
                                }
                                if (Result.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                    IsSaveSuccessAirportPair = true;
                                }
                            }
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = true;
                            if (IsPopUp)
                            {
                                //Clear session & close browser
                                Session["AirportPairID"] = null;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPaxInfoPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
            }
        }
        protected void dgAirportPairs_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            e.Canceled = true;
                            UtilitiesService.AirportPair oAirportPairs = new UtilitiesService.AirportPair();
                            GridDataItem Item = dgAirportPairs.SelectedItems[0] as GridDataItem;
                            oAirportPairs.AirportPairID = Convert.ToInt64(Item.GetDataKeyValue("AirportPairID").ToString().Trim());
                            oAirportPairs.IsDeleted = true;
                            using (UtilitiesService.UtilitiesServiceClient UtilitiesService = new UtilitiesService.UtilitiesServiceClient())
                            {
                                if (Session["AirportPairID"] != null)
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Utilities.AirportPair, Convert.ToInt64(Session["AirportPairID"].ToString()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelectionAirportPairs(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Utilities.AirportPairs);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Utilities.AirportPairs);
                                        return;
                                    }
                                }
                                UtilitiesService.DeleteAirportPairs(oAirportPairs);
                            }
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = true;
                            DefaultSelectionAirportPairs(true);
                            DefaultSelectionAirportPairLegs(true);
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                    }
                    finally
                    {
                        if (Session["AirportPairID"] != null)
                        {
                            var returnValue1 = CommonService.UnLock(EntitySet.Utilities.AirportPair, Convert.ToInt64(Session["AirportPairID"].ToString()));
                        }
                    }
                }
            }
        }
        protected void dgAirportPairs_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem dataItem = e.Item as GridDataItem;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
            }
        }
        #endregion
        protected void btnSaveAirportPairs_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveAirportPairs.Value == "Update")
                        {
                            (dgAirportPairs.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgAirportPairs.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
            }
        }
        protected void btnCancelAirportPairs_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["AirportPairID"] != null)
                        {
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Utilities.AirportPair, Convert.ToInt64(Session["AirportPairID"].ToString().Trim()));
                            }
                        }
                        Session["AirportPairID"] = null;
                        DefaultSelectionAirportPairs(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
            }
        }
        private bool ValidateAirportPair()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool IsValidateCustom = true;
                if ((string.IsNullOrEmpty(tbAPAircraft.Text)) && (IsValidateCustom == true))
                {
                    rfvAPAircraft.IsValid = false;
                    RadAjaxManager1.FocusControl(tbAPAircraft.ClientID);
                    IsValidateCustom = false;
                }
                if ((string.IsNullOrEmpty(tbAPDescription.Text)) && (IsValidateCustom == true))
                {
                    rfvAPDescription.IsValid = false;
                    RadAjaxManager1.FocusControl(tbAPDescription.ClientID);
                    IsValidateCustom = false;
                }
                if ((CheckAircraftExist(tbAPAircraft, hdnAPAircraftID) == false) && (IsValidateCustom == true))
                {
                    cvAPAircraft.IsValid = false;
                    RadAjaxManager1.FocusControl(tbAPAircraft.ClientID);
                    IsValidateCustom = false;
                }
                return IsValidateCustom;
            }
        }
        private bool ValidateAirportPairLegs()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool IsValidateCustom = true;
                if ((string.IsNullOrEmpty(tbAPLTypeCode.Text)) && (IsValidateCustom == true))
                {
                    rfvAPLTypeCode.IsValid = false;
                    RadAjaxManager1.FocusControl(tbAPLTypeCode.ClientID);
                    IsValidateCustom = false;
                }
                if ((string.IsNullOrEmpty(tbAPLDeparture.Text)) && (IsValidateCustom == true))
                {
                    rfvAPLDeparture.IsValid = false;
                    RadAjaxManager1.FocusControl(tbAPLDeparture.ClientID);
                    IsValidateCustom = false;
                }
                if ((string.IsNullOrEmpty(tbAPLArrive.Text)) && (IsValidateCustom == true))
                {
                    rfvAPLArrive.IsValid = false;
                    RadAjaxManager1.FocusControl(tbAPLArrive.ClientID);
                    IsValidateCustom = false;
                }
                if ((CheckAircraftExist(tbAPLTypeCode, hdnAPLTypeCodeID) == false) && (IsValidateCustom == true))
                {
                    cvAPLTypeCode.IsValid = false;
                    RadAjaxManager1.FocusControl(tbAPLTypeCode.ClientID);
                    IsValidateCustom = false;
                }
                if ((CheckAirportICAOExist(tbAPLDeparture, hdnAPLDepartureID, lbAPLDepartDesc) == false) && (IsValidateCustom == true))
                {
                    cvAPLDeparture.IsValid = false;
                    RadAjaxManager1.FocusControl(tbAPLDeparture.ClientID);
                    IsValidateCustom = false;
                }
                if ((CheckAirportICAOExist(tbAPLArrive, hdnAPLArriveID, lbAPLAirriveDesc) == false) && (IsValidateCustom == true))
                {
                    cvAPLArrive.IsValid = false;
                    RadAjaxManager1.FocusControl(tbAPLArrive.ClientID);
                    IsValidateCustom = false;
                }
                if (IsValidateCustom == true)
                {
                    string ErrorString = CheckAircraftCapability();
                    if (!string.IsNullOrEmpty(ErrorString.ToString()))
                    {
                        IsValidateCustom = true;
                        string alertMsg = "radalert('" + ErrorString + "', 360, 50, 'Airport Pair Legs');";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                    }
                }
                return IsValidateCustom;
            }
        }
        protected void btnSaveAll_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((hdnSaveAirportPairs.Value == "Update") || (hdnSaveAirportPairs.Value == "Save"))
                        {
                            IsSaveSuccessAirportPair = false;
                            if (ValidateAirportPair() == false)
                            {
                                return;
                            }
                        }
                        if ((hdnSaveAirportPairLegs.Value == "Update") || (hdnSaveAirportPairLegs.Value == "Save"))
                        {
                            IsSaveSuccessAirportPairLegs = false;
                            if (ValidateAirportPairLegs() == false)
                            {
                                return;
                            }
                        }
                        if (hdnSaveAirportPairs.Value == "Save")
                        {
                            (dgAirportPairs.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            if (IsSaveSuccessAirportPair == true)
                            {
                                GridEnableAirportPairs(true, true, true);
                                EnableFormAirportPairs(false);
                                dgAirportPairs.Rebind();
                                DefaultSelectItemAirportPair();
                                GridEnableAirportPairLegs(true, true, true);
                                EnableFormAirportPairLegs(false);
                                dgAirportPairLegs.Rebind();
                                DefaultSelectItemAirportPairLegs();
                                hdnSaveAirportPairs.Value = string.Empty;
                                hdnSaveAirportPairLegs.Value = string.Empty;
                            }
                        }
                        else if (hdnSaveAirportPairs.Value == "Update")
                        {
                            (dgAirportPairs.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            if (IsSaveSuccessAirportPair == true)
                            {
                                GridEnableAirportPairs(true, true, true);
                                EnableFormAirportPairs(false);
                                dgAirportPairs.Rebind();
                                DefaultSelectItemAirportPair();
                                GridEnableAirportPairLegs(true, true, true);
                                EnableFormAirportPairLegs(false);
                                dgAirportPairLegs.Rebind();
                                DefaultSelectItemAirportPairLegs();
                                hdnSaveAirportPairs.Value = string.Empty;
                                hdnSaveAirportPairLegs.Value = string.Empty;
                            }
                        }
                        if ((hdnSaveAirportPairs.Value == string.Empty) && (hdnSaveAirportPairLegs.Value == "Save"))
                        {
                            (dgAirportPairLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            if (IsSaveSuccessAirportPairLegs == true)
                            {
                                GridEnableAirportPairLegs(true, true, true);
                                EnableFormAirportPairLegs(false);
                                dgAirportPairLegs.Rebind();
                                DefaultSelectItemAirportPairLegs();
                                hdnSaveAirportPairs.Value = string.Empty;
                                hdnSaveAirportPairLegs.Value = string.Empty;
                            }
                        }
                        else if ((hdnSaveAirportPairs.Value == string.Empty) && (hdnSaveAirportPairLegs.Value == "Update"))
                        {
                            (dgAirportPairLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            if (IsSaveSuccessAirportPairLegs == true)
                            {
                                GridEnableAirportPairLegs(true, true, true);
                                EnableFormAirportPairLegs(false);
                                dgAirportPairLegs.Rebind();
                                DefaultSelectItemAirportPairLegs();
                                hdnSaveAirportPairs.Value = string.Empty;
                                hdnSaveAirportPairLegs.Value = string.Empty;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
            }
        }
        protected void btnCancelAll_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //if ((hdnSaveAirportPairs.Value == "Update") || (hdnSaveAirportPairs.Value == "Save"))
                        //{
                        if (Session["AirportPairID"] != null)
                        {
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Utilities.AirportPair, Convert.ToInt64(Session["AirportPairID"].ToString().Trim()));
                            }
                        }
                        Session["AirportPairID"] = null;
                        ClearFormAirportPairs();
                        dgAirportPairs.SelectedIndexes.Clear();
                        DefaultSelectionAirportPairs(true);
                        //}
                        //if ((hdnSaveAirportPairLegs.Value == "Update") || (hdnSaveAirportPairLegs.Value == "Save"))
                        //{
                        if (Session["AirportPairDetailID"] != null)
                        {
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Utilities.AirportPairLeg, Convert.ToInt64(Session["AirportPairDetailID"].ToString().Trim()));
                            }
                        }
                        Session["AirportPairDetailID"] = null;
                        ClearFormAirportPairLegs();
                        dgAirportPairLegs.SelectedIndexes.Clear();
                        DefaultSelectionAirportPairLegs(true);
                        //}
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
            }
        }
        #endregion
        #region "Airport Pairs Legs Worksheet"
        private void DefaultSelectionAirportPairLegs(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgAirportPairLegs.Rebind();
                    }
                    if (dgAirportPairLegs.MasterTableView.Items.Count > 0)
                    {
                        dgAirportPairLegs.SelectedIndexes.Add(0);
                        Session["AirportPairDetailID"] = dgAirportPairLegs.Items[0].GetDataKeyValue("AirportPairDetailID").ToString();
                        LoadControlDataAirportPairLegs();
                        EnableFormAirportPairLegs(false);
                        GridEnableAirportPairLegs(true, true, true);
                    }
                    else
                    {
                        Session["AirportPairDetailID"] = null;
                        ClearFormAirportPairLegs();
                        EnableFormAirportPairLegs(false);
                        GridEnableAirportPairLegs(true, true, true);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void DefaultSelectItemAirportPairLegs()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Session["AirportPairDetailID"] != null)
                    {
                        string ID = Session["AirportPairDetailID"].ToString();
                        foreach (GridDataItem Item in dgAirportPairLegs.MasterTableView.Items)
                        {
                            if (Item.GetDataKeyValue("AirportPairDetailID").ToString() == ID.Trim())
                            {
                                Item.Selected = true;
                                break;
                            }
                        }
                        if (dgAirportPairLegs.SelectedItems.Count == 0)
                        {
                            DefaultSelectionAirportPairLegs(false);
                        }
                    }
                    else
                    {
                        DefaultSelectionAirportPairLegs(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private void GridEnableAirportPairLegs(bool Add, bool Edit, bool Delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Add, Edit, Delete))
            {
                LinkButton insertCtl, editCtl, delCtl;
                insertCtl = (LinkButton)dgAirportPairLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                editCtl = (LinkButton)dgAirportPairLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                delCtl = (LinkButton)dgAirportPairLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                if (IsAuthorized(Permission.Utilities.AddAirportPairLegs))
                {
                    insertCtl.Visible = true;
                    if (Add && dgAirportPairs.Items.Count > 0)
                        insertCtl.Enabled = true;
                    else
                        insertCtl.Enabled = false;
                }
                else
                {
                    insertCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Utilities.EditAirportPairLegs))
                {
                    editCtl.Visible = true;
                    if (Edit && dgAirportPairs.Items.Count > 0)
                    {
                        editCtl.Enabled = true;
                        editCtl.OnClientClick = "javascript:return ProcessUpdateAirportPairLegs();";
                    }
                    else
                    {
                        editCtl.Enabled = false;
                        editCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    editCtl.Visible = false;
                }
                if (IsAuthorized(Permission.Utilities.DeleteAirportPairLegs))
                {
                    delCtl.Visible = true;
                    if (Delete && dgAirportPairs.Items.Count > 0)
                    {
                        delCtl.Enabled = true;
                        delCtl.OnClientClick = "javascript:return ProcessDeleteAirportPairLegs();";
                    }
                    else
                    {
                        delCtl.Enabled = false;
                        delCtl.OnClientClick = string.Empty;
                    }
                }
                else
                {
                    delCtl.Visible = false;
                }
            }
        }
        private void EnableFormAirportPairLegs(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                tbAPLTypeCode.Enabled = Enable;
                btnBrowseAPLTypeCode.Enabled = Enable;
                tbAPLFileNumber.Enabled = false;
                tbAPLDescription.Enabled = false;
                tbAPLDeparture.Enabled = Enable;
                btnBrowseAPLDeparture.Enabled = Enable;
                tbAPLArrive.Enabled = Enable;
                btnBrowseAPLArrive.Enabled = Enable;
                tbAPLTotalCost.Enabled = false;
                tbAPLMiles.Enabled = Enable;
                tbAPLTOBias.Enabled = Enable;
                imgbtnAPLTOBias.Enabled = Enable;
                tbAPLTAS.Enabled = Enable;
                rblistAPLWindReliability.Enabled = Enable;
                tbAPLPower.Enabled = Enable;
                btnBrowseAPLPower.Enabled = Enable;
                tbAPLLandBias.Enabled = Enable;
                btnBrowseAPLLandBias.Enabled = Enable;
                tbAPLWinds.Enabled = Enable;
                ddlAPLQuarter.Enabled = Enable;
                tbAPLEte.Enabled = false;
                tbAPLCost.Enabled = false;
                btnAPLWinds.Enabled = !(Enable);
                btnSaveAirportPairLegs.Visible = Enable;
                btnCancelAirportPairLegs.Visible = Enable;
                btnSaveAll.Visible = Enable;
                btnCancelAll.Visible = Enable;
                btnSaveAll.ValidationGroup = "SaveAirportPairLegs";
            }
        }
        private void ClearFormAirportPairLegs()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbAPLTypeCode.Text = string.Empty;
                hdnAPLTypeCodeID.Value = string.Empty;
                hdnAPLMultiTypeCodeID.Value = string.Empty;
                lbAPLMultiTypeCodeCD.Text = string.Empty;
                tbAPLFileNumber.Text = string.Empty;
                hdnAPLAirportPairLegID.Value = string.Empty;
                tbAPLDescription.Text = string.Empty;
                tbAPLDeparture.Text = string.Empty;
                hdnAPLDepartureID.Value = string.Empty;
                lbAPLDepartDesc.Text = string.Empty;
                tbAPLArrive.Text = string.Empty;
                hdnAPLArriveID.Value = string.Empty;
                lbAPLAirriveDesc.Text = string.Empty;
                tbAPLTotalCost.Text = string.Empty;
                tbAPLTotalCost.Text = CurrencyType + "0.00";
                tbAPLMiles.Text = string.Empty;
                hdAPLMiles.Value = string.Empty;
                tbAPLTOBias.Text = string.Empty;
                tbAPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", "0.00");
                tbAPLTAS.Text = string.Empty;
                rblistAPLWindReliability.SelectedIndex = 2;
                tbAPLPower.Text = string.Empty;
                tbAPLLandBias.Text = string.Empty;
                tbAPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", "0.00");
                tbAPLWinds.Text = string.Empty;
                ddlAPLQuarter.SelectedIndex = 0;
                tbAPLEte.Text = string.Empty;
                tbAPLEte.Text = string.Format("{0:" + TenthMinFormat + "}", "0.00");
                tbAPLCost.Text = string.Empty;
                tbAPLCost.Text = CurrencyType + string.Format("{0:" + CurrencyFormat + "}", "0.00");
                hdnSaveAirportPairLegs.Value = string.Empty;
                //When new
                tbAPLFileNumber.Text = tbAPFileNumber.Text;
                tbAPLTypeCode.Text = tbAPAircraft.Text;
                hdnAPLTypeCodeID.Value = hdnAPAircraftID.Value;
                tbAPLDescription.Text = tbAPDescription.Text;
                ddlAPLQuarter.SelectedValue = ddlAPQuarter.SelectedValue;
            }
        }
        private void LoadControlDataAirportPairLegs()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgAirportPairLegs.SelectedItems.Count != 0)
                {
                    GridDataItem Item = dgAirportPairLegs.SelectedItems[0] as GridDataItem;
                    if (Item.GetDataKeyValue("AirportPairDetailID").ToString().Trim().ToUpper() == Session["AirportPairDetailID"].ToString().Trim().ToUpper())
                    {
                        if (Item.GetDataKeyValue("AirportPairDetailID") != null)
                        {
                            hdnAPLAirportPairLegID.Value = Item.GetDataKeyValue("AirportPairDetailID").ToString().Trim();
                        }
                        else
                        {
                            hdnAPLAirportPairLegID.Value = string.Empty;
                        }
                        if (Item.GetDataKeyValue("AircraftCD") != null)
                        {
                            tbAPLTypeCode.Text = Item.GetDataKeyValue("AircraftCD").ToString().Trim();
                        }
                        else
                        {
                            tbAPLTypeCode.Text = tbAPAircraft.Text;
                        }
                        if (Item.GetDataKeyValue("AircraftID") != null)
                        {
                            hdnAPLTypeCodeID.Value = Item.GetDataKeyValue("AircraftID").ToString().Trim();
                        }
                        else
                        {
                            hdnAPLTypeCodeID.Value = hdnAPAircraftID.Value;
                        }
                        if (Item.GetDataKeyValue("AirportPairNumber") != null)
                        {
                            tbAPLFileNumber.Text = Item.GetDataKeyValue("AirportPairNumber").ToString().Trim();
                        }
                        else
                        {
                            tbAPLFileNumber.Text = tbAPFileNumber.Text;
                        }
                        if (Item.GetDataKeyValue("AirportPairDescription") != null)
                        {
                            tbAPLDescription.Text = Item.GetDataKeyValue("AirportPairDescription").ToString().Trim();
                        }
                        else
                        {
                            tbAPLDescription.Text = tbAPDescription.Text;
                        }
                        if (Item.GetDataKeyValue("DAirportIcaoID") != null)
                        {
                            tbAPLDeparture.Text = Item.GetDataKeyValue("DAirportIcaoID").ToString().Trim();
                        }
                        else
                        {
                            tbAPLDeparture.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("DAirportID") != null)
                        {
                            hdnAPLDepartureID.Value = Item.GetDataKeyValue("DAirportID").ToString().Trim();
                        }
                        else
                        {
                            hdnAPLDepartureID.Value = string.Empty;
                        }
                        CheckAirportICAOExist(tbAPLDeparture, hdnAPLDepartureID, lbAPLDepartDesc);
                        if (Item.GetDataKeyValue("AAirportIcaoID") != null)
                        {
                            tbAPLArrive.Text = Item.GetDataKeyValue("AAirportIcaoID").ToString().Trim();
                        }
                        else
                        {
                            tbAPLArrive.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("AAirportID") != null)
                        {
                            hdnAPLArriveID.Value = Item.GetDataKeyValue("AAirportID").ToString().Trim();
                        }
                        else
                        {
                            hdnAPLArriveID.Value = string.Empty;
                        }
                        CheckAirportICAOExist(tbAPLArrive, hdnAPLArriveID, lbAPLAirriveDesc);
                        if (Item.GetDataKeyValue("Distance") != null)
                        {
                            tbAPLMiles.Text = ConvertToKilomenterBasedOnCompanyProfile((decimal)Item.GetDataKeyValue("Distance")).ToString();
                            hdAPLMiles.Value = Item.GetDataKeyValue("Distance").ToString();
                        }
                        else
                        {
                            tbAPLMiles.Text = string.Empty;
                            hdAPLMiles.Value = string.Empty;
                        }
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                        {
                            if (Item.GetDataKeyValue("TakeoffBIAS") != null)
                            {
                                tbAPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", Math.Round(Convert.ToDecimal(Item.GetDataKeyValue("TakeoffBIAS").ToString().Trim()), 1)).Trim();
                            }
                            else
                            {
                                tbAPLTOBias.Text = string.Empty;
                            }
                        }
                        else
                        {
                            if (Item.GetDataKeyValue("TakeoffBIAS") != null)
                            {
                                tbAPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", ConvertTenthsToMins(Item.GetDataKeyValue("TakeoffBIAS").ToString().Trim())).Trim();
                            }
                            else
                            {
                                tbAPLTOBias.Text = string.Empty;
                            }
                        }
                        if (Item.GetDataKeyValue("TrueAirSpeed") != null)
                        {
                            tbAPLTAS.Text = Item.GetDataKeyValue("TrueAirSpeed").ToString().Trim();
                        }
                        else
                        {
                            tbAPLTAS.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("WindReliability") != null)
                        {
                            rblistAPLWindReliability.SelectedValue = Item.GetDataKeyValue("WindReliability").ToString().Trim();
                        }
                        else
                        {
                            rblistAPLWindReliability.SelectedIndex = 0;
                        }
                        if (Item.GetDataKeyValue("PowerSetting") != null)
                        {
                            tbAPLPower.Text = Item.GetDataKeyValue("PowerSetting").ToString().Trim();
                        }
                        else
                        {
                            tbAPLPower.Text = string.Empty;
                        }
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                        {
                            if (Item.GetDataKeyValue("LandingBIAS") != null)
                            {
                                tbAPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", Math.Round(Convert.ToDecimal(Item.GetDataKeyValue("LandingBIAS").ToString().Trim()), 1)).Trim();
                            }
                            else
                            {
                                tbAPLLandBias.Text = string.Empty;
                            }
                        }
                        else
                        {
                            if (Item.GetDataKeyValue("LandingBIAS") != null)
                            {
                                tbAPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", ConvertTenthsToMins(Item.GetDataKeyValue("LandingBIAS").ToString().Trim())).Trim();
                            }
                            else
                            {
                                tbAPLLandBias.Text = string.Empty;
                            }
                        }
                        if (Item.GetDataKeyValue("Winds") != null)
                        {
                            tbAPLWinds.Text = Item.GetDataKeyValue("Winds").ToString().Trim();
                        }
                        else
                        {
                            tbAPLWinds.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("QTR") != null)
                        {
                            ddlAPLQuarter.SelectedValue = Item.GetDataKeyValue("QTR").ToString().Trim();
                        }
                        else
                        {
                            ddlAPLQuarter.SelectedValue = ddlAPQuarter.SelectedValue;
                        }
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                        {
                            if (Item.GetDataKeyValue("ElapseTM") != null)
                            {
                                tbAPLEte.Text = string.Format("{0:" + TenthMinFormat + "}", Math.Round(Convert.ToDecimal(Item.GetDataKeyValue("ElapseTM").ToString().Trim()), 1));
                            }
                            else
                            {
                                tbAPLEte.Text = string.Empty;
                            }
                        }
                        else
                        {
                            if (Item.GetDataKeyValue("ElapseTM") != null)
                            {
                                tbAPLEte.Text = string.Format("{0:" + TenthMinFormat + "}", ConvertTenthsToMins(Item.GetDataKeyValue("ElapseTM").ToString().Trim()));
                            }
                            else
                            {
                                tbAPLEte.Text = string.Empty;
                            }
                        }
                        if (Item.GetDataKeyValue("Cost") != null)
                        {
                            tbAPLCost.Text = CurrencyType + string.Format("{0:" + CurrencyFormat + "}", Math.Round(Convert.ToDecimal(Item.GetDataKeyValue("Cost").ToString().Trim()), 2));
                        }
                        else
                        {
                            tbAPLCost.Text = string.Empty;
                            tbAPLCost.Text = CurrencyType + string.Format("{0:" + CurrencyFormat + "}", "0.00");
                        }
                        tbAPLTotalCost.Text = "0";
                        tbAPLTotalCost.Text = CurrencyType + string.Format("{0:" + CurrencyFormat + "}", "0.00");
                        CalculateTotalCost();
                        Label lbLastUpdatedUser;
                        lbLastUpdatedUser = (Label)dgAirportPairLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                        if (Item.GetDataKeyValue("LastUpdUID") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                        }
                        else
                        {
                            lbLastUpdatedUser.Text = string.Empty;
                        }
                        if (Item.GetDataKeyValue("LastUpdTS") != null)
                        {
                            lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString())));
                        }
                    }
                }
                else
                {
                    tbAPLFileNumber.Text = tbAPFileNumber.Text;
                    tbAPLTypeCode.Text = tbAPAircraft.Text;
                    hdnAPLTypeCodeID.Value = hdnAPAircraftID.Value;
                    tbAPLDescription.Text = tbAPDescription.Text;
                    ddlAPLQuarter.SelectedValue = ddlAPQuarter.SelectedValue;
                }
            }
        }
        private UtilitiesService.AirportPairDetail GetItemsAirportPairLegs(UtilitiesService.AirportPairDetail oAirportPairDetail)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oAirportPairDetail))
            {
                if (hdnSaveAirportPairLegs.Value == "Update")
                {
                    if (!string.IsNullOrEmpty(hdnAPLAirportPairLegID.Value))
                    {
                        oAirportPairDetail.AirportPairDetailID = Convert.ToInt64(hdnAPLAirportPairLegID.Value);
                    }
                }
                else
                {
                    Identity = Identity - 1;
                    oAirportPairDetail.AirportPairDetailID = Identity;
                }
                //if (!string.IsNullOrEmpty(hdnAPLAirportPairLegID.Value))
                //{
                //    oAirportPairDetail.AirportPairDetailID = Convert.ToInt64(hdnAPLAirportPairLegID.Value);
                //}
                if (!string.IsNullOrEmpty(hdnAPAirportPairID.Value))
                {
                    oAirportPairDetail.AirportPairID = Convert.ToInt64(hdnAPAirportPairID.Value);
                }
                if (!string.IsNullOrEmpty(hdnAPLTypeCodeID.Value))
                {
                    oAirportPairDetail.AircraftID = Convert.ToInt64(hdnAPLTypeCodeID.Value);
                }
                if (!string.IsNullOrEmpty(ddlAPLQuarter.SelectedValue))
                {
                    oAirportPairDetail.QTR = Convert.ToDecimal(ddlAPLQuarter.SelectedValue);
                }
                if (!string.IsNullOrEmpty(hdnAPLDepartureID.Value))
                {
                    oAirportPairDetail.DAirportID = Convert.ToInt64(hdnAPLDepartureID.Value);
                }
                if (!string.IsNullOrEmpty(hdnAPLArriveID.Value))
                {
                    oAirportPairDetail.AAirportID = Convert.ToInt64(hdnAPLArriveID.Value);
                }
                //if (!string.IsNullOrEmpty(tbAPLMiles.Text))
                //{
                //    oAirportPairDetail.Distance = Convert.ToDecimal(tbAPLMiles.Text);
                //}
                if (!string.IsNullOrEmpty(hdAPLMiles.Value))
                {
                    oAirportPairDetail.Distance = Convert.ToDecimal(hdAPLMiles.Value);
                }
                if (!string.IsNullOrEmpty(tbAPLPower.Text))
                {
                    oAirportPairDetail.PowerSetting = tbAPLPower.Text;
                }
                if (!string.IsNullOrEmpty(tbAPLTOBias.Text))
                {
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                    {
                        oAirportPairDetail.TakeoffBIAS = Convert.ToDecimal(tbAPLTOBias.Text);
                    }
                    else
                    {
                        oAirportPairDetail.TakeoffBIAS = Convert.ToDecimal(ConvertMinToTenths(tbAPLTOBias.Text, true, string.Empty));
                    }
                }
                if (!string.IsNullOrEmpty(tbAPLLandBias.Text))
                {
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                    {
                        oAirportPairDetail.LandingBIAS = Convert.ToDecimal(tbAPLLandBias.Text);
                    }
                    else
                    {
                        oAirportPairDetail.LandingBIAS = Convert.ToDecimal(ConvertMinToTenths(tbAPLLandBias.Text, true, string.Empty));
                    }
                }
                if (!string.IsNullOrEmpty(tbAPLTAS.Text))
                {
                    oAirportPairDetail.TrueAirSpeed = Convert.ToDecimal(tbAPLTAS.Text);
                }
                if (!string.IsNullOrEmpty(tbAPLWinds.Text))
                {
                    oAirportPairDetail.Winds = Convert.ToDecimal(tbAPLWinds.Text);
                }
                if (!string.IsNullOrEmpty(tbAPLEte.Text))
                {
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                    {
                        oAirportPairDetail.ElapseTM = Convert.ToDecimal(tbAPLEte.Text);
                    }
                    else
                    {
                        oAirportPairDetail.ElapseTM = Convert.ToDecimal(ConvertMinToTenths(tbAPLEte.Text, true, string.Empty));
                    }
                }
                if (!string.IsNullOrEmpty(tbAPLCost.Text))
                {
                    oAirportPairDetail.Cost = Convert.ToDecimal(tbAPLCost.Text.Replace(CurrencyType, string.Empty));
                }
                if (!string.IsNullOrEmpty(ddlAPLQuarter.SelectedValue))
                {
                    oAirportPairDetail.WindReliability = Convert.ToInt32(rblistAPLWindReliability.SelectedValue);
                }
                oAirportPairDetail.IsDeleted = false;
                return oAirportPairDetail;
            }
        }
        protected void btnSaveAirportPairLegs_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSaveAirportPairLegs.Value == "Update")
                        {
                            (dgAirportPairLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgAirportPairLegs.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        protected void btnCancelAirportPairLegs_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["AirportPairDetailID"] != null)
                        {
                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                            {
                                var returnValue = CommonService.UnLock(EntitySet.Utilities.AirportPairLeg, Convert.ToInt64(Session["AirportPairDetailID"].ToString().Trim()));
                            }
                        }
                        Session["AirportPairDetailID"] = null;
                        DefaultSelectionAirportPairLegs(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        protected void tbAPLTypeCode_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbAPLTypeCode.Text))
                        {
                            if (CheckAircraftExist(tbAPLTypeCode, hdnAPLTypeCodeID) == false)
                            {
                                cvAPLTypeCode.IsValid = false;
                                RadAjaxManager1.FocusControl(tbAPLTypeCode.ClientID);
                            }
                            else
                            {
                                PopulateTypeCodeData(true, true, 0);
                            }
                        }
                        else
                        {
                            rfvAPLTypeCode.IsValid = false;
                            RadAjaxManager1.FocusControl(tbAPLTypeCode.ClientID);
                        }
                        if (hdnAPLMultiTypeCodeID.Value.Contains(","))
                        {
                            lbAPLMultiTypeCodeCD.Text = "Multiple";
                        }
                        else
                        {
                            lbAPLMultiTypeCodeCD.Text = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairs);
                }
            }
        }
        protected void tbAPLDeparture_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbAPLDeparture.Text))
                        {
                            if (CheckAirportICAOExist(tbAPLDeparture, hdnAPLDepartureID, lbAPLDepartDesc) == false)
                            {
                                cvAPLDeparture.IsValid = false;
                                RadAjaxManager1.FocusControl(tbAPLDeparture.ClientID);
                                return;
                            }
                            else
                            {
                                CalculateAll(true, true, true, true, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        protected void tbAPLArrive_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbAPLArrive.Text))
                        {
                            if (CheckAirportICAOExist(tbAPLArrive, hdnAPLArriveID, lbAPLAirriveDesc) == false)
                            {
                                cvAPLArrive.IsValid = false;
                                RadAjaxManager1.FocusControl(tbAPLArrive.ClientID);
                                return;
                            }
                            else
                            {
                                CalculateAll(true, true, true, true, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        private bool CheckAirportICAOExist(TextBox TxtBx, HiddenField HdnFld, Label lbAirportDesc)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TxtBx, HdnFld, lbAirportDesc))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool ReturnValue = true;
                    if (!string.IsNullOrEmpty(TxtBx.Text))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objMasterService.GetAirportByAirportICaoID(TxtBx.Text.Trim().ToUpper().ToString()).EntityList;
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.GetAllAirport> AirportList = new List<FlightPakMasterService.GetAllAirport>();
                                AirportList = (List<FlightPakMasterService.GetAllAirport>)objRetVal.ToList();
                                HdnFld.Value = AirportList[0].AirportID.ToString();
                                lbAirportDesc.Text = System.Web.HttpUtility.HtmlEncode(AirportList[0].AirportName.ToString());
                                string builder = string.Empty;
                                builder = "ICAO : " + (AirportList[0].IcaoID != null ? AirportList[0].IcaoID : string.Empty)
                                    + "\n" + "City : " + (AirportList[0].CityName != null ? AirportList[0].CityName : string.Empty)
                                    + "\n" + "State/Province : " + (AirportList[0].StateName != null ? AirportList[0].StateName : string.Empty)
                                    + "\n" + "Country : " + (AirportList[0].CountryName != null ? AirportList[0].CountryName : string.Empty)
                                    + "\n" + "Airport : " + (AirportList[0].AirportName != null ? AirportList[0].AirportName : string.Empty)
                                    + "\n" + "DST Region : " + (AirportList[0].DSTRegionCD != null ? AirportList[0].DSTRegionCD : string.Empty)
                                    + "\n" + "UTC+/- : " + (AirportList[0].OffsetToGMT != null ? AirportList[0].OffsetToGMT.ToString() : string.Empty)
                                    + "\n" + "Longest Runway : " + (AirportList[0].LongestRunway != null ? AirportList[0].LongestRunway.ToString() : string.Empty)
                                    + "\n" + "IATA : " + (AirportList[0].Iata != null ? AirportList[0].Iata.ToString() : string.Empty);
                                lbAirportDesc.ToolTip = builder;
                                ReturnValue = true;
                            }
                            else
                            {
                                HdnFld.Value = string.Empty;
                                lbAirportDesc.Text = string.Empty;
                                ReturnValue = false;
                            }
                        }
                    }
                    return ReturnValue;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        #region "dgAirportPairLegs grid events"
        private void Bind_AirportPairLegs(bool IsDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                UtilitiesService.AirportPairDetail oAirportPairDetail = new UtilitiesService.AirportPairDetail();
                if (Session["AirportPairID"] != null)
                {
                    oAirportPairDetail.AirportPairID = Convert.ToInt64(Session["AirportPairID"].ToString());
                }
                oAirportPairDetail.IsDeleted = false;
                List<UtilitiesService.GetAirportPairDetailByPairNumber> GetAirportPairDetailList = new List<UtilitiesService.GetAirportPairDetailByPairNumber>();
                using (UtilitiesService.UtilitiesServiceClient UtilitiesService = new UtilitiesService.UtilitiesServiceClient())
                {
                    var GetAirportPairDetailInfo = UtilitiesService.GetAirportPairDetailByPairNumber(oAirportPairDetail);
                    if (GetAirportPairDetailInfo.ReturnFlag == true)
                    {
                        GetAirportPairDetailList = GetAirportPairDetailInfo.EntityList;
                    }
                    dgAirportPairLegs.DataSource = GetAirportPairDetailList;
                    if (IsDataBind == true)
                    {
                        dgAirportPairLegs.DataBind();
                    }
                    Session["AirportPairDetailList"] = GetAirportPairDetailList;
                }
            }
        }
        protected void dgAirportPairLegs_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Bind_AirportPairLegs(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        protected void dgAirportPairLegs_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((btnSaveAll.Visible == false) || (hdnSaveAirportPairLegs.Value == string.Empty))
                        {
                            GridDataItem item = dgAirportPairLegs.SelectedItems[0] as GridDataItem;
                            Session["AirportPairDetailID"] = item.GetDataKeyValue("AirportPairDetailID").ToString();
                            LoadControlDataAirportPairLegs();
                            EnableFormAirportPairLegs(false);
                            GridEnableAirportPairLegs(true, true, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        protected void dgAirportPairLegs_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                dgAirportPairLegs.SelectedIndexes.Clear();
                                GridEnableAirportPairLegs(false, false, false);
                                ClearFormAirportPairLegs();
                                EnableFormAirportPairLegs(true);
                                PopulateTypeCodeData(true, true, 0);
                                hdnSaveAirportPairLegs.Value = "Save";
                                DefaulMonth();
                                RadAjaxManager1.FocusControl(tbAPLTypeCode.ClientID);
                                break;
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    if (Session["AirportPairDetailID"] != null)
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Utilities.AirportPairLeg, Convert.ToInt64(Session["AirportPairDetailID"].ToString()));
                                        Session["IsEditLockAirportPairLegs"] = "True";
                                        if (!returnValue.ReturnFlag)
                                        {
                                            if (IsPopUp)
                                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Utilities.AirportPairLegs);
                                            else
                                                ShowAlert(returnValue.LockMessage, ModuleNameConstants.Utilities.AirportPairLegs);
                                            return;
                                        }
                                    }
                                }
                                GridEnableAirportPairLegs(false, false, false);
                                EnableFormAirportPairLegs(true);
                                //LoadControlDataAirportPairLegs();
                                hdnSaveAirportPairLegs.Value = "Update";
                                RadAjaxManager1.FocusControl(tbAPLTypeCode.ClientID);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    column.CurrentFilterValue = string.Empty;
                                    column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        protected void dgAirportPairLegs_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidateCustom = true;
                        //IsValidateCustom = ValidateAirportPairLegs();
                        if (IsValidateCustom == true)
                        {
                            UtilitiesService.AirportPairDetail oAirportPairDetail = new UtilitiesService.AirportPairDetail();
                            using (UtilitiesService.UtilitiesServiceClient UtilitiesService = new UtilitiesService.UtilitiesServiceClient())
                            {
                                string[] TypeCodeList = new string[100];
                                if (!string.IsNullOrEmpty(hdnAPLMultiTypeCodeID.Value))
                                {
                                    TypeCodeList = hdnAPLMultiTypeCodeID.Value.Split(',');
                                    for (int Index = 0; Index < TypeCodeList.Length; Index++)
                                    {
                                        if (!string.IsNullOrEmpty(TypeCodeList[Index]))
                                        {
                                            hdnAPLTypeCodeID.Value = TypeCodeList[Index].Trim();
                                            if (Index == 0)
                                            {
                                                PopulateTypeCodeData(false, false, 0);
                                                CalculateAll(false, true, true, true, true);
                                            }
                                            else
                                            {
                                                PopulateTypeCodeData(true, true, 0);
                                                CalculateAll(true, true, true, true, true);
                                            }
                                            oAirportPairDetail = GetItemsAirportPairLegs(oAirportPairDetail);
                                            var Result = UtilitiesService.AddAirportPairsDetail(oAirportPairDetail);
                                            if (Result.ReturnFlag == true)
                                            {
                                                ShowSuccessMessage();
                                                IsSaveSuccessAirportPairLegs = true;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    PopulateTypeCodeData(false, false, 0);
                                    CalculateAll(false, true, true, true, true);
                                    oAirportPairDetail = GetItemsAirportPairLegs(oAirportPairDetail);
                                    var Result = UtilitiesService.AddAirportPairsDetail(oAirportPairDetail);
                                    if (Result.ReturnFlag == true)
                                    {
                                        ShowSuccessMessage();
                                        IsSaveSuccessAirportPairLegs = true;
                                    }
                                }
                            }
                            if (IsPopUp)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPaxInfoPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
                finally
                {
                }
            }
        }
        protected void dgAirportPairLegs_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidateCustom = true;
                        //IsValidateCustom = ValidateAirportPairLegs();
                        if (IsValidateCustom == true)
                        {
                            UtilitiesService.AirportPairDetail oAirportPairDetail = new UtilitiesService.AirportPairDetail();
                            using (UtilitiesService.UtilitiesServiceClient UtilitiesService = new UtilitiesService.UtilitiesServiceClient())
                            {
                                string[] TypeCodeList = new string[100];
                                if (!string.IsNullOrEmpty(hdnAPLMultiTypeCodeID.Value))
                                {
                                    TypeCodeList = hdnAPLMultiTypeCodeID.Value.Split(',');
                                    for (int Index = 0; Index < TypeCodeList.Length; Index++)
                                    {
                                        if (!string.IsNullOrEmpty(TypeCodeList[Index].Trim()))
                                        {
                                            hdnAPLTypeCodeID.Value = TypeCodeList[Index].Trim();
                                            if (Index == 0)
                                            {
                                                PopulateTypeCodeData(false, false, 0);
                                                CalculateAll(false, true, true, true, true);
                                            }
                                            else
                                            {
                                                PopulateTypeCodeData(true, true, 0);
                                                CalculateAll(true, true, true, true, true);
                                            }
                                            oAirportPairDetail = GetItemsAirportPairLegs(oAirportPairDetail);
                                            var Result = UtilitiesService.UpdateAirportPairsDetail(oAirportPairDetail);
                                            if (Result.ReturnFlag == true)
                                            {
                                                ShowSuccessMessage();
                                                IsSaveSuccessAirportPairLegs = true;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    PopulateTypeCodeData(false, false, 0);
                                    CalculateAll(false, true, true, true, true);
                                    oAirportPairDetail = GetItemsAirportPairLegs(oAirportPairDetail);
                                    var Result = UtilitiesService.UpdateAirportPairsDetail(oAirportPairDetail);
                                    if (Result.ReturnFlag == true)
                                    {
                                        ShowSuccessMessage();
                                        IsSaveSuccessAirportPairLegs = true;
                                    }
                                }
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    if (Session["AirportPairDetailID"] != null)
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Utilities.AirportPairLeg, Convert.ToInt64(Session["AirportPairDetailID"].ToString()));
                                        Session["IsEditLockAirportPairLegs"] = "False";
                                    }
                                }
                            }
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = true;
                            if (IsPopUp)
                            {
                                //Clear session & close browser
                                Session["AirportPairDetailID"] = null;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close Browser Window", "var oWnd = GetRadWindow(); oWnd.close();var dialog1 = oWnd.get_windowManager().getWindowByName('radPaxInfoPopup');var contentWin = dialog1.get_contentFrame().contentWindow;contentWin.rebindgrid();", true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        protected void dgAirportPairLegs_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            e.Canceled = true;
                            UtilitiesService.AirportPairDetail oAirportPairDetail = new UtilitiesService.AirportPairDetail();
                            GridDataItem Item = dgAirportPairLegs.SelectedItems[0] as GridDataItem;
                            oAirportPairDetail.AirportPairDetailID = Convert.ToInt64(Item.GetDataKeyValue("AirportPairDetailID").ToString().Trim());
                            oAirportPairDetail.IsDeleted = true;
                            using (UtilitiesService.UtilitiesServiceClient UtilitiesService = new UtilitiesService.UtilitiesServiceClient())
                            {
                                if (Session["AirportPairDetailID"] != null)
                                {
                                    var returnValue = CommonService.Lock(EntitySet.Utilities.AirportPairLeg, Convert.ToInt64(Session["AirportPairDetailID"].ToString()));
                                    if (!returnValue.ReturnFlag)
                                    {
                                        e.Item.Selected = true;
                                        DefaultSelectionAirportPairs(false);
                                        if (IsPopUp)
                                            ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Utilities.AirportPairLegs);
                                        else
                                            ShowAlert(returnValue.LockMessage, ModuleNameConstants.Utilities.AirportPairLegs);
                                        return;
                                    }
                                }
                                UtilitiesService.DeleteAirportPairsDetail(oAirportPairDetail);
                            }
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = true;
                            DefaultSelectionAirportPairLegs(true);
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                    }
                    finally
                    {
                        if (Session["AirportPairDetailID"] != null)
                        {
                            var returnValue1 = CommonService.UnLock(EntitySet.Utilities.AirportPairLeg, Convert.ToInt64(Session["AirportPairDetailID"].ToString()));
                        }
                    }
                }
            }
        }
        protected void dgAirportPairLegs_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            //GridDataItem dataItem = e.Item as GridDataItem;
                            GridDataItem item = (GridDataItem)e.Item;
                            if (item["TakeoffBIAS"] != null && !string.IsNullOrEmpty(item["TakeoffBIAS"].Text) && item["TakeoffBIAS"].Text != "&nbsp;")
                            {
                                if (UserPrincipal.Identity._fpSettings != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                    item["TakeoffBIAS"].Text = System.Web.HttpUtility.HtmlEncode(ConvertTenthsToMins(Math.Round(Convert.ToDecimal(item["TakeoffBIAS"].Text), 3).ToString()));
                                else
                                    item["TakeoffBIAS"].Text = System.Web.HttpUtility.HtmlEncode(Math.Round(Convert.ToDecimal(item["TakeoffBIAS"].Text), 1).ToString());
                            }
                            if (item["LandingBIAS"] != null && !string.IsNullOrEmpty(item["LandingBIAS"].Text) && item["LandingBIAS"].Text != "&nbsp;")
                            {
                                if (UserPrincipal.Identity._fpSettings != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                    item["LandingBIAS"].Text = System.Web.HttpUtility.HtmlEncode(ConvertTenthsToMins(Math.Round(Convert.ToDecimal(item["LandingBIAS"].Text), 3).ToString()));
                                else
                                    item["LandingBIAS"].Text = System.Web.HttpUtility.HtmlEncode(Math.Round(Convert.ToDecimal(item["LandingBIAS"].Text), 1).ToString());
                            }
                            if (item["ElapseTM"] != null && !string.IsNullOrEmpty(item["ElapseTM"].Text) && item["ElapseTM"].Text != "&nbsp;")
                            {
                                if (UserPrincipal.Identity._fpSettings != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                    item["ElapseTM"].Text = System.Web.HttpUtility.HtmlEncode(ConvertTenthsToMins(Math.Round(Convert.ToDecimal(item["ElapseTM"].Text), 3).ToString()));
                                else
                                    item["ElapseTM"].Text = System.Web.HttpUtility.HtmlEncode(Math.Round(Convert.ToDecimal(item["ElapseTM"].Text), 1).ToString());
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        #endregion
        protected void tbAPLMiles_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbAPLMiles.Text))
                        {
                            decimal Miles = 0;
                            hdAPLMiles.Value = "0";
                            if (decimal.TryParse(tbAPLMiles.Text, out Miles))
                            {
                                hdAPLMiles.Value = ConvertToMilesBasedOnCompanyProfile(Miles).ToString();
                            }
                            CalculateAll(false, true, true, true, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        protected void tbAPLPower_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbAPLPower.Text))
                        {
                            PopulateTypeCodeData(true, false, 0);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        protected void tbAPLTOBias_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbAPLTOBias.Text))
                        {
                            CalculateAll(false, true, true, true, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        protected void tbAPLLandBias_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbAPLLandBias.Text))
                        {
                            CalculateAll(false, true, true, true, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        protected void tbAPLTAS_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbAPLTAS.Text))
                        {
                            CalculateAll(false, true, true, true, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        protected void rblistAPLWindReliability_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CalculateAll(false, true, true, true, true);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        protected void ddlAPLQuarter_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbAPLPower.Text))
                        {
                            CalculateWind();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Utilities.AirportPairLegs);
                }
            }
        }
        private bool CheckIsBiasValid(string BiasValue)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(BiasValue))
            {
                return true;
            }
        }
        private string CheckAircraftCapability()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                StringBuilder ErrorString = new StringBuilder();
                ErrorString.Append(CheckAircraftCapabilityLegDistance());
                ErrorString.Append(CheckAircraftCapabilityLegTime());
                return ErrorString.ToString();
            }
        }
        private string CheckAircraftCapabilityLegDistance()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                string ReturnString = string.Empty;
                Int64 AircraftID;
                double LegDistance = 0;
                double aircrPS1 = 0.0, aircrPS2 = 0.0, aircrPS3 = 0.0;
                string[] TypeCodeList = new string[100];
                if (!string.IsNullOrEmpty(hdAPLMiles.Value))
                {
                    LegDistance = Convert.ToDouble(hdAPLMiles.Value);
                }
                if (!string.IsNullOrEmpty(hdnAPLMultiTypeCodeID.Value))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        TypeCodeList = hdnAPLMultiTypeCodeID.Value.Split(',');
                        for (int Index = 0; Index < TypeCodeList.Length; Index++)
                        {
                            if (!string.IsNullOrEmpty(TypeCodeList[Index]))
                            {
                                //hdnAPLTypeCodeID.Value = TypeCodeList[Index].Trim();
                                AircraftID = Convert.ToInt64(TypeCodeList[Index].Trim());
                                var objRetAircraftList = objMasterService.GetAircraftByAircraftID(AircraftID).EntityList;
                                if (objRetAircraftList.Count() != 0)
                                {
                                    if (objRetAircraftList[0].PowerSettings1TrueAirSpeed != null && objRetAircraftList[0].PowerSettings1HourRange != null)
                                        aircrPS1 = ((double)(objRetAircraftList[0].PowerSettings1TrueAirSpeed)) * ((double)(objRetAircraftList[0].PowerSettings1HourRange));
                                    if (objRetAircraftList[0].PowerSettings2TrueAirSpeed != null && objRetAircraftList[0].PowerSettings2HourRange != null)
                                        aircrPS2 = ((double)(objRetAircraftList[0].PowerSettings2TrueAirSpeed)) * ((double)(objRetAircraftList[0].PowerSettings2HourRange));
                                    if (objRetAircraftList[0].PowerSettings3TrueAirSpeed != null && objRetAircraftList[0].PowerSettings3HourRange != null)
                                        aircrPS3 = ((double)(objRetAircraftList[0].PowerSettings3TrueAirSpeed)) * ((double)(objRetAircraftList[0].PowerSettings3HourRange));
                                    if (objRetAircraftList[0].PowerSetting == "1" && aircrPS1 < LegDistance)
                                    {
                                        ReturnValue = false;
                                    }
                                    else if (objRetAircraftList[0].PowerSetting == "2" && aircrPS2 < LegDistance)
                                    {
                                        ReturnValue = false;
                                    }
                                    else if (objRetAircraftList[0].PowerSetting == "3" && aircrPS3 < LegDistance)
                                    {
                                        ReturnValue = false;
                                    }
                                }
                            }
                            if (ReturnValue == false)
                            {
                                break;
                            }
                        }
                    }
                }
                else
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        AircraftID = Convert.ToInt64(hdnAPLTypeCodeID.Value);
                        var objRetAircraftList = objMasterService.GetAircraftByAircraftID(AircraftID).EntityList;
                        if (objRetAircraftList[0].PowerSettings1TrueAirSpeed != null && objRetAircraftList[0].PowerSettings1HourRange != null)
                            aircrPS1 = ((double)(objRetAircraftList[0].PowerSettings1TrueAirSpeed)) * ((double)(objRetAircraftList[0].PowerSettings1HourRange));
                        if (objRetAircraftList[0].PowerSettings2TrueAirSpeed != null && objRetAircraftList[0].PowerSettings2HourRange != null)
                            aircrPS2 = ((double)(objRetAircraftList[0].PowerSettings2TrueAirSpeed)) * ((double)(objRetAircraftList[0].PowerSettings2HourRange));
                        if (objRetAircraftList[0].PowerSettings3TrueAirSpeed != null && objRetAircraftList[0].PowerSettings3HourRange != null)
                            aircrPS3 = ((double)(objRetAircraftList[0].PowerSettings3TrueAirSpeed)) * ((double)(objRetAircraftList[0].PowerSettings3HourRange));
                        if (objRetAircraftList[0].PowerSetting == "1" && aircrPS1 < LegDistance)
                        {
                            ReturnValue = false;
                        }
                        else if (objRetAircraftList[0].PowerSetting == "2" && aircrPS2 < LegDistance)
                        {
                            ReturnValue = false;
                        }
                        else if (objRetAircraftList[0].PowerSetting == "3" && aircrPS3 < LegDistance)
                        {
                            ReturnValue = false;
                        }
                    }
                }
                if (ReturnValue == false)
                {
                    ReturnString = "Warning - Leg Distance Beyond Aircraft Capability.";
                }
                return ReturnString;
            }
        }
        private string CheckAircraftCapabilityLegTime()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                string ReturnString = string.Empty;
                Int64 AircraftID;
                double FlightHours = 0;
                string[] TypeCodeList = new string[100];
                if (!string.IsNullOrEmpty(tbAPLEte.Text))
                {
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                    {
                        FlightHours = Convert.ToDouble(tbAPLEte.Text);
                    }
                    else
                    {
                        FlightHours = Convert.ToDouble(ConvertMinToTenths(tbAPLEte.Text, true, string.Empty));
                    }
                }
                if (!string.IsNullOrEmpty(hdnAPLMultiTypeCodeID.Value))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        TypeCodeList = hdnAPLMultiTypeCodeID.Value.Split(',');
                        for (int Index = 0; Index < TypeCodeList.Length; Index++)
                        {
                            if (!string.IsNullOrEmpty(TypeCodeList[Index]))
                            {
                                AircraftID = Convert.ToInt64(TypeCodeList[Index].Trim());
                                var objRetAircraftList = objMasterService.GetAircraftByAircraftID(AircraftID).EntityList;
                                if (objRetAircraftList.Count() != 0)
                                {
                                    if (objRetAircraftList[0].PowerSetting == "1" && (double)objRetAircraftList[0].PowerSettings1HourRange < FlightHours)
                                    {
                                        ReturnValue = false;
                                    }
                                    else if (objRetAircraftList[0].PowerSetting == "2" && (double)objRetAircraftList[0].PowerSettings1HourRange < FlightHours)
                                    {
                                        ReturnValue = false;
                                    }
                                    else if (objRetAircraftList[0].PowerSetting == "3" && (double)objRetAircraftList[0].PowerSettings1HourRange < FlightHours)
                                    {
                                        ReturnValue = false;
                                    }
                                }
                            }
                            if (ReturnValue == false)
                            {
                                break;
                            }
                        }
                    }
                }
                else
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        AircraftID = Convert.ToInt64(hdnAPLTypeCodeID.Value);
                        var objRetAircraftList = objMasterService.GetAircraftByAircraftID(AircraftID).EntityList;
                        if (objRetAircraftList[0].PowerSetting == "1" && (double)objRetAircraftList[0].PowerSettings1HourRange < FlightHours)
                        {
                            ReturnValue = false;
                        }
                        else if (objRetAircraftList[0].PowerSetting == "2" && (double)objRetAircraftList[0].PowerSettings1HourRange < FlightHours)
                        {
                            ReturnValue = false;
                        }
                        else if (objRetAircraftList[0].PowerSetting == "3" && (double)objRetAircraftList[0].PowerSettings1HourRange < FlightHours)
                        {
                            ReturnValue = false;
                        }
                    }
                }
                if (ReturnValue == false)
                {
                    ReturnString = "Warning - Leg Time Aloft Beyond Aircraft Capability.";
                }
                return ReturnString;
            }
        }
        private void CalculateDistance()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((!string.IsNullOrEmpty(tbAPLDeparture.Text)) &&
                    (!string.IsNullOrEmpty(hdnAPLDepartureID.Value)) &&
                    (!string.IsNullOrEmpty(tbAPLArrive.Text)) &&
                    (!string.IsNullOrEmpty(hdnAPLArriveID.Value)))
                {
                    using (CalculationService.CalculationServiceClient CalculationService = new CalculationService.CalculationServiceClient())
                    {
                        double Miles = 0;
                        Miles = Math.Round(CalculationService.GetDistance(Convert.ToInt64(hdnAPLDepartureID.Value), Convert.ToInt64(hdnAPLArriveID.Value)), 2);
                        tbAPLMiles.Text = ConvertToKilomenterBasedOnCompanyProfile((decimal)Miles).ToString();
                        hdAPLMiles.Value = Miles.ToString();
                    }
                }
                else
                {
                    tbAPLMiles.Text = "0";
                    hdAPLMiles.Value = "0";
                }
            }
        }
        private void CalculateWind()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((!string.IsNullOrEmpty(tbAPLDeparture.Text)) &&
                    (!string.IsNullOrEmpty(hdnAPLDepartureID.Value)) &&
                    (!string.IsNullOrEmpty(tbAPLArrive.Text)) &&
                    (!string.IsNullOrEmpty(hdnAPLArriveID.Value)) &&
                    (!string.IsNullOrEmpty(hdnAPLTypeCodeID.Value)))
                {
                    using (CalculationService.CalculationServiceClient CalculationService = new CalculationService.CalculationServiceClient())
                    {
                        tbAPLWinds.Text = Math.Round(CalculationService.GetWind(Convert.ToInt64(hdnAPLDepartureID.Value), Convert.ToInt64(hdnAPLArriveID.Value), Convert.ToInt32(rblistAPLWindReliability.SelectedValue), Convert.ToInt64(hdnAPLTypeCodeID.Value), ddlAPLQuarter.SelectedValue), 2).ToString();
                    }
                }
                else
                {
                    tbAPLWinds.Text = "0";
                }
            }
        }
        private void PopulateTypeCodeData(bool IsGetFromDB, bool IsForUpdate, Int64 MultiTypeCodeID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((!string.IsNullOrEmpty(tbAPLTypeCode.Text)) && (!string.IsNullOrEmpty(hdnAPLTypeCodeID.Value)))
                {
                    List<FlightPakMasterService.Aircraft> lstAircraft = new List<FlightPakMasterService.Aircraft>();
                    using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objAircraft = objService.GetAircraftByAircraftID(Convert.ToInt64(hdnAPLTypeCodeID.Value)).EntityList;
                        if (objAircraft.Count() != 0)
                        {
                            lstAircraft = (List<FlightPakMasterService.Aircraft>)objAircraft;
                        }
                    }
                    if (lstAircraft.Count != 0)
                    {
                        if (IsGetFromDB == true)
                        {
                            if (IsForUpdate)
                            {
                                if (lstAircraft[0].PowerSetting != null)
                                {
                                    tbAPLPower.Text = lstAircraft[0].PowerSetting.ToString();
                                }
                            }
                            if (tbAPLPower.Text == "3")
                            {
                                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                                {
                                    if (lstAircraft[0].PowerSettings3TakeOffBias != null)
                                    {
                                        tbAPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", Math.Round(Convert.ToDecimal(lstAircraft[0].PowerSettings3TakeOffBias.Value.ToString()), 1)).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings3LandingBias != null)
                                    {
                                        tbAPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", Math.Round(Convert.ToDecimal(lstAircraft[0].PowerSettings3LandingBias.Value.ToString()), 1)).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings3TrueAirSpeed != null)
                                    {
                                        tbAPLTAS.Text = lstAircraft[0].PowerSettings3TrueAirSpeed.Value.ToString();
                                    }
                                }
                                else
                                {
                                    if (lstAircraft[0].PowerSettings3TakeOffBias != null)
                                    {
                                        tbAPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", ConvertTenthsToMins(lstAircraft[0].PowerSettings3TakeOffBias.Value.ToString())).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings3LandingBias != null)
                                    {
                                        tbAPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", ConvertTenthsToMins(lstAircraft[0].PowerSettings3LandingBias.Value.ToString())).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings3TrueAirSpeed != null)
                                    {
                                        tbAPLTAS.Text = lstAircraft[0].PowerSettings3TrueAirSpeed.Value.ToString();
                                    }
                                }
                            }
                            else if (tbAPLPower.Text == "2")
                            {
                                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                                {
                                    if (lstAircraft[0].PowerSettings2TakeOffBias != null)
                                    {
                                        tbAPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", Math.Round(Convert.ToDecimal(lstAircraft[0].PowerSettings2TakeOffBias.Value.ToString()), 1)).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings2LandingBias != null)
                                    {
                                        tbAPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", Math.Round(Convert.ToDecimal(lstAircraft[0].PowerSettings2LandingBias.Value.ToString()), 1)).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings2TrueAirSpeed != null)
                                    {
                                        tbAPLTAS.Text = lstAircraft[0].PowerSettings2TrueAirSpeed.Value.ToString();
                                    }
                                }
                                else
                                {
                                    if (lstAircraft[0].PowerSettings2TakeOffBias != null)
                                    {
                                        tbAPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", ConvertTenthsToMins(lstAircraft[0].PowerSettings2TakeOffBias.Value.ToString())).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings2LandingBias != null)
                                    {
                                        tbAPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", ConvertTenthsToMins(lstAircraft[0].PowerSettings2LandingBias.Value.ToString())).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings2TrueAirSpeed != null)
                                    {
                                        tbAPLTAS.Text = lstAircraft[0].PowerSettings2TrueAirSpeed.Value.ToString();
                                    }
                                }
                            }
                            else //if (lstAircraft[0].PowerSetting == "1")
                            {
                                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                                {
                                    if (lstAircraft[0].PowerSettings1TakeOffBias != null)
                                    {
                                        tbAPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", Math.Round(Convert.ToDecimal(lstAircraft[0].PowerSettings1TakeOffBias.Value.ToString()), 1)).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings1LandingBias != null)
                                    {
                                        tbAPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", Math.Round(Convert.ToDecimal(lstAircraft[0].PowerSettings1LandingBias.Value.ToString()), 1)).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings1TrueAirSpeed != null)
                                    {
                                        tbAPLTAS.Text = lstAircraft[0].PowerSettings1TrueAirSpeed.Value.ToString();
                                    }
                                }
                                else
                                {
                                    if (lstAircraft[0].PowerSettings1TakeOffBias != null)
                                    {
                                        tbAPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", ConvertTenthsToMins(lstAircraft[0].PowerSettings1TakeOffBias.Value.ToString())).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings1LandingBias != null)
                                    {
                                        tbAPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", ConvertTenthsToMins(lstAircraft[0].PowerSettings1LandingBias.Value.ToString())).Trim();
                                    }
                                    if (lstAircraft[0].PowerSettings1TrueAirSpeed != null)
                                    {
                                        tbAPLTAS.Text = lstAircraft[0].PowerSettings1TrueAirSpeed.Value.ToString();
                                    }
                                }
                            }
                        }
                        else
                        {
                            lstAircraft[0].PowerSetting = tbAPLPower.Text.Trim();
                        }
                    }
                    else
                    {
                        tbAPLPower.Text = "0";
                        tbAPLTOBias.Text = string.Format("{0:" + TenthMinFormat + "}", "0.0").Trim();
                        tbAPLLandBias.Text = string.Format("{0:" + TenthMinFormat + "}", "0.0").Trim();
                        tbAPLTAS.Text = "0";
                    }
                }
            }
        }
        private void CalculateETE()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((!string.IsNullOrEmpty(tbAPLWinds.Text)) &&
                    (!string.IsNullOrEmpty(tbAPLLandBias.Text)) &&
                    (!string.IsNullOrEmpty(tbAPLTAS.Text)) &&
                    (!string.IsNullOrEmpty(tbAPLTOBias.Text)) &&
                    (!string.IsNullOrEmpty(hdAPLMiles.Value)))
                {
                    using (CalculationService.CalculationServiceClient CalculationService = new CalculationService.CalculationServiceClient())
                    {
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                        {
                            tbAPLEte.Text = Math.Round(CalculationService.GetIcaoEte(Convert.ToDouble(tbAPLWinds.Text), Convert.ToDouble(tbAPLLandBias.Text), Convert.ToDouble(tbAPLTAS.Text), Convert.ToDouble(tbAPLTOBias.Text), Convert.ToDouble(hdAPLMiles.Value), Convert.ToDateTime(DateTime.Today)), 2).ToString();
                        }
                        else
                        {
                            string TOBias = ConvertMinToTenths(tbAPLTOBias.Text, true, string.Empty);
                            string LandBias = ConvertMinToTenths(tbAPLLandBias.Text, true, string.Empty);
                            tbAPLEte.Text = Math.Round(CalculationService.GetIcaoEte(Convert.ToDouble(tbAPLWinds.Text), Convert.ToDouble(LandBias), Convert.ToDouble(tbAPLTAS.Text), Convert.ToDouble(TOBias), Convert.ToDouble(hdAPLMiles.Value), Convert.ToDateTime(DateTime.Today)), 2).ToString();
                            tbAPLEte.Text = ConvertTenthsToMins(tbAPLEte.Text);
                        }
                    }
                }
                else
                {
                    tbAPLEte.Text = string.Format("{0:" + TenthMinFormat + "}", "0.00");
                }
                tbAPLEte.Text = string.Format("{0:" + TenthMinFormat + "}", tbAPLEte.Text).Trim();
            }
        }
        protected decimal ConvertToKilomenterBasedOnCompanyProfile(decimal Miles)
        {
            if (UserPrincipal.Identity._fpSettings._IsKilometer == null ? false : (bool)UserPrincipal.Identity._fpSettings._IsKilometer)
                return Math.Round(Miles * 1.852M, 0);
            else
                return Miles;
        }
        protected decimal ConvertToMilesBasedOnCompanyProfile(decimal Kilometers)
        {
            if (UserPrincipal.Identity._fpSettings._IsKilometer == null ? false : (bool)UserPrincipal.Identity._fpSettings._IsKilometer)
                return Math.Round(Kilometers / 1.852M, 0);
            else
                return Kilometers;
        }
        private void CalculateCost()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(hdnAPLTypeCodeID.Value))
                {
                    decimal lnChrg_Rate = 0.0M;
                    decimal lnCost = 0.0M;
                    decimal LegDistance = 0;
                    decimal LegETE = 0;
                    decimal LegFlightCost = 0;
                    string lcChrg_Unit = string.Empty;
                    Int64 AircraftID = Convert.ToInt64(hdnAPLTypeCodeID.Value);
                    if (!string.IsNullOrEmpty(hdAPLMiles.Value))
                    {
                        LegDistance = Convert.ToDecimal(hdAPLMiles.Value);
                    }
                    if (!string.IsNullOrEmpty(tbAPLEte.Text))
                    {
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                        {
                            LegETE = Convert.ToDecimal(tbAPLEte.Text);
                        }
                        else
                        {
                            LegETE = Convert.ToDecimal(ConvertMinToTenths(tbAPLEte.Text, true, string.Empty));
                        }
                    }
                    FlightPakMasterService.Aircraft Aircraft = GetAircraft(AircraftID);
                    if (Aircraft != null)
                    {
                        lnChrg_Rate = Aircraft.ChargeRate == null ? 0.0M : (decimal)Aircraft.ChargeRate;
                        lcChrg_Unit = Aircraft.ChargeUnit;
                    }
                    switch (lcChrg_Unit)
                    {
                        case "N":
                            {
                                lnCost = (LegDistance == null ? 0 : (decimal)LegDistance) * lnChrg_Rate;
                                break;
                            }
                        case "K":
                            {
                                lnCost = (LegDistance == null ? 0 : (decimal)LegDistance) * lnChrg_Rate * 1.852M;
                                break;
                            }
                        case "S":
                            {
                                lnCost = (LegDistance == null ? 0 : (decimal)LegDistance) * lnChrg_Rate * 1.1508M;
                                break;
                            }
                        case "H":
                            {
                                lnCost = (LegETE == null ? 0 : (decimal)LegETE) * lnChrg_Rate;
                                break;
                            }
                        default: lnCost = 0.0M; break;
                    }
                    LegFlightCost = lnCost;
                    tbAPLCost.Text = Math.Round(LegFlightCost, 2).ToString();
                    tbAPLCost.Text = CurrencyType + string.Format("{0:" + CurrencyFormat + "}", Math.Round(Convert.ToDecimal(tbAPLCost.Text), 2));
                }
                else
                {
                    tbAPLCost.Text = "0";
                    tbAPLCost.Text = CurrencyType + string.Format("{0:" + CurrencyFormat + "}", "0.00");
                }
            }
        }
        private FlightPak.Web.FlightPakMasterService.Aircraft GetAircraft(Int64 AircraftID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AircraftID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.Aircraft retAircraft = new FlightPakMasterService.Aircraft();
                    var objPowerSetting = objDstsvc.GetAircraftByAircraftID(AircraftID);
                    if (objPowerSetting.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = objPowerSetting.EntityList;
                        if (AircraftList != null && AircraftList.Count > 0)
                            retAircraft = AircraftList[0];
                        else
                            retAircraft = null;
                    }
                    return retAircraft;
                }
            }
        }
        private void CalculateTotalCost()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Decimal TotalFlightCost = 0;
                tbAPLTotalCost.Text = string.Empty;
                foreach (GridDataItem Item in dgAirportPairLegs.Items)
                {
                    if (Item.GetDataKeyValue("Cost") != null)
                    {
                        TotalFlightCost = TotalFlightCost + Convert.ToDecimal(Item.GetDataKeyValue("Cost"));
                    }
                }
                tbAPLTotalCost.Text = CurrencyType + string.Format("{0:" + CurrencyFormat + "}", Math.Round(TotalFlightCost, 2).ToString());
            }
        }
        private void CalculateAll(bool CalcDistance, bool CalcWind, bool CalcETE, bool CalcCost, bool CalcTotalCost)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (CalcDistance)
                {
                    CalculateDistance();
                }
                if (CalcWind)
                {
                    CalculateWind();
                }
                if (CalcETE)
                {
                    CalculateETE();
                }
                if (CalcCost)
                {
                    CalculateCost();
                }
                if (CalcTotalCost)
                {
                    CalculateTotalCost();
                }
            }
        }
        private void DefaultInputSetting()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgAirportPairLegs.Items.Count == 0)
                {
                    if ((UserPrincipal.Identity != null) && (UserPrincipal.Identity._airportICAOCd != null))
                    {
                        tbAPLDeparture.Text = UserPrincipal.Identity._airportICAOCd.ToString();
                        tbAPLDeparture_OnTextChanged(tbAPLDeparture, EventArgs.Empty);
                    }
                }
            }
        }
        #endregion
        #region "Mintues/Tenths Conversion"
        public List<StandardFlightpakConversion> LoadStandardFPConversion()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<StandardFlightpakConversion> conversionList = new List<StandardFlightpakConversion>();
                // Set Standard Conversions
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.0M, StartMinutes = 0, EndMinutes = 5, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.1M, StartMinutes = 6, EndMinutes = 11, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.2M, StartMinutes = 12, EndMinutes = 17, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.3M, StartMinutes = 18, EndMinutes = 23, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.4M, StartMinutes = 24, EndMinutes = 29, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.5M, StartMinutes = 30, EndMinutes = 35, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.6M, StartMinutes = 36, EndMinutes = 41, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.7M, StartMinutes = 42, EndMinutes = 47, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.8M, StartMinutes = 48, EndMinutes = 53, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.9M, StartMinutes = 54, EndMinutes = 59, ConversionType = 1 });
                // Set Flightpak Conversions
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.0M, StartMinutes = 0, EndMinutes = 2, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.1M, StartMinutes = 3, EndMinutes = 8, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.2M, StartMinutes = 9, EndMinutes = 14, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.3M, StartMinutes = 15, EndMinutes = 21, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.4M, StartMinutes = 22, EndMinutes = 27, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.5M, StartMinutes = 28, EndMinutes = 32, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.6M, StartMinutes = 33, EndMinutes = 39, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.7M, StartMinutes = 40, EndMinutes = 44, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.8M, StartMinutes = 45, EndMinutes = 50, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.9M, StartMinutes = 51, EndMinutes = 57, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 1.0M, StartMinutes = 58, EndMinutes = 59, ConversionType = 2 });
                return conversionList;
            }
        }
        /// <summary>
        /// Method to Get Tenths Conversion Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <param name="isTenths">Pass Boolean Value</param>
        /// <param name="conversionType">Pass Conversion Type</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        protected string ConvertMinToTenths(String time, Boolean isTenths, String conversionType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time, isTenths, conversionType))
            {
                string result = "0.0";

                if (time.IndexOf(":") != -1)
                {
                    string[] timeArray = time.Split(':');
                    decimal hour = Convert.ToDecimal(timeArray[0]);
                    decimal minute = Convert.ToDecimal(timeArray[1]);

                    if (isTenths && (conversionType == "1" || conversionType == "2")) // Standard and Flightpak Conversion
                    {
                        if (Session["StandardFlightpakConversion"] != null)
                        {
                            List<StandardFlightpakConversion> StandardFlightpakConversionList = (List<StandardFlightpakConversion>)Session["StandardFlightpakConversion"];
                            var standardList = StandardFlightpakConversionList.ToList().Where(x => minute >= x.StartMinutes && minute <= x.EndMinutes && x.ConversionType.ToString() == conversionType).ToList();

                            if (standardList.Count > 0)
                            {
                                result = Convert.ToString(hour + standardList[0].Tenths);
                            }
                        }
                    }
                    //else if (isTenths && conversionType == "3") // Tenths - Min Conversion : Others
                    //{
                    //    List<GetPOHomeBaseSetting> homeBaseSetting = (List<GetPOHomeBaseSetting>)Session[Master.POHomeBaseKey];
                    //    List<GetPOTenthsConversion> tenthConvList = homeBaseSetting[0].TenthConversions.ToList().Where(x => minute >= x.StartMinimum && minute <= x.EndMinutes).ToList();

                    //    if (tenthConvList.Count > 0)
                    //    {
                    //        result = Convert.ToString(hour + tenthConvList[0].Tenths);
                    //    }
                    //}
                    else
                    {
                        decimal decimalOfMin = 0;
                        if (minute > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = minute / 60;

                        result = Convert.ToString(hour + decimalOfMin);
                    }
                }
                return result;
            }
        }
        /// <summary>
        /// Method to Get Minutes from decimal Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        protected string ConvertTenthsToMins(String time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time))
            {

                string result = "00:00";
                int val;
                if (!string.IsNullOrEmpty(time))
                {
                    if (time.IndexOf(".") < 0)

                        time = time + ".0";

                    if (time.IndexOf(".") != -1)
                    {
                        string[] timeArray = time.Split('.');
                        decimal hour = Convert.ToDecimal(timeArray[0]);
                        decimal decimal_min = Convert.ToDecimal(String.Concat("0.", timeArray[1]));
                        decimal decimalOfMin = 0;
                        if (decimal_min > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = (decimal_min * 60 / 100);
                        decimal finalval = Math.Round(hour + decimalOfMin, 2);
                        result = Convert.ToString(finalval).Replace(".", ":");
                        if (result.IndexOf(":") < 0)
                            result = result + ":00";

                        timeArray = result.Split(':');
                        if (timeArray[0].Length == 1)
                        {
                            result = "0" + result;
                        }
                        if (timeArray[1].Length == 1)
                        {
                            result = result + "0";
                        }
                    }
                    else if (int.TryParse(time, out val))
                    {
                        result = time;
                    }
                }

                return result;
            }
        }
        #endregion
    }
}