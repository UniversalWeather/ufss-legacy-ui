﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CQReportViewer.aspx.cs"
    MasterPageFile="~/Framework/Masters/ReportSetting.master" Inherits="FlightPak.Web.Views.Reports.CQReportViewer" %>

<%@ Register TagPrefix="UC" TagName="DatePicker" Src="~/UserControls/UCRptDateRange.ascx" %>
<%@ Register Src="~/UserControls/UCMultiSelect.ascx" TagName="Grid" TagPrefix="UCMultiSelect" %>
<%@ Register Src="~/UserControls/UCReportHtmlSelection.ascx" TagName="Grid" TagPrefix="UCReportHtmlSelection" %>
<%@ Register Src="~/UserControls/UCRptMonthYear.ascx" TagName="Grid" TagPrefix="UCRptMonthYear" %>
<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <link href="../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
    <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jquery.alerts.js") %>"></script>
    <script type="text/javascript" src="../../Scripts/Common.js"></script>
    <style type="text/css">
        .style1
        {
            width: 131px;
        }
        .style2
        {
            width: 176px;
        }
        .style3
        {
            width: 178px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript">

            function openWin(url, value, radWin) {
                var oWnd = radopen(url + value, radWin);
                oWnd.ReloadOnShow = true;
            }

            function openWinTrip(radWin) {
                var url = '';

                if (radWin == "RadWindow2") {
                    url = '../Transactions/CharterQuote/CharterQuoteSearchAll.aspx?IsExpressQuote=' + document.getElementById('<%=hdnIsExpressQuote.ClientID%>').value + '&FromPage=Report';
                }
                else if (radWin == "RadWindow3") {

                    var filename = window.location.search.substring(1);
                    if (filename == "xmlFilename=PREGeneralDeclarationAPDX.xml" || filename == "xmlFilename=PRETSGeneralDeclaration.xml") {
                        url = '../Reports/QuoteLegsPopup.aspx?MainID=' + document.getElementById('<%=hdnQuote.ClientID%>').value + '&MultiSelect=true';
                    }
                    else {
                        url = '../Reports/QuoteLegsPopup.aspx?MainID=' + document.getElementById('<%=hdnQuote.ClientID%>').value + '&MultiSelect=true';

                    }
                }
                else if (radWin == "RadWindow4") {
                    url = '../Reports/QuoteOnFilePopup.aspx?FileID=' + document.getElementById('<%=hdnFileNum.ClientID%>').value + '&FromPage=Report';
                }
                else if (radWin == "radVendorPopup") {
                    url = '../Settings/Logistics/VendorMasterPopUp.aspx?VendorCD=' + document.getElementById('<%=tbVendorCode.ClientID%>').value;
                }
                else if (radWin == "radTypeCodePopup") {
                    url = "../Settings/Fleet/AircraftPopup.aspx?AircraftCD=" + document.getElementById('<%=tbTypeCode.ClientID%>').value + "&tailNoAirCraftID=" + document.getElementById("<%=hdnTypeCode.ClientID%>").value;
                }
                else if (radWin == "radCQCPopups") {
                    url = '../CharterQuote/CharterQuoteCustomerPopup.aspx?Code=' + document.getElementById('<%=tbCustomer.ClientID%>').value;
                }
                else if (radWin == "RadSalesPersonPopup") {
                    url = '../CharterQuote/SalesPersonPopup.aspx?Code=' + document.getElementById('<%=tbSalesPerson.ClientID%>').value;
                }
                else if (radWin == "RadFormatReportPopup") {
                    if (document.getElementById('<%=hdnCQReportID.ClientID%>').value != "") {
                        url = "FileFormatReport.aspx?CQReportHeaderID=" + document.getElementById('<%=hdnCQReportID.ClientID%>').value;
                    }
                    else {
                        radalert('Please select the CharterQuote report.', 360, 50, 'CharterQuote Report Writer');
                    }
                }
                else if (radWin == "RadHomeBasePopup") {
                    url = "../Settings/Company/CompanyMasterPopup.aspx?HomeBase=" + document.getElementById("<%=tbCQHomeBase.ClientID%>").value;
                }

                var oWnd = radopen(url, radWin);
            }


            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function OnClientCloseFileNum(oWnd, args) {
                var combo = $find('<%= tbFileNum.ClientID %>');
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById('<%=tbFileNum.ClientID%>').value = arg.FileNUM;
                        document.getElementById('<%=hdnFileNum.ClientID%>').value = arg.CQFileID;
                        if (arg.CQFileID != null)
                            document.getElementById('<%=lbcvFileNum.ClientID%>').innerHTML = "";
                        var step = "SearchBox_TextChanged";
                        var ajaxManager = $find('<%= RadAjaxManager1.ClientID%>');
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbFileNum.ClientID%>").value = "";
                        combo.clearSelection();
                    }

                    var a = document.getElementById('<%=tbQuote.ClientID %>');
                    var c = document.getElementById('<%=btnQuote.ClientID %>');
                    a.disabled = false;
                    a.value = '';
                    c.disabled = false;
                }
                else {
                    document.getElementById('<%=tbQuote.ClientID %>').disabled = true;
                    document.getElementById('<%=btnQuote.ClientID%>').disable = true;
                }
            }
            function OnClientClose3(oWnd, args) {
                var combo = $find("<%= tbLeg.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbLeg.ClientID%>").value = arg.LegNum;
                        document.getElementById("<%=hdnLeg.ClientID%>").value = arg.LegID;

                        var step = "SearchLeg_TextChanged";
                        var ajaxManager = $find('<%= RadAjaxManager1.ClientID%>');
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbLeg.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }

            }
            function OnClientClose4(oWnd, args) {
                var combo = $find("<%= tbQuote.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbQuote.ClientID%>").value = arg.QuoteNUM;
                        document.getElementById("<%=hdnQuote.ClientID%>").value = arg.CQMainID;
                        var step = "SearchQuote_TextChanged";
                        var ajaxManager = $find('<%= RadAjaxManager1.ClientID%>');
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbQuote.ClientID%>").value = "";
                        document.getElementById("<%=hdnQuote.ClientID%>").value = "";
                        combo.clearSelection();
                    }

                    var a = document.getElementById('<%=tbLeg.ClientID%>');
                    var b = document.getElementById('<%=btnLeg.ClientID%>');
                    a.disabled = false;
                    a.value = '';
                    b.disabled = false;
                }
                else {
                    document.getElementById('<%=tbLeg.ClientID%>').disable = true;
                    document.getElementById('<%=btnLeg.ClientID%>').disable = true;
                }
            }

            function OnClientCloseHomeBasePopup(oWnd, args) {
                var combo = $find("<%= tbHomebase.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbHomebase.ClientID%>").value = arg.HomeBase;
                        document.getElementById("<%=hdnHomebase.ClientID%>").value = arg.HomebaseID;

                        var step = "tbHomebase_OnTextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                        if (arg.VendorCD != null)
                            document.getElementById("<%=lbcvHomebase.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbHomebase.ClientID%>").value = "";
                        document.getElementById("<%=hdnHomebase.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseTailNumberPopup(oWnd, args) {
                var combo = $find("<%= tbTailNumber.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbTailNumber.ClientID%>").value = arg.TailNum;
                        document.getElementById("<%=hdnTailNumber.ClientID%>").value = arg.FleetId;

                        var step = "tbTailNumber_OnTextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                        if (arg.VendorCD != null)
                            document.getElementById("<%=lbcvTailNumber.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbTailNumber.ClientID%>").value = "";
                        document.getElementById("<%=hdnTailNumber.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }


            function OnClientVendorClose(oWnd, args) {
                var combo = $find("<%= tbVendorCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbVendorCode.ClientID%>").value = arg.VendorCD;
                        document.getElementById("<%=hdnVendorCode.ClientID%>").value = arg.VendorID;

                        var step = "tbVendorCode_OnTextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                        if (arg.VendorCD != null)
                            document.getElementById("<%=lbcvVendorCode.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbVendorCode.ClientID%>").value = "";
                        document.getElementById("<%=hdnVendorCode.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientTypeClose(oWnd, args) {
                var combo = $find("<%= tbTypeCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbTypeCode.ClientID%>").value = arg.AircraftCD;
                        document.getElementById("<%=hdnTypeCode.ClientID%>").value = arg.AircraftID;
                        if (arg.AircraftID != null)
                            document.getElementById("<%=lbcvTypeCode.ClientID%>").innerHTML = "";
                        var step = "tbTypeCode_OnTextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbTypeCode.ClientID%>").value = "";
                        document.getElementById("<%=hdnTypeCode.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseCharterQuotCustomerPopup(oWnd, args) {
                var combo = $find("<%= tbCustomer.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbCustomer.ClientID%>").value = arg.CQCustomerCD;
                        document.getElementById("<%=hdnCustomer.ClientID%>").value = arg.CQCustomerID;
                        var step = "tbCustomer_OnTextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }

                        if (arg.CQCustomerCD != null)
                            document.getElementById("<%=lbcvCustomer.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbCustomer.ClientID%>").value = "";
                        document.getElementById("<%=hdnCustomer.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCloseSalesPersonPopup(oWnd, args) {
                var combo = $find("<%= tbSalesPerson.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbSalesPerson.ClientID%>").value = arg.SalesPersonCD;
                        document.getElementById("<%=hdnSalesPerson.ClientID%>").value = arg.SalesPersonID;
                        var step = "tbSalesPerson_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                        if (arg.Code != null)
                            document.getElementById("<%=lbcvSalesPerson.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbSalesPerson.ClientID%>").value = "";
                        document.getElementById("<%=hdnSalesPerson.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function SetButtonStatus(sender, target1, target2) {
                if (sender.value.length >= 0) {
                    document.getElementById(target1).disabled = false;
                    document.getElementById(target2).disabled = false;
                }
                else {
                    document.getElementById(target1).disabled = false;
                    document.getElementById(target2).disabled = false;
                }
            }

            function OnClientClose(oWnd, args) {
                var arg = args.get_argument();
                if (arg != null) {
                    var combo = $find(arg.CallingButton);
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById(arg.CallingButton).value = arg.Arg1;
                        }
                        else {
                            document.getElementById(arg.CallingButton).value = "";
                            combo.clearSelection();
                        }
                    }
                }
            }

            function FormatSelected() {

                //                var ddlItem = document.getElementById('ddlFormat');
                //                if (ddlItem.options[ddlItem.selectedIndex].value == 'WORD') {

                if (document.getElementById('FileExcel')) {
                    document.getElementById('FileExcel').style.visibility = 'hidden';
                }
                if (document.getElementById('FileExcelGdl')) {
                    document.getElementById('FileExcelGdl').style.visibility = 'hidden';
                }
                if (document.getElementById('Wizard1')) {
                    document.getElementById('Wizard1').style.visibility = 'hidden';
                }
                //                }
            }

        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true"
        ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" OnClientClose="OnClientClose" Modal="true"
                Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow2" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseFileNum" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/CharterQuote/CharterQuoteSearchAll.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow3" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose3" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/QuoteLegsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow4" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose4" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Reports/QuoteOnFilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCQCPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadHomeBaseMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseHomeBasePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFleetProfilePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseTailNumberPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFleetProfileCRUDPopup" runat="server" Height="800px" Width="800px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radVendorPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientVendorClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/VendorMasterPopUp.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radTypeCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientTypeClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/AircraftPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadSalesPersonPopup" runat="server" OnClientResizeEnd="GetDimensions"
                Title="SalesPerson" OnClientClose="OnClientCloseSalesPersonPopup" AutoSize="true"
                KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                NavigateUrl="~/Views/CharterQuote/SalesPersonPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCQCPopups" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseCharterQuotCustomerPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/CharterQuote/CharterQuoteCustomerPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadFormatReportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClosetrpFormatReportPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadHomeBasePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClosetrpHomeBasePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadClientPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClosetrpClientPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadUserGroupPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClosetrpUserGroupPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/UserGrouppopup.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div class="art-setting-menu-report">
        <div class="art-setting-content-report">
            <div class="globalMenu_mapping">
                <%--<a href="#" class="globalMenu_mapping">Home</a> <span class="breadcrumbCaret"></span>
            <a href="#" class="globalMenu_mapping">Settings</a>--%>
            </div>
            <div style="clear: both;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button runat="server" ID="btnDatabase" Text="Database" CssClass="rpt_nav" OnClick="btnDb_Click"
                                            CausesValidation="false" />
                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="btnPreFlight" Text="Preflight" CssClass="rpt_nav"
                                            OnClick="btnPre_Click" CausesValidation="false" />
                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="btnPostFlight" Text="Postflight" CssClass="rpt_nav"
                                            OnClick="btnPost_Click" CausesValidation="false" />
                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="btnCharter" Text="Charter" CssClass="rpt_nav" OnClick="btnCharter_Click"
                                            CausesValidation="false" />
                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="btnCorp" Text="Corporate Request" CssClass="rpt_nav"
                                            OnClick="btnCorpReq_Click" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="right">
                            <div class="mandatory">
                                <span>Bold</span> Indicates required field</div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="art-setting-sidebar-report">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <telerik:RadPanelBar runat="server" ExpandMode="SingleExpandedItem" ID="pnlReportTab"
                                AllowCollapseAllItems="true" CssClass="RadNavMenu">
                                <CollapseAnimation Type="None"></CollapseAnimation>
                                <ExpandAnimation Type="None"></ExpandAnimation>
                            </telerik:RadPanelBar>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="art-setting-content-rpt">
                <telerik:RadCodeBlock ID="RadCodeBlock3" runat="server">
                    <script type="text/javascript" language="javascript">
                        function openWin(radWin) {
                            var url = '';
                            if (radWin == "RadHomeBasePopup") {
                                url = "../Settings/Company/CompanyMasterPopup.aspx?HomeBase=" + document.getElementById("<%=tbCQHomeBase.ClientID%>").value;
                            }
                            else if (radWin == "RadClientPopup") {
                                url = "../Settings/Company/ClientCodePopup.aspx?ClientCD=" + document.getElementById('<%=tbCQClient.ClientID%>').value;
                            }
                            else if (radWin == "RadUserGroupPopup") {
                                url = "../Settings/Company/UserGrouppopup.aspx?UserGroupCD=" + document.getElementById('<%=tbCQUserGroup.ClientID%>').value;
                            }
                            else if (radWin == "RadFormatReportPopup") {
                                if (document.getElementById('<%=hdnCQReportID.ClientID%>').value != "") {
                                    url = "FileFormatReport.aspx?CQReportHeaderID=" + document.getElementById('<%=hdnCQReportID.ClientID%>').value;
                                }
                                else {
                                    radalert('Please select the Charter Quote report.', 360, 50, 'Charter Quote Writer');
                                }
                            }
                            if (url != "") {
                                var oWnd = radopen(url, radWin);
                            }
                        }
                        function OnClientClosetrpHomeBasePopup(oWnd, args) {
                            var combo = $find("<%= tbCQHomeBase.ClientID %>");
                            //get the transferred arguments
                            var arg = args.get_argument();
                            if (arg !== null) {
                                if (arg) {
                                    document.getElementById("<%=tbCQHomeBase.ClientID%>").value = arg.HomeBase;
                                    document.getElementById("<%=hdnCQHomeBaseID.ClientID%>").value = arg.HomebaseID;
                                    var step = "tbCQHomeBase_OnTextChanged";
                                    var ajaxManager = $find('<%= RadAjaxManager1.ClientID%>');
                                    if (step) {
                                        ajaxManager.ajaxRequest(step);
                                    }
                                }
                                else {
                                    document.getElementById("<%=tbCQHomeBase.ClientID%>").value = "";
                                    document.getElementById("<%=hdnCQHomeBaseID.ClientID%>").value = "";
                                    combo.clearSelection();
                                }
                            }
                        }
                        function OnClientClosetrpClientPopup(oWnd, args) {
                            var combo = $find("<%= tbCQClient.ClientID %>");
                            //get the transferred arguments
                            var arg = args.get_argument();
                            if (arg !== null) {
                                if (arg) {
                                    document.getElementById("<%=tbCQClient.ClientID%>").value = htmlDecode(arg.ClientCD);
                                    document.getElementById("<%=hdnCQClientID.ClientID%>").value = arg.ClientID;
                                    var step = "tbCQClient_OnTextChanged";
                                    var ajaxManager = $find('<%= RadAjaxManager1.ClientID%>');
                                    if (step) {
                                        ajaxManager.ajaxRequest(step);
                                    }
                                }
                                else {
                                    document.getElementById("<%=tbCQClient.ClientID%>").value = "";
                                    document.getElementById("<%=hdnCQClientID.ClientID%>").value = "";
                                    combo.clearSelection();
                                }
                            }
                        }
                        function OnClientClosetrpUserGroupPopup(oWnd, args) {
                            var combo = $find("<%= tbCQUserGroup.ClientID %>");
                            //get the transferred arguments
                            var arg = args.get_argument();
                            if (arg !== null) {
                                if (arg) {
                                    document.getElementById("<%=tbCQUserGroup.ClientID%>").value = arg.UserGroupCD;
                                    document.getElementById("<%=hdnCQUserGroupID.ClientID%>").value = arg.UserGroupCD;
                                    var step = "tbCQUserGroup_OnTextChanged";
                                    var ajaxManager = $find('<%= RadAjaxManager1.ClientID%>');
                                    if (step) {
                                        ajaxManager.ajaxRequest(step);
                                    }
                                }
                                else {
                                    document.getElementById("<%=tbCQUserGroup.ClientID%>").value = "";
                                    document.getElementById("<%=hdnCQUserGroupID.ClientID%>").value = "";
                                    combo.clearSelection();
                                }
                            }
                        }
                        function OnClientClosetrpFormatReportPopup(oWnd, args) {
                            var arg = args.get_argument();
                            if (arg !== null) {
                                if (arg) {
                                    if (arg.IsPrint == "1") {
                                        document.getElementById("<%=btnRptView.ClientID%>").click();
                                    }
                                }
                            }
                        }
                        function ProcessUpdateTrip() {
                            var grid = $find("<%=dgCharterQuoteWriter.ClientID%>");
                            if (grid.get_masterTableView().get_selectedItems().length == 0) {
                                radalert('Please select a record from the above table.', 330, 100, "Edit", "");
                                return false;
                            }
                        }
                        function ProcessDeleteTrip(customMsg) {

                            //get reference to the client grid object using the global window variable set in the user control's Page_Load server handler
                            var grid = $find("<%=dgCharterQuoteWriter.ClientID%>");

                            var msg = 'Are you sure you want to delete this record?';

                            if (customMsg != null) {
                                msg = customMsg;
                            }

                            if (grid.get_masterTableView().get_selectedItems().length > 0) {

                                radconfirm(msg, callBackFn, 330, 100, '', 'Delete');
                                return false;
                            }
                            else {
                                radalert('Please select a record from the above table.', 330, 100, "Delete", "");
                                return false;
                            }
                        }
                        function prepareSearchInput(input) { input.value = input.value; }
                    </script>
                </telerik:RadCodeBlock>
                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest"
                    OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="tbCQReportID_OnTextChanged">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="tbCQHomeBase_OnTextChanged">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="tbCQClient_OnTextChanged">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="tbCQUserGroup_OnTextChanged">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="DivExternalForm2">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm2" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm2" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="SearchBox_TextChanged">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm2" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="SearchQuote_TextChanged">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm2" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="SearchLeg_TextChanged">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm2" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                    </AjaxSettings>
                </telerik:RadAjaxManager>
                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
                <div id="DivExternalForm" runat="server" class="ExternalForm">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:Panel ID="pnlGridPanel" runat="server" Visible="true">
                                    <telerik:RadPanelBar ID="pnlTripRptWrt" Width="100%" ExpandAnimation-Type="none"
                                        CollapseAnimation-Type="none" runat="server">
                                        <Items>
                                            <telerik:RadPanelItem runat="server" Expanded="false" Text="Charter Quote Writer">
                                                <Items>
                                                    <telerik:RadPanelItem Value="CQWriter" runat="server">
                                                        <ContentTemplate>
                                                            <telerik:RadGrid ID="dgCharterQuoteWriter" runat="server" AllowSorting="true" AutoGenerateColumns="false"
                                                                PageSize="10" AllowPaging="true" AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true"
                                                                Height="240px" OnNeedDataSource="dgCharterQuoteWriter_BindData" OnSelectedIndexChanged="dgCharterQuoteWriter_SelectedIndexChanged"
                                                                OnItemCommand="dgCharterQuoteWriter_ItemCommand" OnUpdateCommand="dgCharterQuoteWriter_UpdateCommand"
                                                                OnInsertCommand="dgCharterQuoteWriter_InsertCommand" OnDeleteCommand="dgCharterQuoteWriter_DeleteCommand"
                                                                OnPreRender="dgCharterQuoteWriter_PreRender">
                                                                <MasterTableView DataKeyNames="CQReportHeaderID,ReportID,ReportDescription,HomebaseCD,HomebaseID,ClientCD,ClientID,UserGroupCD,UserGroupID,IsDeleted"
                                                                    CommandItemDisplay="Bottom">
                                                                    <Columns>
                                                                        <telerik:GridBoundColumn DataField="CQReportHeaderID" HeaderText="CQReportHeaderID"
                                                                            CurrentFilterFunction="EqualTo" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                                                                            HeaderStyle-Width="120px" Display="false" FilterDelay="500">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="ReportID" HeaderText="Report ID" CurrentFilterFunction="StartsWith"
                                                                            ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="120px" FilterDelay="500">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="ReportDescription" HeaderText="Description" CurrentFilterFunction="StartsWith"
                                                                            ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="200px" FilterDelay="500">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="HomeBaseCD" HeaderText="Home Base" CurrentFilterFunction="StartsWith"
                                                                            ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="120px" FilterDelay="500">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="UserGroupCD" HeaderText="User Group" CurrentFilterFunction="StartsWith"
                                                                            ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="120px" FilterDelay="500">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="ClientCD" HeaderText="Client" CurrentFilterFunction="StartsWith"
                                                                            ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="120px" FilterDelay="500">
                                                                        </telerik:GridBoundColumn>
                                                                    </Columns>
                                                                    <CommandItemTemplate>
                                                                        <div style="padding: 5px 5px; float: left; clear: both;">
                                                                            <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"><img style="border:0px;vertical-align:middle;" alt="Add" 
                                                                        src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                                                                            <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdateTrip();"
                                                                                ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>"  /></asp:LinkButton>
                                                                            <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDeleteTrip();"
                                                                                runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                                                                        </div>
                                                                        <div>
                                                                            <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                                                                        </div>
                                                                    </CommandItemTemplate>
                                                                </MasterTableView>
                                                                <ClientSettings EnablePostBackOnRowClick="true">
                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                    <Selecting AllowRowSelect="true" />
                                                                </ClientSettings>
                                                                <GroupingSettings CaseSensitive="false" />
                                                            </telerik:RadGrid>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Panel ID="pnlControlsPanel" runat="server" Visible="true">
                                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <div class="tblspace_5">
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table>
                                                                                                        <tr align="left" valign="top">
                                                                                                            <td class="tdLabel80">
                                                                                                                Report ID
                                                                                                            </td>
                                                                                                            <td class="tdLabel150">
                                                                                                                <asp:TextBox ID="tbCQReportID" CssClass="text100" runat="server" MaxLength="8" AutoPostBack="true"
                                                                                                                    OnTextChanged="tbCQReportID_OnTextChanged" />
                                                                                                                <asp:HiddenField ID="hdnCQReportID" runat="server" />
                                                                                                            </td>
                                                                                                            <td class="tdLabel70">
                                                                                                                Description
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="tbCQReportDescription" CssClass="text200" runat="server" MaxLength="40" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr align="left" valign="top">
                                                                                                            <td>
                                                                                                            </td>
                                                                                                            <td colspan="3">
                                                                                                                <asp:RequiredFieldValidator ID="rfvCQReportID" runat="server" ValidationGroup="CQSave"
                                                                                                                    ControlToValidate="tbCQReportID" Text="Report ID is Required." Display="Dynamic"
                                                                                                                    CssClass="alert-text" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                                                                                <asp:CustomValidator ID="cvCQReportID" runat="server" ControlToValidate="tbCQReportID"
                                                                                                                    ErrorMessage="Unique Report ID is Required." Display="Dynamic" CssClass="alert-text"
                                                                                                                    ValidationGroup="CQSave"></asp:CustomValidator>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table>
                                                                                                        <tr align="left" valign="top">
                                                                                                            <td class="tdLabel80">
                                                                                                                Home Base
                                                                                                            </td>
                                                                                                            <td class="tdLabel150">
                                                                                                                <asp:TextBox ID="tbCQHomeBase" CssClass="text100" runat="server" MaxLength="4" AutoPostBack="true"
                                                                                                                    OnTextChanged="tbCQHomeBase_OnTextChanged" />
                                                                                                                <asp:Button ID="btnCQHomeBase" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadHomeBasePopup');return false;" />
                                                                                                                <asp:HiddenField ID="hdnCQHomeBaseID" runat="server" />
                                                                                                            </td>
                                                                                                            <td class="tdLabel70">
                                                                                                                Client
                                                                                                            </td>
                                                                                                            <td class="tdLabel150">
                                                                                                                <asp:TextBox ID="tbCQClient" CssClass="text100" runat="server" MaxLength="5" AutoPostBack="true"
                                                                                                                    OnTextChanged="tbCQClient_OnTextChanged" />
                                                                                                                <asp:Button ID="btnCQClient" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadClientPopup');return false;" />
                                                                                                                <asp:HiddenField ID="hdnCQClientID" runat="server" />
                                                                                                            </td>
                                                                                                            <td class="tdLabel80">
                                                                                                                User Group
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="tbCQUserGroup" CssClass="text100" runat="server" MaxLength="5" AutoPostBack="true"
                                                                                                                    OnTextChanged="tbCQUserGroup_OnTextChanged" />
                                                                                                                <asp:Button ID="btnCQUserGroup" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadUserGroupPopup');return false;" />
                                                                                                                <asp:HiddenField ID="hdnCQUserGroupID" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr align="left" valign="top">
                                                                                                            <td>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:CustomValidator ID="cvCQHomeBase" runat="server" ControlToValidate="tbCQHomeBase"
                                                                                                                    ValidationGroup="CQSave" ErrorMessage="Invalid Home Base." Display="Dynamic"
                                                                                                                    CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:CustomValidator ID="cvCQClient" runat="server" ControlToValidate="tbCQClient"
                                                                                                                    ValidationGroup="CQSave" ErrorMessage="Invalid Client Code" Display="Dynamic"
                                                                                                                    CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:CustomValidator ID="cvCQUserGroup" runat="server" ControlToValidate="tbCQUserGroup"
                                                                                                                    ValidationGroup="CQSave" ErrorMessage="Invalid Client Code." Display="Dynamic"
                                                                                                                    CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="right">
                                                                                                    <table>
                                                                                                        <tr>
                                                                                                            <td colspan="2">
                                                                                                                <asp:Button ID="btnFormatReport" runat="server" CssClass="button" Text="Format Editor"
                                                                                                                    OnClientClick="javascript:openWin('RadFormatReportPopup');return false;" />
                                                                                                            </td>
                                                                                                            <td class="custom_radbutton">
                                                                                                                <telerik:RadButton ID="btnSave" runat="server" Text="Save" ValidationGroup="CQSave"
                                                                                                                    OnClick="btnSave_OnClick" />
                                                                                                                <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_OnClick" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </asp:Panel>
                                                                                    <asp:HiddenField ID="hdnSave" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </telerik:RadPanelItem>
                                                </Items>
                                            </telerik:RadPanelItem>
                                        </Items>
                                    </telerik:RadPanelBar>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </div>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <div class="tblspace_10">
                            </div>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="100%" class="border-box">
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td class="style3">
                                        <span class="mnd_text">
                                            <asp:Label runat="server" ID="lbTitle"></asp:Label></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style3">
                                        <div class="tblspace_10">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style3">
                                        <asp:RadioButtonList runat="server" ID="rdBl" OnSelectedIndexChanged="rdBl_SelectedIndexChanged"
                                            AutoPostBack="true">
                                            <asp:ListItem Text="File No." Value="FileNumber" Selected="True" />
                                            <asp:ListItem Text="Date Range" Value="DateRange" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="Report" runat="server">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <b>Date From</b>
                                                                </td>
                                                                <td>
                                                                    <UC:DatePicker ID="DateFrom" runat="server" Required="true" DateValidationgroup="ReportValidator" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lbcvDateFrom" CssClass="alert-text"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>Date To</b>
                                                                </td>
                                                                <td>
                                                                    <UC:DatePicker ID="DateTo" runat="server" Required="true" DateValidationgroup="ReportValidator" />
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lbcvDateTo" CssClass="alert-text"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Tail No.
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="tbTailNumber" runat="server" MaxLength="8" OnTextChanged="tbTailNumber_OnTextChanged"
                                                                        AutoPostBack="true"></asp:TextBox>
                                                                    <asp:HiddenField ID="hdnTailNumber" runat="server" />
                                                                    <asp:Button ID="btnTailNumber" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinTrip('radFleetProfilePopup');return false;" />
                                                                    <asp:Label ID="lbcvTailNumber" runat="server" CssClass="alert-text"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Vendor
                                                                </td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="tbVendorCode" runat="server" MaxLength="8" OnTextChanged="tbVendorCode_OnTextChanged"
                                                                        AutoPostBack="true"></asp:TextBox>
                                                                    <asp:HiddenField ID="hdnVendorCode" runat="server" />
                                                                    <asp:Button ID="btnVendor" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinTrip('radVendorPopup');return false;" />
                                                                    <asp:Label ID="lbcvVendorCode" runat="server" CssClass="alert-text"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Home Base
                                                                </td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="tbHomebase" runat="server" MaxLength="8" OnTextChanged="tbHomebase_OnTextChanged"
                                                                        AutoPostBack="true"></asp:TextBox>
                                                                    <asp:HiddenField ID="hdnHomebase" runat="server" />
                                                                    <asp:Button ID="btnHomebase" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinTrip('RadHomeBaseMasterPopup');return false;" />
                                                                    <asp:Label ID="lbcvHomebase" runat="server" CssClass="alert-text"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Type Code
                                                                </td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="tbTypeCode" runat="server" MaxLength="8" OnTextChanged="tbTypeCode_OnTextChanged"
                                                                        AutoPostBack="true"></asp:TextBox>
                                                                    <asp:HiddenField ID="hdnTypeCode" runat="server" />
                                                                    <asp:Button ID="btnTypeCode" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinTrip('radTypeCodePopup');return false;" />
                                                                    <asp:Label ID="lbcvTypeCode" runat="server" CssClass="alert-text"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Customer
                                                                </td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="tbCustomer" runat="server" MaxLength="8" OnTextChanged="tbCustomer_OnTextChanged"
                                                                        AutoPostBack="true"></asp:TextBox>
                                                                    <asp:HiddenField ID="hdnCustomer" runat="server" />
                                                                    <asp:Button ID="btnCustomer" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinTrip('radCQCPopups');return false;" />
                                                                    <asp:Label ID="lbcvCustomer" runat="server" CssClass="alert-text"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Salesperson
                                                                </td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="tbSalesPerson" runat="server" MaxLength="8" AutoPostBack="true"
                                                                        OnTextChanged="tbSalesPerson_TextChanged"></asp:TextBox>
                                                                    <asp:HiddenField ID="hdnSalesPerson" runat="server" />
                                                                    <asp:Button ID="btnSalesPerson" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinTrip('RadSalesPersonPopup');return false;" />
                                                                    <asp:Label ID="lbcvSalesPerson" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:PlaceHolder>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div id="DivExternalForm2" runat="server" class="ExternalForm">
                                                        <asp:PlaceHolder ID="QuoteReport" runat="server">
                                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td class="tdLabel90">
                                                                                    <b>File No.</b>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbFileNum" runat="server" AutoPostBack="true" OnTextChanged="SearchBox_TextChanged"
                                                                                        onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                    <asp:HiddenField ID="hdnFileNum" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Button ID="btnFileNum" runat="server" OnClientClick="javascript:openWinTrip('RadWindow2');return false;"
                                                                                        CssClass="browse-button" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label CssClass="alert-text" runat="server" ID="lbcvFileNum"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                </td>
                                                                                <td colspan="3">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    Quote
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbQuote" runat="server" Enabled="false" AutoPostBack="true" OnTextChanged="SearchQuote_TextChanged"></asp:TextBox>
                                                                                    <asp:HiddenField ID="hdnQuote" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Button ID="btnQuote" runat="server" Enabled="false" OnClientClick="javascript:openWinTrip('RadWindow4');return false;"
                                                                                        CssClass="browse-button" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lbcvQuote" runat="server" CssClass="alert-text"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                </td>
                                                                                <td colspan="3">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label Text="Leg" runat="server" ID="lblLeg"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbLeg" runat="server" Enabled="false" onKeyPress="return fnAllowNumericAndChar(this, event,',')"
                                                                                        AutoPostBack="true" OnTextChanged="SearchLeg_TextChanged"></asp:TextBox>
                                                                                    <asp:HiddenField ID="hdnLeg" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Button ID="btnLeg" runat="server" Enabled="false" OnClientClick="javascript:openWinTrip('RadWindow3');return false;"
                                                                                        CssClass="browse-button" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lbcvLeg" runat="server" CssClass="alert-text"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:PlaceHolder>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="plTripItinerary" runat="server">
                                                        <%--<table>
                                                        <tr>
                                                            <td>
                                                                CREW-ADDL CREW
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtAddlcrew" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdLabel90">
                                                                CREW-PILOT 1
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtpilot1" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                CREW-PILOT 2
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtpilot2" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Message Body
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtmsg" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>--%>
                                                    </asp:Panel>
                                                    <asp:Panel ID="plTripsheetMain" runat="server">
                                                        <%--<table>
                                                        <tr>
                                                            <td>
                                                                AC Release
                                                            </td>
                                                            <td>
                                                                &nbsp;&nbsp;&nbsp;<asp:TextBox ID="tbAIRCRAFTRELEASED" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Message Body
                                                            </td>
                                                            <td>
                                                                &nbsp;&nbsp;&nbsp;<asp:TextBox ID="tbTripMain" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>--%>
                                                    </asp:Panel>
                                                    <asp:Panel ID="plcanpass" runat="server">
                                                        <%--<table>
                                                        <tr>
                                                            <td class="tdLabel90">
                                                                HAMILTON_ONTARIO
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtho" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                LANDSDOWNE_ONTARIO
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtlo" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                TRC_TELEPHONE
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtar" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                TELEPHONE_REPORT_CENTER
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txttrc" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                VICTORIA_BRITISH_COLUMBIA
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtvbc" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                WINDSOR_ONTARIO
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtwo" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>--%>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style3">
                                        <div class="tblspace_10">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td valign="top">
                                        <asp:Button runat="server" ID="btExport" CssClass="ui_nav" Text="Export" OnClick="btnExport_Click"
                                            ValidationGroup="ReportValidator" />
                                    </td>
                                    <td valign="top">
                                        <asp:Button ID="btnRptView" runat="server" CssClass="ui_nav" OnClick="btnRptView_Click"
                                            Text="Preview" ValidationGroup="ReportValidator" />
                                    </td>
                                    <td valign="top">
                                        <asp:Button ID="btnReset" runat="server" CssClass="ui_nav" OnClick="btnReset_Click"
                                            CausesValidation="False" Text="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:Panel runat="server" ID="ExportPanal">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="tdLabel100">
                                                        Export File Type
                                                    </td>
                                                    <td style="margin-left: 40px">
                                                        <asp:DropDownList runat="server" ID="ddlFormat" ClientIDMode="Static" onchange="JavaScript:FormatSelected();">
                                                            <%--<asp:ListItem Text="HTML Table" Value="HTML"></asp:ListItem>--%>
                                                            <asp:ListItem Text="Excel" Value="Excel"></asp:ListItem>
                                                            <asp:ListItem Text="MHTML" Value="MHTML"></asp:ListItem>
                                                            <%--<asp:ListItem Text="Word" Value="WORD"></asp:ListItem>--%>
                                                            <%-- <asp:ListItem Text="Visual FoxPro" Value="VFO"></asp:ListItem>

                                     <asp:ListItem Text="FoxPro 2.x" Value="FP"></asp:ListItem>

                                     <asp:ListItem Text="Comma Delimited Text" Value="CSV"></asp:ListItem>

                                     <asp:ListItem Text="Excel5.0" Value="Excel5"></asp:ListItem>

                                     <asp:ListItem Text="Excel2.0,3.0 and 4.0" Value="Excel2"></asp:ListItem>

                                     <asp:ListItem Text="Lotus 123.2.x" Value="Lotus"></asp:ListItem>

                                     <asp:ListItem Text="Data Interchange Format" Value="DIF"></asp:ListItem>

                                     <asp:ListItem Text="System Data Foramt" Value="SDF"></asp:ListItem>

                                     <asp:ListItem Text="Symblic Link Format" Value="SLF"></asp:ListItem>--%>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Button runat="server" CssClass="ui_nav" ID="btExportFileFormat" Text="Generate"
                                                            OnClick="btnExportFileFormat_Click" />
                                                    </td>
                                                    <td>
                                                        <asp:Button runat="server" CssClass="ui_nav" ID="btClose" Text="Close" OnClick="btnCostFileFormat_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel runat="server" ID="FileExcel" ClientIDMode="Static" Width="100%">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel120">
                                                                    <asp:LinkButton ID="lbTripSheet" CssClass="link_small" runat="server" OnClick="lbTripSheet_Click">Download Tripsheet</asp:LinkButton>
                                                                </td>
                                                                <td class="tdLabel150">
                                                                    <asp:LinkButton ID="lbTripsheetAlerts" CssClass="link_small" runat="server" OnClick="lbTripsheetAlerts_Click">Download Tripsheet Alerts</asp:LinkButton>
                                                                </td>
                                                                <td class="tdLabel160">
                                                                    <asp:LinkButton ID="lbPassengerNotes" CssClass="link_small" runat="server" OnClick="lbPassengerNotes_Click">Download Passenger Notes</asp:LinkButton>
                                                                </td>
                                                                <td class="tdLabel160">
                                                                    <asp:LinkButton ID="lbPassengerAlerts" CssClass="link_small" runat="server" OnClick="lbPassengerAlerts_Click">Download Passenger Alerts</asp:LinkButton>
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="lbFuelVendor" CssClass="link_small" runat="server" OnClick="lbFuelVendor_Click">Download Fuel Vendor</asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="lbAircraftAdditionalInfomation" CssClass="link_small" runat="server"
                                                                        OnClick="lbAircraftAdditionalInfomation_Click">Download Aircraft Additional Infomation</asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="FileExcelGdl" ClientIDMode="Static" Width="100%">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel120">
                                                                    <asp:LinkButton ID="lbgdl1" CssClass="link_small" runat="server" OnClick="lbTripGdl1_Click">Download General Declaration</asp:LinkButton>
                                                                </td>
                                                                <td class="tdLabel150">
                                                                    <asp:LinkButton ID="lbgdl2" CssClass="link_small" runat="server" OnClick="lbTripGdl2_Click">Download General Declaration</asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="lblError" CssClass="alert-text"></asp:Label>
                            <asp:Wizard ID="Wizard1" runat="server" DisplaySideBar="false" ActiveStepIndex="0"
                                Height="209px" Width="100%" BackColor="#f0f0f0" FinishPreviousButtonStyle-CssClass="ui_nav"
                                CancelButtonStyle-CssClass="ui_nav" FinishCompleteButtonStyle-CssClass="ui_nav"
                                StepNextButtonStyle-CssClass="ui_nav" StepPreviousButtonStyle-CssClass="ui_nav"
                                StartNextButtonStyle-CssClass="ui_nav" OnFinishButtonClick="OnFinish" OnNextButtonClick="OnNext"
                                ClientIDMode="Static">
                                <WizardSteps>
                                    <asp:WizardStep ID="WizardStep1" runat="server">
                                        <asp:Label runat="server" ID="lblw1" Text="Click on each of the Available fields by name and the right arrow to select them. Add them in the preferred display order or use the Up/Down arrows to reorder them for reporting. Click on the Selected fields and left arrow to de-select them."></asp:Label>
                                        <br />
                                        <br />
                                        <UCReportHtmlSelection:Grid ID="UCRHS" runat="server" />
                                    </asp:WizardStep>
                                    <asp:WizardStep ID="WizardStep2" runat="server">
                                        <asp:Label runat="server" ID="lblw2" Text="Select how data is to be sorted"></asp:Label>
                                        <br />
                                        <br />
                                        <br />
                                        <UCReportHtmlSelection:Grid ID="UCRHSort" runat="server" />
                                        <%-- <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel1">

                                            <telerik:RadListBox runat="server" ID="SelectionSource" Height="200px" Width="200px"

                                                SelectionMode="Single" />

                                            <telerik:RadListBox runat="server" ID="SelectionDestination" AllowReorder="false"

                                                SelectionMode="Single" Height="200px" Width="200px">

                                            </telerik:RadListBox>

                                        </telerik:RadAjaxPanel>--%>
                                    </asp:WizardStep>
                                    <asp:WizardStep ID="WizardStep4" runat="server">
                                        <asp:Label runat="server" ID="lblw3" Text="Edit the Header Text "></asp:Label>
                                        <br />
                                        <br />
                                        <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel2">
                                            <table>
                                                <tr>
                                                    <td>
                                                        Field
                                                    </td>
                                                    <td>
                                                        HTML Heading
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <telerik:RadListBox runat="server" ID="SelectedSource" Height="200px" Width="200px"
                                                            SelectionMode="Single" />
                                                    </td>
                                                    <td>
                                                        <telerik:RadListBox runat="server" ID="HeaderBySource" AllowReorder="false" SelectionMode="Single"
                                                            Height="200px" Width="200px">
                                                        </telerik:RadListBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </telerik:RadAjaxPanel>
                                    </asp:WizardStep>
                                    <asp:WizardStep ID="WizardStep3" runat="server">
                                        <table width="100%">
                                            <tr>
                                                <td width="60%">
                                                    <fieldset>
                                                        <legend>Page Layout</legend>
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="tdLabel130">
                                                                                Use HTML Defaults
                                                                            </td>
                                                                            <td class="tdLabel80">
                                                                                <asp:CheckBox ID="chkHeader" runat="server" Text="Header" />
                                                                            </td>
                                                                            <td class="tdLabel70">
                                                                                <asp:CheckBox ID="chkFooter" runat="server" Text="Footer" />
                                                                            </td>
                                                                            <td class="tdLabel100">
                                                                                <asp:CheckBox ID="chkBackground" runat="server" Text="Background" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="tdLabel130">
                                                                                Title
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtTitle" runat="server" Width="300px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="tdLabel130">
                                                                                Foreground Color
                                                                            </td>
                                                                            <td class="tdLabel60">
                                                                                <telerik:RadColorPicker ID="ForegroundColor" runat="server" SelectedColor="" ShowIcon="True">
                                                                                    <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                                </telerik:RadColorPicker>
                                                                            </td>
                                                                            <td class="tdLabel110">
                                                                                Background Color
                                                                            </td>
                                                                            <td>
                                                                                <telerik:RadColorPicker ID="BackgroundColor" runat="server" SelectedColor="" ShowIcon="True">
                                                                                    <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                                </telerik:RadColorPicker>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                                <td width="40%">
                                                    <fieldset>
                                                        <legend>Table Layout</legend>
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="tdLabel130">
                                                                                Border Color
                                                                            </td>
                                                                            <td>
                                                                                <telerik:RadColorPicker ID="TableBorderColor" runat="server" SelectedColor="" ShowIcon="True">
                                                                                    <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                                </telerik:RadColorPicker>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="tdLabel130">
                                                                                Foreground Color
                                                                            </td>
                                                                            <td>
                                                                                <telerik:RadColorPicker ID="TableForegroundColor" runat="server" SelectedColor=""
                                                                                    ShowIcon="True">
                                                                                    <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                                </telerik:RadColorPicker>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td class="tdLabel130">
                                                                                Background Color
                                                                            </td>
                                                                            <td>
                                                                                <telerik:RadColorPicker ID="TableBackgroundColor" runat="server" SelectedColor=""
                                                                                    ShowIcon="True">
                                                                                    <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                                </telerik:RadColorPicker>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:WizardStep>
                                </WizardSteps>
                            </asp:Wizard>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <asp:HiddenField ID="hdnIsExpressQuote" runat="server" />
                                        <asp:Button ID="Button1" runat="server" Visible="false" CssClass="button" OnClick="btnSubmit_Click"
                                            Text="Preview Report" />
                                        <%--<asp:TextBox ID="tbclient" runat="server" CssClass="text60" ></asp:TextBox>&nbsp--%>
                                        <asp:TextBox ID="txtOutput" Visible="false" runat="server" TextMode="MultiLine" Width="300"
                                            Height="35"></asp:TextBox>
                                        <asp:TextBox ID="txtHtmlTable" runat="server" Visible="false" TextMode="MultiLine"
                                            Width="300" Height="35"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
