﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/ReportSetting.master" 
    AutoEventWireup="true" CodeBehind="AdHocPreFlightReports.aspx.cs" Inherits="FlightPak.Web.Views.Reports.AdHocPreFlightReports" %>

<%@ Register TagPrefix="uc" TagName="DatePicker" Src="~/UserControls/UCRptDateRange.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <div class="art-setting-menu-report">
        <div class="art-setting-content-report">
            <div class="globalMenu_mapping">
                <%--<a href="#" class="globalMenu_mapping">Home</a> <span class="breadcrumbCaret"></span>
            <a href="#" class="globalMenu_mapping">Settings</a>--%>
            </div>
            <div style="clear: both;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button runat="server" ID="btnDatabase" Text="Database" CssClass="rpt_nav" OnClick="btnDb_Click"
                                            CausesValidation="false" />
                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="btnPreFlight" Text="Preflight" CssClass="rpt_nav"
                                            OnClick="btnPre_Click" CausesValidation="false" />
                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="btnPostFlight" Text="Postflight" CssClass="rpt_nav"
                                            OnClick="btnPost_Click" CausesValidation="false" />
                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="btnCharter" Text="Charter" CssClass="rpt_nav" OnClick="btnCharter_Click"
                                            CausesValidation="false" />
                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="btnCorp" Text="Corporate Request" CssClass="rpt_nav"
                                            OnClick="btnCorpReq_Click" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="right">
                            <div class="mandatory">
                                <span>Bold</span> Indicates required field</div>
                        </td>
                    </tr>
                </table>
            </div>
            
            <div class="art-setting-sidebar-report">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <telerik:RadPanelBar runat="server" ExpandMode="SingleExpandedItem" ID="pnlReportTab"
                                AllowCollapseAllItems="true" CssClass="RadNavMenu">
                                <CollapseAnimation Type="None"></CollapseAnimation>
                                <ExpandAnimation Type="None"></ExpandAnimation>
                            </telerik:RadPanelBar>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="art-setting-content-rpt">
                <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
                    <script type="text/javascript" src="<%=ResolveClientUrl("~/Scripts/Common.js") %>"></script>
                    <script type="text/javascript">

                        function ProcessExport() {

                            //get reference to the client grid object using the global window variable set in the user control's Page_Load server handler                            
                            var grid = $find("<%=dgSearch.ClientID %>");


                            if (grid.get_masterTableView().get_selectedItems().length > 0) {
                                return true;
                            }
                            else {
                                radalert('Please select Preflight Trip.', 330, 100, "Export", "");
                                return false;
                            }
                        }

                        //This function is to enable\disable checkboxes on chklegs checked.
                        function SetEnableOrDisable() {
                            var legs = document.getElementById("<%= chkLegs.ClientID %>");
                            var crew = document.getElementById("<%= chkCrew.ClientID %>");
                            var pax = document.getElementById("<%= chkPax.ClientID %>");
                            
                            if (legs.checked == true) {
                                crew.disabled = false;
                                pax.disabled = false;
                            }
                            else {
                                crew.disabled = true;
                                pax.disabled = true;
                                crew.checked = false;
                                pax.checked = false;
                            }
                        }

                        //this function is used to navigate to pop up screen's with the selected code
                        function openWin(radWin) {
                            var url = '';
                            if (radWin == "RadClientCodePopup") {
                                url = '/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById("<%=tbClientCode.ClientID%>").value;
                            }
                            else if (radWin == "radFleetProfilePopup") {
                                url = '/Views/Settings/Fleet/FleetProfilePopup.aspx';
                            }
                            else if (radWin == "radFromTrip") {
                                url = '../Transactions/Preflight/PreflightSearch.aspx?FromPage=Reports';
                            }
                            else if (radWin == "radToTrip") {
                                url = '../Transactions/Preflight/PreflightSearch.aspx?FromPage=Reports';
                            }
                            var oWnd = radopen(url, radWin);
                        }
                        // this function is used to display the value of selected Client code from popup
                        function ClientCidePopupClose(oWnd, args) {
                            var combo = $find("<%= tbClientCode.ClientID %>");
                            //get the transferred arguments
                            var arg = args.get_argument();
                            if (arg !== null) {
                                if (arg) {
                                    document.getElementById("<%=tbClientCode.ClientID%>").value = arg.ClientCD;
                                    document.getElementById("<%=hdnClient.ClientID%>").value = arg.ClientID;
                                    if (arg.ClientDescription != null)
                                        document.getElementById("<%=lbClientDesc.ClientID%>").innerHTML = arg.ClientDescription;
                                    if (arg.ClientCD != null)
                                        document.getElementById("<%=lbcvClient.ClientID%>").innerHTML = "";
                                }
                                else {
                                    document.getElementById("<%=tbClientCode.ClientID%>").value = "";
                                    document.getElementById("<%=hdnClient.ClientID%>").value = "";
                                    document.getElementById("<%=lbClientDesc.ClientID%>").innerHTML = "";
                                    combo.clearSelection();
                                }
                            }
                        }

                        // this function is used to display the value of selected TailNumber from popup
                        function OnClientTailNoClose(oWnd, args) {
                            var combo = $find("<%= tbTailNumber.ClientID %>");
                            //get the transferred arguments
                            var arg = args.get_argument();
                            if (arg !== null) {
                                if (arg) {
                                    document.getElementById("<%=tbTailNumber.ClientID%>").value = arg.TailNum;
                                    document.getElementById("<%=hdnTailNo.ClientID%>").value = arg.FleetID;
                                    if (arg.TailNum != null)
                                        document.getElementById("<%=lbcvTailNumber.ClientID%>").innerHTML = "";
                                }
                                else {
                                    document.getElementById("<%=tbTailNumber.ClientID%>").value = "";
                                    document.getElementById("<%=hdnTailNo.ClientID%>").value = "";
                                }
                            }
                        }

                        function OnClientCloseFromTrip(oWnd, args) {
                            var combo = $find("<%= tbFromTrip.ClientID %>");
                            //get the transferred arguments
                            var arg = args.get_argument();
                            if (arg !== null) {
                                if (arg) {
                                    document.getElementById("<%=tbFromTrip.ClientID%>").value = arg.TripNUM;
                                    document.getElementById("<%=hdnFromTripID.ClientID%>").value = arg.TripID;

                                    var step = "FromTrip_TextChanged";
                                    var ajaxManager = $find('<%= RadAjaxManager1.ClientID%>');
                                    if (step) {
                                        ajaxManager.ajaxRequest(step);
                                    }
                                }
                                else {
                                    document.getElementById("<%=tbFromTrip.ClientID%>").value = "";
                                    combo.clearSelection();
                                }
                            }
                        }

                        function OnClientCloseToTrip(oWnd, args) {
                            var combo = $find("<%= tbToTrip.ClientID %>");
                            //get the transferred arguments
                            var arg = args.get_argument();
                            if (arg !== null) {
                                if (arg) {
                                    document.getElementById("<%=tbToTrip.ClientID%>").value = arg.TripNUM;
                                    document.getElementById("<%=hdnToTripID.ClientID%>").value = arg.TripID;

                                    var step = "ToTrip_TextChanged";
                                    var ajaxManager = $find('<%= RadAjaxManager1.ClientID%>');
                                    if (step) {
                                        ajaxManager.ajaxRequest(step);
                                    }
                                }
                                else {
                                    document.getElementById("<%=tbToTrip.ClientID%>").value = "";
                                    combo.clearSelection();
                                }
                            }
                        }

                        // this function is used to get the dimensions
                        function GetDimensions(sender, args) {
                            var bounds = sender.getWindowBounds();
                            return;
                        }


                        //this function is used to close this window
                        function CloseAndRebind(arg) {
                            GetRadWindow().Close();
                            GetRadWindow().BrowserWindow.refreshPage(arg);
                        }

                        //this function is used to set the frame element
                        function GetRadWindow() {
                            var oWindow = null;
                            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                            return oWindow;
                        }

                        function RowDblClick() {
                            var masterTable = $find("<%= dgSearch.ClientID %>").get_masterTableView();
                            masterTable.fireCommand("InitInsert", "");
                            return false;
                        }

                        function confirmCallBackFn(arg) {
                            if (arg == true) {
                                document.getElementById('<%=btnYes.ClientID%>').click();
                            }
                            else {
                                document.getElementById('<%=btnNo.ClientID%>').click();
                            }
                        }

                        function CloseRadWindow(arg) {
                            GetRadWindow().Close();
                        }

                        function returnToParent() {
                            oArg = new Object();
                            grid = $find("<%= dgSearch.ClientID %>");
                            var MasterTable = grid.get_masterTableView();
                            var selectedRows = MasterTable.get_selectedItems();
                            var SelectedMain = false;

                            for (var i = 0; i < selectedRows.length; i++) {
                                var row = selectedRows[i];
                                var cell1 = MasterTable.getCellByColumnUniqueName(row, "TripID");
                                var cell2 = MasterTable.getCellByColumnUniqueName(row, "TripNUM");
                            }

                            if (selectedRows.length > 0) {
                                oArg.TripID = cell1.innerHTML;
                                oArg.TripNUM = cell2.innerHTML;
                            }
                            else {
                                oArg.TripID = "";
                                oArg.TripNUM = "";
                            }
                            var oWnd = GetRadWindow();
                            if (oArg) {
                                oWnd.close(oArg);
                            }
                        }

                        function Close() {
                            GetRadWindow().Close();
                        }

                        function prepareSearchInput(input) { input.value = input.value; }
                    </script>
                </telerik:RadCodeBlock>
                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="btnSearch">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />                                
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="btnCancel">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="chkLegs" />
                                <telerik:AjaxUpdatedControl ControlID="chkPax" />
                                <telerik:AjaxUpdatedControl ControlID="chkCrew" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="tbClientCode">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="lbClientDesc" />
                                <telerik:AjaxUpdatedControl ControlID="lbcvClient" />
                                <telerik:AjaxUpdatedControl ControlID="hdnClient" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="tbTailNumber">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="lbTailNumber" />
                                <telerik:AjaxUpdatedControl ControlID="lbcvTailNumber" />
                                <telerik:AjaxUpdatedControl ControlID="hdnTailNo" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                    </AjaxSettings>
                </telerik:RadAjaxManager>
                <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true"
                    ShowContentDuringLoad="false">
                    <Windows>
                        <telerik:RadWindow ID="RadClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                            OnClientClose="ClientCidePopupClose" AutoSize="true" KeepInScreenBounds="true"
                            Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
                        </telerik:RadWindow>
                        <telerik:RadWindow ID="radFleetProfilePopup" runat="server" OnClientResizeEnd="GetDimensions"
                            OnClientClose="OnClientTailNoClose" AutoSize="true" KeepInScreenBounds="true"
                            Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
                        </telerik:RadWindow>
                        <telerik:RadWindow ID="radFleetProfileCRUDPopup" runat="server" Height="800px" Width="1100px"
                            ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                            VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
                        </telerik:RadWindow>
                        <telerik:RadWindow ID="radFromTrip" runat="server" OnClientResizeEnd="GetDimensions"
                            OnClientClose="OnClientCloseFromTrip" AutoSize="true" KeepInScreenBounds="true"
                            Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/PreflightSearch.aspx">
                        </telerik:RadWindow>
                        <telerik:RadWindow ID="radToTrip" runat="server" OnClientResizeEnd="GetDimensions"
                            OnClientClose="OnClientCloseToTrip" AutoSize="true" KeepInScreenBounds="true"
                            Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/PreflightSearch.aspx">
                        </telerik:RadWindow>
                    </Windows>
                    <ConfirmTemplate>
                        <div class="rwDialogPopup radconfirm">
                            <div class="rwDialogText">
                                {1}
                            </div>
                            <div>
                                <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                                    <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                                <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                                    <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                            </div>
                        </div>
                    </ConfirmTemplate>
                    <AlertTemplate>
                        <div class="rwDialogPopup radalert">
                            <div class="rwDialogText">
                                {1}
                            </div>
                            <div>
                                <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                                    <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                                </a>
                            </div>
                        </div>
                    </AlertTemplate>
                </telerik:RadWindowManager>
                <table cellspacing="0" cellpadding="0" class="box1">
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <span class="mnd_text">
                                            <asp:Label runat="server" ID="lbTitle">Ad hoc Data Export</asp:Label></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="425px">
                                        <fieldset>
                                            <legend>Search</legend>
                                            <table width="425px" cellpadding="1" cellspacing="1">
                                                <tr>
                                                    <td class="tdLabel150">
                                                        <asp:CheckBox ID="chkHomebase" Text="Home Base Only" runat="server" />
                                                    </td>
                                                    <td class="tdLabel150">
                                                        <asp:CheckBox ID="ChkPerTrav" Text="Personal Travel Only" runat="server" />
                                                    </td>
                                                    <td class="tdLabel150">
                                                        <asp:CheckBox ID="chkCompleted" Text="Completed" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" valign="top">
                                                        <table cellpadding="1" cellspacing="1">
                                                            <tr>
                                                                <td class="tdLabel80">
                                                                    From Date
                                                                </td>
                                                                <td class="tdLabel150">
                                                                    <uc:DatePicker ID="ucfromDate" runat="server"></uc:DatePicker>
                                                                </td>
                                                                <td class="tdLabel60">
                                                                    To Date
                                                                </td>
                                                                <td>
                                                                    <uc:DatePicker ID="uctoDate" runat="server"></uc:DatePicker>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    From Trip
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="tbFromTrip" runat="server" onKeyPress="return fnAllowNumeric(this, event)"
                                                                        CssClass="tdLabel80"></asp:TextBox>
                                                                    <asp:HiddenField ID="hdnFromTripID" runat="server" />
                                                                </td>
                                                                <td>
                                                                    To Trip
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="tbToTrip" runat="server" onKeyPress="return fnAllowNumeric(this, event)"
                                                                        CssClass="tdLabel80"></asp:TextBox>
                                                                    <asp:HiddenField ID="hdnToTripID" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel80">
                                                                    Client Code
                                                                </td>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbClientCode" runat="server" CssClass="text80" OnTextChanged="Client_TextChanged"
                                                                                                AutoPostBack="true"></asp:TextBox>
                                                                                            <asp:HiddenField ID="hdnClient" runat="server" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Button ID="btnClientCode" ToolTip="View Clients" OnClientClick="javascript:openWin('RadClientCodePopup');return false;"
                                                                                                CssClass="browse-button" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lbClientDesc" runat="server" CssClass="input_no_bg"></asp:Label>
                                                                                <asp:Label ID="lbcvClient" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="tdLabel60">
                                                                    Tail No.
                                                                </td>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0" border="0">
                                                                        <tr>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:TextBox ID="tbTailNumber" CssClass="text80" runat="server" OnTextChanged="TailNo_TextChanged"
                                                                                                AutoPostBack="true"></asp:TextBox>
                                                                                            <asp:HiddenField ID="hdnTailNo" runat="server" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Button ID="Button1" ToolTip="Search for Tail No." OnClientClick="javascript:openWin('radFleetProfilePopup');return false;"
                                                                                                CssClass="browse-button" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lbTailNumber" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                                                                <asp:Label ID="lbcvTailNumber" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                    <td valign="top" width="300px">
                                        <div style="width: 300px;">
                                            <fieldset>
                                                <legend>Export Options</legend>
                                                <table width="300px" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="tdLabel100">
                                                            Format
                                                        </td>
                                                        <td>
                                                            <asp:RadioButtonList ID="radbtnlst" RepeatDirection="Horizontal" runat="server">
                                                                <asp:ListItem Value="CSV" Selected="True" Text="csv"></asp:ListItem>
                                                                <asp:ListItem Value="EXCEL" Text="xls"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <div id="Div4" runat="server" class="nav-3">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            Details to be exported
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <div id="Div2" runat="server" class="nav-3">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chkLegs" runat="server" Text="Legs" onclick="javascript:SetEnableOrDisable();" />
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chkPax" runat="server" Text="PAX" Enabled="false" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chkCrew" runat="server" Text="Crew" Enabled="false" />
                                                        </td>
                                                        <td>
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <div id="Div3" runat="server" class="nav-6">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="button" OnClientClick="javascript:return ProcessExport();" OnClick="btnExport_Click" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancel_Click" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <div style="height: 29px;">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="Div1" runat="server" class="nav-6">
                            </div>
                        </td>
                    </tr>
                </table>
                <div id="DivExternalForm" runat="server">
                    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
                    <table>
                        <tr>
                            <td>
                                <telerik:RadGrid ID="dgSearch" runat="server" AllowMultiRowSelection="true" AllowSorting="false"
                                    OnNeedDataSource="Search_BindData" OnItemCommand="Search_ItemCommand" OnInsertCommand="Search_InsertCommand"
                                    OnItemCreated="Search_ItemCreated" AutoGenerateColumns="false" PageSize="10"
                                    Width="760px" Height="355px" AllowPaging="false" PagerStyle-AlwaysVisible="false"
                                    OnItemDataBound="Search_ItemDataBound" OnPreRender="dgSearch_PreRender">
                                    <GroupingSettings CaseSensitive="false" />
                                    <MasterTableView DataKeyNames="TripID,RequestorName,HomebaseID,HomebaseCD"
                                        CommandItemDisplay="Bottom" AllowFilteringByColumn="true" AllowPaging="false">
                                        <Columns>
                                            <telerik:GridClientSelectColumn UniqueName="ClientSelectTrip" DataTextField="TripNUM"
                                                HeaderText="Select All" HeaderStyle-Width="90px" HeaderTooltip="Select All" />
                                            <telerik:GridBoundColumn DataField="TripNUM" HeaderText="Trip No." AutoPostBackOnFilter="false"
                                                CurrentFilterFunction="EqualTo" ShowFilterIcon="false" HeaderStyle-Width="60px"
                                                FilterControlWidth="40px" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DispatchNum" HeaderText="Dispatch" AutoPostBackOnFilter="false"
                                                CurrentFilterFunction="StartsWith" ShowFilterIcon="false" HeaderStyle-Width="60px"
                                                FilterControlWidth="40px" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail No." AutoPostBackOnFilter="false"
                                                CurrentFilterFunction="StartsWith" ShowFilterIcon="false" HeaderStyle-Width="60px"
                                                FilterControlWidth="40px" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EstDepartureDT" HeaderText="Departure Date" AutoPostBackOnFilter="false"
                                                CurrentFilterFunction="EqualTo" ShowFilterIcon="false" DataFormatString="{0:MM/dd/yyyy}"
                                                HeaderStyle-Width="80px" FilterControlWidth="60px" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PassengerCD" HeaderText="Passenger/ Requestor" AutoPostBackOnFilter="false" 
                                                CurrentFilterFunction="StartsWith" ShowFilterIcon="false" FilterDelay="500"
                                                HeaderStyle-Width="80px" FilterControlWidth="60px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DepartmentCD" HeaderText="Department" AutoPostBackOnFilter="false"
                                                CurrentFilterFunction="StartsWith" ShowFilterIcon="false" HeaderStyle-Width="80px"
                                                FilterControlWidth="60px" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="AuthorizationCD" HeaderText="Authorization" AutoPostBackOnFilter="false"
                                                CurrentFilterFunction="StartsWith" ShowFilterIcon="false" HeaderStyle-Width="85px"
                                                FilterControlWidth="65px" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TripDescription" HeaderText="Description" AutoPostBackOnFilter="false"
                                                CurrentFilterFunction="StartsWith" ShowFilterIcon="false" DataFormatString="<nobr>{0}</nobr>"
                                                HeaderStyle-Width="180px" FilterControlWidth="160px" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="IsPersonal" HeaderText="Personal" AutoPostBackOnFilter="true"
                                                CurrentFilterFunction="EqualTo" ShowFilterIcon="false" AllowFiltering="false"
                                                HeaderStyle-Width="65px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="HomebaseCD" HeaderText="Home Base" AutoPostBackOnFilter="false"
                                                CurrentFilterFunction="StartsWith" ShowFilterIcon="false" HeaderStyle-Width="80px"
                                                FilterControlWidth="60px" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="FlightNum" HeaderText="Flight No." AutoPostBackOnFilter="false"
                                                CurrentFilterFunction="StartsWith" ShowFilterIcon="false" HeaderStyle-Width="60px"
                                                FilterControlWidth="40px" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="IsCompleted" HeaderText="Completed" AutoPostBackOnFilter="true"
                                                CurrentFilterFunction="EqualTo" ShowFilterIcon="false" AllowFiltering="true"
                                                HeaderStyle-Width="70px" FilterControlWidth="50px" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LastUpdTS" HeaderText="Time" AutoPostBackOnFilter="false"
                                                CurrentFilterFunction="StartsWith" ShowFilterIcon="false" HeaderStyle-Width="70px"
                                                FilterControlWidth="50px" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TechLog" HeaderText="Tech Log" AutoPostBackOnFilter="false"
                                                CurrentFilterFunction="StartsWith" ShowFilterIcon="false" HeaderStyle-Width="70px"
                                                FilterControlWidth="50px" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                        <CommandItemTemplate>
                                            <div style="padding: 5px 5px; text-align: right;">
                                                <asp:Button Visible="false" ID="lbtnInitInsert" runat="server" ToolTip="OK" CommandName="InitInsert"
                                                    CausesValidation="false" CssClass="button" Text="OK"></asp:Button>
                                            </div>
                                        </CommandItemTemplate>
                                    </MasterTableView>
                                    <ClientSettings>
                                        <ClientEvents OnRowDblClick="RowDblClick" />
                                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbMessage" runat="server" CssClass="alert-text" Font-Bold="true"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="tblHidden" style="display: none; position: relative">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnYes" runat="server" Text="Button" OnClick="Yes_Click" />
                                            <asp:Button ID="btnNo" runat="server" Text="Button" OnClick="No_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
