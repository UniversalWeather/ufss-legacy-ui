﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using Telerik.Web.UI;
using System.IO;
using System.Data;
using FlightPak.Common;
using FlightPak.Common.Constants;
using FlightPak.Web.UserControls;

//For Tracing and Exceptioon Handling
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Text.RegularExpressions;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar;
using System.Xml;
using System.Text;
using System.Xml.XPath;
using System.Drawing;
using System.Xml.Xsl;
using System.Xml.Linq;
//using FlightPak.Web.PreflightService;

#endregion
namespace FlightPak.Web.Views.Reports
{
    public partial class CalendarReports : BaseSecuredPage
    {
        string urlBase = "Config\\";
        string filename = string.Empty;
        string ReportString = string.Empty;
        string DateFormat = "MM/dd/yyyy";
        string DaysFormat = "";
        int DisplayDays = 1;

        public Dictionary<string, string> dicColumnList = new Dictionary<string, string>();
        public Dictionary<string, string> dicSelectedList = new Dictionary<string, string>();
        public Dictionary<string, string> dicSelectedListsort = new Dictionary<string, string>();
        public Dictionary<string, string> dicSelectedListsortSel = new Dictionary<string, string>();
        public Dictionary<string, string> dicColumnListsort = new Dictionary<string, string>();

        public Dictionary<string, string> dicSourceList = new Dictionary<string, string>();
        public Dictionary<string, string> dicDestinationList = new Dictionary<string, string>();
        public Dictionary<string, string> dicSourceSortList = new Dictionary<string, string>();
        public Dictionary<string, string> dicDestinationSortList = new Dictionary<string, string>();
        public Dictionary<string, string> dicEmptyList = new Dictionary<string, string>();

        private string SelectedFleetCrewIDs = string.Empty;
        DisplayOptionsPopup objFleetCrewIDs = new DisplayOptionsPopup();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Report"] != null)
            {
                ReportString = Request.QueryString["Report"];
            }
            DaysFormat = Convert.ToString(Session["SCPlannerSlot"]);
            SelectTab(); 
           
            //LoadTabs(pnlReportTab, urlBase + xmltab);
            //btnCalender.CssClass = "rpt_nav";
            //btnPreFlight.CssClass = "rpt_nav";
            //btnPostFlight.CssClass = "rpt_nav";
            //btnDatabase.CssClass = "rpt_nav";
            //btnCorp.CssClass = "rpt_nav";
            //btnCharter.CssClass = "rpt_nav_active";
            //ExpandPanel(pnlReportTab, xmltab, true);

            if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
            {
                DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                RadDatePicker1.DateInput.DateFormat = DateFormat;
            }
            else
            {
                DateFormat = "MM/dd/yyyy";
                RadDatePicker1.DateInput.DateFormat = DateFormat;
            }

            if (ReportString != "RptPlanner")
            {
                lbWeeks.Text = "No. of Weeks";
                lbEndDate.Text = "End Date";
                tbEndDate.Enabled = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(DaysFormat))
                {
                    switch (DaysFormat)
                    {                        
                        case "1.00:00:00":
                            lbWeeks.Text = "Months (Display of 1 Day)";
                            DisplayDays = 1;
                            break;
                        case "12:00:00":
                            lbWeeks.Text = "Months (Display of 12 Hrs)";
                            DisplayDays = 2;
                            break;
                        case "08:00:00":
                            lbWeeks.Text = "Months (Display of 8 Hrs)";
                            DisplayDays = 3;
                            break;
                        case "06:00:00":
                            lbWeeks.Text = "Months (Display of 6 Hrs)";
                            DisplayDays = 4;
                            break;
                        case "01:00:00":
                            lbWeeks.Text = "Months (Display of 1 Hr)";
                            DisplayDays = 5;
                            break;
                        default:
                            lbWeeks.Text = "Months (Display of 1 Day)";
                            DisplayDays = 1;
                            break;
                    }
                }
                else
                {
                    lbWeeks.Text = "Months (Display of 1 Day)";
                    DisplayDays = 1;
                }

                //lbWeeks.Text = "Months (Display of 1 Day)";
                lbEndDate.Text = "End Date";
                tbEndDate.Enabled = false;
            }

            
            hdnRptXMLName.Value = Request.QueryString["xmlFilename"];
            switch (ReportString)
            {
                case "RptWeekly":
                    hdnRptXMLName.Value = "PREScheduleCalendarByWeek.xml";
                    filename = "PREScheduleCalendarByWeek.xml";
                    break;

                case "RptWeeklyFleet":
                    hdnRptXMLName.Value = "PREWeeklyFleetScheduleCalendar.xml";
                    filename = "PREWeeklyFleetScheduleCalendar.xml";
                    break;

                case "RptWeeklyCrew":
                    hdnRptXMLName.Value = "PREWeeklyCrewScheduleCalendar.xml";
                    filename = "PREWeeklyCrewScheduleCalendar.xml";
                    break;

                case "RptWeeklyDetail":
                    hdnRptXMLName.Value = "PREWeeklyDetailScheduleCalendar.xml";
                    filename = "PREWeeklyDetailScheduleCalendar.xml";
                    break;

                case "RptMonthly":
                    hdnRptXMLName.Value = "PREMonthlyScheduleCalendar.xml";
                    filename = "PREMonthlyScheduleCalendar.xml";
                    break;                    

                case "RptDay":

                    hdnRptXMLName.Value = "PREScheduleDayCalendar.xml";
                    filename = "PREScheduleDayCalendar.xml";
                    
                    lbWeeks.Text = "No. of Days";
                    lbEndDate.Text = "End Date";
                    tbEndDate.Enabled = true;

                    break;

                case "RptPlanner":
                    hdnRptXMLName.Value = "PREPlannerScheduleCalendar.xml";
                    filename = "PREPlannerScheduleCalendar.xml";
                    break;

                case "RptBusinessWeek":

                    hdnRptXMLName.Value = "PREScheduleCalendarBusinessWeek.xml";
                    filename = "PREScheduleCalendarBusinessWeek.xml";

                    lbWeeks.Text = "No. of Business Weeks";
                    lbEndDate.Text = "End Date";
                    tbEndDate.Enabled = false;
                    break;

                case "RptCorporate":
                    hdnRptXMLName.Value = "PRECorporateCalendar.xml";
                    filename = "PRECorporateCalendar.xml";
                    break;                  

                default:
                    hdnRptXMLName.Value = "PREScheduleCalendarWeeklyDetail.xml";
                    filename = "PREScheduleCalendarWeeklyDetail.xml";
                    break;                    
            }
            if (!IsPostBack)
            {
                Wizard1.Visible = false;
                ExportPanal.Visible = false;

                Session["SourceList"] = null;
                Session["DestinationList"] = null;
                Session["SourceSortList"] = null;
                Session["DestinationSortList"] = null;


                if (ReportString != "RptPlanner")
                {
                    tbBeginDate.Text = String.Format("{0:" + DateFormat + "}", DateTime.UtcNow);
                    tbEndDate.Text = String.Format("{0:" + DateFormat + "}", DateTime.UtcNow.AddDays(6));
                }
                else
                {
                    tbBeginDate.Text = String.Format("{0:" + DateFormat + "}", DateTime.UtcNow);

                    if (!string.IsNullOrEmpty(DaysFormat))
                    {
                        switch (DaysFormat)
                        {
                            case "1.00:00:00":
                                tbEndDate.Text = String.Format("{0:" + DateFormat + "}", DateTime.UtcNow.AddDays(29));
                                DisplayDays = 1;
                                break;

                            case "12:00:00":                                
                                tbEndDate.Text = String.Format("{0:" + DateFormat + "}", DateTime.UtcNow.AddDays(14));
                                DisplayDays = 2;
                                break;

                            case "08:00:00":                                
                                tbEndDate.Text = String.Format("{0:" + DateFormat + "}", DateTime.UtcNow.AddDays(9));
                                DisplayDays = 3;
                                break;

                            case "06:00:00":                                
                                tbEndDate.Text = String.Format("{0:" + DateFormat + "}", DateTime.UtcNow.AddDays(7));
                                DisplayDays = 4;
                                break;

                            case "01:00:00":
                                tbEndDate.Text = String.Format("{0:" + DateFormat + "}", DateTime.UtcNow.AddDays(1));
                                DisplayDays = 5;
                                break;

                            default:
                                tbEndDate.Text = String.Format("{0:" + DateFormat + "}", DateTime.UtcNow.AddDays(29));
                                DisplayDays = 1;
                                break;
                        }
                    }
                    else
                    {
                        tbEndDate.Text = String.Format("{0:" + DateFormat + "}", DateTime.UtcNow.AddDays(29));
                    }

                    //lbWeeks.Text = "Months (Display of 1 Day)";
                    lbEndDate.Text = "End Date";
                    tbEndDate.Enabled = false;
                }
            

                if (ReportString == "RptBusinessWeek")
                {
                    tbBeginDate.Text = String.Format("{0:" + DateFormat + "}", DateTime.UtcNow);
                    tbEndDate.Text = String.Format("{0:" + DateFormat + "}", DateTime.UtcNow.AddDays(14));
                }

                if (ReportString == "RptDay")
                {
                    tbBeginDate.Text = String.Format("{0:" + DateFormat + "}", DateTime.UtcNow);
                    tbEndDate.Text = String.Format("{0:" + DateFormat + "}", DateTime.UtcNow);
                }
            }

            Session["TabSelect1"] = "Calender";
            if (Session["SCUserSettings"] == null)
            {
                using (var client = new FlightPak.Web.PreflightService.PreflightServiceClient())
                {
                    // get settings from database and update filterSettings option to user settings... and then save
                    Session["SCUserSettings"] = ScheduleCalendarBase.GetUserSettings();
                    var filterAndDisplaySettings = (AdvancedFilterSettings)Session["SCUserSettings"];
                    var userSettings = client.RetrieveFilterSettings();
                    // For new user userSettings will be string.Empty
                    userSettings = GetFilterSettings(CurrentAdvancedFilterTab.FilterCriteria, userSettings);
                    var settingsObj = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(userSettings);
                    filterAndDisplaySettings.Filters = settingsObj.Filters;
                    client.SaveUserDefaultSettings(XMLSerializationHelper.Serialize(filterAndDisplaySettings));
                    Session["SCUserSettings"] = filterAndDisplaySettings;
                }
            }
        }


        public string GetFilterSettings(CurrentAdvancedFilterTab selectedTab, string userSettings)
        {
            var settings = new AdvancedFilterSettings();

            if (!string.IsNullOrWhiteSpace(userSettings)) // existing user
            {
                settings = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(userSettings);
            }

            //Set Filter criteria

            if (settings.Filters == null)
                settings.Filters = new FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria();

            var filters = settings.Filters;
            return XMLSerializationHelper.Serialize(settings);
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            lbScript.Visible = true;
            lbScript.Text = "<script type='text/javascript'>Close()</" + "script>";
        }


        protected void Cancels_Click(object sender, EventArgs e)
        {
            var filterAndDisplaySettings = (AdvancedFilterSettings)Session["SCUserSettings"];

            var serializedXml = XMLSerializationHelper.Serialize<AdvancedFilterSettings>(filterAndDisplaySettings);
            UCFilterCriteria FilterCriterias = new UCFilterCriteria();
            var userSettingsString = FilterCriterias.GetFilterSettings(CurrentAdvancedFilterTab.FilterCriteria, serializedXml);

            var options = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(userSettingsString);


        }

        protected void tbWeeks_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (!string.IsNullOrEmpty(tbBeginDate.Text))
                {
                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                    DateTime DateStart = new DateTime();
                    DateTime DateEnd = new DateTime();
                    int Weeks = 0;
                    int Days = 0;
                    if (!string.IsNullOrEmpty(tbBeginDate.Text))
                    {
                        if (DateFormat != null)
                        {
                            DateStart = Convert.ToDateTime(FormatDate(tbBeginDate.Text, DateFormat));
                        }
                        else
                        {
                            DateStart = Convert.ToDateTime(tbBeginDate.Text);
                        }
                    }
                    if (!string.IsNullOrEmpty(tbEndDate.Text))
                    {
                        if (DateFormat != null)
                        {
                            DateEnd = Convert.ToDateTime(FormatDate(tbEndDate.Text, DateFormat));
                        }
                        else
                        {
                            DateEnd = Convert.ToDateTime(tbEndDate.Text);
                        }
                    }
                    if (tbWeeks.Text.Trim() != string.Empty)
                    {
                        Weeks = Convert.ToInt32(tbWeeks.Text.Trim());
                    }
                    if (Weeks == 0)
                    {
                        Weeks = 1;
                        tbWeeks.Text = "1";
                    }
                    if (ReportString != "RptPlanner")
                    {
                        Days = (Weeks * 7) - 1;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(DaysFormat))
                        {
                            switch (DaysFormat)
                            {
                                case "1.00:00:00":
                                    Days = (Weeks * 30) - 1;
                                    DisplayDays = 1;
                                    break;
                                case "12:00:00":
                                    Days = (Weeks * 15) - 1;
                                    DisplayDays = 2;
                                    break;
                                case "08:00:00":
                                    Days = (Weeks * 10) - 1;
                                    DisplayDays = 3;
                                    break;
                                case "06:00:00":
                                    Days = (Weeks * 8) - 1;
                                    DisplayDays = 4;
                                    break;
                                case "01:00:00":
                                    Days = (Weeks * 2) - 1;
                                    DisplayDays = 5;
                                    break;
                                default:
                                    Days = (Weeks * 30) -1;
                                    DisplayDays = 1;
                                    break;
                            }
                        }
                        else
                        {
                            Days = (Weeks * 30) - 1;
                            DisplayDays = 1;
                        }                        
                    }

                    if (ReportString == "RptBusinessWeek")
                    {
                        Days = (Weeks * 15) - 1;
                    }

                    if (ReportString == "RptDay")
                    {
                        Days = Weeks - 1;
                    }
                    tbEndDate.Text = String.Format("{0:" + DateFormat + "}", DateStart.AddDays(Days));
                }
            }
        }
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            bool IsValidate = true;
            bool ValidateReportDates = false;
            if (ValidateReportDate() && (ReportString != "RptPlanner"))
            {
                string msgToDisplay = "Report Start Date cannot be greater than Report End Date";
                RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "FSS", "alertCrewCallBackFn");
                IsValidate = false;
                ValidateReportDates = true;


            }
            if (ValidateReport7Days() && ValidateReportDates == false && (ReportString != "RptPlanner") && (ReportString != "RptDay"))
            {
                string msgToDisplay = "End Date should be in the increments of 7 starting from " + tbBeginDate.Text;

                if (ReportString == "RptBusinessWeek")
                {
                    msgToDisplay = "End Date should be in the increments of 15 starting from " + tbBeginDate.Text;
                }
                RadWindowManager1.RadAlert(msgToDisplay, 400, 100, "FSS", "alertCrewCallVisiBackFn");
                IsValidate = false;


            }
            if (IsValidate == true)
            {
                if (Session["SCUserSettings"] != null)
                {
                    int Vendors = Convert.ToInt32("1");

                    var filterAndDisplaySettings = (AdvancedFilterSettings)Session["SCUserSettings"];

                    var serializedXml = XMLSerializationHelper.Serialize<AdvancedFilterSettings>(filterAndDisplaySettings);
                    UCFilterCriteria FilterCriterias = new UCFilterCriteria();

                    var settings = new AdvancedFilterSettings();

                    if (!string.IsNullOrWhiteSpace(serializedXml)) // existing user
                    {
                        settings = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(serializedXml);
                    }

                    //Set Filter criteria

                    if (settings.Filters == null)
                        settings.Filters = new FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria();

                    var filters = settings.Filters;

                    var VendorFilters = settings.Filters.VendorsFilter;


                    var options = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(XMLSerializationHelper.Serialize(settings));

                    switch (settings.Filters.VendorsFilter)
                    {
                        case FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.VendorOption.IncludeActiveVendors:
                            Vendors = 1;
                            break;
                        case FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.VendorOption.OnlyShowActiveVendors:
                            Vendors = 2;
                            break;
                        case FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.VendorOption.ExcludeActiveVendors:
                            Vendors = 3;
                            break;
                    }


                    TimeBase timeBase = TimeBase.Local;
                    bool IsShowTrip = false;
                    bool IsActiveOnly = false;
                    bool IsFirstLastLeg = false;
                    bool IsCrew = false;
                    bool IsShowTripStatus = false;
                    bool IsDisplayredFlight = false;
                    bool IsFlightNumber = false;
                    bool IsLegPurpose = false;
                    bool IsSeatAvailable = false;
                    bool IsPaxCount = false;
                    bool IsPaxFullName = false;
                    bool IsRequestor = false;
                    bool IsDepartment = false;
                    bool IsAuthorization = false;
                    bool IsFlightCategory = false;
                    bool IsCummulativeETE = false;
                    bool IsTotalETE = false;
                    bool IsETE = false;

                    bool IsCrewFullName = false;
                    bool IsPax = false;
 
                    int IsAiport = 1;
                    string UserCD = UserPrincipal.Identity._name;
                    Int64 CustomerID = UserPrincipal.Identity._customerID;
                    string BeginDate = FormatDate(tbBeginDate.Text, DateFormat);
                    int NoofWeeks = Convert.ToInt32(tbWeeks.Text.Trim());
                    string EndDate = FormatDate(tbEndDate.Text, DateFormat);
                    string ClientCd = Convert.ToString(options.Filters.Client);
                    string RequestorCd = Convert.ToString(options.Filters.Requestor);
                    string DepartmentCd = Convert.ToString(options.Filters.Department);
                    string FlightCategoryCd = Convert.ToString(options.Filters.FlightCategory);
                    string DutyTypeCd = Convert.ToString(options.Filters.CrewDuty);
                    string HomeBaseCd = Convert.ToString(options.Filters.HomeBase);
                    bool IsTrip = Convert.ToBoolean(options.Filters.Trip);
                    bool IsCancelled = Convert.ToBoolean(options.Filters.Canceled);
                    bool IsHold = Convert.ToBoolean(options.Filters.Hold);
                    bool IsWorksheet = Convert.ToBoolean(options.Filters.WorkSheet);
                    bool IsUnFulFilled = Convert.ToBoolean(options.Filters.UnFulfilled);
                    bool IsAllCrew = Convert.ToBoolean(options.Filters.AllCrew);
                    bool HomeBase = Convert.ToBoolean(options.Filters.HomeBaseOnly);
                    bool IsCrewCalActivity = false;
                    bool FixedWingCrew = Convert.ToBoolean(options.Filters.FixedWingCrewOnly);
                    bool RotaryWingCrew = Convert.ToBoolean(options.Filters.RotaryWingCrewCrewOnly);
                    bool IsDisplayRONPax = Convert.ToBoolean(options.Display.Corporate.DisplayRONPax);                                         
                    bool IsBlackWhiteClr = false;
                    bool IsAircraftClr = false;
                    bool IsColor = false; 
                    bool IsNonReversedColor = false;
 
                    string EFleet = "";
                    string ECrew = "";

                    int ShowLegs = Convert.ToInt32(options.Display.Planner.DisplayLegFleet) > 0 ? Convert.ToInt32(options.Display.Planner.DisplayLegFleet) : 1;
                    int ShowTails = Convert.ToInt32(options.Display.Planner.DisplayTailCrew) > 0 ? Convert.ToInt32(options.Display.Planner.DisplayTailCrew) : 1;

                    if (options.Filters.TimeBase != null)
                    {
                        timeBase = options.Filters.TimeBase;
                    }

                    int SortBy = 1;
                    int IsBoth = 1;

                    /*if (Session["FleetIDs"] != null)
                        EFleet = Convert.ToString(Session["FleetIDs"]);                    

                    if (Session["CrewIDs"] != null)
                        ECrew = Convert.ToString(Session["CrewIDs"]);*/

                    IsColor = chkIsColor.Checked;

                    if (ReportString == "RptWeekly")
                    {
                        IsShowTrip         = Convert.ToBoolean(options.Display.Weekly.ShowTrip);
                        IsFirstLastLeg     = Convert.ToBoolean(options.Display.Weekly.FirstLastLeg);
                        IsCrew             = Convert.ToBoolean(options.Display.Weekly.Crew);
                        IsShowTripStatus   = Convert.ToBoolean(options.Display.Weekly.ShowTripStatus);
                        IsDisplayredFlight = Convert.ToBoolean(options.Display.Weekly.DisplayRedEyeFlightsInContinuation);
                        IsFlightNumber     = Convert.ToBoolean(options.Display.Weekly.FlightNumber);
                        IsLegPurpose       = Convert.ToBoolean(options.Display.Weekly.LegPurpose);
                        IsSeatAvailable    = Convert.ToBoolean(options.Display.Weekly.SeatsAvailable);
                        IsPaxCount         = Convert.ToBoolean(options.Display.Weekly.PaxCount);
                        IsRequestor        = Convert.ToBoolean(options.Display.Weekly.Requestor);
                        IsDepartment       = Convert.ToBoolean(options.Display.Weekly.Department);
                        IsAuthorization    = Convert.ToBoolean(options.Display.Weekly.Authorization);
                        IsFlightCategory   = Convert.ToBoolean(options.Display.Weekly.FlightCategory);
                        IsCummulativeETE   = Convert.ToBoolean(options.Display.Weekly.CummulativeETE);
                        IsTotalETE = Convert.ToBoolean(options.Display.Weekly.TotalETE);
                        IsETE = Convert.ToBoolean(options.Display.Weekly.ETE);

                        IsBlackWhiteClr = Convert.ToBoolean(options.Display.Weekly.BlackWhiteColor);
                        IsAircraftClr = Convert.ToBoolean(options.Display.Weekly.AircraftColors);  

                        Session["REPORT"]  = "RptPREScheduleWeeklyCalendar";

                        if (options.Display.Weekly.DepartArriveInfo == DepartArriveInfo.ICAO)
                        {
                            IsAiport = 1;
                        }
                        else if (options.Display.Weekly.DepartArriveInfo == DepartArriveInfo.City)
                        {
                            IsAiport = 2;
                        }
                        else if (options.Display.Weekly.DepartArriveInfo == DepartArriveInfo.AirportName)
                        {
                            IsAiport = 3;
                        }
                    }
                    else if (ReportString == "RptWeeklyFleet")
                    {
                        IsShowTrip         = Convert.ToBoolean(options.Display.WeeklyFleet.ShowTrip);
                        IsActiveOnly       = Convert.ToBoolean(options.Display.WeeklyFleet.ActivityOnly);
                        IsFirstLastLeg     = Convert.ToBoolean(options.Display.WeeklyFleet.FirstLastLeg);
                        IsCrew             = Convert.ToBoolean(options.Display.WeeklyFleet.Crew);
                        IsShowTripStatus   = Convert.ToBoolean(options.Display.WeeklyFleet.ShowTripStatus);
                        IsDisplayredFlight = Convert.ToBoolean(options.Display.WeeklyFleet.DisplayRedEyeFlightsInContinuation);
                        IsFlightNumber     = Convert.ToBoolean(options.Display.WeeklyFleet.FlightNumber);
                        IsLegPurpose       = Convert.ToBoolean(options.Display.WeeklyFleet.LegPurpose);
                        IsSeatAvailable    = Convert.ToBoolean(options.Display.WeeklyFleet.SeatsAvailable);
                        IsPaxCount         = Convert.ToBoolean(options.Display.WeeklyFleet.PaxCount);
                        IsRequestor        = Convert.ToBoolean(options.Display.WeeklyFleet.Requestor);
                        IsDepartment       = Convert.ToBoolean(options.Display.WeeklyFleet.Department);
                        IsAuthorization    = Convert.ToBoolean(options.Display.WeeklyFleet.Authorization);
                        IsFlightCategory   = Convert.ToBoolean(options.Display.WeeklyFleet.FlightCategory);
                        IsCummulativeETE   = Convert.ToBoolean(options.Display.WeeklyFleet.CummulativeETE);
                        IsTotalETE = Convert.ToBoolean(options.Display.WeeklyFleet.TotalETE);
                        IsETE = Convert.ToBoolean(options.Display.WeeklyFleet.ETE);
                        IsBlackWhiteClr = Convert.ToBoolean(options.Display.WeeklyFleet.BlackWhiteColor);
                        IsAircraftClr = Convert.ToBoolean(options.Display.WeeklyFleet.AircraftColors);                        
                        
                        Session["REPORT"]  = "RptPREWeeklyFleetScheduleCalendar";

                        if (options.Display.WeeklyFleet.DepartArriveInfo == DepartArriveInfo.ICAO)
                        {
                            IsAiport = 1;
                        }
                        else if (options.Display.WeeklyFleet.DepartArriveInfo == DepartArriveInfo.City)
                        {
                            IsAiport = 2;
                        }
                        else if (options.Display.WeeklyFleet.DepartArriveInfo == DepartArriveInfo.AirportName)
                        {
                            IsAiport = 3;
                        }

                    }
                    else if (ReportString == "RptWeeklyCrew")
                    {
                        IsShowTrip         = Convert.ToBoolean(options.Display.WeeklyCrew.ShowTrip);
                        IsActiveOnly       = Convert.ToBoolean(options.Display.WeeklyCrew.ActivityOnly);
                        IsFirstLastLeg     = Convert.ToBoolean(options.Display.WeeklyCrew.FirstLastLeg);
                        IsCrew             = Convert.ToBoolean(options.Display.WeeklyCrew.Crew);
                        IsShowTripStatus   = Convert.ToBoolean(options.Display.WeeklyCrew.ShowTripStatus);
                        IsDisplayredFlight = Convert.ToBoolean(options.Display.WeeklyCrew.DisplayRedEyeFlightsInContinuation);
                        IsFlightNumber     = Convert.ToBoolean(options.Display.WeeklyCrew.FlightNumber);
                        IsLegPurpose       = Convert.ToBoolean(options.Display.WeeklyCrew.LegPurpose);
                        IsSeatAvailable    = Convert.ToBoolean(options.Display.WeeklyCrew.SeatsAvailable);
                        IsPaxCount         = Convert.ToBoolean(options.Display.WeeklyCrew.PaxCount);
                        IsRequestor        = Convert.ToBoolean(options.Display.WeeklyCrew.Requestor);
                        IsDepartment       = Convert.ToBoolean(options.Display.WeeklyCrew.Department);
                        IsAuthorization    = Convert.ToBoolean(options.Display.WeeklyCrew.Authorization);
                        IsFlightCategory   = Convert.ToBoolean(options.Display.WeeklyCrew.FlightCategory);
                        IsCummulativeETE   = Convert.ToBoolean(options.Display.WeeklyCrew.CummulativeETE);
                        IsTotalETE = Convert.ToBoolean(options.Display.WeeklyCrew.TotalETE);
                        IsETE = Convert.ToBoolean(options.Display.WeeklyCrew.ETE);
                        IsBlackWhiteClr = Convert.ToBoolean(options.Display.WeeklyCrew.BlackWhiteColor);
                        IsAircraftClr = Convert.ToBoolean(options.Display.WeeklyCrew.AircraftColors);  
                         

                        Session["REPORT"]  = "RptPREWeeklyCrewScheduleCalendar";

                        if (options.Display.WeeklyCrew.DepartArriveInfo == DepartArriveInfo.ICAO)
                        {
                            IsAiport = 1;
                        }
                        else if (options.Display.WeeklyCrew.DepartArriveInfo == DepartArriveInfo.City)
                        {
                            IsAiport = 2;
                        }
                        else if (options.Display.WeeklyCrew.DepartArriveInfo == DepartArriveInfo.AirportName)
                        {
                            IsAiport = 3;
                        }
                    }
                    else if (ReportString == "RptWeeklyDetail")
                    {
                        IsShowTrip = Convert.ToBoolean(options.Display.WeeklyDetail.ShowTripNumber);
                        IsActiveOnly      = Convert.ToBoolean(options.Display.WeeklyDetail.ActivityOnly);
                        IsFirstLastLeg    = Convert.ToBoolean(options.Display.WeeklyDetail.FirstLastLeg);
                        IsCrew            = Convert.ToBoolean(options.Display.WeeklyDetail.Crew);
                        IsShowTripStatus  = Convert.ToBoolean(options.Display.WeeklyDetail.ShowTripStatus);
                        IsFlightNumber    = Convert.ToBoolean(options.Display.WeeklyDetail.FlightNumber);
                        IsLegPurpose      = Convert.ToBoolean(options.Display.WeeklyDetail.LegPurpose);
                        IsSeatAvailable   = Convert.ToBoolean(options.Display.WeeklyDetail.SeatsAvailable);
                        IsPaxCount        = Convert.ToBoolean(options.Display.WeeklyDetail.PaxCount);
                        IsPaxFullName     = Convert.ToBoolean(options.Display.WeeklyDetail.PaxFullName);
                        IsRequestor       = Convert.ToBoolean(options.Display.WeeklyDetail.Requestor);
                        IsAuthorization   = Convert.ToBoolean(options.Display.WeeklyDetail.Authorization);
                        IsDepartment      = Convert.ToBoolean(options.Display.WeeklyDetail.Department);
                        IsFlightCategory  = Convert.ToBoolean(options.Display.WeeklyDetail.FlightCategory);
                        IsCummulativeETE  = Convert.ToBoolean(options.Display.WeeklyDetail.CummulativeETE);
                        IsTotalETE = Convert.ToBoolean(options.Display.WeeklyDetail.TotalETE);
                        IsETE = Convert.ToBoolean(options.Display.WeeklyDetail.ETE);
                        IsCrewFullName    = Convert.ToBoolean(options.Display.WeeklyDetail.CrewFullName);
                        IsPax             = Convert.ToBoolean(options.Display.WeeklyDetail.Pax);

                        IsBlackWhiteClr = Convert.ToBoolean(options.Display.WeeklyDetail.BlackWhiteColor);
                        IsAircraftClr = Convert.ToBoolean(options.Display.WeeklyDetail.AircraftColors);  

                        Session["REPORT"] = "RptPREWeeklyDetailScheduleCalendar";

                        if (options.Display.WeeklyDetail.DayWeeklyDetailSortOption == DayWeeklyDetailSortOption.TailNumber)
                        {
                            SortBy = 1;
                        }
                        else if (options.Display.WeeklyDetail.DayWeeklyDetailSortOption == DayWeeklyDetailSortOption.DepartureDateTime)
                        {
                            SortBy = 2;
                        }

                        if (options.Display.WeeklyDetail.DepartArriveInfo == DepartArriveInfo.ICAO)
                        {
                            IsAiport = 1;
                        }
                        else if (options.Display.WeeklyDetail.DepartArriveInfo == DepartArriveInfo.City)
                        {
                            IsAiport = 2;
                        }
                        else if (options.Display.WeeklyDetail.DepartArriveInfo == DepartArriveInfo.AirportName)
                        {
                            IsAiport = 3;
                        }

                    }
                    else if (ReportString == "RptDay")
                    {
                        IsActiveOnly = Convert.ToBoolean(options.Display.Day.ActivityOnly);
                        IsCrewCalActivity = Convert.ToBoolean(options.Display.Day.CrewCalActivity);
                        IsFirstLastLeg = Convert.ToBoolean(options.Display.Day.FirstLastLeg);
                        IsShowTripStatus = Convert.ToBoolean(options.Display.Day.ShowTripStatus);
                        IsCummulativeETE = Convert.ToBoolean(options.Display.Day.CummulativeETE);
                        IsTotalETE = Convert.ToBoolean(options.Display.Day.TotalETE);
                        IsETE = Convert.ToBoolean(options.Display.Day.ETE);
                        IsBlackWhiteClr = Convert.ToBoolean(options.Display.Day.BlackWhiteColor);
                        IsAircraftClr = Convert.ToBoolean(options.Display.Day.AircraftColors);  

                        Session["REPORT"] = "RptPREScheduleDayCalendar";

                        if (options.Display.Day.DayWeeklyDetailSortOption == DayWeeklyDetailSortOption.TailNumber)
                        {
                            SortBy = 1;
                        }
                        else if (options.Display.Day.DayWeeklyDetailSortOption == DayWeeklyDetailSortOption.DepartureDateTime)
                        {
                            SortBy = 2;
                        }

                        if (options.Display.Day.DepartArriveInfo == DepartArriveInfo.ICAO)
                        {
                            IsAiport = 1;
                        }
                        else if (options.Display.Day.DepartArriveInfo == DepartArriveInfo.City)
                        {
                            IsAiport = 2;
                        }
                        else if (options.Display.Day.DepartArriveInfo == DepartArriveInfo.AirportName)
                        {
                            IsAiport = 3;
                        }

                    }
                    else if (ReportString == "RptPlanner")
                    {
                        IsActiveOnly      = Convert.ToBoolean(options.Display.Planner.ActiveOnly);

                        IsBlackWhiteClr = Convert.ToBoolean(options.Display.Planner.BlackWhiteColor);
                        IsAircraftClr = Convert.ToBoolean(options.Display.Planner.AircraftColors);
                        IsNonReversedColor = Convert.ToBoolean(options.Display.Planner.NonReversedColors);      

                        Session["REPORT"] = "RptPREMonthlyPlannerScheduleCalendar";

                        if (options.Display.Planner.PlannerDisplayOption == PlannerDisplayOption.Both)
                        {
                            IsBoth = 1;
                        }
                        else if (options.Display.Planner.PlannerDisplayOption == PlannerDisplayOption.FleetOnly)
                        {
                            IsBoth = 2;
                        }
                        else if (options.Display.Planner.PlannerDisplayOption == PlannerDisplayOption.CrewOnly)
                        {
                            IsBoth = 3;
                        }
                    }                   
                    else if (ReportString == "RptBusinessWeek")
                    {
                        IsShowTrip         = Convert.ToBoolean(options.Display.BusinessWeek.ShowTrip);
                        IsActiveOnly       = Convert.ToBoolean(options.Display.BusinessWeek.CrewCalActivity);
                        IsFirstLastLeg     = Convert.ToBoolean(options.Display.BusinessWeek.FirstLastLeg);
                        IsCrew             = Convert.ToBoolean(options.Display.BusinessWeek.Crew);
                        IsShowTripStatus   = Convert.ToBoolean(options.Display.BusinessWeek.ShowTripStatus);
                        IsDisplayredFlight = Convert.ToBoolean(options.Display.BusinessWeek.DisplayRedEyeFlightsInContinuation);
                        IsFlightNumber     = Convert.ToBoolean(options.Display.BusinessWeek.FlightNumber);
                        IsLegPurpose       = Convert.ToBoolean(options.Display.BusinessWeek.LegPurpose);
                        IsSeatAvailable    = Convert.ToBoolean(options.Display.BusinessWeek.SeatsAvailable);
                        IsPaxCount         = Convert.ToBoolean(options.Display.BusinessWeek.PaxCount);
                        IsRequestor        = Convert.ToBoolean(options.Display.BusinessWeek.Requestor);
                        IsDepartment       = Convert.ToBoolean(options.Display.BusinessWeek.Department);
                        IsFlightCategory   = Convert.ToBoolean(options.Display.BusinessWeek.FlightCategory);
                        IsAuthorization    = Convert.ToBoolean(options.Display.BusinessWeek.Authorization);
                        IsCummulativeETE   = Convert.ToBoolean(options.Display.BusinessWeek.CummulativeETE);
                        IsTotalETE = Convert.ToBoolean(options.Display.BusinessWeek.TotalETE);
                        IsETE = Convert.ToBoolean(options.Display.BusinessWeek.ETE);
                        IsBlackWhiteClr = Convert.ToBoolean(options.Display.BusinessWeek.BlackWhiteColor);
                        IsAircraftClr = Convert.ToBoolean(options.Display.BusinessWeek.AircraftColors); 

                        Session["REPORT"]  = "RptPREScheduleCalendarBusinessWeek";

                        if (options.Display.BusinessWeek.DepartArriveInfo == DepartArriveInfo.ICAO)
                        {
                            IsAiport = 1;
                        }
                        else if (options.Display.BusinessWeek.DepartArriveInfo == DepartArriveInfo.City)
                        {
                            IsAiport = 2;
                        }
                        else if (options.Display.BusinessWeek.DepartArriveInfo == DepartArriveInfo.AirportName)
                        {
                            IsAiport = 3;
                        }
                    }
                    else if (ReportString == "RptCorporate")
                    {
                        IsShowTripStatus = Convert.ToBoolean(options.Display.Corporate.ShowTripStatus);

                        IsBlackWhiteClr = Convert.ToBoolean(options.Display.Corporate.BlackWhiteColor);
                        IsAircraftClr = Convert.ToBoolean(options.Display.Corporate.AircraftColors); 

                        Session["REPORT"] = "RptPREScheduleCorporateCalendar";

                        if (options.Display.Corporate.DepartArriveInfo == DepartArriveInfo.ICAO)
                        {
                            IsAiport = 1;
                        }
                        else if (options.Display.Corporate.DepartArriveInfo == DepartArriveInfo.City)
                        {
                            IsAiport = 2;
                        }
                        else if (options.Display.Corporate.DepartArriveInfo == DepartArriveInfo.AirportName)
                        {
                            IsAiport = 3;
                        }
                    }                                  

                    bool Footer = false;
                    if (rbSkip.Checked == true)
                    {
                        Footer = false;
                    }
                    else
                    {
                        Footer = true;
                    }

                    Session["TabSelect1"] = "Calender";
                    Session["FORMAT"] = "PDF";
                    string[] FleetCrew;
                    switch (ReportString)
                    {
                        case "RptWeeklyFleet":                                                        
                             
                             SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyFleet);

                             FleetCrew = SelectedFleetCrewIDs.Split('|');

                             if (!string.IsNullOrEmpty(FleetCrew[0]))
                             {
                                 EFleet = string.Join(",", FleetCrew[0].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                             }                             
                             else
                             {                                 
                                 using (FlightPak.Web.PreflightService.PreflightServiceClient preflightServiceClient = new FlightPak.Web.PreflightService.PreflightServiceClient())
                                 {
                                     var fleetlist = preflightServiceClient.GetFleetInfoForFleetTree(false); // if homebaseonly option is selected, filter by homebaseID
                                     if (fleetlist != null)
                                     {
                                         var fullFleetList = fleetlist.EntityList.Where(x => x.FleetGroupID == null).ToList();
                                         //var groupedLists = list.EntityList.Where(x => x.FleetGroupID != null).ToList();
                                         var distinctfleetIds = fullFleetList.Select(x => x.FleetID).Distinct().ToList();
                                         EFleet = string.Join(",", distinctfleetIds);
                                     }
                                 }
                             }

                             Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";BeginDate=" + BeginDate + ";NoOfWeeks=" + NoofWeeks + ";EndDate=" + EndDate + ";ClientCD=" + ClientCd + ";RequestorCD=" + RequestorCd + ";DepartmentCD=" + DepartmentCd + ";FlightCatagoryCD=" + FlightCategoryCd + ";DutyTypeCD=" + DutyTypeCd + ";HomeBaseCD=" + HomeBaseCd + ";IsTrip=" + IsTrip + ";IsCanceled=" + IsCancelled + ";IsHold=" + IsHold + ";IsWorkSheet=" + IsWorksheet + ";IsUnFulFilled=" + IsUnFulFilled + ";IsAllCrew=" + IsAllCrew + ";HomeBase=" + HomeBase + ";FixedWingCrew=" + FixedWingCrew + ";RotaryWingCrew=" + RotaryWingCrew + ";TimeBase=" + timeBase + ";Vendors=" + Vendors + ";IsAirport=" + IsAiport + ";IsShowTrip=" + IsShowTrip + ";IsActivityOnly=" + IsActiveOnly + ";IsFirstLastLeg=" + IsFirstLastLeg + ";IsCrew=" + IsCrew + ";IsShowTripStatus=" + IsShowTripStatus + ";IsDisplayRedEyeFlight=" + IsDisplayredFlight + ";IsFlightNo=" + IsFlightNumber + ";IsLegPurpose=" + IsLegPurpose + ";IsSeatAvailable=" + IsSeatAvailable + ";IsPaxCount=" + IsPaxCount + ";IsRequestor=" + IsRequestor + ";IsDepartment=" + IsDepartment + ";IsAuthorization=" + IsAuthorization + ";IsFlightCategory=" + IsFlightCategory + ";IsCummulativeETE=" + IsCummulativeETE + ";IsETE=" + IsETE + ";IsTotalETE=" + IsTotalETE + ";Footer=" + Footer + ";BlackWhiteClr=" + IsBlackWhiteClr + ";AircraftClr=" + IsAircraftClr + ";Color=" + IsColor + ";EFleet=" + EFleet;
                            break;

                        case "RptWeeklyCrew":

                             SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyCrew);

                             FleetCrew = SelectedFleetCrewIDs.Split('|');

                             if (!string.IsNullOrEmpty(FleetCrew[1]))
                             {
                                 ECrew = string.Join(",", FleetCrew[1].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                             }
                             else
                             {
                                 using (FlightPak.Web.PreflightService.PreflightServiceClient preflightServiceClient = new FlightPak.Web.PreflightService.PreflightServiceClient())
                                 {
                                     var crewlist = preflightServiceClient.GetCrewInfoForCrewTree(false); // if homebaseonly option is selected, filter by homebaseID
                                     if (crewlist != null)
                                     {
                                         var fullCrewList = crewlist.EntityList.Where(x => x.CrewGroupID == null).ToList();
                                         //var groupedLists = crewlist.EntityList.Where(x => x.CrewGroupID != null).ToList();
                                         var distinctcrewIds = fullCrewList.Select(x => x.CrewID).Distinct().ToList();
                                         ECrew = string.Join(",", distinctcrewIds);
                                     }
                                 }
                             }

                             Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";BeginDate=" + BeginDate + ";NoOfWeeks=" + NoofWeeks + ";EndDate=" + EndDate + ";ClientCD=" + ClientCd + ";RequestorCD=" + RequestorCd + ";DepartmentCD=" + DepartmentCd + ";FlightCatagoryCD=" + FlightCategoryCd + ";DutyTypeCD=" + DutyTypeCd + ";HomeBaseCD=" + HomeBaseCd + ";IsTrip=" + IsTrip + ";IsCanceled=" + IsCancelled + ";IsHold=" + IsHold + ";IsWorkSheet=" + IsWorksheet + ";IsUnFulFilled=" + IsUnFulFilled + ";IsAllCrew=" + IsAllCrew + ";HomeBase=" + HomeBase + ";FixedWingCrew=" + FixedWingCrew + ";RotaryWingCrew=" + RotaryWingCrew + ";TimeBase=" + timeBase + ";Vendors=" + Vendors + ";IsAirport=" + IsAiport + ";IsShowTrip=" + IsShowTrip + ";IsActivityOnly=" + IsActiveOnly + ";IsFirstLastLeg=" + IsFirstLastLeg + ";IsCrew=" + IsCrew + ";IsShowTripStatus=" + IsShowTripStatus + ";IsDisplayRedEyeFlight=" + IsDisplayredFlight + ";IsFlightNo=" + IsFlightNumber + ";IsLegPurpose=" + IsLegPurpose + ";IsSeatAvailable=" + IsSeatAvailable + ";IsPaxCount=" + IsPaxCount + ";IsRequestor=" + IsRequestor + ";IsDepartment=" + IsDepartment + ";IsAuthorization=" + IsAuthorization + ";IsFlightCategory=" + IsFlightCategory + ";IsCummulativeETE=" + IsCummulativeETE + ";IsETE=" + IsETE + ";IsTotalETE=" + IsTotalETE + ";Footer=" + Footer + ";BlackWhiteClr=" + IsBlackWhiteClr + ";AircraftClr=" + IsAircraftClr + ";Color=" + IsColor + ";ECrew=" + ECrew;                    
                            break;

                        case "RptWeekly":

                            SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyMain);

                             FleetCrew = SelectedFleetCrewIDs.Split('|');

                             if (!string.IsNullOrEmpty(FleetCrew[0]))
                             {
                                 EFleet = string.Join(",", FleetCrew[0].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                             } 
                             else if (!string.IsNullOrEmpty(FleetCrew[1]))
                             {
                                 ECrew = string.Join(",", FleetCrew[1].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                             }
                             else
                             {
                                 using (FlightPak.Web.PreflightService.PreflightServiceClient preflightServiceClient = new FlightPak.Web.PreflightService.PreflightServiceClient())
                                 {
                                     var fleetlist = preflightServiceClient.GetFleetInfoForFleetTree(false); // if homebaseonly option is selected, filter by homebaseID
                                     if (fleetlist != null)
                                     {
                                         var fullFleetList = fleetlist.EntityList.Where(x => x.FleetGroupID == null).ToList();
                                         //var groupedLists = list.EntityList.Where(x => x.FleetGroupID != null).ToList();
                                         var distinctfleetIds = fullFleetList.Select(x => x.FleetID).Distinct().ToList();
                                         EFleet = string.Join(",", distinctfleetIds);
                                     }
                                     var crewlist = preflightServiceClient.GetCrewInfoForCrewTree(false); // if homebaseonly option is selected, filter by homebaseID
                                     if (crewlist != null)
                                     {
                                         var fullCrewList = crewlist.EntityList.Where(x => x.CrewGroupID == null).ToList();
                                         //var groupedLists = crewlist.EntityList.Where(x => x.CrewGroupID != null).ToList();
                                         var distinctcrewIds = fullCrewList.Select(x => x.CrewID).Distinct().ToList();
                                         ECrew = string.Join(",", distinctcrewIds);
                                     }
                                 }                                                          
                             }
                             Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";BeginDate=" + BeginDate + ";NoOfWeeks=" + NoofWeeks + ";EndDate=" + EndDate + ";ClientCD=" + ClientCd + ";RequestorCD=" + RequestorCd + ";DepartmentCD=" + DepartmentCd + ";FlightCatagoryCD=" + FlightCategoryCd + ";DutyTypeCD=" + DutyTypeCd + ";HomeBaseCD=" + HomeBaseCd + ";IsTrip=" + IsTrip + ";IsCanceled=" + IsCancelled + ";IsHold=" + IsHold + ";IsWorkSheet=" + IsWorksheet + ";IsUnFulFilled=" + IsUnFulFilled + ";IsAllCrew=" + IsAllCrew + ";HomeBase=" + HomeBase + ";FixedWingCrew=" + FixedWingCrew + ";RotaryWingCrew=" + RotaryWingCrew + ";TimeBase=" + timeBase + ";Vendors=" + Vendors + ";IsAirport=" + IsAiport + ";IsShowTrip=" + IsShowTrip + ";IsActivityOnly=" + IsActiveOnly + ";IsFirstLastLeg=" + IsFirstLastLeg + ";IsCrew=" + IsCrew + ";IsShowTripStatus=" + IsShowTripStatus + ";IsDisplayRedEyeFlight=" + IsDisplayredFlight + ";IsFlightNo=" + IsFlightNumber + ";IsLegPurpose=" + IsLegPurpose + ";IsSeatAvailable=" + IsSeatAvailable + ";IsPaxCount=" + IsPaxCount + ";IsRequestor=" + IsRequestor + ";IsDepartment=" + IsDepartment + ";IsAuthorization=" + IsAuthorization + ";IsFlightCategory=" + IsFlightCategory + ";IsCummulativeETE=" + IsCummulativeETE + ";IsETE=" + IsETE + ";IsTotalETE=" + IsTotalETE + ";Footer=" + Footer + ";BlackWhiteClr=" + IsBlackWhiteClr + ";AircraftClr=" + IsAircraftClr + ";Color=" + IsColor + ";EFleet=" + EFleet + ";ECrew=" + ECrew;
                            break;
                     
                        case "RptWeeklyDetail":

                            SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyDetail);

                             FleetCrew = SelectedFleetCrewIDs.Split('|');

                             if (!string.IsNullOrEmpty(FleetCrew[0]))
                             {
                                 EFleet = string.Join(",", FleetCrew[0].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                             }                              
                             else
                             {
                                 using (FlightPak.Web.PreflightService.PreflightServiceClient preflightServiceClient = new FlightPak.Web.PreflightService.PreflightServiceClient())
                                 {
                                     var fleetlist = preflightServiceClient.GetFleetInfoForFleetTree(false); // if homebaseonly option is selected, filter by homebaseID
                                     if (fleetlist != null)
                                     {
                                         var fullFleetList = fleetlist.EntityList.Where(x => x.FleetGroupID == null).ToList();
                                         //var groupedLists = list.EntityList.Where(x => x.FleetGroupID != null).ToList();
                                         var distinctfleetIds = fullFleetList.Select(x => x.FleetID).Distinct().ToList();
                                         EFleet = string.Join(",", distinctfleetIds);
                                     }
                                     var crewlist = preflightServiceClient.GetCrewInfoForCrewTree(false); // if homebaseonly option is selected, filter by homebaseID
                                     if (crewlist != null)
                                     {
                                         var fullCrewList = crewlist.EntityList.Where(x => x.CrewGroupID == null).ToList();
                                         //var groupedLists = crewlist.EntityList.Where(x => x.CrewGroupID != null).ToList();
                                         var distinctcrewIds = fullCrewList.Select(x => x.CrewID).Distinct().ToList();
                                         ECrew = string.Join(",", distinctcrewIds);
                                     }
                                 }
                             }

                            Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";BeginDate=" + BeginDate + ";NoOfWeeks=" + NoofWeeks + ";EndDate=" + EndDate + ";ClientCD=" + ClientCd + ";RequestorCD=" + RequestorCd + ";DepartmentCD=" + DepartmentCd + ";FlightCatagoryCD=" + FlightCategoryCd + ";DutyTypeCD=" + DutyTypeCd + ";HomeBaseCD=" + HomeBaseCd + ";IsTrip=" + IsTrip + ";IsCanceled=" + IsCancelled + ";IsHold=" + IsHold + ";IsWorkSheet=" + IsWorksheet + ";IsUnFulFilled=" + IsUnFulFilled + ";IsAllCrew=" + IsAllCrew + ";HomeBase=" + HomeBase + ";FixedWingCrew=" + FixedWingCrew + ";RotaryWingCrew=" + RotaryWingCrew + ";TimeBase=" + timeBase + ";Vendors=" + Vendors + ";SortBy=" + SortBy + ";IsAirport=" + IsAiport + ";IsActivityOnly=" + IsActiveOnly + ";IsFirstLastLeg=" + IsFirstLastLeg + ";IsCrew=" + IsCrew + ";IsShowTripStatus=" + IsShowTripStatus + ";IsFlightNo=" + IsFlightNumber + ";IsLegPurpose=" + IsLegPurpose + ";IsSeatAvailable=" + IsSeatAvailable + ";IsPaxCount=" + IsPaxCount + ";IsPaxFullName=" + IsPaxFullName + ";IsShowTrip=" + IsShowTrip + ";IsRequestor=" + IsRequestor + ";IsDepartment=" + IsDepartment + ";IsAuthorization=" + IsAuthorization + ";IsFlightCategory=" + IsFlightCategory + ";IsCummulativeETE=" + IsCummulativeETE + ";IsCrewFullName=" + IsCrewFullName + ";IsPax=" + IsPax + ";Footer=" + Footer + ";BlackWhiteClr=" + IsBlackWhiteClr + ";AircraftClr=" + IsAircraftClr + ";Color=" + IsColor + ";EFleet=" + EFleet;
                            break;                       

                        case "RptDay":

                            SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Day);

                             FleetCrew = SelectedFleetCrewIDs.Split('|');

                             if (!string.IsNullOrEmpty(FleetCrew[0]))
                             {
                                 EFleet = string.Join(",", FleetCrew[0].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                             } 
                             else if (!string.IsNullOrEmpty(FleetCrew[1]))
                             {
                                 ECrew = string.Join(",", FleetCrew[1].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                             }
                             else
                             {
                                 using (FlightPak.Web.PreflightService.PreflightServiceClient preflightServiceClient = new FlightPak.Web.PreflightService.PreflightServiceClient())
                                 {
                                     var fleetlist = preflightServiceClient.GetFleetInfoForFleetTree(false); // if homebaseonly option is selected, filter by homebaseID
                                     if (fleetlist != null)
                                     {
                                         var fullFleetList = fleetlist.EntityList.Where(x => x.FleetGroupID == null).ToList();
                                         //var groupedLists = list.EntityList.Where(x => x.FleetGroupID != null).ToList();
                                         var distinctfleetIds = fullFleetList.Select(x => x.FleetID).Distinct().ToList();
                                         EFleet = string.Join(",", distinctfleetIds);
                                     }
                                     var crewlist = preflightServiceClient.GetCrewInfoForCrewTree(false); // if homebaseonly option is selected, filter by homebaseID
                                     if (crewlist != null)
                                     {
                                         var fullCrewList = crewlist.EntityList.Where(x => x.CrewGroupID == null).ToList();
                                         //var groupedLists = crewlist.EntityList.Where(x => x.CrewGroupID != null).ToList();
                                         var distinctcrewIds = fullCrewList.Select(x => x.CrewID).Distinct().ToList();
                                         ECrew = string.Join(",", distinctcrewIds);
                                     }
                                 }
                             }

                             Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";BeginDate=" + BeginDate + ";NoOfDays=" + NoofWeeks + ";EndDate=" + EndDate + ";ClientCD=" + ClientCd + ";RequestorCD=" + RequestorCd + ";DepartmentCD=" + DepartmentCd + ";FlightCatagoryCD=" + FlightCategoryCd + ";DutyTypeCD=" + DutyTypeCd + ";HomeBaseCD=" + HomeBaseCd + ";IsTrip=" + IsTrip + ";IsCanceled=" + IsCancelled + ";IsHold=" + IsHold + ";IsWorkSheet=" + IsWorksheet + ";IsUnFulFilled=" + IsUnFulFilled + ";IsAllCrew=" + IsAllCrew + ";HomeBase=" + HomeBase + ";FixedWingCrew=" + FixedWingCrew + ";RotaryWingCrew=" + RotaryWingCrew + ";TimeBase=" + timeBase + ";Vendors=" + Vendors + ";SortBy=" + SortBy + ";IsActivityOnly=" + IsActiveOnly + ";IsFirstLastLeg=" + IsFirstLastLeg + ";IsCrewCalActivity=" + IsCrewCalActivity + ";IsShowTripStatus=" + IsShowTripStatus + ";IsCummulativeETE=" + IsCummulativeETE + ";IsAirport=" + IsAiport + ";Footer=" + Footer + ";BlackWhiteClr=" + IsBlackWhiteClr + ";AircraftClr=" + IsAircraftClr + ";Color=" + IsColor + ";EFleet=" + EFleet + ";ECrew=" + ECrew + ";IsTotalETE=" + IsTotalETE + ";IsETE=" + IsETE;
                            break;

                        case "RptPlanner":

                            SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Planner);

                             FleetCrew = SelectedFleetCrewIDs.Split('|');

                             if (!string.IsNullOrEmpty(FleetCrew[0]))
                             {
                                 EFleet = string.Join(",", FleetCrew[0].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                             } 
                             
                             if (!string.IsNullOrEmpty(FleetCrew[1]))
                             {
                                 ECrew = string.Join(",", FleetCrew[1].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                             }

                             if (string.IsNullOrEmpty(FleetCrew[0]) && string.IsNullOrEmpty(FleetCrew[1])) 
                             {
                                 using (FlightPak.Web.PreflightService.PreflightServiceClient preflightServiceClient = new FlightPak.Web.PreflightService.PreflightServiceClient())
                                 {
                                     var fleetlist = preflightServiceClient.GetFleetInfoForFleetTree(false); // if homebaseonly option is selected, filter by homebaseID
                                     if (fleetlist != null)
                                     {
                                         var fullFleetList = fleetlist.EntityList.Where(x => x.FleetGroupID == null).ToList();
                                         //var groupedLists = list.EntityList.Where(x => x.FleetGroupID != null).ToList();
                                         var distinctfleetIds = fullFleetList.Select(x => x.FleetID).Distinct().ToList();
                                         EFleet = string.Join(",", distinctfleetIds);
                                     }
                                     var crewlist = preflightServiceClient.GetCrewInfoForCrewTree(false); // if homebaseonly option is selected, filter by homebaseID
                                     if (crewlist != null)
                                     {
                                         var fullCrewList = crewlist.EntityList.Where(x => x.CrewGroupID == null).ToList();
                                         //var groupedLists = crewlist.EntityList.Where(x => x.CrewGroupID != null).ToList();
                                         var distinctcrewIds = fullCrewList.Select(x => x.CrewID).Distinct().ToList();
                                         ECrew = string.Join(",", distinctcrewIds);
                                     }
                                 }
                             }

                             Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";BeginDate=" + BeginDate + ";NoOfMonths=" + NoofWeeks + ";EndDate=" + EndDate + ";ClientCD=" + ClientCd + ";RequestorCD=" + RequestorCd + ";DepartmentCD=" + DepartmentCd + ";FlightCatagoryCD=" + FlightCategoryCd + ";DutyTypeCD=" + DutyTypeCd + ";HomeBaseCD=" + HomeBaseCd + ";IsTrip=" + IsTrip + ";IsCanceled=" + IsCancelled + ";IsHold=" + IsHold + ";IsWorkSheet=" + IsWorksheet + ";IsUnFulFilled=" + IsUnFulFilled + ";IsAllCrew=" + IsAllCrew + ";HomeBase=" + HomeBase + ";FixedWingCrew=" + FixedWingCrew + ";RotaryWingCrew=" + RotaryWingCrew + ";TimeBase=" + timeBase + ";Vendors=" + Vendors + ";IsBoth=" + IsBoth + ";IsActivityOnly=" + IsActiveOnly + ";Footer=" + Footer + ";BlackWhiteClr=" + IsBlackWhiteClr + ";AircraftClr=" + IsAircraftClr + ";Color=" + IsColor + ";NonReversedColor=" + IsNonReversedColor + ";EFleet=" + EFleet + ";ECrew=" + ECrew + ";DisplayDays=" + DisplayDays + ";ShowLegs=" + ShowLegs + ";ShowTails=" + ShowTails;
                            break;

                        case "RptBusinessWeek":
                            SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.BusinessWeek);

                             FleetCrew = SelectedFleetCrewIDs.Split('|');

                             if (!string.IsNullOrEmpty(FleetCrew[0]))
                             {
                                 EFleet = string.Join(",", FleetCrew[0].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                             } 
                             else if (!string.IsNullOrEmpty(FleetCrew[1]))
                             {
                                 ECrew = string.Join(",", FleetCrew[1].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                             }
                             else
                             {
                                 using (FlightPak.Web.PreflightService.PreflightServiceClient preflightServiceClient = new FlightPak.Web.PreflightService.PreflightServiceClient())
                                 {
                                     var fleetlist = preflightServiceClient.GetFleetInfoForFleetTree(false); // if homebaseonly option is selected, filter by homebaseID
                                     if (fleetlist != null)
                                     {
                                         var fullFleetList = fleetlist.EntityList.Where(x => x.FleetGroupID == null).ToList();
                                         //var groupedLists = list.EntityList.Where(x => x.FleetGroupID != null).ToList();
                                         var distinctfleetIds = fullFleetList.Select(x => x.FleetID).Distinct().ToList();
                                         EFleet = string.Join(",", distinctfleetIds);
                                     }
                                     var crewlist = preflightServiceClient.GetCrewInfoForCrewTree(false); // if homebaseonly option is selected, filter by homebaseID
                                     if (crewlist != null)
                                     {
                                         var fullCrewList = crewlist.EntityList.Where(x => x.CrewGroupID == null).ToList();
                                         //var groupedLists = crewlist.EntityList.Where(x => x.CrewGroupID != null).ToList();
                                         var distinctcrewIds = fullCrewList.Select(x => x.CrewID).Distinct().ToList();
                                         ECrew = string.Join(",", distinctcrewIds);
                                     }
                                 }
                             }

                             Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";BeginDate=" + BeginDate + ";NoOfWeeks=" + NoofWeeks + ";EndDate=" + EndDate + ";ClientCD=" + ClientCd + ";RequestorCD=" + RequestorCd + ";DepartmentCD=" + DepartmentCd + ";FlightCatagoryCD=" + FlightCategoryCd + ";DutyTypeCD=" + DutyTypeCd + ";HomeBaseCD=" + HomeBaseCd + ";IsTrip=" + IsTrip + ";IsCanceled=" + IsCancelled + ";IsHold=" + IsHold + ";IsWorkSheet=" + IsWorksheet + ";IsUnFulFilled=" + IsUnFulFilled + ";IsAllCrew=" + IsAllCrew + ";HomeBase=" + HomeBase + ";FixedWingCrew=" + FixedWingCrew + ";RotaryWingCrew=" + RotaryWingCrew + ";TimeBase=" + timeBase + ";Vendors=" + Vendors + ";IsAirport=" + IsAiport + ";IsShowTrip=" + IsShowTrip + ";IsActivityOnly=" + IsActiveOnly + ";IsFirstLastLeg=" + IsFirstLastLeg + ";IsCrew=" + IsCrew + ";IsShowTripStatus=" + IsShowTripStatus + ";IsDisplayRedEyeFlight=" + IsDisplayredFlight + ";IsFlightNo=" + IsFlightNumber + ";IsLegPurpose=" + IsLegPurpose + ";IsSeatAvailable=" + IsSeatAvailable + ";IsPaxCount=" + IsPaxCount + ";IsRequestor=" + IsRequestor + ";IsDepartment=" + IsDepartment + ";IsAuthorization=" + IsAuthorization + ";IsFlightCategory=" + IsFlightCategory + ";IsCummulativeETE=" + IsCummulativeETE + ";IsETE=" + IsETE + ";IsTotalETE=" + IsTotalETE + ";Footer=" + Footer + ";BlackWhiteClr=" + IsBlackWhiteClr + ";AircraftClr=" + IsAircraftClr + ";Color=" + IsColor + ";EFleet=" + EFleet + ";ECrew=" + ECrew;
                            break;

                        case "RptCorporate":

                            SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Corporate);

                             FleetCrew = SelectedFleetCrewIDs.Split('|');

                             if (!string.IsNullOrEmpty(FleetCrew[0]))
                             {
                                 EFleet = string.Join(",", FleetCrew[0].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                             } 
                             else if (!string.IsNullOrEmpty(FleetCrew[1]))
                             {
                                 ECrew = string.Join(",", FleetCrew[1].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                             }
                             else
                             {
                                 using (FlightPak.Web.PreflightService.PreflightServiceClient preflightServiceClient = new FlightPak.Web.PreflightService.PreflightServiceClient())
                                 {
                                     var fleetlist = preflightServiceClient.GetFleetInfoForFleetTree(false); // if homebaseonly option is selected, filter by homebaseID
                                     if (fleetlist != null)
                                     {
                                         var fullFleetList = fleetlist.EntityList.Where(x => x.FleetGroupID == null).ToList();
                                         //var groupedLists = list.EntityList.Where(x => x.FleetGroupID != null).ToList();
                                         var distinctfleetIds = fullFleetList.Select(x => x.FleetID).Distinct().ToList();
                                         EFleet = string.Join(",", distinctfleetIds);
                                     }
                                     var crewlist = preflightServiceClient.GetCrewInfoForCrewTree(false); // if homebaseonly option is selected, filter by homebaseID
                                     if (crewlist != null)
                                     {
                                         var fullCrewList = crewlist.EntityList.Where(x => x.CrewGroupID == null).ToList();
                                         //var groupedLists = crewlist.EntityList.Where(x => x.CrewGroupID != null).ToList();
                                         var distinctcrewIds = fullCrewList.Select(x => x.CrewID).Distinct().ToList();
                                         ECrew = string.Join(",", distinctcrewIds);
                                     }
                                 }
                             }

                             Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";BeginDate=" + BeginDate + ";NoOfWeeks=" + NoofWeeks + ";EndDate=" + EndDate + ";ClientCD=" + ClientCd + ";RequestorCD=" + RequestorCd + ";DepartmentCD=" + DepartmentCd + ";FlightCatagoryCD=" + FlightCategoryCd + ";DutyTypeCD=" + DutyTypeCd + ";HomeBaseCD=" + HomeBaseCd + ";IsTrip=" + IsTrip + ";IsCanceled=" + IsCancelled + ";IsHold=" + IsHold + ";IsWorkSheet=" + IsWorksheet + ";IsUnFulFilled=" + IsUnFulFilled + ";IsAllCrew=" + IsAllCrew + ";HomeBase=" + HomeBase + ";FixedWingCrew=" + FixedWingCrew + ";RotaryWingCrew=" + RotaryWingCrew + ";TimeBase=" + timeBase + ";Vendors=" + Vendors + ";IsBoth=" + IsBoth + ";IsAirport=" + IsAiport + ";IsActivityOnly=" + IsActiveOnly + ";IsShowTripStatus=" + IsShowTripStatus + ";Footer=" + Footer + ";BlackWhiteClr=" + IsBlackWhiteClr + ";AircraftClr=" + IsAircraftClr + ";Color=" + IsColor + ";EFleet=" + EFleet + ";ECrew=" + ECrew + ";DisplayRONPax=" + IsDisplayRONPax;
                            break;

                        default:

                            SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyFleet);

                             FleetCrew = SelectedFleetCrewIDs.Split('|');

                             if (!string.IsNullOrEmpty(FleetCrew[0]))
                             {
                                 EFleet = string.Join(",", FleetCrew[0].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                             }                             
                             else
                             {
                                 using (FlightPak.Web.PreflightService.PreflightServiceClient preflightServiceClient = new FlightPak.Web.PreflightService.PreflightServiceClient())
                                 {
                                     var fleetlist = preflightServiceClient.GetFleetInfoForFleetTree(false); // if homebaseonly option is selected, filter by homebaseID
                                     if (fleetlist != null)
                                     {
                                         var fullFleetList = fleetlist.EntityList.Where(x => x.FleetGroupID == null).ToList();
                                         //var groupedLists = list.EntityList.Where(x => x.FleetGroupID != null).ToList();
                                         var distinctfleetIds = fullFleetList.Select(x => x.FleetID).Distinct().ToList();
                                         EFleet = string.Join(",", distinctfleetIds);
                                     }                                     
                                 }                          
                             }

                            Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";BeginDate=" + BeginDate + ";NoOfWeeks=" + NoofWeeks + ";EndDate=" + EndDate + ";ClientCD=" + ClientCd + ";RequestorCD=" + RequestorCd + ";DepartmentCD=" + DepartmentCd + ";FlightCatagoryCD=" + FlightCategoryCd + ";DutyTypeCD=" + DutyTypeCd + ";HomeBaseCD=" + HomeBaseCd + ";IsTrip=" + IsTrip + ";IsCanceled=" + IsCancelled + ";IsHold=" + IsHold + ";IsWorkSheet=" + IsWorksheet + ";IsUnFulFilled=" + IsUnFulFilled + ";IsAllCrew=" + IsAllCrew + ";HomeBase=" + HomeBase + ";FixedWingCrew=" + FixedWingCrew + ";RotaryWingCrew=" + RotaryWingCrew + ";TimeBase=" + timeBase + ";Vendors=" + Vendors + ";IsAirport=" + IsAiport + ";IsShowTrip=" + IsShowTrip + ";IsActivityOnly=" + IsActiveOnly + ";IsFirstLastLeg=" + IsFirstLastLeg + ";IsCrew=" + IsCrew + ";IsShowTripStatus=" + IsShowTripStatus + ";IsDisplayRedEyeFlight=" + IsDisplayredFlight + ";IsFlightNo=" + IsFlightNumber + ";IsLegPurpose=" + IsLegPurpose + ";IsSeatAvailable=" + IsSeatAvailable + ";IsPaxCount=" + IsPaxCount + ";IsRequestor=" + IsRequestor + ";IsDepartment=" + IsDepartment + ";IsAuthorization=" + IsAuthorization + ";IsFlightCategory=" + IsFlightCategory + ";IsCummulativeETE=" + IsCummulativeETE + ";Footer=" + Footer + ";BlackWhiteClr=" + IsBlackWhiteClr + ";AircraftClr=" + IsAircraftClr + ";Color=" + IsColor + ";EFleet=" + EFleet;
                            
                            break;
                    }

                    
                   /* if (ReportString == "RptWeeklyFleet")
                    {
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";BeginDate=" + BeginDate + ";NoOfWeeks=" + NoofWeeks + ";EndDate=" + EndDate + ";ClientCD=" + ClientCd + ";RequestorCD=" + RequestorCd + ";DepartmentCD=" + DepartmentCd + ";FlightCatagoryCD=" + FlightCategoryCd + ";DutyTypeCD=" + DutyTypeCd + ";HomeBaseCD=" + HomeBaseCd + ";IsTrip=" + IsTrip + ";IsCanceled=" + IsCancelled + ";IsHold=" + IsHold + ";IsWorkSheet=" + IsWorksheet + ";IsUnFulFilled=" + IsUnFulFilled + ";IsAllCrew=" + IsAllCrew + ";HomeBase=" + HomeBase + ";FixedWingCrew=" + FixedWingCrew + ";RotaryWingCrew=" + RotaryWingCrew + ";TimeBase=" + timeBase + ";Vendors=" + Vendors + ";IsAirport=" + IsAiport + ";IsShowTrip=" + IsShowTrip + ";IsActivityOnly=" + IsActiveOnly + ";IsFirstLastLeg=" + IsFirstLastLeg + ";IsCrew=" + IsCrew + ";IsShowTripStatus=" + IsShowTripStatus + ";IsDisplayRedEyeFlight=" + IsDisplayredFlight + ";IsFlightNo=" + IsFlightNumber + ";IsLegPurpose=" + IsLegPurpose + ";IsSeatAvailable=" + IsSeatAvailable + ";IsPaxCount=" + IsPaxCount + ";IsRequestor=" + IsRequestor + ";IsDepartment=" + IsDepartment + ";IsAuthorization=" + IsAuthorization + ";IsFlightCategory=" + IsFlightCategory + ";IsCummulativeETE=" + IsCummulativeETE + ";Footer=" + Footer + ";BlackWhiteClr=" + IsBlackWhiteClr + ";AircraftClr=" + IsAircraftClr + ";Color=" + IsColor + ";EFleet=" + EFleet;
                    }
                    else if (ReportString == "RptWeeklyCrew" || ReportString == "RptWeeklyFleet" || ReportString == "RptWeeklyDetail" || ReportString == "RptBusinessWeek")
                    {
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";BeginDate=" + BeginDate + ";NoOfWeeks=" + NoofWeeks + ";EndDate=" + EndDate + ";ClientCD=" + ClientCd + ";RequestorCD=" + RequestorCd + ";DepartmentCD=" + DepartmentCd + ";FlightCatagoryCD=" + FlightCategoryCd + ";DutyTypeCD=" + DutyTypeCd + ";HomeBaseCD=" + HomeBaseCd + ";IsTrip=" + IsTrip + ";IsCanceled=" + IsCancelled + ";IsHold=" + IsHold + ";IsWorkSheet=" + IsWorksheet + ";IsUnFulFilled=" + IsUnFulFilled + ";IsAllCrew=" + IsAllCrew + ";HomeBase=" + HomeBase + ";FixedWingCrew=" + FixedWingCrew + ";RotaryWingCrew=" + RotaryWingCrew + ";TimeBase=" + timeBase + ";Vendors=" + Vendors + ";IsAirport=" + IsAiport + ";IsShowTrip=" + IsShowTrip + ";IsActivityOnly=" + IsActiveOnly + ";IsFirstLastLeg=" + IsFirstLastLeg + ";IsCrew=" + IsCrew + ";IsShowTripStatus=" + IsShowTripStatus + ";IsDisplayRedEyeFlight=" + IsDisplayredFlight + ";IsFlightNo=" + IsFlightNumber + ";IsLegPurpose=" + IsLegPurpose + ";IsSeatAvailable=" + IsSeatAvailable + ";IsPaxCount=" + IsPaxCount + ";IsRequestor=" + IsRequestor + ";IsDepartment=" + IsDepartment + ";IsAuthorization=" + IsAuthorization + ";IsFlightCategory=" + IsFlightCategory + ";IsCummulativeETE=" + IsCummulativeETE + ";Footer=" + Footer;
                    }
                    if (ReportString == "RptWeeklyDetail")
                    {
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";BeginDate=" + BeginDate + ";NoOfWeeks=" + NoofWeeks + ";EndDate=" + EndDate + ";ClientCD=" + ClientCd + ";RequestorCD=" + RequestorCd + ";DepartmentCD=" + DepartmentCd + ";FlightCatagoryCD=" + FlightCategoryCd + ";DutyTypeCD=" + DutyTypeCd + ";HomeBaseCD=" + HomeBaseCd + ";IsTrip=" + IsTrip + ";IsCanceled=" + IsCancelled + ";IsHold=" + IsHold + ";IsWorkSheet=" + IsWorksheet + ";IsUnFulFilled=" + IsUnFulFilled + ";IsAllCrew=" + IsAllCrew + ";HomeBase=" + HomeBase + ";FixedWingCrew=" + FixedWingCrew + ";RotaryWingCrew=" + RotaryWingCrew + ";TimeBase=" + timeBase + ";Vendors=" + Vendors + ";SortBy=" + SortBy + ";IsAirport=" + IsAiport + ";IsActivityOnly=" + IsActiveOnly + ";IsFirstLastLeg=" + IsFirstLastLeg + ";IsCrew=" + IsCrew + ";IsShowTripStatus=" + IsShowTripStatus + ";IsFlightNo=" + IsFlightNumber + ";IsLegPurpose=" + IsLegPurpose + ";IsSeatAvailable=" + IsSeatAvailable + ";IsPaxCount=" + IsPaxCount + ";IsPaxFullName=" + IsPaxFullName + ";IsShowTrip=" + IsShowTrip + ";IsRequestor=" + IsRequestor + ";IsDepartment=" + IsDepartment + ";IsAuthorization=" + IsAuthorization + ";IsFlightCategory=" + IsFlightCategory + ";IsCummulativeETE=" + IsCummulativeETE + ";IsCrewFullName=" + IsCrewFullName + ";IsPax=" + IsPax + ";Footer=" + Footer;
                    }
                    else if (ReportString == "RptPlanner")
                    {
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";BeginDate=" + BeginDate + ";NoOfMonths=" + NoofWeeks + ";EndDate=" + EndDate + ";ClientCD=" + ClientCd + ";RequestorCD=" + RequestorCd + ";DepartmentCD=" + DepartmentCd + ";FlightCatagoryCD=" + FlightCategoryCd + ";DutyTypeCD=" + DutyTypeCd + ";HomeBaseCD=" + HomeBaseCd + ";IsTrip=" + IsTrip + ";IsCanceled=" + IsCancelled + ";IsHold=" + IsHold + ";IsWorkSheet=" + IsWorksheet + ";IsUnFulFilled=" + IsUnFulFilled + ";IsAllCrew=" + IsAllCrew + ";HomeBase=" + HomeBase + ";FixedWingCrew=" + FixedWingCrew + ";RotaryWingCrew=" + RotaryWingCrew + ";TimeBase=" + timeBase + ";Vendors=" + Vendors + ";IsBoth=" + IsBoth + ";IsActivityOnly=" + IsActiveOnly + ";Footer=" + Footer;
                    }
                    if (ReportString == "RptDay")
                    {
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";BeginDate=" + BeginDate + ";NoOfDays=" + NoofWeeks + ";EndDate=" + EndDate + ";ClientCD=" + ClientCd + ";RequestorCD=" + RequestorCd + ";DepartmentCD=" + DepartmentCd + ";FlightCatagoryCD=" + FlightCategoryCd + ";DutyTypeCD=" + DutyTypeCd + ";HomeBaseCD=" + HomeBaseCd + ";IsTrip=" + IsTrip + ";IsCanceled=" + IsCancelled + ";IsHold=" + IsHold + ";IsWorkSheet=" + IsWorksheet + ";IsUnFulFilled=" + IsUnFulFilled + ";IsAllCrew=" + IsAllCrew + ";HomeBase=" + HomeBase + ";FixedWingCrew=" + FixedWingCrew + ";RotaryWingCrew=" + RotaryWingCrew + ";TimeBase=" + timeBase + ";Vendors=" + Vendors + ";SortBy=" + SortBy + ";IsActivityOnly=" + IsActiveOnly + ";IsFirstLastLeg=" + IsFirstLastLeg + ";IsCrewCalActivity=" + IsCrewCalActivity + ";IsShowTripStatus=" + IsShowTripStatus + ";IsCummulativeETE=" + IsCummulativeETE + ";IsAirport=" + IsAiport + ";Footer=" + Footer;
                    }
                    if (ReportString == "RptCorporate")
                    {
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";BeginDate=" + BeginDate + ";NoOfWeeks=" + NoofWeeks + ";EndDate=" + EndDate + ";ClientCD=" + ClientCd + ";RequestorCD=" + RequestorCd + ";DepartmentCD=" + DepartmentCd + ";FlightCatagoryCD=" + FlightCategoryCd + ";DutyTypeCD=" + DutyTypeCd + ";HomeBaseCD=" + HomeBaseCd + ";IsTrip=" + IsTrip + ";IsCanceled=" + IsCancelled + ";IsHold=" + IsHold + ";IsWorkSheet=" + IsWorksheet + ";IsUnFulFilled=" + IsUnFulFilled + ";IsAllCrew=" + IsAllCrew + ";HomeBase=" + HomeBase + ";FixedWingCrew=" + FixedWingCrew + ";RotaryWingCrew=" + RotaryWingCrew + ";TimeBase=" + timeBase + ";Vendors=" + Vendors + ";IsBoth=" + IsBoth + ";IsAirport=" + IsAiport + ";IsActivityOnly=" + IsActiveOnly + ";IsShowTripStatus=" + IsShowTripStatus + ";Footer=" + Footer;
                    }
                    if (ReportString == "RptWeekly")
                    {
                        Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";BeginDate=" + BeginDate + ";NoOfWeeks=" + NoofWeeks + ";EndDate=" + EndDate + ";ClientCD=" + ClientCd + ";RequestorCD=" + RequestorCd + ";DepartmentCD=" + DepartmentCd + ";FlightCatagoryCD=" + FlightCategoryCd + ";DutyTypeCD=" + DutyTypeCd + ";HomeBaseCD=" + HomeBaseCd + ";IsTrip=" + IsTrip + ";IsCanceled=" + IsCancelled + ";IsHold=" + IsHold + ";IsWorkSheet=" + IsWorksheet + ";IsUnFulFilled=" + IsUnFulFilled + ";IsAllCrew=" + IsAllCrew + ";HomeBase=" + HomeBase + ";FixedWingCrew=" + FixedWingCrew + ";RotaryWingCrew=" + RotaryWingCrew + ";TimeBase=" + timeBase + ";Vendors=" + Vendors + ";IsAirport=" + IsAiport + ";IsShowTrip=" + IsShowTrip + ";IsActivityOnly=" + IsActiveOnly + ";IsFirstLastLeg=" + IsFirstLastLeg + ";IsCrew=" + IsCrew + ";IsShowTripStatus=" + IsShowTripStatus + ";IsDisplayRedEyeFlight=" + IsDisplayredFlight + ";IsFlightNo=" + IsFlightNumber + ";IsLegPurpose=" + IsLegPurpose + ";IsSeatAvailable=" + IsSeatAvailable + ";IsPaxCount=" + IsPaxCount + ";IsRequestor=" + IsRequestor + ";IsDepartment=" + IsDepartment + ";IsAuthorization=" + IsAuthorization + ";IsFlightCategory=" + IsFlightCategory + ";IsCummulativeETE=" + IsCummulativeETE + ";Footer=" + Footer;
                    }*/
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "loadreport", "LoadReport()", true);
                    //lbScript.Visible = true;
                    //lbScript.Text = "<script type='text/javascript'>Close()</" + "script>";
                }
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag

                    bool Isvalid = true;
                    var filePath = Server.MapPath(urlBase + filename);
                    XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                    var xmlReaderObject = xmlReader.XmlCleanReader();
                    if (xmlReaderObject != null)
                    {

                        XPathDocument ReportFieldDoc = new XPathDocument(xmlReaderObject);
                        string Param = string.Empty;
                        XPathNodeIterator itr = ReportFieldDoc.CreateNavigator().Select("//searchfield");
                        StringBuilder sb = new System.Text.StringBuilder();

                        //Flaw ID :167 - US 139
                        //XmlDocument doc = new XmlDocument();
                        //doc.Load(Server.MapPath(urlBase + filename));

                        while (itr.MoveNext())
                        {
                            string controlName = itr.Current.GetAttribute("name", "");
                            sb.Append(controlName);
                            sb.Append("=");

                            Control ctrlHolder = FindControlRecursive(Report, "lbcv" + controlName);
                            if (ctrlHolder != null)
                            {
                                if (ctrlHolder is Label)
                                {
                                    if (!string.IsNullOrEmpty(((Label)ctrlHolder).Text))
                                        Isvalid = false;
                                }
                            }
                        }

                        if (Isvalid)
                        {
                            if (Page.IsValid)
                            {
                                ExportPanal.Visible = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnExportFileFormat_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag

                    if (Page.IsValid)
                    {
                        switch (ddlFormat.SelectedValue)
                        {
                            case "MHTML":
                                Wizard1.Visible = true;
                                Wizard1.MoveTo(WizardStep1);
                                ProcessColumns();
                                ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[1])).DataSource = dicColumnList;
                                ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[1])).DataBind();
                                ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[1])).DataSource = dicColumnList;
                                ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[1])).DataBind();
                                dicColumnList.Clear();
                                ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[3])).DataSource = dicColumnList;
                                ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[3])).DataBind();
                                ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[3])).DataSource = dicColumnList;
                                ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[3])).DataBind();
                                break;
                            case "Excel":
                                string[] strArr = GetReportTitle().Split(',');
                                if (strArr.Count() > 0)
                                {
                                    Session["REPORT"] = Convert.ToString(strArr[1] + "Export");
                                }
                                Session["FORMAT"] = "EXCEL";
                                Session["PARAMETER"] = ProcessHtmlExcelParam();
                                Response.Redirect("ReportRenderView.aspx");
                                break;
                            default:

                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnCostFileFormat_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag

                    ExportPanal.Visible = false;
                    Wizard1.Visible = false;
                    lblError.Text = "";
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        private string GetReportTitle()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string RptName = string.Empty;
               
                XmlDocument doc = new XmlDocument();
                var filePath = Server.MapPath(urlBase + filename);
                XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                var xmlReaderObject = xmlReader.XmlCleanReader();
                if (xmlReaderObject != null)
                {
                    doc.Load(xmlReaderObject);
                    XmlNodeList elemList = doc.GetElementsByTagName("Report");

                    for (int i = 0; i < elemList.Count; i++)
                    {
                        RptName = elemList[i].Attributes["displayname"].Value + "," + elemList[i].Attributes["name"].Value;
                        break;
                    }

                    return RptName;
                }
                else
                {
                    return "";
                }
            }
        }

        private string ProcessHtmlExcelParam()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string PageFGcolor = ColorTranslator.ToHtml(ForegroundColor.SelectedColor);
                string PageBGColor = ColorTranslator.ToHtml(BackgroundColor.SelectedColor);

                string TableBorderColors = ColorTranslator.ToHtml(TableBorderColor.SelectedColor);
                string TableFGColor = ColorTranslator.ToHtml(TableForegroundColor.SelectedColor);
                string TableBGColor = ColorTranslator.ToHtml(TableBackgroundColor.SelectedColor);
                StringBuilder strBuildparm = new StringBuilder();

                string Dbcolname = string.Empty;
                string AliesColname = string.Empty;
                string SortColname = string.Empty;


                switch (ddlFormat.SelectedValue)
                {

                    case "MHTML":

                        foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                        {
                            Dbcolname = Dbcolname + SelectedItem.Value + ",";
                        }
                        if (Dbcolname != string.Empty)
                        {
                            Dbcolname = Dbcolname.Substring(0, Dbcolname.Length - 1);
                        }
                        foreach (RadListBoxItem SelectedItem in HeaderBySource.Items)
                        {
                            AliesColname = AliesColname + SelectedItem.Text + "|";
                        }
                        AliesColname = AliesColname.Substring(0, AliesColname.Length - 1);

                        foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxDestination.Items)
                        {
                            SortColname = SortColname + SelectedItem.Value + ",";
                        }
                        if (SortColname != string.Empty)
                        {
                            SortColname = SortColname.Substring(0, SortColname.Length - 1);
                        }

                        strBuildparm.Append(ProcessResults());
                        strBuildparm.Append(";SelectedCols=" + Dbcolname);
                        strBuildparm.Append(";DisplayCols=" + AliesColname);
                        strBuildparm.Append(";SortCols=" + SortColname);
                        strBuildparm.Append(";Color=" + PageBGColor + "," + PageFGcolor + "," + TableFGColor + "," + TableBorderColors + "," + TableBGColor);
                        strBuildparm.Append(";ReportTitle=" + txtTitle.Text);

                        FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                        using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            FPPrincipal identityname = (FPPrincipal)Session[Session.SessionID];
                            var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == identity.Identity._customerID && x.HomebaseID == identity.Identity._homeBaseId).ToList();


                            if (chkHeader.Checked)
                            {
                                strBuildparm.Append(";HtmlHeader=" + objCompany[0].HTMLHeader);
                            }
                            if (chkFooter.Checked)
                            {
                                strBuildparm.Append(";HtmlFooter=" + objCompany[0].HTMLFooter);
                            }
                            if (chkBackground.Checked)
                            {
                                strBuildparm.Append(";HtmlBG=" + objCompany[0].HTMLBackgroun);
                            }
                        }
                        break;

                    case "Excel":
                        //    //for Excel file  Reading from RadListBoxSource
                        //   foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxSource.Items)
                        //    {
                        //        Dbcolname = Dbcolname + SelectedItem.Value + ",";
                        //    }
                        //    if (Dbcolname != string.Empty)
                        //    {
                        //        Dbcolname = Dbcolname.Substring(0, Dbcolname.Length - 1);
                        //    }
                        //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxSource.Items)
                        //    {
                        //        AliesColname=AliesColname+SelectedItem.Text+"|";
                        //    }
                        //if (AliesColname != string.Empty)
                        //{
                        //    AliesColname = AliesColname.Substring(0, AliesColname.Length - 1);
                        //}


                        strBuildparm.Append(ProcessResults());
                        //   strBuildparm.Append(";SelectedCols="+Dbcolname);
                        //    strBuildparm.Append(";DisplayCols="+AliesColname);
                        //strBuildparm.Append(";Color=," + PageFGcolor + "," + TableFGColor + "," + TableBorderColors + "," + TableBGColor); 

                        break;

                }

                return strBuildparm.ToString();

                //FlightPak.Web.ReportRenderService.ReportRenderClient RptClient = new ReportRenderService.ReportRenderClient();
                //DataSet dsRpt = RptClient.GetReportHTMLTable(Dbcolname, AliesColname, GetSPName(filename), ProcessResults());
                //  return GetDataTableAsHTML(dsRpt.Tables[0], txtTitle.Text, PageFGcolor, PageBGColor, TableBorderColor, TableFGColor, TableBGColor);
            }
        }

        private void SelectTab()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Session["TabSelect"] = "PreFlight";
                pnlReportTab.Items.Clear();
                string xmltab = string.Empty;
                Session["TabSelect1"] = "Calender";
                xmltab = "CalenderMainTab.xml";
                //LoadTabs(pnlReportTab, urlBase + xmltab);
                //Response.Redirect("CalenderReports.aspx?xmlFilename=PREScheduleCalendarByWeek.xml");
                ExpandPanel(pnlReportTab, xmltab, true);
            }
        }

        protected void LoadTabs(RadPanelBar Rpb, string XmlFilePath)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Rpb, XmlFilePath))
            {
                string attrVal = "";
                XmlDocument doc = new XmlDocument();
                doc.Load(Server.MapPath(XmlFilePath));

                XmlNodeList elemList = doc.GetElementsByTagName("catagory");
                for (int i = 0; i < elemList.Count; i++)
                {
                    attrVal = attrVal + elemList[i].Attributes["text"].Value + ",";
                }
                string[] strval = attrVal.Split(',');
                foreach (string str in strval)
                {
                    Telerik.Web.UI.RadPanelItem rpi = new Telerik.Web.UI.RadPanelItem(str);
                    XmlNodeList xnList = doc.SelectNodes("/catagorys/catagory[@text='" + str + "']/screen[@name!='']");

                    //if(str == "Administration")
                    //rpi.Visible = UserPrincipal.Identity._isSysAdmin;

                    if (xnList.Count > 0)
                    {
                        int ival = 0;
                        foreach (XmlNode child in xnList)
                        {
                            //Added UMPermissionRole for implementing security 
                            //if (IsAuthorized("View" + child.Attributes["UMPermissionRole"].Value))
                            //{
                                rpi.Items.Add(new Telerik.Web.UI.RadPanelItem(child.Attributes["name"].Value, child.Attributes["url"].Value));
                                rpi.Font.Bold = true;
                                rpi.Items[ival].Enabled = (child.Attributes["url"].Value == string.Empty ? false : true);
                                if (!rpi.Enabled)
                                {
                                    rpi.Items[ival].ForeColor = Color.Gray;
                                }
                                ival++;
                            //}
                            //else if (str == "Administration" && UserPrincipal.Identity._isSysAdmin)
                            //{
                            //    rpi.Items.Add(new Telerik.Web.UI.RadPanelItem(child.Attributes["name"].Value, child.Attributes["url"].Value));
                            //    rpi.Font.Bold = true;
                            //    rpi.Items[ival].Enabled = (child.Attributes["url"].Value == string.Empty ? false : true);
                            //    if (!rpi.Enabled)
                            //    {
                            //        rpi.Items[ival].ForeColor = Color.Gray;
                            //    }
                            //    ival++;
                            //}
                            //else if (child.Attributes["UMPermissionRole"].Value == "TripsheetReportWriter")
                            //{
                            //    rpi.Items.Add(new Telerik.Web.UI.RadPanelItem(child.Attributes["name"].Value, child.Attributes["url"].Value));
                            //    rpi.Font.Bold = true;
                            //    rpi.Items[ival].Enabled = (child.Attributes["url"].Value == string.Empty ? false : true);
                            //    ival++;
                            //}
                        }

                        Rpb.Items.Add(rpi);
                    }
                    else if (str != "")
                    {
                        XmlNodeList elemListval = doc.GetElementsByTagName("catagory");
                        for (int ival = 0; ival < elemListval.Count; ival++)
                        {
                            Telerik.Web.UI.RadPanelItem rpia = new Telerik.Web.UI.RadPanelItem(elemListval[ival].Attributes["text"].Value, elemListval[ival].Attributes["url"].Value);
                            Rpb.Items.Add(rpia);
                        }
                        break;

                    }
                }
            }

        }

        private void ExpandPanel(RadPanelBar panelBar, string TabXmlname, bool isExpand)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(panelBar, TabXmlname, isExpand))
            {
                string Selectedvalue = Convert.ToString(Request.QueryString["xmlFilename"]);
                string attrVal = "";
                string TitleTab = string.Empty;
                XDocument doc = XDocument.Load(Server.MapPath(urlBase + TabXmlname));

                if (Request.QueryString["xmlFilename"] != null)
                {

                    var items = doc.Descendants("screen")
                                  .Where(c => c.Attribute("url").Value.Contains(Convert.ToString(Request.QueryString["xmlFilename"])))
                                   .Select(c => new { x = c.Value, y = c.Parent.Attribute("text").Value, z = c.Attribute("name").Value })
                                   .ToList();

                    if (items != null)
                    {
                        if (items.Count != 0)
                        {

                            RadPanelItem selectedItem = panelBar.FindItemByText(items[0].y);

                            if (selectedItem != null)
                            {
                                if (selectedItem.Items.Count > 0)
                                {

                                    RadPanelItem selectedvalue = selectedItem.Items.FindItemByText(items[0].z);
                                    selectedvalue.Selected = true;
                                    selectedItem.Expanded = true;

                                }
                                else
                                {
                                    selectedItem.Selected = true;
                                    while ((selectedItem != null) &&
                                           (selectedItem.Parent.GetType() == typeof(RadPanelItem)))
                                    {
                                        selectedItem = (RadPanelItem)selectedItem.Parent;
                                        selectedItem.Expanded = true;
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }

        private void ProcessColumns()
        {
            dicColumnList = new Dictionary<string, string>();
            string xmlfilename = string.Empty;
            Session["xmlfilename"] = Convert.ToString(Request.QueryString["xmlFilename"]);
            if (Session["xmlfilename"] != null)
            {
                xmlfilename = (Session["xmlfilename"] != null) ? Convert.ToString(Request.QueryString["xmlFilename"]) : string.Empty;
                //if (xmlfilenametrip != string.Empty)
                //{
                //    xmlfilename = xmlfilenametrip;
                //    Session["xmlfilename"] = xmlfilename;
                //}
            }
            else if (filename != null)
            {
                xmlfilename = filename;
            }

           
             var filePath = Server.MapPath(urlBase + xmlfilename);
                    XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                    var xmlReaderObject = xmlReader.XmlCleanReader();
                    if (xmlReaderObject != null)
                    {

                        XPathDocument ReportFieldDoc = new XPathDocument(xmlReaderObject);
                        XPathNodeIterator itr = ReportFieldDoc.CreateNavigator().Select("//Column");
                        StringBuilder sb = new System.Text.StringBuilder();

                        while (itr.MoveNext())
                        {
                            dicColumnList.Add(itr.Current.GetAttribute("name", "").ToString(), itr.Current.GetAttribute("displayname", "").ToString());
                        }
                    }
        }

        private string ProcessResults()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                var filePath = Server.MapPath(urlBase + filename);
                XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                var xmlReaderObject = xmlReader.XmlCleanReader();
                if (xmlReaderObject != null)
                {

                    XPathDocument ReportFieldDoc = new XPathDocument(xmlReaderObject);
                    string Param = string.Empty;
                    XPathNodeIterator itr = ReportFieldDoc.CreateNavigator().Select("//searchfield");
                    StringBuilder sb = new System.Text.StringBuilder();

                    bool ddlmth = false;

                    //--- this is the solution Implementing--

                    XmlDocument doc = new XmlDocument();
                    XmlReaderResolver xmlReader2 = new XmlReaderResolver(filePath);
                    var xmlReaderObject2 = xmlReader.XmlCleanReader();
                    if (xmlReaderObject2 != null)
                    {
                        doc.Load(xmlReaderObject2);

                        //-----------------

                        while (itr.MoveNext())
                        {
                            string controlName = itr.Current.GetAttribute("name", "");
                            sb.Append(controlName);
                            sb.Append("=");

                            Control ctrlHolder = FindControlRecursive(Report, controlName);

                            if (ctrlHolder is TextBox)
                            {
                                Session[Session["TabSelect1"] + ctrlHolder.ID] = ((TextBox)ctrlHolder).Text;
                                sb.Append(((TextBox)ctrlHolder).Text);
                            }

                            if (ctrlHolder is RadioButtonList)
                            {
                                if (((RadioButtonList)ctrlHolder).SelectedItem != null)
                                {
                                    Session[Session["TabSelect1"] + ctrlHolder.ID] = ((RadioButtonList)ctrlHolder).SelectedItem.Value;
                                    sb.Append(((RadioButtonList)ctrlHolder).SelectedItem.Value);
                                }
                            }

                            if (ctrlHolder is CheckBox)
                            {
                                Session[Session["TabSelect1"] + ctrlHolder.ID] = ((CheckBox)ctrlHolder).Checked;
                                if (((CheckBox)ctrlHolder).Checked == true)
                                {
                                    sb.Append("True");
                                }
                                else
                                {
                                    sb.Append("False");
                                }
                            }


                            if (ctrlHolder is HiddenField)
                            {
                                if (((HiddenField)ctrlHolder).Value != null)
                                {
                                    Session[Session["TabSelect1"] + ctrlHolder.ID] = ((HiddenField)ctrlHolder).Value;
                                    sb.Append(((HiddenField)ctrlHolder).Value);
                                }
                            }

                            if (ctrlHolder is DropDownList)
                            {
                                if (ctrlHolder.ClientID.IndexOf("MonthFrom") <= 0)
                                {
                                    //DropDownList ddlmonth = (DropDownList)innerControl.FindControl("MonthFrom");
                                    DropDownList ddlmonth = (DropDownList)ctrlHolder;
                                    if (ddlmonth.SelectedItem.Value != "")
                                    {
                                        sb.Append(ddlmonth.SelectedItem.Text);
                                        Session[Session["TabSelect1"] + ctrlHolder.ID + "ddlMonth"] = ddlmonth.SelectedItem.Value;
                                    }
                                }
                            }

                            if (ctrlHolder is UserControl)
                            {
                                if (ctrlHolder.ClientID.IndexOf("MonthFrom") <= 0)
                                {
                                    /*TextBox tbDateOfBirth = (TextBox)ctrlHolder.FindControl("tbDate");
                                    if (tbDateOfBirth != null)
                                    {
                                        if (tbDateOfBirth.Text.Trim().ToString() != string.Empty)
                                        {
                                            //sb.Append(DateTime.ParseExact(tbDateOfBirth.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture));
                                            sb.Append(Convert.ToDateTime(tbDateOfBirth.Text.Trim(), CultureInfo.InvariantCulture).ToShortDateString());
                                        }
                                    }*/

                                    TextBox tbDateOfBirth = (TextBox)ctrlHolder.FindControl("tbDate");
                                    if (DateFormat != null)
                                    {
                                        //if ((tbDateOfBirth != null) || (tbDateOfBirth.Text.Trim().ToString() != string.Empty))
                                        //{
                                        //    //sb.Append(DateTime.ParseExact(tbDateOfBirth.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture));
                                        //    sb.Append(String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(tbDateOfBirth.Text.Trim())));
                                        //    sb.Append(Convert.ToDateTime(tbDateOfBirth.Text.Trim()));
                                        //}
                                        if (tbDateOfBirth != null)
                                        {
                                            if (tbDateOfBirth.Text.Trim().ToString() != "")
                                            {
                                                sb.Append(FormatDate(tbDateOfBirth.Text.Trim(), DateFormat));
                                                Session[Session["TabSelect1"] + ctrlHolder.ID + "tbDate"] = tbDateOfBirth.Text.Trim();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (tbDateOfBirth != null)
                                        {
                                            if (tbDateOfBirth.Text.Trim().ToString() != "")
                                            {
                                                sb.Append(tbDateOfBirth.Text.Trim());
                                                Session[Session["TabSelect1"] + ctrlHolder.ID + "tbDate"] = tbDateOfBirth.Text.Trim();
                                            }
                                        }
                                        //if ((tbDateOfBirth != null) || (tbDateOfBirth.Text.Trim().ToString() != string.Empty))
                                        //{
                                        //    //sb.Append(DateTime.ParseExact(tbDateOfBirth.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture));
                                        //    sb.Append(tbDateOfBirth.Text.Trim());
                                        //}
                                    }
                                    //this is to identify month and year
                                    XmlNodeList xnList = doc.SelectNodes("/Report/Controls/searchfield[@type='monthyear']");
                                    if (xnList.Count == 2) //Month and year
                                    {
                                        DropDownList ddlmonth = (DropDownList)ctrlHolder.FindControl("ddlMonth");
                                        if (ddlmonth != null && ddlmth == false)
                                        {
                                            Session[Session["TabSelect1"] + ctrlHolder.ID + "ddlMonth"] = ddlmonth.SelectedItem.Value;
                                            sb.Append(ddlmonth.SelectedItem.Text);
                                            ddlmth = true;
                                            sb.Append(";");
                                            continue;
                                        }
                                        DropDownList ddlyear = (DropDownList)ctrlHolder.FindControl("ddlyear");
                                        if (ddlyear != null)
                                        {
                                            Session[Session["TabSelect1"] + ctrlHolder.ID + "ddlyear"] = ddlyear.SelectedItem.Value;
                                            sb.Append(ddlyear.SelectedItem.Text);
                                        }
                                    }
                                    else if (xnList.Count == 1) //year
                                    {

                                        DropDownList ddlyear = (DropDownList)ctrlHolder.FindControl("ddlyear");
                                        if (ddlyear != null)
                                        {
                                            Session[Session["TabSelect1"] + ctrlHolder.ID + "ddlyear"] = ddlyear.SelectedItem.Value;
                                            sb.Append(ddlyear.SelectedItem.Text);
                                        }
                                    }
                                    ///////////////////////////////////////////////////////////////////////////////////////////

                                }
                                else
                                {
                                    RadMonthYearPicker monthpicker = (RadMonthYearPicker)ctrlHolder.FindControl("tlkMonthView");
                                    if (!string.IsNullOrEmpty(monthpicker.SelectedDate.ToString().Trim()))
                                    {
                                        Session[Session["TabSelect1"] + ctrlHolder.ID + "tlkMonthView"] = monthpicker.SelectedDate.ToString();
                                        sb.Append(monthpicker.SelectedDate.ToString());
                                    }

                                }

                            }
                            sb.Append(";");
                            Param = Convert.ToString(sb);

                            if (Param != string.Empty)
                            {

                                Param = Param.Substring(0, Param.Length - 1);
                            }
                            Param = Param + ";UserCD=" + UserPrincipal.Identity._name;
                        }

                        if (Session["SCUserSettings"] != null)
                        {
                            int Vendors = Convert.ToInt32("1");

                            var filterAndDisplaySettings = (AdvancedFilterSettings)Session["SCUserSettings"];

                            var serializedXml = XMLSerializationHelper.Serialize<AdvancedFilterSettings>(filterAndDisplaySettings);
                            UCFilterCriteria FilterCriterias = new UCFilterCriteria();

                            var settings = new AdvancedFilterSettings();

                            if (!string.IsNullOrWhiteSpace(serializedXml)) // existing user
                            {
                                settings = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(serializedXml);
                            }

                            //Set Filter criteria

                            if (settings.Filters == null)
                                settings.Filters = new FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria();

                            var filters = settings.Filters;

                            var VendorFilters = settings.Filters.VendorsFilter;


                            var options = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(XMLSerializationHelper.Serialize(settings));

                            switch (settings.Filters.VendorsFilter)
                            {
                                case FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.VendorOption.IncludeActiveVendors:
                                    Vendors = 1;
                                    break;
                                case FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.VendorOption.OnlyShowActiveVendors:
                                    Vendors = 2;
                                    break;
                                case FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.VendorOption.ExcludeActiveVendors:
                                    Vendors = 3;
                                    break;
                            }

                            TimeBase timeBase = TimeBase.Local;
                            bool IsShowTrip = false;
                            bool IsActiveOnly = false;
                            bool IsFirstLastLeg = false;
                            bool IsCrew = false;
                            bool IsShowTripStatus = false;
                            bool IsDisplayredFlight = false;
                            bool IsFlightNumber = false;
                            bool IsLegPurpose = false;
                            bool IsSeatAvailable = false;
                            bool IsPaxCount = false;
                            bool IsPaxFullName = false;
                            bool IsRequestor = false;
                            bool IsDepartment = false;
                            bool IsAuthorization = false;
                            bool IsFlightCategory = false;
                            bool IsCummulativeETE = false;

                            bool IsCrewFullName = false;
                            bool IsPax = false;

                            int IsAiport = 1;
                            string UserCD = UserPrincipal.Identity._name;
                            Int64 CustomerID = UserPrincipal.Identity._customerID;
                            string BeginDate = FormatDate(tbBeginDate.Text, DateFormat);
                            int NoofWeeks = Convert.ToInt32(tbWeeks.Text.Trim());
                            string EndDate = FormatDate(tbEndDate.Text, DateFormat);
                            string ClientCd = Convert.ToString(options.Filters.Client);
                            string RequestorCd = Convert.ToString(options.Filters.Requestor);
                            string DepartmentCd = Convert.ToString(options.Filters.Department);
                            string FlightCategoryCd = Convert.ToString(options.Filters.FlightCategory);
                            string DutyTypeCd = Convert.ToString(options.Filters.CrewDuty);
                            string HomeBaseCd = Convert.ToString(options.Filters.HomeBase);
                            bool IsTrip = Convert.ToBoolean(options.Filters.Trip);
                            bool IsCancelled = Convert.ToBoolean(options.Filters.Canceled);
                            bool IsHold = Convert.ToBoolean(options.Filters.Hold);
                            bool IsWorksheet = Convert.ToBoolean(options.Filters.WorkSheet);
                            bool IsUnFulFilled = Convert.ToBoolean(options.Filters.UnFulfilled);
                            bool IsAllCrew = Convert.ToBoolean(options.Filters.AllCrew);
                            bool HomeBase = Convert.ToBoolean(options.Filters.HomeBaseOnly);
                            bool IsCrewCalActivity = false;
                            bool FixedWingCrew = Convert.ToBoolean(options.Filters.FixedWingCrewOnly);
                            bool RotaryWingCrew = Convert.ToBoolean(options.Filters.RotaryWingCrewCrewOnly);

                            bool IsBlackWhiteClr = false;
                            bool IsAircraftClr = false;
                            string EFleet = string.Empty;
                            string ECrew = string.Empty;

                            if (options.Filters.TimeBase != null)
                            {
                                timeBase = options.Filters.TimeBase;
                            }

                            int SortBy = 1;
                            int IsBoth = 1;

                            /*if (Session["FleetIDs"] != null)
                                EFleet = Convert.ToString(Session["FleetIDs"]);

                            if (Session["CrewIDs"] != null)
                                ECrew = Convert.ToString(Session["CrewIDs"]);*/

                            if (ReportString == "RptWeekly")
                            {
                                IsShowTrip = Convert.ToBoolean(options.Display.Weekly.ShowTrip);
                                IsFirstLastLeg = Convert.ToBoolean(options.Display.Weekly.FirstLastLeg);
                                IsCrew = Convert.ToBoolean(options.Display.Weekly.Crew);
                                IsShowTripStatus = Convert.ToBoolean(options.Display.Weekly.ShowTripStatus);
                                IsDisplayredFlight = Convert.ToBoolean(options.Display.Weekly.DisplayRedEyeFlightsInContinuation);
                                IsFlightNumber = Convert.ToBoolean(options.Display.Weekly.FlightNumber);
                                IsLegPurpose = Convert.ToBoolean(options.Display.Weekly.LegPurpose);
                                IsSeatAvailable = Convert.ToBoolean(options.Display.Weekly.SeatsAvailable);
                                IsPaxCount = Convert.ToBoolean(options.Display.Weekly.PaxCount);
                                IsRequestor = Convert.ToBoolean(options.Display.Weekly.Requestor);
                                IsDepartment = Convert.ToBoolean(options.Display.Weekly.Department);
                                IsAuthorization = Convert.ToBoolean(options.Display.Weekly.Authorization);
                                IsFlightCategory = Convert.ToBoolean(options.Display.Weekly.FlightCategory);
                                IsCummulativeETE = Convert.ToBoolean(options.Display.Weekly.CummulativeETE);
                                IsBlackWhiteClr = Convert.ToBoolean(options.Display.Weekly.BlackWhiteColor);
                                IsAircraftClr = Convert.ToBoolean(options.Display.Weekly.AircraftColors);
                                //Session["REPORT"] = "RptPREScheduleWeeklyCalendar";

                                if (options.Display.Weekly.DepartArriveInfo == DepartArriveInfo.ICAO)
                                {
                                    IsAiport = 1;
                                }
                                else if (options.Display.Weekly.DepartArriveInfo == DepartArriveInfo.City)
                                {
                                    IsAiport = 2;
                                }
                                else if (options.Display.Weekly.DepartArriveInfo == DepartArriveInfo.AirportName)
                                {
                                    IsAiport = 3;
                                }
                            }
                            else if (ReportString == "RptWeeklyFleet")
                            {
                                IsShowTrip = Convert.ToBoolean(options.Display.WeeklyFleet.ShowTrip);
                                IsActiveOnly = Convert.ToBoolean(options.Display.WeeklyFleet.ActivityOnly);
                                IsFirstLastLeg = Convert.ToBoolean(options.Display.WeeklyFleet.FirstLastLeg);
                                IsCrew = Convert.ToBoolean(options.Display.WeeklyFleet.Crew);
                                IsShowTripStatus = Convert.ToBoolean(options.Display.WeeklyFleet.ShowTripStatus);
                                IsDisplayredFlight = Convert.ToBoolean(options.Display.WeeklyFleet.DisplayRedEyeFlightsInContinuation);
                                IsFlightNumber = Convert.ToBoolean(options.Display.WeeklyFleet.FlightNumber);
                                IsLegPurpose = Convert.ToBoolean(options.Display.WeeklyFleet.LegPurpose);
                                IsSeatAvailable = Convert.ToBoolean(options.Display.WeeklyFleet.SeatsAvailable);
                                IsPaxCount = Convert.ToBoolean(options.Display.WeeklyFleet.PaxCount);
                                IsRequestor = Convert.ToBoolean(options.Display.WeeklyFleet.Requestor);
                                IsDepartment = Convert.ToBoolean(options.Display.WeeklyFleet.Department);
                                IsAuthorization = Convert.ToBoolean(options.Display.WeeklyFleet.Authorization);
                                IsFlightCategory = Convert.ToBoolean(options.Display.WeeklyFleet.FlightCategory);
                                IsCummulativeETE = Convert.ToBoolean(options.Display.WeeklyFleet.CummulativeETE);
                                IsBlackWhiteClr = Convert.ToBoolean(options.Display.WeeklyFleet.BlackWhiteColor);
                                IsAircraftClr = Convert.ToBoolean(options.Display.WeeklyFleet.AircraftColors);

                                //Session["REPORT"] = "RptPREWeeklyFleetScheduleCalendar";

                                if (options.Display.WeeklyFleet.DepartArriveInfo == DepartArriveInfo.ICAO)
                                {
                                    IsAiport = 1;
                                }
                                else if (options.Display.WeeklyFleet.DepartArriveInfo == DepartArriveInfo.City)
                                {
                                    IsAiport = 2;
                                }
                                else if (options.Display.WeeklyFleet.DepartArriveInfo == DepartArriveInfo.AirportName)
                                {
                                    IsAiport = 3;
                                }

                            }
                            else if (ReportString == "RptWeeklyCrew")
                            {
                                IsShowTrip = Convert.ToBoolean(options.Display.WeeklyCrew.ShowTrip);
                                IsActiveOnly = Convert.ToBoolean(options.Display.WeeklyCrew.ActivityOnly);
                                IsFirstLastLeg = Convert.ToBoolean(options.Display.WeeklyCrew.FirstLastLeg);
                                IsCrew = Convert.ToBoolean(options.Display.WeeklyCrew.Crew);
                                IsShowTripStatus = Convert.ToBoolean(options.Display.WeeklyCrew.ShowTripStatus);
                                IsDisplayredFlight = Convert.ToBoolean(options.Display.WeeklyCrew.DisplayRedEyeFlightsInContinuation);
                                IsFlightNumber = Convert.ToBoolean(options.Display.WeeklyCrew.FlightNumber);
                                IsLegPurpose = Convert.ToBoolean(options.Display.WeeklyCrew.LegPurpose);
                                IsSeatAvailable = Convert.ToBoolean(options.Display.WeeklyCrew.SeatsAvailable);
                                IsPaxCount = Convert.ToBoolean(options.Display.WeeklyCrew.PaxCount);
                                IsRequestor = Convert.ToBoolean(options.Display.WeeklyCrew.Requestor);
                                IsDepartment = Convert.ToBoolean(options.Display.WeeklyCrew.Department);
                                IsAuthorization = Convert.ToBoolean(options.Display.WeeklyCrew.Authorization);
                                IsFlightCategory = Convert.ToBoolean(options.Display.WeeklyCrew.FlightCategory);
                                IsCummulativeETE = Convert.ToBoolean(options.Display.WeeklyCrew.CummulativeETE);
                                IsBlackWhiteClr = Convert.ToBoolean(options.Display.WeeklyCrew.BlackWhiteColor);
                                IsAircraftClr = Convert.ToBoolean(options.Display.WeeklyCrew.AircraftColors);
                                //Session["REPORT"] = "RptPREWeeklyCrewScheduleCalendar";

                                if (options.Display.WeeklyCrew.DepartArriveInfo == DepartArriveInfo.ICAO)
                                {
                                    IsAiport = 1;
                                }
                                else if (options.Display.WeeklyCrew.DepartArriveInfo == DepartArriveInfo.City)
                                {
                                    IsAiport = 2;
                                }
                                else if (options.Display.WeeklyCrew.DepartArriveInfo == DepartArriveInfo.AirportName)
                                {
                                    IsAiport = 3;
                                }
                            }
                            else if (ReportString == "RptWeeklyDetail")
                            {
                                IsShowTrip = Convert.ToBoolean(options.Display.WeeklyDetail.ShowTripNumber);
                                IsActiveOnly = Convert.ToBoolean(options.Display.WeeklyDetail.ActivityOnly);
                                IsFirstLastLeg = Convert.ToBoolean(options.Display.WeeklyDetail.FirstLastLeg);
                                IsCrew = Convert.ToBoolean(options.Display.WeeklyDetail.Crew);
                                IsShowTripStatus = Convert.ToBoolean(options.Display.WeeklyDetail.ShowTripStatus);
                                IsFlightNumber = Convert.ToBoolean(options.Display.WeeklyDetail.FlightNumber);
                                IsLegPurpose = Convert.ToBoolean(options.Display.WeeklyDetail.LegPurpose);
                                IsSeatAvailable = Convert.ToBoolean(options.Display.WeeklyDetail.SeatsAvailable);
                                IsPaxCount = Convert.ToBoolean(options.Display.WeeklyDetail.PaxCount);
                                IsPaxFullName = Convert.ToBoolean(options.Display.WeeklyDetail.PaxFullName);
                                IsRequestor = Convert.ToBoolean(options.Display.WeeklyDetail.Requestor);
                                IsAuthorization = Convert.ToBoolean(options.Display.WeeklyDetail.Authorization);
                                IsDepartment = Convert.ToBoolean(options.Display.WeeklyDetail.Department);
                                IsFlightCategory = Convert.ToBoolean(options.Display.WeeklyDetail.FlightCategory);
                                IsCummulativeETE = Convert.ToBoolean(options.Display.WeeklyDetail.CummulativeETE);
                                IsCrewFullName = Convert.ToBoolean(options.Display.WeeklyDetail.CrewFullName);
                                IsPax = Convert.ToBoolean(options.Display.WeeklyDetail.Pax);
                                IsBlackWhiteClr = Convert.ToBoolean(options.Display.WeeklyDetail.BlackWhiteColor);
                                IsAircraftClr = Convert.ToBoolean(options.Display.WeeklyDetail.AircraftColors);
                                //Session["REPORT"] = "RptPREWeeklyDetailScheduleCalendar";

                                if (options.Display.WeeklyDetail.DayWeeklyDetailSortOption == DayWeeklyDetailSortOption.TailNumber)
                                {
                                    SortBy = 1;
                                }
                                else if (options.Display.WeeklyDetail.DayWeeklyDetailSortOption == DayWeeklyDetailSortOption.DepartureDateTime)
                                {
                                    SortBy = 2;
                                }

                                if (options.Display.WeeklyDetail.DepartArriveInfo == DepartArriveInfo.ICAO)
                                {
                                    IsAiport = 1;
                                }
                                else if (options.Display.WeeklyDetail.DepartArriveInfo == DepartArriveInfo.City)
                                {
                                    IsAiport = 2;
                                }
                                else if (options.Display.WeeklyDetail.DepartArriveInfo == DepartArriveInfo.AirportName)
                                {
                                    IsAiport = 3;
                                }

                            }
                            else if (ReportString == "RptDay")
                            {
                                IsActiveOnly = Convert.ToBoolean(options.Display.Day.ActivityOnly);
                                IsCrewCalActivity = Convert.ToBoolean(options.Display.Day.CrewCalActivity);
                                IsFirstLastLeg = Convert.ToBoolean(options.Display.Day.FirstLastLeg);
                                IsShowTripStatus = Convert.ToBoolean(options.Display.Day.ShowTripStatus);
                                IsCummulativeETE = Convert.ToBoolean(options.Display.Day.CummulativeETE);
                                IsBlackWhiteClr = Convert.ToBoolean(options.Display.Day.BlackWhiteColor);
                                IsAircraftClr = Convert.ToBoolean(options.Display.Day.AircraftColors);
                                //Session["REPORT"] = "RptPREScheduleDayCalendar";

                                if (options.Display.Day.DayWeeklyDetailSortOption == DayWeeklyDetailSortOption.TailNumber)
                                {
                                    SortBy = 1;
                                }
                                else if (options.Display.Day.DayWeeklyDetailSortOption == DayWeeklyDetailSortOption.DepartureDateTime)
                                {
                                    SortBy = 2;
                                }

                                if (options.Display.Day.DepartArriveInfo == DepartArriveInfo.ICAO)
                                {
                                    IsAiport = 1;
                                }
                                else if (options.Display.Day.DepartArriveInfo == DepartArriveInfo.City)
                                {
                                    IsAiport = 2;
                                }
                                else if (options.Display.Day.DepartArriveInfo == DepartArriveInfo.AirportName)
                                {
                                    IsAiport = 3;
                                }
                            }
                            else if (ReportString == "RptPlanner")
                            {
                                IsActiveOnly = Convert.ToBoolean(options.Display.Planner.ActiveOnly);
                                IsBlackWhiteClr = Convert.ToBoolean(options.Display.Planner.BlackWhiteColor);
                                IsAircraftClr = Convert.ToBoolean(options.Display.Planner.AircraftColors);
                                //Session["REPORT"] = "RptPREMonthlyPlannerScheduleCalendar";

                                if (options.Display.Planner.PlannerDisplayOption == PlannerDisplayOption.Both)
                                {
                                    IsBoth = 1;
                                }
                                else if (options.Display.Planner.PlannerDisplayOption == PlannerDisplayOption.FleetOnly)
                                {
                                    IsBoth = 2;
                                }
                                else if (options.Display.Planner.PlannerDisplayOption == PlannerDisplayOption.CrewOnly)
                                {
                                    IsBoth = 3;
                                }
                            }
                            else if (ReportString == "RptBusinessWeek")
                            {
                                IsShowTrip = Convert.ToBoolean(options.Display.BusinessWeek.ShowTrip);
                                IsActiveOnly = Convert.ToBoolean(options.Display.BusinessWeek.CrewCalActivity);
                                IsFirstLastLeg = Convert.ToBoolean(options.Display.BusinessWeek.FirstLastLeg);
                                IsCrew = Convert.ToBoolean(options.Display.BusinessWeek.Crew);
                                IsShowTripStatus = Convert.ToBoolean(options.Display.BusinessWeek.ShowTripStatus);
                                IsDisplayredFlight = Convert.ToBoolean(options.Display.BusinessWeek.DisplayRedEyeFlightsInContinuation);
                                IsFlightNumber = Convert.ToBoolean(options.Display.BusinessWeek.FlightNumber);
                                IsLegPurpose = Convert.ToBoolean(options.Display.BusinessWeek.LegPurpose);
                                IsSeatAvailable = Convert.ToBoolean(options.Display.BusinessWeek.SeatsAvailable);
                                IsPaxCount = Convert.ToBoolean(options.Display.BusinessWeek.PaxCount);
                                IsRequestor = Convert.ToBoolean(options.Display.BusinessWeek.Requestor);
                                IsDepartment = Convert.ToBoolean(options.Display.BusinessWeek.Department);
                                IsFlightCategory = Convert.ToBoolean(options.Display.BusinessWeek.FlightCategory);
                                IsAuthorization = Convert.ToBoolean(options.Display.BusinessWeek.Authorization);
                                IsCummulativeETE = Convert.ToBoolean(options.Display.BusinessWeek.CummulativeETE);
                                IsBlackWhiteClr = Convert.ToBoolean(options.Display.BusinessWeek.BlackWhiteColor);
                                IsAircraftClr = Convert.ToBoolean(options.Display.BusinessWeek.AircraftColors);
                                //Session["REPORT"] = "RptPREScheduleCalendarBusinessWeek";

                                if (options.Display.BusinessWeek.DepartArriveInfo == DepartArriveInfo.ICAO)
                                {
                                    IsAiport = 1;
                                }
                                else if (options.Display.BusinessWeek.DepartArriveInfo == DepartArriveInfo.City)
                                {
                                    IsAiport = 2;
                                }
                                else if (options.Display.BusinessWeek.DepartArriveInfo == DepartArriveInfo.AirportName)
                                {
                                    IsAiport = 3;
                                }
                            }
                            else if (ReportString == "RptCorporate")
                            {
                                IsShowTripStatus = Convert.ToBoolean(options.Display.Corporate.ShowTripStatus);
                                IsBlackWhiteClr = Convert.ToBoolean(options.Display.Corporate.BlackWhiteColor);
                                IsAircraftClr = Convert.ToBoolean(options.Display.Corporate.AircraftColors);
                                //Session["REPORT"] = "RptPREScheduleCorporateCalendar";

                                if (options.Display.Corporate.DepartArriveInfo == DepartArriveInfo.ICAO)
                                {
                                    IsAiport = 1;
                                }
                                else if (options.Display.Corporate.DepartArriveInfo == DepartArriveInfo.City)
                                {
                                    IsAiport = 2;
                                }
                                else if (options.Display.Corporate.DepartArriveInfo == DepartArriveInfo.AirportName)
                                {
                                    IsAiport = 3;
                                }
                            }


                            bool Footer = false;
                            if (rbSkip.Checked == true)
                            {
                                Footer = false;
                            }
                            else
                            {
                                Footer = true;
                            }

                            if (Param == string.Empty)
                                Param = "UserCD=" + UserPrincipal.Identity._name;

                            //Added byb Sudhakar
                            Param = Param + ";UserCustomerID=" + UserPrincipal.Identity._customerID;
                            Param = Param + ";UserHomebaseID=" + UserPrincipal.Identity._homeBaseId;
                            Param = Param + ";BeginDate=" + BeginDate;

                            Param = Param + ";EndDate=" + EndDate;
                            Param = Param + ";ClientCD=" + ClientCd;
                            Param = Param + ";RequestorCD=" + RequestorCd;
                            Param = Param + ";DepartmentCD=" + DepartmentCd;
                            Param = Param + ";FlightCatagoryCD=" + FlightCategoryCd;
                            Param = Param + ";DutyTypeCD=" + DutyTypeCd;
                            Param = Param + ";HomeBaseCD=" + HomeBaseCd;
                            Param = Param + ";IsTrip=" + IsTrip;
                            Param = Param + ";IsCanceled=" + IsCancelled;
                            Param = Param + ";IsHold=" + IsHold;
                            Param = Param + ";IsWorkSheet=" + IsWorksheet;
                            Param = Param + ";IsUnFulFilled=" + IsUnFulFilled;
                            Param = Param + ";IsAllCrew=" + IsAllCrew;
                            Param = Param + ";HomeBase=" + HomeBase;
                            Param = Param + ";FixedWingCrew=" + FixedWingCrew;
                            Param = Param + ";RotaryWingCrew=" + RotaryWingCrew;
                            Param = Param + ";TimeBase=" + timeBase;
                            Param = Param + ";Vendors=" + Vendors;
                            Param = Param + ";BlackWhiteClr=" + IsBlackWhiteClr;
                            Param = Param + ";AircraftClr=" + IsAircraftClr;

                            // + ";EndDate=" + EndDate + ";

                            string[] FleetCrew;

                            switch (ReportString)
                            {
                                case "RptWeeklyFleet":

                                    SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyFleet);

                                    FleetCrew = SelectedFleetCrewIDs.Split('|');

                                    if (!string.IsNullOrEmpty(FleetCrew[0]))
                                    {
                                        EFleet = string.Join(",", FleetCrew[0].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                                    }
                                    else
                                    {
                                        using (FlightPak.Web.PreflightService.PreflightServiceClient preflightServiceClient = new FlightPak.Web.PreflightService.PreflightServiceClient())
                                        {
                                            var fleetlist = preflightServiceClient.GetFleetInfoForFleetTree(false); // if homebaseonly option is selected, filter by homebaseID
                                            if (fleetlist != null)
                                            {
                                                var fullFleetList = fleetlist.EntityList.Where(x => x.FleetGroupID == null).ToList();
                                                //var groupedLists = list.EntityList.Where(x => x.FleetGroupID != null).ToList();
                                                var distinctfleetIds = fullFleetList.Select(x => x.FleetID).Distinct().ToList();
                                                EFleet = string.Join(",", distinctfleetIds);
                                            }
                                        }
                                    }

                                    Param = Param + ";NoOfWeeks=" + NoofWeeks;
                                    Param = Param + ";IsAirport=" + IsAiport;
                                    Param = Param + ";IsShowTrip=" + IsShowTrip;
                                    Param = Param + ";IsActivityOnly=" + IsActiveOnly;
                                    Param = Param + ";IsFirstLastLeg=" + IsFirstLastLeg;
                                    Param = Param + ";IsCrew=" + IsCrew;
                                    Param = Param + ";IsShowTripStatus=" + IsShowTripStatus;
                                    Param = Param + ";IsDisplayRedEyeFlight=" + IsDisplayredFlight;
                                    Param = Param + ";IsFlightNo=" + IsFlightNumber;
                                    Param = Param + ";IsLegPurpose=" + IsLegPurpose;
                                    Param = Param + ";IsSeatAvailable=" + IsSeatAvailable;
                                    Param = Param + ";IsPaxCount=" + IsPaxCount;
                                    Param = Param + ";IsRequestor=" + IsRequestor;
                                    Param = Param + ";IsDepartment=" + IsDepartment;
                                    Param = Param + ";IsAuthorization=" + IsAuthorization;
                                    Param = Param + ";IsFlightCategory=" + IsFlightCategory;
                                    Param = Param + ";IsCummulativeETE=" + IsCummulativeETE;
                                    Param = Param + ";Footer=" + Footer;
                                    Param = Param + ";EFleet =" + EFleet;
                                    break;

                                case "RptWeeklyCrew":

                                    SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyCrew);

                                    FleetCrew = SelectedFleetCrewIDs.Split('|');

                                    if (!string.IsNullOrEmpty(FleetCrew[1]))
                                    {
                                        ECrew = string.Join(",", FleetCrew[1].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                                    }
                                    else
                                    {
                                        using (FlightPak.Web.PreflightService.PreflightServiceClient preflightServiceClient = new FlightPak.Web.PreflightService.PreflightServiceClient())
                                        {
                                            var crewlist = preflightServiceClient.GetCrewInfoForCrewTree(false); // if homebaseonly option is selected, filter by homebaseID
                                            if (crewlist != null)
                                            {
                                                var fullCrewList = crewlist.EntityList.Where(x => x.CrewGroupID == null).ToList();
                                                //var groupedLists = crewlist.EntityList.Where(x => x.CrewGroupID != null).ToList();
                                                var distinctcrewIds = fullCrewList.Select(x => x.CrewID).Distinct().ToList();
                                                ECrew = string.Join(",", distinctcrewIds);
                                            }
                                        }
                                    }

                                    Param = Param + ";NoOfWeeks=" + NoofWeeks;
                                    Param = Param + ";IsAirport=" + IsAiport;
                                    Param = Param + ";IsShowTrip=" + IsShowTrip;
                                    Param = Param + ";IsActivityOnly=" + IsActiveOnly;
                                    Param = Param + ";IsFirstLastLeg=" + IsFirstLastLeg;
                                    Param = Param + ";IsCrew=" + IsCrew;
                                    Param = Param + ";IsShowTripStatus=" + IsShowTripStatus;
                                    Param = Param + ";IsDisplayRedEyeFlight=" + IsDisplayredFlight;
                                    Param = Param + ";IsFlightNo=" + IsFlightNumber;
                                    Param = Param + ";IsLegPurpose=" + IsLegPurpose;
                                    Param = Param + ";IsSeatAvailable=" + IsSeatAvailable;
                                    Param = Param + ";IsPaxCount=" + IsPaxCount;
                                    Param = Param + ";IsRequestor=" + IsRequestor;
                                    Param = Param + ";IsDepartment=" + IsDepartment;
                                    Param = Param + ";IsAuthorization=" + IsAuthorization;
                                    Param = Param + ";IsFlightCategory=" + IsFlightCategory;
                                    Param = Param + ";IsCummulativeETE=" + IsCummulativeETE;
                                    Param = Param + ";Footer=" + Footer;
                                    Param = Param + ";ECrew =" + ECrew;
                                    break;

                                case "RptWeekly":

                                    SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyMain);

                                    FleetCrew = SelectedFleetCrewIDs.Split('|');

                                    if (!string.IsNullOrEmpty(FleetCrew[0]))
                                    {
                                        EFleet = string.Join(",", FleetCrew[0].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                                    }
                                    else if (!string.IsNullOrEmpty(FleetCrew[1]))
                                    {
                                        ECrew = string.Join(",", FleetCrew[1].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                                    }
                                    else
                                    {
                                        using (FlightPak.Web.PreflightService.PreflightServiceClient preflightServiceClient = new FlightPak.Web.PreflightService.PreflightServiceClient())
                                        {
                                            var fleetlist = preflightServiceClient.GetFleetInfoForFleetTree(false); // if homebaseonly option is selected, filter by homebaseID
                                            if (fleetlist != null)
                                            {
                                                var fullFleetList = fleetlist.EntityList.Where(x => x.FleetGroupID == null).ToList();
                                                //var groupedLists = list.EntityList.Where(x => x.FleetGroupID != null).ToList();
                                                var distinctfleetIds = fullFleetList.Select(x => x.FleetID).Distinct().ToList();
                                                EFleet = string.Join(",", distinctfleetIds);
                                            }
                                            var crewlist = preflightServiceClient.GetCrewInfoForCrewTree(false); // if homebaseonly option is selected, filter by homebaseID
                                            if (crewlist != null)
                                            {
                                                var fullCrewList = crewlist.EntityList.Where(x => x.CrewGroupID == null).ToList();
                                                //var groupedLists = crewlist.EntityList.Where(x => x.CrewGroupID != null).ToList();
                                                var distinctcrewIds = fullCrewList.Select(x => x.CrewID).Distinct().ToList();
                                                ECrew = string.Join(",", distinctcrewIds);
                                            }
                                        }
                                    }

                                    Param = Param + ";NoOfWeeks=" + NoofWeeks;
                                    Param = Param + ";IsAirport=" + IsAiport;
                                    Param = Param + ";IsShowTrip=" + IsShowTrip;
                                    Param = Param + ";IsActivityOnly=" + IsActiveOnly;
                                    Param = Param + ";IsFirstLastLeg=" + IsFirstLastLeg;
                                    Param = Param + ";IsCrew=" + IsCrew;
                                    Param = Param + ";IsShowTripStatus=" + IsShowTripStatus;
                                    Param = Param + ";IsDisplayRedEyeFlight=" + IsDisplayredFlight;
                                    Param = Param + ";IsFlightNo=" + IsFlightNumber;
                                    Param = Param + ";IsLegPurpose=" + IsLegPurpose;
                                    Param = Param + ";IsSeatAvailable=" + IsSeatAvailable;
                                    Param = Param + ";IsPaxCount=" + IsPaxCount;
                                    Param = Param + ";IsRequestor=" + IsRequestor;
                                    Param = Param + ";IsDepartment=" + IsDepartment;
                                    Param = Param + ";IsAuthorization=" + IsAuthorization;
                                    Param = Param + ";IsFlightCategory=" + IsFlightCategory;
                                    Param = Param + ";IsCummulativeETE=" + IsCummulativeETE;
                                    Param = Param + ";Footer=" + Footer;
                                    Param = Param + ";EFleet =" + EFleet;
                                    Param = Param + ";ECrew =" + ECrew;
                                    break;

                                case "RptWeeklyDetail":

                                    SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyDetail);

                                    FleetCrew = SelectedFleetCrewIDs.Split('|');

                                    if (!string.IsNullOrEmpty(FleetCrew[0]))
                                    {
                                        EFleet = string.Join(",", FleetCrew[0].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                                    }
                                    else
                                    {
                                        using (FlightPak.Web.PreflightService.PreflightServiceClient preflightServiceClient = new FlightPak.Web.PreflightService.PreflightServiceClient())
                                        {
                                            var fleetlist = preflightServiceClient.GetFleetInfoForFleetTree(false); // if homebaseonly option is selected, filter by homebaseID
                                            if (fleetlist != null)
                                            {
                                                var fullFleetList = fleetlist.EntityList.Where(x => x.FleetGroupID == null).ToList();
                                                //var groupedLists = list.EntityList.Where(x => x.FleetGroupID != null).ToList();
                                                var distinctfleetIds = fullFleetList.Select(x => x.FleetID).Distinct().ToList();
                                                EFleet = string.Join(",", distinctfleetIds);
                                            }
                                        }
                                    }

                                    Param = Param + ";NoOfWeeks=" + NoofWeeks;
                                    Param = Param + ";SortBy=" + SortBy;
                                    Param = Param + ";IsAirport=" + IsAiport;
                                    Param = Param + ";IsActivityOnly=" + IsActiveOnly;

                                    Param = Param + ";IsFirstLastLeg=" + IsFirstLastLeg;
                                    Param = Param + ";IsCrew=" + IsCrew;
                                    Param = Param + ";IsShowTripStatus=" + IsShowTripStatus;
                                    Param = Param + ";IsFlightNo=" + IsFlightNumber;
                                    Param = Param + ";IsLegPurpose=" + IsLegPurpose;
                                    Param = Param + ";IsSeatAvailable=" + IsSeatAvailable;
                                    Param = Param + ";IsPaxCount=" + IsPaxCount;
                                    Param = Param + ";IsPaxFullName=" + IsPaxFullName;
                                    Param = Param + ";IsShowTrip=" + IsShowTrip;

                                    Param = Param + ";IsRequestor=" + IsRequestor;
                                    Param = Param + ";IsDepartment=" + IsDepartment;
                                    Param = Param + ";IsAuthorization=" + IsAuthorization;
                                    Param = Param + ";IsFlightCategory=" + IsFlightCategory;
                                    Param = Param + ";IsCummulativeETE=" + IsCummulativeETE;
                                    Param = Param + ";IsCrewFullName=" + IsCrewFullName;
                                    Param = Param + ";IsPax=" + IsPax;
                                    Param = Param + ";Footer=" + Footer;
                                    Param = Param + ";EFleet =" + EFleet;
                                    break;

                                case "RptDay":

                                    SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Day);

                                    FleetCrew = SelectedFleetCrewIDs.Split('|');

                                    if (!string.IsNullOrEmpty(FleetCrew[0]))
                                    {
                                        EFleet = string.Join(",", FleetCrew[0].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                                    }
                                    else if (!string.IsNullOrEmpty(FleetCrew[1]))
                                    {
                                        ECrew = string.Join(",", FleetCrew[1].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                                    }
                                    else
                                    {
                                        using (FlightPak.Web.PreflightService.PreflightServiceClient preflightServiceClient = new FlightPak.Web.PreflightService.PreflightServiceClient())
                                        {
                                            var fleetlist = preflightServiceClient.GetFleetInfoForFleetTree(false); // if homebaseonly option is selected, filter by homebaseID
                                            if (fleetlist != null)
                                            {
                                                var fullFleetList = fleetlist.EntityList.Where(x => x.FleetGroupID == null).ToList();
                                                //var groupedLists = list.EntityList.Where(x => x.FleetGroupID != null).ToList();
                                                var distinctfleetIds = fullFleetList.Select(x => x.FleetID).Distinct().ToList();
                                                EFleet = string.Join(",", distinctfleetIds);
                                            }
                                            var crewlist = preflightServiceClient.GetCrewInfoForCrewTree(false); // if homebaseonly option is selected, filter by homebaseID
                                            if (crewlist != null)
                                            {
                                                var fullCrewList = crewlist.EntityList.Where(x => x.CrewGroupID == null).ToList();
                                                //var groupedLists = crewlist.EntityList.Where(x => x.CrewGroupID != null).ToList();
                                                var distinctcrewIds = fullCrewList.Select(x => x.CrewID).Distinct().ToList();
                                                ECrew = string.Join(",", distinctcrewIds);
                                            }
                                        }
                                    }

                                    Param = Param + ";SortBy=" + SortBy;
                                    Param = Param + ";NoOfDays=" + NoofWeeks;
                                    Param = Param + ";IsActivityOnly=" + IsActiveOnly;
                                    Param = Param + ";IsFirstLastLeg=" + IsFirstLastLeg;
                                    Param = Param + ";IsCrewCalActivity=" + IsCrewCalActivity;
                                    Param = Param + ";IsShowTripStatus=" + IsShowTripStatus;
                                    Param = Param + ";IsCummulativeETE=" + IsCummulativeETE;
                                    Param = Param + ";IsAirport=" + IsAiport;
                                    Param = Param + ";Footer=" + Footer;
                                    Param = Param + ";EFleet =" + EFleet;
                                    Param = Param + ";ECrew =" + ECrew;
                                    break;

                                case "RptPlanner":

                                    SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Planner);

                                    FleetCrew = SelectedFleetCrewIDs.Split('|');

                                    if (!string.IsNullOrEmpty(FleetCrew[0]))
                                    {
                                        EFleet = string.Join(",", FleetCrew[0].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                                    }

                                    if (!string.IsNullOrEmpty(FleetCrew[1]))
                                    {
                                        ECrew = string.Join(",", FleetCrew[1].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                                    }

                                    if (string.IsNullOrEmpty(FleetCrew[0]) && string.IsNullOrEmpty(FleetCrew[1]))
                                    {
                                        using (FlightPak.Web.PreflightService.PreflightServiceClient preflightServiceClient = new FlightPak.Web.PreflightService.PreflightServiceClient())
                                        {
                                            var fleetlist = preflightServiceClient.GetFleetInfoForFleetTree(false); // if homebaseonly option is selected, filter by homebaseID
                                            if (fleetlist != null)
                                            {
                                                var fullFleetList = fleetlist.EntityList.Where(x => x.FleetGroupID == null).ToList();
                                                //var groupedLists = list.EntityList.Where(x => x.FleetGroupID != null).ToList();
                                                var distinctfleetIds = fullFleetList.Select(x => x.FleetID).Distinct().ToList();
                                                EFleet = string.Join(",", distinctfleetIds);
                                            }
                                            var crewlist = preflightServiceClient.GetCrewInfoForCrewTree(false); // if homebaseonly option is selected, filter by homebaseID
                                            if (crewlist != null)
                                            {
                                                var fullCrewList = crewlist.EntityList.Where(x => x.CrewGroupID == null).ToList();
                                                //var groupedLists = crewlist.EntityList.Where(x => x.CrewGroupID != null).ToList();
                                                var distinctcrewIds = fullCrewList.Select(x => x.CrewID).Distinct().ToList();
                                                ECrew = string.Join(",", distinctcrewIds);
                                            }
                                        }
                                    }

                                    Param = Param + ";NoOfMonths=" + NoofWeeks;
                                    Param = Param + ";IsBoth=" + IsBoth;
                                    Param = Param + ";IsActivityOnly=" + IsActiveOnly;
                                    Param = Param + ";Footer=" + Footer;
                                    Param = Param + ";EFleet =" + EFleet;
                                    Param = Param + ";ECrew =" + ECrew;
                                    Param = Param + ";DisplayDays =" + DisplayDays;
                                    break;

                                case "RptBusinessWeek":

                                    SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.BusinessWeek);

                                    FleetCrew = SelectedFleetCrewIDs.Split('|');

                                    if (!string.IsNullOrEmpty(FleetCrew[0]))
                                    {
                                        EFleet = string.Join(",", FleetCrew[0].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                                    }
                                    else if (!string.IsNullOrEmpty(FleetCrew[1]))
                                    {
                                        ECrew = string.Join(",", FleetCrew[1].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                                    }
                                    else
                                    {
                                        using (FlightPak.Web.PreflightService.PreflightServiceClient preflightServiceClient = new FlightPak.Web.PreflightService.PreflightServiceClient())
                                        {
                                            var fleetlist = preflightServiceClient.GetFleetInfoForFleetTree(false); // if homebaseonly option is selected, filter by homebaseID
                                            if (fleetlist != null)
                                            {
                                                var fullFleetList = fleetlist.EntityList.Where(x => x.FleetGroupID == null).ToList();
                                                //var groupedLists = list.EntityList.Where(x => x.FleetGroupID != null).ToList();
                                                var distinctfleetIds = fullFleetList.Select(x => x.FleetID).Distinct().ToList();
                                                EFleet = string.Join(",", distinctfleetIds);
                                            }
                                            var crewlist = preflightServiceClient.GetCrewInfoForCrewTree(false); // if homebaseonly option is selected, filter by homebaseID
                                            if (crewlist != null)
                                            {
                                                var fullCrewList = crewlist.EntityList.Where(x => x.CrewGroupID == null).ToList();
                                                //var groupedLists = crewlist.EntityList.Where(x => x.CrewGroupID != null).ToList();
                                                var distinctcrewIds = fullCrewList.Select(x => x.CrewID).Distinct().ToList();
                                                ECrew = string.Join(",", distinctcrewIds);
                                            }
                                        }
                                    }

                                    Param = Param + ";NoOfWeeks=" + NoofWeeks;
                                    Param = Param + ";IsAirport=" + IsAiport;
                                    Param = Param + ";IsShowTrip=" + IsShowTrip;
                                    Param = Param + ";IsActivityOnly=" + IsActiveOnly;
                                    Param = Param + ";IsFirstLastLeg=" + IsFirstLastLeg;
                                    Param = Param + ";IsCrew=" + IsCrew;
                                    Param = Param + ";IsShowTripStatus=" + IsShowTripStatus;
                                    Param = Param + ";IsDisplayRedEyeFlight=" + IsDisplayredFlight;
                                    Param = Param + ";IsFlightNo=" + IsFlightNumber;
                                    Param = Param + ";IsLegPurpose=" + IsLegPurpose;
                                    Param = Param + ";IsSeatAvailable=" + IsSeatAvailable;
                                    Param = Param + ";IsPaxCount=" + IsPaxCount;
                                    Param = Param + ";IsRequestor=" + IsRequestor;
                                    Param = Param + ";IsDepartment=" + IsDepartment;
                                    Param = Param + ";IsAuthorization=" + IsAuthorization;
                                    Param = Param + ";IsFlightCategory=" + IsFlightCategory;
                                    Param = Param + ";IsCummulativeETE=" + IsCummulativeETE;
                                    Param = Param + ";Footer=" + Footer;
                                    Param = Param + ";EFleet =" + EFleet;
                                    Param = Param + ";ECrew =" + ECrew;
                                    break;

                                case "RptCorporate":

                                    SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.Corporate);

                                    FleetCrew = SelectedFleetCrewIDs.Split('|');

                                    if (!string.IsNullOrEmpty(FleetCrew[0]))
                                    {
                                        EFleet = string.Join(",", FleetCrew[0].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                                    }
                                    else if (!string.IsNullOrEmpty(FleetCrew[1]))
                                    {
                                        ECrew = string.Join(",", FleetCrew[1].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                                    }
                                    else
                                    {
                                        using (FlightPak.Web.PreflightService.PreflightServiceClient preflightServiceClient = new FlightPak.Web.PreflightService.PreflightServiceClient())
                                        {
                                            var fleetlist = preflightServiceClient.GetFleetInfoForFleetTree(false); // if homebaseonly option is selected, filter by homebaseID
                                            if (fleetlist != null)
                                            {
                                                var fullFleetList = fleetlist.EntityList.Where(x => x.FleetGroupID == null).ToList();
                                                //var groupedLists = list.EntityList.Where(x => x.FleetGroupID != null).ToList();
                                                var distinctfleetIds = fullFleetList.Select(x => x.FleetID).Distinct().ToList();
                                                EFleet = string.Join(",", distinctfleetIds);
                                            }
                                            var crewlist = preflightServiceClient.GetCrewInfoForCrewTree(false); // if homebaseonly option is selected, filter by homebaseID
                                            if (crewlist != null)
                                            {
                                                var fullCrewList = crewlist.EntityList.Where(x => x.CrewGroupID == null).ToList();
                                                //var groupedLists = crewlist.EntityList.Where(x => x.CrewGroupID != null).ToList();
                                                var distinctcrewIds = fullCrewList.Select(x => x.CrewID).Distinct().ToList();
                                                ECrew = string.Join(",", distinctcrewIds);
                                            }
                                        }
                                    }

                                    Param = Param + ";NoOfWeeks=" + NoofWeeks;
                                    Param = Param + ";IsBoth=" + IsBoth;
                                    Param = Param + ";IsAirport=" + IsAiport;
                                    Param = Param + ";IsActivityOnly=" + IsActiveOnly;
                                    Param = Param + ";IsShowTripStatus=" + IsShowTripStatus;
                                    Param = Param + ";Footer=" + Footer;
                                    Param = Param + ";EFleet =" + EFleet;
                                    Param = Param + ";ECrew =" + ECrew;
                                    break;

                                default:

                                    SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.WeeklyFleet);

                                    FleetCrew = SelectedFleetCrewIDs.Split('|');

                                    if (!string.IsNullOrEmpty(FleetCrew[0]))
                                    {
                                        EFleet = string.Join(",", FleetCrew[0].Split(',').Select(f => f.Contains('$') ? f.Split('$')[1] : f).ToList().ToArray());
                                    }
                                    else
                                    {
                                        using (FlightPak.Web.PreflightService.PreflightServiceClient preflightServiceClient = new FlightPak.Web.PreflightService.PreflightServiceClient())
                                        {
                                            var fleetlist = preflightServiceClient.GetFleetInfoForFleetTree(false); // if homebaseonly option is selected, filter by homebaseID
                                            if (fleetlist != null)
                                            {
                                                var fullFleetList = fleetlist.EntityList.Where(x => x.FleetGroupID == null).ToList();
                                                //var groupedLists = list.EntityList.Where(x => x.FleetGroupID != null).ToList();
                                                var distinctfleetIds = fullFleetList.Select(x => x.FleetID).Distinct().ToList();
                                                EFleet = string.Join(",", distinctfleetIds);
                                            }
                                        }
                                    }

                                    Param = Param + ";NoOfWeeks=" + NoofWeeks;
                                    Param = Param + ";IsAirport=" + IsAiport;
                                    Param = Param + ";IsShowTrip=" + IsShowTrip;
                                    Param = Param + ";IsActivityOnly=" + IsActiveOnly;
                                    Param = Param + ";IsFirstLastLeg=" + IsFirstLastLeg;
                                    Param = Param + ";IsCrew=" + IsCrew;
                                    Param = Param + ";IsShowTripStatus=" + IsShowTripStatus;
                                    Param = Param + ";IsDisplayRedEyeFlight=" + IsDisplayredFlight;
                                    Param = Param + ";IsFlightNo=" + IsFlightNumber;
                                    Param = Param + ";IsLegPurpose=" + IsLegPurpose;
                                    Param = Param + ";IsSeatAvailable=" + IsSeatAvailable;
                                    Param = Param + ";IsPaxCount=" + IsPaxCount;
                                    Param = Param + ";IsRequestor=" + IsRequestor;
                                    Param = Param + ";IsDepartment=" + IsDepartment;
                                    Param = Param + ";IsAuthorization=" + IsAuthorization;
                                    Param = Param + ";IsFlightCategory=" + IsFlightCategory;
                                    Param = Param + ";IsCummulativeETE=" + IsCummulativeETE;
                                    Param = Param + ";Footer=" + Footer;
                                    Param = Param + ";EFleet =" + EFleet;
                                    break;
                            }



                            /* if (ReportString == "RptWeeklyCrew" || ReportString == "RptWeeklyFleet" || ReportString == "RptBusinessWeek")
                             { 
                                 Param = Param + ";NoOfWeeks=" + NoofWeeks;
                                 Param = Param + ";IsAirport=" + IsAiport;
                                 Param = Param + ";IsShowTrip=" + IsShowTrip;
                                 Param = Param + ";IsActivityOnly=" + IsActiveOnly;
                                 Param = Param + ";IsFirstLastLeg=" + IsFirstLastLeg;
                                 Param = Param + ";IsCrew=" + IsCrew;
                                 Param = Param + ";IsShowTripStatus=" + IsShowTripStatus;
                                 Param = Param + ";IsDisplayRedEyeFlight=" + IsDisplayredFlight;
                                 Param = Param + ";IsFlightNo=" + IsFlightNumber;
                                 Param = Param + ";IsLegPurpose=" + IsLegPurpose;
                                 Param = Param + ";IsSeatAvailable=" + IsSeatAvailable;
                                 Param = Param + ";IsPaxCount=" + IsPaxCount;                                                
                                 Param = Param + ";IsRequestor=" + IsRequestor;
                                 Param = Param + ";IsDepartment=" + IsDepartment;
                                 Param = Param + ";IsAuthorization=" + IsAuthorization;
                                 Param = Param + ";IsFlightCategory=" + IsFlightCategory;
                                 Param = Param + ";IsCummulativeETE=" + IsCummulativeETE;                       
                                 Param = Param + ";Footer=" + Footer;
                                 Param = Param + ";EFleet =" + EFleet;         
                             }
                             else if (ReportString == "RptWeeklyDetail")
                             {             
                                 Param = Param + ";NoOfWeeks=" + NoofWeeks;
                                 Param = Param + ";SortBy=" + SortBy;
                                 Param = Param + ";IsAirport=" + IsAiport;
                                 Param = Param + ";IsActivityOnly=" + IsActiveOnly;

                                 Param = Param + ";IsFirstLastLeg=" + IsFirstLastLeg;
                                 Param = Param + ";IsCrew=" + IsCrew;
                                 Param = Param + ";IsShowTripStatus=" + IsShowTripStatus;
                                 Param = Param + ";IsFlightNo=" + IsFlightNumber;
                                 Param = Param + ";IsLegPurpose=" + IsLegPurpose;
                                 Param = Param + ";IsSeatAvailable=" + IsSeatAvailable;
                                 Param = Param + ";IsPaxCount=" + IsPaxCount;
                                 Param = Param + ";IsPaxFullName=" + IsPaxFullName;
                                 Param = Param + ";IsShowTrip=" + IsShowTrip;

                                 Param = Param + ";IsRequestor=" + IsRequestor;
                                 Param = Param + ";IsDepartment=" + IsDepartment;
                                 Param = Param + ";IsAuthorization=" + IsAuthorization;
                                 Param = Param + ";IsFlightCategory=" + IsFlightCategory;
                                 Param = Param + ";IsCummulativeETE=" + IsCummulativeETE;
                                 Param = Param + ";IsCrewFullName=" + IsCrewFullName;
                                 Param = Param + ";IsPax=" + IsPax;
                                 Param = Param + ";Footer=" + Footer;
                             }
                             else if (ReportString == "RptPlanner")
                             {                   
                                  Param = Param + ";NoOfMonths=" + NoofWeeks;
                                 Param = Param + ";IsBoth=" + IsBoth;
                                 Param = Param + ";IsActivityOnly=" + IsActiveOnly;
                                 Param = Param + ";Footer=" + Footer;                                             
                             }
                             else if (ReportString == "RptDay")
                             {                        
                                 Param = Param + ";SortBy=" + SortBy;
                                 Param = Param + ";NoOfDays=" + NoofWeeks;  
                                 Param = Param + ";IsActivityOnly=" + IsActiveOnly;
                                 Param = Param + ";IsFirstLastLeg=" + IsFirstLastLeg;
                                 Param = Param + ";IsCrewCalActivity=" + IsCrewCalActivity;
                                 Param = Param + ";IsShowTripStatus=" + IsShowTripStatus;
                                 Param = Param + ";IsCummulativeETE=" + IsCummulativeETE;
                                 Param = Param + ";IsAirport=" + IsAiport;
                                 Param = Param + ";Footer=" + Footer;
                             }
                             else if (ReportString == "RptCorporate")
                             {
                                 Param = Param + ";NoOfWeeks=" + NoofWeeks;
                                 Param = Param + ";IsBoth=" + IsBoth;
                                 Param = Param + ";IsAirport=" + IsAiport;
                                 Param = Param + ";IsActivityOnly=" + IsActiveOnly;
                                 Param = Param + ";IsShowTripStatus=" + IsShowTripStatus;
                                 Param = Param + ";Footer=" + Footer;
                             }
                             else if (ReportString == "RptWeekly")
                             {
                                 Param = Param + ";NoOfWeeks=" + NoofWeeks;
                                 Param = Param + ";IsAirport=" + IsAiport;
                                 Param = Param + ";IsShowTrip=" + IsShowTrip;
                                 Param = Param + ";IsActivityOnly=" + IsActiveOnly;
                                 Param = Param + ";IsFirstLastLeg=" + IsFirstLastLeg;
                                 Param = Param + ";IsCrew=" + IsCrew;                        
                                 Param = Param + ";IsShowTripStatus=" + IsShowTripStatus;
                                 Param = Param + ";IsDisplayRedEyeFlight=" + IsDisplayredFlight;
                                 Param = Param + ";IsFlightNo=" + IsFlightNumber;
                                 Param = Param + ";IsLegPurpose=" + IsLegPurpose;
                                 Param = Param + ";IsSeatAvailable=" + IsSeatAvailable;
                                 Param = Param + ";IsPaxCount=" + IsPaxCount;                                                
                                 Param = Param + ";IsRequestor=" + IsRequestor;
                                 Param = Param + ";IsDepartment=" + IsDepartment;
                                 Param = Param + ";IsAuthorization=" + IsAuthorization;
                                 Param = Param + ";IsFlightCategory=" + IsFlightCategory;
                                 Param = Param + ";IsCummulativeETE=" + IsCummulativeETE;
                                 Param = Param + ";Footer=" + Footer;
                             }*/
                        }
                    }
                    return Param;
                }
                else
                {
                    return "";
                }
            }
        }
       
        private Control FindControlRecursive(Control rootControl, string controlID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(rootControl, controlID))
            {
                if (rootControl.ID == controlID) return rootControl;

                foreach (Control controlToSearch in rootControl.Controls)
                {
                    Control controlToReturn =
                        FindControlRecursive(controlToSearch, controlID);
                    if (controlToReturn != null) return controlToReturn;
                }
                return null;
            }
        }

        public void Page_Init(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    
                        //if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        //{
                        //    DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                        //}
                        //else
                        //{
                        //    DateFormat = "MM/dd/yyyy";
                        //}
                    //((RadDatePicker)ucDatePicker.FindControl("RadDatePicker1")).DateInput.DateFormat = DateFormat;
                    GenerateControls();
                    //ProcessColumns();

                    UCRHS.ColumnList = dicColumnList;
                    UCRHSort.ColumnList = dicColumnList;                    

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }


            /* if (Session["dicSelectedList"] != null)
             {
                 Dictionary<string, string> dicTempSelectedList = (Dictionary<string, string>)Session["dicSelectedList"];
                 foreach (KeyValuePair<string, string> pair in dicTempSelectedList)
                 {
                     dicColumnList.Remove(pair.Key);
                 }

                 UCRHS.ColumnList = dicColumnList;
             }
             else
             {
                 UCRHS.ColumnList = dicColumnList;
             }


             if (Session["dicSelectedListSort"] != null)
             {
                 Dictionary<string, string> dicTempSelectedList = (Dictionary<string, string>)Session["dicSelectedListSort"];
                 UCRHSort.RadListBoxSource.DataSource = dicTempSelectedList;
                 UCRHSort.RadListBoxSource.DataBind();
                 dicTempSelectedList = (Dictionary<string, string>)Session["dicSelectedListSortSel"];
                 UCRHSort.RadListBoxDestination.DataSource = dicTempSelectedList;
                 UCRHSort.RadListBoxDestination.DataBind();
             }
             */

            // UCRHSort.ColumnList = dicColumnList;
        }

        private void GenerateControls()
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                filename = Convert.ToString(Request.QueryString["xmlFilename"]);

                if (!string.IsNullOrEmpty(filename))
                    Session["XMLFileName"] = filename;

                if (filename != null)
                {
                     var filePath = Server.MapPath(urlBase + filename);
                    XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                    var xmlReaderObject = xmlReader.XmlCleanReader();
                    if (xmlReaderObject != null)
                    {
                        XPathDocument SearchFieldDoc = new XPathDocument(xmlReaderObject);
                        XslCompiledTransform transform = new XslCompiledTransform();
                        transform.Load(Server.MapPath(urlBase + "MakeControls.xslt"));
                        StringWriter sw = new StringWriter();
                        transform.Transform(SearchFieldDoc, null, sw);
                        string result = sw.ToString();
                        result = result.Replace("xmlns:asp=\"remove\"", "");
                        Control ctrl = Page.ParseControl(result);

                        foreach (Control innerControl in ctrl.Controls)
                        {
                            if (innerControl is TextBox)
                            {
                                //((TextBox)innerControl).TextChanged += Ctrl_OnTextChanged;
                                if (Session[Session["TabSelect1"] + ((TextBox)innerControl).ID] != null)

                                    ((TextBox)innerControl).Text = Session[Session["TabSelect1"] + ((TextBox)innerControl).ID].ToString();
                            }

                            if (innerControl is RadioButtonList)
                            {
                                if (Session[Session["TabSelect1"] + ((RadioButtonList)innerControl).ID] != null)
                                    ((RadioButtonList)innerControl).SelectedValue = Session[Session["TabSelect1"] + ((RadioButtonList)innerControl).ID].ToString();
                            }

                            if (innerControl is CheckBox)
                            {
                                if (Session[Session["TabSelect1"] + ((CheckBox)innerControl).ID] != null)
                                    ((CheckBox)innerControl).Checked = (bool)Session[Session["TabSelect1"] + ((CheckBox)innerControl).ID];
                            }

                            if (innerControl is HiddenField)
                            {
                                if (Session[Session["TabSelect1"] + ((HiddenField)innerControl).ID] != null)
                                    ((HiddenField)innerControl).Value = Session[Session["TabSelect1"] + ((HiddenField)innerControl).ID].ToString();
                            }

                            if (innerControl is DropDownList)
                            {
                                if (innerControl.ClientID.IndexOf("MonthFrom") <= 0)
                                {
                                    //DropDownList ddlmonth = (DropDownList)innerControl.FindControl("MonthFrom");
                                    DropDownList ddlmonth = (DropDownList)innerControl;
                                    if (ddlmonth != null)
                                    {
                                        if (Session[Session["TabSelect1"] + innerControl.ID + "ddlMonth"] != null)
                                        {
                                            ListItem ItemSel = ddlmonth.Items.FindByText(Session[Session["TabSelect1"] + innerControl.ID + "ddlMonth"].ToString());
                                            if (ItemSel != null)
                                                ddlmonth.SelectedValue = ItemSel.Value;
                                        }

                                    }
                                }
                            }

                            if (innerControl is UserControl)
                            {
                                if (innerControl.ClientID.IndexOf("MonthFrom") <= 0)
                                {
                                    if (innerControl.ClientID == "AsOf")
                                    {
                                        //TextBox tbAsOf = (TextBox)innerControl.FindControl("tbDate");
                                        if (DateFormat != null)
                                        {
                                            if (Session[Session["TabSelect1"] + innerControl.ID + "tbDate"] != null)
                                            {
                                                ((TextBox)innerControl.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Session[Session["TabSelect1"] + innerControl.ID + "tbDate"].ToString());
                                            }
                                            else
                                            {
                                                ((TextBox)innerControl.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", System.DateTime.Now);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        TextBox tbDateOfBirth = (TextBox)innerControl.FindControl("tbDate");

                                        if (tbDateOfBirth != null)
                                        {
                                            if (Session[Session["TabSelect1"] + innerControl.ID + "tbDate"] != null)
                                                tbDateOfBirth.Text = Session[Session["TabSelect1"] + innerControl.ID + "tbDate"].ToString();
                                        }
                                    }
                                    DropDownList ddlmonth = (DropDownList)innerControl.FindControl("ddlMonth");
                                    if (ddlmonth != null)
                                    {
                                        if (Session[Session["TabSelect1"] + innerControl.ID + "ddlMonth"] != null)
                                        {

                                            ListItem ItemSel = ddlmonth.Items.FindByText(Session[Session["TabSelect1"] + innerControl.ID + "ddlMonth"].ToString());
                                            if (ItemSel != null)
                                                ddlmonth.SelectedValue = ItemSel.Value;
                                        }

                                    }

                                    DropDownList ddlyear = (DropDownList)innerControl.FindControl("ddlyear");
                                    if (ddlyear != null)
                                    {
                                        if (Session[Session["TabSelect1"] + innerControl.ID + "ddlyear"] != null)
                                        {
                                            ListItem ItemSel = ddlyear.Items.FindByText(Session[Session["TabSelect1"] + innerControl.ID + "ddlyear"].ToString());
                                            if (ItemSel != null)
                                                ddlmonth.SelectedValue = ItemSel.Value;
                                        }

                                    }

                                    RadMonthYearPicker monthpicker = (RadMonthYearPicker)innerControl.FindControl("tlkMonthView");
                                    if (monthpicker != null)
                                    {
                                        if (Session[Session["TabSelect1"] + innerControl.ID + "tlkMonthView"] != null)
                                        {
                                            monthpicker.SelectedDate = (DateTime)Session[Session["TabSelect1"] + innerControl.ID + "tlkMonthView"];
                                        }
                                    }
                                }
                            }
                        }

                        //Report.Controls.Add(ctrl);
                        string[] strArr = GetReportTitle().Split(',');
                        if (strArr.Count() > 0)
                        {
                            //lbTitle.Text = Convert.ToString(strArr[0]);
                        }
                        //Controlvisible(true);
                    }
                }
                else
                {
                    //Controlvisible(false);
                }

                if (filename == "CRTripItinerary.xml")
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["CRTripNUM"]) && Report.FindControl("CRTripNUM") != null)
                    {
                       // ((TextBox)Report.FindControl("CRTripNUM")).Text = Convert.ToString(Request.QueryString["CRTripNUM"]);
                    }

                    if (Request.QueryString["CRTripID"] != null)
                    {
                        hdnCRMainID.Value = Request.QueryString["CRTripID"];
                    }
                    else
                    {
                        hdnCRMainID.Value = string.Empty;
                    }
                }

            }

        }

        protected void tbEditHeaderText_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag

                    if (!string.IsNullOrEmpty(hdnHeaderSourceID.Value))
                    {
                        HeaderBySource.Items[Convert.ToInt32(hdnHeaderSourceID.Value)].Text = tbEditHeaderText.Text;
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void HeaderBySource_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                   
                        tbEditHeaderText.Text = HeaderBySource.SelectedItem.Text;
                        hdnHeaderSourceID.Value = HeaderBySource.SelectedIndex.ToString();
                   
                }                
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void OnFinish(object sender, System.Web.UI.WebControls.WizardNavigationEventArgs e)
        {
            //string fileName = String.Format("data-{0}.htm", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
            //Response.ContentType = "text/htm";
            //Response.AddHeader("content-disposition", "filename=" + fileName);
            //// write string data to Response.OutputStream here
            //Response.Write(ProcessHtmlParam());
            //Response.End();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                   
                        if (Page.IsValid)
                        {
                            string[] strArr = GetReportTitle().Split(',');
                            if (strArr.Count() > 0)
                            {
                                Session["REPORT"] = Convert.ToString(strArr[1] + "mhtml");
                            }
                            Session["FORMAT"] = "MHTML";
                            Session["PARAMETER"] = ProcessHtmlExcelParam();                            
                            Response.Redirect("ReportRenderView.aspx");
                        }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void OnNext_old(object sender, WizardNavigationEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag

                    if (UCRHS.RadListBoxDestination.Items.Count > 0)
                    {
                        lblError.Text = "";

                        if (Page.IsValid)
                        {  // 
                            if (Wizard1.ActiveStep.ID == "WizardStep5")
                            {
                                switch (ddlFormat.SelectedValue)
                                {
                                    case "MHTML":
                                        Wizard1.Visible = true;
                                        Wizard1.MoveTo(WizardStep1);

                                        break;
                                    case "Excel":
                                        string[] strArr = GetReportTitle().Split(',');
                                        if (strArr.Count() > 0)
                                        {
                                            Session["REPORT"] = Convert.ToString(strArr[1] + "Export");
                                        }
                                        Session["FORMAT"] = "EXCEL";
                                        Session["PARAMETER"] = ProcessResults();
                                        Response.Redirect("ReportRenderView.aspx");
                                        break;
                                    default:
                                        Wizard1.Visible = false;
                                        break;
                                }
                            }

                            if (Wizard1.ActiveStep.ID == "WizardStep1")
                            {
                                // UCRHSort.ColumnList = dicColumnList;

                                foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                {
                                    dicSelectedList.Add(SelectedItem.Value, SelectedItem.Text);
                                }

                                UCRHSort.RadListBoxSource.DataSource = dicSelectedList;
                                UCRHSort.RadListBoxSource.DataValueField = "Key";
                                UCRHSort.RadListBoxSource.DataTextField = "Value";
                                UCRHSort.RadListBoxSource.DataBind();


                                Dictionary<string, string> dicTempSelectedList = dicSelectedList;
                                foreach (KeyValuePair<string, string> pair in dicTempSelectedList)
                                {
                                    dicColumnList.Remove(pair.Key);
                                }
                                Dictionary<string, string> Empty = new Dictionary<string, string>();
                                UCRHSort.RadListBoxDestination.DataSource = Empty;
                                UCRHSort.RadListBoxDestination.DataBind();

                                Session["dicSelectedList"] = dicSelectedList;



                                //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                //{
                                //    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                //}

                                //SelectionDestination.DataSource = dicSelectedList;
                                //SelectionDestination.DataValueField = "Key";
                                //SelectionDestination.DataTextField = "Value";
                                //SelectionDestination.DataBind();

                                //SelectionSource.DataSource = dicSelectedList;
                                //SelectionSource.DataValueField = "Key";
                                //SelectionSource.DataTextField = "Value";
                                //SelectionSource.DataBind();
                            }
                            if (Wizard1.ActiveStep.ID == "WizardStep2")
                            {
                                foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                {
                                    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                }


                                foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxSource.Items)
                                {
                                    dicSelectedListsort.Add(SelectedItem.Value, SelectedItem.Text);
                                }

                                foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxDestination.Items)
                                {
                                    dicSelectedListsortSel.Add(SelectedItem.Value, SelectedItem.Text);
                                }

                                Session["dicSelectedListSort"] = dicSelectedListsort;

                                Session["dicSelectedListSortSel"] = dicSelectedListsortSel;

                                SelectedSource.DataSource = dicSelectedList;
                                SelectedSource.DataValueField = "Key";
                                SelectedSource.DataTextField = "Value";
                                SelectedSource.DataBind();

                                HeaderBySource.DataSource = dicSelectedList;
                                HeaderBySource.DataValueField = "Key";
                                HeaderBySource.DataTextField = "Value";
                                HeaderBySource.DataBind();

                            }

                            if (Wizard1.ActiveStep.ID == "WizardStep4")
                            {
                                txtTitle.Text = lbTitle.Text;

                                // uncmd this code after
                                FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                                using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FPPrincipal identityname = (FPPrincipal)Session[Session.SessionID];
                                    var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == identity.Identity._customerID && x.HomebaseID == identity.Identity._homeBaseId).ToList();

                                    if (objCompany[0].HTMLHeader != string.Empty)
                                    {
                                        chkHeader.Enabled = false;
                                    }
                                    if (objCompany[0].HTMLFooter != string.Empty)
                                    {
                                        chkFooter.Enabled = false;
                                    }
                                    if (objCompany[0].HTMLBackgroun != string.Empty)
                                    {
                                        chkBackground.Enabled = false;
                                    }
                                    string[] strArr = GetReportTitle().Split(',');
                                    if (strArr.Count() > 0)
                                    {
                                        txtTitle.Text = Convert.ToString(strArr[0]);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        lblError.Text = "Select the Fields";
                        e.Cancel = true;

                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void OnNext(object sender, WizardNavigationEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    
                        if (UCRHS.RadListBoxDestination.Items.Count > 0)
                        {
                            lblError.Text = "";

                            if (Page.IsValid)
                            {
                                if (Wizard1.ActiveStep.ID == "WizardStep5")
                                {
                                    switch (ddlFormat.SelectedValue)
                                    {
                                        case "MHTML":
                                            Wizard1.Visible = true;
                                            Wizard1.MoveTo(WizardStep1);

                                            break;
                                        case "Excel":
                                            string[] strArr = GetReportTitle().Split(',');
                                            if (strArr.Count() > 0)
                                            {
                                                Session["REPORT"] = Convert.ToString(strArr[1] + "Export");
                                            }
                                            Session["FORMAT"] = "EXCEL";
                                            Session["PARAMETER"] = ProcessResults();
                                            Response.Redirect("ReportRenderView.aspx");
                                            break;
                                        default:
                                            Wizard1.Visible = false;
                                            break;
                                    }
                                }

                                if (Wizard1.ActiveStep.ID == "WizardStep1")
                                {
                                    ////Coded by srini
                                    foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                    {
                                        dicSelectedList.Add(SelectedItem.Value, SelectedItem.Text);
                                    }

                                    UCRHSort.RadListBoxSource.DataSource = dicSelectedList;
                                    UCRHSort.RadListBoxSource.DataValueField = "Key";
                                    UCRHSort.RadListBoxSource.DataTextField = "Value";
                                    UCRHSort.RadListBoxSource.DataBind();


                                    Dictionary<string, string> dicTempSelectedList = dicSelectedList;
                                    foreach (KeyValuePair<string, string> pair in dicTempSelectedList)
                                    {
                                        dicColumnList.Remove(pair.Key);
                                    }
                                    Dictionary<string, string> Empty = new Dictionary<string, string>();
                                    UCRHSort.RadListBoxDestination.DataSource = Empty;
                                    UCRHSort.RadListBoxDestination.DataBind();

                                    Session["dicSelectedList"] = dicSelectedList;

                                    //dicSourceList = new Dictionary<string, string>();
                                    //foreach (RadListBoxItem radlistitem in UCRHS.RadListBoxSource.Items)
                                    //{
                                    //    dicSourceList.Add(radlistitem.Value, radlistitem.Text);
                                    //}
                                    //Session["SourceList"] = dicSourceList;

                                    //dicDestinationList = new Dictionary<string, string>();
                                    //foreach (RadListBoxItem radlistitem in UCRHS.RadListBoxDestination.Items)
                                    //{
                                    //    dicDestinationList.Add(radlistitem.Value, radlistitem.Text);
                                    //}
                                    //Session["DestinationList"] = dicDestinationList;

                                    //if (Session["SourceSortList"] != null)
                                    //{
                                    //    dicSourceSortList = (Dictionary<string, string>)Session["SourceSortList"];
                                    //    UCRHSort.RadListBoxSource.DataSource = dicSourceSortList;
                                    //    UCRHSort.RadListBoxSource.DataValueField = "Key";
                                    //    UCRHSort.RadListBoxSource.DataTextField = "Value";
                                    //    UCRHSort.RadListBoxSource.DataBind();
                                    //}
                                    //else
                                    //{
                                    //    UCRHSort.RadListBoxSource.DataSource = dicColumnList;
                                    //    UCRHSort.RadListBoxSource.DataValueField = "Key";
                                    //    UCRHSort.RadListBoxSource.DataTextField = "Value";
                                    //    UCRHSort.RadListBoxSource.DataBind();
                                    //}

                                    //if (Session["DestinationSortList"] != null)
                                    //{
                                    //    dicSourceSortList = (Dictionary<string, string>)Session["DestinationSortList"];
                                    //    UCRHSort.RadListBoxDestination.DataSource = dicSourceSortList;
                                    //    UCRHSort.RadListBoxDestination.DataValueField = "Key";
                                    //    UCRHSort.RadListBoxDestination.DataTextField = "Value";
                                    //    UCRHSort.RadListBoxDestination.DataBind();
                                    //}
                                    //else
                                    //{
                                    //    UCRHSort.RadListBoxDestination.DataSource = dicEmptyList;
                                    //    UCRHSort.RadListBoxDestination.DataValueField = "Key";
                                    //    UCRHSort.RadListBoxDestination.DataTextField = "Value";
                                    //    UCRHSort.RadListBoxDestination.DataBind();
                                    //}
                                    ////Coded by srini



                                    // UCRHSort.ColumnList = dicColumnList;

                                    ////Commented by srini
                                    //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                    //{
                                    //    dicSelectedList.Add(SelectedItem.Value, SelectedItem.Text);
                                    //}

                                    //UCRHSort.RadListBoxSource.DataSource = dicSelectedList;
                                    //UCRHSort.RadListBoxSource.DataValueField = "Key";
                                    //UCRHSort.RadListBoxSource.DataTextField = "Value";
                                    //UCRHSort.RadListBoxSource.DataBind();


                                    //Dictionary<string, string> dicTempSelectedList = dicSelectedList;
                                    //foreach (KeyValuePair<string, string> pair in dicTempSelectedList)
                                    //{
                                    //    dicColumnList.Remove(pair.Key);
                                    //}
                                    //Dictionary<string, string> Empty = new Dictionary<string, string>();
                                    //UCRHSort.RadListBoxDestination.DataSource = Empty;
                                    //UCRHSort.RadListBoxDestination.DataBind();

                                    //Session["dicSelectedList"] = dicSelectedList;
                                    ////Commented by srini


                                    //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                    //{
                                    //    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                    //}

                                    //SelectionDestination.DataSource = dicSelectedList;
                                    //SelectionDestination.DataValueField = "Key";
                                    //SelectionDestination.DataTextField = "Value";
                                    //SelectionDestination.DataBind();

                                    //SelectionSource.DataSource = dicSelectedList;
                                    //SelectionSource.DataValueField = "Key";
                                    //SelectionSource.DataTextField = "Value";
                                    //SelectionSource.DataBind();
                                }

                                if (Wizard1.ActiveStep.ID == "WizardStep2")
                                {
                                    ////Coded by srini
                                    tbEditHeaderText.Text = string.Empty;
                                    dicSourceSortList = new Dictionary<string, string>();
                                    foreach (RadListBoxItem radlistitem in UCRHSort.RadListBoxSource.Items)
                                    {
                                        dicSourceSortList.Add(radlistitem.Value, radlistitem.Text);
                                    }
                                    Session["SourceSortList"] = dicSourceSortList;
                                    dicDestinationSortList = new Dictionary<string, string>();
                                    foreach (RadListBoxItem radlistitem in UCRHSort.RadListBoxDestination.Items)
                                    {
                                        dicDestinationSortList.Add(radlistitem.Value, radlistitem.Text);
                                    }
                                    Session["DestinationSortList"] = dicDestinationSortList;

                                    if (Session["DestinationList"] != null)
                                    {
                                        dicSourceList = (Dictionary<string, string>)Session["DestinationList"];
                                        SelectedSource.DataSource = dicSourceList;
                                        HeaderBySource.DataSource = dicSourceList;
                                    }
                                    else
                                    {
                                        SelectedSource.DataSource = dicEmptyList;
                                        HeaderBySource.DataSource = dicEmptyList;
                                    }
                                    SelectedSource.DataValueField = "Key";
                                    SelectedSource.DataTextField = "Value";
                                    SelectedSource.DataBind();
                                    HeaderBySource.DataValueField = "Key";
                                    HeaderBySource.DataTextField = "Value";
                                    HeaderBySource.DataBind();

                                    foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                    {
                                        dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                    }


                                    foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxSource.Items)
                                    {
                                        dicSelectedListsort.Add(SelectedItem.Value, SelectedItem.Text);
                                    }

                                    foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxDestination.Items)
                                    {
                                        dicSelectedListsortSel.Add(SelectedItem.Value, SelectedItem.Text);
                                    }

                                    Session["dicSelectedListSort"] = dicSelectedListsort;

                                    Session["dicSelectedListSortSel"] = dicSelectedListsortSel;

                                    SelectedSource.DataSource = dicSelectedList;
                                    SelectedSource.DataValueField = "Key";
                                    SelectedSource.DataTextField = "Value";
                                    SelectedSource.DataBind();

                                    HeaderBySource.DataSource = dicSelectedList;
                                    HeaderBySource.DataValueField = "Key";
                                    HeaderBySource.DataTextField = "Value";
                                    HeaderBySource.DataBind();

                                    ////Coded by srini

                                    ////Commented by srini
                                    //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                    //{
                                    //    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                    //}


                                    //foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxSource.Items)
                                    //{
                                    //    dicSelectedListsort.Add(SelectedItem.Value, SelectedItem.Text);
                                    //}

                                    //foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxDestination.Items)
                                    //{
                                    //    dicSelectedListsortSel.Add(SelectedItem.Value, SelectedItem.Text);
                                    //}

                                    //Session["dicSelectedListSort"] = dicSelectedListsort;

                                    //Session["dicSelectedListSortSel"] = dicSelectedListsortSel;

                                    //SelectedSource.DataSource = dicSelectedList;
                                    //SelectedSource.DataValueField = "Key";
                                    //SelectedSource.DataTextField = "Value";
                                    //SelectedSource.DataBind();

                                    //HeaderBySource.DataSource = dicSelectedList;
                                    //HeaderBySource.DataValueField = "Key";
                                    //HeaderBySource.DataTextField = "Value";
                                    //HeaderBySource.DataBind();
                                    ////Commented by srini
                                }

                                if (Wizard1.ActiveStep.ID == "WizardStep4")
                                {
                                    txtTitle.Text = lbTitle.Text;

                                    // uncmd this code after
                                    FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                                    using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        FPPrincipal identityname = (FPPrincipal)Session[Session.SessionID];
                                        var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == identity.Identity._customerID && x.HomebaseID == identity.Identity._homeBaseId).ToList();

                                        if (objCompany[0].HTMLHeader != string.Empty)
                                        {
                                            chkHeader.Enabled = false;
                                        }
                                        if (objCompany[0].HTMLFooter != string.Empty)
                                        {
                                            chkFooter.Enabled = false;
                                        }
                                        if (objCompany[0].HTMLBackgroun != string.Empty)
                                        {
                                            chkBackground.Enabled = false;
                                        }
                                        string[] strArr = GetReportTitle().Split(',');
                                        if (strArr.Count() > 0)
                                        {
                                            txtTitle.Text = Convert.ToString(strArr[0]);
                                        }
                                    }
                                }

                                //if (Wizard1.ActiveStep.ID == "WizardStep5")
                                //{
                                //    switch (ddlFormat.SelectedValue)
                                //    {
                                //        case "MHTML":
                                //            Wizard1.Visible = true;
                                //            Wizard1.MoveTo(WizardStep1);

                                //            break;
                                //        case "Excel":
                                //            string[] strArr = GetReportTitle().Split(',');
                                //            if (strArr.Count() > 0)
                                //            {
                                //                Session["REPORT"] = Convert.ToString(strArr[1] + "Export");
                                //            }
                                //            Session["FORMAT"] = "EXCEL";
                                //            Session["PARAMETER"] = ProcessResults();
                                //            Response.Redirect("ReportRenderView.aspx");
                                //            break;
                                //        default:
                                //            Wizard1.Visible = false;
                                //            break;
                                //    }
                                //}
                            }
                        }
                        else
                        {
                            lblError.Text = "Select the Fields";
                            e.Cancel = true;
                        }
                }
                
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void OnPrevious(object sender, WizardNavigationEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    
                        if (UCRHS.RadListBoxDestination.Items.Count > 0)
                        {
                            lblError.Text = "";

                            //if (Page.IsValid)
                            //{  // 
                            if (Wizard1.ActiveStep.ID == "WizardStep5")
                            {
                                switch (ddlFormat.SelectedValue)
                                {
                                    case "MHTML":
                                        Wizard1.Visible = true;
                                        Wizard1.MoveTo(WizardStep1);

                                        break;
                                    case "Excel":
                                        string[] strArr = GetReportTitle().Split(',');
                                        if (strArr.Count() > 0)
                                        {
                                            Session["REPORT"] = Convert.ToString(strArr[1] + "Export");
                                        }
                                        Session["FORMAT"] = "EXCEL";
                                        Session["PARAMETER"] = ProcessResults();
                                        Response.Redirect("ReportRenderView.aspx");
                                        break;
                                    default:
                                        Wizard1.Visible = false;
                                        break;
                                }
                            }

                            if (Wizard1.ActiveStep.ID == "WizardStep4")
                            {
                                ////code commented by srini
                                //txtTitle.Text = lbTitle.Text;

                                ////// uncmd this code after
                                //FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                                //using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                                //{
                                //    FPPrincipal identityname = (FPPrincipal)Session[Session.SessionID];
                                //    var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == identity.Identity._customerID && x.HomebaseID == identity.Identity._homeBaseId).ToList();

                                //    if (objCompany[0].HTMLHeader != string.Empty)
                                //    {
                                //        chkHeader.Enabled = false;
                                //    }
                                //    if (objCompany[0].HTMLFooter != string.Empty)
                                //    {
                                //        chkFooter.Enabled = false;
                                //    }
                                //    if (objCompany[0].HTMLBackgroun != string.Empty)
                                //    {
                                //        chkBackground.Enabled = false;
                                //    }
                                //    string[] strArr = GetReportTitle().Split(',');
                                //    if (strArr.Count() > 0)
                                //    {
                                //        txtTitle.Text = Convert.ToString(strArr[0]);
                                //    }
                                //}
                                ////code commented by srini

                                ////Coded by srini
                                if (Session["SourceSortList"] != null)
                                {
                                    dicSourceSortList = (Dictionary<string, string>)Session["SourceSortList"];
                                    UCRHSort.RadListBoxSource.DataSource = dicSourceSortList;
                                    UCRHSort.RadListBoxSource.DataValueField = "Key";
                                    UCRHSort.RadListBoxSource.DataTextField = "Value";
                                    UCRHSort.RadListBoxSource.DataBind();
                                }
                                else
                                {
                                    UCRHSort.RadListBoxSource.DataSource = dicColumnList;
                                    UCRHSort.RadListBoxSource.DataValueField = "Key";
                                    UCRHSort.RadListBoxSource.DataTextField = "Value";
                                    UCRHSort.RadListBoxSource.DataBind();
                                }
                                if (Session["DestinationSortList"] != null)
                                {
                                    dicDestinationSortList = (Dictionary<string, string>)Session["DestinationSortList"];
                                    UCRHSort.RadListBoxDestination.DataSource = dicDestinationSortList;
                                    UCRHSort.RadListBoxDestination.DataValueField = "Key";
                                    UCRHSort.RadListBoxDestination.DataTextField = "Value";
                                    UCRHSort.RadListBoxDestination.DataBind();
                                }
                                else
                                {
                                    UCRHSort.RadListBoxDestination.DataSource = dicColumnList;
                                    UCRHSort.RadListBoxDestination.DataValueField = "Key";
                                    UCRHSort.RadListBoxDestination.DataTextField = "Value";
                                    UCRHSort.RadListBoxDestination.DataBind();
                                }
                                ////Coded by srini
                            }

                            if (Wizard1.ActiveStep.ID == "WizardStep2")
                            {
                                //////code commented by srini
                                foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                {
                                    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                }


                                foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxSource.Items)
                                {
                                    dicSelectedListsort.Add(SelectedItem.Value, SelectedItem.Text);
                                }

                                foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxDestination.Items)
                                {
                                    dicSelectedListsortSel.Add(SelectedItem.Value, SelectedItem.Text);
                                }

                                Session["dicSelectedListSort"] = dicSelectedListsort;

                                Session["dicSelectedListSortSel"] = dicSelectedListsortSel;

                                SelectedSource.DataSource = dicSelectedList;
                                SelectedSource.DataValueField = "Key";
                                SelectedSource.DataTextField = "Value";
                                SelectedSource.DataBind();

                                HeaderBySource.DataSource = dicSelectedList;
                                HeaderBySource.DataValueField = "Key";
                                HeaderBySource.DataTextField = "Value";
                                HeaderBySource.DataBind();
                                //////code commented by srini

                                ////Coded by srini
                                if (Session["SourceList"] != null)
                                {
                                    dicSourceList = (Dictionary<string, string>)Session["SourceList"];
                                    UCRHS.RadListBoxSource.DataSource = dicSourceList;
                                    UCRHS.RadListBoxSource.DataValueField = "Key";
                                    UCRHS.RadListBoxSource.DataTextField = "Value";
                                    UCRHS.RadListBoxSource.DataBind();
                                }
                                else
                                {
                                    UCRHS.RadListBoxSource.DataSource = dicColumnList;
                                    UCRHS.RadListBoxSource.DataValueField = "Key";
                                    UCRHS.RadListBoxSource.DataTextField = "Value";
                                    UCRHS.RadListBoxSource.DataBind();
                                }
                                if (Session["DestinationList"] != null)
                                {
                                    dicDestinationList = (Dictionary<string, string>)Session["DestinationList"];
                                    UCRHS.RadListBoxDestination.DataSource = dicDestinationList;
                                    UCRHS.RadListBoxDestination.DataValueField = "Key";
                                    UCRHS.RadListBoxDestination.DataTextField = "Value";
                                    UCRHS.RadListBoxDestination.DataBind();
                                }
                                else
                                {
                                    UCRHS.RadListBoxDestination.DataSource = dicColumnList;
                                    UCRHS.RadListBoxDestination.DataValueField = "Key";
                                    UCRHS.RadListBoxDestination.DataTextField = "Value";
                                    UCRHS.RadListBoxDestination.DataBind();
                                }
                                ////Coded by srini
                            }

                            if (Wizard1.ActiveStep.ID == "WizardStep1")
                            {
                                // UCRHSort.ColumnList = dicColumnList;

                                foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                {
                                    dicSelectedList.Add(SelectedItem.Value, SelectedItem.Text);
                                }

                                UCRHSort.RadListBoxSource.DataSource = dicSelectedList;
                                UCRHSort.RadListBoxSource.DataValueField = "Key";
                                UCRHSort.RadListBoxSource.DataTextField = "Value";
                                UCRHSort.RadListBoxSource.DataBind();


                                Dictionary<string, string> dicTempSelectedList = dicSelectedList;
                                foreach (KeyValuePair<string, string> pair in dicTempSelectedList)
                                {
                                    dicColumnList.Remove(pair.Key);
                                }
                                Dictionary<string, string> Empty = new Dictionary<string, string>();
                                UCRHSort.RadListBoxDestination.DataSource = Empty;
                                UCRHSort.RadListBoxDestination.DataBind();

                                Session["dicSelectedList"] = dicSelectedList;



                                //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                //{
                                //    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                //}

                                //SelectionDestination.DataSource = dicSelectedList;
                                //SelectionDestination.DataValueField = "Key";
                                //SelectionDestination.DataTextField = "Value";
                                //SelectionDestination.DataBind();

                                //SelectionSource.DataSource = dicSelectedList;
                                //SelectionSource.DataValueField = "Key";
                                //SelectionSource.DataTextField = "Value";
                                //SelectionSource.DataBind();
                            }



                            //}
                        }
                        else
                        {
                            lblError.Text = "Select the Fields";
                            e.Cancel = true;

                        }
                }               
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {

        }

        public bool ValidateReport7Days()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnVal = false;

                if (!string.IsNullOrEmpty(tbBeginDate.Text) && !string.IsNullOrEmpty(tbBeginDate.Text))
                {
                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                    DateTime DateStart = new DateTime();
                    DateTime DateEnd = new DateTime();
                    DateTime TempDate = new DateTime();
                    if (!string.IsNullOrEmpty(tbBeginDate.Text))
                    {
                        if (DateFormat != null)
                        {
                            DateStart = Convert.ToDateTime(FormatDate(tbBeginDate.Text, DateFormat));
                        }
                        else
                        {
                            DateStart = Convert.ToDateTime(tbBeginDate.Text);
                        }
                    }
                    if (!string.IsNullOrEmpty(tbEndDate.Text))
                    {
                        if (DateFormat != null)
                        {
                            DateEnd = Convert.ToDateTime(FormatDate(tbEndDate.Text, DateFormat));
                        }
                        else
                        {
                            DateEnd = Convert.ToDateTime(tbEndDate.Text);
                        }
                    }


                    int Weeks = Convert.ToInt32(tbWeeks.Text.Trim());
                    TempDate = DateStart.AddDays((Weeks * 7) - 1);
                    int Difference = 0;
                    Difference = DateEnd.Subtract(DateStart).Days;
                    hdnEndDate.Value = String.Format("{0:" + DateFormat + "}", TempDate);
                    int Days = (Weeks * 7) - 1;

                    if (ReportString == "RptBusinessWeek")
                    {
                        TempDate = DateStart.AddDays((Weeks * 15) - 1);
                        Difference = 0;
                        Difference = DateEnd.Subtract(DateStart).Days;
                        hdnEndDate.Value = String.Format("{0:" + DateFormat + "}", TempDate);
                        Days = (Weeks * 15) - 1;
                    }

                    if ((Difference % Days) != 0)
                    {
                        ReturnVal = true;
                    }
                }
                return ReturnVal;
            }
        }

        public bool ValidateReportDate()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnVal = false;


                if (!string.IsNullOrEmpty(tbBeginDate.Text) && !string.IsNullOrEmpty(tbBeginDate.Text))
                {
                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                    DateTime DateStart = new DateTime();
                    DateTime DateEnd = new DateTime();
                    if (!string.IsNullOrEmpty(tbBeginDate.Text))
                    {
                        if (DateFormat != null)
                        {
                            DateStart = Convert.ToDateTime(FormatDate(tbBeginDate.Text, DateFormat));
                        }
                        else
                        {
                            DateStart = Convert.ToDateTime(tbBeginDate.Text);
                        }
                    }
                    if (!string.IsNullOrEmpty(tbEndDate.Text))
                    {
                        if (DateFormat != null)
                        {
                            DateEnd = Convert.ToDateTime(FormatDate(tbEndDate.Text, DateFormat));
                        }
                        else
                        {
                            DateEnd = Convert.ToDateTime(tbEndDate.Text);
                        }
                    }
                    if (DateStart > DateEnd)
                    {
                        ReturnVal = true;
                    }
                }

                return ReturnVal;
            }
        }
    }
}