﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Settings.master"
    AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="FlightPak.Web.Views.Reports.Reports" %>


<asp:Content ID="Content2" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js">
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <script type="text/javascript">

        function openReport(radWin) {
            var url = '';
            //Company
            if (radWin == "Accounts") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBAccounts.xml';
            }
            else if (radWin == "Budgets") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBBudgets.xml';
            }
            else if (radWin == "Departments") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBDepartmentAuthorization.xml';
            }
            else if (radWin == "Vendors") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBPayableVendors.xml';
            }
            else if (radWin == "Payments") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBPaymentTypes.xml';
            }
            else if (radWin == "Rates") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBSIFLRates.xml';
            }
            else if (radWin == "VendorList") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBVendorListing.xml';
            }

            //People
            else if (radWin == "CrewAddInfo") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBCrewAdditionalInformation.xml';
            }
            else if (radWin == "CrewChecklist") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBCrewChecklist.xml';
            }
            else if (radWin == "CrewDuty") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBCrewDutyTypes.xml';
            }
            else if (radWin == "CrewRoster") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBCrewRoster.xml';
            }
            else if (radWin == "FlightPurposes") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBFlightPurposes.xml';
            }
            else if (radWin == "PassengerAddInfo") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBPassengerAdditionalInformation.xml';
            }
            else if (radWin == "PassengersReq") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBPassengersRequestors.xml';
            }
            else if (radWin == "PassengersReqII") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBPassengersRequestorsII.xml';
            }

            //Fleet
            else if (radWin == "AircraftDuty") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBAircraftDutyTypes.xml';
            }
            else if (radWin == "AircraftType") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBAircraftType.xml';
            }
            else if (radWin == "DelayType") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBDelayTypes.xml';
            }
            else if (radWin == "FleetProf") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBFleetProfile.xml';
            }
            else if (radWin == "FlightCat") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBFlightCategories.xml';
            }

            //Logistics
            else if (radWin == "Airport") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBAirport.xml';
            }
            else if (radWin == "CustomAirports") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBCustomUVAirports.xml';
            }
            else if (radWin == "FuelLoc") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBFuelLocator.xml';
            }
            else if (radWin == "TripshtChk") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBTripsheetCheckList.xml';
            }
            else if (radWin == "TripshtChkGrp") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBTripsheetCheckListGroup.xml';
            }

            //Charter
            else if (radWin == "CharterQuote") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBCharterQuoteCustomer.xml';
            }
            else if (radWin == "Salespersons") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBSalespersons.xml';
            }

            //Administration
            else if (radWin == "Group") {
                url = 'ReportViewerModule.aspx?xmlFilename=DBUserGroupAcess.xml';
            }

            var oWnd = radopen(url, "RadReportsPopup");
        }
    </script>
    <%--
        var TotalChkBx;
        var Counter;

        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= this.gvLock.Rows.Count %>');

            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }

        function HeaderClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl =
       document.getElementById('<%= this.gvLock.ClientID %>');
            var TargetChildControl = "chkBxSelect";

            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");

            //Checked/Unchecked all the checkBoxes in side the GridView.
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' &&
                Inputs[n].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[n].checked = CheckBox.checked;

            //Reset Counter
            Counter = CheckBox.checked ? TotalChkBx : 0;
        }

        function ChildClick(CheckBox, HCheckBox) {
            //get target control.
            var HeaderCheckBox = document.getElementById(HCheckBox);

            //Modifiy Counter; 
            if (CheckBox.checked && Counter < TotalChkBx)
                Counter++;
            else if (Counter > 0)
                Counter--;

            //Change state of the header CheckBox.
            if (Counter < TotalChkBx)
                HeaderCheckBox.checked = false;
            else if (Counter == TotalChkBx)
                HeaderCheckBox.checked = true;
        }
    </script>--%>
    <%--<telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableCdn="true">
    </telerik:RadScriptManager>--%>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadReportsPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Reports/CalendarReports.aspx">
            </telerik:RadWindow>            
        </Windows>      
    </telerik:RadWindowManager>
    
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top">
                <table>
                    <tr>
                        <td>
                            <table  cellpadding="0" cellspacing="0" width="250px" class="reports_new">
                                <tr>
                                    <td class="tdlabel120" style="background-color: #E4E4E4;">
                                       <b>Company</b> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkAccounts" runat="server" Text="Accounts" OnClientClick="javascript:openReport('Accounts');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkBudgets" runat="server" Text="Budgets" OnClientClick="javascript:openReport('Budgets');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkDepartments" runat="server" Text="Department/Authorization"
                                            OnClientClick="javascript:openReport('Departments');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkVendors" runat="server" Text="Payable Vendors" OnClientClick="javascript:openReport('Vendors');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkPayments" runat="server" Text="Payment Types"
                                            OnClientClick="javascript:openReport('Payments');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkRates" runat="server" Text="SIFL Rates" OnClientClick="javascript:openReport('Rates');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkVendorList" runat="server" Text="Vendor Listing" OnClientClick="javascript:openReport('VendorList');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>                       
                                            
                            <table  cellpadding="0" cellspacing="0" width="250px" class="reports_new">
                                <tr>
                                    <td class="tdlabel120" style="background-color: #E4E4E4;">
                                       <b>Fleet</b> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkAircraftDuty" runat="server" Text="Aircraft Duty Types" OnClientClick="javascript:openReport('AircraftDuty');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkAircraftType" runat="server" Text="Aircraft Types" OnClientClick="javascript:openReport('AircraftType');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkDelayType" runat="server" Text="Delay Types" OnClientClick="javascript:openReport('DelayType');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkFleetProf" runat="server" Text="Fleet Profile" OnClientClick="javascript:openReport('FleetProf');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkFlightCat" runat="server" Text="Flight Categories" OnClientClick="javascript:openReport('FlightCat');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                                                  
                            <table  cellpadding="0" cellspacing="0" width="250px" class="reports_new">
                                <tr>
                                    <td class="tdlabel120" style="background-color: #E4E4E4;">
                                       <b>Charter</b> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkCharterQuote" runat="server" Text="Charter Quote Customer"
                                            OnClientClick="javascript:openReport('CharterQuote');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkSalespersons" runat="server" Text="Salespersons" OnClientClick="javascript:openReport('Salespersons');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>                       
                    </tr>
                </table>
            </td>
            <td valign="top">
                <table>
                    <tr>
                        <td>
                            <table  cellpadding="0" cellspacing="0" width="250px" class="reports_new">
                                <tr>
                                    <td class="tdlabel120" style="background-color: #E4E4E4;">
                                        <b>Crew/PAX</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkCrewAddInfo" runat="server" Text="Crew Additional Information"
                                            OnClientClick="javascript:openReport('CrewAddInfo');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkCrewChecklist" runat="server" Text="Crew Checklist" OnClientClick="javascript:openReport('CrewChecklist');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkCrewDuty" runat="server" Text="Crew Duty Types" OnClientClick="javascript:openReport('CrewDuty');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkCrewRoster" runat="server" Text="Crew Roster" OnClientClick="javascript:openReport('CrewRoster');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkFlightPurposes" runat="server" Text="Flight Purposes" OnClientClick="javascript:openReport('FlightPurposes');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkPassengerAddInfo" runat="server" Text="Passenger Additional Info"
                                            OnClientClick="javascript:openReport('PassengerAddInfo');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkPassengersReq" runat="server" Text="Passengers/Requestors"
                                            OnClientClick="javascript:openReport('PassengersReq');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkPassengersReqII" runat="server" Text="Passengers/Requestors II"
                                            OnClientClick="javascript:openReport('PassengersReqII');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                           
                            <table  cellpadding="0" cellspacing="0" width="250px" class="reports_new">
                                <tr>
                                    <td class="tdlabel120" style="background-color: #E4E4E4;">
                                       <b>Logistics</b> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkAirport" runat="server" Text="Airport" OnClientClick="javascript:openReport('Airport');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkCustomUWA" runat="server" Text="Custom/UWA-Maintained Airports"
                                            OnClientClick="javascript:openReport('CustomAirports');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkFuelLoc" runat="server" Text="Fuel Locators" OnClientClick="javascript:openReport('FuelLoc');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkTripshtChk" runat="server" Text="Tripsheet Checklist" OnClientClick="javascript:openReport('TripshtChk');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkTripshtChkGrp" runat="server" Text="Tripsheet Checklist Group"
                                            OnClientClick="javascript:openReport('TripshtChkGrp');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                           
                            <table  cellpadding="0" cellspacing="0" width="250px" class="reports_new">
                                <tr>
                                    <td class="tdlabel120" style="background-color: #E4E4E4;">
                                       <b>Administration</b> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkGroup" runat="server" Text="Group" OnClientClick="javascript:openReport('Group');return false;"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</asp:Content>
