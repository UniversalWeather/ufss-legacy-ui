﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileFormatReport.aspx.cs"
    Inherits="FlightPak.Web.Views.Reports.FileFormatReport" ValidateRequest="false" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Edit Overview Parameters</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <link href="../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/Common.js") %>"></script>
    <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
    <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jquery.alerts.js") %>"></script>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function chkSelectAll_onchange() {
                var IsSelected;
                if (document.getElementById("<%=chkSelectAll.ClientID %>").checked == true) {
                    IsSelected = "Y";
                }
                else {
                    IsSelected = "N";
                }
                var grid = $find("<%= dgCharterQuoteDetail.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var length = MasterTable.get_dataItems().length;
                for (var i = 0; i < length; i++) {
                    MasterTable.get_dataItems()[i].findElement("tbIsSelected").value = IsSelected;
                }
                return false;
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            var oArg = new Object();
            function Close(param) {
                oArg = new Object();
                oArg.IsPrint = param;
                if (oArg) {
                    GetRadWindow().Close(oArg);
                }
            }
            function fnAllowYN(myfield, e) {
                var key;
                var keychar;

                if (window.event)
                    key = window.event.keyCode;
                else if (e)
                    key = e.which;
                else
                    return true;
                keychar = String.fromCharCode(key);

                // control keys
                if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
                    return true;
                // numbers
                else if (("YNyn").indexOf(keychar) > -1)
                    return true;
                else
                    return false;
            }
            function tbIsSelected_onblur(ctrl) {
                if (ctrl.value == "") {
                    ctrl.value = "N";
                }
                return false;
            }
            function tbMessage_onblur(ctrl) {
                if (ctrl.value == "") {
                    ctrl.value = "";
                }
                return false;
            }
            function tbNumber_onblur(ctrl) {
                if (ctrl.value == "") {
                    ctrl.value = "";
                }
                return false;
            }

            function CheckRange(ctrl, UpperBound, LowerBound, WarningMessage) {
                var ReturnValue = false;
                var ctrlid = document.getElementById(ctrl);
                var CheckRangeFnCallBack = Function.createDelegate(ctrlid, function (shouldSubmit) {
                    ctrlid.value = LowerBound;
                    ctrlid.focus();
                    return ReturnValue;
                });
                if (ctrlid.value != "") {
                    if ((parseInt(ctrlid.value) >= LowerBound) && (parseInt(ctrlid.value) <= UpperBound)) {
                    }
                    else {
                        WarningMessage = WarningMessage.replace("$d$", ctrlid.value);
                        radalert(WarningMessage, 360, 50, 'Charter Quote Writer', CheckRangeFnCallBack);
                    }
                } else {
                    WarningMessage = WarningMessage.replace("$d$", "empty");
                    radalert(WarningMessage, 360, 50, 'Charter Quote Writer', CheckRangeFnCallBack);
                }
                return ReturnValue;
            }

            function tbMessage_onDblclick(txtctrl) {
                var ctrlMessage, ctrlTripSheetReportDetailID, ctrlMessageLength, ctrlWarningMessage;
                if (txtctrl.id.indexOf("tbMessage") != -1) {
                    ctrlMessage = document.getElementById(txtctrl.id.replace("tbMessage", "hdnMessage"));
                    ctrlTripSheetReportDetailID = document.getElementById(txtctrl.id.replace("tbMessage", "hdnGrdTripSheetReportDetailID"));
                    ctrlMessageLength = document.getElementById(txtctrl.id.replace("tbMessage", "hdnMessageLength"));
                    ctrlWarningMessage = document.getElementById(txtctrl.id.replace("tbMessage", "hdnWarningMessage"));
                }
                OpenMessagePopup(ctrlMessage, ctrlTripSheetReportDetailID, ctrlMessageLength, ctrlWarningMessage);
            }

            function OpenMessagePopup(ctrlMessage, ctrlTripSheetReportDetailID, ctrlMessageLength, ctrlWarningMessage) {
                var oWnd1 = $find("<%=RadwinMessagePopup.ClientID%>");
                document.getElementById('<%=tbFRMessage.ClientID%>').value = ctrlMessage.value;
                document.getElementById('<%=hdnFRMessageID.ClientID%>').value = ctrlTripSheetReportDetailID.value;
                document.getElementById('<%=hdnFRMessageLength.ClientID%>').value = ctrlMessageLength.value;
                document.getElementById('<%=lbFRWarningMessage.ClientID%>').innerText = ctrlWarningMessage.value;
                oWnd1.show();
            }

            function CloseMessagePopup() {
                return false;
            }

            function tbFRMessage_onkeypress(ctrl, evnt) {
                var SplitSize = document.getElementById('<%=hdnFRMessageLength.ClientID%>').value.split("-");
                var MaxLength;
                if ((SplitSize.length != 0) && (SplitSize.length == 2))
                    MaxLength = SplitSize[1];
                if (MaxLength.toUpperCase() != "N")
                    document.getElementById('<%=tbFRMessage.ClientID%>').value = document.getElementById('<%=tbFRMessage.ClientID%>').value.substring(0, MaxLength - 1);
                return false;
            }           
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <%--<telerik:AjaxSetting AjaxControlID="DivExternalForm">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>--%>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="RadwinMessagePopup" runat="server" VisibleOnPageLoad="false"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                    Behaviors="close" Title="Edit Message" OnClientClose="CloseMessagePopup">
                    <ContentTemplate>
                        <div>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="tbFRMessage" runat="server" TextMode="MultiLine" Rows="15" Columns="100"
                                            MaxLength="1500" onkeypress="javascript:tbFRMessage_onkeypress(this, event);"></asp:TextBox>
                                        <asp:Label ID="lbFRWarningMessage" runat="server" CssClass="alert-text" />
                                        <asp:HiddenField ID="hdnFRMessageLength" runat="server" />
                                        <asp:HiddenField ID="hdnFRMessageID" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="custom_radbutton" align="right">
                                        <telerik:RadButton ID="btnFRSaveMessage" runat="server" Text="Save" OnClick="btnFRSaveMessage_OnClick" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ContentTemplate>
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <div id="DivExternalForm" runat="server" class="ExternalForm">
            <asp:Panel ID="pnlExternalPanel" runat="server" Visible="true">
                <table width="100%" class="box1">
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkSelectAll" runat="server" Text="Select All" AutoPostBack="true"
                                OnCheckedChanged="chkSelectAll_OnCheckedChanged" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadGrid ID="dgCharterQuoteDetail" runat="server" EnableAJAX="True" AllowMultiRowSelection="false"
                                OnNeedDataSource="dgCharterQuoteDetail_BindData" OnItemDataBound="dgCharterQuoteDetail_ItemDataBound"
                                Height="341px" Width="700px">
                                <MasterTableView AutoGenerateColumns="False" EditMode="InPlace" TableLayout="Fixed"
                                    DataKeyNames="CQReportDetailID,CustomerID,CQReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,ReportProcedure,
                                            ParameterCD,ParamterDescription,ParamterType,ParameterWidth,ParameterCValue,ParameterLValue,ParameterNValue,ParameterValue,
                                            ReportOrder,LastUpdUID,LastUpdTS,IsDeleted,ParameterSize,WarningMessage"
                                    CommandItemDisplay="None" ShowFooter="false" AllowFilteringByColumn="false" AllowPaging="false"
                                    NoMasterRecordsText="There are no parameters available for customization on this report.">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="CQReportDetailID" HeaderText="CQReportDetailID"
                                            UniqueName="CQReportDetailID" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                            AutoPostBackOnFilter="true" Display="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="Parameter Description" UniqueName="ParamterDescription"
                                            CurrentFilterFunction="Contains" ShowFilterIcon="false" AutoPostBackOnFilter="true"
                                            HeaderStyle-Width="200px">
                                            <ItemTemplate>
                                                <asp:Label ID="ParameterDescription" runat="server" Text='<%# Eval("ParamterDescription") %>'></asp:Label>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Y/N" UniqueName="IsSelected" CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false" AllowFiltering="false" HeaderStyle-Width="50px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tbIsSelected" runat="server" Text='<%# Eval("ParameterLValue") %>'
                                                    MaxLength="1" onKeyPress="return fnAllowYN(this, event)" onblur="javascript:return tbIsSelected_onblur(this);"></asp:TextBox>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Number" UniqueName="Number" CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false" AllowFiltering="false" HeaderStyle-Width="120px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tbNumber" runat="server" Text='<%# Eval("ParameterNValue") %>' MaxLength="4"
                                                    onKeyPress="return fnAllowNumeric(this, event)" onblur="javascript:return tbNumber_onblur(this);"></asp:TextBox>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Message" UniqueName="Message" CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false" AllowFiltering="false" HeaderStyle-Width="120px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tbMessage" runat="server" Text='<%# Eval("ParameterValue") %>' MaxLength="7"
                                                    onblur="javascript:return tbMessage_onblur(this);" onDblclick="javascript:tbMessage_onDblclick(this);"></asp:TextBox>
                                                <asp:HiddenField ID="hdnMessage" runat="server" Value='<%# Eval("ParameterValue") %>' />
                                                <asp:HiddenField ID="hdnMessageLength" runat="server" Value='<%# Eval("ParameterSize") %>' />
                                                <asp:HiddenField ID="hdnWarningMessage" runat="server" Value='<%# Eval("WarningMessage") %>' />
                                                <asp:HiddenField ID="hdnGrdTripSheetReportDetailID" runat="server" Value='<%# Eval("CQReportDetailID") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                        <asp:Label ID="lbCustom1" runat="server" Text="See Custom Flight Log option in Company Profile, Preflight Tab, for additional settings."
                                            Visible="true" />
                                    </CommandItemTemplate>
                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                </MasterTableView>
                                <ClientSettings>
                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                                <GroupingSettings CaseSensitive="false" />
                            </telerik:RadGrid>
                            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                                <tr>
                                    <td class="custom_radbutton" align="right">
                                        <telerik:RadButton ID="btnPrint" runat="server" Text="Print" OnClick="btnPrint_OnClick" />
                                        <telerik:RadButton ID="btnSave" runat="server" Text="Save" OnClick="btnSave_OnClick" />
                                        <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" OnClientClicking="Close" />
                                        <asp:Label ID="InjectScript" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdnIsPrint" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </div>
    </form>
</body>
</html>
