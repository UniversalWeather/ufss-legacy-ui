﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Reports
{
    public partial class QuoteLegsPopup : BaseSecuredPage
    {
        private Int64 CQMainID;
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Added for Reassign the Selected Value and highlight the specified row in the Grid
                        if (!string.IsNullOrEmpty(Request.QueryString["MainID"]))
                        {
                            CQMainID = Convert.ToInt64(Request.QueryString["MainID"]);
                        }
                        if (!string.IsNullOrEmpty(Request.QueryString["MultiSelect"]) && Request.QueryString["MultiSelect"] == "true")
                        {
                            dgCharterLeg.AllowMultiRowSelection = true;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                }
            }
        }
        /// <summary>
        /// Bind Country Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgCharterLeg_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CharterQuoteService.CharterQuoteServiceClient QuoteService = new CharterQuoteService.CharterQuoteServiceClient())
                        {
                            var QuoteVal = QuoteService.GetCQLegsByQuoteID(CQMainID);
                            if (QuoteVal.ReturnFlag == true)
                            {
                                dgCharterLeg.DataSource = QuoteVal.EntityList.OrderBy(x => x.LegNUM);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                }
            }

        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgCharterLeg;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                }
            }
        }
    }
}