﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuoteOnFilePopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Reports.QuoteOnFilePopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Quote On File</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript">
                var oArg = new Object();
                var grid = $find("<%= dgCharterQuote.ClientID %>");

                function GetRadWindow() {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow;
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                    return oWindow;
                }

                function returnToParent() {
                    //create the argument that will be returned to the parent page
                    oArg = new Object();
                    grid = $find("<%= dgCharterQuote.ClientID %>");
                    var MasterTable = grid.get_masterTableView();
                    var selectedRows = MasterTable.get_selectedItems();
                    var SelectedMain = false;
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "QuoteNUM");
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "CQMainID");
                    }

                    if (selectedRows.length > 0) {
                        oArg.QuoteNUM = cell1.innerHTML;
                        oArg.CQMainID = cell2.innerHTML;
                    }
                    else {
                        oArg.QuoteNUM = "";
                        oArg.CQMainID = "";
                    }

                    var oWnd = GetRadWindow();
                    if (oArg) {
                        oWnd.close(oArg);
                    }
                }

                function Close() {
                    GetRadWindow().Close();
                }
            </script>
        </telerik:RadCodeBlock>
        <div class="divGridPanel">
            <asp:ScriptManager ID="scr1" runat="server">
            </asp:ScriptManager>
            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
                <AjaxSettings>
                    <telerik:AjaxSetting AjaxControlID="dgCharterQuote">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="dgCharterQuote" LoadingPanelID="RadAjaxLoadingPanel1" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                </AjaxSettings>
            </telerik:RadAjaxManager>
            <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
            <telerik:RadGrid ID="dgCharterQuote" runat="server" AllowMultiRowSelection="false"
                AllowSorting="false" AutoGenerateColumns="false" Height="330px" AllowPaging="false"
                Width="500px" OnNeedDataSource="dgCharterQuote_BindData" AllowFilteringByColumn="false">
                <MasterTableView CommandItemDisplay="Bottom" AllowFilteringByColumn="false">
                    <Columns>
                        <telerik:GridBoundColumn DataField="FileNUM" HeaderText="File No." ShowFilterIcon="false"
                            Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CQMainID" HeaderText="CQMainID" ShowFilterIcon="false"
                            AllowFiltering="false" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="QuoteNUM" HeaderText="Quote No." ShowFilterIcon="false"
                            HeaderStyle-Width="70px" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CQSource" HeaderText="CQSource" ShowFilterIcon="false"
                            HeaderStyle-Width="70px" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="VendorCD" HeaderText="Vendor Code" ShowFilterIcon="false"
                            HeaderStyle-Width="90px" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail No." ShowFilterIcon="false"
                            HeaderStyle-Width="70px" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AircraftCD" HeaderText="Aircraft Code" ShowFilterIcon="false"
                            HeaderStyle-Width="90px" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FlightHoursTotal" HeaderText="Total Charges"
                            ShowFilterIcon="false" HeaderStyle-Width="90px" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="IsFinancialWarning" HeaderText="Financial Warning"
                            ShowFilterIcon="false" HeaderStyle-Width="60px" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="TripStatus" HeaderText="TS Status" ShowFilterIcon="false"
                            HeaderStyle-Width="80px" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="TripNUM" HeaderText="Trip No." ShowFilterIcon="false"
                            HeaderStyle-Width="60px" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn HeaderText="Exception" ShowFilterIcon="false" UniqueName="FileExcep"
                            AllowFiltering="false" HeaderStyle-Width="70px">
                            <ItemTemplate>
                                <asp:Label ID="lbException" runat="server"></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Quote Status" ShowFilterIcon="false" UniqueName="QuoteStatus"
                            AllowFiltering="false" HeaderStyle-Width="90px">
                            <ItemTemplate>
                                <asp:Label ID="lbQuoteStatus" runat="server"></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="LastAcceptDT" HeaderText="Accepted Date" ShowFilterIcon="false"
                            HeaderStyle-Width="110px" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="IsLog" HeaderText="Log No." ShowFilterIcon="false"
                            HeaderStyle-Width="65px" AllowFiltering="false">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div class="grd_ok">
                            <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                                Ok</button>
                            <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings>
                    <ClientEvents OnRowDblClick="returnToParent" />
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
        </div>
    </div>
    </form>
</body>
</html>
