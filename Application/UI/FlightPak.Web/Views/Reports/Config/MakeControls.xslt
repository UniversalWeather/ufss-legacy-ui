﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:UCMultiSelect="FlightPak.Web.UserControls" xmlns:UC="FlightPak.Web.UserControls"

xmlns:asp="remove">

  <xsl:output method="xml" indent="yes" encoding="utf-8"/>

  <xsl:template match="Controls">

    <table >

      <xsl:for-each select="//searchfield">



        <tr>

          <td valign="top" class="tdLabel100">



            <xsl:choose>

              <xsl:when test="@required = 'true'">

                <asp:Label ID="lb{@name}" runat="server" Visible="true" Text="{@displayname}" CssClass="mnd_text"></asp:Label>

                <!--<b>

                  <font color="black" >

                    <xsl:value-of select="@displayname"/>

                  </font>

                </b>-->

              </xsl:when>

              <xsl:when test="@type = 'message'">
                <td colspan="2">
                  <asp:Label ID="lbmsg{@name}" runat="server" Visible="true" Text="{@displayname}" CssClass="alert-text"></asp:Label>
                </td>
              </xsl:when>
              
              <xsl:otherwise>

                <xsl:if test="@type!='checkbox'">

                  <asp:Label ID="lb{@name}" runat="server" Visible="true" Text="{@displayname}" ></asp:Label>
                  <!--<xsl:value-of select="@displayname"/>-->

                </xsl:if>

              </xsl:otherwise>

            </xsl:choose>

            

            <xsl:value-of select="@EType" />



          </td>

          <td>

            <xsl:if test="@type='text'">

              <asp:TextBox id="{@name}" onkeypress="javascript:handle(event);" runat="server"  Text="{@text}" ClientIDMode="Static"/>

            </xsl:if>
            

            <xsl:if test="@type='UC'">

              <UCMultiSelect:UCMS id="{@name}" runat="server"  LoadEntity="{@EType}" />

            </xsl:if>



            <xsl:if test="@type='Date'">

              <UC:DatePicker id="{@name}" runat="server" Required="{@required}"/>

            </xsl:if>



            <xsl:if test="@type='monthview'">

              <UC:MonthView id="{@name}" runat="server" Required="{@required}"/>

            </xsl:if>



            <xsl:if test="@type='monthyear'">

              <UC:MonthYear id="{@name}" runat="server" IsYear="{@isyear}"/>

            </xsl:if>

            <xsl:if test="@type='ddlMonth'">
              <xsl:choose>
                <xsl:when test="@name='MONTHTO'">
                  <asp:DropDownList id="{@name}" runat="server" >
                    <asp:ListItem Value="1">January</asp:ListItem>
                    <asp:ListItem Value="2">February</asp:ListItem>
                    <asp:ListItem Value="3">March</asp:ListItem>
                    <asp:ListItem Value="4">April</asp:ListItem>
                    <asp:ListItem Value="5">May</asp:ListItem>
                    <asp:ListItem Value="6">June</asp:ListItem>
                    <asp:ListItem Value="7">July</asp:ListItem>
                    <asp:ListItem Value="8">August</asp:ListItem>
                    <asp:ListItem Value="9">September</asp:ListItem>
                    <asp:ListItem Value="10">October</asp:ListItem>
                    <asp:ListItem Value="11">November</asp:ListItem>
                    <asp:ListItem Value="12" Selected="True">December</asp:ListItem>
                  </asp:DropDownList>
                </xsl:when>
                <xsl:otherwise>
                  <asp:DropDownList id="{@name}" runat="server" >
                    <asp:ListItem Value="1" Selected="True">January</asp:ListItem>
                    <asp:ListItem Value="2">February</asp:ListItem>
                    <asp:ListItem Value="3">March</asp:ListItem>
                    <asp:ListItem Value="4">April</asp:ListItem>
                    <asp:ListItem Value="5">May</asp:ListItem>
                    <asp:ListItem Value="6">June</asp:ListItem>
                    <asp:ListItem Value="7">July</asp:ListItem>
                    <asp:ListItem Value="8">August</asp:ListItem>
                    <asp:ListItem Value="9">September</asp:ListItem>
                    <asp:ListItem Value="10">October</asp:ListItem>
                    <asp:ListItem Value="11">November</asp:ListItem>
                    <asp:ListItem Value="12">December</asp:ListItem>
                  </asp:DropDownList>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:if>

            <xsl:if test="@type='hiddenfield'">

              <asp:HiddenField id="{@name}" runat="server" value="{@fieldvalue}" />

            </xsl:if>



            <xsl:if test="@type='popup'">

              <xsl:if test="@name='AuthorizationCD'">

                <asp:TextBox id="{@name}" AutoPostBack="true" OnTextChanged="Ctrl_OnTextChanged" runat="server" onkeypress="javascript:handle(event);" ClientIDMode="Static"/>

                <button id="{@name}" onclick="javascript:openAuthWin('{@popupURL}',document.getElementById('hdnDept').value,'RadWindow1');return false;"

                  class="browse-button">

                </button>
                <td>
                  <asp:Label ID="lbcv{@name}" CssClass="alert-text" runat="server" Visible="true"  ClientIDMode="Static"></asp:Label>
                </td>
              </xsl:if>

              <xsl:if test="@name='DepartmentCD'">

                <asp:TextBox id="{@name}" AutoPostBack="true" OnTextChanged="Ctrl_OnTextChanged" runat="server" onkeypress="javascript:handle(event);" ClientIDMode="Static"/>

                <button id="{@name}" onclick="javascript:openDeptWin('{@popupURL}',document.getElementById('{@name}').value,'RadWindow1');return false;"

                  class="browse-button">

                </button>
                <td>
                  <asp:Label ID="lbcv{@name}" CssClass="alert-text" runat="server" Visible="true"  ClientIDMode="Static"></asp:Label>
                </td>
              </xsl:if>

              <xsl:if test="@name='DepartmentGroupID'">

                <asp:TextBox id="{@name}" AutoPostBack="true" OnTextChanged="Ctrl_OnTextChanged" runat="server" onkeypress="javascript:handle(event);" ClientIDMode="Static"/>

                <button id="{@name}" onclick="javascript:openDeptGrpWin('{@popupURL}',document.getElementById('{@name}').value,'RadWindow1');return false;"

                  class="browse-button">

                </button>
                <td>
                  <asp:Label ID="lbcv{@name}" CssClass="alert-text" runat="server" Visible="true"  ClientIDMode="Static"></asp:Label>
                </td>
              </xsl:if>

              <xsl:if test="@name='CRLegNum'">

                <asp:TextBox id="{@name}" runat="server" onkeypress="javascript:handle(event);" ClientIDMode="Static"/>

                <button id="{@name}" onclick="javascript:openWin('{@popupURL}',document.getElementById('hdnCRMainID').value,'RadWindow1');return false;"

                  class="browse-button">

                </button>

              </xsl:if>
              <xsl:if test="@name='LegNum'">

                <asp:TextBox id="{@name}" runat="server" AutoPostBack="true" OnTextChanged="Ctrl_OnTextChanged"  Enabled="false" onkeypress="javascript:handle(event);" ClientIDMode="Static"/>

                <button id="{@name}" onclick="javascript:openWin('{@popupURL}',document.getElementById('hdnMainID').value,'RadWindow1');return false;"

                  class="browse-button">

                </button>
                <td>
                  <asp:Label ID="lbcv{@name}" CssClass="alert-text" runat="server" Visible="true"  ClientIDMode="Static"></asp:Label>
                </td>
              </xsl:if>

              <xsl:if test="@name!='AuthorizationCD'">

                <xsl:if test="@name!='CRLegNum'">
                  
                  <xsl:if test="@name!='LegNum'">

                    <xsl:if test="@name!='DepartmentCD'">

                      <xsl:if test="@name!='DepartmentGroupID'">

                        <asp:TextBox id="{@name}" AutoPostBack="true" OnTextChanged="Ctrl_OnTextChanged" onkeypress="javascript:handle(event);" runat="server" ClientIDMode="Static"/>

                        <button id="{@name}" onclick="javascript:openWin('{@popupURL}',document.getElementById('{@name}').value,'RadWindow1');return false;"

                          class="browse-button">

                        </button>
                        <td>
                          <asp:Label ID="lbcv{@name}" CssClass="alert-text" runat="server" Visible="true"  ClientIDMode="Static"></asp:Label>
                        </td>
                      </xsl:if>

                    </xsl:if>
                    
                  </xsl:if>

                </xsl:if>

              </xsl:if>

            </xsl:if>


            <xsl:if test="@type!='Date'">

              <xsl:if test="@required ='true'">

                <td>
                  <asp:RequiredFieldValidator id ="rfv{@name}" CssClass="alert-text" ErrorMessage=" Required Field" runat="server" ControlToValidate="{@name}" Display="Dynamic"/>
                </td>

              </xsl:if>

            </xsl:if>

            <xsl:if test="@type='radio'">

              <asp:RadioButtonList id="{@name}" runat="server" >

                <xsl:for-each select="choice">


                  <xsl:choose>
                    <xsl:when test="@selected ='true'">
                      <asp:ListItem  Text="{@text}"  value ="{@value}" Selected="True" />
                    </xsl:when>
                    <xsl:otherwise>
                      <asp:ListItem  Text="{@text}"  value ="{@value}"/>
                    </xsl:otherwise>
                  </xsl:choose>

                </xsl:for-each>

              </asp:RadioButtonList>

            </xsl:if>

            
            
            
          </td>

        </tr>
      </xsl:for-each>
      <tr>

        <td colspan="2">
          <table>
            <xsl:for-each select="//searchfield">
              <tr>
                <td >
                  <xsl:if test="@type='checkbox'">
                    <xsl:choose>
                      <xsl:when  test="@checked='true'">
                        <asp:CheckBox id="{@name}" runat="server" Checked="true" Text= "{@displayname}" title="{@title}"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <asp:CheckBox id="{@name}" runat="server" Text= "{@displayname}" title="{@title}"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:if>
                </td>

              </tr>
            </xsl:for-each>
          </table>



        </td>

      </tr>



    </table>



  </xsl:template>

</xsl:stylesheet>




