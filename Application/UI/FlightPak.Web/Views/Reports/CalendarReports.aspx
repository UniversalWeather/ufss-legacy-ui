﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CalendarReports.aspx.cs"
    ClientIDMode="AutoID" Inherits="FlightPak.Web.Views.Reports.CalendarReports" %>

<%@ Register Src="~/UserControls/UCReportHtmlSelection.ascx" TagName="Grid" TagPrefix="UCReportHtmlSelection" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="box1">
<head runat="server">
    <title>Report Criteria</title>
</head>
<body>
    <script src="/Scripts/jquery.js"></script>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript">

        function getQueryVariable(variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    return pair[1];
                }
            }
        }

        var currentTextBox = null;
        var currentDatePicker = null;
        //This method is called to handle the onclick and onfocus client side events for the texbox
        function showPopup(sender, e) {
            //this is a reference to the texbox which raised the event
            //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html
            currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);
            //this gets a reference to the datepicker, which will be shown, to facilitate
            //the selection of a date
            var datePicker = $find("<%= RadDatePicker1.ClientID %>");
            //this variable is used to store a reference to the date picker, which is currently
            //active
            currentDatePicker = datePicker;
            //this method first parses the date, that the user entered or selected, and then
            //sets it as a selected date to the picker
            datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));
            //the code lines below show the calendar, which is used to select a date. The showPopup
            //function takes three arguments - the x and y coordinates where to show the calendar, as
            //well as its height, derived from the offsetHeight property of the textbox
            var position = datePicker.getElementPosition(currentTextBox);
            datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
        }
        //this handler is used to set the text of the TextBox to the value of selected from the popup


        //this handler is used to set the text of the TextBox to the value of selected from the popup
        function dateSelected(sender, args) {


            if (currentTextBox != null) {
                var step = "tbWeeks_TextChanged";

                if (currentTextBox.value != args.get_newValue()) {

                    currentTextBox.value = args.get_newValue();
                    var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                    var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                    if (step) {
                        ajaxManager.ajaxRequest(step);
                    }
                }
            }
        }



        function tbDate_OnKeyDown(sender, event) {
            if (event.keyCode == 9) {
                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                datePicker.hidePopup();
                return true;
            }
        }

        function ValidateDate(control) {
            var MinDate = new Date("01/01/1900");
            var MaxDate = new Date("12/31/2100");
            var SelectedDate;
            if (control.value != "") {
                SelectedDate = new Date(control.value);
                if ((SelectedDate < MinDate) || (SelectedDate > MaxDate)) {
                    radalert("Please enter or select Date between 01/01/1900 and 12/31/2100", 400, 100, "Reports Criteria");
                    control.value = "";
                }
            }
            return false;
        }

        //this function is used to parse the date entered or selected by the user
        function parseDate(sender, e) {
            if (currentDatePicker != null) {
                var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                var dateInput = currentDatePicker.get_dateInput();
                var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                sender.value = formattedDate;
            }
        }

    </script>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function buttonclose() {
                var oWnd = GetRadWindow();
                oWnd.close();
            }
            function Close() {
                GetRadWindow().Close();
            }

            function alertCrewCallBackFn(arg) {

            }

            function alertCrewCallVisiBackFn(arg) {
                document.getElementById('<%=tbEndDate.ClientID%>').value = document.getElementById('<%=hdnEndDate.ClientID%>').value;
            }

            function LoadReport() {
                $.get("ReportRenderView.aspx", function (d) {
                    $("#PDFViewerFrame").attr("src", "PDFViewer.aspx").show();
                });
            };

        </script>
    </telerik:RadCodeBlock>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnablePageMethods="true">
    </telerik:RadScriptManager>
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" MinDate="01/01/1900"
        MaxDate="12/31/2100" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server" ShowRowHeaders="false">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                    ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="tbBeginDate">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbEndDate">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbWeeks">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
        </Windows>
    </telerik:RadWindowManager>
    <div class="art-setting-content">
        <div class="art-setting-sidebar">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <telerik:RadPanelBar runat="server" ExpandMode="SingleExpandedItem" ID="pnlReportTab"
                            AllowCollapseAllItems="true" CssClass="RadNavMenu">
                            <CollapseAnimation Type="None"></CollapseAnimation>
                            <ExpandAnimation Type="None"></ExpandAnimation>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
        </div>
        <div id="DivExternalForm" runat="server" class="ExternalForm">
            <table class="box1" width="100%">
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lbBeginDate" runat="server" Text="Begin Date" CssClass="mnd_text"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="tbBeginDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                        AutoPostBack="true" OnTextChanged="tbWeeks_TextChanged" onclick="showPopup(this, event);"
                                        onfocus="showPopup(this, event);" MaxLength="10" onblur="parseDate(this, event); ValidateDate(this);"
                                        onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                    <asp:HiddenField ID="hdnReportName" runat="server" />
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvBeginDate" runat="server" ControlToValidate="tbBeginDate"
                                        ValidationGroup="Save" ErrorMessage="Begin Date is Required" Display="Dynamic"
                                        CssClass="alert-text" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lbWeeks" runat="server" Text="No. of Weeks" CssClass="mnd_text"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="tbWeeks" runat="server" Text="1" OnTextChanged="tbWeeks_TextChanged"
                                        MaxLength="2" AutoPostBack="true" CssClass="tdLabel20"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvWeek" runat="server" ControlToValidate="tbWeeks"
                                        ValidationGroup="Save" ErrorMessage="Week Entry is Required" Display="Dynamic"
                                        CssClass="alert-text" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lbEndDate" runat="server" Text="End Date" CssClass="mnd_text"></asp:Label>
                                </td>
                                <td class="tdenddate">
                                    <asp:TextBox ID="tbEndDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                        onclick="showPopup(this, event);" onfocus="showPopup(this, event);" MaxLength="10"
                                        onblur="parseDate(this, event); ValidateDate(this);" onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                    <asp:HiddenField ID="hdnEndDate" runat="server" />
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvEndDate" runat="server" ControlToValidate="tbEndDate"
                                        ValidationGroup="Save" ErrorMessage="End Date is Required" Display="Dynamic"
                                        CssClass="alert-text" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rbSkip" Text="Skip Footer" GroupName="Mode" runat="server" Checked="true" />
                                </td>
                                <td>
                                    <asp:RadioButton ID="rbPrint" Text="Print Footer" GroupName="Mode" runat="server"
                                        Checked="false" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:CheckBox ID="chkIsColor" Text="Print PDF report with color" runat="server" Checked="false" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" colspan="3">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button runat="server" ID="btExport" CssClass="button" Text="Export" OnClick="btnExport_Click"
                                                    ValidationGroup="Save" />
                                            </td>
                                            <td valign="top">
                                                <asp:Button runat="server" ID="btnPreview" Text="Preview" CssClass="button" ValidationGroup="Save"
                                                    OnClick="btnShowReports_OnClick" />
                                            </td>
                                            <td valign="top">
                                                <asp:Button runat="server" ID="btnCancel" Text="Cancel" OnClientClick="buttonclose();"
                                                    CausesValidation="false" CssClass="button" />
                                                <asp:Label ID="lbScript" runat="server" Text="" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td>
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <span class="mnd_text">
                                                    <asp:Label runat="server" ID="lbTitle"></asp:Label></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tblspace_10">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:PlaceHolder ID="Report" runat="server"></asp:PlaceHolder>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tblspace_10">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel runat="server" ID="ExportPanal">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="tdLabel100">
                                                    Export File Type
                                                </td>
                                                <td style="margin-left: 40px">
                                                    <asp:DropDownList runat="server" ID="ddlFormat">
                                                        <%--<asp:ListItem Text="HTML Table" Value="HTML"></asp:ListItem>--%>
                                                        <asp:ListItem Text="Excel" Value="Excel"></asp:ListItem>
                                                        <asp:ListItem Text="MHTML" Value="MHTML"></asp:ListItem>
                                                        <%-- <asp:ListItem Text="Visual FoxPro" Value="VFO"></asp:ListItem>

                                     <asp:ListItem Text="FoxPro 2.x" Value="FP"></asp:ListItem>

                                     <asp:ListItem Text="Comma Delimited Text" Value="CSV"></asp:ListItem>

                                     <asp:ListItem Text="Excel5.0" Value="Excel5"></asp:ListItem>

                                     <asp:ListItem Text="Excel2.0,3.0 and 4.0" Value="Excel2"></asp:ListItem>

                                     <asp:ListItem Text="Lotus 123.2.x" Value="Lotus"></asp:ListItem>

                                     <asp:ListItem Text="Data Interchange Format" Value="DIF"></asp:ListItem>

                                     <asp:ListItem Text="System Data Foramt" Value="SDF"></asp:ListItem>

                                     <asp:ListItem Text="Symblic Link Format" Value="SLF"></asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Button runat="server" CssClass="ui_nav" ID="btExportFileFormat" Text="Generate"
                                                        OnClick="btnExportFileFormat_Click" />
                                                </td>
                                                <td>
                                                    <asp:Button runat="server" CssClass="ui_nav" ID="btClose" Text="Close" OnClick="btnCostFileFormat_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Label runat="server" ID="lblError" CssClass="alert-text"></asp:Label>
                        <asp:Wizard ID="Wizard1" runat="server" DisplaySideBar="false" ActiveStepIndex="0"
                            Height="209px" Width="100%" BackColor="#f0f0f0" FinishPreviousButtonStyle-CssClass="ui_nav"
                            CancelButtonStyle-CssClass="ui_nav" FinishCompleteButtonStyle-CssClass="ui_nav"
                            StepNextButtonStyle-CssClass="ui_nav" StepPreviousButtonStyle-CssClass="ui_nav"
                            StartNextButtonStyle-CssClass="ui_nav" OnFinishButtonClick="OnFinish" OnNextButtonClick="OnNext"
                            OnPreviousButtonClick="OnPrevious">
                            <WizardSteps>
                                <asp:WizardStep ID="WizardStep1" runat="server">
                                    <asp:Label runat="server" ID="lblw1" Text="Click on each of the Available fields by name and the right arrow to select them. Add them in the preferred display order or use the Up/Down arrows to reorder them for reporting. Click on the Selected fields and left arrow to de-select them."></asp:Label>
                                    <br />
                                    <br />
                                    <UCReportHtmlSelection:Grid ID="UCRHS" runat="server" />
                                </asp:WizardStep>
                                <asp:WizardStep ID="WizardStep2" runat="server">
                                    <asp:Label runat="server" ID="lblw2" Text="Select how data is to be sorted"></asp:Label>
                                    <br />
                                    <br />
                                    <br />
                                    <UCReportHtmlSelection:Grid ID="UCRHSort" runat="server" />
                                    <%-- <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel1">

                                            <telerik:RadListBox runat="server" ID="SelectionSource" Height="200px" Width="200px"

                                                SelectionMode="Single" />

                                            <telerik:RadListBox runat="server" ID="SelectionDestination" AllowReorder="false"

                                                SelectionMode="Single" Height="200px" Width="200px">

                                            </telerik:RadListBox>

                                        </telerik:RadAjaxPanel>--%>
                                </asp:WizardStep>
                                <asp:WizardStep ID="WizardStep4" runat="server">
                                    <asp:Label runat="server" ID="lblw3" Text="Edit the Header Text "></asp:Label>
                                    <asp:TextBox ID="tbEditHeaderText" runat="server" OnTextChanged="tbEditHeaderText_OnTextChanged"
                                        AutoPostBack="true" ClientIDMode="Static" />
                                    <br />
                                    <br />
                                    <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel2">
                                        <table>
                                            <tr>
                                                <td>
                                                    Field
                                                </td>
                                                <td>
                                                    HTML Heading
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <telerik:RadListBox runat="server" ID="SelectedSource" Height="200px" Width="200px"
                                                        SelectionMode="Single" />
                                                </td>
                                                <td>
                                                    <telerik:RadListBox runat="server" ID="HeaderBySource" AllowReorder="false" SelectionMode="Single"
                                                        Height="200px" Width="200px" OnSelectedIndexChanged="HeaderBySource_OnSelectedIndexChanged"
                                                        AutoPostBack="true">
                                                    </telerik:RadListBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </telerik:RadAjaxPanel>
                                </asp:WizardStep>
                                <asp:WizardStep ID="WizardStep3" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td width="60%">
                                                <fieldset>
                                                    <legend>Page Layout</legend>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="tdLabel130">
                                                                            Use HTML Defaults
                                                                        </td>
                                                                        <td class="tdLabel80">
                                                                            <asp:CheckBox ID="chkHeader" runat="server" Text="Header" />
                                                                        </td>
                                                                        <td class="tdLabel70">
                                                                            <asp:CheckBox ID="chkFooter" runat="server" Text="Footer" />
                                                                        </td>
                                                                        <td class="tdLabel100">
                                                                            <asp:CheckBox ID="chkBackground" runat="server" Text="Background" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="tdLabel130">
                                                                            Title
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtTitle" runat="server" Width="300px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="tdLabel130">
                                                                            Foreground Color
                                                                        </td>
                                                                        <td class="tdLabel60">
                                                                            <telerik:RadColorPicker ID="ForegroundColor" runat="server" SelectedColor="" ShowIcon="True">
                                                                                <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                            </telerik:RadColorPicker>
                                                                        </td>
                                                                        <td class="tdLabel110">
                                                                            Background Color
                                                                        </td>
                                                                        <td>
                                                                            <telerik:RadColorPicker ID="BackgroundColor" runat="server" SelectedColor="" ShowIcon="True">
                                                                                <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                            </telerik:RadColorPicker>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                            <td width="40%">
                                                <fieldset>
                                                    <legend>Table Layout</legend>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="tdLabel130">
                                                                            Border Color
                                                                        </td>
                                                                        <td>
                                                                            <telerik:RadColorPicker ID="TableBorderColor" runat="server" SelectedColor="" ShowIcon="True">
                                                                                <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                            </telerik:RadColorPicker>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="tdLabel130">
                                                                            Foreground Color
                                                                        </td>
                                                                        <td>
                                                                            <telerik:RadColorPicker ID="TableForegroundColor" runat="server" SelectedColor=""
                                                                                ShowIcon="True">
                                                                                <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                            </telerik:RadColorPicker>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="tdLabel130">
                                                                            Background Color
                                                                        </td>
                                                                        <td>
                                                                            <telerik:RadColorPicker ID="TableBackgroundColor" runat="server" SelectedColor=""
                                                                                ShowIcon="True">
                                                                                <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                            </telerik:RadColorPicker>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:WizardStep>
                            </WizardSteps>
                        </asp:Wizard>
                        <asp:HiddenField ID="hdnDept" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hdnCRMainID" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hdnMainID" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hdnHeaderSourceID" runat="server" />
                        <asp:HiddenField ID="hdnRptXMLName" runat="server" ClientIDMode="Static" />
                    </td>
                </tr>
            </table>
             <iframe style="display:none;" name="PDFViewer" id="PDFViewerFrame" width="950px" height="940px"></iframe>
        </div>
    </div>
    </form>
</body>
</html>
