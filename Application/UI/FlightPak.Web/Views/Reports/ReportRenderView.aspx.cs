﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.Framework;
//using FlightPak.Web.ReportRenderService;

//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using System.Data;
using System.Xml;
using FlightPak.Web.ReportService;
using System.IO;




namespace FlightPak.Web.Views.Reports
{
    public partial class ReportRenderView : BaseSecuredPage
    {

        #region Page Variables
        readonly Byte[] rptStream;
        protected string strReportFileName = string.Empty;
        protected string strReportPath = string.Empty;
        protected string strReportFormat = string.Empty;
        protected List<FlightPak.Web.ReportRenderService.ReportParameter> rptParams;
        private ExceptionManager exManager;
        string strReportFolder = string.Empty;
        #endregion

        #region Constructor
        public ReportRenderView() { }
        public ReportRenderView(Byte[] streamObject)
        {
            rptStream = streamObject;

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SetReportPropertiesFromSession();
                        LoadReport();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
            //SetReportProperties();
        }


        private string SelectFolder()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string TabSelect = string.Empty;
                if (Request.QueryString["ReportType"] != null)
                {
                    if (isValidReportType(Request.QueryString["ReportType"]))
                        TabSelect = Request.QueryString["ReportType"];
                    else
                        TabSelect = Convert.ToString(Session["TabSelect"]);
                }
                else { TabSelect = Convert.ToString(Session["TabSelect"]); }
                switch (TabSelect)
                {
                    case "PreFlight":
                        strReportFolder = System.Configuration.ConfigurationManager.AppSettings["SSRSReportsPreFlight"];
                        break;
                    case "PostFlight":
                        strReportFolder = System.Configuration.ConfigurationManager.AppSettings["SSRSReportsPostFlight"];
                        break;
                    case "Db":
                        strReportFolder = System.Configuration.ConfigurationManager.AppSettings["SSRSReportsDBCatalog"];
                        break;
                    case "CorpReq":
                        strReportFolder = System.Configuration.ConfigurationManager.AppSettings["SSRSReportsCorpRequest"];
                        break;
                    case "Charter":
                        strReportFolder = System.Configuration.ConfigurationManager.AppSettings["SSRSReportsCharterQuote"];
                        break;
                    default:
                        strReportFolder = System.Configuration.ConfigurationManager.AppSettings["SSRSReportsFolder"];
                        break;
                }
                return strReportFolder;
            }
        }



        void SetReportPropertiesFromSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string strParamsNotFound = string.Empty;
                string strErrorMsg = string.Empty;
                string strParam = string.Empty;

                strReportFolder = SelectFolder();

                if (strReportFolder == "")
                {
                    throw new Exception("Report Folder path is not configured.");
                }

                //Populate data from Query String
                Dictionary<string, string> QSParams = new Dictionary<string, string>();
                foreach (string key in Session.Keys)
                {
                    if (key == "REPORT") strReportFileName = Convert.ToString(Session[key]);
                    else if (key == "FORMAT") strReportFormat = Convert.ToString(Session[key]);
                    else if (key == "PARAMETER") strParam = Convert.ToString(Session[key]);
                }

                if (strReportFileName == "RptUTAirportPairs" || strReportFileName == "RptUTItineraryPlanning" || strReportFileName == "RptUTWindsOnWorldAirRoutes" || strReportFileName == "RptDBAirport" || strReportFileName == "RptLocatorAirport" || strReportFileName == "RptLocatorHotel" || strReportFileName == "RptLocatorVendor" || strReportFileName == "RptLocatorCustom" || strReportFileName == "RptLocatorAirportExport" || strReportFileName == "RptLocatorHotelExport" || strReportFileName == "RptLocatorVendorExport" || strReportFileName == "RptLocatorCustomExport" || strReportFileName == "RptDBFleetProfile" || strReportFileName == "RptDBFleetProfileExport")
                {
                    strReportFolder = System.Configuration.ConfigurationManager.AppSettings["SSRSReportsDBCatalog"];
                }

                if (strParam != string.Empty)
                {

                    string[] strMainParm = strParam.Split(';');
                    foreach (string strpair in strMainParm)
                    {
                        string[] strPairs = strpair.Split('=');
                        QSParams.Add(Convert.ToString(strPairs[0].ToUpper()), Convert.ToString(strPairs[1]));
                    }
                }

                if (strReportFileName == "") strErrorMsg += "Report Name is required<BR>";
                if (strReportFormat == "") strErrorMsg += "Report Format is required<BR>";
                if (strErrorMsg != string.Empty) throw new Exception(strErrorMsg);

                //set the complete report path name
                strReportPath = @"/" + strReportFolder + "/" + strReportFileName;

                //GetReportParameters
                //Retrive report parameter info for the given report name
                //Set ParamValue property for the same object and pass it to GetReportData() to render the report
                using (FlightPak.Web.ReportRenderService.ReportRenderClient svcReports = new FlightPak.Web.ReportRenderService.ReportRenderClient())
                {
                    rptParams = svcReports.GetReportParameters(strReportPath);

                    //Set the parameter values for each parameter [ParamValue]
                    //RreportParameter has below 3 properties
                    //ParamName, ParamValue, ParamType
                    foreach (FlightPak.Web.ReportRenderService.ReportParameter rptParam in rptParams)
                    {
                        //loops through each parameter and set the parameter value
                        //Set report parameters from Querystring values
                        if (QSParams.ContainsKey(rptParam.ParamName.ToUpper()))
                        {
                            rptParam.ParamValue = Microsoft.Security.Application.Encoder.HtmlEncode(QSParams[rptParam.ParamName.ToUpper()]); //UserCD = "UC"
                        }

                        if (rptParam.ParamType.ToString().ToUpper() == "DATETIME" && rptParam.ParamValue == "")
                            rptParam.ParamValue = null;

                        //if(rptParam.ParamValue == "")
                        //{ 
                        //    //Will ensure if the parameter value is empty
                        //    strParamsNotFound += rptParam.ParamName + ",";
                        //}
                    }

                    if (strParamsNotFound != "")
                    {
                        throw new Exception("Data not supplied for report parameter(s): " + strParamsNotFound);
                    }
                }
            }

        }

        #region SetReportParams
        protected void SetReportProperties()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string strParamsNotFound = string.Empty;
                string strErrorMsg = string.Empty;

                strReportFolder = System.Configuration.ConfigurationManager.AppSettings["SSRSReportsFolder"];
                if (strReportFolder == "")
                {
                    throw new Exception("Report Folder path is not configured.");
                }

                //Populate data from Query String
                Dictionary<string, string> QSParams = new Dictionary<string, string>();
                foreach (string KeyName in Request.QueryString.Keys)
                {
                    if (KeyName.ToUpper() == "REPORT") strReportFileName = Request.QueryString[KeyName];
                    else if (KeyName.ToUpper() == "FORMAT") strReportFormat = Request.QueryString[KeyName];
                    else QSParams.Add(KeyName.ToUpper(), Request.QueryString[KeyName]);
                }

                if (strReportFileName == "") strErrorMsg += "Report Name is required<BR>";
                if (strReportFormat == "") strErrorMsg += "Report Format is required<BR>";
                if (strErrorMsg != string.Empty) throw new Exception(strErrorMsg);

                //set the complete report path name
                strReportPath = @"/" + strReportFolder + "/" + strReportFileName;

                //GetReportParameters
                //Retrive report parameter info for the given report name
                //Set ParamValue property for the same object and pass it to GetReportData() to render the report
                using (FlightPak.Web.ReportRenderService.ReportRenderClient svcReports = new FlightPak.Web.ReportRenderService.ReportRenderClient())
                {

                    rptParams = svcReports.GetReportParameters(strReportPath);

                    //Set the parameter values for each parameter [ParamValue]
                    //RreportParameter has below 3 properties
                    //ParamName, ParamValue, ParamType
                    foreach (FlightPak.Web.ReportRenderService.ReportParameter rptParam in rptParams)
                    {
                        //loops through each parameter and set the parameter value
                        //Set report parameters from Querystring values
                        if (QSParams.ContainsKey(rptParam.ParamName.ToUpper()))
                        {
                            rptParam.ParamValue = QSParams[rptParam.ParamName.ToUpper()]; //UserCD = "UC"
                        }

                        if (rptParam.ParamValue == "")
                        {
                            //Will ensure if the parameter value is empty
                            strParamsNotFound += rptParam.ParamName + ",";
                        }
                    }

                    if (strParamsNotFound != "")
                    {
                        throw new Exception("Data not supplied for report parameter(s): " + strParamsNotFound);
                    }

                }
            }
        }
        #endregion

        #region GenerateTempReport

        private void GenerateTempReport()
        {
            if (Session["IsTempFile"] != null)
            {
                string istemp = Session["IsTempFile"].ToString();

                if (istemp == "1")
                {

                    string filepath = "/XTSDEV2/" + strReportFolder + "/" + strReportFileName;

                    XmlDocument doc = DownloadReportFromServer(filepath);


                    if (doc != null)
                    {
                        Session["xmldoc"] = doc;
                        GenerateRdl();
                    }
                    else
                    {
                        Session["xmldoc"] = null;
                    }

                }

            }
        }

        private XmlDocument DownloadReportFromServer(string filepath)
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.XmlResolver = null;
            XmlDocument xmldoc = new XmlDocument();
            try
            {

                string reportPath = ""; //Path of the report that is currently being processed.
                byte[] reportDefinition = null; //Holds the Original RDL byte array.

                ReportServiceClient client = new ReportServiceClient();

                reportDefinition = client.GetReport(filepath);

                using (MemoryStream memOriginalRDL = new MemoryStream(reportDefinition))
                {
                    using (XmlReader reader = XmlReader.Create(memOriginalRDL, settings))
                    {
                        xmldoc.Load(reader);
                    }
                    memOriginalRDL.Close();
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occured: " + ex.Message.ToString());
            }

            return xmldoc;
        }

        public void GenerateRdl()
        {
            if (Session["xmldoc"] != null && Session["dicNewColoumnList_no"] != null)
            {
                Dictionary<string, string> dicNewColoumnList_no = (Dictionary<string, string>)Session["dicNewColoumnList_no"];

                XmlDocument doc = (XmlDocument)Session["xmldoc"];


                int countItems = dicNewColoumnList_no.Count;
                // Create an XML document

                if (countItems > 0)
                {

                    // Report element

                    int tablixrows_count = 0;

                    XmlNode TablixColumns = null;
                    XmlNode TablixColumnHierarchy = null;
                    XmlNode TablixRows1 = null;

                    foreach (XmlNode node in doc.SelectNodes("descendant::*"))
                    {
                        if (node.Name == "TablixColumns")
                        {
                            TablixColumns = node;
                        }

                        if (node.Name == "TablixColumnHierarchy")
                        {
                            TablixColumnHierarchy = node;
                        }


                        if (node.Name == "TablixRows")
                        {
                            tablixrows_count++;

                            if (tablixrows_count == 1)
                            {
                                TablixRows1 = node;
                            }
                        }
                    }

                    XmlNode TablixMembers = null;
                    XmlNode TablixCells1 = null;


                    foreach (XmlNode node in TablixColumnHierarchy.ChildNodes)
                    {
                        //TablixRows = node;
                        if (node.Name == "TablixMembers")
                        {
                            TablixMembers = node;
                        }
                    }


                    int count_items = 0;

                    foreach (XmlNode node_TablixRow in TablixRows1.ChildNodes)
                    {
                        count_items = 0;

                        //SelectNodes("descendant::*")

                        foreach (XmlNode node in node_TablixRow.SelectNodes("descendant::*"))
                        {

                            //TablixRows = node;
                            if (node.Name == "TablixCells")
                            {
                                XmlNode TablixCells1_new = GetSelectedColumns(doc, node, dicNewColoumnList_no);
                                node.RemoveAll();
                                count_items = TablixCells1_new.ChildNodes.Count;
                                for (int i = 0; i < count_items; i++)
                                {
                                    XmlNode child_new = TablixCells1_new.ChildNodes[0];
                                    node.AppendChild(child_new);

                                }
                            }
                        }

                    }


                    count_items = 0;

                    XmlNode TablixColumns_new = GetSelectedColumns(doc, TablixColumns, dicNewColoumnList_no);
                    TablixColumns.RemoveAll();
                    count_items = TablixColumns_new.ChildNodes.Count;
                    while (count_items > 0)
                    {
                        XmlNode child_new = TablixColumns_new.ChildNodes[0];
                        TablixColumns.AppendChild(child_new);
                        count_items = TablixColumns_new.ChildNodes.Count;
                    }

                    XmlNode TablixMembers_new = GetSelectedColumns(doc, TablixMembers, dicNewColoumnList_no);
                    TablixMembers.RemoveAll();
                    count_items = TablixMembers_new.ChildNodes.Count;
                    while (count_items > 0)
                    {
                        XmlNode child_new = TablixMembers_new.ChildNodes[0];
                        TablixMembers.AppendChild(child_new);
                        count_items = TablixMembers_new.ChildNodes.Count;
                    }

                    //string filename = "Temp_VarunMahajan";
                    //string editedfile = "~/Views/RDLFiles/" + filename + ".rdl";
                    //doc.Save(Server.MapPath(editedfile));


                    byte[] newReport = null; //Holds the Modified RDL byte array from the stream.

                    using (MemoryStream memModifiedRDL = new MemoryStream())
                    {
                        doc.Save(memModifiedRDL);
                        newReport = memModifiedRDL.GetBuffer();
                        memModifiedRDL.Close();
                    }




                    //ReportService2005.ReportingService2005 rs = new ReportService2005.ReportingService2005();
                    //rs.Credentials = new System.Net.NetworkCredential(@"universalrdn\d_vmahajan", "Manu!567");
                    //rs.Url = "http://hrdssrsw01:8212/ReportServer_FPK2/ReportService2005.asmx";

                    //Put it back in to Report Server database.
                    string filepath = "/XTSDEV2/PostFlight/TempVarunMahajan";
                    // ReportService2005.Warning[] warning = rs.SetReportDefinition(filepath, newReport);


                    ReportServiceClient client = new ReportServiceClient();

                    string result = client.uploadReport(filepath, newReport);

                    //string fName = Server.MapPath(editedfile);
                    //FileInfo fi = new FileInfo(fName);
                    //long sz = fi.Length;

                    //Response.ClearContent();
                    //Response.ContentType = MimeType(Path.GetExtension(fName));
                    //Response.AddHeader("Content-Disposition", string.Format("attachment; filename = {0}", System.IO.Path.GetFileName(fName)));
                    //Response.AddHeader("Content-Length", sz.ToString("F0"));
                    //Response.TransmitFile(fName);
                    //Response.End();

                    if (result == "1")
                    {
                        //btn_GoToReport.Visible = true;
                        //lbl_result.Text = "Report Upload Sucessfully";
                        //lbl_result.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        //lbl_result.Text = "Error! in Report Uploading";
                        //lbl_result.ForeColor = System.Drawing.Color.Red;
                    }

                }
                else
                {
                    //lbl_result.Text = "Select Atleast One of column";
                    //lbl_result.ForeColor = System.Drawing.Color.Red;
                }

            }

        }

        public XmlNode GetSelectedColumns(XmlDocument doc, XmlNode Tablix1, Dictionary<string, string> dicNewColoumnList_no)
        {

            XmlElement TablixCells_new = doc.CreateElement("TablixCell1");

            foreach (KeyValuePair<string, string> li in dicNewColoumnList_no)
            {
                XmlNode Tablix1_clone = Tablix1.CloneNode(true);

                int selected_index = int.Parse(li.Key);

                int matchindex = 0;

                foreach (XmlNode node in Tablix1_clone.ChildNodes)
                {
                    matchindex++;

                    if (matchindex == selected_index)
                    {
                        TablixCells_new.AppendChild(node);
                        break;
                    }
                }


            }

            return TablixCells_new;
        }

        #endregion

        #region LoadReport
        protected void LoadReport()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (FlightPak.Web.ReportRenderService.ReportRenderClient svcReports = new FlightPak.Web.ReportRenderService.ReportRenderClient())
                {
                    if (Session["IsTempFile"] != null)
                    {
                        string istemp = Session["IsTempFile"].ToString();

                        if (istemp == "1")
                        {
                            GenerateTempReport();
                            //strReportPath = "/PostFlight/TempVarunMahajan";  This item is still uncommented in the code which runs on the server. So it always refers to the hard coded
                        }

                    }

                    ReportRenderView rptObject = new ReportRenderView(svcReports.GetReportData(Microsoft.Security.Application.Encoder.HtmlEncode(strReportPath), Microsoft.Security.Application.Encoder.HtmlEncode(strReportFormat), rptParams));
                    svcReports.Close();

                    if (Session["FORMAT"] != null && Convert.ToString(Session["FORMAT"]) == "PDF")
                    {
                        Session["Base64String"] = Microsoft.Security.Application.Encoder.HtmlEncode(Convert.ToBase64String(rptObject.rptStream));

                        string xmlFileName = Request.QueryString["xmlFilename"],
                        reportType = Request.QueryString["ReportType"],
                        redirectUrl = string.Empty;

                        if (Session["REPORT"].Equals("RptFlightLog"))
                        {
                            redirectUrl = "PDFViewer.aspx";
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(xmlFileName))
                            {
                                if (xmlFileName == "PreTSTripSheetMain.xml")
                                    redirectUrl = "TripSheetReportViewer.aspx";
                                else
                                    redirectUrl = "ReportViewer.aspx";
                                redirectUrl = string.Concat(redirectUrl, string.Format("?xmlFilename={0}&ReportType={1}", xmlFileName, reportType));
                            }
                        }
                        if (!string.IsNullOrEmpty(redirectUrl) && !string.IsNullOrWhiteSpace(redirectUrl))
                        {
                            var NewUrl = IsLocalUrl(redirectUrl);
                            if (!string.IsNullOrEmpty(NewUrl) && !string.IsNullOrWhiteSpace(NewUrl))
                            {
                                Response.Redirect(NewUrl, true);
                            }
                        }
                        //return;
                    }
                    Response.Buffer = true;
                    Response.ClearHeaders();
                    //wirte back to current/popup window
                    Response.ClearContent();
                    Response.AppendHeader("content-length", rptObject.rptStream.Length.ToString());

                    string strAttachmentName = strReportFileName;
                    if (strAttachmentName.Length > 3 && strAttachmentName.Substring(0, 3).ToUpper() == "RPT")
                    {
                        strAttachmentName = strAttachmentName.Substring(3, strAttachmentName.Length - 3);
                    }
                    //Veracode Fix for 2992
                    Response.AppendHeader("content-disposition", "attachment;filename=" + Server.UrlEncode(strAttachmentName + "." + GetFileExtension(strReportFormat)));

                    //ContentType should be changed according to the Report Format requested
                    Response.ContentType = GetContentType(strReportFormat);
                    Response.BinaryWrite(GetFormatedString(rptObject.rptStream));
                    //  Response.StatusCode = 200;

                    Response.Flush();
                    Response.End();
                    Response.Clear();
                }

            }

        }
        #endregion

        #region Get Content Type for the rendering format
        protected string GetContentType(string strFormat)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(strFormat))
            {
                string strUpperFormat = strFormat.ToUpper();

                if (strUpperFormat == "PDF") return Framework.Constants.ReportConstants.MIMEtype_PDF;
                else if (strUpperFormat == "EXCEL") return Framework.Constants.ReportConstants.MIMEtype_EXCEL;
                else if (strUpperFormat == "WORD") return Framework.Constants.ReportConstants.MIMEtype_WORD;
                else if (strUpperFormat == "XML") return Framework.Constants.ReportConstants.MIMEtype_XML;
                else if (strUpperFormat == "CSV") return Framework.Constants.ReportConstants.MIMEtype_CSV;
                else if (strUpperFormat == "HTML") return Framework.Constants.ReportConstants.MIMEtype_MHTML; //HTML is not supported
                else if (strUpperFormat == "MHTML") return Framework.Constants.ReportConstants.MIMEtype_MHTML;
                else return Framework.Constants.ReportConstants.MIMEtype_UNKNOWN;
            }
        }

        #endregion

        #region Get Extension Type for the rendering format
        protected string GetFileExtension(string strFormat)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(strFormat))
            {
                string strUpperFormat = strFormat.ToUpper();

                if (strUpperFormat == "PDF") return Framework.Constants.ReportConstants.Extension_PDF;
                else if (strUpperFormat == "EXCEL") return Framework.Constants.ReportConstants.Extension_EXCEL;
                else if (strUpperFormat == "WORD") return Framework.Constants.ReportConstants.Extension_WORD;
                else if (strUpperFormat == "XML") return Framework.Constants.ReportConstants.Extension_XML;
                else if (strUpperFormat == "CSV") return Framework.Constants.ReportConstants.Extension_CSV;
                else if (strUpperFormat == "HTML") return Framework.Constants.ReportConstants.Extension_MHTML; //HTML is not supported
                else if (strUpperFormat == "MHTML") return Framework.Constants.ReportConstants.Extension_MHTML;
                else return Framework.Constants.ReportConstants.Extension_UNKNOWN;
            }
        }

        #endregion

        #region Check valid Report Type
        public bool isValidReportType(string reportType)
        {
            switch (reportType)
            {
                case FlightPak.Web.Framework.Constants.ReportConstants.ReportType.Preflight:
                    return true;
                case FlightPak.Web.Framework.Constants.ReportConstants.ReportType.Postflight:
                    return true;
                case FlightPak.Web.Framework.Constants.ReportConstants.ReportType.Database:
                    return true;
                case FlightPak.Web.Framework.Constants.ReportConstants.ReportType.Charter:
                    return true;
                case FlightPak.Web.Framework.Constants.ReportConstants.ReportType.CorporateRequest:
                    return true;
                default:
                    return false;
            }
        }
        #endregion
    }
}

