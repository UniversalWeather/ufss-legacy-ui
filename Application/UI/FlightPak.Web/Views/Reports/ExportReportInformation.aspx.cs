﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.XPath;
using System.Text;
using System.Xml;
using System.Drawing;
using FlightPak.Web.Framework.Prinicipal;
using System.Xml.Linq;
using Telerik.Web.UI;
using FlightPak.Common;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Reports
{
    public partial class ExportReportInformation : BaseSecuredPage
    {
        string ReportString = string.Empty;
        string p1 = string.Empty;
        string p2 = string.Empty;
        string p3 = string.Empty;
        string p4 = string.Empty;
        string p5 = string.Empty;
        string p6 = string.Empty;
        string p7 = string.Empty;
        string p8 = string.Empty;
        string p9 = string.Empty;
        string p10 = string.Empty;
        string p11 = string.Empty;
        string p12 = string.Empty;
        string p13 = string.Empty;
        string p14 = string.Empty;
        string p15 = string.Empty;

        string urlBase = "Config\\";
        string filename = string.Empty;
        public Dictionary<string, string> dicColumnList = new Dictionary<string, string>();
        public Dictionary<string, string> dicSelectedList = new Dictionary<string, string>();
        public Dictionary<string, string> dicSelectedListsort = new Dictionary<string, string>();
        public Dictionary<string, string> dicSelectedListsortSel = new Dictionary<string, string>();
        public Dictionary<string, string> dicColumnListsort = new Dictionary<string, string>();

        public Dictionary<string, string> dicSourceList = new Dictionary<string, string>();
        public Dictionary<string, string> dicDestinationList = new Dictionary<string, string>();
        public Dictionary<string, string> dicSourceSortList = new Dictionary<string, string>();
        public Dictionary<string, string> dicDestinationSortList = new Dictionary<string, string>();
        public Dictionary<string, string> dicEmptyList = new Dictionary<string, string>();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Report"] != null)
            {
                ReportString = Request.QueryString["Report"];
            }

            if (Request.QueryString["P1"] != null)
            {
                p1 = Request.QueryString["P1"];
            }

            if (Request.QueryString["P2"] != null)
            {
                p2 = Request.QueryString["P2"];
            }

            if (Request.QueryString["P3"] != null)
            {
                p3 = Request.QueryString["P3"];
            }

            if (Request.QueryString["P4"] != null)
            {
                p4 = Request.QueryString["P4"];
            }

            if (Request.QueryString["P5"] != null)
            {
                p5 = Request.QueryString["P5"];
            }

            if (Request.QueryString["P6"] != null)
            {
                p6 = Request.QueryString["P6"];
            }

            if (Request.QueryString["P7"] != null)
            {
                p7 = Request.QueryString["P7"];
            }

            if (Request.QueryString["P8"] != null)
            {
                p8 = Request.QueryString["P8"];
            }

            if (Request.QueryString["P9"] != null)
            {
                p9 = Request.QueryString["P9"];
            }

            if (Request.QueryString["P10"] != null)
            {
                p10 = Request.QueryString["P10"];
            }

            if (Request.QueryString["P11"] != null)
            {
                p11 = Request.QueryString["P11"];
            }

            if (Request.QueryString["P12"] != null)
            {
                p12 = Request.QueryString["P12"];
            }

            if (Request.QueryString["P13"] != null)
            {
                p13 = Request.QueryString["P13"];
            }

            if (Request.QueryString["P14"] != null)
            {
                p14 = Request.QueryString["P14"];
            }

            if (Request.QueryString["P15"] != null)
            {
                p15 = Request.QueryString["P15"];
            }

            //hdnRptXMLName.Value = Request.QueryString["xmlFilename"];
            switch (ReportString)
            {
                case "RptUTAirportPairs":
                    hdnRptXMLName.Value = "UTAirportPairs.xml";
                    filename = "UTAirportPairs.xml";
                    break;

                case "RptDBAirport":
                    hdnRptXMLName.Value = "DBAirport.xml";
                    filename = "DBAirport.xml";
                    break;

                case "RptUTItineraryPlanning":
                    hdnRptXMLName.Value = "UTItineraryPlanning.xml";
                    filename = "UTItineraryPlanning.xml";
                    break;

                case "RptUTWindsOnWorldAirRoutes":
                    hdnRptXMLName.Value = "UTWindsOnWorldAirRoutes.xml";
                    filename = "UTWindsOnWorldAirRoutes.xml";
                    break;

                case "RptLocatorAirport":
                    hdnRptXMLName.Value = "LocatorAirport.xml";
                    filename = "LocatorAirport.xml";
                    break;

                case "RptLocatorHotel":
                    hdnRptXMLName.Value = "LocatorHotel.xml";
                    filename = "LocatorHotel.xml";
                    break;

                case "RptLocatorVendor":
                    hdnRptXMLName.Value = "LocatorVendor.xml";
                    filename = "LocatorVendor.xml";
                    break;

                case "RptLocatorCustom":
                    hdnRptXMLName.Value = "LocatorCustom.xml";
                    filename = "LocatorCustom.xml";
                    break;

                case "RptPOSTExpenseCatalogExport":
                    hdnRptXMLName.Value = "POSTExpenseCatalog.xml";
                    filename = "POSTExpenseCatalog.xml";
                    break;
                case "RptDBCrewRosterExport":
                    hdnRptXMLName.Value = "DBCrewRoster.xml";
                    filename = "DBCrewRoster.xml";
                    break;
                case "RptDBFleetProfile":
                    hdnRptXMLName.Value = "DBFleetProfile.xml";
                    filename = "DBFleetProfile.xml";
                    break;

                case "RptDBDeptAuthorization":
                    hdnRptXMLName.Value = "DBDepartmentAuthorization.xml";
                    filename = "DBDepartmentAuthorization.xml";
                    break;

                default:
                    hdnRptXMLName.Value = "UTAirportPairs.xml";
                    filename = "UTAirportPairs.xml";
                    break;
            }
            if (!IsPostBack)
            {
                Wizard1.Visible = false;
                ExportPanal.Visible = false;

                Session["SourceList"] = null;
                Session["DestinationList"] = null;
                Session["SourceSortList"] = null;
                Session["DestinationSortList"] = null;
            }
            //Session["TabSelect1"] = "Calender";
            if (Session["TabSelect"] == null)
                Session["TabSelect"] = "Db";
        }
        
        protected void Cancel_Click(object sender, EventArgs e)
        {
            lbScript.Visible = true;
            lbScript.Text = "<script type='text/javascript'>Close()</" + "script>";
        }

        #region "Reports
        protected void btnShowReports_OnClick(object sender, EventArgs e)
        {
            //Handle methods throguh exception manager with return flag
            switch (ReportString)
            {
                case "RptUTAirportPairs":
                    Session["REPORT"] = "RptUTAirportPairs";
                    Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";AirportPairNumber=" + p1;
                    break;

                case "RptDBAirport":
                    Session["REPORT"] = "RptDBAirport";
                    Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";IcaoID=" + p1;
                    //Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";IcaoID=CZML"; 

                    break;

                case "RptUTItineraryPlanning":
                    Session["REPORT"] = "RptUTItineraryPlanning";
                    Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";ItineraryNumber=" + p1;
                    break;

                case "RptUTWindsOnWorldAirRoutes":
                    Session["REPORT"] = "RptUTWindsOnWorldAirRoutes";
                    Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";Departure=" + p1 + ";Arrival=" + p2;
                    break;

                case "RptLocatorAirport":
                    Session["REPORT"] = "RptLocatorAirport";
                    Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";IcaoID=" + p1 + ";CountryCD=" + p2 + ";Runway=" + Convert.ToInt32(p3) + ";CityName=" + p4 + ";MetroCD=" + p5 + ";IATA=" + p6 + ";MilesFrom=" + Convert.ToInt32(p7) + ";StateName=" + p8 + ";AirportName=" + p9 + ";IsHeliport=" + p10 + ";IsEntryPort=" + p11 + ";IsInActive=" + p12 + ";CountryID=" + p13 + ";AirportID=" + p14;
                    break;

                case "RptLocatorHotel":
                    Session["REPORT"] = "RptLocatorHotel";
                    Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";IcaoID=" + p1 + ";CountryCD=" + p2 + ";MilesFrom=" + Convert.ToInt32(p3) + ";CityName=" + p4 + ";MetroCD=" + p5 + ";IATA=" + p6 + ";StateName=" + p7 + ";AirportName=" + p8 + ";CountryID=" + p9 + ";AirportID=" + p10;
                    break;

                case "RptLocatorVendor":
                    Session["REPORT"] = "RptLocatorVendor";
                    Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";IcaoID=" + p1 + ";CountryCD=" + p2 + ";MilesFrom=" + Convert.ToInt32(p3) + ";CityName=" + p4 + ";MetroCD=" + p5 + ";AircraftType=" + p6 + ";IATA=" + p7 + ";StateName=" + p8 + ";AirportName=" + p9 + ";AirportID=" + p10;
                    break;

                case "RptLocatorCustom":
                    Session["REPORT"] = "RptLocatorCustom";
                    Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";IcaoID=" + p1 + ";CountryCD=" + p2 + ";MilesFrom=" + Convert.ToInt32(p3) + ";CityName=" + p4 + ";MetroCD=" + p5 + ";IATA=" + p6 + ";StateName=" + p7 + ";AirportName=" + p8 + ";AirportID=" + p9;
                    break;

                case "RptPOSTExpenseCatalogExport":
                    Session["REPORT"] = "RptPOSTExpenseCatalogExport";
                    Session["PARAMETER"] = string.Format("UserCD={0};LogNum={1};LegNum={2};SlipNum={3}", UserPrincipal.Identity._name, p1, p2, p3);
                    break;

                default:
                    Session["REPORT"] = "RptUTAirportPairs";
                    Session["PARAMETER"] = "UserCD=" + UserPrincipal.Identity._name + ";UserCustomerID=" + UserPrincipal.Identity._customerID + ";AirportPairNumber=" + p1;
                    break;
            }
            Session["FORMAT"] = "PDF";
            Response.Redirect(@"..\Reports\ReportRenderView.aspx");
            lbScript.Visible = true;
            lbScript.Text = "<script type='text/javascript'>Close()</" + "script>";
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag

                    bool Isvalid = true;
                    var filePath = Server.MapPath(urlBase + filename);
                    XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                    var xmlReaderObject = xmlReader.XmlCleanReader();
                    if (xmlReaderObject != null)
                    {
                        XPathDocument ReportFieldDoc = new XPathDocument(xmlReaderObject);
                        string Param = string.Empty;
                        XPathNodeIterator itr = ReportFieldDoc.CreateNavigator().Select("//searchfield");
                        StringBuilder sb = new System.Text.StringBuilder();

                        XmlDocument doc = new XmlDocument();
                        doc.Load(Server.MapPath(urlBase + filename));

                        while (itr.MoveNext())
                        {
                            string controlName = itr.Current.GetAttribute("name", "");
                            sb.Append(controlName);
                            sb.Append("=");

                            Control ctrlHolder = FindControlRecursive(Report, "lbcv" + controlName);
                            if (ctrlHolder != null)
                            {
                                if (ctrlHolder is Label)
                                {
                                    if (!string.IsNullOrEmpty(((Label)ctrlHolder).Text))
                                        Isvalid = false;
                                }
                            }
                        }

                        if (Isvalid)
                        {
                            if (Page.IsValid)
                            {
                                ExportPanal.Visible = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        private Control FindControlRecursive(Control rootControl, string controlID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(rootControl, controlID))
            {
                if (rootControl.ID == controlID) return rootControl;

                foreach (Control controlToSearch in rootControl.Controls)
                {
                    Control controlToReturn =
                        FindControlRecursive(controlToSearch, controlID);
                    if (controlToReturn != null) return controlToReturn;
                }
                return null;
            }
        }

        protected void btnExportFileFormat_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag

                    if (Page.IsValid)
                    {
                        switch (ddlFormat.SelectedValue)
                        {
                            case "MHTML":
                                Wizard1.Visible = true;
                                Wizard1.MoveTo(WizardStep1);
                                ProcessColumns();
                                ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[1])).DataSource = dicColumnList;
                                ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[1])).DataBind();
                                ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[1])).DataSource = dicColumnList;
                                ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[1])).DataBind();
                                dicColumnList.Clear();
                                ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[3])).DataSource = dicColumnList;
                                ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[3])).DataBind();
                                ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[3])).DataSource = dicColumnList;
                                ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[3])).DataBind();
                                break;
                            case "Excel":
                                string[] strArr = GetReportTitle().Split(',');
                                if (strArr.Count() > 0)
                                {
                                    Session["REPORT"] = Convert.ToString(strArr[1] + "Export");
                                }
                                Session["FORMAT"] = "EXCEL";
                                Session["PARAMETER"] = ProcessHtmlExcelParam();
                                Response.Redirect(ResolveUrl("~/Views/Reports/ReportRenderView.aspx"), false);
                                break;
                            default:

                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnCostFileFormat_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag

                    ExportPanal.Visible = false;
                    Wizard1.Visible = false;
                    lblError.Text = "";
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        private string GetReportTitle()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string RptName = string.Empty;
                XmlDocument doc = new XmlDocument();
                doc.Load(Server.MapPath(urlBase + filename));
                XmlNodeList elemList = doc.GetElementsByTagName("Report");

                for (int i = 0; i < elemList.Count; i++)
                {
                    RptName = elemList[i].Attributes["displayname"].Value + "," + elemList[i].Attributes["name"].Value;
                    break;
                }

                return RptName;
            }
        }

        private string ProcessHtmlExcelParam()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string PageFGcolor = ColorTranslator.ToHtml(ForegroundColor.SelectedColor);
                string PageBGColor = ColorTranslator.ToHtml(BackgroundColor.SelectedColor);

                string TableBorderColors = ColorTranslator.ToHtml(TableBorderColor.SelectedColor);
                string TableFGColor = ColorTranslator.ToHtml(TableForegroundColor.SelectedColor);
                string TableBGColor = ColorTranslator.ToHtml(TableBackgroundColor.SelectedColor);
                StringBuilder strBuildparm = new StringBuilder();

                string Dbcolname = string.Empty;
                string AliesColname = string.Empty;
                string SortColname = string.Empty;


                switch (ddlFormat.SelectedValue)
                {

                    case "MHTML":

                        foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                        {
                            Dbcolname = Dbcolname + SelectedItem.Value + ",";
                        }
                        if (Dbcolname != string.Empty)
                        {
                            Dbcolname = Dbcolname.Substring(0, Dbcolname.Length - 1);
                        }
                        foreach (RadListBoxItem SelectedItem in HeaderBySource.Items)
                        {
                            AliesColname = AliesColname + SelectedItem.Text + "|";
                        }
                        AliesColname = AliesColname.Substring(0, AliesColname.Length - 1);

                        foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxDestination.Items)
                        {
                            SortColname = SortColname + SelectedItem.Value + ",";
                        }
                        if (SortColname != string.Empty)
                        {
                            SortColname = SortColname.Substring(0, SortColname.Length - 1);
                        }

                        strBuildparm.Append(ProcessResults());
                        strBuildparm.Append(";SelectedCols=" + Dbcolname);
                        strBuildparm.Append(";DisplayCols=" + AliesColname);
                        strBuildparm.Append(";SortCols=" + SortColname);
                        strBuildparm.Append(";Color=" + PageBGColor + "," + PageFGcolor + "," + TableFGColor + "," + TableBorderColors + "," + TableBGColor);
                        strBuildparm.Append(";ReportTitle=" + txtTitle.Text);

                        FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                        using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            FPPrincipal identityname = (FPPrincipal)Session[Session.SessionID];
                            var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == identity.Identity._customerID && x.HomebaseID == identity.Identity._homeBaseId).ToList();


                            if (chkHeader.Checked)
                            {
                                strBuildparm.Append(";HtmlHeader=" + objCompany[0].HTMLHeader);
                            }
                            if (chkFooter.Checked)
                            {
                                strBuildparm.Append(";HtmlFooter=" + objCompany[0].HTMLFooter);
                            }
                            if (chkBackground.Checked)
                            {
                                strBuildparm.Append(";HtmlBG=" + objCompany[0].HTMLBackgroun);
                            }
                        }
                        break;

                    case "Excel":
                        //    //for Excel file  Reading from RadListBoxSource
                        //   foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxSource.Items)
                        //    {
                        //        Dbcolname = Dbcolname + SelectedItem.Value + ",";
                        //    }
                        //    if (Dbcolname != string.Empty)
                        //    {
                        //        Dbcolname = Dbcolname.Substring(0, Dbcolname.Length - 1);
                        //    }
                        //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxSource.Items)
                        //    {
                        //        AliesColname=AliesColname+SelectedItem.Text+"|";
                        //    }
                        //if (AliesColname != string.Empty)
                        //{
                        //    AliesColname = AliesColname.Substring(0, AliesColname.Length - 1);
                        //}


                        strBuildparm.Append(ProcessResults());
                        //   strBuildparm.Append(";SelectedCols="+Dbcolname);
                        //    strBuildparm.Append(";DisplayCols="+AliesColname);
                        //strBuildparm.Append(";Color=," + PageFGcolor + "," + TableFGColor + "," + TableBorderColors + "," + TableBGColor); 

                        break;

                }

                return strBuildparm.ToString();

                //FlightPak.Web.ReportRenderService.ReportRenderClient RptClient = new ReportRenderService.ReportRenderClient();
                //DataSet dsRpt = RptClient.GetReportHTMLTable(Dbcolname, AliesColname, GetSPName(filename), ProcessResults());
                //  return GetDataTableAsHTML(dsRpt.Tables[0], txtTitle.Text, PageFGcolor, PageBGColor, TableBorderColor, TableFGColor, TableBGColor);
            }
        }


        #endregion

        protected void tbEditHeaderText_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag

                    if (!string.IsNullOrEmpty(hdnHeaderSourceID.Value))
                    {
                        HeaderBySource.Items[Convert.ToInt32(hdnHeaderSourceID.Value)].Text = tbEditHeaderText.Text;
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void HeaderBySource_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag

                    tbEditHeaderText.Text = HeaderBySource.SelectedItem.Text;
                    hdnHeaderSourceID.Value = HeaderBySource.SelectedIndex.ToString();

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }


        protected void OnFinish(object sender, System.Web.UI.WebControls.WizardNavigationEventArgs e)
        {
            //string fileName = String.Format("data-{0}.htm", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
            //Response.ContentType = "text/htm";
            //Response.AddHeader("content-disposition", "filename=" + fileName);
            //// write string data to Response.OutputStream here
            //Response.Write(ProcessHtmlParam());
            //Response.End();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag

                    if (Page.IsValid)
                    {
                        string[] strArr = GetReportTitle().Split(',');
                        if (strArr.Count() > 0)
                        {
                            Session["REPORT"] = Convert.ToString(strArr[1] + "mhtml");
                        }
                        Session["FORMAT"] = "MHTML";
                        Session["PARAMETER"] = ProcessHtmlExcelParam();
                        Response.Redirect("ReportRenderView.aspx");
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void OnNext_old(object sender, WizardNavigationEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag

                    if (UCRHS.RadListBoxDestination.Items.Count > 0)
                    {
                        lblError.Text = "";

                        if (Page.IsValid)
                        {  // 
                            if (Wizard1.ActiveStep.ID == "WizardStep5")
                            {
                                switch (ddlFormat.SelectedValue)
                                {
                                    case "MHTML":
                                        Wizard1.Visible = true;
                                        Wizard1.MoveTo(WizardStep1);

                                        break;
                                    case "Excel":
                                        string[] strArr = GetReportTitle().Split(',');
                                        if (strArr.Count() > 0)
                                        {
                                            Session["REPORT"] = Convert.ToString(strArr[1] + "Export");
                                        }
                                        Session["FORMAT"] = "EXCEL";
                                        Session["PARAMETER"] = ProcessResults();
                                        Response.Redirect("ReportRenderView.aspx");
                                        break;
                                    default:
                                        Wizard1.Visible = false;
                                        break;
                                }
                            }

                            if (Wizard1.ActiveStep.ID == "WizardStep1")
                            {
                                // UCRHSort.ColumnList = dicColumnList;

                                foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                {
                                    dicSelectedList.Add(SelectedItem.Value, SelectedItem.Text);
                                }

                                UCRHSort.RadListBoxSource.DataSource = dicSelectedList;
                                UCRHSort.RadListBoxSource.DataValueField = "Key";
                                UCRHSort.RadListBoxSource.DataTextField = "Value";
                                UCRHSort.RadListBoxSource.DataBind();


                                Dictionary<string, string> dicTempSelectedList = dicSelectedList;
                                foreach (KeyValuePair<string, string> pair in dicTempSelectedList)
                                {
                                    dicColumnList.Remove(pair.Key);
                                }
                                Dictionary<string, string> Empty = new Dictionary<string, string>();
                                UCRHSort.RadListBoxDestination.DataSource = Empty;
                                UCRHSort.RadListBoxDestination.DataBind();

                                Session["dicSelectedList"] = dicSelectedList;



                                //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                //{
                                //    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                //}

                                //SelectionDestination.DataSource = dicSelectedList;
                                //SelectionDestination.DataValueField = "Key";
                                //SelectionDestination.DataTextField = "Value";
                                //SelectionDestination.DataBind();

                                //SelectionSource.DataSource = dicSelectedList;
                                //SelectionSource.DataValueField = "Key";
                                //SelectionSource.DataTextField = "Value";
                                //SelectionSource.DataBind();
                            }
                            if (Wizard1.ActiveStep.ID == "WizardStep2")
                            {
                                foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                {
                                    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                }


                                foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxSource.Items)
                                {
                                    dicSelectedListsort.Add(SelectedItem.Value, SelectedItem.Text);
                                }

                                foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxDestination.Items)
                                {
                                    dicSelectedListsortSel.Add(SelectedItem.Value, SelectedItem.Text);
                                }

                                Session["dicSelectedListSort"] = dicSelectedListsort;

                                Session["dicSelectedListSortSel"] = dicSelectedListsortSel;

                                SelectedSource.DataSource = dicSelectedList;
                                SelectedSource.DataValueField = "Key";
                                SelectedSource.DataTextField = "Value";
                                SelectedSource.DataBind();

                                HeaderBySource.DataSource = dicSelectedList;
                                HeaderBySource.DataValueField = "Key";
                                HeaderBySource.DataTextField = "Value";
                                HeaderBySource.DataBind();

                            }

                            if (Wizard1.ActiveStep.ID == "WizardStep4")
                            {
                                txtTitle.Text = lbTitle.Text;

                                // uncmd this code after
                                FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                                using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FPPrincipal identityname = (FPPrincipal)Session[Session.SessionID];
                                    var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == identity.Identity._customerID && x.HomebaseID == identity.Identity._homeBaseId).ToList();

                                    if (objCompany[0].HTMLHeader != string.Empty)
                                    {
                                        chkHeader.Enabled = false;
                                    }
                                    if (objCompany[0].HTMLFooter != string.Empty)
                                    {
                                        chkFooter.Enabled = false;
                                    }
                                    if (objCompany[0].HTMLBackgroun != string.Empty)
                                    {
                                        chkBackground.Enabled = false;
                                    }
                                    string[] strArr = GetReportTitle().Split(',');
                                    if (strArr.Count() > 0)
                                    {
                                        txtTitle.Text = Convert.ToString(strArr[0]);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        lblError.Text = "Select the Fields";
                        e.Cancel = true;

                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void OnNext(object sender, WizardNavigationEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag

                    if (UCRHS.RadListBoxDestination.Items.Count > 0)
                    {
                        lblError.Text = "";

                        if (Page.IsValid)
                        {
                            if (Wizard1.ActiveStep.ID == "WizardStep5")
                            {
                                switch (ddlFormat.SelectedValue)
                                {
                                    case "MHTML":
                                        Wizard1.Visible = true;
                                        Wizard1.MoveTo(WizardStep1);

                                        break;
                                    case "Excel":
                                        string[] strArr = GetReportTitle().Split(',');
                                        if (strArr.Count() > 0)
                                        {
                                            Session["REPORT"] = Convert.ToString(strArr[1] + "Export");
                                        }
                                        Session["FORMAT"] = "EXCEL";
                                        Session["PARAMETER"] = ProcessResults();
                                        Response.Redirect("ReportRenderView.aspx");
                                        break;
                                    default:
                                        Wizard1.Visible = false;
                                        break;
                                }
                            }

                            if (Wizard1.ActiveStep.ID == "WizardStep1")
                            {
                                ////Coded by srini                               

                                foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                {
                                    dicSelectedList.Add(SelectedItem.Value, SelectedItem.Text);
                                }

                                UCRHSort.RadListBoxSource.DataSource = dicSelectedList;
                                UCRHSort.RadListBoxSource.DataValueField = "Key";
                                UCRHSort.RadListBoxSource.DataTextField = "Value";
                                UCRHSort.RadListBoxSource.DataBind();


                                Dictionary<string, string> dicTempSelectedList = dicSelectedList;
                                foreach (KeyValuePair<string, string> pair in dicTempSelectedList)
                                {
                                    dicColumnList.Remove(pair.Key);
                                }
                                Dictionary<string, string> Empty = new Dictionary<string, string>();
                                UCRHSort.RadListBoxDestination.DataSource = Empty;
                                UCRHSort.RadListBoxDestination.DataBind();

                                Session["dicSelectedList"] = dicSelectedList;
                                Session["DestinationList"] = dicSelectedList;

                                //dicSourceList = new Dictionary<string, string>();
                                //foreach (RadListBoxItem radlistitem in UCRHSort.RadListBoxSource.Items)
                                //{
                                //    dicSourceList.Add(radlistitem.Value, radlistitem.Text);
                                //}                                
                                //Session["SourceList"] = dicSourceList;

                                //Session["DestinationList"] = dicSelectedList;

                                dicSourceList = new Dictionary<string, string>();
                                foreach (RadListBoxItem radlistitem in UCRHS.RadListBoxSource.Items)
                                {
                                    dicSourceList.Add(radlistitem.Value, radlistitem.Text);
                                }
                                Session["SourceList"] = dicSourceList;

                                dicDestinationList = new Dictionary<string, string>();
                                foreach (RadListBoxItem radlistitem in UCRHS.RadListBoxDestination.Items)
                                {
                                    dicDestinationList.Add(radlistitem.Value, radlistitem.Text);
                                }
                                Session["DestinationList"] = dicDestinationList;

                                if (Session["SourceSortList"] != null)
                                {
                                    dicSourceSortList = (Dictionary<string, string>)Session["SourceSortList"];
                                    UCRHSort.RadListBoxSource.DataSource = dicSourceSortList;
                                    UCRHSort.RadListBoxSource.DataValueField = "Key";
                                    UCRHSort.RadListBoxSource.DataTextField = "Value";
                                    UCRHSort.RadListBoxSource.DataBind();
                                }
                                else
                                {
                                    ProcessColumns();
                                    ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[1])).DataSource = dicColumnList;
                                    ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[1])).DataBind();
                                    UCRHSort.RadListBoxSource.DataSource = dicColumnList;
                                    UCRHSort.RadListBoxSource.DataValueField = "Key";
                                    UCRHSort.RadListBoxSource.DataTextField = "Value";
                                    UCRHSort.RadListBoxSource.DataBind();
                                }

                                if (Session["DestinationSortList"] != null)
                                {
                                    dicSourceSortList = (Dictionary<string, string>)Session["DestinationSortList"];
                                    UCRHSort.RadListBoxDestination.DataSource = dicSourceSortList;
                                    UCRHSort.RadListBoxDestination.DataValueField = "Key";
                                    UCRHSort.RadListBoxDestination.DataTextField = "Value";
                                    UCRHSort.RadListBoxDestination.DataBind();
                                }
                                else
                                {
                                    UCRHSort.RadListBoxDestination.DataSource = dicEmptyList;
                                    UCRHSort.RadListBoxDestination.DataValueField = "Key";
                                    UCRHSort.RadListBoxDestination.DataTextField = "Value";
                                    UCRHSort.RadListBoxDestination.DataBind();
                                }
                                ////Coded by srini



                                // UCRHSort.ColumnList = dicColumnList;

                                ////Commented by srini
                                //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                //{
                                //    dicSelectedList.Add(SelectedItem.Value, SelectedItem.Text);
                                //}

                                //UCRHSort.RadListBoxSource.DataSource = dicSelectedList;
                                //UCRHSort.RadListBoxSource.DataValueField = "Key";
                                //UCRHSort.RadListBoxSource.DataTextField = "Value";
                                //UCRHSort.RadListBoxSource.DataBind();


                                //Dictionary<string, string> dicTempSelectedList = dicSelectedList;
                                //foreach (KeyValuePair<string, string> pair in dicTempSelectedList)
                                //{
                                //    dicColumnList.Remove(pair.Key);
                                //}
                                //Dictionary<string, string> Empty = new Dictionary<string, string>();
                                //UCRHSort.RadListBoxDestination.DataSource = Empty;
                                //UCRHSort.RadListBoxDestination.DataBind();

                                //Session["dicSelectedList"] = dicSelectedList;
                                ////Commented by srini


                                //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                //{
                                //    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                //}

                                //SelectionDestination.DataSource = dicSelectedList;
                                //SelectionDestination.DataValueField = "Key";
                                //SelectionDestination.DataTextField = "Value";
                                //SelectionDestination.DataBind();

                                //SelectionSource.DataSource = dicSelectedList;
                                //SelectionSource.DataValueField = "Key";
                                //SelectionSource.DataTextField = "Value";
                                //SelectionSource.DataBind();
                            }

                            if (Wizard1.ActiveStep.ID == "WizardStep2")
                            {
                                ////Coded by srini
                                tbEditHeaderText.Text = string.Empty;
                                dicSourceSortList = new Dictionary<string, string>();
                                foreach (RadListBoxItem radlistitem in UCRHSort.RadListBoxSource.Items)
                                {
                                    dicSourceSortList.Add(radlistitem.Value, radlistitem.Text);
                                }
                                Session["SourceSortList"] = dicSourceSortList;
                                dicDestinationSortList = new Dictionary<string, string>();
                                foreach (RadListBoxItem radlistitem in UCRHSort.RadListBoxDestination.Items)
                                {
                                    dicDestinationSortList.Add(radlistitem.Value, radlistitem.Text);
                                }
                                Session["DestinationSortList"] = dicDestinationSortList;

                                if (Session["DestinationList"] != null)
                                {
                                    dicSourceList = (Dictionary<string, string>)Session["DestinationList"];
                                    SelectedSource.DataSource = dicSourceList;
                                    HeaderBySource.DataSource = dicSourceList;
                                }
                                else
                                {
                                    SelectedSource.DataSource = dicEmptyList;
                                    HeaderBySource.DataSource = dicEmptyList;
                                }
                                SelectedSource.DataValueField = "Key";
                                SelectedSource.DataTextField = "Value";
                                SelectedSource.DataBind();
                                HeaderBySource.DataValueField = "Key";
                                HeaderBySource.DataTextField = "Value";
                                HeaderBySource.DataBind();

                                foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                {
                                    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                }


                                foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxSource.Items)
                                {
                                    dicSelectedListsort.Add(SelectedItem.Value, SelectedItem.Text);
                                }

                                foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxDestination.Items)
                                {
                                    dicSelectedListsortSel.Add(SelectedItem.Value, SelectedItem.Text);
                                }

                                Session["dicSelectedListSort"] = dicSelectedListsort;

                                Session["dicSelectedListSortSel"] = dicSelectedListsortSel;

                                SelectedSource.DataSource = dicSelectedList;
                                SelectedSource.DataValueField = "Key";
                                SelectedSource.DataTextField = "Value";
                                SelectedSource.DataBind();

                                HeaderBySource.DataSource = dicSelectedList;
                                HeaderBySource.DataValueField = "Key";
                                HeaderBySource.DataTextField = "Value";
                                HeaderBySource.DataBind();

                                ////Coded by srini

                                ////Commented by srini
                                //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                //{
                                //    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                //}


                                //foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxSource.Items)
                                //{
                                //    dicSelectedListsort.Add(SelectedItem.Value, SelectedItem.Text);
                                //}

                                //foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxDestination.Items)
                                //{
                                //    dicSelectedListsortSel.Add(SelectedItem.Value, SelectedItem.Text);
                                //}

                                //Session["dicSelectedListSort"] = dicSelectedListsort;

                                //Session["dicSelectedListSortSel"] = dicSelectedListsortSel;

                                //SelectedSource.DataSource = dicSelectedList;
                                //SelectedSource.DataValueField = "Key";
                                //SelectedSource.DataTextField = "Value";
                                //SelectedSource.DataBind();

                                //HeaderBySource.DataSource = dicSelectedList;
                                //HeaderBySource.DataValueField = "Key";
                                //HeaderBySource.DataTextField = "Value";
                                //HeaderBySource.DataBind();
                                ////Commented by srini
                            }

                            if (Wizard1.ActiveStep.ID == "WizardStep4")
                            {
                                txtTitle.Text = lbTitle.Text;

                                // uncmd this code after
                                FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                                using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    FPPrincipal identityname = (FPPrincipal)Session[Session.SessionID];
                                    var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == identity.Identity._customerID && x.HomebaseID == identity.Identity._homeBaseId).ToList();

                                    if (objCompany[0].HTMLHeader != string.Empty)
                                    {
                                        chkHeader.Enabled = false;
                                    }
                                    if (objCompany[0].HTMLFooter != string.Empty)
                                    {
                                        chkFooter.Enabled = false;
                                    }
                                    if (objCompany[0].HTMLBackgroun != string.Empty)
                                    {
                                        chkBackground.Enabled = false;
                                    }
                                    string[] strArr = GetReportTitle().Split(',');
                                    if (strArr.Count() > 0)
                                    {
                                        txtTitle.Text = Convert.ToString(strArr[0]);
                                    }
                                }
                            }

                            //if (Wizard1.ActiveStep.ID == "WizardStep5")
                            //{
                            //    switch (ddlFormat.SelectedValue)
                            //    {
                            //        case "MHTML":
                            //            Wizard1.Visible = true;
                            //            Wizard1.MoveTo(WizardStep1);

                            //            break;
                            //        case "Excel":
                            //            string[] strArr = GetReportTitle().Split(',');
                            //            if (strArr.Count() > 0)
                            //            {
                            //                Session["REPORT"] = Convert.ToString(strArr[1] + "Export");
                            //            }
                            //            Session["FORMAT"] = "EXCEL";
                            //            Session["PARAMETER"] = ProcessResults();
                            //            Response.Redirect("ReportRenderView.aspx");
                            //            break;
                            //        default:
                            //            Wizard1.Visible = false;
                            //            break;
                            //    }
                            //}
                        }
                    }
                    else
                    {
                        lblError.Text = "Select the Fields";
                        Session["SourceSortList"] = null;
                        e.Cancel = true;
                    }
                }

                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        public void Page_Init(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag                  
                UCRHS.ColumnList = dicColumnList;
                UCRHSort.ColumnList = dicColumnList;
            }


            /* if (Session["dicSelectedList"] != null)
             {
                 Dictionary<string, string> dicTempSelectedList = (Dictionary<string, string>)Session["dicSelectedList"];
                 foreach (KeyValuePair<string, string> pair in dicTempSelectedList)
                 {
                     dicColumnList.Remove(pair.Key);
                 }

                 UCRHS.ColumnList = dicColumnList;
             }
             else
             {
                 UCRHS.ColumnList = dicColumnList;
             }


             if (Session["dicSelectedListSort"] != null)
             {
                 Dictionary<string, string> dicTempSelectedList = (Dictionary<string, string>)Session["dicSelectedListSort"];
                 UCRHSort.RadListBoxSource.DataSource = dicTempSelectedList;
                 UCRHSort.RadListBoxSource.DataBind();
                 dicTempSelectedList = (Dictionary<string, string>)Session["dicSelectedListSortSel"];
                 UCRHSort.RadListBoxDestination.DataSource = dicTempSelectedList;
                 UCRHSort.RadListBoxDestination.DataBind();
             }
             */

            // UCRHSort.ColumnList = dicColumnList;
        }

        private void ExpandPanel(RadPanelBar panelBar, string TabXmlname, bool isExpand)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(panelBar, TabXmlname, isExpand))
            {
                string Selectedvalue = Convert.ToString(Request.QueryString["xmlFilename"]);
                string attrVal = "";
                string TitleTab = string.Empty;
                XDocument doc = XDocument.Load(Server.MapPath(urlBase + TabXmlname));

                if (Request.QueryString["xmlFilename"] != null)
                {

                    var items = doc.Descendants("screen")
                                  .Where(c => c.Attribute("url").Value.Contains(Convert.ToString(Request.QueryString["xmlFilename"])))
                                   .Select(c => new { x = c.Value, y = c.Parent.Attribute("text").Value, z = c.Attribute("name").Value })
                                   .ToList();

                    if (items != null)
                    {
                        if (items.Count != 0)
                        {

                            RadPanelItem selectedItem = panelBar.FindItemByText(items[0].y);

                            if (selectedItem != null)
                            {
                                if (selectedItem.Items.Count > 0)
                                {

                                    RadPanelItem selectedvalue = selectedItem.Items.FindItemByText(items[0].z);
                                    selectedvalue.Selected = true;
                                    selectedItem.Expanded = true;

                                }
                                else
                                {
                                    selectedItem.Selected = true;
                                    while ((selectedItem != null) &&
                                           (selectedItem.Parent.GetType() == typeof(RadPanelItem)))
                                    {
                                        selectedItem = (RadPanelItem)selectedItem.Parent;
                                        selectedItem.Expanded = true;
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }

        private void ProcessColumns()
        {
            dicColumnList = new Dictionary<string, string>();
            string xmlfilename = string.Empty;
            Session["xmlfilename"] = Convert.ToString(Request.QueryString["xmlFilename"]);
            if (Session["xmlfilename"] != null)
            {
                xmlfilename = (Session["xmlfilename"] != null) ? Convert.ToString(Request.QueryString["xmlFilename"]) : string.Empty;
                //if (xmlfilenametrip != string.Empty)
                //{
                //    xmlfilename = xmlfilenametrip;
                //    Session["xmlfilename"] = xmlfilename;
                //}
            }
            else if (filename != null)
            {
                xmlfilename = filename;
            }

            var filePath = Server.MapPath(urlBase + xmlfilename);
            XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
            var xmlReaderObject = xmlReader.XmlCleanReader();
            if (xmlReaderObject != null)
            {
                XPathDocument ReportFieldDoc = new XPathDocument(xmlReaderObject);
                XPathNodeIterator itr = ReportFieldDoc.CreateNavigator().Select("//Column");
                StringBuilder sb = new System.Text.StringBuilder();

                while (itr.MoveNext())
                {
                    dicColumnList.Add(itr.Current.GetAttribute("name", "").ToString(), itr.Current.GetAttribute("displayname", "").ToString());
                }
            }
        }

        private string ProcessResults()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                var filePath = Server.MapPath(urlBase + filename);
                XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                var xmlReaderObject = xmlReader.XmlCleanReader();
                if (xmlReaderObject != null)
                {
                    XPathDocument ReportFieldDoc = new XPathDocument(xmlReaderObject);
                    string Param = string.Empty;
                    XPathNodeIterator itr = ReportFieldDoc.CreateNavigator().Select("//searchfield");
                    StringBuilder sb = new System.Text.StringBuilder();
                    bool ddlmth = false;

                    XmlDocument doc = new XmlDocument();
                    doc.Load(Server.MapPath(urlBase + filename));

                    //while (itr.MoveNext())
                    //{
                    //    string controlName = itr.Current.GetAttribute("name", "");
                    //    sb.Append(controlName);
                    //    sb.Append("=");

                    //    Control ctrlHolder = FindControlRecursive(Report, controlName);

                    //    if (ctrlHolder is TextBox)
                    //    {
                    //        Session[Session["TabSelect1"] + ctrlHolder.ID] = ((TextBox)ctrlHolder).Text;
                    //        sb.Append(((TextBox)ctrlHolder).Text);
                    //    }

                    //    if (ctrlHolder is RadioButtonList)
                    //    {
                    //        if (((RadioButtonList)ctrlHolder).SelectedItem != null)
                    //        {
                    //            Session[Session["TabSelect1"] + ctrlHolder.ID] = ((RadioButtonList)ctrlHolder).SelectedItem.Value;
                    //            sb.Append(((RadioButtonList)ctrlHolder).SelectedItem.Value);
                    //        }
                    //    }

                    //    if (ctrlHolder is CheckBox)
                    //    {
                    //        Session[Session["TabSelect1"] + ctrlHolder.ID] = ((CheckBox)ctrlHolder).Checked;
                    //        if (((CheckBox)ctrlHolder).Checked == true)
                    //        {
                    //            sb.Append("True");
                    //        }
                    //        else
                    //        {
                    //            sb.Append("False");
                    //        }
                    //    }


                    //    if (ctrlHolder is HiddenField)
                    //    {
                    //        if (((HiddenField)ctrlHolder).Value != null)
                    //        {
                    //            Session[Session["TabSelect1"] + ctrlHolder.ID] = ((HiddenField)ctrlHolder).Value;
                    //            sb.Append(((HiddenField)ctrlHolder).Value);
                    //        }
                    //    }

                    //    if (ctrlHolder is DropDownList)
                    //    {
                    //        if (ctrlHolder.ClientID.IndexOf("MonthFrom") <= 0)
                    //        {
                    //            DropDownList ddlmonth = (DropDownList)innerControl.FindControl("MonthFrom");
                    //            DropDownList ddlmonth = (DropDownList)ctrlHolder;
                    //            if (ddlmonth.SelectedItem.Value != "")
                    //            {
                    //                sb.Append(ddlmonth.SelectedItem.Text);
                    //                Session[Session["TabSelect1"] + ctrlHolder.ID + "ddlMonth"] = ddlmonth.SelectedItem.Value;
                    //            }
                    //        }
                    //    }

                    //    if (ctrlHolder is UserControl)
                    //    {
                    //        if (ctrlHolder.ClientID.IndexOf("MonthFrom") <= 0)
                    //        {
                    //            /*TextBox tbDateOfBirth = (TextBox)ctrlHolder.FindControl("tbDate");
                    //            if (tbDateOfBirth != null)
                    //            {
                    //                if (tbDateOfBirth.Text.Trim().ToString() != string.Empty)
                    //                {
                    //                    sb.Append(DateTime.ParseExact(tbDateOfBirth.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture));
                    //                    sb.Append(Convert.ToDateTime(tbDateOfBirth.Text.Trim(), CultureInfo.InvariantCulture).ToShortDateString());
                    //                }
                    //            }*/



                    //            this is to identify month and year
                    //            XmlNodeList xnList = doc.SelectNodes("/Report/Controls/searchfield[@type='monthyear']");
                    //            if (xnList.Count == 2) //Month and year
                    //            {
                    //                DropDownList ddlmonth = (DropDownList)ctrlHolder.FindControl("ddlMonth");
                    //                if (ddlmonth != null && ddlmth == false)
                    //                {
                    //                    Session[Session["TabSelect1"] + ctrlHolder.ID + "ddlMonth"] = ddlmonth.SelectedItem.Value;
                    //                    sb.Append(ddlmonth.SelectedItem.Text);
                    //                    ddlmth = true;
                    //                    sb.Append(";");
                    //                    continue;
                    //                }
                    //                DropDownList ddlyear = (DropDownList)ctrlHolder.FindControl("ddlyear");
                    //                if (ddlyear != null)
                    //                {
                    //                    Session[Session["TabSelect1"] + ctrlHolder.ID + "ddlyear"] = ddlyear.SelectedItem.Value;
                    //                    sb.Append(ddlyear.SelectedItem.Text);
                    //                }
                    //            }
                    //            else if (xnList.Count == 1) //year
                    //            {

                    //                DropDownList ddlyear = (DropDownList)ctrlHolder.FindControl("ddlyear");
                    //                if (ddlyear != null)
                    //                {
                    //                    Session[Session["TabSelect1"] + ctrlHolder.ID + "ddlyear"] = ddlyear.SelectedItem.Value;
                    //                    sb.Append(ddlyear.SelectedItem.Text);
                    //                }
                    //            }
                    //            /////////////////////////////////////////////////////////////////////////////////////////

                    //        }
                    //        else
                    //        {
                    //            RadMonthYearPicker monthpicker = (RadMonthYearPicker)ctrlHolder.FindControl("tlkMonthView");
                    //            if (!string.IsNullOrEmpty(monthpicker.SelectedDate.ToString().Trim()))
                    //            {
                    //                Session[Session["TabSelect1"] + ctrlHolder.ID + "tlkMonthView"] = monthpicker.SelectedDate.ToString();
                    //                sb.Append(monthpicker.SelectedDate.ToString());
                    //            }

                    //        }

                    //    }
                    //    sb.Append(param1);
                    //    sb.Append(";");
                    //    Param = Convert.ToString(sb);

                    //    if (Param != string.Empty)
                    //    {

                    //        Param = Param.Substring(0, Param.Length - 1);
                    //    }
                    //    Param = Param + ";UserCD=" + UserPrincipal.Identity._name;
                    //}

                    //if (Param == string.Empty)
                    //    Param = "UserCD=" + UserPrincipal.Identity._name;



                    //Added byb Sudhakar

                    switch (ReportString)
                    {
                        case "RptUTAirportPairs":
                            Param = "AirportPairNumber=" + p1;
                            break;

                        case "RptDBFleetProfile":
                            //Param = "UserCD=" + UserPrincipal.Identity._name + ";TailNum=" + p1;
                            Param = "TailNum=" + p1;
                            break;


                        case "RptDBAirport":
                            Param = "IcaoID=" + p1;
                            break;

                        case "RptUTItineraryPlanning":
                            Param = "ItineraryNumber=" + p1;
                            break;

                        case "RptUTWindsOnWorldAirRoutes":
                            Param = "Departure=" + p1 + ";Arrival=" + p2;
                            break;

                        case "RptLocatorAirport":
                            Param = "IcaoID=" + p1 + ";CountryCD=" + p2 + ";Runway=" + Convert.ToInt32(p3) + ";CityName=" + p4 + ";MetroCD=" + p5 + ";IATA=" + p6 + ";MilesFrom=" + Convert.ToInt32(p7) + ";StateName=" + p8 + ";AirportName=" + p9 + ";IsHeliport=" + p10 + ";IsEntryPort=" + p11 + ";IsInActive=" + p12 + ";CountryID=" + p13 + ";AirportID=" + p14;
                            break;

                        case "RptLocatorHotel":
                            Param = "IcaoID=" + p1 + ";CountryCD=" + p2 + ";MilesFrom=" + Convert.ToInt32(p3) + ";CityName=" + p4 + ";MetroCD=" + p5 + ";IATA=" + p6 + ";StateName=" + p7 + ";AirportName=" + p8 + ";CountryID=" + p9 + ";AirportID=" + p10;
                            break;

                        case "RptLocatorVendor":
                            Param = "IcaoID=" + p1 + ";CountryCD=" + p2 + ";MilesFrom=" + Convert.ToInt32(p3) + ";CityName=" + p4 + ";MetroCD=" + p5 + ";AircraftType=" + p6 + ";IATA=" + p7 + ";StateName=" + p8 + ";AirportName=" + p9 + ";AirportID=" + p10;
                            break;

                        case "RptLocatorCustom":
                            Param = "IcaoID=" + p1 + ";CountryCD=" + p2 + ";MilesFrom=" + Convert.ToInt32(p3) + ";CityName=" + p4 + ";MetroCD=" + p5 + ";IATA=" + p6 + ";StateName=" + p7 + ";AirportName=" + p8 + ";AirportID=" + p9;
                            break;

                        case "RptPOSTExpenseCatalogExport":
                            Param = string.Format("LogNum={0};LegNum={1};SlipNum={2}", p1, p2, p3);
                            break;
                        case "RptDBCrewRosterExport":
                            Param = String.Format(
                                    "CrewCD={0};CrewRosterReport={1};Crewchecklist={2};Inactivechecklist={3};" +
                                    "CrewCurrency={4};CrewTypeRating={5};TenToMin={6};ReportHeaderID=10002175006;TempSettings=FUELDTLS::1||FUELVEND::1",
                                    p1, p2, p3, p4, p5, p6, UserPrincipal.Identity._fpSettings._TimeDisplayTenMin);
                            break;
                        default:
                            Param = "AirportPairNumber=" + p1;
                            break;
                    }

                    Param = Param + ";UserCD=" + UserPrincipal.Identity._name;
                    Param = Param + ";UserCustomerID=" + UserPrincipal.Identity._customerID;
                    Param = Param + ";UserHomebaseID=" + UserPrincipal.Identity._homeBaseId;
                    return Param;

                }
                else
                {
                    return "";
                }
            }

        }

        protected void OnPrevious(object sender, WizardNavigationEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag

                    if (UCRHS.RadListBoxDestination.Items.Count > 0)
                    {
                        lblError.Text = "";

                        //if (Page.IsValid)
                        //{  // 
                        if (Wizard1.ActiveStep.ID == "WizardStep5")
                        {
                            switch (ddlFormat.SelectedValue)
                            {
                                case "MHTML":
                                    Wizard1.Visible = true;
                                    Wizard1.MoveTo(WizardStep1);

                                    break;
                                case "Excel":
                                    string[] strArr = GetReportTitle().Split(',');
                                    if (strArr.Count() > 0)
                                    {
                                        Session["REPORT"] = Convert.ToString(strArr[1] + "Export");
                                    }
                                    Session["FORMAT"] = "EXCEL";
                                    Session["PARAMETER"] = ProcessResults();
                                    Response.Redirect("ReportRenderView.aspx");
                                    break;
                                default:
                                    Wizard1.Visible = false;
                                    break;
                            }
                        }

                        if (Wizard1.ActiveStep.ID == "WizardStep4")
                        {
                            ////code commented by srini
                            //txtTitle.Text = lbTitle.Text;

                            ////// uncmd this code after
                            //FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                            //using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                            //{
                            //    FPPrincipal identityname = (FPPrincipal)Session[Session.SessionID];
                            //    var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == identity.Identity._customerID && x.HomebaseID == identity.Identity._homeBaseId).ToList();

                            //    if (objCompany[0].HTMLHeader != string.Empty)
                            //    {
                            //        chkHeader.Enabled = false;
                            //    }
                            //    if (objCompany[0].HTMLFooter != string.Empty)
                            //    {
                            //        chkFooter.Enabled = false;
                            //    }
                            //    if (objCompany[0].HTMLBackgroun != string.Empty)
                            //    {
                            //        chkBackground.Enabled = false;
                            //    }
                            //    string[] strArr = GetReportTitle().Split(',');
                            //    if (strArr.Count() > 0)
                            //    {
                            //        txtTitle.Text = Convert.ToString(strArr[0]);
                            //    }
                            //}
                            ////code commented by srini

                            ////Coded by srini
                            if (Session["SourceSortList"] != null)
                            {
                                dicSourceSortList = (Dictionary<string, string>)Session["SourceSortList"];
                                UCRHSort.RadListBoxSource.DataSource = dicSourceSortList;
                                UCRHSort.RadListBoxSource.DataValueField = "Key";
                                UCRHSort.RadListBoxSource.DataTextField = "Value";
                                UCRHSort.RadListBoxSource.DataBind();
                            }
                            else
                            {
                                UCRHSort.RadListBoxSource.DataSource = dicColumnList;
                                UCRHSort.RadListBoxSource.DataValueField = "Key";
                                UCRHSort.RadListBoxSource.DataTextField = "Value";
                                UCRHSort.RadListBoxSource.DataBind();
                            }
                            if (Session["DestinationSortList"] != null)
                            {
                                dicDestinationSortList = (Dictionary<string, string>)Session["DestinationSortList"];
                                UCRHSort.RadListBoxDestination.DataSource = dicDestinationSortList;
                                UCRHSort.RadListBoxDestination.DataValueField = "Key";
                                UCRHSort.RadListBoxDestination.DataTextField = "Value";
                                UCRHSort.RadListBoxDestination.DataBind();
                            }
                            else
                            {
                                UCRHSort.RadListBoxDestination.DataSource = dicColumnList;
                                UCRHSort.RadListBoxDestination.DataValueField = "Key";
                                UCRHSort.RadListBoxDestination.DataTextField = "Value";
                                UCRHSort.RadListBoxDestination.DataBind();
                            }
                            ////Coded by srini
                        }

                        if (Wizard1.ActiveStep.ID == "WizardStep2")
                        {
                            //////code commented by srini
                            foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                            {
                                dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                            }


                            foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxSource.Items)
                            {
                                dicSelectedListsort.Add(SelectedItem.Value, SelectedItem.Text);
                            }

                            foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxDestination.Items)
                            {
                                dicSelectedListsortSel.Add(SelectedItem.Value, SelectedItem.Text);
                            }

                            Session["dicSelectedListSort"] = dicSelectedListsort;

                            Session["dicSelectedListSortSel"] = dicSelectedListsortSel;

                            SelectedSource.DataSource = dicSelectedList;
                            SelectedSource.DataValueField = "Key";
                            SelectedSource.DataTextField = "Value";
                            SelectedSource.DataBind();

                            HeaderBySource.DataSource = dicSelectedList;
                            HeaderBySource.DataValueField = "Key";
                            HeaderBySource.DataTextField = "Value";
                            HeaderBySource.DataBind();
                            //////code commented by srini

                            ////Coded by srini
                            if (Session["SourceList"] != null)
                            {
                                dicSourceList = (Dictionary<string, string>)Session["SourceList"];
                                UCRHS.RadListBoxSource.DataSource = dicSourceList;
                                UCRHS.RadListBoxSource.DataValueField = "Key";
                                UCRHS.RadListBoxSource.DataTextField = "Value";
                                UCRHS.RadListBoxSource.DataBind();
                            }
                            else
                            {
                                UCRHS.RadListBoxSource.DataSource = dicColumnList;
                                UCRHS.RadListBoxSource.DataValueField = "Key";
                                UCRHS.RadListBoxSource.DataTextField = "Value";
                                UCRHS.RadListBoxSource.DataBind();
                            }
                            if (Session["DestinationList"] != null)
                            {
                                dicDestinationList = (Dictionary<string, string>)Session["DestinationList"];
                                UCRHS.RadListBoxDestination.DataSource = dicDestinationList;
                                UCRHS.RadListBoxDestination.DataValueField = "Key";
                                UCRHS.RadListBoxDestination.DataTextField = "Value";
                                UCRHS.RadListBoxDestination.DataBind();
                            }
                            else
                            {
                                UCRHS.RadListBoxDestination.DataSource = dicColumnList;
                                UCRHS.RadListBoxDestination.DataValueField = "Key";
                                UCRHS.RadListBoxDestination.DataTextField = "Value";
                                UCRHS.RadListBoxDestination.DataBind();
                            }
                            ////Coded by srini
                        }

                        if (Wizard1.ActiveStep.ID == "WizardStep1")
                        {
                            // UCRHSort.ColumnList = dicColumnList;

                            foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                            {
                                dicSelectedList.Add(SelectedItem.Value, SelectedItem.Text);
                            }

                            UCRHSort.RadListBoxSource.DataSource = dicSelectedList;
                            UCRHSort.RadListBoxSource.DataValueField = "Key";
                            UCRHSort.RadListBoxSource.DataTextField = "Value";
                            UCRHSort.RadListBoxSource.DataBind();


                            Dictionary<string, string> dicTempSelectedList = dicSelectedList;
                            foreach (KeyValuePair<string, string> pair in dicTempSelectedList)
                            {
                                dicColumnList.Remove(pair.Key);
                            }
                            Dictionary<string, string> Empty = new Dictionary<string, string>();
                            UCRHSort.RadListBoxDestination.DataSource = Empty;
                            UCRHSort.RadListBoxDestination.DataBind();

                            Session["dicSelectedList"] = dicSelectedList;
                            //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                            //{
                            //    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                            //}

                            //SelectionDestination.DataSource = dicSelectedList;
                            //SelectionDestination.DataValueField = "Key";
                            //SelectionDestination.DataTextField = "Value";
                            //SelectionDestination.DataBind();

                            //SelectionSource.DataSource = dicSelectedList;
                            //SelectionSource.DataValueField = "Key";
                            //SelectionSource.DataTextField = "Value";
                            //SelectionSource.DataBind();
                        }
                        //}
                    }
                    else
                    {
                        lblError.Text = "Select the Fields";
                        e.Cancel = true;

                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {

        }

    }
}