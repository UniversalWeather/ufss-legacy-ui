﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Web.Views.Reports
{
    public partial class FormatReport : BaseSecuredPage
    {
        #region Comments
        
        // Bug 8013 - This Bug was fixed by Karthikeyan.S on 9,Jan 2014
        //  Description - Need to show the ForeColor as Red , When the Textbox in the Grid contains Message.

        #endregion

        private ExceptionManager exManager;
        string ModuleNameConstants = "Tripsheet Report Details";

        protected void Page_Load(object sender, EventArgs e)
        {
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgTripSheetDetail, dgTripSheetDetail, RadAjaxLoadingPanel1);
                        int ReportNumber = 0;
                        Dictionary<string, int> dicRptType = new Dictionary<string, int>();
                        if (Session["SnxmlFilename"] != null)
                        {
                            dicRptType = (Dictionary<string, int>)Session["SnxmlFilename"];
                        }
                        if (Session["SnxmlFilename"] != null)
                        {
                            ReportNumber = dicRptType["xmlFilename"];
                            if (Request.QueryString["ReportNumber"] != null && Request.QueryString["ReportNumber"].Trim() != string.Empty)
                            {
                                ReportNumber = Convert.ToInt32(Convert.ToString(Request.QueryString["ReportNumber"]).Trim());
                            }
                            //int title = 0;
                            //title = (int)Session["SnxmlFilename"];
                            if (ReportNumber == 1)
                                Page.Header.Title ="Overview"; 
                            if (ReportNumber == 2)
                                Page.Header.Title = "Tripsheet Main";
                            if (ReportNumber == 13)
                                Page.Header.Title = "Summary";
                            if (ReportNumber == 3)
                                Page.Header.Title = "Itinerary Report";
                            if (ReportNumber == 4)
                                Page.Header.Title = "FlightLog Information";
                            if (ReportNumber == 5)
                                Page.Header.Title = "Pax Profile";
                            if (ReportNumber == 6)
                                Page.Header.Title = "International Data";
                            if (ReportNumber == 7)
                                Page.Header.Title = "PAX Travel Authorization";
                            if (ReportNumber == 8)
                                Page.Header.Title = "TripSheet Alert";
                            if (ReportNumber == 9)
                                Page.Header.Title = "Paer Form";
                            if (ReportNumber == 10)
                                Page.Header.Title = "General Declaration";
                            if (ReportNumber == 11)
                                Page.Header.Title = "CrewDuty Overview";
                            if (ReportNumber == 12)
                                Page.Header.Title = "TripSheet Checklist";
                            if (ReportNumber == 14)
                                Page.Header.Title = "CanPassForm";
                            if (ReportNumber == 16)
                                Page.Header.Title = "GeneralDeclaration APDX";
                        }
                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            //CheckAutorization(Permission.Database.ViewCompanyProfileCatalog);
                            hdnIsPrint.Value = string.Empty;

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }

        protected void dgTripSheetDetail_BindData_old(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int ReportNumber = 0;
                        Dictionary<string, int> dicRptType = new Dictionary<string, int>();
                        if (Session["SnxmlFilename"] != null)
                        {
                            dicRptType = (Dictionary<string, int>)Session["SnxmlFilename"];
                        }
                        ReportNumber = dicRptType["xmlFilename"];
                        if (Request.QueryString["ReportNumber"] != null && Request.QueryString["ReportNumber"].Trim() != string.Empty)
                        {
                            ReportNumber = Convert.ToInt32(Request.QueryString["ReportNumber"].Trim());
                        }
                        Int64 TripsheetReportHeaderID = 0;
                        if (!string.IsNullOrEmpty(Request.QueryString["TripsheetReportHeaderID"]))
                        {
                            TripsheetReportHeaderID = Convert.ToInt64(Request.QueryString["TripsheetReportHeaderID"]);
                        }

                        GetTripSheetReportDetail oTripSheetReportDetail = new GetTripSheetReportDetail();
                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var ObjRetVal = ObjService.GetTripSheetReportDetail().EntityList.Where(x => x.TripSheetReportHeaderID == TripsheetReportHeaderID
                                                                                                    && x.ReportNumber == ReportNumber
                                                                                                    && x.ParameterVariable != null).OrderBy(x => x.ParameterDescription).ToList();
                            //if (ObjRetVal.Count() != 0)
                            //{
                            dgTripSheetDetail.DataSource = ObjRetVal;
                            //}
                            if (ObjRetVal.Count() != 0)
                            {
                                btnPrint.Visible = true;
                                btnSave.Visible = true;
                            }
                            else
                            {
                                btnPrint.Visible = false;
                                btnSave.Visible = false;
                            }

                            Session["lstTripsheetReportDetail"] = ObjRetVal;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }

        protected void dgTripSheetDetail_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int ReportNumber = 0;
                        Dictionary<string, int> dicRptType = new Dictionary<string, int>();
                        if (Session["SnxmlFilename"] != null)
                        {
                            dicRptType = (Dictionary<string, int>)Session["SnxmlFilename"];
                            ReportNumber = dicRptType["xmlFilename"];
                        }
                       
                        Int64 TripsheetReportHeaderID = 0;
                        if (!string.IsNullOrEmpty(Request.QueryString["TripsheetReportHeaderID"]))
                        {
                            TripsheetReportHeaderID = Convert.ToInt64(Request.QueryString["TripsheetReportHeaderID"]);
                        }
                        if (!string.IsNullOrEmpty(Request.QueryString["ReportNumber"]))
                        {
                            ReportNumber = Convert.ToInt32(Request.QueryString["ReportNumber"]);
                            if (dicRptType.Where(p => p.Key == "xmlFilename").Count() == 0)
                            {
                                dicRptType.Add("xmlFilename", ReportNumber);
                                Session["SnxmlFilename"] = dicRptType;
                            }
                        }
                        GetTripSheetReportDetail oTripSheetReportDetail = new GetTripSheetReportDetail();
                        List<GetTripSheetReportDetail> lstTripSheetReportDetail = new List<GetTripSheetReportDetail>();
                        using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (dicRptType.ContainsKey("xmlFilename"))
                            {
                                if (dicRptType["xmlFilename"] != 4)
                                {
                                    var ObjRetVal = ObjService.GetTripSheetReportDetail().EntityList.Where(x => x.TripSheetReportHeaderID == TripsheetReportHeaderID
                                                                    && x.ReportNumber == ReportNumber
                                                                    && x.ParameterVariable != null
                                                                    ).OrderBy(x => x.ParameterDescription).ToList();
                                    if (ObjRetVal.Count != 0)
                                    {
                                        lstTripSheetReportDetail = ObjRetVal;
                                    }
                                    //dgTripSheetDetail.MasterTableView.ShowFooter = false;
                                }
                                else
                                {
                                    var ObjRetVal = ObjService.GetTripSheetReportDetail().EntityList.Where(x => x.TripSheetReportHeaderID == TripsheetReportHeaderID
                                                                    && x.ReportNumber == ReportNumber
                                                                    && x.ParameterVariable != null
                                                                    && (x.ParameterVariable == "BLANK" || x.ParameterVariable == "SIGNATURE" ||
                                                                    x.ParameterVariable == "MINUTES" || x.ParameterVariable == "COMMENTS"
                                                                    || x.ParameterVariable == "DISPATCHNO" || x.ParameterVariable == "LANDSCAPE"
                                                                    || x.ParameterVariable == "DISPATCHER" || x.ParameterVariable == "DSPTEMAIL" || x.ParameterVariable == "DSPTPHONE")
                                                                    ).OrderBy(x => x.ParameterDescription).ToList();
                                    if (ObjRetVal.Count != 0)
                                    {
                                        lstTripSheetReportDetail = ObjRetVal;
                                    }
                                    //dgTripSheetDetail.MasterTableView.ShowFooter = true;
                                }
                            }
                        }
                        dgTripSheetDetail.DataSource = lstTripSheetReportDetail;
                        if (lstTripSheetReportDetail.Count != 0)
                        {
                            btnPrint.Visible = true;
                            btnSave.Visible = true;
                        }
                        else
                        {
                            btnPrint.Visible = false;
                            btnSave.Visible = false;
                        }
                        Session["lstTripsheetReportDetail"] = lstTripSheetReportDetail;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }

        protected void dgTripSheetDetail_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, int> dicRptType = new Dictionary<string, int>();
                        if (Session["SnxmlFilename"] != null)
                        {
                            dicRptType = (Dictionary<string, int>)Session["SnxmlFilename"];
                        }
                        if (e.Item is GridCommandItem)
                        {
                            GridCommandItem Item = (GridCommandItem)e.Item;
                            if (dicRptType.ContainsKey("xmlFilename"))
                            {
                                if (dicRptType["xmlFilename"] != null)
                                {
                                    if (dicRptType["xmlFilename"] == 4)
                                    {
                                        ((Label)Item.FindControl("lbCustom1")).Visible = true;
                                    }
                                    else
                                    {
                                        ((Label)Item.FindControl("lbCustom1")).Visible = false;
                                    }
                                }
                            }
                        }
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            if (Item.GetDataKeyValue("ParameterType") != null)
                            {
                                if (Item.GetDataKeyValue("ParameterType").ToString().Trim().ToUpper() == "L")
                                {
                                    ((TextBox)Item.FindControl("tbIsSelected")).Enabled = true;
                                    ((TextBox)Item.FindControl("tbNumber")).Enabled = false;
                                    ((TextBox)Item.FindControl("tbNumber")).Text = string.Empty;
                                    ((TextBox)Item.FindControl("tbMessage")).Enabled = false;

                                    if (Convert.ToBoolean(Item.GetDataKeyValue("ParameterLValue")) == true)
                                    {
                                        ((TextBox)Item.FindControl("tbIsSelected")).Text = "Y";
                                    }
                                    else if (Convert.ToBoolean(Item.GetDataKeyValue("ParameterLValue")) == false)
                                    {
                                        ((TextBox)Item.FindControl("tbIsSelected")).Text = "N";
                                    }
                                    else
                                    {
                                        ((TextBox)Item.FindControl("tbIsSelected")).Text = string.Empty;
                                    }
                                    //Commented by Ramesh on 1st Aug 2013 to fix defect# 7175
                                    //if (!string.IsNullOrEmpty(((TextBox)Item.FindControl("tbMessage")).Text))
                                    //{
                                    //    ((TextBox)Item.FindControl("tbMessage")).Text = "Message";
                                    //}
                                    //else
                                    //{
                                    ((TextBox)Item.FindControl("tbMessage")).Text = string.Empty;
                                    //}
                                }
                                else if (Item.GetDataKeyValue("ParameterType").ToString().Trim().ToUpper() == "N")
                                {
                                    ((TextBox)Item.FindControl("tbIsSelected")).Enabled = false;
                                    ((TextBox)Item.FindControl("tbIsSelected")).Text = string.Empty;
                                    ((TextBox)Item.FindControl("tbNumber")).Enabled = true;
                                    ((TextBox)Item.FindControl("tbMessage")).Enabled = false;
                                    //Commented by Ramesh on 1st Aug 2013 to fix defect# 7175
                                    //if (!string.IsNullOrEmpty(((TextBox)Item.FindControl("tbMessage")).Text))
                                    //{
                                    //    ((TextBox)Item.FindControl("tbMessage")).Text = "Message";
                                    //}
                                    //else
                                    //{
                                    ((TextBox)Item.FindControl("tbMessage")).Text = string.Empty;
                                    //}
                                    if (Item.GetDataKeyValue("ParameterSize") != null)
                                    {
                                        string WarningMessage = string.Empty;
                                        string LowerBound = string.Empty, UpperBound = string.Empty;
                                        string[] SplitSize = new string[2];
                                        SplitSize = Item.GetDataKeyValue("ParameterSize").ToString().Trim().Split('-');

                                        if (Item.GetDataKeyValue("WarningMessage") != null)
                                        {
                                            WarningMessage = Item.GetDataKeyValue("WarningMessage").ToString();
                                        }

                                        if ((!string.IsNullOrEmpty(SplitSize[0])) && (!string.IsNullOrEmpty(SplitSize[1])))
                                        {
                                            LowerBound = SplitSize[0];
                                            UpperBound = SplitSize[1];
                                            ((TextBox)Item.FindControl("tbNumber")).Attributes.Add("onblur", "CheckRange('" + ((TextBox)Item.FindControl("tbNumber")).ClientID + "','"
                                                                                                                            + UpperBound + "','" + LowerBound + "','" + WarningMessage + "')");
                                        }
                                    }
                                }
                                else
                                {
                                    ((TextBox)Item.FindControl("tbIsSelected")).Enabled = false;
                                    ((TextBox)Item.FindControl("tbIsSelected")).Text = string.Empty;
                                    ((TextBox)Item.FindControl("tbNumber")).Enabled = false;
                                    ((TextBox)Item.FindControl("tbNumber")).Text = string.Empty;
                                    ((TextBox)Item.FindControl("tbMessage")).Enabled = true;
                                    ((TextBox)Item.FindControl("tbMessage")).ReadOnly = true;
                                    if (!string.IsNullOrEmpty(((TextBox)Item.FindControl("tbMessage")).Text))
                                    {
                                        ((TextBox)Item.FindControl("tbMessage")).Text = "Message";

                                    }
                                    else
                                    {
                                        ((TextBox)Item.FindControl("tbMessage")).Text = string.Empty;
                                    }
                                }
                                //Commented by Ramesh on 1st Aug 2013 to fix defect# 7175

                                //if (Convert.ToBoolean(Item.GetDataKeyValue("ParameterLValue")) == true && Convert.ToBoolean(((TextBox)Item.FindControl("tbNumber")).Enabled) == false && Convert.ToBoolean(((TextBox)Item.FindControl("tbMessage")).Enabled) == false)
                                //{
                                //    ((TextBox)Item.FindControl("tbIsSelected")).Text = "Y";
                                //}
                                //else if (Convert.ToBoolean(Item.GetDataKeyValue("ParameterLValue")) == false && Convert.ToBoolean(((TextBox)Item.FindControl("tbNumber")).Enabled) == false && Convert.ToBoolean(((TextBox)Item.FindControl("tbMessage")).Enabled) == false)
                                //{
                                //    ((TextBox)Item.FindControl("tbIsSelected")).Text = "N";
                                //}
                                //else
                                //{
                                //    ((TextBox)Item.FindControl("tbIsSelected")).Text = string.Empty;
                                //    ((TextBox)Item.FindControl("tbIsSelected")).Enabled = false;
                                //}
                                //HttpUtility.HtmlEncode("ParameterLValue");
                            }
                            if (((TextBox)Item.FindControl("tbMessage")).Enabled == true)
                            {
                                if (((TextBox)Item.FindControl("tbMessage")).Text == "Message")
                                {
                                    ((TextBox)Item.FindControl("tbMessage")).ToolTip = "Double Click to Edit or View the Message";
                                    ((TextBox)Item.FindControl("tbMessage")).ForeColor = System.Drawing.Color.Red;  //8013
                                }
                                else
                                {
                                    ((TextBox)Item.FindControl("tbMessage")).Text = "Message";
                                    ((TextBox)Item.FindControl("tbMessage")).ForeColor = System.Drawing.Color.Black;  //8013
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgTripSheetDetail;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }

        private void DeleteTripSheetReportDetail()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                {
                    TripSheetReportDetail objTripSheetReportDetail;
                    foreach (GridDataItem Item in dgTripSheetDetail.Items)
                    {
                        objTripSheetReportDetail = new TripSheetReportDetail();
                        if (Item.GetDataKeyValue("TripSheetReportDetailID") != null)
                        {
                            objTripSheetReportDetail.TripSheetReportDetailID = Convert.ToInt64(Item.GetDataKeyValue("TripSheetReportDetailID").ToString());
                        }
                        objTripSheetReportDetail.IsDeleted = true;
                        objService.DeleteTripSheetReportDetail(objTripSheetReportDetail);
                    }
                }
            }
        }

        private List<TripSheetReportDetail> SaveTripSheetReportDetail(string OpType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                {
                    List<TripSheetReportDetail> oTripSheetReportDetail = new List<TripSheetReportDetail>();
                    TripSheetReportDetail objTripSheetReportDetail;
                    foreach (GridDataItem Item in dgTripSheetDetail.Items)
                    {
                        objTripSheetReportDetail = new TripSheetReportDetail();
                        if (Item.GetDataKeyValue("TripSheetReportDetailID") != null)
                        {
                            objTripSheetReportDetail.TripSheetReportDetailID = Convert.ToInt64(Item.GetDataKeyValue("TripSheetReportDetailID").ToString());
                        }
                        if (Item.GetDataKeyValue("CustomerID") != null)
                        {
                            objTripSheetReportDetail.CustomerID = Convert.ToInt64(Item.GetDataKeyValue("CustomerID").ToString());
                        }
                        if (Item.GetDataKeyValue("TripSheetReportHeaderID") != null)
                        {
                            objTripSheetReportDetail.TripSheetReportHeaderID = Convert.ToInt64(Item.GetDataKeyValue("TripSheetReportHeaderID").ToString());
                        }
                        if (Item.GetDataKeyValue("ReportNumber") != null)
                        {
                            objTripSheetReportDetail.ReportNumber = Convert.ToInt32(Item.GetDataKeyValue("ReportNumber").ToString());
                            hdnIsPrint.Value = Convert.ToString(Item.GetDataKeyValue("ReportNumber").ToString());
                        }
                        if (Item.GetDataKeyValue("ReportDescription") != null)
                        {
                            objTripSheetReportDetail.ReportDescription = Item.GetDataKeyValue("ReportDescription").ToString();
                        }
                        //if (!string.IsNullOrEmpty(((TextBox)Item.FindControl("tbIsSelected")).Text))
                        //{
                        //    if (((TextBox)Item.FindControl("tbIsSelected")).Text == "Y")
                        //    {
                        //        objTripSheetReportDetail.IsSelected = true;
                        //    }
                        //    else
                        //    {
                        //        objTripSheetReportDetail.IsSelected = false;
                        //    }
                        //}
                        if (Item.GetDataKeyValue("IsSelected") != null)
                        {
                            objTripSheetReportDetail.IsSelected = Convert.ToBoolean(Item.GetDataKeyValue("IsSelected").ToString());
                        }
                        if (Item.GetDataKeyValue("Copies") != null)
                        {
                            objTripSheetReportDetail.Copies = Convert.ToInt32(Item.GetDataKeyValue("Copies").ToString());
                        }
                        if (Item.GetDataKeyValue("ReportProcedure") != null)
                        {
                            objTripSheetReportDetail.ReportProcedure = Item.GetDataKeyValue("ReportProcedure").ToString();
                        }
                        if (Item.GetDataKeyValue("ParameterVariable") != null)
                        {
                            objTripSheetReportDetail.ParameterVariable = Item.GetDataKeyValue("ParameterVariable").ToString();
                        }
                        if (Item.GetDataKeyValue("ParameterDescription") != null)
                        {
                            objTripSheetReportDetail.ParameterDescription = Item.GetDataKeyValue("ParameterDescription").ToString();
                        }
                        if (Item.GetDataKeyValue("ParameterType") != null)
                        {
                            objTripSheetReportDetail.ParameterType = Item.GetDataKeyValue("ParameterType").ToString();
                        }
                        if (Item.GetDataKeyValue("ParameterWidth") != null)
                        {
                            objTripSheetReportDetail.ParameterWidth = Convert.ToInt32(Item.GetDataKeyValue("ParameterWidth").ToString());
                        }
                        if (Item.GetDataKeyValue("ParameterCValue") != null)
                        {
                            objTripSheetReportDetail.ParameterCValue = Item.GetDataKeyValue("ParameterCValue").ToString();
                        }
                        if (!string.IsNullOrEmpty(((TextBox)Item.FindControl("tbIsSelected")).Text))
                        {
                            if (((TextBox)Item.FindControl("tbIsSelected")).Text.ToUpper() == "Y")
                            {
                                objTripSheetReportDetail.ParameterLValue = true;
                            }
                            else
                            {
                                objTripSheetReportDetail.ParameterLValue = false;
                            }
                            //objTripSheetReportDetail.ParameterLValue = Convert.ToBoolean(Item.GetDataKeyValue("ParameterLValue").ToString());
                        }
                        //if (Item.GetDataKeyValue("ParameterNValue") != null)
                        //{
                        //    objTripSheetReportDetail.ParameterNValue = Convert.ToInt32(Item.GetDataKeyValue("ParameterNValue").ToString());
                        //}
                        if (!string.IsNullOrEmpty(((TextBox)Item.FindControl("tbNumber")).Text))
                        {
                            objTripSheetReportDetail.ParameterNValue = Convert.ToInt32(((TextBox)Item.FindControl("tbNumber")).Text);
                        }
                        if (!string.IsNullOrEmpty(((TextBox)Item.FindControl("tbMessage")).Text))
                        {
                            //objTripSheetReportDetail.ParameterValue = Item.GetDataKeyValue("ParameterValue").ToString();
                            objTripSheetReportDetail.ParameterValue = Convert.ToString(Item.GetDataKeyValue("ParameterValue"));
                        }
                        if (Item.GetDataKeyValue("ReportOrder") != null)
                        {
                            objTripSheetReportDetail.ReportOrder = Convert.ToInt32(Item.GetDataKeyValue("ReportOrder").ToString());
                        }
                        if (Item.GetDataKeyValue("TripSheetOrder") != null)
                        {
                            objTripSheetReportDetail.TripSheetOrder = Convert.ToInt32(Item.GetDataKeyValue("TripSheetOrder").ToString());
                        }
                        if (Item.GetDataKeyValue("LastUpdUID") != null)
                        {
                            objTripSheetReportDetail.LastUpdUID = Item.GetDataKeyValue("LastUpdUID").ToString();
                        }
                        if (Item.GetDataKeyValue("LastUpdTS") != null)
                        {
                            objTripSheetReportDetail.LastUpdTS = Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString());
                        }
                        if (Item.GetDataKeyValue("IsDeleted") != null)
                        {
                            objTripSheetReportDetail.IsDeleted = Convert.ToBoolean(Item.GetDataKeyValue("IsDeleted").ToString());
                        }
                        if (Item.GetDataKeyValue("ParameterSize") != null)
                        {
                            objTripSheetReportDetail.ParameterSize = Item.GetDataKeyValue("ParameterSize").ToString();
                        }
                        if (Item.GetDataKeyValue("WarningMessage") != null)
                        {
                            objTripSheetReportDetail.WarningMessage = Item.GetDataKeyValue("WarningMessage").ToString();
                        }
                        oTripSheetReportDetail.Add(objTripSheetReportDetail);
                        if (OpType == "Save")
                        {
                            objService.AddTripSheetReportDetail(objTripSheetReportDetail);
                            hdnIsPrint.Value = "";
                            Session["FormatReportPrint"] = "0";
                        }
                    }
                    if (OpType == "Print")
                    {
                        Session["TripSheetReportDetailList"] = oTripSheetReportDetail;
                        InjectScript.Text = "<script type='text/javascript'>Close(" + hdnIsPrint.Value + ")</script>";
                        Session["FormatReportPrint"] = "1";
                    }
                    else
                    {
                        hdnIsPrint.Value = "0";
                    }
                    return oTripSheetReportDetail;
                }
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Ramesh:Commented not required to delete.
                        //DeleteTripSheetReportDetail();
                        SaveTripSheetReportDetail("Save");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Fadeoutdiv", "fadeoutdivsaved();", true);
                hdnSaveMSG.Value = "Saved";
            }
        }

        protected void btnPrint_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveTripSheetReportDetail("Print");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }

        protected void btnFRSaveMessage_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        List<GetTripSheetReportDetail> oTripSheetReportDetail = new List<GetTripSheetReportDetail>();
                        if (!string.IsNullOrEmpty(hdnFRMessageID.Value))
                        {
                            oTripSheetReportDetail = ((List<GetTripSheetReportDetail>)Session["lstTripsheetReportDetail"]).Where(x => x.TripSheetReportDetailID == Convert.ToInt64(hdnFRMessageID.Value)).ToList();
                            if ((oTripSheetReportDetail.Count != 0) && (oTripSheetReportDetail.Count == 1))
                            {
                                TripSheetReportDetail objTripSheetReportDetail = new TripSheetReportDetail();
                                objTripSheetReportDetail.Copies = oTripSheetReportDetail[0].Copies;
                                objTripSheetReportDetail.CustomerID = oTripSheetReportDetail[0].CustomerID;
                                objTripSheetReportDetail.IsDeleted = oTripSheetReportDetail[0].IsDeleted;
                                objTripSheetReportDetail.IsSelected = oTripSheetReportDetail[0].IsSelected;
                                objTripSheetReportDetail.LastUpdTS = oTripSheetReportDetail[0].LastUpdTS;
                                objTripSheetReportDetail.LastUpdUID = oTripSheetReportDetail[0].LastUpdUID;
                                objTripSheetReportDetail.ParameterCValue = oTripSheetReportDetail[0].ParameterCValue;
                                objTripSheetReportDetail.ParameterDescription = oTripSheetReportDetail[0].ParameterDescription;
                                objTripSheetReportDetail.ParameterLValue = oTripSheetReportDetail[0].ParameterLValue;
                                objTripSheetReportDetail.ParameterNValue = oTripSheetReportDetail[0].ParameterNValue;
                                objTripSheetReportDetail.ParameterSize = oTripSheetReportDetail[0].ParameterSize;
                                objTripSheetReportDetail.ParameterType = oTripSheetReportDetail[0].ParameterType;
                                objTripSheetReportDetail.ParameterVariable = oTripSheetReportDetail[0].ParameterVariable;
                                objTripSheetReportDetail.ParameterWidth = oTripSheetReportDetail[0].ParameterWidth;
                                objTripSheetReportDetail.ReportDescription = oTripSheetReportDetail[0].ReportDescription;
                                objTripSheetReportDetail.ReportNumber = oTripSheetReportDetail[0].ReportNumber;
                                objTripSheetReportDetail.ReportOrder = oTripSheetReportDetail[0].ReportOrder;
                                objTripSheetReportDetail.ReportProcedure = oTripSheetReportDetail[0].ReportProcedure;
                                objTripSheetReportDetail.TripSheetOrder = oTripSheetReportDetail[0].TripSheetOrder;
                                objTripSheetReportDetail.TripSheetReportHeaderID = oTripSheetReportDetail[0].TripSheetReportHeaderID;
                                objTripSheetReportDetail.WarningMessage = oTripSheetReportDetail[0].WarningMessage;
                                objTripSheetReportDetail.TripSheetReportDetailID = oTripSheetReportDetail[0].TripSheetReportDetailID;
                                objTripSheetReportDetail.ParameterValue = tbFRMessage.Text;
                                using (FlightPakMasterService.MasterCatalogServiceClient objService = new MasterCatalogServiceClient())
                                {
                                    objService.AddTripSheetReportDetail(objTripSheetReportDetail);
                                }
                            }
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "ClosePopup", "CloseMessagePopup();", true);
                            dgTripSheetDetail.Rebind();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }
               
        protected void chkSelectAll_OnCheckedChanged(object sender, EventArgs e)
        {
            RadWindowManager1.RadConfirm("All parameter values will be modified. Do you want to proceed?", "confirmCallBackFormatReport", 300, 100, null, ModuleNameConstants);
        }
       
        protected void SetYesNo(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string SelectedValue = string.Empty;
                        if (chkSelectAll.Checked == true)
                        {
                            SelectedValue = "Y";
                        }
                        else
                        {
                            SelectedValue = "N";
                        }
                        foreach (GridDataItem Item in dgTripSheetDetail.Items)
                        {
                            ((TextBox)Item.FindControl("tbIsSelected")).Text = SelectedValue;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }

        //PP23012013
        protected void btnCancelYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string SelectedValue = string.Empty;
                        if (chkSelectAll.Checked == true)
                        {
                            SelectedValue = "Y";
                        }
                        else
                        {
                            SelectedValue = "N";
                        }
                        foreach (GridDataItem Item in dgTripSheetDetail.Items)
                        {
                            if (((TextBox) Item.FindControl("tbIsSelected")).Enabled == true)
                            {
                                ((TextBox) Item.FindControl("tbIsSelected")).Text = SelectedValue;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }

        protected void btnCancelNo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        chkSelectAll.Checked = false;

                        RadAjaxManager.GetCurrent(Page).FocusControl(chkSelectAll);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }
    }
}