﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreflightTripSheetReportViewer.aspx.cs"
    EnableEventValidation="false" MasterPageFile="~/Framework/Masters/Popup.Master"
    Inherits="FlightPak.Web.Views.Reports.PreflightTripSheetReportViewer" %>

<%@ Register TagPrefix="UC" TagName="DatePicker" Src="~/UserControls/UCRptDateRange.ascx" %>
<%@ Register Src="~/UserControls/UCMultiSelect.ascx" TagName="Grid" TagPrefix="UCMultiSelect" %>
<%@ Register Src="~/UserControls/UCReportHtmlSelection.ascx" TagName="Grid" TagPrefix="UCReportHtmlSelection" %>
<%@ Register Src="~/UserControls/UCRptMonthYear.ascx" TagName="Grid" TagPrefix="UCRptMonthYear" %>
<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">
    <link href="../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
    <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jquery.alerts.js") %>"></script>
    <script type="text/javascript" src="../../Scripts/Common.js"></script>
    <style type="text/css">
        .style1
        {
            width: 131px;
        }
        .style2
        {
            width: 176px;
        }
        .style3
        {
            width: 178px;
        }
        .style4 {
              margin-left:-30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript">
            var currentTextBox = null;
            var currentDatePicker = null;

            //This method is called to handle the onclick and onfocus client side events for the texbox
            function showPopup(sender, e) {
                //this is a reference to the texbox which raised the event
                //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                //this gets a reference to the datepicker, which will be shown, to facilitate
                //the selection of a date
                var datePicker = $find("<%= RadDatePicker1.ClientID %>");

                //this variable is used to store a reference to the date picker, which is currently
                //active
                currentDatePicker = datePicker;

                //this method first parses the date, that the user entered or selected, and then
                //sets it as a selected date to the picker
                datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));

                //the code lines below show the calendar, which is used to select a date. The showPopup
                //function takes three arguments - the x and y coordinates where to show the calendar, as
                //well as its height, derived from the offsetHeight property of the textbox
                var position = datePicker.getElementPosition(currentTextBox);
                datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
            }
            //this handler is used to set the text of the TextBox to the value of selected from the popup
            function dateSelected(sender, args) {
                if (currentTextBox != null) {
                    //currentTextBox is the currently selected TextBox. Its value is set to the newly selected
                    //value of the picker
                    currentTextBox.value = args.get_newValue();
                }
            }
            function handle(e) {
                if (e.keyCode == 13) {
                    document.getElementById('<%=btnRptView.ClientID%>').focus();
                }
            }

            function CopySuccessful(arg) {
                document.getElementById('<%=Leg.ClientID%>').focus();
            }

            function openWin(url, value, radWin) {

                //                if (url == "../Settings/Company/AuthorizationPopup.aspx?AuthorizationCD=1") 
                //                {
                //                    if (document.getElementById('<%=hdnDept.ClientID%>').value == '') {
                //                        alert("Please Select the Department Code");
                //                    }
                //                    else {
                //                        url = "../Settings/Company/AuthorizationPopup.aspx?AuthorizationCD=1&deptid=" + document.getElementById('<%=hdnDept.ClientID%>').value;
                //                        var oWnd = radopen(url + value, radWin);
                //                        oWnd.ReloadOnShow = true;
                //                    }

                //                }
                //                else 
                //                {
                var oWnd = radopen(url + value, radWin);
                oWnd.ReloadOnShow = true;
                // }

            }

            function openWinTrip(radWin) {
                var url = '';
                //                var oWnd = radopen("../Settings/Company/CompanyMasterPopup.aspx?HomeBase=" + document.getElementById("<%=TripNum.ClientID%>").value, "RadWindow1");
                //                var oWnd = radopen("../Settings/Company/CompanyMasterPopup.aspx?HomeBase=" + document.getElementById("<%=Leg.ClientID%>").value, "RadWindow2");
                //                var oWnd = radopen("../Settings/Company/CompanyMasterPopup.aspx?HomeBase=" + document.getElementById("<%=Passenger.ClientID%>").value, "RadWindow3");
                if (radWin == "RadWindow2") {
                    url = '../Transactions/Preflight/PreflightSearchRPTPopup.aspx?FromPage=Report';
                }
                else if (radWin == "RadWindow3") {

                    var filename = window.location.search.substring(1);
                    if (filename == "xmlFilename=PREGeneralDeclarationAPDX.xml" || filename == "xmlFilename=PRETSGeneralDeclaration.xml") {
                        url = '../Transactions/Preflight/PreflightLegPopup.aspx?TripID=' + document.getElementById('<%=hdnTripNum.ClientID%>').value + '&MultiSelect=true';
                    }
                    else {
                        url = '../Transactions/Preflight/PreflightLegPopup.aspx?TripID=' + document.getElementById('<%=hdnTripNum.ClientID%>').value + '&MultiSelect=true';

                    }
                }
                else if (radWin == "RadWindow4") {
                    url = '../Transactions/Preflight/PreflightPaxPopup.aspx?IsUIReports=1&TripID=' + document.getElementById('<%=hdnTripNum.ClientID%>').value + '&MultiSelect=true';
                }

                else if (radWin == "RadDepartmentMasterPopup") {
                    url = '/Views/Settings/Company/DepartmentAuthorizationPopup.aspx?IsUIReports=1&DepartmentCD=' + document.getElementById('<%=tbDepartmentCD.ClientID%>').value;
                }
                else if (radWin == "RadAuthorizationMasterPopup") {
                    url = '/Views/Settings/Company/AuthorizationPopup.aspx?IsUIReports=1&AuthorizationCD=' + document.getElementById('<%=tbAuth.ClientID%>').value + '&deptId=' + document.getElementById('<%=hdnDepartment.ClientID%>').value;
                }
                else if (radWin == "RadClientCodeMasterPopup") {
                    url = '/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('<%=tbClientCode.ClientID%>').value;
                }
                else if (radWin == "RadHomeBaseMasterPopup") {
                    url = '/Views/Settings/Company/CompanyMasterPopup.aspx?HomeBase='+document.getElementById("<%=tbHomebase.ClientID%>").value;
                }
                else if (radWin == "radPaxInfoPopup") {
                    url = '/Views/Settings/people/PassengerRequestorsPopup.aspx?IsUIReports=1';
                }
                else if (radWin == "radCrewRosterPopup") {
                    url = '/Views/Settings/People/CrewRosterPopup.aspx?IsUIReports=1';
                }
                else if (radWin == "radFleetProfilePopup") {
                    url = '/Views/Settings/Fleet/FleetProfilePopup.aspx?IsUIReports=1';
                }
                else if (radWin == "RadCatPopup") {
                    url = '/Views/Settings/Fleet/FlightCategoryPopup.aspx?FlightCatagoryCD='+ document.getElementById("<%= tbCat.ClientID %>").value;
                }
    var oWnd = radopen(url, radWin);

}


function ConfirmClose(WinName) {
    var oManager = GetRadWindowManager();
    var oWnd = oManager.GetWindowByName(WinName);
    var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
    CloseButton.onclick = function () {
        CurrentWinName = oWnd.Id;
        radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
    }
}

function GetDimensions(sender, args) {
    var bounds = sender.getWindowBounds();
    return;
}
function GetRadWindow() {
    var oWindow = null;
    if (window.radWindow) oWindow = window.radWindow;
    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
    return oWindow;
}


function OnClientClose(oWnd, args) {
    var arg = args.get_argument();
    if (arg != null) {
        var combo = $find(arg.CallingButton);

        if (arg.DepartmentID != '')
            document.getElementById('<%=hdnDept.ClientID%>').value = arg.DepartmentID;
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById(arg.CallingButton).value = arg.Arg1;
                        }
                        else {
                            document.getElementById(arg.CallingButton).value = "";
                            combo.clearSelection();
                        }
                    }
                }
            }


            function OnClientClose2(oWnd, args) {
                var combo = $find('<%= TripNum.ClientID %>');
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById('<%=TripNum.ClientID%>').value = arg.TripNUM;
                        document.getElementById('<%=hdnTripNum.ClientID%>').value = arg.TripID;

                        var step = "SearchBox_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }

                    else {
                        document.getElementById("<%=TripNum.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                    var a = document.getElementById('<%=Leg.ClientID %>');
                    var c = document.getElementById('<%=btnLeg.ClientID %>');
                    a.disabled = false;
                    a.value = '';
                    c.disabled = false;
                    var b = document.getElementById('<%=Passenger.ClientID%>');
                    var d = document.getElementById('<%=btnPassenger.ClientID%>');
                    b.disabled = false;
                    b.value = '';
                    d.disabled = false;
                    document.getElementById('<%=lbTripSearchMessage.ClientID%>').innerHTML = "";

                }
                else {
                    document.getElementById('<%=Leg.ClientID %>').disabled = true;
                    document.getElementById('<%=Passenger.ClientID%>').disable = true;
                }
            }
            function OnClientClose3(oWnd, args) {
                var combo = $find("<%= Leg.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {

                        document.getElementById("<%=Leg.ClientID%>").value = arg.LegNUM;
                        document.getElementById("<%=hdnLeg.ClientID%>").value = arg.LegID;

                        var step = "SearchLeg_TextChanged";
                        var ajaxManager = $find('<%= RadAjaxManager1.ClientID%>');
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=Leg.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }

            }
            function OnClientClose4(oWnd, args) {
                var combo = $find("<%= Passenger.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {

                        document.getElementById("<%=Passenger.ClientID%>").value = arg.PassengerRequestorCD;
                        //document.getElementById("<%=hdnPassenger.ClientID%>").value = arg.PreflightPassengerListID;
                        var step = "SearchPassenger_TextChanged";
                        var ajaxManager = $find('<%= RadAjaxManager1.ClientID%>');
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }

                    }
                    else {
                        document.getElementById("<%=Passenger.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }

            }
            function OnClientCloseDepartmentPopup(oWnd, args) {


                var combo = $find("<%= tbDepartmentCD.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbDepartmentCD.ClientID%>").value = arg.DepartmentCD;
                        document.getElementById("<%=hdnDepartment.ClientID%>").value = arg.DepartmentID;

                        var step = "Dept_TextChanged";
                        var ajaxManager = $find('<%= RadAjaxManager1.ClientID%>');
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbDepartmentCD.ClientID%>").value = "";
                        document.getElementById("<%=hdnDepartment.ClientID%>").value = "";
                        combo.clearSelection();
                    }

                    var dep = document.getElementById('<%=tbAuth.ClientID %>');
                    var btn = document.getElementById('<%=btnAuth.ClientID %>');
                    dep.disabled = false;
                    dep.value = '';
                    btn.disabled = false;
                }
                else {
                    document.getElementById('<%=tbAuth.ClientID %>').disabled = true;
                }
            }
            function OnClientCloseAuthorizationPopup(oWnd, args) {

                var combo = $find("<%= tbAuth.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbAuth.ClientID%>").value = arg.AuthorizationCD;
                        document.getElementById("<%=hdnAuth.ClientID%>").value = arg.AuthorizationID;
                        CheckDeptAuthorization();

                        var step = "tbAuth_OnTextChanged";
                        var ajaxManager = $find('<%= RadAjaxManager1.ClientID%>');
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbAuth.ClientID%>").value = "";
                        document.getElementById("<%=hdnAuth.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseClientCodePopup(oWnd, args) {

                var combo = $find("<%= tbClientCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClientCode.ClientID%>").value = htmlDecode(arg.ClientCD)
                        document.getElementById("<%=hdnClientCode.ClientID%>").value = arg.ClientID

                        var step = "tbClientCde_OnTextChanged";
                        var ajaxManager = $find('<%= RadAjaxManager1.ClientID%>');
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbClientCode.ClientID%>").value = "";
                        document.getElementById("<%=hdnClientCode.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseHomeBasePopup(oWnd, args) {

                var combo = $find("<%= tbHomebase.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbHomebase.ClientID%>").value = arg.HomeBase;
                        document.getElementById("<%=hdnHomebase.ClientID%>").value = arg.HomebaseID;

                        var step = "tbHomebase_OnTextChanged";
                        var ajaxManager = $find('<%= RadAjaxManager1.ClientID%>');
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbHomebase.ClientID%>").value = "";
                        document.getElementById("<%=hdnHomebase.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseCrewPopup(oWnd, args) {
                var combo = $find("<%= tbCrew.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCrew.ClientID%>").value = arg.CrewCD;
                        document.getElementById("<%=hdnCrew.ClientID%>").value = arg.CrewID;

                        var step = "tbCrew_OnTextChanged";
                        var ajaxManager = $find('<%= RadAjaxManager1.ClientID%>');
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbCrew.ClientID%>").value = "";
                        document.getElementById("<%=hdnCrew.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseCatPopup(oWnd, args) {
                var combo = $find("<%= tbCat.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbCat.ClientID%>").value = arg.FlightCatagoryCD;
                        document.getElementById("<%=hdnCat.ClientID%>").value = arg.FlightCategoryID;

                        var step = "tbCat_OnTextChanged";
                        var ajaxManager = $find('<%= RadAjaxManager1.ClientID%>');
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbCat.ClientID%>").value = "";
                        document.getElementById("<%=hdnCat.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseTailNumberPopup(oWnd, args) {
                var combo = $find("<%= tbTailNumber.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbTailNumber.ClientID%>").value = arg.TailNum;
                        document.getElementById("<%=hdnTailNumber.ClientID%>").value = arg.FleetId;

                        var step = "tbTailNumber_OnTextChanged";
                        var ajaxManager = $find('<%= RadAjaxManager1.ClientID%>');
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbTailNumber.ClientID%>").value = "";
                        document.getElementById("<%=hdnTailNumber.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function OnClientCloseRequestorPopup(oWnd, args) {
                var combo = $find("<%= tbRequestor.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbRequestor.ClientID%>").value = arg.PassengerRequestorCD;
                        document.getElementById("<%=hdnRequestor.ClientID%>").value = arg.PassengerID;

                        var step = "tbRequestor_OnTextChanged";
                        var ajaxManager = $find('<%= RadAjaxManager1.ClientID%>');
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbRequestor.ClientID%>").value = "";
                        document.getElementById("<%=hdnRequestor.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }
            function CheckDeptAuthorization() {
                if (document.getElementById('<%=tbDepartmentCD.ClientID%>').value == "") {
                    alert("Please enter the Department Code before entering the Authorization Code.");
                    document.getElementById('<%=tbAuth.ClientID%>').value = "";
                    document.getElementById('<%=tbDepartmentCD.ClientID%>').focus();
                    return false;
                }
            }

            function SetButtonStatus(sender, target1, target2) {
                if (sender.value.length >= 0) {
                    document.getElementById(target1).disabled = false;
                    document.getElementById(target2).disabled = false;
                }

                else {
                    document.getElementById(target1).disabled = false;
                    document.getElementById(target2).disabled = false;

                }

            }

            function FormatSelected() {

                if (document.getElementById('TripExcel')) {
                    document.getElementById('TripExcel').style.visibility = 'hidden';
                }
                if (document.getElementById('TripExcelGdl')) {
                    document.getElementById('TripExcelGdl').style.visibility = 'hidden';
                }
                if (document.getElementById('Wizard1')) {
                    document.getElementById('Wizard1').style.visibility = 'hidden';
                }
            }
            function openTab(type) {
                if (type == '' || type == undefined || type == null) {
                    if ($telerik.isMobileSafari) {
                        var url = 'ReportRenderView.aspx';
                        var a = window.document.createElement("a");
                        a.target = '_parent';
                        a.href = url;
                        window.open(url, '_parent');
                    } else {
                        $.get("ReportRenderView.aspx", function (d) {
                            $("#PDFViewerFrame").attr("src", "PDFViewer.aspx").show();
                        });
                    }
                }
                else {
                    window.location.href = "ReportRenderView.aspx";
                }
            };

        </script>
    </telerik:RadCodeBlock>
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" DateFormat="MM/dd/yyyy" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server" ShowRowHeaders="false">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                    ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true"
        ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" OnClientClose="OnClientClose" Modal="true"
                Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow2" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose2" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/PreflightSearchRPTPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow3" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose3" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/PreflightLegPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindow4" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose4" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/PreflightPaxPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadDepartmentMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseDepartmentPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadAuthorizationMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseAuthorizationPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadClientCodeMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseClientCodePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadHomeBaseMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseHomeBasePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewRosterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseCrewPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/people/CrewRosterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewRosterCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadCatPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseCatPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseRequestorPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/people/PassengerRequestorsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFleetProfilePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseTailNumberPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFleetProfileCRUDPopup" runat="server" Height="800px" Width="800px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadHomeBasePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClosetrpHomeBasePopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadClientPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClosetrpClientPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadUserGroupPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClosetrpUserGroupPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadFormatReportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClosetrpFormatReportPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
        </Windows>
        <%-- <Windows>
            <telerik:RadWindow ID="radCrewPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseFederalTaxPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radlegPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseStateTaxPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="radPASSANGERPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseSalesTaxPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
        </Windows>--%>
    </telerik:RadWindowManager>
    <div class="art-setting-menu-report">
        <div class="art-setting-content-report">
            <div class="globalMenu_mapping">
                <%--<a href="#" class="globalMenu_mapping">Home</a> <span class="breadcrumbCaret"></span>
            <a href="#" class="globalMenu_mapping">Settings</a>--%>
            </div>
           
     
            <div class="art-setting-content-rpt">
                <telerik:RadCodeBlock ID="RadCodeBlock3" runat="server">
                    <script type="text/javascript" language="javascript">
                        function openWin(radWin) {
                            var url = '';
                            if (radWin == "RadHomeBasePopup") {
                                url = "../Settings/Company/CompanyMasterPopup.aspx?HomeBase=" + document.getElementById("<%=tbTrpRptWrtHomeBase.ClientID%>").value;
                            }
                            else if (radWin == "RadClientPopup") {
                                url = "../Settings/Company/ClientCodePopup.aspx?IsUIReports=1&ClientCD=" + document.getElementById('<%=tbTrpRptWrtClient.ClientID%>').value;
                            }
                            else if (radWin == "RadUserGroupPopup") {
                                url = "../Settings/Company/UserGrouppopup.aspx?IsUIReports=1&UserGroupCD=" + document.getElementById('<%=tbTrpRptWrtUserGroup.ClientID%>').value;
                            }
                            else if (radWin == "RadFormatReportPopup") {
                                if (document.getElementById('<%=hdnTrpRptWrtReportID.ClientID%>').value != "") {

                                    oArg = new Object();
                                    grid = $find("<%= dgReportSheet.ClientID %>");
                                    var MasterTable = grid.get_masterTableView();
                                    var selectedRows = MasterTable.get_selectedItems();
                                    var SelectedMain = false;
                                    var ReportNumber = '';
                                    if (selectedRows.length > 0) {
                                        for (var i = 0; i < selectedRows.length; i++) {
                                            var row = selectedRows[i];
                                            var ReportNumber = $find("<%=dgReportSheet.ClientID %>").get_masterTableView().get_selectedItems()[0].getDataKeyValue("ReportNumber");
                                        }
                                    }
                                    url = "FormatReport.aspx?TripsheetReportHeaderID=" + document.getElementById('<%=hdnTrpRptWrtReportID.ClientID%>').value + "&ReportNumber=" + ReportNumber;
                                }
                                else {
                                    radalert('Please select the Tripsheet report.', 360, 50, 'Tripsheet Report Writer');
                                }
                            }
                if (url != "") {
                    var oWnd = radopen(url, radWin);
                }
            }
            function OnClientClosetrpHomeBasePopup(oWnd, args) {
                var combo = $find("<%= tbTrpRptWrtHomeBase.ClientID %>");
                            //get the transferred arguments
                            var arg = args.get_argument();
                            if (arg !== null) {
                                if (arg) {
                                    document.getElementById("<%=tbTrpRptWrtHomeBase.ClientID%>").value = arg.HomeBase;
                                    document.getElementById("<%=hdnTrpRptWrtHomeBaseID.ClientID%>").value = arg.HomebaseID;
                                }
                                else {
                                    document.getElementById("<%=tbTrpRptWrtHomeBase.ClientID%>").value = "";
                                    document.getElementById("<%=hdnTrpRptWrtHomeBaseID.ClientID%>").value = "";
                                    combo.clearSelection();
                                }
                            }
                        }
                        function OnClientClosetrpClientPopup(oWnd, args) {
                            var combo = $find("<%= tbTrpRptWrtClient.ClientID %>");
                            //get the transferred arguments
                            var arg = args.get_argument();
                            if (arg !== null) {
                                if (arg) {
                                    document.getElementById("<%=tbTrpRptWrtClient.ClientID%>").value = htmlDecode(arg.ClientCD);
                                    document.getElementById("<%=hdnTrpRptWrtClientID.ClientID%>").value = arg.ClientID;
                                }
                                else {
                                    document.getElementById("<%=tbTrpRptWrtClient.ClientID%>").value = "";
                                    document.getElementById("<%=hdnTrpRptWrtClientID.ClientID%>").value = "";
                                    combo.clearSelection();
                                }
                            }
                        }
                        function OnClientClosetrpUserGroupPopup(oWnd, args) {
                            var combo = $find("<%= tbTrpRptWrtUserGroup.ClientID %>");
                            //get the transferred arguments
                            var arg = args.get_argument();
                            if (arg !== null) {
                                if (arg) {
                                    document.getElementById("<%=tbTrpRptWrtUserGroup.ClientID%>").value = arg.UserGroupCD;
                                    document.getElementById("<%=hdnTrpRptWrtUserGroupID.ClientID%>").value = arg.UserGroupCD;
                                }
                                else {
                                    document.getElementById("<%=tbTrpRptWrtUserGroup.ClientID%>").value = "";
                                    document.getElementById("<%=hdnTrpRptWrtUserGroupID.ClientID%>").value = "";
                                    combo.clearSelection();
                                }
                            }
                        }
                        function OnClientClosetrpFormatReportPopup(oWnd, args) {
                            var arg = args.get_argument();
                            if (arg !== null) {
                                if (arg) {
                                    if (arg.IsPrint != "") {
                                        document.getElementById("<%=hdnPrintNumber.ClientID%>").value = arg.IsPrint;
                                        document.getElementById("<%=btnRptView1.ClientID%>").click();
                                    }
                                }
                            }
                        }
                        function ProcessUpdateTrip() {
                            var grid = $find("<%=dgTripsheetWriter.ClientID%>");
                            if (grid.get_masterTableView().get_selectedItems().length == 0) {
                                radalert('Please select a record from the above table', 330, 100, "Edit", "");
                                return false;
                            }
                        }
                        function ProcessDeleteTrip(customMsg) {

                            //get reference to the client grid object using the global window variable set in the user control's Page_Load server handler
                            var grid = $find("<%=dgTripsheetWriter.ClientID%>");

                            var msg = 'Are you sure you want to delete this record?';

                            if (customMsg != null) {
                                msg = customMsg;
                            }

                            if (grid.get_masterTableView().get_selectedItems().length > 0) {

                                radconfirm(msg, callBackFn, 330, 100, '', 'Delete');
                                return false;
                            }
                            else {
                                radalert('Please select a record from the above table', 330, 100, "Delete", "");
                                return false;
                            }
                        }
                        function prepareSearchInput(input) { input.value = input.value; }
                    </script>
                </telerik:RadCodeBlock>
                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="tbTrpRptWrtReportID_OnTextChanged">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="tbTrpRptWrtHomeBase_OnTextChanged">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="tbTrpRptWrtClient_OnTextChanged">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="tbTrpRptWrtUserGroup_OnTextChanged">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="DivExternalForm2">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm2" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>

                        <telerik:AjaxSetting AjaxControlID="DivExternalForm3">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm3" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm2" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="SearchBox_TextChanged">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm2" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="SearchLeg_TextChanged">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm2" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="SearchPassenger_TextChanged">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivExternalForm2" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="dgTripsheetWriter">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="DivLinks" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="DivLinks">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="ExportPanal" LoadingPanelID="RadAjaxLoadingPanel1" />
                                <telerik:AjaxUpdatedControl ControlID="TripExcel" LoadingPanelID="RadAjaxLoadingPanel1" />
                                <telerik:AjaxUpdatedControl ControlID="Wizard1" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="btExportFileFormat">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="Wizard1" LoadingPanelID="RadAjaxLoadingPanel1" />
                                <telerik:AjaxUpdatedControl ControlID="UCRHS" LoadingPanelID="RadAjaxLoadingPanel1" />
                                <telerik:AjaxUpdatedControl ControlID="TripExcel" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="btExport">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="Wizard1" LoadingPanelID="RadAjaxLoadingPanel1" />
                                <telerik:AjaxUpdatedControl ControlID="DivLinks" LoadingPanelID="RadAjaxLoadingPanel1" />
                                <telerik:AjaxUpdatedControl ControlID="TripExcel" LoadingPanelID="RadAjaxLoadingPanel1" />
                                <telerik:AjaxUpdatedControl ControlID="ExportPanal" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="btClose">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="Wizard1" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <telerik:AjaxSetting AjaxControlID="TripExcel">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="Wizard1" LoadingPanelID="RadAjaxLoadingPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                    </AjaxSettings>
                </telerik:RadAjaxManager>
                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
                <asp:Panel ID="pnlReportContent" runat="server" DefaultButton="btnRptView" >
                    <div id="DivExternalForm" runat="server" class="border-box">
                        <table width="99%" cellpadding="0" cellspacing="0" >
                            <tr>
                                <td>
                                    <asp:Panel ID="pnlGridPanel" runat="server" Visible="true"   >
                                        <telerik:RadPanelBar ID="pnlTripRptWrt" Width="100%"  ExpandAnimation-Type="none"
                                            CollapseAnimation-Type="none" runat="server">
                                            <Items>
                                                <telerik:RadPanelItem runat="server" Expanded="false" Text="Tripsheet Report Writer">
                                                    <Items>
                                                        <telerik:RadPanelItem Value="TripsheetReportWriter" runat="server">
                                                            <ContentTemplate>
                                                                <telerik:RadGrid ID="dgTripsheetWriter" runat="server" AllowSorting="true" AutoGenerateColumns="false"
                                                                    PageSize="10" AllowPaging="true" AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true"
                                                                    Height="240px" OnNeedDataSource="dgTripsheetWriter_BindData" OnSelectedIndexChanged="dgTripsheetWriter_SelectedIndexChanged"
                                                                    OnItemCommand="dgTripsheetWriter_ItemCommand" OnUpdateCommand="dgTripsheetWriter_UpdateCommand"
                                                                    OnInsertCommand="dgTripsheetWriter_InsertCommand" OnDeleteCommand="dgTripsheetWriter_DeleteCommand"
                                                                    OnPreRender="dgTripsheetWriter_PreRender" >
                                                                    <MasterTableView DataKeyNames="TripSheetReportHeaderID,ReportID,ReportDescription,HomebaseCD,HomebaseID,ClientCD,ClientID,UserGroupCD,UserGroupID,IsDeleted"
                                                                        CommandItemDisplay="Bottom">
                                                                        <Columns>
                                                                            <telerik:GridBoundColumn DataField="TripSheetReportHeaderID" HeaderText="TripSheetReportHeaderID"
                                                                                CurrentFilterFunction="EqualTo" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                                                                                HeaderStyle-Width="120px" Display="false" FilterDelay="500">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn DataField="ReportID" HeaderText="Report ID" CurrentFilterFunction="StartsWith"
                                                                                ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="120px" FilterDelay="500">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn DataField="ReportDescription" HeaderText="Description" CurrentFilterFunction="StartsWith"
                                                                                ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="200px" FilterDelay="500">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn DataField="HomeBaseCD" HeaderText="Home Base" CurrentFilterFunction="StartsWith"
                                                                                ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="120px" FilterDelay="500">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn DataField="UserGroupCD" HeaderText="User Group" CurrentFilterFunction="StartsWith"
                                                                                ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="120px" FilterDelay="500">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridBoundColumn DataField="ClientCD" HeaderText="Client" CurrentFilterFunction="StartsWith"
                                                                                ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="120px" FilterDelay="500">
                                                                            </telerik:GridBoundColumn>
                                                                        </Columns>
                                                                        <CommandItemTemplate>
                                                                            <div style="padding: 5px 5px; float: left; clear: both;">
                                                                                <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"><img style="border:0px;vertical-align:middle;" alt="Add" 
                                                                        src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdateTrip();"
                                                                                    ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>"  /></asp:LinkButton>
                                                                                <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDeleteTrip();"
                                                                                    runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                                                                            </div>
                                                                            <div>
                                                                                <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                                                                            </div>
                                                                        </CommandItemTemplate>
                                                                    </MasterTableView>
                                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                        <Selecting AllowRowSelect="true" />
                                                                    </ClientSettings>
                                                                    <GroupingSettings CaseSensitive="false" />
                                                                </telerik:RadGrid>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Panel ID="pnlControlsPanel" runat="server" Visible="true">
                                                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <div class="tblspace_5">
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <table>
                                                                                                            <tr align="left" valign="top">
                                                                                                                <td class="tdLabel80">
                                                                                                                    Report ID
                                                                                                                </td>
                                                                                                                <td class="tdLabel150">
                                                                                                                    <asp:TextBox ID="tbTrpRptWrtReportID" CssClass="text100" runat="server" MaxLength="8"
                                                                                                                        AutoPostBack="true" OnTextChanged="tbTrpRptWrtReportID_OnTextChanged" />
                                                                                                                    <asp:HiddenField ID="hdnTrpRptWrtReportID" runat="server" />
                                                                                                                    <asp:HiddenField ID="hdnReportNumber" runat="server" />
                                                                                                                </td>
                                                                                                                <td class="tdLabel70">
                                                                                                                    Description
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="tbTrpRptWrtDescription" CssClass="text200" runat="server" MaxLength="40" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr align="left" valign="top">
                                                                                                                <td>
                                                                                                                </td>
                                                                                                                <td colspan="3">
                                                                                                                    <asp:RequiredFieldValidator ID="rfvTrpRptWrtReportID" runat="server" ValidationGroup="TrpRptSave"
                                                                                                                        ControlToValidate="tbTrpRptWrtReportID" Text="Report ID is Required." Display="Dynamic"
                                                                                                                        CssClass="alert-text" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                                                                                    <asp:CustomValidator ID="cvTrpRptWrtReportID" runat="server" ControlToValidate="tbTrpRptWrtReportID"
                                                                                                                        ErrorMessage="Unique Report ID is Required." Display="Dynamic" CssClass="alert-text"
                                                                                                                        ValidationGroup="TrpRptSave"></asp:CustomValidator>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <table>
                                                                                                            <tr align="left" valign="top">
                                                                                                                <td class="tdLabel80">
                                                                                                                    Home Base
                                                                                                                </td>
                                                                                                                <td class="tdLabel150">
                                                                                                                    <asp:TextBox ID="tbTrpRptWrtHomeBase" CssClass="text100" runat="server" MaxLength="4"
                                                                                                                        AutoPostBack="true" OnTextChanged="tbTrpRptWrtHomeBase_OnTextChanged" />
                                                                                                                    <asp:Button ID="btnTrpRptWrtHomeBase" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadHomeBasePopup');return false;" />
                                                                                                                    <asp:HiddenField ID="hdnTrpRptWrtHomeBaseID" runat="server" />
                                                                                                                </td>
                                                                                                                <td class="tdLabel70">
                                                                                                                    Client
                                                                                                                </td>
                                                                                                                <td class="tdLabel150">
                                                                                                                    <asp:TextBox ID="tbTrpRptWrtClient" CssClass="text100" runat="server" MaxLength="5"
                                                                                                                        AutoPostBack="true" OnTextChanged="tbTrpRptWrtClient_OnTextChanged" />
                                                                                                                    <asp:Button ID="btnTrpRptWrtClient" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadClientPopup');return false;" />
                                                                                                                    <asp:HiddenField ID="hdnTrpRptWrtClientID" runat="server" />
                                                                                                                </td>
                                                                                                                <td class="tdLabel80">
                                                                                                                    User Group
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="tbTrpRptWrtUserGroup" CssClass="text100" runat="server" MaxLength="5"
                                                                                                                        AutoPostBack="true" OnTextChanged="tbTrpRptWrtUserGroup_OnTextChanged" />
                                                                                                                    <asp:Button ID="btnTrpRptWrtUserGroup" runat="server" CssClass="browse-button" OnClientClick="javascript:openWin('RadUserGroupPopup');return false;" />
                                                                                                                    <asp:HiddenField ID="hdnTrpRptWrtUserGroupID" runat="server" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr align="left" valign="top">
                                                                                                                <td>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:CustomValidator ID="cvTrpRptWrtHomeBase" runat="server" ControlToValidate="tbTrpRptWrtHomeBase"
                                                                                                                        ValidationGroup="TrpRptSave" ErrorMessage="Invalid Home Base." Display="Dynamic"
                                                                                                                        CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:CustomValidator ID="cvTrpRptWrtClient" runat="server" ControlToValidate="tbTrpRptWrtClient"
                                                                                                                        ValidationGroup="TrpRptSave" ErrorMessage="Invalid Client Code" Display="Dynamic"
                                                                                                                        CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:CustomValidator ID="cvTrpRptWrtUserGroup" runat="server" ControlToValidate="tbTrpRptWrtUserGroup"
                                                                                                                        ValidationGroup="TrpRptSave" ErrorMessage="Invalid Client Code." Display="Dynamic"
                                                                                                                        CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <table>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <telerik:RadGrid ID="dgReportSheet" runat="server" AllowSorting="true" AutoGenerateColumns="false"
                                                                                                                        OnItemDataBound="CrewRoster_ItemDataBound" PageSize="10" AllowPaging="false"
                                                                                                                        AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" Width="750px" Height="341px"
                                                                                                                        OnPreRender="dgReportSheet_PreRender" OnItemCommand="dgReportSheet_ItemCommand"
                                                                                                                        >
                                                                                                                        <MasterTableView DataKeyNames="TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected"
                                                                                                                            ClientDataKeyNames="ReportNumber" AllowPaging="false" CommandItemDisplay="None">
                                                                                                                            <Columns>
                                                                                                                                <telerik:GridTemplateColumn HeaderText="Assigned" CurrentFilterFunction="StartsWith"
                                                                                                                                    HeaderStyle-Width="100px" AutoPostBackOnFilter="false" ShowFilterIcon="false"
                                                                                                                                    AllowFiltering="false" UniqueName="ReportNum" FilterDelay="500">
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <asp:CheckBox ID="chkReportNum" runat="server" />
                                                                                                                                    </ItemTemplate>
                                                                                                                                </telerik:GridTemplateColumn>
                                                                                                                                <telerik:GridBoundColumn DataField="ReportDescription" HeaderText="Report Description"
                                                                                                                                    HeaderStyle-Width="650px" UniqueName="ReportDescription" CurrentFilterFunction="StartsWith"
                                                                                                                                    ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500">
                                                                                                                                </telerik:GridBoundColumn>
                                                                                                                            </Columns>
                                                                                                                        </MasterTableView>
                                                                                                                        <ClientSettings EnablePostBackOnRowClick="true">
                                                                                                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                                                                            <Selecting AllowRowSelect="true" />
                                                                                                                        </ClientSettings>
                                                                                                                        <GroupingSettings CaseSensitive="false" />
                                                                                                                    </telerik:RadGrid>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="right">
                                                                                                        <table>
                                                                                                            <tr>
                                                                                                                <td colspan="2">
                                                                                                                    <asp:Button ID="btnFormatReport" runat="server" CssClass="button" Text="Format Editor"
                                                                                                                        OnClientClick="javascript:openWin('RadFormatReportPopup');return false;" />
                                                                                                                    <asp:HiddenField ID="hdnPrintNumber" runat="server" />
                                                                                                                </td>
                                                                                                                <td class="custom_radbutton">
                                                                                                                    <telerik:RadButton ID="btnSave" runat="server" Text="Save" ValidationGroup="TrpRptSave"
                                                                                                                        OnClick="btnSave_OnClick" />
                                                                                                                    <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_OnClick" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </asp:Panel>
                                                                                        <asp:HiddenField ID="hdnSave" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </telerik:RadPanelItem>
                                                    </Items>
                                                </telerik:RadPanelItem>
                                            </Items>
                                        </telerik:RadPanelBar>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <div class="tblspace_10">
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" width="100%" class="border-box">
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td class="style3">
                                            <span class="mnd_text">
                                                <asp:Label runat="server" ID="lbTitle"></asp:Label></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style3">
                                            <div class="tblspace_10">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style3">
                                            <asp:RadioButtonList runat="server" ID="rdBl" OnSelectedIndexChanged="rdBl_SelectedIndexChanged"
                                                AutoPostBack="true">
                                                <asp:ListItem Text="Trip No." Value="TripNumber" Selected="True" />
                                                <asp:ListItem Text="Date Range" Value="DateRange" />
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td>
                                                       <div id="DivExternalForm3" runat="server" class="ExternalForm">
                                                        <asp:PlaceHolder ID="Report" runat="server">
                                                            <asp:HiddenField ID="hdnDept" runat="server" />
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <b>Date From</b>
                                                                    </td>
                                                                    <td>
                                                                        <UC:DatePicker ID="DateFrom" runat="server" Required="true" DateValidationgroup="ReportValidator" />
                                                                    </td>
                                                                    <%--<td>
                                                                <asp:RequiredFieldValidator ID="rfvDateFrom" CssClass="alert-text" ErrorMessage="Required Field"
                                                                    runat="server" ControlToValidate="ucDateFrom" Display="Dynamic" />
                                                            </td>--%>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <b>Date To</b>
                                                                    </td>
                                                                    <td>
                                                                        <UC:DatePicker ID="DateTo" runat="server" Required="true" DateValidationgroup="ReportValidator" />
                                                                    </td>
                                                                    <%--<td>
                                                                <asp:RequiredFieldValidator ID="rfvDateto" CssClass="alert-text" ErrorMessage="Required Field"
                                                                    runat="server" ControlToValidate="ucDateTo" Display="Dynamic" />
                                                            </td>--%>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Client Code
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbClientCode" runat="server" MaxLength="8" OnTextChanged="tbClientCde_OnTextChanged"
                                                                            AutoPostBack="true"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdnClientCode" runat="server" />
                                                                        <asp:Button ID="btnClientCode" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinTrip('RadClientCodeMasterPopup');return false;" />
                                                                        <asp:CustomValidator ID="cvClientCde" runat="server" ControlToValidate="tbClientCode"
                                                                            ErrorMessage="Invalid Client Code." Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Home Base
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbHomebase" runat="server" MaxLength="8" OnTextChanged="tbHomebase_OnTextChanged"
                                                                            AutoPostBack="true"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdnHomebase" runat="server" />
                                                                        <asp:Button ID="btnHomebase" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinTrip('RadHomeBaseMasterPopup');return false;" />
                                                                        <asp:CustomValidator ID="cvHomebase" runat="server" ControlToValidate="tbHomebase"
                                                                            ErrorMessage="Invalid Homebase." Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Requestor
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbRequestor" runat="server" MaxLength="8" OnTextChanged="tbRequestor_OnTextChanged"
                                                                            AutoPostBack="true"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdnRequestor" runat="server" />
                                                                        <asp:Button ID="btnRequestor" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinTrip('radPaxInfoPopup');return false;" />
                                                                        <asp:CustomValidator ID="cvRequestor" runat="server" ControlToValidate="tbRequestor"
                                                                            ErrorMessage="Invalid Requestor." Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Crew
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCrew" runat="server" MaxLength="8" OnTextChanged="tbCrew_OnTextChanged"
                                                                            AutoPostBack="true"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdnCrew" runat="server" />
                                                                        <asp:Button ID="btnCrew" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinTrip('radCrewRosterPopup');return false;" />
                                                                        <asp:CustomValidator ID="cvCrew" runat="server" ControlToValidate="tbCrew" ErrorMessage="Invalid Crew."
                                                                            Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Tail No.
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbTailNumber" runat="server" MaxLength="8" OnTextChanged="tbTailNumber_OnTextChanged"
                                                                            AutoPostBack="true"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdnTailNumber" runat="server" />
                                                                        <asp:Button ID="btnTailNumber" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinTrip('radFleetProfilePopup');return false;" />
                                                                        <asp:CustomValidator ID="cvTailNumber" runat="server" ControlToValidate="tbTailNumber"
                                                                            ErrorMessage="Invalid Tail Number." Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Department
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbDepartmentCD" runat="server" MaxLength="8" AutoPostBack="true"
                                                                            OnTextChanged="Dept_TextChanged"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdnDepartment" runat="server" />
                                                                        <asp:Button ID="btnDepartment" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinTrip('RadDepartmentMasterPopup');return false;" />
                                                                        <asp:Label ID="lbdept" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Authorization
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbAuth" runat="server" MaxLength="8" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                            Enabled="false" OnTextChanged="tbAuth_OnTextChanged" AutoPostBack="true"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdnAuth" runat="server" />
                                                                        <asp:Button ID="btnAuth" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinTrip('RadAuthorizationMasterPopup');return false;" />
                                                                        <asp:CustomValidator ID="cvAuth" runat="server" ControlToValidate="tbAuth" ErrorMessage="Invalid Authorization."
                                                                            Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Category
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbCat" runat="server" MaxLength="8" OnTextChanged="tbCat_OnTextChanged"
                                                                            AutoPostBack="true"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdnCat" runat="server" />
                                                                        <asp:Button ID="btnCat" runat="server" CssClass="browse-button" OnClientClick="javascript:openWinTrip('RadCatPopup');return false;" />
                                                                        <asp:CustomValidator ID="cvCat" runat="server" ControlToValidate="tbCat" ErrorMessage="Invalid Flight Category."
                                                                            Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkTrip" runat="server" Text="Trip" Checked="true" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkCanceled" runat="server" Text="Canceled" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkHold" runat="server" Text="Hold" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkWorkSheet" runat="server" Text="Worksheet" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkUnFulFilled" runat="server" Text="Unfulfilled" />
                                                                    </td>
                                                                </tr>
                                                                <tr style="display: none;">
                                                                    <td>
                                                                        <asp:CheckBox ID="chkScheduledService" runat="server" Text="Scheduled Service" Checked="true" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:PlaceHolder>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div id="DivExternalForm2" runat="server" class="ExternalForm">
                                                            <asp:PlaceHolder ID="TripReport" runat="server">
                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="tdLabel90">
                                                                                        <b>No.</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="TripNum" runat="server" AutoPostBack="true" OnTextChanged="SearchBox_TextChanged"
                                                                                            onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdnTripNum" runat="server" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Button ID="btnTripNum" runat="server" OnClientClick="javascript:openWinTrip('RadWindow2');return false;"
                                                                                            CssClass="browse-button" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:RequiredFieldValidator ID="rfvTripNum" CssClass="alert-text" ErrorMessage="Trip No. is Required"
                                                                                            runat="server" ControlToValidate="TripNum" Display="Dynamic" ValidationGroup="ReportValidator" />
                                                                                        <asp:Label CssClass="alert-text" runat="server" ID="lbTripSearchMessage" ForeColor="Red"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label Text="Leg" runat="server" ID="lblLeg"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="Leg" runat="server" Enabled="false" onKeyPress="return fnAllowNumericAndChar(this, event,',-')"
                                                                                            AutoPostBack="true" OnTextChanged="SearchLeg_TextChanged"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdnLeg" runat="server" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Button ID="btnLeg" runat="server" Enabled="false" OnClientClick="javascript:openWinTrip('RadWindow3');return false;"
                                                                                            CssClass="browse-button" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:RequiredFieldValidator ID="rfvLeg" CssClass="alert-text" ErrorMessage="Leg No. is Required"
                                                                                            runat="server" ControlToValidate="Leg" Display="Dynamic" ValidationGroup="ReportValidator" />
                                                                                        <asp:CustomValidator ID="cvLeg" runat="server" ControlToValidate="Leg" ErrorMessage="Invalid Trip Leg"
                                                                                            Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Passenger
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="Passenger" runat="server" Enabled="false" AutoPostBack="true" OnTextChanged="SearchPassenger_TextChanged"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdnPassenger" runat="server" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Button ID="btnPassenger" runat="server" Enabled="false" OnClientClick="javascript:openWinTrip('RadWindow4');return false;"
                                                                                            CssClass="browse-button" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CustomValidator ID="cvPassenger" runat="server" ControlToValidate="Passenger"
                                                                                            ErrorMessage="Invalid Passenger" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"></asp:CustomValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:PlaceHolder>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Panel ID="plTripItinerary" runat="server">
                                                            <%--<table>
                                                        <tr>
                                                            <td>
                                                                CREW-ADDL CREW
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtAddlcrew" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdLabel90">
                                                                CREW-PILOT 1
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtpilot1" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                CREW-PILOT 2
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtpilot2" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Message Body
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtmsg" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>--%>
                                                        </asp:Panel>
                                                        <asp:Panel ID="plTripsheetMain" runat="server">
                                                            <%--<table>
                                                        <tr>
                                                            <td>
                                                                AC Release
                                                            </td>
                                                            <td>
                                                                &nbsp;&nbsp;&nbsp;<asp:TextBox ID="tbAIRCRAFTRELEASED" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Message Body
                                                            </td>
                                                            <td>
                                                                &nbsp;&nbsp;&nbsp;<asp:TextBox ID="tbTripMain" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>--%>
                                                        </asp:Panel>
                                                        <asp:Panel ID="plcanpass" runat="server">
                                                            <%--<table>
                                                        <tr>
                                                            <td class="tdLabel90">
                                                                HAMILTON_ONTARIO
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtho" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                LANDSDOWNE_ONTARIO
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtlo" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                TRC_TELEPHONE
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtar" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                TELEPHONE_REPORT_CENTER
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txttrc" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                VICTORIA_BRITISH_COLUMBIA
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtvbc" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                WINDSOR_ONTARIO
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtwo" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>--%>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style3">
                                            <div class="tblspace_10">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td valign="top">
                                            <asp:Button runat="server" ID="btExport" CssClass="ui_nav" Text="Export" OnClick="btnExport_Click"
                                                ValidationGroup="ReportValidator" />
                                        </td>
                                        <td valign="top">
                                            <asp:Button ID="btnRptView" runat="server" CssClass="ui_nav" OnClick="btnRptView_Click"
                                                Text="Preview" ValidationGroup="ReportValidator" />
                                            <asp:Button ID="btnRptView1" runat="server" CssClass="ui_nav" OnClick="btnRptView1_Click"
                                                Style="display: none" Text="Preview" ValidationGroup="ReportValidator" />
                                        </td>
                                        <td valign="top">
                                            <asp:Button ID="btnReset" runat="server" CssClass="ui_nav" OnClick="btnReset_Click"
                                                CausesValidation="False" Text="Reset" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Panel runat="server" ID="ExportPanal">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="tdLabel100">
                                                            Export File Type
                                                        </td>
                                                        <td style="margin-left: 40px">
                                                            <asp:DropDownList runat="server" ID="ddlFormat">
                                                                <asp:ListItem Text="Excel" Value="Excel"></asp:ListItem>
                                                                <asp:ListItem Text="MHTML" Value="MHTML"></asp:ListItem>
                                                                <asp:ListItem Text="Word" Value="WORD"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:Button runat="server" CssClass="ui_nav" ID="btExportFileFormat" Text="Generate"
                                                                OnClick="btnExportFileFormat_Click" />
                                                        </td>
                                                        <td>
                                                            <asp:Button runat="server" CssClass="ui_nav" ID="btClose" Text="Close" OnClick="btnCostFileFormat_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel runat="server" ID="TripExcel" Width="100%">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel120">
                                                                        <asp:LinkButton ID="lbTripSheet" CssClass="link_small" runat="server" OnClick="lbTripSheet_Click">Download Tripsheet</asp:LinkButton>
                                                                    </td>
                                                                    <td class="tdLabel150">
                                                                        <asp:LinkButton ID="lbTripsheetAlerts" CssClass="link_small" runat="server" OnClick="lbTripsheetAlerts_Click">Download Tripsheet Alerts</asp:LinkButton>
                                                                    </td>
                                                                    <td class="tdLabel160">
                                                                        <asp:LinkButton ID="lbPassengerNotes" CssClass="link_small" runat="server" OnClick="lbPassengerNotes_Click">Download Passenger Notes</asp:LinkButton>
                                                                    </td>
                                                                    <td class="tdLabel160">
                                                                        <asp:LinkButton ID="lbPassengerAlerts" CssClass="link_small" runat="server" OnClick="lbPassengerAlerts_Click">Download Passenger Alerts</asp:LinkButton>
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="lbFuelVendor" CssClass="link_small" runat="server" OnClick="lbFuelVendor_Click">Download Fuel Vendor</asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:LinkButton ID="lbAircraftAdditionalInfomation" CssClass="link_small" runat="server"
                                                                            OnClick="lbAircraftAdditionalInfomation_Click">Download Aircraft Additional Infomation</asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel runat="server" ID="TripExcelGdl" ClientIDMode="Static" Width="100%">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel120">
                                                                        <asp:LinkButton ID="lbgdl1" CssClass="link_small" runat="server" OnClick="lbTripGdl1_Click">Download General Declaration</asp:LinkButton>
                                                                    </td>
                                                                    <td class="tdLabel150">
                                                                        <asp:LinkButton ID="lbgdl2" CssClass="link_small" runat="server" OnClick="lbTripGdl2_Click">Download General Declaration</asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblError" CssClass="alert-text"></asp:Label>
                                    <asp:Wizard ID="Wizard1" runat="server" DisplaySideBar="false" ActiveStepIndex="0"
                                        Height="209px" Width="100%" BackColor="#f0f0f0" FinishPreviousButtonStyle-CssClass="ui_nav"
                                        CancelButtonStyle-CssClass="ui_nav" FinishCompleteButtonStyle-CssClass="ui_nav"
                                        StepNextButtonStyle-CssClass="ui_nav" StepPreviousButtonStyle-CssClass="ui_nav"
                                        StartNextButtonStyle-CssClass="ui_nav" OnFinishButtonClick="OnFinish" OnNextButtonClick="OnNext"
                                        ClientIDMode="Static">
                                        <WizardSteps>
                                            <asp:WizardStep ID="WizardStep1" runat="server">
                                                <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel4">
                                                    <asp:Label runat="server" ID="lblw1" Text="Click on each of the Available fields by name and the right arrow to select them. Add them in the preferred display order or use the Up/Down arrows to reorder them for reporting. Click on the Selected fields and left arrow to de-select them."></asp:Label>
                                                    <br />
                                                    <br />
                                                    <UCReportHtmlSelection:Grid ID="UCRHS" ClientIDMode="Static" runat="server" />
                                                </telerik:RadAjaxPanel>                                                
                                            </asp:WizardStep>
                                            <asp:WizardStep ID="WizardStep2" runat="server">
                                                <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel3">
                                                    <asp:Label runat="server" ID="lblw2" Text="Select how data is to be sorted"></asp:Label>
                                                    <br />
                                                    <br />
                                                    <br />
                                                    <UCReportHtmlSelection:Grid ID="UCRHSort" runat="server" />
                                                </telerik:RadAjaxPanel>
                                            </asp:WizardStep>
                                            <asp:WizardStep ID="WizardStep4" runat="server">
                                                <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel2">
                                                    <asp:Label runat="server" ID="lblw3" Text="Edit the Header Text "></asp:Label>
                                                    <br />
                                                    <br />
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                Field
                                                            </td>
                                                            <td>
                                                                HTML Heading
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <telerik:RadListBox runat="server" ID="SelectedSource" Height="200px" Width="200px"
                                                                    SelectionMode="Single" />
                                                            </td>
                                                            <td>
                                                                <telerik:RadListBox runat="server" ID="HeaderBySource" AllowReorder="false" SelectionMode="Single"
                                                                    Height="200px" Width="200px">
                                                                </telerik:RadListBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </telerik:RadAjaxPanel>
                                            </asp:WizardStep>
                                            <asp:WizardStep ID="WizardStep3" runat="server">
                                                <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel1">
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="60%">
                                                                <fieldset>
                                                                    <legend>Page Layout</legend>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel130">
                                                                                            Use HTML Defaults
                                                                                        </td>
                                                                                        <td class="tdLabel80">
                                                                                            <asp:CheckBox ID="chkHeader" runat="server" Text="Header" />
                                                                                        </td>
                                                                                        <td class="tdLabel70">
                                                                                            <asp:CheckBox ID="chkFooter" runat="server" Text="Footer" />
                                                                                        </td>
                                                                                        <td class="tdLabel100">
                                                                                            <asp:CheckBox ID="chkBackground" runat="server" Text="Background" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel130">
                                                                                            Title
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="txtTitle" runat="server" Width="300px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel130">
                                                                                            Foreground Color
                                                                                        </td>
                                                                                        <td class="tdLabel60">
                                                                                            <telerik:RadColorPicker ID="ForegroundColor" runat="server" SelectedColor="" ShowIcon="True">
                                                                                                <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                                            </telerik:RadColorPicker>
                                                                                        </td>
                                                                                        <td class="tdLabel110">
                                                                                            Background Color
                                                                                        </td>
                                                                                        <td>
                                                                                            <telerik:RadColorPicker ID="BackgroundColor" runat="server" SelectedColor="" ShowIcon="True">
                                                                                                <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                                            </telerik:RadColorPicker>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                            <td width="40%">
                                                                <fieldset>
                                                                    <legend>Table Layout</legend>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel130">
                                                                                            Border Color
                                                                                        </td>
                                                                                        <td>
                                                                                            <telerik:RadColorPicker ID="TableBorderColor" runat="server" SelectedColor="" ShowIcon="True">
                                                                                                <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                                            </telerik:RadColorPicker>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel130">
                                                                                            Foreground Color
                                                                                        </td>
                                                                                        <td>
                                                                                            <telerik:RadColorPicker ID="TableForegroundColor" runat="server" SelectedColor=""
                                                                                                ShowIcon="True">
                                                                                                <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                                            </telerik:RadColorPicker>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel130">
                                                                                            Background Color
                                                                                        </td>
                                                                                        <td>
                                                                                            <telerik:RadColorPicker ID="TableBackgroundColor" runat="server" SelectedColor=""
                                                                                                ShowIcon="True">
                                                                                                <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                                            </telerik:RadColorPicker>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </telerik:RadAjaxPanel>
                                            </asp:WizardStep>
                                        </WizardSteps>
                                    </asp:Wizard>
                                </td>
                            </tr>
                        </tr>
                        <tr>
                            <td>
                                <div id="DivLinks" runat="server" class="ExternalForm">
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td>
                                                <div class="preview_list_items">
                                                    <span>
                                                        <asp:LinkButton ID="lnkOverview" runat="server" Text="Overview" Visible="false" OnClick="lnkOverview_Click"></asp:LinkButton></span>
                                                    <span>
                                                        <asp:LinkButton ID="lnkTripSheet" runat="server" Text="Tripsheet" Visible="false"
                                                            OnClick="lnkTripSheet_Click"></asp:LinkButton></span> <span>
                                                                <asp:LinkButton ID="lnkSummary" runat="server" Text="Tripsheet Summary" Visible="false" OnClick="lnkSummary_Click"></asp:LinkButton></span>
                                                    <span>
                                                        <asp:LinkButton ID="lnkIternary" runat="server" Text="Itinerary" Visible="false"
                                                            OnClick="lnkIternary_Click"></asp:LinkButton></span> <span>
                                                                <asp:LinkButton ID="lnkFlightLog" runat="server" Text="Flight Log" Visible="false"
                                                                    OnClick="lnkFlightLog_Click"></asp:LinkButton></span> <span>
                                                                        <asp:LinkButton ID="lnkPaxProfile" runat="server" Text="Passenger Profile" Visible="false"
                                                                            OnClick="lnkPaxProfile_Click"></asp:LinkButton></span> <span>
                                                                                <asp:LinkButton ID="lnkInternational" runat="server" Text="International Data" Visible="false"
                                                                                    OnClick="lnkInternational_Click"></asp:LinkButton></span>
                                                    <span>
                                                        <asp:LinkButton ID="lnkPaxTravelAuth" runat="server" Text="Passenger Travel Authorization"
                                                            Visible="false" OnClick="lnkPaxTravelAuth_Click"></asp:LinkButton></span>
                                                    <span>
                                                        <asp:LinkButton ID="lnkAlerts" runat="server" Text="Tripsheet Alerts" Visible="false" OnClick="lnkAlerts_Click"></asp:LinkButton></span>
                                                    <span>
                                                        <asp:LinkButton ID="lnkPAERForm" runat="server" Text="PAER Form" Visible="false"
                                                            OnClick="lnkPAERForm_Click"></asp:LinkButton></span> <span>
                                                                <asp:LinkButton ID="lnkGeneralDeclaration" runat="server" Text="General Declaration"
                                                                    Visible="false" OnClick="lnkGeneralDeclaration_Click"></asp:LinkButton></span>
                                                    <span>
                                                        <asp:LinkButton ID="lnkCrewDutyOverview" runat="server" Text="Crew Duty Overview"
                                                            Visible="false" OnClick="lnkCrewDutyOverview_Click"></asp:LinkButton></span>
                                                    <span>
                                                        <asp:LinkButton ID="lnkTripChecklist" runat="server" Text="Trip Checklist" Visible="false"
                                                            OnClick="lnkTripChecklist_Click"></asp:LinkButton></span> <span>
                                                                <asp:LinkButton ID="lnkCanPassForm" runat="server" Text="CANPASS Form" Visible="false"
                                                                    OnClick="lnkCanPassForm_Click"></asp:LinkButton></span> <span>
                                                                        <asp:LinkButton ID="lnkGDiscuss" runat="server" Text="General Declaration Appendix 1/2"
                                                                            ValidationGroup="ReportValidator" Visible="false" OnClick="lnkGDiscuss_Click"></asp:LinkButton></span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <asp:Button ID="Button1" runat="server" Visible="false" CssClass="button" OnClick="btnSubmit_Click"
                                                Text="Preview Report" />
                                            <asp:HiddenField ID="hdnReportName" runat="server" />
                                            <%--<asp:TextBox ID="tbclient" runat="server" CssClass="text60" ></asp:TextBox>&nbsp--%>
                                            <asp:TextBox ID="txtOutput" Visible="false" runat="server" TextMode="MultiLine" Width="300"
                                                Height="35"></asp:TextBox>
                                            <asp:TextBox ID="txtHtmlTable" runat="server" Visible="false" TextMode="MultiLine"
                                                Width="300" Height="35"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <div class="tblspace_5">
                                </div>
                            </td>
                        </tr>
                    </table>
                    <iframe style="display:none;" name="PDFViewer" id="PDFViewerFrame" width="980px" height="1000px"></iframe>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td align="right">
                                <table cellspacing="0" cellpadding="0">
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
