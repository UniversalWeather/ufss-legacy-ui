﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.PreflightService;

//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Xml;
using System.Drawing;
using System.Xml.Linq;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Reports
{
    public partial class AdHocPreFlightReports : BaseSecuredPage
    {
        string urlBase = "Config\\";
        private ExceptionManager exManager;
        public PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
        string DateFormat = "MM/dd/yyyy";
        public Int64 TripID = 0;


        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                            DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;

                        Session["TabSelect"] = "PreFlight";
                        SelectTab();
                        if (!IsPostBack)
                        {
                            lbMessage.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            lbcvClient.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                            // Enabling Client Control based on User Principal Identity
                            if (UserPrincipal.Identity._clientId != null && UserPrincipal.Identity._clientId > 0)
                            {
                                hdnClient.Value = UserPrincipal.Identity._clientId.ToString();
                                tbClientCode.Text = UserPrincipal.Identity._clientCd;

                                // Get Client Description from Master Service
                                try
                                {
                                    FlightPakMasterService.Client client = new FlightPakMasterService.Client();
                                    client = GetClient((long)UserPrincipal.Identity._clientId);
                                    if (client != null && client.ClientDescription != null)
                                        lbClientDesc.Text = System.Web.HttpUtility.HtmlEncode(client.ClientDescription);
                                }
                                catch (Exception ee1)
                                {
                                    //Manually Handled
                                }

                                tbClientCode.Enabled = false;
                                btnClientCode.Enabled = false;
                            }
                            else
                            {
                                tbClientCode.Text = string.Empty;
                                hdnClient.Value = string.Empty;
                                lbClientDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            }

                            
                            if (DateFormat != null)
                            {
                                if (Session[Session["TabSelect"] + "DATEFROMtbDate"] != null)
                                {
                                    ((TextBox)ucfromDate.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Session[Session["TabSelect"] + "DATEFROMtbDate"].ToString());
                                }
                                if (Session[Session["TabSelect"] + "DATETOtbDate"] != null)
                                {
                                    ((TextBox)uctoDate.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Session[Session["TabSelect"] + "DATETOtbDate"].ToString());
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void Search_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindData(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void Search_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                InsertSelectedRow();
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void Search_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        InsertSelectedRow();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        private void InsertSelectedRow()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PreflightService.PreflightServiceClient objService = new PreflightService.PreflightServiceClient())
                {
                    if (dgSearch.SelectedItems.Count > 0)
                    {
                        GridDataItem item = (GridDataItem)dgSearch.SelectedItems[0];
                        TripID = Convert.ToInt64(item.GetDataKeyValue("TripID").ToString());

                        if (Request.QueryString["IsPopup"] != null && Request.QueryString["IsPopup"].ToString() == "YES")
                        {
                            RadAjaxManager1.ResponseScripts.Add(@"returnToParent();");
                        }
                        else
                        {
                            Int64 convTripNum = 0;
                            Int64 TripNUM = 0;
                            if (Int64.TryParse(item["TripNUM"].Text, out convTripNum))
                                TripNUM = convTripNum;

                            if (Session["PREFLIGHTMAIN"] != null)
                            {
                                PreflightMain Trip = (PreflightMain)Session["PREFLIGHTMAIN"];
                            }
                            else
                            {
                                PreflightMain obj = new PreflightMain();
                                GridDataItem dataitem = (GridDataItem)dgSearch.SelectedItems[0];
                                obj.TripID = Convert.ToInt64(dataitem.GetDataKeyValue("TripID").ToString());
                                var result = objService.GetTrip(TripID);

                                if (result.ReturnFlag == true)
                                {
                                    Session["PREFLIGHTMAIN"] = (PreflightMain)result.EntityInfo;

                                    // Bind the Saved Exceptions into the Session
                                    if (result.EntityInfo.PreflightTripExceptions != null && result.EntityInfo.PreflightTripExceptions.Count > 0)
                                        Session["PREEXCEPTION"] = result.EntityInfo.PreflightTripExceptions;
                                }
                                RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                            }
                        }
                    }
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Ramesh: Added to fix defect# 2175
                        if (DateFormat != null)
                        {
                            if (((TextBox)ucfromDate.FindControl("tbDate")).Text != null)
                            {
                                //((TextBox)ucfromDate.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Session[Session["TabSelect"] + "DATEFROMtbDate"].ToString());
                                Session[Session["TabSelect"] + "DATEFROMtbDate"] = ((TextBox)ucfromDate.FindControl("tbDate")).Text.Trim();
                            }
                            if (((TextBox)uctoDate.FindControl("tbDate")).Text != null)
                            {
                                //((TextBox)ucfromDate.FindControl("tbToDate")).Text = String.Format("{0:" + DateFormat + "}", Session[Session["TabSelect"] + "DATETOtbDate"].ToString());
                                Session[Session["TabSelect"] + "DATETOtbDate"] = ((TextBox)uctoDate.FindControl("tbDate")).Text.Trim();
                            }
                        }
                        BindData(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        private void BindData(bool isBindRequired)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isBindRequired))
            {
                Int64 fromTrip = 0;
                Int64 toTrip = 0;
                Int64 clientID = 0;
                Int64 homeBaseID = 0;
                Int64 fleetID = 0;
                bool isPersonal = false;
                bool isCompleted = false;
                DateTime fromDate = DateTime.MinValue;
                DateTime toDate = DateTime.MinValue;

                if (UserPrincipal.Identity._homeBaseId != null && chkHomebase.Checked == true)
                    homeBaseID = (long)UserPrincipal.Identity._homeBaseId;

                if (!string.IsNullOrEmpty(hdnClient.Value) && hdnClient.Value.Trim() != "undefined")
                    clientID = Convert.ToInt64(hdnClient.Value);

                if (!string.IsNullOrEmpty(hdnTailNo.Value) && hdnTailNo.Value.Trim() != "undefined")
                    fleetID = Convert.ToInt64(hdnTailNo.Value);

                if (ChkPerTrav.Checked == true)
                    isPersonal = true;

                if (chkCompleted.Checked == true)
                    isCompleted = true;

                try
                {
                    TextBox tbFromDate = (TextBox)ucfromDate.FindControl("tbDate");
                    if (!string.IsNullOrEmpty(tbFromDate.Text))
                        fromDate = Convert.ToDateTime(tbFromDate.Text + " 00:00");
                }
                catch (Exception ex)
                {
                    //Manually Handled
                    fromDate = DateTime.Now;
                }

                try
                {
                    TextBox tbToDate = (TextBox)uctoDate.FindControl("tbDate");
                    if (!string.IsNullOrEmpty(tbToDate.Text))
                        toDate = Convert.ToDateTime(tbToDate.Text + " 00:00");
                }
                catch (Exception ex)
                {
                    //Manually Handled
                    toDate = DateTime.Now;
                }

                if (tbFromTrip.Text != "")
                {
                    fromTrip = Convert.ToInt64(tbFromTrip.Text);
                }

                if (tbToTrip.Text != "")
                {
                    toTrip = Convert.ToInt64(tbToTrip.Text);
                }

                using (PreflightService.PreflightServiceClient objService = new PreflightService.PreflightServiceClient())
                {
                    var objretval = objService.GetPreReportList(homeBaseID, clientID, fleetID, isPersonal, isCompleted, fromDate, toDate, fromTrip, toTrip);
                    if (objretval != null && objretval.ReturnFlag)
                    {
                        dgSearch.DataSource = objretval.EntityList;
                        //Session["pretrips"] = objretval.EntityList;
                        if (isBindRequired)
                            dgSearch.Rebind();//.databind();
                    }
                    else
                    {
                        dgSearch.DataSource = string.Empty;
                        dgSearch.DataBind();
                    }
                }

            }
        }

        /// <summary>
        /// Method to validate ClientCodes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Client_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbcvClient.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbClientDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnClient.Value = string.Empty;
                        if (!string.IsNullOrEmpty(tbClientCode.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient ClientService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var ClientVal = ClientService.GetClientCodeList().EntityList.Where(x => x.ClientCD.ToString().ToUpper().Trim().Equals(tbClientCode.Text.ToUpper().Trim())).ToList();
                                if (ClientVal != null && ClientVal.Count > 0)
                                {
                                    tbClientCode.Text = ClientVal[0].ClientCD;
                                    if (((FlightPakMasterService.Client)ClientVal[0]).ClientDescription != null)
                                        lbClientDesc.Text = System.Web.HttpUtility.HtmlEncode(((FlightPakMasterService.Client)ClientVal[0]).ClientDescription.ToUpper());
                                    hdnClient.Value = ((FlightPakMasterService.Client)ClientVal[0]).ClientID.ToString();
                                }
                                else
                                {
                                    lbcvClient.Text = System.Web.HttpUtility.HtmlEncode("Invalid Client Code.");
                                    tbClientCode.Focus();
                                }
                            }
                        }
                        BindData(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        /// <summary>
        /// Method to validate TailNumber
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TailNo_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbcvTailNumber.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnTailNo.Value = string.Empty;
                        if (!string.IsNullOrEmpty(tbTailNumber.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient TailService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var TailVal = TailService.GetFleetProfileList().EntityList.Where(x => x.TailNum.ToString().ToUpper().Trim().Equals(tbTailNumber.Text.ToUpper().Trim())).ToList();
                                if (TailVal != null && TailVal.Count > 0)
                                {
                                    tbTailNumber.Text = TailVal[0].TailNum;
                                    hdnTailNo.Value = ((FlightPakMasterService.Fleet)TailVal[0]).FleetID.ToString();
                                }
                                else
                                {
                                    lbcvTailNumber.Text = System.Web.HttpUtility.HtmlEncode("Invalid Aircraft Tail No.");
                                    tbTailNumber.Focus();
                                }
                            }
                        }
                        BindData(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }

        }

        public string FormatDate(string dt)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(dt))
            {
                string FormattedDate = string.Empty;
                if (!string.IsNullOrEmpty(dt))
                {
                    DateTimeFormatInfo formatInfo = new DateTimeFormatInfo();
                    if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        formatInfo.ShortDatePattern = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                    else
                        formatInfo.ShortDatePattern = "MM/dd/yyyy"; // American, MDY

                    List<string> dtList = new List<string>();

                    char splitter;
                    if (dt.Contains("-"))
                        splitter = '-';
                    else if (dt.Contains("."))
                        splitter = '.';
                    else
                        splitter = '/';

                    switch (formatInfo.ShortDatePattern)
                    {
                        case "dd/MM/yyyy": // British, French, DMY
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[0] + "/" + dtList[2];
                            break;
                        case "dd.MM.yyyy": // German
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[0] + "/" + dtList[2];
                            break;
                        case "yyyy.MM.dd": // ANSI
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[2] + "/" + dtList[0];
                            break;
                        case "dd-MM-yyyy": // Italian
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[0] + "/" + dtList[2];
                            break;
                        case "yyyy/MM/dd": // Japan, Taiwan, YMD
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[2] + "/" + dtList[0];
                            break;
                        case "MM/dd/yyyy": // Default
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[0] + "/" + dtList[1] + "/" + dtList[2];
                            break;
                    }
                }
                return FormattedDate;
            }
        }

        public DateTime FormatDate(string Date, string Format)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Date, Format))
            {
                string DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;

                string[] DateAndTime = Date.Split(' ');

                string[] SplitDate = new string[3];
                string[] SplitFormat = new string[3];

                if (DateFormat.Contains("/"))
                {
                    SplitDate = DateAndTime[0].Split('/');
                    SplitFormat = Format.Split('/');
                }
                else if (DateFormat.Contains("-"))
                {
                    SplitDate = DateAndTime[0].Split('-');
                    SplitFormat = Format.Split('-');
                }
                else if (DateFormat.Contains("."))
                {
                    SplitDate = DateAndTime[0].Split('.');
                    SplitFormat = Format.Split('.');
                }

                int dd = 0, mm = 0, yyyy = 0;
                for (int Index = 0; Index < SplitFormat.Count(); Index++)
                {
                    if (SplitFormat[Index].ToLower().Contains("d"))
                    {
                        dd = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("m"))
                    {
                        mm = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("y"))
                    {
                        yyyy = Convert.ToInt16(SplitDate[Index]);
                    }
                }
                return new DateTime(yyyy, mm, dd);
            }
        }

        /// <summary>
        /// Method to Set Eception, Is Persoal symbols on databound 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Search_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (e.Item is GridDataItem)
                {
                    try
                    {
                        GridDataItem item = (GridDataItem)e.Item;

                        if (item["EstDepartureDT"] != null && item["EstDepartureDT"].Text != "&nbsp;")
                        {
                            DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "EstDepartureDT");
                            if (date != null)
                                item["EstDepartureDT"].Text = Microsoft.Security.Application.Encoder.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", date));
                        }

                        if (item["IsPersonal"] != null && item["IsPersonal"].Text.ToUpper() == "TRUE")
                        {
                            item["IsPersonal"].Text = "*";
                            item["IsPersonal"].ForeColor = System.Drawing.Color.Red;
                            item["IsPersonal"].HorizontalAlign = HorizontalAlign.Center;
                        }
                        else if (item["IsPersonal"].Text.ToUpper() == "FALSE")
                        {
                            item["IsPersonal"].Text = string.Empty;
                        }

                        if (item["IsCompleted"] != null && item["IsCompleted"].Text.ToUpper() == "TRUE")
                        {
                            item["IsCompleted"].Text = "Y";
                            item["IsCompleted"].HorizontalAlign = HorizontalAlign.Center;
                        }
                        else if (item["IsCompleted"].Text.ToUpper() == "FALSE")
                        {
                            item["IsCompleted"].Text = string.Empty;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Manually Handled
                    }
                }
            }
        }

        /// <summary>
        /// Get Client Details based on Client ID
        /// </summary>
        /// <param name="ClientID">Pass Client ID</param>
        /// <returns>Returns Client Object</returns>
        public FlightPak.Web.FlightPakMasterService.Client GetClient(Int64 ClientID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPak.Web.FlightPakMasterService.Client client = new FlightPakMasterService.Client();
                    var objRetVal = objDstsvc.GetClientCodeList();
                    List<FlightPakMasterService.Client> ClientList = new List<FlightPakMasterService.Client>();

                    if (objRetVal.ReturnFlag)
                    {
                        ClientList = objRetVal.EntityList.Where(x => x.ClientID == ClientID).ToList();
                        if (ClientList != null && ClientList.Count > 0)
                        {
                            client = ClientList[0];
                        }
                        else
                            client = null;
                    }
                    return client;
                }
            }

        }

        protected void Yes_Click(object sender, EventArgs e)
        {
            GridDataItem item = (GridDataItem)dgSearch.SelectedItems[0];
            TripID = Convert.ToInt64(item.GetDataKeyValue("TripID").ToString());

            Int64 convTripNum = 0;
            Int64 TripNUM = 0;
            if (Int64.TryParse(item["TripNUM"].Text, out convTripNum))
                TripNUM = convTripNum;
            Int64 clientID = 0;
            Int64 homeBaseID = 0;
            Int64 fleetID = 0;
            bool isPersonal = false;
            bool isCompleted = false;
            DateTime fromDate = DateTime.MinValue;
            DateTime toDate = DateTime.MinValue;
            Int64 fromTrip = 0;
            Int64 toTrip = 0;

            if (UserPrincipal.Identity._homeBaseId != null && chkHomebase.Checked == true)
                homeBaseID = (long)UserPrincipal.Identity._homeBaseId;

            if (!string.IsNullOrEmpty(hdnClient.Value) && hdnClient.Value.Trim() != "undefined")
                clientID = Convert.ToInt64(hdnClient.Value);

            if (!string.IsNullOrEmpty(hdnTailNo.Value) && hdnTailNo.Value.Trim() != "undefined")
                fleetID = Convert.ToInt64(hdnTailNo.Value);

            if (ChkPerTrav.Checked == true)
                isPersonal = true;

            if (chkCompleted.Checked == true)
                isCompleted = true;

            try
            {
                TextBox tbFromDate = (TextBox)ucfromDate.FindControl("tbDate");
                if (!string.IsNullOrEmpty(tbFromDate.Text))
                    fromDate = Convert.ToDateTime(tbFromDate.Text + " 00:00");
            }
            catch (Exception ex)
            {
                //Manually Handled
                fromDate = DateTime.Now;
            }

            try
            {
                TextBox tbToDate = (TextBox)uctoDate.FindControl("tbDate");
                if (!string.IsNullOrEmpty(tbToDate.Text))
                    toDate = Convert.ToDateTime(tbToDate.Text + " 00:00");
            }
            catch (Exception ex)
            {
                //Manually Handled
                toDate = DateTime.Now;
            }

            using (PreflightServiceClient objService = new PreflightServiceClient())
            {
                var objRetVal = objService.GetPreReportList(homeBaseID, clientID, fleetID, isPersonal, isCompleted, fromDate, toDate, fromTrip, toTrip);

                
                if (objRetVal.ReturnFlag)
                {
                    PreflightMain obj = new PreflightMain();
                    GridDataItem dataitem = (GridDataItem)dgSearch.SelectedItems[0];
                    obj.TripID = Convert.ToInt64(dataitem.GetDataKeyValue("TripID").ToString());
                    var result = objService.GetTrip(obj.TripID);

                    if (result.ReturnFlag == true)
                    {
                        Session["PREFLIGHTMAIN"] = (PreflightMain)result.EntityInfo;

                        // Bind the Saved Exceptions into the Session
                        if (result.EntityInfo.PreflightTripExceptions != null && result.EntityInfo.PreflightTripExceptions.Count > 0)
                            Session["PREEXCEPTION"] = result.EntityInfo.PreflightTripExceptions;
                    }
                    RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                }
            }
        }

        protected void No_Click(object sender, EventArgs e)
        {
            RadAjaxManager1.ResponseScripts.Add(@"CloseRadWindow('CloseWindow');");
        }

        protected void FromTrip_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnFromTripID.Value = string.Empty;
                        if (tbFromTrip.Text.Trim() != string.Empty)
                        {
                            using (PreflightService.PreflightServiceClient objService = new PreflightService.PreflightServiceClient())
                            {
                                PreflightMain Pretrips = new PreflightMain();
                                Pretrips.TripNUM = Convert.ToInt64(tbFromTrip.Text);

                                //var objRetVal = objService.GetTrip(Pretrips).EntityInfo;
                                //if (objRetVal != null)
                                //{
                                //    hdnFromTripID.Value = objRetVal.TripID.ToString();
                                //}
                                //else
                                //{
                                //    RadAjaxManager1.FocusControl(tbFromTrip.ClientID);
                                //}
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void ToTrip_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //lbcvToLog.Text = string.Empty;
                        hdnToTripID.Value = string.Empty;
                        if (tbToTrip.Text.Trim() != string.Empty)
                        {
                            using (PreflightService.PreflightServiceClient objService = new PreflightService.PreflightServiceClient())
                            {
                                PreflightMain Pretrips = new PreflightMain();
                                Pretrips.TripNUM = Convert.ToInt64(tbToTrip.Text);

                                //var objRetVal = objService.GetTrip(Pretrips).EntityInfo;
                                //if (objRetVal != null)
                                //{
                                //    hdnToTripID.Value = objRetVal.TripID.ToString();
                                //}
                                //else
                                //{
                                //    RadAjaxManager1.FocusControl(tbToTrip.ClientID);
                                //}
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void Search_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridHeaderItem)
            {
                GridHeaderItem item = (GridHeaderItem)e.Item;
                Label lbl = new Label();
                lbl.ID = "Label1";
                lbl.Text = "Select All";
                item["ClientSelectTrip"].Controls.Add(lbl);
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool IsTripID = false;
                        if (dgSearch.Items.Count > 0)
                        {
                            if (dgSearch.SelectedItems.Count > 0)
                            {
                                string TripIDs = string.Empty;
                                foreach (GridDataItem item in dgSearch.SelectedItems)
                                {
                                    TripIDs += item.GetDataKeyValue("TripID").ToString() + ',';
                                }
                                TripIDs = TripIDs.Substring(0, TripIDs.Length - 1);

                                if (!string.IsNullOrEmpty(TripIDs))
                                {
                                    IsTripID = true;
                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("Please select Preflight Trip.", 250, 150, "AdHoc Report", "");
                                }
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Please select Preflight Trip.", 250, 150, "AdHoc Report", "");
                            }
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Please select Preflight Trip.", 250, 150, "AdHoc Report", "");
                        }

                        if (IsTripID)
                        {
                            Session["TabSelect"] = "PreFlight";

                            if (radbtnlst.SelectedValue != null)
                                Session["FORMAT"] = radbtnlst.SelectedValue;
                            else
                                Session["FORMAT"] = "CSV";

                            Session["PARAMETER"] = ParamTripNumber();
                            Session["REPORT"] = "RptPREAdhocInformation";
                            Response.Redirect("ReportRenderView.aspx", true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        private string ParamTripNumber()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string paramater = string.Empty;

                paramater += "UserCD=" + UserPrincipal.Identity._name.Trim();
                paramater += ";UserCustomerID=" + UserPrincipal.Identity._customerID;
                paramater += ";UserHomebaseID=" + UserPrincipal.Identity._homeBaseId;

                if (chkLegs.Checked)
                    paramater += ";IsLeg=" + "true";
                else
                    paramater += ";IsLeg=" + "false";

                if (chkPax.Checked)
                    paramater += ";IsPax=" + "true";
                else
                    paramater += ";IsPax=" + "false";

                if (chkCrew.Checked)
                    paramater += ";IsCrew=" + "true";
                else
                    paramater += ";IsCrew=" + "false";

                string TripIDs = string.Empty;

                
                if (dgSearch.SelectedItems.Count > 0)
                {
                    foreach (GridDataItem item in dgSearch.SelectedItems)
                    {
                        TripIDs += item.GetDataKeyValue("TripID").ToString() + ',';
                    }
                    TripIDs = TripIDs.Substring(0, TripIDs.Length - 1);
                }

                if (!string.IsNullOrEmpty(TripIDs))
                    paramater += ";TripIDs=" + TripIDs;

                return paramater;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        chkLegs.Checked = false;
                        chkPax.Checked = false;
                        chkCrew.Checked = false;

                        chkPax.Enabled = false;
                        chkCrew.Enabled = false;

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnPre_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "PreFlight";
                        pnlReportTab.Items.Clear();
                        //LoadTabs(pnlReportTab, urlBase + "PreFlightMainTab.xml");
                        Response.Redirect("ReportViewer.aspx?xmlFilename=PREAircraftCalendarI.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnPost_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "PostFlight";
                        pnlReportTab.Items.Clear();
                        // LoadTabs(pnlReportTab, urlBase + "PostFlightMainTab.xml");
                        Response.Redirect("ReportViewer.aspx?xmlFilename=POSTAircraftArrivals.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnCharter_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "Charter";
                        pnlReportTab.Items.Clear();
                        // LoadTabs(pnlReportTab, urlBase + "CharterMainTab.xml");
                        //Response.Redirect("CQReportViewer.aspx?xmlFilename=CQAircraftProfile.xml");
                        Response.Redirect("ReportViewer.aspx?xmlFilename=CQAircraftProfile.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnCorpReq_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "CorpReq";
                        pnlReportTab.Items.Clear();
                        //LoadTabs(pnlReportTab, urlBase + "CorporateFlightMainTab.xml");
                        Response.Redirect("ReportViewer.aspx?xmlFilename=CRTripItinerary.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }

        protected void btnDb_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "Db";
                        pnlReportTab.Items.Clear();
                        LoadTabs(pnlReportTab, urlBase + "DatabaseMainTab.xml");
                        Response.Redirect("ReportViewer.aspx?xmlFilename=DBAccounts.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void LoadTabs(RadPanelBar Rpb, string XmlFilePath)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Rpb, XmlFilePath))
            {
                string attrVal = "";
                XmlDocument doc = new XmlDocument();
                doc.Load(Server.MapPath(XmlFilePath));

                XmlNodeList elemList = doc.GetElementsByTagName("catagory");
                for (int i = 0; i < elemList.Count; i++)
                {
                    attrVal = attrVal + elemList[i].Attributes["text"].Value + ",";
                }
                string[] strval = attrVal.Split(',');
                foreach (string str in strval)
                {
                    Telerik.Web.UI.RadPanelItem rpi = new Telerik.Web.UI.RadPanelItem(str);
                    XmlNodeList xnList = doc.SelectNodes("/catagorys/catagory[@text='" + str + "']/screen[@name!='']");
                    if (xnList.Count > 0)
                    {
                        int ival = 0;
                        foreach (XmlNode child in xnList)
                        {
                            //Added UMPermissionRole for implementing security 
                            if (IsAuthorized("View" + child.Attributes["UMPermissionRole"].Value))
                            {

                                rpi.Items.Add(new Telerik.Web.UI.RadPanelItem(child.Attributes["name"].Value, child.Attributes["url"].Value));
                                rpi.Font.Bold = true;
                                rpi.Items[ival].Enabled = (child.Attributes["url"].Value == string.Empty ? false : true);
                                if (!rpi.Enabled)
                                {
                                    rpi.Items[ival].ForeColor = Color.Gray;
                                }
                                ival++;
                            }
                            else if (child.Attributes["UMPermissionRole"].Value == "TripsheetReportWriter")
                            {
                                rpi.Items.Add(new Telerik.Web.UI.RadPanelItem(child.Attributes["name"].Value, child.Attributes["url"].Value));
                                rpi.Font.Bold = true;
                                rpi.Items[ival].Enabled = (child.Attributes["url"].Value == string.Empty ? false : true);
                                ival++;
                            }
                        }
                        Rpb.Items.Add(rpi);
                    }
                    else if (str != "")
                    {
                        XmlNodeList elemListval = doc.GetElementsByTagName("catagory");
                        for (int ival = 0; ival < elemListval.Count; ival++)
                        {
                            Telerik.Web.UI.RadPanelItem rpia = new Telerik.Web.UI.RadPanelItem(elemListval[ival].Attributes["text"].Value, elemListval[ival].Attributes["url"].Value);
                            Rpb.Items.Add(rpia);
                        }
                        break;
                    }
                }
            }
        }

        private void SelectTab()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string TabSelect = Convert.ToString(Session["TabSelect"]);
                pnlReportTab.Items.Clear();
                string xmltab = string.Empty;
                switch (TabSelect)
                {
                    case "PreFlight":
                        xmltab = "PreFlightMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav_active";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav";
                        break;
                    case "PostFlight":
                        xmltab = "PostFlightMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav_active";
                        btnDatabase.CssClass = "rpt_nav";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav";
                        break;
                    case "Db":
                        xmltab = "DatabaseMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav_active";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav";
                        break;
                    case "CorpReq":
                        xmltab = "CorporateFlightMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav";
                        btnCorp.CssClass = "rpt_nav_active";
                        btnCharter.CssClass = "rpt_nav";
                        break;
                    case "Charter":
                        xmltab = "CharterMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav_active";
                        break;
                    default:
                        Session["TabSelect"] = "Db";
                        xmltab = "DatabaseMainTab.xml";
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav_active";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav";
                        Response.Redirect("ReportViewer.aspx?xmlFilename=DBAccounts.xml");
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        break;
                }
                ExpandPanel(pnlReportTab, xmltab, true);
            }
        }

        private void ExpandPanel(RadPanelBar panelBar, string TabXmlname, bool isExpand)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(panelBar, TabXmlname, isExpand))
            {
                string Selectedvalue = Convert.ToString(Request.QueryString["xmlFilename"]);
                //string attrVal = "";
                string TitleTab = string.Empty;
                XDocument doc = XDocument.Load(Server.MapPath(urlBase + TabXmlname));

                if (Request.QueryString["xmlFilename"] != null)
                {

                    var items = doc.Descendants("screen")
                                  .Where(c => c.Attribute("url").Value.Contains(Convert.ToString(Request.QueryString["xmlFilename"])))
                                   .Select(c => new { x = c.Value, y = c.Parent.Attribute("text").Value, z = c.Attribute("name").Value })
                                   .ToList();

                    if (items != null)
                    {
                        if (items.Count != 0)
                        {
                            RadPanelItem selectedItem = panelBar.FindItemByText(items[0].y);

                            if (selectedItem != null)
                            {
                                if (selectedItem.Items.Count > 0)
                                {
                                    RadPanelItem selectedvalue = selectedItem.Items.FindItemByText(items[0].z);
                                    selectedvalue.Selected = true;
                                    selectedItem.Expanded = true;
                                }
                                else
                                {
                                    selectedItem.Selected = true;
                                    while ((selectedItem != null) &&
                                           (selectedItem.Parent.GetType() == typeof(RadPanelItem)))
                                    {
                                        selectedItem = (RadPanelItem)selectedItem.Parent;
                                        selectedItem.Expanded = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void dgSearch_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgSearch, Page.Session);
        }
    }
}