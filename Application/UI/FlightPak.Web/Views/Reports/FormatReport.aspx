﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormatReport.aspx.cs" Inherits="FlightPak.Web.Views.Reports.FormatReport"
    ValidateRequest="false" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Edit Overview Parameters</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <link href="../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="../../Scripts/custom-grid-scroll.min.js"></script>
    <link href="/Scripts/CustomScrollBar/jquery.mCustomScrollbar.css" rel="stylesheet" />
    
    <style type="text/css">
        .mCSB_scrollTools {
            width: 28px !important;
            right: 0px !important;
                
        }

        .mCSB_dragger_bar {
            width: 22px !important;
        }


    </style>
    
</head>
<body>
    <form id="form1" runat="server">
    <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/Common.js") %>"></script>
    <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
    <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jquery.alerts.js") %>"></script>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            $(document).ready(function () {
              
                $("#dgTripSheetDetailPanel input").each(function (index, value) {
                    if (!(index == 1 || $(this).attr('disabled') == 'disabled')) {
                        $(this).change(function () {
                            $('#messagebox').fadeIn();
                            $('#messagebox').html('Tripsheet parameters have been changed.');
                        });
                    }
                });
            });

            function fadeoutdivsaving() {
                $('#messagebox').html("Saving Tripsheet parameters.");
                $('#messagebox').fadeIn();
            }

            function fadeoutdivsaved() {
                $('#messagebox').html("Tripsheet parameters saved.");
                $('#messagebox').fadeIn();
            }
            function chkSelectAll_onchange() {
                var IsSelected;
                if (document.getElementById("<%=chkSelectAll.ClientID %>").checked == true) {
                    IsSelected = "Y";
                }
                else {
                    IsSelected = "N";
                }
                var grid = $find("<%= dgTripSheetDetail.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var length = MasterTable.get_dataItems().length;
                for (var i = 0; i < length; i++) {
                    MasterTable.get_dataItems()[i].findElement("tbIsSelected").value = IsSelected;
                }
                return false;
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            var oArg = new Object();
            function Close(param) {
                oArg = new Object();
                oArg.IsPrint = param;
                if (oArg) {
                    GetRadWindow().Close(oArg);
                }
            }
            function fnAllowYN(myfield, e) {
                var key;
                var keychar;

                if (window.event)
                    key = window.event.keyCode;
                else if (e)
                    key = e.which;
                else
                    return true;
                keychar = String.fromCharCode(key);

                // control keys
                if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
                    return true;
                    // numbers
                else if (("YNyn").indexOf(keychar) > -1)
                    return true;
                else
                    return false;
            }
            function tbIsSelected_onblur(ctrl) {
                if (ctrl.value == "") {
                    ctrl.value = "N";
                }
                else if (ctrl.value == "n") {
                    ctrl.value = "N";
                }
                else if (ctrl.value == "y") {
                    ctrl.value = "Y";
                }
                return false;
            }
            function tbMessage_onblur(ctrl) {
                if (ctrl.value == "") {
                    ctrl.value = "";
                }
                return false;
            }
            function tbNumber_onblur(ctrl) {
                if (ctrl.value == "") {
                    ctrl.value = "";
                }
                return false;
            }

            function CheckRange(ctrl, UpperBound, LowerBound, WarningMessage) {
                var ReturnValue = false;
                var ctrlid = document.getElementById(ctrl);
                var CheckRangeFnCallBack = Function.createDelegate(ctrlid, function (shouldSubmit) {
                    ctrlid.value = LowerBound;
                    ctrlid.focus();
                    return ReturnValue;
                });
                if (ctrlid.value != "") {
                    if ((parseInt(ctrlid.value) >= LowerBound) && (parseInt(ctrlid.value) <= UpperBound)) {
                    }
                    else {
                        WarningMessage = WarningMessage.replace("$d$", ctrlid.value);
                        radalert(WarningMessage, 360, 50, 'Tripsheet Report Writer', CheckRangeFnCallBack);
                    }
                } else {
                    WarningMessage = WarningMessage.replace("$d$", "empty");
                    radalert(WarningMessage, 360, 50, 'Tripsheet Report Writer', CheckRangeFnCallBack);
                }
                return ReturnValue;
            }

            function tbMessage_onDblclick(txtctrl) {
                var ctrlMessage, ctrlTripSheetReportDetailID, ctrlMessageLength, ctrlWarningMessage;
                if (txtctrl.id.indexOf("tbMessage") != -1) {
                    ctrlMessage = document.getElementById(txtctrl.id.replace("tbMessage", "hdnMessage"));
                    ctrlTripSheetReportDetailID = document.getElementById(txtctrl.id.replace("tbMessage", "hdnGrdTripSheetReportDetailID"));
                    ctrlMessageLength = document.getElementById(txtctrl.id.replace("tbMessage", "hdnMessageLength"));
                    ctrlWarningMessage = document.getElementById(txtctrl.id.replace("tbMessage", "hdnWarningMessage"));
                }
                OpenMessagePopup(ctrlMessage, ctrlTripSheetReportDetailID, ctrlMessageLength, ctrlWarningMessage);
            }

            function OpenMessagePopup(ctrlMessage, ctrlTripSheetReportDetailID, ctrlMessageLength, ctrlWarningMessage) {
                var oWnd1 = $find("<%=RadwinMessagePopup.ClientID%>");
                document.getElementById('<%=tbFRMessage.ClientID%>').value = ctrlMessage.value;
                document.getElementById('<%=hdnFRMessageID.ClientID%>').value = ctrlTripSheetReportDetailID.value;
                document.getElementById('<%=hdnFRMessageLength.ClientID%>').value = ctrlMessageLength.value;
                document.getElementById('<%=lbFRWarningMessage.ClientID%>').innerText = ctrlWarningMessage.value;
                oWnd1.show();
            }

            function CloseMessagePopup() {
                return false;
            }

            function tbFRMessage_onkeypress(ctrl, evnt) {
                var SplitSize = document.getElementById('<%=hdnFRMessageLength.ClientID%>').value.split("-");
                var MaxLength;
                if ((SplitSize.length != 0) && (SplitSize.length == 2))
                    MaxLength = SplitSize[1];
                if (MaxLength.toUpperCase() != "N")
                    document.getElementById('<%=tbFRMessage.ClientID%>').value = document.getElementById('<%=tbFRMessage.ClientID%>').value.substring(0, MaxLength - 1);
                return false;
            }

            function confirmCallBackFormatReport(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnCancelYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnCancelNo.ClientID%>').click();
                }
            }

        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <%--<telerik:AjaxSetting AjaxControlID="DivExternalForm">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>--%>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="RadwinMessagePopup" runat="server" VisibleOnPageLoad="false"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                    Behaviors="close" Title="Edit Message" OnClientClose="CloseMessagePopup">
                    <ContentTemplate>
                        <div>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="tbFRMessage" runat="server" TextMode="MultiLine" Rows="15" Columns="100"
                                            MaxLength="1500" onkeypress="javascript:tbFRMessage_onkeypress(this, event);"></asp:TextBox>
                                        <asp:Label ID="lbFRWarningMessage" runat="server" CssClass="alert-text" />
                                        <asp:HiddenField ID="hdnFRMessageLength" runat="server" />
                                        <asp:HiddenField ID="hdnFRMessageID" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="custom_radbutton" align="right">
                                        <telerik:RadButton ID="btnFRSaveMessage" runat="server" Text="Save" OnClick="btnFRSaveMessage_OnClick" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ContentTemplate>
                </telerik:RadWindow>
            </Windows>
            <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        </telerik:RadWindowManager>
        <div id="DivExternalForm" runat="server" class="ExternalForm">
            <asp:Panel ID="pnlExternalPanel" runat="server" Visible="true">
                <table width="100%" class="box1 tripsheetbox2" cellpadding="0px" cellspacing="0px">
                    <tr>
                        <td   style="height: 26px;line-height:26px">
                            <asp:CheckBox ID="chkSelectAll" runat="server" Text="Select All" AutoPostBack="true"
                                OnCheckedChanged="chkSelectAll_OnCheckedChanged" />
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <div runat="server" clientidmode="Static" id="messagebox" class="success_msg" style="width:208px;height:12px;text-align:center" ></div>
                            <telerik:RadGrid ID="dgTripSheetDetail" runat="server" EnableAJAX="True" AllowMultiRowSelection="false"
                                OnNeedDataSource="dgTripSheetDetail_BindData" OnItemDataBound="dgTripSheetDetail_ItemDataBound"
                                AllowFilteringByColumn="true" Height="341px" Width="600px">
                                <MasterTableView AutoGenerateColumns="False" AllowFilteringByColumn="true" EditMode="InPlace" CssClass="ipad-scroll-fix content"
                                    TableLayout="Fixed" DataKeyNames="TripSheetReportDetailID,CustomerID,TripSheetReportHeaderID,ReportNumber,ReportDescription,IsSelected,Copies,ReportProcedure,
                                            ParameterVariable,ParameterDescription,ParameterType,ParameterWidth,ParameterCValue,ParameterLValue,ParameterNValue,ParameterValue,
                                            ReportOrder,TripSheetOrder,LastUpdUID,LastUpdTS,IsDeleted,ParameterSize,WarningMessage"
                                    CommandItemDisplay="None" ShowFooter="false" AllowPaging="false" NoMasterRecordsText="There are no parameters available for customization on this report.">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="TripSheetReportDetailID" HeaderText="TripSheetReportDetailID"
                                            UniqueName="TripSheetReportDetailID" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                            AutoPostBackOnFilter="true" Display="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="Parameter Description" UniqueName="ParameterDescription"
                                            DataField="ParameterDescription" CurrentFilterFunction="StartsWith" ShowFilterIcon="false"
                                            AutoPostBackOnFilter="true" AllowFiltering="true" FilterControlWidth="330px"
                                            HeaderStyle-Width="350px">
                                            <ItemTemplate>
                                                <asp:Label ID="ParameterDescription" runat="server" Text='<%# Eval("ParameterDescription") %>'></asp:Label>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Y/N" UniqueName="IsSelected" CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false" AllowFiltering="false" HeaderStyle-Width="50px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tbIsSelected" runat="server" Text='<%# Eval("ParameterLValue") %>'
                                                    CssClass="tdLabel30" MaxLength="1" onKeyPress="return fnAllowYN(this, event)"
                                                    onblur="javascript:return tbIsSelected_onblur(this);"></asp:TextBox>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Number" UniqueName="Number" CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false" AllowFiltering="false" HeaderStyle-Width="75px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tbNumber" runat="server" Text='<%# Eval("ParameterNValue") %>' MaxLength="4"
                                                    CssClass="tdLabel50" onKeyPress="return fnAllowNumeric(this, event)" onblur="javascript:return tbNumber_onblur(this);"></asp:TextBox>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Message" UniqueName="Message" CurrentFilterFunction="Contains"
                                            ShowFilterIcon="false" AllowFiltering="false" HeaderStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tbMessage" runat="server" Text='<%# Eval("ParameterValue") %>' MaxLength="7"
                                                    CssClass="tdLabel75" onblur="javascript:return tbMessage_onblur(this);" onDblclick="javascript:tbMessage_onDblclick(this);"></asp:TextBox>
                                                <asp:HiddenField ID="hdnMessage" runat="server" Value='<%# Eval("ParameterValue") %>' />
                                                <asp:HiddenField ID="hdnMessageLength" runat="server" Value='<%# Eval("ParameterSize") %>' />
                                                <asp:HiddenField ID="hdnWarningMessage" runat="server" Value='<%# Eval("WarningMessage") %>' />
                                                <asp:HiddenField ID="hdnGrdTripSheetReportDetailID" runat="server" Value='<%# Eval("TripSheetReportDetailID") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                        <asp:Label ID="lbCustom1" runat="server" Text="See Custom Flight Log option in Company Profile, Preflight Tab, for additional settings."
                                            Visible="true" />
                                    </CommandItemTemplate>
                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                </MasterTableView>
                                <ClientSettings EnablePostBackOnRowClick="False">
                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                                <GroupingSettings CaseSensitive="false" />
                            </telerik:RadGrid>
                            <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                                <tr>
                                    <td class="custom_radbutton" align="right">
                                        <telerik:RadButton ID="btnPrint" runat="server" Text="Print" OnClick="btnPrint_OnClick" />
                                        <telerik:RadButton ID="btnSave" runat="server" Text="Save" OnClick="btnSave_OnClick" OnClientClicking="fadeoutdivsaving" />
                                        <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" OnClientClicking="Close" />
                                        <asp:Label ID="InjectScript" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdnIsPrint" runat="server" />
                                        <asp:HiddenField ClientIDMode="Static" ID="hdnSaveMSG" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table id="tblHidden" style="display: none;">
                                <tr>
                                    <td>                                  
                                        <asp:Button ID="btnCancelYes" runat="server" Text="Button" OnClick="btnCancelYes_Click" />
                                        <asp:Button ID="btnCancelNo" runat="server" Text="Button" OnClick="btnCancelNo_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </div>
    </form>
    <script src="/Scripts/CustomScrollBar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript">
        function pageLoad() {
            if (IsMobileSafari()) {
                $(".rgDataDiv").mCustomScrollbar({
                    scrollButtons: { enable: false },
                    theme: "3d-thick",
                    scrollbarPosition: "outside",
                    scrollInertia: 0
                });
            }
        }
    </script>
</body>
</html>
