﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportViewerModule.aspx.cs"
    MasterPageFile="~/Framework/Masters/Popup.Master" Inherits="FlightPak.Web.Views.Reports.ReportViewerModule" %>

<%@ Register Src="~/UserControls/UCMultiSelect.ascx" TagName="Grid" TagPrefix="UCMultiSelect" %>
<%@ Register Src="~/UserControls/UCReportHtmlSelection.ascx" TagName="Grid" TagPrefix="UCReportHtmlSelection" %>
<%@ Register Src="~/UserControls/UCRptMonthYear.ascx" TagName="Grid" TagPrefix="UCRptMonthYear" %>
<%@ Register TagPrefix="uc" TagName="DatePicker" Src="~/UserControls/UCRptDateRange.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SettingHeadContent" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript">

            $(document).ready(function () {
               
                TripsheetWriterReport();
            });

            function FormatEditorOpen() {
                var ReportNumber = $("input[name$='ReportNumber']").first().val();
                var Title = $("span[id$='lbTitle']").first().text();
                if (ReportNumber) {
                    url = "/Views/Reports/FormatReport.aspx?TripsheetReportHeaderID=10002175006&ReportNumber=" + ReportNumber;
                    var oWnd = popupwindow(url, "", 700, 500);
                }
            }

            function TripsheetWriterReport() {

                var ReportType = $("input:hidden[name$='ReportType']");
                var count = $(':radio').length;

                if (ReportType.length > 0) {
                    if (ReportType.first().val().toLowerCase() == "tripsheetwriterreport") {
                        if (count > 1) {
                            var value1 = $(':radio').first().val();
                            var value2 = $(':radio').last().val();

                            if (value1 == "TripNo" & value2 == "DateRange") {
                                $("#FormatEditor").show();
                                var Trip = $('#TripNum').attr("name");
                                var RadioName = $(':radio').first().attr("name");
                                var SelectedValue = $('input[type=radio][name=' + RadioName + ']:checked').val();
                                if (SelectedValue == 'TripNo') {
                                    TRW_TripNo_Selected();
                                }
                                else if (SelectedValue == 'DateRange') {
                                    TRW_DateRange_Selected();
                                }
                                $('input[type=radio][name=' + RadioName + ']').live('change', function () {
                                    if (this.value == 'TripNo') {
                                        TRW_TripNo_Selected();
                                    }
                                    else if (this.value == 'DateRange') {
                                        TRW_DateRange_Selected();
                                    }
                                });


                            }
                        }
                    }
                }
            }
            
            function OnResponseEnd() {
                TripsheetWriterReport();
            }
            function TRW_TripNo_Selected() {
                $('.text80').parent().parent().hide();
                $('#TripNum').parent().parent().show();
                $('#ClientCD').parent().parent().hide();
                $('#HomeBaseCD').parent().parent().hide();
                $('#RequestorCD').parent().parent().hide();
                $('#TailNum').parent().parent().hide();
                $('#DepartmentCD').parent().parent().hide();
                $('#AuthorizationCD').parent().parent().hide();
                $('#CatagoryCD').parent().parent().hide();
                $('#CrewCD').parent().parent().hide();
                $(':checkbox').parent().parent().hide();

                $('.text80').val('');
            }
            function TRW_DateRange_Selected() {
                $('.text80').parent().parent().show();
                $('#TripNum').parent().parent().hide();
                $('#ClientCD').parent().parent().show();
                $('#HomeBaseCD').parent().parent().show();
                $('#RequestorCD').parent().parent().show();
                $('#TailNum').parent().parent().show();
                $('#DepartmentCD').parent().parent().show();
                $('#AuthorizationCD').parent().parent().show();
                $('#CatagoryCD').parent().parent().show();
                $('#CrewCD').parent().parent().show();
                $(':checkbox').parent().parent().show();

                $('#TripNum').val('');
            }

            function handle(e) {
                if (e.keyCode == 13) {
                    document.getElementById('<%=btnRptView.ClientID%>').focus();
                }
            }

            function openWin(url, value, radWin) {
                var oWnd = radopen(url + value, radWin);

                oWnd.ReloadOnShow = true;
            }


            function openDeptGrpWin(url, value, radWin) {
                if (document.getElementById('DepartmentCD').value == "") {
                    var oWnd = radopen(url + value, radWin);
                    oWnd.ReloadOnShow = true;
                }
                else {
                    radalert("Department Group cannot be selected once Department is selected", 330, 110);
                }
            }

            function openDeptWin(url, value, radWin) {
                if (document.getElementById('DepartmentGroupID') == null) {
                    var oWnd = radopen(url + value, radWin);
                    oWnd.ReloadOnShow = true;
                }
                else if (document.getElementById('DepartmentGroupID') != null && document.getElementById('DepartmentGroupID').value == "") {
                    var oWnd = radopen(url + value, radWin);
                    oWnd.ReloadOnShow = true;
                }
                else {
                    radalert("Department cannot be selected once Department Group is selected", 330, 110);
                }
            }

            function openAuthWin(url, value, radWin) {
                if (document.getElementById('DepartmentGroupID') == null) {
                    if (document.getElementById('<%=hdnDept.ClientID%>').value != "" && document.getElementById('DepartmentCD').value != "") {
                        var oWnd = radopen(url + value, radWin);
                        oWnd.ReloadOnShow = true;
                    }
                    else {
                        radalert("Please select a department.", 330, 110);
                    }
                } else if (document.getElementById('DepartmentGroupID') != null && document.getElementById('DepartmentGroupID').value == "") {
                    if (document.getElementById('<%=hdnDept.ClientID%>').value != "" && document.getElementById('DepartmentCD').value != "") {
                        var oWnd = radopen(url + value, radWin);
                        oWnd.ReloadOnShow = true;
                    }
                    else {
                        radalert("Please select a department.", 330, 110);
                    }
                }
                else {
                    radalert("Authorization cannot be selected once Department Group is selected", 330, 110);
                }
            }

            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function OnClientClose(oWnd, args) {
                var arg = args.get_argument();
                if (arg != null) {
                    var combo = $find(arg.CallingButton);
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById(arg.CallingButton).value = arg.Arg1;

                            if (arg.CallingButton != "CRLegNum" && document.getElementById('lbcv' + arg.CallingButton)) {
                                document.getElementById('lbcv' + arg.CallingButton).innerHTML = "";
                            }

                            if (arg.CallingButton.toLowerCase() != "legnum" && document.getElementById('lbcv' + arg.CallingButton)) {
                                document.getElementById('lbcv' + arg.CallingButton).innerHTML = "";
                            }

                            if ((arg.CallingButton == "DepartmentCD" && arg.DepartmentID != '') && document.getElementById('<%=hdnDept.ClientID%>')) {
                                document.getElementById('<%=hdnDept.ClientID%>').value = arg.DepartmentID;
                                var productElement = document.getElementById('AuthorizationCD');
                                if (productElement != null) {
                                    document.getElementById('AuthorizationCD').value = "";
                                    document.getElementById('AuthorizationCD').disabled = false;
                                }
                                var element = document.getElementById('DepartmentGroupID');
                                if (element != null) {
                                    document.getElementById('DepartmentGroupID').disabled = true;
                                }
                            }

                            if ((arg.CallingButton == "CRTripNUM" && arg.DepartmentID != '') && document.getElementById('<%=hdnCRMainID.ClientID%>'))
                                document.getElementById('<%=hdnCRMainID.ClientID%>').value = arg.CRMainID;


                            if ((arg.CallingButton.toLowerCase() == "tripnum" && arg.DepartmentID != '') && document.getElementById('<%=hdnMainID.ClientID%>')) {
                                document.getElementById('<%=hdnMainID.ClientID%>').value = arg.TripID;
                                var LegElement = document.getElementById('LegNum');
                                if (LegElement != null) {
                                    document.getElementById('LegNum').value = "";
                                    document.getElementById('LegNum').disabled = false;
                                }
                            }

                            if (arg.CallingButton == "DepartmentGroupID") {
                                document.getElementById('<%=hdnDept.ClientID%>').value = "";

                                var productElement = document.getElementById('AuthorizationCD');
                                if (productElement != null) {
                                    document.getElementById('AuthorizationCD').value = "";
                                    document.getElementById('AuthorizationCD').disabled = true;
                                }
                                var element = document.getElementById('DepartmentCD');
                                if (element != null) {
                                    document.getElementById('DepartmentCD').value = "";
                                    document.getElementById('DepartmentCD').disabled = true;
                                }
                            }
                            __doPostBack("hdnPostBack", "");
                        }
                        else {
                            document.getElementById(arg.CallingButton).value = "";
                            combo.clearSelection();
                        }
                    }
                }
            }

            var currentTextBox = null;
            var currentDatePicker = null;

            //This method is called to handle the onclick and onfocus client side events for the texbox
            function showPopup(sender, e) {
                //this is a reference to the texbox which raised the event
                //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                //this gets a reference to the datepicker, which will be shown, to facilitate
                //the selection of a date
                var datePicker = $find("<%= RadDatePicker1.ClientID %>");

                //this variable is used to store a reference to the date picker, which is currently
                //active
                currentDatePicker = datePicker;

                //this method first parses the date, that the user entered or selected, and then
                //sets it as a selected date to the picker
                datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));

                //the code lines below show the calendar, which is used to select a date. The showPopup
                //function takes three arguments - the x and y coordinates where to show the calendar, as
                //well as its height, derived from the offsetHeight property of the textbox
                var position = datePicker.getElementPosition(currentTextBox);
                datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
            }
            //this handler is used to set the text of the TextBox to the value of selected from the popup
            function dateSelected(sender, args) {
                if (currentTextBox != null) {
                    //currentTextBox is the currently selected TextBox. Its value is set to the newly selected
                    //value of the picker
                    currentTextBox.value = args.get_newValue();
                }
            }
            //this function is used to parse the date entered or selected by the user
            function parseDate(sender, e) {
                if (currentDatePicker != null) {
                    var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                    var dateInput = currentDatePicker.get_dateInput();
                    var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                    sender.value = formattedDate;
                }
            }

            function HeaderBySourceEnable() {
                var listBox = $find('<%= HeaderBySource.ClientID%>');
                //alert(listBox._element.disabled);
                listBox.trackChanges();
                listBox._element.disabled = false;
                listBox.commitChanges();
                //alert(listBox._element.disabled);
            }

            function CheckValid(btnCalled) {
                var ReturnVal = true;
                var ReportType = $("input:hidden[name$='ReportType']");
                var count = $(':radio').length;
              
                if (ReportType.length > 0) {
                    if (ReportType.first().val().toLowerCase() == "tripsheetwriterreport") {
                        if (count > 1) {
                            var value1 = $(':radio').first().val();
                            var value2 = $(':radio').last().val();

                            if (value1 == "TripNo" & value2 == "DateRange") {
                                var RadioName = $(':radio').first().attr("name");
                                var SelectedValue = $('input[type=radio][name=' + RadioName + ']:checked').val();
                                if (SelectedValue == 'TripNo') {
                                    $('.text80').val('');
                                    if ($('#TripNum').val())
                                    { ReturnVal = true; }
                                    else { radalert('Select Trip No. to print the report.', 360, 100, btnCalled.value, ""); ReturnVal = false; }
                                }
                                else if (SelectedValue == 'DateRange') {
                                    $('#TripNum').val('');

                                    if ($('.text80').first().val() != "" & $('.text80').last().val() != "")
                                    { ReturnVal = true; }
                                    else { radalert('Select Date. to print the report.', 360, 100, btnCalled.value, ""); ReturnVal = false; }

                                }
                            }
                        }
                    }
                }
                if (document.getElementById('hdnRptXMLName').value == 'POSTDetailBillingByTrip.xml') {
                    var DateFrom, DateTo, LogNo;

                    DateFrom = document.getElementsByName('ctl00$SettingBodyContent$DATEFROM$tbDate')[0].value;
                    DateTo = document.getElementsByName('ctl00$SettingBodyContent$DATETO$tbDate')[0].value;
                    LogNo = document.getElementById('LogNum').value;

                    if ((DateFrom == '' && DateTo == '') && LogNo == '') {
                        radalert('Select either Date or Log No. to print the report.', 360, 100, btnCalled.value, "");
                        ReturnVal = false;
                    }
                }
                if (document.getElementById('hdnRptXMLName').value == 'POSTExpense.xml') {
                    var DateFrom, DateTo, LogNo;

                    DateFrom = document.getElementsByName('ctl00$SettingBodyContent$DATEFROM$tbDate')[0].value;
                    DateTo = document.getElementsByName('ctl00$SettingBodyContent$DATETO$tbDate')[0].value;
                    LogNo = document.getElementById('LogNum').value;

                    if ((DateFrom == '' && DateTo == '') && LogNo == '') {
                        radalert('Select either Date or Log No. to print the report.', 360, 100, btnCalled.value, "");
                        ReturnVal = false;
                    }
                }
              if (ReturnVal == true && btnCalled.value == "Preview") {
                  document.getElementById('<%= btnRptView1.ClientID%>').click();
                 // openTab();
                }
                else if (ReturnVal == true && btnCalled.value == "Export") {
                    document.getElementById('<%= btExport1.ClientID%>').click();
                }
                //return false;
            }

            function ExportReport_Click() {
                document.getElementById('<%=btExportFileFormat.ClientID%>').click();
               // openTab();
            }

            function openTab() {
                if ($telerik.isMobileSafari) {
                    var url = 'ReportRenderView.aspx';
                    var a = window.document.createElement("a");
                    a.target = '_parent';
                    a.href = url;
                    window.open(url, '_parent');
                } else {
                    $.get("ReportRenderView.aspx", function(d) {
                        $("#PDFViewerFrame").attr("src", "PDFViewer.aspx").show();
                    });
                }
            };
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" DateFormat="MM/dd/yyyy" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server" ShowRowHeaders="false">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                    ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true"
        ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" OnClientClose="OnClientClose" Modal="true"
                Behaviors="close" VisibleStatusbar="false">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1" >
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        
        <ClientEvents  OnResponseEnd="OnResponseEnd" />
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <asp:Panel ID="pnlReportContent" runat="server" DefaultButton="btnRptView">
        <div id="DivExternalForm" runat="server" class="ExternalForm">
            
            <div class="art-setting-menu-report">
                <div class="globalMenu_mapping">
                    <%--<a href="#" class="globalMenu_mapping">Home</a> <span class="breadcrumbCaret"></span>
            <a href="#" class="globalMenu_mapping">Settings</a>--%>
                </div>
                <div style="clear: both;">
                    <uc:DatePicker ID="ucDatePicker" runat="server" Visible="false"></uc:DatePicker>
                    <table width="100%" cellpadding="0" cellspacing="0" id="Tabtable" style="display: none;">
                        <tr>
                            <td align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button runat="server" ID="btnDatabase" Text="Database" CssClass="rpt_nav" OnClick="btnDb_Click"
                                                CausesValidation="false" />
                                        </td>
                                        <td>
                                            <asp:Button runat="server" ID="btnPreFlight" Text="Preflight" CssClass="rpt_nav"
                                                OnClick="btnPre_Click" CausesValidation="false" />
                                        </td>
                                        <td>
                                            <asp:Button runat="server" ID="btnPostFlight" Text="Postflight" CssClass="rpt_nav"
                                                OnClick="btnPost_Click" CausesValidation="false" />
                                        </td>
                                        <td>
                                            <asp:Button runat="server" ID="btnCharter" Text="Charter" CssClass="rpt_nav" OnClick="btnCharter_Click"
                                                CausesValidation="false" />
                                        </td>
                                        <td>
                                            <asp:Button runat="server" ID="btnCorp" Text="Corporate Request" CssClass="rpt_nav"
                                                OnClick="btnCorpReq_Click" CausesValidation="false" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="right">
                                <div class="mandatory">
                                    <span>Bold</span> Indicates required field</div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="art-setting-content-report">
                    <div class="art-setting-sidebar-report">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <telerik:RadPanelBar runat="server" ExpandMode="SingleExpandedItem" ID="pnlReportTab"
                                        AllowCollapseAllItems="true" CssClass="RadNavMenu">
                                        <CollapseAnimation Type="None"></CollapseAnimation>
                                        <ExpandAnimation Type="None"></ExpandAnimation>
                                    </telerik:RadPanelBar>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="art-setting-content-rpt">
                        <table cellspacing="0" cellpadding="0" width="100%" class="border-box">
                            <tr>
                                <td>
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <span class="mnd_text">
                                                    <asp:Label runat="server" ID="lbTitle"></asp:Label></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tblspace_10">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:PlaceHolder ID="Report" runat="server"></asp:PlaceHolder>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tblspace_10">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td valign="top">
                                                <asp:Button runat="server" ID="btExport" CssClass="ui_nav" Text="Export" OnClick="btnExport_Click"
                                                    OnClientClick="javascript:return CheckValid(this);" />
                                                <asp:Button runat="server" ID="btExport1" CssClass="ui_nav" Text="Export" OnClick="btnExport_Click"
                                                    Style="display: none;" />
                                            </td>
                                            <td valign="top">
                                                <asp:Button ID="btnRptView" runat="server" CssClass="ui_nav" OnClientClick="javascript:return CheckValid(this);"
                                                    OnClick="btnRptView_Click" Text="Preview" />
                                                <input type="button" class="ui_nav" value="Format Editor" id="FormatEditor"   onclick="FormatEditorOpen(); return false;" style="display:none;" />
                                                <asp:Button ID="btnRptView1" runat="server" CssClass="ui_nav" OnClick="btnRptView_Click"
                                                    Style="display: none;" Text="Preview" />
                                            </td>
                                            <td valign="top">
                                                <asp:Button ID="btnReset" runat="server" CausesValidation="false" CssClass="ui_nav"
                                                    OnClick="btnReset_Click" Text="Reset" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Panel runat="server" ID="ExportPanal">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td class="tdLabel100">
                                                                Export File Type
                                                            </td>
                                                            <td style="margin-left: 40px">
                                                                <asp:DropDownList runat="server" ID="ddlFormat">
                                                                    <%--<asp:ListItem Text="HTML Table" Value="HTML"></asp:ListItem>--%>
                                                                    <asp:ListItem Text="Excel" Value="Excel"></asp:ListItem>
                                                                    <asp:ListItem Text="MHTML" Value="MHTML"></asp:ListItem>
                                                                    <%-- <asp:ListItem Text="Visual FoxPro" Value="VFO"></asp:ListItem>

                                     <asp:ListItem Text="FoxPro 2.x" Value="FP"></asp:ListItem>

                                     <asp:ListItem Text="Comma Delimited Text" Value="CSV"></asp:ListItem>

                                     <asp:ListItem Text="Excel5.0" Value="Excel5"></asp:ListItem>

                                     <asp:ListItem Text="Excel2.0,3.0 and 4.0" Value="Excel2"></asp:ListItem>

                                     <asp:ListItem Text="Lotus 123.2.x" Value="Lotus"></asp:ListItem>

                                     <asp:ListItem Text="Data Interchange Format" Value="DIF"></asp:ListItem>

                                     <asp:ListItem Text="System Data Foramt" Value="SDF"></asp:ListItem>

                                     <asp:ListItem Text="Symblic Link Format" Value="SLF"></asp:ListItem>--%>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:Button runat="server" CssClass="ui_nav" ID="btExportFileFormat" Text="Generate"
                                                                   OnClientClick="ExportReport_Click()" />
                                                                  <asp:Button runat="server" CssClass="ui_nav" ID="btExportFileFormat1" Text="Generate"
                                                                    OnClick="btnExportFileFormat_Click"  Style="display: none;" />
                                                            </td>
                                                            <td>
                                                                <asp:Button runat="server" CssClass="ui_nav" ID="btClose" Text="Close" OnClick="btnCostFileFormat_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblError" CssClass="alert-text"></asp:Label>
                                    <asp:Wizard ID="Wizard1" runat="server" DisplaySideBar="false" ActiveStepIndex="0"
                                        Height="209px" Width="100%" BackColor="#f0f0f0" FinishPreviousButtonStyle-CssClass="ui_nav"
                                        CancelButtonStyle-CssClass="ui_nav" FinishCompleteButtonStyle-CssClass="ui_nav"
                                        StepNextButtonStyle-CssClass="ui_nav" StepPreviousButtonStyle-CssClass="ui_nav"
                                        StartNextButtonStyle-CssClass="ui_nav" OnFinishButtonClick="OnFinish" OnNextButtonClick="OnNext"
                                        OnPreviousButtonClick="OnPrevious">
                                        <WizardSteps>
                                            <asp:WizardStep ID="WizardStep1" runat="server">
                                                <asp:Label runat="server" ID="lblw1" Text="Click on each of the Available fields by name and the right arrow to select them. Add them in the preferred display order or use the Up/Down arrows to reorder them for reporting. Click on the Selected fields and left arrow to de-select them."></asp:Label>
                                                <br />
                                                <br />
                                                <UCReportHtmlSelection:Grid ID="UCRHS" runat="server" />
                                            </asp:WizardStep>
                                            <asp:WizardStep ID="WizardStep2" runat="server">
                                                <asp:Label runat="server" ID="lblw2" Text="Select how data is to be sorted"></asp:Label>
                                                <br />
                                                <br />
                                                <br />
                                                <UCReportHtmlSelection:Grid ID="UCRHSort" runat="server" />
                                                <%-- <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel1">

                                            <telerik:RadListBox runat="server" ID="SelectionSource" Height="200px" Width="200px"

                                                SelectionMode="Single" />

                                            <telerik:RadListBox runat="server" ID="SelectionDestination" AllowReorder="false"

                                                SelectionMode="Single" Height="200px" Width="200px">

                                            </telerik:RadListBox>

                                        </telerik:RadAjaxPanel>--%>
                                            </asp:WizardStep>
                                            <asp:WizardStep ID="WizardStep4" runat="server">
                                                <asp:Label runat="server" ID="lblw3" Text="Edit the Header Text "></asp:Label>
                                                <asp:TextBox ID="tbEditHeaderText" runat="server" OnTextChanged="tbEditHeaderText_OnTextChanged"
                                                    AutoPostBack="true" ClientIDMode="Static" />
                                                <br />
                                                <br />
                                                <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel2">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                Field
                                                            </td>
                                                            <td>
                                                                HTML Heading
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <telerik:RadListBox runat="server" ID="SelectedSource" Height="200px" Width="200px"
                                                                    SelectionMode="Single" />
                                                            </td>
                                                            <td>
                                                                <telerik:RadListBox runat="server" ID="HeaderBySource" AllowReorder="false" SelectionMode="Single"
                                                                    Height="200px" Width="200px" OnSelectedIndexChanged="HeaderBySource_OnSelectedIndexChanged"
                                                                    AutoPostBack="true">
                                                                </telerik:RadListBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </telerik:RadAjaxPanel>
                                            </asp:WizardStep>
                                            <asp:WizardStep ID="WizardStep3" runat="server">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="60%">
                                                            <fieldset>
                                                                <legend>Page Layout</legend>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td class="tdLabel130">
                                                                                        Use HTML Defaults
                                                                                    </td>
                                                                                    <td class="tdLabel80">
                                                                                        <asp:CheckBox ID="chkHeader" runat="server" Text="Header" />
                                                                                    </td>
                                                                                    <td class="tdLabel70">
                                                                                        <asp:CheckBox ID="chkFooter" runat="server" Text="Footer" />
                                                                                    </td>
                                                                                    <td class="tdLabel100">
                                                                                        <asp:CheckBox ID="chkBackground" runat="server" Text="Background" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td class="tdLabel130">
                                                                                        Title
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtTitle" runat="server" Width="300px"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td class="tdLabel130">
                                                                                        Foreground Color
                                                                                    </td>
                                                                                    <td class="tdLabel60">
                                                                                        <telerik:RadColorPicker ID="ForegroundColor" runat="server" SelectedColor="" ShowIcon="True">
                                                                                            <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                                        </telerik:RadColorPicker>
                                                                                    </td>
                                                                                    <td class="tdLabel110">
                                                                                        Background Color
                                                                                    </td>
                                                                                    <td>
                                                                                        <telerik:RadColorPicker ID="BackgroundColor" runat="server" SelectedColor="" ShowIcon="True">
                                                                                            <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                                        </telerik:RadColorPicker>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                        <td width="40%">
                                                            <fieldset>
                                                                <legend>Table Layout</legend>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td class="tdLabel130">
                                                                                        Border Color
                                                                                    </td>
                                                                                    <td>
                                                                                        <telerik:RadColorPicker ID="TableBorderColor" runat="server" SelectedColor="" ShowIcon="True">
                                                                                            <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                                        </telerik:RadColorPicker>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td class="tdLabel130">
                                                                                        Foreground Color
                                                                                    </td>
                                                                                    <td>
                                                                                        <telerik:RadColorPicker ID="TableForegroundColor" runat="server" SelectedColor=""
                                                                                            ShowIcon="True">
                                                                                            <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                                        </telerik:RadColorPicker>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td class="tdLabel130">
                                                                                        Background Color
                                                                                    </td>
                                                                                    <td>
                                                                                        <telerik:RadColorPicker ID="TableBackgroundColor" runat="server" SelectedColor=""
                                                                                            ShowIcon="True">
                                                                                            <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                                        </telerik:RadColorPicker>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:WizardStep>
                                        </WizardSteps>
                                    </asp:Wizard>
                                </td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td>
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:Button ID="Button1" runat="server" Visible="false" CssClass="button" OnClick="btnSubmit_Click"
                                                    Text="Preview Report" />
                                                <%--<asp:TextBox ID="tbclient" runat="server" CssClass="text60" ></asp:TextBox>&nbsp--%>
                                                <asp:TextBox ID="txtOutput" Visible="false" runat="server" TextMode="MultiLine" Width="300"
                                                    Height="35"></asp:TextBox>
                                                <asp:TextBox ID="txtHtmlTable" runat="server" Visible="false" TextMode="MultiLine"
                                                    Width="300" Height="35"></asp:TextBox>
                                                <asp:HiddenField ID="hdnDept" runat="server" ClientIDMode="Static" />
                                                <asp:HiddenField ID="hdnCRMainID" runat="server" ClientIDMode="Static" />
                                                <asp:HiddenField ID="hdnMainID" runat="server" ClientIDMode="Static" />
                                                <asp:HiddenField ID="hdnHeaderSourceID" runat="server" />
                                                <asp:HiddenField ID="hdnRptXMLName" runat="server" ClientIDMode="Static" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <div class="tblspace_5">
                                    </div>
                                </td>
                            </tr>
                        </table>
                         <iframe style="display:none;" name="PDFViewer" id="PDFViewerFrame" width="980px" height="1000px"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
