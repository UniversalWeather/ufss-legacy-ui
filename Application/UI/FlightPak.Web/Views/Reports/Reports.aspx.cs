﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Common;
using FlightPak.Common.Constants;

namespace FlightPak.Web.Views.Reports
{
    public partial class Reports : BaseSecuredPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //var objBaseSecuredMasterPage = new BaseSecuredMasterPage();
            if (!HaveModuleAccess(ModuleId.DatabaseReports))
            {
                Response.Redirect("~/Views/Home.aspx?m=DatabaseReports");
            }

            if (!Page.IsPostBack)
            {
                //LoadData();
            }
        }

   //     private void LoadData()
   //     {
   //         using (CommonService.CommonServiceClient common = new CommonService.CommonServiceClient())
   //         {
   //             var LockData = common.GetAllLocks();
   //             if (LockData.ReturnFlag == true)
   //             {
   //                 gvLock.DataSource = LockData.EntityList.ToList();
   //                 gvLock.DataBind();
   //             }
   //         }
   //     }


   //     protected void gvLock_RowDataBound(object sender, GridViewRowEventArgs e)
   //     {
   //         if (e.Row.RowType == DataControlRowType.Header)
   //         {
   //             LinkButton lnkSelected = (LinkButton)e.Row.FindControl("lnkDeleteSelected");
   //             lnkSelected.Attributes.Add("onclick", "javascript:return " +
   //             "confirm('Are you sure you want to delete lock for these selected records." +
   //             DataBinder.Eval(e.Row.DataItem, "0") + "')");
   //         }

   //         //if (e.Row.RowType == DataControlRowType.DataRow)
   //         //{
   //         //    LinkButton l = (LinkButton)e.Row.FindControl("LinkButton1");
   //         //    l.Attributes.Add("onclick", "javascript:return " +
   //         //    "confirm('Are you sure you want to delete this Lock " +
   //         //    DataBinder.Eval(e.Row.DataItem, "FPLockID") + "')");
   //         //}

   //         if (e.Row.RowType == DataControlRowType.DataRow &&
   //(e.Row.RowState == DataControlRowState.Normal ||
   // e.Row.RowState == DataControlRowState.Alternate))
   //         {
   //             CheckBox chkBxSelect = (CheckBox)e.Row.Cells[1].FindControl("chkBxSelect");
   //             CheckBox chkBxHeader = (CheckBox)this.gvLock.HeaderRow.FindControl("chkBxHeader");
   //             chkBxSelect.Attributes["onclick"] = string.Format
   //                                                    (
   //                                                       "javascript:ChildClick(this,'{0}');",
   //                                                       chkBxHeader.ClientID
   //                                                    );
   //         }


   //     }

   //     protected void gvLock_RowCommand(object sender, GridViewCommandEventArgs e)
   //     {
   //         if (e.CommandName == "Delete")
   //         {
   //             Int64 FPLockID = Convert.ToInt64(e.CommandArgument);
   //             DeleteLockedRecord(FPLockID);
   //         }
   //         if (e.CommandName == "DeleteSelected")
   //         {
   //             foreach (GridViewRow row in gvLock.Rows)
   //             {
   //                 CheckBox checkbox = (CheckBox)row.FindControl("chkBxSelect");

   //                 //Check if the checkbox is checked.
   //                 //value in the HtmlInputCheckBox's Value property is set as the //value of the delete command's parameter.
   //                 if (checkbox.Checked)
   //                 {
   //                     // Retreive the Employee ID
   //                     Int64 FPLockID = Convert.ToInt32(gvLock.DataKeys[row.RowIndex].Value);
   //                     using (CommonService.CommonServiceClient common = new CommonService.CommonServiceClient())
   //                     {
   //                         var DeleteRecord = common.DeleteLock(FPLockID);
   //                     }
   //                 }
   //             }
   //             LoadData();
   //         }

   //     }

   //     protected void gvLock_RowDeleting(object sender, GridViewDeleteEventArgs e)
   //     {
   //         //  int FPLockID = (int)gvLock.DataKeys[e.RowIndex].Value;
   //         //DeleteRecordByID(FPLockID);
   //     }

   //     private void DeleteLockedRecord(Int64 FPLockID)
        //{
        //    using (CommonService.CommonServiceClient common = new CommonService.CommonServiceClient())
        //    {
        //        var DeleteRecord = common.DeleteLock(FPLockID);
        //        if (DeleteRecord.ReturnFlag == true)
        //        {
        //            LoadData();
        //        }
        //    }
        //}

    }
}