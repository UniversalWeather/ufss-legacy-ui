﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.IO;
using System.Globalization;
using System.Text;
using Telerik.Web.UI;
using System.Xml;
using System.Xml.Linq;
using System.Drawing;
using System.Data;
using FlightPak.Web.Framework.Prinicipal;

//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using System.Data;
using FlightPak.Web.Framework.Helpers;


namespace FlightPak.Web.Views.Reports
{
    public partial class ReportViewerModule : BaseSecuredPage
    {
        //TODO Server.MapPath
        string urlBase = "Config\\";
        public Dictionary<string, string> dicColumnList = new Dictionary<string, string>();
        public Dictionary<string, string> dicSelectedList = new Dictionary<string, string>();
        public Dictionary<string, string> dicSelectedListsort = new Dictionary<string, string>();
        public Dictionary<string, string> dicSelectedListsortSel = new Dictionary<string, string>();
        public Dictionary<string, string> dicColumnListsort = new Dictionary<string, string>();

        public Dictionary<string, string> dicSourceList = new Dictionary<string, string>();
        public Dictionary<string, string> dicDestinationList = new Dictionary<string, string>();
        public Dictionary<string, string> dicSourceSortList = new Dictionary<string, string>();
        public Dictionary<string, string> dicDestinationSortList = new Dictionary<string, string>();
        public Dictionary<string, string> dicEmptyList = new Dictionary<string, string>();

        public FlightPakMasterService.Company CompanySett = new FlightPakMasterService.Company();
        string filename = string.Empty;
        private ExceptionManager exManager;
        string ReportName = string.Empty;
        string DateFormat = string.Empty;
        public bool IsNotExist = false;
        public bool IsDept = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Hardcoded for now to invoke from DB catalog
                        //Session["TabSelect"] = "Db";
                        //if (Session["PageLoad"] == null)
                        //{
                        //   // SelectTab();
                        //}
                        //else
                        //{
                        //    //string TabSelect = Convert.ToString(Session["TabSelect"]);
                        //    //pnlReportTab.Items.Clear();
                        //    //string xmltab = string.Empty;
                        //    //xmltab = "CharterMainTab.xml";
                        //    //LoadTabs(pnlReportTab, urlBase + xmltab);
                        //    //btnPreFlight.CssClass = "rpt_nav";
                        //    //btnPostFlight.CssClass = "rpt_nav";
                        //    //btnDatabase.CssClass = "rpt_nav";
                        //    //btnCorp.CssClass = "rpt_nav";
                        //    //btnCharter.CssClass = "rpt_nav_active";
                        //    //ExpandPanel(pnlReportTab, xmltab, true);
                        //}
                        if (!IsPostBack)
                        {
                            Wizard1.Visible = false;
                            ExportPanal.Visible = false;

                            Session["SourceList"] = null;
                            Session["DestinationList"] = null;
                            Session["SourceSortList"] = null;
                            Session["DestinationSortList"] = null;
                            Session["IsTempFile"] = null;
                        }

                        hdnRptXMLName.Value = Request.QueryString["xmlFilename"];

                        if (btnRptView.Visible)
                            btnRptView.Focus();

                        DisableAuthorization();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }


        public void Page_Init(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                        }
                        else
                        {
                            DateFormat = "MM/dd/yyyy";
                        }
                        ((RadDatePicker)ucDatePicker.FindControl("RadDatePicker1")).DateInput.DateFormat = DateFormat;
                        GenerateControls();
                        ProcessColumns();

                        UCRHS.ColumnList = dicColumnList;
                        UCRHSort.ColumnList = dicColumnList;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }


            /* if (Session["dicSelectedList"] != null)
             {
                 Dictionary<string, string> dicTempSelectedList = (Dictionary<string, string>)Session["dicSelectedList"];
                 foreach (KeyValuePair<string, string> pair in dicTempSelectedList)
                 {
                     dicColumnList.Remove(pair.Key);
                 }

                 UCRHS.ColumnList = dicColumnList;
             }
             else
             {
                 UCRHS.ColumnList = dicColumnList;
             }


             if (Session["dicSelectedListSort"] != null)
             {
                 Dictionary<string, string> dicTempSelectedList = (Dictionary<string, string>)Session["dicSelectedListSort"];
                 UCRHSort.RadListBoxSource.DataSource = dicTempSelectedList;
                 UCRHSort.RadListBoxSource.DataBind();
                 dicTempSelectedList = (Dictionary<string, string>)Session["dicSelectedListSortSel"];
                 UCRHSort.RadListBoxDestination.DataSource = dicTempSelectedList;
                 UCRHSort.RadListBoxDestination.DataBind();
             }
             */

            // UCRHSort.ColumnList = dicColumnList;
        }


        private void GenerateControls()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                filename = Convert.ToString(Request.QueryString["xmlFilename"]);

                if (!string.IsNullOrEmpty(filename))
                    Session["XMLFileName"] = filename;

                if (filename != null)
                {
                    var filePath = Server.MapPath(urlBase + filename);
                    XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                    var xmlReaderObject = xmlReader.XmlCleanReader();
                    if (xmlReaderObject != null)
                    {
                        XPathDocument SearchFieldDoc = new XPathDocument(xmlReaderObject);
                        XslCompiledTransform transform = new XslCompiledTransform();
                        transform.Load(Server.MapPath(urlBase + "MakeControls.xslt"));
                        StringWriter sw = new StringWriter();
                        transform.Transform(SearchFieldDoc, null, sw);
                        string result = sw.ToString();
                        result = result.Replace("xmlns:asp=\"remove\"", "");
                        Control ctrl = Page.ParseControl(result);

                        foreach (Control innerControl in ctrl.Controls)
                        {
                            if (innerControl is TextBox)
                            {
                                ((TextBox)innerControl).TextChanged += Ctrl_OnTextChanged;
                                if (Session[Session["TabSelect"] + ((TextBox)innerControl).ID] != null)

                                    ((TextBox)innerControl).Text = Session[Session["TabSelect"] + ((TextBox)innerControl).ID].ToString();
                            }

                            if (innerControl is RadioButtonList)
                            {
                                if (Session[Session["TabSelect"] + ((RadioButtonList)innerControl).ID] != null)
                                    ((RadioButtonList)innerControl).SelectedValue = Session[Session["TabSelect"] + ((RadioButtonList)innerControl).ID].ToString();
                            }

                            if (innerControl is CheckBox)
                            {
                                if (Session[Session["TabSelect"] + ((CheckBox)innerControl).ID] != null)
                                    ((CheckBox)innerControl).Checked = (bool)Session[Session["TabSelect"] + ((CheckBox)innerControl).ID];
                            }



                            if (innerControl is DropDownList)
                            {
                                if (innerControl.ClientID.IndexOf("MonthFrom") <= 0)
                                {
                                    //DropDownList ddlmonth = (DropDownList)innerControl.FindControl("MonthFrom");
                                    DropDownList ddlmonth = (DropDownList)innerControl;
                                    if (ddlmonth != null)
                                    {
                                        if (Session[Session["TabSelect"] + innerControl.ID + "ddlMonth"] != null)
                                        {
                                            ListItem ItemSel = ddlmonth.Items.FindByText(Session[Session["TabSelect"] + innerControl.ID + "ddlMonth"].ToString());
                                            if (ItemSel != null)
                                                ddlmonth.SelectedValue = ItemSel.Value;
                                        }

                                    }
                                }
                            }

                            if (innerControl is UserControl)
                            {
                                if (innerControl.ClientID.IndexOf("MonthFrom") <= 0)
                                {
                                    if (innerControl.ClientID == "AsOf")
                                    {
                                        //TextBox tbAsOf = (TextBox)innerControl.FindControl("tbDate");
                                        if (DateFormat != null)
                                        {
                                            if (Session[Session["TabSelect"] + innerControl.ID + "tbDate"] != null)
                                            {
                                                ((TextBox)innerControl.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Session[Session["TabSelect"] + innerControl.ID + "tbDate"].ToString());
                                            }
                                            else
                                            {
                                                ((TextBox)innerControl.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", System.DateTime.Now);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        TextBox tbDateOfBirth = (TextBox)innerControl.FindControl("tbDate");

                                        if (tbDateOfBirth != null)
                                        {
                                            if (Session[Session["TabSelect"] + innerControl.ID + "tbDate"] != null)
                                                tbDateOfBirth.Text = Session[Session["TabSelect"] + innerControl.ID + "tbDate"].ToString();
                                        }
                                    }
                                    DropDownList ddlmonth = (DropDownList)innerControl.FindControl("ddlMonth");
                                    if (ddlmonth != null)
                                    {
                                        if (Session[Session["TabSelect"] + innerControl.ID + "ddlMonth"] != null)
                                        {

                                            ListItem ItemSel = ddlmonth.Items.FindByText(Session[Session["TabSelect"] + innerControl.ID + "ddlMonth"].ToString());
                                            if (ItemSel != null)
                                                ddlmonth.SelectedValue = ItemSel.Value;
                                        }

                                    }

                                    DropDownList ddlyear = (DropDownList)innerControl.FindControl("ddlyear");
                                    if (ddlyear != null)
                                    {
                                        if (Session[Session["TabSelect"] + innerControl.ID + "ddlyear"] != null)
                                        {
                                            ListItem ItemSel = ddlyear.Items.FindByText(Session[Session["TabSelect"] + innerControl.ID + "ddlyear"].ToString());
                                            if (ItemSel != null)
                                                ddlmonth.SelectedValue = ItemSel.Value;
                                        }

                                    }

                                    RadMonthYearPicker monthpicker = (RadMonthYearPicker)innerControl.FindControl("tlkMonthView");
                                    if (monthpicker != null)
                                    {
                                        if (Session[Session["TabSelect"] + innerControl.ID + "tlkMonthView"] != null)
                                        {
                                            monthpicker.SelectedDate = (DateTime)Session[Session["TabSelect"] + innerControl.ID + "tlkMonthView"];
                                        }
                                    }
                                }
                            }
                        }

                        Report.Controls.Add(ctrl);
                        string[] strArr = GetReportTitle().Split(',');
                        if (strArr.Count() > 0)
                        {
                            lbTitle.Text = Convert.ToString(strArr[0]);
                        }
                        Controlvisible(true);
                    }
                }
                else
                {
                    Controlvisible(false);
                }

                if (filename == "CRTripItinerary.xml")
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["CRTripNUM"]) && Report.FindControl("CRTripNUM") != null)
                    {
                        ((TextBox)Report.FindControl("CRTripNUM")).Text = Convert.ToString(Request.QueryString["CRTripNUM"]);
                    }

                    if (Request.QueryString["CRTripID"] != null)
                    {
                        hdnCRMainID.Value = Request.QueryString["CRTripID"];
                    }
                    else
                    {
                        hdnCRMainID.Value = string.Empty;
                    }
                }

            }

        }


        private void SelectTab()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string TabSelect = Convert.ToString(Session["TabSelect"]);
                pnlReportTab.Items.Clear();
                string xmltab = string.Empty;
                switch (TabSelect)
                {
                    case "PreFlight":
                        xmltab = "PreFlightMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav_active";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav";
                        break;
                    case "PostFlight":
                        xmltab = "PostFlightMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav_active";
                        btnDatabase.CssClass = "rpt_nav";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav";
                        break;
                    case "Db":
                        xmltab = "DatabaseMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav_active";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav";
                        break;
                    case "CorpReq":
                        xmltab = "CorporateFlightMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav";
                        btnCorp.CssClass = "rpt_nav_active";
                        btnCharter.CssClass = "rpt_nav";
                        break;
                    case "Charter":
                        xmltab = "CharterMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav_active";
                        if (!string.IsNullOrEmpty(filename) && (filename == "CQTripStatus.xml" || filename == "CQAircraftProfile.xml"))
                        {
                            Session["PageLoad"] = "pageload";
                            //Response.Redirect("ReportViewer.aspx?xmlFilename=CQTripStatus.xml");
                            Response.Redirect("ReportViewer.aspx?xmlFilename=" + filename);
                        }
                        else
                            //Response.Redirect("CQReportViewer.aspx?xmlFilename=CQAircraftProfile.xml");
                            Response.Redirect("CQReportViewer.aspx?xmlFilename=CQItinerary.xml");
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        break;
                    default:
                        Session["TabSelect"] = "Db";
                        xmltab = "DatabaseMainTab.xml";
                        btnPreFlight.CssClass = "rpt_nav_active";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav";
                        Response.Redirect("ReportViewer.aspx?xmlFilename=DBAccounts.xml");
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        break;
                }
                ExpandPanel(pnlReportTab, xmltab, true);
            }
        }


        private void ExpandPanel(RadPanelBar panelBar, string TabXmlname, bool isExpand)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(panelBar, TabXmlname, isExpand))
            {
                string Selectedvalue = Convert.ToString(Request.QueryString["xmlFilename"]);
                string attrVal = "";
                string TitleTab = string.Empty;
                var filePath = Server.MapPath(urlBase + TabXmlname);
                XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                var xmlReaderObject = xmlReader.XmlCleanReader();
                if (xmlReaderObject != null)
                {
                    XDocument doc = XDocument.Load(xmlReaderObject);

                    if (Request.QueryString["xmlFilename"] != null)
                    {

                        var items = doc.Descendants("screen")
                                      .Where(c => c.Attribute("url").Value.Contains(Convert.ToString(Request.QueryString["xmlFilename"])))
                                       .Select(c => new { x = c.Value, y = c.Parent.Attribute("text").Value, z = c.Attribute("name").Value })
                                       .ToList();

                        if (items != null)
                        {
                            if (items.Count != 0)
                            {

                                RadPanelItem selectedItem = panelBar.FindItemByText(items[0].y);

                                if (selectedItem != null)
                                {
                                    if (selectedItem.Items.Count > 0)
                                    {

                                        RadPanelItem selectedvalue = selectedItem.Items.FindItemByText(items[0].z);
                                        selectedvalue.Selected = true;
                                        selectedItem.Expanded = true;

                                    }
                                    else
                                    {
                                        selectedItem.Selected = true;
                                        while ((selectedItem != null) &&
                                               (selectedItem.Parent.GetType() == typeof(RadPanelItem)))
                                        {
                                            selectedItem = (RadPanelItem)selectedItem.Parent;
                                            selectedItem.Expanded = true;
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }


        private void Controlvisible(bool flag)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                btExport.Visible = flag;
                btnRptView.Visible = flag;
                btnReset.Visible = flag;

                if (flag)
                {
                    string RptName = string.Empty;
                    XmlDocument doc = new XmlDocument();
                    var filePath = Server.MapPath(urlBase + filename);

                    XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                    var xmlReaderObject = xmlReader.XmlCleanReader();
                    if (xmlReaderObject != null)
                    {
                        doc.Load(xmlReaderObject);

                        XmlNodeList elemList = doc.GetElementsByTagName("Report");

                        for (int i = 0; i < elemList.Count; i++)
                        {
                            RptName = elemList[i].Attributes["displayname"].Value + "," + elemList[i].Attributes["name"].Value;

                            if (elemList[i].Attributes["preview"].Value != null)
                            {
                                if (elemList[i].Attributes["preview"].Value.ToUpper().Equals("TRUE"))
                                {
                                    btnRptView.Visible = true;
                                }
                                else if (elemList[i].Attributes["preview"].Value.ToUpper().Equals("FALSE"))
                                {
                                    btnRptView.Visible = false;
                                }
                            }
                            if ((elemList[i].Attributes["excel"].Value != null) && (elemList[i].Attributes["mhtml"].Value != null))
                            {

                                if ((elemList[i].Attributes["excel"].Value.ToUpper().Equals("FALSE")) && (elemList[i].Attributes["mhtml"].Value.ToUpper().Equals("FALSE")))
                                {
                                    btExport.Visible = false;
                                }
                                else
                                {

                                    if (elemList[i].Attributes["excel"].Value.ToUpper().Equals("FALSE"))
                                    {
                                        ddlFormat.Items.Remove(new ListItem("Excel"));
                                    }

                                    if (elemList[i].Attributes["mhtml"].Value.ToUpper().Equals("FALSE"))
                                    {
                                        ddlFormat.Items.Remove(new ListItem("MHTML"));
                                    }

                                }

                            }
                            //else
                            //{

                            //    if (elemList[i].Attributes["excel"].Value != null)
                            //    {
                            //        if (elemList[i].Attributes["excel"].Value.ToUpper().Equals("FALSE"))
                            //        {
                            //            ddlFormat.Items.Remove(new ListItem("Excel"));


                            //        }
                            //    }

                            //    if (elemList[i].Attributes["mhtml"].Value != null)
                            //    {
                            //        if (elemList[i].Attributes["mhtml"].Value.ToUpper().Equals("FALSE"))
                            //        {
                            //            ddlFormat.Items.Remove(new ListItem("MHTML"));

                            //        }
                            //    }
                            //}

                            //if (elemList[i].Attributes["mhtml"].Value != null)
                            //{
                            //    if (elemList[i].Attributes["mhtml"].Value.ToUpper().Equals("FALSE"))
                            //    {
                            //      //  ddlFormat.Items.Remove(new ListItem("MHTML"));                                  
                            //    }
                            //}


                            break;
                        }
                    }
                }
            }
        }

        private string GetReportTitle()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string RptName = string.Empty;
                XmlDocument doc = new XmlDocument();
                var filePath = Server.MapPath(urlBase + filename);

                XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                var xmlReaderObject = xmlReader.XmlCleanReader();
                if (xmlReaderObject != null)
                {
                    doc.Load(xmlReaderObject);

                    XmlNodeList elemList = doc.GetElementsByTagName("Report");

                    for (int i = 0; i < elemList.Count; i++)
                    {
                        RptName = elemList[i].Attributes["displayname"].Value + "," + elemList[i].Attributes["name"].Value;
                        break;
                    }

                    return RptName;
                }
                return "";
            }
        }

        private void DisableAuthorization()
        {
            Control tbdept = FindControlRecursive(Report, "DepartmentCD");
            Control tbAuth = FindControlRecursive(Report, "AuthorizationCD");

            if (tbdept != null && tbAuth != null)
            {
                ((TextBox)tbAuth).Enabled = false;
            }

        }

        private string ProcessResults()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                var filePath = Server.MapPath(urlBase + filename);
                XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                var xmlReaderObject = xmlReader.XmlCleanReader();
                if (xmlReaderObject != null)
                {
                    XPathDocument ReportFieldDoc = new XPathDocument(xmlReaderObject);
                    string Param = string.Empty;
                    XPathNodeIterator itr = ReportFieldDoc.CreateNavigator().Select("//searchfield");
                    StringBuilder sb = new System.Text.StringBuilder();

                    bool ddlmth = false;

                    XmlDocument doc = new XmlDocument();
                    XmlReaderResolver xmlReader2 = new XmlReaderResolver(filePath);
                    var xmlReaderObject2 = xmlReader.XmlCleanReader();
                    if (xmlReaderObject2 != null)
                    {
                        doc.Load(xmlReaderObject2);

                        while (itr.MoveNext())
                        {
                            string controlName = itr.Current.GetAttribute("name", "");
                            sb.Append(controlName);
                            sb.Append("=");

                            Control ctrlHolder = FindControlRecursive(Report, controlName);

                            if (ctrlHolder is TextBox)
                            {
                                Session[Session["TabSelect"] + ctrlHolder.ID] = ((TextBox)ctrlHolder).Text;
                                sb.Append(((TextBox)ctrlHolder).Text);
                            }

                            if (ctrlHolder is RadioButtonList)
                            {
                                if (((RadioButtonList)ctrlHolder).SelectedItem != null)
                                {
                                    Session[Session["TabSelect"] + ctrlHolder.ID] = ((RadioButtonList)ctrlHolder).SelectedItem.Value;
                                    sb.Append(((RadioButtonList)ctrlHolder).SelectedItem.Value);
                                }
                            }

                            if (ctrlHolder is CheckBox)
                            {
                                Session[Session["TabSelect"] + ctrlHolder.ID] = ((CheckBox)ctrlHolder).Checked;
                                if (((CheckBox)ctrlHolder).Checked == true)
                                {
                                    sb.Append("True");
                                }
                                else
                                {
                                    sb.Append("False");
                                }
                            }


                            if (ctrlHolder is HiddenField)
                            {
                                if (((HiddenField)ctrlHolder).Value != null)
                                {
                                    Session[Session["TabSelect"] + ctrlHolder.ID] = ((HiddenField)ctrlHolder).Value;
                                    sb.Append(((HiddenField)ctrlHolder).Value);
                                }
                            }

                            if (ctrlHolder is DropDownList)
                            {
                                if (ctrlHolder.ClientID.IndexOf("MonthFrom") <= 0)
                                {
                                    //DropDownList ddlmonth = (DropDownList)innerControl.FindControl("MonthFrom");
                                    DropDownList ddlmonth = (DropDownList)ctrlHolder;
                                    if (ddlmonth.SelectedItem.Value != "")
                                    {
                                        sb.Append(ddlmonth.SelectedItem.Text);
                                        Session[Session["TabSelect"] + ctrlHolder.ID + "ddlMonth"] = ddlmonth.SelectedItem.Value;
                                    }
                                }
                            }

                            if (ctrlHolder is UserControl)
                            {
                                if (ctrlHolder.ClientID.IndexOf("MonthFrom") <= 0)
                                {
                                    /*TextBox tbDateOfBirth = (TextBox)ctrlHolder.FindControl("tbDate");
                                    if (tbDateOfBirth != null)
                                    {
                                        if (tbDateOfBirth.Text.Trim().ToString() != string.Empty)
                                        {
                                            //sb.Append(DateTime.ParseExact(tbDateOfBirth.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture));
                                            sb.Append(Convert.ToDateTime(tbDateOfBirth.Text.Trim(), CultureInfo.InvariantCulture).ToShortDateString());
                                        }
                                    }*/

                                    TextBox tbDateOfBirth = (TextBox)ctrlHolder.FindControl("tbDate");
                                    if (DateFormat != null)
                                    {
                                        //if ((tbDateOfBirth != null) || (tbDateOfBirth.Text.Trim().ToString() != string.Empty))
                                        //{
                                        //    //sb.Append(DateTime.ParseExact(tbDateOfBirth.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture));
                                        //    sb.Append(String.Format("{0:" + DateFormat + "}", Convert.ToDateTime(tbDateOfBirth.Text.Trim())));
                                        //    sb.Append(Convert.ToDateTime(tbDateOfBirth.Text.Trim()));
                                        //}
                                        if (tbDateOfBirth != null)
                                        {
                                            if (tbDateOfBirth.Text.Trim().ToString() != "")
                                            {
                                                sb.Append(FormatDate(tbDateOfBirth.Text.Trim(), DateFormat));
                                                Session[Session["TabSelect"] + ctrlHolder.ID + "tbDate"] = tbDateOfBirth.Text.Trim();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (tbDateOfBirth != null)
                                        {
                                            if (tbDateOfBirth.Text.Trim().ToString() != "")
                                            {
                                                sb.Append(tbDateOfBirth.Text.Trim());
                                                Session[Session["TabSelect"] + ctrlHolder.ID + "tbDate"] = tbDateOfBirth.Text.Trim();
                                            }
                                        }
                                        //if ((tbDateOfBirth != null) || (tbDateOfBirth.Text.Trim().ToString() != string.Empty))
                                        //{
                                        //    //sb.Append(DateTime.ParseExact(tbDateOfBirth.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture));
                                        //    sb.Append(tbDateOfBirth.Text.Trim());
                                        //}
                                    }
                                    //this is to identify month and year
                                    XmlNodeList xnList = doc.SelectNodes("/Report/Controls/searchfield[@type='monthyear']");
                                    if (xnList.Count == 2) //Month and year
                                    {
                                        DropDownList ddlmonth = (DropDownList)ctrlHolder.FindControl("ddlMonth");
                                        if (ddlmonth != null && ddlmth == false)
                                        {
                                            Session[Session["TabSelect"] + ctrlHolder.ID + "ddlMonth"] = ddlmonth.SelectedItem.Value;
                                            sb.Append(ddlmonth.SelectedItem.Text);
                                            ddlmth = true;
                                            sb.Append(";");
                                            continue;
                                        }
                                        DropDownList ddlyear = (DropDownList)ctrlHolder.FindControl("ddlyear");
                                        if (ddlyear != null)
                                        {
                                            Session[Session["TabSelect"] + ctrlHolder.ID + "ddlyear"] = ddlyear.SelectedItem.Value;
                                            sb.Append(ddlyear.SelectedItem.Text);
                                        }
                                    }
                                    else if (xnList.Count == 1) //year
                                    {

                                        DropDownList ddlyear = (DropDownList)ctrlHolder.FindControl("ddlyear");
                                        if (ddlyear != null)
                                        {
                                            Session[Session["TabSelect"] + ctrlHolder.ID + "ddlyear"] = ddlyear.SelectedItem.Value;
                                            sb.Append(ddlyear.SelectedItem.Text);
                                        }
                                    }
                                    ///////////////////////////////////////////////////////////////////////////////////////////

                                }
                                else
                                {
                                    RadMonthYearPicker monthpicker = (RadMonthYearPicker)ctrlHolder.FindControl("tlkMonthView");
                                    if (!string.IsNullOrEmpty(monthpicker.SelectedDate.ToString().Trim()))
                                    {
                                        Session[Session["TabSelect"] + ctrlHolder.ID + "tlkMonthView"] = monthpicker.SelectedDate.ToString();
                                        sb.Append(monthpicker.SelectedDate.ToString());
                                    }

                                }

                            }
                            sb.Append(";");
                            Param = Convert.ToString(sb);

                            if (Param != string.Empty)
                            {

                                Param = Param.Substring(0, Param.Length - 1);
                            }
                            Param = Param + ";UserCD=" + UserPrincipal.Identity._name;
                        }

                        if (Param == string.Empty)
                            Param = "UserCD=" + UserPrincipal.Identity._name;

                        //Added byb Sudhakar
                        Param = Param + ";UserCustomerID=" + UserPrincipal.Identity._customerID;
                        Param = Param + ";UserHomebaseID=" + UserPrincipal.Identity._homeBaseId;
                        Param = Param + ";TenToMin=" + UserPrincipal.Identity._fpSettings._TimeDisplayTenMin;
                        Param = Param + ";ReportHeaderID=" + "10002175006";
                        Param = Param + ";TempSettings=" + "FUELDTLS::1||FUELVEND::1";

                        return Param;
                    }
                   
                }
                return "";
            }
        }

        private void ProcessColumns()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (filename != null)
                {
                    var filePath = Server.MapPath(urlBase + filename);
                    XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                    var xmlReaderObject = xmlReader.XmlCleanReader();
                    if (xmlReaderObject != null)
                    {
                        XPathDocument ReportFieldDoc = new XPathDocument(xmlReaderObject);
                        XPathNodeIterator itr = ReportFieldDoc.CreateNavigator().Select("//Column");

                        StringBuilder sb = new System.Text.StringBuilder();

                        while (itr.MoveNext())
                        {
                            dicColumnList.Add(itr.Current.GetAttribute("name", "").ToString(), itr.Current.GetAttribute("displayname", "").ToString());
                        }
                    }
                }
            }
        }

        private Control FindControlRecursive(Control rootControl, string controlID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(rootControl, controlID))
            {
                if (rootControl.ID == controlID) return rootControl;

                foreach (Control controlToSearch in rootControl.Controls)
                {
                    Control controlToReturn =
                        FindControlRecursive(controlToSearch, controlID);
                    if (controlToReturn != null) return controlToReturn;
                }
                return null;
            }
        }


        protected void OnFinish(object sender, System.Web.UI.WebControls.WizardNavigationEventArgs e)
        {
            //string fileName = String.Format("data-{0}.htm", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
            //Response.ContentType = "text/htm";
            //Response.AddHeader("content-disposition", "filename=" + fileName);
            //// write string data to Response.OutputStream here
            //Response.Write(ProcessHtmlParam());
            //Response.End();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Page.IsValid)
                        {
                            string[] strArr = GetReportTitle().Split(',');
                            if (strArr.Count() > 0)
                            {

                                Session["REPORT"] = Convert.ToString(strArr[1] + "mhtml");

                            }
                            Session["FORMAT"] = "MHTML";
                            Session["PARAMETER"] = ProcessHtmlExcelParam();
                            Response.Redirect("ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }

        protected void OnNext_old(object sender, WizardNavigationEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (UCRHS.RadListBoxDestination.Items.Count > 0)
                        {
                            lblError.Text = "";

                            if (Page.IsValid)
                            {  // 
                                if (Wizard1.ActiveStep.ID == "WizardStep5")
                                {
                                    switch (ddlFormat.SelectedValue)
                                    {
                                        case "MHTML":
                                            Wizard1.Visible = true;
                                            Wizard1.MoveTo(WizardStep1);

                                            break;
                                        case "Excel":
                                            string[] strArr = GetReportTitle().Split(',');
                                            if (strArr.Count() > 0)
                                            {
                                                Session["REPORT"] = Convert.ToString(strArr[1] + "Export");
                                            }
                                            Session["FORMAT"] = "EXCEL";
                                            Session["PARAMETER"] = ProcessResults();
                                            Response.Redirect("ReportRenderView.aspx");
                                            break;
                                        default:
                                            Wizard1.Visible = false;
                                            break;
                                    }
                                }

                                if (Wizard1.ActiveStep.ID == "WizardStep1")
                                {
                                    // UCRHSort.ColumnList = dicColumnList;

                                    foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                    {
                                        dicSelectedList.Add(SelectedItem.Value, SelectedItem.Text);
                                    }

                                    UCRHSort.RadListBoxSource.DataSource = dicSelectedList;
                                    UCRHSort.RadListBoxSource.DataValueField = "Key";
                                    UCRHSort.RadListBoxSource.DataTextField = "Value";
                                    UCRHSort.RadListBoxSource.DataBind();


                                    Dictionary<string, string> dicTempSelectedList = dicSelectedList;
                                    foreach (KeyValuePair<string, string> pair in dicTempSelectedList)
                                    {
                                        dicColumnList.Remove(pair.Key);
                                    }
                                    Dictionary<string, string> Empty = new Dictionary<string, string>();
                                    UCRHSort.RadListBoxDestination.DataSource = Empty;
                                    UCRHSort.RadListBoxDestination.DataBind();

                                    Session["dicSelectedList"] = dicSelectedList;



                                    //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                    //{
                                    //    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                    //}

                                    //SelectionDestination.DataSource = dicSelectedList;
                                    //SelectionDestination.DataValueField = "Key";
                                    //SelectionDestination.DataTextField = "Value";
                                    //SelectionDestination.DataBind();

                                    //SelectionSource.DataSource = dicSelectedList;
                                    //SelectionSource.DataValueField = "Key";
                                    //SelectionSource.DataTextField = "Value";
                                    //SelectionSource.DataBind();
                                }
                                if (Wizard1.ActiveStep.ID == "WizardStep2")
                                {
                                    foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                    {
                                        dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                    }


                                    foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxSource.Items)
                                    {
                                        dicSelectedListsort.Add(SelectedItem.Value, SelectedItem.Text);
                                    }

                                    foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxDestination.Items)
                                    {
                                        dicSelectedListsortSel.Add(SelectedItem.Value, SelectedItem.Text);
                                    }

                                    Session["dicSelectedListSort"] = dicSelectedListsort;

                                    Session["dicSelectedListSortSel"] = dicSelectedListsortSel;

                                    SelectedSource.DataSource = dicSelectedList;
                                    SelectedSource.DataValueField = "Key";
                                    SelectedSource.DataTextField = "Value";
                                    SelectedSource.DataBind();

                                    HeaderBySource.DataSource = dicSelectedList;
                                    HeaderBySource.DataValueField = "Key";
                                    HeaderBySource.DataTextField = "Value";
                                    HeaderBySource.DataBind();

                                }

                                if (Wizard1.ActiveStep.ID == "WizardStep4")
                                {
                                    txtTitle.Text = lbTitle.Text;

                                    // uncmd this code after
                                    FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                                    using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        FPPrincipal identityname = (FPPrincipal)Session[Session.SessionID];
                                        var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == identity.Identity._customerID && x.HomebaseID == identity.Identity._homeBaseId).ToList();

                                        if (objCompany[0].HTMLHeader != string.Empty)
                                        {
                                            chkHeader.Enabled = false;
                                        }
                                        if (objCompany[0].HTMLFooter != string.Empty)
                                        {
                                            chkFooter.Enabled = false;
                                        }
                                        if (objCompany[0].HTMLBackgroun != string.Empty)
                                        {
                                            chkBackground.Enabled = false;
                                        }
                                        string[] strArr = GetReportTitle().Split(',');
                                        if (strArr.Count() > 0)
                                        {
                                            txtTitle.Text = Convert.ToString(strArr[0]);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            lblError.Text = "Select the Fields";
                            e.Cancel = true;

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void OnNext(object sender, WizardNavigationEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (UCRHS.RadListBoxDestination.Items.Count > 0)
                        {
                            lblError.Text = "";

                            if (Page.IsValid)
                            {
                                if (Wizard1.ActiveStep.ID == "WizardStep1")
                                {
                                    ////Coded by srini
                                    dicSourceList = new Dictionary<string, string>();
                                    foreach (RadListBoxItem radlistitem in UCRHS.RadListBoxSource.Items)
                                    {
                                        dicSourceList.Add(radlistitem.Value, radlistitem.Text);
                                    }
                                    Session["SourceList"] = dicSourceList;

                                    dicDestinationList = new Dictionary<string, string>();
                                    foreach (RadListBoxItem radlistitem in UCRHS.RadListBoxDestination.Items)
                                    {
                                        dicDestinationList.Add(radlistitem.Value, radlistitem.Text);
                                    }
                                    Session["DestinationList"] = dicDestinationList;

                                    if (Session["SourceSortList"] != null)
                                    {
                                        dicSourceSortList = (Dictionary<string, string>)Session["SourceSortList"];
                                        UCRHSort.RadListBoxSource.DataSource = dicSourceSortList;
                                        UCRHSort.RadListBoxSource.DataValueField = "Key";
                                        UCRHSort.RadListBoxSource.DataTextField = "Value";
                                        UCRHSort.RadListBoxSource.DataBind();
                                    }
                                    else
                                    {
                                        UCRHSort.RadListBoxSource.DataSource = dicColumnList;
                                        UCRHSort.RadListBoxSource.DataValueField = "Key";
                                        UCRHSort.RadListBoxSource.DataTextField = "Value";
                                        UCRHSort.RadListBoxSource.DataBind();
                                    }

                                    if (Session["DestinationSortList"] != null)
                                    {
                                        dicSourceSortList = (Dictionary<string, string>)Session["DestinationSortList"];
                                        UCRHSort.RadListBoxDestination.DataSource = dicSourceSortList;
                                        UCRHSort.RadListBoxDestination.DataValueField = "Key";
                                        UCRHSort.RadListBoxDestination.DataTextField = "Value";
                                        UCRHSort.RadListBoxDestination.DataBind();
                                    }
                                    else
                                    {
                                        UCRHSort.RadListBoxDestination.DataSource = dicEmptyList;
                                        UCRHSort.RadListBoxDestination.DataValueField = "Key";
                                        UCRHSort.RadListBoxDestination.DataTextField = "Value";
                                        UCRHSort.RadListBoxDestination.DataBind();
                                    }
                                    ////Coded by srini



                                    // UCRHSort.ColumnList = dicColumnList;

                                    ////Commented by srini
                                    //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                    //{
                                    //    dicSelectedList.Add(SelectedItem.Value, SelectedItem.Text);
                                    //}

                                    //UCRHSort.RadListBoxSource.DataSource = dicSelectedList;
                                    //UCRHSort.RadListBoxSource.DataValueField = "Key";
                                    //UCRHSort.RadListBoxSource.DataTextField = "Value";
                                    //UCRHSort.RadListBoxSource.DataBind();


                                    //Dictionary<string, string> dicTempSelectedList = dicSelectedList;
                                    //foreach (KeyValuePair<string, string> pair in dicTempSelectedList)
                                    //{
                                    //    dicColumnList.Remove(pair.Key);
                                    //}
                                    //Dictionary<string, string> Empty = new Dictionary<string, string>();
                                    //UCRHSort.RadListBoxDestination.DataSource = Empty;
                                    //UCRHSort.RadListBoxDestination.DataBind();

                                    //Session["dicSelectedList"] = dicSelectedList;
                                    ////Commented by srini


                                    //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                    //{
                                    //    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                    //}

                                    //SelectionDestination.DataSource = dicSelectedList;
                                    //SelectionDestination.DataValueField = "Key";
                                    //SelectionDestination.DataTextField = "Value";
                                    //SelectionDestination.DataBind();

                                    //SelectionSource.DataSource = dicSelectedList;
                                    //SelectionSource.DataValueField = "Key";
                                    //SelectionSource.DataTextField = "Value";
                                    //SelectionSource.DataBind();
                                }

                                if (Wizard1.ActiveStep.ID == "WizardStep2")
                                {
                                    ////Coded by srini
                                    tbEditHeaderText.Text = string.Empty;
                                    dicSourceSortList = new Dictionary<string, string>();
                                    foreach (RadListBoxItem radlistitem in UCRHSort.RadListBoxSource.Items)
                                    {
                                        dicSourceSortList.Add(radlistitem.Value, radlistitem.Text);
                                    }
                                    Session["SourceSortList"] = dicSourceSortList;
                                    dicDestinationSortList = new Dictionary<string, string>();
                                    foreach (RadListBoxItem radlistitem in UCRHSort.RadListBoxDestination.Items)
                                    {
                                        dicDestinationSortList.Add(radlistitem.Value, radlistitem.Text);
                                    }
                                    Session["DestinationSortList"] = dicDestinationSortList;

                                    if (Session["DestinationList"] != null)
                                    {
                                        dicSourceList = (Dictionary<string, string>)Session["DestinationList"];
                                        SelectedSource.DataSource = dicSourceList;
                                        HeaderBySource.DataSource = dicSourceList;
                                    }
                                    else
                                    {
                                        SelectedSource.DataSource = dicEmptyList;
                                        HeaderBySource.DataSource = dicEmptyList;
                                    }
                                    SelectedSource.DataValueField = "Key";
                                    SelectedSource.DataTextField = "Value";
                                    SelectedSource.DataBind();
                                    HeaderBySource.DataValueField = "Key";
                                    HeaderBySource.DataTextField = "Value";
                                    HeaderBySource.DataBind();
                                    ////Coded by srini

                                    ////Commented by srini
                                    //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                    //{
                                    //    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                    //}


                                    //foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxSource.Items)
                                    //{
                                    //    dicSelectedListsort.Add(SelectedItem.Value, SelectedItem.Text);
                                    //}

                                    //foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxDestination.Items)
                                    //{
                                    //    dicSelectedListsortSel.Add(SelectedItem.Value, SelectedItem.Text);
                                    //}

                                    //Session["dicSelectedListSort"] = dicSelectedListsort;

                                    //Session["dicSelectedListSortSel"] = dicSelectedListsortSel;

                                    //SelectedSource.DataSource = dicSelectedList;
                                    //SelectedSource.DataValueField = "Key";
                                    //SelectedSource.DataTextField = "Value";
                                    //SelectedSource.DataBind();

                                    //HeaderBySource.DataSource = dicSelectedList;
                                    //HeaderBySource.DataValueField = "Key";
                                    //HeaderBySource.DataTextField = "Value";
                                    //HeaderBySource.DataBind();
                                    ////Commented by srini
                                }

                                if (Wizard1.ActiveStep.ID == "WizardStep4")
                                {
                                    txtTitle.Text = lbTitle.Text;

                                    // uncmd this code after
                                    FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                                    using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        FPPrincipal identityname = (FPPrincipal)Session[Session.SessionID];
                                        var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == identity.Identity._customerID && x.HomebaseID == identity.Identity._homeBaseId).ToList();

                                        if (objCompany[0].HTMLHeader != string.Empty)
                                        {
                                            chkHeader.Enabled = false;
                                        }
                                        if (objCompany[0].HTMLFooter != string.Empty)
                                        {
                                            chkFooter.Enabled = false;
                                        }
                                        if (objCompany[0].HTMLBackgroun != string.Empty)
                                        {
                                            chkBackground.Enabled = false;
                                        }
                                        string[] strArr = GetReportTitle().Split(',');
                                        if (strArr.Count() > 0)
                                        {
                                            txtTitle.Text = Convert.ToString(strArr[0]);
                                        }
                                    }
                                }

                                if (Wizard1.ActiveStep.ID == "WizardStep5")
                                {
                                    switch (ddlFormat.SelectedValue)
                                    {
                                        case "MHTML":
                                            Wizard1.Visible = true;
                                            Wizard1.MoveTo(WizardStep1);

                                            break;
                                        case "Excel":
                                            string[] strArr = GetReportTitle().Split(',');
                                            if (strArr.Count() > 0)
                                            {
                                                Session["REPORT"] = Convert.ToString(strArr[1] + "Export");
                                            }
                                            Session["FORMAT"] = "EXCEL";
                                            Session["PARAMETER"] = ProcessResults();
                                            Response.Redirect("ReportRenderView.aspx");
                                            break;
                                        default:
                                            Wizard1.Visible = false;
                                            break;
                                    }
                                }





                            }
                        }
                        else
                        {
                            lblError.Text = "Select the Fields";
                            e.Cancel = true;

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void OnPrevious(object sender, WizardNavigationEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (UCRHS.RadListBoxDestination.Items.Count > 0)
                        {
                            lblError.Text = "";

                            //if (Page.IsValid)
                            //{  // 
                            if (Wizard1.ActiveStep.ID == "WizardStep5")
                            {
                                switch (ddlFormat.SelectedValue)
                                {
                                    case "MHTML":
                                        Wizard1.Visible = true;
                                        Wizard1.MoveTo(WizardStep1);

                                        break;
                                    case "Excel":
                                        string[] strArr = GetReportTitle().Split(',');
                                        if (strArr.Count() > 0)
                                        {
                                            Session["REPORT"] = Convert.ToString(strArr[1] + "Export");
                                        }
                                        Session["FORMAT"] = "EXCEL";
                                        Session["PARAMETER"] = ProcessResults();
                                        Response.Redirect("ReportRenderView.aspx");
                                        break;
                                    default:
                                        Wizard1.Visible = false;
                                        break;
                                }
                            }

                            if (Wizard1.ActiveStep.ID == "WizardStep4")
                            {
                                ////code commented by srini
                                //txtTitle.Text = lbTitle.Text;

                                //// uncmd this code after
                                //FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                                //using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                                //{
                                //    FPPrincipal identityname = (FPPrincipal)Session[Session.SessionID];
                                //    var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == identity.Identity._customerID && x.HomebaseID == identity.Identity._homeBaseId).ToList();

                                //    if (objCompany[0].HTMLHeader != string.Empty)
                                //    {
                                //        chkHeader.Enabled = false;
                                //    }
                                //    if (objCompany[0].HTMLFooter != string.Empty)
                                //    {
                                //        chkFooter.Enabled = false;
                                //    }
                                //    if (objCompany[0].HTMLBackgroun != string.Empty)
                                //    {
                                //        chkBackground.Enabled = false;
                                //    }
                                //    string[] strArr = GetReportTitle().Split(',');
                                //    if (strArr.Count() > 0)
                                //    {
                                //        txtTitle.Text = Convert.ToString(strArr[0]);
                                //    }
                                //}
                                ////code commented by srini

                                ////Coded by srini
                                if (Session["SourceSortList"] != null)
                                {
                                    dicSourceSortList = (Dictionary<string, string>)Session["SourceSortList"];
                                    UCRHSort.RadListBoxSource.DataSource = dicSourceSortList;
                                    UCRHSort.RadListBoxSource.DataValueField = "Key";
                                    UCRHSort.RadListBoxSource.DataTextField = "Value";
                                    UCRHSort.RadListBoxSource.DataBind();
                                }
                                else
                                {
                                    UCRHSort.RadListBoxSource.DataSource = dicColumnList;
                                    UCRHSort.RadListBoxSource.DataValueField = "Key";
                                    UCRHSort.RadListBoxSource.DataTextField = "Value";
                                    UCRHSort.RadListBoxSource.DataBind();
                                }
                                if (Session["DestinationSortList"] != null)
                                {
                                    dicDestinationSortList = (Dictionary<string, string>)Session["DestinationSortList"];
                                    UCRHSort.RadListBoxDestination.DataSource = dicDestinationSortList;
                                    UCRHSort.RadListBoxDestination.DataValueField = "Key";
                                    UCRHSort.RadListBoxDestination.DataTextField = "Value";
                                    UCRHSort.RadListBoxDestination.DataBind();
                                }
                                else
                                {
                                    UCRHSort.RadListBoxDestination.DataSource = dicColumnList;
                                    UCRHSort.RadListBoxDestination.DataValueField = "Key";
                                    UCRHSort.RadListBoxDestination.DataTextField = "Value";
                                    UCRHSort.RadListBoxDestination.DataBind();
                                }
                                ////Coded by srini
                            }

                            if (Wizard1.ActiveStep.ID == "WizardStep2")
                            {
                                //////code commented by srini
                                //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                //{
                                //    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                //}


                                //foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxSource.Items)
                                //{
                                //    dicSelectedListsort.Add(SelectedItem.Value, SelectedItem.Text);
                                //}

                                //foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxDestination.Items)
                                //{
                                //    dicSelectedListsortSel.Add(SelectedItem.Value, SelectedItem.Text);
                                //}

                                //Session["dicSelectedListSort"] = dicSelectedListsort;

                                //Session["dicSelectedListSortSel"] = dicSelectedListsortSel;

                                //SelectedSource.DataSource = dicSelectedList;
                                //SelectedSource.DataValueField = "Key";
                                //SelectedSource.DataTextField = "Value";
                                //SelectedSource.DataBind();

                                //HeaderBySource.DataSource = dicSelectedList;
                                //HeaderBySource.DataValueField = "Key";
                                //HeaderBySource.DataTextField = "Value";
                                //HeaderBySource.DataBind();
                                //////code commented by srini

                                ////Coded by srini
                                if (Session["SourceList"] != null)
                                {
                                    dicSourceList = (Dictionary<string, string>)Session["SourceList"];
                                    UCRHS.RadListBoxSource.DataSource = dicSourceList;
                                    UCRHS.RadListBoxSource.DataValueField = "Key";
                                    UCRHS.RadListBoxSource.DataTextField = "Value";
                                    UCRHS.RadListBoxSource.DataBind();
                                }
                                else
                                {
                                    UCRHS.RadListBoxSource.DataSource = dicColumnList;
                                    UCRHS.RadListBoxSource.DataValueField = "Key";
                                    UCRHS.RadListBoxSource.DataTextField = "Value";
                                    UCRHS.RadListBoxSource.DataBind();
                                }
                                if (Session["DestinationList"] != null)
                                {
                                    dicDestinationList = (Dictionary<string, string>)Session["DestinationList"];
                                    UCRHS.RadListBoxDestination.DataSource = dicDestinationList;
                                    UCRHS.RadListBoxDestination.DataValueField = "Key";
                                    UCRHS.RadListBoxDestination.DataTextField = "Value";
                                    UCRHS.RadListBoxDestination.DataBind();
                                }
                                else
                                {
                                    UCRHS.RadListBoxDestination.DataSource = dicColumnList;
                                    UCRHS.RadListBoxDestination.DataValueField = "Key";
                                    UCRHS.RadListBoxDestination.DataTextField = "Value";
                                    UCRHS.RadListBoxDestination.DataBind();
                                }
                                ////Coded by srini
                            }

                            if (Wizard1.ActiveStep.ID == "WizardStep1")
                            {
                                // UCRHSort.ColumnList = dicColumnList;

                                foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                {
                                    dicSelectedList.Add(SelectedItem.Value, SelectedItem.Text);
                                }

                                UCRHSort.RadListBoxSource.DataSource = dicSelectedList;
                                UCRHSort.RadListBoxSource.DataValueField = "Key";
                                UCRHSort.RadListBoxSource.DataTextField = "Value";
                                UCRHSort.RadListBoxSource.DataBind();


                                Dictionary<string, string> dicTempSelectedList = dicSelectedList;
                                foreach (KeyValuePair<string, string> pair in dicTempSelectedList)
                                {
                                    dicColumnList.Remove(pair.Key);
                                }
                                Dictionary<string, string> Empty = new Dictionary<string, string>();
                                UCRHSort.RadListBoxDestination.DataSource = Empty;
                                UCRHSort.RadListBoxDestination.DataBind();

                                Session["dicSelectedList"] = dicSelectedList;



                                //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                //{
                                //    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                //}

                                //SelectionDestination.DataSource = dicSelectedList;
                                //SelectionDestination.DataValueField = "Key";
                                //SelectionDestination.DataTextField = "Value";
                                //SelectionDestination.DataBind();

                                //SelectionSource.DataSource = dicSelectedList;
                                //SelectionSource.DataValueField = "Key";
                                //SelectionSource.DataTextField = "Value";
                                //SelectionSource.DataBind();
                            }



                            //}
                        }
                        else
                        {
                            lblError.Text = "Select the Fields";
                            e.Cancel = true;

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        private string ProcessHtmlExcelParam()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string PageFGcolor = ColorTranslator.ToHtml(ForegroundColor.SelectedColor);
                string PageBGColor = ColorTranslator.ToHtml(BackgroundColor.SelectedColor);

                string TableBorderColors = ColorTranslator.ToHtml(TableBorderColor.SelectedColor);
                string TableFGColor = ColorTranslator.ToHtml(TableForegroundColor.SelectedColor);
                string TableBGColor = ColorTranslator.ToHtml(TableBackgroundColor.SelectedColor);
                StringBuilder strBuildparm = new StringBuilder();

                string Dbcolname = string.Empty;
                string AliesColname = string.Empty;
                string SortColname = string.Empty;


                switch (ddlFormat.SelectedValue)
                {

                    case "MHTML":

                        foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                        {
                            Dbcolname = Dbcolname + SelectedItem.Value + ",";
                        }
                        if (Dbcolname != string.Empty)
                        {
                            Dbcolname = Dbcolname.Substring(0, Dbcolname.Length - 1);
                        }
                        foreach (RadListBoxItem SelectedItem in HeaderBySource.Items)
                        {
                            AliesColname = AliesColname + SelectedItem.Text + "|";
                        }
                        AliesColname = AliesColname.Substring(0, AliesColname.Length - 1);

                        foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxDestination.Items)
                        {
                            SortColname = SortColname + SelectedItem.Value + ",";
                        }
                        if (SortColname != string.Empty)
                        {
                            SortColname = SortColname.Substring(0, SortColname.Length - 1);
                        }

                        strBuildparm.Append(ProcessResults());
                        strBuildparm.Append(";SelectedCols=" + Dbcolname);
                        strBuildparm.Append(";DisplayCols=" + AliesColname);
                        strBuildparm.Append(";SortCols=" + SortColname);
                        strBuildparm.Append(";Color=" + PageBGColor + "," + PageFGcolor + "," + TableFGColor + "," + TableBorderColors + "," + TableBGColor);
                        strBuildparm.Append(";ReportTitle=" + txtTitle.Text);

                        FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                        using (FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            FPPrincipal identityname = (FPPrincipal)Session[Session.SessionID];
                            var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == identity.Identity._customerID && x.HomebaseID == identity.Identity._homeBaseId).ToList();


                            if (chkHeader.Checked)
                            {
                                strBuildparm.Append(";HtmlHeader=" + objCompany[0].HTMLHeader);
                            }
                            if (chkFooter.Checked)
                            {
                                strBuildparm.Append(";HtmlFooter=" + objCompany[0].HTMLFooter);
                            }
                            if (chkBackground.Checked)
                            {
                                strBuildparm.Append(";HtmlBG=" + objCompany[0].HTMLBackgroun);
                            }
                        }
                        break;

                    case "Excel":
                        //    //for Excel file  Reading from RadListBoxSource
                        //   foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxSource.Items)
                        //    {
                        //        Dbcolname = Dbcolname + SelectedItem.Value + ",";
                        //    }
                        //    if (Dbcolname != string.Empty)
                        //    {
                        //        Dbcolname = Dbcolname.Substring(0, Dbcolname.Length - 1);
                        //    }
                        //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxSource.Items)
                        //    {
                        //        AliesColname=AliesColname+SelectedItem.Text+"|";
                        //    }
                        //if (AliesColname != string.Empty)
                        //{
                        //    AliesColname = AliesColname.Substring(0, AliesColname.Length - 1);
                        //}


                        strBuildparm.Append(ProcessResults());
                        //   strBuildparm.Append(";SelectedCols="+Dbcolname);
                        //    strBuildparm.Append(";DisplayCols="+AliesColname);
                        //strBuildparm.Append(";Color=," + PageFGcolor + "," + TableFGColor + "," + TableBorderColors + "," + TableBGColor); 

                        break;

                }

                return strBuildparm.ToString();

                //FlightPak.Web.ReportRenderService.ReportRenderClient RptClient = new ReportRenderService.ReportRenderClient();
                //DataSet dsRpt = RptClient.GetReportHTMLTable(Dbcolname, AliesColname, GetSPName(filename), ProcessResults());
                //  return GetDataTableAsHTML(dsRpt.Tables[0], txtTitle.Text, PageFGcolor, PageBGColor, TableBorderColor, TableFGColor, TableBGColor);
            }

        }

        private string GetDataTableAsHTML(DataTable thisTable, string Title, string PageFGcolor, string PageBGColor, string TableBorderColor, string TableFGColor, string TableBGColor)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(thisTable, Title, PageFGcolor, PageBGColor, TableBorderColor, TableFGColor, TableBGColor))
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                sb.Append("<HTML><head><title> " + Title + "</title></head>");
                sb.Append("<body style='color: " + PageFGcolor + "' bgcolor = '" + PageBGColor + "'>");
                sb.Append(" <font size=+5><b>");
                sb.Append("<center>");
                sb.Append(txtTitle.Text);
                sb.Append("</center></b>");
                sb.Append(" </center></b></font>");
                sb.Append("<TABLE id='GenTable' align = 'center' border=2 bordercolor='" + TableBorderColor + "' style='color: " + TableFGColor + "' bgcolor = '" + TableBGColor + "'>");
                sb.Append("<TR ALIGN='CENTER'>");
                //first append the column names.
                foreach (DataColumn column in thisTable.Columns)
                {
                    sb.Append("<th align ='right'>");
                    sb.Append(column.ColumnName);
                    sb.Append("</th>");
                }
                sb.Append("</TR>");
                // next, the column values.
                foreach (DataRow row in thisTable.Rows)
                {
                    sb.Append("<TR ALIGN='right'>");

                    foreach (DataColumn column in thisTable.Columns)
                    {
                        sb.Append("<TD>");
                        if (row[column].ToString().Trim().Length > 0)
                            sb.Append(row[column]);
                        else
                            sb.Append(" ");
                        sb.Append("</TD>");
                    }
                    sb.Append("</TR>");
                }
                sb.Append("</TABLE></body></HTML>");
                return sb.ToString();
            }
        }



        private string GetSPName(string xmlFileName)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(xmlFileName))
            {
                string attrVal = string.Empty;
                string SPNameval = string.Empty;
                XmlDocument doc = new XmlDocument();

                string TabSelect = Convert.ToString(Session["TabSelect"]);
                pnlReportTab.Items.Clear();
                switch (TabSelect)
                {
                    case "PreFlight":
                        doc.Load(Server.MapPath(urlBase + "PreFlightMainTab.xml"));
                        break;
                    case "PostFlight":
                        doc.Load(Server.MapPath(urlBase + "PostFlightMainTab.xml"));
                        break;
                    case "Db":
                        doc.Load(Server.MapPath(urlBase + "DatabaseMainTab.xml"));
                        break;
                    case "CorpReq":
                        doc.Load(Server.MapPath(urlBase + "CorporateFlightMainTab.xml"));
                        break;
                    case "Charter":
                        doc.Load(Server.MapPath(urlBase + "CharterMainTab.xml"));
                        break;
                }



                XmlNodeList xnList = doc.SelectNodes("/catagorys/catagory/screen[@url='ReportViewer.aspx?xmlFilename=" + xmlFileName + "']");
                if (xnList.Count > 0)
                {
                    foreach (XmlNode child in xnList)
                    {
                        SPNameval = child.Attributes["SPName"].Value;
                        break;

                    }
                }
                return SPNameval;
            }
        }



        protected void LoadTabs(RadPanelBar Rpb, string XmlFilePath)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Rpb, XmlFilePath))
            {
                string attrVal = "";
                XmlDocument doc = new XmlDocument();
                doc.Load(Server.MapPath(XmlFilePath));

                XmlNodeList elemList = doc.GetElementsByTagName("catagory");
                for (int i = 0; i < elemList.Count; i++)
                {
                    attrVal = attrVal + elemList[i].Attributes["text"].Value + ",";
                }
                string[] strval = attrVal.Split(',');
                foreach (string str in strval)
                {
                    Telerik.Web.UI.RadPanelItem rpi = new Telerik.Web.UI.RadPanelItem(str);
                    XmlNodeList xnList = doc.SelectNodes("/catagorys/catagory[@text='" + str + "']/screen[@name!='']");

                    //if(str == "Administration")
                    //rpi.Visible = UserPrincipal.Identity._isSysAdmin;

                    if (xnList.Count > 0)
                    {
                        int ival = 0;
                        foreach (XmlNode child in xnList)
                        {

                            //Added UMPermissionRole for implementing security 
                            if (IsAuthorized("View" + child.Attributes["UMPermissionRole"].Value))
                            {

                                rpi.Items.Add(new Telerik.Web.UI.RadPanelItem(child.Attributes["name"].Value, child.Attributes["url"].Value));
                                rpi.Font.Bold = true;
                                rpi.Items[ival].Enabled = (child.Attributes["url"].Value == string.Empty ? false : true);
                                if (!rpi.Enabled)
                                {
                                    rpi.Items[ival].ForeColor = Color.Gray;
                                }
                                ival++;
                            }
                            else if (str == "Administration" && UserPrincipal.Identity._isSysAdmin)
                            {
                                rpi.Items.Add(new Telerik.Web.UI.RadPanelItem(child.Attributes["name"].Value, child.Attributes["url"].Value));
                                rpi.Font.Bold = true;
                                rpi.Items[ival].Enabled = (child.Attributes["url"].Value == string.Empty ? false : true);
                                if (!rpi.Enabled)
                                {
                                    rpi.Items[ival].ForeColor = Color.Gray;
                                }
                                ival++;
                            }

                        }
                        Rpb.Items.Add(rpi);
                    }
                    else if (str != "")
                    {
                        XmlNodeList elemListval = doc.GetElementsByTagName("catagory");
                        for (int ival = 0; ival < elemListval.Count; ival++)
                        {
                            Telerik.Web.UI.RadPanelItem rpia = new Telerik.Web.UI.RadPanelItem(elemListval[ival].Attributes["text"].Value, elemListval[ival].Attributes["url"].Value);
                            Rpb.Items.Add(rpia);
                        }
                        break;

                    }
                }
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            Wizard1.Visible = true;
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }

        protected void btnExportFileFormat_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Page.IsValid)
                        {
                            switch (ddlFormat.SelectedValue)
                            {
                                case "MHTML":

                                    if (Request.QueryString["xmlFilename"] != null)
                                    {
                                        string filename = Request.QueryString["xmlFilename"].ToLower();
                                        if (filename == "postdelaydetail.xml" || filename == "postpassengerflightsummary.xml" || filename == "POSTBudgetActualVarianceReport.xml")
                                        {
                                            Session["IsTempFile"] = "1";
                                        }
                                        else
                                        {
                                            Session["IsTempFile"] = null;
                                        }
                                    }

                                    Wizard1.Visible = true;
                                    Wizard1.MoveTo(WizardStep1);
                                    break;
                                case "Excel":
                                    string[] strArr = GetReportTitle().Split(',');
                                    if (strArr.Count() > 0)
                                    {
                                        Session["REPORT"] = Convert.ToString(strArr[1] + "Export");
                                    }
                                    Session["FORMAT"] = "EXCEL";
                                    Session["PARAMETER"] = ProcessHtmlExcelParam();
                                    // Response.Redirect("ReportRenderView.aspx");
                                    break;
                                default:

                                    break;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }


        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool Isvalid = true;
                        var filePath = Server.MapPath(urlBase + filename);
                        XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                        var xmlReaderObject = xmlReader.XmlCleanReader();
                        if (xmlReaderObject != null)
                        {
                            XPathDocument ReportFieldDoc = new XPathDocument(xmlReaderObject);
                            string Param = string.Empty;
                            XPathNodeIterator itr = ReportFieldDoc.CreateNavigator().Select("//searchfield");
                            StringBuilder sb = new System.Text.StringBuilder();

                            //XmlDocument doc = new XmlDocument();
                            //doc.Load(Server.MapPath(urlBase + filename));

                            while (itr.MoveNext())
                            {
                                string controlName = itr.Current.GetAttribute("name", "");
                                sb.Append(controlName);
                                sb.Append("=");

                                Control ctrlHolder = FindControlRecursive(Report, "lbcv" + controlName);
                                if (ctrlHolder != null)
                                {
                                    if (ctrlHolder is Label)
                                    {
                                        if (!string.IsNullOrEmpty(((Label)ctrlHolder).Text))
                                            Isvalid = false;
                                    }
                                }
                            }

                            if (Isvalid)
                            {
                                if (Page.IsValid)
                                {
                                    ExportPanal.Visible = true;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }
        protected void btnCostFileFormat_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ExportPanal.Visible = false;
                        Wizard1.Visible = false;
                        lblError.Text = "";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }

        protected void btnPre_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "PreFlight";
                        pnlReportTab.Items.Clear();
                        Session.Remove("PageLoad");
                        //LoadTabs(pnlReportTab, urlBase + "PreFlightMainTab.xml");
                        Response.Redirect("ReportViewer.aspx?xmlFilename=PREAircraftCalendarI.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnPost_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "PostFlight";
                        pnlReportTab.Items.Clear();
                        // LoadTabs(pnlReportTab, urlBase + "PostFlightMainTab.xml");
                        Session.Remove("PageLoad");
                        Response.Redirect("ReportViewer.aspx?xmlFilename=POSTAircraftArrivals.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnCharter_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "Charter";
                        pnlReportTab.Items.Clear();
                        // LoadTabs(pnlReportTab, urlBase + "CharterMainTab.xml");
                        Session.Remove("PageLoad");
                        //Response.Redirect("CQReportViewer.aspx?xmlFilename=CQAircraftProfile.xml");
                        Response.Redirect("ReportViewer.aspx?xmlFilename=CQAircraftProfile.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnCorpReq_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "CorpReq";
                        pnlReportTab.Items.Clear();
                        Session.Remove("PageLoad");
                        //LoadTabs(pnlReportTab, urlBase + "CorporateFlightMainTab.xml");
                        Response.Redirect("ReportViewer.aspx?xmlFilename=CRApprovalAuditSummary.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }

        protected void btnDb_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "Db";
                        pnlReportTab.Items.Clear();
                        LoadTabs(pnlReportTab, urlBase + "DatabaseMainTab.xml");
                        Session.Remove("PageLoad");
                        Response.Redirect("ReportViewer.aspx?xmlFilename=DBAccounts.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }


        protected void btnRptView_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool Isvalid = true;
                         var filePath = Server.MapPath(urlBase + filename);
                        XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                        var xmlReaderObject = xmlReader.XmlCleanReader();
                        if (xmlReaderObject != null)
                        {
                            XPathDocument ReportFieldDoc = new XPathDocument(xmlReaderObject);
                            string Param = string.Empty;
                            XPathNodeIterator itr = ReportFieldDoc.CreateNavigator().Select("//searchfield");
                            StringBuilder sb = new System.Text.StringBuilder();

                            //XmlDocument doc = new XmlDocument();
                            //doc.Load(Server.MapPath(urlBase + filename));

                            while (itr.MoveNext())
                            {
                                string controlName = itr.Current.GetAttribute("name", "");
                                sb.Append(controlName);
                                sb.Append("=");

                                Control ctrlHolder = FindControlRecursive(Report, "lbcv" + controlName);
                                if (ctrlHolder != null)
                                {
                                    if (ctrlHolder is Label)
                                    {
                                        if (!string.IsNullOrEmpty(((Label)ctrlHolder).Text))
                                            Isvalid = false;
                                    }
                                }
                            }

                            if (Isvalid)
                            {
                                if (Page.IsValid)
                                {
                                    string[] strArr = GetReportTitle().Split(',');
                                    if (strArr.Count() > 0)
                                    {
                                        Session["REPORT"] = Convert.ToString(strArr[1]);
                                    }
                                    Session["FORMAT"] = "PDF";
                                    Session["PARAMETER"] = ProcessResults();
                                    //Response.Redirect("ReportRenderView.aspx");
                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "key", " openTab();", true);

                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "openTab", "openTab()", true);

                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }


        private void ResetResults()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                var filePath = Server.MapPath(urlBase + filename);
                XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                var xmlReaderObject = xmlReader.XmlCleanReader();
                if (xmlReaderObject != null)
                {
                    XPathDocument ReportFieldDoc = new XPathDocument(xmlReaderObject);
                    string Param = string.Empty;
                    XPathNodeIterator itr = ReportFieldDoc.CreateNavigator().Select("//searchfield");
                    StringBuilder sb = new System.Text.StringBuilder();
                    bool ddlmth = false;
                    //XmlDocument doc = new XmlDocument();
                    //doc.Load(Server.MapPath(urlBase + filename));
                    while (itr.MoveNext())
                    {
                        string controlName = itr.Current.GetAttribute("name", "");
                        sb.Append(controlName);
                        sb.Append("=");

                        Control ctrlHolder = FindControlRecursive(Report, controlName);

                        Control CVControl = FindControlRecursive(Report, "lbcv" + controlName);

                        if (CVControl != null && CVControl is Label)
                        {
                            ((Label)CVControl).Text = string.Empty;
                        }

                        if (ctrlHolder is Label)
                        {
                            ((Label)ctrlHolder).Text = string.Empty;
                        }

                        if (ctrlHolder is RequiredFieldValidator)
                        {
                            ((RequiredFieldValidator)ctrlHolder).IsValid = false;
                            ((RequiredFieldValidator)ctrlHolder).Text = string.Empty;
                        }

                        if (ctrlHolder is TextBox)
                        {
                            ((TextBox)ctrlHolder).Text = string.Empty;
                        }

                        if (ctrlHolder is RadioButtonList)
                        {
                            if (((RadioButtonList)ctrlHolder).SelectedItem != null)
                            {
                                sb.Append(((RadioButtonList)ctrlHolder).SelectedItem.Value);
                            }
                        }

                        if (ctrlHolder is CheckBox)
                        {
                            ((CheckBox)ctrlHolder).Checked = false;

                        }

                        if (ctrlHolder is UserControl)
                        {
                            if (ctrlHolder.ClientID.IndexOf("MonthFrom") <= 0)
                            {
                                TextBox tbDateOfBirth = (TextBox)ctrlHolder.FindControl("tbDate");
                                if (tbDateOfBirth != null)
                                {
                                    tbDateOfBirth.Text = string.Empty;
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ResetResults();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }

        protected void HeaderBySource_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbEditHeaderText.Text = HeaderBySource.SelectedItem.Text;
                        hdnHeaderSourceID.Value = HeaderBySource.SelectedIndex.ToString();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void Ctrl_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (sender is TextBox)
                        {
                            Control ctrlHolder = FindControlRecursive(Report, "lbcv" + ((TextBox)sender).ID);
                            if (ctrlHolder != null)
                            {
                                if (ctrlHolder is Label)
                                {
                                    ((Label)ctrlHolder).Text = string.Empty;
                                }
                            }

                            #region "TailNum"
                            if (((TextBox)sender).ID.ToLower() == "tailnum")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string tailnos = ((TextBox)sender).Text.Trim();
                                    string[] TailNo = tailnos.Split(',');

                                    List<FlightPak.Web.FlightPakMasterService.Fleet> FleetList = new List<FlightPakMasterService.Fleet>();
                                    for (int i = 0; i < TailNo.Length; i++)
                                    {
                                        string Tail = TailNo[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetFleetProfileList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                FleetList = objRetVal.EntityList.Where(x => x.TailNum.ToUpper().Trim() == Tail.ToUpper()).ToList();
                                                if (FleetList != null && FleetList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "AccountNum"

                            if (((TextBox)sender).ID.ToLower() == "accountnum")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    List<FlightPak.Web.FlightPakMasterService.GetAccounts> AccountList = new List<FlightPakMasterService.GetAccounts>();
                                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var objRetVal = objDstsvc.GetAllAccountList();
                                        if (objRetVal.ReturnFlag)
                                        {
                                            AccountList = objRetVal.EntityList.Where(x => x.AccountNum.Trim() == ((TextBox)sender).Text).ToList();
                                            if (AccountList != null && AccountList.Count > 0)
                                            {

                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                        else
                                        {
                                            IsNotExist = true;
                                        }
                                    }
                                }
                            }

                            //Ramesh - Added for enhancement 1178 to allow multiple selection in Accounts lookup in Postflight Expense report.
                            if (((TextBox)sender).ID.ToLower() == "multipleaccountnum")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string accountList = ((TextBox)sender).Text.Trim();
                                    string[] accountNum = accountList.Split(',');
                                    for (int i = 0; i < accountNum.Length; i++)
                                    {
                                        string account = accountNum[i];

                                        List<FlightPak.Web.FlightPakMasterService.GetAccounts> AccountList = new List<FlightPakMasterService.GetAccounts>();
                                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objDstsvc.GetAllAccountList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                // AccountList = objRetVal.EntityList.Where(x => x.AccountNum.Trim() == ((TextBox)sender).Text).ToList();
                                                AccountList = objRetVal.EntityList.Where(x => x.AccountNum.Trim() == account.Trim()).ToList();
                                                if (AccountList != null && AccountList.Count > 0)
                                                {

                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "AircraftCD"
                            if (((TextBox)sender).ID.ToLower() == "aircraftcd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string aircraftCDS = ((TextBox)sender).Text.Trim();
                                    string[] aircraftCD = aircraftCDS.Split(',');

                                    List<FlightPak.Web.FlightPakMasterService.GetAllAircraft> AircraftList = new List<FlightPakMasterService.GetAllAircraft>();
                                    for (int i = 0; i < aircraftCD.Length; i++)
                                    {
                                        string aircradCd = aircraftCD[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetAircraftList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                AircraftList = objRetVal.EntityList.Where(x => x.AircraftCD.ToUpper().Trim() == aircradCd.ToUpper()).ToList();
                                                if (AircraftList != null && AircraftList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "AircraftDutyCD"
                            if (((TextBox)sender).ID.ToLower() == "aircraftdutycd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string aircraftDutyCDs = ((TextBox)sender).Text.Trim();
                                    string[] aircraftDutyCD = aircraftDutyCDs.Split(',');

                                    List<FlightPak.Web.FlightPakMasterService.AircraftDuty> AircraftDutyList = new List<FlightPakMasterService.AircraftDuty>();
                                    for (int i = 0; i < aircraftDutyCD.Length; i++)
                                    {
                                        string aircraftDutyCd = aircraftDutyCD[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetAircraftDuty();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                AircraftDutyList = objRetVal.EntityList.Where(x => x.AircraftDutyCD.ToUpper().Trim() == aircraftDutyCd.ToUpper()).ToList();
                                                if (AircraftDutyList != null && AircraftDutyList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "AircraftTypeCD"
                            if (((TextBox)sender).ID.ToLower() == "aircrafttypecd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string aircraftCDS = ((TextBox)sender).Text.Trim();
                                    string[] aircraftCD = aircraftCDS.Split(',');

                                    List<FlightPak.Web.FlightPakMasterService.GetAllAircraft> AircraftList = new List<FlightPakMasterService.GetAllAircraft>();
                                    for (int i = 0; i < aircraftCD.Length; i++)
                                    {
                                        string aircradCd = aircraftCD[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetAircraftList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                AircraftList = objRetVal.EntityList.Where(x => x.AircraftCD.ToUpper().Trim() == aircradCd.ToUpper()).ToList();
                                                if (AircraftList != null && AircraftList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "AuthorizationCD"
                            if (((TextBox)sender).ID.ToLower() == "authorizationcd")
                            {
                                Control ctrlDept = FindControlRecursive(Report, "DepartmentCD");
                                if (ctrlDept != null && ctrlDept is TextBox && !string.IsNullOrEmpty(((TextBox)ctrlDept).Text))
                                {
                                    if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                    {
                                        List<FlightPak.Web.FlightPakMasterService.DepartmentAuthorization> AuthorizationList = new List<FlightPakMasterService.DepartmentAuthorization>();
                                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objDstsvc.GetDepartmentAuthorizationList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                string authorizationCDs = ((TextBox)sender).Text.Trim();
                                                string[] authorizationCD = authorizationCDs.Split(',');

                                                for (int i = 0; i < authorizationCD.Length; i++)
                                                {
                                                    string authorization = authorizationCD[i];
                                                    AuthorizationList = objRetVal.EntityList.Where(x => x.AuthorizationCD.ToUpper().Trim() == authorization.ToUpper()).ToList();
                                                    if (AuthorizationList != null && AuthorizationList.Count > 0)
                                                    {
                                                        Control ctrlHolder1 = FindControlRecursive(Report, "AuthorizationCD");
                                                        if (ctrlHolder1 != null && ctrlHolder1 is TextBox)
                                                        {
                                                            ((TextBox)ctrlHolder1).Enabled = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Control ctrlHolder1 = FindControlRecursive(Report, "AuthorizationCD");
                                                        if (ctrlHolder1 != null && ctrlHolder1 is TextBox)
                                                        {
                                                            ((TextBox)ctrlHolder1).Enabled = true;
                                                        }
                                                        IsNotExist = true;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Control ctrlHolder1 = FindControlRecursive(Report, "AuthorizationCD");
                                                if (ctrlHolder1 != null && ctrlHolder1 is TextBox)
                                                {
                                                    ((TextBox)ctrlHolder1).Enabled = true;
                                                }
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ctrlHolder != null)
                                        {
                                            if (ctrlHolder is Label)
                                            {
                                                Control lbdisplay = FindControlRecursive(Report, "lbcvAuthorizationCD");
                                                if (lbdisplay != null && lbdisplay is Label)
                                                    ((Label)lbdisplay).Text = string.Empty;
                                            }
                                        }

                                        Control ctrlHolder1 = FindControlRecursive(Report, "AuthorizationCD");
                                        if (ctrlHolder1 != null && ctrlHolder1 is TextBox)
                                        {
                                            ((TextBox)ctrlHolder1).Enabled = true;
                                        }

                                        if (IsDept)
                                        {
                                            Control ctrlHolder2 = FindControlRecursive(Report, "AuthorizationCD");
                                            if (ctrlHolder2 != null && ctrlHolder2 is TextBox)
                                            {
                                                ((TextBox)ctrlHolder2).Enabled = false;
                                            }
                                        }

                                        IsNotExist = false;
                                    }
                                }
                                else
                                {
                                    Control ctrlHolder1 = FindControlRecursive(Report, "AuthorizationCD");
                                    if (ctrlHolder1 != null && ctrlHolder1 is TextBox)
                                    {
                                        ((TextBox)ctrlHolder1).Text = string.Empty;
                                        ((TextBox)ctrlHolder1).Enabled = false;
                                    }
                                    hdnDept.Value = string.Empty;
                                }
                            }
                            #endregion

                            #region "DepartmentCD"
                            if (((TextBox)sender).ID.ToLower() == "departmentcd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    List<FlightPak.Web.FlightPakMasterService.GetAllDeptAuth> DeptList = new List<FlightPakMasterService.GetAllDeptAuth>();
                                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var objRetVal = objDstsvc.GetAllDeptAuthorizationList();
                                        if (objRetVal.ReturnFlag)
                                        {
                                            string departmentCDs = ((TextBox)sender).Text.Trim();
                                            string[] departmentCD = departmentCDs.Split(',');

                                            for (int i = 0; i < departmentCD.Length; i++)
                                            {
                                                string department = departmentCD[i];
                                                using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    DeptList = objRetVal.EntityList.Where(x => x.DepartmentCD.ToUpper().Trim() == department.ToUpper()).ToList();
                                                    if (DeptList != null && DeptList.Count > 0)
                                                    {
                                                        Control ctrlHolder1 = FindControlRecursive(Report, "AuthorizationCD");
                                                        if (ctrlHolder1 != null && ctrlHolder1 is TextBox)
                                                        {
                                                            ((TextBox)ctrlHolder1).Enabled = true;
                                                            ((TextBox)ctrlHolder1).Text = string.Empty;
                                                        }
                                                        hdnDept.Value = DeptList[0].DepartmentID.ToString();
                                                        Control ctrlDepGrp = FindControlRecursive(Report, "DepartmentGroupID");
                                                        if (ctrlDepGrp != null && ctrlDepGrp is TextBox)
                                                        {
                                                            ((TextBox)ctrlDepGrp).Text = string.Empty;
                                                            ((TextBox)ctrlDepGrp).Enabled = false;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        IsNotExist = true;
                                                        Control ctrlHolder1 = FindControlRecursive(Report, "AuthorizationCD");
                                                        if (ctrlHolder1 != null && ctrlHolder1 is TextBox)
                                                        {
                                                            ((TextBox)ctrlHolder1).Text = string.Empty;
                                                            ((TextBox)ctrlHolder1).Enabled = false;
                                                        }
                                                        hdnDept.Value = string.Empty;
                                                        Control lbdisplay = FindControlRecursive(Report, "lbcvAuthorizationCD");
                                                        if (lbdisplay != null && lbdisplay is Label)
                                                            ((Label)lbdisplay).Text = string.Empty;
                                                        IsDept = true;
                                                        Control ctrlDepGrp = FindControlRecursive(Report, "DepartmentGroupID");
                                                        if (ctrlDepGrp != null && ctrlDepGrp is TextBox)
                                                        {
                                                            ((TextBox)ctrlDepGrp).Text = string.Empty;
                                                            ((TextBox)ctrlDepGrp).Enabled = false;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            IsNotExist = true;
                                            Control ctrlHolder1 = FindControlRecursive(Report, "AuthorizationCD");
                                            if (ctrlHolder1 != null && ctrlHolder1 is TextBox)
                                            {
                                                ((TextBox)ctrlHolder1).Text = string.Empty;
                                                ((TextBox)ctrlHolder1).Enabled = false;
                                            }
                                            hdnDept.Value = string.Empty;
                                            Control lbdisplay = FindControlRecursive(Report, "lbcvAuthorizationCD");
                                            if (lbdisplay != null && lbdisplay is Label)
                                                ((Label)lbdisplay).Text = string.Empty;
                                            IsDept = true;
                                            Control ctrlDepGrp = FindControlRecursive(Report, "DepartmentGroupID");
                                            if (ctrlDepGrp != null && ctrlDepGrp is TextBox)
                                            {
                                                ((TextBox)ctrlDepGrp).Text = string.Empty;
                                                ((TextBox)ctrlDepGrp).Enabled = false;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    Control ctrlHolder1 = FindControlRecursive(Report, "AuthorizationCD");
                                    if (ctrlHolder1 != null && ctrlHolder1 is TextBox)
                                    {
                                        ((TextBox)ctrlHolder1).Text = string.Empty;
                                        ((TextBox)ctrlHolder1).Enabled = false;
                                    }
                                    hdnDept.Value = string.Empty;

                                    Control lbdisplay = FindControlRecursive(Report, "lbcvAuthorizationCD");
                                    if (lbdisplay != null && lbdisplay is Label)
                                        ((Label)lbdisplay).Text = string.Empty;
                                    IsDept = true;


                                    Control ctrlDepGrp = FindControlRecursive(Report, "DepartmentGroupID");
                                    if (ctrlDepGrp != null && ctrlDepGrp is TextBox)
                                    {
                                        ((TextBox)ctrlDepGrp).Enabled = true;
                                    }
                                }
                            }
                            #endregion

                            #region "DepartmentGroupID"
                            if (((TextBox)sender).ID.ToLower() == "departmentgroupid")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    List<FlightPak.Web.FlightPakMasterService.GetAllDepartmentGroup> DeptGroupList = new List<FlightPakMasterService.GetAllDepartmentGroup>();
                                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var objRetVal = objDstsvc.GetDeptGroupInfo();
                                        if (objRetVal.ReturnFlag)
                                        {
                                            string departmentGroupCDs = ((TextBox)sender).Text.Trim();
                                            string[] departmentGroupCD = departmentGroupCDs.Split(',');

                                            for (int i = 0; i < departmentGroupCD.Length; i++)
                                            {
                                                string departmentGroup = departmentGroupCD[i];

                                                DeptGroupList = objRetVal.EntityList.Where(x => x.DepartmentGroupCD.ToUpper().Trim() == departmentGroup.ToUpper()).ToList();
                                                if (DeptGroupList != null && DeptGroupList.Count > 0)
                                                {
                                                    Control ctrlDept = FindControlRecursive(Report, "DepartmentCD");
                                                    if (ctrlDept != null && ctrlDept is TextBox)
                                                    {
                                                        ((TextBox)ctrlDept).Text = string.Empty;
                                                        ((TextBox)ctrlDept).Enabled = false;
                                                    }
                                                }
                                                else
                                                {
                                                    Control ctrlHolder1 = FindControlRecursive(Report, "DepartmentCD");
                                                    if (ctrlHolder1 != null && ctrlHolder1 is TextBox)
                                                    {
                                                        ((TextBox)ctrlHolder1).Text = string.Empty;
                                                        ((TextBox)ctrlHolder1).Enabled = false;
                                                    }
                                                    IsNotExist = true;
                                                    Control lbdisplay = FindControlRecursive(Report, "lbcvDepartmentGroupID");
                                                    if (lbdisplay != null && lbdisplay is Label)
                                                        ((Label)lbdisplay).Text = string.Empty;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            IsNotExist = true;
                                            Control ctrlHolder1 = FindControlRecursive(Report, "DepartmentCD");
                                            if (ctrlHolder1 != null && ctrlHolder1 is TextBox)
                                            {
                                                ((TextBox)ctrlHolder1).Text = string.Empty;
                                                ((TextBox)ctrlHolder1).Enabled = false;
                                            }
                                            Control lbdisplay = FindControlRecursive(Report, "lbcvDepartmentGroupID");
                                            if (lbdisplay != null && lbdisplay is Label)
                                                ((Label)lbdisplay).Text = string.Empty;
                                        }
                                    }
                                }
                                else
                                {
                                    Control ctrlHolder1 = FindControlRecursive(Report, "DepartmentCD");
                                    if (ctrlHolder1 != null && ctrlHolder1 is TextBox)
                                    {
                                        ((TextBox)ctrlHolder1).Enabled = true;
                                    }
                                    Control lbdisplay = FindControlRecursive(Report, "lbcvDepartmentGroupID");
                                    if (lbdisplay != null && lbdisplay is Label)
                                        ((Label)lbdisplay).Text = string.Empty;
                                }
                            }
                            #endregion

                            #region "TailNumber"
                            if (((TextBox)sender).ID.ToLower() == "tailnumber")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string tailnos = ((TextBox)sender).Text.Trim();
                                    string[] TailNo = tailnos.Split(',');

                                    List<FlightPak.Web.FlightPakMasterService.Fleet> FleetList = new List<FlightPakMasterService.Fleet>();
                                    for (int i = 0; i < TailNo.Length; i++)
                                    {
                                        string Tail = TailNo[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetFleetProfileList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                FleetList = objRetVal.EntityList.Where(x => x.TailNum.ToUpper().Trim() == Tail.ToUpper()).ToList();
                                                if (FleetList != null && FleetList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "ClientCD"
                            if (((TextBox)sender).ID.ToLower() == "clientcd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    List<FlightPak.Web.FlightPakMasterService.Client> ClientList = new List<FlightPakMasterService.Client>();
                                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var objRetVal = objDstsvc.GetClientCodeList();
                                        if (objRetVal.ReturnFlag)
                                        {
                                            ClientList = objRetVal.EntityList.Where(x => x.ClientCD.ToUpper().Trim() == (((TextBox)sender).Text).ToUpper().Trim()).ToList();
                                            if (ClientList != null && ClientList.Count > 0)
                                            {

                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                        else
                                        {
                                            IsNotExist = true;
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "CQCustomerCD"
                            if (((TextBox)sender).ID.ToLower() == "cqcustomercd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string cqcustomers = ((TextBox)sender).Text.Trim();
                                    string[] cqcustomerCDs = cqcustomers.Split(',');

                                    for (int i = 0; i < cqcustomerCDs.Length; i++)
                                    {
                                        string cqCus = cqcustomerCDs[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            FlightPak.Web.FlightPakMasterService.CQCustomer objCQCustomer = new FlightPakMasterService.CQCustomer();
                                            objCQCustomer.CQCustomerCD = cqCus;
                                            var objRetVal = objMasterService.GetCQCustomerByCQCustomerCD(objCQCustomer).EntityList;
                                            if (objRetVal != null && objRetVal.Count > 0)
                                            {
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "Customer"
                            if (((TextBox)sender).ID.ToLower() == "customer")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string cqcustomers = ((TextBox)sender).Text.Trim();
                                    string[] cqcustomerCDs = cqcustomers.Split(',');

                                    for (int i = 0; i < cqcustomerCDs.Length; i++)
                                    {
                                        string cqCus = cqcustomerCDs[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            FlightPak.Web.FlightPakMasterService.CQCustomer objCQCustomer = new FlightPakMasterService.CQCustomer();
                                            objCQCustomer.CQCustomerCD = cqCus;
                                            var objRetVal = objMasterService.GetCQCustomerByCQCustomerCD(objCQCustomer).EntityList;
                                            if (objRetVal != null && objRetVal.Count > 0)
                                            {
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "CustomerName"
                            if (((TextBox)sender).ID.ToLower() == "customername")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string leadSourceCDs = ((TextBox)sender).Text.Trim();
                                    string[] leadSourceCD = leadSourceCDs.Split(',');

                                    List<FlightPakMasterService.LeadSource> LeadSourceList = new List<FlightPakMasterService.LeadSource>();
                                    for (int i = 0; i < leadSourceCD.Length; i++)
                                    {
                                        string leadSource = leadSourceCD[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetLeadSourceList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                LeadSourceList = objRetVal.EntityList.Where(x => x.LeadSourceCD.ToUpper().Trim() == leadSource.ToUpper()).ToList();
                                                if (LeadSourceList != null && LeadSourceList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "CrewCD"
                            if (((TextBox)sender).ID.ToLower() == "crewcd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    if (!string.IsNullOrEmpty(filename) && filename == "DBCrewRoster.xml")
                                    {
                                        List<FlightPak.Web.FlightPakMasterService.GetAllCrew> CrewList = new List<FlightPakMasterService.GetAllCrew>();
                                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objDstsvc.GetCrewList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                CrewList = objRetVal.EntityList.Where(x => x.CrewCD.ToUpper().Trim() == (((TextBox)sender).Text).ToUpper()).ToList();
                                                if (CrewList != null && CrewList.Count > 0)
                                                {

                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string crewCDS = ((TextBox)sender).Text.Trim();
                                        string[] crewCD = crewCDS.Split(',');

                                        List<FlightPak.Web.FlightPakMasterService.GetAllCrew> CrewList = new List<FlightPakMasterService.GetAllCrew>();
                                        for (int i = 0; i < crewCD.Length; i++)
                                        {
                                            string crew = crewCD[i];
                                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                var objRetVal = objMasterService.GetCrewList();
                                                if (objRetVal.ReturnFlag)
                                                {
                                                    CrewList = objRetVal.EntityList.Where(x => x.CrewCD.ToUpper().Trim() == crew.ToUpper()).ToList();
                                                    if (CrewList != null && CrewList.Count > 0)
                                                    {

                                                    }
                                                    else
                                                    {
                                                        IsNotExist = true;
                                                    }
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "CrewDutyTypeCD"
                            if (((TextBox)sender).ID.ToLower() == "crewdutytypecd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string crewDutyCDS = ((TextBox)sender).Text.Trim();
                                    string[] crewDutyCD = crewDutyCDS.Split(',');

                                    List<FlightPak.Web.FlightPakMasterService.CrewDutyType> CrewDutyList = new List<FlightPakMasterService.CrewDutyType>();
                                    for (int i = 0; i < crewDutyCD.Length; i++)
                                    {
                                        string crewDuty = crewDutyCD[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetCrewDutyTypeList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                CrewDutyList = objRetVal.EntityList.Where(x => x.DutyTypeCD.ToUpper().Trim() == crewDuty.ToUpper()).ToList();
                                                if (CrewDutyList != null && CrewDutyList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "CrewGroupCD"
                            if (((TextBox)sender).ID.ToLower() == "crewgroupcd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string crewGroupCDS = ((TextBox)sender).Text.Trim();
                                    string[] crewgroupCD = crewGroupCDS.Split(',');

                                    List<FlightPak.Web.FlightPakMasterService.GetAllCrewGroup> CrewGroupList = new List<FlightPakMasterService.GetAllCrewGroup>();
                                    for (int i = 0; i < crewgroupCD.Length; i++)
                                    {
                                        string crewGroup = crewgroupCD[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetCrewGroupInfo();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                CrewGroupList = objRetVal.EntityList.Where(x => x.CrewGroupCD.ToUpper().Trim() == crewGroup.ToUpper()).ToList();
                                                if (CrewGroupList != null && CrewGroupList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "FleetGroupCD"
                            if (((TextBox)sender).ID.ToLower() == "fleetgroupcd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string fleetGroupCDS = ((TextBox)sender).Text.Trim();
                                    string[] fleetgroupCD = fleetGroupCDS.Split(',');

                                    List<FlightPak.Web.FlightPakMasterService.GetAllFleetGroup> FleetGrouptList = new List<FlightPakMasterService.GetAllFleetGroup>();
                                    for (int i = 0; i < fleetgroupCD.Length; i++)
                                    {
                                        string fleetGroup = fleetgroupCD[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetFleetGroup();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                FleetGrouptList = objRetVal.EntityList.Where(x => x.FleetGroupCD.ToUpper().Trim() == fleetGroup.ToUpper()).ToList();
                                                if (FleetGrouptList != null && FleetGrouptList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "FlightCatagoryCD"
                            if (((TextBox)sender).ID.ToLower() == "flightcatagorycd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string flightCatergoryCDs = ((TextBox)sender).Text.Trim();
                                    string[] flightCatergoryCD = flightCatergoryCDs.Split(',');

                                    List<FlightPak.Web.FlightPakMasterService.FlightCatagory> FlightCatergoryList = new List<FlightPakMasterService.FlightCatagory>();
                                    for (int i = 0; i < flightCatergoryCD.Length; i++)
                                    {
                                        string flightcatergorycd = flightCatergoryCD[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetFlightCategoryList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                FlightCatergoryList = objRetVal.EntityList.Where(x => x.FlightCatagoryCD.ToUpper().Trim() == flightcatergorycd.ToUpper()).ToList();
                                                if (FlightCatergoryList != null && FlightCatergoryList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "CatagoryCD"
                            if (((TextBox)sender).ID.ToLower() == "catagorycd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string flightCatergoryCDs = ((TextBox)sender).Text.Trim();
                                    string[] flightCatergoryCD = flightCatergoryCDs.Split(',');

                                    List<FlightPak.Web.FlightPakMasterService.FlightCatagory> FlightCatergoryList = new List<FlightPakMasterService.FlightCatagory>();
                                    for (int i = 0; i < flightCatergoryCD.Length; i++)
                                    {
                                        string flightcatergorycd = flightCatergoryCD[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetFlightCategoryList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                FlightCatergoryList = objRetVal.EntityList.Where(x => x.FlightCatagoryCD.ToUpper().Trim() == flightcatergorycd.ToUpper()).ToList();
                                                if (FlightCatergoryList != null && FlightCatergoryList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "FlightPurposeCD"
                            if (((TextBox)sender).ID.ToLower() == "flightpurposecd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string flightPurposeCDs = ((TextBox)sender).Text.Trim();
                                    string[] flightPurposeCD = flightPurposeCDs.Split(',');

                                    List<FlightPak.Web.FlightPakMasterService.FlightPurpose> FlightPurposeList = new List<FlightPakMasterService.FlightPurpose>();
                                    for (int i = 0; i < flightPurposeCD.Length; i++)
                                    {
                                        string flightpurpose = flightPurposeCD[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetFlightPurposeList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                FlightPurposeList = objRetVal.EntityList.Where(x => x.FlightPurposeCD.ToUpper().Trim() == flightpurpose.ToUpper()).ToList();
                                                if (FlightPurposeList != null && FlightPurposeList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "HomeBase"
                            if (((TextBox)sender).ID.ToLower() == "homebase")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    if (!string.IsNullOrEmpty(filename) && filename == "POSTBudgetActualVarianceReport.xml")
                                    {
                                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objDstsvc.GetCompanyMasterListInfo();
                                            List<FlightPakMasterService.GetAllCompanyMaster> CompanyList = new List<FlightPakMasterService.GetAllCompanyMaster>();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                CompanyList = objRetVal.EntityList.Where(x => x.HomebaseCD != null && x.HomebaseCD.ToString().ToUpper().Trim().Equals(((TextBox)sender).Text.ToUpper().Trim())).ToList();
                                                if (CompanyList != null && CompanyList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string homebases = ((TextBox)sender).Text.Trim();
                                        string[] homebase = homebases.Split(',');

                                        List<FlightPakMasterService.GetAllCompanyMaster> CompanyList = new List<FlightPakMasterService.GetAllCompanyMaster>();
                                        for (int i = 0; i < homebase.Length; i++)
                                        {
                                            string homebaseCD = homebase[i];
                                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                var objRetVal = objMasterService.GetCompanyMasterListInfo();
                                                if (objRetVal.ReturnFlag)
                                                {
                                                    CompanyList = objRetVal.EntityList.Where(x => x.HomebaseCD.ToUpper().Trim() == homebaseCD.ToUpper()).ToList();
                                                    if (CompanyList != null && CompanyList.Count > 0)
                                                    {
                                                    }
                                                    else
                                                    {
                                                        IsNotExist = true;
                                                    }
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "HomeBaseCD"
                            if (((TextBox)sender).ID.ToLower() == "homebasecd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    if (!string.IsNullOrEmpty(filename) && filename == "POSTBudgetActualVarianceReport.xml")
                                    {
                                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objDstsvc.GetCompanyMasterListInfo();
                                            List<FlightPakMasterService.GetAllCompanyMaster> CompanyList = new List<FlightPakMasterService.GetAllCompanyMaster>();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                CompanyList = objRetVal.EntityList.Where(x => x.HomebaseCD != null && x.HomebaseCD.ToString().ToUpper().Trim().Equals(((TextBox)sender).Text.ToUpper().Trim())).ToList();
                                                if (CompanyList != null && CompanyList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string homebases = ((TextBox)sender).Text.Trim();
                                        string[] homebase = homebases.Split(',');

                                        List<FlightPakMasterService.GetAllCompanyMaster> CompanyList = new List<FlightPakMasterService.GetAllCompanyMaster>();
                                        for (int i = 0; i < homebase.Length; i++)
                                        {
                                            string homebaseCD = homebase[i];
                                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                var objRetVal = objMasterService.GetCompanyMasterListInfo();
                                                if (objRetVal.ReturnFlag)
                                                {
                                                    CompanyList = objRetVal.EntityList.Where(x => x.HomebaseCD.ToUpper().Trim() == homebaseCD.ToUpper()).ToList();
                                                    if (CompanyList != null && CompanyList.Count > 0)
                                                    {
                                                    }
                                                    else
                                                    {
                                                        IsNotExist = true;
                                                    }
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "IcaoID"
                            if (((TextBox)sender).ID.ToLower() == "icaoid")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                                        var objRetVal = objDstsvc.GetAirportByAirportICaoID((((TextBox)sender).Text).Trim());
                                        if (objRetVal.ReturnFlag)
                                        {
                                            AirportLists = objRetVal.EntityList;
                                            if (AirportLists != null && AirportLists.Count > 0)
                                            {
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                        else
                                        {
                                            IsNotExist = true;
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "PassengerCD"
                            if (((TextBox)sender).ID.ToLower() == "passengercd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string passengers = ((TextBox)sender).Text.Trim();
                                    string[] passengerCDs = passengers.Split(',');
                                    for (int i = 0; i < passengerCDs.Length; i++)
                                    {
                                        string pax = passengerCDs[i];
                                        List<FlightPak.Web.FlightPakMasterService.GetAllPassenger> PassengerList = new List<FlightPakMasterService.GetAllPassenger>();
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetPassengerList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                PassengerList = objRetVal.EntityList.Where(x => x.PassengerRequestorCD.ToUpper().Trim() == pax.ToUpper().Trim()).ToList();
                                                if (PassengerList != null && PassengerList.Count > 0)
                                                {

                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "PassengerRequestorCD"
                            if (((TextBox)sender).ID.ToLower() == "passengerrequestorcd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string passengers = ((TextBox)sender).Text.Trim();
                                    string[] passengerCDs = passengers.Split(',');
                                    for (int i = 0; i < passengerCDs.Length; i++)
                                    {
                                        string pax = passengerCDs[i];
                                        List<FlightPak.Web.FlightPakMasterService.GetAllPassenger> PassengerList = new List<FlightPakMasterService.GetAllPassenger>();
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetPassengerList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                PassengerList = objRetVal.EntityList.Where(x => x.PassengerRequestorCD.ToUpper().Trim() == pax.ToUpper()).ToList();
                                                if (PassengerList != null && PassengerList.Count > 0)
                                                {

                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "RequestorCD"
                            if (((TextBox)sender).ID.ToLower() == "requestorcd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string passengers = ((TextBox)sender).Text.Trim();
                                    string[] passengerCDs = passengers.Split(',');
                                    for (int i = 0; i < passengerCDs.Length; i++)
                                    {
                                        string pax = passengerCDs[i];

                                        List<FlightPak.Web.FlightPakMasterService.GetAllPassenger> PassengerList = new List<FlightPakMasterService.GetAllPassenger>();
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetPassengerList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                PassengerList = objRetVal.EntityList.Where(x => x.PassengerRequestorCD.ToUpper().Trim() == pax.ToUpper()).ToList();
                                                if (PassengerList != null && PassengerList.Count > 0)
                                                {

                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "PaxID"
                            if (((TextBox)sender).ID.ToLower() == "paxid")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string passengers = ((TextBox)sender).Text.Trim();
                                    string[] passengerCDs = passengers.Split(',');
                                    for (int i = 0; i < passengerCDs.Length; i++)
                                    {
                                        string pax = passengerCDs[i];

                                        List<FlightPak.Web.FlightPakMasterService.GetAllPassenger> PassengerList = new List<FlightPakMasterService.GetAllPassenger>();
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetPassengerList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                PassengerList = objRetVal.EntityList.Where(x => x.PassengerRequestorCD.ToUpper().Trim() == pax.ToUpper()).ToList();
                                                if (PassengerList != null && PassengerList.Count > 0)
                                                {

                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "PassengerGroupCD"
                            if (((TextBox)sender).ID.ToLower() == "passengergroupcd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string passengerGroupCDs = ((TextBox)sender).Text.Trim();
                                    string[] passengerGroupCD = passengerGroupCDs.Split(',');
                                    for (int i = 0; i < passengerGroupCD.Length; i++)
                                    {
                                        string passengerGroup = passengerGroupCD[i];

                                        List<FlightPak.Web.FlightPakMasterService.PassengerGroup> PassengerGroupList = new List<FlightPakMasterService.PassengerGroup>();
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetPaxGroupList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                PassengerGroupList = objRetVal.EntityList.Where(x => x.PassengerGroupCD.ToUpper().Trim() == passengerGroup.ToUpper()).ToList();
                                                if (PassengerGroupList != null && PassengerGroupList.Count > 0)
                                                {

                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "PaymentTypeCD"
                            if (((TextBox)sender).ID.ToLower() == "paymenttypecd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string paymentTypeCDs = ((TextBox)sender).Text.Trim();
                                    string[] paymentTypeCD = paymentTypeCDs.Split(',');

                                    List<FlightPak.Web.FlightPakMasterService.PaymentType> PaymentList = new List<FlightPakMasterService.PaymentType>();
                                    for (int i = 0; i < paymentTypeCD.Length; i++)
                                    {
                                        string paymentType = paymentTypeCD[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetPaymentType();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                PaymentList = objRetVal.EntityList.Where(x => x.PaymentTypeCD.ToUpper().Trim() == paymentType.ToUpper()).ToList();
                                                if (PaymentList != null && PaymentList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "SalesPerson"
                            if (((TextBox)sender).ID.ToLower() == "salesperson")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string salesPersonCDs = ((TextBox)sender).Text.Trim();
                                    string[] salesPersonCD = salesPersonCDs.Split(',');

                                    List<FlightPak.Web.FlightPakMasterService.GetSalesPerson> SalesPersonList = new List<FlightPakMasterService.GetSalesPerson>();
                                    for (int i = 0; i < salesPersonCD.Length; i++)
                                    {
                                        string salesPerson = salesPersonCD[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetSalesPersonList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                SalesPersonList = objRetVal.EntityList.Where(x => x.SalesPersonCD.ToUpper().Trim() == salesPerson.ToUpper()).ToList();
                                                if (SalesPersonList != null && SalesPersonList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "SalesPersonCD"
                            if (((TextBox)sender).ID.ToLower() == "salespersoncd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string salesPersonCDs = ((TextBox)sender).Text.Trim();
                                    string[] salesPersonCD = salesPersonCDs.Split(',');

                                    List<FlightPak.Web.FlightPakMasterService.GetSalesPerson> SalesPersonList = new List<FlightPakMasterService.GetSalesPerson>();
                                    for (int i = 0; i < salesPersonCD.Length; i++)
                                    {
                                        string salesPerson = salesPersonCD[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetSalesPersonList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                SalesPersonList = objRetVal.EntityList.Where(x => x.SalesPersonCD.ToUpper().Trim() == salesPerson.ToUpper()).ToList();
                                                if (SalesPersonList != null && SalesPersonList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "VendorCD"
                            if (((TextBox)sender).ID.ToLower() == "vendorcd")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string vendorCDs = ((TextBox)sender).Text.Trim();
                                    string[] vendorCD = vendorCDs.Split(',');

                                    List<FlightPak.Web.FlightPakMasterService.GetAllVendor> VendorList = new List<FlightPakMasterService.GetAllVendor>();
                                    for (int i = 0; i < vendorCD.Length; i++)
                                    {
                                        string vendor = vendorCD[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetPayableVendorInfo();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                VendorList = objRetVal.EntityList.Where(x => x.VendorCD.ToUpper().Trim() == vendor.ToUpper()).ToList();
                                                if (VendorList != null && VendorList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "TripRangeFrom"
                            if (((TextBox)sender).ID.ToLower() == "triprangefrom")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    List<FlightPak.Web.FlightPakMasterService.Client> TripList = new List<FlightPakMasterService.Client>();
                                    using (PreflightService.PreflightServiceClient objDstsvc = new PreflightService.PreflightServiceClient())
                                    {
                                        var objRetVal = objDstsvc.GetTripByTripNum(Convert.ToInt64(((TextBox)sender).Text.Trim()));
                                        if (objRetVal.ReturnFlag)
                                        {
                                        }
                                        else
                                        {
                                            IsNotExist = true;
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "TripRangeTo"
                            if (((TextBox)sender).ID.ToLower() == "triprangeto")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    List<FlightPak.Web.FlightPakMasterService.Client> TripList = new List<FlightPakMasterService.Client>();
                                    using (PreflightService.PreflightServiceClient objDstsvc = new PreflightService.PreflightServiceClient())
                                    {
                                        var objRetVal = objDstsvc.GetTripByTripNum(Convert.ToInt64(((TextBox)sender).Text.Trim()));
                                        if (objRetVal.ReturnFlag)
                                        {
                                        }
                                        else
                                        {
                                            IsNotExist = true;
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "TripNum"
                            if (((TextBox)sender).ID.ToLower() == "tripnum")
                            {
                                Control ctrlHolder1 = FindControlRecursive(Report, "LegNum");

                                Control cvctrlHolder = FindControlRecursive(Report, "lbcvLegNum");
                                if (cvctrlHolder != null)
                                {
                                    if (cvctrlHolder is Label)
                                    {
                                        ((Label)cvctrlHolder).Text = string.Empty;
                                    }
                                }

                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    using (PreflightService.PreflightServiceClient objDstsvc = new PreflightService.PreflightServiceClient())
                                    {
                                        string tripnums = ((TextBox)sender).Text.Trim();
                                        string[] tripNos = tripnums.Split(',');
                                        if (tripNos.Length == 1)
                                        {
                                            string tripNum = tripNos[0];
                                            long lngtripNum = 0;
                                            if (long.TryParse(tripNum, out lngtripNum))
                                            {
                                                var objRetVal = objDstsvc.GetTripByTripNum(lngtripNum);

                                                if (objRetVal.ReturnFlag)
                                                {
                                                    hdnMainID.Value = objRetVal.EntityList[0].TripID.ToString();
                                                    if (ctrlHolder1 != null && ctrlHolder1 is TextBox)
                                                    {
                                                        ((TextBox)ctrlHolder1).Text = string.Empty;
                                                        ((TextBox)ctrlHolder1).Enabled = true;

                                                    }

                                                }
                                                else
                                                {
                                                    if (ctrlHolder1 != null && ctrlHolder1 is TextBox)
                                                    {
                                                        ((TextBox)ctrlHolder1).Text = string.Empty;
                                                        ((TextBox)ctrlHolder1).Enabled = false;
                                                    }


                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                if (ctrlHolder1 != null && ctrlHolder1 is TextBox)
                                                {
                                                    ((TextBox)ctrlHolder1).Text = string.Empty;
                                                    ((TextBox)ctrlHolder1).Enabled = false;
                                                }

                                                IsNotExist = true;
                                            }
                                        }
                                        else
                                        {

                                            for (int i = 0; i < tripNos.Length; i++)
                                            {
                                                string tripNum = tripNos[i];
                                                long lngtripNum = 0;
                                                if (long.TryParse(tripNum, out lngtripNum))
                                                {
                                                    var objRetVal = objDstsvc.GetTripByTripNum(lngtripNum).EntityList;
                                                    if (objRetVal != null && objRetVal.Count > 0)
                                                    {

                                                    }
                                                    else
                                                    {
                                                        IsNotExist = true;
                                                    }
                                                }
                                                else
                                                    IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (ctrlHolder1 != null && ctrlHolder1 is TextBox)
                                    {
                                        ((TextBox)ctrlHolder1).Text = string.Empty;
                                        ((TextBox)ctrlHolder1).Enabled = false;
                                    }
                                }
                            }
                            #endregion

                            #region "ChecklistCode"
                            if (((TextBox)sender).ID.ToLower() == "checklistcode")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    string crewCheckCDs = ((TextBox)sender).Text.Trim();
                                    string[] crewCheckCD = crewCheckCDs.Split(',');

                                    List<FlightPak.Web.FlightPakMasterService.CrewCheckList> CrewCheckList = new List<FlightPakMasterService.CrewCheckList>();
                                    for (int i = 0; i < crewCheckCD.Length; i++)
                                    {
                                        string crewCheckList = crewCheckCD[i];
                                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objRetVal = objMasterService.GetCrewChecklistList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                CrewCheckList = objRetVal.EntityList.Where(x => x.CrewCheckCD.ToUpper().Trim() == crewCheckList.ToUpper()).ToList();
                                                if (CrewCheckList != null && CrewCheckList.Count > 0)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region "LogNum"
                            if (((TextBox)sender).ID.ToLower() == "lognum")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    List<FlightPak.Web.FlightPakMasterService.Client> TripList = new List<FlightPakMasterService.Client>();
                                    using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
                                    {
                                        long logNum = 0;
                                        if (long.TryParse(((TextBox)sender).Text.Trim(), out logNum))
                                        {
                                            var ObjRetval = objService.GetPostflightTrips(0, 0, 0, false, false, DateTime.MinValue).EntityList.Where(x => x.LogNum == logNum).ToList();
                                            if (ObjRetval != null && ObjRetval.Count > 0)
                                            {

                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                        else
                                            IsNotExist = true;
                                    }
                                }
                            }
                            #endregion

                            #region "SlipNUM"
                            if (((TextBox)sender).ID.ToLower() == "slipnum")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
                                    {
                                        long slipNum = 0;
                                        if (long.TryParse(((TextBox)sender).Text.Trim(), out slipNum))
                                        {
                                            var objRetVal = objService.GetExpList();
                                            if (objRetVal != null && objRetVal.ReturnFlag)
                                            {
                                                var retVal = objRetVal.EntityList.Where(x => x.IsDeleted == false && x.SlipNUM == slipNum).SingleOrDefault();
                                                if (retVal != null)
                                                {
                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                        else
                                            IsNotExist = true;

                                    }
                                }
                            }
                            #endregion

                            #region "CRTripNUM"
                            if (((TextBox)sender).ID.ToLower() == "crtripnum")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    long CRTripNum = 0;
                                    if (long.TryParse(((TextBox)sender).Text.Trim(), out CRTripNum))
                                    {
                                        using (CorporateRequestService.CorporateRequestServiceClient objService = new CorporateRequestService.CorporateRequestServiceClient())
                                        {
                                            var objRetVal = objService.GetRequestByTripNum(CRTripNum);
                                            if (objRetVal.ReturnFlag)
                                            {
                                                hdnCRMainID.Value = objRetVal.EntityList[0].CRMainID.ToString();
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                                hdnCRMainID.Value = string.Empty;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        IsNotExist = true;
                                        hdnCRMainID.Value = string.Empty;
                                    }
                                }
                            }
                            #endregion

                            #region "CRLegNum"
                            if (((TextBox)sender).ID.ToLower() == "crlegnum")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    long CRLegNum = 0;
                                    if (long.TryParse(((TextBox)sender).Text.Trim(), out CRLegNum))
                                    {

                                        using (CorporateRequestService.CorporateRequestServiceClient objService = new CorporateRequestService.CorporateRequestServiceClient())
                                        {
                                            var objRetVal = objService.GetRequest(Convert.ToInt64(hdnCRMainID.Value)).EntityList;
                                            if (objRetVal != null && objRetVal.Count > 0)
                                            {
                                                List<CorporateRequestService.CRLeg> crLegs = new List<CorporateRequestService.CRLeg>();
                                                crLegs = objRetVal[0].CRLegs.Where(x => x.IsDeleted == false && x.LegNUM == CRLegNum).ToList();
                                                if (crLegs != null && crLegs.Count > 0)
                                                {

                                                }
                                                else
                                                {
                                                    IsNotExist = true;
                                                }
                                            }
                                            else
                                            {
                                                IsNotExist = true;
                                            }
                                        }
                                    }
                                    else
                                        IsNotExist = true;

                                }
                            }
                            #endregion


                            #region "TripLegNum"
                            if (((TextBox)sender).ID.ToLower() == "legnum")
                            {
                                if (!string.IsNullOrEmpty(((TextBox)sender).Text))
                                {
                                    long LegNum = 0;

                                    string Legnums = ((TextBox)sender).Text.Trim();
                                    string[] legNos = Legnums.Split(',');
                                    using (PreflightService.PreflightServiceClient objService = new PreflightService.PreflightServiceClient())
                                    {
                                        foreach (string leg in legNos)
                                        {

                                            if (leg != string.Empty && long.TryParse(leg, out LegNum))
                                            {

                                                var objRetVal = objService.GetTrip(Convert.ToInt64(hdnMainID.Value));
                                                if (objRetVal.ReturnFlag)
                                                {
                                                    List<PreflightService.PreflightLeg> preflightlegs = new List<PreflightService.PreflightLeg>();

                                                    if (objRetVal.EntityList != null && objRetVal.EntityList[0].PreflightLegs != null && objRetVal.EntityList[0].PreflightLegs.Where(x => x.IsDeleted == false && x.LegNUM == LegNum).ToList().Count > 0)
                                                    {

                                                    }
                                                    else
                                                        IsNotExist = true;
                                                }
                                                else
                                                    IsNotExist = true;
                                            }
                                            else
                                                IsNotExist = true;
                                        }
                                    }
                                }
                            }
                            #endregion

                            if (IsNotExist)
                            {
                                if (ctrlHolder != null)
                                {
                                    if (ctrlHolder is Label)
                                    {
                                        Control lbdisplay = FindControlRecursive(Report, "lb" + ((TextBox)sender).ID);
                                        if (lbdisplay != null && lbdisplay is Label)
                                            ((Label)ctrlHolder).Text = ((Label)lbdisplay).Text + " is not valid.";
                                    }
                                }
                                RadAjaxManager.GetCurrent(Page).FocusControl((TextBox)sender);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void tbEditHeaderText_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(hdnHeaderSourceID.Value))
                        {
                            HeaderBySource.Items[Convert.ToInt32(hdnHeaderSourceID.Value)].Text = tbEditHeaderText.Text;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("DivExternalForm") > -1)
                        {
                            e.Updated = DivExternalForm;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
    }

}
