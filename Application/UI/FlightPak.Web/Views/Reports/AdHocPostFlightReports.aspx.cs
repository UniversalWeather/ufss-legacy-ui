﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.PostflightService;

//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Xml;
using System.Drawing;
using System.Xml.Linq;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Reports
{
    public partial class AdHocPostFlightReports : BaseSecuredPage
    {
        private ExceptionManager exManager;
        public PostflightService.PostflightMain Log = new PostflightService.PostflightMain();
        string DateFormat = "MM/dd/yyyy";
        public Int64 POLogID = 0;
        

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                            DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                        
                        string xmltab = string.Empty;
                        xmltab = "POSTAdhocExport.xml";                        
                        if (!IsPostBack)
                        {
                            lbMessage.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            lbcvClient.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                            // Enabling Client Control based on User Principal Identity
                            if (UserPrincipal.Identity._clientId != null && UserPrincipal.Identity._clientId > 0)
                            {
                                hdnClient.Value = UserPrincipal.Identity._clientId.ToString();
                                tbClientCode.Text = UserPrincipal.Identity._clientCd;

                                // Get Client Description from Master Service
                                try
                                {
                                    FlightPakMasterService.Client client = new FlightPakMasterService.Client();
                                    client = GetClient((long)UserPrincipal.Identity._clientId);
                                    if (client != null && client.ClientDescription != null)
                                        lbClientDesc.Text = System.Web.HttpUtility.HtmlEncode(client.ClientDescription);
                                }
                                catch (Exception ee1)
                                {
                                    //Manually Handled
                                }

                                tbClientCode.Enabled = false;
                                btnClientCode.Enabled = false;
                            }
                            else
                            {
                                tbClientCode.Text = string.Empty;
                                hdnClient.Value = string.Empty;
                                lbClientDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            }

                            
                            //Ramesh: Added to fix defect# 2175
                            if (DateFormat != null)
                            if (!string.IsNullOrEmpty(DateFormat)) 
                            {
                                if (Session[Session["TabSelect"] + "DATEFROMtbDate"] != null)
                                {
                                    ((TextBox)ucfromDate.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Session[Session["TabSelect"] + "DATEFROMtbDate"].ToString());
                                }
                                if (Session[Session["TabSelect"] + "DATETOtbDate"] != null)
                                {
                                    ((TextBox)uctoDate.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Session[Session["TabSelect"] + "DATETOtbDate"].ToString());
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POMain);
                }
            }
        }

        protected void Search_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindData(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POMain);
                }
            }
        }

        protected void Search_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                InsertSelectedRow();
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POMain);
                }
            }
        }

        protected void Search_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        InsertSelectedRow();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POMain);
                }
            }
        }

        private void InsertSelectedRow()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
                {
                    if (dgSearch.SelectedItems.Count > 0)
                    {
                        GridDataItem item = (GridDataItem)dgSearch.SelectedItems[0];
                        POLogID = Convert.ToInt64(item.GetDataKeyValue("POLogID").ToString());

                        if (Request.QueryString["IsPopup"] != null && Request.QueryString["IsPopup"].ToString() == "YES")
                        {
                            RadAjaxManager1.ResponseScripts.Add(@"returnToParent();");
                        }
                        else
                        {
                            Int64 convTripNum = 0;
                            Int64 LogNum = 0;
                            if (Int64.TryParse(item["LogNum"].Text, out convTripNum))
                                LogNum = convTripNum;

                            if (Session["POSTFLIGHTMAIN"] != null)
                            {
                                PostflightMain TripLog = (PostflightMain)Session["POSTFLIGHTMAIN"];

                                if ((TripLog.TripActionStatus == TripAction.RecordNowLockedForEdit && TripLog.POLogID > 0) || (TripLog.TripActionStatus == TripAction.NewRecord && TripLog.POLogID == 0))
                                {
                                    RadWindowManager1.RadConfirm("Log " + TripLog.LogNum.ToString() + " unsaved, ignore changes and load the selected Log : " + LogNum + " ?", "confirmCallBackFn", 330, 100, null, "Confirmation!");
                                }
                                else
                                {
                                    // Clear the Existing Session Values
                                    Session.Remove("POSTFLIGHTMAIN");
                                    Session.Remove("HOMEBASEKEY");
                                    Session.Remove("FLEETRATEKEY");
                                    Session.Remove("SIFLRATEKEY");
                                    Session.Remove("FLEETPROFILEKEY");
                                    Session.Remove("POEXCEPTION");
                                    Session.Remove("POPAXINFO");
                                    Session.Remove("POCREWINFO");
                                    Session.Remove("POSTFLIGHTMAIN_RETAIN");

                                    PostflightMain obj = new PostflightMain();
                                    GridDataItem dataitem = (GridDataItem)dgSearch.SelectedItems[0];
                                    obj.POLogID = Convert.ToInt64(dataitem.GetDataKeyValue("POLogID").ToString());
                                    var result = objService.GetTrip(obj);

                                    if (result.ReturnFlag == true)
                                    {
                                        Session["POSTFLIGHTMAIN"] = (PostflightMain)result.EntityInfo;

                                        // Bind the Saved Exceptions into the Session
                                        if (result.EntityInfo.PostflightTripExceptions != null && result.EntityInfo.PostflightTripExceptions.Count > 0)
                                            Session["POEXCEPTION"] = result.EntityInfo.PostflightTripExceptions;
                                    }
                                    RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                                }
                            }
                            else
                            {
                                PostflightMain obj = new PostflightMain();
                                GridDataItem dataitem = (GridDataItem)dgSearch.SelectedItems[0];
                                obj.POLogID = Convert.ToInt64(dataitem.GetDataKeyValue("POLogID").ToString());
                                var result = objService.GetTrip(obj);

                                if (result.ReturnFlag == true)
                                {
                                    Session["POSTFLIGHTMAIN"] = (PostflightMain)result.EntityInfo;

                                    // Bind the Saved Exceptions into the Session
                                    if (result.EntityInfo.PostflightTripExceptions != null && result.EntityInfo.PostflightTripExceptions.Count > 0)
                                        Session["POEXCEPTION"] = result.EntityInfo.PostflightTripExceptions;
                                }
                                RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                            }
                        }
                    }
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Ramesh: Added to fix defect# 2175
                        //if (DateFormat != null)
                        if (!string.IsNullOrEmpty(DateFormat)) 
                        {
                            if (((TextBox)ucfromDate.FindControl("tbDate")).Text != null)
                            {
                                //((TextBox)ucfromDate.FindControl("tbDate")).Text = String.Format("{0:" + DateFormat + "}", Session[Session["TabSelect"] + "DATEFROMtbDate"].ToString());
                                 Session[Session["TabSelect"] + "DATEFROMtbDate"] = ((TextBox)ucfromDate.FindControl("tbDate")).Text.Trim();
                                 Session[string.Format("{0}" + "{1}", Session["TabSelect"], "DATEFROMtbDate")] = ((TextBox)ucfromDate.FindControl("tbDate")).Text.Trim();
                            }
                            if (((TextBox)uctoDate.FindControl("tbDate")).Text != null)
                            {
                                //((TextBox)ucfromDate.FindControl("tbToDate")).Text = String.Format("{0:" + DateFormat + "}", Session[Session["TabSelect"] + "DATETOtbDate"].ToString());
                                Session[Session["TabSelect"] + "DATETOtbDate"] = ((TextBox)uctoDate.FindControl("tbDate")).Text.Trim();
                                Session[string.Format("{0}" + "{1}", Session["TabSelect"], "DATETOtbDate")] = ((TextBox)uctoDate.FindControl("tbDate")).Text.Trim();
                            }
                        }
                        BindData(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POMain);
                }
            }
        }

        private bool DelegateFunc(GetPostflightList obj)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(obj))
            {

                bool retStatus = false;
                string tailNo = tbTailNumber.Text;
                if (obj.TailNum == tailNo)
                    retStatus = true;
                TextBox tbfromdate = (TextBox)ucfromDate.FindControl("Date");
                if (tbfromdate != null)
                {
                    DateTime strtdate = Convert.ToDateTime(tbfromdate.Text);
                    if (obj.EstDepartureDT == strtdate)
                        retStatus = true;
                }

                return retStatus;
            }
        }

        private void BindData(bool isBindRequired)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isBindRequired))
            {
                Int64 fromLog = 0;
                Int64 toLog = 0;
                Int64 clientID = 0;
                Int64 homeBaseID = 0;
                Int64 fleetID = 0;
                bool isPersonal = false;
                bool isCompleted = false;
                DateTime fromDate = DateTime.MinValue;
                DateTime toDate = DateTime.MinValue;

                if (UserPrincipal.Identity._homeBaseId != null && chkHomebase.Checked == true)
                    homeBaseID = (long)UserPrincipal.Identity._homeBaseId;

                if (!string.IsNullOrEmpty(hdnClient.Value) && hdnClient.Value.Trim() != "undefined")
                    clientID = Convert.ToInt64(hdnClient.Value);

                if (!string.IsNullOrEmpty(hdnTailNo.Value) && hdnTailNo.Value.Trim() != "undefined")
                    fleetID = Convert.ToInt64(hdnTailNo.Value);

                /*if (ChkPerTrav.Checked == true)
                    isPersonal = true;

                if (chkCompleted.Checked == true)
                    isCompleted = true;*/
                isPersonal = ChkPerTrav.Checked;
                isCompleted = chkCompleted.Checked;
                try
                {
                    TextBox tbFromDate = (TextBox)ucfromDate.FindControl("tbDate");
                    if (!string.IsNullOrEmpty(tbFromDate.Text))
                        fromDate = Convert.ToDateTime(tbFromDate.Text + " 00:00");
                }
                catch (Exception ex)
                {
                    //Manually Handled
                    fromDate = DateTime.Now;
                }

                try
                {
                    TextBox tbToDate = (TextBox)uctoDate.FindControl("tbDate");
                    if (!string.IsNullOrEmpty(tbToDate.Text))
                        toDate = Convert.ToDateTime(tbToDate.Text + " 00:00");
                }
                catch (Exception ex)
                {
                    //Manually Handled
                    toDate = DateTime.Now;
                }

                if (tbFromLog.Text != "")
                {
                    fromLog = Convert.ToInt64(tbFromLog.Text);
                }
                
                if (tbToLog.Text != "")
                {
                    toLog = Convert.ToInt64(tbToLog.Text);
                }

                using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
                {
                    var ObjRetval = objService.GetPoReportList(homeBaseID, clientID, fleetID, isPersonal, isCompleted, fromDate, toDate, fromLog, toLog);
                    if (ObjRetval != null && ObjRetval.ReturnFlag)
                    {
                        dgSearch.DataSource = ObjRetval.EntityList;
                        Session["POtrips"] = ObjRetval.EntityList;
                        if (isBindRequired)
                            dgSearch.Rebind();//.DataBind();
                    }
                    else
                    {
                        dgSearch.DataSource = string.Empty;
                        dgSearch.DataBind();
                    }
                }
            }
        }

        /// <summary>
        /// Method to validate ClientCodes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Client_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbcvClient.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbClientDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnClient.Value = string.Empty;
                        if (!string.IsNullOrEmpty(tbClientCode.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient ClientService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var ClientVal = ClientService.GetClientCodeList().EntityList.Where(x => x.ClientCD.ToString().ToUpper().Trim().Equals(tbClientCode.Text.ToUpper().Trim())).ToList();
                                if (ClientVal != null && ClientVal.Count > 0)
                                {
                                    tbClientCode.Text = ClientVal[0].ClientCD;
                                    if (((FlightPakMasterService.Client)ClientVal[0]).ClientDescription != null)
                                        lbClientDesc.Text = System.Web.HttpUtility.HtmlEncode(((FlightPakMasterService.Client)ClientVal[0]).ClientDescription.ToUpper());
                                    hdnClient.Value = ((FlightPakMasterService.Client)ClientVal[0]).ClientID.ToString();
                                }
                                else
                                {
                                    lbcvClient.Text = System.Web.HttpUtility.HtmlEncode("Invalid Client Code.");
                                    tbClientCode.Focus();
                                }
                            }
                        }
                        BindData(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POMain);
                }
            }
        }

        /// <summary>
        /// Method to validate TailNumber
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TailNo_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbcvTailNumber.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnTailNo.Value = string.Empty;
                        if (!string.IsNullOrEmpty(tbTailNumber.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient TailService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var TailVal = TailService.GetFleetProfileList().EntityList.Where(x => x.TailNum.ToString().ToUpper().Trim().Equals(tbTailNumber.Text.ToUpper().Trim())).ToList();
                                if (TailVal != null && TailVal.Count > 0)
                                {
                                    tbTailNumber.Text = TailVal[0].TailNum;
                                    hdnTailNo.Value = ((FlightPakMasterService.Fleet)TailVal[0]).FleetID.ToString();
                                }
                                else
                                {
                                    lbcvTailNumber.Text = System.Web.HttpUtility.HtmlEncode("Invalid Aircraft Tail No.");
                                    tbTailNumber.Focus();
                                }
                            }
                        }
                        BindData(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POMain);
                }
            }
        }

        public string FormatDate(string dt)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(dt))
            {
                string FormattedDate = string.Empty;
                if (!string.IsNullOrEmpty(dt))
                {
                    DateTimeFormatInfo formatInfo = new DateTimeFormatInfo();
                    if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        formatInfo.ShortDatePattern = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                    else
                        formatInfo.ShortDatePattern = "MM/dd/yyyy"; // American, MDY

                    List<string> dtList = new List<string>();

                    char splitter;
                    if (dt.Contains("-"))
                        splitter = '-';
                    else if (dt.Contains("."))
                        splitter = '.';
                    else
                        splitter = '/';

                    switch (formatInfo.ShortDatePattern)
                    {
                        case "dd/MM/yyyy": // British, French, DMY
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[0] + "/" + dtList[2];
                            break;
                        case "dd.MM.yyyy": // German
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[0] + "/" + dtList[2];
                            break;
                        case "yyyy.MM.dd": // ANSI
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[2] + "/" + dtList[0];
                            break;
                        case "dd-MM-yyyy": // Italian
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[0] + "/" + dtList[2];
                            break;
                        case "yyyy/MM/dd": // Japan, Taiwan, YMD
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[2] + "/" + dtList[0];
                            break;
                        case "MM/dd/yyyy": // Default
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[0] + "/" + dtList[1] + "/" + dtList[2];
                            break;
                    }
                }
                return FormattedDate;
            }
        }

        public DateTime FormatDate(string Date, string Format)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Date, Format))
            {
                string DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;

                string[] DateAndTime = Date.Split(' ');

                string[] SplitDate = new string[3];
                string[] SplitFormat = new string[3];

                if (DateFormat.Contains("/"))
                {
                    SplitDate = DateAndTime[0].Split('/');
                    SplitFormat = Format.Split('/');
                }
                else if (DateFormat.Contains("-"))
                {
                    SplitDate = DateAndTime[0].Split('-');
                    SplitFormat = Format.Split('-');
                }
                else if (DateFormat.Contains("."))
                {
                    SplitDate = DateAndTime[0].Split('.');
                    SplitFormat = Format.Split('.');
                }

                int dd = 0, mm = 0, yyyy = 0;
                for (int Index = 0; Index < SplitFormat.Count(); Index++)
                {
                    if (SplitFormat[Index].ToLower().Contains("d"))
                    {
                        dd = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("m"))
                    {
                        mm = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("y"))
                    {
                        yyyy = Convert.ToInt16(SplitDate[Index]);
                    }
                }
                return new DateTime(yyyy, mm, dd);
            }
        }

        /// <summary>
        /// Method to Set Eception, Is Persoal symbols on databound 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Search_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (e.Item is GridDataItem)
                {
                    try
                    {
                        GridDataItem item = (GridDataItem)e.Item;

                        if (item["EstDepartureDT"] != null && item["EstDepartureDT"].Text != "&nbsp;")
                        {
                            DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "EstDepartureDT");
                            if (date != null)
                            {
                                String dateFormat = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", date);
                                item["EstDepartureDT"].Text = Microsoft.Security.Application.Encoder.HtmlEncode(dateFormat);
                            }
                        }

                        if (item["IsException"] != null && item["IsException"].Text.ToUpper() == "TRUE")
                        {
                            item["IsException"].Text = "!";
                            item["IsException"].ForeColor = System.Drawing.Color.Red;
                            item["IsException"].HorizontalAlign = HorizontalAlign.Center;
                        }
                        else if (item["IsException"].Text.ToUpper() == "FALSE")
                        {
                            item["IsException"].Text = string.Empty;
                        }

                        if (item["IsPersonal"] != null && item["IsPersonal"].Text.ToUpper() == "TRUE")
                        {
                            item["IsPersonal"].Text = "*";
                            item["IsPersonal"].ForeColor = System.Drawing.Color.Red;
                            item["IsPersonal"].HorizontalAlign = HorizontalAlign.Center;
                        }
                        else if (item["IsPersonal"].Text.ToUpper() == "FALSE")
                        {
                            item["IsPersonal"].Text = string.Empty;
                        }

                        if (item["IsCompleted"] != null && item["IsCompleted"].Text.ToUpper() == "TRUE")
                        {
                            item["IsCompleted"].Text = "Y";
                            item["IsCompleted"].HorizontalAlign = HorizontalAlign.Center;
                        }
                        else if (item["IsCompleted"].Text.ToUpper() == "FALSE")
                        {
                            item["IsCompleted"].Text = string.Empty;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Manually Handled
                    }
                }
            }
        }

        /// <summary>
        /// Get Client Details based on Client ID
        /// </summary>
        /// <param name="ClientID">Pass Client ID</param>
        /// <returns>Returns Client Object</returns>
        public FlightPak.Web.FlightPakMasterService.Client GetClient(Int64 ClientID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPak.Web.FlightPakMasterService.Client client = new FlightPakMasterService.Client();
                    var objRetVal = objDstsvc.GetClientCodeList();
                    List<FlightPakMasterService.Client> ClientList = new List<FlightPakMasterService.Client>();

                    if (objRetVal.ReturnFlag)
                    {
                        ClientList = objRetVal.EntityList.Where(x => x.ClientID == ClientID).ToList();
                        if (ClientList != null && ClientList.Count > 0)
                        {
                            client = ClientList[0];
                        }
                        else
                            client = null;
                    }
                    return client;
                }
            }

        }

        protected void Yes_Click(object sender, EventArgs e)
        {
            GridDataItem item = (GridDataItem)dgSearch.SelectedItems[0];
            POLogID = Convert.ToInt64(item.GetDataKeyValue("POLogID").ToString());

            Int64 convTripNum = 0;
            Int64 LogNum = 0;
            if (Int64.TryParse(item["LogNum"].Text, out convTripNum))
                LogNum = convTripNum;
            Int64 clientID = 0;
            Int64 homeBaseID = 0;
            Int64 fleetID = 0;
            bool isPersonal = false;
            bool isCompleted = false;
            DateTime fromDate = DateTime.MinValue;
            DateTime toDate = DateTime.MinValue;
            Int64 fromLog = 0;
            Int64 toLog = 0;

            if (UserPrincipal.Identity._homeBaseId != null && chkHomebase.Checked == true)
                homeBaseID = (long)UserPrincipal.Identity._homeBaseId;

            if (!string.IsNullOrEmpty(hdnClient.Value) && hdnClient.Value.Trim() != "undefined")
                clientID = Convert.ToInt64(hdnClient.Value);

            if (!string.IsNullOrEmpty(hdnTailNo.Value) && hdnTailNo.Value.Trim() != "undefined")
                fleetID = Convert.ToInt64(hdnTailNo.Value);

            if (ChkPerTrav.Checked == true)
                isPersonal = true;

            if (chkCompleted.Checked == true)
                isCompleted = true;

            try
            {
                TextBox tbFromDate = (TextBox)ucfromDate.FindControl("tbDate");
                if (!string.IsNullOrEmpty(tbFromDate.Text))
                    fromDate = Convert.ToDateTime(tbFromDate.Text + " 00:00");
            }
            catch (Exception ex)
            {
                //Manually Handled
                fromDate = DateTime.Now;
            }

            try
            {
                TextBox tbToDate = (TextBox)uctoDate.FindControl("tbDate");
                if (!string.IsNullOrEmpty(tbToDate.Text))
                    toDate = Convert.ToDateTime(tbToDate.Text + " 00:00");
            }
            catch (Exception ex)
            {
                //Manually Handled
                toDate = DateTime.Now;
            }

            using (PostflightServiceClient PosfSvc = new PostflightServiceClient())
            {
                var objRetVal = PosfSvc.GetPoReportList(homeBaseID, clientID, fleetID, isPersonal, isCompleted, fromDate, toDate, fromLog, toLog);

                // GetPostflightList Pologlst = new GetPostflightList();

                if (objRetVal.ReturnFlag)
                {
                    PostflightMain obj = new PostflightMain();
                    GridDataItem dataitem = (GridDataItem)dgSearch.SelectedItems[0];
                    obj.POLogID = Convert.ToInt64(dataitem.GetDataKeyValue("POLogID").ToString());
                    var result = PosfSvc.GetTrip(obj);

                    if (result.ReturnFlag == true)
                    {
                        Session["POSTFLIGHTMAIN"] = (PostflightMain)result.EntityInfo;

                        // Bind the Saved Exceptions into the Session
                        if (result.EntityInfo.PostflightTripExceptions != null && result.EntityInfo.PostflightTripExceptions.Count > 0)
                            Session["POEXCEPTION"] = result.EntityInfo.PostflightTripExceptions;
                    }
                    RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                }
            }
        }

        protected void No_Click(object sender, EventArgs e)
        {
            RadAjaxManager1.ResponseScripts.Add(@"CloseRadWindow('CloseWindow');");
        }

        protected void FromLog_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //lbcvFromLog.Text = string.Empty;
                        hdnFromLogID.Value = string.Empty;
                        if (tbFromLog.Text.Trim() != string.Empty)
                        {
                            using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
                            {
                                PostflightMain POtrips = new PostflightMain();
                                POtrips.LogNum = Convert.ToInt64(tbFromLog.Text);

                                var objRetVal = objService.GetTrip(POtrips).EntityInfo;
                                if (objRetVal != null)
                                {
                                    hdnFromLogID.Value = objRetVal.POLogID.ToString();
                                }
                                else
                                {
                                    RadAjaxManager1.FocusControl(tbFromLog.ClientID);
                                    //lbcvFromLog.Text = "Invalid Postflight Log No.";
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void ToLog_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //lbcvToLog.Text = string.Empty;
                        hdnToLogID.Value = string.Empty;
                        if (tbToLog.Text.Trim() != string.Empty)
                        {
                            using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
                            {
                                PostflightMain POtrips = new PostflightMain();
                                POtrips.LogNum = Convert.ToInt64(tbToLog.Text);

                                var objRetVal = objService.GetTrip(POtrips).EntityInfo;
                                if (objRetVal != null)
                                {
                                    hdnToLogID.Value = objRetVal.POLogID.ToString();
                                }
                                else
                                {
                                    RadAjaxManager1.FocusControl(tbToLog.ClientID);
                                    //lbcvToLog.Text = "Invalid Postflight Log No.";
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void Search_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridHeaderItem)
            {
                GridHeaderItem item = (GridHeaderItem)e.Item;
                Label lbl = new Label();
                lbl.ID = "Label1";
                lbl.Text = "Select All";
                item["ClientSelectLog"].Controls.Add(lbl);
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool IsLogID = false;
                        if (dgSearch.Items.Count > 0)
                        {
                            if (dgSearch.SelectedItems.Count > 0)
                            {
                                string PoLogIDs = string.Empty;
                                foreach (GridDataItem item in dgSearch.SelectedItems)
                                {
                                    PoLogIDs += item.GetDataKeyValue("POLogID").ToString() + ',';
                                }
                                PoLogIDs = PoLogIDs.Substring(0, PoLogIDs.Length - 1);

                                if (!string.IsNullOrEmpty(PoLogIDs))
                                {
                                    IsLogID = true;
                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("Please select Postflight log.", 250, 150, "AdHoc Report", "");
                                }
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Please select Postflight log.", 250, 150, "AdHoc Report", "");
                            }
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Please select Postflight log.", 250, 150, "AdHoc Report", "");
                        }

                        if (IsLogID)
                        {
                            Session["TabSelect"] = "PostFlight";

                            if (radbtnlst.SelectedValue != null)
                                Session["FORMAT"] = radbtnlst.SelectedValue;
                            else
                                Session["FORMAT"] = "CSV";

                            Session["PARAMETER"] = ParamLogNumber();
                            Session["REPORT"] = "RptPOSTAdhocInformation";
                            Response.Redirect("ReportRenderView.aspx",true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        private string ParamLogNumber()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string paramater = string.Empty;

                paramater += "UserCD=" + UserPrincipal.Identity._name.Trim();
                paramater += ";UserCustomerID=" + UserPrincipal.Identity._customerID;
                paramater += ";UserHomebaseID=" + UserPrincipal.Identity._homeBaseId;

                if (chkLegs.Checked)
                    paramater += ";IsLeg=" + "true";
                else
                    paramater += ";IsLeg=" + "false";

                if (chkPax.Checked)
                    paramater += ";IsPax=" + "true";
                else
                    paramater += ";IsPax=" + "false";

                if (chkCrew.Checked)
                    paramater += ";IsCrew=" + "true";
                else
                    paramater += ";IsCrew=" + "false";

                if (chkExpenses.Checked)
                    paramater += ";IsExpense=" + "true";
                else
                    paramater += ";IsExpense=" + "false";

                string PoLogIDs = string.Empty;

                //List<GetPOReportSearch> POMains = new List<GetPOReportSearch>();
                //if (Session["POtrips"] != null)
                //{
                //    POMains = (List<GetPOReportSearch>)Session["POtrips"];
                //}
                //if (POMains != null && POMains.Count > 0)
                //{
                //    foreach (GetPOReportSearch POTrips in POMains)
                //    {
                //        GridClientSelectColumn selCol = new GridClientSelectColumn();
                //        selCol = (GridClientSelectColumn)dgSearch.MasterTableView.GetColumn("ClientSelectLog");
                //        if (selCol != null && selCol.Selected == true)
                //        {
                //            GridDataItem Item = (GridDataItem)dgSearch.SelectedItems[0];
                //            PoLogIDs += Item.GetDataKeyValue("POLogID").ToString();
                //        }
                //    }
                //}

                if (dgSearch.SelectedItems.Count > 0)
                {
                    foreach (GridDataItem item in dgSearch.SelectedItems)
                    {
                        PoLogIDs += item.GetDataKeyValue("POLogID").ToString() + ',';
                    }
                    PoLogIDs = PoLogIDs.Substring(0, PoLogIDs.Length - 1);
                }

                if (!string.IsNullOrEmpty(PoLogIDs))
                    paramater += ";PoLogIDs=" + PoLogIDs;
                                
                return paramater;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        chkLegs.Checked = false;
                        chkPax.Checked = false;
                        chkExpenses.Checked = false;
                        chkCrew.Checked = false;

                        chkPax.Enabled = false;
                        chkExpenses.Enabled = false;
                        chkCrew.Enabled = false;

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnPost_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "PostFlight";                                                
                        Response.Redirect("ReportViewer.aspx?xmlFilename=POSTAircraftArrivals.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }            

        private void SelectTab()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string xmltab = string.Empty;
                xmltab = "POSTAdhocExport.xml";
            }
        }        

        protected void dgSearch_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgSearch, Page.Session);
        }
    }
}