﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.CharterQuoteService;
namespace FlightPak.Web.Views.Reports
{
    public partial class FileFormatReport : BaseSecuredPage
    {
        private ExceptionManager exManager;
        string ModuleNameConstants = "Charter Quote Report Details";

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, int> dicRptType = new Dictionary<string, int>();
                        if (Session["SnxmlFilename"] != null)
                        {
                            dicRptType = (Dictionary<string, int>)Session["SnxmlFilename"];
                        }
                        if (Session["SnxmlFilename"] != null)
                        {
                            //int title = 0;
                            //title = (int)Session["SnxmlFilename"];
                            if (dicRptType["xmlFilename"] == 1)
                                Page.Header.Title = "Itinerary";
                            if (dicRptType["xmlFilename"] == 2)
                                Page.Header.Title = "Notes/Alerts";
                            if (dicRptType["xmlFilename"] == 3)
                                Page.Header.Title = "Aircraft Profile";
                            if (dicRptType["xmlFilename"] == 4)
                                Page.Header.Title = "Trip Status";
                        }
                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            //CheckAutorization(Permission.Database.ViewCompanyProfileCatalog);
                            hdnIsPrint.Value = string.Empty;

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }

        protected void dgCharterQuoteDetail_BindData_old(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, int> dicRptType = new Dictionary<string, int>();
                        if (Session["SnxmlFilename"] != null)
                        {
                            dicRptType = (Dictionary<string, int>)Session["SnxmlFilename"];
                        }
                        Int64 CQReportHeaderID = 0;
                        if (!string.IsNullOrEmpty(Request.QueryString["CQReportHeaderID"]))
                        {
                            CQReportHeaderID = Convert.ToInt64(Request.QueryString["CQReportHeaderID"]);
                        }

                        GetCQReportDetail oCQReportDetail = new GetCQReportDetail();
                        using (CharterQuoteService.CharterQuoteServiceClient ObjService = new CharterQuoteService.CharterQuoteServiceClient())
                        {
                            var ObjRetVal = ObjService.GetCQReportDetail().EntityList.Where(x => x.CQReportHeaderID == CQReportHeaderID
                                                                                                    && x.ReportNumber == dicRptType["xmlFilename"]
                                                                                                    && x.ParameterCD != null).OrderBy(x => x.ParamterDescription).ToList();

                            dgCharterQuoteDetail.DataSource = ObjRetVal;

                            if (ObjRetVal.Count() != 0)
                            {
                                btnPrint.Visible = true;
                                btnSave.Visible = true;
                            }
                            else
                            {
                                btnPrint.Visible = false;
                                btnSave.Visible = false;
                            }

                            Session["lstCQReportDetail"] = ObjRetVal;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }

        protected void dgCharterQuoteDetail_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, int> dicRptType = new Dictionary<string, int>();
                        if (Session["SnxmlFilename"] != null)
                        {
                            dicRptType = (Dictionary<string, int>)Session["SnxmlFilename"];
                        }
                        Int64 CQReportHeaderID = 0;
                        if (!string.IsNullOrEmpty(Request.QueryString["CQReportHeaderID"]))
                        {
                            CQReportHeaderID = Convert.ToInt64(Request.QueryString["CQReportHeaderID"]);
                        }

                        GetCQReportDetail oCQReportDetail = new GetCQReportDetail();
                        List<GetCQReportDetail> lstCQReportDetail = new List<GetCQReportDetail>();
                        using (CharterQuoteService.CharterQuoteServiceClient ObjService = new CharterQuoteService.CharterQuoteServiceClient())
                        {
                            if (dicRptType["xmlFilename"] != 4)
                            {
                                var ObjRetVal = ObjService.GetCQReportDetail().EntityList.Where(x => x.CQReportHeaderID == CQReportHeaderID
                                                                && x.ReportNumber == dicRptType["xmlFilename"]
                                                                && x.ParameterCD != null
                                                                ).OrderBy(x => x.ParamterDescription).ToList();
                                if (ObjRetVal.Count != 0)
                                {
                                    lstCQReportDetail = ObjRetVal;
                                }
                                //dgCharterQuoteDetail.MasterTableView.ShowFooter = false;
                            }
                            else
                            {
                                var ObjRetVal = ObjService.GetCQReportDetail().EntityList.Where(x => x.CQReportHeaderID == CQReportHeaderID
                                                                && x.ReportNumber == dicRptType["xmlFilename"]
                                                                && x.ParameterCD != null
                                                                && (x.ParameterCD == "BLANK" || x.ParameterCD == "SIGNATURE" ||
                                                                x.ParameterCD == "MINUTES" || x.ParameterCD == "COMMENTS"
                                                                || x.ParameterCD == "DISPATCHNO" || x.ParameterCD == "LANDSCAPE")
                                                                ).OrderBy(x => x.ParamterDescription).ToList();
                                if (ObjRetVal.Count != 0)
                                {
                                    lstCQReportDetail = ObjRetVal;
                                }
                                //dgCharterQuoteDetail.MasterTableView.ShowFooter = true;
                            }
                        }
                        dgCharterQuoteDetail.DataSource = lstCQReportDetail;
                        if (lstCQReportDetail.Count != 0)
                        {
                            btnPrint.Visible = true;
                            btnSave.Visible = true;
                        }
                        else
                        {
                            btnPrint.Visible = false;
                            btnSave.Visible = false;
                        }
                        Session["lstCQReportDetail"] = lstCQReportDetail;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }

        protected void dgCharterQuoteDetail_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, int> dicRptType = new Dictionary<string, int>();
                        if (Session["SnxmlFilename"] != null)
                        {
                            dicRptType = (Dictionary<string, int>)Session["SnxmlFilename"];
                        }
                        if (e.Item is GridCommandItem)
                        {
                            GridCommandItem Item = (GridCommandItem)e.Item;
                            if (dicRptType["xmlFilename"] != null)
                            {
                                if (dicRptType["xmlFilename"] == 4)
                                {
                                    ((Label)Item.FindControl("lbCustom1")).Visible = true;
                                }
                                else
                                {
                                    ((Label)Item.FindControl("lbCustom1")).Visible = false;
                                }
                            }
                        }
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            if (Item.GetDataKeyValue("ParamterType") != null)
                            {
                                if (Item.GetDataKeyValue("ParamterType").ToString().Trim().ToUpper() == "L")
                                {
                                    ((TextBox)Item.FindControl("tbIsSelected")).Enabled = true;

                                    if (Convert.ToBoolean(Item.GetDataKeyValue("ParameterLValue")) == true)
                                    {
                                        ((TextBox)Item.FindControl("tbIsSelected")).Text = "Y";
                                    }
                                    else if (Convert.ToBoolean(Item.GetDataKeyValue("ParameterLValue")) == false)
                                    {
                                        ((TextBox)Item.FindControl("tbIsSelected")).Text = "N";
                                    }
                                    else
                                    {
                                        ((TextBox)Item.FindControl("tbIsSelected")).Text = string.Empty;
                                    }

                                    ((TextBox)Item.FindControl("tbNumber")).Enabled = false;
                                    //Added by Ramesh on 4th Nov 2013 to fix defect# 7175
                                    ((TextBox)Item.FindControl("tbNumber")).Text = string.Empty;

                                    ((TextBox)Item.FindControl("tbMessage")).Enabled = false;

                                    //Commented by Ramesh on 4th Nov 2013 to fix defect# 7175
                                    //if (!string.IsNullOrEmpty(((TextBox)Item.FindControl("tbMessage")).Text))
                                    //{
                                    //    ((TextBox)Item.FindControl("tbMessage")).Text = "Message";
                                    //}
                                    //else
                                    //{
                                        ((TextBox)Item.FindControl("tbMessage")).Text = string.Empty;
                                    //}
                                }
                                else if (Item.GetDataKeyValue("ParamterType").ToString().Trim().ToUpper() == "N")
                                {
                                    ((TextBox)Item.FindControl("tbIsSelected")).Enabled = false;
                                    ((TextBox)Item.FindControl("tbIsSelected")).Text = string.Empty;
                                    ((TextBox)Item.FindControl("tbNumber")).Enabled = true;
                                    ((TextBox)Item.FindControl("tbMessage")).Enabled = false;

                                    //Commented by Ramesh on 4th Nov 2013 to fix defect# 7175
                                    //if (!string.IsNullOrEmpty(((TextBox)Item.FindControl("tbMessage")).Text))
                                    //{
                                    //    ((TextBox)Item.FindControl("tbMessage")).Text = "Message";
                                    //}
                                    //else
                                    //{
                                        ((TextBox)Item.FindControl("tbMessage")).Text = string.Empty;
                                    //}
                                    if (Item.GetDataKeyValue("ParameterSize") != null)
                                    {
                                        string WarningMessage = string.Empty;
                                        string LowerBound = string.Empty, UpperBound = string.Empty;
                                        string[] SplitSize = new string[2];
                                        SplitSize = Item.GetDataKeyValue("ParameterSize").ToString().Trim().Split('-');

                                        if (Item.GetDataKeyValue("WarningMessage") != null)
                                        {
                                            WarningMessage = Item.GetDataKeyValue("WarningMessage").ToString();
                                        }

                                        if ((!string.IsNullOrEmpty(SplitSize[0])) && (!string.IsNullOrEmpty(SplitSize[1])))
                                        {
                                            LowerBound = SplitSize[0];
                                            UpperBound = SplitSize[1];
                                            ((TextBox)Item.FindControl("tbNumber")).Attributes.Add("onblur", "CheckRange('" + ((TextBox)Item.FindControl("tbNumber")).ClientID + "','"
                                                                                                                            + UpperBound + "','" + LowerBound + "','" + WarningMessage + "')");
                                        }
                                    }
                                }
                                else
                                {
                                    ((TextBox)Item.FindControl("tbIsSelected")).Enabled = false;
                                    ((TextBox)Item.FindControl("tbIsSelected")).Text = string.Empty;
                                    ((TextBox)Item.FindControl("tbNumber")).Enabled = false;
                                    //Added by Ramesh on 4th Nov 2013 to fix defect# 7175
                                    ((TextBox)Item.FindControl("tbNumber")).Text = string.Empty;

                                    ((TextBox)Item.FindControl("tbMessage")).Enabled = true;
                                    ((TextBox)Item.FindControl("tbMessage")).ReadOnly = true;
                                    if (!string.IsNullOrEmpty(((TextBox)Item.FindControl("tbMessage")).Text))
                                    {
                                        ((TextBox)Item.FindControl("tbMessage")).Text = "Message";
                                    }
                                    else
                                    {
                                        ((TextBox)Item.FindControl("tbMessage")).Text = string.Empty;
                                    }
                                }
                                //Commented by Ramesh on 4th Nov 2013 to fix defect# 7175
                                //if (Convert.ToBoolean(Item.GetDataKeyValue("ParameterLValue")) == true)
                                //{
                                //    ((TextBox)Item.FindControl("tbIsSelected")).Text = "Y";
                                //}
                                //else if (Convert.ToBoolean(Item.GetDataKeyValue("ParameterLValue")) == false)
                                //{
                                //    ((TextBox)Item.FindControl("tbIsSelected")).Text = "N";
                                //}
                                //else
                                //{
                                //    ((TextBox)Item.FindControl("tbIsSelected")).Text = string.Empty;
                                //}
                                //HttpUtility.HtmlEncode("ParameterLValue");
                            }
                            // Added by Ramesh on 4th Nov 2013 to fix defect# 7175
                            if (((TextBox)Item.FindControl("tbMessage")).Enabled == true)
                            {
                                ((TextBox)Item.FindControl("tbMessage")).ToolTip = "Double Click to Edit or View the Message";
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgCharterQuoteDetail;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }

        private void DeleteCQReportDetail()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (CharterQuoteService.CharterQuoteServiceClient objService = new CharterQuoteService.CharterQuoteServiceClient())
                {
                    CQReportDetail objCQReportDetail;
                    foreach (GridDataItem Item in dgCharterQuoteDetail.Items)
                    {
                        objCQReportDetail = new CQReportDetail();
                        if (Item.GetDataKeyValue("CQReportDetailID") != null)
                        {
                            objCQReportDetail.CQReportDetailID = Convert.ToInt64(Item.GetDataKeyValue("CQReportDetailID").ToString());
                        }
                        objCQReportDetail.IsDeleted = true;
                        objService.DeleteCQReportDetail(objCQReportDetail);
                    }
                }
            }
        }

        private List<CharterQuoteService.CQReportDetail> SaveCQReportDetail(string OpType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (CharterQuoteService.CharterQuoteServiceClient objService = new CharterQuoteService.CharterQuoteServiceClient())
                {
                    List<CQReportDetail> oCQReportDetail = new List<CQReportDetail>();
                    CQReportDetail objCQReportDetail;
                    foreach (GridDataItem Item in dgCharterQuoteDetail.Items)
                    {
                        objCQReportDetail = new CharterQuoteService.CQReportDetail();
                        if (Item.GetDataKeyValue("CQReportDetailID") != null)
                        {
                            objCQReportDetail.CQReportDetailID = Convert.ToInt64(Item.GetDataKeyValue("CQReportDetailID").ToString());
                        }
                        if (Item.GetDataKeyValue("CustomerID") != null)
                        {
                            objCQReportDetail.CustomerID = Convert.ToInt64(Item.GetDataKeyValue("CustomerID").ToString());
                        }
                        if (Item.GetDataKeyValue("CQReportHeaderID") != null)
                        {
                            objCQReportDetail.CQReportHeaderID = Convert.ToInt64(Item.GetDataKeyValue("CQReportHeaderID").ToString());
                        }
                        if (Item.GetDataKeyValue("ReportNumber") != null)
                        {
                            objCQReportDetail.ReportNumber = Convert.ToInt32(Item.GetDataKeyValue("ReportNumber").ToString());
                        }
                        if (Item.GetDataKeyValue("ReportDescription") != null)
                        {
                            objCQReportDetail.ReportDescription = Item.GetDataKeyValue("ReportDescription").ToString();
                        }
                        //if (!string.IsNullOrEmpty(((TextBox)Item.FindControl("tbIsSelected")).Text))
                        //{
                        //    if (((TextBox)Item.FindControl("tbIsSelected")).Text == "Y")
                        //    {
                        //        objTripSheetReportDetail.IsSelected = true;
                        //    }
                        //    else
                        //    {
                        //        objTripSheetReportDetail.IsSelected = false;
                        //    }
                        //}
                        if (Item.GetDataKeyValue("IsSelected") != null)
                        {
                            objCQReportDetail.IsSelected = Convert.ToBoolean(Item.GetDataKeyValue("IsSelected").ToString());
                        }
                        if (Item.GetDataKeyValue("Copies") != null)
                        {
                            objCQReportDetail.Copies = Convert.ToInt32(Item.GetDataKeyValue("Copies").ToString());
                        }
                        if (Item.GetDataKeyValue("ReportProcedure") != null)
                        {
                            objCQReportDetail.ReportProcedure = Item.GetDataKeyValue("ReportProcedure").ToString();
                        }
                        if (Item.GetDataKeyValue("ParameterCD") != null)
                        {
                            objCQReportDetail.ParameterCD = Item.GetDataKeyValue("ParameterCD").ToString();
                        }
                        if (Item.GetDataKeyValue("ParamterDescription") != null)
                        {
                            objCQReportDetail.ParamterDescription = Item.GetDataKeyValue("ParamterDescription").ToString();
                        }
                        if (Item.GetDataKeyValue("ParamterType") != null)
                        {
                            objCQReportDetail.ParamterType = Item.GetDataKeyValue("ParamterType").ToString();
                        }
                        if (Item.GetDataKeyValue("ParameterWidth") != null)
                        {
                            objCQReportDetail.ParameterWidth = Convert.ToInt32(Item.GetDataKeyValue("ParameterWidth").ToString());
                        }
                        if (Item.GetDataKeyValue("ParameterCValue") != null)
                        {
                            objCQReportDetail.ParameterCValue = Item.GetDataKeyValue("ParameterCValue").ToString();
                        }
                        if (!string.IsNullOrEmpty(((TextBox)Item.FindControl("tbIsSelected")).Text))
                        {
                            if (((TextBox)Item.FindControl("tbIsSelected")).Text.ToUpper() == "Y")
                            {
                                objCQReportDetail.ParameterLValue = true;
                            }
                            else
                            {
                                objCQReportDetail.ParameterLValue = false;
                            }
                            //objTripSheetReportDetail.ParameterLValue = Convert.ToBoolean(Item.GetDataKeyValue("ParameterLValue").ToString());
                        }
                        //if (Item.GetDataKeyValue("ParameterNValue") != null)
                        //{
                        //    objTripSheetReportDetail.ParameterNValue = Convert.ToInt32(Item.GetDataKeyValue("ParameterNValue").ToString());
                        //}
                        if (!string.IsNullOrEmpty(((TextBox)Item.FindControl("tbNumber")).Text))
                        {
                            objCQReportDetail.ParameterNValue = Convert.ToInt32(((TextBox)Item.FindControl("tbNumber")).Text);
                        }
                        if (!string.IsNullOrEmpty(((TextBox)Item.FindControl("tbMessage")).Text))
                        {
                            objCQReportDetail.ParameterValue = Item.GetDataKeyValue("ParameterValue").ToString();
                        }
                        if (Item.GetDataKeyValue("ReportOrder") != null)
                        {
                            objCQReportDetail.ReportOrder = Convert.ToInt32(Item.GetDataKeyValue("ReportOrder").ToString());
                        }
                        if (Item.GetDataKeyValue("LastUpdUID") != null)
                        {
                            objCQReportDetail.LastUpdUID = Item.GetDataKeyValue("LastUpdUID").ToString();
                        }
                        if (Item.GetDataKeyValue("LastUpdTS") != null)
                        {
                            objCQReportDetail.LastUpdTS = Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString());
                        }
                        if (Item.GetDataKeyValue("IsDeleted") != null)
                        {
                            objCQReportDetail.IsDeleted = Convert.ToBoolean(Item.GetDataKeyValue("IsDeleted").ToString());
                        }
                        if (Item.GetDataKeyValue("ParameterSize") != null)
                        {
                            objCQReportDetail.ParameterSize = Item.GetDataKeyValue("ParameterSize").ToString();
                        }
                        if (Item.GetDataKeyValue("WarningMessage") != null)
                        {
                            objCQReportDetail.WarningMessage = Item.GetDataKeyValue("WarningMessage").ToString();
                        }
                        oCQReportDetail.Add(objCQReportDetail);
                        if (OpType == "Save")
                        {
                            objService.AddCQReportDetail(objCQReportDetail);
                            hdnIsPrint.Value = "";
                        }
                    }
                    if (OpType == "Print")
                    {
                        Session["CQReportDetailList"] = oCQReportDetail;
                        InjectScript.Text = "<script type='text/javascript'>Close('1')</script>";
                        hdnIsPrint.Value = "1";
                    }
                    else
                    {
                        InjectScript.Text = "<script type='text/javascript'>Close('')</script>";
                    }
                    return oCQReportDetail;
                }
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DeleteCQReportDetail();
                        SaveCQReportDetail("Save");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }

        protected void btnPrint_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveCQReportDetail("Print");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }

        protected void btnFRSaveMessage_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        List<GetCQReportDetail> oCQReportDetail = new List<GetCQReportDetail>();
                        if (!string.IsNullOrEmpty(hdnFRMessageID.Value))
                        {
                            oCQReportDetail = ((List<GetCQReportDetail>)Session["lstCQReportDetail"]).Where(x => x.CQReportDetailID == Convert.ToInt64(hdnFRMessageID.Value)).ToList();
                            if ((oCQReportDetail.Count != 0) && (oCQReportDetail.Count == 1))
                            {
                                CQReportDetail objCQReportDetail = new CQReportDetail();
                                objCQReportDetail.Copies = oCQReportDetail[0].Copies;
                                objCQReportDetail.CustomerID = oCQReportDetail[0].CustomerID;
                                objCQReportDetail.IsDeleted = oCQReportDetail[0].IsDeleted;
                                objCQReportDetail.IsSelected = oCQReportDetail[0].IsSelected;
                                objCQReportDetail.LastUpdTS = oCQReportDetail[0].LastUpdTS;
                                objCQReportDetail.LastUpdUID = oCQReportDetail[0].LastUpdUID;
                                objCQReportDetail.ParameterCValue = oCQReportDetail[0].ParameterCValue;
                                objCQReportDetail.ParamterDescription = oCQReportDetail[0].ParamterDescription;
                                objCQReportDetail.ParameterLValue = oCQReportDetail[0].ParameterLValue;
                                objCQReportDetail.ParameterNValue = oCQReportDetail[0].ParameterNValue;
                                objCQReportDetail.ParamterType = oCQReportDetail[0].ParamterType;
                                objCQReportDetail.ParameterCD = oCQReportDetail[0].ParameterCD;
                                objCQReportDetail.ParameterWidth = oCQReportDetail[0].ParameterWidth;
                                objCQReportDetail.ReportDescription = oCQReportDetail[0].ReportDescription;
                                objCQReportDetail.ReportNumber = oCQReportDetail[0].ReportNumber;
                                objCQReportDetail.ReportOrder = oCQReportDetail[0].ReportOrder;
                                objCQReportDetail.ReportProcedure = oCQReportDetail[0].ReportProcedure;
                                objCQReportDetail.CQReportHeaderID = oCQReportDetail[0].CQReportHeaderID;
                                objCQReportDetail.CQReportDetailID = oCQReportDetail[0].CQReportDetailID;
                                objCQReportDetail.ParameterValue = tbFRMessage.Text;
                                objCQReportDetail.ParameterSize = oCQReportDetail[0].ParameterSize;
                                objCQReportDetail.WarningMessage = oCQReportDetail[0].WarningMessage;
                                using (CharterQuoteService.CharterQuoteServiceClient objService = new CharterQuoteService.CharterQuoteServiceClient())
                                {
                                    objService.AddCQReportDetail(objCQReportDetail);
                                }
                            }
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "ClosePopup", "CloseMessagePopup();", true);
                            dgCharterQuoteDetail.Rebind();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }

        protected void chkSelectAll_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string SelectedValue = string.Empty;
                        if (chkSelectAll.Checked == true)
                        {
                            SelectedValue = "Y";
                        }
                        else
                        {
                            SelectedValue = "N";
                        }
                        foreach (GridDataItem Item in dgCharterQuoteDetail.Items)
                        {
                            ((TextBox)Item.FindControl("tbIsSelected")).Text = SelectedValue;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants);
                }
            }
        }
    }
}