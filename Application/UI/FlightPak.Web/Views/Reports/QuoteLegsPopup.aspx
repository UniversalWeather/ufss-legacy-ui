﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuoteLegsPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Reports.QuoteLegsPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Quote Legs</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript">

                var oArg = new Object();
                var grid = $find("<%= dgCharterLeg.ClientID %>");

                //this function is used to set the frame element
                function GetRadWindow() {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow;
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                    return oWindow;
                }

                function returnToParent() {
                    //create the argument that will be returned to the parent page
                    oArg = new Object();
                    grid = $find("<%= dgCharterLeg.ClientID %>");
                    var MasterTable = grid.get_masterTableView();
                    var selectedRows = MasterTable.get_selectedItems();
                    var SelectedMain = false;
                    if (selectedRows.length > 0) {
                        for (var i = 0; i < selectedRows.length; i++) {
                            var row = selectedRows[i];
                            var cell1 = MasterTable.getCellByColumnUniqueName(row, "LegNUM");
                            var cell2 = MasterTable.getCellByColumnUniqueName(row, "CQLegID");

                            if (i == 0) {
                                oArg.LegNum = cell1.innerHTML + ",";
                                oArg.LegID = cell2.innerHTML + ",";
                            }
                            else {
                                oArg.LegNum += cell1.innerHTML + ",";
                                oArg.LegID += cell2.innerHTML + ",";
                            }
                        }
                    }
                    else {
                        oArg.LegNum = "";
                        oArg.LegID = "";
                    }

                    if (oArg.LegNum != "")
                        oArg.LegNum = oArg.LegNum.substring(0, oArg.LegNum.length - 1)
                    else
                        oArg.LegNum = "";

                    if (oArg.LegID != "")
                        oArg.LegID = oArg.LegID.substring(0, oArg.LegID.length - 1)
                    else
                        oArg.LegID = "";

                    oArg.Arg1 = oArg.LegNum;
                    oArg.CallingButton = "LegNum";

                    oArg.Arg2 = oArg.LegID;
                    oArg.CallingButton = "LegID";

                    var oWnd = GetRadWindow();
                    if (oArg) {
                        oWnd.close(oArg);
                    }
                }

                //this function is used to close this window
                function Close() {
                    GetRadWindow().Close();
                }
            </script>
        </telerik:RadCodeBlock>
        <div class="divGridPanel">
            <asp:ScriptManager ID="scr1" runat="server">
            </asp:ScriptManager>
            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
                <AjaxSettings>
                    <telerik:AjaxSetting AjaxControlID="dgCharterLeg">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="dgCharterLeg" LoadingPanelID="RadAjaxLoadingPanel1" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                </AjaxSettings>
            </telerik:RadAjaxManager>
            <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
            <telerik:RadGrid ID="dgCharterLeg" runat="server" AllowMultiRowSelection="false"
                AllowSorting="false" AutoGenerateColumns="false" Height="330px" AllowPaging="false"
                Width="500px" OnNeedDataSource="dgCharterLeg_BindData" AllowFilteringByColumn="false">
                <MasterTableView CommandItemDisplay="Bottom" AllowFilteringByColumn="false">
                    <Columns>
                        <telerik:GridBoundColumn DataField="CQLegID" HeaderText="CQLegID" ShowFilterIcon="false"
                            AllowFiltering="false" Display="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LegNUM" HeaderText="Leg No." ShowFilterIcon="false"
                            AllowFiltering="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Description" HeaderText="Description" ShowFilterIcon="false"
                            AllowFiltering="false">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <CommandItemTemplate>
                        <div class="grd_ok">
                            <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                                Ok</button>
                            <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings>
                    <ClientEvents OnRowDblClick="returnToParent" />
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
        </div>
    </div>
    </form>
</body>
</html>
