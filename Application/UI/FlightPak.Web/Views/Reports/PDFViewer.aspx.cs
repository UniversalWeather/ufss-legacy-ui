﻿using System.Collections;
using FlightPak.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightPak.Web.Views.Reports
{
    public partial class PDFViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static object GetBase64ReportData()
        {
            var result = new FSSOperationResult<object>();
            var Base64String = HttpContext.Current.Session["Base64String"];
            HttpContext.Current.Session.Remove("Base64String");
            result.Success = true;
            result.StatusCode = String.IsNullOrEmpty(Convert.ToString(Base64String)) ? HttpStatusCode.NoContent : HttpStatusCode.OK;
            result.Result = Base64String;
            return result;
        }
    }
}