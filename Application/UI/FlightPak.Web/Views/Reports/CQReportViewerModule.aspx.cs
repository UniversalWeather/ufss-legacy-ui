﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Xml.Linq;
using System.IO;
using System.Globalization;
using System.Text;
using Telerik.Web.UI;
using System.Xml;
using System.Drawing;
using System.Data;
using FlightPak.Web.Framework.Prinicipal;
//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using FlightPak.Web.CharterQuoteService;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers; //ajss\

namespace FlightPak.Web.Views.Reports
{
    public partial class CQReportViewerModule : BaseSecuredPage
    {
        //TODO Server.MapPath
        string urlBase = "Config\\";
        public Dictionary<string, string> dicColumnList = new Dictionary<string, string>();
        public Dictionary<string, string> dicSelectedList = new Dictionary<string, string>();
        public Dictionary<string, string> dicSelectedListsort = new Dictionary<string, string>();
        public Dictionary<string, string> dicSelectedListsortSel = new Dictionary<string, string>();
        public Dictionary<string, string> dicColumnListsort = new Dictionary<string, string>();
        
        public Dictionary<string, string> dicSourceList = new Dictionary<string, string>();
        public Dictionary<string, string> dicDestinationList = new Dictionary<string, string>();
        public Dictionary<string, string> dicSourceSortList = new Dictionary<string, string>();
        public Dictionary<string, string> dicDestinationSortList = new Dictionary<string, string>();
        public Dictionary<string, string> dicEmptyList = new Dictionary<string, string>();

        public FlightPakMasterService.Company CompanySett = new FlightPakMasterService.Company();
        string filename = string.Empty;
        private ExceptionManager exManager;
        string ReportName = string.Empty;
        string DateFormat = string.Empty;
        public string reportCriteriaType = string.Empty;
        public bool ValidatedReport = true;

        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Hardcoded for now to invoke from DB catalog
                        Session["TabSelect"] = "Charter";

                        if (!string.IsNullOrEmpty(Request.QueryString["TabModuleSelect"]))
                        {
                            Session["TabSelect"] = Request.QueryString["TabModuleSelect"];
                        }

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(DivExternalForm, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCQHomeBase, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCQClient, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCQUserGroup, DivExternalForm, RadAjaxLoadingPanel1);

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), DivExternalForm2, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(DivExternalForm2, DivExternalForm2, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnFileNum, DivExternalForm2, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnQuote, DivExternalForm2, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnLeg, DivExternalForm2, RadAjaxLoadingPanel1);

                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), DivExternalForm3, RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(DivExternalForm3, DivExternalForm3, RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnTailNumber, DivExternalForm3, RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnVendor, DivExternalForm3, RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnHomebase, DivExternalForm3, RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnTypeCode, DivExternalForm3, RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCustomer, DivExternalForm3, RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSalesPerson, DivExternalForm3, RadAjaxLoadingPanel1);

                        if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                        }
                        else
                        {
                            DateFormat = "MM/dd/yyyy";
                        }

                        //==Sudhakar::Disable Date Range
                        if (rdBl.Visible)
                            reportCriteriaType = rdBl.SelectedValue.ToString();
                        else
                            reportCriteriaType = "FileNumber";
                        //==============================
                        Session["TabSelect"] = "Charter";
                        //SelectTab();
                        if (!IsPostBack)
                        {
                            Wizard1.Visible = false;
                            FileExcel.Visible = false;
                            FileExcelGdl.Visible = false;
                            ExportPanal.Visible = false;
                            FileControlsDisplay();
                            //    PopupEnable();
                            OnPageLoad();   //ajss   

                            Session["SourceList"] = null;
                            Session["DestinationList"] = null;
                            Session["SourceSortList"] = null;
                            Session["DestinationSortList"] = null;

                            string fileNum = string.Empty;
                            string fileId = string.Empty;
                            string quoteNum = string.Empty;
                            string quoteId = string.Empty;
                            string isExpressQuote = "false";

                            //Sudhakar:: Populate File Number if the user is re-directed from Charter Print icon
                            if (Request.QueryString["filenum"] != null && Request.QueryString["fileid"] != null)
                            {
                                if (!string.IsNullOrEmpty(Request.QueryString["filenum"]) && (Request.QueryString["xmlFilename"] != null) && (Request.QueryString["xmlFilename"] == "CQItinerary.xml"))
                                {
                                    fileNum = Convert.ToString(Request.QueryString["filenum"]);
                                }
                                if (!string.IsNullOrEmpty(Request.QueryString["fileid"]))
                                {
                                    fileId = Request.QueryString["fileid"];
                                }
                                if (!string.IsNullOrEmpty(Request.QueryString["quotenum"]))
                                {
                                    quoteNum = Request.QueryString["quotenum"];
                                }
                                if (!string.IsNullOrEmpty(Request.QueryString["quoteid"]))
                                {
                                    quoteId = Request.QueryString["quoteid"];
                                }
                                if (!string.IsNullOrEmpty(Request.QueryString["isexpressquote"]))
                                {
                                    isExpressQuote = Request.QueryString["isexpressquote"];
                                }

                                tbFileNum.Text = fileNum;
                                hdnFileNum.Value = fileId;
                                tbQuote.Text = quoteNum;
                                hdnQuote.Value = quoteId;
                                hdnIsExpressQuote.Value = isExpressQuote;

                                //Mohan : Updated the filenum, fileid and quotenum from query string at initial loaded.
                                Session["FileDetails"] = fileNum + ":" + quoteNum + "::" + fileId + ":" + quoteId + "::" + isExpressQuote;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                    if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                    if (!string.IsNullOrEmpty(tbFileNum.Text))
                    {
                        tbQuote.Enabled = true;
                        btnQuote.Enabled = true;
                    }
                    else
                    {
                        tbQuote.Enabled = false;
                        btnQuote.Enabled = false;
                        tbLeg.Enabled = false;
                        btnLeg.Enabled = false;
                    }
                    if (Session["FileDetails"] != null)
                    {
                        string[] details = Session["FileDetails"].ToString().Split(':');
                        if (!string.IsNullOrEmpty(details[1]))
                        {
                            tbLeg.Enabled = true;
                            btnLeg.Enabled = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        public void Page_Init(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GenerateControls();
                        ProcessColumns(string.Empty);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        private void GenerateControls()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                filename = Convert.ToString(Request.QueryString["xmlFilename"]);
                if (filename != null)
                {
                    if (filename == "CQTripStatus.xml")
                        pnlGridPanel.Visible = false;

                    if (filename == "CharterMainTab.xml")
                        pnlGridPanel.Visible = false;

                    var filePath = Server.MapPath(urlBase + filename);
                    XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                    var xmlReaderObject = xmlReader.XmlCleanReader();
                    if (xmlReaderObject != null)
                    {
                        XPathDocument SearchFieldDoc = new
                        XPathDocument(xmlReaderObject);
                        XslCompiledTransform transform = new XslCompiledTransform();
                        transform.Load(Server.MapPath(urlBase + "MakeControls.xslt"));
                        StringWriter sw = new StringWriter();
                        transform.Transform(SearchFieldDoc, null, sw);
                        string result = sw.ToString();
                        result = result.Replace("xmlns:asp=\"remove\"", "");
                        Control ctrl = Page.ParseControl(result);
                        //   Report.Controls.Add(ctrl);
                        string[] strArr = GetReportTitle().Split(',');
                        if (strArr.Count() > 0)
                        {
                            lbTitle.Text = Convert.ToString(strArr[0]);
                        }
                        Controlvisible(true);
                    }
                    //Prakash 29-04-13
                    if (Session["FileDetails"] != null)
                    {
                        string[] fileDetails = Session["FileDetails"].ToString().Split(':');

                        if (!string.IsNullOrEmpty(fileDetails[0]))
                        {
                            tbFileNum.Text = fileDetails[0];
                        }
                        if (!string.IsNullOrEmpty(fileDetails[1]))
                        {
                            tbQuote.Text = fileDetails[1];
                        }
                        if (!string.IsNullOrEmpty(fileDetails[2]))
                        {
                            tbLeg.Text = fileDetails[2];
                        }
                        if (!string.IsNullOrEmpty(fileDetails[3]))
                        {
                            hdnFileNum.Value = fileDetails[3];
                        }
                        if (!string.IsNullOrEmpty(fileDetails[4]))
                        {
                            hdnQuote.Value = fileDetails[4];
                        }
                        if (!string.IsNullOrEmpty(fileDetails[5]))
                        {
                            hdnLeg.Value = fileDetails[5];
                        }
                        if (!string.IsNullOrEmpty(fileDetails[6]))
                        {
                            hdnIsExpressQuote.Value = fileDetails[6];
                        }
                        else
                            hdnIsExpressQuote.Value = "false";
                    }

                    if (Session["DateDetails"] != null)
                    {
                        List<string> dateList = new List<string>();

                        dateList = (List<string>)Session["DateDetails"];

                        TextBox tbDateFrom = (TextBox)DateFrom.FindControl("tbDate");
                        tbDateFrom.Text = dateList[0];
                        TextBox tbDateTo = (TextBox)DateTo.FindControl("tbDate");
                        tbDateTo.Text = dateList[1];
                        tbTailNumber.Text = dateList[2];
                        hdnTailNumber.Value = dateList[3];
                        tbVendorCode.Text = dateList[4];
                        hdnVendorCode.Value = dateList[5];
                        tbHomebase.Text = dateList[6];
                        hdnHomebase.Value = dateList[7];
                        tbTypeCode.Text = dateList[8];
                        hdnTypeCode.Value = dateList[9];
                        tbCustomer.Text = dateList[10];
                        hdnCustomer.Value = dateList[11];
                        tbSalesPerson.Text = dateList[12];
                        hdnSalesPerson.Value = dateList[13];
                    }
                    //Prakash 29-04-13
                }
                else
                {
                    Controlvisible(false);
                }
            }
        }

        private string GetReportTitle()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string RptName = string.Empty;

                //MemoryStream stream = new MemoryStream(Encoding.Default.GetBytes(Server.MapPath(urlBase + filename)));
                //XmlReaderSettings settings = new XmlReaderSettings();

                //// allow entity parsing but do so more safely
                //settings.DtdProcessing = DtdProcessing.Prohibit;
                //settings.MaxCharactersFromEntities = 1024;
                //settings.XmlResolver = new XmlUrlResolver();

                //XmlReader reader = XmlReader.Create(stream, settings);
                //XmlDocument doc = new XmlDocument();
                //doc.Load(reader);

               
                XmlDocument doc = new XmlDocument();
                 var filePath = Server.MapPath(urlBase + filename);
                    XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                    var xmlReaderObject = xmlReader.XmlCleanReader();
                    if (xmlReaderObject != null)
                    {
                        doc.Load(xmlReaderObject);

                        XmlNodeList elemList = doc.GetElementsByTagName("Report");

                        for (int i = 0; i < elemList.Count; i++)
                        {
                            RptName = elemList[i].Attributes["displayname"].Value + "," + elemList[i].Attributes["name"].Value;
                            break;
                        }
                        return RptName;
                    }
                    return "";
            }
        }

        private void SelectTab()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string TabSelect = Convert.ToString(Session["TabSelect"]);
                pnlReportTab.Items.Clear();
                string xmltab = string.Empty;
                switch (TabSelect)
                {
                    case "PreFlight":
                        xmltab = "PreFlightMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav_active";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav";
                        break;
                    case "PostFlight":
                        xmltab = "PostFlightMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav_active";
                        btnDatabase.CssClass = "rpt_nav";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav";
                        break;
                    case "Db":
                        xmltab = "DatabaseMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav_active";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav";
                        break;
                    case "CorpReq":
                        xmltab = "CorporateFlightMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav";
                        btnCorp.CssClass = "rpt_nav_active";
                        btnCharter.CssClass = "rpt_nav";
                        break;
                    case "Charter":
                        xmltab = "CharterMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav_active";
                        break;
                    default:
                        Session["TabSelect"] = "Db";
                        xmltab = "DatabaseMainTab.xml";
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav_active";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav";
                        Response.Redirect("ReportViewer.aspx?xmlFilename=DBAccounts.xml");
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        break;
                }
                ExpandPanel(pnlReportTab, xmltab, true);
            }
        }
        private void ExpandPanel(RadPanelBar panelBar, string TabXmlname, bool isExpand)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(panelBar, TabXmlname, isExpand))
            {
                string Selectedvalue = Convert.ToString(Request.QueryString["xmlFilename"]);
                //string attrVal = "";
                string TitleTab = string.Empty;
                XDocument doc = XDocument.Load(Server.MapPath(urlBase + TabXmlname));

                if (Request.QueryString["xmlFilename"] != null)
                {

                    var items = doc.Descendants("screen")
                                  .Where(c => c.Attribute("url").Value.Contains(Convert.ToString(Request.QueryString["xmlFilename"])))
                                   .Select(c => new { x = c.Value, y = c.Parent.Attribute("text").Value, z = c.Attribute("name").Value })
                                   .ToList();

                    if (items != null)
                    {
                        if (items.Count != 0)
                        {

                            RadPanelItem selectedItem = panelBar.FindItemByText(items[0].y);

                            if (selectedItem != null)
                            {
                                if (selectedItem.Items.Count > 0)
                                {

                                    RadPanelItem selectedvalue = selectedItem.Items.FindItemByText(items[0].z);
                                    if (selectedvalue != null)
                                    {
                                        selectedvalue.Selected = true;
                                        selectedItem.Expanded = true;
                                    }
                                }
                                else
                                {
                                    selectedItem.Selected = true;
                                    while ((selectedItem != null) &&
                                           (selectedItem.Parent.GetType() == typeof(RadPanelItem)))
                                    {
                                        selectedItem = (RadPanelItem)selectedItem.Parent;
                                        selectedItem.Expanded = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void Controlvisible(bool flag)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flag))
            {
                //btExport.Visible = flag;
                btnRptView.Visible = flag;
                btnReset.Visible = flag;

                if (flag)
                {
                    string RptName = string.Empty;

                    //MemoryStream stream = new MemoryStream(Encoding.Default.GetBytes(Server.MapPath(urlBase + filename)));
                    //XmlReaderSettings settings = new XmlReaderSettings();

                    //// allow entity parsing but do so more safely
                    //settings.DtdProcessing = DtdProcessing.Prohibit;
                    //settings.MaxCharactersFromEntities = 1024;
                    //settings.XmlResolver = new XmlUrlResolver();

                    //XmlReader reader = XmlReader.Create(stream, settings);
                    //XmlDocument doc = new XmlDocument();
                    //doc.Load(reader);


                    XmlDocument doc = new XmlDocument();
                    var filePath = Server.MapPath(urlBase + filename);
                    XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                    var xmlReaderObject = xmlReader.XmlCleanReader();
                    if (xmlReaderObject != null)
                    {
                        doc.Load(xmlReaderObject);

                        XmlNodeList elemList = doc.GetElementsByTagName("Report");

                        for (int i = 0; i < elemList.Count; i++)
                        {
                            RptName = elemList[i].Attributes["displayname"].Value + "," + elemList[i].Attributes["name"].Value;

                            if (elemList[i].Attributes["preview"].Value != null)
                            {
                                if (elemList[i].Attributes["preview"].Value.ToUpper().Equals("TRUE"))
                                {
                                    btnRptView.Visible = true;
                                }
                                else if (elemList[i].Attributes["preview"].Value.ToUpper().Equals("FALSE"))
                                {
                                    btnRptView.Visible = false;
                                }
                            }

                            if ((elemList[i].Attributes["excel"].Value != null) && (elemList[i].Attributes["mhtml"].Value != null))
                            {
                                if ((elemList[i].Attributes["excel"].Value.ToUpper().Equals("FALSE")) && (elemList[i].Attributes["mhtml"].Value.ToUpper().Equals("FALSE")))
                                {
                                    btExport.Visible = false;
                                }
                                else
                                {

                                    if (elemList[i].Attributes["excel"].Value.ToUpper().Equals("FALSE"))
                                    {
                                        //ddlFormat.Items.Remove(new ListItem("Excel"));
                                    }

                                    if (elemList[i].Attributes["mhtml"].Value.ToUpper().Equals("FALSE"))
                                    {
                                        //ddlFormat.Items.Remove(new ListItem("MHTML"));
                                    }

                                }
                            }
                            //else
                            //{
                            //    if (elemList[i].Attributes["excel"].Value != null)
                            //    {
                            //        if (elemList[i].Attributes["excel"].Value.ToUpper().Equals("FALSE"))
                            //        {
                            //            //ddlFormat.Items.Remove(new ListItem("Excel"));
                            //        }
                            //    }

                            //    if (elemList[i].Attributes["mhtml"].Value != null)
                            //    {
                            //        if (elemList[i].Attributes["mhtml"].Value.ToUpper().Equals("FALSE"))
                            //        {
                            //            //ddlFormat.Items.Remove(new ListItem("MHTML"));
                            //        }
                            //    }
                            //}

                            //if (elemList[i].Attributes["mhtml"].Value != null)
                            //{
                            //    if (elemList[i].Attributes["mhtml"].Value.ToUpper().Equals("FALSE"))
                            //    {
                            //      //  ddlFormat.Items.Remove(new ListItem("MHTML"));                                  
                            //    }
                            //}
                            break;
                        }
                    }
                }
            }
        }

        private string ProcessResults()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string Param = string.Empty;
                if (rdBl.SelectedValue == "FileNumber")
                //if (reportCriteriaType == "TripNumber")
                {
                    Param = ParamFileNumber();
                }
                else
                {
                    TextBox ucInsDate = (TextBox)DateFrom.FindControl("tbDate");
                    if (!string.IsNullOrEmpty(ucInsDate.Text.Trim().ToString()))
                    {
                        //Param = "BeginDate=" + Convert.ToDateTime(ucInsDate.Text.Trim(), CultureInfo.InvariantCulture).ToShortDateString();
                        Param = "DATEFROM=" + Convert.ToDateTime(FormatDate(ucInsDate.Text.Trim(), DateFormat));
                    }
                    //if (!string.IsNullOrEmpty(((RadDatePicker)ucDateFrom.FindControl("RadDatePicker1")).DateInput.Text))
                    //{
                    //    Param = "BeginDate=" + ((RadDatePicker)ucDateFrom.FindControl("RadDatePicker1")).DateInput.Text;
                    //}
                    TextBox ucInsDate1 = (TextBox)DateTo.FindControl("tbDate");
                    if (!string.IsNullOrEmpty(ucInsDate1.Text.Trim().ToString()))
                    {
                        //Param = Param + ";EndDate=" +  Convert.ToDateTime(ucInsDate1.Text.Trim(),  CultureInfo.InvariantCulture).ToShortDateString();
                        Param = Param + ";DATETO=" + Convert.ToDateTime(FormatDate(ucInsDate1.Text.Trim(), DateFormat));
                    }

                    if (tbVendorCode.Text != string.Empty)
                    {
                        Param = Param + ";VendorCD=" + tbVendorCode.Text;
                    }
                    if (tbHomebase.Text != string.Empty)
                    {

                        Param = Param + ";HomeBaseCD=" + tbHomebase.Text;
                    }
                    if (tbTypeCode.Text != string.Empty)
                    {
                        Param = Param + ";AircraftCD=" + tbTypeCode.Text;
                    }
                    if (tbCustomer.Text != string.Empty)
                    {
                        Param = Param + ";CQCustomerCD=" + tbCustomer.Text;
                    }
                    if (tbTailNumber.Text != string.Empty)
                    {
                        Param = Param + ";TailNum=" + tbTailNumber.Text;
                    }
                    if (tbSalesPerson.Text != string.Empty)
                    {
                        Param = Param + ";SalesPersonCD=" + tbSalesPerson.Text;
                    }
                    //if (tbDepartmentCD.Text != string.Empty)
                    //{
                    //    Param = Param + ";DepartmentCD=" + tbDepartmentCD.Text;
                    //}
                    //if (tbAuth.Text != string.Empty)
                    //{
                    //    Param = Param + ";AuthorizationCD=" + tbAuth.Text;
                    //}
                    //if (tbCat.Text != string.Empty)
                    //{
                    //    Param = Param + ";CatagoryCD=" + tbCat.Text;
                    //}
                    //if (chkTrip.Checked != false)
                    //{
                    //Param = Param + ";IsTrip=" + chkTrip.Checked;
                    //}
                    //if (chkCanceled.Checked != false)
                    //{
                    //Param = Param + ";IsCanceled=" + chkCanceled.Checked;
                    //}
                    //if (chkHold.Checked != false)
                    //{
                    //Param = Param + ";IsHold=" + chkHold.Checked;
                    //}
                    //if (chkWorkSheet.Checked != false)
                    //{
                    // Param = Param + ";IsWorkSheet=" + chkWorkSheet.Checked;
                    //}
                    //if (chkUnFulFilled.Checked != false)
                    //{
                    //Param = Param + ";IsUnFulFilled=" + chkUnFulFilled.Checked;
                    //}
                    //if (chkScheduledService.Checked != false)
                    //{
                    //Param = Param + ";IsScheduledService=" + chkScheduledService.Checked;
                    //}
                    Param = Param + ParamFileNumber();
                    Param = Param + ";UserCD=" + UserPrincipal.Identity._name;

                    //Added byb Sudhakar
                    Param = Param + ";UserCustomerID=" + UserPrincipal.Identity._customerID;
                    Param = Param + ";UserHomebaseID=" + UserPrincipal.Identity._homeBaseId;
                    // Param = Param + ";TenToMin=" + UserPrincipal.Identity._fpSettings._TimeDisplayTenMin;
                }
                return Param;
            }
        }


        private void ProcessColumns(string xmlfilenamefile)
        {
            dicColumnList = new Dictionary<string, string>();
            string xmlfilename = string.Empty;
            if (Session["xmlCQFilename"] != null || xmlfilenamefile != string.Empty)
            {
                xmlfilename = (Session["xmlCQFilename"] != null) ? Convert.ToString(Session["xmlCQFilename"]) : string.Empty;
                if (xmlfilenamefile != string.Empty)
                {
                    xmlfilename = xmlfilenamefile;
                    Session["xmlCQFilename"] = xmlfilename;

                }

            }
            else if (filename != null)
            {
                xmlfilename = filename;
            }
            var filePath = Server.MapPath(urlBase + xmlfilename);
            XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
            var xmlReaderObject = xmlReader.XmlCleanReader();
            if (xmlReaderObject != null)
            {
                XPathDocument ReportFieldDoc = new XPathDocument(xmlReaderObject);
                XPathNodeIterator itr = ReportFieldDoc.CreateNavigator().Select("//Column");
                StringBuilder sb = new System.Text.StringBuilder();

                while (itr.MoveNext())
                {
                    dicColumnList.Add(itr.Current.GetAttribute("name", "").ToString(), itr.Current.GetAttribute("displayname", "").ToString());
                }
            }
        }
        private Control FindControlRecursive(Control rootControl, string controlID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (rootControl.ID == controlID) return rootControl;

                foreach (Control controlToSearch in rootControl.Controls)
                {
                    Control controlToReturn =
                        FindControlRecursive(controlToSearch, controlID);
                    if (controlToReturn != null) return controlToReturn;
                }
                return null;
            }
        }
        protected void OnFinish(object sender, System.Web.UI.WebControls.WizardNavigationEventArgs e)
        {
            //string fileName = String.Format("data-{0}.htm", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
            //Response.ContentType = "text/htm";
            //Response.AddHeader("content-disposition", "filename=" + fileName);
            //// write string data to Response.OutputStream here
            //Response.Write(ProcessHtmlParam());
            //Response.End();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Page.IsValid)
                        {

                            string[] strArr = GetReportTitle().Split(',');
                            if (Session["CQSubLink"] != null)
                            {
                                string[] strxmlRptnameArr = Convert.ToString(Session["CQSubLink"]).Split(',');
                                Session["REPORT"] = strxmlRptnameArr[0] + "mhtml";
                            }
                            else if (strArr.Count() > 0)
                            {
                                Session["REPORT"] = Convert.ToString(strArr[1] + "mhtml");
                            }
                            Session["FORMAT"] = "MHTML";
                            // Session["PARAMETER"] = ProcessHtmlExcelParam();
                            Session["CQSubLink"] = null;
                            //Wizard1.Visible = false;
                            Response.Redirect("ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }

        protected void OnNext(object sender, WizardNavigationEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (UCRHS.RadListBoxDestination.Items.Count > 0)
                        {
                            lblError.Text = "";

                            if (Page.IsValid)
                            {
                                if (Wizard1.ActiveStep.ID == "WizardStep5")
                                {
                                    switch (ddlFormat.SelectedValue)
                                    {
                                        case "MHTML":
                                            Wizard1.Visible = true;
                                            Wizard1.MoveTo(WizardStep1);

                                            break;
                                        case "Excel":
                                            string[] strArr = GetReportTitle().Split(',');
                                            if (strArr.Count() > 0)
                                            {
                                                Session["REPORT"] = Convert.ToString(strArr[1] + "Export");
                                            }
                                            Session["FORMAT"] = "EXCEL";
                                            Session["PARAMETER"] = ProcessResults();
                                            Response.Redirect("ReportRenderView.aspx");
                                            break;
                                        default:
                                            Wizard1.Visible = false;
                                            break;
                                    }
                                }

                                if (Wizard1.ActiveStep.ID == "WizardStep1")
                                {
                                    // UCRHSort.ColumnList = dicColumnList;

                                    foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                    {
                                        dicSelectedList.Add(SelectedItem.Value, SelectedItem.Text);
                                    }

                                    UCRHSort.RadListBoxSource.DataSource = dicSelectedList;
                                    UCRHSort.RadListBoxSource.DataValueField = "Key";
                                    UCRHSort.RadListBoxSource.DataTextField = "Value";
                                    UCRHSort.RadListBoxSource.DataBind();


                                    Dictionary<string, string> dicTempSelectedList = dicSelectedList;
                                    foreach (KeyValuePair<string, string> pair in dicTempSelectedList)
                                    {
                                        dicColumnList.Remove(pair.Key);
                                    }
                                    Dictionary<string, string> Empty = new Dictionary<string, string>();
                                    UCRHSort.RadListBoxDestination.DataSource = Empty;
                                    UCRHSort.RadListBoxDestination.DataBind();

                                    Session["dicSelectedList"] = dicSelectedList;

                                    //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                    //{
                                    //    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                    //}

                                    //SelectionDestination.DataSource = dicSelectedList;
                                    //SelectionDestination.DataValueField = "Key";
                                    //SelectionDestination.DataTextField = "Value";
                                    //SelectionDestination.DataBind();

                                    //SelectionSource.DataSource = dicSelectedList;
                                    //SelectionSource.DataValueField = "Key";
                                    //SelectionSource.DataTextField = "Value";
                                    //SelectionSource.DataBind();
                                }
                                if (Wizard1.ActiveStep.ID == "WizardStep2")
                                {
                                    foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                    {
                                        dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                    }

                                    foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxSource.Items)
                                    {
                                        dicSelectedListsort.Add(SelectedItem.Value, SelectedItem.Text);
                                    }

                                    foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxDestination.Items)
                                    {
                                        dicSelectedListsortSel.Add(SelectedItem.Value, SelectedItem.Text);
                                    }

                                    Session["dicSelectedListSort"] = dicSelectedListsort;

                                    Session["dicSelectedListSortSel"] = dicSelectedListsortSel;

                                    SelectedSource.DataSource = dicSelectedList;
                                    SelectedSource.DataValueField = "Key";
                                    SelectedSource.DataTextField = "Value";
                                    SelectedSource.DataBind();

                                    HeaderBySource.DataSource = dicSelectedList;
                                    HeaderBySource.DataValueField = "Key";
                                    HeaderBySource.DataTextField = "Value";
                                    HeaderBySource.DataBind();

                                }

                                if (Wizard1.ActiveStep.ID == "WizardStep4")
                                {
                                    txtTitle.Text = lbTitle.Text;

                                    // uncmd this code after
                                    FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                                    FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient();
                                    FPPrincipal identityname = (FPPrincipal)Session[Session.SessionID];
                                    var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == identity.Identity._customerID && x.HomebaseID == identity.Identity._homeBaseId).ToList();

                                    if (objCompany[0].HTMLHeader != string.Empty)
                                    {
                                        chkHeader.Enabled = false;
                                    }
                                    if (objCompany[0].HTMLFooter != string.Empty)
                                    {
                                        chkFooter.Enabled = false;
                                    }
                                    if (objCompany[0].HTMLBackgroun != string.Empty)
                                    {
                                        chkBackground.Enabled = false;
                                    }
                                    string[] strArr = GetReportTitle().Split(',');
                                    if (strArr.Count() > 0)
                                    {
                                        txtTitle.Text = Convert.ToString(strArr[0]);
                                    }
                                }
                            }
                        }
                        else
                        {
                            lblError.Text = "Select the Fields";
                            e.Cancel = true;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void LoadTabs(RadPanelBar Rpb, string XmlFilePath)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Rpb, XmlFilePath))
            {
                string attrVal = "";
                XmlDocument doc = new XmlDocument();
                doc.Load(Server.MapPath(XmlFilePath));

                XmlNodeList elemList = doc.GetElementsByTagName("catagory");
                for (int i = 0; i < elemList.Count; i++)
                {
                    attrVal = attrVal + elemList[i].Attributes["text"].Value + ",";
                }
                string[] strval = attrVal.Split(',');
                foreach (string str in strval)
                {
                    Telerik.Web.UI.RadPanelItem rpi = new Telerik.Web.UI.RadPanelItem(str);
                    XmlNodeList xnList = doc.SelectNodes("/catagorys/catagory[@text='" + str + "']/screen[@name!='']");
                    if (xnList.Count > 0)
                    {
                        int ival = 0;
                        foreach (XmlNode child in xnList)
                        {
                            //Added UMPermissionRole for implementing security
                            if (IsAuthorized("View" + child.Attributes["UMPermissionRole"].Value))
                            {
                                rpi.Items.Add(new Telerik.Web.UI.RadPanelItem(child.Attributes["name"].Value, child.Attributes["url"].Value));
                                rpi.Font.Bold = true;
                                rpi.Items[ival].Enabled = (child.Attributes["url"].Value == string.Empty ? false : true);
                                if (!rpi.Enabled)
                                {
                                    rpi.Items[ival].ForeColor = Color.Gray;
                                }

                                ival++;
                            }
                        }
                        Rpb.Items.Add(rpi);
                    }
                    else if (str != "")
                    {
                        XmlNodeList elemListval = doc.GetElementsByTagName("catagory");
                        for (int ival = 0; ival < elemListval.Count; ival++)
                        {
                            Telerik.Web.UI.RadPanelItem rpia = new Telerik.Web.UI.RadPanelItem(elemListval[ival].Attributes["text"].Value, elemListval[ival].Attributes["url"].Value);
                            Rpb.Items.Add(rpia);
                        }
                        break;

                    }
                }
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Wizard1.Visible = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnExportFileFormat_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Page.IsValid)
                        {
                            if (ddlFormat.SelectedValue == "WORD")
                            {
                                previewClick("WORD");
                            }
                            else
                            {
                                if (ddlFormat.SelectedValue == "Excel")
                                {
                                    Wizard1.Visible = false;
                                }
                                //   ExportPanal.Visible = true;
                                FileExcelGdl.Visible = (filename == "PREGeneralDeclarationAPDX.xml" || filename == "PRETSGeneralDeclaration.xml") ? true : false;
                                lbgdl1.Text = (filename == "PREGeneralDeclarationAPDX.xml") ? "Download General Declaration Appendix 1" : "Download General Declaration";
                                lbgdl2.Text = (filename == "PREGeneralDeclarationAPDX.xml") ? "Download General Declaration Appendix 2" : "";
                                lbgdl2.Visible = (filename == "PRETSGeneralDeclaration.xml") ? false : true;
                                //  btExportFileFormat.Visible = (filename == "PreTSTripSheetMain.xml" || filename == "PREGeneralDeclarationAPDX.xml" || filename == "PRETSGeneralDeclaration.xml") ? false : true;
                                FileExcel.Visible = (filename == "PreTSTripSheetMain.xml") ? true : false;

                                FileExcelGdl.Visible = (filename == "PREGeneralDeclarationAPDX.xml" || filename == "PRETSGeneralDeclaration.xml") ? true : false;
                                //  btExportFileFormat.Visible = (filename == "PreTSTripSheetMain.xml") ? false : true;
                                FileExcel.Visible = (filename == "PreTSTripSheetMain.xml") ? true : false;
                            }
                            if (Session["CQSubLink"] == null)
                            {
                                SubLinksInvoke();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        private void SubLinksInvoke()
        {
            if (Session["CQSubLink"] != null)
            {
                string[] strxmlRptnameArr = Convert.ToString(Session["CQSubLink"]).Split(',');
                Export(strxmlRptnameArr[0], strxmlRptnameArr[1]);
            }
            else
            {
                string[] strArr = GetReportTitle().Split(',');

                if ((FileExcelGdl.Visible == false) && (FileExcel.Visible == false))
                {
                    //     btExportFileFormat.Visible = true;
                    FileExcel.Visible = false;
                    switch (ddlFormat.SelectedValue)
                    {
                        case "MHTML":
                            Wizard1.Visible = true;
                            Wizard1.MoveTo(WizardStep1);
                            break;
                        case "Excel":

                            if (strArr.Count() > 0)
                            {
                                Session["REPORT"] = Convert.ToString(strArr[1] + "Export");
                            }
                            Session["FORMAT"] = "EXCEL";

                            Session["PARAMETER"] = ProcessHtmlExcelParam();
                            Response.Redirect("ReportRenderView.aspx");
                            break;
                    }
                }
            }
        }

        private string ProcessHtmlExcelParam()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ReportName))
            {
                string PageFGcolor = ColorTranslator.ToHtml(ForegroundColor.SelectedColor);
                string PageBGColor = ColorTranslator.ToHtml(BackgroundColor.SelectedColor);

                string TableBorderColors = ColorTranslator.ToHtml(TableBorderColor.SelectedColor);
                string TableFGColor = ColorTranslator.ToHtml(TableForegroundColor.SelectedColor);
                string TableBGColor = ColorTranslator.ToHtml(TableBackgroundColor.SelectedColor);
                StringBuilder strBuildparm = new StringBuilder();

                string Dbcolname = string.Empty;
                string AliesColname = string.Empty;
                string SortColname = string.Empty;

                switch (ddlFormat.SelectedValue)
                {
                    case "MHTML":

                        foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                        {
                            Dbcolname = Dbcolname + SelectedItem.Value + ",";
                        }
                        if (Dbcolname != string.Empty)
                        {
                            Dbcolname = Dbcolname.Substring(0, Dbcolname.Length - 1);
                        }
                        foreach (RadListBoxItem SelectedItem in HeaderBySource.Items)
                        {
                            AliesColname = AliesColname + SelectedItem.Text + "|";
                        }
                        AliesColname = AliesColname.Substring(0, AliesColname.Length - 1);

                        foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxDestination.Items)
                        {
                            SortColname = SortColname + SelectedItem.Value + ",";
                        }
                        if (SortColname != string.Empty)
                        {
                            SortColname = SortColname.Substring(0, SortColname.Length - 1);
                        }

                        strBuildparm.Append(ProcessResults());
                        strBuildparm.Append(";SelectedCols=" + Dbcolname);
                        strBuildparm.Append(";DisplayCols=" + AliesColname);
                        strBuildparm.Append(";SortCols=" + SortColname);
                        strBuildparm.Append(";Color=" + PageBGColor + "," + PageFGcolor + "," + TableFGColor + "," + TableBorderColors + "," + TableBGColor);
                        strBuildparm.Append(";ReportTitle=" + txtTitle.Text);

                        FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                        FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient();
                        FPPrincipal identityname = (FPPrincipal)Session[Session.SessionID];
                        var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == identity.Identity._customerID && x.HomebaseID == identity.Identity._homeBaseId).ToList();


                        if (chkHeader.Checked)
                        {
                            strBuildparm.Append(";HtmlHeader=" + objCompany[0].HTMLHeader);
                        }
                        if (chkFooter.Checked)
                        {
                            strBuildparm.Append(";HtmlFooter=" + objCompany[0].HTMLFooter);
                        }
                        if (chkBackground.Checked)
                        {
                            strBuildparm.Append(";HtmlBG=" + objCompany[0].HTMLBackgroun);
                        }
                        break;

                    case "Excel":
                        //    //for Excel file  Reading from RadListBoxSource
                        //   foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxSource.Items)
                        //    {
                        //        Dbcolname = Dbcolname + SelectedItem.Value + ",";
                        //    }
                        //    if (Dbcolname != string.Empty)
                        //    {
                        //        Dbcolname = Dbcolname.Substring(0, Dbcolname.Length - 1);
                        //    }
                        //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxSource.Items)
                        //    {
                        //        AliesColname=AliesColname+SelectedItem.Text+"|";
                        //    }
                        //if (AliesColname != string.Empty)
                        //{
                        //    AliesColname = AliesColname.Substring(0, AliesColname.Length - 1);
                        //}

                        //======== [Start] Fix by Sudhakar ==============

                        strBuildparm.Append(ProcessResults());
                        //if (rdBl.SelectedValue == "TripNumber")
                        //{
                        //    strBuildparm.Append(ParamTripNumber());
                        //}
                        //else
                        //{
                        //    strBuildparm.Append(ProcessResults());
                        //}
                        //======= [End] Fix by Sudhakar ==============

                        //   strBuildparm.Append(";SelectedCols="+Dbcolname);
                        //    strBuildparm.Append(";DisplayCols="+AliesColname);
                        //strBuildparm.Append(";Color=," + PageFGcolor + "," + TableFGColor + "," + TableBorderColors + "," + TableBGColor); 

                        break;
                }
                return strBuildparm.ToString();

                //FlightPak.Web.ReportRenderService.ReportRenderClient RptClient = new ReportRenderService.ReportRenderClient();
                //DataSet dsRpt = RptClient.GetReportHTMLTable(Dbcolname, AliesColname, GetSPName(filename), ProcessResults());
                //  return GetDataTableAsHTML(dsRpt.Tables[0], txtTitle.Text, PageFGcolor, PageBGColor, TableBorderColor, TableFGColor, TableBGColor);
            }
        }

        public void Export(string ReportName, string xmlfilename)
        {
            if (Page.IsValid)
            {
                switch (ddlFormat.SelectedValue)
                {
                    case "MHTML":

                        Wizard1.Visible = true;
                        ProcessColumns(xmlfilename);
                        UCRHS.ColumnList = dicColumnList;
                        UCRHSort.ColumnList = dicColumnList;
                        Wizard1.MoveTo(WizardStep1);
                        //ExportPanal.Visible = false;
                        break;
                    case "Excel":
                        Session["FORMAT"] = "EXCEL";
                        Session["REPORT"] = Convert.ToString(ReportName + "Export");
                        //if (rdBl.SelectedValue.ToString() == "DateRange")
                        if (reportCriteriaType == "DateRange")
                        {
                            Session["PARAMETER"] = ProcessResults();

                        }
                        else if (reportCriteriaType == "TripNumber")
                        {
                            Session["PARAMETER"] = ParamFileNumber();

                        }
                        Response.Redirect("ReportRenderView.aspx");
                        break;
                    case "WORD":
                        previewClick("WORD");
                        break;
                    default:
                        Wizard1.Visible = false;
                        break;
                }
            }
        }

        private void previewClick(string FileFormat)
        {
            filename = Convert.ToString(Request.QueryString["xmlFilename"]);

            if (Page.IsValid)
            {
                string[] strArr = GetReportTitle().Split(',');
                if (strArr.Count() > 0)
                {
                    Session["REPORT"] = Convert.ToString(strArr[1]);
                }
                Session["FORMAT"] = FileFormat;
                if (reportCriteriaType == "FileNumber")
                {
                    Session["PARAMETER"] = ParamFileNumber();
                }
                else
                {
                    Session["PARAMETER"] = ProcessResults();
                }
                if (ValidateCQDate() == false)
                {
                    return;
                }
                //Session["PARAMETER"] = "StartTime='12/10/2012';EndTime='12/10/2012';CrewCDs='JKM,ECJ';CrewGroupCDs='6789,122';AircraftCDs='10010,ACD';UserCD='UC";
                Response.Redirect("ReportRenderView.aspx");
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["CQSubLink"] = null;
                        if (ValidateCQDate() == false)
                        {
                            return;
                        }
                        if (Page.IsValid)
                        {
                            //   ExportPanal.Visible = true;
                            FileExcelGdl.Visible = (filename == "PREGeneralDeclarationAPDX.xml" || filename == "PRETSGeneralDeclaration.xml") ? true : false;
                            lbgdl1.Text = (filename == "PREGeneralDeclarationAPDX.xml") ? "Download General Declaration Appendix 1" : "Download General Declaration";
                            lbgdl2.Text = (filename == "PREGeneralDeclarationAPDX.xml") ? "Download General Declaration Appendix 2" : "";
                            lbgdl2.Visible = (filename == "PRETSGeneralDeclaration.xml") ? false : true;
                            //  btExportFileFormat.Visible = (filename == "PreTSTripSheetMain.xml" || filename == "PREGeneralDeclarationAPDX.xml" || filename == "PRETSGeneralDeclaration.xml") ? false : true;
                            FileExcel.Visible = (filename == "PreTSTripSheetMain.xml") ? true : false;
                            if (FileExcelGdl.Visible || FileExcel.Visible)
                            {
                                ExportPanal.Visible = false;
                            }
                            else
                            {
                                ExportPanal.Visible = true;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnCostFileFormat_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Wizard1.Visible = false;
                        ExportPanal.Visible = false;
                        FileExcel.Visible = false;
                        FileExcelGdl.Visible = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnPre_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "PreFlight";
                        pnlReportTab.Items.Clear();
                        //LoadTabs(pnlReportTab, urlBase + "PreFlightMainTab.xml");
                        Response.Redirect("ReportViewer.aspx?xmlFilename=PREAircraftCalendarI.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnPost_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "PostFlight";
                        pnlReportTab.Items.Clear();
                        // LoadTabs(pnlReportTab, urlBase + "PostFlightMainTab.xml");
                        Response.Redirect("ReportViewer.aspx?xmlFilename=POSTAircraftArrivals.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnCharter_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "Charter";
                        pnlReportTab.Items.Clear();
                        // LoadTabs(pnlReportTab, urlBase + "CharterMainTab.xml");
                        Response.Redirect("CQReportViewer.aspx?xmlFilename=CQAircraftProfile.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnCorpReq_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "CorpReq";
                        pnlReportTab.Items.Clear();
                        //LoadTabs(pnlReportTab, urlBase + "CorporateFlightMainTab.xml");
                        Response.Redirect("ReportViewer.aspx?xmlFilename=CRTripItinerary.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }

        protected void btnDb_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "Db";
                        pnlReportTab.Items.Clear();
                        LoadTabs(pnlReportTab, urlBase + "DatabaseMainTab.xml");
                        Response.Redirect("ReportViewer.aspx?xmlFilename=DBAccounts.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }


        private string ParamFileNumber()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                string paramater = string.Empty;

                if (reportCriteriaType == "FileNumber")
                {
                    paramater += "UserCD=" + UserPrincipal.Identity._name;
                    if (tbFileNum.Text != string.Empty)
                    {
                        paramater += ";FileNumber=" + tbFileNum.Text;
                    }
                    if (tbQuote.Text != string.Empty)
                    {
                        paramater += ";QuoteNUM=" + tbQuote.Text;
                    }
                    if (tbLeg.Text != string.Empty)
                    {
                        paramater = paramater + ";LegNUM=" + tbLeg.Text;
                    }

                    //paramater = paramater + "IsTrip=false;IsCanceled=false;IsHold=false;IsWorkSheet=false;IsUnFulFilled=false;IsScheduledService=false";
                }


                if (Session["CQReportHeaderID"] != null)
                {
                    string TxtVariable = Session["CQReportHeaderID"].ToString().Trim();
                    paramater = paramater + ";ReportHeaderID=" + TxtVariable;

                }
                Dictionary<string, int> dicRptType = new Dictionary<string, int>();
                if (Session["SnxmlFilename"] != null)
                {
                    dicRptType = (Dictionary<string, int>)Session["SnxmlFilename"];
                    paramater = paramater + ";ReportNumber=" + dicRptType["xmlFilename"];
                }

                if (Session["CQReportDetailList"] != null)
                {
                    List<GetCQReportDetail> lstCQReportDetail = new List<GetCQReportDetail>();
                    GetCQReportDetail objCQReportDetail = new GetCQReportDetail();
                    using (CharterQuoteService.CharterQuoteServiceClient ObjService = new CharterQuoteService.CharterQuoteServiceClient())
                    {
                        var ObjRetVal = ObjService.GetCQReportDetail();
                        //.EntityList.Where(x => x.TripSheetReportHeaderID == Convert.ToInt64(Session["CQReportHeaderID"])).ToList();
                        if (ObjRetVal.ReturnFlag == true)
                        {
                            var ObjRetVal1 = ObjRetVal.EntityList.Where(x => x.CQReportHeaderID == Convert.ToInt64(Session["CQReportHeaderID"])
                                                                            && x.ReportNumber == dicRptType["xmlFilename"]).ToList();
                            lstCQReportDetail = ObjRetVal1.ToList();
                        }
                    }
                    string strParam = string.Empty;
                    string str = string.Empty;
                    List<CQReportDetail> lstcngCQReportDetail = new List<CQReportDetail>();
                    List<CQReportDetail> oCQReportDetail = new List<CQReportDetail>();
                    oCQReportDetail = (List<CQReportDetail>)Session["CQReportDetailList"];
                    bool IsChanged = false;

                    for (int Index = 0; Index < oCQReportDetail.Count; Index++)
                    {
                        IsChanged = false;
                        for (int LIndex = 0; LIndex < lstCQReportDetail.Count; LIndex++)
                        {
                            if (oCQReportDetail[Index].CQReportDetailID == lstCQReportDetail[LIndex].CQReportDetailID)
                            {
                                if (oCQReportDetail[Index].ParameterLValue != lstCQReportDetail[LIndex].ParameterLValue)
                                {
                                    IsChanged = true;
                                }
                                if (oCQReportDetail[Index].ParameterNValue != lstCQReportDetail[LIndex].ParameterNValue)
                                {
                                    IsChanged = true;
                                }
                                if (oCQReportDetail[Index].ParameterValue != lstCQReportDetail[LIndex].ParameterValue)
                                {
                                    IsChanged = true;
                                }
                                if (IsChanged == true)
                                {
                                    lstcngCQReportDetail.Add(oCQReportDetail[Index]);

                                    strParam = strParam + oCQReportDetail[Index].ParameterCD + "::";

                                    if (oCQReportDetail[Index].ParamterType.ToUpper() == "L")
                                        strParam = strParam + oCQReportDetail[Index].ParameterLValue + "||";
                                    else if (oCQReportDetail[Index].ParamterType.ToUpper() == "N")
                                        strParam = strParam + oCQReportDetail[Index].ParameterNValue + "||";
                                    else
                                        strParam = strParam + oCQReportDetail[Index].ParameterValue + "||";

                                    break;
                                }
                            }
                        }

                    }
                    if (!string.IsNullOrEmpty(strParam.ToString()))
                    {
                        paramater = paramater + ";TempSettings=" + strParam;
                    }

                }

                //Added byb Sudhakar
                if (reportCriteriaType == "FileNumber")
                {
                    paramater = paramater + ";UserCustomerID=" + UserPrincipal.Identity._customerID;
                    paramater = paramater + ";UserHomebaseID=" + UserPrincipal.Identity._homeBaseId;
                }
                return paramater;
            }
        }
        //private static string DummyMatch(List<GetTripSheetReportDetail> lstTripSheetReportDetail, List<TripSheetReportDetail> oTripSheetReportDetail)
        //{
        //    //DummyMatch(ref paramater, lstTripSheetReportDetail, ref str, lstcngTripSheetReportDetail, oTripSheetReportDetail, ref IsChanged);
        //    string strParam = string.Empty;
        //    foreach (var item in oTripSheetReportDetail)
        //    {

        //        if (lstTripSheetReportDetail.Where(x => x.TripSheetReportDetailID == item.TripSheetReportDetailID
        //                && x.ReportNumber == item.ReportNumber && x.IsSelected == item.IsSelected
        //                && x.ParameterValue.StringCompare(item.ParameterValue)).Count() > 0)
        //        {
        //            strParam = strParam + item.ParameterDescription + "::";

        //            if (item.ParameterType.ToUpper() == "L")
        //                strParam = strParam + item.ParameterLValue + "||";
        //            else if (item.ParameterType.ToUpper() == "N")
        //                strParam = strParam + item.ParameterNValue + "||";
        //            else
        //                strParam = strParam + item.ParameterValue + "||";

        //        }


        //    }
        //    return strParam;

        //}

        //private static void DummyMatch(ref string paramater, List<GetTripSheetReportDetail> lstTripSheetReportDetail, ref string str, List<TripSheetReportDetail> lstcngTripSheetReportDetail, List<TripSheetReportDetail> oTripSheetReportDetail, ref bool IsChanged)
        //{
        //    for (int Index = 0; Index < oTripSheetReportDetail.Count; Index++)
        //    {
        //        IsChanged = false;

        //        for (int LIndex = 0; LIndex < lstTripSheetReportDetail.Count; LIndex++)
        //        {
        //            if (oTripSheetReportDetail[Index].TripSheetReportDetailID == lstTripSheetReportDetail[LIndex].TripSheetReportDetailID)
        //            {
        //                if (oTripSheetReportDetail[Index].IsSelected != lstTripSheetReportDetail[LIndex].IsSelected)
        //                {
        //                    IsChanged = true;
        //                }
        //                if (oTripSheetReportDetail[Index].ReportNumber != lstTripSheetReportDetail[LIndex].ReportNumber)
        //                {
        //                    IsChanged = true;
        //                }
        //                if (oTripSheetReportDetail[Index].ParameterValue != lstTripSheetReportDetail[LIndex].ParameterValue)
        //                {
        //                    IsChanged = true;
        //                }
        //                if (IsChanged == true)
        //                {

        //                    lstcngTripSheetReportDetail.Add(oTripSheetReportDetail[Index]);

        //                }
        //            }
        //        }
        //    }
        //    if (lstcngTripSheetReportDetail.Count > 0)
        //    {
        //        for (int i = 0; i < lstcngTripSheetReportDetail.Count; i++)
        //        {
        //            if (lstcngTripSheetReportDetail.Count == 1)
        //                str = "<" + lstcngTripSheetReportDetail[i].ParameterDescription + ">" + "::" + "<" + lstcngTripSheetReportDetail[i].IsSelected + ">" + "||" + "<" + lstcngTripSheetReportDetail[i].ParameterDescription + ">" + "::" + "<" + lstcngTripSheetReportDetail[i].ReportNumber + ">" + "||" + "<" + lstcngTripSheetReportDetail[i].ParameterDescription + ">" + "::" + "<" + lstcngTripSheetReportDetail[i].ParameterValue + ">";
        //            else
        //                str = str + "<" + lstcngTripSheetReportDetail[i].ParameterDescription + ">" + "::" + "<" + lstcngTripSheetReportDetail[i].IsSelected + ">" + "||" + "<" + lstcngTripSheetReportDetail[i].ParameterDescription + ">" + "::" + "<" + lstcngTripSheetReportDetail[i].ReportNumber + ">" + "||" + "<" + lstcngTripSheetReportDetail[i].ParameterDescription + ">" + "::" + "<" + lstcngTripSheetReportDetail[i].ParameterValue + ">" + "||"; ;
        //        }
        //    }


        //    paramater = paramater + ";TempSetting" + str;
        //}
        protected void btnRptView_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ValidatingReport();

                        if (ValidatedReport)
                        {
                            filename = Convert.ToString(Request.QueryString["xmlFilename"]);

                            if (Page.IsValid)
                            {
                                //Prakash 29-04-13

                                if (rdBl.SelectedValue.ToString() == "FileNumber")
                                {
                                    if (!string.IsNullOrEmpty(tbFileNum.Text))
                                    {
                                        Session["DateDetails"] = null;
                                        string filenum = string.Empty;
                                        string quotenum = string.Empty;
                                        string legnum = string.Empty;
                                        string fileID = string.Empty;
                                        string quoteID = string.Empty;
                                        string legID = string.Empty;
                                        string isExpressQuote = string.Empty;

                                        filenum = tbFileNum.Text;
                                        if (!string.IsNullOrEmpty(tbQuote.Text))
                                            quotenum = tbQuote.Text;
                                        if (!string.IsNullOrEmpty(tbLeg.Text))
                                            legnum = tbLeg.Text;
                                        if (!string.IsNullOrEmpty(hdnFileNum.Value))
                                            fileID = hdnFileNum.Value;
                                        if (!string.IsNullOrEmpty(hdnQuote.Value))
                                            quoteID = hdnQuote.Value;
                                        if (!string.IsNullOrEmpty(hdnLeg.Value))
                                            legID = hdnLeg.Value;
                                        if (!string.IsNullOrEmpty(hdnIsExpressQuote.Value))
                                            isExpressQuote = hdnIsExpressQuote.Value;

                                        Session["FileDetails"] = filenum + ":" + quotenum + ":" + legnum + ":" + fileID + ":" + quoteID + ":" + legID + ":" + isExpressQuote;
                                    }
                                }
                                else if (rdBl.SelectedValue.ToString() == "DateRange")
                                {
                                    Session["FileDetails"] = null;
                                    List<string> dateList = new List<string>();
                                    TextBox tbDateFrom = (TextBox)DateFrom.FindControl("tbDate");
                                    TextBox tbDateTo = (TextBox)DateTo.FindControl("tbDate");
                                    dateList.Add(tbDateFrom.Text);
                                    dateList.Add(tbDateTo.Text);
                                    dateList.Add(tbTailNumber.Text);
                                    dateList.Add(hdnTailNumber.Value);
                                    dateList.Add(tbVendorCode.Text);
                                    dateList.Add(hdnVendorCode.Value);
                                    dateList.Add(tbHomebase.Text);
                                    dateList.Add(hdnHomebase.Value);
                                    dateList.Add(tbTypeCode.Text);
                                    dateList.Add(hdnTypeCode.Value);
                                    dateList.Add(tbCustomer.Text);
                                    dateList.Add(hdnCustomer.Value);
                                    dateList.Add(tbSalesPerson.Text);
                                    dateList.Add(hdnSalesPerson.Value);

                                    Session["DateDetails"] = dateList;
                                }
                                //Prakash 29-04-13

                                string[] strArr = GetReportTitle().Split(',');
                                if (strArr.Count() > 0)
                                {
                                    Session["REPORT"] = Convert.ToString(strArr[1]);
                                }
                                Session["FORMAT"] = "PDF";
                                if (reportCriteriaType == "FileNumber")
                                {
                                    Session["PARAMETER"] = ParamFileNumber();
                                }
                                else
                                {
                                    Session["PARAMETER"] = ProcessResults();
                                }
                                if (ValidateCQDate() == false)
                                {
                                    return;
                                }
                                //Session["PARAMETER"] = "StartTime='12/10/2012';EndTime='12/10/2012';CrewCDs='JKM,ECJ';CrewGroupCDs='6789,122';AircraftCDs='10010,ACD';UserCD='UC";
                                Response.Redirect("ReportRenderView.aspx");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void ValidatingReport()
        {
            if (rdBl.SelectedValue.ToString() == "FileNumber")
            {
                if (string.IsNullOrEmpty(tbFileNum.Text))
                {
                    lbcvFileNum.Text = "File No. is Required";
                    ValidatedReport = false;
                }
                if (!string.IsNullOrEmpty(lbcvFileNum.Text) || !string.IsNullOrEmpty(lbcvQuote.Text) || !string.IsNullOrEmpty(lbcvLeg.Text))
                {
                    ValidatedReport = false;
                }
            }
            else if (rdBl.SelectedValue.ToString() == "DateRange")
            {
                if (!string.IsNullOrEmpty(lbcvCustomer.Text) || !string.IsNullOrEmpty(lbcvHomebase.Text) || !string.IsNullOrEmpty(lbcvSalesPerson.Text) || !string.IsNullOrEmpty(lbcvTailNumber.Text) || !string.IsNullOrEmpty(lbcvTypeCode.Text) || !string.IsNullOrEmpty(lbcvVendorCode.Text))
                {
                    ValidatedReport = false;
                }
            }
        }

        public void ExportRefresh(string ReportName, string xmlfilename)
        {

            ProcessColumns(xmlfilename);
            UCRHS.ColumnList = dicColumnList;
            UCRHSort.ColumnList = dicColumnList;

        }

        protected void lbTripSheet_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string strxmlRptname = "RptPRETSTripSheet,PRETripSheetMainSub.xml";
                        string[] strxmlRptnameArr = strxmlRptname.Split(',');
                        ExportRefresh(strxmlRptnameArr[0], strxmlRptnameArr[1]);
                        ExportPanal.Visible = true;
                        Session["CQSubLink"] = strxmlRptnameArr[0] + "," + strxmlRptnameArr[1];
                        //Export("RptPRETSTripSheet","PRETripSheetMainSub.xml");

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void lbTripsheetAlerts_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string strxmlRptname = "RptPRETSTripSheetAlerts,PRETripSheetAlertsSub.xml";
                        string[] strxmlRptnameArr = strxmlRptname.Split(',');
                        ExportRefresh(strxmlRptnameArr[0], strxmlRptnameArr[1]);
                        ExportPanal.Visible = true;
                        Session["CQSubLink"] = strxmlRptnameArr[0] + "," + strxmlRptnameArr[1];
                        //  Export("RptPRETSTripSheetAlerts", "PRETripSheetAlertsSub.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void lbPassengerNotes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string strxmlRptname = "RptPRETSPAXNotes,PRETripSheetPassengerNotesSub.xml";
                        string[] strxmlRptnameArr = strxmlRptname.Split(',');
                        ExportRefresh(strxmlRptnameArr[0], strxmlRptnameArr[1]);
                        ExportPanal.Visible = true;
                        Session["CQSubLink"] = strxmlRptnameArr[0] + "," + strxmlRptnameArr[1];

                        // Export("RptPRETSPAXNotes", "PRETripSheetPassengerNotesSub.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void lbPassengerAlerts_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        string strxmlRptname = "RptPRETSPAXAlerts,PRETripSheetPAXAlertSub.xml";
                        string[] strxmlRptnameArr = strxmlRptname.Split(',');
                        ExportRefresh(strxmlRptnameArr[0], strxmlRptnameArr[1]);
                        ExportPanal.Visible = true;
                        Session["CQSubLink"] = strxmlRptnameArr[0] + "," + strxmlRptnameArr[1];
                        //   Export("RptPRETSPAXAlerts", "PRETripSheetPAXAlertSub.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }

        protected void lbAircraftAdditionalInfomation_Click(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                        {
                            try
                            {
                                //Handle methods throguh exception manager with return flag
                                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                                exManager.Process(() =>
                                {

                                    string strxmlRptname = "RptPRETSAircraftAdditional,PRETripSheetAircraftAdditionalSub.xml";
                                    string[] strxmlRptnameArr = strxmlRptname.Split(',');
                                    ExportRefresh(strxmlRptnameArr[0], strxmlRptnameArr[1]);
                                    ExportPanal.Visible = true;
                                    Session["CQSubLink"] = strxmlRptnameArr[0] + "," + strxmlRptnameArr[1];


                                    // Export("RptPRETSAircraftAdditional", "PRETripSheetAircraftAdditionalSub.xml");
                                }, FlightPak.Common.Constants.Policy.UILayer);

                            }
                            catch (Exception ex)
                            {
                                //The exception will be handled, logged and replaced by our custom exception. 
                                ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }
        protected void lbFuelVendor_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string strxmlRptname = "RptPRETSBestFuelVendor,PRETripSheetBestFuelVendorSub.xml";
                        string[] strxmlRptnameArr = strxmlRptname.Split(',');
                        ExportRefresh(strxmlRptnameArr[0], strxmlRptnameArr[1]);
                        ExportPanal.Visible = true;
                        Session["CQSubLink"] = strxmlRptnameArr[0] + "," + strxmlRptnameArr[1];



                        //    Export("RptPRETSBestFuelVendor", "PRETripSheetBestFuelVendorSub.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void lbTripGdl1_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        filename = Convert.ToString(Request.QueryString["xmlFilename"]);



                        if (filename == "PRETSGeneralDeclaration.xml")
                        {

                            string strxmlRptname = "RptPREGeneralDeclaration,PRETripSheetGeneralDeclarationSub.xml";
                            string[] strxmlRptnameArr = strxmlRptname.Split(',');
                            ExportRefresh(strxmlRptnameArr[0], strxmlRptnameArr[1]);
                            ExportPanal.Visible = true;
                            Session["CQSubLink"] = strxmlRptnameArr[0] + "," + strxmlRptnameArr[1];
                            //    Export("RptPREGeneralDeclaration", "PRETripSheetGeneralDeclarationSub.xml");
                        }
                        else if (filename == "PREGeneralDeclarationAPDX.xml")
                        {
                            string strxmlRptname = "RptPRETSGeneralDeclarationAPDX,PRETripSheetGeneralDeclarationAPDXSub.xml";
                            string[] strxmlRptnameArr = strxmlRptname.Split(',');
                            ExportRefresh(strxmlRptnameArr[0], strxmlRptnameArr[1]);
                            ExportPanal.Visible = true;
                            Session["CQSubLink"] = strxmlRptnameArr[0] + "," + strxmlRptnameArr[1];
                            //  Export("RptPRETSGeneralDeclarationAPDX", "PRETripSheetGeneralDeclarationAPDXSub.xml");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void lbTripGdl2_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        filename = Convert.ToString(Request.QueryString["xmlFilename"]);

                        if (filename == "PREGeneralDeclarationAPDX.xml")
                        {
                            string strxmlRptname = "RptPRETSGeneralDeclarationAPDXpm,PRETripSheetGeneralDeclarationAPDXpmSub.xml";
                            string[] strxmlRptnameArr = strxmlRptname.Split(',');
                            ExportRefresh(strxmlRptnameArr[0], strxmlRptnameArr[1]);
                            ExportPanal.Visible = true;
                            Session["CQSubLink"] = strxmlRptnameArr[0] + "," + strxmlRptnameArr[1];
                            // Export("RptPREGeneralDeclarationAPDXpm", "PRETripSheetGeneralDeclarationAPDXpmSub.xml");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        private void ResetResults()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbTailNumber.Text = string.Empty;
                tbLeg.Text = string.Empty;
                //Passenger.Text = string.Empty;
                //TripNum.Text = string.Empty;
                //Leg.Text = string.Empty;
                //Passenger.Text = string.Empty;
                //txtpilot1.Text = string.Empty;
                //txtpilot2.Text = string.Empty;
                //txtAddlcrew.Text = string.Empty;
                //txtmsg.Text = string.Empty;
                //tbAIRCRAFTRELEASED.Text = string.Empty;
                //tbTripMain.Text = string.Empty;
                //lbTripSearchMessage.Text = string.Empty;
                tbLeg.Enabled = false;
                btnLeg.Enabled = false;
                //Passenger.Enabled = false;
                //btnPassenger.Enabled = false;
                //tbAuth.Text = string.Empty;
                //tbCat.Text = string.Empty;
                //tbClientCode.Text = string.Empty;
                //tbCrew.Text = string.Empty;
                //tbDepartmentCD.Text = string.Empty;
                tbHomebase.Text = string.Empty;
                //tbRequestor.Text = string.Empty;
                tbTailNumber.Text = string.Empty;
                tbFileNum.Text = string.Empty;
                tbQuote.Text = string.Empty;
                tbQuote.Enabled = false;
                btnQuote.Enabled = false;


                tbVendorCode.Text = string.Empty;
                tbTypeCode.Text = string.Empty;
                tbCustomer.Text = string.Empty;
                tbSalesPerson.Text = string.Empty;

                lbcvCustomer.Text = string.Empty;
                lbcvFileNum.Text = string.Empty;
                lbcvHomebase.Text = string.Empty;
                lbcvLeg.Text = string.Empty;
                lbcvQuote.Text = string.Empty;
                lbcvSalesPerson.Text = string.Empty;
                lbcvTailNumber.Text = string.Empty;
                lbcvTypeCode.Text = string.Empty;
                lbcvVendorCode.Text = string.Empty;

                TextBox tbPurDate = (TextBox)DateFrom.FindControl("tbDate");
                tbPurDate.Text = string.Empty;
                TextBox tbPurDate1 = (TextBox)DateTo.FindControl("tbDate");
                tbPurDate1.Text = string.Empty;
                Session["CQSubLink"] = null;

            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ResetResults();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        public string FormatDate(string dt)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(dt))
            {
                string FormattedDate = string.Empty;
                if (!string.IsNullOrEmpty(dt))
                {
                    DateTimeFormatInfo formatInfo = new DateTimeFormatInfo();
                    if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        formatInfo.ShortDatePattern = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                    else
                        formatInfo.ShortDatePattern = "MM/dd/yyyy"; // American, MDY

                    List<string> dtList = new List<string>();

                    char splitter;
                    if (dt.Contains("-"))
                        splitter = '-';
                    else if (dt.Contains("."))
                        splitter = '.';
                    else
                        splitter = '/';

                    switch (formatInfo.ShortDatePattern)
                    {
                        case "dd/MM/yyyy": // British, French, DMY
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[0] + "/" + dtList[2];
                            break;
                        case "dd.MM.yyyy": // German
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[0] + "/" + dtList[2];
                            break;
                        case "yyyy.MM.dd": // ANSI
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[2] + "/" + dtList[0];
                            break;
                        case "dd-MM-yyyy": // Italian
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[0] + "/" + dtList[2];
                            break;
                        case "yyyy/MM/dd": // Japan, Taiwan, YMD
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[2] + "/" + dtList[0];
                            break;
                        case "MM/dd/yyyy": // Default
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[0] + "/" + dtList[1] + "/" + dtList[2];
                            break;
                    }
                }
                return FormattedDate;
            }
        }

        //ajss
        #region UI Validations"

        private bool ValidateCQDate()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                DateTime BeginDate = new DateTime();
                DateTime EndDate = new DateTime();
                if (rdBl.SelectedItem.Value == "DateRange")
                {
                    TextBox ucInsDate = (TextBox)DateFrom.FindControl("tbDate");
                    if (!string.IsNullOrEmpty(ucInsDate.Text.Trim().ToString()))
                    {
                        BeginDate = Convert.ToDateTime(FormatDate(ucInsDate.Text.Trim(), DateFormat));
                    }
                    //else
                    //{
                    //    RadWindowManager1.RadAlert("Date From is required.", 400, 50, "Tripsheet Report Writer", "");
                    //    return ReturnValue = false;
                    //}
                    TextBox ucInsDate1 = (TextBox)DateTo.FindControl("tbDate");
                    if (!string.IsNullOrEmpty(ucInsDate1.Text.Trim().ToString()))
                    {
                        EndDate = Convert.ToDateTime(FormatDate(ucInsDate1.Text.Trim(), DateFormat));
                    }
                    //else
                    //{
                    //    RadWindowManager1.RadAlert("Date To is required.", 400, 50, "Tripsheet Report Writer", "");
                    //    return ReturnValue = false;
                    //}
                    if (BeginDate > EndDate)
                    {
                        RadWindowManager1.RadAlert("The Charter Quote Begin Date cannot be later than the Tripsheet End Date.", 400, 50, "Charter Quote Report Writer", "");
                        ReturnValue = false;
                    }
                }
                return ReturnValue;
            }
        }
        #endregion

        //ajse

        #region CQReport
        protected void rdBl_SelectedIndexChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FileControlsDisplay();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        private void FileControlsDisplay()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {   //Prakash 29-04-13
                if (Session["FileDetails"] == null && Session["DateDetails"] == null)
                {
                    ResetResults();
                }
                //Prakash 29-04-13
                if (rdBl.SelectedValue.ToString() == "FileNumber")
                {
                    filename = Convert.ToString(Request.QueryString["xmlFilename"]);
                    //Report.Visible = (filename == "CQQuoteReport.xml") ? true : false;
                    Report.Visible = false;
                    QuoteReport.Visible = true;
                }
                else if (rdBl.SelectedValue.ToString() == "DateRange")
                {
                    QuoteReport.Visible = false;
                    Report.Visible = true;
                }
            }
        }
        protected void tbTailNumber_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnTailNumber.Value = string.Empty;
                        lbcvTailNumber.Text = string.Empty;
                        if (tbTailNumber.Text.Trim() != string.Empty)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objCompanySvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objCompanySvc.GetFleet().EntityList.Where(x => x.TailNum.Trim().ToUpper() == tbTailNumber.Text.Trim().ToUpper()).ToList();
                                if (objRetVal.Count != 0)
                                {
                                    List<FlightPakMasterService.GetFleet> FleetList = new List<FlightPakMasterService.GetFleet>();
                                    FleetList = (List<FlightPakMasterService.GetFleet>)objRetVal;
                                    hdnTailNumber.Value = FleetList[0].FleetID.ToString();
                                }
                                else
                                {
                                    hdnTailNumber.Value = string.Empty;
                                    lbcvTailNumber.Text = "Invalid Tail No.";
                                    RadAjaxManager1.FocusControl(tbTailNumber.ClientID);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void tbVendorCode_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnVendorCode.Value = string.Empty;
                        lbcvVendorCode.Text = string.Empty;
                        if (!string.IsNullOrEmpty(tbVendorCode.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objservice = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objservice.GetVendorInfo().EntityList.Where(x => x.VendorCD == tbVendorCode.Text && x.IsDeleted == false).ToList();
                                if (objRetVal.Count() > 0 && objRetVal != null)
                                {
                                    hdnVendorCode.Value = objRetVal[0].VendorID.ToString();
                                }
                                else
                                {
                                    lbcvVendorCode.Text = "Invalid Vendor Code";
                                    RadAjaxManager1.FocusControl(tbVendorCode);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }


        protected void tbHomebase_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbcvHomebase.Text = string.Empty;
                        hdnHomebase.Value = string.Empty;
                        if (tbHomebase.Text.Trim() != string.Empty)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objCompanySvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objCompanySvc.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD.Trim().ToUpper() == (tbHomebase.Text.Trim().ToUpper())).ToList();
                                if (objRetVal.Count != 0)
                                {
                                    List<FlightPakMasterService.GetAllCompanyMaster> CompanyList = new List<FlightPakMasterService.GetAllCompanyMaster>();
                                    CompanyList = (List<FlightPakMasterService.GetAllCompanyMaster>)objRetVal.ToList();
                                    hdnHomebase.Value = CompanyList[0].HomebaseID.ToString();
                                    RadAjaxManager1.FocusControl(tbTypeCode.ClientID);
                                }
                                else
                                {
                                    lbcvHomebase.Text = "Invalid Homebase";
                                    hdnHomebase.Value = string.Empty;
                                    RadAjaxManager1.FocusControl(tbHomebase.ClientID);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void SearchBox_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckFileNumberExist(tbFileNum);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void CheckFileNumberExist(TextBox txtbx)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    tbQuote.Text = string.Empty;
                    tbLeg.Text = string.Empty;
                    lbcvQuote.Text = string.Empty;
                    lbcvLeg.Text = string.Empty;
                    lbcvFileNum.Text = string.Empty;
                    hdnQuote.Value = string.Empty;
                    hdnLeg.Value = string.Empty;
                    bool IsExpressQuote = false;

                    if(!string.IsNullOrEmpty(hdnIsExpressQuote.Value))
                    {
                        IsExpressQuote = Convert.ToBoolean(hdnIsExpressQuote.Value);
                    }
                    if (tbFileNum.Text.Trim()!=string.Empty && System.Text.RegularExpressions.Regex.IsMatch(tbFileNum.Text, "^[0-9]*$"))
                    {
                        using (CharterQuoteService.CharterQuoteServiceClient objService = new CharterQuoteService.CharterQuoteServiceClient())
                        {
                            var objRetVal = objService.GetCQRequestByFileNum(Convert.ToInt64(tbFileNum.Text), IsExpressQuote).EntityList;
                            if (objRetVal != null && objRetVal.Count != 0)
                            {
                                List<CQFile> CQFileList = new List<CQFile>();
                                CQFileList = (List<CQFile>)objRetVal.ToList();
                                hdnFileNum.Value = CQFileList[0].CQFileID.ToString();
                                RadAjaxManager1.FocusControl(tbQuote.ClientID);
                                tbQuote.Enabled = true;
                                btnQuote.Enabled = true;
                            }
                            else
                            {
                                RadAjaxManager1.FocusControl(tbFileNum.ClientID);
                                tbQuote.Enabled = false;
                                tbLeg.Enabled = false;
                                btnQuote.Enabled = false;
                                btnLeg.Enabled = false;
                                lbcvFileNum.Text = "Invalid Charter Quote File No.";
                            }
                        }
                    }
                    else
                    {
                        tbQuote.Enabled = false;
                        tbLeg.Enabled = false;
                        btnQuote.Enabled = false;
                        btnLeg.Enabled = false;
                        lbcvFileNum.Text = "Invalid Charter Quote File No.";
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void SearchQuote_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckQuoteExist(tbQuote);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void CheckQuoteExist(TextBox txtbx)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnQuote.Value = string.Empty;
                    lbcvQuote.Text = string.Empty;
                    tbLeg.Text = string.Empty;
                    lbcvLeg.Text = string.Empty;
                    hdnLeg.Value = string.Empty;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (CharterQuoteService.CharterQuoteServiceClient objcharterSerivce = new CharterQuoteService.CharterQuoteServiceClient())
                        {
                            if (!string.IsNullOrEmpty(hdnFileNum.Value))
                            {
                                var objRetVal = objcharterSerivce.GetQuoteByFileID(Convert.ToInt64(hdnFileNum.Value)).EntityList.Where(x => x.QuoteNUM == Convert.ToInt64(txtbx.Text.Trim())).ToList();
                                if (objRetVal.Count > 0)
                                {
                                    hdnQuote.Value = objRetVal[0].CQMainID.ToString();
                                    tbLeg.Enabled = true;
                                    btnLeg.Enabled = true;
                                }
                                else
                                {
                                    hdnQuote.Value = string.Empty;
                                    lbcvQuote.Text = "Invalid Charter Quote No.";
                                    tbLeg.Enabled = false;
                                    btnLeg.Enabled = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        tbLeg.Enabled = false;
                        btnLeg.Enabled = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void SearchLeg_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckLegExist(tbLeg);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void CheckLegExist(TextBox txtbx)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    hdnLeg.Value = string.Empty;
                    lbcvLeg.Text = string.Empty;

                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        string legIds = txtbx.Text.Trim();
                        string[] legIDArr = legIds.Split(',');

                        for (int i = 0; i < legIDArr.Length; i++)
                        {
                            string legid = string.Empty;
                            legid = legIDArr[i];

                            using (CharterQuoteService.CharterQuoteServiceClient objcharterSerivce = new CharterQuoteService.CharterQuoteServiceClient())
                            {
                                if (!string.IsNullOrEmpty(hdnQuote.Value))
                                {
                                    var objRetVal = objcharterSerivce.GetCQLegsByQuoteID(Convert.ToInt64(hdnQuote.Value)).EntityList.Where(x => x.LegNUM == Convert.ToInt64(legid.Trim())).ToList();
                                    if (objRetVal.Count > 0)
                                    {
                                        hdnLeg.Value += objRetVal[0].CQLegID.ToString() + ",";
                                    }
                                    else
                                    {
                                        hdnLeg.Value = string.Empty;
                                        lbcvLeg.Text = "Invalid Charter Leg";
                                    }
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(hdnLeg.Value))
                            hdnLeg.Value = hdnLeg.Value.Substring(0, hdnLeg.Value.Length - 1);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void tbTypeCode_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnTypeCode);
                        lbcvTypeCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnTypeCode.Value = string.Empty;
                        if (!string.IsNullOrEmpty(tbTypeCode.Text))
                        {
                            List<FlightPak.Web.FlightPakMasterService.GetAllAircraft> AircraftList = new List<FlightPakMasterService.GetAllAircraft>();
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetAircraftList();
                                if (objRetVal.ReturnFlag)
                                {
                                    AircraftList = objRetVal.EntityList.Where(x => x.AircraftCD.ToUpper() == tbTypeCode.Text.ToUpper()).ToList();
                                    if (AircraftList != null && AircraftList.Count > 0)
                                    {
                                        hdnTypeCode.Value = AircraftList[0].AircraftID.ToString();
                                    }
                                    else
                                    {
                                        hdnTypeCode.Value = string.Empty;
                                        lbcvTypeCode.Text = System.Web.HttpUtility.HtmlEncode("Invalid Aircraft Type Code");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbTypeCode);
                                    }
                                }
                                else
                                {
                                    hdnTypeCode.Value = string.Empty;
                                    lbcvTypeCode.Text = System.Web.HttpUtility.HtmlEncode("Invalid Aircraft Type Code");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbTypeCode);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void tbCustomer_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnCustomer);
                        lbcvCustomer.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnCustomer.Value = string.Empty;
                        if (!string.IsNullOrEmpty(tbCustomer.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPak.Web.FlightPakMasterService.CQCustomer objCQCustomer = new FlightPakMasterService.CQCustomer();
                                objCQCustomer.CQCustomerCD = tbCustomer.Text.Trim();
                                var objRetVal = objMasterService.GetCQCustomerByCQCustomerCD(objCQCustomer).EntityList;
                                if (objRetVal != null && objRetVal.Count > 0)
                                {
                                    hdnCustomer.Value = objRetVal[0].CQCustomerID.ToString();
                                    tbCustomer.Text = objRetVal[0].CQCustomerCD.ToString();
                                }
                                else
                                {
                                    hdnCustomer.Value = string.Empty;
                                    lbcvCustomer.Text = System.Web.HttpUtility.HtmlEncode("Invalid Customer Code");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbCustomer);
                                }
                            }
                        }
                        else
                        {
                            tbCustomer.Text = string.Empty;
                            hdnCustomer.Value = string.Empty;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCustomer);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void tbSalesPerson_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnSalesPerson.Value = string.Empty;
                        lbcvSalesPerson.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        if (!string.IsNullOrEmpty(tbSalesPerson.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var RetValue = objMasterService.GetSalesPersonList().EntityList.Where(x => x.SalesPersonCD != null && x.SalesPersonCD.Trim().ToUpper().Equals(tbSalesPerson.Text.ToString().ToUpper().Trim()));
                                if (RetValue.Count() == 0 || RetValue == null)
                                {
                                    lbcvSalesPerson.Text = System.Web.HttpUtility.HtmlEncode("Invalid Salesperson Code");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbSalesPerson);
                                }
                                else
                                {
                                    List<FlightPakMasterService.GetSalesPerson> SalesPersonList = new List<FlightPakMasterService.GetSalesPerson>();
                                    SalesPersonList = (List<FlightPakMasterService.GetSalesPerson>)RetValue.ToList();
                                    hdnSalesPerson.Value = SalesPersonList[0].SalesPersonID.ToString();
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        #endregion


        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (e.Argument.ToString() == "SearchBox_TextChanged")
                {
                    SearchBox_TextChanged(sender, e);
                }
                if (e.Argument.ToString() == "SearchQuote_TextChanged")
                {
                    SearchQuote_TextChanged(sender, e);
                }
                if (e.Argument.ToString() == "SearchLeg_TextChanged")
                {
                    SearchLeg_TextChanged(sender, e);
                }
                if (e.Argument.ToString() == "tbCQClient_OnTextChanged")
                {
                    tbCQClient_OnTextChanged(sender, e);
                }
                if (e.Argument.ToString() == "tbCQHomeBase_OnTextChanged")
                {
                    tbCQHomeBase_OnTextChanged(sender, e);
                }
                if (e.Argument.ToString() == "tbCQReportID_OnTextChanged")
                {
                    tbCQReportID_OnTextChanged(sender, e);
                }
                if (e.Argument.ToString() == "tbCQUserGroup_OnTextChanged")
                {
                    tbCQUserGroup_OnTextChanged(sender, e);
                }
                if (e.Argument.ToString() == "tbCustomer_OnTextChanged")
                {
                    tbCustomer_OnTextChanged(sender, e);
                }
                if (e.Argument.ToString() == "tbHomebase_OnTextChanged")
                {
                    tbHomebase_OnTextChanged(sender, e);
                }
                if (e.Argument.ToString() == "tbHomebase_OnTextChanged")
                {
                    tbHomebase_OnTextChanged(sender, e);
                }
                if (e.Argument.ToString() == "tbSalesPerson_TextChanged")
                {
                    tbSalesPerson_TextChanged(sender, e);
                }
                if (e.Argument.ToString() == "tbTailNumber_OnTextChanged")
                {
                    tbTailNumber_OnTextChanged(sender, e);
                }
                if (e.Argument.ToString() == "tbTypeCode_OnTextChanged")
                {
                    tbTypeCode_OnTextChanged(sender, e);
                }
                if (e.Argument.ToString() == "tbVendorCode_OnTextChanged")
                {
                    tbVendorCode_OnTextChanged(sender, e);
                }
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
            }
        }

        protected void tbCQHomeBase_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckhomebaseExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void tbCQClient_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckClientCodeExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void tbCQUserGroup_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckUserGroupExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void tbCQReportID_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckReportIDExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        private bool CheckhomebaseExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                if (tbCQHomeBase.Text.Trim() != string.Empty)
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objCompanySvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objCompanySvc.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD.Trim().ToUpper() == (tbCQHomeBase.Text.Trim().ToUpper())).ToList();
                        if (objRetVal.Count != 0)
                        {
                            List<FlightPakMasterService.GetAllCompanyMaster> CompanyList = new List<FlightPakMasterService.GetAllCompanyMaster>();
                            CompanyList = (List<FlightPakMasterService.GetAllCompanyMaster>)objRetVal.ToList();
                            hdnCQHomeBaseID.Value = CompanyList[0].HomebaseID.ToString();
                            RadAjaxManager1.FocusControl(tbCQClient.ClientID);
                            returnVal = false;
                        }
                        else
                        {
                            cvCQHomeBase.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCQHomeBase.ClientID);
                            returnVal = true;
                        }
                    }
                }
                return returnVal;
            }
        }
        private bool CheckClientCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (tbCQClient.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetClientByClientCD(tbCQClient.Text.Trim().ToUpper().ToString()).EntityList;
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.ClientByClientCDResult> ClientList = new List<FlightPakMasterService.ClientByClientCDResult>();
                                ClientList = (List<FlightPakMasterService.ClientByClientCDResult>)objRetVal.ToList();
                                hdnCQClientID.Value = ClientList[0].ClientID.ToString();
                                RadAjaxManager1.FocusControl(tbCQUserGroup.ClientID);
                                returnVal = false;
                            }
                            else
                            {
                                cvCQClient.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCQClient.ClientID);
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckUserGroupExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (tbCQUserGroup.Text.Trim() != string.Empty)
                    {
                        using (AdminService.AdminServiceClient objUserGroupsvc = new AdminService.AdminServiceClient())
                        {
                            var objRetVal = objUserGroupsvc.GetUserGroupList(UserPrincipal.Identity._customerID).EntityList.Where(x => x.UserGroupCD.Trim().ToUpper().ToString() == tbCQUserGroup.Text.Trim().ToUpper().ToString()).ToList();
                            if (objRetVal.Count != 0)
                            {
                                List<AdminService.UserGroup> UserGroupList = new List<AdminService.UserGroup>();
                                UserGroupList = (List<AdminService.UserGroup>)objRetVal.ToList();
                                hdnCQUserGroupID.Value = UserGroupList[0].UserGroupID.ToString();
                                returnVal = false;
                            }
                            else
                            {
                                cvCQUserGroup.IsValid = false;
                                RadAjaxManager1.FocusControl(tbCQClient.ClientID);
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckReportIDExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                if (tbCQReportID.Text.Trim() != string.Empty)
                {
                    using (CharterQuoteService.CharterQuoteServiceClient CqReportService = new CharterQuoteService.CharterQuoteServiceClient())
                    {
                        var CQReportHeaderInfo = CqReportService.GetCQHeaderList().EntityList.Where(x => x.ReportID.Trim().ToUpper().ToString() == tbCQReportID.Text.Trim().ToUpper().ToString()).ToList();
                        if (CQReportHeaderInfo.Count != 0)
                        {
                            cvCQReportID.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCQReportID.ClientID);
                            returnVal = true;
                            //List<GetTripSheetReportHeader> TripSheetReportHeaderList = new List<GetTripSheetReportHeader>();
                            //TripSheetReportHeaderList = (List<GetTripSheetReportHeader>)TripSheetReportHeaderInfo.ToList();
                            //hdnCQReportID.Value = TripSheetReportHeaderList[0].TripSheetReportHeaderID.ToString();
                            //RadAjaxManager1.FocusControl(tbCQDescription.ClientID);
                            //returnVal = false;                            
                        }
                        else
                        {
                            RadAjaxManager1.FocusControl(tbCQReportDescription.ClientID);
                            returnVal = false;
                        }
                    }
                }
                return returnVal;
            }
        }


        protected void dgCharterQuoteWriter_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        List<GetCQReportHeader> CQReportHeaderList = new List<GetCQReportHeader>();
                        using (CharterQuoteService.CharterQuoteServiceClient CQReportService = new CharterQuoteService.CharterQuoteServiceClient())
                        {
                            var CQReportHeaderInfo = CQReportService.GetCQHeaderList();
                            if (CQReportHeaderInfo.ReturnFlag == true)
                            {
                                CQReportHeaderList = CQReportHeaderInfo.EntityList.Where(x => x.IsDeleted == false).ToList<GetCQReportHeader>();
                            }
                            dgCharterQuoteWriter.DataSource = CQReportHeaderList;
                            Session["CQReportHeaderList"] = CQReportHeaderList;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void dgCharterQuoteWriter_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (btnSave.Visible == false)
                        {
                            GridDataItem item = dgCharterQuoteWriter.SelectedItems[0] as GridDataItem;
                            Session["ReportID"] = item["ReportID"].Text;
                            Session["CQReportHeaderID"] = item["CQReportHeaderID"].Text;
                            EnableForm(false);
                            LoadControlData(Session["ReportID"].ToString());
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void dgCharterQuoteWriter_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                GridEnable(false, false, false);
                                hdnSave.Value = "Update";
                                EnableForm(true);
                                RadAjaxManager1.FocusControl(tbCQReportDescription.ClientID);
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                GridEnable(false, false, false);
                                ClearForm();
                                hdnSave.Value = "Save";
                                EnableForm(true);
                                RadAjaxManager1.FocusControl(tbCQReportID.ClientID);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    column.CurrentFilterValue = string.Empty;
                                    column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void dgCharterQuoteWriter_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidateCustom = true;
                        if (CheckhomebaseExist())
                        {
                            cvCQHomeBase.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCQHomeBase.ClientID);
                            IsValidateCustom = false;
                        }
                        if (CheckClientCodeExist())
                        {
                            cvCQClient.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCQClient.ClientID);
                            IsValidateCustom = false;
                        }
                        if (CheckUserGroupExist())
                        {
                            cvCQUserGroup.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCQUserGroup.ClientID);
                            IsValidateCustom = false;
                        }
                        if (IsValidateCustom)
                        {
                            using (CharterQuoteService.CharterQuoteServiceClient objService = new CharterQuoteService.CharterQuoteServiceClient())
                            {
                                CharterQuoteService.CQReportHeader oCQReportHeader = new CharterQuoteService.CQReportHeader();
                                oCQReportHeader = GetItems(oCQReportHeader);
                                var Result = objService.UpdateCQReportHeader(oCQReportHeader);
                                if (Result.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                }
                                dgCharterQuoteWriter.Rebind();
                                EnableForm(false);
                                GridEnable(true, true, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void dgCharterQuoteWriter_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidateCustom = true;
                        if (CheckReportIDExist())
                        {
                            cvCQReportID.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCQReportID.ClientID);
                            IsValidateCustom = false;
                        }
                        if (CheckhomebaseExist())
                        {
                            cvCQHomeBase.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCQHomeBase.ClientID);
                            IsValidateCustom = false;
                        }
                        if (CheckClientCodeExist())
                        {
                            cvCQClient.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCQClient.ClientID);
                            IsValidateCustom = false;
                        }
                        if (CheckUserGroupExist())
                        {
                            cvCQUserGroup.IsValid = false;
                            RadAjaxManager1.FocusControl(tbCQUserGroup.ClientID);
                            IsValidateCustom = false;
                        }
                        if (IsValidateCustom)
                        {
                            using (CharterQuoteService.CharterQuoteServiceClient objService = new CharterQuoteService.CharterQuoteServiceClient())
                            {
                                CharterQuoteService.CQReportHeader oCQReportHeader = new CharterQuoteService.CQReportHeader();
                                oCQReportHeader = GetItems(oCQReportHeader);
                                var Result = objService.AddCQReportHeader(oCQReportHeader);
                                if (Result.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                }
                                dgCharterQuoteWriter.Rebind();
                                EnableForm(false);
                                GridEnable(true, true, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
                finally
                {
                }
            }
        }
        protected void dgCharterQuoteWriter_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            e.Canceled = true;
                            using (CharterQuoteService.CharterQuoteServiceClient objService = new CharterQuoteService.CharterQuoteServiceClient())
                            {
                                hdnSave.Value = "Delete";
                                CharterQuoteService.CQReportHeader oCQReportHeader = new CharterQuoteService.CQReportHeader();
                                oCQReportHeader = GetItems(oCQReportHeader);
                                var Result = objService.DeleteCQReportHeader(oCQReportHeader);
                                dgCharterQuoteWriter.Rebind();
                                if (dgCharterQuoteWriter.Items.Count > 0)
                                {
                                    dgCharterQuoteWriter.SelectedIndexes.Add(0);
                                    SetDefaultRecord();
                                    if (dgCharterQuoteWriter.SelectedItems.Count > 0)
                                    {
                                        GridDataItem item = (GridDataItem)dgCharterQuoteWriter.SelectedItems[0];
                                        Session["ReportID"] = item.GetDataKeyValue("ReportID").ToString();
                                        LoadControlData(item.GetDataKeyValue("ReportID").ToString());
                                    }
                                }
                                else
                                {
                                    tbCQReportID.Text = string.Empty;
                                    tbCQReportDescription.Text = string.Empty;
                                    tbCQHomeBase.Text = string.Empty;
                                    tbCQClient.Text = string.Empty;
                                    tbCQUserGroup.Text = string.Empty;
                                }
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                    }
                    finally
                    {
                    }
                }
            }
        }

        private CQReportHeader GetItems(CQReportHeader oCQReportHeader)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCQReportHeader))
            {
                oCQReportHeader.CQReportHeaderID = -1;
                if ((hdnSave.Value == "Update") || (hdnSave.Value == "Delete"))
                {
                    if (!string.IsNullOrEmpty(hdnCQReportID.Value))
                        oCQReportHeader.CQReportHeaderID = Convert.ToInt64(hdnCQReportID.Value);
                }
                if (!string.IsNullOrEmpty(tbCQReportID.Text))
                {
                    oCQReportHeader.ReportID = tbCQReportID.Text;
                }
                if (!string.IsNullOrEmpty(tbCQReportDescription.Text))
                {
                    oCQReportHeader.ReportDescription = tbCQReportDescription.Text;
                }
                if (!string.IsNullOrEmpty(tbCQHomeBase.Text))
                {
                    oCQReportHeader.HomebaseID = Convert.ToInt64(hdnCQHomeBaseID.Value);
                }
                if (!string.IsNullOrEmpty(tbCQClient.Text))
                {
                    oCQReportHeader.ClientID = Convert.ToInt64(hdnCQClientID.Value);
                }
                if (!string.IsNullOrEmpty(tbCQUserGroup.Text))
                {
                    oCQReportHeader.UserGroupID = Convert.ToInt64(hdnCQUserGroupID.Value);
                }
                oCQReportHeader.IsDeleted = false;
                return oCQReportHeader;
            }
        }
        private void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                if ((hdnSave.Value == "Save") && (Enable == true))
                {
                    tbCQReportID.Enabled = true;
                }
                else
                {
                    tbCQReportID.Enabled = false;
                }
                tbCQReportDescription.Enabled = Enable;
                tbCQHomeBase.Enabled = Enable;
                btnCQHomeBase.Enabled = Enable;
                tbCQClient.Enabled = Enable;
                btnCQClient.Enabled = Enable;
                tbCQUserGroup.Enabled = Enable;
                btnCQUserGroup.Enabled = Enable;
                btnSave.Visible = Enable;
                btnCancel.Visible = Enable;
            }
        }
        private void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbCQReportID.Text = string.Empty;
                hdnCQReportID.Value = string.Empty;
                tbCQReportDescription.Text = string.Empty;
                tbCQHomeBase.Text = string.Empty;
                tbCQClient.Text = string.Empty;
                tbCQUserGroup.Text = string.Empty;
                hdnSave.Value = string.Empty;
                hdnCQHomeBaseID.Value = string.Empty;
                hdnCQClientID.Value = string.Empty;
                hdnCQUserGroupID.Value = string.Empty;
            }
        }
        private void LoadControlData(string ReportID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ReportID))
            {
                if (dgCharterQuoteWriter.SelectedItems.Count != 0)
                {
                    GridDataItem Item = dgCharterQuoteWriter.SelectedItems[0] as GridDataItem;
                    if (Session["ReportID"] != null)
                    {
                        if (Item["ReportID"].Text.Trim() == Session["ReportID"].ToString().Trim())
                        {
                            if (Item.GetDataKeyValue("CQReportHeaderID") != null)
                            {
                                hdnCQReportID.Value = Item.GetDataKeyValue("CQReportHeaderID").ToString().Trim();
                                Session["CQReportHeaderID"] = Item.GetDataKeyValue("CQReportHeaderID").ToString().Trim();
                            }
                            else
                            {
                                hdnCQReportID.Value = string.Empty;
                                Session["CQReportHeaderID"] = null;
                            }
                            if (Item.GetDataKeyValue("ReportID") != null)
                            {
                                tbCQReportID.Text = Item.GetDataKeyValue("ReportID").ToString().Trim();
                            }
                            else
                            {
                                tbCQReportID.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("ReportDescription") != null)
                            {
                                tbCQReportDescription.Text = Item.GetDataKeyValue("ReportDescription").ToString().Trim();
                            }
                            else
                            {
                                tbCQReportDescription.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("HomebaseCD") != null)
                            {
                                tbCQHomeBase.Text = Item.GetDataKeyValue("HomebaseCD").ToString().Trim();
                                hdnCQHomeBaseID.Value = Item.GetDataKeyValue("HomebaseID").ToString().Trim();
                            }
                            else
                            {
                                tbCQHomeBase.Text = string.Empty;
                                hdnCQHomeBaseID.Value = string.Empty;
                            }
                            if (Item.GetDataKeyValue("ClientCD") != null)
                            {
                                tbCQClient.Text = Item.GetDataKeyValue("ClientCD").ToString().Trim();
                                hdnCQClientID.Value = Item.GetDataKeyValue("ClientID").ToString().Trim();
                            }
                            else
                            {
                                tbCQClient.Text = string.Empty;
                                hdnCQClientID.Value = string.Empty;
                            }
                            if (Item.GetDataKeyValue("UserGroupCD") != null)
                            {
                                tbCQUserGroup.Text = Item.GetDataKeyValue("UserGroupCD").ToString().Trim();
                                hdnCQUserGroupID.Value = Item.GetDataKeyValue("UserGroupID").ToString().Trim();
                            }
                            else
                            {
                                tbCQUserGroup.Text = string.Empty;
                                hdnCQUserGroupID.Value = string.Empty;
                            }
                        }
                    }
                }
            }
        }
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton insertCtl, editCtl, delCtl;
                    insertCtl = (LinkButton)dgCharterQuoteWriter.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                    delCtl = (LinkButton)dgCharterQuoteWriter.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                    editCtl = (LinkButton)dgCharterQuoteWriter.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                    bool AddTrip = true;
                    bool EditTrip = true;
                    bool DeleteTrip = true;
                    if (AddTrip)
                    {
                        insertCtl.Visible = true;
                        if (add)
                            insertCtl.Enabled = true;
                        else
                            insertCtl.Enabled = false;
                    }
                    else
                    {
                        insertCtl.Visible = false;
                    }
                    if (DeleteTrip)
                    {
                        delCtl.Visible = true;
                        if (delete)
                        {
                            delCtl.Enabled = true;
                            delCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            delCtl.Enabled = false;
                            delCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        delCtl.Visible = false;
                    }
                    if (EditTrip)
                    {
                        editCtl.Visible = true;
                        if (edit)
                        {
                            editCtl.Enabled = true;
                            editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            editCtl.Enabled = false;
                            editCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        editCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            (dgCharterQuoteWriter.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgCharterQuoteWriter.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                        dgCharterQuoteWriter.Rebind();
                        GridEnable(true, true, true);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ClearForm();
                        EnableForm(false);
                        if (Session["ReportID"] != null)
                        {
                            LoadControlData(Session["ReportID"].ToString());
                        }
                        GridEnable(true, true, true);
                        if (dgCharterQuoteWriter.Items.Count > 0)
                        {
                            dgCharterQuoteWriter.SelectedIndexes.Add(0);
                            SetDefaultRecord();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        private void SetDefaultRecord()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Doubt
                foreach (GridDataItem Item in dgCharterQuoteWriter.Items)
                {
                    if (Item.GetDataKeyValue("ReportID") != null)
                    {
                        if ((UserPrincipal != null) &&
                            (UserPrincipal.Identity != null) &&
                            (UserPrincipal.Identity._fpSettings != null) &&
                            (!string.IsNullOrEmpty(UserPrincipal.Identity._fpSettings._TripsheetRptWriteGroup)))
                        {
                            if (Item.GetDataKeyValue("ReportID").ToString().ToUpper().Trim() == UserPrincipal.Identity._fpSettings._TripsheetRptWriteGroup.ToUpper().Trim())
                            {
                                Item.Selected = true;
                                Session["ReportID"] = Item.GetDataKeyValue("ReportID").ToString();
                                Session["CQReportHeaderID"] = Item.GetDataKeyValue("CQReportHeaderID").ToString();
                                LoadControlData(Item.GetDataKeyValue("ReportID").ToString());
                                break;
                            }
                        }
                        else
                        {
                            if (Item.GetDataKeyValue("ReportID").ToString().Trim().ToUpper() == "DEFAULT")
                            {
                                Item.Selected = true;
                                Session["ReportID"] = Item.GetDataKeyValue("ReportID").ToString();
                                Session["CQReportHeaderID"] = Item.GetDataKeyValue("CQReportHeaderID").ToString();
                                LoadControlData(Item.GetDataKeyValue("ReportID").ToString());
                                break;
                            }
                        }
                    }
                }
            }
        }
        private void OnPageLoad()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCharterQuoteWriter, dgCharterQuoteWriter, RadAjaxLoadingPanel1);
                RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgCharterQuoteWriter.ClientID));
                EnableForm(false);
                dgCharterQuoteWriter.Rebind();
                if (dgCharterQuoteWriter.Items.Count > 0)
                {
                    dgCharterQuoteWriter.SelectedIndexes.Add(0);
                    SetDefaultRecord();
                    if (dgCharterQuoteWriter.SelectedItems.Count > 0)
                    {
                        GridDataItem item = (GridDataItem)dgCharterQuoteWriter.SelectedItems[0];
                        Session["ReportID"] = item.GetDataKeyValue("ReportID").ToString();
                        LoadControlData(item.GetDataKeyValue("ReportID").ToString());
                    }
                }
                GetReportNumber();
            }
        }
        private void GetReportNumber()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, int> dicRptType = new Dictionary<string, int>();
                if (Request.QueryString["xmlFilename"] != null)
                {
                    if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "CharterMainTab.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 5);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "CQItinerary.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 1);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "CQNotesAlerts.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 2);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "CQAircraftProfile.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 3);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "CQTripStatus.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 4);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "CQPaxProfile.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 6);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "CQQuoteLog.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 7);
                    }

                    Session["SnxmlFilename"] = dicRptType;
                }
            }
        }
    }
}

