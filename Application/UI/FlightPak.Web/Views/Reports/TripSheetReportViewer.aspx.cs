﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Xml.Linq;
using System.IO;
using System.Globalization;
using System.Text;
using Telerik.Web.UI;
using System.Xml;
using System.Drawing;
using System.Data;
using FlightPak.Web.Framework.Prinicipal;
//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using System.Data;
using FlightPak.Web.PreflightService;
using FlightPak.Web.FlightPakMasterService; //ajss\
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Reports
{
    public partial class TripSheetReportViewer : BaseSecuredPage
    {
        //TODO Server.MapPath
        string urlBase = "Config\\";
        public Dictionary<string, string> dicColumnList = new Dictionary<string, string>();
        public Dictionary<string, string> dicSelectedList = new Dictionary<string, string>();
        public Dictionary<string, string> dicSelectedListsort = new Dictionary<string, string>();
        public Dictionary<string, string> dicSelectedListsortSel = new Dictionary<string, string>();
        public Dictionary<string, string> dicColumnListsort = new Dictionary<string, string>();
        public FlightPakMasterService.Company CompanySett = new FlightPakMasterService.Company();
        string filename = string.Empty;
        private ExceptionManager exManager;
        string ReportName = string.Empty;
        string DateFormat = string.Empty;
        public string reportCriteriaType = string.Empty;
        const string filterReportSessionName = "filterReport";

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!HaveModuleAccess(ModuleId.TripsheetRWReports))
            {
                Response.Redirect("~/Views/Home.aspx?m=TripsheetRWReports");
            }

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lblError.Text = "";
                        if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                        }
                        else
                        {
                            DateFormat = "MM/dd/yyyy";
                        }

                        //==Sudhakar::Disable Date Range
                        if (rdBl.Visible)
                            reportCriteriaType = rdBl.SelectedValue.ToString();
                        else
                            reportCriteriaType = "TripNumber";
                        //==============================
                        Session["TabSelect"] = "PreFlight";
                        SelectTab();
                        if (!IsPostBack)
                        {
                            if (!string.IsNullOrEmpty(TripNum.Text))
                            {
                                Leg.Enabled = true;
                                Passenger.Enabled = true;
                            }
                            Wizard1.Visible = false;
                            TripExcel.Visible = false;
                            TripExcelGdl.Visible = false;
                            ExportPanal.Visible = false;

                            //    PopupEnable();
                            OnPageLoad();   //ajss

                            if (Request.QueryString["tripnum"] != null && Request.QueryString["tripid"] != null)
                            {
                                string tripnum = string.Empty;
                                string tripid = string.Empty;
                                tripnum = Request.QueryString["tripnum"].ToString();
                                TripNum.Text = tripnum;
                                tripid = Request.QueryString["tripid"].ToString();
                                hdnTripNum.Value = tripid;
                                Leg.Enabled = true;
                                btnLeg.Enabled = true;
                                Passenger.Enabled = true;
                                btnPassenger.Enabled = true;
                                Session["TripDetails"] = tripnum + ":::" + tripid + "::";
                            }
                            DefaultSelection(true);
                            EnableReportSheet(false);

                            if (Session["Base64String"] != null)
                            {
                                PDFViewerFrame.Attributes.Add("src", "/Views/Reports/PDFViewer.aspx");
                                PDFViewerFrame.Visible = true;
                                ShowSubLinks((List<int>)Session["ReportNumbers"]);
                                GenerateControls();
                                EnableControlAsPerTripNumber();

                            }
                            TripControlsDisplay();
                        }
                        else
                        {
                            PDFViewerFrame.Visible = false;
                        }


                        if (btnRptView.Visible)
                            btnRptView.Focus();

                    }, FlightPak.Common.Constants.Policy.UILayer);

                    if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                    if (!string.IsNullOrEmpty(tbDepartmentCD.Text))
                    {
                        tbAuth.Enabled = true;
                        btnAuth.Enabled = true;
                        CheckDepartmentExist();
                    }
                    else
                    {
                        tbAuth.Enabled = false;
                        btnAuth.Enabled = false;
                        lbdept.Text = string.Empty;
                    }

                    if (Session["TripDetails"] != null)
                    {
                        string[] details = Session["TripDetails"].ToString().Split(':');
                        if (!string.IsNullOrEmpty(details[1]))
                        {
                            Leg.Enabled = true;
                            btnLeg.Enabled = true;
                            Passenger.Enabled = true;
                            btnPassenger.Enabled = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }

        protected void SearchBox_TextChanged(object sender, EventArgs e)
        {
            EnableControlAsPerTripNumber();
        }

        private void EnableControlAsPerTripNumber()
        {
            if (!string.IsNullOrEmpty(TripNum.Text))
            {
                PreflightMain Trip = new PreflightMain();


                Int64 SearchTripNum = 0;
                if (Int64.TryParse(TripNum.Text, out SearchTripNum))
                {
                    using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                    {
                        var objRetVal = PrefSvc.GetTripByTripNum(SearchTripNum);
                        if (objRetVal.ReturnFlag)
                        {
                            Trip = objRetVal.EntityList[0];
                            lbTripSearchMessage.Text = string.Empty;
                            hdnTripNum.Value = Trip.TripID.ToString();
                            //Session["Trip"] = TripNum.Text;
                            Leg.Enabled = true;
                            Leg.Text = string.Empty;
                            btnLeg.Enabled = true;
                            Passenger.Enabled = true;
                            btnPassenger.Enabled = true;
                        }
                        else
                        {
                            //lbTripSearchMessage.Text = "Trip No Does Not Exist";
                            lbTripSearchMessage.Text = "Invalid Trip Number";
                            hdnTripNum.Value = "";
                            //Session["Trip"] = "";
                            Leg.Enabled = false;
                            Leg.Text = string.Empty;
                            btnLeg.Enabled = false;
                            Passenger.Enabled = false;
                            btnPassenger.Enabled = false;
                        }
                    }
                }
                else
                {
                    lbTripSearchMessage.Text = "Invalid Trip Number";
                    hdnTripNum.Value = "";
                    //Session["Trip"] = "";
                    Leg.Enabled = false;
                    Leg.Text = string.Empty;
                    btnLeg.Enabled = false;
                    Passenger.Enabled = false;
                    btnPassenger.Enabled = false;
                }

            }
            else
            {
                hdnTripNum.Value = "";
                //Session["Trip"] = "";
                lbTripSearchMessage.Text = "";
                Leg.Enabled = false;
                Leg.Text = string.Empty;
                btnLeg.Enabled = false;
                Passenger.Enabled = false;
                btnPassenger.Enabled = false;
            }

        }

        public void Page_Init(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //karthik - Removing this code to check
                        //GenerateControls();
                        //ProcessColumns(string.Empty);
                        //UCRHS.ColumnList = dicColumnList;
                        //UCRHSort.ColumnList = dicColumnList;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        private void GenerateControls()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                filename = Convert.ToString(Request.QueryString["xmlFilename"]);
                if (Session["ReportNameBylink"] != null)
                    filename = Convert.ToString(Session["ReportNameBylink"]);
                if (filename != null)
                {
                    //==Sudhakar::Disable Date Range
                    if (filename == "PRETSPairForm.xml" || filename == "PRETSGeneralDeclaration.xml" || filename == "PREGeneralDeclarationAPDX.xml")
                        rdBl.Visible = false;
                    //==============================

                    var filePath = Server.MapPath(urlBase + filename);
                    XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                    var xmlReaderObject = xmlReader.XmlCleanReader();
                    if (xmlReaderObject != null)
                    {
                        XPathDocument SearchFieldDoc = new
                        XPathDocument(xmlReaderObject);
                        XslCompiledTransform transform = new XslCompiledTransform();
                        transform.Load(Server.MapPath(urlBase + "MakeControls.xslt"));
                        StringWriter sw = new StringWriter();
                        transform.Transform(SearchFieldDoc, null, sw);
                        string result = sw.ToString();
                        result = result.Replace("xmlns:asp=\"remove\"", "");
                        Control ctrl = Page.ParseControl(result);
                        //   Report.Controls.Add(ctrl);
                        string[] strArr = GetReportTitle().Split(',');
                        if (strArr.Count() > 0)
                        {
                            lbTitle.Text = Convert.ToString(strArr[0]);
                        }
                        Controlvisible(true);
                    }

                    //Prakash 29-04-13
                    if (Session["TripDetails"] != null)
                    {
                        string[] tripDetails = Session["TripDetails"].ToString().Split(':');

                        if (!string.IsNullOrEmpty(tripDetails[0]))
                        {
                            TripNum.Text = tripDetails[0];
                        }
                        if (!string.IsNullOrEmpty(tripDetails[1]))
                        {
                            Leg.Text = tripDetails[1];
                        }
                        if (!string.IsNullOrEmpty(tripDetails[2]))
                        {
                            Passenger.Text = tripDetails[2];
                        }
                        if (!string.IsNullOrEmpty(tripDetails[3]))
                        {
                            hdnTripNum.Value = tripDetails[3];
                        }
                        if (!string.IsNullOrEmpty(tripDetails[4]))
                        {
                            hdnLeg.Value = tripDetails[4];
                        }
                        if (!string.IsNullOrEmpty(tripDetails[5]))
                        {
                            hdnPassenger.Value = tripDetails[5];
                        }
                    }

                    if (Session["DateDetails"] != null)
                    {
                        List<string> dateList = new List<string>();

                        dateList = (List<string>)Session["DateDetails"];

                        TextBox tbDateFrom = (TextBox)DateFrom.FindControl("tbDate");
                        tbDateFrom.Text = dateList[0];
                        TextBox tbDateTo = (TextBox)DateTo.FindControl("tbDate");
                        tbDateTo.Text = dateList[1];

                        tbClientCode.Text = dateList[2];
                        hdnClientCode.Value = dateList[3];
                        tbHomebase.Text = dateList[4];
                        hdnHomebase.Value = dateList[5];
                        tbRequestor.Text = dateList[6];
                        hdnRequestor.Value = dateList[7];
                        tbCrew.Text = dateList[8];
                        hdnCrew.Value = dateList[9];
                        tbTailNumber.Text = dateList[10];
                        hdnTailNumber.Value = dateList[11];
                        tbDepartmentCD.Text = dateList[12];
                        hdnDepartment.Value = dateList[13];
                        tbAuth.Text = dateList[14];
                        hdnAuth.Value = dateList[15];
                        tbCat.Text = dateList[16];
                        hdnCat.Value = dateList[17];
                        if (!string.IsNullOrEmpty(dateList[18]) && dateList[18] == "true")
                            chkTrip.Checked = true;
                        else
                            chkTrip.Checked = false;

                        if (!string.IsNullOrEmpty(dateList[19]) && dateList[19] == "true")
                            chkCanceled.Checked = true;
                        else
                            chkCanceled.Checked = false;

                        if (!string.IsNullOrEmpty(dateList[20]) && dateList[20] == "true")
                            chkHold.Checked = true;
                        else
                            chkHold.Checked = false;

                        if (!string.IsNullOrEmpty(dateList[21]) && dateList[21] == "true")
                            chkWorkSheet.Checked = true;
                        else
                            chkWorkSheet.Checked = false;

                        if (!string.IsNullOrEmpty(dateList[22]) && dateList[22] == "true")
                            chkUnFulFilled.Checked = true;
                        else
                            chkUnFulFilled.Checked = false;

                        if (!string.IsNullOrEmpty(dateList[23]) && dateList[23] == "true")
                            chkScheduledService.Checked = true;
                        else
                            chkScheduledService.Checked = false;

                        reportCriteriaType = "DateRange";
                        rdBl.SelectedIndex = 1;
                    }
                    //Prakash 29-04-13
                }
                else
                {
                    Controlvisible(false);
                }
            }

        }

        private void SelectTab()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string TabSelect = Convert.ToString(Session["TabSelect"]);
                pnlReportTab.Items.Clear();
                string xmltab = string.Empty;
                switch (TabSelect)
                {
                    case "PreFlight":
                        xmltab = "PreFlightMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav_active";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav";
                        break;
                    case "PostFlight":
                        xmltab = "PostFlightMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav_active";
                        btnDatabase.CssClass = "rpt_nav";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav";
                        break;
                    case "Db":
                        xmltab = "DatabaseMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav_active";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav";
                        break;
                    case "CorpReq":
                        xmltab = "CorporateFlightMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav";
                        btnCorp.CssClass = "rpt_nav_active";
                        btnCharter.CssClass = "rpt_nav";
                        break;
                    case "Charter":

                        xmltab = "CharterMainTab.xml";
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav_active";
                        break;
                    default:
                        Session["TabSelect"] = "Db";
                        xmltab = "DatabaseMainTab.xml";
                        btnPreFlight.CssClass = "rpt_nav";
                        btnPostFlight.CssClass = "rpt_nav";
                        btnDatabase.CssClass = "rpt_nav_active";
                        btnCorp.CssClass = "rpt_nav";
                        btnCharter.CssClass = "rpt_nav";
                        Response.Redirect("ReportViewer.aspx?xmlFilename=DBAccounts.xml");
                        LoadTabs(pnlReportTab, urlBase + xmltab);
                        break;
                }
                ExpandPanel(pnlReportTab, xmltab, true);
            }
        }
        private void ExpandPanel(RadPanelBar panelBar, string TabXmlname, bool isExpand)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(panelBar, TabXmlname, isExpand))
            {
                string Selectedvalue = Convert.ToString(Request.QueryString["xmlFilename"]);
                string attrVal = "";
                string TitleTab = string.Empty;
                var filePath = Server.MapPath(urlBase + TabXmlname);
                XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                var xmlReaderObject = xmlReader.XmlCleanReader();
                if (xmlReaderObject != null)
                {
                    XDocument doc = XDocument.Load(xmlReaderObject);

                    if (Request.QueryString["xmlFilename"] != null)
                    {

                        var items = doc.Descendants("screen")
                                      .Where(c => c.Attribute("url").Value.Contains(Convert.ToString(Request.QueryString["xmlFilename"])))
                                       .Select(c => new { x = c.Value, y = c.Parent.Attribute("text").Value, z = c.Attribute("name").Value })
                                       .ToList();

                        if (items != null)
                        {
                            if (items.Count != 0)
                            {

                                RadPanelItem selectedItem = panelBar.FindItemByText(items[0].y);

                                if (selectedItem != null)
                                {
                                    if (selectedItem.Items.Count > 0)
                                    {

                                        RadPanelItem selectedvalue = selectedItem.Items.FindItemByText(items[0].z);
                                        selectedvalue.Selected = true;
                                        selectedItem.Expanded = true;

                                    }
                                    else
                                    {
                                        selectedItem.Selected = true;
                                        while ((selectedItem != null) &&
                                               (selectedItem.Parent.GetType() == typeof(RadPanelItem)))
                                        {
                                            selectedItem = (RadPanelItem)selectedItem.Parent;
                                            selectedItem.Expanded = true;
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }

        private void TripControlsDisplay()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Prakash 29-04-13
                if (Session["TripDetails"] == null && Session["DateDetails"] == null)
                {
                    ResetResults();
                }
                //Prakash 29-04-13
                //if (rdBl.SelectedValue.ToString() == "TripNumber")
                if (reportCriteriaType == "TripNumber")
                {
                    Report.Visible = false;
                    TripReport.Visible = true;
                    filename = Convert.ToString(Request.QueryString["xmlFilename"]);
                    plTripItinerary.Visible = (filename == "PRETSTripSheetItinerary.xml") ? true : false;
                    plTripsheetMain.Visible = (filename == "PreTSTripSheetMain.xml") ? true : false;
                    rfvLeg.Enabled = (filename == "PREGeneralDeclarationAPDX.xml" || filename == "PRETSGeneralDeclaration.xml") ? true : false;
                    lblLeg.Font.Bold = (filename == "PREGeneralDeclarationAPDX.xml" || filename == "PRETSGeneralDeclaration.xml") ? true : false;
                    plcanpass.Visible = (filename == "PRETSCanPassForm.xml") ? true : false;



                }
                //else if (rdBl.SelectedValue.ToString() == "DateRange")
                else if (reportCriteriaType == "DateRange")
                {
                    Report.Visible = true;
                    TripReport.Visible = false;
                    rfvLeg.Enabled = false;
                    plTripItinerary.Visible = (filename == "PRETSTripSheetItinerary.xml") ? true : false;
                    plTripsheetMain.Visible = (filename == "PreTSTripSheetMain.xml") ? true : false;
                    plcanpass.Visible = (filename == "PRETSCanPassForm.xml") ? true : false;
                }
            }

        }
        private void Controlvisible(bool flag)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flag))
            {
                btExport.Visible = flag;
                btnRptView.Visible = flag;
                btnReset.Visible = flag;

                if (flag)
                {
                    string RptName = string.Empty;
                    XmlDocument doc = new XmlDocument();
                    var filePath = Server.MapPath(urlBase + filename);
                    XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                    var xmlReaderObject = xmlReader.XmlCleanReader();
                    if (xmlReaderObject != null)
                    {
                        doc.Load(xmlReaderObject);

                        XmlNodeList elemList = doc.GetElementsByTagName("Report");

                        for (int i = 0; i < elemList.Count; i++)
                        {
                            RptName = elemList[i].Attributes["displayname"].Value + "," + elemList[i].Attributes["name"].Value;

                            if (elemList[i].Attributes["preview"].Value != null)
                            {
                                if (elemList[i].Attributes["preview"].Value.ToUpper().Equals("TRUE"))
                                {
                                    btnRptView.Visible = true;
                                }
                                else if (elemList[i].Attributes["preview"].Value.ToUpper().Equals("FALSE"))
                                {
                                    btnRptView.Visible = false;
                                }
                            }

                            if ((elemList[i].Attributes["excel"].Value != null) && (elemList[i].Attributes["mhtml"].Value != null))
                            {

                                if ((elemList[i].Attributes["excel"].Value.ToUpper().Equals("FALSE")) && (elemList[i].Attributes["mhtml"].Value.ToUpper().Equals("FALSE")))
                                {
                                    btExport.Visible = false;
                                }
                                else
                                {

                                    if (elemList[i].Attributes["excel"].Value.ToUpper().Equals("FALSE"))
                                    {
                                        ddlFormat.Items.Remove(new ListItem("Excel"));
                                    }

                                    if (elemList[i].Attributes["mhtml"].Value.ToUpper().Equals("FALSE"))
                                    {
                                        ddlFormat.Items.Remove(new ListItem("MHTML"));
                                    }

                                }

                            }
                            //else
                            //{
                            //    if (elemList[i].Attributes["excel"].Value != null)
                            //    {
                            //        if (elemList[i].Attributes["excel"].Value.ToUpper().Equals("FALSE"))
                            //        {
                            //            ddlFormat.Items.Remove(new ListItem("Excel"));


                            //        }
                            //    }

                            //    if (elemList[i].Attributes["mhtml"].Value != null)
                            //    {
                            //        if (elemList[i].Attributes["mhtml"].Value.ToUpper().Equals("FALSE"))
                            //        {
                            //            ddlFormat.Items.Remove(new ListItem("MHTML"));

                            //        }
                            //    }
                            //}

                            //if (elemList[i].Attributes["mhtml"].Value != null)
                            //{
                            //    if (elemList[i].Attributes["mhtml"].Value.ToUpper().Equals("FALSE"))
                            //    {
                            //      //  ddlFormat.Items.Remove(new ListItem("MHTML"));                                  
                            //    }
                            //}


                            break;
                        }
                    }
                }
            }
        }

        private string GetReportTitle()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                // Karthik - Added to check -- Will be removed in future.
                if (Session["ReportNameBylink"] != null)
                    filename = Convert.ToString(Session["ReportNameBylink"]);
                string RptName = string.Empty;
                XmlDocument doc = new XmlDocument();
                var filePath = Server.MapPath(urlBase + filename);
                XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
                var xmlReaderObject = xmlReader.XmlCleanReader();
                if (xmlReaderObject != null)
                {
                    doc.Load(xmlReaderObject);

                    XmlNodeList elemList = doc.GetElementsByTagName("Report");

                    for (int i = 0; i < elemList.Count; i++)
                    {
                        RptName = elemList[i].Attributes["displayname"].Value + "," + elemList[i].Attributes["name"].Value;
                        break;
                    }

                    return RptName;
                }
                return "";
            }
        }

        private string ProcessResults()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string Param = string.Empty;
                //if (rdBl.SelectedValue == "TripNumber")
                if (reportCriteriaType == "TripNumber")
                {
                    Param = ParamTripNumber();
                }
                else
                {
                    TextBox ucInsDate = (TextBox)DateFrom.FindControl("tbDate");
                    if (!string.IsNullOrEmpty(ucInsDate.Text.Trim().ToString()))
                    {
                        //Param = "BeginDate=" + Convert.ToDateTime(ucInsDate.Text.Trim(), CultureInfo.InvariantCulture).ToShortDateString();
                        Param = "BeginDate=" + Convert.ToDateTime(FormatDate(ucInsDate.Text.Trim(), DateFormat));
                    }
                    //if (!string.IsNullOrEmpty(((RadDatePicker)ucDateFrom.FindControl("RadDatePicker1")).DateInput.Text))
                    //{
                    //    Param = "BeginDate=" + ((RadDatePicker)ucDateFrom.FindControl("RadDatePicker1")).DateInput.Text;
                    //}
                    TextBox ucInsDate1 = (TextBox)DateTo.FindControl("tbDate");
                    if (!string.IsNullOrEmpty(ucInsDate1.Text.Trim().ToString()))
                    {
                        //Param = Param + ";EndDate=" +  Convert.ToDateTime(ucInsDate1.Text.Trim(),  CultureInfo.InvariantCulture).ToShortDateString();
                        Param = Param + ";EndDate=" + Convert.ToDateTime(FormatDate(ucInsDate1.Text.Trim(), DateFormat));
                    }

                    if (tbClientCode.Text != string.Empty)
                    {
                        Param = Param + ";ClientCD=" + tbClientCode.Text;
                    }



                    if (tbHomebase.Text != string.Empty)
                    {

                        Param = Param + ";HomeBaseCD=" + tbHomebase.Text;
                    }
                    if (tbRequestor.Text != string.Empty)
                    {
                        Param = Param + ";RequestorCD=" + tbRequestor.Text;
                    }
                    if (tbCrew.Text != string.Empty)
                    {
                        Param = Param + ";CrewCD=" + tbCrew.Text;
                    }
                    if (tbTailNumber.Text != string.Empty)
                    {
                        Param = Param + ";TailNum=" + tbTailNumber.Text;
                    }
                    if (tbDepartmentCD.Text != string.Empty)
                    {
                        Param = Param + ";DepartmentCD=" + tbDepartmentCD.Text;
                    }
                    if (tbAuth.Text != string.Empty)
                    {
                        Param = Param + ";AuthorizationCD=" + tbAuth.Text;
                    }
                    if (tbCat.Text != string.Empty)
                    {
                        Param = Param + ";CatagoryCD=" + tbCat.Text;
                    }
                    //if (chkTrip.Checked != false)
                    //{
                    Param = Param + ";IsTrip=" + chkTrip.Checked;
                    //}
                    //if (chkCanceled.Checked != false)
                    //{
                    Param = Param + ";IsCanceled=" + chkCanceled.Checked;
                    //}
                    //if (chkHold.Checked != false)
                    //{
                    Param = Param + ";IsHold=" + chkHold.Checked;
                    //}
                    //if (chkWorkSheet.Checked != false)
                    //{
                    Param = Param + ";IsWorkSheet=" + chkWorkSheet.Checked;
                    //}
                    //if (chkUnFulFilled.Checked != false)
                    //{
                    Param = Param + ";IsUnFulFilled=" + chkUnFulFilled.Checked;
                    //}
                    //if (chkScheduledService.Checked != false)
                    //{
                    Param = Param + ";IsScheduledService=" + chkScheduledService.Checked;
                    //}
                    Param = Param + ParamTripNumber();
                    Param = Param + ";UserCD=" + UserPrincipal.Identity._name;

                    //Added byb Sudhakar
                    Param = Param + ";UserCustomerID=" + UserPrincipal.Identity._customerID;
                    Param = Param + ";UserHomebaseID=" + UserPrincipal.Identity._homeBaseId;
                    Param = Param + ";TenToMin=" + UserPrincipal.Identity._fpSettings._TimeDisplayTenMin;
                }
                return Param;
            }
        }


        //private void ProcessColumns()
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
        //    {
        //        XPathDocument ReportFieldDoc = new XPathDocument(Server.MapPath(urlBase + filename));
        //        XPathNodeIterator itr = ReportFieldDoc.CreateNavigator().Select("//Column");
        //        StringBuilder sb = new System.Text.StringBuilder();
        //        while (itr.MoveNext())
        //        {
        //            dicColumnList.Add(itr.Current.GetAttribute("name", "").ToString(), itr.Current.GetAttribute("displayname", "").ToString());
        //        }
        //    }
        //}

        public void BindReportSheetDetail(Int64 TripSheetReportHeaderID)
        {
            //using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
            //{
            //    FlightPakMasterService.GetTripSheetReportDetail ReportSheetDetail = new FlightPakMasterService.GetTripSheetReportDetail();

            //    var ReportSheetValue = CrewRosterService.GetReportSheetDetail(TripSheetReportHeaderID);

            //    if (ReportSheetValue.ReturnFlag == true)
            //    {
            //        dgReportSheet.DataSource = ReportSheetValue.EntityList;
            //        dgReportSheet.DataBind();
            //    }
            //}
            using (MasterCatalogServiceClient CrewRosterService = new MasterCatalogServiceClient())
            {
                FlightPakMasterService.GetTripSheetDetailByUMPermission ReportSheetDetail = new FlightPakMasterService.GetTripSheetDetailByUMPermission();

                var ReportSheetValue = CrewRosterService.GetReportSheetDetailByUMPermission(TripSheetReportHeaderID);

                if (ReportSheetValue.ReturnFlag == true)
                {
                    dgReportSheet.DataSource = ReportSheetValue.EntityList;
                    dgReportSheet.DataBind();
                }
            }
        }

        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgTripsheetWriter.Rebind();
                    }
                    if (dgTripsheetWriter.MasterTableView.Items.Count > 0)
                    {
                        //dgTripsheetWriter.SelectedIndexes.Add(0);
                        if (UserPrincipal != null && UserPrincipal.Identity != null && UserPrincipal.Identity._fpSettings != null && UserPrincipal.Identity._fpSettings._TripsheetRptWriteGroup != null && UserPrincipal.Identity._fpSettings._TripsheetRptWriteGroup.Trim() != string.Empty)
                        {
                            foreach (GridDataItem Item in dgTripsheetWriter.Items)
                            {
                                if (Item["ReportID"].Text.Trim() == UserPrincipal.Identity._fpSettings._TripsheetRptWriteGroup.Trim())
                                {
                                    Item.Selected = true;
                                    if (Item.GetDataKeyValue("TripSheetReportHeaderID") != null)
                                    {
                                        Session["TripSheetReportHeaderID"] = Item.GetDataKeyValue("TripSheetReportHeaderID").ToString();
                                    }
                                    BindReportSheetDetail(Convert.ToInt64(Item.GetDataKeyValue("TripSheetReportHeaderID").ToString()));
                                    break;
                                }
                            }
                        }
                        else
                        {
                            dgTripsheetWriter.SelectedIndexes.Add(0);
                            GridDataItem Item = dgTripsheetWriter.SelectedItems[0] as GridDataItem;
                            Item.Selected = true;
                            if (Item.GetDataKeyValue("TripSheetReportHeaderID") != null)
                            {
                                Session["TripSheetReportHeaderID"] = Item.GetDataKeyValue("TripSheetReportHeaderID").ToString();
                            }
                            BindReportSheetDetail(Convert.ToInt64(Item.GetDataKeyValue("TripSheetReportHeaderID").ToString()));
                        }

                    }
                    else
                    {
                        ClearForm();
                        GridEnable(true, true, true);
                        EnableForm(false);
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        private void ProcessColumns(string xmlfilenametrip)
        {
            dicColumnList = new Dictionary<string, string>();
            string xmlfilename = string.Empty;
            if (Session["xmlTripSheetFilename"] != null || xmlfilenametrip != string.Empty)
            {
                xmlfilename = (Session["xmlTripSheetFilename"] != null) ? Convert.ToString(Session["xmlTripSheetFilename"]) : string.Empty;
                if (xmlfilenametrip != string.Empty)
                {
                    xmlfilename = xmlfilenametrip;
                    Session["xmlTripSheetFilename"] = xmlfilename;
                }
            }
            else if (filename != null)
            {
                xmlfilename = filename;
            }

            var filePath = Server.MapPath(urlBase + xmlfilename);
            XmlReaderResolver xmlReader = new XmlReaderResolver(filePath);
            var xmlReaderObject = xmlReader.XmlCleanReader();
            if (xmlReaderObject != null)
            {
                XPathDocument ReportFieldDoc = new XPathDocument(xmlReaderObject);
                XPathNodeIterator itr = ReportFieldDoc.CreateNavigator().Select("//Column");
                StringBuilder sb = new System.Text.StringBuilder();

                while (itr.MoveNext())
                {
                    dicColumnList.Add(itr.Current.GetAttribute("name", "").ToString(), itr.Current.GetAttribute("displayname", "").ToString());
                }
            }
        }
        private Control FindControlRecursive(Control rootControl, string controlID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (rootControl.ID == controlID) return rootControl;

                foreach (Control controlToSearch in rootControl.Controls)
                {
                    Control controlToReturn =
                        FindControlRecursive(controlToSearch, controlID);
                    if (controlToReturn != null) return controlToReturn;
                }
                return null;
            }
        }
        protected void OnFinish(object sender, System.Web.UI.WebControls.WizardNavigationEventArgs e)
        {
            //string fileName = String.Format("data-{0}.htm", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
            //Response.ContentType = "text/htm";
            //Response.AddHeader("content-disposition", "filename=" + fileName);
            //// write string data to Response.OutputStream here
            //Response.Write(ProcessHtmlParam());
            //Response.End();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Page.IsValid)
                        {

                            string[] strArr = GetReportTitle().Split(',');
                            if (Session["TripSheetSubLink"] != null)
                            {
                                string[] strxmlRptnameArr = Convert.ToString(Session["TripSheetSubLink"]).Split(',');
                                Session["REPORT"] = strxmlRptnameArr[0] + "mhtml";
                            }
                            else if (strArr.Count() > 0)
                            {
                                Session["REPORT"] = Convert.ToString(strArr[1] + "mhtml");
                            }
                            Session["FORMAT"] = "MHTML";
                            Session["PARAMETER"] = ProcessHtmlExcelParam();
                            Session["TripSheetSubLink"] = null;
                            Wizard1.Visible = false;
                            Response.Redirect("ReportRenderView.aspx");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }
        protected void OnNext(object sender, WizardNavigationEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (UCRHS.RadListBoxDestination.Items.Count > 0)
                        {
                            lblError.Text = "";

                            if (Page.IsValid)
                            {  // 
                                if (Wizard1.ActiveStep.ID == "WizardStep5")
                                {
                                    switch (ddlFormat.SelectedValue)
                                    {
                                        case "MHTML":
                                            Wizard1.Visible = true;
                                            Wizard1.MoveTo(WizardStep1);

                                            break;
                                        case "Excel":
                                            string[] strArr = GetReportTitle().Split(',');
                                            if (strArr.Count() > 0)
                                            {
                                                Session["REPORT"] = Convert.ToString(strArr[1] + "Export");
                                            }
                                            Session["FORMAT"] = "EXCEL";
                                            Session["PARAMETER"] = ProcessResults();
                                            Response.Redirect("ReportRenderView.aspx");
                                            break;
                                        default:
                                            Wizard1.Visible = false;
                                            break;
                                    }
                                }

                                if (Wizard1.ActiveStep.ID == "WizardStep1")
                                {
                                    // UCRHSort.ColumnList = dicColumnList;

                                    foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                    {
                                        dicSelectedList.Add(SelectedItem.Value, SelectedItem.Text);
                                    }

                                    UCRHSort.RadListBoxSource.DataSource = dicSelectedList;
                                    UCRHSort.RadListBoxSource.DataValueField = "Key";
                                    UCRHSort.RadListBoxSource.DataTextField = "Value";
                                    UCRHSort.RadListBoxSource.DataBind();


                                    Dictionary<string, string> dicTempSelectedList = dicSelectedList;
                                    foreach (KeyValuePair<string, string> pair in dicTempSelectedList)
                                    {
                                        dicColumnList.Remove(pair.Key);
                                    }
                                    Dictionary<string, string> Empty = new Dictionary<string, string>();
                                    UCRHSort.RadListBoxDestination.DataSource = Empty;
                                    UCRHSort.RadListBoxDestination.DataBind();

                                    Session["dicSelectedList"] = dicSelectedList;



                                    //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                    //{
                                    //    dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                    //}

                                    //SelectionDestination.DataSource = dicSelectedList;
                                    //SelectionDestination.DataValueField = "Key";
                                    //SelectionDestination.DataTextField = "Value";
                                    //SelectionDestination.DataBind();

                                    //SelectionSource.DataSource = dicSelectedList;
                                    //SelectionSource.DataValueField = "Key";
                                    //SelectionSource.DataTextField = "Value";
                                    //SelectionSource.DataBind();
                                }
                                if (Wizard1.ActiveStep.ID == "WizardStep2")
                                {
                                    foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                                    {
                                        dicSelectedList.Add(SelectedItem.Text, SelectedItem.Text);
                                    }


                                    foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxSource.Items)
                                    {
                                        dicSelectedListsort.Add(SelectedItem.Value, SelectedItem.Text);
                                    }

                                    foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxDestination.Items)
                                    {
                                        dicSelectedListsortSel.Add(SelectedItem.Value, SelectedItem.Text);
                                    }

                                    Session["dicSelectedListSort"] = dicSelectedListsort;

                                    Session["dicSelectedListSortSel"] = dicSelectedListsortSel;

                                    SelectedSource.DataSource = dicSelectedList;
                                    SelectedSource.DataValueField = "Key";
                                    SelectedSource.DataTextField = "Value";
                                    SelectedSource.DataBind();

                                    HeaderBySource.DataSource = dicSelectedList;
                                    HeaderBySource.DataValueField = "Key";
                                    HeaderBySource.DataTextField = "Value";
                                    HeaderBySource.DataBind();

                                }

                                if (Wizard1.ActiveStep.ID == "WizardStep4")
                                {
                                    txtTitle.Text = lbTitle.Text;

                                    // uncmd this code after
                                    FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                                    FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient();
                                    FPPrincipal identityname = (FPPrincipal)Session[Session.SessionID];
                                    var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == identity.Identity._customerID && x.HomebaseID == identity.Identity._homeBaseId).ToList();

                                    if (objCompany[0].HTMLHeader != string.Empty)
                                    {
                                        chkHeader.Enabled = false;
                                    }
                                    if (objCompany[0].HTMLFooter != string.Empty)
                                    {
                                        chkFooter.Enabled = false;
                                    }
                                    if (objCompany[0].HTMLBackgroun != string.Empty)
                                    {
                                        chkBackground.Enabled = false;
                                    }
                                    string[] strArr = GetReportTitle().Split(',');
                                    if (strArr.Count() > 0)
                                    {
                                        txtTitle.Text = Convert.ToString(strArr[0]);
                                    }
                                }
                            }
                        }
                        else
                        {
                            lblError.Text = "Select the Fields";
                            e.Cancel = true;

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }
        protected void LoadTabs(RadPanelBar Rpb, string XmlFilePath)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Rpb, XmlFilePath))
            {
                string attrVal = "";
                XmlDocument doc = new XmlDocument();
                doc.Load(Server.MapPath(XmlFilePath));

                XmlNodeList elemList = doc.GetElementsByTagName("catagory");
                for (int i = 0; i < elemList.Count; i++)
                {
                    attrVal = attrVal + elemList[i].Attributes["text"].Value + ",";
                }
                string[] strval = attrVal.Split(',');
                foreach (string str in strval)
                {
                    Telerik.Web.UI.RadPanelItem rpi = new Telerik.Web.UI.RadPanelItem(str);
                    XmlNodeList xnList = doc.SelectNodes("/catagorys/catagory[@text='" + str + "']/screen[@name!='']");
                    if (xnList.Count > 0)
                    {
                        int ival = 0;
                        foreach (XmlNode child in xnList)
                        {
                            //Added UMPermissionRole for implementing security
                            if (IsAuthorized("View" + child.Attributes["UMPermissionRole"].Value))
                            {
                                rpi.Items.Add(new Telerik.Web.UI.RadPanelItem(child.Attributes["name"].Value, child.Attributes["url"].Value));
                                rpi.Font.Bold = true;
                                rpi.Items[ival].Enabled = (child.Attributes["url"].Value == string.Empty ? false : true);
                                if (!rpi.Enabled)
                                {
                                    rpi.Items[ival].ForeColor = Color.Gray;
                                }

                                ival++;
                            }
                            else if (child.Attributes["UMPermissionRole"].Value == "TripsheetReportWriter")
                            {
                                rpi.Items.Add(new Telerik.Web.UI.RadPanelItem(child.Attributes["name"].Value, child.Attributes["url"].Value));
                                rpi.Font.Bold = true;
                                rpi.Items[ival].Enabled = (child.Attributes["url"].Value == string.Empty ? false : true);
                                ival++;
                            }
                        }
                        Rpb.Items.Add(rpi);
                    }
                    else if (str != "")
                    {
                        XmlNodeList elemListval = doc.GetElementsByTagName("catagory");
                        for (int ival = 0; ival < elemListval.Count; ival++)
                        {
                            Telerik.Web.UI.RadPanelItem rpia = new Telerik.Web.UI.RadPanelItem(elemListval[ival].Attributes["text"].Value, elemListval[ival].Attributes["url"].Value);
                            Rpb.Items.Add(rpia);
                        }
                        break;

                    }



                }
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Wizard1.Visible = true;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        public void Export(string ReportName, string xmlfilename)
        {

            if (Page.IsValid)
            {
                switch (ddlFormat.SelectedValue)
                {
                    case "MHTML":
                        //// Karthik - Commented to check
                        ////Wizard1.Visible = true;
                        ////ProcessColumns(xmlfilename);
                        ////UCRHS.ColumnList = dicColumnList;
                        ////UCRHSort.ColumnList = dicColumnList;
                        ////Wizard1.MoveTo(WizardStep1);
                        //////ExportPanal.Visible = false;
                        Wizard1.Visible = true;
                        Wizard1.MoveTo(WizardStep1);
                        ProcessColumns(xmlfilename);
                        ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[1])).DataSource = dicColumnList;
                        ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[1])).DataBind();
                        ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[1])).DataSource = dicColumnList;
                        ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[1])).DataBind();
                        dicColumnList.Clear();
                        ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[3])).DataSource = dicColumnList;
                        ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[3])).DataBind();
                        ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[3])).DataSource = dicColumnList;
                        ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[3])).DataBind();

                        break;
                    case "Excel":
                        Session["FORMAT"] = "EXCEL";
                        Session["REPORT"] = Convert.ToString(ReportName + "Export");
                        //if (rdBl.SelectedValue.ToString() == "DateRange")
                        if (reportCriteriaType == "DateRange")
                        {
                            Session["PARAMETER"] = ProcessResults();

                        }
                        else if (reportCriteriaType == "TripNumber")
                        {
                            Session["PARAMETER"] = ParamTripNumber();

                        }
                        Response.Redirect("ReportRenderView.aspx");
                        break;
                    case "WORD":
                        previewClick("WORD");
                        break;
                    default:
                        Wizard1.Visible = false;
                        break;
                }
            }
        }
        public void ExportRefresh(string ReportName, string xmlfilename)
        {
            ////Karthik - commented to check
            ////ProcessColumns(xmlfilename);
            ////UCRHS.ColumnList = dicColumnList;
            ////UCRHSort.ColumnList = dicColumnList;

            Wizard1.Visible = true;
            Wizard1.MoveTo(WizardStep1);
            ProcessColumns(xmlfilename);
            ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[1])).DataSource = dicColumnList;
            ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[1])).DataBind();
            ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[1])).DataSource = dicColumnList;
            ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[1])).DataBind();
            dicColumnList.Clear();
            ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[3])).DataSource = dicColumnList;
            ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[3])).DataBind();
            ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[3])).DataSource = dicColumnList;
            ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[3])).DataBind();

        }



        private string ProcessHtmlExcelParam()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ReportName))
            {

                string PageFGcolor = ColorTranslator.ToHtml(ForegroundColor.SelectedColor);
                string PageBGColor = ColorTranslator.ToHtml(BackgroundColor.SelectedColor);

                string TableBorderColors = ColorTranslator.ToHtml(TableBorderColor.SelectedColor);
                string TableFGColor = ColorTranslator.ToHtml(TableForegroundColor.SelectedColor);
                string TableBGColor = ColorTranslator.ToHtml(TableBackgroundColor.SelectedColor);
                StringBuilder strBuildparm = new StringBuilder();

                string Dbcolname = string.Empty;
                string AliesColname = string.Empty;
                string SortColname = string.Empty;


                switch (ddlFormat.SelectedValue)
                {

                    case "MHTML":

                        foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxDestination.Items)
                        {
                            Dbcolname = Dbcolname + SelectedItem.Value + ",";
                        }
                        if (Dbcolname != string.Empty)
                        {
                            Dbcolname = Dbcolname.Substring(0, Dbcolname.Length - 1);
                        }
                        foreach (RadListBoxItem SelectedItem in HeaderBySource.Items)
                        {
                            AliesColname = AliesColname + SelectedItem.Text + "|";
                        }
                        AliesColname = AliesColname.Substring(0, AliesColname.Length - 1);

                        foreach (RadListBoxItem SelectedItem in UCRHSort.RadListBoxDestination.Items)
                        {
                            SortColname = SortColname + SelectedItem.Value + ",";
                        }
                        if (SortColname != string.Empty)
                        {
                            SortColname = SortColname.Substring(0, SortColname.Length - 1);
                        }

                        strBuildparm.Append(ProcessResults());
                        strBuildparm.Append(";SelectedCols=" + Dbcolname);
                        strBuildparm.Append(";DisplayCols=" + AliesColname);
                        strBuildparm.Append(";SortCols=" + SortColname);
                        strBuildparm.Append(";Color=" + PageBGColor + "," + PageFGcolor + "," + TableFGColor + "," + TableBorderColors + "," + TableBGColor);
                        strBuildparm.Append(";ReportTitle=" + txtTitle.Text);

                        FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                        FlightPakMasterService.MasterCatalogServiceClient CompanyService = new FlightPakMasterService.MasterCatalogServiceClient();
                        FPPrincipal identityname = (FPPrincipal)Session[Session.SessionID];
                        var objCompany = CompanyService.GetCompanyMasterListInfo().EntityList.Where(x => x.CustomerID == identity.Identity._customerID && x.HomebaseID == identity.Identity._homeBaseId).ToList();


                        if (chkHeader.Checked)
                        {
                            strBuildparm.Append(";HtmlHeader=" + objCompany[0].HTMLHeader);
                        }
                        if (chkFooter.Checked)
                        {
                            strBuildparm.Append(";HtmlFooter=" + objCompany[0].HTMLFooter);
                        }
                        if (chkBackground.Checked)
                        {
                            strBuildparm.Append(";HtmlBG=" + objCompany[0].HTMLBackgroun);
                        }
                        break;

                    case "Excel":
                        //    //for Excel file  Reading from RadListBoxSource
                        //   foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxSource.Items)
                        //    {
                        //        Dbcolname = Dbcolname + SelectedItem.Value + ",";
                        //    }
                        //    if (Dbcolname != string.Empty)
                        //    {
                        //        Dbcolname = Dbcolname.Substring(0, Dbcolname.Length - 1);
                        //    }
                        //foreach (RadListBoxItem SelectedItem in UCRHS.RadListBoxSource.Items)
                        //    {
                        //        AliesColname=AliesColname+SelectedItem.Text+"|";
                        //    }
                        //if (AliesColname != string.Empty)
                        //{
                        //    AliesColname = AliesColname.Substring(0, AliesColname.Length - 1);
                        //}

                        //======== [Start] Fix by Sudhakar ==============

                        strBuildparm.Append(ProcessResults());
                        //if (rdBl.SelectedValue == "TripNumber")
                        //{
                        //    strBuildparm.Append(ParamTripNumber());
                        //}
                        //else
                        //{
                        //    strBuildparm.Append(ProcessResults());
                        //}
                        //======= [End] Fix by Sudhakar ==============

                        //   strBuildparm.Append(";SelectedCols="+Dbcolname);
                        //    strBuildparm.Append(";DisplayCols="+AliesColname);
                        //strBuildparm.Append(";Color=," + PageFGcolor + "," + TableFGColor + "," + TableBorderColors + "," + TableBGColor); 

                        break;

                }

                return strBuildparm.ToString();

                //FlightPak.Web.ReportRenderService.ReportRenderClient RptClient = new ReportRenderService.ReportRenderClient();
                //DataSet dsRpt = RptClient.GetReportHTMLTable(Dbcolname, AliesColname, GetSPName(filename), ProcessResults());
                //  return GetDataTableAsHTML(dsRpt.Tables[0], txtTitle.Text, PageFGcolor, PageBGColor, TableBorderColor, TableFGColor, TableBGColor);
            }

        }
        protected void btnExportFileFormat_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Page.IsValid)
                        {
                            if (ddlFormat.SelectedValue == "WORD")
                            {
                                previewClick("WORD");
                            }
                            else
                            {
                                if (ddlFormat.SelectedValue == "Excel")
                                {
                                    Wizard1.Visible = false;
                                }
                                // Commented due to General Declaration
                                //////   ExportPanal.Visible = true;
                                ////TripExcelGdl.Visible = (filename == "PREGeneralDeclarationAPDX.xml" || filename == "PRETSGeneralDeclaration.xml") ? true : false;
                                ////lbgdl1.Text = (filename == "PREGeneralDeclarationAPDX.xml") ? "Download General Declaration Appendix 1" : "Download General Declaration";
                                ////lbgdl2.Text = (filename == "PREGeneralDeclarationAPDX.xml") ? "Download General Declaration Appendix 2" : "";
                                ////lbgdl2.Visible = (filename == "PRETSGeneralDeclaration.xml") ? false : true;
                                //////  btExportFileFormat.Visible = (filename == "PreTSTripSheetMain.xml" || filename == "PREGeneralDeclarationAPDX.xml" || filename == "PRETSGeneralDeclaration.xml") ? false : true;
                                TripExcel.Visible = (filename == "PreTSTripSheetMain.xml") ? true : false;

                                //TripExcelGdl.Visible = (filename == "PREGeneralDeclarationAPDX.xml" || filename == "PRETSGeneralDeclaration.xml") ? true : false;
                                //  btExportFileFormat.Visible = (filename == "PreTSTripSheetMain.xml") ? false : true;
                                //TripExcel.Visible = (filename == "PreTSTripSheetMain.xml") ? true : false;
                            }
                            if (Session["TripSheetSubLink"] == null)
                            {
                                SubLinksInvoke();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }

        private void SubLinksInvoke()
        {
            if (Session["TripSheetSubLink"] != null)
            {
                string[] strxmlRptnameArr = Convert.ToString(Session["TripSheetSubLink"]).Split(',');
                Export(strxmlRptnameArr[0], strxmlRptnameArr[1]);

            }
            else
            {
                if (Session["ReportNameBylink"] == null || (Session["ReportNameBylink"] != null && string.IsNullOrWhiteSpace(Session["ReportNameBylink"].ToString()) == true))
                {
                    lblError.Text = "Please select a report from the following to export.";
                    return;
                }
                string[] strArr = GetReportTitle().Split(',');

                if ((TripExcelGdl.Visible == false) && (TripExcel.Visible == false))
                {
                    //     btExportFileFormat.Visible = true;
                    //TripExcel.Visible = false;
                    switch (ddlFormat.SelectedValue)
                    {
                        case "MHTML":
                            Wizard1.Visible = true;
                            Wizard1.MoveTo(WizardStep1);
                            ProcessColumns(string.Empty);
                            //UCRHS.ColumnList = dicColumnList;
                            //UCRHS.DataBind();
                            //UCRHSort.ColumnList = dicColumnList;
                            //UCRHSort.DataBind();
                            ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[1])).DataSource = dicColumnList;
                            ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[1])).DataBind();
                            ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[1])).DataSource = dicColumnList;
                            ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[1])).DataBind();
                            dicColumnList.Clear();
                            ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[3])).DataSource = dicColumnList;
                            ((Telerik.Web.UI.RadListBox)(UCRHS.Controls[1].Controls[3])).DataBind();
                            ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[3])).DataSource = dicColumnList;
                            ((Telerik.Web.UI.RadListBox)(UCRHSort.Controls[1].Controls[3])).DataBind();
                            break;
                        case "Excel":
                            if (strArr.Count() > 0)
                            {
                                Session["REPORT"] = Convert.ToString(strArr[1] + "Export");
                            }
                            Session["FORMAT"] = "EXCEL";

                            Session["PARAMETER"] = ProcessHtmlExcelParam();
                            Response.Redirect("ReportRenderView.aspx");
                            break;

                    }

                }
            }
        }

        private void EnableReportSheet(bool Enable)
        {
            foreach (GridDataItem Item in dgReportSheet.Items)
            {
                Item.Enabled = Enable;
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TripSheetReportDetailList"] = null;
                        Session["ClickedButton"] = "Export";
                        List<int> ReportNumber = new List<int>();
                        foreach (GridDataItem Item in dgReportSheet.Items)
                        {
                            if (((CheckBox)(Item["ReportNum"].FindControl("chkReportNum"))).Checked == true)
                            {
                                ReportNumber.Add(Convert.ToInt32(Item.GetDataKeyValue("ReportNumber")));
                            }
                        }
                        if (ReportNumber != null && ReportNumber.Count > 0)
                        {
                            if (ReportNumber.Contains(1))
                            {
                                lnkOverview.Visible = true;
                            }
                            else
                            {
                                lnkOverview.Visible = false;
                            }

                            if (ReportNumber.Contains(2))
                            {
                                lnkTripSheet.Visible = true;
                            }
                            else
                            {
                                lnkTripSheet.Visible = false;
                            }


                            if (ReportNumber.Contains(3))
                            {
                                lnkIternary.Visible = true;
                            }
                            else
                            {
                                lnkIternary.Visible = false;
                            }
                            if (ReportNumber.Contains(4))
                            {
                                lnkFlightLog.Visible = true;
                            }
                            else
                            {
                                lnkFlightLog.Visible = false;
                            }

                            if (ReportNumber.Contains(5))
                            {
                                lnkPaxProfile.Visible = true;
                            }
                            else
                            {
                                lnkPaxProfile.Visible = false;
                            }
                            if (ReportNumber.Contains(6))
                            {
                                lnkInternational.Visible = true;
                            }
                            else
                            {
                                lnkInternational.Visible = false;
                            }
                            //karthik - Excel/MHTML/Word Not Provided for this Report
                            if (ReportNumber.Contains(7))
                            {
                                lnkPaxTravelAuth.Visible = true;
                            }
                            else
                            {
                                lnkPaxTravelAuth.Visible = false;
                            }

                            if (ReportNumber.Contains(8))
                            {
                                lnkAlerts.Visible = true;
                            }
                            else
                            {
                                lnkAlerts.Visible = false;
                            }

                            if (ReportNumber.Contains(9))
                            {
                                lnkPAERForm.Visible = true;
                            }
                            else
                            {
                                lnkPAERForm.Visible = false;
                            }
                            if (ReportNumber.Contains(10))
                            {
                                lnkGeneralDeclaration.Visible = true;
                            }
                            else
                            {
                                lnkGeneralDeclaration.Visible = false;
                            }

                            if (ReportNumber.Contains(11))
                            {
                                lnkCrewDutyOverview.Visible = true;
                            }
                            else
                            {
                                lnkCrewDutyOverview.Visible = false;
                            }
                            if (ReportNumber.Contains(12))
                            {
                                lnkTripChecklist.Visible = true;
                            }
                            else
                            {
                                lnkTripChecklist.Visible = false;
                            }
                            if (ReportNumber.Contains(13))
                            {
                                lnkSummary.Visible = true;
                            }
                            else
                            {
                                lnkSummary.Visible = false;
                            }

                            if (ReportNumber.Contains(14))
                            {
                                lnkCanPassForm.Visible = true;
                            }
                            else
                            {
                                lnkCanPassForm.Visible = false;
                            }
                            if (ReportNumber.Contains(16))
                            {
                                lnkGDiscuss.Visible = true;
                            }
                            else
                            {
                                lnkGDiscuss.Visible = false;
                            }
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Please select Report(s) from Tripsheet Report Writer", 400, 100, "Tripsheet Report Writer", "");
                        }
                        Export();


                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }
        protected void btnCostFileFormat_Click(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Wizard1.Visible = false;
                        ExportPanal.Visible = false;
                        TripExcel.Visible = false;
                        TripExcelGdl.Visible = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        public void Export()
        {
            Session["TripSheetSubLink"] = null;
            if (ValidateTripsheetDate() == false)
            {
                return;
            }
            if (Page.IsValid)
            {
                ExportPanal.Visible = true;
            }
        }
        protected void btnPre_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "PreFlight";
                        pnlReportTab.Items.Clear();
                        Response.Redirect("ReportViewer.aspx?xmlFilename=PREAircraftCalendarI.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnPost_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "PostFlight";
                        pnlReportTab.Items.Clear();
                        Response.Redirect("ReportViewer.aspx?xmlFilename=POSTAircraftArrivals.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnCharter_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "Charter";
                        pnlReportTab.Items.Clear();
                        Response.Redirect("ReportViewer.aspx?xmlFilename=CQAircraftProfile.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        protected void btnCorpReq_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "CorpReq";
                        pnlReportTab.Items.Clear();
                        Response.Redirect("ReportViewer.aspx?xmlFilename=CRTripItinerary.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }

        protected void btnDb_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "Db";
                        pnlReportTab.Items.Clear();
                        LoadTabs(pnlReportTab, urlBase + "DatabaseMainTab.xml");
                        Response.Redirect("ReportViewer.aspx?xmlFilename=DBAccounts.xml");
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }


        private string ParamTripNumber()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                string paramater = string.Empty;

                if (reportCriteriaType == "TripNumber")
                {

                    if (TripNum.Text != string.Empty)
                    {
                        paramater = "TripNum=" + TripNum.Text;
                    }
                    if (Leg.Text != string.Empty)
                    {
                        paramater = paramater + ";LegNUM=" + Leg.Text;
                    }
                    if (Passenger.Text != string.Empty)
                    {
                        paramater = paramater + ";PassengerCD=" + Passenger.Text;
                    }
                }
                if (reportCriteriaType == "TripNumber")
                {

                    if (paramater == string.Empty)
                    {
                        paramater = "UserCD=" + UserPrincipal.Identity._name;
                    }
                    else
                    {
                        paramater = paramater + ";UserCD=" + UserPrincipal.Identity._name + ";";
                    }

                    paramater = paramater + "IsTrip=false;IsCanceled=false;IsHold=false;IsWorkSheet=false;IsUnFulFilled=false;IsScheduledService=false";
                }

                if (Session["TripSheetReportHeaderID"] != null)
                {
                    var TxtVariable = Session["TripSheetReportHeaderID"];
                    paramater = paramater + ";ReportHeaderID=" + TxtVariable;

                }
                Dictionary<string, int> dicRptType = new Dictionary<string, int>();
                int ReportNumber = 1;
                bool Checked = false;
                foreach (GridDataItem Item in dgReportSheet.MasterTableView.Items)
                {
                    if (((CheckBox)(Item["ReportNum"].FindControl("chkReportNum"))).Checked == true)
                    {
                        Checked = true;
                    }
                }
                int TestInt = 0;
                if (hdnPrintNumber.Value.Trim() != "" && int.TryParse(hdnPrintNumber.Value.Trim(), out TestInt))
                {
                    if (Session["SelectedReportNumber"] != null)
                    {
                        paramater = paramater + ";ReportNumber=" + Convert.ToString(Session["SelectedReportNumber"]);
                    }
                    else
                    {
                        RadWindowManager1.RadAlert("Report Number not passed", 400, 100, "FlightPak System", "");
                    }
                    ReportNumber = Convert.ToInt32(hdnPrintNumber.Value.Trim());
                }
                else
                {
                    if (!Checked)
                    {
                        if (Session["SnxmlFilename"] != null)
                        {
                            dicRptType = (Dictionary<string, int>)Session["SnxmlFilename"];
                            paramater = paramater + ";ReportNumber=" + dicRptType["xmlFilename"];
                            ReportNumber = Convert.ToInt32(dicRptType["xmlFilename"]);
                        }
                    }
                    else
                    {
                        if (Session["SelectedReportNumber"] != null)
                        {
                            paramater = paramater + ";ReportNumber=" + Convert.ToString(Session["SelectedReportNumber"]);
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Report Number not passed", 400, 100, "FlightPak System", "");
                        }
                        ReportNumber = Convert.ToInt32(Session["SelectedReportNumber"]);
                    }
                }

                if (Session["TripSheetReportDetailList"] != null)
                {
                    List<GetTripSheetReportDetail> lstTripSheetReportDetail = new List<GetTripSheetReportDetail>();
                    GetTripSheetReportDetail objTripSheetReportDetail = new GetTripSheetReportDetail();
                    using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var ObjRetVal = ObjService.GetTripSheetReportDetail();
                        //.EntityList.Where(x => x.TripSheetReportHeaderID == Convert.ToInt64(Session["TripSheetReportHeaderID"])).ToList();
                        if (ObjRetVal.ReturnFlag == true)
                        {
                            var ObjRetVal1 = ObjRetVal.EntityList.Where(x => x.TripSheetReportHeaderID == Convert.ToInt64(Session["TripSheetReportHeaderID"])
                                                                            && x.ReportNumber == ReportNumber).ToList();
                            lstTripSheetReportDetail = ObjRetVal1.ToList();
                        }
                    }
                    string strParam = string.Empty;
                    string str = string.Empty;
                    List<TripSheetReportDetail> lstcngTripSheetReportDetail = new List<TripSheetReportDetail>();
                    List<TripSheetReportDetail> oTripSheetReportDetail = new List<TripSheetReportDetail>();
                    oTripSheetReportDetail = (List<TripSheetReportDetail>)Session["TripSheetReportDetailList"];
                    bool IsChanged = false;

                    for (int Index = 0; Index < oTripSheetReportDetail.Count; Index++)
                    {
                        IsChanged = false;
                        for (int LIndex = 0; LIndex < lstTripSheetReportDetail.Count; LIndex++)
                        {
                            if (oTripSheetReportDetail[Index].TripSheetReportDetailID == lstTripSheetReportDetail[LIndex].TripSheetReportDetailID)
                            {
                                if (oTripSheetReportDetail[Index].ParameterLValue != lstTripSheetReportDetail[LIndex].ParameterLValue)
                                {
                                    IsChanged = true;
                                }
                                if (oTripSheetReportDetail[Index].ParameterNValue != lstTripSheetReportDetail[LIndex].ParameterNValue)
                                {
                                    IsChanged = true;
                                }
                                if (oTripSheetReportDetail[Index].ParameterValue != lstTripSheetReportDetail[LIndex].ParameterValue)
                                {
                                    IsChanged = true;
                                }
                                if (IsChanged == true)
                                {
                                    lstcngTripSheetReportDetail.Add(oTripSheetReportDetail[Index]);

                                    strParam = strParam + oTripSheetReportDetail[Index].ParameterVariable + "::";

                                    if (oTripSheetReportDetail[Index].ParameterType.ToUpper() == "L")
                                        strParam = strParam + oTripSheetReportDetail[Index].ParameterLValue + "||";
                                    else if (oTripSheetReportDetail[Index].ParameterType.ToUpper() == "N")
                                        strParam = strParam + oTripSheetReportDetail[Index].ParameterNValue + "||";
                                    else
                                        strParam = strParam + oTripSheetReportDetail[Index].ParameterValue + "||";

                                    break;
                                }
                            }
                        }

                    }
                    if (!string.IsNullOrEmpty(strParam.ToString()) && Session["FormatReportPrint"].ToString() == "1")
                    {
                        paramater = paramater + ";TempSettings=" + strParam;
                        Session["FormatReportPrint"] = 0;
                    }

                }

                //Added byb Sudhakar
                if (reportCriteriaType == "TripNumber")
                {
                    paramater = paramater + ";UserCustomerID=" + UserPrincipal.Identity._customerID;
                    paramater = paramater + ";UserHomebaseID=" + UserPrincipal.Identity._homeBaseId;
                    paramater = paramater + ";TenToMin=" + UserPrincipal.Identity._fpSettings._TimeDisplayTenMin;
                }
                return paramater;
            }
        }
        //private static string DummyMatch(List<GetTripSheetReportDetail> lstTripSheetReportDetail, List<TripSheetReportDetail> oTripSheetReportDetail)
        //{
        //    //DummyMatch(ref paramater, lstTripSheetReportDetail, ref str, lstcngTripSheetReportDetail, oTripSheetReportDetail, ref IsChanged);
        //    string strParam = string.Empty;
        //    foreach (var item in oTripSheetReportDetail)
        //    {

        //        if (lstTripSheetReportDetail.Where(x => x.TripSheetReportDetailID == item.TripSheetReportDetailID
        //                && x.ReportNumber == item.ReportNumber && x.IsSelected == item.IsSelected
        //                && x.ParameterValue.StringCompare(item.ParameterValue)).Count() > 0)
        //        {
        //            strParam = strParam + item.ParameterDescription + "::";

        //            if (item.ParameterType.ToUpper() == "L")
        //                strParam = strParam + item.ParameterLValue + "||";
        //            else if (item.ParameterType.ToUpper() == "N")
        //                strParam = strParam + item.ParameterNValue + "||";
        //            else
        //                strParam = strParam + item.ParameterValue + "||";

        //        }


        //    }
        //    return strParam;

        //}

        //private static void DummyMatch(ref string paramater, List<GetTripSheetReportDetail> lstTripSheetReportDetail, ref string str, List<TripSheetReportDetail> lstcngTripSheetReportDetail, List<TripSheetReportDetail> oTripSheetReportDetail, ref bool IsChanged)
        //{
        //    for (int Index = 0; Index < oTripSheetReportDetail.Count; Index++)
        //    {
        //        IsChanged = false;

        //        for (int LIndex = 0; LIndex < lstTripSheetReportDetail.Count; LIndex++)
        //        {
        //            if (oTripSheetReportDetail[Index].TripSheetReportDetailID == lstTripSheetReportDetail[LIndex].TripSheetReportDetailID)
        //            {
        //                if (oTripSheetReportDetail[Index].IsSelected != lstTripSheetReportDetail[LIndex].IsSelected)
        //                {
        //                    IsChanged = true;
        //                }
        //                if (oTripSheetReportDetail[Index].ReportNumber != lstTripSheetReportDetail[LIndex].ReportNumber)
        //                {
        //                    IsChanged = true;
        //                }
        //                if (oTripSheetReportDetail[Index].ParameterValue != lstTripSheetReportDetail[LIndex].ParameterValue)
        //                {
        //                    IsChanged = true;
        //                }
        //                if (IsChanged == true)
        //                {

        //                    lstcngTripSheetReportDetail.Add(oTripSheetReportDetail[Index]);

        //                }
        //            }
        //        }
        //    }
        //    if (lstcngTripSheetReportDetail.Count > 0)
        //    {
        //        for (int i = 0; i < lstcngTripSheetReportDetail.Count; i++)
        //        {
        //            if (lstcngTripSheetReportDetail.Count == 1)
        //                str = "<" + lstcngTripSheetReportDetail[i].ParameterDescription + ">" + "::" + "<" + lstcngTripSheetReportDetail[i].IsSelected + ">" + "||" + "<" + lstcngTripSheetReportDetail[i].ParameterDescription + ">" + "::" + "<" + lstcngTripSheetReportDetail[i].ReportNumber + ">" + "||" + "<" + lstcngTripSheetReportDetail[i].ParameterDescription + ">" + "::" + "<" + lstcngTripSheetReportDetail[i].ParameterValue + ">";
        //            else
        //                str = str + "<" + lstcngTripSheetReportDetail[i].ParameterDescription + ">" + "::" + "<" + lstcngTripSheetReportDetail[i].IsSelected + ">" + "||" + "<" + lstcngTripSheetReportDetail[i].ParameterDescription + ">" + "::" + "<" + lstcngTripSheetReportDetail[i].ReportNumber + ">" + "||" + "<" + lstcngTripSheetReportDetail[i].ParameterDescription + ">" + "::" + "<" + lstcngTripSheetReportDetail[i].ParameterValue + ">" + "||"; ;
        //        }
        //    }


        //    paramater = paramater + ";TempSetting" + str;
        //}
        protected void btnRptView_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TripSheetReportDetailList"] = null;
                        Session["ClickedButton"] = "Preview";
                        List<int> ReportNumber = new List<int>();
                        foreach (GridDataItem Item in dgReportSheet.Items)
                        {
                            if (((CheckBox)(Item["ReportNum"].FindControl("chkReportNum"))).Checked == true)
                            {
                                ReportNumber.Add(Convert.ToInt32(Item.GetDataKeyValue("ReportNumber")));
                            }
                        }
                        Session["ReportNumbers"] = ReportNumber;
                        ShowSubLinks(ReportNumber);
                        ExportPanal.Visible = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }

        private void ShowSubLinks(List<int> ReportNumber)
        {
            if (ReportNumber != null && ReportNumber.Count > 0)
            {
                if (ReportNumber.Contains(1))
                {
                    lnkOverview.Visible = true;
                }
                else
                {
                    lnkOverview.Visible = false;
                }

                if (ReportNumber.Contains(2))
                {
                    lnkTripSheet.Visible = true;
                }
                else
                {
                    lnkTripSheet.Visible = false;
                }


                if (ReportNumber.Contains(3))
                {
                    lnkIternary.Visible = true;
                }
                else
                {
                    lnkIternary.Visible = false;
                }
                if (ReportNumber.Contains(4))
                {
                    lnkFlightLog.Visible = true;
                }
                else
                {
                    lnkFlightLog.Visible = false;
                }

                if (ReportNumber.Contains(5))
                {
                    lnkPaxProfile.Visible = true;
                }
                else
                {
                    lnkPaxProfile.Visible = false;
                }
                if (ReportNumber.Contains(6))
                {
                    lnkInternational.Visible = true;
                }
                else
                {
                    lnkInternational.Visible = false;
                }
                if (ReportNumber.Contains(7))
                {
                    lnkPaxTravelAuth.Visible = true;
                }
                else
                {
                    lnkPaxTravelAuth.Visible = false;
                }

                if (ReportNumber.Contains(8))
                {
                    lnkAlerts.Visible = true;
                }
                else
                {
                    lnkAlerts.Visible = false;
                }

                if (ReportNumber.Contains(9))
                {
                    lnkPAERForm.Visible = true;
                }
                else
                {
                    lnkPAERForm.Visible = false;
                }
                if (ReportNumber.Contains(10))
                {
                    lnkGeneralDeclaration.Visible = true;
                }
                else
                {
                    lnkGeneralDeclaration.Visible = false;
                }

                if (ReportNumber.Contains(11))
                {
                    lnkCrewDutyOverview.Visible = true;
                }
                else
                {
                    lnkCrewDutyOverview.Visible = false;
                }
                if (ReportNumber.Contains(12))
                {
                    lnkTripChecklist.Visible = true;
                }
                else
                {
                    lnkTripChecklist.Visible = false;
                }
                if (ReportNumber.Contains(13))
                {
                    lnkSummary.Visible = true;
                }
                else
                {
                    lnkSummary.Visible = false;
                }

                if (ReportNumber.Contains(14))
                {
                    lnkCanPassForm.Visible = true;
                }
                else
                {
                    lnkCanPassForm.Visible = false;
                }
                if (ReportNumber.Contains(16))
                {
                    lnkGDiscuss.Visible = true;
                }
                else
                {
                    lnkGDiscuss.Visible = false;
                }
            }
            else
            {
                RadWindowManager1.RadAlert("Please select Report(s) from Tripsheet Report Writer", 400, 100, "Tripsheet Report Writer", "");
                //Tripsheet Report 
                //Please select Report from Tripsheet Report Writer Section
                //previewClick("PDF");
            }
        }


        protected void btnRptView1_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["ClickedButton"] = "Preview";
                        List<int> ReportNumber = new List<int>();

                        if (hdnPrintNumber.Value == "1")
                        {
                            lnkOverview_Click(sender, e);
                        }
                        else if (hdnPrintNumber.Value == "2")
                        {
                            lnkTripSheet_Click(sender, e);
                        }
                        else if (hdnPrintNumber.Value == "13")
                        {
                            lnkSummary_Click(sender, e);
                        }
                        else if (hdnPrintNumber.Value == "3")
                        {
                            lnkIternary_Click(sender, e);
                        }
                        else if (hdnPrintNumber.Value == "4")
                        {
                            lnkFlightLog_Click(sender, e);
                        }
                        else if (hdnPrintNumber.Value == "5")
                        {
                            lnkPaxProfile_Click(sender, e);
                        }
                        else if (hdnPrintNumber.Value == "6")
                        {
                            lnkInternational_Click(sender, e);
                        }
                        else if (hdnPrintNumber.Value == "7")
                        {
                            lnkPaxTravelAuth_Click(sender, e);
                        }
                        else if (hdnPrintNumber.Value == "8")
                        {
                            lnkAlerts_Click(sender, e);
                        }
                        else if (hdnPrintNumber.Value == "9")
                        {
                            lnkPAERForm_Click(sender, e);
                        }
                        else if (hdnPrintNumber.Value == "10")
                        {
                            lnkGeneralDeclaration_Click(sender, e);
                        }
                        else if (hdnPrintNumber.Value == "11")
                        {
                            lnkCrewDutyOverview_Click(sender, e);
                        }
                        else if (hdnPrintNumber.Value == "12")
                        {
                            lnkTripChecklist_Click(sender, e);
                        }
                        else if (hdnPrintNumber.Value == "14")
                        {
                            lnkCanPassForm_Click(sender, e);
                        }
                        else if (hdnPrintNumber.Value == "16")
                        {
                            lnkGDiscuss_Click(sender, e);
                        }

                        //  previewClick("PDF");


                        //previewClick("PDF");

                        //filename = Convert.ToString(Request.QueryString["xmlFilename"]);

                        //if (Page.IsValid)
                        //{
                        //    string[] strArr = GetReportTitle().Split(',');
                        //    if (strArr.Count() > 0)
                        //    {
                        //        Session["REPORT"] = Convert.ToString(strArr[1]);
                        //    }
                        //    Session["FORMAT"] = "PDF";
                        //    if (reportCriteriaType == "TripNumber")
                        //    {
                        //        Session["PARAMETER"] = ParamTripNumber();
                        //    }
                        //    else
                        //    {
                        //        Session["PARAMETER"] = ProcessResults();
                        //    }
                        //    if (ValidateTripsheetDate() == false)
                        //    {
                        //        return;
                        //    }
                        //    //Session["PARAMETER"] = "StartTime='12/10/2012';EndTime='12/10/2012';CrewCDs='JKM,ECJ';CrewGroupCDs='6789,122';AircraftCDs='10010,ACD';UserCD='UC";
                        //    Response.Redirect("ReportRenderView.aspx");
                        //}
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }




        public void DefaultAction()
        {
            //if (rdBl.SelectedValue.ToString() == "TripNumber")
            //{
            //    if (!string.IsNullOrEmpty(TripNum.Text))
            //    {
            //    }
            //}
            //else if (rdBl.SelectedValue.ToString() == "DateRange")
            //{
            //}
            Session["ReportNameBylink"] = filename;
            Wizard1.Visible = false;
            if (Session["ClickedButton"] == "Preview")
            {
                previewLinkClick("PDF");
            }
            else if (Session["ClickedButton"] == "Export")
            {
                //TripExcel.Visible = (filename == "PreTSTripSheetMain.xml") ? true : false;
                if (filename != "PreTSTripSheetMain.xml")
                    //TripExcel.Style.Add("Display", "None");
                    TripExcel.Visible = false;
                else
                { VisibleTripSheetSubLinks(true); TripExcel.Visible = true; }
                Export();
            }
        }

        protected void lnkOverview_Click(object sender, EventArgs e)
        {
            Session["SelectedReportNumber"] = "1";
            filename = "PRETSOverview.xml";
            DefaultAction();
        }

        protected void lnkTripSheet_Click(object sender, EventArgs e)
        {
            Session["SelectedReportNumber"] = "2";
            filename = "PreTSTripSheetMain.xml";
            DefaultAction();
        }

        protected void lnkSummary_Click(object sender, EventArgs e)
        {
            Session["SelectedReportNumber"] = "13";
            filename = "PREPRETSSummary.xml";
            DefaultAction();
        }

        protected void lnkIternary_Click(object sender, EventArgs e)
        {
            Session["SelectedReportNumber"] = "3";
            filename = "PRETSTripSheetItinerary.xml";
            DefaultAction();
        }


        protected void lnkFlightLog_Click(object sender, EventArgs e)
        {
            Session["SelectedReportNumber"] = "4";
            filename = "PRETSFlightLogInformation.xml";
            DefaultAction();
        }

        protected void lnkPaxProfile_Click(object sender, EventArgs e)
        {
            Session["SelectedReportNumber"] = "5";
            filename = "PRETSPaxProfile.xml";
            DefaultAction();
        }

        protected void lnkInternational_Click(object sender, EventArgs e)
        {
            Session["SelectedReportNumber"] = "6";
            filename = "PRETSInternationalData.xml";
            DefaultAction();
        }

        protected void lnkPaxTravelAuth_Click(object sender, EventArgs e)
        {
            Session["SelectedReportNumber"] = "7";
            filename = "PRETSPaxTravelAuthorization.xml";
            DefaultAction();
        }

        protected void lnkAlerts_Click(object sender, EventArgs e)
        {
            Session["SelectedReportNumber"] = "8";
            filename = "PRETSTripSheetAlert.xml";
            DefaultAction();
        }

        protected void lnkPAERForm_Click(object sender, EventArgs e)
        {
            Session["SelectedReportNumber"] = "9";
            filename = "PRETSPairForm.xml";
            DefaultAction();
        }

        protected void lnkGeneralDeclaration_Click(object sender, EventArgs e)
        {
            Session["SelectedReportNumber"] = "10";
            if (Leg.Text.Trim() != "")
            {
                filename = "PRETSGeneralDeclaration.xml";
                DefaultAction();
            }
            else
            {
                lblLeg.Font.Bold = true;
                lblLeg.ForeColor = Color.Black;
                RadWindowManager1.RadAlert("Leg No is Required", 400, 100, "FlightPak System", "CopySuccessful");
            }
        }

        protected void lnkCrewDutyOverview_Click(object sender, EventArgs e)
        {
            Session["SelectedReportNumber"] = "11";
            filename = "PRETSCrewDutyOverview.xml";
            DefaultAction();
        }






        protected void lnkTripChecklist_Click(object sender, EventArgs e)
        {
            Session["SelectedReportNumber"] = "12";
            filename = "PRETSTripSheetChecklist.xml";
            DefaultAction();
        }

        protected void lnkCanPassForm_Click(object sender, EventArgs e)
        {
            Session["SelectedReportNumber"] = "14";
            filename = "PRETSCanPassForm.xml";
            DefaultAction();
        }

        protected void lnkGDiscuss_Click(object sender, EventArgs e)
        {
            Session["SelectedReportNumber"] = "16";
            if (Leg.Text.Trim() != "")
            {
                filename = "PREGeneralDeclarationAPDX.xml";
                DefaultAction();
            }
            else
            {
                lblLeg.Font.Bold = true;
                lblLeg.ForeColor = Color.Black;
                RadWindowManager1.RadAlert("Leg No is Required", 400, 100, "FlightPak System", "CopySuccessful");
            }
        }




        private void previewLinkClick(string FileFormat)
        {
            if (Page.IsValid)
            {
                //Prakash 29-04-13

                if (rdBl.SelectedValue.ToString() == "TripNumber")
                {
                    if (!string.IsNullOrEmpty(TripNum.Text))
                    {
                        Session["DateDetails"] = null;
                        string tripnum = string.Empty;
                        string legnum = string.Empty;
                        string paxname = string.Empty;
                        string tripID = string.Empty;
                        string legID = string.Empty;
                        string paxID = string.Empty;
                        tripnum = TripNum.Text;
                        if (!string.IsNullOrEmpty(Leg.Text))
                            legnum = Leg.Text;
                        if (!string.IsNullOrEmpty(Passenger.Text))
                            paxname = Passenger.Text;
                        if (!string.IsNullOrEmpty(hdnTripNum.Value))
                            tripID = hdnTripNum.Value;
                        if (!string.IsNullOrEmpty(hdnLeg.Value))
                            legID = hdnLeg.Value;
                        if (!string.IsNullOrEmpty(hdnPassenger.Value))
                            paxID = hdnLeg.Value;

                        Session["TripDetails"] = tripnum + ":" + legnum + ":" + paxname + ":" + tripID + ":" + legID + ":" + paxID;
                    }
                }
                else if (rdBl.SelectedValue.ToString() == "DateRange")
                {
                    Session["TripDetails"] = null;
                    List<string> dateList = new List<string>();
                    TextBox tbDateFrom = (TextBox)DateFrom.FindControl("tbDate");
                    TextBox tbDateTo = (TextBox)DateTo.FindControl("tbDate");
                    dateList.Add(tbDateFrom.Text);
                    dateList.Add(tbDateTo.Text);
                    dateList.Add(tbClientCode.Text);
                    dateList.Add(hdnClientCode.Value);
                    dateList.Add(tbHomebase.Text);
                    dateList.Add(hdnHomebase.Value);
                    dateList.Add(tbRequestor.Text);
                    dateList.Add(hdnRequestor.Value);
                    dateList.Add(tbCrew.Text);
                    dateList.Add(hdnCrew.Value);
                    dateList.Add(tbTailNumber.Text);
                    dateList.Add(hdnTailNumber.Value);
                    dateList.Add(tbDepartmentCD.Text);
                    dateList.Add(hdnDepartment.Value);
                    dateList.Add(tbAuth.Text);
                    dateList.Add(hdnAuth.Value);
                    dateList.Add(tbCat.Text);
                    dateList.Add(hdnCat.Value);
                    if (chkTrip.Checked == true)
                        dateList.Add("true");
                    else
                        dateList.Add("false");

                    if (chkCanceled.Checked == true)
                        dateList.Add("true");
                    else
                        dateList.Add("false");

                    if (chkHold.Checked == true)
                        dateList.Add("true");
                    else
                        dateList.Add("false");

                    if (chkWorkSheet.Checked == true)
                        dateList.Add("true");
                    else
                        dateList.Add("false");

                    if (chkUnFulFilled.Checked == true)
                        dateList.Add("true");
                    else
                        dateList.Add("false");

                    if (chkScheduledService.Checked == true)
                        dateList.Add("true");
                    else
                        dateList.Add("false");

                    Session["DateDetails"] = dateList;
                }
                //Prakash 29-04-13

                string[] strArr = GetReportTitle().Split(',');
                if (strArr.Count() > 0)
                {
                    Session["REPORT"] = Convert.ToString(strArr[1]);
                    if (Convert.ToString(strArr[1]) == "RptPRETSFlightLog")
                    {
                        if (isFlighLogLandscape() == false)
                            Session["REPORT"] = "RptPRETSFlightLogPortrait";
                    }
                }
                Session["FORMAT"] = FileFormat;
                if (reportCriteriaType == "TripNumber")
                {
                    Session["PARAMETER"] = ParamTripNumber();
                }
                else
                {
                    Session["PARAMETER"] = ProcessResults();
                }
                if (ValidateTripsheetDate() == false)
                {
                    return;
                }
                //Session["PARAMETER"] = "StartTime='12/10/2012';EndTime='12/10/2012';CrewCDs='JKM,ECJ';CrewGroupCDs='6789,122';AircraftCDs='10010,ACD';UserCD='UC";
                string redirectUrl = string.Format("ReportRenderView.aspx?xmlFilename={0}&ReportType={1}", Request.QueryString["xmlFilename"], Request.QueryString["ReportType"]);
                var NewUrl = IsLocalUrl(redirectUrl);
                if (!string.IsNullOrEmpty(NewUrl) && !string.IsNullOrWhiteSpace(NewUrl))
                {
                    Response.Redirect(NewUrl, true);
                }
                
            }
        }

        private bool isFlighLogLandscape()
        {
            Int64 TripsheetReportHeaderID = 0;
            TripsheetReportHeaderID = Convert.ToInt64(Session["TripsheetReportHeaderID"]);
            Int32 ReportNumber = 0;
            ReportNumber = Convert.ToInt32(Session["SelectedReportNumber"]);
            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var ObjRetVal = ObjService.GetTripSheetReportDetail().EntityList.Where(x => x.TripSheetReportHeaderID == TripsheetReportHeaderID
                                                && x.ReportNumber == ReportNumber
                                                && x.ParameterVariable != null
                                                && x.ParameterVariable == "LANDSCAPE"
                                                ).OrderBy(x => x.ParameterDescription).ToList();
                if (ObjRetVal.Count > 0)
                    return ObjRetVal.FirstOrDefault().ParameterLValue == null ? true : ObjRetVal.FirstOrDefault().ParameterLValue.Value;
            }
            return true;
        }

        private void previewClick(string FileFormat)
        {
            if (Request.QueryString["xmlFilename"] != null)
            {
                if (filename.Trim() != string.Empty)
                    filename = Convert.ToString(Request.QueryString["xmlFilename"]);
            }

            if (Page.IsValid)
            {
                //Prakash 29-04-13

                if (rdBl.SelectedValue.ToString() == "TripNumber")
                {
                    if (!string.IsNullOrEmpty(TripNum.Text))
                    {
                        Session["DateDetails"] = null;
                        string tripnum = string.Empty;
                        string legnum = string.Empty;
                        string paxname = string.Empty;
                        string tripID = string.Empty;
                        string legID = string.Empty;
                        string paxID = string.Empty;
                        tripnum = TripNum.Text;
                        if (!string.IsNullOrEmpty(Leg.Text))
                            legnum = Leg.Text;
                        if (!string.IsNullOrEmpty(Passenger.Text))
                            paxname = Passenger.Text;
                        if (!string.IsNullOrEmpty(hdnTripNum.Value))
                            tripID = hdnTripNum.Value;
                        if (!string.IsNullOrEmpty(hdnLeg.Value))
                            legID = hdnLeg.Value;
                        if (!string.IsNullOrEmpty(hdnPassenger.Value))
                            paxID = hdnLeg.Value;

                        Session["TripDetails"] = tripnum + ":" + legnum + ":" + paxname + ":" + tripID + ":" + legID + ":" + paxID;
                    }
                }
                else if (rdBl.SelectedValue.ToString() == "DateRange")
                {
                    Session["TripDetails"] = null;
                    List<string> dateList = new List<string>();
                    TextBox tbDateFrom = (TextBox)DateFrom.FindControl("tbDate");
                    TextBox tbDateTo = (TextBox)DateTo.FindControl("tbDate");
                    dateList.Add(tbDateFrom.Text);
                    dateList.Add(tbDateTo.Text);
                    dateList.Add(tbClientCode.Text);
                    dateList.Add(hdnClientCode.Value);
                    dateList.Add(tbHomebase.Text);
                    dateList.Add(hdnHomebase.Value);
                    dateList.Add(tbRequestor.Text);
                    dateList.Add(hdnRequestor.Value);
                    dateList.Add(tbCrew.Text);
                    dateList.Add(hdnCrew.Value);
                    dateList.Add(tbTailNumber.Text);
                    dateList.Add(hdnTailNumber.Value);
                    dateList.Add(tbDepartmentCD.Text);
                    dateList.Add(hdnDepartment.Value);
                    dateList.Add(tbAuth.Text);
                    dateList.Add(hdnAuth.Value);
                    dateList.Add(tbCat.Text);
                    dateList.Add(hdnCat.Value);
                    if (chkTrip.Checked == true)
                        dateList.Add("true");
                    else
                        dateList.Add("false");

                    if (chkCanceled.Checked == true)
                        dateList.Add("true");
                    else
                        dateList.Add("false");

                    if (chkHold.Checked == true)
                        dateList.Add("true");
                    else
                        dateList.Add("false");

                    if (chkWorkSheet.Checked == true)
                        dateList.Add("true");
                    else
                        dateList.Add("false");

                    if (chkUnFulFilled.Checked == true)
                        dateList.Add("true");
                    else
                        dateList.Add("false");

                    if (chkScheduledService.Checked == true)
                        dateList.Add("true");
                    else
                        dateList.Add("false");

                    Session["DateDetails"] = dateList;
                }
                //Prakash 29-04-13

                string[] strArr = GetReportTitle().Split(',');
                if (strArr.Count() > 0)
                {
                    Session["REPORT"] = Convert.ToString(strArr[1]);
                }
                Session["FORMAT"] = FileFormat;
                if (reportCriteriaType == "TripNumber")
                {
                    Session["PARAMETER"] = ParamTripNumber();
                }
                else
                {
                    Session["PARAMETER"] = ProcessResults();
                }
                if (ValidateTripsheetDate() == false)
                {
                    return;
                }
                //Session["PARAMETER"] = "StartTime='12/10/2012';EndTime='12/10/2012';CrewCDs='JKM,ECJ';CrewGroupCDs='6789,122';AircraftCDs='10010,ACD';UserCD='UC";
                Response.Redirect("ReportRenderView.aspx");
            }
        }
        protected void rdBl_SelectedIndexChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        reportCriteriaType = rdBl.SelectedValue.ToString();
                        VisibleLinks(false);
                        TripControlsDisplay();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void lbTripSheet_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string strxmlRptname = "RptPRETSTripSheet,PRETripSheetMainSub.xml";
                        string[] strxmlRptnameArr = strxmlRptname.Split(',');
                        ExportRefresh(strxmlRptnameArr[0], strxmlRptnameArr[1]);
                        ExportPanal.Visible = true;
                        Session["TripSheetSubLink"] = strxmlRptnameArr[0] + "," + strxmlRptnameArr[1];
                        //Export("RptPRETSTripSheet","PRETripSheetMainSub.xml");
                        SubLinksInvoke();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void lbTripsheetAlerts_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string strxmlRptname = "RptPRETSTripSheetAlerts,PRETripSheetAlertsSub.xml";
                        string[] strxmlRptnameArr = strxmlRptname.Split(',');
                        ExportRefresh(strxmlRptnameArr[0], strxmlRptnameArr[1]);
                        ExportPanal.Visible = true;
                        Session["TripSheetSubLink"] = strxmlRptnameArr[0] + "," + strxmlRptnameArr[1];
                        //  Export("RptPRETSTripSheetAlerts", "PRETripSheetAlertsSub.xml");
                        SubLinksInvoke();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void lbPassengerNotes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string strxmlRptname = "RptPRETSPAXNotes,PRETripSheetPassengerNotesSub.xml";
                        string[] strxmlRptnameArr = strxmlRptname.Split(',');
                        ExportRefresh(strxmlRptnameArr[0], strxmlRptnameArr[1]);
                        ExportPanal.Visible = true;
                        Session["TripSheetSubLink"] = strxmlRptnameArr[0] + "," + strxmlRptnameArr[1];

                        // Export("RptPRETSPAXNotes", "PRETripSheetPassengerNotesSub.xml");
                        SubLinksInvoke();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void lbPassengerAlerts_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        string strxmlRptname = "RptPRETSPAXAlerts,PRETripSheetPAXAlertSub.xml";
                        string[] strxmlRptnameArr = strxmlRptname.Split(',');
                        ExportRefresh(strxmlRptnameArr[0], strxmlRptnameArr[1]);
                        ExportPanal.Visible = true;
                        Session["TripSheetSubLink"] = strxmlRptnameArr[0] + "," + strxmlRptnameArr[1];
                        //   Export("RptPRETSPAXAlerts", "PRETripSheetPAXAlertSub.xml");
                        SubLinksInvoke();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }
        protected void lbAircraftAdditionalInfomation_Click(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                        {
                            try
                            {
                                //Handle methods throguh exception manager with return flag
                                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                                exManager.Process(() =>
                                {

                                    string strxmlRptname = "RptPRETSAircraftAdditional,PRETripSheetAircraftAdditionalSub.xml";
                                    string[] strxmlRptnameArr = strxmlRptname.Split(',');
                                    ExportRefresh(strxmlRptnameArr[0], strxmlRptnameArr[1]);

                                    Session["TripSheetSubLink"] = strxmlRptnameArr[0] + "," + strxmlRptnameArr[1];


                                    // Export("RptPRETSAircraftAdditional", "PRETripSheetAircraftAdditionalSub.xml");
                                    SubLinksInvoke();
                                    ExportPanal.Visible = true;
                                }, FlightPak.Common.Constants.Policy.UILayer);

                            }
                            catch (Exception ex)
                            {
                                //The exception will be handled, logged and replaced by our custom exception. 
                                ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }

        }
        protected void lbFuelVendor_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string strxmlRptname = "RptPRETSBestFuelVendor,PRETripSheetBestFuelVendorSub.xml";
                        string[] strxmlRptnameArr = strxmlRptname.Split(',');
                        ExportRefresh(strxmlRptnameArr[0], strxmlRptnameArr[1]);
                        ExportPanal.Visible = true;
                        Session["TripSheetSubLink"] = strxmlRptnameArr[0] + "," + strxmlRptnameArr[1];



                        //    Export("RptPRETSBestFuelVendor", "PRETripSheetBestFuelVendorSub.xml");
                        SubLinksInvoke();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void lbTripGdl1_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        filename = Convert.ToString(Request.QueryString["xmlFilename"]);



                        if (filename == "PRETSGeneralDeclaration.xml")
                        {

                            string strxmlRptname = "RptPREGeneralDeclaration,PRETripSheetGeneralDeclarationSub.xml";
                            string[] strxmlRptnameArr = strxmlRptname.Split(',');
                            ExportRefresh(strxmlRptnameArr[0], strxmlRptnameArr[1]);
                            ExportPanal.Visible = true;
                            Session["TripSheetSubLink"] = strxmlRptnameArr[0] + "," + strxmlRptnameArr[1];
                            //    Export("RptPREGeneralDeclaration", "PRETripSheetGeneralDeclarationSub.xml");
                        }
                        else if (filename == "PREGeneralDeclarationAPDX.xml")
                        {
                            string strxmlRptname = "RptPRETSGeneralDeclarationAPDX,PRETripSheetGeneralDeclarationAPDXSub.xml";
                            string[] strxmlRptnameArr = strxmlRptname.Split(',');
                            ExportRefresh(strxmlRptnameArr[0], strxmlRptnameArr[1]);
                            ExportPanal.Visible = true;
                            Session["TripSheetSubLink"] = strxmlRptnameArr[0] + "," + strxmlRptnameArr[1];
                            //  Export("RptPRETSGeneralDeclarationAPDX", "PRETripSheetGeneralDeclarationAPDXSub.xml");
                        }
                        SubLinksInvoke();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void lbTripGdl2_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        filename = Convert.ToString(Request.QueryString["xmlFilename"]);

                        if (filename == "PREGeneralDeclarationAPDX.xml")
                        {
                            string strxmlRptname = "RptPRETSGeneralDeclarationAPDXpm,PRETripSheetGeneralDeclarationAPDXpmSub.xml";
                            string[] strxmlRptnameArr = strxmlRptname.Split(',');
                            ExportRefresh(strxmlRptnameArr[0], strxmlRptnameArr[1]);
                            ExportPanal.Visible = true;
                            Session["TripSheetSubLink"] = strxmlRptnameArr[0] + "," + strxmlRptnameArr[1];
                            // Export("RptPREGeneralDeclarationAPDXpm", "PRETripSheetGeneralDeclarationAPDXpmSub.xml");
                            SubLinksInvoke();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        private void ResetResults()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                ExportPanal.Visible = false;
                TripNum.Text = string.Empty;
                Leg.Text = string.Empty;
                Passenger.Text = string.Empty;
                TripNum.Text = string.Empty;
                Leg.Text = string.Empty;
                Passenger.Text = string.Empty;
                //txtpilot1.Text = string.Empty;
                //txtpilot2.Text = string.Empty;
                //txtAddlcrew.Text = string.Empty;
                //txtmsg.Text = string.Empty;
                //tbAIRCRAFTRELEASED.Text = string.Empty;
                //tbTripMain.Text = string.Empty;
                lbTripSearchMessage.Text = string.Empty;
                Leg.Enabled = false;
                btnLeg.Enabled = false;
                Passenger.Enabled = false;
                btnPassenger.Enabled = false;
                tbAuth.Text = string.Empty;
                tbCat.Text = string.Empty;
                tbClientCode.Text = string.Empty;
                tbCrew.Text = string.Empty;
                tbDepartmentCD.Text = string.Empty;
                tbHomebase.Text = string.Empty;
                tbRequestor.Text = string.Empty;
                tbTailNumber.Text = string.Empty;
                chkTrip.Checked = true;
                chkCanceled.Checked = false;
                chkHold.Checked = false;
                chkWorkSheet.Checked = false;
                chkUnFulFilled.Checked = false;
                chkScheduledService.Checked = false;
                TextBox tbPurDate = (TextBox)DateFrom.FindControl("tbDate");
                tbPurDate.Text = string.Empty;
                TextBox tbPurDate1 = (TextBox)DateTo.FindControl("tbDate");
                tbPurDate1.Text = string.Empty;
                Session["TripSheetSubLink"] = null;

                //Added to Remove Links
                lnkOverview.Visible = false;
                lnkTripSheet.Visible = false;
                lnkSummary.Visible = false;
                lnkIternary.Visible = false;
                lnkFlightLog.Visible = false;
                lnkPaxProfile.Visible = false;
                lnkInternational.Visible = false;
                lnkPaxTravelAuth.Visible = false;
                lnkAlerts.Visible = false;
                lnkPAERForm.Visible = false;
                lnkGeneralDeclaration.Visible = false;
                lnkCrewDutyOverview.Visible = false;
                lnkTripChecklist.Visible = false;
                lnkCanPassForm.Visible = false;
                lnkGDiscuss.Visible = false;

                VisibleTripSheetSubLinks(false);


            }
        }

        private void VisibleTripSheetSubLinks(bool enable)
        {
            //tripsheet sublinks
            lbTripSheet.Visible = lbTripsheetAlerts.Visible =
            lbPassengerNotes.Visible = lbPassengerAlerts.Visible =
            lbFuelVendor.Visible = lbAircraftAdditionalInfomation.Visible = enable;
        }




        protected void btnReset_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ResetResults();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void Dept_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //lbHomeBase.Text = string.Empty;
                        lbdept.Text = string.Empty;
                        hdnDepartment.Value = string.Empty;
                        if (!string.IsNullOrEmpty(tbDepartmentCD.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                            {

                                var objRetVal = Service.GetAllDeptAuthorizationList().EntityList.Where(x => x.DepartmentCD.ToString().ToUpper().Trim() == tbDepartmentCD.Text.ToUpper().Trim()).ToList();
                                if (objRetVal != null && objRetVal.Count > 0)
                                {
                                    tbDepartmentCD.Text = ((FlightPak.Web.FlightPakMasterService.GetAllDeptAuth)objRetVal[0]).DepartmentCD.ToString().ToUpper();
                                    hdnDepartment.Value = ((FlightPak.Web.FlightPakMasterService.GetAllDeptAuth)objRetVal[0]).DepartmentID.ToString();

                                }
                                else
                                {
                                    lbdept.Text = "Invalid Department Code.";
                                    tbDepartmentCD.Text = string.Empty;
                                    tbDepartmentCD.Focus();
                                    tbAuth.Enabled = false;
                                    tbAuth.Text = "";
                                    btnAuth.Enabled = false;
                                }
                            }
                        }
                        else
                        {
                            lbdept.Text = "";
                            tbAuth.Enabled = false;
                            tbAuth.Text = "";
                            btnAuth.Enabled = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POExpense);
                }
            }
        }
        public string FormatDate(string dt)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(dt))
            {
                string FormattedDate = string.Empty;
                if (!string.IsNullOrEmpty(dt))
                {
                    DateTimeFormatInfo formatInfo = new DateTimeFormatInfo();
                    if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        formatInfo.ShortDatePattern = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                    else
                        formatInfo.ShortDatePattern = "MM/dd/yyyy"; // American, MDY

                    List<string> dtList = new List<string>();

                    char splitter;
                    if (dt.Contains("-"))
                        splitter = '-';
                    else if (dt.Contains("."))
                        splitter = '.';
                    else
                        splitter = '/';

                    switch (formatInfo.ShortDatePattern)
                    {
                        case "dd/MM/yyyy": // British, French, DMY
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[0] + "/" + dtList[2];
                            break;
                        case "dd.MM.yyyy": // German
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[0] + "/" + dtList[2];
                            break;
                        case "yyyy.MM.dd": // ANSI
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[2] + "/" + dtList[0];
                            break;
                        case "dd-MM-yyyy": // Italian
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[0] + "/" + dtList[2];
                            break;
                        case "yyyy/MM/dd": // Japan, Taiwan, YMD
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[2] + "/" + dtList[0];
                            break;
                        case "MM/dd/yyyy": // Default
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[0] + "/" + dtList[1] + "/" + dtList[2];
                            break;
                    }
                }
                return FormattedDate;
            }
        }

        #region UI Validations"
        protected void SearchLeg_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((!string.IsNullOrEmpty(hdnTripNum.ClientID)) && (!string.IsNullOrEmpty(Leg.Text)))
                        {
                            using (PreflightService.PreflightServiceClient ObjService = new PreflightServiceClient())
                            {

                                var objRetVal = ObjService.GetTrip(Convert.ToInt64(hdnTripNum.Value));
                                List<PreflightLeg> Dt = new List<PreflightLeg>();
                                Dt = objRetVal.EntityList[0].PreflightLegs;
                                if (Dt.Count > 0)
                                {
                                    string[] Text = Leg.Text.Split(',');
                                    List<long> listOfLegIDs = new List<long>();
                                    foreach (string legid in Text)
                                    {
                                        if (legid.Contains('-'))
                                        {
                                            string[] range = legid.Split('-');
                                            for (long i = Convert.ToInt64(range[0]); i <= Convert.ToInt64(range[1]); i++)
                                            {
                                                listOfLegIDs.Add(i);
                                            }
                                        }
                                        else
                                            listOfLegIDs.Add(Convert.ToInt64(legid));
                                    }
                                    long[] LegIDs = listOfLegIDs.Distinct().ToArray();
                                    string result = string.Join(",", LegIDs);
                                    Leg.Text = result;
                                    for (int i = 0; i < LegIDs.Length; i++)
                                    {
                                        long legID = LegIDs[i];
                                        var objRetVal1 = (from u in Dt
                                                          where u.LegNUM == legID
                                                          select u);

                                        if (objRetVal1.Count() != 0)
                                        {
                                            cvLeg.IsValid = true;
                                            RadAjaxManager1.FocusControl(Passenger.ClientID);
                                        }
                                        else
                                        {
                                            cvLeg.IsValid = false;
                                            RadAjaxManager1.FocusControl(Leg.ClientID);
                                        }
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void SearchPassenger_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if ((!string.IsNullOrEmpty(hdnTripNum.ClientID)) && (!string.IsNullOrEmpty(Passenger.Text)))
                        {
                            using (PreflightService.PreflightServiceClient ObjService = new PreflightServiceClient())
                            {
                                var objRetVal = ObjService.GetTrip(Convert.ToInt64(hdnTripNum.Value));
                                List<FlightPak.Web.FlightPakMasterService.Passenger> PassengerRequestorlst = new List<FlightPak.Web.FlightPakMasterService.Passenger>();
                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    string[] paxCDs = Passenger.Text.Split(',');

                                    for (int i = 0; i < paxCDs.Length; i++)
                                    {
                                        string paxCD = paxCDs[i];
                                        PassengerRequestorlst = objDstsvc.GetPassengerRequestorList().EntityList.Where(x => x.PassengerRequestorCD == paxCD && x.IsDeleted == false).ToList();
                                        if (PassengerRequestorlst != null && PassengerRequestorlst.Count > 0)
                                        {
                                            Int64 passengerID = 0;
                                            passengerID = PassengerRequestorlst[0].PassengerRequestorID;
                                            if (objRetVal != null)
                                            {
                                                bool paxInList = objRetVal.EntityList[0].PreflightLegs.Any(x => x.PreflightPassengerLists.Any(y => y.PassengerID == passengerID));
                                                if (paxInList)
                                                {
                                                    cvPassenger.IsValid = true;
                                                }
                                                else
                                                {
                                                    cvPassenger.IsValid = false;
                                                    RadAjaxManager1.FocusControl(Passenger.ClientID);
                                                }
                                            }
                                            else
                                            {
                                                cvPassenger.IsValid = false;
                                                RadAjaxManager1.FocusControl(Passenger.ClientID);
                                            }
                                        }
                                        else
                                        {
                                            cvPassenger.IsValid = false;
                                            RadAjaxManager1.FocusControl(Passenger.ClientID);
                                        }
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void tbClientCde_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckClientCodeExist(tbClientCode, cvClientCde);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        private bool CheckClientCodeExist(TextBox txtbx, CustomValidator cval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetClientByClientCD(txtbx.Text.Trim().ToUpper().ToString()).EntityList;
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.ClientByClientCDResult> ClientList = new List<FlightPakMasterService.ClientByClientCDResult>();
                                ClientList = (List<FlightPakMasterService.ClientByClientCDResult>)objRetVal.ToList();
                                hdnClientCode.Value = ClientList[0].ClientID.ToString();
                                RadAjaxManager1.FocusControl(tbHomebase.ClientID); //txtbx.Focus();                                
                                returnVal = false;
                            }
                            else
                            {
                                cval.IsValid = false;
                                txtbx.Text = string.Empty;
                                RadAjaxManager1.FocusControl(txtbx.ClientID); //txtbx.Focus();
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void tbHomebase_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckHomebaseExist(tbHomebase, cvHomebase);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        private bool CheckHomebaseExist(TextBox txtbx, CustomValidator cval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCompanySvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCompanySvc.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD.Trim().ToUpper() == (txtbx.Text.Trim().ToUpper())).ToList();
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.GetAllCompanyMaster> CompanyList = new List<FlightPakMasterService.GetAllCompanyMaster>();
                                CompanyList = (List<FlightPakMasterService.GetAllCompanyMaster>)objRetVal.ToList();
                                hdnHomebase.Value = CompanyList[0].HomebaseID.ToString();
                                RadAjaxManager1.FocusControl(tbRequestor.ClientID);
                                returnVal = false;
                            }
                            else
                            {
                                cval.IsValid = false;
                                txtbx.Text = string.Empty;
                                RadAjaxManager1.FocusControl(txtbx.ClientID);
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void tbRequestor_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckRequestorExist(tbRequestor, cvRequestor);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        private bool CheckRequestorExist(TextBox txtbx, CustomValidator cval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCompanySvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCompanySvc.GetPassengerRequestorByPassengerRequestorCD(txtbx.Text.Trim().ToUpper()).EntityList.Where(x => x.PassengerRequestorCD.Trim().ToUpper() == (txtbx.Text.Trim().ToUpper())).ToList();
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.PassengerRequestorResult> PassengerRequestorResultList = new List<FlightPakMasterService.PassengerRequestorResult>();
                                PassengerRequestorResultList = (List<FlightPakMasterService.PassengerRequestorResult>)objRetVal.ToList();
                                hdnRequestor.Value = PassengerRequestorResultList[0].PassengerRequestorID.ToString();
                                RadAjaxManager1.FocusControl(tbCrew.ClientID);
                                returnVal = false;
                            }
                            else
                            {
                                cval.IsValid = false;
                                txtbx.Text = string.Empty;
                                RadAjaxManager1.FocusControl(txtbx.ClientID);
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void tbCrew_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckCrewExist(tbCrew, cvCrew);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        private bool CheckCrewExist(TextBox txtbx, CustomValidator cval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCompanySvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCompanySvc.GetAllCrewForGrid().EntityList.Where(x => x.CrewCD.Trim().ToUpper() == txtbx.Text.Trim().ToUpper()).ToList();
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.GetAllCrewForGrid> CrewList = new List<FlightPakMasterService.GetAllCrewForGrid>();
                                CrewList = (List<FlightPakMasterService.GetAllCrewForGrid>)objRetVal;
                                hdnCrew.Value = CrewList[0].CrewID.ToString();
                                RadAjaxManager1.FocusControl(tbTailNumber.ClientID);
                                returnVal = false;
                            }
                            else
                            {
                                cval.IsValid = false;
                                txtbx.Text = string.Empty;
                                RadAjaxManager1.FocusControl(txtbx.ClientID);
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void tbTailNumber_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckTailNumberExist(tbTailNumber, cvTailNumber);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        private bool CheckTailNumberExist(TextBox txtbx, CustomValidator cval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCompanySvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCompanySvc.GetFleet().EntityList.Where(x => x.TailNum.Trim().ToUpper() == txtbx.Text.Trim().ToUpper()).ToList();
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.GetFleet> FleetList = new List<FlightPakMasterService.GetFleet>();
                                FleetList = (List<FlightPakMasterService.GetFleet>)objRetVal;
                                hdnTailNumber.Value = FleetList[0].FleetID.ToString();
                                RadAjaxManager1.FocusControl(tbDepartmentCD.ClientID);
                                returnVal = false;
                            }
                            else
                            {
                                cval.IsValid = false;
                                txtbx.Text = string.Empty;
                                RadAjaxManager1.FocusControl(txtbx.ClientID);
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckDepartmentExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                //lbHomeBase.Text = string.Empty;
                lbdept.Text = string.Empty;
                hdnDepartment.Value = string.Empty;
                if (!string.IsNullOrEmpty(tbDepartmentCD.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {

                        var objRetVal = Service.GetAllDeptAuthorizationList().EntityList.Where(x => x.DepartmentCD.ToString().ToUpper().Trim() == tbDepartmentCD.Text.ToUpper().Trim()).ToList();
                        if (objRetVal != null && objRetVal.Count > 0)
                        {
                            tbDepartmentCD.Text = ((FlightPak.Web.FlightPakMasterService.GetAllDeptAuth)objRetVal[0]).DepartmentCD.ToString().ToUpper();
                            hdnDepartment.Value = ((FlightPak.Web.FlightPakMasterService.GetAllDeptAuth)objRetVal[0]).DepartmentID.ToString();

                        }
                        else
                        {
                            lbdept.Text = "Invalid Department Code.";
                            //tbDepartmentCD.Text = string.Empty;
                            tbDepartmentCD.Focus();
                            tbAuth.Enabled = false;
                            tbAuth.Text = "";
                            btnAuth.Enabled = false;
                        }
                    }
                }
                else
                {
                    lbdept.Text = "";
                    tbAuth.Enabled = false;
                    tbAuth.Text = "";
                    btnAuth.Enabled = false;
                }
                return returnVal;

            }
        }
        protected void tbAuth_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckAuthorizationExist(tbAuth, cvAuth);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        private bool CheckAuthorizationExist(TextBox txtbx, CustomValidator cval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCompanySvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCompanySvc.GetDepartmentAuthorizationList().EntityList.Where(x => x.AuthorizationCD.Trim().ToUpper().ToString() == txtbx.Text.Trim().ToUpper().ToString()).ToList();
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.DepartmentAuthorization> AuthList = new List<FlightPakMasterService.DepartmentAuthorization>();
                                AuthList = (List<FlightPakMasterService.DepartmentAuthorization>)objRetVal;
                                hdnAuth.Value = AuthList[0].AuthorizationID.ToString();
                                RadAjaxManager1.FocusControl(tbCat.ClientID);
                                returnVal = false;
                            }
                            else
                            {
                                cval.IsValid = false;
                                txtbx.Text = string.Empty;
                                RadAjaxManager1.FocusControl(txtbx.ClientID);
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void tbCat_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckFlightCategoryExist(tbCat, cvCat);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        private bool CheckFlightCategoryExist(TextBox txtbx, CustomValidator cval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(txtbx))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (txtbx.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCompanySvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCompanySvc.GetFlightCategoryByCategoryCD(txtbx.Text.Trim().ToUpper().ToString()).EntityList;
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.FlightCategoryByCategoryCDResult> FlightCategoryList = new List<FlightPakMasterService.FlightCategoryByCategoryCDResult>();
                                FlightCategoryList = (List<FlightPakMasterService.FlightCategoryByCategoryCDResult>)objRetVal;
                                hdnCat.Value = FlightCategoryList[0].FlightCategoryID.ToString();
                                RadAjaxManager1.FocusControl(chkTrip.ClientID);
                                returnVal = false;
                            }
                            else
                            {
                                cval.IsValid = false;
                                txtbx.Text = string.Empty;
                                RadAjaxManager1.FocusControl(txtbx.ClientID);
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool ValidateTripsheetDate()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool ReturnValue = true;
                DateTime BeginDate = new DateTime();
                DateTime EndDate = new DateTime();
                if (rdBl.SelectedItem.Value == "DateRange")
                {
                    TextBox ucInsDate = (TextBox)DateFrom.FindControl("tbDate");
                    if (!string.IsNullOrEmpty(ucInsDate.Text.Trim().ToString()))
                    {
                        BeginDate = Convert.ToDateTime(FormatDate(ucInsDate.Text.Trim(), DateFormat));
                    }
                    //else
                    //{
                    //    RadWindowManager1.RadAlert("Date From is required.", 400, 50, "Tripsheet Report Writer", "");
                    //    return ReturnValue = false;
                    //}
                    TextBox ucInsDate1 = (TextBox)DateTo.FindControl("tbDate");
                    if (!string.IsNullOrEmpty(ucInsDate1.Text.Trim().ToString()))
                    {
                        EndDate = Convert.ToDateTime(FormatDate(ucInsDate1.Text.Trim(), DateFormat));
                    }
                    //else
                    //{
                    //    RadWindowManager1.RadAlert("Date To is required.", 400, 50, "Tripsheet Report Writer", "");
                    //    return ReturnValue = false;
                    //}
                    if (BeginDate > EndDate)
                    {
                        RadWindowManager1.RadAlert("The Tripsheet Begin Date cannot be later than the Tripsheet End Date.", 400, 50, "Tripsheet Report Writer", "");
                        ReturnValue = false;
                    }
                }
                return ReturnValue;
            }
        }
        #endregion

        #region "Tripsheet Report Writer"
        string ModuleNameConstant = "Tripsheet Report Writer";
        private void OnPageLoad()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgTripsheetWriter, dgTripsheetWriter, RadAjaxLoadingPanel1);
                RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgTripsheetWriter.ClientID));
                EnableForm(false);
                dgTripsheetWriter.Rebind();
                if (dgTripsheetWriter.Items.Count > 0)
                {
                    dgTripsheetWriter.SelectedIndexes.Add(0);
                    SetDefaultRecord();
                }
                GetReportNumber();
            }
        }
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton insertCtl, editCtl, delCtl;
                    insertCtl = (LinkButton)dgTripsheetWriter.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
                    delCtl = (LinkButton)dgTripsheetWriter.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
                    editCtl = (LinkButton)dgTripsheetWriter.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
                    bool AddTrip = true;
                    bool EditTrip = true;
                    bool DeleteTrip = true;
                    if (AddTrip)
                    {
                        insertCtl.Visible = true;
                        if (add)
                            insertCtl.Enabled = true;
                        else
                            insertCtl.Enabled = false;
                    }
                    else
                    {
                        insertCtl.Visible = false;
                    }
                    if (DeleteTrip)
                    {
                        delCtl.Visible = true;
                        if (delete)
                        {
                            delCtl.Enabled = true;
                            delCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            delCtl.Enabled = false;
                            delCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        delCtl.Visible = false;
                    }
                    if (EditTrip)
                    {
                        editCtl.Visible = true;
                        if (edit)
                        {
                            editCtl.Enabled = true;
                            editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                        }
                        else
                        {
                            editCtl.Enabled = false;
                            editCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        editCtl.Visible = false;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        protected void dgTripsheetWriter_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        List<GetTripSheetReportHeader> TripSheetReportHeaderList = new List<GetTripSheetReportHeader>();
                        using (FlightPakMasterService.MasterCatalogServiceClient TripSheetReportService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var TripSheetReportHeaderInfo = TripSheetReportService.GetTripSheetReportHeaderList();
                            if (TripSheetReportHeaderInfo.ReturnFlag == true)
                            {
                                TripSheetReportHeaderList = TripSheetReportHeaderInfo.EntityList.Where(x => x.IsDeleted == false).ToList<GetTripSheetReportHeader>();
                            }
                            dgTripsheetWriter.DataSource = TripSheetReportHeaderList;
                            Session["TripSheetReportHeaderList"] = TripSheetReportHeaderList;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void CrewRoster_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;

                            if (Item.GetDataKeyValue("IsSelected") != null)
                            {
                                if (Convert.ToBoolean(Item.GetDataKeyValue("IsSelected").ToString()) == true)
                                {
                                    ((CheckBox)(Item["ReportNum"].FindControl("chkReportNum"))).Checked = true;
                                }
                                else
                                {
                                    ((CheckBox)(Item["ReportNum"].FindControl("chkReportNum"))).Checked = false;
                                }
                            }
                        }
                        dgReportSheet.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None;

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Reports.ReportsConstantName);
                }
            }
        }
        protected void dgTripsheetWriter_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (btnSave.Visible == false)
                        {
                            GridDataItem item = dgTripsheetWriter.SelectedItems[0] as GridDataItem;
                            Session["ReportID"] = item["ReportID"].Text;
                            Session["TripSheetReportHeaderID"] = item["TripSheetReportHeaderID"].Text;
                            Session["ReportNameBylink"] = null;
                            BindReportSheetDetail(Convert.ToInt64(item["TripSheetReportHeaderID"].Text));
                            EnableForm(false);
                            VisibleLinks(false);
                            LoadControlData(Session["ReportID"].ToString());
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        public void VisibleLinks(bool Enable)
        {
            lnkOverview.Visible = Enable;
            lnkTripSheet.Visible = Enable;
            lnkSummary.Visible = Enable;
            lnkIternary.Visible = Enable;
            lnkFlightLog.Visible = Enable;
            lnkPaxProfile.Visible = Enable;
            lnkInternational.Visible = Enable;
            lnkPaxTravelAuth.Visible = Enable;
            lnkAlerts.Visible = Enable;
            lnkPAERForm.Visible = Enable;
            lnkGeneralDeclaration.Visible = Enable;
            lnkCrewDutyOverview.Visible = Enable;
            lnkTripChecklist.Visible = Enable;
            lnkCanPassForm.Visible = Enable;
            lnkGDiscuss.Visible = Enable;
        }
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["TripSheetReportHeaderID"] != null)
                        {
                            string ID = Session["TripSheetReportHeaderID"].ToString();
                            foreach (GridDataItem Item in dgTripsheetWriter.MasterTableView.Items)
                            {
                                if (Item["TripSheetReportHeaderID"].Text.Trim() == ID.Trim())
                                {
                                    Item.Selected = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            EnableForm(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }
        protected void dgTripsheetWriter_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                GridEnable(false, false, false);
                                hdnSave.Value = "Update";
                                EnableForm(true);
                                SelectItem();
                                RadAjaxManager1.FocusControl(tbTrpRptWrtDescription.ClientID);
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                GridEnable(false, false, false);
                                ClearForm();
                                hdnSave.Value = "Save";
                                EnableForm(true);
                                //dgReportSheet.Enabled = true;
                                EnableReportSheet(true);
                                RadAjaxManager1.FocusControl(tbTrpRptWrtReportID.ClientID);
                                break;
                            case RadGrid.FilterCommandName:
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    column.CurrentFilterValue = string.Empty;
                                    column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }

        protected void dgReportSheet_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session.Remove(filterReportSessionName);
                        switch (e.CommandName)
                        {
                            case "RowClick":
                                Dictionary<string, int> dicRptType = new Dictionary<string, int>();
                                GridDataItem itemRow = (GridDataItem)e.Item;
                                int ReportNumber = 2;
                                int.TryParse(Convert.ToString(itemRow.GetDataKeyValue("ReportNumber")),
                                    out ReportNumber);
                                dicRptType.Add("xmlFilename", ReportNumber <= 0 ? 2 : ReportNumber);
                                Session["SnxmlFilename"] = dicRptType;
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[filterReportSessionName] = filterPair.Second.ToString();
                                if (btnSave.Visible == false)
                                {
                                    GridDataItem item = dgTripsheetWriter.SelectedItems[0] as GridDataItem;
                                    Session["ReportID"] = item["ReportID"].Text;
                                    Session["TripSheetReportHeaderID"] = item["TripSheetReportHeaderID"].Text;
                                    Session["ReportNameBylink"] = null;
                                    BindReportSheetDetail(Convert.ToInt64(item["TripSheetReportHeaderID"].Text));
                                    EnableForm(false);
                                    VisibleLinks(false);
                                    LoadControlData(Session["ReportID"].ToString());
                                }
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }


        protected void dgReportSheet_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(filterReportSessionName, dgReportSheet, Page.Session);
        }

        protected void dgTripsheetWriter_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgTripsheetWriter, Page.Session);
        }

        protected void dgTripsheetWriter_UpdateCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidateCustom = true;
                        if (CheckhomebaseExist())
                        {
                            cvTrpRptWrtHomeBase.IsValid = false;
                            RadAjaxManager1.FocusControl(tbTrpRptWrtHomeBase.ClientID);
                            IsValidateCustom = false;
                        }
                        if (CheckClientCodeExist())
                        {
                            cvTrpRptWrtClient.IsValid = false;
                            RadAjaxManager1.FocusControl(tbTrpRptWrtClient.ClientID);
                            IsValidateCustom = false;
                        }
                        if (CheckUserGroupExist())
                        {
                            cvTrpRptWrtUserGroup.IsValid = false;
                            RadAjaxManager1.FocusControl(tbTrpRptWrtUserGroup.ClientID);
                            IsValidateCustom = false;
                        }
                        if (IsValidateCustom)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.TripSheetReportHeader oTripSheetReportHeader = new FlightPakMasterService.TripSheetReportHeader();
                                oTripSheetReportHeader = GetItems(oTripSheetReportHeader);
                                var Result = objService.UpdateTripSheetReportHeader(oTripSheetReportHeader);
                                List<int> ReportNumber = new List<int>();
                                foreach (GridDataItem Item in dgReportSheet.Items)
                                {
                                    if (((CheckBox)(Item["ReportNum"].FindControl("chkReportNum"))).Checked == true)
                                    {
                                        ReportNumber.Add(Convert.ToInt32(Item.GetDataKeyValue("ReportNumber")));
                                    }
                                }
                                var Result1 = objService.UpdateReportSheetDetail(oTripSheetReportHeader.TripSheetReportHeaderID, ReportNumber);
                                if (Result.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                }
                                dgTripsheetWriter.Rebind();
                                SelectItem();
                                EnableForm(false);

                                GridEnable(true, true, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void dgTripsheetWriter_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;
                        bool IsValidateCustom = true;
                        if (CheckReportIDExist())
                        {
                            cvTrpRptWrtReportID.IsValid = false;
                            RadAjaxManager1.FocusControl(tbTrpRptWrtReportID.ClientID);
                            IsValidateCustom = false;
                        }
                        if (CheckhomebaseExist())
                        {
                            cvTrpRptWrtHomeBase.IsValid = false;
                            RadAjaxManager1.FocusControl(tbTrpRptWrtHomeBase.ClientID);
                            IsValidateCustom = false;
                        }
                        if (CheckClientCodeExist())
                        {
                            cvTrpRptWrtClient.IsValid = false;
                            RadAjaxManager1.FocusControl(tbTrpRptWrtClient.ClientID);
                            IsValidateCustom = false;
                        }
                        if (CheckUserGroupExist())
                        {
                            cvTrpRptWrtUserGroup.IsValid = false;
                            RadAjaxManager1.FocusControl(tbTrpRptWrtUserGroup.ClientID);
                            IsValidateCustom = false;
                        }
                        if (IsValidateCustom)
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.TripSheetReportHeader oTripSheetReportHeader = new FlightPakMasterService.TripSheetReportHeader();
                                oTripSheetReportHeader = GetItems(oTripSheetReportHeader);
                                var Result = objService.AddTripSheetReportHeader(oTripSheetReportHeader);
                                if (Result.ReturnFlag == true)
                                {
                                    ShowSuccessMessage();
                                }
                                dgTripsheetWriter.Rebind();
                                DefaultSelection(true);
                                EnableForm(false);
                                GridEnable(true, true, true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
                finally
                {
                }
            }
        }
        protected void dgTripsheetWriter_DeleteCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            e.Canceled = true;
                            using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                hdnSave.Value = "Delete";
                                FlightPakMasterService.TripSheetReportHeader oTripSheetReportHeader = new FlightPakMasterService.TripSheetReportHeader();
                                oTripSheetReportHeader = GetItems(oTripSheetReportHeader);
                                var Result = objService.DeleteTripSheetReportHeader(oTripSheetReportHeader);
                                dgTripsheetWriter.Rebind();
                                DefaultSelection(true);
                                GridEnable(true, true, true);
                            }
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessage(ex, ModuleNameConstant);
                    }
                    finally
                    {
                    }
                }
            }
        }
        private TripSheetReportHeader GetItems(TripSheetReportHeader oTripSheetReportHeader)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oTripSheetReportHeader))
            {
                oTripSheetReportHeader.TripSheetReportHeaderID = -1;
                if ((hdnSave.Value == "Update") || (hdnSave.Value == "Delete"))
                {
                    oTripSheetReportHeader.TripSheetReportHeaderID = Convert.ToInt64(hdnTrpRptWrtReportID.Value);
                }
                if (!string.IsNullOrEmpty(tbTrpRptWrtReportID.Text))
                {
                    oTripSheetReportHeader.ReportID = tbTrpRptWrtReportID.Text;
                }
                if (!string.IsNullOrEmpty(tbTrpRptWrtDescription.Text))
                {
                    oTripSheetReportHeader.ReportDescription = tbTrpRptWrtDescription.Text;
                }
                if (!string.IsNullOrEmpty(tbTrpRptWrtHomeBase.Text))
                {
                    oTripSheetReportHeader.HomebaseID = Convert.ToInt64(hdnTrpRptWrtHomeBaseID.Value);
                }
                if (!string.IsNullOrEmpty(tbTrpRptWrtClient.Text))
                {
                    oTripSheetReportHeader.ClientID = Convert.ToInt64(hdnTrpRptWrtClientID.Value);
                }
                if (!string.IsNullOrEmpty(tbTrpRptWrtUserGroup.Text))
                {
                    oTripSheetReportHeader.UserGroupID = Convert.ToInt64(hdnTrpRptWrtUserGroupID.Value);
                }
                oTripSheetReportHeader.IsDeleted = false;
                return oTripSheetReportHeader;
            }
        }
        private void EnableForm(bool Enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Enable))
            {
                if ((hdnSave.Value == "Save") && (Enable == true))
                {
                    tbTrpRptWrtReportID.Enabled = true;
                }
                else
                {
                    tbTrpRptWrtReportID.Enabled = false;
                }
                tbTrpRptWrtDescription.Enabled = Enable;
                tbTrpRptWrtHomeBase.Enabled = Enable;
                btnTrpRptWrtHomeBase.Enabled = Enable;
                tbTrpRptWrtClient.Enabled = Enable;
                btnTrpRptWrtClient.Enabled = Enable;
                tbTrpRptWrtUserGroup.Enabled = Enable;
                btnTrpRptWrtUserGroup.Enabled = Enable;
                EnableReportSheet(Enable);
                btnSave.Visible = Enable;
                btnCancel.Visible = Enable;
            }
        }
        private void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbTrpRptWrtReportID.Text = string.Empty;
                hdnTrpRptWrtReportID.Value = string.Empty;
                tbTrpRptWrtDescription.Text = string.Empty;
                tbTrpRptWrtHomeBase.Text = string.Empty;
                tbTrpRptWrtClient.Text = string.Empty;
                tbTrpRptWrtUserGroup.Text = string.Empty;
                hdnSave.Value = string.Empty;
                hdnTrpRptWrtHomeBaseID.Value = string.Empty;
                hdnTrpRptWrtClientID.Value = string.Empty;
                hdnTrpRptWrtUserGroupID.Value = string.Empty;
            }
        }
        private void LoadControlData(string ReportID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ReportID))
            {
                if (dgTripsheetWriter.SelectedItems.Count != 0)
                {
                    GridDataItem Item = dgTripsheetWriter.SelectedItems[0] as GridDataItem;
                    if (Session["ReportID"] != null)
                    {
                        if (Item["ReportID"].Text.Trim() == Session["ReportID"].ToString().Trim())
                        {
                            if (Item.GetDataKeyValue("TripSheetReportHeaderID") != null)
                            {
                                hdnTrpRptWrtReportID.Value = Item.GetDataKeyValue("TripSheetReportHeaderID").ToString().Trim();
                            }
                            else
                            {
                                hdnTrpRptWrtReportID.Value = string.Empty;
                            }
                            if (Item.GetDataKeyValue("ReportID") != null)
                            {
                                tbTrpRptWrtReportID.Text = Item.GetDataKeyValue("ReportID").ToString().Trim();
                            }
                            else
                            {
                                tbTrpRptWrtReportID.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("ReportDescription") != null)
                            {
                                tbTrpRptWrtDescription.Text = Item.GetDataKeyValue("ReportDescription").ToString().Trim();
                            }
                            else
                            {
                                tbTrpRptWrtDescription.Text = string.Empty;
                            }
                            if (Item.GetDataKeyValue("HomebaseCD") != null)
                            {
                                tbTrpRptWrtHomeBase.Text = Item.GetDataKeyValue("HomebaseCD").ToString().Trim();
                                hdnTrpRptWrtHomeBaseID.Value = Item.GetDataKeyValue("HomebaseID").ToString().Trim();
                            }
                            else
                            {
                                tbTrpRptWrtHomeBase.Text = string.Empty;
                                hdnTrpRptWrtHomeBaseID.Value = string.Empty;
                            }
                            if (Item.GetDataKeyValue("ClientCD") != null)
                            {
                                tbTrpRptWrtClient.Text = Item.GetDataKeyValue("ClientCD").ToString().Trim();
                                hdnTrpRptWrtClientID.Value = Item.GetDataKeyValue("ClientID").ToString().Trim();
                            }
                            else
                            {
                                tbTrpRptWrtClient.Text = string.Empty;
                                hdnTrpRptWrtClientID.Value = string.Empty;
                            }
                            if (Item.GetDataKeyValue("UserGroupCD") != null)
                            {
                                tbTrpRptWrtUserGroup.Text = Item.GetDataKeyValue("UserGroupCD").ToString().Trim();
                                hdnTrpRptWrtUserGroupID.Value = Item.GetDataKeyValue("UserGroupID").ToString().Trim();
                            }
                            else
                            {
                                tbTrpRptWrtUserGroup.Text = string.Empty;
                                hdnTrpRptWrtUserGroupID.Value = string.Empty;
                            }
                        }
                    }
                }
            }
        }
        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ClearForm();
                        EnableForm(false);
                        if (Session["ReportID"] != null)
                        {
                            LoadControlData(Session["ReportID"].ToString());
                        }
                        GridEnable(true, true, true);
                        if (dgTripsheetWriter.Items.Count > 0)
                        {
                            dgTripsheetWriter.SelectedIndexes.Add(0);
                            SetDefaultRecord();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value == "Update")
                        {
                            (dgTripsheetWriter.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                        }
                        else
                        {
                            (dgTripsheetWriter.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                        }
                        //dgTripsheetWriter.Rebind();
                        GridEnable(true, true, true);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void tbTrpRptWrtHomeBase_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckhomebaseExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void tbTrpRptWrtClient_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckClientCodeExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void tbTrpRptWrtUserGroup_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckUserGroupExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void tbTrpRptWrtReportID_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CheckReportIDExist();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        private bool CheckhomebaseExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                if (tbTrpRptWrtHomeBase.Text.Trim() != string.Empty)
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objCompanySvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objCompanySvc.GetCompanyMasterListInfo().EntityList.Where(x => x.HomebaseCD.Trim().ToUpper() == (tbTrpRptWrtHomeBase.Text.Trim().ToUpper())).ToList();
                        if (objRetVal.Count != 0)
                        {
                            List<FlightPakMasterService.GetAllCompanyMaster> CompanyList = new List<FlightPakMasterService.GetAllCompanyMaster>();
                            CompanyList = (List<FlightPakMasterService.GetAllCompanyMaster>)objRetVal.ToList();
                            hdnTrpRptWrtHomeBaseID.Value = CompanyList[0].HomebaseID.ToString();
                            RadAjaxManager1.FocusControl(tbTrpRptWrtClient.ClientID);
                            returnVal = false;
                        }
                        else
                        {
                            cvTrpRptWrtHomeBase.IsValid = false;
                            RadAjaxManager1.FocusControl(tbTrpRptWrtHomeBase.ClientID);
                            returnVal = true;
                        }
                    }
                }
                return returnVal;
            }
        }
        private bool CheckClientCodeExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (tbTrpRptWrtClient.Text.Trim() != string.Empty)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objCountrysvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objCountrysvc.GetClientByClientCD(tbTrpRptWrtClient.Text.Trim().ToUpper().ToString()).EntityList;
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.ClientByClientCDResult> ClientList = new List<FlightPakMasterService.ClientByClientCDResult>();
                                ClientList = (List<FlightPakMasterService.ClientByClientCDResult>)objRetVal.ToList();
                                hdnTrpRptWrtClientID.Value = ClientList[0].ClientID.ToString();
                                RadAjaxManager1.FocusControl(tbTrpRptWrtUserGroup.ClientID);
                                returnVal = false;
                            }
                            else
                            {
                                cvTrpRptWrtClient.IsValid = false;
                                RadAjaxManager1.FocusControl(tbTrpRptWrtClient.ClientID);
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckUserGroupExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool returnVal = false;
                    if (tbTrpRptWrtUserGroup.Text.Trim() != string.Empty)
                    {
                        using (AdminService.AdminServiceClient objUserGroupsvc = new AdminService.AdminServiceClient())
                        {
                            var objRetVal = objUserGroupsvc.GetUserGroupList(UserPrincipal.Identity._customerID).EntityList.Where(x => x.UserGroupCD.Trim().ToUpper().ToString() == tbTrpRptWrtUserGroup.Text.Trim().ToUpper().ToString()).ToList();
                            if (objRetVal.Count != 0)
                            {
                                List<AdminService.UserGroup> UserGroupList = new List<AdminService.UserGroup>();
                                UserGroupList = (List<AdminService.UserGroup>)objRetVal.ToList();
                                hdnTrpRptWrtUserGroupID.Value = UserGroupList[0].UserGroupID.ToString();
                                returnVal = false;
                            }
                            else
                            {
                                cvTrpRptWrtUserGroup.IsValid = false;
                                RadAjaxManager1.FocusControl(tbTrpRptWrtClient.ClientID);
                                returnVal = true;
                            }
                        }
                    }
                    return returnVal;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        private bool CheckReportIDExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                if (tbTrpRptWrtReportID.Text.Trim() != string.Empty)
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient TripSheetReportService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var TripSheetReportHeaderInfo = TripSheetReportService.GetTripSheetReportHeaderList().EntityList.Where(x => x.ReportID.Trim().ToUpper().ToString() == tbTrpRptWrtReportID.Text.Trim().ToUpper().ToString()).ToList();
                        if (TripSheetReportHeaderInfo.Count != 0)
                        {
                            cvTrpRptWrtReportID.IsValid = false;
                            RadAjaxManager1.FocusControl(tbTrpRptWrtReportID.ClientID);
                            returnVal = true;
                            //List<GetTripSheetReportHeader> TripSheetReportHeaderList = new List<GetTripSheetReportHeader>();
                            //TripSheetReportHeaderList = (List<GetTripSheetReportHeader>)TripSheetReportHeaderInfo.ToList();
                            //hdnTrpRptWrtReportID.Value = TripSheetReportHeaderList[0].TripSheetReportHeaderID.ToString();
                            //RadAjaxManager1.FocusControl(tbTrpRptWrtDescription.ClientID);
                            //returnVal = false;                            
                        }
                        else
                        {
                            RadAjaxManager1.FocusControl(tbTrpRptWrtDescription.ClientID);
                            returnVal = false;
                        }
                    }
                }
                return returnVal;
            }
        }
        private void SetDefaultRecord()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                foreach (GridDataItem Item in dgTripsheetWriter.Items)
                {
                    if (Item.GetDataKeyValue("ReportID") != null)
                    {
                        if ((UserPrincipal != null) &&
                            (UserPrincipal.Identity != null) &&
                            (UserPrincipal.Identity._fpSettings != null) &&
                            (!string.IsNullOrEmpty(UserPrincipal.Identity._fpSettings._TripsheetRptWriteGroup)))
                        {
                            if (Item.GetDataKeyValue("ReportID").ToString().ToUpper().Trim() == UserPrincipal.Identity._fpSettings._TripsheetRptWriteGroup.ToUpper().Trim())
                            {
                                Item.Selected = true;
                                Session["ReportID"] = Item.GetDataKeyValue("ReportID").ToString();
                                Session["TripSheetReportHeaderID"] = Item.GetDataKeyValue("TripSheetReportHeaderID").ToString();
                                LoadControlData(Item.GetDataKeyValue("ReportID").ToString());
                                break;
                            }
                        }
                        else
                        {
                            if (Item.GetDataKeyValue("ReportID").ToString().Trim().ToUpper() == "DEFAULT")
                            {
                                Item.Selected = true;
                                Session["ReportID"] = Item.GetDataKeyValue("ReportID").ToString();
                                Session["TripSheetReportHeaderID"] = Item.GetDataKeyValue("TripSheetReportHeaderID").ToString();
                                LoadControlData(Item.GetDataKeyValue("ReportID").ToString());
                                break;
                            }
                        }
                    }
                }


                //else
                //{
                //    foreach (GridDataItem Item in dgTripsheetWriter.Items)
                //    {
                //        if (Item.GetDataKeyValue("ReportID") != null)
                //        {
                //            if (Item.GetDataKeyValue("ReportID").ToString().Trim().ToUpper() == "DEFAULT")
                //            {
                //                Item.Selected = true;
                //                break;
                //            }
                //        }
                //    }
                //}
            }
        }
        private void GetReportNumber()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, int> dicRptType = new Dictionary<string, int>();
                if (Request.QueryString["xmlFilename"] != null)
                {

                    if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "PRETSOverview.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 1);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "PreTSTripSheetMain.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 2);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "PREPRETSSummary.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 13);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "PRETSTripSheetItinerary.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 3);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "PRETSFlightLogInformation.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 4);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "PRETSPaxProfile.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 5);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "PRETSInternationalData.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 6);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "PRETSPaxTravelAuthorization.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 7);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "PRETSTripSheetAlert.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 8);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "PRETSPairForm.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 9);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "PRETSGeneralDeclaration.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 10);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "PRETSCrewDutyOverview.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 11);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "PRETSTripSheetChecklist.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 12);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "PRETSCanPassForm.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 14);
                    }
                    else if (Request.QueryString["xmlFilename"].ToString().Trim().ToUpper() == "PREGeneralDeclarationAPDX.xml".Trim().ToUpper())
                    {
                        dicRptType.Add("xmlFilename", 16);
                    }

                    Session["SnxmlFilename"] = dicRptType;
                }
            }
        }
        #endregion
    }
}

