﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExportReportInformation.aspx.cs"
    Inherits="FlightPak.Web.Views.Reports.ExportReportInformation" %>

<%@ Register Src="~/UserControls/UCReportHtmlSelection.ascx" TagName="Grid" TagPrefix="UCReportHtmlSelection" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/javascript">
        
        function getQueryVariable(variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    return pair[1];
                }
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function buttonclose() {
                var oWnd = GetRadWindow();
                oWnd.close();
            }
            function Close() {
                GetRadWindow().Close();
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnablePageMethods="true">
    </telerik:RadScriptManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
        </Windows>
    </telerik:RadWindowManager>
    <div class="art-setting-content-reportWidgets" style="width:auto;">
        <div class="art-setting-sidebar-report">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <telerik:RadPanelBar runat="server" ExpandMode="SingleExpandedItem" ID="pnlReportTab"
                            AllowCollapseAllItems="true" CssClass="RadNavMenu">
                            <CollapseAnimation Type="None"></CollapseAnimation>
                            <ExpandAnimation Type="None"></ExpandAnimation>
                        </telerik:RadPanelBar>
                    </td>
                </tr>
            </table>
        </div>
        <div id="DivExternalForm" runat="server" class="ExternalForm">
            <table class="padleft_10 padtop_10" width="100%">
                <tr>
                    <td colspan="2" valign="top">
                        <table>
                            <tr>
                                <td>
                                    <asp:Button runat="server" ID="btExport" CssClass="button" Text="Export" OnClick="btnExport_Click" />
                                </td>
                                <%--<td valign="top">
                                    <asp:Button runat="server" ID="btnPreview" Text="Preview" CssClass="button" OnClick="btnShowReports_OnClick" />
                                </td>--%>
                                <td valign="top">
                                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" OnClientClick="buttonclose();"
                                        CssClass="button" />
                                    <asp:Label ID="lbScript" runat="server" Text="" Visible="false"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <%--<td>
                                                <asp:Button runat="server" ID="btnExport" Text="Export" OnClientClick1="javascript:return Export_Click();"
                                                    CssClass="button" OnClientClick="document.forms[0].target = '_blank';buttonclose();"
                                                    OnClick="btnShowReports_OnClick" />
                                            
                                            </td>--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td>
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <span class="mnd_text">
                                                    <asp:Label runat="server" ID="lbTitle"></asp:Label></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tblspace_10">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:PlaceHolder ID="Report" runat="server"></asp:PlaceHolder>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="tblspace_10">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel runat="server" ID="ExportPanal">
                                        <table class="padleft_10 padtop_10">
                                            <tr>
                                                <td class="tdLabel100">
                                                    Export File Type
                                                </td>
                                                <td style="margin-left: 40px">
                                                    <asp:DropDownList ID="ddlFormat" runat="server">
                                                        <asp:ListItem Text="Excel" Value="Excel" />
                                                        <asp:ListItem Text="MHTML" Value="MHTML" />
                                                        <%-- <asp:ListItem Text="XML" Value="1" />
                                                        <asp:ListItem Text="CSV" Value="2" />--%>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Button runat="server" CssClass="ui_nav" ID="btExportFileFormat" Text="Generate"
                                                        OnClick="btnExportFileFormat_Click" />
                                                </td>
                                                <td>
                                                    <asp:Button runat="server" CssClass="ui_nav" ID="btClose" Text="Close" OnClick="btnCostFileFormat_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label runat="server" ID="lblError" CssClass="alert-text"></asp:Label>
                        <asp:Wizard ID="Wizard1" runat="server" DisplaySideBar="false" ActiveStepIndex="0"
                            Height="209px" Width="100%" BackColor="#f0f0f0" FinishPreviousButtonStyle-CssClass="ui_nav"
                            CancelButtonStyle-CssClass="ui_nav" FinishCompleteButtonStyle-CssClass="ui_nav"
                            StepNextButtonStyle-CssClass="ui_nav" StepPreviousButtonStyle-CssClass="ui_nav"
                            StartNextButtonStyle-CssClass="ui_nav" OnFinishButtonClick="OnFinish" OnNextButtonClick="OnNext"
                            OnPreviousButtonClick="OnPrevious">
                            <WizardSteps>
                                <asp:WizardStep ID="WizardStep1" runat="server">
                                    <asp:Label runat="server" ID="lblw1" Text="Click on each of the Available fields by name and the right arrow to select them. Add them in the preferred display order or use the Up/Down arrows to reorder them for reporting. Click on the Selected fields and left arrow to de-select them."></asp:Label>
                                    <br />
                                    <br />
                                    <UCReportHtmlSelection:Grid ID="UCRHS" runat="server" />
                                </asp:WizardStep>
                                <asp:WizardStep ID="WizardStep2" runat="server">
                                    <asp:Label runat="server" ID="lblw2" Text="Select how data is to be sorted"></asp:Label>
                                    <br />
                                    <br />
                                    <br />
                                    <UCReportHtmlSelection:Grid ID="UCRHSort" runat="server" />
                                    <%-- <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel1">

                                            <telerik:RadListBox runat="server" ID="SelectionSource" Height="200px" Width="200px"

                                                SelectionMode="Single" />

                                            <telerik:RadListBox runat="server" ID="SelectionDestination" AllowReorder="false"

                                                SelectionMode="Single" Height="200px" Width="200px">

                                            </telerik:RadListBox>

                                        </telerik:RadAjaxPanel>--%>
                                </asp:WizardStep>
                                <asp:WizardStep ID="WizardStep4" runat="server">
                                    <asp:Label runat="server" ID="lblw3" Text="Edit the Header Text "></asp:Label>
                                    <asp:TextBox ID="tbEditHeaderText" runat="server" OnTextChanged="tbEditHeaderText_OnTextChanged"
                                        AutoPostBack="true" ClientIDMode="Static" />
                                    <br />
                                    <br />
                                    <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel2">
                                        <table>
                                            <tr>
                                                <td>
                                                    Field
                                                </td>
                                                <td>
                                                    HTML Heading
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <telerik:RadListBox runat="server" ID="SelectedSource" Height="200px" Width="200px"
                                                        SelectionMode="Single" />
                                                </td>
                                                <td>
                                                    <telerik:RadListBox runat="server" ID="HeaderBySource" AllowReorder="false" SelectionMode="Single"
                                                        Height="200px" Width="200px" OnSelectedIndexChanged="HeaderBySource_OnSelectedIndexChanged"
                                                        AutoPostBack="true">
                                                    </telerik:RadListBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </telerik:RadAjaxPanel>
                                </asp:WizardStep>
                                <asp:WizardStep ID="WizardStep3" runat="server">
                                    <table width="100%" class="ERI_colorpicker">
                                        <tr>
                                            <td width="40%">
                                                <fieldset>
                                                    <legend>Page Layout</legend>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="tdLabel130">
                                                                            Use HTML Defaults
                                                                        </td>
                                                                        <td class="tdLabel80">
                                                                            <asp:CheckBox ID="chkHeader" runat="server" Text="Header" />
                                                                        </td>
                                                                        <td class="tdLabel70">
                                                                            <asp:CheckBox ID="chkFooter" runat="server" Text="Footer" />
                                                                        </td>
                                                                        <td class="tdLabel100">
                                                                            <asp:CheckBox ID="chkBackground" runat="server" Text="Background" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="tdLabel130">
                                                                            Title
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtTitle" runat="server" Width="300px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="tdLabel130">
                                                                            Foreground Color
                                                                        </td>
                                                                        <td class="tdLabel60">
                                                                            <telerik:RadColorPicker ID="ForegroundColor" runat="server" SelectedColor="" ShowIcon="True">
                                                                                <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                            </telerik:RadColorPicker>
                                                                        </td>
                                                                        <td class="tdLabel110">
                                                                            Background Color
                                                                        </td>
                                                                        <td>
                                                                            <telerik:RadColorPicker ID="BackgroundColor" runat="server" SelectedColor="" ShowIcon="True">
                                                                                <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                            </telerik:RadColorPicker>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                            <td width="40%">
                                                <fieldset>
                                                    <legend>Table Layout</legend>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="tdLabel130">
                                                                            Border Color
                                                                        </td>
                                                                        <td>
                                                                            <telerik:RadColorPicker ID="TableBorderColor" runat="server" SelectedColor="" ShowIcon="True">
                                                                                <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                            </telerik:RadColorPicker>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="tdLabel130">
                                                                            Foreground Color
                                                                        </td>
                                                                        <td>
                                                                            <telerik:RadColorPicker ID="TableForegroundColor" runat="server" SelectedColor=""
                                                                                ShowIcon="True">
                                                                                <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                            </telerik:RadColorPicker>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="tdLabel130">
                                                                            Background Color
                                                                        </td>
                                                                        <td>
                                                                            <telerik:RadColorPicker ID="TableBackgroundColor" runat="server" SelectedColor=""
                                                                                ShowIcon="True">
                                                                                <Localization RGBSlidersTabText=" RGB Sliders" />
                                                                            </telerik:RadColorPicker>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:WizardStep>
                            </WizardSteps>
                        </asp:Wizard>
                        <asp:HiddenField ID="hdnDept" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hdnCRMainID" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hdnMainID" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hdnHeaderSourceID" runat="server" />
                        <asp:HiddenField ID="hdnRptXMLName" runat="server" ClientIDMode="Static" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
