﻿using FlightPak.Web.Framework.Constants;
using FlightPak.Web.PostflightService;
using FlightPak.Web.ViewModels;
using FlightPak.Web.ViewModels.Postflight;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using FlightPak.Web.Framework.Helpers;
using Newtonsoft.Json;
using Omu.ValueInjecter;
using System.Net;
using FlightPak.Web.BusinessLite.Postflight;
using FlightPak.Web.Views.Transactions.PostFlight;
using FlightPak.Web.BusinessLite;
using FlightPak.Web.Framework.Error;
using FlightPak.Web.ViewModels.MasterData;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Transactions
{
    public partial class PostFlightValidator : TripManagerBase
    {
        private static ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable Summary_Validate_Retrieve()
        {
            Hashtable RetObj = new Hashtable();
            List<PostflightLegViewModel> Dt = new List<PostflightLegViewModel>();

            UserPrincipalViewModel UserPrincipal = getUserPrincipal();
            if (UserPrincipal == null) return RetObj;
            string DateFormat = UserPrincipal._ApplicationDateFormat;
            decimal? TenMin = UserPrincipal._TimeDisplayTenMin == null ? POTenthMinuteFormat.Tenth : UserPrincipal._TimeDisplayTenMin;
            decimal? ElapseTM = UserPrincipal._ElapseTMRounding;
 
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
            {
                PostflightLogViewModel TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                if (TripLog != null && TripLog.PostflightLegs != null)
                {
                    Dt = TripLog.PostflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                    if (Dt.Count > 0)
                    {
                        var obj = (from u in Dt
                                   where (u.ArrivalAirport != null && u.DepartureAirport != null && u.ArrivalAirport.AirportID != 0 && u.DepartureAirport.AirportID != 0 && u.IsDeleted == false)
                                   select new
                                   {
                                       LegNum = u.LegNUM,
                                       Leg = u.LegNUM,
                                       Depart = u.DepartureAirport.IcaoID ?? string.Empty,
                                       Arrive = u.ArrivalAirport.IcaoID ?? string.Empty,
                                       Miles = u.Distance,
                                       Scheduled = u.ScheduledTM == null ? null : u.ScheduledTM.FSSParseDateTimeString(string.Format("{0} HH:mm", DateFormat)),
                                       Out = u.OutboundDTTM == null ? null : u.OutboundDTTM.Value.ToString("HH:mm"),
                                       Off = u.TimeOff,
                                       On = u.TimeOn,
                                       In = u.BlockIN,
                                       BlkHrs = (UserPrincipal._TimeDisplayTenMin == POTenthMinuteFormat.Minute ? FPKConversionHelper.ConvertTenthsToMins(Math.Round(u.BlockHours ?? 0, 3).ToString()) : Math.Round(u.BlockHours ?? 0, 1).ToString()),
                                       FltHrs = (UserPrincipal._TimeDisplayTenMin == POTenthMinuteFormat.Minute ? FPKConversionHelper.ConvertTenthsToMins(Math.Round(u.FlightHours ?? 0, 3).ToString()) : Math.Round(u.FlightHours ?? 0, 1).ToString()),
                                       FAR = u.FedAviationRegNum,
                                       Category = (u.FlightCatagory != null && u.FlightCatagory.FlightCatagoryCD != null) ? u.FlightCatagory.FlightCatagoryCD : string.Empty,
                                       End = u.IsDutyEnd,
                                       FltNo = u.FlightNum,
                                       DepartDate = u.ScheduledTM == null ? null : u.ScheduledTM.FSSParseDateTimeString(DateFormat),
                                       ArrivalDate = u.InboundDTTM == null ? null : u.InboundDTTM.FSSParseDateTimeString(DateFormat),
                                       DepartTime = u.ScheduledTM == null ? null : u.ScheduledTM.FSSParseDateTimeString("HH:mm"),
                                       ArrivalTime = u.InboundDTTM == null ? null : u.InboundDTTM.FSSParseDateTimeString("HH:mm"),
                                       FlightCost = u.FlightCost == null ? "0.00" : Math.Round(Convert.ToDecimal(u.FlightCost ?? 0), 2).ToString()
                                   }).OrderBy(x => x.LegNum);
                        RetObj["meta"] = new PagingMetaData() { Page = obj.Count() / 10, Size = 10, total_items = obj.Count() };
                        RetObj["results"] = obj;
                    }
                    else
                    {
                        RetObj["meta"] = new PagingMetaData() { Page = Dt.Count() / 10, Size = 10, total_items = Dt.Count() };
                        RetObj["results"] = Dt;
                    }
                }
                else
                {
                    RetObj["meta"] = new PagingMetaData() { Page = Dt.Count() / 10, Size = 10, total_items = Dt.Count() };
                    RetObj["results"] = Dt;
                }
            }
            else
            {
                RetObj["meta"] = new PagingMetaData() { Page = Dt.Count() / 10, Size = 10, total_items = Dt.Count() };
                RetObj["results"] = Dt;
            }
            return RetObj;
        }
   
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> DomesticInternational_Validate_Retrieve(string HomeBaseCountryId, string DepartureCountryId, string ArrivalCountryId, string LegNUM)
        {
            FSSOperationResult<Hashtable> RetObj = new FSSOperationResult<Hashtable>();
            Hashtable RetObject = new Hashtable();
            if (RetObj.IsAuthorized() == false)
            {
                return RetObj;
            }
            decimal LegType = 0;

            if (HomeBaseCountryId != DepartureCountryId || HomeBaseCountryId != ArrivalCountryId)
            {
                LegType = WebConstants.LegDutyType.International; //"INT";
            }
            else if (HomeBaseCountryId == DepartureCountryId || HomeBaseCountryId == ArrivalCountryId)
            {
                LegType = WebConstants.LegDutyType.Domestic; //"DOM";
            }
            else
            {
                LegType = WebConstants.LegDutyType.Domestic; //"DOM";
            }
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
            {
                var pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                if (pfViewModel.PostflightLegs != null && pfViewModel.PostflightLegs.Count > 0)
                {
                    var leg = pfViewModel.PostflightLegs.FirstOrDefault(l => l.IsDeleted == false && l.LegNUM == Convert.ToInt64(LegNUM));
                    if (leg != null)
                    {
                        //leg.CheckGroup = (LegType == WebConstants.LegDutyType.Domestic) ? "DOM" : "INT";
                        //leg.DutyTYPE = (LegType == "DOM") ? WebConstants.LegDutyType.Domestic : WebConstants.LegDutyType.International;
                        leg.DutyTYPE = LegType;
                    }
                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                }
            }
            RetObject["LegType"] = LegType;
            RetObject["DutyTYPE"] = LegType;//== "DOM" ? 1 : 2; 
            RetObj.Result = RetObject;
            RetObj.StatusCode = HttpStatusCode.OK;
            RetObj.Success = true;
            return RetObj;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> POCalculateMiles_Validate_Retrieve(string departICAOID, string arriveICAOID)
        {
            FSSOperationResult<Hashtable> RetObj = new FSSOperationResult<Hashtable>();
            Hashtable RetObject = new Hashtable();
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();
            if (RetObj.IsAuthorized() == false)
            {
                return RetObj;
            }

            RetObject = PostflightLegLite.POCalculateMiles(departICAOID, arriveICAOID);
      
            RetObj.Result = RetObject;
            RetObj.StatusCode = HttpStatusCode.OK;
            RetObj.Success = true;
            return RetObj;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightLegDelayTypeViewModel> PODelayType_Validate_Retrieve(string DelayType)
        {
            FSSOperationResult<PostflightLegDelayTypeViewModel> RetObj = new FSSOperationResult<PostflightLegDelayTypeViewModel>();
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();
            if (UserPrincipal == null)
            {
                RetObj.Result = null;
                RetObj.StatusCode = HttpStatusCode.Unauthorized;
                RetObj.Success = false;
                RetObj.ErrorsList.Add("User not logged in");
                return RetObj;
            }

            PostflightLegDelayTypeViewModel objDelyType = DBCatalogsDataLoader.GetDelayTypeInfo_FromFilter(0, DelayType);
            if (objDelyType != null && objDelyType.DelayTypeID != 0)
            {
                RetObj.Result = objDelyType;
                RetObj.StatusCode = HttpStatusCode.OK;
                RetObj.Success = true;
                return RetObj;
            }
            else
            {
                RetObj.Result = null;
                RetObj.StatusCode = HttpStatusCode.OK;
                RetObj.Success = false;
                RetObj.ErrorsList.Add("Invalid Delay Type Code.");
                return RetObj;
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightLegViewModel> CrewDutyHours_Validate_Retrieve_VM(int LegNUM, string blockHours, string flightHours, string dutyHours)
        {
            var result = new FSSOperationResult<PostflightLegViewModel>();
            if (result.IsAuthorized() == false)
            {
                return result;
            }
            SetCrewDutyHours(LegNUM, blockHours, flightHours, dutyHours);
            var pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            PostflightLegViewModel currentLeg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.LegNUM == Convert.ToInt32(LegNUM) && x.IsDeleted == false);

            result.StatusCode = HttpStatusCode.OK;
            result.Result = currentLeg;
            result.Success = true;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> ScheduledHours_Calculate_Validate_Retrieve(int LegNUM, string scheduleTime, string outTime, string inTime, string offTime, string onTime, string scheduleDate, string outDate, string inDate, string hdnMiles, string tbStMiles)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                FSSOperationResult<Hashtable> RetObj = new FSSOperationResult<Hashtable>();
                Hashtable RetObject = new Hashtable();
                Hashtable DutyHoursCal = new Hashtable();
                UserPrincipalViewModel UserPrincipal = getUserPrincipal();
                if (RetObj.IsAuthorized() == false)
                {
                    return RetObj;
                }

                // Default Declarations
                DateTime scheduleDateTM = DateTime.MinValue;
                DateTime outDateTM = DateTime.MinValue;
                DateTime inDateTM = DateTime.MinValue;
                Boolean isInTimeEmtpy = string.IsNullOrWhiteSpace(inTime);
                // Final Result Variables
                string blockHrs = "0";
                string flightHrs = "0";

                string scheduleTimeErrMsg = ValidateTime(scheduleTime);
                if (scheduleTimeErrMsg != string.Empty)
                    scheduleTime = "00:00";

                string outTimeErrMsg = ValidateTime(outTime);
                if (outTimeErrMsg != string.Empty)
                    outTime = "00:00";

                string offTimeErrMsg = ValidateTime(offTime);
                if (offTimeErrMsg != string.Empty)
                    offTime = "00:00";

                string onTimeErrMsg = ValidateTime(onTime);
                if (onTimeErrMsg != string.Empty)
                    onTime = "00:00";

                string inTimeErrMsg = ValidateTime(inTime);
                if (inTimeErrMsg != string.Empty)
                    inTime = "00:00";

                if (string.IsNullOrEmpty(inDate) && string.IsNullOrEmpty(outDate))
                {
                    RetObject["OutDate"] = scheduleDate;
                    RetObject["InDate"] = scheduleDate;
                }

                string tbBlockHours = string.Empty;
                string tbFlightHours = string.Empty;
                string tbDutyHours = string.Empty;
                string tbFlightCost = string.Empty;

                string tbAirFrameHours = string.Empty;
                string tbEngineers1Hr = string.Empty;
                string tbEngineers2Hr = string.Empty;
                string tbEngineers3Hr = string.Empty;
                string tbEngineers4Hr = string.Empty;

                decimal tbAirFramecycles = 0;
                decimal tbEngineers1Cycle = 0;
                decimal tbEngineers2Cycle = 0;
                decimal tbEngineers3Cycle = 0;
                decimal tbEngineers4Cycle = 0;
                AircraftViewModel objAircraft = new AircraftViewModel();
                var pfViewModel = new PostflightLogViewModel();
                var currentLeg = new PostflightLegViewModel();
                pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

                if (string.IsNullOrEmpty(inDate) && string.IsNullOrEmpty(outDate))
                {
                    //bool AutoPopulateLegTimes = false;
                    //if (UserPrincipal.Identity != null && UserPrincipal._IsAutoPopulated != null)
                    //    AutoPopulateLegTimes = UserPrincipal._IsAutoPopulated;

                    //if (AutoPopulateLegTimes)
                    //{
                    //    tbOutDate.Text = tbScheduledDate.Text;
                    //    tbInDate.Text = tbScheduledDate.Text;
                    //}
                }
                else
                {
                    DateTime dtFltOffTime = DateTime.MinValue;
                    DateTime dtFltOnTime = DateTime.MinValue;
                    TimeSpan blockSpan = new TimeSpan(0, 0, 0);
                    TimeSpan flightSpan = new TimeSpan(0, 0, 0);
                    try
                    {
                        scheduleDateTM = Convert.ToDateTime(scheduleDate + " " + scheduleTime, CultureInfo.InvariantCulture);
                        inDateTM = Convert.ToDateTime(inDate + " " + inTime, CultureInfo.InvariantCulture);

                        if (UserPrincipal._DutyBasis == WebConstants.POCrewDutyBasis.ScheduledDep)
                        {
                            if (Convert.ToDateTime(scheduleDate) < Convert.ToDateTime(outDate))
                                outDateTM = Convert.ToDateTime(scheduleDate + " " + outTime, CultureInfo.InvariantCulture);
                            else
                                outDateTM = Convert.ToDateTime(outDate + " " + outTime, CultureInfo.InvariantCulture);

                            if (outDateTM < scheduleDateTM && (Convert.ToDateTime(scheduleDate) != Convert.ToDateTime(inDate)))
                            {
                                var diffDays = Convert.ToInt32((Convert.ToDateTime(inDate) - Convert.ToDateTime(scheduleDate)).TotalDays);
                                TimeSpan diffSpan = new TimeSpan( 0, 0, 0);
                                TimeSpan oneDaySpan = new TimeSpan(24 * (diffDays == 0 ? 1 : diffDays), 0, 0);
                                diffSpan = scheduleDateTM.Subtract(outDateTM);
                                diffSpan = oneDaySpan - diffSpan;
                                DateTime diffDateTM = scheduleDateTM.Add(diffSpan);
                                if (inDateTM < diffDateTM)
                                {
                                    diffDateTM = outDateTM;
                                }
                                outDateTM = diffDateTM;
                            }
                        }
                        else
                            outDateTM = Convert.ToDateTime(outDate + " " + outTime, CultureInfo.InvariantCulture);

                        // Block Hours
                        if (!isInTimeEmtpy)
                        {
                            blockSpan = inDateTM.Subtract(outDateTM);
                            blockHrs = FPKConversionHelper.ConvertTenthsToMins(Convert.ToString(Math.Round(Convert.ToDecimal(blockSpan.TotalHours), 3)));
                        }
                        // Flight Hours
                        if (UserPrincipal._DutyBasis == WebConstants.POCrewDutyBasis.ScheduledDep)
                        {
                            dtFltOffTime = Convert.ToDateTime(scheduleDate + " " + offTime, CultureInfo.InvariantCulture);
                            dtFltOnTime = Convert.ToDateTime(scheduleDate + " " + onTime, CultureInfo.InvariantCulture); 
                        }
                        else
                        {
                            dtFltOffTime = Convert.ToDateTime(outDate + " " + offTime, CultureInfo.InvariantCulture);
                            dtFltOnTime = Convert.ToDateTime(outDate + " " + onTime, CultureInfo.InvariantCulture); 
                        }

                        if (dtFltOnTime < dtFltOffTime)
                            dtFltOnTime = Convert.ToDateTime(inDate + " " + onTime, CultureInfo.InvariantCulture);

                        flightSpan = dtFltOnTime.Subtract(dtFltOffTime);
                    }
                    catch (Exception ex)
                    {
                        
                    }

                    if (dtFltOffTime != null && dtFltOffTime != DateTime.MinValue)
                        flightHrs = FPKConversionHelper.ConvertTenthsToMins(Convert.ToString(Math.Round(Convert.ToDecimal(flightSpan.TotalHours), 3)));
                    else
                        flightHrs = "00:00";

                    // To check if the selection is Tenths, in Company Profile
                    DBCatalogsDataLoader objDataLoader = new DBCatalogsDataLoader();
                    DutyHoursCal = PODutyHourCalculation(LegNUM, scheduleDateTM, outDateTM, inDateTM, UserPrincipal, pfViewModel, objDataLoader);
                    if (UserPrincipal != null && UserPrincipal._TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
                    {
                        tbBlockHours = FPKConversionHelper.ConvertMinToTenths(blockHrs, false, (int)UserPrincipal._HoursMinutesCONV).ToString();
                        tbFlightHours = FPKConversionHelper.ConvertMinToTenths(flightHrs, false, (int)UserPrincipal._HoursMinutesCONV).ToString();
                        tbDutyHours = FPKConversionHelper.ConvertMinToTenths(Convert.ToString(DutyHoursCal["TotalHours"]), false, (int)UserPrincipal._HoursMinutesCONV).ToString();
                    } 
                    else
                    {
                        tbBlockHours = blockHrs;
                        tbFlightHours = flightHrs;
                        tbDutyHours = Convert.ToString(DutyHoursCal["TotalHours"]);
                    }

                    //Calculation Adjustment for String
                    tbBlockHours = FormatValue(tbBlockHours);
                    tbFlightHours = FormatValue(tbFlightHours);
                    tbDutyHours = FormatValue(tbDutyHours);

                    // Call Fleet Cost 
                    DateTime departDTTM = DateTime.MinValue;
                    DateTime arrivalDTTM = DateTime.MinValue;
                    TimeSpan deparrSpan = new TimeSpan(0, 0, 0);
                    if (pfViewModel != null && pfViewModel.PostflightLegs != null && pfViewModel.PostflightLegs.Count > 0)
                    {
                        if (pfViewModel.PostflightMain.Aircraft != null)
                            objAircraft = pfViewModel.PostflightMain.Aircraft;

                        currentLeg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.LegNUM == LegNUM && x.IsDeleted == false && x.State != TripEntityState.Deleted);
                        if (currentLeg != null && currentLeg.ScheduleDTTMLocal != null)
                        {
                            departDTTM = (DateTime) currentLeg.ScheduleDTTMLocal;
                            deparrSpan = inDateTM.Subtract(scheduleDateTM);
                            arrivalDTTM = departDTTM.Add(deparrSpan);
                        }
                    }
                    if (departDTTM == null || departDTTM == DateTime.MinValue)
                    {
                        departDTTM = scheduleDateTM;
                        arrivalDTTM = inDateTM;
                    }
                    tbFlightCost = POCalculateFleetCost(objAircraft, departDTTM, arrivalDTTM, tbBlockHours, tbFlightHours, hdnMiles, tbStMiles).ToString();

                    if (UserPrincipal._IsAutoFillAF == true && UserPrincipal._AircraftBlockFlight == POAircraftBasis.BlockTime)
                    {
                        tbAirFrameHours = tbBlockHours;

                        tbEngineers1Hr = tbBlockHours;
                        tbEngineers2Hr = tbBlockHours;
                        tbEngineers3Hr = tbBlockHours;
                        tbEngineers4Hr = tbBlockHours;

                        tbAirFramecycles = 1;
                        tbEngineers1Cycle= 1;
                        tbEngineers2Cycle = 1;
                        tbEngineers3Cycle = 1;
                        tbEngineers4Cycle = 1;
                    }
                    else if (UserPrincipal._IsAutoFillAF == true && UserPrincipal._AircraftBlockFlight == POAircraftBasis.FlightTime)
                    {
                        tbAirFrameHours = tbFlightHours;

                        tbEngineers1Hr = tbFlightHours;
                        tbEngineers2Hr = tbFlightHours;
                        tbEngineers3Hr = tbFlightHours;
                        tbEngineers4Hr = tbFlightHours;

                        tbAirFramecycles = 1;
                        tbEngineers1Cycle = 1;
                        tbEngineers2Cycle = 1;
                        tbEngineers3Cycle = 1;
                        tbEngineers4Cycle = 1;
                    }
                }

                RetObject["DutyHoursStyle"] = DutyHoursCal["DutyHoursStyle"];
                if (pfViewModel != null && pfViewModel.PostflightLegs != null && pfViewModel.PostflightLegs.Count > 0)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(DutyHoursCal["FAR"])) == false)
                        currentLeg.FedAviationRegNum = Convert.ToString(DutyHoursCal["FAR"]);

                    currentLeg.BeginningDutyHours = Convert.ToString(DutyHoursCal["DutyBegin"]);
                    currentLeg.EndDutyHours = Convert.ToString(DutyHoursCal["DutyEnd"]);

                    currentLeg.BlockHoursTime = tbBlockHours;
                    currentLeg.FlightHoursTime = tbFlightHours;
                    currentLeg.DutyHrsTime = tbDutyHours;

                    currentLeg.FlightCost = string.IsNullOrEmpty(tbFlightCost) ? 0 : Convert.ToDecimal(tbFlightCost);

                    if (UserPrincipal._IsAutoFillAF == true)
                    {
                        currentLeg.AirFrameHoursTime = string.IsNullOrEmpty(tbAirFrameHours) ? currentLeg.AirFrameHoursTime : tbAirFrameHours;
                        currentLeg.Engine1HoursTime = string.IsNullOrEmpty(tbEngineers1Hr) ? currentLeg.Engine1HoursTime : tbEngineers1Hr;
                        currentLeg.Engine2HoursTime = string.IsNullOrEmpty(tbEngineers2Hr) ? currentLeg.Engine2HoursTime : tbEngineers2Hr;
                        currentLeg.Engine3HoursTime = string.IsNullOrEmpty(tbEngineers3Hr) ? currentLeg.Engine3HoursTime : tbEngineers3Hr;
                        currentLeg.Engine4HoursTime = string.IsNullOrEmpty(tbEngineers4Hr) ? currentLeg.Engine4HoursTime : tbEngineers4Hr;

                        currentLeg.AirframeCycle = tbAirFramecycles;
                        currentLeg.Engine1Cycle = tbEngineers1Cycle;
                        currentLeg.Engine2Cycle = tbEngineers2Cycle;
                        currentLeg.Engine3Cycle = tbEngineers3Cycle;
                        currentLeg.Engine4Cycle = tbEngineers4Cycle;
                    }
                }

                HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                RetObject["Leg"] = currentLeg;
                RetObj.Result = RetObject;
                RetObj.StatusCode = HttpStatusCode.OK;
                RetObj.Success = true;
                return RetObj;
            }
        }

        public static Hashtable PODutyHourCalculation(int LegNUM, DateTime vScheduledTM, DateTime vOutboundDTTM, DateTime vInboundDTTM, UserPrincipalViewModel UserPrincipal, PostflightLogViewModel pfViewModel, IDBCatalogsDataLoader iDataLoader)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Hashtable RetObject = new Hashtable();
                decimal TotalHours = 0;
                DateTime CHECKStartTime = DateTime.MinValue;
                DateTime CHECKEndTime = DateTime.MinValue;
                DateTime StartTime = CHECKStartTime = DateTime.MinValue;
                DateTime EndTime = CHECKEndTime = DateTime.MinValue;
                if (pfViewModel != null && pfViewModel.PostflightLegs != null && pfViewModel.PostflightLegs.Count > 0)
                {
                    double dutyBegin = 0;
                    double dutyEnd = 0;
                    double maxDutyHrs = 0;
                    double dutyStartEnd = 0;

                    PostflightLegViewModel currentLeg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.LegNUM == LegNUM && x.IsDeleted == false && x.State != TripEntityState.Deleted);
                    PostflightLegViewModel PreviousLeg = new PostflightLegViewModel();

                    if (currentLeg != null)
                    {
                        if (currentLeg.CrewCurrency != null && !string.IsNullOrEmpty(currentLeg.CrewCurrency))
                        {
                            var objCrewDutyRule = iDataLoader.GetCrewDutyRulesInfo_FromFilter_I(0,currentLeg.CrewCurrency.Trim());
                            if (objCrewDutyRule != null && objCrewDutyRule.CrewDutyRulesID > 0)
                            {
                                dutyBegin = Convert.ToDouble(objCrewDutyRule.DutyDayBeingTM);
                                dutyEnd = Convert.ToDouble(objCrewDutyRule.DutyDayEndTM);
                                maxDutyHrs = Convert.ToDouble(objCrewDutyRule.MaximumDutyHrs);

                                if (currentLeg.IsDutyEnd == true && !string.IsNullOrEmpty(objCrewDutyRule.FedAviatRegNum))
                                {
                                    RetObject["FAR"] = objCrewDutyRule.FedAviatRegNum.Trim();
                                }

                                if (pfViewModel.PostflightLegs.Count == 1 || pfViewModel.PostflightLegs.Count == LegNUM || currentLeg.IsDutyEnd == true)
                                    dutyStartEnd = dutyBegin + dutyEnd;
                                else
                                    dutyStartEnd = dutyBegin;
                            }
                        }

                        if ((pfViewModel.PostflightLegs.Count == 1) || ((currentLeg.LegNUM == 1)))
                        {
                            if (UserPrincipal._DutyBasis != null && UserPrincipal._DutyBasis == WebConstants.POCrewDutyBasis.ScheduledDep) // No Out Date Display
                            {
                                StartTime = Convert.ToDateTime(currentLeg.ScheduledTM ?? vScheduledTM);
                            }
                            else
                            {
                                StartTime = Convert.ToDateTime(currentLeg.OutboundDTTM ?? vOutboundDTTM);
                            }
                            EndTime = Convert.ToDateTime(currentLeg.InboundDTTM ?? vInboundDTTM);
                            TimeSpan DutyBeginTime = TimeSpan.Zero;
                            TimeSpan DutyHours = TimeSpan.Zero;

                            if (dutyStartEnd != 0)
                            {
                                DutyHours = ConvertDoubleToTimeSpan(dutyStartEnd);
                            }
                            if (StartTime != CHECKStartTime && EndTime != CHECKEndTime && StartTime < EndTime)
                            {
                                DutyHours = EndTime.Subtract(StartTime) + DutyHours;
                            }
                            else
                            {
                                DutyHours = TimeSpan.Zero;
                            }
                            if (maxDutyHrs < DutyHours.TotalHours)
                            {
                                RetObject["DutyHoursStyle"] = "red";
                            }
                            else
                            {
                                RetObject["DutyHoursStyle"] =  "gray";
                            }
                            TotalHours = Convert.ToDecimal(DutyHours.TotalHours);
                        }
                        else
                        {
                            string StartingFARRule = null;
                            string EndingFARRule = null;

                            //string MaxEndingFARRule = null;
                            // Finding the End Time
                            if (currentLeg.InboundDTTM != null)
                            {
                                EndTime = Convert.ToDateTime(currentLeg.InboundDTTM);
                                EndingFARRule = currentLeg.CrewCurrency;
                            }

                            // Yet To calculate Duty Hours
                            if (Convert.ToInt32(LegNUM) > 1)
                            {
                                PreviousLeg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.LegNUM == LegNUM - 1 && x.IsDeleted == false && x.State != TripEntityState.Deleted);
                            }

                            //Finding the Start time
                            //Block Time is checked (Company Profile)
                            if (UserPrincipal._DutyBasis != null && UserPrincipal._DutyBasis == WebConstants.POCrewDutyBasis.ScheduledDep) // No Out Date Display
                            {
                                if (currentLeg.ScheduledTM != null)
                                {
                                    StartTime = Convert.ToDateTime(currentLeg.ScheduledTM);
                                }

                                StartingFARRule = currentLeg.CrewCurrency;
                                //Block Time is checked (Company Profile) 
                                for (int i = (Convert.ToInt32(LegNUM) - 1); i >= 1; i--)
                                {
                                    PostflightLegViewModel PrevLeg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.LegNUM == i && x.IsDeleted == false && x.State != TripEntityState.Deleted);
                                    if (PrevLeg != null)
                                    {
                                        if (PrevLeg.LegNUM > 0 && PrevLeg.IsDutyEnd != true)
                                        {
                                            StartTime = Convert.ToDateTime(PrevLeg.ScheduledTM);
                                            if (PrevLeg.LegNUM == 1)
                                            {
                                                StartingFARRule = PrevLeg.CrewCurrency;
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //Schedule Time is checked (Company Profile)
                                if (currentLeg.OutboundDTTM != null)
                                {
                                    StartTime = Convert.ToDateTime(currentLeg.OutboundDTTM);
                                }
                                StartingFARRule = currentLeg.CrewCurrency;
                                //Schedule Time is checked (Company Profile)

                                for (int i = (Convert.ToInt32(LegNUM) - 1); i >= 1; i--)
                                {
                                    PostflightLegViewModel PrevLeg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.LegNUM == i && x.IsDeleted == false && x.State != TripEntityState.Deleted);
                                    if (PrevLeg != null && PrevLeg.LegNUM > 0 && PrevLeg.IsDutyEnd != true)
                                    {
                                        if (PrevLeg.OutboundDTTM != null && (!string.IsNullOrEmpty(PrevLeg.OutboundDTTM.ToString())))
                                        {
                                            StartTime = Convert.ToDateTime(PrevLeg.OutboundDTTM);
                                            if (PrevLeg.LegNUM == 1)
                                            {
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }

                            // Finding the Duty Begin and Finding the Duty End of Crew Rules
                            if (!string.IsNullOrEmpty(StartingFARRule))
                            {
                                var objCrewDutyRule = iDataLoader.GetCrewDutyRulesInfo_FromFilter_I(0, StartingFARRule.Trim());
                                if (objCrewDutyRule != null && objCrewDutyRule.CrewDutyRulesID > 0)
                                {
                                    if (objCrewDutyRule.DutyDayBeingTM != null)
                                        dutyBegin = Convert.ToDouble(objCrewDutyRule.DutyDayBeingTM);
                                }
                            }
                            if (!string.IsNullOrEmpty(EndingFARRule))
                            {
                                var objCrewDutyRule = iDataLoader .GetCrewDutyRulesInfo_FromFilter_I(0, EndingFARRule.Trim());
                                if (objCrewDutyRule != null && objCrewDutyRule.CrewDutyRulesID > 0)
                                {
                                    if (objCrewDutyRule.DutyDayEndTM != null)
                                        dutyEnd = Convert.ToDouble(objCrewDutyRule.DutyDayEndTM);
                                    if (objCrewDutyRule.MaximumDutyHrs != null)
                                        maxDutyHrs = Convert.ToDouble(objCrewDutyRule.MaximumDutyHrs);

                                    if (currentLeg.IsDutyEnd == true && !string.IsNullOrEmpty(objCrewDutyRule.FedAviatRegNum))
                                        RetObject["FAR"] = objCrewDutyRule.FedAviatRegNum.Trim();
                                }
                            }

                            TimeSpan DutyBeginTime = TimeSpan.Zero;
                            TimeSpan DutyHours = TimeSpan.Zero;

                            //Calculating the Duty Hours
                            if (dutyStartEnd != 0)
                            {
                                DutyHours = ConvertDoubleToTimeSpan(dutyStartEnd);
                            }

                            //Calculating Duty Hours without Rules
                            if (StartTime != CHECKStartTime && EndTime != CHECKEndTime && StartTime < EndTime)
                            {
                                DutyHours = EndTime.Subtract(StartTime) + DutyHours;
                            }
                            else
                            {
                                DutyHours = TimeSpan.Zero;
                            }
                            TotalHours = Convert.ToDecimal(DutyHours.TotalHours);
                            if (maxDutyHrs < DutyHours.TotalHours)
                            {
                                RetObject["DutyHoursStyle"] = "red";
                            }
                            else
                            {
                                RetObject["DutyHoursStyle"] =  "gray";
                            }
                        }
                    }

                    RetObject["DutyBegin"] = ConvertDoubleToTimeSpan(dutyBegin, 0).ToString();
                    RetObject["DutyEnd"] = ConvertDoubleToTimeSpan(dutyEnd, 0).ToString();
                    RetObject["TotalHours"] = FPKConversionHelper.ConvertTenthsToMins(Convert.ToString(TotalHours));
                }
                return RetObject;
            }
        }

        private static decimal POCalculateFleetCost(AircraftViewModel objAircraft, DateTime? departDTTM, DateTime? arrivalDTTM, string tbBlockHours, string tbFlightHours, string hdnMiles, string tbStMiles)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(departDTTM, arrivalDTTM))
            {
                UserPrincipalViewModel UserPrincipal = getUserPrincipal();
                decimal fleetCost = 0.0M;
                decimal blockHour = 0;
                decimal miles = 0;
                decimal stMiles = 0;
                decimal chargeRate = 0.0M;
                string chargeUnit = string.Empty;

                // Fix for #2334
                string Hours = "0";
                if (UserPrincipal != null)
                {
                    if (UserPrincipal._AircraftBasis != null && UserPrincipal._AircraftBasis == 1)
                    {
                        if (!string.IsNullOrEmpty(tbBlockHours))
                            Hours = tbBlockHours;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(tbFlightHours))
                            Hours = tbFlightHours;
                    }
                }


                if (UserPrincipal != null)
                {
                    if (UserPrincipal._TimeDisplayTenMin == POTenthMinuteFormat.Minute)
                    {
                        if (!string.IsNullOrEmpty(Hours))
                            blockHour = Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(Hours, false, (int)UserPrincipal._HoursMinutesCONV));
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Hours))
                            blockHour = Convert.ToDecimal(Hours);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(Hours))
                        blockHour = Convert.ToDecimal(Hours);
                }

                if (!string.IsNullOrEmpty(hdnMiles))
                {
                    miles = Convert.ToDecimal(hdnMiles);
                }

                if (!string.IsNullOrEmpty(tbStMiles))
                {
                    stMiles = Convert.ToDecimal(tbStMiles);
                }

                PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
                pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

                if (pfViewModel != null && pfViewModel.PostflightMain != null && pfViewModel.PostflightMain.FleetID != null && pfViewModel.PostflightMain.FleetID != 0)
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient MasterClient = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetval = MasterClient.GetFleetChargeRateList((long)pfViewModel.PostflightMain.FleetID);
                        if (departDTTM != null)
                        {
                            if (objRetval.ReturnFlag)
                            {
                                List<FlightPakMasterService.FleetChargeRate> FleetchargeRate = (from Fleetchrrate in objRetval.EntityList
                                                                                                where (Fleetchrrate.BeginRateDT <= arrivalDTTM
                                                                                                && Fleetchrrate.EndRateDT >= departDTTM)
                                                                                                where Fleetchrrate.FleetID == pfViewModel.PostflightMain.FleetID
                                                                                                select Fleetchrrate).ToList();
                                if (FleetchargeRate.Count > 0)
                                {
                                    chargeRate = FleetchargeRate[0].ChargeRate == null ? 0.0M : (decimal)FleetchargeRate[0].ChargeRate;
                                    chargeUnit = FleetchargeRate[0].ChargeUnit;
                                }
                            }
                        }
                    }
                }

                if (chargeRate == 0.0M && objAircraft != null)
                {
                    chargeRate = objAircraft.ChargeRate == null ? 0.0M : (decimal)objAircraft.ChargeRate;
                    chargeUnit = objAircraft.ChargeUnit;
                }

                if (!string.IsNullOrWhiteSpace(chargeUnit) && chargeUnit.ToUpper() == "H")
                {
                    fleetCost = blockHour * chargeRate;
                }
                else if (!string.IsNullOrWhiteSpace(chargeUnit) && chargeUnit.ToUpper() == "N")
                {
                    fleetCost = miles * chargeRate;
                }
                else if (!string.IsNullOrWhiteSpace(chargeUnit) && chargeUnit.ToUpper() == "S")
                {
                    fleetCost = Math.Floor(miles * 1.1508M) * chargeRate;
                }
                else if (!string.IsNullOrWhiteSpace(chargeUnit) && chargeUnit.ToUpper() == "K")
                {
                    fleetCost = Math.Floor(miles * 1.852M) * chargeRate;
                }

                return Math.Round(fleetCost, 2);
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GMT_Validate_Retrieve(string date, string time, string Dateformat, string airportid, bool isGMT)
        {
            Dictionary<string, string> RetObj = new Dictionary<string, string>();
            DateTime ldGmtDep = new DateTime();
            if (!string.IsNullOrEmpty(airportid))
            {
                string[] TimeArray = time.Split(':');
                string _hour = "00";
                string _minute = "00";
                if (!string.IsNullOrWhiteSpace(TimeArray[0]))
                    _hour = TimeArray[0];
                if (!string.IsNullOrWhiteSpace(TimeArray[1]))
                    _minute = TimeArray[1];

                int StartHrs = Convert.ToInt16(_hour);
                int StartMts = Convert.ToInt16(_minute);

                RetObj["time"] = MiscUtils.CalculationTimeAdjustment(_hour + ":" + _minute);

                DateTime? dt = new DateTime();
                dt = dt.FSSParseDateTime(date, Dateformat);
                dt = dt.Value.AddHours(StartHrs);
                dt = dt.Value.AddMinutes(StartMts);

                using (CalculationService.CalculationServiceClient ObjCalculation = new CalculationService.CalculationServiceClient())
                {
                    ldGmtDep = ObjCalculation.GetGMT(Convert.ToInt64(airportid), dt.Value, true, isGMT);
                }

                string UtcDate = String.Format(CultureInfo.InvariantCulture, "{0:" + Dateformat + "}", ldGmtDep);
                string UtcTime = String.Format("{0:00}:{0:00}", ldGmtDep.Hour, ldGmtDep.Minute);
                RetObj.Add("UtcDate", UtcDate);
                RetObj.Add("UtcTime", UtcTime);
            }
            return JsonConvert.SerializeObject(RetObj);
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<CrewDutyRuleViewModel> POCrewDutyRule_Validate_Retrieve(long CrewDutyRulesID, string CrewDutyRuleCD)
        {
            FSSOperationResult<CrewDutyRuleViewModel> RetObj = new FSSOperationResult<CrewDutyRuleViewModel>();
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();
            if (RetObj.IsAuthorized() == false)
            {
                return RetObj;
            }

            CrewDutyRuleViewModel objDutyRule = DBCatalogsDataLoader.GetCrewDutyRulesInfo_FromFilter(CrewDutyRulesID, CrewDutyRuleCD);
            if (objDutyRule != null && objDutyRule.CrewDutyRulesID != 0)
            {
                objDutyRule.CrewDutyRuleCD = objDutyRule.CrewDutyRuleCD.Trim();
                RetObj.Result = objDutyRule;
                RetObj.StatusCode = HttpStatusCode.OK;
                RetObj.Success = true;
                return RetObj;
            }
            else
            {
                RetObj.Result = null;
                RetObj.StatusCode = HttpStatusCode.OK;
                RetObj.Success = false;
                RetObj.ErrorsList.Add(WebErrorConstants.CrewDutyRuleCodeNotExist);
                return RetObj;
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<FlightCatagoryViewModel> POCategory_Validate_Retrieve(string CategoryCD, string LegNum)
        {
            FSSOperationResult<FlightCatagoryViewModel> RetObj = new FSSOperationResult<FlightCatagoryViewModel>();
            Hashtable RetObject = new Hashtable();
            Hashtable DutyHoursCal = new Hashtable();
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();
            if (UserPrincipal == null)
            {
                RetObj.Result = null;
                RetObj.StatusCode = HttpStatusCode.Unauthorized;
                RetObj.Success = false;
                RetObj.ErrorsList.Add("User not logged in");
                return RetObj;
            }

            List<FlightPak.Web.FlightPakMasterService.GetAllFlightCategoryWithFilters> Category = new List<FlightPak.Web.FlightPakMasterService.GetAllFlightCategoryWithFilters>();
            FlightCatagoryViewModel objflightCategory = new FlightCatagoryViewModel();

            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetAllFlightCategoryWithFilters(0, 0, CategoryCD.ToUpper().Trim(), true);
                if (objRetVal.ReturnFlag)
                {
                    Category = objRetVal.EntityList;
                    if (Category != null && Category.Count > 0)
                    {
                        objflightCategory = (FlightCatagoryViewModel)objflightCategory.InjectFrom(Category[0]);
                        RetObj.Result = objflightCategory;
                        RetObj.StatusCode = HttpStatusCode.OK;
                        RetObj.Success = true;
                    }
                    else
                    {
                        RetObj.Result = null;
                        RetObj.StatusCode = HttpStatusCode.OK;
                        RetObj.Success = false;
                        RetObj.ErrorsList.Add("Category Does Not Exist");
                    }
                }
                else
                {
                    RetObj.Result = null;
                    RetObj.StatusCode = HttpStatusCode.OK;
                    RetObj.Success = false;
                    RetObj.ErrorsList.Add("Category Does Not Exist");
                }
            }
            return RetObj;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightLegViewModel> POEndofDuty_Check_Validate_Retrieve(int LegNum, string blockHours, string flightHours, string dutyHours)
        {
            FSSOperationResult<PostflightLegViewModel> RetObj = new FSSOperationResult<PostflightLegViewModel>();
            Hashtable RetObject = new Hashtable();
            Hashtable DutyHoursCal = new Hashtable();
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();
            if (!RetObj.IsAuthorized())
            {
                return RetObj;
            }

            PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
            pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            PostflightLegViewModel Leg = new PostflightLegViewModel();
            Leg = pfViewModel.PostflightLegs.Where(l => l.IsDeleted == false && l.LegNUM == Convert.ToInt64(LegNum)).FirstOrDefault();

            DateTime vScheduledTM = (DateTime)Leg.ScheduledTM;
            DateTime vOutboundDTTM = (DateTime)Leg.OutboundDTTM;
            DateTime vInboundDTTM = (DateTime)Leg.InboundDTTM;

            //SavePostflightToSession(null, null);
            DBCatalogsDataLoader db= new DBCatalogsDataLoader();
            DutyHoursCal = PODutyHourCalculation(LegNum, vScheduledTM, vOutboundDTTM, vInboundDTTM, UserPrincipal, pfViewModel, db);
            if (UserPrincipal._TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
            {
                RetObject["DutyHours"] = FPKConversionHelper.ConvertMinToTenths(DutyHoursCal["TotalHours"].ToString(), true, (int)UserPrincipal._HoursMinutesCONV).ToString();
            }
            else
            {
                RetObject["DutyHours"] = DutyHoursCal["TotalHours"].ToString();
            }
            SetCrewDutyHours(LegNum, blockHours, flightHours, dutyHours);

            ReCalculateLegDutyHour();
            if (pfViewModel.PostflightLegs != null)
            {
                PostflightLegViewModel currLeg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.LegNUM == Convert.ToInt32(LegNum) && x.IsDeleted == false);
                RetObj.Result = currLeg;
            }

            ApplyRONValue(pfViewModel);

            RetObj.StatusCode = HttpStatusCode.OK;
            RetObj.Success = true;
            return RetObj;

        }

        #region Internal Methos

        internal static void SetCrewDutyHours(int LegNUM, string blockHours, string flightHours, string dutyHours)
        {
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
            {
                UserPrincipalViewModel UserPrincipal = getUserPrincipal();
                PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
                pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

                if (pfViewModel != null && pfViewModel.PostflightLegs != null && pfViewModel.PostflightLegs.Count(x => x.IsDeleted == false) > 0 && LegNUM > 0)
                {
                    PostflightLegViewModel SelectLeg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.IsDeleted == false && x.LegNUM == LegNUM);
                    PostflightLegViewModel PreviousLeg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.IsDeleted == false && x.LegNUM == (LegNUM - 1));
                    PostflightLegViewModel NextLeg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.IsDeleted == false && x.LegNUM == (LegNUM + 1));

                    if (SelectLeg != null && SelectLeg.PostflightLegCrews != null && SelectLeg.PostflightLegCrews.Count(x => x.IsDeleted == false) > 0)
                    {
                        foreach (PostflightLegCrewViewModel SelectCrew in SelectLeg.PostflightLegCrews.Where(x => x.IsDeleted == false))
                        {
                            decimal blockTime = 0;
                            decimal flightTime = 0;
                            decimal dutyTime = 0;
                            // check for Augmented
                            if (SelectCrew.IsAugmentCrew != null && SelectCrew.IsAugmentCrew != true)
                            {
                                if (UserPrincipal != null)
                                {
                                    if (UserPrincipal._TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
                                    {
                                        if (!string.IsNullOrEmpty(blockHours))
                                            blockTime = Convert.ToDecimal(blockHours);
                                        if (!string.IsNullOrEmpty(flightHours))
                                            flightTime = Convert.ToDecimal(flightHours);
                                        if (!string.IsNullOrEmpty(dutyHours))
                                            dutyTime = Convert.ToDecimal(dutyHours);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(blockHours))
                                            blockTime = System.Math.Round(Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(blockHours, true, (int)UserPrincipal._HoursMinutesCONV).ToString()), 3);
                                        if (!string.IsNullOrEmpty(flightHours))
                                            flightTime = System.Math.Round(Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(flightHours, true, (int)UserPrincipal._HoursMinutesCONV).ToString()), 3);
                                        if (!string.IsNullOrEmpty(dutyHours))
                                            dutyTime = Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(dutyHours, true, (int)UserPrincipal._HoursMinutesCONV).ToString());
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(blockHours))
                                        blockTime = System.Math.Round(Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(blockHours, true, (int)UserPrincipal._HoursMinutesCONV).ToString()), 3);
                                    if (!string.IsNullOrEmpty(flightHours))
                                        flightTime = System.Math.Round(Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(flightHours, true, (int)UserPrincipal._HoursMinutesCONV).ToString()), 3);
                                    if (!string.IsNullOrEmpty(dutyHours))
                                        dutyTime = Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(dutyHours, true, (int)UserPrincipal._HoursMinutesCONV).ToString());
                                }

                                SelectCrew.BlockHours = blockTime;
                                SelectCrew.FlightHours = flightTime;
                            }

                            // check for Duty Hours
                            if (SelectCrew.IsOverride != null && SelectCrew.IsOverride != true)
                            {
                                TimeSpan dutyBegin = new TimeSpan(0);
                                if (SelectLeg.BeginningDutyHours != null && (!string.IsNullOrEmpty(SelectLeg.BeginningDutyHours.ToString().Trim())))
                                    dutyBegin = TimeSpan.ParseExact(SelectLeg.BeginningDutyHours.ToString(), "c", null);

                                TimeSpan schTime = new TimeSpan(0);
                                if (SelectLeg.ScheduledTM != null)
                                    schTime = SelectLeg.ScheduledTM.Value.TimeOfDay;

                                string dutybeginhrs = "00:00";
                                DateTime dutyBeginDT = DateTime.MinValue;
                                if (PreviousLeg != null)
                                {
                                    if (PreviousLeg.IsDutyEnd == null && PreviousLeg.IsDutyEnd == false)
                                        dutyBegin = TimeSpan.Zero;
                                }

                                if (UserPrincipal._DutyBasis != null && UserPrincipal._DutyBasis == WebConstants.POCrewDutyBasis.ScheduledDep) // No Out Date Display
                                {
                                    if ((dutyBegin != null) && (SelectLeg.ScheduledTM != null))
                                    {
                                        dutyBeginDT = SelectLeg.ScheduledTM.Value.Subtract(dutyBegin);
                                        dutybeginhrs = dutyBeginDT.TimeOfDay.ToString();
                                    }
                                }
                                else
                                {
                                    if ((dutyBegin != null) && (SelectLeg.OutboundDTTM != null))
                                    {
                                        dutyBeginDT = SelectLeg.OutboundDTTM.Value.Subtract(dutyBegin);
                                        dutybeginhrs = dutyBeginDT.TimeOfDay.ToString();
                                    }
                                }

                                double DutyEndFAR = 0;
                                if (!string.IsNullOrEmpty(SelectLeg.CrewCurrency))
                                {
                                    var objCrewDutyRule = DBCatalogsDataLoader.GetCrewDutyRulesInfo_FromFilter(0, SelectLeg.CrewCurrency.Trim());
                                    if (objCrewDutyRule != null && objCrewDutyRule.CrewDutyRulesID > 0)
                                    {
                                        DutyEndFAR = Convert.ToDouble(objCrewDutyRule.DutyDayEndTM);
                                    }
                                }

                                TimeSpan EndTime = new TimeSpan(0);

                                if (SelectLeg.InboundDTTM != null)
                                    EndTime = SelectLeg.InboundDTTM.Value.TimeOfDay;

                                string dutyEndinghrs = "00:00";

                                DateTime dutyEndDT = DateTime.MinValue;

                                TimeSpan dutyEnd = TimeSpan.Zero;
                                if (DutyEndFAR != 0)
                                {
                                    double hours = Math.Floor(DutyEndFAR);
                                    double minutes = (DutyEndFAR - hours) * 60.0;

                                    Int32 actualHours = (Int32)Math.Floor(hours);
                                    Int32 actualMinutes = (Int32)Math.Floor(minutes);

                                    TimeSpan nTime = new TimeSpan(actualHours, actualMinutes, 0);

                                    dutyEnd = nTime;
                                }
                                if (dutyEnd != null && SelectLeg.InboundDTTM != null)
                                {
                                    dutyEndDT = SelectLeg.InboundDTTM.Value.Add(dutyEnd);
                                    dutyEndinghrs = dutyEndDT.TimeOfDay.ToString();
                                }
                                SelectCrew.CrewDutyStartTM = dutyBeginDT;
                                SelectCrew.CrewDutyEndTM = dutyEndDT;
                            }
                            if (SelectCrew.IsRemainOverNightOverride != null && SelectCrew.IsRemainOverNightOverride != true)
                            {
                                long lastleg = pfViewModel.PostflightLegs.Select(t => t.LegNUM.Value).Max();
                                if (SelectLeg.LegNUM.Value == lastleg)
                                {
                                    ApplyRONValue(pfViewModel);
                                }
                            }
                        }
                    }
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                if (pfViewModel.PostflightLegs != null)
                {
                    foreach (PostflightLegViewModel Leg in pfViewModel.PostflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM))
                    {
                        if (Leg.PostflightLegCrews != null)
                        {
                            foreach (PostflightLegCrewViewModel Crew in Leg.PostflightLegCrews.Where(x => x.IsDeleted == false))
                            {
                                if (Crew.IsOverride != null && Crew.IsOverride != true)
                                {
                                    Crew.DutyHours = DutyCrew(Convert.ToInt64(Crew.CrewID), Convert.ToInt32(Leg.LegNUM));
                                }
                            }
                        }
                    }
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;

                //Calculate Crew Duty Hour
                ReCalculateCrewDutyHour();
            }
        }

        internal static decimal DutyCrew(Int64 CrewId, int LegNum)
        {
            DateTime CrewDutyStartTime = DateTime.MinValue;
            DateTime CrewDutyEndTime = DateTime.MinValue;
            decimal DutyHours = 0;
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();
            PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
            pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

            if (pfViewModel != null && pfViewModel.PostflightLegs != null && pfViewModel.PostflightLegs.Count > 0)
            {
                PostflightLegViewModel leg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.LegNUM == LegNum && x.IsDeleted == false);
                if (leg != null && leg.PostflightLegCrews != null)
                {
                    PostflightLegCrewViewModel crew = leg.PostflightLegCrews.FirstOrDefault(x => x.CrewID == CrewId && x.IsDeleted == false);

                    if (crew != null)
                    {
                        if (crew.CrewDutyStartTM != null && crew.CrewDutyStartTM.HasValue)
                            CrewDutyStartTime = Convert.ToDateTime(crew.CrewDutyStartTM);

                        if (crew.CrewDutyEndTM != null && crew.CrewDutyEndTM.HasValue)
                            CrewDutyEndTime = Convert.ToDateTime(crew.CrewDutyEndTM);
                    }

                    for (int i = (LegNum - 1); i > 0; i--)
                    {
                        PostflightLegViewModel prevleg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.LegNUM == i && x.IsDeleted == false);

                        if (prevleg != null)
                        {
                            if (prevleg.PostflightLegCrews != null)
                            {
                                PostflightLegCrewViewModel prevlegCrew = prevleg.PostflightLegCrews.FirstOrDefault(x => x.IsDeleted == false && x.CrewID == CrewId);
                                bool Overrride = false;
                                if (prevlegCrew != null)
                                    Overrride = Convert.ToBoolean(prevlegCrew.IsOverride != null ? prevlegCrew.IsOverride : false);

                                if (prevleg.IsDutyEnd != true && Overrride != true)
                                {
                                    if (prevlegCrew != null && (prevlegCrew.CrewDutyStartTM != null && prevlegCrew.CrewDutyStartTM.HasValue))
                                    {
                                        CrewDutyStartTime = Convert.ToDateTime(prevlegCrew.CrewDutyStartTM);
                                    }
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            if (CrewDutyEndTime != DateTime.MinValue && CrewDutyStartTime != DateTime.MinValue)
                DutyHours = System.Math.Round(Convert.ToDecimal(CrewDutyEndTime.Subtract(CrewDutyStartTime).TotalHours), 2);

            if (DutyHours < 0)
                DutyHours = 0;

            return DutyHours;
        }

        internal static void ApplyRONValue(PostflightLogViewModel tripLog)
        {
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();
            PostflightCrewLite pfCrewLite=new PostflightCrewLite();
            foreach (var tripLegs in tripLog.PostflightLegs.Where(tt => tt.IsDeleted == false))
            {
                long LegNum = tripLegs.LegNUM.Value;
                if (tripLegs.PostflightLegCrews != null)
                {
                    foreach (var tripLegCrew in tripLegs.PostflightLegCrews.Where(c => c.IsDeleted == false).ToList())
                    {
                        if (tripLegCrew.IsRemainOverNightOverride != null && tripLegCrew.IsRemainOverNightOverride != true)
                        {
                            tripLegCrew.RemainOverNight = pfCrewLite.RONCalculation(Convert.ToInt64(tripLegCrew.CrewID), LegNum, UserPrincipal, tripLog);
                        }
                    }
                }
            }
        }

        internal static void ReCalculateCrewDutyHour()
        {
            decimal dutyHours = 0;
            TimeSpan dutyBegin = new TimeSpan(0);
            TimeSpan schTime = new TimeSpan(0);
            string dutybeginhrs = "00:00";
            DateTime? dutyBeginDT = null;
            TimeSpan EndTime = new TimeSpan(0);
            string dutyEndinghrs = "00:00";
            DateTime? dutyEndDT = null;
            DateTime? dutyBeginRONDT = null;
            TimeSpan dutyEnd = TimeSpan.Zero;
            TimeSpan DutyHours = TimeSpan.Zero;

            List<PostflightLegCrewViewModel> CurrentCrewLegList = new List<PostflightLegCrewViewModel>();
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();

            PostflightLogViewModel pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            //null check
            if (pfViewModel == null || pfViewModel.PostflightLegs == null)
                return;

            int LegCount = pfViewModel.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted).ToList().Count;

            for (int cnt = 1; cnt <= LegCount; cnt++)
            {
                PostflightLegViewModel currentLeg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.LegNUM == cnt && x.IsDeleted == false && x.State != TripEntityState.Deleted);
                PostflightLegViewModel PreviousLeg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.LegNUM == cnt - 1 && x.IsDeleted == false && x.State != TripEntityState.Deleted);
                if (currentLeg != null)
                {
                    if (currentLeg.PostflightLegCrews != null && currentLeg.PostflightLegCrews.Count > 0)
                    {
                        //PostflightLegCrewViewModel currentLegCrew = currentLeg.PostflightLegCrews.FirstOrDefault(x => x.IsDeleted == false && x.State != TripEntityState.Deleted);

                        double DutyEndFAR = 0, DutyBeginFAR = 0;
                        if (!string.IsNullOrWhiteSpace(currentLeg.CrewCurrency))
                        {
                            var RulesVal = DBCatalogsDataLoader.GetCrewDutyRulesInfo_FromFilter(0, currentLeg.CrewCurrency.Trim());
                            if (RulesVal != null)
                            {
                                if (RulesVal.DutyDayEndTM != null)
                                    DutyEndFAR = Convert.ToDouble(RulesVal.DutyDayEndTM);

                                if (RulesVal.DutyDayBeingTM != null)
                                    DutyBeginFAR = Convert.ToDouble(RulesVal.DutyDayBeingTM);
                            }
                        }

                        //DutyBeginning
                        if (!string.IsNullOrWhiteSpace(currentLeg.BeginningDutyHours))
                            //if (currentLeg.BeginningDutyHours != null && (!string.IsNullOrEmpty(currentLeg.BeginningDutyHours.ToString().Trim())))
                            dutyBegin = TimeSpan.ParseExact(currentLeg.BeginningDutyHours, "c", null);

                        if (DutyBeginFAR != 0)
                        {
                            double hours = Math.Floor(DutyBeginFAR);
                            double minutes = (DutyBeginFAR - hours) * 60.0;

                            Int32 actualHours = (Int32)Math.Floor(hours);
                            Int32 actualMinutes = (Int32)Math.Floor(minutes);

                            TimeSpan nTime = new TimeSpan(actualHours, actualMinutes, 0);
                            dutyBegin = nTime;
                        }

                        if (DutyEndFAR != 0)
                        {
                            double hours = Math.Floor(DutyEndFAR);
                            double minutes = (DutyEndFAR - hours) * 60.0;

                            Int32 actualHours = (Int32)Math.Floor(hours);
                            Int32 actualMinutes = (Int32)Math.Floor(minutes);

                            TimeSpan nTime = new TimeSpan(actualHours, actualMinutes, 0);
                            dutyEnd = nTime;
                        }


                        foreach (PostflightLegCrewViewModel selectedCrew in currentLeg.PostflightLegCrews.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted))
                        {
                            if (string.IsNullOrWhiteSpace(selectedCrew.CrewCode) && selectedCrew.CrewID != null)
                            {
                                var objRetVal = selectedCrew.CrewID.HasValue ? DBCatalogsDataLoader.GetCrewInfo_FromCrewId((long)selectedCrew.CrewID) : new CrewViewModel();
                                if (objRetVal != null && !string.IsNullOrWhiteSpace(objRetVal.CrewCD))
                                    selectedCrew.CrewCode = objRetVal.CrewCD.ToUpper();
                            }
                            if (selectedCrew.IsOverride != true)
                            {
                                //Begin Hours
                                bool isDutyBeginAdd = false;
                                bool isSchedTM = false;
                                if (UserPrincipal._DutyBasis != null && UserPrincipal._DutyBasis == WebConstants.POCrewDutyBasis.ScheduledDep && currentLeg.ScheduledTM != null)
                                    isSchedTM = true;

                                if (isSchedTM || currentLeg.OutboundDTTM != null)
                                {
                                    if (PreviousLeg != null)
                                    {
                                        for (int i = (Convert.ToInt32(currentLeg.LegNUM) - 1); i >= 1; i--)
                                        {
                                            PostflightLegViewModel PrevLeg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.LegNUM == i && x.IsDeleted == false && x.State != TripEntityState.Deleted);
                                            PostflightLegViewModel NxtLeg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.LegNUM == i + 1 && x.IsDeleted == false && x.State != TripEntityState.Deleted);// Added By Jajati

                                            if (PrevLeg != null && PrevLeg.PostflightLegCrews != null)
                                            {
                                                PostflightLegCrewViewModel prevlegCrew = PrevLeg.PostflightLegCrews.FirstOrDefault(x => x.IsDeleted == false && x.CrewID == selectedCrew.CrewID);

                                                if (prevlegCrew != null && PrevLeg.LegNUM > 0 && PrevLeg.IsDutyEnd != true)
                                                {
                                                    if (prevlegCrew.IsOverride == true)
                                                    {
                                                        string dtCrew = "", dtLeg = "";
                                                        
                                                        dtCrew = prevlegCrew.CrewDutyStartTM.ToString();
                                                        if (isSchedTM)
                                                            dtLeg = PrevLeg.ScheduledTM.Value.Subtract(dutyBegin).ToString();
                                                        else
                                                            dtLeg = PrevLeg.OutboundDTTM.Value.Subtract(dutyBegin).ToString();

                                                        string dt = Convert.ToDateTime(dtLeg).Date.ToString(UserPrincipal._ApplicationDateFormat);
                                                        var time = Convert.ToDateTime(dtCrew).ToString("H:mm:ss");

                                                        dutyBeginDT = DateTime.ParseExact(dt + " " + time, UserPrincipal._ApplicationDateFormat + " H:mm:ss", CultureInfo.InvariantCulture);
                                                        dutybeginhrs = dutyBeginDT.Value.TimeOfDay.ToString();
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        if (isSchedTM)
                                                        {
                                                            if (PrevLeg.ScheduledTM.HasValue)
                                                                dutyBeginDT = PrevLeg.ScheduledTM.Value.Subtract(dutyBegin);
                                                        }
                                                        else
                                                        {
                                                            if (PrevLeg.OutboundDTTM.HasValue)
                                                                dutyBeginDT = PrevLeg.OutboundDTTM.Value.Subtract(dutyBegin);
                                                        }

                                                        if(dutyBeginDT.HasValue)
                                                        dutybeginhrs = dutyBeginDT.Value.TimeOfDay.ToString();
                                                        if (PrevLeg.LegNUM == 1)
                                                        {
                                                            break;
                                                        }
                                                    }

                                                }
                                                else // In case Crew null in Previous Leg
                                                {
                                                    if (isSchedTM)
                                                    {
                                                        if (NxtLeg.ScheduledTM.HasValue)
                                                        dutyBeginDT = NxtLeg.ScheduledTM.Value.Subtract(dutyBegin);
                                                    }
                                                    else
                                                    {
                                                        if (NxtLeg.OutboundDTTM.HasValue)
                                                        dutyBeginDT = NxtLeg.OutboundDTTM.Value.Subtract(dutyBegin);
                                                    }
                                                    if (dutyBeginDT.HasValue)
                                                    dutybeginhrs = dutyBeginDT.Value.TimeOfDay.ToString();

                                                    if (prevlegCrew == null || prevlegCrew.CrewID == 0)
                                                        isDutyBeginAdd = true;

                                                    if (PreviousLeg != null && PreviousLeg.PostflightLegCrews != null)
                                                    {
                                                        foreach (PostflightLegCrewViewModel previewsPOCrew in PreviousLeg.PostflightLegCrews)
                                                        {
                                                            if (previewsPOCrew.CrewID == selectedCrew.CrewID)
                                                                isDutyBeginAdd = false;
                                                        }
                                                    }

                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if (isSchedTM)
                                                {
                                                    if (currentLeg.ScheduledTM.HasValue)
                                                        dutyBeginDT = currentLeg.ScheduledTM.Value.Subtract(dutyBegin);
                                                }
                                                else
                                                {
                                                    if (currentLeg.OutboundDTTM.HasValue)
                                                    dutyBeginDT = currentLeg.OutboundDTTM.Value.Subtract(dutyBegin);
                                                }
                                                if (dutyBeginDT.HasValue)
                                                dutybeginhrs = dutyBeginDT.Value.TimeOfDay.ToString();
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (isSchedTM)
                                        {
                                            if (currentLeg.ScheduledTM.HasValue)
                                                dutyBeginDT = currentLeg.ScheduledTM.Value.Subtract(dutyBegin);
                                        }
                                        else
                                        {
                                            if (currentLeg.OutboundDTTM.HasValue)
                                                dutyBeginDT = currentLeg.OutboundDTTM.Value.Subtract(dutyBegin);
                                        }
                                        if (dutyBeginDT.HasValue)
                                        dutybeginhrs = dutyBeginDT.Value.TimeOfDay.ToString();
                                    }
                                }

                                bool isDutyEndAdd = false;
                                //End Hours
                                if (currentLeg.InboundDTTM.HasValue)
                                {
                                    PostflightLegViewModel NxtLeg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.LegNUM == currentLeg.LegNUM + 1 && x.IsDeleted == false);// Added By Jajati

                                    if (NxtLeg != null && NxtLeg.PostflightLegCrews != null && NxtLeg.PostflightLegCrews.Count > 0 && (currentLeg.IsDutyEnd == null || currentLeg.IsDutyEnd == false) )
                                    {

                                        PostflightLegCrewViewModel nextlegCrew = NxtLeg.PostflightLegCrews.FirstOrDefault(x => x.IsDeleted == false && x.CrewID == selectedCrew.CrewID);

                                        if (nextlegCrew != null)
                                        {
                                            dutyEndDT = currentLeg.InboundDTTM;
                                            if (dutyEndDT.HasValue)
                                            dutyEndinghrs = dutyEndDT.Value.TimeOfDay.ToString();
                                        }
                                        else
                                        {
                                            dutyEndDT = currentLeg.InboundDTTM.Value.Add(dutyEnd);
                                            if (dutyEndDT.HasValue)
                                            dutyEndinghrs = dutyEndDT.Value.TimeOfDay.ToString();
                                            isDutyEndAdd = true;
                                        }

                                    }
                                    else
                                    {
                                        dutyEndDT = currentLeg.InboundDTTM.Value.Add(dutyEnd);
                                        if (dutyEndDT.HasValue)
                                        dutyEndinghrs = dutyEndDT.Value.TimeOfDay.ToString();
                                    }

                                    dutyBeginRONDT = currentLeg.InboundDTTM.Value;
                                }

                                if ((dutyBeginDT.HasValue) && (dutyEndDT.HasValue))
                                    DutyHours = dutyEndDT.Value.Subtract(dutyBeginDT.Value);
                                else
                                    DutyHours = TimeSpan.Zero;

                                if (selectedCrew.BlockHours == null || selectedCrew.BlockHours == 0 || selectedCrew.IsAugmentCrew !=true)
                                    selectedCrew.BlockHours = (decimal)currentLeg.BlockHours;
                                if (selectedCrew.FlightHours == null || selectedCrew.FlightHours == 0 || selectedCrew.IsAugmentCrew != true)
                                    selectedCrew.FlightHours = (decimal)currentLeg.FlightHours;
                                dutyHours = Convert.ToDecimal(DutyHours.TotalHours);

                                string res = "", result = "";
                                result = FPKConversionHelper.ConvertTenthsToMins(Convert.ToString(dutyHours));
                                string dutyHrs = "0";
                                if (UserPrincipal._TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
                                {
                                    res = FPKConversionHelper.ConvertMinToTenths(result, true, (int)UserPrincipal._HoursMinutesCONV);
                                    dutyHrs = res;
                                }
                                else
                                    dutyHrs = Convert.ToString(dutyHours);

                                if (currentLeg.LegNUM == 1 || (PreviousLeg != null && PreviousLeg.IsDutyEnd != null && PreviousLeg.IsDutyEnd == true) || isDutyBeginAdd == true || (selectedCrew.IsOverride != true && selectedCrew.CrewDutyStartTM!=null))
                                    selectedCrew.CrewDutyStartTM = dutyBeginDT;

                                if (currentLeg.LegNUM == LegCount || (currentLeg.IsDutyEnd != null && currentLeg.IsDutyEnd == true) || isDutyEndAdd == true || (selectedCrew.IsOverride != true && selectedCrew.CrewDutyEndTM!=null))
                                    selectedCrew.CrewDutyEndTM = dutyEndDT;

                                if (dutyEndDT < dutyBeginDT)
                                 selectedCrew.DutyHrsTime = "0";
                                else
                                    selectedCrew.DutyHrsTime = dutyHrs; 
                            

                            }
                            selectedCrew.POLegID = currentLeg.POLegID;
                            

                            if (pfViewModel.POLogID == 0)
                                selectedCrew.State = TripEntityState.Added;

                            else if (pfViewModel.POLogID > 0)
                                if (selectedCrew.State != TripEntityState.Added)
                                {
                                    selectedCrew.State = TripEntityState.Modified;
                                }

                        }
                    }
                }//End
            }
            //SaveIntoSession(pfViewModel);
            HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
        }

        internal static void ReCalculateLegDutyHour()
        {
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
            {
                UserPrincipalViewModel UserPrincipal = getUserPrincipal();
                PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
                pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

                if (pfViewModel.POLogID == 0)
                    pfViewModel.PostflightMain.State = TripEntityState.Added;
                else if (pfViewModel.POLogID > 0)
                    pfViewModel.PostflightMain.State = TripEntityState.Modified;

                // Scheduled
                DateTime? scheduleDateTM = null;
                DateTime? outDateTM = null;
                DateTime? inDateTM = null;

                // Get and Set Time Fields
                string outTime = "00:00";
                string inTime = "00:00";

                decimal? blockTime = 0;
                decimal? flightTime = 0;
                decimal? dutyTime = 0;
                int LegCount = 0;

                if (pfViewModel.PostflightLegs == null)
                    return;

                LegCount = pfViewModel.PostflightLegs.Where(x => x.IsDeleted == false).ToList().Count;
                var polegs = pfViewModel.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted).OrderBy(p=>p.LegNUM).ToList();
                foreach (var poLegItem in polegs)
                {
                    PostflightLegViewModel currentLeg = poLegItem;
                    
                    PostflightLegViewModel PreviousLeg = new PostflightLegViewModel();

                    scheduleDateTM = currentLeg.ScheduledTM;
                    inDateTM = currentLeg.InboundDTTM;
                    outDateTM = currentLeg.OutboundDTTM;

                    outTime = currentLeg.BlockOut;
                    inTime = currentLeg.BlockIN;
                    blockTime = currentLeg.BlockHours;
                    flightTime = currentLeg.FlightHours;
                    dutyTime = currentLeg.DutyHrs;

                    string result = "0";
                    decimal TotalHours = 0;
                    DateTime CHECKStartTime = DateTime.MinValue;
                    DateTime CHECKEndTime = DateTime.MinValue;
                    DateTime StartTime = CHECKStartTime = DateTime.MinValue;
                    DateTime EndTime = CHECKEndTime = DateTime.MinValue;

                    PostflightLogViewModel Trip = new PostflightLogViewModel();
                    Trip = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

                    if (pfViewModel != null && pfViewModel.PostflightLegs != null && LegCount > 0)
                    {

                        double dutyBegin = 0;
                        double dutyEnd = 0;
                        double maxDutyHrs = 0;
                        double dutyStartEnd = 0;

                        if (currentLeg != null)
                        {
                            if (currentLeg.CrewCurrency != null && !string.IsNullOrEmpty(currentLeg.CrewCurrency))
                            {
                                var objCrewDutyRule = DBCatalogsDataLoader.GetCrewDutyRulesInfo_FromFilter(0, currentLeg.CrewCurrency.Trim());
                                if (objCrewDutyRule != null && objCrewDutyRule.CrewDutyRulesID > 0)
                                {
                                    dutyBegin = Convert.ToDouble(objCrewDutyRule.DutyDayBeingTM);
                                    dutyEnd = Convert.ToDouble(objCrewDutyRule.DutyDayEndTM);
                                    maxDutyHrs = Convert.ToDouble(objCrewDutyRule.MaximumDutyHrs);
                                    if (((LegCount == 1 || currentLeg.IsDutyEnd == true)) || LegCount == currentLeg.LegNUM)
                                        dutyStartEnd = dutyBegin + dutyEnd;
                                    else
                                        dutyStartEnd = dutyBegin;
                                }
                            }
                            if ((LegCount == 1) || ((currentLeg.LegNUM == 1)))
                            {

                                if (UserPrincipal._DutyBasis != null && UserPrincipal._DutyBasis == WebConstants.POCrewDutyBasis.ScheduledDep) // No Out Date Display
                                {
                                    StartTime = Convert.ToDateTime(currentLeg.ScheduledTM);
                                }
                                else
                                {
                                    StartTime = Convert.ToDateTime(currentLeg.OutboundDTTM);
                                }
                                EndTime = Convert.ToDateTime(currentLeg.InboundDTTM);
                                TimeSpan DutyBeginTime = TimeSpan.Zero;
                                TimeSpan DutyHours = TimeSpan.Zero;

                                if (dutyStartEnd != 0)
                                {
                                    DutyHours = ConvertDoubleToTimeSpan(dutyStartEnd);
                                }
                                if (StartTime != CHECKStartTime && EndTime != CHECKEndTime && StartTime < EndTime)
                                {
                                    DutyHours = EndTime.Subtract(StartTime) + DutyHours;
                                }
                                else
                                {
                                    DutyHours = TimeSpan.Zero;
                                }
                                TotalHours = Convert.ToDecimal(DutyHours.TotalHours);
                            }
                            else
                            {
                                string StartingFARRule = null;
                                string EndingFARRule = null;

                                // Finding the End Time
                                if (currentLeg.InboundDTTM > currentLeg.OutboundDTTM)
                                {
                                    if (currentLeg.InboundDTTM != null && currentLeg.InboundDTTM != DateTime.MinValue)
                                    {
                                        EndTime = Convert.ToDateTime(currentLeg.InboundDTTM);
                                        EndingFARRule = currentLeg.CrewCurrency;
                                    }
                                }
                                else
                                {
                                    EndTime = DateTime.MinValue;
                                }

                                // Yet To calculate Duty Hours
                                if (poLegItem.LegNUM > 1)
                                {
                                    PreviousLeg = Trip.PostflightLegs.FirstOrDefault(x => x.LegNUM == poLegItem.LegNUM - 1 && x.IsDeleted == false);
                                }

                                //Finding the Start time
                                //Block Time is checked (Company Profile)
                                if (UserPrincipal._DutyBasis != null && UserPrincipal._DutyBasis == WebConstants.POCrewDutyBasis.ScheduledDep) // No Out Date Display
                                {
                                    if (currentLeg.ScheduledTM != null)
                                    {
                                        StartTime = Convert.ToDateTime(currentLeg.ScheduledTM);
                                    }

                                    StartingFARRule = currentLeg.CrewCurrency;
                                    //Block Time is checked (Company Profile) 
                                    for (int i = (Convert.ToInt32(poLegItem.LegNUM.Value) - 1); i >= 1; i--)
                                    {
                                        PostflightLegViewModel PrevLeg = Trip.PostflightLegs.FirstOrDefault(x => x.LegNUM == i && x.IsDeleted == false);

                                        if (PrevLeg != null)
                                        {
                                            if (PrevLeg.LegNUM > 0 && PrevLeg.IsDutyEnd != true)
                                            {
                                                StartTime = Convert.ToDateTime(PrevLeg.ScheduledTM);
                                                if (PrevLeg.LegNUM == 1)
                                                {
                                                    StartingFARRule = PrevLeg.CrewCurrency;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //Schedule Time is checked (Company Profile)
                                    if (currentLeg.OutboundDTTM != null)
                                    {
                                        StartTime = Convert.ToDateTime(currentLeg.OutboundDTTM);
                                    }
                                    StartingFARRule = currentLeg.CrewCurrency;
                                    //Schedule Time is checked (Company Profile)

                                    for (int i = (Convert.ToInt32(poLegItem.LegNUM) - 1); i >= 1; i--)
                                    {
                                        PostflightLegViewModel PrevLeg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.LegNUM == i && x.IsDeleted == false);

                                        if (PrevLeg != null && PrevLeg.LegNUM > 0 && PrevLeg.IsDutyEnd != true)
                                        {
                                            if (PrevLeg.OutboundDTTM != null && (!string.IsNullOrEmpty(PrevLeg.OutboundDTTM.ToString())))
                                            {
                                                StartTime = Convert.ToDateTime(PrevLeg.OutboundDTTM);
                                                if (PrevLeg.LegNUM == 1)
                                                {
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }

                                // Finding the Duty Begin and Finding the Duty End of Crew Rules
                                if (!string.IsNullOrEmpty(StartingFARRule))
                                {
                                    var objCrewDutyRule = DBCatalogsDataLoader.GetCrewDutyRulesInfo_FromFilter(0, StartingFARRule.Trim());
                                    if (objCrewDutyRule != null && objCrewDutyRule.CrewDutyRulesID > 0)
                                    {
                                        if (objCrewDutyRule.DutyDayBeingTM != null && objCrewDutyRule.DutyDayBeingTM.HasValue)
                                            dutyBegin = Convert.ToDouble(objCrewDutyRule.DutyDayBeingTM);
                                    }

                                }
                                if (!string.IsNullOrEmpty(EndingFARRule))
                                {
                                    var objCrewDutyRule = DBCatalogsDataLoader.GetCrewDutyRulesInfo_FromFilter(0, EndingFARRule.Trim());
                                    if (objCrewDutyRule != null && objCrewDutyRule.CrewDutyRulesID > 0)
                                    {
                                        if (objCrewDutyRule.DutyDayEndTM != null && objCrewDutyRule.DutyDayEndTM.HasValue)
                                            dutyEnd = Convert.ToDouble(objCrewDutyRule.DutyDayEndTM);
                                        if (objCrewDutyRule.MaximumDutyHrs != null && objCrewDutyRule.MaximumDutyHrs.HasValue)
                                            maxDutyHrs = Convert.ToDouble(objCrewDutyRule.MaximumDutyHrs);
                                    }
                                }

                                TimeSpan DutyBeginTime = TimeSpan.Zero;
                                TimeSpan DutyHours = TimeSpan.Zero;

                                //Calculating the Duty Hours
                                if (dutyStartEnd != 0)
                                {
                                    DutyHours = ConvertDoubleToTimeSpan(dutyStartEnd);
                                }

                                //Calculating Duty Hours without Rules
                                if (StartTime != CHECKStartTime && EndTime != CHECKEndTime && StartTime < EndTime)
                                {
                                    DutyHours = EndTime.Subtract(StartTime) + DutyHours;
                                }
                                else
                                {
                                    DutyHours = TimeSpan.Zero;
                                }
                                TotalHours = Convert.ToDecimal(DutyHours.TotalHours);
                            }
                        }
                        result = FPKConversionHelper.ConvertTenthsToMins(Convert.ToString(TotalHours));

                        string res = "";
                        if (UserPrincipal._TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
                        {
                            res = FPKConversionHelper.ConvertMinToTenths(result, true, (int)UserPrincipal._HoursMinutesCONV);
                            currentLeg.DutyHrs = Convert.ToDecimal(res);
                        }
                        else
                        {
                            res = result;
                            currentLeg.DutyHrs = TotalHours;
                        }
                    }
                    if (currentLeg.State != TripEntityState.Added)
                        currentLeg.State = TripEntityState.Modified;

                    if (pfViewModel.PostflightLegs != null && LegCount > 0)
                    {
                        if (pfViewModel.PostflightLegs.IndexOf(currentLeg) < 0)
                        {
                            if (currentLeg.State == TripEntityState.Added)
                            {
                                pfViewModel.PostflightLegs.Add(currentLeg);
                            }
                        }
                    }
                    else
                    {
                        currentLeg.LegNUM = 1;
                        pfViewModel.PostflightLegs.Add(currentLeg);
                    }
                    //SaveIntoSession(pfViewModel);
                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                }//End loop.  
            }
        }

        #endregion

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PayableVendorViewModel> PayableVendor_Validate_Retrieve_VM(string VendorCD)
        {
            FSSOperationResult<PayableVendorViewModel> result = new FSSOperationResult<PayableVendorViewModel>();
            if (!result.IsAuthorized())
            {
                return result;
            }
            result.Success = true;
            result.StatusCode = System.Net.HttpStatusCode.OK;
            result.Result = DBCatalogsDataLoader.GetPayableVendorInfo_FromVendorCD(VendorCD);
            result.Success = result.Result != null && result.Result.VendorID != 0;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PaymentTypeViewModel> PaymentType_Validate_Retrieve_VM(string PaymentCD)
        {
            FSSOperationResult<PaymentTypeViewModel> result = new FSSOperationResult<PaymentTypeViewModel>();
            if (!result.IsAuthorized())
            {
                return result;
            }
            result.Success = true;
            result.StatusCode = System.Net.HttpStatusCode.OK;
            result.Result = DBCatalogsDataLoader.GetPaymentTypeInfo_FromPaymentTypeCD(PaymentCD);
            result.Success = result.Result != null && result.Result.PaymentTypeID != 0;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<CrewViewModel> Crew_Validate_Retrieve_VM(string CrewCD)
        {
            FSSOperationResult<CrewViewModel> result = new FSSOperationResult<CrewViewModel>();
            if (!result.IsAuthorized())
            {
                return result;
            }
            result.Success = true;
            result.StatusCode = System.Net.HttpStatusCode.OK;
            result.Result = DBCatalogsDataLoader.GetCrewInfo_FromCrewCD(CrewCD);
            result.ResultList = new List<CrewViewModel> { result.Result };
            result.Success = result.Result != null && result.Result.CrewID != 0;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<FlightCatagoryViewModel> FlightCatagory_Validate_Retrieve_VM(string FlightCatagoryCD)
        {
            FSSOperationResult<FlightCatagoryViewModel> result = new FSSOperationResult<FlightCatagoryViewModel>();
            UserPrincipalViewModel userPrincipal = TripManagerBase.getUserPrincipal();
            if (!result.IsAuthorized())
            {
                return result;
            }
            result.Success = true;
            result.StatusCode = System.Net.HttpStatusCode.OK;
            result.Result = DBCatalogsDataLoader.GetFlightCategoryInfo_FromCD(FlightCatagoryCD);
            result.Success = result.Result != null && result.Result.FlightCategoryID != 0;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<FuelLocatorViewModel> FuelLocator_Validate_Retrieve_VM(string FuelLocatorCD)
        {
            FSSOperationResult<FuelLocatorViewModel> result = new FSSOperationResult<FuelLocatorViewModel>();
            UserPrincipalViewModel userPrincipal = TripManagerBase.getUserPrincipal();
            if (!result.IsAuthorized())
            {
                return result;
            }
            result.Success = true;
            result.StatusCode = System.Net.HttpStatusCode.OK;
            result.Result = DBCatalogsDataLoader.GetFuelLocatorInfo_FromLocatorCD(FuelLocatorCD);
            result.Success = result.Result != null && result.Result.FuelLocatorID != 0;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightLegPassengerViewModel> Requestor_Validate_Retrieve_VM(string PassengerRequestorCD)
        {
            FSSOperationResult<PostflightLegPassengerViewModel> Resultdata = new FSSOperationResult<PostflightLegPassengerViewModel>();
            if (Resultdata.IsAuthorized() == false)
            {
                return Resultdata;
            }
            try
            {
                Resultdata.Success = true;
                Resultdata.StatusCode = HttpStatusCode.OK;
                var paxVM=DBCatalogsDataLoader.GetPassengerReqestorInfo_FromPassengerReqCD(PassengerRequestorCD);
                if (paxVM.PassengerRequestorID == 0)
                {
                    Resultdata.Success = false;
                }
                Resultdata.Result = paxVM;
                
            }
            catch (Exception ex)
            {
                Resultdata.Success = false;
                Resultdata.StatusCode = HttpStatusCode.OK;
            }
            return Resultdata;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> TailNum_PostflightValidate_Retrieve_KO(string TailNumber)
        {
            Hashtable hashResult = new Hashtable();
            FSSOperationResult<Hashtable> Resultdata = new FSSOperationResult<Hashtable>();
            if (Resultdata.IsAuthorized() == false)
            {
                return Resultdata;
            }
            Resultdata.Result = hashResult;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    Resultdata.StatusCode = HttpStatusCode.OK;
                    Resultdata.Result = hashResult;
                    FleetViewModel fleetVM = new FleetViewModel();
                    hashResult["ErrorTailNumber"] = string.Empty;
                    if (!string.IsNullOrEmpty(TailNumber))
                    {
                        HomebaseViewModel homebaseVM = new HomebaseViewModel();
                        hashResult["homebase"] = homebaseVM;

                        using (FlightPakMasterService.MasterCatalogServiceClient TailService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var TailVal = TailService.GetAllFleetWithFilters(0, false, "B", string.Empty, 0, TailNumber, 0).EntityList;
                            if (TailVal != null && TailVal.Count > 0)
                            {
                                hashResult["AircraftID"] = TailVal[0].AircraftID.Value.ToString();                                
                                fleetVM = (FleetViewModel)fleetVM.InjectFrom(TailVal[0]);
                                fleetVM.AircraftID = TailVal[0].AircraftID.Value;
                                fleetVM.AircraftCD = TailVal[0].AirCraft_AircraftCD;
                                hashResult["Fleet"] = fleetVM;

                                hashResult["IsCalculateLeg"] = true;


                                //if (!string.IsNullOrEmpty(hashResult["TailNumber"].ToString()))
                                //    ControlEnable("Button", btnTailNoChargeHistory, true, "charter-rate-button");
                                //else
                                //    ControlEnable("Button", btnTailNoChargeHistory, false, "charter-rate-disable-button");

                                //Bug 2910 - #5428
                                if (TailVal[0].HomebaseID != null && TailVal[0].HomebaseID > 0)
                                {
                                    Int64 HomebaseID = (long)TailVal[0].HomebaseID;
                                    hashResult["HomebaseID"] = TailVal[0].HomebaseID.ToString();

                                    using (FlightPakMasterService.MasterCatalogServiceClient ObjmasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var objCompanyRetVal = DBCatalogsDataLoader.GetHomebaseInfo_FromHomebaseId(HomebaseID);
                                        
                                        if (objCompanyRetVal!=null && objCompanyRetVal.HomebaseID>0)
                                        {                                            
                                                homebaseVM = (HomebaseViewModel)homebaseVM.InjectFrom(objCompanyRetVal);
                                                hashResult["homebase"] = homebaseVM;

                                                #region check firstleg and if details are not filled replace dep leg
                                                var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                                                if (TripLog.PostflightLegs != null && TripLog.PostflightLegs.Count() > 0)
                                                {
                                                    PostflightLegViewModel firstleg = TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.LegNUM == 1 && (x.ArriveICAOID == null || x.ArriveICAOID == 0)).SingleOrDefault();
                                                    if (firstleg != null)
                                                    {
                                                        firstleg = PostflightTripManager.SetLegUserPrincipalSettings(firstleg);
                                                        if (objCompanyRetVal != null)
                                                        {
                                                            var HomebaseAirport = DBCatalogsDataLoader.GetAirportInfo_FromAirportId((long)objCompanyRetVal.HomebaseAirportID);
                                                            if (HomebaseAirport != null)
                                                            {
                                                                if (firstleg.ArriveICAOID == null || firstleg.ArriveICAOID <= 0)
                                                                {
                                                                    firstleg.DepartureAirport = (AirportViewModel)firstleg.DepartureAirport.InjectFrom(HomebaseAirport);
                                                                    firstleg.DepartICAOID = HomebaseAirport.AirportID;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion                                            
                                        }
                                    }
                                }
                                else
                                {
                                    Resultdata.Success = true;
                                    Resultdata.ErrorsList.Add(System.Web.HttpUtility.HtmlEncode("Homebase not found."));
                                
                                }
                            }
                            else
                            {
                                Resultdata.Success = false;
                                Resultdata.ErrorsList.Add(System.Web.HttpUtility.HtmlEncode("Invalid Aircraft Tail No."));
                                
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    Resultdata.Success = false;
                    Resultdata.Result = hashResult;
                    Resultdata.StatusCode = HttpStatusCode.OK;
                    return Resultdata;
                }
            }            
            Resultdata.StatusCode = HttpStatusCode.OK;
            Resultdata.Result = hashResult;
            return Resultdata;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightExpenseCatalogViewModel> LogNo_PostflightValidate_Retrieve_KO(long? LogNum)
        {
            PostflightExpenseCatalogViewModel ExpenseCatalogVM = new PostflightExpenseCatalogViewModel();
            FSSOperationResult<PostflightExpenseCatalogViewModel> ResultObj = new FSSOperationResult<PostflightExpenseCatalogViewModel>();
            UserPrincipalViewModel userPrincipal = TripManagerBase.getUserPrincipal();
            if (userPrincipal == null)
            {
                ResultObj.Success = false;
                ResultObj.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                ResultObj.ErrorsList.Add(WebErrorConstants.UnauthorizdOperation);
                return ResultObj;
            }
            using (PostflightServiceClient POService = new PostflightServiceClient())
            {
                PostflightMain trip = new PostflightMain();
                if (LogNum != null)
                {
                    trip.LogNum = LogNum;
                    var retTrip = POService.GetTrip(trip).EntityInfo;
                    if (retTrip != null && retTrip.POLogID > 0)
                    {
                        ExpenseCatalogVM.POLogID = retTrip.POLogID;
                        ExpenseCatalogVM.LogNum = retTrip.LogNum;
                        ExpenseCatalogVM.DispatchNUM = retTrip.DispatchNum;
                        foreach (var POleg in retTrip.PostflightLegs.OrderBy(u => u.LegNUM))
                        {
                            PostflightLegViewModel POlegVM = new PostflightLegViewModel();
                            POlegVM = (PostflightLegViewModel)POlegVM.InjectFrom(POleg);
                            POlegVM = PostflightTripManager.SetLegUserPrincipalSettings(POlegVM);
                            ExpenseCatalogVM.PostflightLeg.Add(POlegVM);
                        }
                        if (retTrip.PostflightLegs.Count() > 0)
                        {
                            var currentLeg = retTrip.PostflightLegs.OrderBy(u => u.LegNUM).FirstOrDefault();
                            ExpenseCatalogVM.Fleet = retTrip.Fleet != null ? (FleetViewModel)new FleetViewModel().InjectFrom(retTrip.Fleet) : new FleetViewModel();
                            ExpenseCatalogVM.Airport = DBCatalogsDataLoader.GetAirportInfo_FromICAO(currentLeg.Airport1.IcaoID);
                            if (currentLeg.DepatureFBO != null && currentLeg.FBO1 != null)
                            {
                                ExpenseCatalogVM.FBO.FBOCD = currentLeg.FBO1.FBOCD;
                                ExpenseCatalogVM.FBOID = currentLeg.DepatureFBO.Value;
                                if (currentLeg.FBO1.FBOVendor != null)
                                    ExpenseCatalogVM.FBO.FBOVendor = System.Web.HttpUtility.HtmlEncode(currentLeg.FBO1.FBOVendor.ToString().ToUpper());
                            }
                            else
                            {
                                if (currentLeg.Airport1 != null)
                                    ExpenseCatalogVM.FBO = DBCatalogsDataLoader.GetFBOChoice_ForAirportId(currentLeg.Airport1.AirportID);

                            }
                            if (!string.IsNullOrEmpty(currentLeg.InboundDTTM.ToString()) && currentLeg.InboundDTTM.HasValue)
                                ExpenseCatalogVM.PurchaseDT = currentLeg.InboundDTTM.Value;
                            else
                                ExpenseCatalogVM.PurchaseDT = DateTime.Today;

                            if (retTrip.PostflightCrews != null && retTrip.PostflightCrews.Count > 0)
                            {
                                PostflightCrew currentCrewPIC = retTrip.PostflightCrews.Where(o => o.IsDeleted == false && o.DutyTYPE == "P").FirstOrDefault();//.Find(crew =>crew.DutyTYPE == ddlLegs.SelectedValue);
                                if (currentCrewPIC != null)
                                {
                                    ExpenseCatalogVM.Crew = DBCatalogsDataLoader.GetCrewInfo_FromCrewId(currentCrewPIC.CrewID.Value);
                                }
                            }
                            ExpenseCatalogVM.FlightCategory = currentLeg.FlightCatagory != null ? (FlightCatagoryViewModel)new FlightCatagoryViewModel().InjectFrom(currentLeg.FlightCatagory) : new FlightCatagoryViewModel();
                        }
                        ResultObj.Success = true;
                        ResultObj.StatusCode = HttpStatusCode.OK;
                        ResultObj.Result = ExpenseCatalogVM;
                    }
                }
                else
                {
                    ResultObj.Success = false;
                    ResultObj.StatusCode = HttpStatusCode.OK;
                }
                return ResultObj;
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightLegPassengerViewModel> AssociatePassengerCD_Retrieve_KO(string PassengerCD)
        {
            FSSOperationResult<PostflightLegPassengerViewModel> ResultData = new FSSOperationResult<PostflightLegPassengerViewModel>();
            if (ResultData.IsAuthorized() == false)
            {
                return ResultData;
            }
            var pax = DBCatalogsDataLoader.GetPassengerReqestorInfo_FromPassengerReqCD(PassengerCD);
            if (pax != null && pax.PassengerRequestorID > 0 && !string.IsNullOrWhiteSpace(pax.IsEmployeeType) && pax.IsEmployeeType.Trim().ToUpper() != "G")
            {                
                ResultData.Result = pax;
                ResultData.StatusCode = HttpStatusCode.OK;
                ResultData.Success = true;
            }
            else
            {
                ResultData.StatusCode = HttpStatusCode.OK;
                ResultData.Success = false;
            }
            return ResultData;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<FleetViewModel> TailNum_Validate_Retrieve_VM(string TailNum)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<FleetViewModel>>(() =>
            {
                FSSOperationResult<FleetViewModel> ResultObj = new FSSOperationResult<FleetViewModel>();
                FleetViewModel fleetVM = new FleetViewModel();
                if (ResultObj.IsAuthorized() == false)
                {
                    return ResultObj;
                }
               
                using (FlightPakMasterService.MasterCatalogServiceClient TailService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var TailVal = TailService.GetAllFleetWithFilters(0, false, "B", string.Empty, 0, TailNum, 0).EntityList;
                    if (TailVal != null && TailVal.Count > 0)
                    {
                        fleetVM = (FleetViewModel)fleetVM.InjectFrom(TailVal[0]);

                        ResultObj.StatusCode = HttpStatusCode.OK;
                        ResultObj.Result = fleetVM;
                        ResultObj.Success = true;
                    }
                    else
                    {
                        ResultObj.StatusCode = HttpStatusCode.OK;
                        ResultObj.Success = false;
                    }
                }
                return ResultObj;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }
    }
}

