﻿using System;
using FlightPak.Web.ViewModels.Postflight;
using FlightPak.Web.ViewModels;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Transactions.PostFlight
{
    public partial class PostFlightSearchAll : BaseSecuredPage
    {
        private static ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            PostflightTripManager.CheckSessionAndRedirect();
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            exManager.Process(() =>
            {
                UserPrincipalViewModel upVM = PostflightTripManager.getUserPrincipal();
                PostflightMainViewModel poMainVM = new PostflightMainViewModel(upVM._ApplicationDateFormat);
                var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(poMainVM);
                hdnPOSearchLogMainInitializationSchemaObject.Value = Microsoft.Security.Application.Encoder.HtmlEncode(json);
            }, FlightPak.Common.Constants.Policy.UILayer);
        }
    }
}