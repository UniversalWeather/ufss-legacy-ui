﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.PostflightService;

//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Transactions.PostFlight
{
    public partial class PostflightSearchALLRPTPopup : BaseSecuredPage
    {
        private ExceptionManager exManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Request.QueryString["Postflight"] != null)
                        {
                            if (Request.QueryString["Postflight"].ToString() == "Reports")
                            {
                                dgSearch.AllowMultiRowSelection = true;
                            }
                            else
                            {
                                dgSearch.AllowMultiRowSelection = false;
                            }
                        }
                        else
                        {
                            dgSearch.AllowMultiRowSelection = false;
                        }

                        if (!IsPostBack)
                        {
                            lbMessage.Text = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POMain);
                }
            }
        }
        protected void dgPostflightMainDetails_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
                {
                    var ObjRetval = objService.GetPostflightTrips(0, 0, 0, false, false, DateTime.MinValue);
                    if (ObjRetval.ReturnFlag)
                    {
                        dgSearch.DataSource = ObjRetval.EntityList;
                    }
                }
            }
        }

        protected void dgSearch_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                switch (e.CommandName)
                {
                    case RadGrid.InitInsertCommandName:
                        e.Canceled = true;
                        InsertSelectedRow();
                        break;
                    case RadGrid.FilterCommandName:
                        Pair filterPair = (Pair)e.CommandArgument;
                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                        break;
                    default:
                        break;
                }
            }
        }

        protected void dgSearch_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                InsertSelectedRow();
            }
        }

        private void InsertSelectedRow()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
                {
                    PostflightMain obj = new PostflightMain();
                    GridDataItem item = (GridDataItem)dgSearch.SelectedItems[0];
                    if (Request.QueryString["Postflight"].ToString() == "Reports")
                    {

                        obj.POLogID = Convert.ToInt64(item["POLogID"].Text);
                        var result = objService.GetTrip(obj);
                        Session["POSTFLIGHTMAIN"] = (PostflightMain)result.EntityInfo;
                        RadAjaxManager1.ResponseScripts.Add(@"returntoparent('navigateToInserted');");
                    }
                    else
                    {
                        obj.POLogID = Convert.ToInt64(item["POLogID"].Text);
                        var result = objService.GetTrip(obj);
                        Session["POSTFLIGHTMAIN"] = (PostflightMain)result.EntityInfo;
                        
                        // Bind the Saved Exceptions into the Session
                        if (result.EntityInfo.PostflightTripExceptions != null && result.EntityInfo.PostflightTripExceptions.Count > 0)
                            Session["POEXCEPTION"] = result.EntityInfo.PostflightTripExceptions;

                        RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                    }
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (tbClientCode.Text == string.Empty && tbTailNumber.Text == string.Empty)
                        {
                            using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
                            {
                                var ObjRetval = objService.GetPostflightTrips(0, 0, 0, false, false, DateTime.MinValue);
                                dgSearch.DataSource = ObjRetval.EntityList;
                                dgSearch.DataBind();
                            }
                        }
                        else
                        {
                            using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
                            {
                                var ObjRetval = objService.GetPostflightTrips(0, 0, 0, false, false, DateTime.MinValue).EntityList.Where(obj => DelegateFunc(obj) == true).ToList();
                                dgSearch.DataSource = ObjRetval;
                                dgSearch.DataBind();
                            }
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POMain);
                }
            }
        }

        private bool DelegateFunc(GetPostflightList obj)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(obj))
            {

                bool retStatus = false;
                string tailNo = tbTailNumber.Text;
                if (obj.TailNum == tailNo)
                    retStatus = true;
                TextBox tbstartdate = (TextBox)ucstartDate.FindControl("Date");
                if (tbstartdate != null)
                {
                    DateTime strtdate = Convert.ToDateTime(tbstartdate.Text);
                    if (obj.EstDepartureDT == strtdate)
                        retStatus = true;
                }

                return retStatus;
            }
        }

        /// <summary>
        /// To display message for multiselect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgSearch_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (e.Item is GridCommandItem)
                {
                    GridCommandItem commandItem = e.Item as GridCommandItem;
                    HtmlGenericControl container = (HtmlGenericControl)commandItem.FindControl("divMultiSelect");

                    if (Request.QueryString["Postflight"] != null)
                    {
                        if (Request.QueryString["Postflight"].ToString() == "Reports")
                        {
                            container.Visible = true;
                        }
                        else
                        {
                            container.Visible = false;
                        }
                    }
                }
            }
        }

        protected void dgSearch_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgSearch, Page.Session);
        }

    }
}