﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PostFlightExpensesPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.PostFlight.PostFlightExpensesPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ MasterType VirtualPath="~/Framework/Masters/PostFlight.Master" %>
<%@ Register TagPrefix="uc" TagName="DatePicker" Src="~/UserControls/UCDate.ascx" %>
<%@ Register TagPrefix="uc" TagName="ucSearch" Src="~/UserControls/UCSeeAllPostflightMain.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Postflight Search All Expenses</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="radScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<%=ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript">

            // this function is used to get the dimensions
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            //this function is used to close this window
            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshPage(arg);
            }

            //this function is used to set the frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                return oWindow;
            }

            function RowDblClick() {
                var masterTable = $find("<%= dgSearch.ClientID %>").get_masterTableView();
                masterTable.fireCommand("InitInsert", "");
                return false;
            }

            //this function is used to navigate to pop up screen's with the selected code
            function openWin(radWin) {
                var url = '';
                if (radWin == "radFleetProfilePopup") {
                    url = '/Views/Settings/Fleet/FleetProfilePopup.aspx?TailNumber=' + document.getElementById('<%=tbTailNumber.ClientID%>').value + '&FromPage=postflight';
                }
                else if (radWin == "radAirportPopup") {
                    url = '/Views/Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbICAO.ClientID%>').value;
                }
                else if (radWin == "radPaymentTypePopup") {
                    url = '/Views/Settings/Company/PaymentTypePopup.aspx';
                }
                else if (radWin == "radPayableVendorPopup") {
                    url = '/Views/Settings/Logistics/PayableVendorPopUp.aspx';
                }
                else if (radWin == "radFuelLocatorPopup") {
                    url = '/Views/Settings/Logistics/FuelLocatorPopup.aspx';
                }
                else if (radWin == "radAccountMasterPopup") {
                    url = '/Views/Settings/Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById('<%=tbAccountNumber.ClientID%>').value;
                }
                else if (radWin == "radLogPopup") {
                    url = '/Views/Transactions/PostFlight/PostFlightSearchAll.aspx?IsPopup=YES';
                }

                var oWnd = radopen(url, radWin);
            }

            // this function is used to display the value of selected TailNumber from popup
            function OnClientTailNoClose(oWnd, args) {
                var combo = $find("<%= tbTailNumber.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbTailNumber.ClientID%>").value = arg.TailNum;
                        if (arg.FleetID != null)
                            document.getElementById("<%=hdnTailNo.ClientID%>").value = arg.FleetID;
                        if (arg.TailNum != null)
                            document.getElementById("<%=lbcvTailNumber.ClientID%>").innerHTML = "";

                        var step = "TailNo_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbTailNumber.ClientID%>").value = "";
                        document.getElementById("<%=hdnTailNo.ClientID%>").value = "";
                    }
                }
            }

            function OnAirportClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbICAO.ClientID%>").value = arg.ICAO;
                        document.getElementById("<%=hdnICAO.ClientID%>").value = arg.AirportID;
                        if (arg.AirportName != null && arg.AirportName != "&nbsp;")
                            document.getElementById("<%=lbICAODesc.ClientID%>").innerHTML = arg.AirportName;
                        else
                            document.getElementById("<%=lbICAODesc.ClientID%>").innerHTML = "";

                        if (arg.ICAO != null)
                            document.getElementById("<%=lbICAO.ClientID%>").innerHTML = "";

                        var step = "ICAO_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbICAO.ClientID%>").value = "";
                        document.getElementById("<%=lbICAODesc.ClientID%>").value = "";
                        document.getElementById("<%=lbICAO.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnICAO.ClientID%>").value = "";
                    }
                }
            }

            function OnPayTypeClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbPaymentType.ClientID%>").value = arg.PaymentTypeCD;
                        if (arg.PaymentTypeDescription != null && arg.PaymentTypeDescription != "&nbsp;")
                            document.getElementById("<%=lbPaymentTypeDesc.ClientID%>").innerHTML = arg.PaymentTypeDescription;
                        else
                            document.getElementById("<%=lbPaymentTypeDesc.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnPaymentType.ClientID%>").value = arg.PaymentTypeID;
                        if (arg.PaymentTypeCD != null)
                            document.getElementById("<%=lbPaymentType.ClientID%>").innerText = "";

                        var step = "PaymentType_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbPaymentType.ClientID%>").value = "";
                        document.getElementById("<%=lbPaymentTypeDesc.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnPaymentType.ClientID%>").value = "";
                        document.getElementById("<%=lbPaymentType.ClientID%>").innerText = "";
                    }
                }
            }

            function OnVendorPayClose(oWnd, args) {

                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbPayVendor.ClientID%>").value = arg.VendorCD;
                        document.getElementById("<%=hdnPayVendor.ClientID%>").value = arg.VendorID;
                        if (arg.Name != null)
                            document.getElementById("<%=lbPayVendorDesc.ClientID%>").innerHTML = arg.Name;
                        if (arg.VendorCD != null)
                            document.getElementById("<%=lbPayVendor.ClientID%>").innerHTML = "";

                        var step = "PayVendor_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbPayVendor.ClientID%>").value = "";
                        document.getElementById("<%=lbPayVendorDesc.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnPayVendor.ClientID%>").value = "";
                        document.getElementById("<%=lbPayVendor.ClientID%>").innerHTML = "";
                    }
                }
            }

            function OnFuelLocatorClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbFuelLocator.ClientID%>").value = arg.FuelLocatorCD;
                        document.getElementById("<%=hdnFuelLocator.ClientID%>").value = arg.FuelLocatorID;
                        if (arg.FuelLocatorCD != null)
                            document.getElementById("<%=lbFuelLocator.ClientID%>").innerHTML = "";

                        var step = "FuelLocator_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }

                    }
                    else {
                        document.getElementById("<%=tbFuelLocator.ClientID%>").value = "";
                        document.getElementById("<%=hdnFuelLocator.ClientID%>").value = "";
                        document.getElementById("<%=lbFuelLocator.ClientID%>").innerHTML = "";
                    }
                }
            }

            function OnClientAccountClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbAccountNumber.ClientID%>").value = arg.AccountNum;
                        document.getElementById("<%=hdnAccountNumber.ClientID%>").value = arg.AccountID;
                        if (arg.AccountDescription != null && arg.AccountDescription != "&nbsp;")
                            document.getElementById("<%=lbAccDescription.ClientID%>").innerHTML = arg.AccountDescription;
                        else
                            document.getElementById("<%=lbAccDescription.ClientID%>").innerHTML = "";

                        if (arg.AccountNum != null)
                            document.getElementById("<%=lbAccountNumber.ClientID%>").innerHTML = "";

                        var step = "AccountNumber_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbAccountNumber.ClientID%>").value = "";
                        document.getElementById("<%=lbAccDescription.ClientID%>").innerHTML = "";
                        document.getElementById("<%=lbAccountNumber.ClientID%>").innerHTML = "";
                    }
                }
            }

            function OnClientLogClose(oWnd, args) {
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbLogNo.ClientID%>").value = arg.LogNum;
                        document.getElementById("<%=hdnPOLogID.ClientID%>").value = arg.POLogID;

                        if (arg.LogNum != null)
                            document.getElementById("<%=lbLogNo.ClientID%>").innerHTML = "";

                        var step = "LogNo_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbLogNo.ClientID%>").value = "";
                        document.getElementById("<%=hdnPOLogID.ClientID%>").value = "";
                        document.getElementById("<%=lbLogNo.ClientID%>").innerHTML = "";
                    }
                }
            }


        </script>
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgSearch.ClientID %>");

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgSearch.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "POLogID");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "PostflightExpenseID");
                    var cell3 = MasterTable.getCellByColumnUniqueName(row, "SlipNUM");

                    if (i == 0) {
                        oArg.POLogID = cell1.innerHTML;
                        oArg.PostflightExpenseID = cell2.innerHTML;
                        oArg.SlipNUM = cell3.innerHTML;
                    }
                    else {
                        oArg.POLogID += "," + cell1.innerHTML;
                        oArg.PostflightExpenseID += "," + cell2.innerHTML;
                        oArg.SlipNUM += "," + cell3.innerHTML;
                    }
                }
                if (selectedRows.length == 0){
                    oArg.POLogID = "";
                    oArg.PostflightExpenseID = "";
                    oArg.SlipNUM = "";
                }

                oArg.Arg1 = oArg.SlipNUM;
                oArg.CallingButton = "SlipNUM";

                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbLogNo">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddlLegs" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true"
        ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="radFleetProfilePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientTailNoClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnAirportClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaymentTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnPayTypeClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/PaymentTypePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPayableVendorPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnVendorPayClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/PayableVendorPopUp.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFuelLocatorPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnFuelLocatorClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/FuelLocatorPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAccountMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientAccountClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radLogPopup" runat="server" Width="700px" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PostFlight/PostFlightSearchAll.aspx" OnClientClose="OnClientLogClose">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server">
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table cellspacing="0" cellpadding="0" class="box1">
            <tr>
                <td>
                    <table id="tblSearchForm" runat="server" border="0">
                        <tr>
                            <td valign="top">
                                <fieldset>
                                    <legend>Search</legend>
                                    <table border="0">
                                        <tr>
                                            <td valign="top" class="tdLabel200">
                                                <table border="0">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:CheckBox ID="chkHomebase" Text="Home Base Only" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdLabel50">
                                                            Go To
                                                        </td>
                                                        <td>
                                                            <uc:DatePicker ID="ucGoToDate" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:RadioButtonList ID="rblSearch" runat="server" RepeatDirection="Horizontal" RepeatColumns="2">
                                                                <asp:ListItem Value="0" Text="Include All" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Value="1" Text="Fuel Only"></asp:ListItem>
                                                                <asp:ListItem Value="2" Text="Exclude Fuel"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td valign="top">
                                                <table>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel50">
                                                                        Tail No.
                                                                    </td>
                                                                    <td class="tdLabel100">
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbTailNumber" CssClass="text60" runat="server" MaxLength="6" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                        OnTextChanged="TailNo_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                                    <asp:HiddenField ID="hdnTailNo" runat="server" />
                                                                                    <asp:HiddenField ID="hdnAircraftID" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Button ID="btnTailNo" OnClientClick="javascript:openWin('radFleetProfilePopup');return false;"
                                                                                        runat="server" CssClass="browse-button" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="lbTailNumber" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                                                                    <asp:Label ID="lbcvTailNumber" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel50">
                                                                        ICAO
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0" id="tblICAO">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbICAO" runat="server" CssClass="text60" AutoPostBack="true" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                        MaxLength="8" OnTextChanged="ICAO_TextChanged" />
                                                                                    <asp:HiddenField ID="hdnICAO" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Button ID="btnICAO" OnClientClick="javascript:openWin('radAirportPopup');return false;"
                                                                                        runat="server" CssClass="browse-button" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="lbICAODesc" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                                                                    <asp:Label ID="lbICAO" runat="server" Visible="true" CssClass="alert-text"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel50">
                                                                        Log No.
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0" id="Table1">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbLogNo" runat="server" CssClass="text60" AutoPostBack="true" OnTextChanged="LogNo_TextChanged"
                                                                                        onKeyPress="return fnAllowNumeric(this, event)" MaxLength="8" />
                                                                                    <asp:HiddenField ID="hdnPOLogID" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Button ID="btnBrowseLogs" OnClientClick="javascript:openWin('radLogPopup');return false;"
                                                                                        runat="server" CssClass="browse-button" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="lbLogNo" runat="server" Visible="true" CssClass="alert-text"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="tdLabel50">
                                                                        Leg No.
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlLegs" Width="65px" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td valign="top">
                                                <table>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel100">
                                                                        Payment Type
                                                                    </td>
                                                                    <td class="tdLabel100">
                                                                        <table cellpadding="0" cellspacing="0" id="tblPaymentType">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbPaymentType" CssClass="text60" runat="server" AutoPostBack="true"
                                                                                        OnTextChanged="PaymentType_TextChanged" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                        MaxLength="8">
                                                                                    </asp:TextBox>
                                                                                    <asp:HiddenField ID="hdnPaymentType" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Button ID="btnPaymentType" OnClientClick="javascript:openWin('radPaymentTypePopup');return false;"
                                                                                        runat="server" CssClass="browse-button" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="lbPaymentTypeDesc" CssClass="input_no_bg" runat="server" />
                                                                                    <asp:Label ID="lbPaymentType" runat="server" Visible="true" CssClass="alert-text"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="tdLabel100">
                                                                        Payables Vendor
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0" id="tblPayVendor">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbPayVendor" runat="server" CssClass="text60" AutoPostBack="true"
                                                                                        OnTextChanged="PayVendor_TextChanged" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                        MaxLength="8" />
                                                                                    <asp:HiddenField ID="hdnPayVendor" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Button ID="btnPayVendor" OnClientClick="javascript:openWin('radPayableVendorPopup');return false;"
                                                                                        runat="server" CssClass="browse-button" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="lbPayVendor" runat="server" Visible="true" CssClass="alert-text"></asp:Label>
                                                                                    <asp:Label ID="lbPayVendorDesc" CssClass="input_no_bg" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel100">
                                                                        Fuel Locator
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0" id="tblFuelLocator">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbFuelLocator" CssClass="text60" runat="server" AutoPostBack="true"
                                                                                        OnTextChanged="FuelLocator_TextChanged" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                                        MaxLength="8" />
                                                                                    <asp:HiddenField ID="hdnFuelLocator" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Button ID="btnFuelLocator" OnClientClick="javascript:openWin('radFuelLocatorPopup');return false;"
                                                                                        runat="server" CssClass="browse-button" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <asp:Label ID="lbFuelLocator" runat="server" Visible="true" CssClass="alert-text"></asp:Label>
                                                                        <asp:Label ID="lbFuelLocatorDesc" CssClass="input_no_bg" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel100">
                                                                        Account No.
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0" id="tblAccount">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbAccountNumber" runat="server" CssClass="text60" AutoPostBack="true"
                                                                                        OnTextChanged="AccountNumber_TextChanged" onKeyPress="return fnAllowNumericAndChar(this, event,'.')"
                                                                                        MaxLength="8" />
                                                                                    <asp:HiddenField ID="hdnAccountNumber" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Button ID="btnAccountNo" OnClientClick="javascript:openWin('radAccountMasterPopup');return false;"
                                                                                        runat="server" CssClass="browse-button" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <asp:Label ID="lbAccountNumber" runat="server" Visible="true" CssClass="alert-text"></asp:Label>
                                                                        <asp:Label ID="lbAccDescription" CssClass="input_no_bg" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td valign="top">
                                                <table>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel100">
                                                                        Invoice No.
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbInvoiceNo" CssClass="text60" runat="server" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                            MaxLength="25">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel100">
                                                                        Exp. Amount
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbExpAmount" CssClass="text60" runat="server" onKeyPress="return fnAllowNumeric(this, event)"
                                                                            MaxLength="14">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                            <td valign="middle">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="Search_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="dgSearch" runat="server" AllowMultiRowSelection="false" AllowSorting="true"
                                    OnItemCommand="Search_ItemCommand" OnNeedDataSource="Search_BindData" OnInsertCommand="Search_InsertCommand"
                                    OnItemDataBound="Search_ItemDataBound" AutoGenerateColumns="false" PageSize="10"
                                    Width="960px" AllowPaging="true" OnPreRender="dgSearch_PreRender">
                                    <MasterTableView CommandItemDisplay="Bottom" DataKeyNames="HomebaseID">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="POLogID" HeaderText="LogNo" CurrentFilterFunction="EqualTo"
                                                Display="false" FilterDelay="500" ShowFilterIcon="false" UniqueName="POLogID">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PostflightExpenseID" HeaderText="PostflightExpenseID"
                                                CurrentFilterFunction="EqualTo" Display="false" FilterDelay="500" ShowFilterIcon="false" UniqueName="PostflightExpenseID">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LogNum" HeaderText="Log No." CurrentFilterFunction="EqualTo"
                                                ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LegNUM" HeaderText="Leg No." CurrentFilterFunction="EqualTo"
                                                ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="InvoiceNUM" HeaderText="Invoice No." CurrentFilterFunction="StartsWith"
                                                ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="SlipNUM" HeaderText="Slip No." CurrentFilterFunction="EqualTo"
                                                ShowFilterIcon="false" AutoPostBackOnFilter="false" UniqueName="SlipNUM" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PurchaseDt" HeaderText="Purchase Date" CurrentFilterFunction="EqualTo"
                                                ShowFilterIcon="false" AutoPostBackOnFilter="false" UniqueName="PurchaseDt" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail No." CurrentFilterFunction="StartsWith"
                                                ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="IcaoID" HeaderText="ICAO" CurrentFilterFunction="StartsWith"
                                                ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="AccountDescription" HeaderText="Acct. Desc" CurrentFilterFunction="StartsWith"
                                                ShowFilterIcon="false" DataFormatString="<nobr>{0}</nobr>" AutoPostBackOnFilter="false"
                                                FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ExpenseAMT" HeaderText="Exp. Amount" CurrentFilterFunction="EqualTo"
                                                ShowFilterIcon="false" AutoPostBackOnFilter="false" ItemStyle-HorizontalAlign="Right"
                                                FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="FuelQTY" HeaderText="Fuel Quantity" CurrentFilterFunction="EqualTo"
                                                ShowFilterIcon="false" AutoPostBackOnFilter="false" ItemStyle-HorizontalAlign="Right"
                                                FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="UnitPrice" HeaderText="Unit Price" CurrentFilterFunction="EqualTo"
                                                ShowFilterIcon="false" AutoPostBackOnFilter="false" ItemStyle-HorizontalAlign="Right"
                                                FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PostFuelPrice" HeaderText="Posted Price" CurrentFilterFunction="EqualTo"
                                                ShowFilterIcon="false" AutoPostBackOnFilter="false" ItemStyle-HorizontalAlign="Right"
                                                FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="FuelLocatorCD" HeaderText="Fuel Locator" CurrentFilterFunction="StartsWith"
                                                ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="HomebaseCD" HeaderText="Home Base" CurrentFilterFunction="StartsWith"
                                                ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                        <CommandItemTemplate>
                                            <div style="padding: 5px 5px; text-align: right;">
                                            <asp:Button ID="lbtnInitInsert" runat="server" ToolTip="OK" CommandName="InitInsert"
                                                    CausesValidation="false" CssClass="button" Text="OK" style="display:none;"></asp:Button>
                                                <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                                                    Ok</button>
                                            </div>
                                        </CommandItemTemplate>
                                    </MasterTableView>
                                    <ClientSettings>
                                        <ClientEvents OnRowDblClick="returnToParent" />
                                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                    <GroupingSettings CaseSensitive="false"></GroupingSettings>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <asp:Label ID="lbMessage" runat="server" CssClass="alert-text" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
