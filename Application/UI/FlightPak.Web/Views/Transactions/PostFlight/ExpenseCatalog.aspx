﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/PostFlight.Master"
    AutoEventWireup="true" CodeBehind="ExpenseCatalog.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.PostFlight.ExpenseCatalog"
    MaintainScrollPositionOnPostback="true" %>
<%@ Import Namespace="System.Web.Optimization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PostFlightHeadContent" runat="server">
    <style>
        .rightJustified {
            text-align: right
        }
        table#ctl00_ctl00_MainContent_tblTripHeader tbody tr td:first-of-type{
            width:80px!important;
        }
    </style>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <%=Scripts.Render("~/bundles/knockout") %>
         <%=Scripts.Render("~/bundles/postflightExpenseCatalog") %>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PostFlightBodyContent" runat="server">
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true"
        ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="radFleetProfilePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="FleetPopupClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFleetProfileCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCompanyMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="HomeBasePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCompanyCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="HomeBasePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAccountMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientAccountClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAccountMasterCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPayableVendorPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnVendorPayClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close,Move" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/PayableVendorPopUp.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPayableVendorCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                Top="0px" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnAirportClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewRosterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnCrewCodeClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewRosterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewRosterCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadFBOCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnFBOCodeClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/FBOPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFBOCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnFBOCodeClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/FBOPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaymentTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnPayTypeClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/PaymentTypePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaymentTypeCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                Top="0px" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFlightCategoryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnFlightCatClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFlightCategoryCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnFlightCatClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadFuelLocatorPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnFuelLocatorClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/FuelLocatorPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadFuelLocatorCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnFuelLocatorClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/FuelLocatorPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadSearchPopup" runat="server" Width="980px" Height="486px" OnClientResizeEnd="GetDimensions"
                 KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PostFlight/PostFlightSearchAllExpenses.aspx" OnClientClose="OnSearchAllExpensesClose">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadLogPopup" runat="server" Width="750px" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PostFlight/PostFlightSearchAll.aspx" OnClientClose="OnLogPopupClose">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadRetrievePopup" runat="server" Width="750px" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PostFlight/PreflightSearchRetrieve.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdExportReport" runat="server" Width="750px" Title="Export Expense Catalog Report" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Reports/ExportReportInformation.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" style="width: auto; float: left;">
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table width="100%" cellpadding="0" cellspacing="0" class="border-box" id="tblLeg">
            <tr>
                <td>
                    <table width="100%">
                        <tr style="display: none;">
                            <td>
                                <asp:HiddenField ID="hdnPOLegExpenseInitializationObject" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table style="height:5px;text-indent:5px" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <label id="lbLastModified" class="acnt_info" data-bind="text: ExpenseCatalog.LastUpdatedBy"></label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <div class="tab-nav-top">
                                    <span class="head-title">Expense Catalog</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" border="0">
                                    <tr>
                                        <td>
                                            <div id="divMessage" style="display: inline; width: 25%; margin-left: 5px">

                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 40%">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <div class="global-search">
                                                            <span>
                                                                <input type="button" id="btnSearch" class="searchsubmitbutton"  />
                                                                <input type="text" id="tbSearchBox" onkeypress="return fnAllowNumeric(this, event)" class="SearchBox_sc_crew" data-bind="value: ExpenseSearch" />
                                                                <input type="hidden" id="hdnPostflightExpenseID" data-bind="value: ExpenseCatalog.PostflightExpenseID"  />
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td valign="middle">
                                                        <a href="#" id="lnkSeeAll" onclick="javascript:openWin('RadSearchPopup');return false;">See All</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right" style="width: 30%">
                                            <table cellpadding="0" cellspacing="0" style="float:right">
                                                <tr>
                                                    <td>
                                                         <a href="#" id="lnkPreview" class="search-icon" title="Preview Report" data-bind="click: lnkPreviewClick, css: PrintPreviewBtn"/>
                                                    </td>
                                                    <td>
                                                        <a href="#" id="lnkExport" class="save-icon" title="Export Report" data-bind="click: lnkExportClick, css: ExportBtn"/>
                                                    </td>
                                                     <td>
                                                        <a href="#" title="Print" class="print-icon" onclick="window.print();"></a>
                                                    </td>
                                                    <td>
                                                        <a href= "../../Help/ViewHelp.aspx?Screen=ExpenseCatalog"
                                                            target="_blank" title="Help" class="help-icon"></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            <label id="lbSearchMsg" class="alert-text"></label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" id="ExpenseEntryArea">
                                    <tr>
                                        <td>
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td class="tdLabel130">
                                                        <span class="mnd_text">Slip No.</span>
                                                    </td>
                                                    <td>
                                                        <input type="text" id="tbSlipNumber" class="inpt_non_edit text110" data-bind="value: ExpenseCatalog.SlipNUM" readonly="readonly" />
                                                    </td>
                                                    <td align="right">
                                                        <table cellpadding="0" cellspacing="0" style="float:right">
                                                            <tr>
                                                                <td>
                                                                    <a href="#" id="lnkInitInsert" title="Add" data-bind="click: lnkInitInsertClick"><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></a>
                                                                    <a href="#" id="lnkInitEdit" title="Edit" data-bind="click: lnkInitEditClick"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></a>
                                                                    <a href="#" id="lnkDelete" title="Delete" data-bind="click: lnkDeleteClick"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%">
                                            <table width="100%" class="border-box">
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel120">Log No.
                                                                </td>
                                                                <td class="tdLabel210">
                                                                    <table cellpadding="0" cellspacing="0" id="Table1">
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" id="tbLogNo" onkeypress="return fnAllowNumeric(this, event)" class="text110" maxlength="8" data-bind="enable: ECEditMode, value: ExpenseCatalog.LogNum" />
                                                                                <input type="hidden" id="hdnPOLogID" data-bind="value: ExpenseCatalog.POLogID"  />
                                                                            </td>
                                                                            <td>
                                                                                <input type="button" id="btnBrowseLogs" data-bind="enable: ECEditMode, css: BrowseBtn" title="Search for Log No."  onclick="    javascript: openWin('RadLogPopup'); return false;" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="tdLabel120">Leg No.
                                                                </td>
                                                                <td>
                                                                    <select id="ddlLegs" style="width:65px" data-bind="enable: ECEditMode, options: ExpenseCatalog.PostflightLeg, optionsText: 'LegNUM', optionsValue: 'POLegID', value: ExpenseCatalog.POLegID"></select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel120"></td>
                                                                <td class="tdLabel210">
                                                                    <label id="lbLogNo" class="alert-text"></label>
                                                                </td>
                                                                <td class="tdLabel120"></td>
                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel120">Tail No.
                                                                    <label id="Label1"></label>
                                                                </td>
                                                                <td class="tdLabel210">
                                                                    <table cellpadding="0" cellspacing="0" id="Table3">
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" id="tbTailNo" maxlength="9" class="text110" data-bind="enable: ECEditMode, value: ExpenseCatalog.Fleet.TailNum, event: { change: TailNumValidator_KO }" />
                                                                                <input type="hidden" id="hdnFleetID" data-bind="value: ExpenseCatalog.FleetID"  />
                                                                            </td>
                                                                            <td>
                                                                                <input type="button" id="btnTailNo" data-bind="enable: ECEditMode, css: BrowseBtn" title="Search for Tail No."  onclick="    javascript: openWin('radFleetProfilePopup'); return false;" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="tdLabel120">
                                                                    <label id="lbHomeBaseExpense" data-bind="attr: { title: self.ExpenseCatalog.Homebase.HomebaseAirport.tooltipInfo }">Home Base</label>
                                                                </td>
                                                                <td>
                                                                    <table cellpadding="0" cellspacing="0" id="tblHomeBase">
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" id="tbHomeBase" class="text110"  data-bind="enable: ECEditMode, value: ExpenseCatalog.Homebase.HomebaseCD, event: { change: HomebaseValidator_KO }"/>
                                                                                <input type="hidden" id="hdnHomeBase" data-bind="value: ExpenseCatalog.HomebaseID"  />
                                                                            </td>
                                                                            <td>
                                                                                <input type="button" id="btnHomeBase" data-bind="enable: ECEditMode, css: BrowseBtn" title="Search for Home Base"  onclick="    javascript: openWin('radCompanyMasterPopup'); return false;" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel120"></td>
                                                                <td class="tdLabel210">
                                                                    <label id="lbcvTailNo" class="alert-text" ></label>
                                                                </td>
                                                                <td class="tdLabel120"></td>
                                                                <td>
                                                                    <label id="lbHomeBaseDesc" class="input_no_bg" data-bind="text: ExpenseCatalog.Homebase.BaseDescription.kotouppercase(), attr: { title: self.ExpenseCatalog.Homebase.HomebaseAirport.tooltipInfo }" ></label>
                                                                    <label id="lbHomeBase" class="alert-text"></label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="mnd_text tdLabel120">Account No.
                                                                </td>
                                                                <td class="tdLabel210">
                                                                    <table cellpadding="0" cellspacing="0" id="tblAccount">
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" id="tbAccountNumber" class="text110" maxlength="8" data-bind="enable: ECEditMode, value: ExpenseCatalog.Account.AccountNum, event: { change: AccountValidator_KO }" />
                                                                                <input type="hidden" id="hdnAccountNumber" data-bind="value: ExpenseCatalog.AccountID"  />
                                                                            </td>
                                                                            <td>
                                                                                <input type="button" id="btnAccountNo" data-bind="enable: ECEditMode, css: BrowseBtn" title="View Account Numbers"  onclick="    javascript: openWin('radAccountMasterPopup'); return false;" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="tdLabel120">
                                                                    <label ID="lbICAOTitle" data-bind="attr: { title: self.ExpenseCatalog.Airport.tooltipInfo }">ICAO</label>
                                                                </td>
                                                                <td>
                                                                    <table cellpadding="0" cellspacing="0" id="tblICAO">
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" id="tbICAO" class="text110" maxlength="8" data-bind="enable: ECEditMode, value: ExpenseCatalog.Airport.IcaoID, event: { change: AirportValidator_KO },"  />
                                                                                <input type="hidden" id="hdnICAO" data-bind="value: ExpenseCatalog.AirportID"  />
                                                                            </td>
                                                                            <td>
                                                                                <input type="button" id="btnICAO" data-bind="enable: ECEditMode, css: BrowseBtn" title="Display Airports"  onclick="    javascript: openWin('radAirportPopup'); return false;" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel120"></td>
                                                                <td class="tdLabel210">
                                                                    <label id="lbAccountNumber" class="alert-text" ></label>
                                                                </td>
                                                                <td class="tdLabel120"></td>
                                                                <td>
                                                                    <label id="lbICAODesc" class="input_no_bg" data-bind="text: ExpenseCatalog.Airport.AirportName.kotouppercase(), attr: { title: self.ExpenseCatalog.Airport.tooltipInfo }"></label>
                                                                    <label id="lbICAO" class="alert-text"></label>
                                                                 </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel120">Account Description
                                                                </td>
                                                                <td class="tdLabel210">
                                                                    <input type="text" id="tbAccDescription" class="inpt_non_edit text180" maxlength="8" data-bind="value: ExpenseCatalog.Account.AccountDescription.kotouppercase()" readonly="readonly"/>
                                                                </td>
                                                                <td class="tdLabel120">FBO
                                                                </td>
                                                                <td>
                                                                    <table cellpadding="0" cellspacing="0" id="tblFBO">
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" id="tbFBO" class="text110" maxlength="8" data-bind="enable: ECEditMode, value: ExpenseCatalog.FBO.FBOCD, event: { change: FBOValidator_KO }" />
                                                                                <input type="hidden" id="hdnFBO" data-bind="value: ExpenseCatalog.FBOID"  />
                                                                            </td>
                                                                            <td>
                                                                                <input type="button" id="btnFBO" data-bind="enable: ECEditMode, css: BrowseBtn" title="View FBO Records"  onclick="    javascript: fnValidateFBO(); return false;" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel120"></td>
                                                                <td class="tdLabel210"></td>
                                                                <td class="tdLabel120"></td>
                                                                <td>
                                                                    <label id="lbFBODesc" class="input_no_bg" data-bind="text: ExpenseCatalog.FBO.FBOVendor.kotouppercase()"></label>
                                                                    <label id="lbFBO" class="alert-text"></label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel120">Account Period
                                                                </td>
                                                                <td class="tdLabel210">
                                                                    <input type="text" id="tbAcctPeriod" onkeypress="return fnAllowNumeric(this, event)" class="text110" maxlength="6" data-bind="enable: ECEditMode, value: ExpenseCatalog.AccountPeriod" />
                                                                </td>
                                                                <td class="tdLabel120">Purchase Date
                                                                </td>
                                                                <td class="tdLabel100">
                                                                     <input id="tbPurchDate" class="text80" type="text" maxlength="10" data-bind="datepicker: ExpenseCatalog.PurchaseDT, enable: ECEditMode"/>
                                                                </td>
                                                                <td>
                                                                    <input type="checkbox" id="chkIncludeOnBilling" data-bind="enable: ECEditMode, checked: ExpenseCatalog.IsBilling" /> <label id="lbchkIncludeOnBilling" for="chkIncludeOnBilling">Include On Billing</label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%">
                                            <table width="100%" class="border-box">
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel120">Crew
                                                                </td>
                                                                <td class="tdLabel210">
                                                                    <table cellpadding="0" cellspacing="0" id="tblCrew">
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" id="tbCrew" class="text110" maxlength="8" data-bind="enable: ECEditMode, value: ExpenseCatalog.Crew.CrewCD, event: { change: CrewValidator_KO }" />
                                                                                <input type="hidden" id="hdnCrew" data-bind="value: ExpenseCatalog.CrewID"  />
                                                                            </td>
                                                                            <td>
                                                                                <input type="button" id="btnCrew" data-bind="enable: ECEditMode, css: BrowseBtn" title="View Crew Roster"  onclick="    javascript: openWin('radCrewRosterPopup'); return false;" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="tdLabel120">Payables Vendor
                                                                </td>
                                                                <td>
                                                                    <table cellpadding="0" cellspacing="0" id="tblPayVendor">
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" id="tbPayVendor" class="text110" maxlength="8" data-bind="enable: ECEditMode, value: ExpenseCatalog.PayableVendor.VendorCD, event: { change: PaymentVendorValidator_KO }" />
                                                                                <input type="hidden" id="hdnPayVendor" data-bind="value: ExpenseCatalog.PaymentVendorID"  />
                                                                            </td>
                                                                            <td>
                                                                                <input type="button" id="btnPayVendor" data-bind="enable: ECEditMode, css: BrowseBtn" title="View Payable Vendors"  onclick="    javascript: openWin('radPayableVendorPopup'); return false;" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel120"></td>
                                                                <td class="tdLabel210">
                                                                    <label id="lbCrewDesc" class="input_no_bg" data-bind="text: ExpenseCatalog.Crew.DisplayName.kotouppercase()"></label>
                                                                    <label id="lbCrew" class="alert-text"></label>
                                                                </td>
                                                                <td class="tdLabel120"></td>
                                                                <td>
                                                                    <label id="lbPayVendorDesc" class="input_no_bg" data-bind="text: ExpenseCatalog.PayableVendor.Name.kotouppercase()"></label>
                                                                    <label id="lbPayVendor" class="alert-text"></label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel120">Flight Category
                                                                </td>
                                                                <td class="tdLabel210">
                                                                    <table cellpadding="0" cellspacing="0" id="tblFlightCat">
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" id="tbFlightCat" class="text110" maxlength="8" data-bind="enable: ECEditMode, value: ExpenseCatalog.FlightCategory.FlightCatagoryCD, event: { change: FlightCatagoryValidator_KO }" />
                                                                                <input type="hidden" id="hdnFlightCat" data-bind="value: ExpenseCatalog.FlightCategoryID"  />
                                                                            </td>
                                                                            <td>
                                                                                <input type="button" id="btnFlightCat" data-bind="enable: ECEditMode, css: BrowseBtn" title="View Flight Categories"  onclick="    javascript: openWin('radFlightCategoryPopup'); return false;" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td class="tdLabel120">Dispatch No.
                                                                </td>
                                                                <td>
                                                                    <input type="text" id="tbDispatchNumber" class="text110" maxlength="12" data-bind="enable: ECEditMode, value: ExpenseCatalog.DispatchNUM" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel120"></td>
                                                                <td>
                                                                    <label id="lbFlightDesc" class="input_no_bg" data-bind="text: ExpenseCatalog.FlightCategory.FlightCatagoryDescription.kotouppercase()"></label>
                                                                    <label id="lbFlightCat" class="alert-text"></label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                         <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel120">Invoice No.
                                                                </td>
                                                                <td class="tdLabel210">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" id="tbInvoiceNumber" class="text110" maxlength="25" data-bind="enable: ECEditMode, value: ExpenseCatalog.InvoiceNUM" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                               <td class="tdLabel120">Payment Type
                                                                </td>
                                                                 <td>
                                                                    <table cellpadding="0" cellspacing="0" id="tblPaymentType">
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" id="tbPaymentType" class="text110" maxlength="8" data-bind="enable: ECEditMode, value: ExpenseCatalog.PaymentType.PaymentTypeCD, event: { change: PaymentTypeValidator_KO }" />
                                                                                <input type="hidden" id="hdnPaymentType" data-bind="value: ExpenseCatalog.PaymentTypeID"  />
                                                                            </td>
                                                                            <td>
                                                                                <input type="button" id="btnPaymentType" data-bind="enable: ECEditMode, css: BrowseBtn" title="View Payment Types"  onclick="    javascript: openWin('radPaymentTypePopup'); return false;" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                     <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr><td width="450"></td>
                                                                <td>
                                                                   <label id="lbPaymentTypeDesc" class="input_no_bg" data-bind="text: ExpenseCatalog.PaymentType.PaymentTypeDescription.kotouppercase()"></label>
                                                                   <label id="lbPaymentType" class="alert-text"></label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" cellspacing="0" cellpadding="0" class="border-box">
                                                <tr>
                                                    <td width="48%">
                                                        <table width="100%" id="tblFuelEntry" runat="server">
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <input type="checkbox" id="chkFuelEntry" data-bind="enable: ECEditMode, checked: FuelEntryEnabled" /> <label id="lbchkFuelEntry" for="chkFuelEntry">Fuel Entry</label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel120">Fuel Units
                                                                            </td>
                                                                            <td>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <select id="ddlUnits" data-bind="enable: self.EditFuelEntry, value: self.ExpenseCatalog.FuelPurchase">
                                                                                                <option value="0">Select Type</option>
                                                                                                <option value="1">U.S. Gallons</option>
                                                                                                <option value="2">Liters</option>
                                                                                                <option value="3">Imp. Gallons</option>
                                                                                                <option value="4">Pounds</option>
                                                                                                <option value="5">Kilos</option>
                                                                                            </select>
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="text" id="tbUnits" class="text110" style="display:none"/>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel120">Fuel Locator
                                                                            </td>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0" id="tblFuelLocator">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <input type="text" id="tbFuelLocator" class="text110" maxlength="8" data-bind="enable: self.EditFuelEntry, value: ExpenseCatalog.FuelLocator.FuelLocatorCD, event: { change: FuelLocatorValidator_KO }" />
                                                                                            <input type="hidden" id="hdnFuelLocator" data-bind="value: ExpenseCatalog.PaymentTypeID"  />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="button" id="btnFuelLocator" data-bind="enable: self.EditFuelEntry, css: BrowseBtn" title="View Fuel Locators"  onclick="    javascript: openWin('RadFuelLocatorPopup'); return false;" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel120"></td>
                                                                            <td>
                                                                                <label id="lbFuelLocatorDesc" class="input_no_bg" data-bind="text: ExpenseCatalog.FuelLocator.FuelLocatorDescription.kotouppercase()"></label>
                                                                                <label id="lbFuelLocator" class="alert-text"></label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel120">Quantity
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="tbQuantity" class="text110" onfocus="this.select();" maxlength="17" onkeypress="return fnAllowNumericAndChar(this, event,'.')"  data-bind=" enable: self.EditFuelEntry, value: self.ExpenseCatalog.FuelQTY, event: { change: FuelQtyUnitPrice_Changed }" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel120">Unit Price
                                                                            </td>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td align="left" class="pr_radtextbox_100">
                                                                                            <input type="text" class="rightJustified" pattern="[0-9]*" maxlength="17" id="tbUnitPrice" data-bind="enable: self.EditFuelEntry, CostCurrencyElement: self.ExpenseCatalog.UnitPrice, precision: 4, event: { change: FuelQtyUnitPrice_Changed }" />                                                                    
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel120">Posted Price
                                                                            </td>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td align="left" class="pr_radtextbox_100">
                                                                                            <input type="text" pattern="[0-9]*" maxlength="17" class="rightJustified" id="tbPostedPrice"data-bind="enable: self.EditFuelEntry, CostCurrencyElement: self.ExpenseCatalog.PostFuelPrice, precision: 4" />                                                                    
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td valign="top" width="52%">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <fieldset>
                                                                                    <legend>Taxes</legend>
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td class="tdLabel100">Federal Tax
                                                                                            </td>
                                                                                            <td align="left" class="pr_radtextbox_100">
                                                                                                <input type="text" pattern="[0-9]*" maxlength="17" id="tbFedTax" class="rightJustified" data-bind="enable: self.EditFuelEntry, CostCurrencyElement: self.ExpenseCatalog.FederalTAX" />                                                                    
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>State Tax
                                                                                            </td>
                                                                                            <td align="left" class="pr_radtextbox_100">
                                                                                                <input type="text" pattern="[0-9]*" maxlength="17" id="tbReligonalTax" class="rightJustified" data-bind="enable: self.EditFuelEntry, CostCurrencyElement: self.ExpenseCatalog.SateTAX" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Sales Tax
                                                                                            </td>
                                                                                            <td align="left" class="pr_radtextbox_100">
                                                                                                <input type="text" pattern="[0-9]*" maxlength="17" id="tbSalesTax" class="rightJustified" data-bind="enable: self.EditFuelEntry, CostCurrencyElement: self.ExpenseCatalog.SaleTAX" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </fieldset>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <div class="tblspace_10">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel120">Total Expense
                                                                            </td>
                                                                            <td align="left" class="pr_radtextbox_100">
                                                                                <input type="text" pattern="[0-9]*" maxlength="17" class="rightJustified" id="tbTotalExpense" data-bind="enable: self.ECEditMode, CostCurrencyElement: ExpenseCatalog.ExpenseAMT, event: { change: FuelTotalExpense_Changed }"  />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left">
                                                        <telerik:RadPanelBar ID="pnlExpenseNotes" ExpandAnimation-Type="None" CollapseAnimation-Type="none"
                                                            runat="server" Width="100%">
                                                            <Items>
                                                                <telerik:RadPanelItem runat="server" Expanded="true" Text="Expense Notes">
                                                                    <Items>
                                                                        <telerik:RadPanelItem>
                                                                            <ContentTemplate>
                                                                                <table cellpadding="0" cellspacing="0" width="100%" class="note-box">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <textarea id="tbExpenseNotes" class="textarea-668x80" data-bind="enable: ECEditMode, textinput: ExpenseCatalog.PostflightNote.Notes" >
                                                                                            </textarea>
                                                                                            <input type="hidden" id="hdnExpenseNoteID" data-bind="value: ExpenseCatalog.PostflightNote.PostflightNoteID" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ContentTemplate>
                                                                        </telerik:RadPanelItem>
                                                                    </Items>
                                                                </telerik:RadPanelItem>
                                                            </Items>
                                                        </telerik:RadPanelBar>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table width="100%" class="tblButtonArea">
            <tr>
                <td align="right">
                     <input type="button" id="btnCopyExpense"  value="Copy Expense"  title="Copy Selected Record" data-bind="enable: ECEditMode, click: btnCopyExpenseClick" />
                    <input type="button" id="btnSave"  value="Save"  title="Save Changes" data-bind="enable: ECEditMode, click: btnSaveChangesClick" />
                    <input type="button" id="btnCancel"  value="Cancel" title="Cancel All Changes" data-bind="enable: ECEditMode, click: btnCancelClick"/>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
