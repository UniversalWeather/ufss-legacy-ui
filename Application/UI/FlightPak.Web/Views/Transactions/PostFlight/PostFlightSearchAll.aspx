﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PostFlightSearchAll.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.PostFlight.PostFlightSearchAll" %>
 <%@ Import Namespace="System.Web.Optimization" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Postflight Search All</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="radScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <%=Scripts.Render("~/bundles/jquery") %>
        <%=Scripts.Render("~/bundles/jqgridjs") %>
        <%=Scripts.Render("~/bundles/sitecommon") %>
        <%=Styles.Render("~/bundles/jqgrid") %>
        <%=Scripts.Render("~/bundles/knockout") %>
        <%=Scripts.Render("~/bundles/postflight") %>
            <style>
                .ui-jqgrid .ui-jqgrid-hbox {
                    float: left;
                }
            </style>
        <script type="text/javascript">
            var jqgridTableId = '#gridAllTrips';
            var hdnClient = "";
            var hdHomebaseID = "";
            var AppdateFormat = "DD/MM/YYYY";
            var POSearchAllViewModel = function () {
                self.POSearchAll = {};
                self.GotoDate = ko.observable();
                var POSearchAllInitializationObject = htmlDecode($("[id$='hdnPOSearchLogMainInitializationSchemaObject']").val());
                self.POSearchAll = ko.mapping.fromJS(JSON.parse(POSearchAllInitializationObject));
            };
            $(document).ready(function () {
                $.extend(jQuery.jgrid.defaults, {
                    prmNames: {
                        page: "page", rows: "size", order: "dir", sort: "sort"
                    }
                });
                $("#btnSubmit").click(function () {
                    var selectedrows = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                    if (selectedrows) {
                        var rowData = $(jqgridTableId).getRowData(selectedrows);
                        var lastSel = rowData['POLogID'];
                        var LogNum = rowData['LogNum'];
                        $("#hdSelectedPOLogID").val(lastSel);
                        $("#hdSelectedLogNum").val(LogNum);
                        returnToParent();
                    }
                    else { showMessageBox('Please select a log.', popupTitle); }
                    return false;
                });

                AppdateFormat = self.UserPrincipal._ApplicationDateFormat;

                var startDepart = new Date();
                if (self.UserPrincipal._Searchack != null) {
                    startDepart.setDate(startDepart.getDate() - (parseInt(self.UserPrincipal._Searchack)));
                }
                else {
                    startDepart.setDate(startDepart.getDate() - (30));
                }
                startDepart = moment(startDepart).format(isodateformat);
                $('#tbDepDate').val(moment(startDepart, isodateformat).format(AppdateFormat.toUpperCase()));
                 

                var isiPad = navigator.userAgent.match(/iPad/i) != null;
                var WidthjqGrid;
                if (isiPad) {
                    WidthjqGrid = 950;
                    $("body").css("width", "950px");
                }
                else {
                    WidthjqGrid = 1080;
                    $("body").css("width", "1095px");
                    $(".ui-jqgrid-bdiv").addClass("HideScorll");
                }
                function dataFormatter(cellvalue, options, rowObject) {
                    if (moment(cellvalue, "YYYY-MM-DD").year() == 1)
                        return "";

                    return moment(cellvalue).format(AppdateFormat.toUpperCase());
                }
                function DateTimeFormat(cellvalue, options, rowObject) {
                    return moment(cellvalue).format(AppdateFormat.toUpperCase() + " HH:mm:ss")
                }
                function expFormatter(cellvalue, options, rowObject) {
                    return cellvalue?"!":"";
                }
                function personalFormatter(cellvalue, options, rowObject) {
                    return cellvalue ? "*" : "";
                }

                jQuery(jqgridTableId).jqGrid({
                    url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                    mtype: 'GET',
                    datatype: "json",
                    cache: false,
                    async: false,
                    ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
                    serializeGridData: function (postData) {
                        
                        if (postData._search == undefined || postData._search == false) {
                            if (postData.filters === undefined) postData.filters = null;
                        }
                        if (postData.filters != null) {
                            var filters = JSON.parse(postData.filters);
                            $.each(filters.rules, function (key, result) {
                                if (result.field == "LastUpdTS" || result.field == "EstDepartureDT") {
                                    var value = result.data.trim();
                                    if (value.length == 10)
                                    {
                                        // convert to ISO date
                                        result.data = moment(value, [self.UserPrincipal._ApplicationDateFormat.toUpperCase(), isodateformat]).format(isodateformat);
                                    }
                                    else if (value.length == 19)
                                    {
                                        // convert to ISO datetime and change name of filter
                                        result.data = moment(value, self.UserPrincipal._ApplicationDateFormat.toUpperCase()+ " HH:mm:ss").format(isodatetimeformat);
                                        result.field = "LastUpdTSDateTime";
                                    }
                                    else {
                                        result.data = "";
                                    }
                               }
                                postData.filters = JSON.stringify(filters)
                            });
                        }
                        postData.apiType = 'fss';
                        postData.method = 'PostflightMainDetails';
                        var ClientID = 0;
                        if (!IsNullOrEmpty($("#tbClientCode").val())) {
                            ClientID = POSearchAll.ClientID();
                        }

                        var homebaseID = 0;
                        if ($('#chkHomebase').prop("checked")) {
                            homebaseID = self.UserPrincipal._homeBaseId;
                        }
                        var isCompleted = false;
                        if ($('#chkCompleted').prop("checked")) {
                            isCompleted = true;
                        }
                        var isPersonal = false;
                        if ($('#ChkPerTrav').prop("checked")) {
                            isPersonal = true;
                        }
                        var fleetID = 0;
                        if (!IsNullOrEmpty($("#tbTailNumber").val())) {
                            fleetID = POSearchAll.FleetID();
                        }

                        startDepart = moment(new Date(0)).format(isodateformat);
                        if (!IsNullOrEmpty($('#tbDepDate').val())) {
                            startDepart = moment($('#tbDepDate').val(), AppdateFormat.toUpperCase()).format(isodateformat);
                        } else {
                            startDepart = '0001-01-01';
                        }
                        postData.clientID = ClientID;
                        postData.isCompleted = isCompleted;
                        postData.isPersonal = isPersonal;
                        postData.fleetID = fleetID;
                        postData.startDate = startDepart;
                        postData.homebaseID = homebaseID;
                        return postData;
                    },
                    autoheight: true,
                    width: WidthjqGrid,
                    autowidth: false,
                    shrinkToFit: false,
                    viewrecords: true,
                    rowNum: $("#rowNum").val(),
                    pager: "#pg_gridPager",
                    colNames: ['POLogID', 'Log No.', 'Trip No.', 'Dispatch', 'Tail No.', 'Departure Date', 'Passenger / Requestor', 'Department', 'Authorization', 'Description', 'Excp', 'Personal', 'Home Base', 'Flight No.', 'Completed', 'Time', 'Tech Log'],
                    colModel: [
                        { name: 'POLogID', index: 'POLogID', key: true, hidden: true },
                        { name: 'LogNum', index: 'LogNum', width: 55 },
                        { name: 'TripID', index: 'TripID', width: 55 },
                        { name: 'DispatchNUM', index: 'DispatchNUM', width: 55 },
                        { name: 'TailNum', index: 'TailNum', width: 75},
                        { name: 'EstDepartureDT', index: 'EstDepartureDT', formatter: dataFormatter, width: 70, searchoptions:{ sopt:['eq']} },
                        { name: 'PassengerCD', index: 'PassengerCD', width: 80 },
                        { name: 'DepartmentCD', index: 'DepartmentCD', width: 70 },
                        { name: 'AuthorizationCD', index: 'AuthorizationCD',  width: 80},
                        { name: 'POMainDescription', index: 'POMainDescription', width: 70 },
                        {
                            name: 'IsException', index: 'IsException', formatter: expFormatter, search: false, width: 30, cellattr: function (rowId, cellValue, rawObject, cm, rdata) {
                                var style = "";
                                if (cellValue == "!") {
                                    style = 'color:red;';
                                    if (style)
                                        return "Style='" + style + "'";
                                    return "";
                                }
                            }
                        },
                        {
                            name: 'IsPersonal', index: 'IsPersonal', search: false, width: 55, formatter: personalFormatter, cellattr: function (rowId, cellValue, rawObject, cm, rdata) {
                            var style = "";
                            if (cellValue == "*") {
                                style = 'color:red;';
                                if (style)
                                    return "Style='" + style + "'";
                                return "";
                            }
                        } },
                        { name: 'HomebaseCD', index: 'HomebaseCD', width: 55 },
                        { name: 'FlightNum', index: 'FlightNum', width: 55 },
                        {
                            name: 'IsCompleted', index: 'IsCompleted', search: false, width: 65, formatter: function(cellvalue, options, rowObject) {                                    
                                return cellvalue?"Y":"";
                            }
                        },
                        { name: 'LastUpdTS', index: 'LastUpdTS', width: 120, formatter: DateTimeFormat, searchoptions: { sopt: ['eq'] } },
                        { name: 'TechLog', index: 'TechLog', width: 70 },
                    ],
                    ondblClickRow: function (rowId) {
                        var rowData = jQuery(this).getRowData(rowId);
                        var lastSel = rowData['POLogID'];//replace name with any column
                        var LogNum = rowData['LogNum'];
                        $("#hdSelectedPOLogID").val(lastSel);
                        $("#hdSelectedLogNum").val(LogNum);
                        returnToParent();
                    },
                    onSelectRow: function (id) {
                        var rowData = $(this).getRowData(id);
                        var lastSel = rowData['POLogID'];//replace name with any column

                        if (id !== lastSel) {
                            $(this).find(".selected").removeClass('selected');
                            $('#results_table').jqGrid('resetSelection', lastSel, true);
                            $(this).find('.ui-state-highlight').addClass('selected');
                            lastSel = id;
                        }
                    },
                    gridComplete: function () {
                        $('#gbox_gridAllTrips .ui-jqgrid-htable th.ui-th-ltr > div').css({ paddingLeft: 0, paddingRight: 0 });
                        var popupContainerID = '#RadWindowWrapper_ctl00_ctl00_MainContent_SettingBodyContent_RadRetrievePopup';
                        var popupTable = '#RadWindowWrapper_ctl00_ctl00_MainContent_SettingBodyContent_RadRetrievePopup > table';
                        $(popupTable, parent.document).css({ height: '' })
                        $(popupContainerID, parent.document).css({ height: $('.preflight-search-box').height() + 50, width: $('.preflight-search-box').width() + 27 })
                        .css({ left: ($(parent.window).width() - $(popupContainerID, parent.document).width()) / 2, top: ($(parent.window).height() - $(popupContainerID, parent.document).height()) / 2 });
                        $('html').css({ backgroundColo: 'white' });
                        ApplyCustomScroll($(".ui-jqgrid-bdiv"));
                    }
                });
                $("#pagesizebox").insertBefore('.ui-paging-info');
                $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });

                if (isiPad == false) {
                    $(".ui-jqgrid-bdiv").addClass("HideScorll");
                }
            });

            //this function is used to navigate to pop up screen's with the selected code
            function openWin(radWin) {
                var url = '';
                if (radWin == "RadClientCodePopup") {
                    url = '/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById("tbClientCode").value;
                }
                else if (radWin == "radFleetProfilePopup") {
                    url = '/Views/Settings/Fleet/FleetProfilePopup.aspx';
                }
                var oWnd = radopen(url, radWin);
            }
            // this function is used to display the value of selected Client code from popup
            function ClientCidePopupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        self.POSearchAll.Client.ClientCD(arg.ClientCD);
                        self.POSearchAll.ClientID(arg.ClientID);
                        self.POSearchAll.Client.ClientDescription(arg.ClientDescription);
                        $('#lbcvClient').text('');
                    }
                    else {
                        self.POSearchAll.Client.ClientCD("");
                        self.POSearchAll.ClientID("");
                        self.POSearchAll.Client.ClientDescription("");
                    }
                }
            }

            function OnClientTailNoClose(oWnd, args) {
                var combo = $("tbTailNumber");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        self.POSearchAll.Fleet.TailNum(arg.TailNum);
                        self.POSearchAll.FleetID(arg.FleetID);
                        $('#lbcvTailNumber').text('');
                    }
                    else {
                        self.POSearchAll.Fleet.TailNum("");
                        self.POSearchAll.FleetID("");
                    }
                }
            }
            function ClientValidator_KO() {
                var btn = document.getElementById("btnClientCode");
                var clientErrorLabel = "#lbcvClient";
                Client_Validate_Retrieve_KO(self.POSearchAll.Client, self.POSearchAll.ClientID, clientErrorLabel, btn);
            }
            function TailNumValidator_KO() {
                var btn = document.getElementById("btnTailNo");
                var fleetErrorlabel = "lbcvTailNumber";
                TailNum_Validate_Retrieve_KO(self.POSearchAll.Fleet, self.POSearchAll.FleetID, fleetErrorlabel, btn);
            }

            function returnToParent() {
                oArg = new Object();
                var selectedRows = $(jqgridTableId).jqGrid('getGridParam', 'selrow');

                if (selectedRows.length > 0) {
                    oArg.POLogID = $("#hdSelectedPOLogID").val();
                    oArg.LogNum = $("#hdSelectedLogNum").val();
                }
                else {
                    oArg.POLogID = "";
                    oArg.LogNum = "";
                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }
            function reloadPageSize() {
                var myGrid = $(jqgridTableId);
                var currentValue = $("#rowNum").val();
                myGrid.setGridParam({ rowNum: currentValue });
                myGrid.trigger('reloadGrid');
            }
            function reloadPage() {
                var myGrid = $(jqgridTableId);
                myGrid.trigger('reloadGrid');
            }
            function Close() {
                GetRadWindow().Close();
            }
        </script>
        </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true"
        ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="RadClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="ClientCidePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFleetProfilePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientTailNoClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFleetProfileCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <div id="DivExternalForm">
        <asp:HiddenField ID="hdnPOSearchLogMainInitializationSchemaObject" runat="server" />
        <input id="hdSelectedPOLogID" type="hidden" />
        <input id="hdSelectedLogNum" type="hidden" />
        <table cellspacing="0" cellpadding="0" class="box1">
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td valign="top" width="50%">
                                <fieldset>
                                    <legend>Search</legend>
                                    <table width="100%" border="0">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150">
                                                            <input id="chkHomebase" type="checkbox" name="chkHomebase"/>
                                                            <label for="chkHomebase">Home Base Only</label>
                                                        </td>
                                                        <td class="tdLabel130">
                                                            Starting Depart Date
                                                        </td>
                                                        <td class="tdLabel120">
                                                             <input id="tbDepDate" maxlength="10" type="text" class="text80" data-bind="datepicker: GotoDate" />
                                                        </td>
                                                        <td style="display:none">
                                                            <input id="chkIsException" type="checkbox" name="chkIsException"/>
                                                            <label for="chkHomebase">Exception</label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150">
                                                            <input id="ChkPerTrav" type="checkbox" name="ChkPerTrav"/><label for="ChkPerTrav">Personal Travel Only</label>
                                                        </td>
                                                        <td class="tdLabel130">
                                                            Client Code
                                                        </td>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        
                                                                        <input type="text" class="text80" id="tbClientCode" maxlength="5" data-bind="value: POSearchAll.Client.ClientCD, event: { change: ClientValidator_KO }"
                                                                             onchange="RemoveSpecialChars(this); fnAllowAlphaNumeric(this, event); return false;" />
                                                                        <input type="hidden" id="hdnClient"data-bind="value: POSearchAll.ClientID" />
                                                                    </td>
                                                                    <td>
                                                                        <input type="button" id="btnClientCode" title="View Clients" class="browse-button" onclick="javascript: openWin('RadClientCodePopup'); return false;"/>

                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                        </td>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <label id="lbClientDesc" data-bind="text: POSearchAll.Client.ClientDescription.kotouppercase()" class="input_no_bg"></label>
                                                                        <label id="lbcvClient" class="alert-text"></label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel150">
                                                            <input id="chkCompleted" type="checkbox" name="chkCompleted"/>
                                                            <label for="chkCompleted">Completed</label>
                                                        </td>
                                                        <td class="tdLabel130">
                                                            Tail No.
                                                        </td>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0" border="0">
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                  
                                                                                    <input type="text" class="text80" id="tbTailNumber" data-bind="value: POSearchAll.Fleet.TailNum, event: { change: TailNumValidator_KO }" />
                                                                                    <input type="hidden" id="hdnTailNo" data-bind="value: POSearchAll.FleetID" />
                                                                                </td>
                                                                                <td>
                                                                                    <input type="button" id="btnTailNo" title="Search for Tail No." class="browse-button" onclick="javascript: openWin('radFleetProfilePopup'); return false;"/>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label id="lbTailNumber" class="input_no_bg"></label>
                                                                        <label id="lbcvTailNumber" class="alert-text"></label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                            <td>
                                <input type="button" class="button" value="Search" id="btnSearch" onclick="reloadPage(); return false;" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                 <table id="gridAllTrips" style="width: 981px !important;" class="table table-striped table-hover table-bordered"></table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                             <div class="grid_icon">
                                                <div role="group" id="pg_gridPager"></div>
                                                <span class="Span">Page Size:</span>
                                                <input class="PageSize" id="rowNum" type="text" value="20" maxlength="5" />
                                                <input id="btnChange" class="btnChange" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(); return false;" />
                                            </div>
                                            <div style="padding: 5px 5px; text-align: right;">
                                                <input id="btnSubmit" type="button" class="button okButton" value="OK" />
                                            </div>
                                </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
