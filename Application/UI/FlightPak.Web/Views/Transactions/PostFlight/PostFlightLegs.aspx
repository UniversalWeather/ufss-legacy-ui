﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/PostFlight.Master"
    AutoEventWireup="true" CodeBehind="PostFlightLegs.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.PostFlight.PostFlightLegs"
    MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Framework/Masters/PostFlight.Master" %>
<%@ Register TagPrefix="uc" TagName="BottomButtonControls" Src="~/UserControls/Postflight/UCPostflightBottomButtonControls.ascx" %>
<%@ Register TagPrefix="uc" TagName="Search" Src="~/UserControls/Postflight/UCPostflightSearch.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PostFlightHeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PostFlightBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <style type="text/css">
            .prfl-nav-icons {
            left:-2px !important;
            }
        </style>
         <%=Scripts.Render("~/bundles/postflightlegs") %>
        
    </telerik:RadCodeBlock>
    
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true"
        ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="RadClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="ClientCodePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadDepartCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientDeptClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAccountMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientAccountClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAccountMasterCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadAuthCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientAuthClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="/Views/Settings/Company/AuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="RequestorPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewDutyRulesPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="RulesPopupClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewDutyRulesPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewDutyRulesCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadFlightCatagoryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="FlightCatagoryClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radDelayTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="DelayTypePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/DelayTypePopUp.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radDelayTypeCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="DelayTypePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/DelayTypePopUp.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadSearchPopup" runat="server" Width="750px" OnClientResizeEnd="GetDimensions" OnClientClose="OnLogSearchClose"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PostFlight/PostFlightSearchAll.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadRetrievePopup" runat="server" Width="750px" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" OnClientClose="OnPFSearchRetrieveClose" Behaviors="Close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/PostFlight/PreflightSearchRetrieve.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="History" NavigateUrl="~/Views/Transactions/History.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>

    <input type="hidden" id="hdCommonLabel"/>
    <input type="hidden" id="HomebaseAirpotObj"/>
    <input type="hidden" id="ArriAirpotObj"/>
    <input type="hidden" id="DepAirpotObj"/>
    <input type="hidden" id="AircraftObj"/>
    <input type="hidden" id="DateFormat" data-bind="value: UserPrincipal._ApplicationDateFormat"/>
    <asp:HiddenField ID="hdnPostflightMainInitializationObject" runat="server" />
        <asp:HiddenField ID="hdnUserPrincipalInitializationObject" runat="server" />
    <asp:HiddenField runat="server" ID="hdnPostflightLegInitializationObject" />
    <div id="DivExternalForm" runat="server" style="width: auto; float: left;">
        <input type="hidden" id="hdTimeDisplayTenMin" data-bind="value: UserPrincipal._TimeDisplayTenMin == null ? 1 : UserPrincipal._TimeDisplayTenMin"/>
        <input type="hidden" id="hdShowRequestor" data-bind="value: UserPrincipal._IsReqOnly"/>
        <input type="hidden" id="hdHighlightTailno" data-bind="value: UserPrincipal._IsANotes" />
        <input type="hidden" id="hdIsDepartAuthReq" data-bind="value: UserPrincipal._IsDepartAuthDuringReqSelection"/>
        <input type="hidden" id="hdSetFarRules" data-bind="value: UserPrincipal._FedAviationRegNum == null ? '' : UserPrincipal._FedAviationRegNum"/>
        <input type="hidden" id="hdSetCrewRules" data-bind="value: UserPrincipal._IsAutoCreateCrewAvailInfo"/>
        <input type="hidden" id="hdSetWindReliability" data-bind="value: UserPrincipal._WindReliability == null ? 3 : UserPrincipal._WindReliability"/>
        <input type="hidden" id="hdHomeCategory" data-bind="value: UserPrincipal._DefaultFlightCatID == null ? 0 : UserPrincipal._DefaultFlightCatID"/>
        <input type="hidden" id="hdHomeChkListGroup"/>
        <input type="hidden" id="hdnHomeBaseCountry"/>
        <input type="hidden" id="hdnKilo" data-bind="value: UserPrincipal._IsKilometer"/>
        <input type="hidden" id="hdElapseTMRounding" data-bind="value: UserPrincipal._ElapseTMRounding"/>
        <input type="hidden" id="hdIsAutomaticCalcLegTimes" data-bind="value: UserPrincipal._IsAutomaticCalcLegTimes == null ? false : UserPrincipal._IsAutomaticCalcLegTimes"/>
        <table width="100%" cellpadding="0" cellspacing="0" class="border-box">
            <tr>
                <td align="left">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <uc:Search ID="UCSearch" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td align="right">
                                <input type="hidden" id="hdnLeg" data-bind="value: CurrentLegNum"/>
                                <input type="hidden" id="previousTabHidden"/>
                                <input type="button" id="btnAddLeg" data-bind="attr: { 'data-leg': CurrentLegNum }, enable: POEditMode(), css: POEditMode() == true ? 'ui_nav' : 'ui_nav_disable'" onclick="AddLeg()" title="Add New Leg" value="Add Leg" />
                                <input type="button" id="btnInsertLeg" data-bind="attr: { 'data-leg': CurrentLegNum }, enable: POEditMode(), css: POEditMode() == true ? 'ui_nav' : 'ui_nav_disable'" onclick="btnInsertLegButton_Click()" title="Insert New Leg Prior to the Highlighted Leg" value="Insert Leg" />
                                <input type="button" id="btnDeleteLeg" data-bind="attr: { 'data-leg': CurrentLegNum }, enable: POEditMode(), css: POEditMode() == true ? 'ui_nav' : 'ui_nav_disable'" onclick="btnDeleteLegButton_Click()" title="Delete Existing Leg" value="Delete Leg"/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" id="tblLegs" runat="server">
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="Windows7">
                                            <ul id="rtsLegs" data-bind="foreach: Legs" class="tabStrip">
                                                <li class="tabStripItemSelected" data-bind="attr: { 'id': 'Leg' + LegNUM() }, event: { click: LegTab_Click }" >
                                                    Leg <span data-bind="text: LegNUM"></span>(<span data-bind="text: DepartureAirport.IcaoID"></span>-<span data-bind="text: ArrivalAirport.IcaoID"></span>)
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <table width="100%" class="border-box">
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0" width="100%">
                                                            <tr>
                                                                <td valign="top" style="width: 38%">
                                                                    <table id="tblDepartArrive" runat="server">
                                                                        <tr>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <label><input type="radio" id="rbDomestic" name="LegType" data-bind="checked: CurrentLegData.DutyTYPE, checkedValue: 1, enable: POEditMode, event: { click: changeLegType }"  />Domestic</label>&nbsp;
                                                                                            <label><input type="radio" id="rbInternational" name="LegType" data-bind="checked: CurrentLegData.DutyTYPE, checkedValue: 2, enable: POEditMode, event: { click: changeLegType }" />International</label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel80">
                                                                                            <label id="lbDepartIcao" style="font-weight: bold" data-bind="attr: { title: self.DepartureAirportToolTipInfo }" >Departure</label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <input id="tbDepart" type="text" data-bind="value: CurrentLegData.DepartureAirport.IcaoID, enable: POEditMode, event: { change: departureChanged }"  class="text60" maxlength="4" />
                                                                                                        <input id="hdDepart" type="hidden" data-bind="value: CurrentLegData.DepartureAirport.AirportID" />
                                                                                                        <input id="hdAircraft" type="hidden" data-bind="value: Postflight.PostflightMain.Aircraft.AircraftID"/>
                                                                                                        <input id="hdHomebaseAirport" type="hidden" />
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <input id="btnClosestIcao" type="button" title="Display Airports" data-bind="enable: POEditMode, css: BrowseBtn" onclick="    javascript: openWinAirport('DEPART'); return false;" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <label id="lbDepart" data-bind="text: CurrentLegData.DepartureAirport.AirportName, attr: { title: self.DepartureAirportToolTipInfo }" style="display: block" class="input_no_bg"></label>
                                                                                            <label id="lbcvDepart" class="alert-text" style="display: block"></label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel80">
                                                                                            <label id="lbArriveIcao" style="font-weight: bold" data-bind="attr: { title: self.ArrivalAirportToolTipInfo }">Arrival</label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <input id="tbArrival" type="text" data-bind="value: CurrentLegData.ArrivalAirport.IcaoID, enable: POEditMode, event: { change: arrivalChanged }" maxlength="4" class="text60" />
                                                                                                        <input id="hdArrival" type="hidden" data-bind="value: CurrentLegData.ArrivalAirport.AirportID"/>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <input id="btnArrival" type="button" title="Display Airports" data-bind="enable: POEditMode, css: BrowseBtn" onclick="javascript: openWinAirport('ARRIVAL'); return false;" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <label id="lbArrival" data-bind="text: CurrentLegData.ArrivalAirport.AirportName, attr: { title: self.ArrivalAirportToolTipInfo }" style="font-weight: bold" class="input_no_bg"></label>
                                                                                            <label id="lbcvArrival" class="alert-text" style="display: block"></label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel80" valign="top">
                                                                                            <label id="lbMiles" data-bind="text: UserPrincipal._IsKilometer == true ? 'Kilometers' : 'Miles(N)'"></label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <input id="hdnMiles" type="hidden" data-bind="value: IsKilometers" />
                                                                                            <input id="tbMiles" type="text" data-bind="value: CurrentLegData.ConvertedDistance, enable: POEditMode, event: { change: MilesValidation }" maxlength="5" onfocus="this.select();" onkeypress="return fnAllowNumericAndOneDecimal('miles',this, event)" onblur="return validateEmptyMilesTextbox(this, event)" class="text60 rt_text" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel80">
                                                                                            State Miles
                                                                                        </td>
                                                                                        <td>
                                                                                            <input id="tbStMiles" type="text" data-bind="value: CurrentLegData.StatuteMiles, enable: POEditMode, event: { change: MilesValidation }" maxlength="5" onfocus="this.select();" onkeypress="return fnAllowNumericAndOneDecimal('miles',this, event)" onblur="return validateEmptyStatuteMilesTextbox(this, event)" class="text60 rt_text" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td valign="top" style="width: 62%">
                                                                    <fieldset>
                                                                        <legend>Scheduled</legend>
                                                                        <asp:Panel ID="pnlScheduled" runat="server">
                                                                            <table width="100%" id="tblScheduled" runat="server">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td class="tdLabel80">
                                                                                                    Time & Date
                                                                                                </td>
                                                                                                <td valign="top">
                                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td valign="top">
                                                                                                                <input ID="tbScheduledTime" type="text" data-bind="timepicker: CurrentLegData.ScheduledTime, enable: POEditMode, event: { change: ScheduledTimeChanged }" onchange="return ValidateScheduleTime(this, 'Postflight Leg');" class="text35" />&nbsp;
                                                                                                            </td>
                                                                                                            <td valign="top">
                                                                                                                <input ID="tbScheduledDate" type="text" data-bind="datepicker: CurrentLegData.ScheduledDate, enable: POEditMode, event: { change: DateTimeChanged }" class="textDatePicker" maxlength="15" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td class="tdLabel80" valign="top">
                                                                                                    Out
                                                                                                </td>
                                                                                                <td>
                                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td valign="top">
                                                                                                                <input ID="tbOutTime" type="text" data-bind="timepicker: CurrentLegData.OutTime, enable: POEditMode, event: { change: OutTimeChanged }" onchange="return ValidateScheduleTime(this, 'Postflight Leg');" class="text35" />&nbsp;
                                                                                                            </td>
                                                                                                            <td valign="top">
                                                                                                                <input ID="tbOutDate" type="text" data-bind="datepicker: CurrentLegData.OutDate, enable: POEditMode, event: { change: DateTimeChanged }, visible: (self.UserPrincipal._DutyBasis == '2' ? false : true)" class="textDatePicker" maxlength="15" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td class="tdLabel80">
                                                                                                    Off
                                                                                                </td>
                                                                                                <td class="tdLabel180">
                                                                                                    <input ID="tbOffTime" type="text" data-bind="timepicker: CurrentLegData.TimeOff, enable: POEditMode, event: { change: OffTimeChanged }" onchange="return ValidateScheduleTime(this, 'Postflight Leg');" class="text35" />&nbsp;
                                                                                                </td>
                                                                                                <td class="tdLabel80">
                                                                                                    Block Hours
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input ID="tbBlockHours" type="text" data-bind="value: CurrentLegData.BlockHoursTime" disabled="disabled" class="text33" />&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td class="tdLabel80">
                                                                                                    On
                                                                                                </td>
                                                                                                <td class="tdLabel180">
                                                                                                    <input ID="tbOnTime" type="text" data-bind="timepicker: CurrentLegData.TimeOn, enable: POEditMode, event: { change: OnTimeChanged }" onchange="return ValidateScheduleTime(this, 'Postflight Leg');" class="text35" />&nbsp;
                                                                                                </td>
                                                                                                <td class="tdLabel80">
                                                                                                    Flight Hours
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input ID="tbFlightHours" type="text" data-bind="value: CurrentLegData.FlightHoursTime" disabled="disabled" class="text33" />&nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td class="tdLabel80">
                                                                                                    In
                                                                                                </td>
                                                                                                <td class="tdLabel180">
                                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td valign="top">
                                                                                                                <input ID="tbInTime" type="text" data-bind="timepicker: CurrentLegData.InTime, enable: POEditMode, event: { change: InTimeChanged }" onchange="return ValidateScheduleTime(this, 'Postflight Leg');" class="text35" />&nbsp;
                                                                                                            </td>
                                                                                                            <td valign="top">
                                                                                                                <input ID="tbInDate" type="text" data-bind="datepicker: CurrentLegData.InDate, enable: POEditMode, event: { change: DateTimeChanged }" class="textDatePicker" maxlength="15" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td class="tdLabel80">
                                                                                                    Duty Hours
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input ID="tbDutyHours" type="text" data-bind="value: CurrentLegData.DutyHrsTime" disabled="disabled" class="text33" />&nbsp;
                                                                                                    <input id="hdnDutyBegin" type="hidden" data-bind="value: CurrentLegData.BeginningDutyHours" />
                                                                                                    <input id="hdnDutyEnd" type="hidden" data-bind="value: CurrentLegData.EndDutyHours" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <label id="lbErrMsg" style="font-weight: bold" class="alert-text"></label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:Panel>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <div class="tblspace_5">
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0" width="100%">
                                                            <tr>
                                                                <td valign="top" width="28%">
                                                                    <fieldset>
                                                                        <legend>Delay</legend>
                                                                        <table id="tblDelay" runat="server">
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="tdLabel70" valign="top">
                                                                                                Type
                                                                                            </td>
                                                                                            <td>
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <input id="tbDelayType" type="text" data-bind="value: CurrentLegData.DelayType.DelayTypeCD, enable: POEditMode, event: { change: delayTypeChange }" maxlength="2" onfocus="this.select();" class="text40" />
                                                                                                            <input id="hdnDelayType" type="hidden" data-bind="value: CurrentLegData.DelayTypeID" />
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <input id="btnDelayType" type="button" title="View Delay Types" data-bind="enable: POEditMode, css: BrowseBtn" onclick="javascript: openWin('radDelayTypePopup'); return false;" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr> 
                                                                                            <td>
                                                                                                <label id="lbDelayDesc" data-bind="text: CurrentLegData.DelayType.DelayTypeDescShort.kotouppercase()" style="font-weight: bold" class="input_no_bg"></label>
                                                                                                <label id="lbcvDelay" style="font-weight: bold" class="alert-text"></label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="tdLabel70" valign="top">
                                                                                                Time
                                                                                            </td>
                                                                                            <td>
                                                                                                <input id="tbDelayTime" type="text" data-bind="value: CurrentLegData.DelayTMTime, enable: POEditMode" onfocus="this.select();" class="text60" onKeyPress="return fnAllowNumericAndChar(this, event,':.')" onblur="return ValidateTimeForTenMin(this, 'Postflight Leg')" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <div class="tblspace_12">
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </fieldset>
                                                                </td>
                                                                <td valign="top" width="12%">
                                                                    <fieldset>
                                                                        <legend>FAR</legend>
                                                                        <table cellpadding="0" cellspacing="0" id="tblFAR" >
                                                                            <tr>
                                                                                <td>
                                                                                    <label><input type="radio" id="rbFAR1" name="FAR" data-bind="checked: CurrentLegData.FedAviationRegNum, enable: POEditMode, checkedValue: '91'" />91</label><br />
                                                                                    <label><input type="radio" id="rbFAR2" name="FAR" data-bind="checked: CurrentLegData.FedAviationRegNum, enable: POEditMode, checkedValue: '121'" />121</label><br />
                                                                                    <label><input type="radio" id="rbFAR3" name="FAR" data-bind="checked: CurrentLegData.FedAviationRegNum, enable: POEditMode, checkedValue: '125'" />125</label><br />
                                                                                    <label><input type="radio" id="rbFAR4" name="FAR" data-bind="checked: CurrentLegData.FedAviationRegNum, enable: POEditMode, checkedValue: '135'" />135</label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </fieldset>
                                                                </td>
                                                                <td valign="top" width="30%">
                                                                    <fieldset>
                                                                        <legend>Crew</legend>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <label><input id="chkEndofDutyDay" type="checkbox" data-bind="checked: CurrentLegData.IsDutyEnd, enable: POEditMode, event: { click: endofDutyDayChange }"  />End of Duty Day</label><br/>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table id="poLegCrewDutyRule" >
                                                                                        <tr>
                                                                                            <td class="tdLabel80">
                                                                                                <label id="lbRulesTitle" data-bind="attr: { title: self.CrewDutyToolTipInfo }" >Rules</label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <input id="tbRules" type="text" data-bind="value: CurrentLegData.CrewDutyRule.CrewDutyRuleCD, enable: POEditMode, event: { change: crewDutyTypeChange }" maxlength="4" onfocus="this.select();" class="text80" />
                                                                                                            <input id="hdnRules" type="hidden" data-bind="value: CurrentLegData.CrewDutyRulesID" />
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <input id="btnRules" type="button" title="View Crew Duty Rules" data-bind="enable: POEditMode, css: BrowseBtn" onclick="javascript: openWin('radCrewDutyRulesPopup'); return false;" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2">
                                                                                                <label id="lbRules" data-bind="text: CurrentLegData.CrewDutyRule.CrewDutyRulesDescription.kotouppercase(), attr: { title: self.CrewDutyToolTipInfo }" style="font-weight: bold" class="input_no_bg"></label>
                                                                                                <label id="lbcvRules" style="font-weight: bold" class="alert-text"></label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <div class="tblspace_8">
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </fieldset>
                                                                </td>
                                                                <td width="30%" valign="top">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <table cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel100">
                                                                                            Flight No.
                                                                                        </td>
                                                                                        <td>
                                                                                            <input id="tbFlightNo" type="text" data-bind="value: CurrentLegData.FlightNum, enable: POEditMode" maxlength="12" onfocus="this.select();" onKeyPress="return fnAllowAlphaNumeric(this, event)" class="text80" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <table id="tblFlightCost" runat="server" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td class="tdLabel100">
                                                                                            Flight Cost
                                                                                        </td>
                                                                                        <td class="pr_radtextbox_85">
                                                                                            <input id="tbFlightCost" data-bind="CostCurrencyElement: CurrentLegData.FlightCost" maxlength="9" disabled="disabled" class="text80" style="text-align: right;" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <div class="tblspace_5">
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0" width="100%">
                                                            <tr>
                                                                <td valign="top" width="70%">
                                                                    <fieldset>
                                                                        <legend>Fuel Burn</legend>
                                                                        <asp:Panel ID="pnlFuelBurn" runat="server">
                                                                            <table width="100%" cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td class="tdLabel70" valign="top">
                                                                                                    Out
                                                                                                </td>
                                                                                                <td class="tdLabel70">
                                                                                                    <input id="tbFuelOut" type="text" data-bind="value: CurrentLegData.FuelOut, enable: POEditMode, event: { change: fnFuelOutChange }" maxlength="6" onfocus="this.select();" onKeyPress="return fnAllowNumeric(this, event)" class="text50 rt_text" />
                                                                                                </td>
                                                                                                <td class="tdLabel30">
                                                                                                    In
                                                                                                </td>
                                                                                                <td class="tdLabel70">
                                                                                                    <input id="tbFuelIn" type="text" data-bind="value: CurrentLegData.FuelIn, enable: POEditMode, event: { blur: fnFuelBurnCalculate }" maxlength="6"  onKeyPress="return fnAllowNumeric(this, event)" class="text50 rt_text" />
                                                                                                </td>
                                                                                                <td class="tdLabel40">
                                                                                                    Used
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input id="tbFuelUsed" type="text" data-bind="value: CurrentLegData.FuelUsed, enable: POEditMode" maxlength="7" readonly="readonly" tabindex="-1" onfocus="this.select();" onKeyPress="return fnAllowNumeric(this, event)" class="text50 rt_text" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <label id="lbcvFuelUsed" style="font-weight: bold" class="alert-text"></label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <div class="tblspace_4">
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:Panel>
                                                                    </fieldset>
                                                                </td>
                                                                <td valign="top">
                                                                    <fieldset>
                                                                        <legend>Fuel Unit</legend>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <label><input type="radio" id="rbFuelBurnPound" name="FuelBurn" data-bind="checked: self.CurrentLegData.FuelBurn, enable: POEditMode, checkedValue: 1" />Pounds</label>&nbsp;
                                                                                    <label><input type="radio" id="rbFuelBurnKilos" name="FuelBurn" data-bind="checked: self.CurrentLegData.FuelBurn, enable: POEditMode, checkedValue: 2" />Kilos</label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0" width="100%">
                                                            <tr>
                                                                <td valign="top" width="70%">
                                                                    <fieldset>
                                                                        <legend>Others</legend>
                                                                        <table id="tblOthers" runat="server">
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="tdLabel70">
                                                                                                Purpose
                                                                                            </td>
                                                                                            <td>
                                                                                                <input id="tbPurpose" type="text" data-bind="value: CurrentLegData.FlightPurpose, enable: POEditMode" maxlength="40" onfocus="this.select();" class="text350" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="tdLabel70">
                                                                                                Client
                                                                                            </td>
                                                                                            <td class="tdLabel190">
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <input id="tbClient" type="text"  data-bind="value: CurrentLegData.Client.ClientCD, enable: POEditMode, event: { change: ClientCodeValidation }" maxlength="5" class="text80" onKeyPress="return fnAllowAlphaNumeric(this, event)" />
                                                                                                            <input id="hdnClient" type="hidden" data-bind="value: CurrentLegData.ClientID" />
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <input id="btnClientCode" type="button" title="View Clients" data-bind="enable: POEditMode, css: BrowseBtn" onclick="javascript: openWin('RadClientCodePopup'); return false;" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td class="tdLabel80">
                                                                                                Requestor
                                                                                            </td>
                                                                                            <td>
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <input id="tbRequestor" type="text"  data-bind="value: CurrentLegData.Passenger.PassengerRequestorCD, enable: POEditMode, event: { change: RequestorValidator }" maxlength="5" class="text80" onKeyPress="return fnAllowAlphaNumeric(this, event)" />
                                                                                                            <input id="hdnRequestor" type="hidden" data-bind="value: CurrentLegData.Passenger.PassengerRequestorID" />
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <input id="btnRequestor" type="button" title="View Clients" data-bind="enable: POEditMode, css: BrowseBtn" onclick="javascript: openWin('radPaxInfoPopup'); return false;" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="tdLabel260">
                                                                                                <label id="lbClientDesc" data-bind="text: CurrentLegData.Client.ClientDescription.kotouppercase()" style="font-weight: bold" class="input_no_bg"></label>
                                                                                                <label id="lbcvClient" style="font-weight: bold" class="alert-text"></label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <label id="lbRequestor" data-bind="text: CurrentLegData.Passenger.PassengerName.kotouppercase()" style="font-weight: bold" class="input_no_bg"></label>
                                                                                                <label id="lbcvRequestor" style="font-weight: bold" class="alert-text"></label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="tdLabel70">
                                                                                                Department
                                                                                            </td>
                                                                                            <td class="tdLabel190">
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <input id="tbDepartment" type="text"  data-bind="value: CurrentLegData.Department.DepartmentCD, enable: POEditMode, event: { change: DepartmentValidator }" maxlength="8" class="text80" onKeyPress="return fnAllowAlphaNumeric(this, event)" />
                                                                                                            <input id="hdnDepartment" type="hidden" data-bind="value: CurrentLegData.Department.DepartmentID" />
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <input id="btnDepartment" type="button" title="View Department" data-bind="enable: POEditMode, css: BrowseBtn" onclick="    javascript: openWin('RadDepartCodePopup'); return false;" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td class="tdLabel80">
                                                                                                Authorization
                                                                                            </td>
                                                                                            <td>
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <input id="tbAuthorization" type="text"  data-bind="value: CurrentLegData.DepartmentAuthorization.AuthorizationCD, enable: POEditMode, event: { change: AuthorizationValidator }" maxlength="8" class="text80" onKeyPress="return fnAllowAlphaNumeric(this, event)" />
                                                                                                            <input id="hdnAuthorization" type="hidden" data-bind="value: CurrentLegData.DepartmentAuthorization.AuthorizationID" />
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <input id="btnAuthorization" type="button" title="View Authorizations" data-bind="enable: POEditMode, css: BrowseBtn" onclick="    javascript: fnValidateAuth(); return false;" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="tdLabel260" valign="top">
                                                                                                <label id="lbDepartmentName" data-bind="text: CurrentLegData.Department.DepartmentName.kotouppercase()" style="font-weight: bold" class="input_no_bg"></label>
                                                                                                <label id="lbcvDepartment" style="font-weight: bold" class="alert-text"></label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <label id="lbAuthorizationDescription" data-bind="text: CurrentLegData.DepartmentAuthorization.DeptAuthDescription.kotouppercase()" style="font-weight: bold" class="input_no_bg"></label>
                                                                                                <label id="lbcvAuthorization" style="font-weight: bold" class="alert-text"></label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="tdLabel70">
                                                                                                Category
                                                                                            </td>
                                                                                            <td class="tdLabel190">
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <input id="tbCategory" type="text"  data-bind="value: CurrentLegData.FlightCatagory.FlightCatagoryCD, enable: POEditMode, event: { change: CategoryValidation }" maxlength="8" class="text80" onKeyPress="return fnAllowAlphaNumeric(this, event)" />
                                                                                                            <input id="hdnCategory" type="hidden" data-bind="value: CurrentLegData.FlightCategoryID" />
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <input id="btnCategory" type="button" title="View Flight Categories" data-bind="enable: POEditMode, css: BrowseBtn" onclick="    javascript: openWin('RadFlightCatagoryPopup'); return false;" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td class="tdLabel80">
                                                                                                Account No.
                                                                                            </td>
                                                                                            <td>
                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <input id="tbAccountNumber" type="text"  data-bind="value: CurrentLegData.Account.AccountNum, enable: POEditMode, event: { change: AccountValidator }" maxlength="60" class="text80" onkeypress="return fnAllowNumericAndDot(this, event)" />
                                                                                                            <input id="hdnAccountNumber" type="hidden" data-bind="value: CurrentLegData.Account.AccountID" />
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <input id="btnAccountNo" type="button" title="View Account Numbers" data-bind="enable: POEditMode, css: BrowseBtn" onclick="    javascript: openWin('radAccountMasterPopup'); return false;" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="tdLabel260" valign="top">
                                                                                                <label id="lbCategoryDescription" data-bind="text: CurrentLegData.FlightCatagory.FlightCatagoryDescription.kotouppercase()" style="font-weight: bold" class="input_no_bg"></label>
                                                                                                <label id="lbcvCategory" style="font-weight: bold" class="alert-text"></label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <label id="lbAccountDescription" data-bind="text: CurrentLegData.Account.AccountDescription.kotouppercase()" style="font-weight: bold" class="input_no_bg"></label>
                                                                                                <label id="lbcvAccountNumber" style="font-weight: bold" class="alert-text"></label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="tdLabel70" valign="top">
                                                                                                Tech Log
                                                                                            </td>
                                                                                            <td>
                                                                                                <input id="tbTechLog" type="text"  data-bind="value: CurrentLegData.TechLog, enable: POEditMode" class="text80" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </fieldset>
                                                                </td>
                                                                <td valign="top" width="30%">
                                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <fieldset>
                                                                                    <legend>Cargo</legend>
                                                                                    <asp:Panel ID="pnlCargo" runat="server">
                                                                                        <table id="tblCargo" runat="server">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table>
                                                                                                        <tr>
                                                                                                            <td class="tdLabel100">
                                                                                                                Out
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <input id="tbCargoOut" type="text"  data-bind="value: CurrentLegData.CargoOut, enable: POEditMode" maxlength="6" class="text50 rt_text" onKeyPress="return fnAllowNumeric(this, event)"/>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="tdLabel100">
                                                                                                                In
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <input id="tbCargoIn" type="text"  data-bind="value: CurrentLegData.CargoIn, enable: POEditMode" maxlength="6" class="text50 rt_text" onKeyPress="return fnAllowNumeric(this, event)" onblur="Javascript:fnCargoCalculate(this); return false;"/>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="tdLabel100">
                                                                                                                Through
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <input id="tbCargoThrough" type="text"  data-bind="value: CurrentLegData.CargoThru, enable: POEditMode" maxlength="7" class="text50 rt_text" onKeyPress="return fnAllowNumeric(this, event)" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <label id="lbcvCargoThrough" style="font-weight: bold" class="alert-text"></label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table cellspacing="0" cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <div class="tblspace_48">
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </asp:Panel>
                                                                                </fieldset>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadPanelBar ID="RadPanelBarAdjustments" ExpandAnimation-Type="None" CollapseAnimation-Type="none"
                                    runat="server" CssClass="postflight-panel-bar712">
                                    <Items>
                                        <telerik:RadPanelItem runat="server" Expanded="true" Text="Adjustments">
                                            <ContentTemplate>
                                                <table width="100%" class="box-bg-normal-pad10" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td valign="top" width="30%">
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <fieldset>
                                                                            <legend>Airframe</legend>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        Hours
                                                                                    </td>
                                                                                    <td>
                                                                                        <input id="tbAirFrameHours" type="text"  data-bind="formatSingleDecimal: CurrentLegData.AirFrameHoursTime, enable: POEditModeAdjustment" onfocus="this.select();" class="text100" onKeyPress="return fnAllowNumericAndChar(this, event,':.')" onblur="return ValidateAdjustmentTimeForTenMin(this, 'Postflight Leg')" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        Cycles
                                                                                    </td>
                                                                                    <td>
                                                                                        <input id="tbAirFramecycles" type="text"  data-bind="value: CurrentLegData.AirframeCycle, enable: POEditModeAdjustment" MaxLength="6" class="text100" onKeyPress="return fnAllowNumeric(this, event)" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </fieldset>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="tblspace_10">
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <fieldset>
                                                                            <legend>Fuel Consumption</legend>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        Tonnes
                                                                                    </td>
                                                                                    <td class="po_leg_radmask">
                                                                                        <input id="tbFuelConsTonnes" type="text" data-bind="value: CurrentLegData.FuelOn, enable: POEditMode" MaxLength="13" class="text100" onchange="checkdecimalValueWithParam(this,'Postflight Leg',6,6);" onKeyPress="return fnAllowNumericAndChar(this, event,'.')" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="tblspace_2">
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </fieldset>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top">
                                                            <fieldset>
                                                                <legend>Engines</legend>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left">
                                                                            Hours
                                                                        </td>
                                                                        <td align="left">
                                                                            Cycles
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            1
                                                                        </td>
                                                                        <td>
                                                                            <input id="tbEngineers1Hr" type="text" data-bind="formatSingleDecimal: CurrentLegData.Engine1HoursTime, enable: POEditModeAdjustment" class="text100" onfocus="this.select();" onKeyPress="return fnAllowNumericAndChar(this, event,':.')" onblur="return ValidateAdjustmentTimeForTenMin(this, 'Postflight Leg')"/>
                                                                        </td>
                                                                        <td>
                                                                            <input id="tbEngineers1Cycle" type="text" data-bind="value: CurrentLegData.Engine1Cycle, enable: POEditModeAdjustment" class="text100" maxlength="6" onfocus="this.select();" onKeyPress="return fnAllowNumeric(this, event)" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            2
                                                                        </td>
                                                                        <td>
                                                                            <input id="tbEngineers2Hr" type="text" data-bind="formatSingleDecimal: CurrentLegData.Engine2HoursTime, enable: POEditModeAdjustment" class="text100" onfocus="this.select();" onKeyPress="return fnAllowNumericAndChar(this, event,':.')" onblur="return ValidateAdjustmentTimeForTenMin(this, 'Postflight Leg')"/>
                                                                        </td>
                                                                        <td>
                                                                            <input id="tbEngineers2Cycle" type="text" data-bind="value: CurrentLegData.Engine2Cycle, enable: POEditModeAdjustment" class="text100" maxlength="6" onfocus="this.select();" onKeyPress="return fnAllowNumeric(this, event)" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            3
                                                                        </td>
                                                                        <td>
                                                                            <input id="tbEngineers3Hr" type="text" data-bind="formatSingleDecimal: CurrentLegData.Engine3HoursTime, enable: POEditModeAdjustment" class="text100" onfocus="this.select();" onKeyPress="return fnAllowNumericAndChar(this, event,':.')" onblur="return ValidateAdjustmentTimeForTenMin(this, 'Postflight Leg')"/>
                                                                        </td>
                                                                        <td>
                                                                            <input id="tbEngineers3Cycle" type="text" data-bind="value: CurrentLegData.Engine3Cycle, enable: POEditModeAdjustment" class="text100" maxlength="6" onfocus="this.select();" onKeyPress="return fnAllowNumeric(this, event)" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            4
                                                                        </td>
                                                                        <td>
                                                                            <input id="tbEngineers4Hr" type="text" data-bind="formatSingleDecimal: CurrentLegData.Engine4HoursTime, enable: POEditModeAdjustment" class="text100" onfocus="this.select();" onKeyPress="return fnAllowNumericAndChar(this, event,':.')" onblur="return ValidateAdjustmentTimeForTenMin(this, 'Postflight Leg')"/>
                                                                        </td>
                                                                        <td>
                                                                            <input id="tbEngineers4Cycle" type="text" data-bind="value: CurrentLegData.Engine4Cycle, enable: POEditModeAdjustment" class="text100" maxlength="6" onfocus="this.select();" onKeyPress="return fnAllowNumeric(this, event)" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                        <td valign="top">
                                                            <fieldset>
                                                                <legend>Reversers</legend>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left">
                                                                            Hours
                                                                        </td>
                                                                        <td align="left">
                                                                            Cycles
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            1
                                                                        </td>
                                                                        <td>
                                                                            <input id="tbReverse1Hr" type="text" data-bind="formatSingleDecimal: CurrentLegData.Re1HoursTime, enable: POEditModeAdjustment" class="text100" onfocus="this.select();" onKeyPress="return fnAllowNumericAndChar(this, event,':.')" onblur="return ValidateAdjustmentTimeForTenMin(this, 'Postflight Leg')"/>
                                                                        </td>
                                                                        <td>
                                                                            <input id="tbReverse1Cycle" type="text" data-bind="value: CurrentLegData.Re1Cycle, enable: POEditModeAdjustment" class="text100" maxlength="6" onfocus="this.select();" onKeyPress="return fnAllowNumeric(this, event)" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            2
                                                                        </td>
                                                                        <td>
                                                                            <input id="tbReverse2Hr" type="text" data-bind="formatSingleDecimal: CurrentLegData.Re2HoursTime, enable: POEditModeAdjustment" class="text100" onfocus="this.select();" onKeyPress="return fnAllowNumericAndChar(this, event,':.')" onblur="return ValidateAdjustmentTimeForTenMin(this, 'Postflight Leg')"/>
                                                                        </td>
                                                                        <td>
                                                                            <input id="tbReverse2Cycle" type="text" data-bind="value: CurrentLegData.Re2Cycle, enable: POEditModeAdjustment" class="text100" maxlength="6" onfocus="this.select();" onKeyPress="return fnAllowNumeric(this, event)" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            3
                                                                        </td>
                                                                        <td>
                                                                            <input id="tbReverse3Hr" type="text" data-bind="formatSingleDecimal: CurrentLegData.Re3HoursTime, enable: POEditModeAdjustment" class="text100" onfocus="this.select();" onKeyPress="return fnAllowNumericAndChar(this, event,':.')" onblur="return ValidateAdjustmentTimeForTenMin(this, 'Postflight Leg')"/>
                                                                        </td>
                                                                        <td>
                                                                            <input id="tbReverse3Cycle" type="text" data-bind="value: CurrentLegData.Re3Cycle, enable: POEditModeAdjustment" class="text100" maxlength="6" onfocus="this.select();" onKeyPress="return fnAllowNumeric(this, event)" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            4
                                                                        </td>
                                                                        <td>
                                                                            <input id="tbReverse4Hr" type="text" data-bind="formatSingleDecimal: CurrentLegData.Re4HoursTime, enable: POEditModeAdjustment" class="text100" onfocus="this.select();" onKeyPress="return fnAllowNumericAndChar(this, event,':.')" onblur="return ValidateAdjustmentTimeForTenMin(this, 'Postflight Leg')"/>
                                                                        </td>
                                                                        <td>
                                                                            <input id="tbReverse4Cycle" type="text" data-bind="value: CurrentLegData.Re4Cycle, enable: POEditModeAdjustment" class="text100" maxlength="6" onfocus="this.select();" onKeyPress="return fnAllowNumeric(this, event)" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <div class="nav-space">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <a href="https://www.eurocontrol.int/articles/small-emitters-tool" target="_blank">
                                                                Click for Method A / Method B Calculator </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelBar>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <uc:BottomButtonControls ID="UCBottomButtonControls" runat="server" />
    </div>
</asp:Content>
