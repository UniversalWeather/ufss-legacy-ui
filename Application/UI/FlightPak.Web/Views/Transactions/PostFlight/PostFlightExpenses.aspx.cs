﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.PostflightService;
using System.Globalization;
using System.Text.RegularExpressions;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.ViewModels;
using FlightPak.Web.ViewModels.Postflight;
using Newtonsoft.Json;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.Views.Transactions.PostFlight
{
    public partial class PostFlightExpenses : BaseSecuredPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PostflightTripManager.CheckSessionAndRedirect();
            PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
            if (Session[WebSessionKeys.CurrentPostflightLog] != null)
            {
                pfViewModel = (PostflightLogViewModel)Session[WebSessionKeys.CurrentPostflightLog];
            }
            else
            {
                pfViewModel.PostflightMain = new PostflightMainViewModel(PostflightTripManager.getUserPrincipal()._ApplicationDateFormat);
                pfViewModel.PostflightMain = PostflightTripManager.CreateOrCancelTrip(pfViewModel.PostflightMain);
            }
            hdnPostflightMainInitializationObject.Value = Microsoft.Security.Application.Encoder.HtmlEncode(JsonConvert.SerializeObject(pfViewModel));
            UserPrincipalViewModel userPrincipal = PostflightTripManager.getUserPrincipal();
            hdnUserPrincipalInitializationObject.Value = Microsoft.Security.Application.Encoder.HtmlEncode(JsonConvert.SerializeObject(userPrincipal));
            hdnPostflightExpensesObject.Value = Microsoft.Security.Application.Encoder.HtmlEncode(JsonConvert.SerializeObject(new PostflightLegExpenseViewModel()));
        }
    }
}
