﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.PostflightService;

//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Transactions.PostFlight
{
    public partial class PostFlightExpensesPopup : BaseSecuredPage
    {
        private ExceptionManager exManager;
        string DateFormat = "MM/dd/yyyy";

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                            DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;

                        if (!IsPostBack)
                        {
                            lbMessage.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            ddlLegs.Items.Clear();
                            ddlLegs.Enabled = false;

                            if (UserPrincipal.Identity._fpSettings._IsDeactivateHomeBaseFilter == false)
                            {
                                chkHomebase.Checked = true;
                            }

                            BindData(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POExpenseCatalog);
                }
            }
        }

        protected void Search_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindData(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POExpenseCatalog);
                }
            }
        }

        protected void Search_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                switch (e.CommandName)
                {
                    case RadGrid.InitInsertCommandName:
                        e.Canceled = true;
                        InsertSelectedRow();
                        break;
                    case RadGrid.FilterCommandName:
                        Pair filterPair = (Pair)e.CommandArgument;
                        Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                        break;
                    default:
                        break;
                }
            }
        }

        protected void Search_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                InsertSelectedRow();
            }
        }

        private void InsertSelectedRow()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                // Clear the Existing Session Values
                Session.Remove("POSTFLIGHTEXPENSE");

                using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
                {
                    PostflightExpense expense = new PostflightExpense();
                    GridDataItem item = (GridDataItem)dgSearch.SelectedItems[0];

                    expense.PostflightExpenseID = Convert.ToInt64(item["PostflightExpenseID"].Text);
                    var result = objService.GetPOExpense(expense);
                    Session["POSTFLIGHTEXPENSE"] = (PostflightExpense)result.EntityInfo;
                    RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('BindfromExpensePopup');");
                }
            }
        }

        protected void Search_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    try
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem item = (GridDataItem)e.Item;
                            if (item["PurchaseDt"] != null && !string.IsNullOrEmpty(item["PurchaseDt"].Text) && item["PurchaseDt"].Text != "&nbsp;")
                            {
                                DateTime PurchaseDt = (DateTime)DataBinder.Eval(item.DataItem, "PurchaseDt");
                                if (PurchaseDt != null)
                                    item["PurchaseDt"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", PurchaseDt));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //Manually Handled
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindData(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POExpenseCatalog);
                }
            }
        }

        /// <summary>
        /// Method to Bind Data from DB
        /// </summary>
        protected void BindData(bool isDataBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
                {
                    var ObjRetval = objService.GetExpList();

                    if (ObjRetval != null && ObjRetval.ReturnFlag)
                    {
                        Int64? _homeBaseID = null;
                        DateTime? _goToDTTM = null;
                        string _fuelSelected = "0";
                        string _tailNum = string.Empty;
                        string _icaoID = string.Empty;
                        Int64? _logID = null;
                        Int64? _legID = null;
                        string _paymentTypeID = string.Empty;
                        string _payableVendorID = string.Empty;
                        string _fuelLocID = string.Empty;
                        string _accNoID = string.Empty;
                        string _invoiceNo = string.Empty;
                        decimal? _expAmount = null;

                        if (chkHomebase.Checked == true)
                            _homeBaseID = UserPrincipal.Identity._fpSettings._HomebaseID;

                        TextBox tbGoToDTTM = (TextBox)ucGoToDate.FindControl("tbDate");
                        if (!string.IsNullOrEmpty(tbGoToDTTM.Text))
                            _goToDTTM = DateTime.ParseExact(tbGoToDTTM.Text, DateFormat, CultureInfo.InvariantCulture);

                        _fuelSelected = rblSearch.SelectedValue;

                        if (!string.IsNullOrEmpty(tbTailNumber.Text))
                            _tailNum = tbTailNumber.Text.Trim();

                        if (!string.IsNullOrEmpty(tbICAO.Text))
                            _icaoID = tbICAO.Text.Trim();

                        if (!string.IsNullOrEmpty(hdnPOLogID.Value))
                            _logID = Convert.ToInt64(hdnPOLogID.Value);

                        if (ddlLegs.Items.Count > 0)
                            _legID = Convert.ToInt64(ddlLegs.SelectedValue);

                        if (!string.IsNullOrEmpty(tbPaymentType.Text))
                            _paymentTypeID = tbPaymentType.Text.Trim();

                        if (!string.IsNullOrEmpty(tbPayVendor.Text))
                            _payableVendorID = tbPayVendor.Text.Trim();

                        if (!string.IsNullOrEmpty(tbFuelLocator.Text))
                            _fuelLocID = tbFuelLocator.Text.Trim();

                        if (!string.IsNullOrEmpty(tbAccountNumber.Text))
                            _accNoID = tbAccountNumber.Text.Trim();

                        if (!string.IsNullOrEmpty(tbInvoiceNo.Text))
                            _invoiceNo = tbInvoiceNo.Text;

                        if (!string.IsNullOrEmpty(tbExpAmount.Text))
                            _expAmount = Convert.ToDecimal(tbExpAmount.Text);

                        List<GetPostflightExpenseList> FilteredList = new List<GetPostflightExpenseList>();
                        FilteredList = ObjRetval.EntityList.Where(x => x.HomebaseID == (_homeBaseID != null ? _homeBaseID : x.HomebaseID)
                            && x.PurchaseDT == (_goToDTTM != null ? _goToDTTM : x.PurchaseDT)
                            && x.TailNum == (_tailNum != string.Empty ? _tailNum : x.TailNum)
                            && x.IcaoID == (_icaoID != string.Empty ? _icaoID : x.IcaoID)
                            && x.POLogID == (_logID != null ? _logID : x.POLogID)
                            && x.POLegID == (_legID != null ? _legID : x.POLegID)
                            && x.PaymentTypeCD == (_paymentTypeID != string.Empty ? _paymentTypeID : x.PaymentTypeCD)
                            && x.VendorCD == (_payableVendorID != string.Empty ? _payableVendorID : x.VendorCD)
                            && x.FuelLocatorCD == (_fuelLocID != string.Empty ? _fuelLocID : x.FuelLocatorCD)
                            && x.AccountNum == (_accNoID != string.Empty ? _accNoID : x.AccountNum)
                            && x.InvoiceNUM == (_invoiceNo != string.Empty ? _invoiceNo : x.InvoiceNUM)
                            && x.ExpenseAMT == (_expAmount != null ? _expAmount : x.ExpenseAMT)).ToList();

                        if (_fuelSelected == "1")
                            FilteredList = FilteredList.Where(x => x.FuelQTY > 0).ToList();
                        else if (_fuelSelected == "2")
                            FilteredList = FilteredList.Where(x => x.FuelQTY == 0).ToList();

                        dgSearch.DataSource = FilteredList.Where(x => x.IsDeleted == false).ToList();
                        if (isDataBind)
                            dgSearch.DataBind();
                    }
                }
            }
        }

        #region FILTERS
        /// <summary>
        /// Method to validate TailNumber
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TailNo_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbcvTailNumber.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        //tbTypeCode.Text = string.Empty;
                        hdnTailNo.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbTailNumber.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient TailService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                //var TailVal = TailService.GetFleetProfileList().EntityList.Where(x => x.TailNum.ToString().ToUpper().Trim().Equals(tbTailNumber.Text.ToUpper().Trim())).ToList();
                                var TailVal = TailService.GetAllFleetWithFilters(0, false, "B", string.Empty, 0, tbTailNumber.Text, 0).EntityList;
                                if (TailVal != null && TailVal.Count > 0)
                                {
                                    hdnAircraftID.Value = TailVal[0].AircraftID.Value.ToString();
                                    //var AircraftVal = TailService.GetAircraftByAircraftID((long)TailVal[0].AircraftID).EntityList;
                                    hdnTailNo.Value = TailVal[0].FleetID.ToString();
                                }
                                else
                                {
                                    lbcvTailNumber.Text = System.Web.HttpUtility.HtmlEncode("Invalid Aircraft Tail No.");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbTailNumber);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POExpenseCatalog);
                }
            }

        }

        /// <summary>
        /// Method to validate ICAO ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ICAO_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbICAODesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbICAO.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnICAO.Value = string.Empty;
                        if (!string.IsNullOrEmpty(tbICAO.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetAirportByAirportICaoID(tbICAO.Text.Trim()).EntityList;
                                if (objRetVal != null && objRetVal.Count() > 0)
                                {
                                    tbICAO.Text = objRetVal[0].IcaoID.ToString();
                                    hdnICAO.Value = objRetVal[0].AirportID.ToString();
                                    if (objRetVal[0].AirportName != null)
                                        lbICAODesc.Text = System.Web.HttpUtility.HtmlEncode(objRetVal[0].AirportName.ToString().ToUpper().Trim());
                                }
                                else
                                {
                                    lbICAO.Text = System.Web.HttpUtility.HtmlEncode("Invalid ICAO Code");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbICAO);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POExpenseCatalog);
                }
            }
        }

        /// <summary>
        /// Event to populate Log number based on text change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LogNo_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbLogNo.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnPOLogID.Value = string.Empty;
                        ddlLegs.Enabled = false;
                        ddlLegs.Items.Clear();

                        if (!string.IsNullOrEmpty(tbLogNo.Text))
                        {
                            if (tbLogNo.Text.Trim() == "0")
                            {
                                LogNo_TextChangedToZero();
                            }
                            else
                            {
                                using (PostflightServiceClient POService = new PostflightServiceClient())
                                {
                                    PostflightMain trip = new PostflightMain();
                                    trip.LogNum = Convert.ToInt64(tbLogNo.Text);
                                    var retTrip = (ReturnValueOfPostflightMain)WCFCall(() => POService.GetTrip(trip));

                                    if (retTrip.ReturnFlag && retTrip.EntityInfo != null && retTrip.EntityInfo.POLogID > 0)
                                    {
                                        if (retTrip.EntityInfo.LogNum != null)
                                            tbLogNo.Text = retTrip.EntityInfo.LogNum.ToString();
                                        else
                                            tbLogNo.Text = "0";
                                        hdnPOLogID.Value = retTrip.EntityInfo.POLogID.ToString();

                                        if (retTrip.EntityInfo.PostflightLegs != null && retTrip.EntityInfo.PostflightLegs.Count > 0)
                                        {
                                            ddlLegs.Enabled = true;
                                            ddlLegs.DataSource = retTrip.EntityInfo.PostflightLegs.Where(x => x.IsDeleted == false).ToList();
                                            ddlLegs.DataTextField = "LegNUM";
                                            ddlLegs.DataValueField = "POLegID";
                                            ddlLegs.DataBind();
                                        }
                                    }
                                    else
                                    {
                                        lbLogNo.Text = System.Web.HttpUtility.HtmlEncode("Invalid Log Number");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbLogNo);
                                    }
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POExpenseCatalog);
                }
            }
        }

        /// <summary>
        /// Method to validate Payment Type Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PaymentType_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbPaymentTypeDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbPaymentType.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnPaymentType.Value = string.Empty;
                        if (!string.IsNullOrEmpty(tbPaymentType.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetAllPaymentTypeWithFilters(0, tbPaymentType.Text, false).EntityList;
                                //.GetPaymentType().EntityList.Where(x => x.PaymentTypeCD.ToString().ToUpper().Trim().Equals(tbPaymentType.Text.ToUpper().Trim())).ToList();
                                if (objRetVal != null && objRetVal.Count() > 0)
                                {
                                    tbPaymentType.Text = objRetVal[0].PaymentTypeCD.ToString();
                                    hdnPaymentType.Value = objRetVal[0].PaymentTypeID.ToString();
                                    if (!string.IsNullOrEmpty(objRetVal[0].PaymentTypeDescription))
                                        lbPaymentTypeDesc.Text = System.Web.HttpUtility.HtmlEncode(objRetVal[0].PaymentTypeDescription.ToString().ToUpper());
                                }
                                else
                                {
                                    lbPaymentType.Text = System.Web.HttpUtility.HtmlEncode("Invalid Payment Type Code");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbPaymentType);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POExpenseCatalog);
                }
            }
        }

        /// <summary>
        /// Method to validate Vendor Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PayVendor_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbPayVendor.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnPayVendor.Value = string.Empty;
                        lbPayVendorDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                        if (!string.IsNullOrEmpty(tbPayVendor.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetAllVendorWithFilters(string.Empty, 0, tbPayVendor.Text, string.Empty, false).EntityList;
                                    //.GetPayableVendorInfo().EntityList.Where(x => x.VendorCD.ToString().ToUpper().Trim().Equals(tbPayVendor.Text.ToUpper().Trim())).ToList();
                                if (objRetVal != null && objRetVal.Count() > 0)
                                {
                                    tbPayVendor.Text = objRetVal[0].VendorCD.ToString().ToUpper();
                                    hdnPayVendor.Value = objRetVal[0].VendorID.ToString();
                                    if (objRetVal[0].Name != null)
                                        lbPayVendorDesc.Text = System.Web.HttpUtility.HtmlEncode(objRetVal[0].Name.ToString().ToUpper());
                                }
                                else
                                {
                                    lbPayVendor.Text = System.Web.HttpUtility.HtmlEncode("Invalid Vendor Code");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbPayVendor);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POExpenseCatalog);
                }
            }
        }

        /// <summary>
        /// Method to validate Account Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AccountNumber_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbAccountNumber.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbAccDescription.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnAccountNumber.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbAccountNumber.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetAccountWithFilters(tbAccountNumber.Text, 0, string.Empty, false).EntityList;
                                    //.GetAllAccountList().EntityList.Where(x => x.AccountNum.ToString().ToUpper().Trim().Equals(tbAccountNumber.Text.ToUpper().Trim())).ToList();
                                if (objRetVal != null && objRetVal.Count() > 0)
                                {
                                    hdnAccountNumber.Value = objRetVal[0].AccountID.ToString();
                                    tbAccountNumber.Text = objRetVal[0].AccountNum.ToString().ToUpper();

                                    if (!string.IsNullOrEmpty(objRetVal[0].AccountDescription))
                                        lbAccDescription.Text = System.Web.HttpUtility.HtmlEncode(objRetVal[0].AccountDescription.ToString().ToUpper());
                                }
                                else
                                {
                                    lbAccountNumber.Text = System.Web.HttpUtility.HtmlEncode("Invalid Account Number");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbAccountNumber);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POExpenseCatalog);
                }
            }
        }

        /// <summary>
        /// Method to validate Fuel Locator Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FuelLocator_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbFuelLocator.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnFuelLocator.Value = string.Empty;
                        lbFuelLocatorDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        if (!string.IsNullOrEmpty(tbFuelLocator.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetFuelLocatorWithFilters(0, tbFuelLocator.Text, 0, false).EntityList;
                                    //.GetFuelLocatorInfo().EntityList.Where(x => x.FuelLocatorCD.ToString().ToUpper().Trim().Equals(tbFuelLocator.Text.ToUpper().Trim())).ToList();
                                if (objRetVal != null && objRetVal.Count() > 0)
                                {
                                    tbFuelLocator.Text = objRetVal[0].FuelLocatorCD.ToString();
                                    hdnFuelLocator.Value = objRetVal[0].FuelLocatorID.ToString();

                                    if (!string.IsNullOrEmpty(objRetVal[0].FuelLocatorDescription))
                                        lbFuelLocatorDesc.Text = System.Web.HttpUtility.HtmlEncode(objRetVal[0].FuelLocatorDescription.ToUpper());
                                }
                                else
                                {
                                    lbFuelLocator.Text = System.Web.HttpUtility.HtmlEncode("Invalid Fuel Locator Code");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbFuelLocator);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POExpenseCatalog);
                }
            }
        }

        private void LogNo_TextChangedToZero()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ddlLegs.Items.Clear();
                        ddlLegs.Enabled = false;
                        ddlLegs.SelectedValue = string.Empty;
                        hdnPOLogID.Value = string.Empty;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POExpenseCatalog);
                }
            }
        }
        #endregion

        protected void dgSearch_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgSearch, Page.Session);
        }
    }
}