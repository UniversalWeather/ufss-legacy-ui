﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using FlightPak.Web.PostflightService;
using Telerik.Web.UI;
//For Tracing and Exception Handling
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.Services;
using System.Web.Script.Services;
using FlightPak.Web.ViewModels;
using FlightPak.Web.ViewModels.Postflight;
using System.Web;
using FlightPak.Web.Framework.Constants;
using System.Net;
using Omu.ValueInjecter;
using FlightPak.Web.ViewModels.MasterData;
using FlightPak.Web.BusinessLite;
using FlightPak.Web.Framework.Helpers;
namespace FlightPak.Web.Views.Transactions.PostFlight
{
    public partial class ExpenseCatalog : BaseSecuredPage
    {
        private static ExceptionManager exManager;
        private static UserPrincipalViewModel _UserPrincipal
        {
            get { return TripManagerBase.getUserPrincipal(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RadAjaxManager manager = RadAjaxManager.GetCurrent(Page);
            if (!IsLogPopup)
            {
                ContentPlaceHolder contentPlaceHolder = (ContentPlaceHolder)Master.Master.FindControl("MainContent");

                RadPanelBar pnlTracker = (RadPanelBar)contentPlaceHolder.FindControl("pnlTracker");
                RadPanelItem pnlTrackItem = pnlTracker.FindItemByValue("pnlItemTracker");
                pnlTrackItem.Expanded = false;
                pnlTracker.Enabled = false;

                RadPanelBar pnlException = (RadPanelBar)contentPlaceHolder.FindControl("pnlException");
                RadPanelItem pnlExpItem = pnlException.FindItemByValue("pnlItemException");
                pnlExpItem.Expanded = false;
                pnlException.Enabled = false;

            }
            PostflightExpenseCatalogViewModel PostflightLegExpenseVM = new PostflightExpenseCatalogViewModel();
            var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(PostflightLegExpenseVM);
            hdnPOLegExpenseInitializationObject.Value = Microsoft.Security.Application.Encoder.HtmlEncode(json);
        }

        private static PostflightExpenseCatalogViewModel GetPostflightExpenseData(long expenseId)
        {
            PostflightExpenseCatalogViewModel POLegExpenseVM = new PostflightExpenseCatalogViewModel();
            using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
            {
                PostflightExpense expense = new PostflightExpense();
                expense.PostflightExpenseID = expenseId;
                var retResult = objService.GetPOExpense(expense);
                if (retResult != null && retResult.ReturnFlag)
                {
                    var result = retResult.EntityInfo;

                    POLegExpenseVM = (PostflightExpenseCatalogViewModel)POLegExpenseVM.InjectFrom(result);
                    POLegExpenseVM.Fleet = result.Fleet != null ? (FleetViewModel)new FleetViewModel().InjectFrom(result.Fleet) : new FleetViewModel();
                    POLegExpenseVM.Account = result.Account != null ? (AccountViewModel)new AccountViewModel().InjectFrom(result.Account) : new AccountViewModel();
                    POLegExpenseVM.Airport = result.Airport != null ? (AirportViewModel)new AirportViewModel().InjectFrom(result.Airport) : new AirportViewModel();
                    POLegExpenseVM.FBO = result.FBO != null ? (FBOViewModel)new FBOViewModel().InjectFrom(result.FBO) : new FBOViewModel();
                    POLegExpenseVM.FuelLocator = result.FuelLocator != null ? (FuelLocatorViewModel)new FuelLocatorViewModel().InjectFrom(result.FuelLocator) : new FuelLocatorViewModel();
                    POLegExpenseVM.PaymentType = result.PaymentType != null ? (PaymentTypeViewModel)new PaymentTypeViewModel().InjectFrom(result.PaymentType) : new PaymentTypeViewModel();
                    POLegExpenseVM.Crew = result.Crew != null ? (CrewViewModel)new CrewViewModel().InjectFrom(result.Crew) : new CrewViewModel();
                    POLegExpenseVM.PayableVendor = result.Vendor != null ? (PayableVendorViewModel)new PayableVendorViewModel().InjectFrom(result.Vendor) : new PayableVendorViewModel();
                    POLegExpenseVM.FlightCategory = result.FlightCatagory != null ? (FlightCatagoryViewModel)new FlightCatagoryViewModel().InjectFrom(result.FlightCatagory) : new FlightCatagoryViewModel();
                    if (result.HomebaseID.HasValue)
                        POLegExpenseVM.Homebase = DBCatalogsDataLoader.GetHomebaseInfo_FromHomebaseId(result.HomebaseID.Value);
                    if (result.PostflightMain != null && result.PostflightMain.LogNum != null)
                        POLegExpenseVM.LogNum = result.PostflightMain.LogNum.Value;
                    else
                        POLegExpenseVM.LogNum = 0;

                    if (result.PostflightMain != null && result.PostflightMain.PostflightLegs != null)
                    {
                        foreach (var POleg in result.PostflightMain.PostflightLegs.OrderBy(u => u.LegNUM))
                        {
                            PostflightLegViewModel POlegVM = new PostflightLegViewModel();
                            POlegVM = (PostflightLegViewModel)POlegVM.InjectFrom(POleg);
                            POLegExpenseVM.PostflightLeg.Add(POlegVM);
                        }
                    }
                    if (result.PostflightNotes != null)
                    {
                        foreach (PostflightNote pnote in result.PostflightNotes)
                        {
                            PostflightNoteViewModel pnoteVM = new PostflightNoteViewModel();
                            pnoteVM = (PostflightNoteViewModel)pnoteVM.InjectFrom(pnote);
                            POLegExpenseVM.PostflightNotes.Add(pnoteVM);
                        }
                    }
                    if (POLegExpenseVM.LastUpdTS != null)
                    {
                        using (CalculationService.CalculationServiceClient calcSvc = new FlightPak.Web.CalculationService.CalculationServiceClient())
                        {
                            UserPrincipalViewModel upViewModel = TripManagerBase.getUserPrincipal();
                            DateTime? dt = POLegExpenseVM.LastUpdTS;
                            dt = calcSvc.GetGMT(upViewModel._airportId, (DateTime)POLegExpenseVM.LastUpdTS, false, false);                            
                            POLegExpenseVM.LastUpdatedBy = "Last Modified: " + POLegExpenseVM.LastUpdUID + " " + dt.FSSParseDateTimeString(upViewModel._ApplicationDateFormat + " HH:mm") + " LCL" + " (" + POLegExpenseVM.LastUpdTS.FSSParseDateTimeString(upViewModel._ApplicationDateFormat + " HH:mm") + " UTC)";
                        }
                    }
                    HttpContext.Current.Session[WebSessionKeys.PostflightExpenseCatalog] = POLegExpenseVM;
                }
            }
            return POLegExpenseVM;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightExpenseCatalogViewModel> ExpenseSearch(Int64? SlipNUM)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<PostflightExpenseCatalogViewModel>>(() =>
               {
                   FSSOperationResult<PostflightExpenseCatalogViewModel> ResultObj = new FSSOperationResult<PostflightExpenseCatalogViewModel>();
                   if (ResultObj.IsAuthorized() == false)
                   {
                       return ResultObj;
                   }
                   PostflightExpenseCatalogViewModel POLegExpenseVM = new PostflightExpenseCatalogViewModel();
                   using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
                   {
                       var ObjRetval = objService.GetExpList();
                       if (ObjRetval != null && ObjRetval.ReturnFlag)
                       {
                           var retVal = ObjRetval.EntityList.Where(x => x.IsDeleted == false && x.SlipNUM == SlipNUM).SingleOrDefault();
                           if (retVal != null)
                           {
                               POLegExpenseVM = GetPostflightExpenseData(retVal.PostflightExpenseID);

                               ResultObj.Success = true;
                               ResultObj.StatusCode = HttpStatusCode.OK;
                               ResultObj.Result = POLegExpenseVM;
                           }
                           else
                           {
                               ResultObj.Success = false;
                               ResultObj.StatusCode = HttpStatusCode.OK;
                           }
                       }
                   }

                   return ResultObj;
               }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightExpenseCatalogViewModel> ExpenseSearchByExpenseId(long ExpenseId)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<PostflightExpenseCatalogViewModel>>(() =>
            {
                FSSOperationResult<PostflightExpenseCatalogViewModel> ResultObj = new FSSOperationResult<PostflightExpenseCatalogViewModel>();
                if (ResultObj.IsAuthorized() == false)
                {
                    return ResultObj;
                }
                PostflightExpenseCatalogViewModel POExpenseVM = new PostflightExpenseCatalogViewModel();
                POExpenseVM = GetPostflightExpenseData(ExpenseId);
                ResultObj.Success = true;
                ResultObj.StatusCode = HttpStatusCode.OK;
                ResultObj.Result = POExpenseVM;
                return ResultObj;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> DeleteExpenseLog(Int64 POExpenseID)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<bool>>(() =>
               {
                   FSSOperationResult<bool> ResultObj = new FSSOperationResult<bool>();
                   if (ResultObj.IsAuthorized() == false)
                   {
                       return ResultObj;
                   }

                   using (PostflightServiceClient objService = new PostflightServiceClient())
                   {
                       var ReturnValue = objService.DeleteExpenseCatalog(POExpenseID);

                       if (ReturnValue != null && ReturnValue.ReturnFlag == true)
                       {
                           ResultObj.StatusCode = HttpStatusCode.OK;
                           ResultObj.Success = true;
                       }
                       else
                       {
                           ResultObj.StatusCode = HttpStatusCode.OK;
                           ResultObj.Success = false;
                       }
                   }

                   return ResultObj;
               }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightExpenseCatalogViewModel> Save(PostflightExpenseCatalogViewModel ExpenseCatalogVM)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<PostflightExpenseCatalogViewModel>>(() =>
               {
                   FSSOperationResult<PostflightExpenseCatalogViewModel> ResultObj = new FSSOperationResult<PostflightExpenseCatalogViewModel>();
                   if (ResultObj.IsAuthorized() == false)
                   {
                       return ResultObj;
                   }
                   PostflightExpense Expense = new PostflightExpense();
                   Expense = (PostflightExpense)Expense.InjectFrom(ExpenseCatalogVM);
                   if (Expense.PostflightExpenseID == 0)
                   {
                       Expense.State = TripEntityState.Added;
                   }
                   else if (Expense.PostflightExpenseID > 0)
                   {
                        Expense.State = TripEntityState.Modified;
                   }

                   if (Expense.POLogID == null)
                   {
                       if (Expense.State == TripEntityState.Added)
                       {
                           Expense.LegNUM = 0;
                           Expense.POLegID = null;
                       }
                   }
                   else
                   {
                       if (Expense.LegNUM == null) Expense.LegNUM = 0;
                   }

                   PostflightNote expNote = new PostflightNote();
                   if (ExpenseCatalogVM.PostflightExpenseID == 0 && ExpenseCatalogVM.PostflightNote.PostflightNoteID == 0)
                   {
                       if (!string.IsNullOrWhiteSpace(ExpenseCatalogVM.PostflightNote.Notes))
                       {

                           expNote.Notes = ExpenseCatalogVM.PostflightNote.Notes;
                           expNote.State = TripEntityState.Added;
                           Expense.PostflightNotes = new List<PostflightNote>();
                           Expense.PostflightNotes.Add(expNote);
                       }
                   }
                   else if (Expense.State == TripEntityState.Added)
                   {
                       if (!string.IsNullOrWhiteSpace(ExpenseCatalogVM.PostflightNote.Notes))
                       {
                           expNote.Notes = ExpenseCatalogVM.PostflightNote.Notes;
                           expNote.State = TripEntityState.Added;
                           Expense.PostflightNotes = new List<PostflightNote>();
                           Expense.PostflightNotes.Add(expNote);
                       }
                   }
                   else if (Expense.PostflightExpenseID > 0)
                   {
                       expNote.PostflightExpenseID = Expense.PostflightExpenseID;
                       if (ExpenseCatalogVM.PostflightNote.PostflightNoteID > 0)
                       {
                           long PONoteID = Convert.ToInt64(ExpenseCatalogVM.PostflightNote.PostflightNoteID);
                           if (PONoteID > 0)
                           {
                               expNote.PostflightNoteID = PONoteID;
                               expNote.State = TripEntityState.Modified;
                           }
                       }
                       else
                           expNote.State = TripEntityState.Added;

                       expNote.Notes = ExpenseCatalogVM.PostflightNote.Notes;
                       Expense.PostflightNotes = new List<PostflightNote>();
                       Expense.PostflightNotes.Add(expNote);
                   }
                   using (PostflightServiceClient Service = new PostflightServiceClient())
                   {
                       var ReturnValue = Service.UpdateExpenseCatalog(Expense);
                       if (ReturnValue != null && ReturnValue.ReturnFlag == true)
                       {
                           if (ExpenseCatalogVM.FromExpenseTab) PostflightTripManager.SearchPostFlightLog((long)ExpenseCatalogVM.LogNum);
                           ExpenseCatalogVM = GetPostflightExpenseData(ReturnValue.EntityInfo.PostflightExpenseID);
                           ResultObj.StatusCode = HttpStatusCode.OK;
                           ResultObj.Success = true;
                           ResultObj.Result = ExpenseCatalogVM;
                       }
                       else
                       {
                           ResultObj.StatusCode = HttpStatusCode.OK;
                           ResultObj.Success = false;
                       }
                   }
                   return ResultObj;
               }, FlightPak.Common.Constants.Policy.UILayer);
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> lnkPreview_Click(string logNum, string legNum, string slipNum)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<bool>>(() =>
               {
                   FSSOperationResult<bool> ResultObj = new FSSOperationResult<bool>();
                   if (ResultObj.IsAuthorized() == false)
                   {
                       return ResultObj;
                   }
                   HttpContext.Current.Session["TabSelect"] = "PostFlight";
                   HttpContext.Current.Session["REPORT"] = "RptPOSTExpenseCatalog";
                   HttpContext.Current.Session["FORMAT"] = "PDF";
                   HttpContext.Current.Session["PARAMETER"] = string.Format("SlipNum={0};LogNum={1};LegNum={2};UserCD={3}", slipNum, logNum, legNum, _UserPrincipal._name);
                   ResultObj.Success = true;
                   ResultObj.StatusCode = HttpStatusCode.OK;
                   return ResultObj;
               }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightExpenseCatalogViewModel> GetPostFlightExpenseCatalog()
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<PostflightExpenseCatalogViewModel>>(() =>
               {
                   FSSOperationResult<PostflightExpenseCatalogViewModel> ResultObj = new FSSOperationResult<PostflightExpenseCatalogViewModel>();
                   if (ResultObj.IsAuthorized() == false)
                   {
                       return ResultObj;
                   }
                   if (HttpContext.Current.Session[WebSessionKeys.PostflightExpenseCatalog] != null)
                   {
                       PostflightExpenseCatalogViewModel POLegExpenseVM = (PostflightExpenseCatalogViewModel)HttpContext.Current.Session[WebSessionKeys.PostflightExpenseCatalog];
                       ResultObj.StatusCode = HttpStatusCode.OK;
                       ResultObj.Success = true;
                       ResultObj.Result = POLegExpenseVM;
                   }
                   return ResultObj;
               }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightExpenseCatalogViewModel> AddNewExpenseCatalog()
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<PostflightExpenseCatalogViewModel>>(() =>
               {
                   FSSOperationResult<PostflightExpenseCatalogViewModel> ResultObj = new FSSOperationResult<PostflightExpenseCatalogViewModel>();
                   if (ResultObj.IsAuthorized() == false)
                   {
                       return ResultObj;
                   }
                   PostflightExpenseCatalogViewModel POLegExpenseVM = new PostflightExpenseCatalogViewModel();
                   POLegExpenseVM.HomebaseID = _UserPrincipal._homeBaseId.Value;
                   POLegExpenseVM.Homebase = DBCatalogsDataLoader.GetHomebaseInfo_FromHomebaseId(POLegExpenseVM.HomebaseID.Value);
                   POLegExpenseVM.PurchaseDT = DateTime.Today;
                   POLegExpenseVM.AccountPeriod = DateTime.Now.Date.Year.ToString().Substring(2, 2).ToString() + "" + DateTime.Now.Date.Month.ToString("d2");
                   POLegExpenseVM.LogNum = 0;
                   ResultObj.StatusCode = HttpStatusCode.OK;
                   ResultObj.Success = true;
                   ResultObj.Result = POLegExpenseVM;
                   return ResultObj;
               }, FlightPak.Common.Constants.Policy.UILayer);
        }
    }
}
