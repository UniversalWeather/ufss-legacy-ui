﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/PostFlight.Master"
    AutoEventWireup="true" CodeBehind="PostFlightExpenses.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.PostFlight.PostFlightExpenses"
    MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Framework/Masters/PostFlight.Master" %>
<%@ Register Src="~/UserControls/Postflight/UCPostflightSearch.ascx" TagName="Search" TagPrefix="UCPostflight" %>
<%@ Register Src="~/UserControls/Postflight/UCPostflightBottomButtonControls.ascx" TagPrefix="uc" TagName="UCPostflightBottomButtonControls" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PostFlightHeadContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <style type="text/css">
            .prfl-nav-icons {
            left:-2px !important;
            }
        </style>                
        <%= Scripts.Render("~/bundles/postflightexpenses") %>
        <script type="text/javascript">
            function InitializePostflightExpenseVM()
            {
                var schemaElement = document.getElementById('<%=hdnPostflightExpensesObject.ClientID%>').value;
                var jsonSchema = JSON.parse(htmlDecode(schemaElement));
                self.CurrentLegExpenseData = ko.mapping.fromJS(jsonSchema);
                ko.watch(self.CurrentLegExpenseData, { depth: 1, tagFields: true }, function (parents, child, item) {
                    if (afterInitialization == true && self.ExpenseEditMode() == true && jQuery.inArray(child._fieldName, PropertyNamesList.split(",")) != -1 ) {
                        setTimeout(function () {
                            SavePostflightLegExpense();
                        }, 100);
                    }
                });

            }
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PostFlightBodyContent" runat="server">
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true"
        ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="radCompanyMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="HomeBasePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCompanyCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="HomeBasePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAccountMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientAccountClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAccountMasterCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPayableVendorPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnVendorPayClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close,Move" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/PayableVendorPopUp.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPayableVendorCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                Top="0px" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnAirportClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" Height="800px" Width="1100px"
                Top="0px" ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewRosterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnCrewCodeClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewRosterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFBOCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnFBOCodeClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/FBOPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewRosterCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadFBOCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnFBOCodeClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/FBOPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaymentTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnPayTypeClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/PaymentTypePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaymentTypeCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                Top="0px" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadFlightCategoryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnFlightCatClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadFuelLocatorPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnFuelLocatorClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/FuelLocatorPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadFuelLocatorCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                Top="0px" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadSearchPopup" runat="server" Width="750px" OnClientResizeEnd="GetDimensions" OnClientClose="OnLogSearchClose"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PostFlight/PostFlightSearchAll.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadRetrievePopup" runat="server" Width="750px" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" OnClientClose="OnPFSearchRetrieveClose" Behaviors="Close"
                 VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/PostFlight/PreflightSearchRetrieve.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="History" NavigateUrl="~/Views/Transactions/History.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" style="width: auto; float: left;">
        <asp:HiddenField ID="hdnPostflightMainInitializationObject" runat="server" />
        <asp:HiddenField ID="hdnUserPrincipalInitializationObject" runat="server" />
        <asp:HiddenField ID="hdnPostflightExpensesObject" runat="server" />
        <input type="hidden" id="expenseRowNumber" data-bind="value: self.CurrentLegExpenseData.RowNumber" />
         <table cellpadding="0" cellspacing="0" width="100%" class="border-box">
            <tr id="123">
                <td>
                    <UCPostflight:Search ID="PostflightSearch" runat="server" />
            </td>
            </tr>
        </table>
        
        <table width="100%" cellpadding="0" cellspacing="0" class="border-box" id="tblForm">
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td valign="top">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="Windows7">
                                            <div class="ULTab-subTab" style="width:100%">
                                                <ul id="legTabExpenses" data-bind="foreach: Legs" class="tabStrip">
                                                    <li class="tabStripItem" data-bind="attr: { 'id': 'Leg' + LegNUM() }, event: { click: ExpenseLegtabClick }" >
                                                        Leg <span data-bind="text: LegNUM"></span>(<span data-bind="text: DepartureAirport.IcaoID"></span>-<span data-bind="text: ArrivalAirport.IcaoID"></span>)
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <table  id="ExpensesListGrid" class="table table-striped table-hover table-bordered"></table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table cellspacing="0" cellpadding="0" class="tblButtonArea" id="ActionArea">
                                    <tr>
                                        <td>
                                            <label id="lbAddExpenseError" class="alert-text"></label>
                                            <input type="button" id="btnAddExpenses" value="Add Expenses" title="Add Expenses" data-bind="click: self.btnAddExpense_Click, enable: self.POEditMode, css: ExpenseButtonStyle" />
                                            <input type="button" id="btnDeleteExpenses" value="Delete Expenses" title="Delete Expenses"  data-bind="click: self.btnDeleteExpense_Click, enable: self.POEditMode, css: ExpenseButtonStyle" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" id="ExpenseEntryArea" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td class="tdLabel125">
                                                        <span class="mnd_text">Slip No.</span>
                                                    </td>
                                                    <td>
                                                        <input type="text" id="tbSlipNumber" class="inpt_non_edit text100" onkeypress="return fnAllowAlphaNumeric(this, event)" data-bind="value: self.CurrentLegExpenseData.SlipNUM, enable: false" />
                                                        <input type="hidden" id="hdnPostflightExpenseID" data-bind="value: self.CurrentLegExpenseData.PostflightExpenseID" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td valign="top" width="50%">
                                                        <table width="100%" class="border-box" border="0">
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel120">
                                                                                <label id="lbHomeBaseTitle" data-bind="attr: { title: self.CurrentLegExpenseData.Homebase.HomebaseAirport.tooltipInfo }">Home Base</label>
                                                                            </td>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0" id="tblHomeBase">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <input type="text" id="tbHomeBase" class="text60" onKeyPress="return fnAllowAlphaNumeric(this, event)"  style="text-transform: uppercase" maxlength="4" data-bind="value: self.CurrentLegExpenseData.Homebase.HomebaseCD, enable: self.ExpenseEditMode, event: { change: HomebaseValidator_KO }" />
                                                                                            <input type="hidden" id="hdnHomeBase" data-bind="value: self.CurrentLegExpenseData.HomebaseID" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="button" id="btnHomeBase" title="Search for Home Base" onclick="javascript: openWin('radCompanyMasterPopup'); return false;" data-bind="    enable: self.ExpenseEditMode, css: BrowseBtnExpense" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <label id="lbHomeBase" class="input_no_bg" data-bind="text: self.CurrentLegExpenseData.Homebase.BaseDescription, attr: { title: self.CurrentLegExpenseData.Homebase.HomebaseAirport.tooltipInfo }" ></label>
                                                                                <label id="lbcvHomeBase" class="alert-text" ></label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel120">
                                                                                <label ID="lbICAOTitle" data-bind="attr:{ title: self.CurrentLegExpenseData.Airport.tooltipInfo}">ICAO</label>
                                                                            </td>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0" id="tblICAO">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <input type="text" ID="tbICAO" class="text60" onkeypress="return fnAllowAlphaNumeric(this, event)" style="text-transform: uppercase" maxlength="4" data-bind="enable: self.ExpenseEditMode, value: self.CurrentLegExpenseData.Airport.IcaoID, event: { change: AirportValidator_KO }" />
                                                                                            <input type="hidden" id="hdnICAO" data-bind="value: self.CurrentLegExpenseData.AirportID" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="button" id="btnICAO" title="Display Airports" onclick="javascript: openWin('radAirportPopup'); return false;"  data-bind="    enable: self.ExpenseEditMode, css: BrowseBtnExpense" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <label ID="lbICAODesc" class="input_no_bg" data-bind="attr: { title: self.CurrentLegExpenseData.Airport.tooltipInfo }, text: self.CurrentLegExpenseData.Airport.AirportName"></label>
                                                                                <label id="lbICAO" class="alert-text"></label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel120">
                                                                                FBO
                                                                            </td>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0" id="tblFBO">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <input type="text" id="tbFBO" class="text60" onkeypress="return fnAllowAlphaNumeric(this, event)" style="text-transform: uppercase" maxlength="4" data-bind="    enable: self.ExpenseEditMode, value: self.CurrentLegExpenseData.FBO.FBOCD, event: { change: FBOValidator_KO }"/>
                                                                                            <input type="hidden" id="hdnFBO" data-bind="value: self.CurrentLegExpenseData.FBOID" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="button" id="btnFBO" title="View FBO Records" onclick="javascript: openWin('RadFBOCodePopup'); return false;" data-bind="    enable: self.ExpenseEditMode, css: BrowseBtnExpense"/>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <label id="lbFBODesc" class="input_no_bg"  data-bind="text: self.CurrentLegExpenseData.FBO.FBOVendor"></label>
                                                                                <label ID="lbFBO" class="alert-text"></label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="mnd_text tdLabel120">
                                                                                Account No.
                                                                            </td>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0" id="tblAccount">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <input type="text" id="tbAccountNumber" class="text60" onkeypress="return fnAllowNumericAndDot(this, event)" style="text-transform: uppercase" maxlength="8" data-bind="    enable: self.ExpenseEditMode, value: self.CurrentLegExpenseData.Account.AccountNum, event: { change: AccountValidator_KO }"/>
                                                                                            <input type="hidden" id="hdnAccountNumber" data-bind="value: self.CurrentLegExpenseData.AccountID" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="button" id="btnAccountNo" title="View Account Numbers" onclick="javascript:openWin('radAccountMasterPopup');return false;"  data-bind="    enable: self.ExpenseEditMode, css: BrowseBtnExpense"/>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <label id="lbAccountNumber" class="alert-text" ></label>
                                                                                <label id="lbAccDescription" class="input_no_bg" data-bind="text: self.CurrentLegExpenseData.Account.AccountDescription"></label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0" border="0">
                                                                        <tr>
                                                                            <td class="tdLabel120">
                                                                                Account Period
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="tbAcctPeriod" class="text60" onKeyPress="return fnAllowNumeric(this, event)" maxlength="6" data-bind="    enable: self.ExpenseEditMode, value: self.CurrentLegExpenseData.AccountPeriod"/>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel120">
                                                                                Purchase Date
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="tbPurchaseDate" class="text80" maxlength="10" 
                                                                                    data-bind=" enable: self.ExpenseEditMode, datepicker: self.CurrentLegExpenseData.PurchaseDT" readonly/>
                                                                            </td>
                                                                            <td>
                                                                                <input type="checkbox" id="chkIncludeOnBilling" data-bind="enable: self.ExpenseEditMode, checked: self.CurrentLegExpenseData.IsBilling"  >Include On Billing
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td valign="top" width="50%">
                                                        <table width="100%" class="border-box">
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel120">
                                                                                Crew
                                                                            </td>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0" id="tblCrew">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <input type="text" id="tbExpCrewCode" class="text60" onkeypress="return fnAllowAlphaNumeric(this, event)" style="text-transform: uppercase" maxlength="8" data-bind="enable: self.ExpenseEditMode, value: self.CurrentLegExpenseData.Crew.CrewCD, event: { change: CrewValidator_KO }"/>
                                                                                            <input type="hidden" id="hdnCrew" data-bind="value: self.CurrentLegExpenseData.CrewID" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="button" id="btnCrew" title="View Crew Roster" onclick="javascript: openWin('radCrewRosterPopup'); return false;"  data-bind="    enable: self.ExpenseEditMode, css: BrowseBtnExpense"/>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <label id="lbCrewDesc" class="input_no_bg"  data-bind="text: self.CurrentLegExpenseData.Crew.DisplayName"></label>
                                                                                <label id="lbCrew" class="alert-text"></label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel120">
                                                                                Flight Category
                                                                            </td>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0" id="tblFlightCat">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <input type="text" id="tbFlightCat" class="text60" onkeypress="return fnAllowAlphaNumeric(this, event)" style="text-transform: uppercase" data-bind=" enable: self.ExpenseEditMode, value: self.CurrentLegExpenseData.FlightCategory.FlightCatagoryCD, event: { change: FlightCatagoryValidator_KO }"/>
                                                                                            <input type="hidden" id="hdnFlightCat"  data-bind="value: self.CurrentLegExpenseData.FlightCategoryID" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="button" id="btnFlightCat" title="View Flight Categories" onclick="javascript: openWin('RadFlightCategoryPopup'); return false;"  data-bind="    enable: self.ExpenseEditMode, css: BrowseBtnExpense"/>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <label id="lbFlightDesc" class="input_no_bg"  data-bind="text: self.CurrentLegExpenseData.FlightCategory.FlightCatagoryDescription"></label>
                                                                                <label id="lbFlightCat" class="alert-text"></label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel120">
                                                                                Dispatch No.
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="tbDispatchNumber" class="text60" onkeypress="return fnAllowAlphaNumeric(this, event)" maxlength="12" data-bind=" enable: self.ExpenseEditMode, value: self.CurrentLegExpenseData.DispatchNUM" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel120">
                                                                                Invoice No.
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="tbInvoiceNumber" class="text60" maxlength="25" data-bind=" enable: self.ExpenseEditMode, value: self.CurrentLegExpenseData.InvoiceNUM" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel120">
                                                                                Payment Type
                                                                            </td>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0" id="tblPaymentType">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <input type="text" id="tbPaymentType" class="text60" onkeypress="return fnAllowAlphaNumeric(this, event)" style="text-transform: uppercase" maxlength="2" data-bind=" enable: self.ExpenseEditMode, value: self.CurrentLegExpenseData.PaymentType.PaymentTypeCD, event: { change: PaymentTypeValidator_KO }"/>
                                                                                            <input type="hidden" id="hdnPaymentType" data-bind="value: self.CurrentLegExpenseData.PaymentTypeID" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="button" id="btnPaymentType" title="View Payment Types" onclick="javascript: openWin('radPaymentTypePopup'); return false;"  data-bind="    enable: self.ExpenseEditMode, css: BrowseBtnExpense"/>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <label id="lbPaymentTypeDesc" class="input_no_bg"  data-bind="text: self.CurrentLegExpenseData.PaymentType.PaymentTypeDescription"></label>
                                                                                <label id="lbPaymentType" class="alert-text"></label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel120">
                                                                                Payable Vendor
                                                                            </td>
                                                                            <td>
                                                                                <table cellpadding="0" cellspacing="0" id="tblPayVendor">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <input type="text" id="tbPayVendor" class="text60" onkeypress="return fnAllowAlphaNumeric(this, event)" style="text-transform: uppercase" data-bind=" enable: self.ExpenseEditMode, value: self.CurrentLegExpenseData.PayableVendor.VendorCD, event: { change: PaymentVendorValidator_KO }"/>
                                                                                            <input type="hidden" id="hdnPayVendor" data-bind="value: self.CurrentLegExpenseData.PaymentVendorID" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <input type="button" id="btnPayVendor" title="View Payable Vendors" onclick="javascript: openWin('radPayableVendorPopup'); return false;"  data-bind="    enable: self.ExpenseEditMode, css: BrowseBtnExpense"/>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <label ID="lbPayVendorDesc" class="input_no_bg" data-bind="text: self.CurrentLegExpenseData.PayableVendor.Name"></label>
                                                                                <label id="lbPayVendor" class="alert-text"></label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" cellspacing="0" cellpadding="0" id="tblFuelEntry">
                                    <tr>
                                        <td width="50%">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <input type="checkbox" id="chkFuelEntry" data-bind="checked: self.CurrentLegExpenseData.IsAutomaticCalculation, enable: self.ExpenseEditMode" />Fuel Entry
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel130">
                                                                    Fuel Units
                                                                </td>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <select id="ddlUnits" data-bind="enable: self.EditFuelEntry, value: self.CurrentLegExpenseData.FuelPurchase">
                                                                                    <option value="0">Select Type</option>
                                                                                    <option value="1">U.S. Gallons</option>
                                                                                    <option value="2">Liters</option>
                                                                                    <option value="3">Imp. Gallons</option>
                                                                                    <option value="4">Pounds</option>
                                                                                    <option value="5">Kilos</option>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                            <tr>
                                                                <td class="tdLabel130">
                                                                    Fuel Locator
                                                                </td>
                                                                <td align="left">
                                                                    <table cellpadding="0" cellspacing="0" id="tblFuelLocator">
                                                                        <tr>
                                                                            <td align="left">
                                                                                <input type="text" id="tbFuelLocator" maxlength="4" class="text60" onkeypress="return fnAllowAlphaNumeric(this, event)"  style="text-transform: uppercase" data-bind="    enable: self.EditFuelEntry, value: self.CurrentLegExpenseData.FuelLocator.FuelLocatorCD, event: { change: FuelLocatorValidator_KO }"/>
                                                                                <input type="hidden" id="hdnFuelLocator"  data-bind="value: self.CurrentLegExpenseData.FuelLocatorID" />
                                                                            </td>
                                                                            <td>
                                                                                <input type="button" id="btnFuelLocator" title="View Fuel Locators" onclick="javascript: openWin('RadFuelLocatorPopup'); return false;"  data-bind="    enable: self.EditFuelEntry, css: BrowseBtnFuel"/>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <label id="lbFuelLocatorDesc" class="input_no_bg"  data-bind="text: self.CurrentLegExpenseData.FuelLocator.FuelLocatorDescription"></label>
                                                                    <label id="lbFuelLocator" class="alert-text"></label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                            <tr>
                                                                <td class="tdLabel130">
                                                                    Quantity
                                                                </td>
                                                                <td align="left">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td align="left">
                                                                                <input type="text" id="tbQuantity" onfocus="this.select();" maxlength="17" onkeypress="return fnAllowNumericAndChar(this, event,'.')"  data-bind=" enable: self.EditFuelEntry, formatDoubleDecimal: self.CurrentLegExpenseData.FuelQTY, event: { change: FuelQtyUnitPrice_Changed }" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel130">
                                                                    Unit Price
                                                                </td>
                                                                <td align="left">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td align="left" class="pr_radtextbox_100">
                                                                        <input type="text" pattern="[0-9]*" class="rightJustified" maxlength="17" id="tbUnitPrice" style="text-align: right;" data-bind="enable: self.EditFuelEntry, CostCurrencyElement: self.CurrentLegExpenseData.UnitPrice, precision: 4, EmptyIfNull: true, event: { change: FuelQtyUnitPrice_Changed }" />                                                                    
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel130">
                                                                    Posted Price
                                                                </td>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td align="left" class="pr_radtextbox_100">
                                                                        <input type="text" pattern="[0-9]*" class="rightJustified" maxlength="17" id="tbPostedPrice" style="text-align: right;" data-bind="enable: self.EditFuelEntry, CostCurrencyElement: self.CurrentLegExpenseData.PostFuelPrice, precision: 4, EmptyIfNull: true" />                                                                    
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top" width="50%">
                                            <table width="100%">
                                                <tr>
                                                    <td valign="top">
                                                        <fieldset>
                                                            <legend>Taxes</legend>
                                                            <table width="100%">
                                                                <tr>
                                                                    <td class="tdLabel110">
                                                                        Federal Tax
                                                                    </td>
                                                                    <td align="left" class="pr_radtextbox_100">
                                                                        <input type="text" pattern="[0-9]*" class="rightJustified" maxlength="17" id="tbFedTax" style="text-align: right;" data-bind="enable: self.ExpenseEditMode, CostCurrencyElement: self.CurrentLegExpenseData.FederalTAX, EmptyIfNull: true" />                                                                    
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        State Tax
                                                                    </td>
                                                                    <td align="left" class="pr_radtextbox_100">
                                                                        <input type="text" pattern="[0-9]*" class="rightJustified" maxlength="17" id="tbReligonalTax" style="text-align: right;" data-bind="enable: self.ExpenseEditMode, CostCurrencyElement: self.CurrentLegExpenseData.SateTAX, EmptyIfNull: true" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Sales Tax
                                                                    </td>
                                                                    <td align="left" class="pr_radtextbox_100">
                                                                        <input type="text" pattern="[0-9]*" class="rightJustified" maxlength="17" id="tbSalesTax" style="text-align: right;" data-bind="enable: self.ExpenseEditMode, CostCurrencyElement: self.CurrentLegExpenseData.SaleTAX, EmptyIfNull: true" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </fieldset>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table>
                                                <tr>
                                                    <td class="tdLabel123">
                                                        Total Expense
                                                    </td>
                                                    <td align="left" class="pr_radtextbox_100">
                                                        <input type="text" pattern="[0-9]*" class="rightJustified" maxlength="17" id="tbTotalExpense" style="text-align: right;" data-bind="enable: self.ExpenseEditMode, CostCurrencyElement: self.CurrentLegExpenseData.ExpenseAMT, EmptyIfNull: true, event: { change: FuelTotalExpense_Changed }"  />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <div class="tblspace_10">
                    </div>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td align="left">
                    <telerik:RadPanelBar ID="pnlExpenseNotes" ExpandAnimation-Type="None" CollapseAnimation-Type="none"
                        runat="server" CssClass="postflight-panel-bar">
                        <Items>
                            <telerik:RadPanelItem runat="server" Expanded="true" Text="Expense Notes">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%" class="note-box">
                                        <tr>
                                            <td class="po_lognotes">
                                                <span class="riSingle RadInput RadInput_Default" style="width:160px;">
                                                    <textarea id="tbExpenseNotes"  rows="2" cols="17" class="riTextBox"  data-bind="enable: self.ExpenseEditMode, textinput: self.CurrentLegExpenseData.PostflightNote.Notes" >
                                                    </textarea>
                                                </span>
                                                <input type="hidden" id="hdnExpenseNoteID" data-bind="value: self.CurrentLegExpenseData.PostflightNote.PostflightNoteID" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelBar>
                </td>
            </tr>
        </table>

        <UC:UCPostflightBottomButtonControls runat="server" ID="UCPostflightBottomButtonControls1" />
    </div>
</asp:Content>
