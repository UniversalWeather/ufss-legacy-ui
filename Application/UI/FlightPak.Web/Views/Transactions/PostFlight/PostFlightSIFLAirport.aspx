﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PostFlightSIFLAirport.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.PostFlight.PostFlightSIFLAirport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="/Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
</head>
<body>
    <form id="form1" runat="server">
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
        <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jquery.alerts.js") %>"></script>
        <script type="text/javascript">
            var jqgriddgPostFlightSIFLAirport = "#dgPostFlightSIFLAirport";
            var TitleSIFLAlert = "Postflight";
            var rowTitleCity = "";
            var isdepartAirport = false;
            var rownumber = 1;
        </script>

        <script type="text/javascript">

            $(document).ready(function () {
                var pagetitle="Select ";
                var flag = getQuerystring("flag", "");
                if (flag == "Depart") {
                    isdepartAirport = true;
                    rowTitleCity="Dep ICAO - City"
                    pagetitle+=" Departure ICAO";
                } else {
                    isdepartAirport = false;
                    rowTitleCity = "Arr ICAO - City"
                    pagetitle+=" Arrival ICAO";
                }
                initializeGrid();

                document.title = pagetitle;
            });

            function initializeGrid() {
                jQuery(jqgriddgPostFlightSIFLAirport).jqGrid({

                    url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/GetPostflightSIFLAirportData',                    
                    mtype: 'POST',
                    datatype: "json",
                    cache: false,
                    async: true,
                    loadonce:true,
                    ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
                    serializeGridData: function (postData) {
                        if (postData._search == undefined || postData._search == false) {
                            if (postData.filters === undefined) postData.filters = null;
                        }
                        postData._search = undefined;
                        postData.nd = undefined;
                        postData.size = undefined;
                        postData.page = undefined;
                        postData.sort = undefined;
                        postData.dir = undefined;
                        postData.filters = undefined;
                        postData.isDepartAirport = isdepartAirport;
                        return JSON.stringify(postData);
                    },                  
                    height: 320,
                    width: 450,
                    viewrecords: true,
                    pager: "#pg_gridPager",
                    enableclear: true,
                    emptyrecords: "No records to view.",
                    colNames: ["AirportName", "AirportID", "ICAOID", "IcaoCity", "Leg", rowTitleCity],
                    colModel: [
                        { name: 'AirportName', index: 'AirportName', align: 'left', hidden: true },
                        { name: 'AirportID', index: 'AirportID', align: 'left', key: true, hidden: true },
                        { name: 'IcaoID', index: 'IcaoID', align: 'left', hidden: true },
                        { name: 'CityName', index: 'CityName', align: 'left', hidden: true },
                        {
                            name: 'rownumber', index: 'rownumber', align: 'left',sortable:false,width:100, formatter: function (cellvalue, options, rowData) {
                                return rownumber++;
                            }
                        },
                        {
                            name: 'cityicao', index: 'cityicao', align: 'left',sortable:false, width: 100, formatter: function (cellvalue, options, rowData) {
                                return GetValidatedValue(rowData, "IcaoID") + " - " + GetValidatedValue(rowData, "CityName");
                            }
                        }
                    ],
                    ondblClickRow: function (rowId) {
                        OkButtonClick();
                    },
                    loadComplete: function (rowData) {
                        rownumber = 1;
                    }
                });

            }
            //this function is used to close this window
            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshGrid(arg);
            }

            //this function is used to set the frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                return oWindow;
            }

            //this function is used to get the selected code
            function returnToParent(rowData) {
                //create the argument that will be returned to the parent page
                oArg = new Object();

                if (rowData != null) {
                    oArg.LegID = rowData["LegID"];
                    oArg.ICAOID = rowData["IcaoID"];
                    oArg.AirportID = rowData["AirportID"];
                    oArg.AirportName = rowData["AirportName"];
                }
                else {
                    oArg.LegID = "";
                    oArg.ICAOID = "";
                    oArg.AirportID = "";
                    oArg.AirportName = "";
                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }
            function Close() {
                GetRadWindow().Close();
            }
            function prepareSearchInput(input) { input.value = input.value; }

            function OkButtonClick() {
                var selr = $(jqgriddgPostFlightSIFLAirport).jqGrid('getGridParam', 'selrow');
                if (selr != null) {
                    var rowData = $(jqgriddgPostFlightSIFLAirport).getRowData(selr);
                    returnToParent(rowData, 0);
                } else {
                    jAlert("Please select record.", TitleSIFLAlert);
                    return false;
                }
            }

        </script>

        <div id="DivExternalForm" runat="server">
            <table cellspacing="0" cellpadding="0" class="box1">
                <tr>
                    <td>
                        <table id="dgPostFlightSIFLAirport" class="table table-striped table-hover table-bordered"></table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="grid_icon">
                            <div role="group" id="pg_gridPager"></div>
                        </div>
                        <div class="clear"></div>
                        <div style="padding: 5px 5px; text-align: right;">
                            <input id="btnSubmit" class="button okButton" value="OK" type="button" onclick="OkButtonClick();" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
