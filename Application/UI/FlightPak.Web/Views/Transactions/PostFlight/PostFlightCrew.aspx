﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/PostFlight.Master"
    AutoEventWireup="true" CodeBehind="PostFlightCrew.aspx.cs" Inherits="FlightPak.Web.Views.Settings.PostFlight.PostFlightCrew"
    MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Framework/Masters/PostFlight.Master" %>
<%@ Register TagPrefix="uc" TagName="Search" Src="~/UserControls/Postflight/UCPostflightSearch.ascx" %>
<%@ Register TagPrefix="uc" TagName="BottomButtonControls" Src="~/UserControls/Postflight/UCPostflightBottomButtonControls.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PostFlightHeadContent" runat="server">   
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <style type="text/css">
            table.box_po #crewTableTr td .grid_icon { padding: 0;}
            /*This CSS here for temporary basis need to move into css file for post flight*/
            /* START JQUERY SCROLLBAR HORIZANTAL VERTICAL */
             .scroll-pane { width: 100%; height: 477px; overflow: auto; }
            .jspContainer { overflow: hidden; }
            .jspPane { position: absolute; }
            .jspVerticalBar { position: absolute; top: 0; right: 0; width: 11px; height: 100%;}
            .jspHorizontalBar { position: absolute; bottom: 0; left: 0; width: 100%; height: 11px;}
            .jspCap { display: none; }
            .jspHorizontalBar .jspCap { float: left; }
            .jspTrack { background: rgba(49, 49, 49, 0.15); position: relative; border-radius: 10px; }
            .jspDrag { background: rgba(49, 49, 49, 0.30); position: relative; top: 0; left: 0; cursor: pointer; border-radius: 10px; }
            .jspHorizontalBar .jspTrack, .jspHorizontalBar .jspDrag { float: left; height: 100%; }
            .jspArrow { background: #50506d; text-indent: -20000px; display: block; cursor: pointer; padding: 0; margin: 0; }
            .jspArrow.jspDisabled { cursor: default; background: none; }
            .jspVerticalBar .jspArrow { height: 0px; }
            .jspHorizontalBar .jspArrow { width: 0px; float: left; height: 100%; }
            .jspVerticalBar .jspArrow:focus { outline: none; }
            .jspCorner { background: #eeeef4; float: left; height: 100%; }
             #crewTableTr table tr.jqgrow td { height: 26px !important}
            /* Yuk! CSS Hack for IE6 3 pixel bug :( */
            * html .jspCorner{margin: 0 -3px 0 0;}
        /*END JQUERY SCROLLBAR HORIZANTAL VERTICAL */
        </style>
        <style type="text/css">
            .prfl-nav-icons {
            left:-2px !important;
            }
        </style>                
        <script type="text/javascript">

            // this function is used to display the CrewRoster popup
            function ShowCrewPopup() {
              var oWnd = radopen("/Views/Transactions/PostFlight/PostFligtCrewRosterPopUp.aspx?CrewID=" + document.getElementById('hdnCrewInfoId').value + "&CrewCD=" + document.getElementById('hdnCrewCDInfo').value + "&FirstName=" + document.getElementById('hdnCrewNames').value + "", "radCrewRosterPopup"); // Added for Postflight Crew Tab
                return false;
            }

            // this function is used to get the dimensions
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            //this function is used to resize the pop-up window
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }                    

            function OnradCrewRosterPopupClose(oWnd, args) {
                var arg = args.get_argument();
                if (arg != null) {
                    document.getElementById('hdnCrewInfo').value = arg.val;
                    AddCrews(arg.val,false);
                }
                return false;
            }
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PostFlightBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <%= Scripts.Render("~/bundles/postflightcrew") %>
         <%= Scripts.Render("~/bundles/jqueryScrollBundle") %>
        <script type="text/javascript">
            //To display plane image at right place
            isLogNotesExpanded = false;

            function pnlLogNotesExpand() {
                isLogNotesExpanded = true;
            }
            function pnlLogNotesCollapse() {
                isLogNotesExpanded = false;
            }

            $(document).ready(function () {
                $("#CrewSearchSubmit").click(function () {
                    CrewCodeValidate();
                    return false;
                });

                $("#SearchBoxCrew").keypress(function (e) {
                    if (e.keyCode == 13) {
                        CrewCodeValidate();
                        return false;
                    }
                });

                $("#SearchBoxCrew").blur(function () {
                    CrewCodeValidate();
                    return false;
                });

                function CrewCodeValidate() {
                    var CrewCD=document.getElementById("SearchBoxCrew").value;
                    var CrewID=document.getElementById("hdnCrewInfoId").value;
                    var btn = document.getElementById("CrewSearchSubmit");
                    var crewErrorLabel = "lblSearchCrew";
                    Crew_Validate_Retrieve(CrewCD, crewErrorLabel, btn);
                }
            });
        </script>
    </telerik:RadCodeBlock>
   
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Visible="true" runat="server"
        Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true"
        ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="radCrewRosterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" ReloadOnShow="true" Behaviors="Close"
                VisibleStatusbar="false" OnClientClose="OnradCrewRosterPopupClose">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewRosterCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadSearchPopup" runat="server" Width="750px" OnClientResizeEnd="GetDimensions" OnClientClose="OnLogSearchClose"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PostFlight/PostFlightSearchAll.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadRetrievePopup" runat="server" Width="750px" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" OnClientClose="OnPFSearchRetrieveClose" Behaviors="Close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/PostFlight/PreflightSearchRetrieve.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="History" NavigateUrl="~/Views/Transactions/History.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" style="width: auto; float: left;">
        <asp:HiddenField ID="hdnPostflightMainInitializationObject" runat="server" />
        <asp:HiddenField ID="hdnUserPrincipalInitializationObject" runat="server" />
        <asp:HiddenField ID="hdnPostflightLegObject" runat="server" ClientIDMode="Static" />
        <table width="100%" cellpadding="0" cellspacing="0" class="border-box" id="tblForm"
            runat="server">
            <tr>
               <td align="left">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <uc:Search ID="UCSearch" runat="server" />
                            </td>
                        </tr>
                    </table>                   
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                             <ul id="rtsLegs" data-bind="foreach: Legs" class="tabStrip">
                                                 <li class="tabStripItemSelected" data-bind="attr: { 'id': 'Leg' + LegNUM() }, event: { click: LegTab_Click }"" >
                                                     Leg <span data-bind="text: LegNUM"></span>(<span data-bind="    text: DepartureAirport.IcaoID"></span>-<span data-bind="    text: ArrivalAirport.IcaoID"></span>)
                                                 </li>
                                             </ul>                                       
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" class="box_po" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td>
                                            <table width="100%" class="nav_bg_preflight" id="tblCrewSearch" runat="server" cellpadding="0" data-bind="visible: POEditMode()"
                                                cellspacing="0" border="0">
                                                <tr>
                                                    <td align="left" class="tdLabel180">
                                                        <div class="global-search">
                                                            <span>
                                                                 <input type="button" id="CrewSearchSubmit" class="searchsubmitbutton"/>
                                                                 <input type="text" id="SearchBoxCrew" class="SearchBox_sc_crew"/>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td align="left">
                                                         <a id="lnkCrewRoster" onclick="javascript: ShowCrewPopup();return false" class="link_small" href="javascript:ShowCrewPopup();return false">Crew Roster</a>
                                                        <input id="hdnCrewInfo" type="hidden" />
                                                        <input id="hdnCrewInfoId" type="hidden" />
                                                        <input id="hdnCrewNames" type="hidden" />
                                                        <input id="hdnCrewCDInfo" type="hidden" />
                                                        <input id="hdnInsert" type="hidden" />
                                                    </td>
                                                    <td align="right">
                                                         <table>
                                                            <tr>
                                                                 <td class="tdLabel70" align="left">
                                                                    Depart Date
                                                                </td>
                                                                <td class="tdLabel80" align="left">
                                                                    <label id="lblDepartDate" data-bind="text: CurrentLegData.ScheduledDate" />                                                                   
                                                                </td>
                                                                <td class="tdLabel70" align="left">
                                                                    Depart ICAO
                                                                </td>
                                                                <td align="left">
                                                                     <label id="lblDeaprtICAO" data-bind="text: CurrentLegData.DepartureAirport.IcaoID" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tdLabel70" align="left">
                                                                    Arrive Date
                                                                </td>
                                                                <td class="tdLabel80" align="left">
                                                                     <label id="lblArriveDate" data-bind="text: CurrentLegData.InDate" /> 
                                                                </td>
                                                                <td class="tdLabel70" align="left">
                                                                    Arrive ICAO
                                                                </td>
                                                                <td align="left">
                                                                    <label id="lblArriveICAO" data-bind="text: CurrentLegData.ArrivalAirport.IcaoID" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="3" class="srch_category">
                                                        (Search by Crew Code)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="3" style="padding-left: 10px;">
                                                        <label id="lblSearchCrew" class="alert-text" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <input id="hdnLeg" type="hidden"/>
                                            <input id="hdnCrewCD" type="hidden" />
                                         </td>
                                    </tr>
                                    <tr id="crewTableTr">
                                        <td>
                                            <table id="jqCrewGrid" class="table table-striped table-hover table-bordered"></table>

                                        </td>
                                        <td>
                                        <div class="grid_icon">
                                            <div role="group" id="pg_gridPagerCrew">  
                                            </div>                              
                                        </div>
                                    </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <div class="tblspace_5">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr runat="server" ID="divtr1">
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <div class="tblspace_10">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr runat="server" ID="divtr2">
                <td>
                    <table width="100%">
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="tdLabel90">
                                            Crew Name
                                        </td>
                                        <td class="tdLabel200">
                                            <input type="text" id="tbCrewName" tabindex="25" readonly="readonly" class="inpt_non_edit text330" data-bind="value: CrewName()"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="right"> 
                                <input type="button" id="btnRonAdj"  tabindex="26" title="Adjust RON" class="ui_nav_disable" value="Adjust RON" data-bind="enable: POEditMode(), event: { click: AdjustRON_Click }"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr runat="server" ID="divtr3">
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <div class="tblspace_10">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr runat="server" ID="divtr4">
                <td>
                    
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="50%">
                                <fieldset>
                                    <legend>Adjust Crew Duty Hours</legend>
                                    <table id="tblDutyAdj" runat="server"  cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td width="50%">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="left">
                                                                        <span class="mnd_text">Duty Begin</span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel60">
                                                                        Date
                                                                    </td>
                                                                    <td>
                                                                         <input ID="tbDateBegin" type="text"  tabindex="27" class="textDatePicker inpt_non_edit text90" readonly="readonly" disabled="disabled" data-bind="textInput: SelectedCrewData!=undefined ? SelectedCrewData.CrewDutyStartDate : ''"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel60">
                                                                        Time
                                                                    </td>
                                                                    <td id="dutyBegin">
                                                                        <input ID="tbDutyBegintime" tabindex="28" type="text" class="datetimechange inpt_non_edit text40" readonly="readonly" disabled="disabled" data-bind="textInput: SelectedCrewData != undefined ? SelectedCrewData.CrewDutyStartTime : ''" onblur="return ValidateScheduleTime(this, 'Postflight Crew');"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="50%">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel80" valign="top">
                                                                        <span class="mnd_text">Duty End</span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel60" valign="top">
                                                                        Date
                                                                    </td>
                                                                    <td>
                                                                          <input ID="tbDateEndDate" tabindex="29" type="text" class="textDatePicker inpt_non_edit text90" readonly="readonly" disabled="disabled" data-bind="textInput: SelectedCrewData != undefined ? SelectedCrewData.CrewDutyEndDate : ''"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel60">
                                                                        Time
                                                                    </td>
                                                                     <td id="dutyEnd">
                                                                        <input ID="tbDutyEndtime" tabindex="30" type="text" class="datetimechange inpt_non_edit text40" readonly="readonly" disabled="disabled" data-bind="textInput: SelectedCrewData != undefined ? SelectedCrewData.CrewDutyEndTime : ''" onblur="return ValidateScheduleTime(this, 'Postflight Crew');"/>
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <input type="button" tabindex="31" id="btnDutyAdjReset" class="reset-icon-disable" disabled="disabled" title="Reset" value="Reset" data-bind="event: { click: DutyAdjReset_Click }"/>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                            <td valign="top" width="30%">
                                <fieldset>
                                    <legend>Adjust Augment Crew Hours</legend>
                                    <table id="tblAugCrewhrs">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel80" valign="top">
                                                            Block Hours
                                                        </td>
                                                        <td>
                                                            <input id="tbCrewBlockHours" tabindex="32" type="text" class="inpt_non_edit text50" onfocus="this.select();" MaxLength="5" onKeyPress="return fnAllowNumericAndChar(this, event,': .')" disabled="disabled" readonly="readonly" data-bind="textInput: SelectedCrewData != undefined ? SelectedCrewData.BlockHoursTime : ''"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel80" valign="top">
                                                            Flight Hours
                                                        </td>
                                                        <td class="tdLabel50">
                                                            <input id="tbCrewFlightHours" tabindex="33" type="text" class="inpt_non_edit text50" onfocus="this.select();" MaxLength="5" onKeyPress="return fnAllowNumericAndChar(this, event,': .')" disabled="disabled" readonly="readonly" data-bind="textInput: SelectedCrewData != undefined ? SelectedCrewData.FlightHoursTime : ''"/>
                                                        </td>
                                                        <td align="left">
                                                            <input type="button" tabindex="34" id="btnAugmentCrewReset" class="reset-icon-disable" disabled="disabled" title="Reset" value="Reset" data-bind="event: { click: AugmentCrewReset_Click }"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td valign="middle">
                                                             <label id="lbCrewAugmentMsg" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                            <td valign="middle" width="20%" style="display: none;">
                                <table>
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="button" tabindex="35" id="btnCrewUpdate" title="Apply Adjustments" class="button" value="Apply Adjustments" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
            <tr runat="server" ID="divtr5">
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <div class="tblspace_10">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr runat="server" ID="divtr6">
                <td>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td width="39%">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <table class="border-box" width="100%" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel80">
                                                                                Depart
                                                                            </td>
                                                                            <td>
                                                                                <input ID="tbDepart" tabindex="36" type="text" class="inpt_non_edit text100" readonly="readonly" data-bind="value: CurrentLegData.DepartureAirport.IcaoID"/>
                                                                                <input type="hidden" id="hdnDepart" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel80">
                                                                                Arrive
                                                                            </td>
                                                                            <td>
                                                                                <input id="tbArrive" tabindex="37" type="text" class="inpt_non_edit text100" readonly="readonly" data-bind="value: CurrentLegData.ArrivalAirport.IcaoID"/>
                                                                                <input type="hidden" id="hdnArrive" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel80">
                                                                                 <label id="lbMiles" />
                                                                            </td>
                                                                            <td>
                                                                                <input id="tbMiles" tabindex="38" type="text" class="inpt_non_edit text100" readonly="readonly" data-bind="value: CurrentLegData.ConvertedDistance"/>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <div class="tblspace_5">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="border-box" width="100%" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel80">
                                                                                FAR Part
                                                                            </td>
                                                                            <td>
                                                                                <input id="tbFARParts" tabindex="39" type="text" class="inpt_non_edit text100" readonly="readonly" data-bind="value: CurrentLegData.FedAviationRegNum"/>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td class="tdLabel80">
                                                                                Crew Rules
                                                                            </td>
                                                                            <td>
                                                                               <input id="tbCrewRules" tabindex="40" type="text" class="inpt_non_edit text100" readonly="readonly" data-bind="value: CurrentLegData.CrewCurrency, attr: { 'title': CurrentLegData.CrewRulesToolTipInfo }""/>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                                 <label id="lbRules" class="input_no_bg" data-bind="text: CurrentLegData.CrewDutyRulesDescription, attr: { 'title': CurrentLegData.CrewRulesToolTipInfo }" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="1%">
                                &nbsp;
                            </td>
                            <td width="60%">
                                <fieldset>
                                    <legend>Leg Times</legend>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel80" valign="top">
                                                                        Time & Date
                                                                    </td>
                                                                    <td valign="top">
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td class="tdLabel56">
                                                                                    <input id="tbLegTime" tabindex="41" type="text" class="inpt_non_edit text40" readonly="readonly" data-bind="value: CurrentLegData.ScheduledTime"/>
                                                                                </td>
                                                                                <td>
                                                                                     <input id="tbLegDate" tabindex="42" type="text" class="inpt_non_edit text80" readonly="readonly" data-bind="value: CurrentLegData.ScheduledDate"/>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel80" valign="top">
                                                                        Out
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td class="tdLabel56">
                                                                                    <input id="tbLegOuttime" tabindex="43" type="text" class="inpt_non_edit text40" readonly="readonly" data-bind="timepicker: CurrentLegData.OutTime" />
                                                                                </td>
                                                                                <td>
                                                                                    <input id="tbLegOutdate" tabindex="43" type="text" class="inpt_non_edit text80" readonly="readonly" data-bind="datepicker: CurrentLegData.OutDate, visible: (self.UserPrincipal._DutyBasis == '2' ? false : true)" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>                                                                         
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel80" valign="top">
                                                                        Off
                                                                    </td>
                                                                    <td class="tdLabel160">
                                                                         <input id="tbOff" type="text" tabindex="44" class="inpt_non_edit text40" readonly="readonly" data-bind="timepicker: CurrentLegData.TimeOff"/>
                                                                    </td>
                                                                    <td class="tdLabel80">
                                                                        Block Hours
                                                                    </td>
                                                                    <td>
                                                                        <input id="tbBlockHours" tabindex="45" type="text" class="inpt_non_edit text70" readonly="readonly" data-bind="value: CurrentLegData.BlockHoursTime"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel80" valign="top">
                                                                        On
                                                                    </td>
                                                                    <td class="tdLabel160">
                                                                        <input id="tbOn" type="text" tabindex="46" class="inpt_non_edit text40" readonly="readonly" data-bind="timepicker:CurrentLegData.TimeOn"/>
                                                                    </td>
                                                                    <td class="tdLabel80">
                                                                        Flight Hours
                                                                    </td>
                                                                    <td>
                                                                         <input id="tbFlightHours" tabindex="47" type="text" class="inpt_non_edit text70" readonly="readonly"  data-bind="value:CurrentLegData.FlightHoursTime"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                             <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel80" valign="top">
                                                                        In
                                                                    </td>
                                                                     <td valign="top" class="tdLabel160">
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td class="tdLabel56">
                                                                                    <input id="tbIn" tabindex="48" type="text" class="inpt_non_edit text40" readonly="readonly" data-bind="timepicker: CurrentLegData.InTime"/>
                                                                                </td>
                                                                                <td>
                                                                                     <input id="tbLegIndate" tabindex="49" type="text" class="inpt_non_edit text80" readonly="readonly" data-bind="value: CurrentLegData.InDate"/>
                                                                                </td>
                                                                             </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td class="tdLabel80">
                                                                        Duty Hours
                                                                    </td>
                                                                    <td>
                                                                        <input id="tbDutyHours" type="text" tabindex="50" class="inpt_non_edit text70" readonly="readonly" data-bind="value: CurrentLegData.DutyHrsTime"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>      

        <uc:BottomButtonControls ID="UCBottomButtonControls" runat="server" />
        <table id="tblHidden" style="display: none;">
            <tr>
                <td>
                    <div runat="server" ID="divtblHidden">
                        <input type="hidden" id="hdnBlockHours" data-bind="value: CurrentLegData.BlockHoursTime" />
                         <input type="hidden" id="hdnFlightHours" data-bind="value: CurrentLegData.FlightHoursTime" />
                 	</div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
