﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.PostflightService;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.CalculationService;

//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Newtonsoft.Json;
using FlightPak.Web.ViewModels.Postflight;
using System.Web;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.ViewModels;

namespace FlightPak.Web.Views.Transactions.PostFlight
{
    public partial class PostFlightPAX : BaseSecuredPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PostflightTripManager.CheckSessionAndRedirect();
            if (!IsPostBack)
            {
                PostflightTripManager.ValidatePostflightSession();
                var pax =new { PaxViewModel = new PostflightLegPassengerViewModel(), PaxSIFLViewModel= new PostflightLegSIFLViewModel()};
                hdnPostflightPaxInitializationObject.Value = HttpUtility.HtmlEncode(JsonConvert.SerializeObject(pax));
                PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
                if (Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    pfViewModel = (PostflightLogViewModel)Session[WebSessionKeys.CurrentPostflightLog];
                }
                else
                {
                    pfViewModel.PostflightMain = new PostflightMainViewModel(PostflightTripManager.getUserPrincipal()._ApplicationDateFormat);
                    pfViewModel.PostflightMain = PostflightTripManager.CreateOrCancelTrip(pfViewModel.PostflightMain);
                }
                hdnPostflightMainInitializationObject.Value = Microsoft.Security.Application.Encoder.HtmlEncode(JsonConvert.SerializeObject(pfViewModel));
                UserPrincipalViewModel userPrincipal = PostflightTripManager.getUserPrincipal();
                hdnUserPrincipalInitializationObject.Value = Microsoft.Security.Application.Encoder.HtmlEncode(JsonConvert.SerializeObject(userPrincipal));
                PostflightTripManager.ExceptionLimitLevelUpdate(1);
                PostflightTripManager.ExceptionLimitLevelUpdate(2);
            }
        }
    }

}
