﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlightPak.Web.PreflightService;
using FlightPak.Web.PostflightService;
using FlightPak.Web.Framework.Helpers;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Constants;
using System.Web.Services;
using System.Web.Script.Services;
using System.Collections;
using FlightPak.Web.ViewModels;
using System.Net;
using System.Web;

namespace FlightPak.Web.Views.Transactions.PostFlight
{
    public partial class PreflightSearchRetrieve : BaseSecuredPage
    {
        private static ExceptionManager exManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            ClientViewModel Client = new ClientViewModel();
            var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(Client);
            hdnPFRetrieveInitializationSchemaObject.Value = Microsoft.Security.Application.Encoder.HtmlEncode(json);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> PFSearchRetrieveRowSelect(Int64 TripID, bool isPostFlight)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<Hashtable>>(() =>
            {
                FSSOperationResult<Hashtable> ResultObj = new FSSOperationResult<Hashtable>();
                Hashtable HashRetObj = new Hashtable();
                string retMessage = string.Empty;
                if (ResultObj.IsAuthorized() == false)
                {
                    return ResultObj;
                }
                using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                {
                    bool tripmoved = false;
                    bool isConfirm = false;
                    var result = PrefSvc.GetTrip(TripID);
                    if (result.ReturnFlag)
                    {
                        PreflightService.PreflightMain Trip = result.EntityList[0];
                        HttpContext.Current.Session["PreflightTrip"] = Trip;

                        if (Trip.FleetID != null && Trip.FleetID > 0)
                        {
                            if (isPostFlight)
                            {
                                if (Trip.IsLog == true)
                                {
                                    using (PostflightServiceClient Service = new PostflightServiceClient())
                                    {
                                        PostflightService.PostflightMain obj = new PostflightService.PostflightMain();
                                        obj.TripID = Trip.TripID;
                                        obj.DispatchNum = Trip.DispatchNUM;
                                        var POReturnVal = Service.GetTrip(obj);

                                        if (POReturnVal != null && POReturnVal.EntityInfo != null && POReturnVal.EntityInfo.DispatchNum != null && POReturnVal.EntityInfo.IsCompleted != null && !string.IsNullOrEmpty(POReturnVal.EntityInfo.DispatchNum) && POReturnVal.EntityInfo.IsCompleted == true)
                                        {
                                            retMessage = "Flight log is marked completed, hence cannot be retrieved / updated!";
                                        }
                                        else
                                        {
                                            retMessage = "The selected Tripsheet is already logged in Postflight. Selecting YES will remove all data previously entered per leg in this log.";
                                            isConfirm = true;
                                        }
                                    }
                                }
                                else
                                {
                                    tripmoved = GetAndSavePreflight(isPostFlight,false,true);
                                }
                            }
                            else
                            {
                                tripmoved = GetAndSavePreflight(isPostFlight,false,true);
                            }
                        }
                        else
                        {
                            retMessage = "Tail No. Is Required to Retrieve Trip!";
                        }
                    }
                    HashRetObj["Message"] = retMessage;
                    HashRetObj["boolResult"] = tripmoved;
                    HashRetObj["isConfirm"] = isConfirm;
                    ResultObj.StatusCode = HttpStatusCode.OK;
                    ResultObj.Success = true;
                    ResultObj.Result = HashRetObj;
                }
                return ResultObj;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> ConfirmPFSearchRetrieve(bool isPostFlight)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<bool>>(() =>
            {
                FSSOperationResult<bool> ResultObj = new FSSOperationResult<bool>();
                if (ResultObj.IsAuthorized() == false)
                {
                    return ResultObj;
                }
                bool tripmoved = false;
                tripmoved = GetAndSavePreflight(isPostFlight,false,true);
                if (tripmoved)
                {
                    ResultObj.StatusCode = HttpStatusCode.OK;
                    ResultObj.Success = true;
                }
                else
                {
                    ResultObj.StatusCode = HttpStatusCode.OK;
                    ResultObj.Success = false;
                }
                return ResultObj;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        protected static bool GetAndSavePreflight(bool isPostFlight, bool IsPostedFromPreflight, bool IsRetrieved)
        {
            bool AutoPopulateLegTimes = false;
            String AircraftWingType = String.Empty;
            TimeSpan TaxiTime = new TimeSpan(0, 0, 0);
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isPostFlight))
            {
                bool result = false;
                PreflightService.PreflightMain Trip = (PreflightService.PreflightMain)HttpContext.Current.Session["PreflightTrip"];
                if (Trip != null)
                {
                    PostflightService.PostflightMain postFlight = new PostflightService.PostflightMain();
                    UserPrincipalViewModel UserPrincipal = TripManagerBase.getUserPrincipal();
 					postFlight.IsPostedFromPreflight = IsPostedFromPreflight;
                    
                    // This Property we are using to identify if Log created via Postflight > RetreiveTrip
                    postFlight.IsTripToSaveFromPreflightLogEvent = IsRetrieved;

                        postFlight.TripID = Trip.TripID;

                    if (Trip.DispatchNUM != null)
                        postFlight.DispatchNum = Trip.DispatchNUM.Trim();
                    else
                        postFlight.DispatchNum = string.Empty;
                    if (Trip.EstDepartureDT != null)
                        postFlight.EstDepartureDT = Trip.EstDepartureDT;
                    if (Trip.AircraftID != null)
                        postFlight.AircraftID = Trip.AircraftID;
                    if (Trip.RequestorFirstName != null)
                        postFlight.RequestorName = Trip.RequestorFirstName;
                    if (Trip.AccountID != null)
                        postFlight.AccountID = Trip.AccountID;
                    if (Trip.DepartmentID != null)
                        postFlight.DepartmentID = Trip.DepartmentID;
                    if (Trip.AuthorizationID != null)
                        postFlight.AuthorizationID = Trip.AuthorizationID;
                    if (Trip.AuthorizationDescription != null)
                        postFlight.AuthorizationDescription = Trip.AuthorizationDescription;

                    if (Trip.RecordType != null)
                        postFlight.RecordType = Trip.RecordType;

                    if (Trip.FlightNUM != null)
                        postFlight.FlightNum = Trip.FlightNUM;
                    else
                        postFlight.FlightNum = string.Empty;

                    if (Trip.HomebaseID != null)
                        postFlight.HomebaseID = Trip.HomebaseID;
                    if (Trip.ClientID != null)
                        postFlight.ClientID = Trip.ClientID;
                    if (Trip.FleetID != null)
                        postFlight.FleetID = Trip.FleetID;
                    if (Trip.PassengerRequestorID != null)
                        postFlight.PassengerRequestorID = Trip.PassengerRequestorID;
                    if (Trip.DepartmentDescription != null)
                        postFlight.DepartmentDescription = Trip.DepartmentDescription;
                    else
                        postFlight.DepartmentDescription = string.Empty;
                    if (Trip.IsCompleted != null)
                        postFlight.IsCompleted = Trip.IsCompleted;
                    else
                        postFlight.IsCompleted = false;

                    postFlight.LastUpdUID = UserPrincipal._name;

                    postFlight.POMainDescription = Trip.TripDescription;

                    if (Trip != null && Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                    {
                        List<PreflightLeg> PreflightLegList = Trip.PreflightLegs.Where(d => d.IsDeleted == false && d.State != PreflightService.TripEntityState.Deleted).OrderBy(x => x.LegNUM).ToList();

                        postFlight.PostflightLegs = new List<PostflightLeg>();
                        decimal totalDutyHour = 0;

                        AutoPopulateLegTimes = UserPrincipal._IsAutoPopulated;

                        if (!string.IsNullOrEmpty(UserPrincipal._TaxiTime))
                        {
                            TaxiTime = TimeSpan.ParseExact(UserPrincipal._TaxiTime, "c", null);
                        }

                        PostflightService.PostflightLeg previousPOLeg = new PostflightLeg();
                        foreach (PreflightLeg preFlightLeg in PreflightLegList)
                        {
                            PreflightLeg nextPreLeg = PreflightLegList.Where(x => x.LegNUM == preFlightLeg.LegNUM + 1).FirstOrDefault();

                            #region "Preflight Leg"
                            PostflightService.PostflightLeg PostLeg = new PostflightLeg();
                            if (preFlightLeg.DepartICAOID != null)
                                PostLeg.DepartICAOID = preFlightLeg.DepartICAOID;
                            if (preFlightLeg.ArriveICAOID != null)
                                PostLeg.ArriveICAOID = preFlightLeg.ArriveICAOID;
                            if (preFlightLeg.TripID != null)
                                PostLeg.TripLegID = preFlightLeg.TripID;
                            if (preFlightLeg.LegNUM != null)
                                PostLeg.LegNUM = preFlightLeg.LegNUM;

                            if (preFlightLeg.FlightPurpose != null)
                                PostLeg.FlightPurpose = preFlightLeg.FlightPurpose;

                            if (preFlightLeg.DutyTYPE1 != null)
                                PostLeg.DutyTYPE = preFlightLeg.DutyTYPE1;

                            if (preFlightLeg.DutyTYPE1 == null && preFlightLeg.CheckGroup == "INT")
                                PostLeg.DutyTYPE = WebConstants.LegDutyType.International;

                            if (preFlightLeg.DutyTYPE1 == null && preFlightLeg.CheckGroup == "DOM")
                                PostLeg.DutyTYPE = WebConstants.LegDutyType.Domestic;

                            if (preFlightLeg.IsDutyEnd != null)
                                PostLeg.IsDutyEnd = preFlightLeg.IsDutyEnd;

                            if (PreflightLegList.Count == PostLeg.LegNUM)
                                PostLeg.IsDutyEnd = true;

                            using (FlightPakMasterService.MasterCatalogServiceClient TailService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var TailVal = TailService.GetAircraftByAircraftID((long)Trip.AircraftID).EntityList;
                                if (TailVal != null && TailVal.Count > 0)
                                {
                                    AircraftWingType = ((FlightPakMasterService.Aircraft)TailVal[0]).IsFixedRotary;
                                }
                            }

                            if (!string.IsNullOrEmpty(AircraftWingType) && AircraftWingType == "F") //Fixed Wing Aircraft
                            {
                                if (UserPrincipal._LogFixed == 1) //Load Home Time
                                {
                                    if (preFlightLeg.HomeDepartureDTTM != null && preFlightLeg.HomeDepartureDTTM != DateTime.MinValue) // Check for NULL
                                    {
                                        PostLeg.ScheduledTM = preFlightLeg.HomeDepartureDTTM;
                                        PostLeg.ScheduleDTTMLocal = preFlightLeg.DepartureDTTMLocal;

                                        if (AutoPopulateLegTimes)
                                        {
                                            PostLeg.OutboundDTTM = preFlightLeg.HomeDepartureDTTM;
                                            PostLeg.TimeOff = ((DateTime)preFlightLeg.HomeDepartureDTTM).ToString("HH:mm");
                                            PostLeg.BlockOut = ((DateTime)preFlightLeg.HomeDepartureDTTM.Value.Subtract(TaxiTime)).ToString("HH:mm");
                                            PostLeg.OutboundDTTM = preFlightLeg.HomeDepartureDTTM.Value.Subtract(TaxiTime);
                                        }
                                    }
                                    if (preFlightLeg.HomeArrivalDTTM != null && preFlightLeg.HomeArrivalDTTM != DateTime.MinValue)
                                    {
                                        if (AutoPopulateLegTimes)
                                        {
                                            PostLeg.InboundDTTM = preFlightLeg.HomeArrivalDTTM;
                                            PostLeg.TimeOn = ((DateTime)preFlightLeg.HomeArrivalDTTM).ToString("HH:mm");
                                            PostLeg.BlockIN = ((DateTime)preFlightLeg.HomeArrivalDTTM.Value.Add(TaxiTime)).ToString("HH:mm");
                                            PostLeg.InboundDTTM = preFlightLeg.HomeArrivalDTTM.Value.Add(TaxiTime);
                                        }
                                    }
                                }
                                else if (UserPrincipal._LogFixed == 2) //Load UTC Time
                                {
                                    if (preFlightLeg.DepartureGreenwichDTTM != null && preFlightLeg.DepartureGreenwichDTTM != DateTime.MinValue) // Check for NULL
                                    {
                                        PostLeg.ScheduledTM = preFlightLeg.DepartureGreenwichDTTM;
                                        PostLeg.ScheduleDTTMLocal = preFlightLeg.DepartureDTTMLocal;
                                        if (AutoPopulateLegTimes)
                                        {
                                            PostLeg.OutboundDTTM = preFlightLeg.DepartureGreenwichDTTM;
                                            PostLeg.TimeOff = ((DateTime)preFlightLeg.DepartureGreenwichDTTM).ToString("HH:mm");
                                            PostLeg.BlockOut = ((DateTime)preFlightLeg.DepartureGreenwichDTTM.Value.Subtract(TaxiTime)).ToString("HH:mm");
                                            PostLeg.OutboundDTTM = preFlightLeg.DepartureGreenwichDTTM.Value.Subtract(TaxiTime);
                                        }
                                    }
                                    if (preFlightLeg.ArrivalGreenwichDTTM != null && preFlightLeg.ArrivalGreenwichDTTM != DateTime.MinValue)
                                    {
                                        if (AutoPopulateLegTimes)
                                        {
                                            PostLeg.InboundDTTM = preFlightLeg.ArrivalGreenwichDTTM;
                                            PostLeg.TimeOn = ((DateTime)preFlightLeg.ArrivalGreenwichDTTM).ToString("HH:mm");
                                            PostLeg.BlockIN = ((DateTime)preFlightLeg.ArrivalGreenwichDTTM.Value.Add(TaxiTime)).ToString("HH:mm");
                                            PostLeg.InboundDTTM = preFlightLeg.ArrivalGreenwichDTTM.Value.Add(TaxiTime);
                                        }
                                    }
                                }
                            }
                            else if (!string.IsNullOrEmpty(AircraftWingType) && AircraftWingType == "R") //Rotary Wing Aircraft
                            {
                                if (UserPrincipal._LogRotary == 1) //Load Local Time
                                {
                                    if (preFlightLeg.DepartureDTTMLocal != null && preFlightLeg.DepartureDTTMLocal != DateTime.MinValue) // Check for NULL
                                    {
                                        PostLeg.ScheduledTM = preFlightLeg.DepartureDTTMLocal;
                                        PostLeg.ScheduleDTTMLocal = preFlightLeg.DepartureDTTMLocal;

                                        if (AutoPopulateLegTimes)
                                        {
                                            PostLeg.OutboundDTTM = preFlightLeg.DepartureDTTMLocal;
                                            PostLeg.TimeOff = ((DateTime)preFlightLeg.DepartureDTTMLocal).ToString("HH:mm");
                                            PostLeg.BlockOut = ((DateTime)preFlightLeg.DepartureDTTMLocal.Value.Subtract(TaxiTime)).ToString("HH:mm");
                                            PostLeg.OutboundDTTM = preFlightLeg.DepartureDTTMLocal.Value.Subtract(TaxiTime);
                                        }
                                    }
                                    if (preFlightLeg.ArrivalDTTMLocal != null && preFlightLeg.ArrivalDTTMLocal != DateTime.MinValue)
                                    {
                                        if (AutoPopulateLegTimes)
                                        {
                                            PostLeg.InboundDTTM = preFlightLeg.ArrivalDTTMLocal;
                                            PostLeg.TimeOn = ((DateTime)preFlightLeg.ArrivalDTTMLocal).ToString("HH:mm");
                                            PostLeg.BlockIN = ((DateTime)preFlightLeg.ArrivalDTTMLocal.Value.Add(TaxiTime)).ToString("HH:mm");
                                            PostLeg.InboundDTTM = preFlightLeg.ArrivalDTTMLocal.Value.Add(TaxiTime);
                                        }
                                    }
                                }
                                else if (UserPrincipal._LogRotary == 2) //Load UTC Time
                                {
                                    if (preFlightLeg.DepartureGreenwichDTTM != null && preFlightLeg.DepartureGreenwichDTTM != DateTime.MinValue) // Check for NULL
                                    {
                                        PostLeg.ScheduledTM = preFlightLeg.DepartureGreenwichDTTM;
                                        PostLeg.ScheduleDTTMLocal = preFlightLeg.DepartureDTTMLocal;

                                        if (AutoPopulateLegTimes)
                                        {
                                            PostLeg.OutboundDTTM = preFlightLeg.DepartureGreenwichDTTM;
                                            PostLeg.TimeOff = ((DateTime)preFlightLeg.DepartureGreenwichDTTM).ToString("HH:mm");
                                            PostLeg.BlockOut = ((DateTime)preFlightLeg.DepartureGreenwichDTTM.Value.Subtract(TaxiTime)).ToString("HH:mm");
                                            PostLeg.OutboundDTTM = preFlightLeg.DepartureGreenwichDTTM.Value.Subtract(TaxiTime);
                                        }
                                    }
                                    if (preFlightLeg.ArrivalGreenwichDTTM != null && preFlightLeg.ArrivalGreenwichDTTM != DateTime.MinValue)
                                    {
                                        if (AutoPopulateLegTimes)
                                        {
                                            PostLeg.InboundDTTM = preFlightLeg.ArrivalGreenwichDTTM;
                                            PostLeg.TimeOn = ((DateTime)preFlightLeg.ArrivalGreenwichDTTM).ToString("HH:mm");
                                            PostLeg.BlockIN = ((DateTime)preFlightLeg.ArrivalGreenwichDTTM.Value.Add(TaxiTime)).ToString("HH:mm");
                                            PostLeg.InboundDTTM = preFlightLeg.ArrivalGreenwichDTTM.Value.Add(TaxiTime);
                                        }
                                    }
                                }
                            }

                            //loading Local time values
                            if (preFlightLeg.DepartureDTTMLocal != null && preFlightLeg.DepartureDTTMLocal != DateTime.MinValue) // Check for NULL
                            {
                                if (AutoPopulateLegTimes)
                                {
                                    PostLeg.TimeOffLocal = ((DateTime)preFlightLeg.DepartureDTTMLocal).ToString("HH:mm");
                                    if (preFlightLeg.DepartureDTTMLocal != null)
                                        PostLeg.BlockOutLocal = ((DateTime)preFlightLeg.DepartureDTTMLocal.Value.Subtract(TaxiTime)).ToString("HH:mm");
                                }
                            }
                            if (preFlightLeg.ArrivalDTTMLocal != null && preFlightLeg.ArrivalDTTMLocal != DateTime.MinValue)
                            {
                                if (AutoPopulateLegTimes)
                                {
                                    PostLeg.TimeOnLocal = ((DateTime)preFlightLeg.ArrivalDTTMLocal).ToString("HH:mm");
                                    if (preFlightLeg.ArrivalDTTMLocal != null)
                                        PostLeg.BlockInLocal = ((DateTime)preFlightLeg.ArrivalDTTMLocal.Value.Add(TaxiTime)).ToString("HH:mm");
                                }
                            }


                            TimeSpan blockhours = new TimeSpan(0, 0, 0);
                            TimeSpan flighthours = new TimeSpan(0, 0, 0);
                            //calcualtion block and flight hours
                            if (AutoPopulateLegTimes)
                            {
                                if (PostLeg.InboundDTTM != null && PostLeg.OutboundDTTM != null)
                                {
                                    blockhours = PostLeg.InboundDTTM.Value.Subtract(PostLeg.OutboundDTTM.Value);
                                    flighthours = blockhours.Subtract(TaxiTime.Add(TaxiTime));
                                }
                            }

                            //calculating flight hours
                            if (preFlightLeg.CrewDutyRulesID != null && preFlightLeg.CrewDutyRule != null)
                                PostLeg.CrewCurrency = preFlightLeg.CrewDutyRule.CrewDutyRuleCD;

                            decimal _dutyBegin = 0;
                            decimal _dutyEnd = 0;

                            decimal dutyhours = Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(((blockhours.Days * 24 + blockhours.Hours) + ":" + blockhours.Minutes), false, (int)UserPrincipal._HoursMinutesCONV));

                            if (!string.IsNullOrEmpty(PostLeg.CrewCurrency))
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient RulesService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var RulesVal = RulesService.GetCrewDutyRulesWithFilters(0, PostLeg.CrewCurrency, false).EntityList;
                                    if (RulesVal != null && RulesVal.Count > 0)
                                    {
                                        if (RulesVal[0].DutyDayBeingTM != null && RulesVal[0].DutyDayBeingTM.HasValue)
                                            _dutyBegin = Convert.ToDecimal(RulesVal[0].DutyDayBeingTM);
                                        if (RulesVal[0].DutyDayEndTM != null && RulesVal[0].DutyDayEndTM.HasValue)
                                            _dutyEnd = Convert.ToDecimal(RulesVal[0].DutyDayEndTM);
                                        if (RulesVal[0].FedAviatRegNum != null && !string.IsNullOrEmpty(RulesVal[0].FedAviatRegNum))
                                        {
                                            PostLeg.FedAviationRegNum = RulesVal[0].FedAviatRegNum.Trim();
                                        }
                                    }
                                }
                            }

                            if (dutyhours > 0 && _dutyBegin > 0)
                            {
                                if ((PostLeg.LegNUM == 1) || (previousPOLeg != null && previousPOLeg.IsDutyEnd == true))
                                    dutyhours = dutyhours + _dutyBegin;
                            }

                            if (((PostLeg.IsDutyEnd != null && PostLeg.IsDutyEnd == true) || (PreflightLegList.Count == PostLeg.LegNUM)) && dutyhours > 0 && _dutyEnd > 0)
                                dutyhours = dutyhours + _dutyEnd;

                            //Leg difference
                            TimeSpan diffhours = new TimeSpan(0, 0, 0);
                            if (previousPOLeg != null && previousPOLeg.InboundDTTM != null && PostLeg.OutboundDTTM != null && (previousPOLeg.IsDutyEnd == null || previousPOLeg.IsDutyEnd == false))
                            {
                                diffhours = PostLeg.OutboundDTTM.Value.Subtract(previousPOLeg.InboundDTTM.Value);
                            }
                            decimal dhour = Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(((diffhours.Days * 24 + diffhours.Hours) + ":" + diffhours.Minutes), false, (int)UserPrincipal._HoursMinutesCONV));

                            totalDutyHour = totalDutyHour + dutyhours + dhour;

                            PostLeg.BlockHours = Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(((blockhours.Days * 24 + blockhours.Hours) + ":" + blockhours.Minutes), false, (int)UserPrincipal._HoursMinutesCONV));
                            PostLeg.FlightHours = Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(((flighthours.Days * 24 + flighthours.Hours) + ":" + flighthours.Minutes), false, (int)UserPrincipal._HoursMinutesCONV));
                            PostLeg.DutyHrs = totalDutyHour;
                            PostLeg.BeginningDutyHours = FPKConversionHelper.ConvertTenthsToMins(Math.Round(_dutyBegin, 3).ToString());
                            PostLeg.EndDutyHours = FPKConversionHelper.ConvertTenthsToMins(Math.Round(_dutyEnd, 3).ToString());

                            if (PostLeg.IsDutyEnd != null && PostLeg.IsDutyEnd == true)
                                totalDutyHour = 0;

                            if (preFlightLeg.Distance != null)
                                PostLeg.Distance = preFlightLeg.Distance;
                            if (preFlightLeg.PassengerRequestorID != null)
                                PostLeg.PassengerRequestorID = preFlightLeg.PassengerRequestorID;
                            if (preFlightLeg.DepartmentID != null)
                                PostLeg.DepartmentID = preFlightLeg.DepartmentID;
                            if (preFlightLeg.AuthorizationID != null)
                                PostLeg.AuthorizationID = preFlightLeg.AuthorizationID;
                            if (preFlightLeg.FlightCategoryID != null)
                                PostLeg.FlightCategoryID = preFlightLeg.FlightCategoryID;
                            if (preFlightLeg.FedAviationRegNUM != null)
                                PostLeg.FedAviationRegNum = preFlightLeg.FedAviationRegNUM;
                            if (preFlightLeg.ClientID != null)
                                PostLeg.ClientID = preFlightLeg.ClientID;
                            if (preFlightLeg.FlightPurpose != null)
                                PostLeg.FlightPurpose = preFlightLeg.FlightPurpose;
                            if (preFlightLeg.LastUpdUID != null)
                                PostLeg.LastUpdUID = preFlightLeg.LastUpdUID;
                            if (preFlightLeg.LastUpdTS != null)
                                PostLeg.LastUpdTS = preFlightLeg.LastUpdTS;
                            if (preFlightLeg.RestHours != null)
                                PostLeg.RestHrs = preFlightLeg.RestHours;
                            if (preFlightLeg.FlightNUM != null)
                                PostLeg.FlightNum = preFlightLeg.FlightNUM;
                            if (preFlightLeg.AccountID != null)
                                PostLeg.AccountID = preFlightLeg.AccountID;
                            if (preFlightLeg.FlightCost != null)
                                PostLeg.FlightCost = preFlightLeg.FlightCost;

                            if (preFlightLeg.PassengerTotal != null)
                                PostLeg.PassengerTotal = preFlightLeg.PassengerTotal;
                            if (UserPrincipal._IsAutoFillAF)
                            {
                                //loading leg adjust parameter
                                if (UserPrincipal._AircraftBlockFlight != null && UserPrincipal._AircraftBlockFlight == 1)
                                {
                                    PostLeg.AirFrameHours = PostLeg.BlockHours;
                                    PostLeg.Engine1Hours = PostLeg.BlockHours;
                                    PostLeg.Engine2Hours = PostLeg.BlockHours;
                                    PostLeg.Engine3Hours = PostLeg.BlockHours;
                                    PostLeg.Engine4Hours = PostLeg.BlockHours;

                                    PostLeg.AirframeCycle = 1;
                                    PostLeg.Engine1Cycle = 1;
                                    PostLeg.Engine2Cycle = 1;
                                    PostLeg.Engine3Cycle = 1;
                                    PostLeg.Engine4Cycle = 1;
                                }
                                else
                                {
                                    PostLeg.AirFrameHours = PostLeg.FlightHours;
                                    PostLeg.Engine1Hours = PostLeg.FlightHours;
                                    PostLeg.Engine2Hours = PostLeg.FlightHours;
                                    PostLeg.Engine3Hours = PostLeg.FlightHours;
                                    PostLeg.Engine4Hours = PostLeg.FlightHours;

                                    PostLeg.AirframeCycle = 1;
                                    PostLeg.Engine1Cycle = 1;
                                    PostLeg.Engine2Cycle = 1;
                                    PostLeg.Engine3Cycle = 1;
                                    PostLeg.Engine4Cycle = 1;
                                }
                            }

                            if (UserPrincipal._FuelBurnKiloLBSGallon != null)
                            {
                                PostLeg.FuelBurn = UserPrincipal._FuelBurnKiloLBSGallon;
                            }

                            #endregion

                            #region "Crew"
                            PostLeg.PostflightCrews = new List<PostflightCrew>();
                            foreach (PreflightCrewList preflightCrew in preFlightLeg.PreflightCrewLists)
                            {
                                string spec3 = "0.000";
                                string spec4 = "0.000";

                                //DutyBeginning
                                TimeSpan dutyBegin = new TimeSpan(0);
                                try
                                {
                                    if (!string.IsNullOrEmpty(PostLeg.BeginningDutyHours))
                                    {
                                        dutyBegin = TimeSpan.ParseExact(PostLeg.BeginningDutyHours.ToString(), "c", null);
                                    }
                                }
                                catch
                                {
                                    //Manually Handled
                                }
                                TimeSpan schTime = new TimeSpan(0);
                                try
                                {
                                    if (PostLeg.ScheduledTM != null && PostLeg.ScheduledTM.HasValue)
                                        schTime = PostLeg.ScheduledTM.Value.TimeOfDay;
                                }
                                catch
                                {
                                    //Manually Handled
                                }
                                DateTime? dutyBeginDT = null;
                                try
                                {
                                    if (dutyBegin != null && PostLeg.ScheduledTM.HasValue)
                                    {
                                        if (PostLeg.ScheduledTM != null)
                                            dutyBeginDT = PostLeg.ScheduledTM.Value.Subtract(dutyBegin);
                                    }
                                }
                                catch
                                {
                                    //Manually Handled
                                }
                                //DutyEnd
                                TimeSpan dutyEnd = new TimeSpan(0);
                                try
                                {
                                    if (!string.IsNullOrEmpty(PostLeg.EndDutyHours))
                                        dutyEnd = TimeSpan.ParseExact(PostLeg.EndDutyHours.ToString(), "c", null);
                                }
                                catch
                                {
                                    //Manually Handled
                                }
                                TimeSpan EndTime = new TimeSpan(0);
                                try
                                {
                                    if (PostLeg.InboundDTTM != null && PostLeg.InboundDTTM.HasValue)
                                        EndTime = PostLeg.InboundDTTM.Value.TimeOfDay;
                                }
                                catch
                                {
                                    //Manually Handled
                                }

                                DateTime? dutyEndDT = null;
                                try
                                {
                                    if (dutyEnd != null)
                                    {
                                        if (PostLeg.InboundDTTM != null && PostLeg.InboundDTTM.HasValue)
                                            dutyEndDT = PostLeg.InboundDTTM.Value.Add(dutyBegin);
                                    }
                                }
                                catch
                                {
                                    //Manually Handled
                                }

                                PostflightCrew crew = new PostflightCrew();
                                if (preflightCrew.CrewID != null)
                                    crew.CrewID = preflightCrew.CrewID;
                                if (preflightCrew.CrewLastName != null)
                                    crew.CrewLastName = preflightCrew.CrewLastName;
                                if (preflightCrew.CrewMiddleName != null)
                                    crew.CrewMiddleName = preflightCrew.CrewMiddleName;
                                if (preflightCrew.CrewFirstName != null)
                                    crew.CrewFirstName = preflightCrew.CrewFirstName;
                                if (preflightCrew.CustomerID != null)
                                    crew.CustomerID = preflightCrew.CustomerID;

                                crew.IsRemainOverNightOverride = false;

                                if (PostLeg.InboundDTTM != null)
                                    crew.InboundDTTM = PostLeg.InboundDTTM;
                                if (PostLeg.OutboundDTTM != null)
                                    crew.OutboundDTTM = PostLeg.OutboundDTTM;
                                if (preflightCrew.DutyTYPE != null)
                                    crew.DutyTYPE = preflightCrew.DutyTYPE;
                                if (preFlightLeg.DutyHours != null)
                                    crew.DutyHours = PostLeg.DutyHrs;
                                if (preFlightLeg.FlightHours != null)
                                    crew.FlightHours = PostLeg.FlightHours;
                                if (PostLeg.BlockHours != null)
                                    crew.BlockHours = PostLeg.BlockHours;
                                if (PostLeg.BeginningDutyHours != null)
                                    crew.BeginningDuty = dutyBegin.ToString("hh\\:mm");
                                if (PostLeg.EndDutyHours != null)
                                    crew.DutyEnd = dutyEnd.ToString("hh\\:mm");

                                crew.RemainOverNight = RONCalculation(Trip, Convert.ToInt64(preflightCrew.CrewID), Convert.ToInt64(PostLeg.LegNUM), Convert.ToBoolean(PostLeg.IsDutyEnd), AircraftWingType); //Convert.ToDecimal(RON);
                                crew.DaysAway = DaysAwayCalculation(Trip, Convert.ToInt64(preflightCrew.CrewID), Convert.ToInt64(PostLeg.LegNUM), AircraftWingType, UserPrincipal); //DaysAwayCalc;

                                bool isStartDutyAdd = true;
                                if (previousPOLeg != null && previousPOLeg.PostflightCrews != null)
                                {
                                    foreach (PostflightCrew previewsPOCrew in previousPOLeg.PostflightCrews)
                                    {
                                        if (previewsPOCrew.CrewID == crew.CrewID)
                                            isStartDutyAdd = false;
                                    }
                                }

                                bool isEndDutyAdd = true;
                                if (nextPreLeg != null && nextPreLeg.PreflightCrewLists != null)
                                {
                                    foreach (PreflightCrewList pfCrew in nextPreLeg.PreflightCrewLists)
                                    {
                                        if (pfCrew.CrewID == crew.CrewID)
                                            isEndDutyAdd = false;
                                    }
                                }

                                if (PostLeg.LegNUM == 1 || (previousPOLeg != null && previousPOLeg.IsDutyEnd != null && previousPOLeg.IsDutyEnd == true) || isStartDutyAdd == true)
                                    crew.CrewDutyStartTM = dutyBeginDT;

                                if (PostLeg.LegNUM == PreflightLegList.Count() || (PostLeg.IsDutyEnd != null && PostLeg.IsDutyEnd == true) || isEndDutyAdd == true)
                                    crew.CrewDutyEndTM = dutyEndDT;

                                crew.TakeOffDay = 0;
                                crew.TakeOffNight = 0;
                                crew.LandingDay = 0;
                                crew.LandingNight = 0;
                                crew.Instrument = 0;
                                crew.Night = 0;
                                crew.ApproachPrecision = 0;
                                crew.ApproachNonPrecision = 0;
                                crew.Specification3 = Convert.ToDecimal(spec3);
                                crew.Specification4 = Convert.ToDecimal(spec4);
                                crew.OrderNUM = preflightCrew.OrderNUM; // Fix for #2631
                                crew.State = PostflightService.TripEntityState.Added;
                                PostLeg.PostflightCrews.Add(crew);
                            }
                            #endregion

                            #region "Pax"
                            PostLeg.PostflightPassengers = new List<PostflightPassenger>();
                            foreach (PreflightPassengerList preflightPax in preFlightLeg.PreflightPassengerLists)
                            {
                                PostflightPassenger pax = new PostflightPassenger();
                                if (preflightPax.CustomerID != null)
                                    pax.CustomerID = preflightPax.CustomerID;
                                if (preflightPax.PassengerID != null)
                                    pax.PassengerID = preflightPax.PassengerID;
                                if (preflightPax.PassengerFirstName != null)
                                    pax.PassengerFirstName = preflightPax.PassengerFirstName;
                                else
                                    pax.PassengerFirstName = string.Empty;
                                if (preflightPax.PassengerLastName != null)
                                    pax.PassengerLastName = preflightPax.PassengerLastName;
                                else
                                    pax.PassengerLastName = string.Empty;
                                if (preflightPax.PassengerMiddleName != null)
                                    pax.PassengerMiddleName = preflightPax.PassengerMiddleName;
                                else
                                    pax.PassengerMiddleName = string.Empty;
                                if (preflightPax.PassportID != null)
                                    pax.PassportNUM = preflightPax.PassportID.ToString();
                                else
                                    pax.PassportNUM = string.Empty;
                                if (preflightPax.FlightPurposeID != null)
                                    pax.FlightPurposeID = preflightPax.FlightPurposeID;

                                if (preflightPax.IsNonPassenger != null)
                                    pax.IsNonPassenger = preflightPax.IsNonPassenger;
                                if (preflightPax.OrderNUM != null)
                                    pax.OrderNUM = preflightPax.OrderNUM;
                                if (preflightPax.Billing != null)
                                    pax.Billing = preflightPax.Billing;
                                else
                                    pax.Billing = string.Empty;
                                if (preflightPax.WaitList != null)
                                    pax.WaitList = preflightPax.WaitList;
                                else
                                    pax.WaitList = string.Empty;
                                pax.IsDeleted = (bool)preflightPax.IsDeleted;

                                pax.State = PostflightService.TripEntityState.Added;
                                PostLeg.PostflightPassengers.Add(pax);
                            }

                            #region "check if blocked seats needs to be logged to postflight"
                            if (UserPrincipal._IsBlockSeats)
                            {
                                int TotalBlockedPaxCount = 0;
                                if (preFlightLeg.PassengerTotal != null && preFlightLeg.PassengerTotal > 0)
                                {
                                    if (preFlightLeg.PreflightPassengerLists != null && preFlightLeg.PreflightPassengerLists.Count >= 0)
                                    {
                                        TotalBlockedPaxCount = (int)(preFlightLeg.PassengerTotal - preFlightLeg.PreflightPassengerLists.Count);
                                    }
                                }

                                if (TotalBlockedPaxCount > 0)
                                {
                                    for (int blockedCount = 1; blockedCount <= TotalBlockedPaxCount; blockedCount++)
                                    {
                                        PostflightPassenger pax = new PostflightPassenger();
                                        if (preFlightLeg.CustomerID != null)
                                            pax.CustomerID = preFlightLeg.CustomerID;
                                        pax.PassengerID = null;
                                        pax.PassengerFirstName = "BLOCKED#" + blockedCount.ToString();
                                        pax.PassengerLastName = string.Empty;
                                        pax.PassengerMiddleName = string.Empty;
                                        pax.PassportNUM = string.Empty;
                                        //pax.FlightPurposeID = 0;
                                        if (UserPrincipal._FlightPurpose != null && !String.IsNullOrEmpty(UserPrincipal._FlightPurpose))
                                        {
                                            using (FlightPakMasterService.MasterCatalogServiceClient FlightPurposeService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                var FlightPurposeVal = FlightPurposeService.GetAllFlightPurposesWithFilters(0, 0, UserPrincipal._FlightPurpose, false, false, false).EntityList;
                                                //.GetFlightPurposeList().EntityList.Where(x => x.FlightPurposeCD.ToString().ToUpper().Trim().Equals(UserPrincipal.Identity._fpSettings._FlightPurpose.ToUpper().Trim()) && x.IsDeleted == false).ToList();
                                                if (FlightPurposeVal != null && FlightPurposeVal.Count > 0)
                                                    pax.FlightPurposeID = FlightPurposeVal[0].FlightPurposeID;
                                            }
                                        }

                                        pax.IsNonPassenger = true;
                                        pax.OrderNUM = preFlightLeg.PreflightPassengerLists.Count + blockedCount;
                                        pax.Billing = string.Empty;
                                        pax.WaitList = string.Empty;
                                        pax.State = PostflightService.TripEntityState.Added;
                                        PostLeg.PostflightPassengers.Add(pax);
                                    }
                                }
                            }
                            #endregion

                            #region SIFL
                            PostLeg.PostflightSIFLs = new List<PostflightSIFL>();
                            foreach (PreflightTripSIFL preflightSifl in preFlightLeg.PreflightTripSIFLs)
                            {
                                PostflightSIFL sifl = new PostflightSIFL();
                                sifl.State = PostflightService.TripEntityState.Added;
                                if (preflightSifl.CustomerID != null)
                                    sifl.CustomerID = preflightSifl.CustomerID;
                                if (preflightSifl.FareLevelID != null)
                                    sifl.FareLevelID = preflightSifl.FareLevelID;

                                sifl.PassengerFirstName = string.Empty;
                                sifl.PassengerLastName = string.Empty;
                                sifl.PassengerMiddleName = string.Empty;

                                if (preflightSifl.PassengerRequestorID != null)
                                {
                                    sifl.PassengerRequestorID = preflightSifl.PassengerRequestorID;

                                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var paxList = Service.GetPassengerbyPassengerRequestorID(Convert.ToInt64(preflightSifl.PassengerRequestorID)).EntityList;

                                        if (paxList.Count > 0)
                                        {
                                            if (!string.IsNullOrEmpty(paxList[0].FirstName))
                                                sifl.PassengerFirstName = paxList[0].FirstName;
                                            if (!string.IsNullOrEmpty(paxList[0].LastName))
                                                sifl.PassengerLastName = paxList[0].LastName;
                                            if (!string.IsNullOrEmpty(paxList[0].MiddleInitial))
                                                sifl.PassengerMiddleName = paxList[0].MiddleInitial;
                                        }
                                    }
                                }

                                if (preflightSifl.EmployeeTYPE != null)
                                    sifl.EmployeeTYPE = preflightSifl.EmployeeTYPE;
                                if (preflightSifl.DepartICAOID != null)
                                    sifl.DepartICAOID = preflightSifl.DepartICAOID;
                                if (preflightSifl.ArriveICAOID != null)
                                    sifl.ArriveICAOID = preflightSifl.ArriveICAOID;
                                if (preflightSifl.Distance != null)
                                    sifl.Distance = preflightSifl.Distance;
                                if (preflightSifl.LoadFactor != null)
                                    sifl.LoadFactor = preflightSifl.LoadFactor;
                                if (preflightSifl.AircraftMultiplier != null)
                                    sifl.AircraftMultiplier = preflightSifl.AircraftMultiplier;
                                if (preflightSifl.Rate1 != null)
                                    sifl.Rate1 = preflightSifl.Rate1;
                                if (preflightSifl.Rate2 != null)
                                    sifl.Rate2 = preflightSifl.Rate2;
                                if (preflightSifl.Rate3 != null)
                                    sifl.Rate3 = preflightSifl.Rate3;
                                if (preflightSifl.Distance1 != null)
                                    sifl.Distance1 = preflightSifl.Distance1;
                                if (preflightSifl.Distance2 != null)
                                    sifl.Distance2 = preflightSifl.Distance2;
                                if (preflightSifl.Distance3 != null)
                                    sifl.Distance3 = preflightSifl.Distance3;
                                if (preflightSifl.TeminalCharge != null)
                                    sifl.TeminalCharge = preflightSifl.TeminalCharge;
                                if (preflightSifl.AmtTotal != null)
                                    sifl.AmtTotal = preflightSifl.AmtTotal;
                                if (preflightSifl.LastUpdUID != null)
                                    sifl.LastUpdUID = preflightSifl.LastUpdUID;
                                if (preflightSifl.LastUpdTS != null)
                                    sifl.LastUpdTS = preflightSifl.LastUpdTS;
                                if (preflightSifl.AssociatedPassengerID != null)
                                    sifl.AssociatedPassengerID = preflightSifl.AssociatedPassengerID;

                                PostLeg.PostflightSIFLs.Add(sifl);
                            }
                            #endregion

                            #endregion

                            PostLeg.State = PostflightService.TripEntityState.Added;
                            postFlight.PostflightLegs.Add(PostLeg);

                            previousPOLeg = PostLeg;

                        }
                    }

                    postFlight.State = PostflightService.TripEntityState.Added;
                    using (PostflightServiceClient Service = new PostflightServiceClient())
                    {
                        var ReturnValue = Service.Update(postFlight);
                        if (ReturnValue != null && ReturnValue.EntityInfo != null)
                        {
                            long logID = ReturnValue.EntityInfo.POLogID;

                            if (logID > 0)
                            {
                                // Clear All Postflight Sessions
                                HttpContext.Current.Session.Remove("POSTFLIGHTMAIN");
                                HttpContext.Current.Session.Remove("HOMEBASEKEY");
                                HttpContext.Current.Session.Remove("FLEETRATEKEY");
                                HttpContext.Current.Session.Remove("SIFLRATEKEY");
                                HttpContext.Current.Session.Remove("FLEETPROFILEKEY");
                                HttpContext.Current.Session.Remove("POEXCEPTION");
                                HttpContext.Current.Session.Remove("POPAXINFO");
                                HttpContext.Current.Session.Remove("POCREWINFO");
                                HttpContext.Current.Session.Remove("POSTFLIGHTMAIN_RETAIN");
                                HttpContext.Current.Session.Remove("SIFLPERSONAL");

                                PostflightService.PostflightMain obj = new PostflightService.PostflightMain();
                                obj.POLogID = logID;
                                var returnVal = Service.GetTrip(obj);
                                if (returnVal.ReturnFlag)
                                {
                                    FlightPak.Web.ViewModels.Postflight.PostflightLogViewModel pfViewModel = FlightPak.Web.Views.Transactions.PostFlight.PostflightTripManager.InjectWholePODbModelToLogViewModel(returnVal.EntityInfo);
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.Lock(EntitySet.Postflight.PostflightMain, returnVal.EntityInfo.POLogID);
                                        if (returnValue.ReturnFlag)
                                        {
                                            pfViewModel.PostflightMain.POEditMode = true;
                                            returnVal.EntityInfo.TripActionStatus = TripAction.RecordNowLockedForEdit;
                                        }
                                    }
                                    HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentPostflightLog] = (PostflightService.PostflightMain)returnVal.EntityInfo;
                                    HttpContext.Current.Session[FlightPak.Web.Framework.Constants.WebSessionKeys.CurrentPostflightLog] = pfViewModel;

                                    // Bind the Saved Exceptions into the Session
                                    if (returnVal.EntityInfo.PostflightTripExceptions != null && returnVal.EntityInfo.PostflightTripExceptions.Count > 0)
                                        HttpContext.Current.Session["POEXCEPTION"] = returnVal.EntityInfo.PostflightTripExceptions;
                                    FlightPak.Web.Views.Transactions.PostFlightValidator.ReCalculateCrewDutyHour();
                                    result = true;
                                }
                            }
                        }
                    }
                }
                return result;
            }
        }

        /// <summary>
        /// Method to Calculate RON
        /// </summary>
        /// <param name="Crewid">Pass Crew ID</param>
        /// <param name="LegNum">Pass Leg Number</param>
        /// <returns>Return RON Value</returns>
        private static decimal RONCalculation(PreflightService.PreflightMain Trip, Int64 Crewid, Int64 LegNum, Boolean IsDutyEnd, String ronAircraftWingType)
        {
            decimal RON = 0;

            if (Trip != null && Trip.PreflightLegs != null)
            {
                PreflightService.PreflightLeg SelectLeg = Trip.PreflightLegs.Where(x => x.IsDeleted == false && x.LegNUM == LegNum).SingleOrDefault();
                UserPrincipalViewModel UserPrincipal = TripManagerBase.getUserPrincipal();
                if (IsDutyEnd)
                {
                    DateTime? ScheduledTM = null;
                    DateTime? OutboundDTTM = null;
                    DateTime? InboundDTTM = null;
                    DateTime dutyBeginRONDT = DateTime.MinValue;
                    DateTime dutyEndRONDT = DateTime.MinValue;
                    int counter = 0;
                    
                    bool AutoPopulateLegTimes = false;
                    AutoPopulateLegTimes = UserPrincipal._IsAutoPopulated;
                   
                    TimeSpan TaxiTime = new TimeSpan(0, 0, 0);
                    if (!string.IsNullOrEmpty(UserPrincipal._TaxiTime))
                    {
                        TaxiTime = TimeSpan.ParseExact(UserPrincipal._TaxiTime, "c", null);
                    }

                    foreach (PreflightService.PreflightLeg Leg in Trip.PreflightLegs.Where(x => x.IsDeleted == false && x.LegNUM > SelectLeg.LegNUM).ToList().OrderBy(x => x.LegNUM))
                    {
                        if (Leg.LegNUM != SelectLeg.LegNUM)
                        {
                            if (!string.IsNullOrEmpty(ronAircraftWingType) && ronAircraftWingType == "F") //Fixed Wing Aircraft
                            {
                                if (UserPrincipal._LogFixed == 1) //Load Home Time
                                {
                                    if (Leg.HomeDepartureDTTM != null && Leg.HomeDepartureDTTM != DateTime.MinValue) // Check for NULL
                                    {
                                        ScheduledTM = Leg.HomeDepartureDTTM;

                                        if (AutoPopulateLegTimes)
                                            OutboundDTTM = Leg.HomeDepartureDTTM.Value.Subtract(TaxiTime);
                                    }
                                }
                                else if (UserPrincipal._LogFixed == 2) //Load UTC Time
                                {
                                    if (Leg.DepartureGreenwichDTTM != null && Leg.DepartureGreenwichDTTM != DateTime.MinValue) // Check for NULL
                                    {
                                        ScheduledTM = Leg.DepartureGreenwichDTTM;

                                        if (AutoPopulateLegTimes)
                                            OutboundDTTM = Leg.DepartureGreenwichDTTM.Value.Subtract(TaxiTime);
                                    }
                                }
                            }
                            else if (!string.IsNullOrEmpty(ronAircraftWingType) && ronAircraftWingType == "R") //Rotary Wing Aircraft
                            {
                                if (UserPrincipal._LogRotary == 1) //Load Local Time
                                {
                                    if (Leg.DepartureDTTMLocal != null && Leg.DepartureDTTMLocal != DateTime.MinValue) // Check for NULL
                                    {
                                        ScheduledTM = Leg.DepartureDTTMLocal;

                                        if (AutoPopulateLegTimes)
                                            OutboundDTTM = Leg.DepartureDTTMLocal.Value.Subtract(TaxiTime);
                                    }
                                }
                                else if (UserPrincipal._LogRotary == 2) //Load UTC Time
                                {
                                    if (Leg.DepartureGreenwichDTTM != null && Leg.DepartureGreenwichDTTM != DateTime.MinValue) // Check for NULL
                                    {
                                        ScheduledTM = Leg.DepartureGreenwichDTTM;

                                        if (AutoPopulateLegTimes)
                                            OutboundDTTM = Leg.DepartureGreenwichDTTM.Value.Subtract(TaxiTime);
                                    }
                                }
                            }

                            if (Leg.PreflightCrewLists != null)
                            {
                                foreach (PreflightService.PreflightCrewList Crew in Leg.PreflightCrewLists.Where(x => x.IsDeleted == false).ToList())
                                {
                                    if (Crew.CrewID == Crewid)
                                    {
                                        if (UserPrincipal._DutyBasis != null && UserPrincipal._DutyBasis == 2) // No Out Date Display
                                        {
                                            if (ScheduledTM.HasValue)
                                                dutyEndRONDT = ScheduledTM.Value;
                                        }
                                        else
                                        {
                                            if (OutboundDTTM.HasValue)
                                                dutyEndRONDT = OutboundDTTM.Value;
                                        }
                                        counter = 1;
                                        break;
                                    }
                                }
                            }
                            if (counter == 1)
                                break;
                        }
                    }


                    if (!string.IsNullOrEmpty(ronAircraftWingType) && ronAircraftWingType == "F") //Fixed Wing Aircraft
                    {
                        if (UserPrincipal._LogFixed == 1) //Load Home Time
                        {
                            if (SelectLeg.HomeArrivalDTTM != null && SelectLeg.HomeArrivalDTTM != DateTime.MinValue)
                            {
                                if (AutoPopulateLegTimes)
                                    InboundDTTM = SelectLeg.HomeArrivalDTTM.Value.Add(TaxiTime);
                                else
                                    InboundDTTM = SelectLeg.HomeArrivalDTTM.Value;
                            }
                        }
                        else if (UserPrincipal._LogFixed == 2) //Load UTC Time
                        {
                            if (SelectLeg.ArrivalGreenwichDTTM != null && SelectLeg.ArrivalGreenwichDTTM != DateTime.MinValue)
                            {
                                if (AutoPopulateLegTimes)
                                    InboundDTTM = SelectLeg.ArrivalGreenwichDTTM.Value.Add(TaxiTime);
                                else
                                    InboundDTTM = SelectLeg.ArrivalGreenwichDTTM.Value;
                            }
                        }
                    }
                    else if (!string.IsNullOrEmpty(ronAircraftWingType) && ronAircraftWingType == "R") //Rotary Wing Aircraft
                    {
                        if (UserPrincipal._LogRotary == 1) //Load Local Time
                        {
                            if (SelectLeg.ArrivalDTTMLocal != null && SelectLeg.ArrivalDTTMLocal != DateTime.MinValue)
                            {
                                if (AutoPopulateLegTimes)
                                    InboundDTTM = SelectLeg.ArrivalDTTMLocal.Value.Add(TaxiTime);
                                else
                                    InboundDTTM = SelectLeg.ArrivalDTTMLocal.Value;
                            }
                        }
                        else if (UserPrincipal._LogRotary == 2) //Load UTC Time
                        {
                            if (SelectLeg.ArrivalGreenwichDTTM != null && SelectLeg.ArrivalGreenwichDTTM != DateTime.MinValue)
                            {
                                if (AutoPopulateLegTimes)
                                    InboundDTTM = SelectLeg.ArrivalGreenwichDTTM.Value.Add(TaxiTime);
                                else
                                    InboundDTTM = SelectLeg.ArrivalGreenwichDTTM.Value;
                            }
                        }
                    }

                    if (InboundDTTM != null)
                        dutyBeginRONDT = InboundDTTM.Value;

                    if (dutyEndRONDT != DateTime.MinValue && dutyBeginRONDT != DateTime.MinValue)
                    {
                        RON = dutyEndRONDT.Date.Subtract(dutyBeginRONDT.Date).Days;

                        if (Convert.ToDecimal(RON) < 0)
                            RON = 0;
                    }
                }
            }
            return RON;
        }

        /// <summary>
        /// Method to Calculate Days Away for Crew
        /// </summary>
        /// <param name="Crewid">Pass Crew ID</param>
        /// <param name="LegNum">Pass Leg Number</param>
        /// <returns>Returns Days Away Value</returns>
        public static decimal DaysAwayCalculation(PreflightService.PreflightMain Trip, Int64 Crewid, Int64 LegNum, String dwAircraftWingType, UserPrincipalViewModel UserPrincipal)
        {
            int daysAway = 0;

            if (Trip != null && Trip.PreflightLegs != null)
            {
                PreflightService.PreflightLeg previousLeg = Trip.PreflightLegs.Where(l => l.IsDeleted == false && l.State != PreflightService.TripEntityState.Deleted && l.LegNUM < LegNum).OrderByDescending(ln => ln.LegNUM).FirstOrDefault();
                bool bCrewInPrevLeg = previousLeg != null && previousLeg.PreflightCrewLists != null && previousLeg.PreflightCrewLists.Where(c => c.IsDeleted == false && c.State != PreflightService.TripEntityState.Deleted && c.CrewID == Crewid).Count() > 0;
                if (bCrewInPrevLeg == false)
                    previousLeg = null;

                PreflightService.PreflightLeg SelectLeg = Trip.PreflightLegs.Where(x => x.IsDeleted == false && x.LegNUM == LegNum).FirstOrDefault();

                DateTime? ScheduledTM = null;
                DateTime? OutboundDTTM = null;
                DateTime? InboundDTTM = null;
                DateTime? previousScheduledTM = null;
                DateTime? previousOutboundTM = null;
                DateTime? previousInboundTM = null;
                
                bool AutoPopulateLegTimes = false;
                AutoPopulateLegTimes = UserPrincipal._IsAutoPopulated;

                TimeSpan TaxiTime = new TimeSpan(0, 0, 0);
                if (!string.IsNullOrEmpty(UserPrincipal._TaxiTime))
                {
                    TaxiTime = TimeSpan.ParseExact(UserPrincipal._TaxiTime, "c", null);
                }

                List<PreflightService.PreflightLeg> legsList = Trip.PreflightLegs.Where(x => x.IsDeleted == false && x.LegNUM < LegNum).OrderByDescending(x => x.LegNUM).ToList();

                if (!string.IsNullOrEmpty(dwAircraftWingType) && dwAircraftWingType == "F") //Fixed Wing Aircraft
                {
                    if (UserPrincipal._LogFixed == 1) //Load Home Time
                    {
                        if (SelectLeg.HomeDepartureDTTM != null && SelectLeg.HomeDepartureDTTM != DateTime.MinValue) // Check for NULL
                        {
                            ScheduledTM = SelectLeg.HomeDepartureDTTM;

                            if (AutoPopulateLegTimes)
                                OutboundDTTM = SelectLeg.HomeDepartureDTTM.Value.Subtract(TaxiTime);
                        }
                        if (previousLeg != null && previousLeg.HomeDepartureDTTM.HasValue && previousLeg.HomeDepartureDTTM != DateTime.MinValue)
                        {
                            previousScheduledTM = previousLeg.HomeDepartureDTTM.Value;
                            if (AutoPopulateLegTimes)
                                previousOutboundTM = previousLeg.HomeDepartureDTTM.Value.Subtract(TaxiTime);
                        }


                        if (SelectLeg.HomeArrivalDTTM != null && SelectLeg.HomeArrivalDTTM != DateTime.MinValue)
                        {
                            if (AutoPopulateLegTimes)
                                InboundDTTM = SelectLeg.HomeArrivalDTTM.Value.Add(TaxiTime);
                            else
                                InboundDTTM = SelectLeg.HomeArrivalDTTM.Value;
                        }

                        if (previousLeg != null && previousLeg.HomeArrivalDTTM.HasValue && previousLeg.HomeArrivalDTTM.Value != DateTime.MinValue)
                            previousInboundTM = previousLeg.HomeArrivalDTTM.Value;

                    }
                    else if (UserPrincipal._LogFixed == 2) //Load UTC Time
                    {
                        if (SelectLeg.DepartureGreenwichDTTM != null && SelectLeg.DepartureGreenwichDTTM != DateTime.MinValue) // Check for NULL
                        {
                            ScheduledTM = SelectLeg.DepartureGreenwichDTTM;

                            if (AutoPopulateLegTimes)
                                OutboundDTTM = SelectLeg.DepartureGreenwichDTTM.Value.Subtract(TaxiTime);
                        }
                        if (previousLeg != null && previousLeg.DepartureGreenwichDTTM.HasValue && previousLeg.DepartureGreenwichDTTM.Value != DateTime.MinValue)
                        {
                            previousScheduledTM = previousLeg.DepartureGreenwichDTTM.Value;
                            if (AutoPopulateLegTimes)
                                previousOutboundTM = previousLeg.DepartureGreenwichDTTM.Value.Subtract(TaxiTime);
                        }

                        if (SelectLeg.ArrivalGreenwichDTTM != null && SelectLeg.ArrivalGreenwichDTTM != DateTime.MinValue)
                        {
                            if (AutoPopulateLegTimes)
                                InboundDTTM = SelectLeg.ArrivalGreenwichDTTM.Value.Add(TaxiTime);
                            else
                                InboundDTTM = SelectLeg.ArrivalGreenwichDTTM.Value;
                        }

                        if (previousLeg != null && previousLeg.ArrivalGreenwichDTTM.HasValue && previousLeg.ArrivalGreenwichDTTM.Value != DateTime.MinValue)
                            previousInboundTM = previousLeg.ArrivalGreenwichDTTM.Value;
                    }
                }
                else if (!string.IsNullOrEmpty(dwAircraftWingType) && dwAircraftWingType == "R") //Rotary Wing Aircraft
                {
                    if (UserPrincipal._LogRotary == 1) //Load Local Time
                    {
                        if (SelectLeg.DepartureDTTMLocal != null && SelectLeg.DepartureDTTMLocal != DateTime.MinValue) // Check for NULL
                        {
                            ScheduledTM = SelectLeg.DepartureDTTMLocal;

                            if (AutoPopulateLegTimes)
                                OutboundDTTM = SelectLeg.DepartureDTTMLocal.Value.Subtract(TaxiTime);
                        }

                        if (previousLeg != null && previousLeg.DepartureDTTMLocal.HasValue && previousLeg.DepartureDTTMLocal.Value != DateTime.MinValue)
                        {
                            previousScheduledTM = previousLeg.DepartureDTTMLocal.Value;

                            if (AutoPopulateLegTimes)
                                previousOutboundTM = previousLeg.DepartureDTTMLocal.Value.Subtract(TaxiTime);
                        }


                        if (SelectLeg.ArrivalDTTMLocal != null && SelectLeg.ArrivalDTTMLocal != DateTime.MinValue)
                        {
                            if (AutoPopulateLegTimes)
                                InboundDTTM = SelectLeg.ArrivalDTTMLocal.Value.Add(TaxiTime);
                            else
                                InboundDTTM = SelectLeg.ArrivalDTTMLocal.Value;
                        }

                        if (previousLeg != null && previousLeg.ArrivalDTTMLocal.HasValue && previousLeg.ArrivalDTTMLocal.Value != DateTime.MinValue)
                            previousInboundTM = previousLeg.ArrivalDTTMLocal.Value;

                    }
                    else if (UserPrincipal._LogRotary == 2) //Load UTC Time
                    {
                        if (SelectLeg.DepartureGreenwichDTTM != null && SelectLeg.DepartureGreenwichDTTM != DateTime.MinValue) // Check for NULL
                        {
                            ScheduledTM = SelectLeg.DepartureGreenwichDTTM;

                            if (AutoPopulateLegTimes)
                                OutboundDTTM = SelectLeg.DepartureGreenwichDTTM.Value.Subtract(TaxiTime);
                        }

                        if (previousLeg != null && previousLeg.DepartureGreenwichDTTM.HasValue && previousLeg.DepartureGreenwichDTTM.Value != DateTime.MinValue)
                        {
                            previousScheduledTM = previousLeg.DepartureGreenwichDTTM.Value;

                            if (AutoPopulateLegTimes)
                                previousOutboundTM = previousLeg.DepartureGreenwichDTTM.Value.Subtract(TaxiTime);
                        }


                        if (SelectLeg.ArrivalGreenwichDTTM != null && SelectLeg.ArrivalGreenwichDTTM != DateTime.MinValue)
                        {
                            if (AutoPopulateLegTimes)
                                InboundDTTM = SelectLeg.ArrivalGreenwichDTTM.Value.Add(TaxiTime);
                            else
                                InboundDTTM = SelectLeg.ArrivalGreenwichDTTM.Value;
                        }
                        if (previousLeg != null && previousLeg.ArrivalGreenwichDTTM.HasValue && previousLeg.ArrivalGreenwichDTTM.Value != DateTime.MinValue)
                            previousInboundTM = previousLeg.ArrivalGreenwichDTTM.Value;

                    }
                }

                #region OLD Caclulation
                /*              
                if (legsList.Count == 0)
                {
                    if (UserPrincipal._DutyBasis != null && UserPrincipal._DutyBasis == 2) // No Out Date Display
                        scheduleDate = ScheduledTM.Value;
                    else
                    {
                        if (OutboundDTTM.HasValue)
                            scheduleDate = OutboundDTTM.Value;
                        else
                            scheduleDate = ScheduledTM.Value;
                    }

                    if (InboundDTTM.HasValue)
                        inDate = InboundDTTM.Value;

                    if (scheduleDate != DateTime.MinValue && inDate != DateTime.MinValue)
                        daysAway = inDate.Date.Subtract(scheduleDate.Date).Days + 1;
                }
                else
                {
                    int counter = 0;
                    foreach (PreflightService.PreflightLeg Leg in legsList)
                    {
                        if (Leg.PreflightCrewLists != null)
                        {
                            foreach (PreflightService.PreflightCrewList Crew in Leg.PreflightCrewLists.Where(x => x.IsDeleted == false).OrderBy(x => x.OrderNUM))
                            {
                                if (Crew.CrewID == Crewid)
                                {
                                    if (!string.IsNullOrEmpty(AircraftWingType) && AircraftWingType == "F") //Fixed Wing Aircraft
                                    {
                                        if (UserPrincipal._LogFixed == 1) //Load Home Time
                                        {
                                            if (Leg.HomeArrivalDTTM != null && Leg.HomeArrivalDTTM != DateTime.MinValue)
                                            {
                                                if (AutoPopulateLegTimes)
                                                    InboundDTTM = Leg.HomeArrivalDTTM.Value.Add(TaxiTime);
                                                else
                                                    InboundDTTM = Leg.HomeArrivalDTTM.Value;
                                            }
                                        }
                                        else if (UserPrincipal._LogFixed == 2) //Load UTC Time
                                        {
                                            if (Leg.ArrivalGreenwichDTTM != null && Leg.ArrivalGreenwichDTTM != DateTime.MinValue)
                                            {
                                                if (AutoPopulateLegTimes)
                                                    InboundDTTM = Leg.ArrivalGreenwichDTTM.Value.Add(TaxiTime);
                                                else
                                                    InboundDTTM = Leg.ArrivalGreenwichDTTM.Value;
                                            }
                                        }
                                    }
                                    else if (!string.IsNullOrEmpty(AircraftWingType) && AircraftWingType == "R") //Rotary Wing Aircraft
                                    {
                                        if (UserPrincipal._LogRotary == 1) //Load Local Time
                                        {
                                            if (Leg.ArrivalDTTMLocal != null && Leg.ArrivalDTTMLocal != DateTime.MinValue)
                                            {
                                                if (AutoPopulateLegTimes)
                                                    InboundDTTM = Leg.ArrivalDTTMLocal.Value.Add(TaxiTime);
                                                else
                                                    InboundDTTM = Leg.ArrivalDTTMLocal.Value;
                                            }
                                        }
                                        else if (UserPrincipal._LogRotary == 2) //Load UTC Time
                                        {
                                            if (Leg.ArrivalGreenwichDTTM != null && Leg.ArrivalGreenwichDTTM != DateTime.MinValue)
                                            {
                                                if (AutoPopulateLegTimes)
                                                    InboundDTTM = Leg.ArrivalGreenwichDTTM.Value.Add(TaxiTime);
                                                else
                                                    InboundDTTM = Leg.ArrivalGreenwichDTTM.Value;
                                            }
                                        }
                                    }

                                    if (InboundDTTM != null && InboundDTTM.HasValue)
                                        inDate = InboundDTTM.Value;

                                    counter = 1;
                                    break;
                                }
                            }
                        }
                        if (counter == 1)
                        {
                            break;
                        }
                    }

                    if (UserPrincipal._DutyBasis != null && UserPrincipal._DutyBasis == 2) // No Out Date Display
                    {
                        if (ScheduledTM != null && ScheduledTM.HasValue)
                            scheduleDate = ScheduledTM.Value;
                    }
                    else
                    {
                        if (OutboundDTTM != null && OutboundDTTM.HasValue)
                            scheduleDate = OutboundDTTM.Value;
                        else
                            scheduleDate = ScheduledTM.Value;
                    }

                    if (scheduleDate != DateTime.MinValue && inDate != DateTime.MinValue)
                        daysAway = scheduleDate.Date.Subtract(inDate.Date).Days;

                    if (InboundDTTM != null && InboundDTTM.HasValue)
                        daysAway += InboundDTTM.Value.Subtract(scheduleDate.Date).Days;
                }
*/
                #endregion

                DateTime scheduleDate = DateTime.MinValue;
                DateTime inDate = DateTime.MinValue;
                DateTime previousScheduleDate = DateTime.MinValue;
                DateTime previousInDate = DateTime.MinValue;

                if (previousLeg != null)
                {
                    if (UserPrincipal._DutyBasis != null && UserPrincipal._DutyBasis == 2) 
                    {
                        if ((previousScheduledTM != null))
                            previousScheduleDate = previousScheduledTM.Value;
                    }
                    else
                    {
                        if ((previousOutboundTM != null))
                            previousScheduleDate = previousOutboundTM.Value;
                    }

                    if (previousInboundTM != null)
                        previousInDate = previousInboundTM.Value;
                }

                if (UserPrincipal._DutyBasis != null && UserPrincipal._DutyBasis == 2)
                {
                    if ((ScheduledTM != null))
                        scheduleDate = ScheduledTM.Value;
                }
                else
                {
                    if ((OutboundDTTM != null))
                        scheduleDate = OutboundDTTM.Value;
                }

                if (InboundDTTM != null)
                    inDate = InboundDTTM.Value;

                if (previousLeg == null)
                {
                    daysAway = inDate.Date.Subtract(scheduleDate.Date).Days + 1;
                    daysAway = daysAway > 0 ? daysAway : 0;
                }
                else if (scheduleDate.Date == previousScheduleDate.Date && inDate.Date == previousInDate.Date)
                {
                    daysAway = 0;
                }
                else if (scheduleDate.Date == previousScheduleDate.Date && inDate.Date != previousInDate.Date)
                {
                    daysAway = inDate.Date.Subtract(scheduleDate.Date).Days;
                    daysAway = daysAway > 0 ? daysAway : 0;
                }
                else if (scheduleDate.Date != previousScheduleDate.Date && inDate.Date == previousInDate.Date)
                {
                    daysAway = inDate.Date.Subtract(scheduleDate.Date).Days;
                    daysAway = daysAway > 0 ? daysAway : 0;
                }
                else if (scheduleDate.Date != previousScheduleDate.Date && inDate.Date != previousInDate.Date)
                {
                    int daysBetweenLegs = scheduleDate.Date.Subtract(previousInDate.Date).Days - 1;
                    daysBetweenLegs = daysBetweenLegs > 0 ? daysBetweenLegs : 0;

                    daysAway = inDate.Date.Subtract(scheduleDate.Date).Days + 1 + daysBetweenLegs;
                    daysAway = daysAway > 0 ? daysAway : 0;
                }

            }
            return Convert.ToDecimal(daysAway);
        }

        //Call from Preflight
        public bool GetTripandConfirm(Int64 TripID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripID))
            {
                using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                {
                    bool tripmoved = false;
                    var result = PrefSvc.GetTrip(TripID);
                    PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                    if (result.ReturnFlag)
                    {
                        Trip = result.EntityList[0];
                        Session["PreflightTrip"] = Trip;

                        if (Trip.FleetID != null && Trip.FleetID > 0) //Bug 2375 - #4874
                        {
                            tripmoved = GetAndSavePreflight(false, true,false);
                        }
                    }
                    return tripmoved;
                }
            }
        }
    }
}