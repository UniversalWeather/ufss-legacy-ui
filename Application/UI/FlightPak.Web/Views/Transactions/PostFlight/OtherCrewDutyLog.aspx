﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/PostFlight.Master"
    AutoEventWireup="true" ClientIDMode="AutoID" CodeBehind="OtherCrewDutyLog.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.PostFlight.OtherCrewDutyLog" MaintainScrollPositionOnPostback="true" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ MasterType VirtualPath="~/Framework/Masters/PostFlight.Master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="PostFlightBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <%=Scripts.Render("~/bundles/postflightOtherCrewDutyLog") %>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true"
        ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="radCrewRosterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="CrewRosterPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewRosterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewRosterCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewDutyTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="CrewDutyTypePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewDutyTypePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewDutyTypeCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAircraftTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="AircraftTypePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/AircraftTypePopUp.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAircraftTypeCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="AircraftTypePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/AircraftTypePopUp.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="ClientCodePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="AirportMasterPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadHomeBasePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="HomeBasePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadRetrievePopup" runat="server" Width="750px" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PostFlight/PreflightSearchRetrieve.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadCopyMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                Title="Export Report Information" KeepInScreenBounds="true" AutoSize="false"
                Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="CopyUserGroupAccessPopUp.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" style="width: auto; float: left;">
        <table class="ocd_log_popup">
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" style="width: 100%;">
                        <tr>
                            <td align="left">
                                <div class="nav-space">
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left">
                                <div class="tab-nav-top">
                                    <span class="head-title">Other Crew Duty Log</span>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellpadding="0" cellspacing="0" class="head-sub-menu">
                        <tr>
                            <td>
                                <div class="status-list">
                                    <table width="100%">
                                        <tr>
                                            <td class="tdLabel130">
                                               <input type="checkbox" id="chkHomebase" style="margin-top:1px !important;"/><label for="chkHomebase">Home Base Only</label>
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <label id="lbGoTo">Go To</label>
                                                        </td>
                                                        <td>
                                                            <input id="tbGoTo" class="text80" maxlength="10" type="text" data-bind="datepicker: OtherCrewDuty.GotoDate" />
                                                        </td>
                                                        <td>
                                                            <input type="button" class="button" value="Search" id="btnSearch" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <div class="last-updated-text">
                                                <label id="lbLastUpdatedUser" style="position:relative; width:200px;" class="acnt_info" data-bind="text: OtherCrewDuty.LastModifiedDate" ></label>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="right">
                                <table cellpadding="0" cellspacing="0" id="lnkbuttons" style="width:100%;">
                                    <tr>
                                        <td style="text-align: right !important; padding-right: 6px;">
                                            <a href="#" id="lnkCopy" title="Copy" data-bind="click: lnkCopyClick">
                                                <img style="border:0px;vertical-align:middle;" alt="Copy" src="<%=ResolveClientUrl("~/App_Themes/Default/images/copy.png") %>"/></a>
                                            <a href="#" id="lnkInitInsert" title="New" data-bind="click: lnkInitInsertClick">
                                               <img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></a>
                                            <a href="#" id="lnkInitEdit" title="Edit" data-bind="click: lnkInitEditClick">
                                                <img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></a>
                                            <a href="#" id="lnkDelete" title="Delete" data-bind="click: lnkDeleteClick">
                                                <img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div id="divMessage" style="width:23%; display:none;margin-left: 7px">                            
                    </div>
                    <table>
                        <tr>
                           <td class="postflight-custom-grid">
                             <div class="jqgrid">
                                 <div>
                                     <table class="box1">
                                         <tr>
                                             <td>
                                                 <table id="dgOtherCrewDuty" class="table table-striped table-hover table-bordered"></table>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td>
                                                 <div class="grid_icon">
                                                     <div role="group" id="pg_gridPager"></div>
                                                     <div id="pagesizebox">
                                                         <span>Size:</span>
                                                         <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                                         <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(); return false;" />
                                                     </div>
                                                 </div>
                                             </td>
                                         </tr>
                                     </table>
                                 </div>
                             </div>
                            </td>
                        </tr>
                    </table>
                    <table cellpadding="0" cellspacing="0" width="100%" > 
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="50%">
                                            <table>
                                                <tr>
                                                    <td class="tdLabel100">
                                                        <span class="mnd_text">Crew</span>
                                                    </td>
                                                    <td>
                                                        <asp:HiddenField ID="hdnOtherCrewDutyInitializationObject" runat="server" />
                                                        <input type="text" id="tbCrew1" class="text80" onkeydown="return fnAllowAlphaNumeric(this, event)" onblur="return RemoveSpecialChars(this)" data-bind="enable: OCDEditMode, value: OtherCrewDuty.Crew.CrewCD, event: { change: CrewValidator_KO }"/>
                                                        <input type="hidden" data-bind="value: OtherCrewDuty.CrewID" id="hdnCrewCD" />
                                                        <input type="button" title="View Crew Roster" id="btnCrew" onclick="javascript: openWin('radCrewRosterPopup'); return false;" data-bind="enable: OCDEditMode, css: BrowseBtn" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                     <label id="lbCrewDesc" class="input_no_bg" data-bind="text: OtherCrewDuty.Crew.DisplayName.kotouppercase()"></label>
                                                                    <label id="lbCrewCode" class="alert-text"></label>  
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel100">
                                                        <span class="mnd_text">Duty</span>
                                                    </td>
                                                    <td>
                                                        <input type="text" id="tbDuty" class="text80" onblur="return RemoveSpecialChars(this)" data-bind="enable: OCDEditMode, value: OtherCrewDuty.CrewDutyType.DutyTypeCD, event: { change: CrewDutyTypeValidator_KO }"/>
                                                        <input type="hidden" id="hdnDuty" data-bind="value: OtherCrewDuty.DutyTypeID" />
                                                        <input type="button" title="View Crew Duty Types" id="btnDuty" onclick="javascript: openWin('radCrewDutyTypePopup'); return false;" data-bind="enable: OCDEditMode, css: BrowseBtn" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <label id="lbDutyDesc" data-bind="text: OtherCrewDuty.CrewDutyType.DutyTypesDescription.kotouppercase()" class="input_no_bg"></label>
                                                                    <label id="lbDutyCode" class="alert-text"></label>  
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel100">
                                                        <label id="lbSessionDate">Session Date</label>
                                                    </td>
                                                    <td>
                                                        <input id="tbSessionDate" class="text80" type="text" maxlength="10" data-bind="enable: OCDEditMode, datepicker: OtherCrewDuty.SessionDT"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel100">
                                                        <label id="lbAircraftType">Aircraft Type</label>
                                                    </td>
                                                    <td>
                                                        <input type="text" id="tbAircraftType" class="text80" onblur="return RemoveSpecialChars(this)" data-bind="enable: OCDEditMode, value: OtherCrewDuty.Aircraft.AircraftCD, event: { change: AircraftTypeValidator_KO }"/>
                                                        <input type="hidden" id="hdnAircraftID" data-bind="value: OtherCrewDuty.AircraftID"  />
                                                        <input type="button" id="btnAircraftType" title="View Aircraft Types" onclick="javascript: openWin('radAircraftTypePopup'); return false;" data-bind="enable: OCDEditMode, css: BrowseBtn"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                     <label id="lbAircraftTypeDesc" data-bind="text: OtherCrewDuty.Aircraft.AircraftDescription.kotouppercase()" class="input_no_bg"></label>
                                                                    <label id="lbAircraftTypeCode" class="alert-text"></label>  
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel100">
                                                        <label id="lbStartDate">Start Date</label>
                                                    </td>
                                                    <td>
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel90">
                                                                     <input id="tbStartDate" class="text80" type="text" maxlength="10" data-bind="datepicker: OtherCrewDuty.DepartureDTTMLocalDate, enable: OCDEditMode, event: { change: DateTimeChanged }"/>
                                                                </td>
                                                                <td>
                                                                    <input ID="tbStartTime" type="text" maxlength="5" data-bind="timepicker: OtherCrewDuty.DepartureDTTMLocalTime, enable: OCDEditMode, event: { change: DateTimeChanged }" onblur="return RemoveTimeValidationMessage();" class="datetimechange" />
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>                                               
                                                <tr>
                                                    <td class="tdLabel100">
                                                        <label id="lbEndDate">End Date</label>
                                                    </td>
                                                    <td>
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="tdLabel90">
                                                                    <input id="tbEndDate" class="text80" type="text" maxlength="10" data-bind="datepicker: OtherCrewDuty.ArrivalDTTMLocalDate, enable: OCDEditMode, event: { change: DateTimeChanged }"/>
                                                                </td>
                                                                <td>
                                                                    <input ID="tbEndTime" type="text" maxlength="5" data-bind="timepicker: OtherCrewDuty.ArrivalDTTMLocalTime, enable: OCDEditMode, event: { change: DateTimeChanged }" onblur="return RemoveTimeValidationMessage();" class="datetimechange" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top" width="50%">
                                            <table>
                                                <tr>
                                                    <td class="tdLabel100">
                                                        <label id="lbClient">Client</label>
                                                    </td>
                                                    <td>
                                                        <input type="text" id="tbClient" class="text80" data-bind="enable: OCDEditMode, value: OtherCrewDuty.Client.ClientCD, event: { change: ClientValidator_KO }" onblur="return RemoveSpecialChars(this)" />
                                                        <input type="hidden" id="hdnClient" data-bind="value: OtherCrewDuty.ClientID" />
                                                        <input type="button" data-bind="enable: OCDEditMode, css: BrowseBtn" title="View Clients" id="btnClient" onclick="javascript: openWin('RadClientCodePopup'); return false;" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                     <label id="lbClientDesc" data-bind="text: OtherCrewDuty.Client.ClientDescription.kotouppercase()" class="input_no_bg"></label>
                                                                    <label id="lbClientCode" class="alert-text"></label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel100">
                                                        <label id="lbICAO">ICAO</label>
                                                    </td>
                                                    <td>
                                                        <input type="text" id="tbICAO" class="text80" onblur="return RemoveSpecialChars(this)" data-bind="enable: OCDEditMode, value: OtherCrewDuty.Airport.IcaoID, event: { change: AirportValidator_KO } " />
                                                        <input type="hidden" id="hdnAirportID" data-bind="value: OtherCrewDuty.AirportID" />
                                                        <input type="button" data-bind="enable: OCDEditMode, css: BrowseBtn" title="Display Airports" id="btnICAO" onclick="javascript: openWin('radAirportPopup'); return false;" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <label id="lbICAODesc" data-bind="text: OtherCrewDuty.Airport.AirportName.kotouppercase(), attr: { 'title': OtherCrewDuty.Airport.tooltipInfo }" class="input_no_bg"></label>
                                                                    <label id="lbICAOcode" class="alert-text"></label>                                                                  
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel100">
                                                        <label id="lbHomeBase">Home Base</label>
                                                    </td>
                                                    <td>
                                                        <input type="text" id="tbHomeBase" class="text80" onblur="return RemoveSpecialChars(this)" data-bind="enable: OCDEditMode, value: OtherCrewDuty.Homebase.HomebaseCD, event: { change: HomebaseValidator_KO }"/>
                                                        <input type="hidden" id="hdnHomeBase" data-bind="value: OtherCrewDuty.HomeBaseID"/>
                                                        <input type="hidden" id="hdnHomeBaseAirportID" data-bind="value: OtherCrewDuty.Homebase.HomebaseAirportID"/>
                                                        <input type="button" data-bind="enable: OCDEditMode, css: BrowseBtn" title="Search for Home Base" id="btnHomeBase" onclick="javascript: openWin('RadHomeBasePopup'); return false;" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <label id="lbHomeBaseDesc" data-bind="text: OtherCrewDuty.Homebase.BaseDescription.kotouppercase(), attr: { 'title': OtherCrewDuty.Homebase.HomebaseAirport.tooltipInfo }" class="input_no_bg"></label>
                                                                    <label id="lbHomebaseCode" class="alert-text"></label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="tdLabel100" valign="top">
                                                        <label id="lbComment">Comment</label>
                                                    </td>
                                                    <td class="po_comment">
                                                        <span class="riSingle RadInput ">
                                                            <textarea id="tbComment" class="riTextBox" data-bind="enable: OCDEditMode, value: OtherCrewDuty.SimulatorDescription">
                                                            </textarea>
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <div id="divTimeValidateMessage" style="color: red; display: none;"></div>
                                                    </td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <fieldset>
                                                            <legend>Approaches </legend>
                                                            <table width="100%">
                                                                <tr>
                                                                    <td class="tdLabel80">
                                                                        <label id="lbPrecisions">Precision</label>
                                                                    </td>
                                                                    <td class="tdLabel20">
                                                                        <input type="text" id="tbPrecisions" class="text30" onkeypress="return fnAllowNumeric(this, event)" maxlength="2" data-bind="enable: OCDEditMode, value: OtherCrewDuty.ApproachPrecision" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label id="lbNonPrecisions">Non-Precision</label>
                                                                    </td>
                                                                    <td>
                                                                        <input type="text" id="tbNonPrecisions" class="text30" maxlength="2" onkeypress="return fnAllowNumeric(this, event)" data-bind="enable: OCDEditMode, value: OtherCrewDuty.ApproachNonPrecision" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </fieldset>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top" id="tdCustom">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <fieldset>
                                                            <table style="width: 100%;">
                                                                <tr>
                                                                    <td class="tdLabel50">
                                                                        <input type="checkbox" id="chkNav" data-bind="enable: OCDEditMode, checked: OtherCrewDuty.Specification1" /> <label id="lbchkNav" for="chkNav">Nav</label>
                                                                    </td>
                                                                    <td class="tdLabel50">
                                                                         <input type="checkbox" id="chkHld" data-bind="enable: OCDEditMode, checked: OtherCrewDuty.Sepcification2" /><label id="lbchkHld" for="chkHld">Hld</label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label id="lbCR">C/R</label>
                                                                    </td>
                                                                    <td>
                                                                        <input type="text" id="tbCR" class="text60" onblur="return validateEmptyTakeoffTextbox(this, event)" maxlength="3" onkeypress="return fnAllowNumericAndDot(this, event)" data-bind="enable: OCDEditMode, value: OtherCrewDuty.Specification3"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label id="lbPos">Pos</label>
                                                                    </td>
                                                                    <td>
                                                                        <input type="text" id="tbPos" class="text60" onblur="return validateEmptyTakeoffTextbox(this, event)" maxlength="3" onkeypress="return fnAllowNumericAndDot(this, event)" data-bind="enable: OCDEditMode, value: OtherCrewDuty.Specification4" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </fieldset>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <fieldset>
                                                            <table style="width: 100%;">
                                                                <tr>
                                                                    <td class="tdLabel50">
                                                                        <input type="checkbox" id="chkPIC" data-bind="enable: OCDEditMode, checked: OtherCrewDuty.DutyTYPEpic" /><label for="chkPIC">PIC</label><%--<label data-bind="text: OtherCrewDuty.DutyTYPEpic"></label>--%>
                                                                    </td>
                                                                    <td class="tdLabel50">
                                                                        <input type="checkbox" id="chkInstructor" data-bind="enable: OCDEditMode, checked: OtherCrewDuty.DutyTYPEinstructor, checkedValue: true" /><label for="chkInstructor">Instructor</label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <input type="checkbox" id="chkSIC" data-bind="enable: OCDEditMode, checked: OtherCrewDuty.DutyTYPEsic" /><label for="chkSIC">SIC</label>
                                                                    </td>
                                                                    <td>
                                                                        <input type="checkbox" id="chkAttendant" data-bind="enable: OCDEditMode, checked: OtherCrewDuty.DutyTYPEattendant" /><label for="chkAttendant">Attendant</label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <input type="checkbox" id="chkEngineer" data-bind="enable: OCDEditMode, checked: OtherCrewDuty.DutyTYPEengineer" /><label for="chkEngineer">Engineer</label>
                                                                    </td>
                                                                    <td>
                                                                        <input type="checkbox" id="chkOther" data-bind="enable: OCDEditMode, checked: OtherCrewDuty.DutyTYPEother" /><label for="chkOther">Other</label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </fieldset>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table style="width: 100%;">
                                    <tr>
                                        <td>
                                            <fieldset>
                                                <legend>Takeoff </legend>
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td>
                                                            <label id="lbTakeOffDay">Day</label>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="tbTakeOffDay" class="text40" maxlength="2" onblur="return validateEmptyTakeoffTextbox(this, event)" onkeypress="return fnAllowNumeric(this, event)" data-bind="enable: OCDEditMode, value: OtherCrewDuty.TakeOffDay" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label id="lbTakeOffNight">Night</label>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="tbTakeOffNight" class="text40" maxlength="2" onblur="return validateEmptyTakeoffTextbox(this, event)" onkeypress="return fnAllowNumeric(this, event)" data-bind="enable: OCDEditMode, value: OtherCrewDuty.TakeOffNight" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                        <td>
                                            <fieldset>
                                                <legend>Landings</legend>
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td>
                                                            <label id="lbLandingsDay">Day</label>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="tbLandingsDay" class="text40" maxlength="2" onblur="return validateEmptyTakeoffTextbox(this, event)" onkeypress="return fnAllowNumeric(this, event)" data-bind="enable: OCDEditMode, value: OtherCrewDuty.LandingDay" />
                                                           </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label id="lbLandingsNight">Night</label>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="tbLandingsNight" class="text40" maxlength="2" onblur="return validateEmptyTakeoffTextbox(this, event)" onkeypress="return fnAllowNumeric(this, event)" data-bind="enable: OCDEditMode, value: OtherCrewDuty.LandingNight" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                        <td>
                                            <fieldset>
                                                <legend>Hours</legend>
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td>
                                                            <label id="lbInstrument">Instrument</label>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="tbInstrument" class="tdLabel70"  onfocus="this.select();" onblur="return ValidateTimeForTenMin(this, 'Other Crew Duty')" onkeypress="return fnAllowNumericAndChar(this, event,': .')" data-bind="enable: OCDEditMode, value: OtherCrewDuty.InstrumentTM" />
                                                        </td>
                                                        <td>
                                                            <label id="lbFlight">Flight</label>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="tbFlight" class="tdLabel70"  onfocus="this.select();" onblur="return ValidateTimeForTenMin(this, 'Other Crew Duty')" onkeypress="return fnAllowNumericAndChar(this, event,': .')" data-bind="enable: OCDEditMode, value: OtherCrewDuty.FlightHoursTM" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label id="lbHoursNight">Night</label>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="tbHoursNight" class="tdLabel70"  onfocus="this.select();" onblur="return ValidateTimeForTenMin(this, 'Other Crew Duty')" onkeypress="return fnAllowNumericAndChar(this, event,': .')" data-bind="enable: OCDEditMode, value: OtherCrewDuty.NightTM" />
                                                        </td>
                                                        <td>
                                                            <label id="lbHoursDuty">Duty</label>
                                                        </td>
                                                        <td>
                                                            <input type="text" id="tbHoursDuty" class="tdLabel70"  onfocus="this.select();" onblur="return ValidateTimeForTenMin(this, 'Other Crew Duty')" onkeypress="return fnAllowNumericAndChar(this, event,': .')" data-bind="enable: OCDEditMode, value: OtherCrewDuty.DutyHoursTM" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                                <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                                    <tr align="right">
                                        <td>
                                            <input type="button" id="btnSaveChanges"  value="Save"  title="Save Changes" data-bind="enable: OCDEditMode, click:btnSaveChangesClick" />
                                        </td>
                                        <td>
                                            <input type="button" id="btnCancel"  value="Cancel" title="Cancel All Changes" data-bind="enable: OCDEditMode, click:btnCancelClick"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table id="tblHidden" style="display: none;">
            <tr>
                <td>
                    <asp:HiddenField ID="hdnTempHoursTxtbox" runat="server" />
                    <asp:HiddenField ID="hdnEditNew" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
