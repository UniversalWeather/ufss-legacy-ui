﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.PostflightService;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
//Added for Other crew duty log popup - Ramesh
using System.Web.Services;
using System.Web.Script.Services;
using System.Collections;
using FlightPak.Web.ViewModels;
using FlightPak.Web.ViewModels.Postflight;
using Omu.ValueInjecter;
using FlightPak.Web.Framework.Constants;
using System.Web;
using System.Net;
using FlightPak.Web.BusinessLite;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Transactions.PostFlight
{
    public partial class OtherCrewDutyLog : BaseSecuredPage
    {
        private static ExceptionManager exManager;
        private static UserPrincipalViewModel _UserPrincipal
        {
            get { return TripManagerBase.getUserPrincipal(); }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable OtherCrewDutyGridBind(bool homeBaseOnly, DateTime? gotoDate)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Hashtable>(() =>
                {
                    UserPrincipalViewModel userPrincipal = _UserPrincipal;
                    Hashtable RetObj = new Hashtable();
                    if (userPrincipal == null)
                    {
                        RetObj["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
                        return RetObj;
                    }

                    List<GetOtherCrewDutyLog> ObjRetval = new List<GetOtherCrewDutyLog>();
                    List<OtherCrewDutyLogViewModel> ocdVMList = new List<OtherCrewDutyLogViewModel>();
                    using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
                    {
                        ObjRetval = objService.GetOtherCrewDutyLog().EntityList;
                        if (homeBaseOnly)
                            ObjRetval = ObjRetval.Where(x => (x.HomeBaseID == userPrincipal._homeBaseId)).ToList();
                        if (gotoDate != null)
                            ObjRetval = ObjRetval.Where(x => x.SessionDT >= gotoDate).ToList();

                        var obj = (from u in ObjRetval
                                   where u.IsDeleted == false
                                   orderby u.SessionDT descending, u.SimulatorID descending
                                   select u);

                        foreach (GetOtherCrewDutyLog item in obj)
                        {
                            OtherCrewDutyLogViewModel ocdVM = new OtherCrewDutyLogViewModel(userPrincipal);
                            ocdVM = (OtherCrewDutyLogViewModel)ocdVM.InjectFrom(item);
                            ocdVMList.Add(ocdVM);
                        }
                        HttpContext.Current.Session[WebSessionKeys.OtherCrewDutyLog] = ocdVMList;
                    }

                    RetObj["meta"] = new PagingMetaData() { Page = ocdVMList.Count() / 10, Size = 10, total_items = ocdVMList.Count() };
                    RetObj["results"] = ocdVMList;
                    return RetObj;
                }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> DeleteOtherCrewDuty(Int64 OtherCrewSimulatorID)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<bool>>(() =>
                {
                    FSSOperationResult<bool> ResultObj = new FSSOperationResult<bool>();
                    if (ResultObj.IsAuthorized() == false)
                    {
                        return ResultObj;
                    }

                    using (PostflightServiceClient objService = new PostflightServiceClient())
                    {
                        var ReturnValue = objService.DeleteOtherCrewDuty(OtherCrewSimulatorID);

                        if (ReturnValue != null && ReturnValue.ReturnFlag == true)
                        {
                            ResultObj.StatusCode = HttpStatusCode.OK;
                            ResultObj.Success = true;
                        }
                        else
                        {
                            ResultObj.StatusCode = HttpStatusCode.OK;
                            ResultObj.Success = false;
                        }
                    }

                    return ResultObj;
                }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<long> Save(OtherCrewDutyLogViewModel OtherCrewDutyVM)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<long>>(() =>
              {
                  FSSOperationResult<long> ResultObj = new FSSOperationResult<long>();
                  if (ResultObj.IsAuthorized() == false)
                  {
                      return ResultObj;
                  }

                  PostflightSimulatorLog OtherCrewLog = new PostflightSimulatorLog();
                  OtherCrewDutyVM.TripID = null;
                  OtherCrewDutyVM.LastUptTS = DateTime.UtcNow;
                  OtherCrewDutyVM.IsDeleted = false;

                  using (PostflightServiceClient objService = new PostflightServiceClient())
                  {
                      OtherCrewLog = (PostflightSimulatorLog)OtherCrewLog.InjectFrom(OtherCrewDutyVM);
                      if (OtherCrewDutyVM.SimulatorID == 0)
                          OtherCrewLog.State = TripEntityState.Added;
                      else if (OtherCrewDutyVM.SimulatorID > 0)
                          OtherCrewLog.State = TripEntityState.Modified;

                      var ReturnValue = objService.UpdateOtherCrew(OtherCrewLog);

                      if (ReturnValue != null && ReturnValue.ReturnFlag == true)
                      {
                          ResultObj.StatusCode = HttpStatusCode.OK;
                          ResultObj.Success = true;
                          ResultObj.Result = ReturnValue.EntityInfo.SimulatorID;
                      }
                      else
                      {
                          ResultObj.StatusCode = HttpStatusCode.OK;
                          ResultObj.Success = false;
                      }
                  }
                  return ResultObj;
              }, FlightPak.Common.Constants.Policy.UILayer);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RadAjaxManager manager = RadAjaxManager.GetCurrent(Page);
            if (!IsLogPopup)
            {
                ContentPlaceHolder contentPlaceHolder = (ContentPlaceHolder)Master.Master.FindControl("MainContent");

                RadPanelBar pnlTracker = (RadPanelBar)contentPlaceHolder.FindControl("pnlTracker");
                RadPanelItem pnlTrackItem = pnlTracker.FindItemByValue("pnlItemTracker");
                pnlTrackItem.Expanded = false;
                pnlTracker.Enabled = false;        

                RadPanelBar pnlException = (RadPanelBar)contentPlaceHolder.FindControl("pnlException");
                RadPanelItem pnlExpItem = pnlException.FindItemByValue("pnlItemException");
                pnlExpItem.Expanded = false;
                pnlException.Enabled = false;

            }

            OtherCrewDutyLogViewModel ocdVM = new OtherCrewDutyLogViewModel(_UserPrincipal);
            var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(ocdVM);
            hdnOtherCrewDutyInitializationObject.Value = Microsoft.Security.Application.Encoder.HtmlEncode(json);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<OtherCrewDutyLogViewModel> GetOtherCrewDutyRowData(Int64 otherCrewSimulatorID)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<OtherCrewDutyLogViewModel>>(() =>
                {
                    FSSOperationResult<OtherCrewDutyLogViewModel> ResultObj = new FSSOperationResult<OtherCrewDutyLogViewModel>();
                    if (ResultObj.IsAuthorized() == false)
                    {
                        return ResultObj;
                    }
                    OtherCrewDutyLogViewModel ocdVM = new OtherCrewDutyLogViewModel(_UserPrincipal);
                    if (HttpContext.Current.Session[WebSessionKeys.OtherCrewDutyLog] != null)
                    {
                        List<OtherCrewDutyLogViewModel> ocdVMList = (List<OtherCrewDutyLogViewModel>)HttpContext.Current.Session[WebSessionKeys.OtherCrewDutyLog];
                        var ocdVMRow = ocdVMList.Where(x => x.SimulatorID == otherCrewSimulatorID).FirstOrDefault();
                        ocdVM = ocdVMRow;
                        ocdVM.Homebase = DBCatalogsDataLoader.GetHomebaseInfo_FromHomebaseCD(ocdVMRow.HomebaseCD);
                        ocdVM.Aircraft = DBCatalogsDataLoader.GetAircraftInfo_FromAircraftCD(ocdVMRow.AircraftCD);
                        ocdVM.Client = DBCatalogsDataLoader.GetClientInfo_FromClientCD(ocdVMRow.ClientCD);
                        ocdVM.Airport = DBCatalogsDataLoader.GetAirportInfo_FromICAO(ocdVMRow.IcaoID);
                        ocdVM.CrewDutyType = DBCatalogsDataLoader.GetCrewDutyTypeInfo_FromDutyTypeCD(ocdVMRow.DutyTypeCD);
                        ocdVM.Crew = DBCatalogsDataLoader.GetCrewInfo_FromCrewCD(ocdVMRow.CrewCD);

                        if (ocdVM.LastUptTS != null)
                        {
                            if (_UserPrincipal._airportId != null)
                            {
                                using (CalculationService.CalculationServiceClient calcSvc = new FlightPak.Web.CalculationService.CalculationServiceClient())
                                {
                                    DateTime? dt = ocdVM.LastUptTS;
                                    dt = calcSvc.GetGMT(_UserPrincipal._airportId, (DateTime)dt, false, false);
                                    ocdVM.LastModifiedDate = "Last Modified: " + ocdVM.LastUserID + "<br/>" + dt.FSSParseDateTimeString(_UserPrincipal._ApplicationDateFormat + " HH:mm") + " LCL" + " (" + ocdVM.LastUptTS.FSSParseDateTimeString(_UserPrincipal._ApplicationDateFormat + " HH:mm") + " UTC)";
                                }
                            }
                        }

                        ResultObj.StatusCode = HttpStatusCode.OK;
                        ResultObj.Success = true;
                        ResultObj.Result = ocdVM;
                    }
                    return ResultObj;
                }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<OtherCrewDutyLogViewModel> AddNewOtherCrewDutylog()
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<OtherCrewDutyLogViewModel>>(() =>
              {
                  FSSOperationResult<OtherCrewDutyLogViewModel> ResultObj = new FSSOperationResult<OtherCrewDutyLogViewModel>();
                  if (ResultObj.IsAuthorized() == false)
                  {
                      return ResultObj;
                  }
                  UserPrincipalViewModel userPrincipal = _UserPrincipal;
                  OtherCrewDutyLogViewModel ocdVM = new OtherCrewDutyLogViewModel(userPrincipal);
                  ocdVM.HomeBaseID = userPrincipal._homeBaseId.Value;
                  ocdVM.Homebase = DBCatalogsDataLoader.GetHomebaseInfo_FromHomebaseId(userPrincipal._homeBaseId.Value);
                  ocdVM.AirportID = userPrincipal._airportId;
                  ocdVM.Airport = DBCatalogsDataLoader.GetAirportInfo_FromICAO(userPrincipal._airportICAOCd);
                  ocdVM.SessionDT = DateTime.Now;
                  ocdVM.DepartureDTTMLocalDate = DateTime.Now.Date.ToString();
                  ocdVM.ArrivalDTTMLocalDate = DateTime.Now.Date.ToString();

                  ResultObj.StatusCode = HttpStatusCode.OK;
                  ResultObj.Success = true;
                  ResultObj.Result = ocdVM;
                  return ResultObj;
              }, FlightPak.Common.Constants.Policy.UILayer);
        }
    }
}
