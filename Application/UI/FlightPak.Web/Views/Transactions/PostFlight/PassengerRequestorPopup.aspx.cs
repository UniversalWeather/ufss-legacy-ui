﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.PostflightService;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Transactions.PostFlight
{
    public partial class PassengerRequestorPopup : BaseSecuredPage
    {
        private ExceptionManager exManager;
        string DateFormat = "MM/dd/yyyy";
        private long CQcustomerid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            if (Request.QueryString["ShowFillPopUp"] != null)
                            {
                                string show = "";
                                show = Request.QueryString["ShowFillPopUp"].ToString().Trim();

                                if (show == "TRUE")
                                {
                                    pnlFlightPurpose.Visible = true;
                                }
                                else
                                {
                                    pnlFlightPurpose.Visible = false;
                                }
                                if (!string.IsNullOrEmpty(Request.QueryString["CQcustomerID"]))
                                {
                                    CQcustomerid = Convert.ToInt64(Request.QueryString["CQcustomerID"]);
                                    if (CQcustomerid != 0)
                                    {
                                        chkCustomerID.Visible = true;
                                        //chkCustomerID.Checked = true;
                                    }
                                    else
                                    {
                                        chkCustomerID.Visible = false;
                                        //chkCustomerID.Checked = false;
                                    }
                                }
                                else
                                {
                                    chkCustomerID.Visible = false;
                                    //chkCustomerID.Checked = false;
                                }
                            }
                            else
                            {
                                chkCustomerID.Visible = false;
                                //chkCustomerID.Checked = false;
                                pnlFlightPurpose.Visible = false;
                            }

                            lbMessage.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            Session["selectedpostpaxItems"] = null;     
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Postflight.POPassenger);
                }
            }
        }

        protected void PassengerCatalog_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //using (PostflightServiceClient Service = new PostflightServiceClient())
                        //{
                        //    var retVal = Service.GetPaxList();
                        //    if (retVal.ReturnFlag == true)
                        //    {
                        //        dgPassengerCatalog.DataSource = retVal.EntityList.OrderBy(x => x.PassengerName);
                        //    }
                        //}
                        GetPassenger(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Postflight.POPassenger);
                }
            }
        }

        protected void PassengerCatalog_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                string resolvedurl = string.Empty;
                switch (e.CommandName)
                {
                    case RadGrid.InitInsertCommandName:
                        e.Canceled = true;
                        InsertSelectedRow();
                        break;
                    case RadGrid.EditCommandName:
                        e.Canceled = true;
                        e.Item.Selected = true;

                        GridDataItem item = dgPassengerCatalog.SelectedItems[0] as GridDataItem;
                        Session["PassengerRequestorID"] = item["PassengerRequestorID"].Text;
                        TryResolveUrl("/Views/Settings/People/PassengerCatalog.aspx?IsPopup=", out resolvedurl);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'rdPaxInfo');", true);
                        ScriptManager.RegisterStartupScript(this,this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radPaxInfoCRUDPopup');", true);
                        break;
                    case RadGrid.PerformInsertCommandName:
                        e.Canceled = true;
                        TryResolveUrl("/Views/Settings/People/PassengerCatalog.aspx?IsPopup=Add", out resolvedurl);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'rdPaxInfo');", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radPaxInfoCRUDPopup');", true);
                        break;
                    case RadGrid.FilterCommandName:
                        foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                        {
                            column.CurrentFilterValue = string.Empty;
                            column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                        }
		                Pair filterPair = (Pair)e.CommandArgument;
		                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();

                        break;
                    default:
                        break;
                }
            }
        }
        protected void dgPassengerCatalog_DeleteCommand(object source, GridCommandEventArgs e)
        {
            string PassengerRequestorID = string.Empty;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                {
                    try
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            e.Canceled = true;
                            MasterCatalogServiceClient PassengerService = new MasterCatalogServiceClient();
                            FlightPak.Web.FlightPakMasterService.Passenger oPassenger = new FlightPak.Web.FlightPakMasterService.Passenger();
                            GridDataItem item = dgPassengerCatalog.SelectedItems[0] as GridDataItem;
                            PassengerRequestorID = item.GetDataKeyValue("PassengerRequestorID").ToString();
                            oPassenger.PassengerRequestorID = Convert.ToInt64(item.GetDataKeyValue("PassengerRequestorID").ToString());
                            oPassenger.PassengerRequestorCD = item.GetDataKeyValue("PassengerRequestorCD").ToString();
                            if (item.GetDataKeyValue("IsActive") != null)
                            {
                                if (Convert.ToBoolean(item.GetDataKeyValue("IsActive")) == true)
                                {
                                    string alertMsg = "Active passenger cannot be deleted";
                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                                    ShowLockAlertPopup(alertMsg, ModuleNameConstants.Postflight.POPassenger);
                                    return;
                                }
                            }
                            oPassenger.IsDeleted = true;
                            var returnValue = CommonService.Lock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(item.GetDataKeyValue("PassengerRequestorID").ToString()));
                            if (!returnValue.ReturnFlag)
                            {
                                e.Item.Selected = true;
                                ShowLockAlertPopup(returnValue.LockMessage, ModuleNameConstants.Postflight.POPassenger);
                                return;
                            }
                            PassengerService.DeletePassengerRequestor(oPassenger);
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = true;

                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        //The exception will be handled, logged and replaced by our custom exception. 
                        ProcessErrorMessagePopup(ex, ModuleNameConstants.Postflight.POPassenger);
                    }
                    finally
                    {
                        //Unlock the record
                        var returnValue1 = CommonService.UnLock(EntitySet.Database.PassengerRequestor, Convert.ToInt64(PassengerRequestorID));
                    }
                }
            }
        }

        protected void PassengerCatalog_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                InsertSelectedRow();
            }
        }

        private void InsertSelectedRow()
        {
            List<POGetAllPassenger> selectedItems = (List<POGetAllPassenger>)Session["selectedpostpaxItems"];

            if (selectedItems != null)
            {
                if (selectedItems.Count > 0)
                {
                    // if (dgPassengerCatalog.SelectedItems.Count > 0)

                    List<POGetAllPassenger> PaxList = new List<POGetAllPassenger>();
                    string existingPax = string.Empty;

                    foreach (POGetAllPassenger paxinfo in selectedItems)
                    {
                        if (CheckIfExists(paxinfo.PassengerRequestorCD))
                        {
                            existingPax += paxinfo.PassengerRequestorCD + ",";

                            //lbMessage.Text = "Warning: Passenger Code(s) [" + existingPax.Remove((existingPax.Length - 1), 1) + "] already Exists.";
                            //lbMessage.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {

                            POGetAllPassenger pax = new POGetAllPassenger();
                            pax.PassengerRequestorID = paxinfo.PassengerRequestorID;
                            pax.PassengerRequestorCD = paxinfo.PassengerRequestorCD;
                            if (!string.IsNullOrEmpty(paxinfo.PassengerName))
                                pax.PassengerName = paxinfo.PassengerName;
                            else
                                pax.PassengerName = string.Empty;

                            if (!string.IsNullOrEmpty(paxinfo.FirstName))
                                pax.FirstName = paxinfo.FirstName;
                            else
                                pax.FirstName = string.Empty;
                            if (!string.IsNullOrEmpty(paxinfo.MiddleInitial))
                                pax.MiddleInitial = paxinfo.MiddleInitial;
                            else
                                pax.MiddleInitial = string.Empty;
                            if (!string.IsNullOrEmpty(paxinfo.LastName))
                                pax.LastName = paxinfo.LastName;
                            else
                                pax.LastName = string.Empty;
                            if (paxinfo.FlightPurposeID != null)
                                pax.FlightPurposeID = paxinfo.FlightPurposeID;
                            else
                                pax.FlightPurposeID = 0;
                            if (paxinfo.ClientID != null)
                                pax.ClientID = paxinfo.ClientID;
                            else
                                pax.ClientID = 0;
                            if (paxinfo.DepartmentID != null)
                                pax.DepartmentID = paxinfo.DepartmentID;
                            else
                                pax.DepartmentID = 0;
                            if (paxinfo.PhoneNum != null)
                                pax.PhoneNum = paxinfo.PhoneNum;
                            else
                                pax.PhoneNum = string.Empty;
                            if (paxinfo.HomebaseID != null)
                                pax.HomebaseID = paxinfo.HomebaseID;
                            else
                                pax.HomebaseID = 0;
                            if (paxinfo.IsActive != null)
                                pax.IsActive = paxinfo.IsActive;
                            else
                                pax.IsActive = false;
                            if (paxinfo.IsEmployeeType != null)
                                pax.IsEmployeeType = paxinfo.IsEmployeeType;
                            else
                                pax.IsEmployeeType = null;

                            // Passport Details
                            if (!string.IsNullOrEmpty(paxinfo.StandardBilling))
                                pax.StandardBilling = paxinfo.StandardBilling;
                            else
                                pax.StandardBilling = string.Empty;

                            if (!string.IsNullOrEmpty(paxinfo.NationalityName))
                                pax.NationalityName = paxinfo.NationalityName;
                            else
                                pax.NationalityName = string.Empty;
                            long passID = 0;
                            string passNum = string.Empty;
                            DateTime passExpDt = DateTime.MinValue;
                            DateTime passIssueDt = DateTime.MinValue;
                            string issueCity = string.Empty;
                            bool isDefaultPass = false;
                            string nation = string.Empty;

                            if (paxinfo.PassportID != null && paxinfo.PassportID != 0)
                                passID = Convert.ToInt64(paxinfo.PassportID);
                            else
                                passID = 0;

                            if (paxinfo.PassportNum != null && paxinfo.PassportNum != "0")
                                passNum = paxinfo.PassportNum;
                            else
                                passNum = string.Empty;

                            if (paxinfo.IssueCity != null)
                                issueCity = paxinfo.IssueCity;

                            if (paxinfo.PassportExpiryDT != null)
                                passExpDt = Convert.ToDateTime(paxinfo.PassportExpiryDT, CultureInfo.InvariantCulture);

                            if (paxinfo.IsDefaultPassport != null)
                                isDefaultPass = paxinfo.IsDefaultPassport;

                            if (paxinfo.IssueDT != null)
                                passIssueDt = Convert.ToDateTime(paxinfo.IssueDT, CultureInfo.InvariantCulture);

                            if (paxinfo.Nation != null && !string.IsNullOrEmpty(paxinfo.Nation))
                                nation = paxinfo.Nation.ToUpper();
                            pax.PassportID = passID;
                            pax.PassportNum = passNum; // String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", passNum);
                            pax.PassportExpiryDT = passExpDt;
                            pax.IssueDT = passIssueDt;
                            pax.IssueCity = issueCity;
                            pax.NationalityCD = nation;
                            pax.IsDefaultPassport = isDefaultPass;
                            pax.IsDeleted = false;
                            if (paxinfo.AssociatedWithCD != null)
                                pax.AssociatedWithCD = paxinfo.AssociatedWithCD;
                            else
                                pax.AssociatedWithCD = string.Empty;

                            if (paxinfo.IsSIFL != null)
                            {
                                pax.IsSIFL = paxinfo.IsSIFL;
                            }
                            //BRM 18
                            if (paxinfo.DepartPercentage != null)
                                pax.DepartPercentage = paxinfo.DepartPercentage;

                            PaxList.Add(pax);
                        }
                    }



                    //foreach (GridDataItem Item in dgPassengerCatalog.SelectedItems)
                    //{
                    //    if (CheckIfExists(Item.GetDataKeyValue("PassengerRequestorCD").ToString()))
                    //    {
                    //        existingPax += Item.GetDataKeyValue("PassengerRequestorCD").ToString() + ",";

                    //        //lbMessage.Text = "Warning: Passenger Code(s) [" + existingPax.Remove((existingPax.Length - 1), 1) + "] already Exists.";
                    //        //lbMessage.ForeColor = System.Drawing.Color.Red;
                    //    }
                    //    else
                    //    {

                    //        POGetAllPassenger pax = new POGetAllPassenger();
                    //        pax.PassengerRequestorID = Convert.ToInt64(Item.GetDataKeyValue("PassengerRequestorID").ToString());
                    //        pax.PassengerRequestorCD = Item.GetDataKeyValue("PassengerRequestorCD").ToString();
                    //        if (Item.GetDataKeyValue("PassengerName") != null)
                    //            pax.PassengerName = Item.GetDataKeyValue("PassengerName").ToString();
                    //        else
                    //            pax.PassengerName = string.Empty;

                    //        if (Item.GetDataKeyValue("FirstName") != null)
                    //            pax.FirstName = Item.GetDataKeyValue("FirstName").ToString();
                    //        else
                    //            pax.FirstName = string.Empty;
                    //        if (Item.GetDataKeyValue("MiddleInitial") != null)
                    //            pax.MiddleInitial = Item.GetDataKeyValue("MiddleInitial").ToString();
                    //        else
                    //            pax.MiddleInitial = string.Empty;
                    //        if (Item.GetDataKeyValue("LastName") != null)
                    //            pax.LastName = Item.GetDataKeyValue("LastName").ToString();
                    //        else
                    //            pax.LastName = string.Empty;
                    //        if (Item.GetDataKeyValue("FlightPurposeID") != null)
                    //            pax.FlightPurposeID = Convert.ToInt64(Item.GetDataKeyValue("FlightPurposeID").ToString());
                    //        else
                    //            pax.FlightPurposeID = 0;
                    //        if (Item.GetDataKeyValue("ClientID") != null)
                    //            pax.ClientID = Convert.ToInt64(Item.GetDataKeyValue("ClientID").ToString());
                    //        else
                    //            pax.ClientID = 0;
                    //        if (Item.GetDataKeyValue("DepartmentID") != null)
                    //            pax.DepartmentID = Convert.ToInt64(Item.GetDataKeyValue("DepartmentID").ToString());
                    //        else
                    //            pax.DepartmentID = 0;
                    //        if (Item.GetDataKeyValue("PhoneNum") != null)
                    //            pax.PhoneNum = Item.GetDataKeyValue("PhoneNum").ToString();
                    //        else
                    //            pax.PhoneNum = string.Empty;
                    //        if (Item.GetDataKeyValue("HomebaseID") != null)
                    //            pax.HomebaseID = Convert.ToInt64(Item.GetDataKeyValue("HomebaseID").ToString());
                    //        else
                    //            pax.HomebaseID = 0;
                    //        if (Item.GetDataKeyValue("IsActive") != null)
                    //            pax.IsActive = Convert.ToBoolean(Item.GetDataKeyValue("IsActive"));
                    //        else
                    //            pax.IsActive = false;
                    //        if (Item.GetDataKeyValue("IsEmployeeType") != null)
                    //            pax.IsEmployeeType = Item.GetDataKeyValue("IsEmployeeType").ToString();
                    //        else
                    //            pax.IsEmployeeType = null;

                    //        // Passport Details
                    //        if (Item.GetDataKeyValue("StandardBilling") != null)
                    //            pax.StandardBilling = Item.GetDataKeyValue("StandardBilling").ToString();
                    //        else
                    //            pax.StandardBilling = string.Empty;

                    //        if (Item.GetDataKeyValue("NationalityName") != null)
                    //            pax.NationalityName = Item.GetDataKeyValue("NationalityName").ToString();
                    //        else
                    //            pax.NationalityName = string.Empty;
                    //        long passID = 0;
                    //        string passNum = string.Empty;
                    //        DateTime passExpDt = DateTime.MinValue;
                    //        DateTime passIssueDt = DateTime.MinValue;
                    //        string issueCity = string.Empty;
                    //        bool isDefaultPass = false;
                    //        string nation = string.Empty;

                    //         if (Item.GetDataKeyValue("PassportID") != null && Item.GetDataKeyValue("PassportID").ToString() != "0")
                    //            passID = (long)Item.GetDataKeyValue("PassportID");
                    //        else
                    //            passID = 0;

                    //        if (Item.GetDataKeyValue("PassportNum") != null && Item.GetDataKeyValue("PassportNum").ToString() != "0")
                    //            passNum = Item.GetDataKeyValue("PassportNum").ToString();
                    //        else
                    //            passNum = string.Empty;

                    //        if (Item.GetDataKeyValue("IssueCity") != null)
                    //            issueCity = Item.GetDataKeyValue("IssueCity").ToString();

                    //        if (Item.GetDataKeyValue("PassportExpiryDT") != null && !string.IsNullOrEmpty(Item.GetDataKeyValue("PassportExpiryDT").ToString()))
                    //            passExpDt = Convert.ToDateTime(Item.GetDataKeyValue("PassportExpiryDT"), CultureInfo.InvariantCulture);

                    //        if (Item.GetDataKeyValue("IsDefaultPassport") != null)
                    //            isDefaultPass = Convert.ToBoolean(Item.GetDataKeyValue("IsDefaultPassport"));

                    //        if (Item.GetDataKeyValue("IssueDT") != null)
                    //            passIssueDt = Convert.ToDateTime(Item.GetDataKeyValue("IssueDT"), CultureInfo.InvariantCulture);

                    //        if (Item.GetDataKeyValue("Nation") != null && !string.IsNullOrEmpty(Item.GetDataKeyValue("Nation").ToString()))
                    //            nation = Item.GetDataKeyValue("Nation").ToString().ToUpper();
                    //        pax.PassportID = passID;
                    //        pax.PassportNum = passNum; // String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", passNum);
                    //        pax.PassportExpiryDT = passExpDt;
                    //        pax.IssueDT = passIssueDt;
                    //        pax.IssueCity = issueCity;
                    //        pax.NationalityCD = nation;
                    //        pax.IsDefaultPassport = isDefaultPass;
                    //        pax.IsDeleted = false;
                    //        if (Item.GetDataKeyValue("AssociatedWithCD") != null)
                    //            pax.AssociatedWithCD = Item.GetDataKeyValue("AssociatedWithCD").ToString();
                    //        else
                    //            pax.AssociatedWithCD = string.Empty;

                    //        if (Item.GetDataKeyValue("IsSIFL") != null)
                    //        {
                    //            pax.IsSIFL = (bool)Item.GetDataKeyValue("IsSIFL");
                    //        }
                    //        //BRM 18
                    //        if (Item.GetDataKeyValue("DepartPercentage") != null)
                    //            pax.DepartPercentage = Convert.ToDecimal(Item.GetDataKeyValue("DepartPercentage").ToString());

                    //        PaxList.Add(pax);
                    //    }
                    //}

                    if (existingPax != string.Empty)
                    {
                        RadWindowManager1.RadAlert("Warning: Passenger Code(s) [" + existingPax.Remove((existingPax.Length - 1), 1) + "] already Exists.", 330, 100, ModuleNameConstants.Postflight.POPassenger, "");
                    }
                    else
                    {
                        Session["NewPAXInfo"] = PaxList;
                        RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                    }
                }
                else
                {
                    ShowLockAlertPopup("Please select a Passenger", ModuleNameConstants.Database.PassengerRequestor);
                }
            }
            else
            {
                ShowLockAlertPopup("Please select a Passenger", ModuleNameConstants.Database.PassengerRequestor);

            }
        }


        private bool CheckIfExists(string PaxCode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PaxCode))
            {
                bool ReturnValue = false;
                try
                {
                    if (Session["POPAXINFO"] != null)
                    {
                        List<PassengerInLegs> PaxList = (List<PassengerInLegs>)Session["POPAXINFO"];
                        if (PaxList != null && PaxList.Count > 0)
                        {
                            var retVal = (from pax in PaxList
                                          where pax.PaxCode != null && pax.PaxCode.ToUpper().Trim() == PaxCode.ToUpper().Trim()
                                          select pax);

                            if (retVal.Count() > 0)
                                ReturnValue = true;
                        }
                    }
                }
                catch (Exception)
                {
                    //Manually Handled
                    // Due to error throw, when null value occured.
                }
                return ReturnValue;
            }
        }


        protected void Filter_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GetPassenger(true);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
        protected void chkCustomerID_OnCheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GetPassenger(true);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }

        protected void DefaultBlockedSeat_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbDefaultBlockedSeat.Text))
                        {
                            CheckAlreadyDefaultBlockedSeatExist();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }
        }

        private bool CheckAlreadyDefaultBlockedSeatExist()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((tbDefaultBlockedSeat.Text != string.Empty) && (tbDefaultBlockedSeat.Text != null))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RetValue = Service.GetAllFlightPurposesWithFilters(0, 0, tbDefaultBlockedSeat.Text, false, false, false).EntityList;
                        //.GetFlightPurposeList().EntityList.Where(x => x.FlightPurposeCD.Trim().ToUpper().Equals(tbDefaultBlockedSeat.Text.ToString().ToUpper().Trim())).ToList();
                        if (RetValue.Count() == 0 || RetValue == null)
                        {
                            cvDefaultBlockedSeat.IsValid = false;
                            RadAjaxManager1.FocusControl(tbDefaultBlockedSeat.ClientID);
                            // tbDefaultBlockedSeat.Focus();
                            return true;
                        }
                        else
                        {
                            hdnDefaultBlockedSeat.Value = RetValue[0].FlightPurposeID.ToString().Trim();
                            //RadAjaxManager1.FocusControl(tbSIFLLayoversHrs.ClientID);
                            // tbSIFLLayoversHrs.Focus();
                            return false;
                        }
                    }
                }
                return false;
            }
        }

        private void GetPassenger(bool CallBind)
        {
            DoSearchfilter(CallBind);
            //if (CallBind)
            //    dgPassengerCatalog.DataBind();

        }

        protected void chkFillAllLegs_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                flightpurposechange();
            }
        }

        private void flightpurposechange()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (chkFillAllLegs.Checked == true)
                {
                    tbDefaultBlockedSeat.Enabled = true;
                    btnDefaultBlockedSeat.Enabled = true;
                }
                else
                {
                    tbDefaultBlockedSeat.Enabled = false;
                    btnDefaultBlockedSeat.Enabled = false;
                }
            }
        }

        public void DoSearchfilter(bool bind)
        {
            long ClientId = 0;
            long PassengerId = 0;
            string PassengerCD = string.Empty;
            bool ActiveOnly = false;
            bool RequestorOnly = false;
            string ICAOID = string.Empty;
            long PaxGroupId = 0;
            long CQCustomerID = 0;

            using (FlightPakMasterService.MasterCatalogServiceClient Service1 = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                if (!string.IsNullOrEmpty(tbPaxGroup.Text))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient ObjPaxService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPak.Web.FlightPakMasterService.PassengerGroup> PaxGroupLists = new List<FlightPak.Web.FlightPakMasterService.PassengerGroup>();
                        var ObjRetValPax = ObjPaxService.GetPaxGroupList();
                        PaxGroupLists = ObjRetValPax.EntityList.Where(x => x.PassengerGroupCD.ToLower().Trim().Equals(tbPaxGroup.Text.ToLower().Trim())).ToList();
                        if (PaxGroupLists.Count > 0)
                        {
                            hdnPaxGroupID.Value = PaxGroupLists[0].PassengerGroupID.ToString();
                        }
                        else
                        {
                            lbcvPaxGroup.Text = System.Web.HttpUtility.HtmlEncode("Invalid Paxgroupcode");
                        }
                    }
                }
                else
                {
                    lbcvPaxGroup.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    hdnPaxGroupID.Value = string.Empty;
                }
                List<GetAllPassengerWithFilters> FilterPax = new List<GetAllPassengerWithFilters>(); 
                //List<GetAllPassengerWithFilters> FilterGroupPax = new List<GetAllPassengerWithFilters>();
                //if (!string.IsNullOrEmpty(hdnPaxGroupID.Value))
                // {
                //using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                //{
                List<FlightPak.Web.FlightPakMasterService.GetAllPassengerWithFilters> CrewGroupLists = new List<FlightPak.Web.FlightPakMasterService.GetAllPassengerWithFilters>();
                if (!string.IsNullOrEmpty(hdnPaxGroupID.Value))
                {
                    PaxGroupId = Convert.ToInt64(hdnPaxGroupID.Value);
                }
                if (chkActiveOnly.Checked)
                {
                    ActiveOnly = true;
                }
                if (chkRequestOnly.Checked)
                {
                    RequestorOnly = true;
                }

                var ObjRetVal = Service1.GetAllPassengerWithFilters(ClientId, CQCustomerID, PassengerId, PassengerCD, ActiveOnly, RequestorOnly, ICAOID, PaxGroupId);

                if (ObjRetVal.ReturnFlag == true)
                {
                    FilterPax = ObjRetVal.EntityList.ToList();
                }
                dgPassengerCatalog.DataSource = FilterPax;

                if (bind)
                {
                    dgPassengerCatalog.DataBind();
                }               

            }
        }


        protected void dgPassengerCatalog_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgPassengerCatalog,
                Page.Session);
            if (Session["selectedpostpaxItems"] != null)
            {
                List<POGetAllPassenger> selectedItems = (List<POGetAllPassenger>)Session["selectedpostpaxItems"];
                int count = 0;
                foreach (POGetAllPassenger pax in selectedItems)
                {
                    foreach (GridItem item in dgPassengerCatalog.MasterTableView.Items)
                    {
                        if (item is GridDataItem)
                        {
                            GridDataItem dataItem = (GridDataItem)item;
                            if (pax.PassengerRequestorID == Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerRequestorID"]))
                            {
                                count++;
                                CheckBox cb = (CheckBox)dataItem["CheckBoxTemplateColumn"].Controls[0].FindControl("CheckBox1");
                                cb.Checked = true;
                                dataItem.Selected = true;
                                break;
                            }
                        }
                    }
                }

                GridHeaderItem headerItem = dgPassengerCatalog.MasterTableView.GetItems(GridItemType.Header)[0] as GridHeaderItem;
                if (headerItem != null)
                    (headerItem.FindControl("headerChkbox") as CheckBox).Checked = (count == dgPassengerCatalog.PageSize ? true : false);

            }
        }

        protected void ToggleRowSelection(object sender, EventArgs e)
        {
            ((sender as CheckBox).NamingContainer as GridItem).Selected = (sender as CheckBox).Checked;
            bool checkHeader = true;
            foreach (GridDataItem dataItem in dgPassengerCatalog.MasterTableView.Items)
            {
                if (!(dataItem.FindControl("CheckBox1") as CheckBox).Checked)
                {
                    checkHeader = false;
                    break;
                }
            }
            GridHeaderItem headerItem = dgPassengerCatalog.MasterTableView.GetItems(GridItemType.Header)[0] as GridHeaderItem;
            (headerItem.FindControl("headerChkbox") as CheckBox).Checked = checkHeader;

            chk_CheckedChanged(sender, e);
        }
        protected void ToggleSelectedState(object sender, EventArgs e)
        {
            CheckBox headerCheckBox = (sender as CheckBox);
            List<POGetAllPassenger> selectedItems;
            if (Session["selectedpostpaxItems"] == null)
            {
                selectedItems = new List<POGetAllPassenger>();
            }
            else
            {
                selectedItems = (List<POGetAllPassenger>)Session["selectedpostpaxItems"];
            }


            foreach (GridDataItem dataItem in dgPassengerCatalog.MasterTableView.Items)
            {
                (dataItem.FindControl("CheckBox1") as CheckBox).Checked = headerCheckBox.Checked;
                dataItem.Selected = headerCheckBox.Checked;

                if (headerCheckBox.Checked)
                {
                    if (dataItem is GridDataItem)
                    {
                        GridDataItem dataItemValue = (GridDataItem)dataItem;
                        POGetAllPassenger paxinfo = new POGetAllPassenger();
                        paxinfo.PassengerRequestorID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerRequestorID"]); ;
                        paxinfo.PassengerRequestorCD = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerRequestorCD"]);
                        paxinfo.PassengerName = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerName"]);
                        paxinfo.FirstName = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["FirstName"]);
                        paxinfo.MiddleInitial = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["MiddleInitial"]);
                        paxinfo.LastName = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["LastName"]);
                        paxinfo.FlightPurposeID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["FlightPurposeID"]);
                        paxinfo.ClientID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["ClientID"]);
                        paxinfo.DepartmentID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["DepartmentID"]);
                        paxinfo.PhoneNum = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PhoneNum"]);
                        paxinfo.HomebaseID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["HomebaseID"]);
                        paxinfo.IsActive = Convert.ToBoolean(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["IsActive"]);
                        paxinfo.IsEmployeeType = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["IsEmployeeType"]);
                        paxinfo.StandardBilling = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["StandardBilling"]);
                        paxinfo.NationalityName = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["NationalityName"]);                   
                        paxinfo.PassportID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassportID"]);
                        paxinfo.PassportNum = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassportNum"]);
                        paxinfo.IssueCity = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["IssueCity"]);
                        paxinfo.PassportExpiryDT = Convert.ToDateTime(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassportExpiryDT"]);
                        paxinfo.IsDefaultPassport = Convert.ToBoolean(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["IsDefaultPassport"]);
                        paxinfo.Nation = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["Nation"]);
                        paxinfo.AssociatedWithCD = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["AssociatedWithCD"]);
                        paxinfo.IsSIFL = Convert.ToBoolean(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["IsSIFL"]);
                        paxinfo.DepartPercentage = Convert.ToDecimal(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["DepartPercentage"]);

                      
                        var exist = selectedItems.Where(X => X.PassengerRequestorID == paxinfo.PassengerRequestorID);
                        if (exist.Count() == 0)
                        {
                            selectedItems.Add(paxinfo);
                            Session["selectedpostpaxItems"] = selectedItems;
                        }

                    }
                }
                else
                {
                    if (dataItem is GridDataItem)
                    {
                        GridDataItem dataItemValue = (GridDataItem)dataItem;
                        POGetAllPassenger paxinfo = new POGetAllPassenger();
                        paxinfo.PassengerRequestorID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerRequestorID"].ToString());                       
                        int count = 0;
                        foreach (POGetAllPassenger paxinfovalue in selectedItems.ToList())
                        {
                            if (paxinfovalue.PassengerRequestorID == paxinfo.PassengerRequestorID)
                            {
                                selectedItems.RemoveAt(count);
                                Session["selectedpostpaxItems"] = selectedItems;
                            }

                            count++;
                        }
                    }

                }
            }


        }

        protected void chk_CheckedChanged(object sender, EventArgs e)
        {
            //System.Collections.ArrayList selectedItems;
            //System.Collections.ArrayList selectedHomebaseCD;        

            List<POGetAllPassenger> selectedItems;
            GridDataItem item = (GridDataItem)(sender as CheckBox).NamingContainer; //item is the row where the checkbox (that invoked the event) resides 
            CheckBox chkbx = (CheckBox)sender;

            if (Session["selectedpostpaxItems"] == null)
            {
                selectedItems = new List<POGetAllPassenger>();
            }
            else
            {
                selectedItems = (List<POGetAllPassenger>)Session["selectedpostpaxItems"];
            }

            if (chkbx.Checked)
            {
                GridDataItem dataItem = (GridDataItem)item;
                POGetAllPassenger paxinfo = new POGetAllPassenger();
                paxinfo.PassengerRequestorID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerRequestorID"]); ;
                paxinfo.PassengerRequestorCD = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerRequestorCD"]);
                paxinfo.PassengerName = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerName"]);
                paxinfo.FirstName = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["FirstName"]);
                paxinfo.MiddleInitial = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["MiddleInitial"]);
                paxinfo.LastName = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["LastName"]);
                paxinfo.FlightPurposeID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["FlightPurposeID"]);
                paxinfo.ClientID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["ClientID"]);
                paxinfo.DepartmentID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["DepartmentID"]);
                paxinfo.PhoneNum = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PhoneNum"]);
                paxinfo.HomebaseID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["HomebaseID"]);
                paxinfo.IsActive = Convert.ToBoolean(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["IsActive"]);
                paxinfo.IsEmployeeType = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["IsEmployeeType"]);
                paxinfo.StandardBilling = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["StandardBilling"]);
                paxinfo.NationalityName = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["NationalityName"]);
                paxinfo.NationalityName = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["NationalityName"]);
                paxinfo.PassportID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassportID"]);
                paxinfo.PassportNum = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassportNum"]);
                paxinfo.IssueCity = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["IssueCity"]);
                paxinfo.PassportExpiryDT = Convert.ToDateTime(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassportExpiryDT"]);
                paxinfo.IsDefaultPassport = Convert.ToBoolean(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["IsDefaultPassport"]);
                paxinfo.Nation = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["Nation"]);
                paxinfo.AssociatedWithCD = Convert.ToString(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["AssociatedWithCD"]);
                paxinfo.IsSIFL = Convert.ToBoolean(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["IsSIFL"]);
                paxinfo.DepartPercentage = Convert.ToDecimal(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["DepartPercentage"]);

                selectedItems.Add(paxinfo);
                Session["selectedpostpaxItems"] = selectedItems;
            }
            else
            {
                GridDataItem dataItem = (GridDataItem)item;
                POGetAllPassenger paxinfo = new POGetAllPassenger();
                paxinfo.PassengerRequestorID = Convert.ToInt64(dataItem.OwnerTableView.DataKeyValues[dataItem.ItemIndex]["PassengerRequestorID"]);              
                int count = 0;
                foreach (POGetAllPassenger paxinfovalue in selectedItems.ToList())
                {
                    if (paxinfovalue.PassengerRequestorID == paxinfo.PassengerRequestorID)
                    {
                        selectedItems.RemoveAt(count);
                        break;
                    }

                    count++;
                }
                Session["selectedpostpaxItems"] = selectedItems;
            }

            // Update database 
        } 

    }
}