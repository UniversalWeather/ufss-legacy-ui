﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/PostFlight.Master"
    AutoEventWireup="true" CodeBehind="PostFlightReports.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.PostFlight.PostFlightReports"
    MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Framework/Masters/PostFlight.Master" %>
<%@ Register TagPrefix="uc" TagName="Help" Src="~/UserControls/UCPOHelpIcons.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PostFlightHeadContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<%=ResolveClientUrl("~/Scripts/Common.js") %>"></script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PostFlightBodyContent" runat="server">
    <script type="text/javascript">

        function openReport(radWin) {
            var url = '';
            //People (or Crew/PAX)
            if (radWin == "CrewAlerts") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTCrewAlerts.xml';
            }
            else if (radWin == "CrewCurr") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTCrewCurrency.xml';
            }
            else if (radWin == "CrewCurrII") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTCrewCurrencyII.xml';
            }
            else if (radWin == "CrewExp") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostCrewExpense.xml';
            }
            else if (radWin == "CrewWork") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTCrewWorkloadIndex.xml';
            }
            else if (radWin == "NonFlightCurr") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTNonFlightCurrency.xml';
            }
            else if (radWin == "NonPilotFlightAnal") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTNonPilotFlightAnalysis.xml';
            }
            else if (radWin == "NonPilotLog") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTNonPilotLog.xml';
            }
            else if (radWin == "PassengerBillDetail") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTPaxBillingDetail.xml';
            }
            else if (radWin == "PassengerBillSum") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTPaxBillingSummary.xml';
            }
            else if (radWin == "PassengerFlightHis") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTPassengerFlightHistory.xml';
            }
            else if (radWin == "FlightPurSum") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTPassengerFlightPurposeSummary.xml';
            }
            else if (radWin == "PassFlightSum") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTPassengerFlightSummary.xml';
            }
            else if (radWin == "PassSIFLHis") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTPassengerSIFLHistory.xml';
            }
            else if (radWin == "PilotFlightAnal") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTPilotFlightAnalysis.xml';
            }
            else if (radWin == "PilotLog") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTPilotLog.xml';
            }
            else if (radWin == "PilotLogII") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTPilotLogII.xml';
            }
            else if (radWin == "PilotTypeAnal") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTPilotTypeAnalysis.xml';
            }
            else if (radWin == "ReqChBkDetDept") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTRequestorChargeBackDetailByDept.xml';
            }
            else if (radWin == "TimeInType") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTTimeInTypeReport.xml';
            }
            else if (radWin == "WorkloadInd") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTWorkloadIndex.xml';
            }

            //Billing
            else if (radWin == "BBBCode") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTBillingByCodeTrip.xml';
            }
            else if (radWin == "BYPassenger") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTBillingByPassengerTrip.xml';
            }
            else if (radWin == "DBYDepart") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTDetailBillingByDeptAuth.xml';
            }
            else if (radWin == "DBBTrip") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTDetailBillingByTrip.xml';
            }
            else if (radWin == "SBYDepart") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTSummaryBillingByDeptAuth.xml';
            }
            else if (radWin == "SBBTrip") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTSummaryBillingByTrip.xml';
            }


            //Fuel
            else if (radWin == "FPurSum") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostFuelPurchaseSummary.xml';
            }
            else if (radWin == "FSTrans") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostFuelSlipTransactions.xml';
            }
            else if (radWin == "FSumFLoc") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostFuelSummaryFuelLocator.xml';
            }
            else if (radWin == "FSumTNo") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostFuelSummaryTailNumber.xml';
            }

            //Charter
            else if (radWin == "CQCBSum") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTCQCustomerBillingSummary.xml';
            }
            else if (radWin == "CQCFHis") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTCQCustomerFlightHistory.xml';
            }
            else if (radWin == "CQLSHis") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTCQLeadSourceHistory.xml';
            }
            else if (radWin == "CQSSum") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTCQSalesPersonSummary.xml';
            }

            //Delay
            else if (radWin == "DDetail") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostDelayDetail.xml';
            }
            else if (radWin == "DSBLogNo") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostDelaySummary.xml';
            }

            //Emission (rename EU?)

            else if (radWin == "EUEmiReport") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostEUEmission.xml';
            }

            //Department
            else if (radWin == "DeptChBkAir") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTDepartmentChargeBackDetailByAircraft.xml';
            }
            else if (radWin == "DeptChBkDet") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTDepartmentChargeBackDetailByRequestor.xml';
            }
            else if (radWin == "DeptChBkSum") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTDepartmentChargeBackSummary.xml';
            }
            else if (radWin == "DeptFlightActReq") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTDepartmentFlightActivityByRequestor.xml';
            }
            else if (radWin == "DeptUsage") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTDepartmentUsage.xml';
            }
            else if (radWin == "DeptAuthor") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTDepartmentAuthorizationDetail.xml';
            }
            else if (radWin == "DeptAuthorDet") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTDeptAuthDetailStatistics.xml';
            }
            else if (radWin == "DeptAuthorSum") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTDepartmentAuthorizationSummary.xml';
            }
            else if (radWin == "DeptAuthorSumStat") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTDeptAuthSummaryStatistics.xml';
            }

            //Trip
            else if (radWin == "DomIntlSum") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostDomesticInternationalSummary.xml';
            }
            else if (radWin == "FCatAnal") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostFlightCategoryAnalysis.xml';
            }
            else if (radWin == "FLogExRpr") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostFlightLogExceptionReport.xml';
            }
            else if (radWin == "FOprSum") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostFlightOperationsSummary.xml';
            }
            else if (radWin == "LegAnalDis") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostLegAnalysisDistance.xml';
            }
            else if (radWin == "LegAnalHours") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostLegAnalysisHours.xml';
            }
            else if (radWin == "RouteFreq") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostRouteFrequencyReport.xml';
            }

            //Cost/Budget
            else if (radWin == "BAVarRpt") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTBudgetActualVarianceReport.xml';
            }
            else if (radWin == "CTBudSum") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostCostTrackingBudgetSummary.xml';
            }
            else if (radWin == "CTCSum") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTCostTrackingCostSummary.xml';
            }
            else if (radWin == "OprCSum") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTOperationalCostSummary.xml';
            }
            else if (radWin == "OprCSumCom") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTOperationalCostSummaryCombined.xml';
            }
            else if (radWin == "PayTAnal") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTPaymentTypeAnalysis.xml';
            }
            else if (radWin == "VPAnal") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTVendorPaymentAnalysis.xml';
            }

            //Fleet
            else if (radWin == "AFStat") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostAnnualFleetStatistics.xml';
            }
            else if (radWin == "FFHis") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostFleetFuelHistory.xml';
            }
            else if (radWin == "FFPurSum") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostFleetFuelPurchaseSummary.xml';
            }
            else if (radWin == "FOprHis") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostFleetOperationsHistory.xml';
            }
            else if (radWin == "FUtil") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostFleetUtilization.xml';
            }

            //Aircraft
            else if (radWin == "AirArr") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostAircraftArrivals.xml';
            }
            else if (radWin == "AirFHis") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostAircraftFlightHistory.xml';
            }
            else if (radWin == "AirPerAnal") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostAircraftPerformanceAnalysis.xml';
            }
            else if (radWin == "AirTypeUtil") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostAircraftTypeUtilization.xml';
            }

            //Expense (include w/ Billing ?)
            else if (radWin == "Expense") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTExpense.xml';
            }

            //Mileage
            else if (radWin == "StMileage") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostStateMileage.xml';
            }

           
            //Ad Hoc Reports
            else if (radWin == "AdHocRptEx") {
                url = '../../Reports/AdHocPostFlightReports.aspx?TabModuleSelect=PostFlight&xmlFilename=POSTAdhocExport.xml';
            }

            //FlightActivity
            else if (radWin == "FlightActivity") {
                url = '../../Reports/ReportViewerModule.aspx?TabModuleSelect=PostFlight&xmlFilename=PostflightActivity.xml';
            }

            var oWnd = radopen(url, "RadPostReportsPopup");
            
            if (IsMobileSafari()) {
                oWnd.set_autoSize(false);
                oWnd.setSize(800, 600);
                oWnd.set_behaviors(Telerik.Web.UI.WindowBehaviors.Move + Telerik.Web.UI.WindowBehaviors.Close + Telerik.Web.UI.WindowBehaviors.Maximize);
            }

            oWnd.Center();
        }
    </script>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadPostReportsPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Reports/PostflightReports.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" style="width: auto; float: left;">
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" width="200px" class="reports_new">
                                    <tr>
                                        <td class="tdlabel120" style="background-color: #E4E4E4;">
                                            <b>Crew/PAX</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkCrewAlerts" runat="server" Font-Bold="true" Text="Crew Alerts"
                                                OnClientClick="javascript:openReport('CrewAlerts');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkCrewCurr" runat="server" Font-Bold="true" Text="Crew Currency"
                                                OnClientClick="javascript:openReport('CrewCurr');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkCrewCurrII" runat="server" Font-Bold="true" Text="Crew Currency II"
                                                OnClientClick="javascript:openReport('CrewCurrII');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkCrewExp" runat="server" Font-Bold="true" Text="Crew Expense"
                                                OnClientClick="javascript:openReport('CrewExp');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkCrewWork" runat="server" Font-Bold="true" Text="Crew Workload Index"
                                                OnClientClick="javascript:openReport('CrewWork');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkNonFlightCurr" runat="server" Font-Bold="true" Text="Non-Flight Currency"
                                                OnClientClick="javascript:openReport('NonFlightCurr');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkNonPilotFlightAnal" runat="server" Font-Bold="true" Text="Non-Pilot Flight Analysis"
                                                OnClientClick="javascript:openReport('NonPilotFlightAnal');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkNonPilotLog" runat="server" Font-Bold="true" Text="Non-Pilot Log"
                                                OnClientClick="javascript:openReport('NonPilotLog');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkPassengerBillDetail" runat="server" Font-Bold="true" Text="Passenger Billing Detail"
                                                OnClientClick="javascript:openReport('PassengerBillDetail');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkPassengerBillSum" runat="server" Font-Bold="true" Text="Passenger Billing Summary"
                                                OnClientClick="javascript:openReport('PassengerBillSum');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkPassengerFlightHis" runat="server" Font-Bold="true" Text="Passenger Flight History"
                                                OnClientClick="javascript:openReport('PassengerFlightHis');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkFlightPurSum" runat="server" Font-Bold="true" Text="Passenger Flight Purpose Summary"
                                                OnClientClick="javascript:openReport('FlightPurSum');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkPassFlightSum" runat="server" Font-Bold="true" Text="Passenger Flight Summary"
                                                OnClientClick="javascript:openReport('PassFlightSum');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkPassSIFLHis" runat="server" Font-Bold="true" Text="Passenger SIFL History"
                                                OnClientClick="javascript:openReport('PassSIFLHis');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkPilotFlightAnal" runat="server" Font-Bold="true" Text="Pilot Flight Analysis"
                                                OnClientClick="javascript:openReport('PilotFlightAnal');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkPilotLog" runat="server" Font-Bold="true" Text="Pilot Log"
                                                OnClientClick="javascript:openReport('PilotLog');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkPilotLogII" runat="server" Font-Bold="true" Text="Pilot Log II"
                                                OnClientClick="javascript:openReport('PilotLogII');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkPilotTypeAnal" runat="server" Font-Bold="true" Text="Pilot Type Analysis"
                                                OnClientClick="javascript:openReport('PilotTypeAnal');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkTimeInType" runat="server" Font-Bold="true" Text="Time In Type "
                                                OnClientClick="javascript:openReport('TimeInType');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkWorkloadInd" runat="server" Font-Bold="true" Text="Workload Index"
                                                OnClientClick="javascript:openReport('WorkloadInd');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                                <table cellpadding="0" cellspacing="0" width="200px" class="reports_new">
                                    <tr>
                                        <td class="tdlabel120" style="background-color: #E4E4E4;">
                                            <b>Fuel</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkFPurSum" runat="server" Font-Bold="true" Text="Fuel Purchase Summary"
                                                OnClientClick="javascript:openReport('FPurSum');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkFSTrans" runat="server" Font-Bold="true" Text="Fuel Slip Transactions"
                                                OnClientClick="javascript:openReport('FSTrans');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkFSumFLoc" runat="server" Font-Bold="true" Text="Fuel Summary/Fuel Locator"
                                                OnClientClick="javascript:openReport('FSumFLoc');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkFSumTNo" runat="server" Font-Bold="true" Text="Fuel Summary/Tail No."
                                                OnClientClick="javascript:openReport('FSumTNo');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>

                                  <table cellpadding="0" cellspacing="0" width="300px" class="reports_new">
                                    <tr>
                                        <td class="tdlabel120" style="background-color: #E4E4E4;">
                                            <b>Charter</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkCQCBSum" runat="server" Font-Bold="true" Text="Charter Quote Customer Billing Summary"
                                                OnClientClick="javascript:openReport('CQCBSum');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkCQCFHis" runat="server" Font-Bold="true" Text="Charter Quote Customer Flight History"
                                                OnClientClick="javascript:openReport('CQCFHis');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkCQLSHis" runat="server" Font-Bold="true" Text="Charter Quote Lead Source History"
                                                OnClientClick="javascript:openReport('CQLSHis');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkCQSSum" runat="server" Font-Bold="true" Text="Charter Quote Salesperson Summary"
                                                OnClientClick="javascript:openReport('CQSSum');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" width="300px" class="reports_new">
                                    <tr>
                                        <td class="tdlabel120" style="background-color: #E4E4E4;">
                                            <b>Financial</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkBBBCode" runat="server" Font-Bold="true" Text="Billing By Billing Code/Trip"
                                                OnClientClick="javascript:openReport('BBBCode');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkBYPassenger" runat="server" Font-Bold="true" Text="Billing By Passenger/Trip"
                                                OnClientClick="javascript:openReport('BYPassenger');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkBAVarRpt" runat="server" Font-Bold="true" Text="Budget/Actual Variance Report"
                                                OnClientClick="javascript:openReport('BAVarRpt');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkCTBudSum" runat="server" Font-Bold="true" Text="Cost Tracking - Budget Summary"
                                                OnClientClick="javascript:openReport('CTBudSum');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkCTCSum" runat="server" Font-Bold="true" Text="Cost Tracking - Cost Summary"
                                                OnClientClick="javascript:openReport('CTCSum');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkDBYDepart" runat="server" Font-Bold="true" Text="Detail Billing By Department/Authorization"
                                                OnClientClick="javascript:openReport('DBYDepart');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkDBBTrip" runat="server" Font-Bold="true" Text="Detail Billing By Trip"
                                                OnClientClick="javascript:openReport('DBBTrip');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkExpense" runat="server" Font-Bold="true" Text="Expense" OnClientClick="javascript:openReport('Expense');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkOprCSum" runat="server" Font-Bold="true" Text="Operational Cost Summary"
                                                OnClientClick="javascript:openReport('OprCSum');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkOprCSumCom" runat="server" Font-Bold="true" Text="Operational Cost Summary - Combined"
                                                OnClientClick="javascript:openReport('OprCSumCom');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkPayTAnal" runat="server" Font-Bold="true" Text="Payment Type Analysis"
                                                OnClientClick="javascript:openReport('PayTAnal');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkSBYDepart" runat="server" Font-Bold="true" Text="Summary Billing By Department/Authorization"
                                                OnClientClick="javascript:openReport('SBYDepart');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkSBBTrip" runat="server" Font-Bold="true" Text="Summary Billing By Trip"
                                                OnClientClick="javascript:openReport('SBBTrip');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkVPAnal" runat="server" Font-Bold="true" Text="Vendor Payment Analysis"
                                                OnClientClick="javascript:openReport('VPAnal');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                                <table cellpadding="0" cellspacing="0" width="300px" class="reports_new">
                                    <tr>
                                        <td class="tdlabel120" style="background-color: #E4E4E4;">
                                            <b>Aircraft</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkAirArr" runat="server" Font-Bold="true" Text="Aircraft Arrivals"
                                                OnClientClick="javascript:openReport('AirArr');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkAirFHis" runat="server" Font-Bold="true" Text="Aircraft Flight History"
                                                OnClientClick="javascript:openReport('AirFHis');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkAirPerAnal" runat="server" Font-Bold="true" Text="Aircraft Performance Analysis"
                                                OnClientClick="javascript:openReport('AirPerAnal');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkAirTypeUtil" runat="server" Font-Bold="true" Text="Aircraft Type Utilization"
                                                OnClientClick="javascript:openReport('AirTypeUtil');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkAFStat" runat="server" Font-Bold="true" Text="Annual Fleet Statistics"
                                                OnClientClick="javascript:openReport('AFStat');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkEUEmiReport" runat="server" Font-Bold="true" Text="EU Emissions Report"
                                                OnClientClick="javascript:openReport('EUEmiReport');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkFOprHis" runat="server" Font-Bold="true" Text="Fleet Operations History"
                                                OnClientClick="javascript:openReport('FOprHis');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkFUtil" runat="server" Font-Bold="true" Text="Fleet Utilization"
                                                OnClientClick="javascript:openReport('FUtil');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                                <table cellpadding="0" cellspacing="0" width="300px" class="reports_new">
                                    <tr>
                                        <td class="tdlabel120" style="background-color: #E4E4E4;">
                                            <b>Fleet</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkFFHis" runat="server" Font-Bold="true" Text="Fleet Fuel History"
                                                OnClientClick="javascript:openReport('FFHis');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkFFPurSum" runat="server" Font-Bold="true" Text="Fleet Fuel Purchase Summary"
                                                OnClientClick="javascript:openReport('FFPurSum');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>                               
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" width="300px" class="reports_new">
                                    <tr>
                                        <td class="tdlabel120" style="background-color: #E4E4E4;">
                                            <b>Department</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkDeptChBkAir" runat="server" Font-Bold="true" Text="Department Charge Back Detail By Aircraft"
                                                OnClientClick="javascript:openReport('DeptChBkAir');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkDeptChBkDet" runat="server" Font-Bold="true" Text="Department Charge Back Detail By Requestor"
                                                OnClientClick="javascript:openReport('DeptChBkDet');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkDeptChBkSum" runat="server" Font-Bold="true" Text="Department Charge Back Summary"
                                                OnClientClick="javascript:openReport('DeptChBkSum');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkDeptFlightActReq" runat="server" Font-Bold="true" Text="Department Flight Activity By Requestor"
                                                OnClientClick="javascript:openReport('DeptFlightActReq');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkDeptUsage" runat="server" Font-Bold="true" Text="Department Usage"
                                                OnClientClick="javascript:openReport('DeptUsage');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkDeptAuthor" runat="server" Font-Bold="true" Text="Department/Authorization Detail"
                                                OnClientClick="javascript:openReport('DeptAuthor');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkDeptAuthorDet" runat="server" Font-Bold="true" Text="Department/Authorization Detail Statistics"
                                                OnClientClick="javascript:openReport('DeptAuthorDet');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkDeptAuthorSum" runat="server" Font-Bold="true" Text="Department/Authorization/Summary"
                                                OnClientClick="javascript:openReport('DeptAuthorSum');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkDeptAuthorSumStat" runat="server" Font-Bold="true" Text="Department/Authorization Summary Statistics"
                                                OnClientClick="javascript:openReport('DeptAuthorSumStat');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkReqChBkDetDept" runat="server" Font-Bold="true" Text="Requestor Charge Back Detail By Department"
                                                OnClientClick="javascript:openReport('ReqChBkDetDept');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                                <table cellpadding="0" cellspacing="0" width="300px" class="reports_new">
                                    <tr>
                                        <td class="tdlabel120" style="background-color: #E4E4E4;">
                                            <b>Trip</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkDDetail" runat="server" Font-Bold="true" Text="Delay Detail"
                                                OnClientClick="javascript:openReport('DDetail');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkDSBLogNo" runat="server" Font-Bold="true" Text="Delay Summary by Log No."
                                                OnClientClick="javascript:openReport('DSBLogNo');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkDomIntlSum" runat="server" Font-Bold="true" Text="Domestic/International Summary"
                                                OnClientClick="javascript:openReport('DomIntlSum');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkFCatAnal" runat="server" Font-Bold="true" Text="Flight Category Analysis"
                                                OnClientClick="javascript:openReport('FCatAnal');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkFLogExRpr" runat="server" Font-Bold="true" Text="Flight Log Exception Report"
                                                OnClientClick="javascript:openReport('FLogExRpr');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkFOprSum" runat="server" Font-Bold="true" Text="Flight Operations Summary"
                                                OnClientClick="javascript:openReport('FOprSum');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkLegAnalDis" runat="server" Font-Bold="true" Text="Leg Analysis/Distance"
                                                OnClientClick="javascript:openReport('LegAnalDis');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkLegAnalHours" runat="server" Font-Bold="true" Text="Leg Analysis/Hours"
                                                OnClientClick="javascript:openReport('LegAnalHours');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkRouteFreq" runat="server" Font-Bold="true" Text="Route Frequency"
                                                OnClientClick="javascript:openReport('RouteFreq');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkStMileage" runat="server" Font-Bold="true" Text="State Mileage"
                                                OnClientClick="javascript:openReport('StMileage');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                              
                                 <table cellpadding="0" cellspacing="0" width="300px" class="reports_new">
                                    <tr>
                                        <td class="tdlabel120" style="background-color: #E4E4E4;">
                                            <b>Ad Hoc Reports</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkAdHocRptEx" runat="server" Font-Bold="true" Text="Ad Hoc Report Extract"
                                                OnClientClick="javascript:openReport('AdHocRptEx');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                                <table cellpadding="0" cellspacing="0" width="300px" class="reports_new">
                                    <tr>
                                        <td class="tdlabel120" style="background-color: #E4E4E4;">
                                            <b>Custom Reports</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkFlightActivity" runat="server" Font-Bold="true" Text="Flight Activity"
                                                OnClientClick="javascript:openReport('FlightActivity');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
