﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PostFlightSelectPassenger.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.PostFlight.PostFlightSelectPassenger" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Select PAX</title>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <link href="/Scripts/jquery.alerts.css" rel="stylesheet" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <script src="/Scripts/jquery-migrate-1.0.0.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.alerts.js" type="text/javascript"></script>

    <script type="text/javascript">
        var selectedRowData = null;
        var jqgridTableId = '#gridSelectPassenger';                        
        
        $(document).ready(function () {

            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                if ((selr != null)) {
                    var rowData = $(jqgridTableId).getRowData(selr);
                    returnToParent(rowData, 0);
                }
                else {
                    jAlert('Please select a passenger.', popupTitle)
                }
                return false;
            });

            jQuery(jqgridTableId).jqGrid({
                url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/GetSelectedPassengerForPopup',
                mtype: 'POST',
                datatype: "json",
                cache: false,
                async: true,
                loadonce: true,
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
                serializeGridData: function (postData) {
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }
                    postData.LegNUM = $("#hdnSelectedLegNum").val();
                    return JSON.stringify(postData);
                },
                height: 260,
                width: 323,
                ignoreCase: true,
                autowidth: false,
                shrinkToFit: false,
                multiselect: false,
                colNames: ['PassengerRequestorID', 'PAX Code', 'EmpType', 'Name',''],
                colModel: [
                     { name: 'PassengerRequestorID', index: 'PassengerRequestorID', width: 60, key: true, hidden: true },
                     { name: 'PassengerRequestorCD', index: 'PassengerRequestorCD', width: 80,  sortable: true },
                     { name: 'EmpType', index: 'EmpType', hidden: true },
                     { name: 'PassengerName', index: 'PassengerName',width:185,sortable:true,search:true },
                     {
                         name: 'PassengerLastName', index: 'PassengerLastName', hidden:true, formatter: function (cellValue, options, rowData) {
                             return GetValidatedValue(rowData, "PassengerFirstName") + ", " + GetValidatedValue(rowData, "PassengerLastName") + " " + GetValidatedValue(rowData, "PassengerMiddleName");
                         }
                     }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData, 0);
                },
                onSelectRow: function (id, status) {
                    if (status)
                        selectedRowData = $(this).getRowData(id);
                    else
                        selectedRowData = null;
                }
            });
            $(jqgridTableId).jqGrid('filterToolbar', {
                defaultSearch: 'bw', searchOnEnter: false, stringResult: true
            });
            $(jqgridTableId).jqGrid('setGridParam', { datatype: 'Local', loadonce: true });

            function returnToParent(rowData, one) {
                
                var oArg = new Object();                
                oArg.PassengerRequestorID = GetValidatedValue(rowData,"PassengerRequestorID");
                oArg.PassengerRequestorCD = GetValidatedValue(rowData,"PassengerRequestorCD");
                oArg.PassengerName = GetValidatedValue(rowData, "PassengerName");
                oArg.IsEmployeeType = GetValidatedValue(rowData, "EmpType");

                oArg.Arg1 = rowData;
                oArg.CallingButton = "btnPax";
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">

        <div class="jqgrid">            
            <div>
                <table class="box1">                    
                    <tr>
                        <td>
                            <table id="gridSelectPassenger" class="table table-striped table-hover table-bordered"></table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="grid_icon">                                                                
                            </div>
                            <div style="padding: 5px 5px; text-align: right;">
                                <input id="btnSubmit" class="button okButton" value="OK" type="button" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
    
</body>
</html>
