﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Net;
using System.Globalization;
using Omu.ValueInjecter;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using FlightPak.Web.BusinessLite;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.Framework.Error;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.ViewModels;
using FlightPak.Web.ViewModels.MasterData;
using FlightPak.Web.ViewModels.Postflight;
using FlightPak.Web.PostflightService;
using FlightPak.Web.BusinessLite.Postflight;
using FlightPak.Web.BusinessLite.Preflight;

namespace FlightPak.Web.Views.Transactions.PostFlight
{
    public partial class PostflightTripManager : TripManagerBase
    {
        private static ExceptionManager exManager;

        #region  Postflight Common

        public enum TripScreens
        {
            Main = 0,
            Leg = 1,
            Crew = 2,
            Pax = 3,
            Expense = 4
        }

        public static void ExceptionLimitLevelUpdate(int? passedLevel)
        {
            var poLogVM = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            if (poLogVM != null && poLogVM.PostflightMain.POEditMode && poLogVM.PostflightMain.POLogID <= 0 && HttpContext.Current.Session[WebSessionKeys.IsNewLogExceptionShow] != null)
            {
                int level = Convert.ToInt32(HttpContext.Current.Session[WebSessionKeys.IsNewLogExceptionShow].ToString());
                if (level == 1 && passedLevel!=1)
                {
                    HttpContext.Current.Session[WebSessionKeys.IsNewLogExceptionShow] = -1;
                }
                else if (level == 0 && passedLevel==1)
                {
                    HttpContext.Current.Session[WebSessionKeys.IsNewLogExceptionShow] = 1;                    
                }
                else if (level == 1 && passedLevel == 1)
                {
                    HttpContext.Current.Session[WebSessionKeys.IsNewLogExceptionShow] = 1;
                }
            }
            else
            {
                HttpContext.Current.Session[WebSessionKeys.IsNewLogExceptionShow] = -1;
            }
        }


        protected static string GetSubmoduleModule(long SubModuleID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SubModuleID))
            {
                string Retval = string.Empty;
                switch (SubModuleID)
                {
                    case 1: Retval = TripScreens.Main.ToString(); break;
                    case 2: Retval = TripScreens.Leg.ToString(); break;
                    case 3: Retval = TripScreens.Crew.ToString(); break;
                    case 4: Retval = TripScreens.Pax.ToString(); break;
                    case 5: Retval = TripScreens.Expense.ToString(); break;
                }

                return Retval;
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string TripException()
        {
            Hashtable RetObj = new Hashtable();
            RetObj["Flag"] = "0";  // default value as fail
            RetObj["ExceptionList"] = "";

            FPPrincipal UserPrincipal = (FPPrincipal)HttpContext.Current.Session[HttpContext.Current.Session.SessionID];
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((HttpContext.Current.Session[WebSessionKeys.POException] == null))
                    HttpContext.Current.Session[WebSessionKeys.POException] = new List<PostflightTripException>();

                if (HttpContext.Current.Session[WebSessionKeys.POException] != null)
                {
                    List<PostflightTripException> ExceptionList = new List<PostflightTripException>();
                    if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] == null)
                        return JsonConvert.SerializeObject(RetObj);

                    var TripLogVM = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                    int allowedExceptionSubModuleLevel = HttpContext.Current.Session[WebSessionKeys.IsNewLogExceptionShow] == null ? -1 : Convert.ToInt32(HttpContext.Current.Session[WebSessionKeys.IsNewLogExceptionShow].ToString());

                    PostflightMain poMain = new PostflightMain();
                    poMain = Inject_POViewModel_To_PostflightMain(TripLogVM, poMain);

                    bool isHaveAnyValidLeg = false;
                    var poValidLegs = poMain.PostflightLegs.Where(t=>t.IsDeleted==false && t.State!= TripEntityState.Deleted).ToList();
                    foreach (var poLeg in poValidLegs)
                    {
                        if (poLeg.DepartICAOID.HasValue == true && poLeg.ArriveICAOID.HasValue == true)
                        {
                            isHaveAnyValidLeg = true;
                            break;
                        }
                    }

                    if (isHaveAnyValidLeg==false && TripLogVM.PostflightMain.POLogID == 0  && TripLogVM.PostflightMain.POEditMode == true)
                    {
                        poMain.PostflightLegs = null;                        
                    }

                    UserPrincipalViewModel userPrincipleVM = (UserPrincipalViewModel)HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel];
                    if (userPrincipleVM == null)
                    {
                        userPrincipleVM = getUserPrincipal();
                    }
                    using (PostflightServiceClient ValidateService = new PostflightServiceClient())
                    {
                        var validateResult = ValidateService.ValidateBusinessRules(poMain);
                        if(poMain.PostflightLegs==null)
                            poMain.PostflightLegs = new List<PostflightLeg>();

                        if (validateResult.ReturnFlag == true)
                        {
                            HttpContext.Current.Session.Remove(WebSessionKeys.POException);
                            ExceptionList = validateResult.EntityList;
                            if (ExceptionList != null && ExceptionList.Count > 0)
                            {
                                HttpContext.Current.Session[WebSessionKeys.POException] = ExceptionList;
                            }                            
                        }
                        else
                        {                            
                            ExceptionList = new List<PostflightTripException>();
                            HttpContext.Current.Session[WebSessionKeys.POException] = ExceptionList;
                        }
                    }
                    ExceptionList = (List<PostflightTripException>)HttpContext.Current.Session[WebSessionKeys.POException];
                    
                    if (ExceptionList.Count > 0)
                    {
                        List<ExceptionTemplate> ExceptionTemplateList = new List<ExceptionTemplate>();
                        using (PostflightServiceClient Service = new PostflightServiceClient())
                        {
                            var ObjExTemplRetVal = Service.GetPOBusinessErrorsList(10009);
                            if (ObjExTemplRetVal != null && ObjExTemplRetVal.ReturnFlag == true)
                            {
                                ExceptionTemplateList = ObjExTemplRetVal.EntityList;
                                if (allowedExceptionSubModuleLevel == -1)
                                {
                                    allowedExceptionSubModuleLevel = Convert.ToInt32(ExceptionTemplateList.Max(e => e.SubModuleID));
                                }
                                var obj = (from Exp in ExceptionList
                                           join ExpTempl in ExceptionTemplateList on
                                           Exp.ExceptionTemplateID equals ExpTempl.ExceptionTemplateID
                                           where ExpTempl.SubModuleID <= allowedExceptionSubModuleLevel
                                           select new
                                           {
                                               Severity = ExpTempl.Severity,
                                               ExceptionDescription = Exp.ExceptionDescription,
                                               SubModuleID = ExpTempl.SubModuleID,
                                               SubModuleGroup = GetSubmoduleModule((long)ExpTempl.SubModuleID),
                                               DisplayOrder = Exp.DisplayOrder == null ? string.Empty : Exp.DisplayOrder
                                           }).OrderBy(x => x.DisplayOrder).ToList();
                                RetObj["Flag"] = "1";
                                RetObj["ExceptionList"] = obj;
                            }
                        }
                    }
                }
            }
            return JsonConvert.SerializeObject(RetObj);
        }

        public static PostflightLogViewModel LogByTripID(long TripId)
        {
            PostflightLogViewModel pfViewModel = new PostflightLogViewModel();

            PostflightMain logResult = new PostflightMain();
            pfViewModel.PostflightMain = new PostflightMainViewModel(getUserPrincipal()._ApplicationDateFormat);
            using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
            {

                PostflightMain obj = new PostflightMain();
                obj.POLogID = TripId;

                var ObjRetval = objService.GetTrip(obj);
                if (ObjRetval.ReturnFlag == true && ObjRetval.EntityInfo != null && ObjRetval.EntityInfo.POLogID > 0 && ObjRetval.EntityInfo.IsDeleted == false)
                {

                    obj.POLogID = Convert.ToInt64(ObjRetval.EntityInfo.POLogID);
                    var result = ObjRetval.EntityInfo;

                    logResult = (PostflightMain)result;

                    pfViewModel = InjectWholePODbModelToLogViewModel(logResult, pfViewModel);
                    if (pfViewModel.PostflightMain.HomebaseID.HasValue == true)
                    {
                        pfViewModel.PostflightMain.Homebase = DBCatalogsDataLoader.GetHomebaseInfo_FromHomebaseId(pfViewModel.PostflightMain.HomebaseID.Value);
                    }
                    if (pfViewModel.PostflightMain.LastUpdTS.HasValue)
                        pfViewModel.PostflightMain.LastUpdTSStr = pfViewModel.PostflightMain.LastUpdTS.Value.ToString("yyyy-MM-dd HH:mm:ss");

                    HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentPostflightLog] = logResult;
                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                    PostflightLogViewModel PFTest = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                    if (result.PostflightTripExceptions != null && result.PostflightTripExceptions.Count > 0)
                        HttpContext.Current.Session[WebSessionKeys.POException] = result.PostflightTripExceptions;

                }
            }
            return pfViewModel;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightLogViewModel> LogSearch(long LogNum)
        {
            return SearchPostFlightLog(LogNum);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightLogViewModel> SearchPostflightTrip(long TripId)
        {
            return SearchPostFlightLog(TripId, true);
        }

        public static FSSOperationResult<PostflightLogViewModel> SearchPostFlightLog(long LogNumOrTripId, bool searchByTripId = false)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<PostflightLogViewModel>>(() =>
            {
                FSSOperationResult<PostflightLogViewModel> ResultObj = new FSSOperationResult<PostflightLogViewModel>();
                if (ResultObj.IsAuthorized() == false)
                {
                    return ResultObj;
                }
                PostflightMain logResult = new PostflightMain();
                PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
                pfViewModel.PostflightMain = new PostflightMainViewModel(getUserPrincipal()._ApplicationDateFormat);
                using (PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient())
                {

                    PostflightMain obj = new PostflightMain();
                    if (searchByTripId)
                    {
                        obj.POLogID = LogNumOrTripId;
                    }
                    else
                    {
                        obj.POLogID = 0;
                        obj.LogNum = LogNumOrTripId;
                    }

                    var ObjRetval = objService.GetTrip(obj);
                    if (ObjRetval.ReturnFlag == true && ObjRetval.EntityInfo != null && ObjRetval.EntityInfo.POLogID > 0 && ObjRetval.EntityInfo.IsDeleted == false)
                    {

                        obj.POLogID = Convert.ToInt64(ObjRetval.EntityInfo.POLogID);
                        var result = ObjRetval.EntityInfo;

                        logResult = (PostflightMain)result;

                        pfViewModel = InjectWholePODbModelToLogViewModel(logResult, pfViewModel);
                        if (pfViewModel.PostflightMain.HomebaseID.HasValue == true)
                        {
                            pfViewModel.PostflightMain.Homebase = DBCatalogsDataLoader.GetHomebaseInfo_FromHomebaseId(pfViewModel.PostflightMain.HomebaseID.Value);
                        }
                        if (pfViewModel.PostflightMain.LastUpdTS.HasValue)
                            pfViewModel.PostflightMain.LastUpdTSStr = pfViewModel.PostflightMain.LastUpdTS.Value.ToString("yyyy-MM-dd HH:mm:ss");

                        HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentPostflightLog] = logResult;
                        HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                        PostflightLogViewModel PFTest = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                        if (result.PostflightTripExceptions != null && result.PostflightTripExceptions.Count > 0)
                            HttpContext.Current.Session[WebSessionKeys.POException] = result.PostflightTripExceptions;

                        ResultObj.Success = true;
                        ResultObj.StatusCode = HttpStatusCode.OK;
                        ResultObj.Result = pfViewModel;

                    }
                    else
                    {
                        ResultObj.Success = false;
                        ResultObj.StatusCode = HttpStatusCode.OK;
                    }
                }
                return ResultObj;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }
        
        
        public static void ValidatePostflightSession(string selectedTab = "")
        {
            PostflightLogViewModel Log = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            if (Log == null)
            {
                HttpContext.Current.Response.Redirect("PostFlightMain.aspx");
            }
            if (selectedTab != "Leg" && (Log.PostflightLegs == null || Log.PostflightLegs.Count == 0))
            {
                HttpContext.Current.Response.Redirect("PostFlightLegs.aspx?seltab=Legs");
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> SavePostflightLog()
        {
            string Msg = string.Empty;
            FSSOperationResult<object> ResultData= new FSSOperationResult<object>();
            if(ResultData.IsAuthorized()==false){
                return ResultData;
            }

            var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            if (TripLog == null)
            {
                ResultData.StatusCode = HttpStatusCode.OK;
                ResultData.Success = false;
                return ResultData;
            }
            PostflightMain poMainTrip = new PostflightMain();
            UserPrincipalViewModel userPrincipleVM = (UserPrincipalViewModel)HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel];
            if (userPrincipleVM == null)
            {
                userPrincipleVM = getUserPrincipal();
            }
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripLog))
            {
                var exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process<FSSOperationResult<object>>(() =>
                {

                    HttpContext.Current.Session[WebSessionKeys.IsNewLogExceptionShow] = -1;

                    if (TripLog.POLogID > 0)
                    {
                        PostflightMain pfExist = (PostflightMain)HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentPostflightLog];
                        poMainTrip = MiscUtils.Clone<PostflightMain>(pfExist);
                    }

                    if (    (userPrincipleVM._IsSIFLCalMessage != null && userPrincipleVM._IsSIFLCalMessage == true && TripLog.PostflightMain.IsPersonal == true)
                        &&  (HttpContext.Current.Session[WebSessionKeys.IsAutoCalculateSIFL] == null 
                            || 
                                (   HttpContext.Current.Session[WebSessionKeys.IsAutoCalculateSIFL] != null 
                                    && HttpContext.Current.Session[WebSessionKeys.IsAutoCalculateSIFL].ToString() == "0"
                                )
                             )
                        )
                    {
                        HttpContext.Current.Session[WebSessionKeys.IsAutoCalculateSIFL] = "1";
                        ResultData.Result = "RECALCULATESIFL";
                        ResultData.Success = false;
                        ResultData.StatusCode = HttpStatusCode.OK;                        
                        return ResultData;
                        //RadWindowManager1.RadConfirm("Recalculate SIFL?", "RecalcSiflCallBackFn", 330, 100, null, "Confirmation!");
                    }

                    PostflightCrewLite poCrewLite = new PostflightCrewLite();

                    if (TripLog != null && TripLog.PostflightLegs != null) 
                    { 
                        foreach(PostflightLegViewModel legVM in TripLog.PostflightLegs.OrderBy(l => l.LegNUM))
                        {
                            if (legVM.PostflightLegCrews != null)
                            {
                                foreach (PostflightLegCrewViewModel crew in legVM.PostflightLegCrews.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted).ToList())
                                {
                                    // Calculate Days Away
                                    crew.DaysAway = poCrewLite.DaysAwayCalculation(TripLog, Convert.ToInt64(crew.CrewID), Convert.ToInt64(legVM.LegNUM));
                                }
                            }
                        }
                        PostFlightValidator.ReCalculateCrewDutyHour();
                    }

                    PostflightMainLite pomLite = new PostflightMainLite();
                    poMainTrip = Inject_POViewModel_To_PostflightMain(TripLog, poMainTrip);
                    
                    var isMandatoryException = ValidateTripLogForMandatoryException(poMainTrip);
                    if (isMandatoryException == false)
                    {
                    
                         string errMsg = string.Empty;

                         using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(poMainTrip))
                         {
                             // Set for Retaining Main Trip Object into Session, if it fails to save.
                             if (poMainTrip.POLogID > 0)
                             {
                                 using (PostflightServiceClient ServiceObj = new PostflightServiceClient())
                                 {
                                     var result = ServiceObj.GetTrip(poMainTrip);
                                     HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentPostflightLog] = (PostflightMain)result.EntityInfo;
                                 }
                             }
                             else
                             {
                                 HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentPostflightLog] = poMainTrip;
                             }

                             //Set Legs Min Date as Log Est Depart date
                             DateTime EstDeptDate = DateTime.MinValue;

                             if (poMainTrip.PostflightLegs != null && poMainTrip.PostflightLegs.Where(x => x.IsDeleted == false && x.ScheduledTM != null).Count() > 0)
                             {
                                 EstDeptDate = poMainTrip.PostflightLegs.Where(x => x.IsDeleted == false && x.ScheduledTM != null).Min(x => x.ScheduledTM).Value;
                                 poMainTrip.EstDepartureDT = EstDeptDate;
                             }

                             List<PostflightLeg> legList = poMainTrip.PostflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                             foreach (PostflightLeg leg in legList)
                             {
                                 // Set Next Date Time Field
                                 if (legList.Count > 1)
                                 {
                                     PostflightLeg nextLeg = legList.Where(x => x.LegNUM == leg.LegNUM + 1).FirstOrDefault();
                                     if (nextLeg != null)
                                         leg.NextDTTM = nextLeg.ScheduledTM;
                                 }

                                 //This is commented out for reusing the days away calculation from PostflightCrewLite
                                 //if (leg.PostflightCrews != null)
                                 //{
                                 //    foreach (PostflightCrew crew in leg.PostflightCrews.Where(x => x.IsDeleted == false).ToList())
                                 //    {
                                 //        // Calculate Days Away
                                 //        crew.DaysAway =pomLite.DaysAwayCalculation(poMainTrip, Convert.ToInt64(crew.CrewID), Convert.ToInt64(leg.LegNUM),userPrincipleVM);
                                 //    }
                                 //}

                                 // Set Passenger Total in PostflightLeg Table
                                 if (leg.PostflightPassengers != null)
                                 {
                                     if (leg.PostflightPassengers.Count > 0)
                                         leg.PassengerTotal = leg.PostflightPassengers.Where(x => x.FlightPurposeID > 1 && x.State!= TripEntityState.Deleted && x.IsDeleted == false).Count();
                                     else
                                         leg.PassengerTotal = 0;
                                 }
                             }


                             // Call clear all foreign key reference
                             poMainTrip = PostflightMainLite.CommonClearFKObjectsBeforeSave(poMainTrip);

                             using (PostflightServiceClient Service = new PostflightServiceClient())
                             {

                                 var ReturnValue = Service.Update(poMainTrip);
                                 if (ReturnValue != null && ReturnValue.ReturnFlag == true)
                                 {
                                     //ClearPostflightSessions();

                                     if (ReturnValue.EntityInfo.POLogID > 0)
                                     {
                                         PostflightMain tripObj = new PostflightMain();
                                         tripObj.POLogID = ReturnValue.EntityInfo.POLogID;
                                         var result = Service.GetTrip(tripObj);
                                         var logResult = (PostflightMain)result.EntityInfo;
                                         HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentPostflightLog] = logResult;
                                         PostflightLogViewModel pfViewModel = InjectWholePODbModelToLogViewModel(logResult);

                                         HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentPostflightLog] = logResult;
                                         HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                                         if (pfViewModel.PostflightMain.LastUpdTS.HasValue)
                                            pfViewModel.PostflightMain.LastUpdTSStr =  pfViewModel.PostflightMain.LastUpdTS.Value.ToString("yyyy-MM-dd HH:mm:ss");
                                         

                                         // Bind the Saved Exceptions into the Session
                                         if (result.EntityInfo.PostflightTripExceptions != null && result.EntityInfo.PostflightTripExceptions.Count > 0)
                                             HttpContext.Current.Session[WebSessionKeys.POException] = result.EntityInfo.PostflightTripExceptions;
                                         else
                                             HttpContext.Current.Session.Remove(WebSessionKeys.POException);

                                         HttpContext.Current.Session[WebSessionKeys.IsAutoCalculateSIFL] = "0";
                                     }
                                 }
                                 else
                                 {
                                     //Session[POSessionKey] = Session[POMainRetain];
                                     ResultData.Success = false;
                                     //Msg = HttpUtility.HtmlEncode("Log has mandatory exceptions, Log is not Saved.");
                                     errMsg = ReturnValue.ErrorMessage;
                                     //Replace the exception with a custom error Message before moving to TEST
                                     ResultData.ErrorsList.Add(errMsg);
                                 }
                             }
                         }
            

                    }
                    else
                    {
                        ResultData.Success = false;
                        Msg = HttpUtility.HtmlEncode("Log has mandatory exceptions, Log is not Saved.");
                        ResultData.ErrorsList.Add(Msg);

                    }


                    return ResultData;
                },FlightPak.Common.Constants.Policy.UILayer);
            }
            return ResultData;
        }

        private static bool ValidateTripLogForMandatoryException(PostflightMain poTripLog)
        {
            bool isMandatoryException = true;
            using (PostflightServiceClient ValidateService = new PostflightServiceClient())
            {
                UserPrincipalViewModel userPrincipleVM = (UserPrincipalViewModel)HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel];
                if (userPrincipleVM == null)
                {
                    userPrincipleVM = getUserPrincipal();
                }
                List<PostflightTripException> ExceptionList = new List<PostflightTripException>();
                var validateResult = ValidateService.ValidateBusinessRules(poTripLog);

                if (validateResult.ReturnFlag == true)
                {
                    HttpContext.Current.Session.Remove(WebSessionKeys.POException);

                    List<PostflightTripException> OldExceptionList = new List<PostflightTripException>();
                    //OldExceptionList = (List<PostflightTripException>)Session["PostflightTripException"];  -- require
                    if (OldExceptionList != null && OldExceptionList.Count > 0)
                    {

                        foreach (PostflightTripException OldMandatoryException in OldExceptionList.ToList())
                        {
                            if (OldMandatoryException.POTripExceptionID == 0)
                                OldExceptionList.Remove(OldMandatoryException);
                        }
                    }
                    ExceptionList = validateResult.EntityList;

                    if (ExceptionList != null && ExceptionList.Count > 0)
                    {
                        if (OldExceptionList == null)
                            OldExceptionList = new List<PostflightTripException>();

                        if (userPrincipleVM._IsLogsheetWarning)
                            OldExceptionList.AddRange(ExceptionList);
                    }
                    HttpContext.Current.Session["PostflightTripException"] = OldExceptionList;

                    HttpContext.Current.Session[WebSessionKeys.POException] = ExceptionList;

                    // Check for Critical Error
                    List<ExceptionTemplate> ExceptionTemplateList = new List<ExceptionTemplate>();

                    var ObjExTemplRetVal = ValidateService.GetPOBusinessErrorsList(10009);
                    if (ObjExTemplRetVal.ReturnFlag)
                    {
                        ExceptionTemplateList = ObjExTemplRetVal.EntityList;
                    }

                    var groupedErrorList = (from Exp in ExceptionList
                                            join ExpTempl in ExceptionTemplateList on Exp.ExceptionTemplateID equals ExpTempl.ExceptionTemplateID
                                            select new
                                            {
                                                Severity = ExpTempl.Severity,
                                                ExceptionDescription = Exp.ExceptionDescription,
                                                SubModuleID = ExpTempl.SubModuleID,
                                                SubModuleGroup = GetSubmoduleModule((long)ExpTempl.SubModuleID)
                                            });



                    // Check if there is No Critical Exception
                    if (groupedErrorList.Where(x => x.Severity == 1).ToList().Count == 0)
                        isMandatoryException= false;
                    else
                        isMandatoryException= true;
                }
                else
                {
                    isMandatoryException = false;
                }
            }
            return isMandatoryException;
        }

        internal static PostflightMain Inject_POViewModel_To_PostflightMain(PostflightLogViewModel poLogVM,PostflightMain POMain)
        {            
            POMain = (PostflightMain)POMain.InjectFrom(poLogVM.PostflightMain);

            POMain.DeletedCrewIDs = new Dictionary<long,long>();
            POMain.DeletedExceptionIDs = new Dictionary<long,long>();
            POMain.DeletedExpenseIDs = new Dictionary<long,long>();
            POMain.DeletedLegIDs = new List<long>();
            POMain.DeletedPAXIDs = new Dictionary<long,long>();
            POMain.DeletedSIFLIDs = new Dictionary<long,long>();
            POMain.RecordLockInfo = new object();

            POMain.DeletedSIFLIDs = poLogVM.PostflightMain.DeletedSIFLIDs;

            POMain.State = POMain.POLogID > 0 ? TripEntityState.Modified : TripEntityState.Added;
            
            if (POMain.PostflightLegs == null)
                POMain.PostflightLegs = new List<PostflightLeg>();

            foreach (var leg in poLogVM.PostflightLegs)
            {
                PostflightLeg POLeg= new PostflightLeg();
                if (leg.POLegID > 0)
                {
                    POLeg = POMain.PostflightLegs.Where(p => p.POLegID == leg.POLegID).FirstOrDefault() ?? new PostflightLeg();
                    if (POMain.PostflightLegs.Exists(p => p.POLegID == leg.POLegID) == false)
                        POMain.PostflightLegs.Add(POLeg);
                }
                else
                    POMain.PostflightLegs.Add(POLeg);

                POLeg = (PostflightLeg)POLeg.InjectFrom(leg);

                if (leg.State != TripEntityState.NoChange)
                {
                    if (POLeg.State != TripEntityState.Deleted && POLeg.IsDeleted == false)
                    {
                        POLeg.State = leg.POLegID > 0 ? TripEntityState.Modified : TripEntityState.Added;
                    }
                    else
                    {
                        POLeg.State = TripEntityState.Deleted;
                        POLeg.IsDeleted = true;
                        POMain.DeletedLegIDs.Add(POLeg.POLegID);
                    }
                }
                // Passengers

                if (POLeg.PostflightPassengers == null)
                    POLeg.PostflightPassengers = new List<PostflightPassenger>();
                foreach (var paxVM in leg.PostflightLegPassengers)
                {
                    PostflightPassenger POPax = new PostflightPassenger();
                    if (paxVM.PostflightPassengerListID > 0)
                    {
                        POPax = POLeg.PostflightPassengers.Where(p => p.PostflightPassengerListID == paxVM.PostflightPassengerListID).FirstOrDefault() ?? new PostflightPassenger();
                    }

                    POPax= (PostflightPassenger)POPax.InjectFrom(paxVM);
                    POPax.PassengerID = paxVM.PassengerRequestorID;
                    if (paxVM.State != TripEntityState.NoChange)
                    {
                        if (paxVM.PostflightPassengerListID > 0)
                            POPax.State = (paxVM.State == TripEntityState.Deleted) ? TripEntityState.Deleted : TripEntityState.Modified;
                        else
                            POPax.State = TripEntityState.Added;
                        if (POLeg.State != TripEntityState.Deleted && POLeg.State != TripEntityState.Added)
                            POLeg.State = TripEntityState.Modified;
                    }
                    if (POLeg.State == TripEntityState.Deleted)
                        POPax.State = TripEntityState.Deleted;

                    if (POPax.State == TripEntityState.Deleted)
                    {
                        POPax.IsDeleted = true;
                        POMain.DeletedPAXIDs[POPax.PostflightPassengerListID] = POLeg.POLegID;
                    }
                    if(POPax.State== TripEntityState.Added)
                        POLeg.PostflightPassengers.Add(POPax);
                }

                // Crew

                if (POLeg.PostflightCrews == null)
                    POLeg.PostflightCrews = new List<PostflightCrew>();
                foreach (var crewVM in leg.PostflightLegCrews)
                {
                    PostflightCrew POCrew = new PostflightCrew();
                    if(crewVM.PostflightCrewListID > 0)
                    {
                        POCrew = POLeg.PostflightCrews.Where(c => c.PostflightCrewListID == crewVM.PostflightCrewListID).FirstOrDefault()?? new PostflightCrew();
                    }

                    POCrew = (PostflightCrew)POCrew.InjectFrom(crewVM);
                    if (crewVM.State != TripEntityState.NoChange)
                    {
                        if (crewVM.PostflightCrewListID > 0)
                            POCrew.State = (crewVM.State == TripEntityState.Deleted) ? TripEntityState.Deleted : TripEntityState.Modified;
                        else
                            POCrew.State = TripEntityState.Added;
                        if (POLeg.State != TripEntityState.Deleted && POLeg.State != TripEntityState.Added)
                            POLeg.State = TripEntityState.Modified;
                    }
                    
                    if (POLeg.State == TripEntityState.Deleted)
                        POCrew.State = TripEntityState.Deleted;

                    if (POCrew.State == TripEntityState.Deleted)
                    {
                        POCrew.IsDeleted = true;
                        POMain.DeletedCrewIDs[POCrew.PostflightCrewListID] = POLeg.POLegID;
                    }

                    POLeg.PostflightCrews.Add(POCrew);
                }

                // Expenses

                if (POLeg.PostflightExpenses == null)
                    POLeg.PostflightExpenses = new List<PostflightExpense>();
                foreach (var expenseVM in leg.PostflightLegExpensesList)
                {
                    PostflightExpense POExpense = null;
                    if (expenseVM.PostflightExpenseID > 0)
                    {
                        POExpense = POLeg.PostflightExpenses.Where(ex => ex.PostflightExpenseID == expenseVM.PostflightExpenseID).FirstOrDefault() ?? new PostflightExpense();
                    }
                    if (POExpense == null)
                        POExpense = new PostflightExpense();
                    POExpense = (PostflightExpense)POExpense.InjectFrom(expenseVM);
                    if (expenseVM.PostflightNotes != null && expenseVM.PostflightNotes.Count > 0)
                    {
                        PostflightNoteViewModel pnoteVM = expenseVM.PostflightNotes.FirstOrDefault();
                        if (POExpense.PostflightNotes == null || POExpense.PostflightNotes.Count() <= 0)
                        {
                            pnoteVM.State = TripEntityState.Added;
                            POExpense.PostflightNotes = new List<PostflightNote>();
                            POExpense.PostflightNotes.Add((PostflightNote)new PostflightNote().InjectFrom(pnoteVM));
                        }
                        else
                        {
                            pnoteVM.State = TripEntityState.Modified;
                            PostflightNote pnote = POExpense.PostflightNotes.FirstOrDefault();
                            pnote = (PostflightNote)pnote.InjectFrom(pnoteVM);
                        }
                    }
                    if (expenseVM.State != TripEntityState.NoChange)
                    {
                        if (expenseVM.PostflightExpenseID > 0)
                            POExpense.State = (expenseVM.State == TripEntityState.Deleted) ? TripEntityState.Deleted : TripEntityState.Modified;
                        else
                            POExpense.State = TripEntityState.Added;
                        if (POLeg.State != TripEntityState.Deleted && POLeg.State != TripEntityState.Added)
                            POLeg.State = TripEntityState.Modified;
                    }

                    if (POLeg.State == TripEntityState.Deleted)
                        POExpense.State = TripEntityState.Deleted;

                    if (POExpense.State == TripEntityState.Deleted)
                    {
                        POExpense.IsDeleted = true;
                        POMain.DeletedExpenseIDs[POExpense.PostflightExpenseID] = POLeg.POLegID;
                    }
                    if (POExpense.State == TripEntityState.Added)
                        POLeg.PostflightExpenses.Add(POExpense);
                }

                // Sifl list
                if (POLeg.PostflightSIFLs == null)
                    POLeg.PostflightSIFLs = new List<PostflightSIFL>();
                
                foreach (PostflightLegSIFLViewModel siflVM in poLogVM.PostflightLegSIFLsList)
                {
                    PostflightSIFL sifl = new PostflightSIFL();
                    if (siflVM.POSIFLID > 0)
                    {
                        sifl = POLeg.PostflightSIFLs.Where(s => s.POSIFLID == siflVM.POSIFLID).FirstOrDefault() ?? new PostflightSIFL();
                    }
                    sifl = (PostflightSIFL)sifl.InjectFrom(siflVM);
                    if (siflVM.State != TripEntityState.NoChange)
                    {
                        if (siflVM.POSIFLID > 0)
                        {
                            sifl.State = (siflVM.State == TripEntityState.Deleted) ? TripEntityState.Deleted : TripEntityState.Modified;
                            if (POLeg.State == TripEntityState.NoChange)
                            {
                                POLeg.State = TripEntityState.Modified;
                            }
                        }
                        else
                            sifl.State = TripEntityState.Added;
                    }
                    if (POLeg.State == TripEntityState.Deleted)
                        sifl.State = TripEntityState.Deleted;


                    if (POLeg.LegNUM == 1)
                    {
                        sifl.POLegID = POLeg.POLegID;
                        if (sifl.State == TripEntityState.Added && sifl.PassengerRequestorID != null && sifl.PassengerRequestorID > 0 && sifl.ArriveICAOID.HasValue && sifl.ArriveICAOID > 0 && sifl.DepartICAOID.HasValue && sifl.DepartICAOID > 0)
                            POLeg.PostflightSIFLs.Add(sifl);
                    }
                    if (POMain.DeletedSIFLIDs == null)
                        POMain.DeletedSIFLIDs = new Dictionary<long, long>();

                    if(sifl.State == TripEntityState.Deleted)
                    {
                        sifl.IsDeleted = true;
                        POMain.DeletedSIFLIDs[sifl.POSIFLID] = POLeg.POLegID;
                    }
                }
            }

            return POMain;
        }

        internal static PostflightLogViewModel InjectWholePODbModelToLogViewModel(PostflightMain logResult, PostflightLogViewModel pfViewModel = null)
        {
            if(pfViewModel==null)
                pfViewModel = new PostflightLogViewModel();

            pfViewModel = InjectPostflightMainToPostflightLogViewModel(logResult, pfViewModel);
            pfViewModel = SetPostflightLegs(logResult, pfViewModel);
            pfViewModel = SetPostflightLegExpenses(logResult, pfViewModel);
            pfViewModel = SetPostflightSiflList(logResult, pfViewModel);

            return pfViewModel;

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> lnkReportPreview_Click(string POLogID)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<bool>>(() =>
               {
                   FSSOperationResult<bool> ResultObj = new FSSOperationResult<bool>();
                   if (ResultObj.IsAuthorized() == false)
                   {
                       return ResultObj;
                   }
                   UserPrincipalViewModel userPrincipal = TripManagerBase.getUserPrincipal();
                   HttpContext.Current.Session["TabSelect"] = "PostFlight";
                   HttpContext.Current.Session["REPORT"] = "RptFlightLog";
                   HttpContext.Current.Session["FORMAT"] = "PDF";
                   HttpContext.Current.Session["PARAMETER"] = string.Format("UserCD={0};LogID={1}", userPrincipal._name, POLogID);
                   ResultObj.Success = true;
                   ResultObj.StatusCode = HttpStatusCode.OK;
                   return ResultObj;
               }, FlightPak.Common.Constants.Policy.UILayer);
        }

        #endregion

        #region PostflightMain

        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static FSSOperationResult<PostflightLogViewModel> NewLogButtonClick()
        {
            FSSOperationResult<PostflightLogViewModel> result = new FSSOperationResult<PostflightLogViewModel>();
            if (!result.IsAuthorized())
            {
                return result;
            }
            try
            {
                if (HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel] != null)
                {
                    PostflightMainLite pmLite = new PostflightMainLite();

                    PostflightLogViewModel pfViewModel = new PostflightLogViewModel();                    
                    pfViewModel.PostflightMain = CreateOrCancelTrip(pfViewModel.PostflightMain);
                    if (pfViewModel.PostflightMain.HomebaseID.HasValue)
                        pfViewModel.PostflightMain.Homebase = DBCatalogsDataLoader.GetHomebaseInfo_FromHomebaseId(pfViewModel.PostflightMain.HomebaseID??0);

                    pfViewModel = pmLite.LoadFleetAndAircraftBasedOnUsersHomebase(pfViewModel);
                    pfViewModel = pmLite.LoadClientInfo(pfViewModel);
                    pfViewModel.PostflightMain.POEditMode = true;
                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                    pfViewModel.PostflightMain.State = TripEntityState.Added;
                    pfViewModel.PostflightMain.TripActionStatus = TripAction.NewRecord;                                        
                    result.Result = pfViewModel;
                    result.StatusCode = HttpStatusCode.OK;
                    result.Success = true;
                    HttpContext.Current.Session[WebSessionKeys.IsNewLogExceptionShow] = 0;
                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                }
                else
                {
                    result.Result = null;
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception ex)
            {
                result.Result = null;
                result.Success = false;
                result.StatusCode = HttpStatusCode.NoContent;
            }
            return result;
        }

        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static FSSOperationResult<PostflightLogViewModel> CancelAPostflightLog()
        {            
            FSSOperationResult<PostflightLogViewModel> ResultObject = new FSSOperationResult<PostflightLogViewModel>();
            if (!ResultObject.IsAuthorized())
            {
                return ResultObject;
            }

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    PostflightLogViewModel TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                    if (TripLog.POLogID > 0)
                    {
                        using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                        {
                            var returnValue = CommonService.UnLock(WebSessionKeys.PostflightMainEntityName, TripLog.PostflightMain.POLogID);
                            if (returnValue.ReturnFlag)
                            {
                                TripLog = new PostflightLogViewModel();
                                TripLog.PostflightMain.POEditMode = false;
                                PostflightMain p = (PostflightMain)HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentPostflightLog];
                                TripLog = InjectWholePODbModelToLogViewModel(p, TripLog);
                                HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;
                                TripLog.PostflightMain.TripActionStatus = TripAction.None;
                                ResultObject.Result = TripLog;
                                ResultObject.Success = true;
                                ResultObject.StatusCode = HttpStatusCode.OK;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(returnValue.ErrorMessage))
                                {
                                    ResultObject.ErrorsList.Add(returnValue.ErrorMessage);
                                    ResultObject.Result = TripLog;
                                    ResultObject.StatusCode = HttpStatusCode.OK;
                                    ResultObject.Success = false;
                                }
                                else
                                {
                                    ResultObject.StatusCode = HttpStatusCode.OK;
                                    ResultObject.Result = TripLog;
                                    ResultObject.Success = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        TripLog.POLogID = 0;
                        TripLog.PostflightMain = CreateOrCancelTrip(TripLog.PostflightMain);
                        TripLog.PostflightMain.POEditMode = false;
                        HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = null;                         
                        // Set the Default Selection as Main Tab
                        //Response.Redirect("PostFlightMain.aspx?seltab=main", false);
                        ResultObject.StatusCode = HttpStatusCode.OK;
                        ResultObject.Result = TripLog;
                        ResultObject.Success = false;
                    }
                }

            }
            return ResultObject;
        }

        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat= ResponseFormat.Json)]
        public static FSSOperationResult<bool> DeletePostflightLog()
        {
            FSSOperationResult<bool> ResultObj= new FSSOperationResult<bool>();
            if (!ResultObj.IsAuthorized())
            {
                return ResultObj;
            }

            PostflightLogViewModel TripLog = new PostflightLogViewModel();
            PostflightMain PostflightLog = (PostflightMain)HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentPostflightLog];
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
            {
                TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            }
            if (TripLog.POLogID > 0 && PostflightLog != null && PostflightLog.POLogID == TripLog.PostflightMain.POLogID && PostflightLog.POLogID == TripLog.POLogID)
            {
                using (PostflightServiceClient Service = new PostflightServiceClient())
                {
                    var ReturnValue = Service.DeleteTrip(TripLog.POLogID);
                    if (ReturnValue.ReturnFlag == true)
                    {
                        HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = null;
                        HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentPostflightLog] = null;                        
                        // Set the Default Selection as Main Tab
                        ResultObj.StatusCode = HttpStatusCode.OK;
                        ResultObj.Success = true;
                        FlightPak.Web.Views.Transactions.Preflight.PreflightTripManager.Update_DeletedLogInfo(PostflightLog);
                    }
                    else
                    {
                        ResultObj.StatusCode = HttpStatusCode.OK;
                        ResultObj.Success = false;
                    }
                }
            }

            return ResultObj;
        }

        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static FSSOperationResult<PostflightLogViewModel> EditPostflightLog()
        {
            FSSOperationResult<PostflightLogViewModel> ResultObj = new FSSOperationResult<PostflightLogViewModel>();
            if (!ResultObj.IsAuthorized())
            {
                return ResultObj;
            }

            if (HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel] != null)
            {
                PostflightLogViewModel pfViewModel = new PostflightLogViewModel();                
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue = CommonService.Lock(WebSessionKeys.PostflightMainEntityName, pfViewModel.POLogID);
                        if (returnValue.ReturnFlag)
                        {
                            pfViewModel.PostflightMain.TripActionStatus = TripAction.RecordNowLockedForEdit;
                            pfViewModel.PostflightMain.POEditMode = true;                            
                            HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                            ResultObj.Result = pfViewModel;
                            ResultObj.StatusCode = HttpStatusCode.OK;
                            ResultObj.Success = true;
                        }
                        else
                        {
                            ResultObj.ErrorsList.Add(returnValue.LockMessage);
                            ResultObj.StatusCode = HttpStatusCode.OK;
                            ResultObj.Success = false;
                        }
                    }
                }
            }
            else
            {                
                ResultObj.Result = null;
                ResultObj.Success = false;
                ResultObj.StatusCode = HttpStatusCode.Unauthorized;
            }
            return ResultObj;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Dictionary<string, object>> GetACurrentTripLog()
        {
            FSSOperationResult<Dictionary<string, object>> fssPostflightCurrentTripLog = new FSSOperationResult<Dictionary<string, object>>();
            if (fssPostflightCurrentTripLog.IsAuthorized() == false)
            {
                return fssPostflightCurrentTripLog;
            }
            try
            {
                PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
                var userSettings = getUserPrincipal();
                pfViewModel.PostflightMain = new PostflightMainViewModel(userSettings._ApplicationDateFormat);

                if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                    if (pfViewModel.PostflightMain.Airport != null && pfViewModel.PostflightMain.Airport.IcaoID!=null)
                    {
                        if (pfViewModel.PostflightMain.Homebase == null || (pfViewModel.PostflightMain.Homebase!=null && pfViewModel.PostflightMain.Homebase.HomebaseID==0))
                        {
                            pfViewModel.PostflightMain.Homebase = new HomebaseViewModel();
                            if (string.IsNullOrWhiteSpace(pfViewModel.PostflightMain.Homebase.HomebaseCD) && pfViewModel.POLogID > 0 && pfViewModel.PostflightMain.HomebaseID.HasValue && pfViewModel.PostflightMain.HomebaseID.Value > 0)
                                pfViewModel.PostflightMain.Homebase = DBCatalogsDataLoader.GetHomebaseInfo_FromHomebaseId(pfViewModel.PostflightMain.HomebaseID.Value);
                        }                        
                        pfViewModel.PostflightMain._airportId = Convert.ToInt64(pfViewModel.PostflightMain.Homebase.HomebaseAirportID);                        
                    }                   

                }
                else
                {
                    pfViewModel.PostflightMain = CreateOrCancelTrip(pfViewModel.PostflightMain);
                    if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] == null)
                    {
                        HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                    }
                    PostflightMainLite pmLite = new PostflightMainLite();
                    pfViewModel.PostflightMain.Airport = DBCatalogsDataLoader.GetAirportInfo_FromAirportId(pfViewModel.PostflightMain._airportId ?? 0) ?? new AirportViewModel();
                    
                }
                Dictionary<string, object> resultData = new Dictionary<string, object>();
                resultData["Principal"] = userSettings;
                resultData["PostflightMain"] = pfViewModel;
                HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                fssPostflightCurrentTripLog.Result = resultData;
                fssPostflightCurrentTripLog.Success = true;
                fssPostflightCurrentTripLog.StatusCode = HttpStatusCode.OK;

            }
            catch (Exception ex)
            {
                if (fssPostflightCurrentTripLog.ErrorsList == null)
                    fssPostflightCurrentTripLog.ErrorsList = new List<string>();

                fssPostflightCurrentTripLog.ErrorsList.Add(ex.Message);
                fssPostflightCurrentTripLog.Success = false;
                fssPostflightCurrentTripLog.StatusCode = HttpStatusCode.NoContent;
            }

            return fssPostflightCurrentTripLog;
        }

        public static PostflightMainViewModel CreateOrCancelTrip(PostflightMainViewModel PostflightMain)
        {
            PostflightMain.LogNum = null;
            PostflightMain.Client = new ClientViewModel();
            PostflightMain.Fleet = new FleetViewModel();
            PostflightMain.Aircraft = new AircraftViewModel();
            PostflightMain.Account = new AccountViewModel();
            PostflightMain.Company = new CompanyViewModel();
            PostflightMain.Customer = new CQCustomerViewModel();
            PostflightMain.Department = new DepartmentViewModel();
            PostflightMain.DepartmentAuthorization = new DepartmentAuthorizationViewModel();
            PostflightMain.Passenger = new PostflightLegPassengerViewModel();
            PostflightMain.TripLastUpdatedOn = new DateTime(DateTime.Now.Ticks, DateTimeKind.Local);
            PostflightMain.DispatchNum = null;

            var userSettings = getUserPrincipal();
            PostflightMain.Homebase = new HomebaseViewModel();            
            PostflightMain._airportId = Convert.ToInt64(userSettings._airportId);
            PostflightMain.HomebaseID = Convert.ToInt64(userSettings._homeBaseId);
            PostflightMain.Homebase.HomebaseAirportID = PostflightMain._airportId;

            
            PostflightMain.Company.HomebaseID = (long)userSettings._homeBaseId;
            PostflightMain.Company.BaseDescription = userSettings._BaseDescription;
            PostflightMain.Company.HomebaseAirportID = userSettings._airportId;

            
            return PostflightMain;
        }

        #region Copy To All Legs functionality
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> CopyValueToAllLegs(String property, String propertyValue)
        {
            FSSOperationResult<bool> copyValueResult = new FSSOperationResult<bool>();
            if(copyValueResult.IsAuthorized() == false)
            {
                copyValueResult.Result = false;
                copyValueResult.StatusCode = HttpStatusCode.Unauthorized;
                copyValueResult.Success = false;
            }
            copyValueResult.StatusCode = HttpStatusCode.OK;
            Dictionary<String, String> copyProperties = new Dictionary<string, string>();
            if (HttpContext.Current.Session["POCopyProperties"] != null)
                copyProperties = (Dictionary<String, String>)HttpContext.Current.Session["POCopyProperties"];
            if (propertyValue == null)
                propertyValue = String.Empty;
            copyProperties[property] = propertyValue;
            HttpContext.Current.Session["POCopyProperties"] = copyProperties;
            PostflightLogViewModel pfViewModel = null;
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
            {
                pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                List<PostflightLegViewModel> legs = pfViewModel.PostflightLegs.Where(l => l.IsDeleted == false && l.State != TripEntityState.Deleted).ToList();
                for (int i = 0; i < legs.Count; i++)
                {
                    legs[i] = ApplyCopiedValuesToLeg(legs[i], property);
                }
            }
            copyValueResult.Result = true;
            copyValueResult.Success = true;
            return copyValueResult;
        }
        #endregion

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> SavePostflightMainLogNotes(String logNotes)
        {
            FSSOperationResult<bool> retObj = new FSSOperationResult<bool>();

            PostflightLogViewModel pfViewModel = new PostflightLogViewModel();

            if (!retObj.IsAuthorized())
            {
                return retObj;
            }
            retObj.StatusCode = HttpStatusCode.OK;
            retObj.Success = true;
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
            {
                pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                if(pfViewModel != null && pfViewModel.PostflightMain != null)
                {
                    pfViewModel.PostflightMain.Notes = String.IsNullOrEmpty(logNotes) ? "": logNotes.Trim();
                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                }
            }

            retObj.Result = true;
            return retObj;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> SavePostflightMainViewModel(PostflightMainViewModel postflightMainVM)
        {
            FSSOperationResult<bool> ResultData = new FSSOperationResult<bool>();
            if (ResultData.IsAuthorized() == false)
            {
                return ResultData;
            }
            var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            if (TripLog != null)
            {
                DateTime? objLastDatetimeUpdate = TripLog.PostflightMain.LastUpdTS;
                TripLog.PostflightMain = postflightMainVM;
                TripLog.PostflightMain.LastUpdTS = objLastDatetimeUpdate;
                ResultData.Result = true;
                ResultData.Success = true;
                ResultData.StatusCode = HttpStatusCode.OK;
            }
            return ResultData;
        }

        #endregion

        #region Pax

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightLegSIFLViewModel> Validate_PaxCode_and_SetProfileSettings(string LegNum, string paxCode, PostflightLegSIFLViewModel SiflViewModel)
        {
            FSSOperationResult<PostflightLegSIFLViewModel> ResultData = new FSSOperationResult<PostflightLegSIFLViewModel>();
            if (ResultData.IsAuthorized() == false)
            {
                return ResultData;
            }

            PostflightLegSIFLViewModel PassengerVM = SiflViewModel;
            var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            if (TripLog != null && SiflViewModel!=null)
            {
                var sifl = TripLog.PostflightLegSIFLsList.Where(f => f.IsDeleted == false && SiflViewModel.RowNumber == f.RowNumber).FirstOrDefault();
                if (sifl != null)
                    sifl = SiflViewModel;
            }
            var exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paxCode))
                {

                    PassengerVM.lbPaxName = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    PassengerVM.alertlbcvPaxName = System.Web.HttpUtility.HtmlEncode(string.Empty);                    
                    PassengerVM.AssocPaxCD = string.Empty;
                    PassengerVM.AssocPaxEnabled = false;
                    PassengerVM.AssociatedPassengerID =null;
                    PassengerVM.AssocPassenger = new PostflightLegPassengerViewModel();
                    PassengerVM.alertlbcvAssoc = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    PassengerVM.EmployeeTYPE = WebConstants.POEmployeeType.G;  //default Non-control
                    

                    if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null && !string.IsNullOrEmpty(paxCode))
                    {
                        var PostflightLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                        long lnLegNumber = 0;
                        long.TryParse(LegNum, out lnLegNumber);
                        var postflightLeg = PostflightLog.PostflightLegs.Where(l => l.LegNUM == lnLegNumber && l.State != TripEntityState.Deleted && l.IsDeleted == false).FirstOrDefault();
                        if (postflightLeg != null)
                        {
                            List<PostflightLegPassengerViewModel> PaxLegs = postflightLeg.PostflightLegPassengers;
                            var retVal = PaxLegs.Where(x => x.PassengerRequestorCD != null && x.PassengerRequestorCD.Trim().ToUpper().Equals(paxCode.Trim().ToUpper())).ToList();

                            PassengerVM.PassengerRequestorCD = paxCode;

                            if (retVal != null && retVal.Count > 0)
                            {
                                if (retVal[0].PassengerRequestorID != null)
                                {
                                    PassengerVM.PassengerRequestorID = retVal[0].PassengerRequestorID;
                                    PassengerVM.Passenger1 = (PostflightLegPassengerViewModel)new PostflightLegPassengerViewModel().InjectFrom(retVal[0]);
                                }

                                if (retVal[0].PassengerFirstName != null && retVal[0].PassengerLastName != null && retVal[0].PassengerMiddleName != null)
                                {
                                    PassengerVM.lbPaxName = System.Web.HttpUtility.HtmlEncode(retVal[0].PassengerLastName + ", " + retVal[0].PassengerFirstName + " " + retVal[0].PassengerMiddleName);
                                }
                                else if (retVal[0].PassengerFirstName != null && retVal[0].PassengerLastName != null && retVal[0].PassengerMiddleName == null)
                                {
                                    PassengerVM.lbPaxName = System.Web.HttpUtility.HtmlEncode(retVal[0].PassengerLastName + ", " + retVal[0].PassengerFirstName);
                                }
                                else if (retVal[0].PassengerFirstName != null && retVal[0].PassengerLastName == null && retVal[0].PassengerMiddleName != null)
                                {
                                    PassengerVM.lbPaxName = System.Web.HttpUtility.HtmlEncode(retVal[0].PassengerFirstName + " " + retVal[0].PassengerMiddleName);
                                }
                                else if (retVal[0].PassengerFirstName == null && retVal[0].PassengerLastName != null && retVal[0].PassengerMiddleName != null)
                                {
                                    PassengerVM.lbPaxName = System.Web.HttpUtility.HtmlEncode(retVal[0].PassengerLastName + ", " + retVal[0].PassengerMiddleName);
                                }
                                PassengerVM.Passenger1.PassengerName = PassengerVM.lbPaxName;
                                string employeeType = string.IsNullOrEmpty(retVal[0].IsEmployeeType) ? string.Empty : retVal[0].IsEmployeeType.Trim();
                                //initializing multiplier value and associated emp id
                                List<string> AscPaxDetail = new List<string> { "0", "0" };
                                PostflightPaxLite paxLite = new PostflightPaxLite();


                                AscPaxDetail = paxLite.getMultiplierAscPaxID(retVal[0].SIFLSecurity, employeeType, retVal[0].AssociatedWithCD, PostflightLog, ref PassengerVM);
                                PassengerVM.AircraftMultiplier = Convert.ToDecimal(AscPaxDetail[0]);

                                if (!string.IsNullOrEmpty(employeeType) && (employeeType == WebConstants.POEmployeeType.C || employeeType == WebConstants.POEmployeeType.N || employeeType == WebConstants.POEmployeeType.G))
                                {
                                    PassengerVM.EmployeeTYPE = employeeType;
                                    // Check if any Associated Pax available
                                    if (!string.IsNullOrEmpty(retVal[0].AssociatedWithCD))
                                    {
                                        long RequestorVal = Convert.ToInt64(AscPaxDetail[1].Trim());
                                        if (RequestorVal > 0)
                                        {
                                            PassengerVM.AssocPaxEnabled = true;
                                            PassengerVM.btnAssocPaxEnabled = true;
                                            //btnAssocPax.CssClass = "browse-button"; <-- used for enable button

                                            PassengerVM.AssocPaxCD = retVal[0].AssociatedWithCD.Trim();
                                            PassengerVM.AssociatedPassengerID = RequestorVal;
                                            if (string.IsNullOrEmpty(PassengerVM.AssocPaxCD) == false)
                                            {
                                                PassengerVM.AssocPassenger = DBCatalogsDataLoader.GetPassengerReqestorInfo_FromPassengerReqCD(PassengerVM.AssocPaxCD);
                                                if (PassengerVM.AssocPassenger != null && PassengerVM.AssocPassenger.IsEmployeeType == WebConstants.POEmployeeType.G)
                                                {
                                                    PassengerVM.AssocPassenger = new PostflightLegPassengerViewModel();
                                                    PassengerVM.AssocPaxCD = string.Empty;
                                                    PassengerVM.AssociatedPassengerID = 0;
                                                }
                                                else
                                                {
                                                    PassengerVM.AssocPassenger.AssociatedWithCD = PassengerVM.AssocPassenger.PassengerRequestorCD;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (employeeType == WebConstants.POEmployeeType.G)
                                        {
                                            PassengerVM.AssocPaxEnabled = true;
                                            PassengerVM.btnAssocPaxEnabled = true;
                                        }
                                    }
                                }
                                else
                                {
                                    PassengerVM.EmployeeTYPE = string.Empty;
                                }



                                // Calculate SIFL Total
                                PassengerVM.SIFLTotal = paxLite.CalculateSIFL(false, PassengerVM, paxLite.GetCurrentSiflRatesList(), PostflightLog.PostflightMain.Fleet);

                                PassengerVM.AmtTotal = paxLite.CalculateGrandTotalSifl();
                                PassengerVM.alertlbcvPaxName = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                ResultData.Success = true;


                            }
                            else
                            {
                                ResultData.Success = false;
                                ResultData.ErrorsList.Add(System.Web.HttpUtility.HtmlEncode("Invalid Passenger Code!"));
                                PassengerVM.alertlbcvPaxName = System.Web.HttpUtility.HtmlEncode("Invalid Passenger Code!");

                            }
                        }
                    }

                    if (TripLog != null)
                    {
                        var siflIndex = TripLog.PostflightLegSIFLsList.FindIndex(f => f.State != TripEntityState.Deleted && f.IsDeleted == false && PassengerVM.RowNumber == f.RowNumber);
                        if (siflIndex != -1)
                        {
                            TripLog.PostflightLegSIFLsList[siflIndex] = PassengerVM;
                        }
                        HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;
                    }


                    ResultData.Result=PassengerVM;
                    ResultData.StatusCode = HttpStatusCode.OK;
                    return ResultData;
                }
            }, FlightPak.Common.Constants.Policy.UILayer);            
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> TripLogPaxSelectionLegs()
        {
            FSSOperationResult<object> ResultData = new FSSOperationResult<object>();
            if (ResultData.IsAuthorized() == false)
            {
                return ResultData;
            }                        
            Hashtable result = new System.Collections.Hashtable();
            List<PostflightLegPassengerViewModel> passengerList = new List<PostflightLegPassengerViewModel>();
            List<Hashtable> Legs = new List<Hashtable>();
            var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            if (TripLog == null)
            {
                TripLog = new PostflightLogViewModel();
                HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;
            }
            if (TripLog != null)
            {
                if (TripLog.PostflightLegs != null && TripLog.PostflightLegs.Count > 0)
                {
                    var liveLegs = TripLog.PostflightLegs.Where(l=>l.IsDeleted==false && l.State != TripEntityState.Deleted).OrderBy(p => p.LegNUM.Value).ToList();
                    foreach (var Leg in liveLegs)
                    {
                        Hashtable LegTemp = new Hashtable();
                        var lblLegDate = "";
                        var lblLegDepart = "";


                        #region Crew Leg header shoud should show local departure date

                        if (Leg.DepartureDTTMLocal != null)
                        {
                            string strDate = String.Format(CultureInfo.InvariantCulture, "{0:" + getUserPrincipal()._ApplicationDateFormat + "}", Leg.DepartureDTTMLocal);
                            lblLegDate = System.Web.HttpUtility.HtmlEncode(strDate);
                        }

                        #endregion
                        if (Leg.DepartureAirport != null && Leg.ArrivalAirport != null && !string.IsNullOrWhiteSpace(Leg.DepartureAirport.IcaoID) && !string.IsNullOrWhiteSpace(Leg.ArrivalAirport.IcaoID))
                        {
                            lblLegDepart = System.Web.HttpUtility.HtmlEncode("("+ Convert.ToString(Leg.DepartureAirport.IcaoID) + "-" + Convert.ToString(Leg.ArrivalAirport.IcaoID)+")");                        
                        }
                        if (Leg.DepartureAirport == null && Leg.ArrivalAirport != null && !string.IsNullOrWhiteSpace(Leg.ArrivalAirport.IcaoID))
                        {
                            lblLegDepart = System.Web.HttpUtility.HtmlEncode("("+string.Empty + "-" + Convert.ToString(Leg.ArrivalAirport.IcaoID)+")");
                        }
                        if (Leg.DepartureAirport != null && !string.IsNullOrWhiteSpace(Leg.DepartureAirport.IcaoID) && Leg.ArrivalAirport == null)
                        {
                            lblLegDepart = System.Web.HttpUtility.HtmlEncode("("+Convert.ToString(Leg.DepartureAirport.IcaoID) + "-" + string.Empty+")");
                        }                       

                        lblLegDepart= System.Web.HttpUtility.HtmlEncode(lblLegDepart + "\r\nPAX - " + (Leg.PassengerTotal??0).ToString());
                        
                        LegTemp["LegColumnTitle"] = lblLegDepart;                        
                        LegTemp["LegNUM"] = Leg.LegNUM;
                        
                        var PaxListSelect = new Dictionary<string, object>();
                        var PaxDepartPercentageList = new Dictionary<string, object>();

                        List<PostflightLegPassengerViewModel> Paxs = Leg.PostflightLegPassengers.Where(p => p.State != TripEntityState.Deleted && p.IsDeleted == false).ToList();
                        foreach (var pax in Paxs.OrderBy(p => p.OrderNUM.Value))
                        {
                            string paxFPKey = Convert.ToString(pax.PassengerRequestorID) + pax.PassengerFirstName;

                            PaxListSelect[paxFPKey] = pax.FlightPurposeID;
                            PaxDepartPercentageList["DepartPercentage" + paxFPKey] = pax.DepartPercentage??0;

                            if(passengerList.Where(p=>p.PassengerRequestorCD== pax.PassengerRequestorCD).ToList().Count == 0)
                                passengerList.Add(pax);
                            else if (passengerList.Exists(p=>p.PassengerFirstName.Trim() == pax.PassengerFirstName.Trim()) == false && pax.PassengerRequestorID==0 && pax.PassengerRequestorCD==null && pax.PostflightPassengerListID > 0)
                            {
                                passengerList.Add(pax);
                            }
                        }

                        LegTemp["PaxFlightpurposeList"] = PaxListSelect;
                        LegTemp["PaxDepartPercentage"] = PaxDepartPercentageList;
                        Legs.Add(LegTemp);
                    }

                    

                    Dictionary<string, string> PurposeList = new Dictionary<string, string>();
                    CacheLite cachedData = new CacheLite();
                    PurposeList = cachedData.GetFlightpurposeListFromCache();
                    if (PurposeList != null && PurposeList.Count <= 0)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient PurposeService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = PurposeService.GetFlightPurposeList();
                            if (objRetVal.ReturnFlag == true)
                            {
                                 PurposeList = objRetVal.EntityList.ToDictionary(p => p.FlightPurposeCD, p => p.FlightPurposeID.ToString());
                                 cachedData.AddFlightPurposeListToCache(PurposeList);
                            }
                        }
                    }
                    passengerList = passengerList.OrderBy(g => g.OrderNUM).ToList();
                    int rowNum = 1;
                    passengerList.ForEach(exp =>
                    {
                        exp.RowNumber = rowNum++;
                    });

                    Hashtable data = new Hashtable();
                    data["meta"] = new FlightPak.Web.ViewModels.PagingMetaData() { Page = 1, Size = passengerList.Count, total_items = passengerList.Count };                    
                    data["results"] = passengerList;


                    result["PassengerList"] = data;
                    result["PurposeList"] = PurposeList;
                    result["results"] = Legs;

                    ResultData.Result = result;
                    ResultData.Success = true;
                    return ResultData;
                }
            }
            return ResultData;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<int> AddNewPax(List<string> passengerRequestorCDs, int? InsertPosition, bool? IsPassengerIds)
        {
            FSSOperationResult<int> ResultData = new FSSOperationResult<int>();
            if (ResultData.IsAuthorized() == false)
            {
                return ResultData;
            }
            PostflightPaxLite Pax = new PostflightPaxLite();
            ResultData.Result = Pax.AddNewPax(passengerRequestorCDs, InsertPosition, IsPassengerIds);
            ResultData.Success = true;
            return ResultData;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<int> DeletePax(List<string> passengerRequestorCDs,List<string> firstNamelist)
        {
            FSSOperationResult<int> ResultData = new FSSOperationResult<int>();
            if (ResultData.IsAuthorized() == false)
            {
                return ResultData;
            }                                    
            PostflightPaxLite Pax = new PostflightPaxLite();
            passengerRequestorCDs = passengerRequestorCDs.Select(p => p.Trim()).ToList();

            if (firstNamelist!=null &&( passengerRequestorCDs==null || passengerRequestorCDs.Count==0))
                ResultData.Result = Pax.DeletePaxByFirstName(firstNamelist);
            else
            {
                ResultData.Result = Pax.DeletePax(passengerRequestorCDs);
                if(firstNamelist!=null && firstNamelist.Count > 0)
                    ResultData.Result = Pax.DeletePaxByFirstName(firstNamelist);
            }

            ResultData.Success = true;
            return ResultData;

        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable GetSelectedPassengerForPopup()
        {
            PostflightPaxLite Pax = new PostflightPaxLite();            
            Hashtable data = new Hashtable();
            List<PostflightLegPassengerViewModel> paxList = Pax.GetPaxListofGrid().Where(p=>p.IsDeleted==false && p.State!= TripEntityState.Deleted && p.PassengerRequestorCD!=null && (p.DateOfBirth==null  || (p.DateOfBirth.HasValue && p.DateOfBirth.Value.AddYears(2) < DateTime.Now))).ToList();
            data["meta"] = new FlightPak.Web.ViewModels.PagingMetaData() { Page = 1, Size = paxList.Count, total_items = paxList.Count };
            data["results"] = paxList;
            return data;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static void SaveFlightPurposeOfOnePaxByLeg(string legNum, string PaxCode, string FlightPurposeID, string PassengerFirstName)
        {
            PostflightPaxLite Pax = new PostflightPaxLite();
            Pax.SaveLegPAXFlightPurpose(legNum, PaxCode, FlightPurposeID, PassengerFirstName);            

        }
        
        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat= ResponseFormat.Json)]
        public static void SavePaxGrid(string GridJson)
        {
            Hashtable data = new Hashtable();
            dynamic Obj = JArray.Parse(GridJson);
            PostflightPaxLite Pax = new PostflightPaxLite();
            Pax.SaveGridtoSession(Obj);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> SavePaxLegDepartPercentage(int legNum, string PaxCode, int departPercentage, string PassengerFirstName)
        {
            FSSOperationResult<object> ResultData = new FSSOperationResult<object>();
            if (ResultData.IsAuthorized()==false)
            {
                return ResultData;
            }
            var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            if (TripLog != null && TripLog.PostflightLegs != null)
            {
                foreach (var LegVM in TripLog.PostflightLegs.Where(l => l.State != TripEntityState.Deleted && l.IsDeleted==false).ToList())
                {
                    if (LegVM.LegNUM == legNum)
                    {
                        if (LegVM.PostflightLegPassengers != null)
                        {
                            foreach (var paxVM in LegVM.PostflightLegPassengers.Where(p => p.State != TripEntityState.Deleted && p.IsDeleted == false).ToList())
                            {
                                bool isAvailable = false;
                                if ((paxVM.PassengerRequestorCD == PaxCode || (paxVM.PassengerFirstName == PassengerFirstName && paxVM.PassengerRequestorID==0)) && paxVM.FlightPurposeID.HasValue && paxVM.FlightPurposeID > 1)
                                {
                                    isAvailable = true;
                                }
                                if (isAvailable)
                                {
                                    paxVM.DepartPercentage = departPercentage;
                                    if (paxVM.PostflightPassengerListID > 0)
                                        paxVM.State = TripEntityState.Modified;

                                    break;
                                }
                            }
                        }
                    }
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;
                ResultData.Success = true;
                ResultData.StatusCode = HttpStatusCode.OK;
                return ResultData;
            }
            else
            {
                ResultData.Success = false;
                ResultData.StatusCode = HttpStatusCode.OK;
                return ResultData;
            }
            
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> CheckDeptPercentage()
        {
            FSSOperationResult<bool> result = new FSSOperationResult<bool>();
            if (result.IsAuthorized() == false)
            {
                return result;
            }
            result.Result = true; result.Success = true;
            
            var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

            if (TripLog != null)
            {
                List<PostflightLegViewModel> legList = TripLog.PostflightLegs.Where(l => l.State != TripEntityState.Deleted && l.IsDeleted==false).ToList() ?? new List<PostflightLegViewModel>();
                //BRM 18 - Loop to check each items for the Discount Percentage is greater than 100
                foreach (PostflightLegViewModel leg in legList)
                {
                    Int32 departPercentage = 0;
                    departPercentage = Convert.ToInt32(leg.PostflightLegPassengers.Where(p => p.State != TripEntityState.Deleted && p.IsDeleted == false && p.FlightPurposeID != null && p.FlightPurposeID > 1).ToList().Select(p => p.DepartPercentage ?? 0).Sum());
                    if (departPercentage > 0 && departPercentage != 100)
                    {
                        result.ErrorsList.Add("Leg " + leg.LegNUM.ToString() + " - Total Department percentage should be 100" + "<br/>");
                        result.Success = false;
                        result.Result = false;
                    }
                }
            }
            return result;            
        }

        #region  PaxSummary
        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat= ResponseFormat.Json)]
        public static Hashtable PaxSummaryGrid()
        {
            Hashtable PaxData = new Hashtable();
            PostflightPaxLite paxlite = new PostflightPaxLite();
            List<PostflightLegPassengerViewModel> paxDataList= paxlite.LoadPaxSummaryGridData();
            if (paxDataList == null)
                paxDataList = new List<PostflightLegPassengerViewModel>();

            PaxData["meta"] = new FlightPak.Web.ViewModels.PagingMetaData() { Page = 1, Size =paxDataList.Count,total_items=paxDataList.Count };
            PaxData["results"]=paxDataList;
            return PaxData;
        }

        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static FSSOperationResult<bool> SavePaxSummaryBillingUpdate(long passengerRequstorID,long legNum,string billingInfo)
        {
            FSSOperationResult<bool> ResultObject = new FSSOperationResult<bool>();
            if (ResultObject.IsAuthorized() == false)
            {
                return ResultObject;
            }
            
            if (HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel] != null)
            {
                PostflightPaxLite poLite = new PostflightPaxLite();
                ResultObject.Success = poLite.SavePaxBillingInfo(passengerRequstorID, legNum, billingInfo);
                ResultObject.StatusCode = HttpStatusCode.OK;
                ResultObject.Result = true;
            }
            else
            {
                ResultObject.StatusCode = HttpStatusCode.Unauthorized;
                ResultObject.Result = false;
                ResultObject.Success = false;
            }            
            return ResultObject;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> UpdatePassportAsSelected(bool updateForAllLegs,long legNum,long selectedPaxID,long passportId)
        {
            FSSOperationResult<bool> ResultData = new FSSOperationResult<bool>();
            if (ResultData.IsAuthorized() == false)
            {
                return ResultData;
            }            
            var pfLogVM = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

            if (pfLogVM.PostflightLegs != null && pfLogVM.PostflightLegs.Count > 0 && pfLogVM.PostflightMain.POEditMode==true)
            {
                var poLeg = pfLogVM.PostflightLegs.Where(l => l.LegNUM == legNum && l.IsDeleted == false && l.State != TripEntityState.Deleted).FirstOrDefault();
                if (poLeg.PostflightLegPassengers != null && poLeg.PostflightLegPassengers.Count > 0)
                {
                    var pax = poLeg.PostflightLegPassengers.Where(p => p.PassengerRequestorID == selectedPaxID).FirstOrDefault();
                    if (pax == null)
                    {
                        ResultData.Success = false;
                        return ResultData;
                    }
                    CacheLite cacheData = new CacheLite();
                    var passport = pax.PassportID.HasValue ? cacheData.GetPassportFromCache(pax.PassportID.Value) : null;
                    bool samePassport = false;
                    if (passport != null && updateForAllLegs==false)
                    {
                        if (passport.PassportID == passportId)
                        {
                            samePassport = true;
                        }
                    }
                    pax.PassportID = passportId;
                    if (passport == null || samePassport==false)
                    {
                        FlightPak.Web.BusinessLite.Preflight.PreflightPaxLite preflightPaxLite = new PreflightPaxLite();    
                        var PaxPassportList = preflightPaxLite.GetPaxPassportData((pax.PassengerRequestorID).ToString()).Where(g => g.IsDeleted == false && g.PassportID == passportId).ToList();
                        if (PaxPassportList != null && PaxPassportList.Count > 0)
                        {
                            cacheData.AddPassportToCache(PaxPassportList[0]);
                        }
                        else
                        {
                            cacheData.AddPassportToCache(new FlightPak.Web.ViewModels.PaxPassportViewModel() { PassengerID = pax.PassengerRequestorID });
                        }
                        passport = pax.PassportID.HasValue ? cacheData.GetPassportFromCache(pax.PassportID.Value):null;
                    }
                    if (passport != null)
                    {
                        PostflightPaxLite poPaxLite = new PostflightPaxLite();
                        if (updateForAllLegs == true)
                        {
                            foreach (var legitem in pfLogVM.PostflightLegs.Where(l=>l.IsDeleted==false && l.State != TripEntityState.Deleted).OrderBy(o=>o.LegNUM))
                            {
                                PostflightLegPassengerViewModel passengerInLeg = legitem.PostflightLegPassengers.Where(p => p.IsDeleted == false && p.State != TripEntityState.Deleted && p.PassengerRequestorID== pax.PassengerRequestorID).FirstOrDefault();
                                if(passengerInLeg!=null)
                                {                                    
                                    poPaxLite.FillPaxPassportDetailsFromPassportObject(ref passengerInLeg, passport);
                                    if (passengerInLeg.PostflightPassengerListID > 0)
                                        passengerInLeg.State = TripEntityState.Modified;
                                }
                            }
                        }
                        else
                        {
                            if (pax.PostflightPassengerListID > 0)
                                pax.State = TripEntityState.Modified;
                            
                            poPaxLite.FillPaxPassportDetailsFromPassportObject(ref pax, passport);                             
                        }
                    }
                    
                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfLogVM;
                    ResultData.Success = true;
                }
                else
                {
                    ResultData.Success = false;
                }
            }
            else
            {
                ResultData.Success = false;
            }
            return ResultData;
        }


        #endregion


        #region "Pax SIFL"
        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static Hashtable InitializePaxSIFLGrid()
        {
            Hashtable ResultObject = new Hashtable();
            List<FlightPak.Web.Framework.Helpers.PostflightSIFLPersonal> paxPersonalList = new List<Framework.Helpers.PostflightSIFLPersonal>();

            PostflightPaxLite paxLite = new PostflightPaxLite();
            var siflPersonnalList = paxLite.PrepareSIFLPersonalList();
            paxPersonalList = siflPersonnalList.GroupBy(p => p.LegNUM).Select(g => g.ToList().FirstOrDefault()).ToList();

            if (paxPersonalList == null)
                paxPersonalList = new List<Framework.Helpers.PostflightSIFLPersonal>();

            for (int i = 0; i < paxPersonalList.Count; i++)            
            {
                var objPax = paxPersonalList[i];
                if (objPax.DateOfBirth != null && objPax.DateOfBirth.Value.AddYears(2) > DateTime.Now)
                {
                    paxPersonalList.RemoveAt(i);
                }
            }

            
            var TripLogVM = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            if (TripLogVM.PostflightMain.POEditMode)
            {
                if (TripLogVM != null && TripLogVM.PostflightMain != null)
                {
                    TripLogVM.PostflightMain.IsPersonal = paxPersonalList.Count > 0 ? true : false;
                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLogVM;
                }
            }

            ResultObject["meta"] = new FlightPak.Web.ViewModels.PagingMetaData() { Page = 1, Size = paxPersonalList.Count, total_items = paxPersonalList.Count };
            ResultObject["results"] = paxPersonalList;
            return ResultObject;            
        }

        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static FSSOperationResult<List<PostflightLegSIFLViewModel>> AddNewSIFLClick()
        {
            FSSOperationResult<List<PostflightLegSIFLViewModel>> ResultData = new FSSOperationResult<List<PostflightLegSIFLViewModel>>();
            if (ResultData.IsAuthorized()== false)
            {
                return ResultData;
            }

            
            var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            if (TripLog.PostflightLegs.Where(l => l.State != TripEntityState.Deleted && l.IsDeleted == false).Count() > 0)
            {
                var leg = TripLog.PostflightLegs.Where(l => l.State != TripEntityState.Deleted && l.IsDeleted == false).FirstOrDefault();                
                if (leg!=null && leg.PostflightLegPassengers == null || leg.PostflightLegPassengers.Where(p => p.State != TripEntityState.Deleted && p.IsDeleted == false).Count() <= 0)
                {
                    ResultData.Success = false;
                    ResultData.ErrorsList.Add("Atleast one Passenger should be available.");
                    ResultData.StatusCode = HttpStatusCode.NotFound;
                    return ResultData;
                }
            }

            PostflightPaxLite paxLite = new PostflightPaxLite();
            return paxLite.AddNewSIFLClick();
        }


        // A flag variable while recalculate button click
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<List<PostflightLegSIFLViewModel>> GetListOfSIFLLegs(bool isUpdateClick)
        {
            FSSOperationResult<List<PostflightLegSIFLViewModel>> ResultData = new FSSOperationResult<List<PostflightLegSIFLViewModel>>();
            if (ResultData.IsAuthorized()== false)
            {
                return ResultData;
            }
            
            //Handle methods throguh exception manager with return flag
            var exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            exManager.Process(() =>
            {
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

                    List<PostflightLegSIFLViewModel> RetainSilfList = new List<PostflightLegSIFLViewModel>();
                    RetainSilfList = TripLog.PostflightLegSIFLsList.Where(s => s.State != TripEntityState.Deleted && s.IsDeleted == false).OrderBy(o=>o.PassengerRequestorCD).ToList();

                    if (TripLog.PostflightMain.POEditMode)
                    {
                        PostflightPaxLite paxLite = new PostflightPaxLite();
                        foreach (var itemSiflVM in RetainSilfList)
                        {
                            itemSiflVM.SIFLTotal = paxLite.CalculateSIFL(true, itemSiflVM, paxLite.GetCurrentSiflRatesList(), TripLog.PostflightMain.Fleet);
                            if (itemSiflVM.POSIFLID > 0 && (itemSiflVM.State == TripEntityState.NoChange) && isUpdateClick == true && TripLog.PostflightMain.POEditMode == true)
                            {
                                itemSiflVM.State = TripEntityState.Modified;
                            }
                        }
                        var amountTotal = paxLite.CalculateGrandTotalSifl();
                        RetainSilfList.ForEach(sf =>
                        {
                            sf.AmtTotal = amountTotal;
                        });
                    }

                    ResultData.Result = RetainSilfList;
                    ResultData.StatusCode = HttpStatusCode.OK;
                    ResultData.Success = true;
                }
                else
                {
                    ResultData.Result = new List<PostflightLegSIFLViewModel>();
                    ResultData.StatusCode = HttpStatusCode.OK;
                    ResultData.Success = false;
                }
            }, FlightPak.Common.Constants.Policy.UILayer);
            return ResultData;
        }

        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static Hashtable GetPostflightSIFLAirportData(bool isDepartAirport)
        {
            Hashtable data = new Hashtable();
            List<AirportViewModel> airportList = new List<AirportViewModel>();            
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
            {
                var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                foreach (var legs in TripLog.PostflightLegs)
                {
                    legs.ArrivalAirport.LegID = legs.POLegID;
                    legs.DepartureAirport.LegID = legs.POLegID;
                }
                if (isDepartAirport)
                    airportList = TripLog.PostflightLegs.Where(l => l.IsDeleted == false && l.DepartureAirport != null).Select(a => a.DepartureAirport).ToList();
                else{
                    airportList = TripLog.PostflightLegs.Where(l => l.IsDeleted == false && l.ArrivalAirport != null).Select(a => a.ArrivalAirport).ToList();
                }
                //var legsList= 
            }
            data["meta"] = new PagingMetaData() { Page = 1, Size = airportList.Count(), total_items = airportList.Count() };
            data["results"] = airportList;
            return data;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightLegSIFLViewModel> SIFLDistanceCalculation(PostflightLegSIFLViewModel SIFLViewModel)
        {
            FSSOperationResult<PostflightLegSIFLViewModel> ResultData = new FSSOperationResult<PostflightLegSIFLViewModel>();
            if (ResultData.IsAuthorized()== false)
            {
                return ResultData;
            }

            
            PostflightPaxLite paxlite= new PostflightPaxLite();
            var TripLog = (PostflightLogViewModel) HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

            try
            {
                var siflObjectIndex = TripLog.PostflightLegSIFLsList.FindIndex(s => s.RowNumber == SIFLViewModel.RowNumber);                    
                if (SIFLViewModel != null && siflObjectIndex!=-1)
                {
                    SIFLViewModel.SIFLTotal = paxlite.CalculateSIFL(true, SIFLViewModel, paxlite.GetCurrentSiflRatesList(), TripLog.PostflightMain.Fleet);
                    ResultData.Success = true;
                    ResultData.StatusCode = HttpStatusCode.OK;
                    ResultData.Result = SIFLViewModel;
                    
                }
                
            }
            catch (Exception)
            {
                ResultData.Success = false;
                ResultData.StatusCode = HttpStatusCode.OK;
                
            }
            

            return ResultData;

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightLegSIFLViewModel> Validation_SIFLIcao_KO(PostflightLegSIFLViewModel siflViewmodel,bool isDepartIcao)
        {
            FSSOperationResult<PostflightLegSIFLViewModel> ResultData= new FSSOperationResult<PostflightLegSIFLViewModel>();
            if (ResultData.IsAuthorized() == false)
            {
                return ResultData;
            }

            PostflightPaxLite paxLite = new PostflightPaxLite();
            return paxLite.Validation_SIFLIcao(siflViewmodel, isDepartIcao);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> Delete_Sifl_Leg(PostflightLegSIFLViewModel siflViewModel)
        {
            FSSOperationResult<bool> ResultData = new FSSOperationResult<bool>();
            if (ResultData.IsAuthorized()==false)
            {
                return ResultData;
            }            
            try
            {
                    var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                    if (TripLog != null)
                    {
                        if (TripLog.PostflightMain.DeletedSIFLIDs== null)
                            TripLog.PostflightMain.DeletedSIFLIDs = new Dictionary<long, long>();

                        if (TripLog.PostflightLegSIFLsList != null)
                        {
                            if (TripLog.PostflightLegSIFLsList.Count > 0)
                            {
                                var siflObjectToDelete = TripLog.PostflightLegSIFLsList.Where(s=>s.RowNumber==siflViewModel.RowNumber).FirstOrDefault();
                                if (siflObjectToDelete != null)
                                {
                                    if (siflObjectToDelete.POSIFLID > 0)
                                    {
                                        siflObjectToDelete.IsDeleted = true;
                                        siflObjectToDelete.State =  TripEntityState.Deleted;
                                        TripLog.PostflightMain.DeletedSIFLIDs.Add(siflObjectToDelete.POSIFLID, siflObjectToDelete.POLegID??0);                                        
                                    }
                                    else
                                    {
                                        var siflDeleteIndex = TripLog.PostflightLegSIFLsList.FindIndex(s => s.RowNumber == siflViewModel.RowNumber);
                                        TripLog.PostflightLegSIFLsList.RemoveAt(siflDeleteIndex);
                                    }
                                    PostflightPaxLite paxLite= new PostflightPaxLite();
                                    foreach (var siflItem in TripLog.PostflightLegSIFLsList.Where(l => l.State != TripEntityState.Deleted && l.IsDeleted == false).ToList())
                                    {
                                        siflItem.SIFLTotal = paxLite.CalculateSIFL(false, siflItem, paxLite.GetCurrentSiflRatesList(), TripLog.PostflightMain.Fleet);
                                    }
                                    var amountTotal= paxLite.CalculateGrandTotalSifl();
                                    foreach (var siflItem in TripLog.PostflightLegSIFLsList.Where(l => l.State != TripEntityState.Deleted && l.IsDeleted == false).ToList())
                                    {
                                        siflItem.AmtTotal = amountTotal;
                                    }

                                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;
                                }
                            }
                        }
                        ResultData.Success = true;
                        ResultData.StatusCode = HttpStatusCode.OK;
                    }
                    else
                    {
                        ResultData.Success = false;
                        ResultData.StatusCode = HttpStatusCode.OK;
                    }       
            }
            catch (Exception ex)
            {
                ResultData.Success = false;
                ResultData.StatusCode = HttpStatusCode.OK;
            }
            return ResultData;


        }

        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat= ResponseFormat.Json)]
        public static FSSOperationResult<List<PostflightLegSIFLViewModel>> Recalculate_SiflClick()
        {
            FSSOperationResult<List<PostflightLegSIFLViewModel>> ResultData = new FSSOperationResult<List<PostflightLegSIFLViewModel>>();
            if(ResultData.IsAuthorized()==false)
            {
                return ResultData;
            }


            var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

            if (TripLog.PostflightMain.DeletedSIFLIDs == null)
                TripLog.PostflightMain.DeletedSIFLIDs = new Dictionary<long, long>();

            var RecalcSiflList = new List<PostflightLegSIFLViewModel>();
            
            // Loop through each leg and Append the SIFL in the List

            // Prepare for SIFL Grid
            if (TripLog.PostflightLegSIFLsList != null)
                RecalcSiflList.AddRange(TripLog.PostflightLegSIFLsList.Where(x => x.State != TripEntityState.Deleted && x.IsDeleted == false));
            

            // To Delete existing SIFL update the SIFL IDs in the Dictionary, this will handled in 
            // the Postflight Manager Class.
            foreach (PostflightLegSIFLViewModel item in RecalcSiflList)
            {
                if (!TripLog.PostflightMain.DeletedSIFLIDs.ContainsKey(item.POSIFLID) && item.POSIFLID != 0 && (item.POLegID??0) > 0)
                    TripLog.PostflightMain.DeletedSIFLIDs.Add(item.POSIFLID,(long)item.POLegID);
            }
            RecalcSiflList = new List<PostflightLegSIFLViewModel>();
            PostflightPaxLite paxLite = new PostflightPaxLite();
            var personalList= paxLite.PrepareSIFLPersonalList();
            if (personalList==null || personalList.Count ==0)
            {
                TripLog.PostflightLegSIFLsList = new List<PostflightLegSIFLViewModel>();
                ResultData.Success = false;
                ResultData.StatusCode = HttpStatusCode.OK;
                HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;
                return ResultData;
            }
            HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;
            
           return paxLite.RecalculateSiflLegs(TripLog);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> SiflReport_Click()
        {
            FSSOperationResult<bool> ResultData = new FSSOperationResult<bool>();
            if (ResultData.IsAuthorized() == false)
            {
                return ResultData;
            }
             var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
             var UserPrincipal = (UserPrincipalViewModel)HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel];
             if (TripLog != null && UserPrincipal != null && TripLog.POLogID > 0 &&(TripLog.PostflightLegSIFLsList != null && TripLog.PostflightLegSIFLsList.Count > 0))
             {
                     HttpContext.Current.Session["REPORT"] = "RptPOSTSIFL";
                     HttpContext.Current.Session["FORMAT"] = "PDF";
                     HttpContext.Current.Session["TabSelect"] = "PostFlight";
                     HttpContext.Current.Session["PARAMETER"] = "UserCD=" + UserPrincipal._name + ";UserCustomerID=" + UserPrincipal._customerID + ";LogID=" + TripLog.POLogID.ToString();

                     ResultData.Success = true;
                     ResultData.StatusCode = HttpStatusCode.OK;
                     ResultData.Result = true;
             }
             else
             {
                 ResultData.Success = false;
                 ResultData.StatusCode = HttpStatusCode.OK;
                 ResultData.Result = false;
             }
             return ResultData;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> SaveSiflViewModel(PostflightLegSIFLViewModel siflViewModel)
        {
            FSSOperationResult<bool> ResultData = new FSSOperationResult<bool>();
            if(ResultData.IsAuthorized() == false)
            {
                return ResultData;
            }
            ResultData.Success= false;
            ResultData.Result = false;
            var TripLog= (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            if (TripLog != null)
            {
                if (TripLog.PostflightLegSIFLsList != null && TripLog.PostflightLegSIFLsList.Count > 0)
                {
                    var existingSifl = TripLog.PostflightLegSIFLsList.Where(s => s.RowNumber == siflViewModel.RowNumber && s.POLegID == siflViewModel.POLegID).FirstOrDefault();
                    if (existingSifl != null)
                    {
                        existingSifl = (PostflightLegSIFLViewModel)existingSifl.InjectFrom(siflViewModel);
                        ResultData.Success = true;
                        ResultData.Result = true;
                        return ResultData;
                    }    
                }
            }
            return ResultData;

        }

            #endregion


        private static PostflightLogViewModel SetPostflightSiflList(PostflightMain logResult, PostflightLogViewModel pfViewModel)
        {
            if (pfViewModel != null & logResult != null)
            {
                pfViewModel.PostflightLegSIFLsList = new List<PostflightLegSIFLViewModel>();
                if (logResult.PostflightSIFLs != null && logResult.PostflightSIFLs.Count > 0)
                {
                    foreach (var siflItem in logResult.PostflightSIFLs)
                    {
                        PostflightLegSIFLViewModel siflViewModel = (PostflightLegSIFLViewModel)new PostflightLegSIFLViewModel().InjectFrom(siflItem);
                        siflViewModel.Passenger1 = new PostflightLegPassengerViewModel();
                        if (siflItem.Passenger1 != null)
                        {
                            siflViewModel.Passenger1 = (PostflightLegPassengerViewModel)siflViewModel.Passenger1.InjectFrom(siflItem.Passenger1);
                            siflViewModel.Passenger1.PassengerRequestorID = siflItem.PassengerRequestorID ?? 0;
                            siflViewModel.PassengerRequestorCD = siflItem.Passenger1.PassengerRequestorCD;
                        }
                        if (siflItem.Passenger != null)
                        {
                            siflViewModel.AssocPassenger = (PostflightLegPassengerViewModel)siflViewModel.AssocPassenger.InjectFrom(siflItem.Passenger);
                            siflViewModel.AssocPassenger.PassengerRequestorID = siflItem.AssociatedPassengerID ?? 0;
                            siflViewModel.AssocPassenger.AssociatedWithCD = siflItem.Passenger.PassengerRequestorCD;
                            siflViewModel.AssocPaxCD = siflItem.Passenger.PassengerRequestorCD;
                        }
                        siflViewModel.DepartICAOID = siflItem.DepartICAOID;
                        siflViewModel.ArriveICAOID = siflItem.ArriveICAOID;

                        siflViewModel.DepartAirport = (AirportViewModel)siflViewModel.DepartAirport.InjectFrom(siflItem.Airport1);

                        siflViewModel.ArriveAirport = (AirportViewModel)siflViewModel.ArriveAirport.InjectFrom(siflItem.Airport);
                        siflViewModel.RowNumber = pfViewModel.PostflightLegSIFLsList.Count + 1;
                        pfViewModel.PostflightLegSIFLsList.Add(siflViewModel);
                    }
                }
            }
            return pfViewModel;
        }

        #endregion        
       
        #region Postflight Trip To Postflight Trip View Model
        public static PostflightLogViewModel InjectPostflightMainToPostflightLogViewModel(PostflightMain Trip, PostflightLogViewModel tripVM)
        {
            UserPrincipalViewModel upViewModel = getUserPrincipal();
            tripVM = (PostflightLogViewModel)tripVM.InjectFrom(Trip);
            tripVM.POLogID = Trip.POLogID;
            tripVM.PostflightMain.dateFormat = upViewModel._ApplicationDateFormat;
            tripVM.PostflightMain = SetPostflightMain(tripVM.PostflightMain, Trip);

            if (tripVM.PostflightMain.LastUpdTS != null)
            {
                if (upViewModel._airportId != null)
                {
                    using (CalculationService.CalculationServiceClient calcSvc = new FlightPak.Web.CalculationService.CalculationServiceClient())
                    {
                        DateTime? dt = tripVM.PostflightMain.LastUpdTS;
                        dt = calcSvc.GetGMT(upViewModel._airportId, (DateTime)tripVM.PostflightMain.LastUpdTS, false, false);
                        DateTime? UTCdt = tripVM.PostflightMain.LastUpdTS.Value.ToUniversalTime();
                        tripVM.PostflightMain.LastUpdatedBy = "Last Modified: " + tripVM.PostflightMain.LastUpdUID + " " + dt.FSSParseDateTimeString(upViewModel._ApplicationDateFormat + " HH:mm") + " LCL" + " (" + tripVM.PostflightMain.LastUpdTS.FSSParseDateTimeString(upViewModel._ApplicationDateFormat + " HH:mm") + " UTC)";
                    }
                }
            }
            return tripVM;
        }

        static PostflightMainViewModel SetPostflightMain(PostflightMainViewModel PostflightMain, PostflightMain Trip)
        {
            PostflightMain = Trip != null ? (PostflightMainViewModel)new PostflightMainViewModel(getUserPrincipal()._ApplicationDateFormat).InjectFrom(Trip) : new PostflightMainViewModel(getUserPrincipal()._ApplicationDateFormat);
            PostflightMain.Client = Trip.Client != null ? (ClientViewModel)new ClientViewModel().InjectFrom(Trip.Client) : new ClientViewModel();
            PostflightMain.Fleet = Trip.Fleet != null ? (FleetViewModel)new FleetViewModel().InjectFrom(Trip.Fleet) : new FleetViewModel();
            PostflightMain.Account = Trip.Account != null ? (AccountViewModel)new AccountViewModel().InjectFrom(Trip.Account) : new AccountViewModel();
            PostflightMain.Aircraft = Trip.Aircraft != null ? (AircraftViewModel)new AircraftViewModel().InjectFrom(Trip.Aircraft) : new AircraftViewModel();
            PostflightMain.Company = Trip.Company != null ? (CompanyViewModel)new CompanyViewModel().InjectFrom(Trip.Company) : new CompanyViewModel();
            PostflightMain.Department = Trip.Department != null ? (DepartmentViewModel)new DepartmentViewModel().InjectFrom(Trip.Department) : new DepartmentViewModel();
            PostflightMain.DepartmentAuthorization = Trip.DepartmentAuthorization != null ? (DepartmentAuthorizationViewModel)new DepartmentAuthorizationViewModel().InjectFrom(Trip.DepartmentAuthorization) : new DepartmentAuthorizationViewModel();

            PostflightMain.Passenger = Trip.Passenger != null ? (PostflightLegPassengerViewModel)new PostflightLegPassengerViewModel().InjectFrom(Trip.Passenger) : new PostflightLegPassengerViewModel();
            PostflightMain.RecordLockInfo = Trip.RecordLockInfo != null ? (object)new object().InjectFrom(Trip.RecordLockInfo) : new object();
            PostflightMain.TripActionStatus = Trip.TripActionStatus != null ? (TripAction)new TripAction().InjectFrom(Trip.TripActionStatus) : new TripAction();            
            PostflightMain.Airport = (Trip.Company!=null && Trip.Company.Airport != null) ? (AirportViewModel)new AirportViewModel().InjectFrom(Trip.Company.Airport) : new AirportViewModel();
            if (string.IsNullOrWhiteSpace(PostflightMain.Aircraft.AircraftCD))
            {
                PostflightMainLite pmLite= new PostflightMainLite();
                PostflightMain.Aircraft = pmLite.LoadAircraftViewModelFromTaiNum(PostflightMain.Fleet.TailNum.Trim());
                PostflightMain.TypeCode = PostflightMain.Aircraft.AircraftCD;
                PostflightMain.TailNum = PostflightMain.Fleet.TailNum;
            }

            using (FlightPakMasterService.MasterCatalogServiceClient TailService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                long aircraftid = Trip.AircraftID ?? 0;
                if(aircraftid==0 && PostflightMain.Aircraft!=null && PostflightMain.Aircraft.AircraftID==0)
                    aircraftid = PostflightMain.Aircraft.AircraftID;
                var AircraftResult=TailService.GetAircraftByAircraftID(aircraftid);
                if(AircraftResult!=null && AircraftResult.ReturnFlag==true && AircraftResult.EntityList!=null && AircraftResult.EntityList.Count > 0){                    
                    PostflightMain.Aircraft.AircraftCD = AircraftResult.EntityList[0].AircraftCD;
                }                
            }
            return PostflightMain;
        }

        static PostflightLogViewModel SetPostflightLegs(PostflightMain Trip, PostflightLogViewModel pfViewModel)
        {
            if (Trip == null || Trip.PostflightLegs == null || Trip.PostflightLegs.Count <= 0)
                return pfViewModel;

            List<PostflightLeg> legs = Trip.PostflightLegs.Where(l => l.IsDeleted == false).OrderBy(lg => lg.LegNUM).ToList();
            if (pfViewModel.PostflightLegs != null)
                pfViewModel.PostflightLegs.Clear();
            foreach(PostflightLeg leg in legs)
            {
                PostflightLegViewModel legVM = new PostflightLegViewModel();
                legVM = (PostflightLegViewModel)legVM.InjectFrom(leg);
                legVM = SetLegUserPrincipalSettings(legVM);
                legVM.ArrivalAirport = (AirportViewModel)new AirportViewModel().InjectFrom(leg.Airport);
                legVM.DepartureAirport = (AirportViewModel)new AirportViewModel().InjectFrom(leg.Airport1);
                legVM.PostflightLegCrews = new List<PostflightLegCrewViewModel>();
                
                legVM.FlightCatagory = leg.FlightCategoryID.HasValue ? DBCatalogsDataLoader.GetFlightCategoryInfo_FromId(leg.FlightCategoryID.Value) : new FlightCatagoryViewModel();
                
                if (legVM.CrewDutyRule == null || legVM.CrewDutyRule.CrewDutyRulesID == 0)
                {
                    legVM.CrewDutyRule = (string.IsNullOrEmpty(legVM.CrewCurrency) == false) ? DBCatalogsDataLoader.GetCrewDutyRulesInfo_FromFilter(0, leg.CrewCurrency.Trim()) : new CrewDutyRuleViewModel();
                    if (legVM.CrewDutyRule != null && legVM.CrewDutyRule.CrewDutyRulesID > 0)
                    {
                        legVM.CrewDutyRulesID = legVM.CrewDutyRule.CrewDutyRulesID;
                        legVM.FedAviationRegNum = string.IsNullOrEmpty(legVM.FedAviationRegNum) ? legVM.CrewDutyRule.FedAviatRegNum.Trim() : legVM.FedAviationRegNum.Trim();
                    }
                }
                int crewRowNumber = 1;
                foreach (var poCrew in leg.PostflightCrews)
                {
                    PostflightLegCrewViewModel poCrewObj = new PostflightLegCrewViewModel();
                    poCrewObj = (PostflightLegCrewViewModel)new PostflightLegCrewViewModel().InjectFrom(poCrew);
                    poCrewObj.RowNumber = crewRowNumber++;
                    legVM.PostflightLegCrews.Add(poCrewObj);
                }
                pfViewModel.PostflightLegs.Add(legVM);

                // Loading Passenger List of Current Leg

                legVM.PostflightLegPassengers = new List<PostflightLegPassengerViewModel>();
                foreach (var poPax in leg.PostflightPassengers.OrderBy(t=>t.OrderNUM).ToList())
                {
                    PostflightLegPassengerViewModel PaxObjectVM= new PostflightLegPassengerViewModel();
                    if(poPax.Passenger!=null)
                        PaxObjectVM = (PostflightLegPassengerViewModel)PaxObjectVM.InjectFrom(poPax.Passenger);

                    PaxObjectVM = (PostflightLegPassengerViewModel)PaxObjectVM.InjectFrom(poPax);

                    legVM.PostflightLegPassengers.Add(PaxObjectVM);
                }
            }
            return pfViewModel;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<List<PostflightLegViewModel>> InitializePostflightLegs()
        {
            FSSOperationResult<List<PostflightLegViewModel>> RetObj = new FSSOperationResult<List<PostflightLegViewModel>>();

            PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();
            
            if (UserPrincipal == null)
            {
                RetObj.Result = null;
                RetObj.StatusCode = HttpStatusCode.Unauthorized;
                RetObj.Success = false;
                RetObj.ErrorsList.Add(WebErrorConstants.UnauthorizdOperation);
                return RetObj;
            }
            RetObj.Success = true;
            RetObj.StatusCode = HttpStatusCode.OK;

            if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
            {
                pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

                if (pfViewModel.PostflightLegs == null || pfViewModel.PostflightLegs.Count <= 0)
                {
                    PostflightLegViewModel leg = new PostflightLegViewModel();
                    leg = SetLegUserPrincipalSettings(leg);
                    pfViewModel.PostflightLegs.Add(leg);
                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                }
                RetObj.Result = pfViewModel.PostflightLegs.Where(l => l.IsDeleted == false).OrderBy(o => o.LegNUM).ToList();

            }
           
            // Test Data
            RetObj.Result = pfViewModel.PostflightLegs.Where(l => l.IsDeleted == false).OrderBy(o => o.LegNUM).ToList();

            return RetObj;
        }

        static PostflightLogViewModel SetPostflightLegExpenses(PostflightMain Trip, PostflightLogViewModel pfViewModel)
        {
            if (Trip != null && Trip.PostflightExpenses != null && Trip.PostflightExpenses.Count() > 0)
            {
                foreach (PostflightExpense expense in Trip.PostflightExpenses.Where(exp => exp.IsDeleted == false))
                {
                    PostflightLegViewModel leg = pfViewModel.PostflightLegs.Where(l => l.POLegID == expense.POLegID).FirstOrDefault();
                    if (leg != null)
                    {
                        PostflightLegExpenseViewModel vmExpense = new PostflightLegExpenseViewModel();
                        vmExpense = (PostflightLegExpenseViewModel)vmExpense.InjectFrom(expense);
                        vmExpense.Airport = vmExpense.AirportID.HasValue ? DBCatalogsDataLoader.GetAirportInfo_FromAirportId(vmExpense.AirportID.Value) : new AirportViewModel();
                        vmExpense.Homebase = vmExpense.HomebaseID.HasValue ? DBCatalogsDataLoader.GetHomebaseInfo_FromHomebaseId(vmExpense.HomebaseID.Value) : new HomebaseViewModel();
                        vmExpense.FBO = vmExpense.AirportID.HasValue && vmExpense.FBOID.HasValue ? DBCatalogsDataLoader.GetFBOInfo_FromFBOId(vmExpense.AirportID.Value, vmExpense.FBOID.Value) : new FBOViewModel();
                        vmExpense.Account = vmExpense.AccountID.HasValue ? DBCatalogsDataLoader.GetAccountInfo_FromAccountId(vmExpense.AccountID.Value) : new AccountViewModel();
                        vmExpense.Crew = vmExpense.CrewID.HasValue ? DBCatalogsDataLoader.GetCrewInfo_FromCrewId(vmExpense.CrewID.Value) : new CrewViewModel();
                        vmExpense.FlightCategory = vmExpense.FlightCategoryID.HasValue ? DBCatalogsDataLoader.GetFlightCategoryInfo_FromId(vmExpense.FlightCategoryID.Value) : new FlightCatagoryViewModel();
                        vmExpense.PaymentType = vmExpense.PaymentTypeID.HasValue ? DBCatalogsDataLoader.GetPaymentTypeInfo_FromPaymentTypeId(vmExpense.PaymentTypeID.Value) : new PaymentTypeViewModel();
                        vmExpense.PayableVendor = vmExpense.PaymentVendorID.HasValue ? DBCatalogsDataLoader.GetPayableVendorInfo_FromVendorId(vmExpense.PaymentVendorID.Value) : new PayableVendorViewModel();
                        vmExpense.FuelLocator = vmExpense.FuelLocatorID.HasValue ? DBCatalogsDataLoader.GetFuelLocatorInfo_FromLocatorId(vmExpense.FuelLocatorID.Value) : new FuelLocatorViewModel();

                        vmExpense.Fleet = vmExpense.FleetID.HasValue ? DBCatalogsDataLoader.GetFleetInfo_FromFleetId(vmExpense.FleetID.Value) : new FleetViewModel();
                        foreach(PostflightNote pnote in expense.PostflightNotes)
                        {
                            PostflightNoteViewModel pnoteVM = new PostflightNoteViewModel();
                            pnoteVM = (PostflightNoteViewModel)pnoteVM.InjectFrom(pnote);
                            vmExpense.PostflightNotes.Add(pnoteVM);
                        }
                        leg.PostflightLegExpensesList.Add(vmExpense);
                    }
                }
            }

            //Keeping this to take care of row number manipulations in jqgrid
            foreach (PostflightLegViewModel leg in pfViewModel.PostflightLegs)
            {
                int rowNum = 1;
                leg.PostflightLegExpensesList.ForEach(exp =>
                {
                    exp.RowNumber = rowNum++;
                });
            }

            return pfViewModel;
        }

        #endregion

        #region Postflight Leg And Validations

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> InitializePostflightLegs(int legNum)
        {
            FSSOperationResult<Hashtable> RetObj = new FSSOperationResult<Hashtable>();
            Hashtable RetObject = new Hashtable();

            if (RetObj.IsAuthorized() == false)
            {
                return RetObj;
            }

            UserPrincipalViewModel userPrincipal = getUserPrincipal();
            PostflightLegViewModel CurrentLeg = new PostflightLegViewModel(userPrincipal);
            PostflightLogViewModel poViewModel = new PostflightLogViewModel();
            List<PostflightLegViewModel> Legs = new List<PostflightLegViewModel>();

            if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
            {
                poViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                if (poViewModel.PostflightLegs == null || poViewModel.PostflightLegs.Count <= 0)
                {
                    poViewModel.PostflightLegs.Add(CreateFirstLeg(poViewModel.PostflightMain));
                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = poViewModel;
                }
                else
                {
                    var firstLeg = poViewModel.PostflightLegs.FirstOrDefault(l => l.LegNUM == legNum && l.IsDeleted == false && l.State != TripEntityState.Deleted);
                    if (firstLeg != null)
                    {
                        if (firstLeg.LegNUM.HasValue && firstLeg.LegNUM.Value==1 && ( firstLeg.DepartICAOID == null || firstLeg.DepartICAOID == 0 || (firstLeg.State == TripEntityState.NoChange && poViewModel.PostflightMain.State == TripEntityState.Added)))
                        {
                            poViewModel.PostflightLegs.FirstOrDefault(l => l.LegNUM == 1 && l.IsDeleted == false && l.State != TripEntityState.Deleted).InjectFrom(CreateFirstLeg(poViewModel.PostflightMain));
                        }
                    }
                    else
                    {
                        poViewModel.PostflightLegs.Add(CreateFirstLeg(poViewModel.PostflightMain));
                    }
                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = poViewModel;
                }
                Legs = poViewModel.PostflightLegs.Where(l => l.IsDeleted == false && l.State != TripEntityState.Deleted).OrderBy(o => o.LegNUM).ToList();
                for (int i = 0; i < Legs.Count; i++)
                {
                    Legs[i] = SetPostflightLegProperty(Legs[i]);
                    Legs[i] = SetLegUserPrincipalSettings(Legs[i]);
                }
                CurrentLeg = LoadLegDetails(poViewModel.PostflightLegs.FirstOrDefault(l => l.IsDeleted == false && l.LegNUM == legNum && l.State != TripEntityState.Deleted));
                RetObject["CurrentLegData"] = CurrentLeg;
            }
             
            // Test Data
            RetObject["Legs"] = Legs;
            CurrentLeg = poViewModel.PostflightLegs.FirstOrDefault(l => l.IsDeleted == false && l.LegNUM == legNum && l.State != TripEntityState.Deleted);
            if (CurrentLeg != null && CurrentLeg.LegNUM > 0)
            {
                RetObject["CurrentLeg"] = CurrentLeg;
                poViewModel.CurrentLegNUM = CurrentLeg.LegNUM;
            }
            else
            {
                RetObject["CurrentLeg"] = Legs[0];
                poViewModel.CurrentLegNUM = Legs[0].LegNUM;
            }

            RetObject["IsHaveLeg"] = (Legs.Count > 0);
            RetObject["POEditMode"] = poViewModel.PostflightMain.POEditMode;

            RetObj.Success = true;
            RetObj.StatusCode = HttpStatusCode.OK;
            RetObj.Result = RetObject;

            return RetObj;
        }

        private static PostflightLegViewModel ApplyCopiedValuesToLeg(PostflightLegViewModel leg, string changeKey)
        {
            Dictionary<String, String> copyProperties = new Dictionary<string, string>();
            if (HttpContext.Current.Session["POCopyProperties"] != null)
                copyProperties = (Dictionary<String, String>)HttpContext.Current.Session["POCopyProperties"];

            foreach (var key in copyProperties.Keys)
            {
                long departmentId = 0;
                switch (key.ToUpper())
                {
                    case "FLIGHTNUMBER":
                        leg.FlightNum = copyProperties[key];
                        break;
                    case "REQUESTOR":
                        long requestorId = 0;
                        long.TryParse(copyProperties[key], out requestorId);
                        if (changeKey == key)
                        {
                            leg.PassengerRequestorID = requestorId;
                            leg.Passenger = new PostflightLegPassengerViewModel();
                        }
                        break;
                    case "ACCOUNT":
                        long accountId = 0;
                        long.TryParse(copyProperties[key], out accountId);
                        if (changeKey == key)
                        {
                            leg.AccountID = accountId;
                            leg.Account = new AccountViewModel();
                        }
                        break;
                    case "DEPARTMENT":
                        long.TryParse(copyProperties[key], out departmentId);
                        if (changeKey == key)
                        {
                            leg.DepartmentID = departmentId;
                            leg.Department = new DepartmentViewModel();
                        }
                        break;
                    case "AUTHORIZATION":
                        long.TryParse(copyProperties["DEPARTMENT"], out departmentId);
                        long authorizationId = 0;
                        long.TryParse(copyProperties[key], out authorizationId);
                        if (changeKey == key)
                        {
                            leg.DepartmentID = departmentId;
                            leg.AuthorizationID = authorizationId;
                            leg.Department = new DepartmentViewModel();
                            leg.DepartmentAuthorization = new DepartmentAuthorizationViewModel();
                        }
                        break;
                    case "DESCRIPTION":
                        leg.FlightPurpose = copyProperties[key];
                        break;
                }
            }
            return leg;
        }

        private static PostflightLegViewModel SetPostflightLegProperty(PostflightLegViewModel leg)
        {
            if (leg.DelayType == null || leg.DelayType.DelayTypeID == 0)
            {
                leg.DelayType = leg.DelayTypeID.HasValue ? DBCatalogsDataLoader.GetDelayTypeInfo_FromFilter(leg.DelayTypeID.Value, string.Empty) : new PostflightLegDelayTypeViewModel();
            }
            if (leg.CrewDutyRule == null || leg.CrewDutyRule.CrewDutyRulesID == 0)
            {
                leg.CrewDutyRule = string.IsNullOrEmpty(leg.CrewCurrency) == false ? DBCatalogsDataLoader.GetCrewDutyRulesInfo_FromFilter(0, leg.CrewCurrency.ToUpper().Trim()) : new CrewDutyRuleViewModel();
                if (leg.CrewDutyRule != null && leg.CrewDutyRule.CrewDutyRulesID > 0)
                {
                    leg.CrewDutyRulesID = leg.CrewDutyRule.CrewDutyRulesID;
                    leg.FedAviationRegNum = string.IsNullOrEmpty(leg.FedAviationRegNum) ? leg.CrewDutyRule.FedAviatRegNum.Trim() : leg.FedAviationRegNum.Trim();
                }
            }
            if (leg.Account == null || leg.Account.AccountID == 0)
            {
                leg.Account = leg.AccountID.HasValue ? DBCatalogsDataLoader.GetAccountInfo_FromAccountId(leg.AccountID.Value) : new AccountViewModel();
            }
            if (leg.Client == null || leg.Client.ClientID == 0)
            {
                leg.Client = leg.ClientID.HasValue ? DBCatalogsDataLoader.GetClientInfo_FromClientID(leg.ClientID.Value) : new ClientViewModel();
            }
            if (leg.Department == null || leg.Department.DepartmentID == 0)
            {
                leg.Department = leg.DepartmentID.HasValue ? DBCatalogsDataLoader.GetDepartmentByIDorCD(leg.DepartmentID.Value, string.Empty) : new DepartmentViewModel();
            }
            if (leg.Department != null && (leg.DepartmentAuthorization == null || leg.DepartmentAuthorization.AuthorizationID == 0))
            {
                leg.DepartmentAuthorization = leg.AuthorizationID.HasValue ? DBCatalogsDataLoader.GetDepartmentAuthorizationByDepIDnAuthorizationCDorID(string.Empty, leg.AuthorizationID.Value, leg.Department.DepartmentID) : new DepartmentAuthorizationViewModel();
            }
            if (leg.Passenger == null || leg.Passenger.PassengerRequestorID == 0)
            {
                leg.Passenger = leg.PassengerRequestorID.HasValue ? DBCatalogsDataLoader.GetPassengerReqestorInfo_FromPassengerReqID(leg.PassengerRequestorID.Value) : new PostflightLegPassengerViewModel();
            }
            if (leg.FlightCatagory == null || leg.FlightCatagory.FlightCategoryID == 0)
            {
                leg.FlightCatagory = leg.FlightCategoryID.HasValue ? DBCatalogsDataLoader.GetFlightCategoryInfo_FromId(leg.FlightCategoryID.Value) : new FlightCatagoryViewModel();
            }
            return leg;
        }

        public static PostflightLegViewModel CreateFirstLeg(PostflightMainViewModel poMainVM)
        {
            PostflightLegViewModel customleg = new PostflightLegViewModel();
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();
            AirportViewModel DepAirport = DBCatalogsDataLoader.GetAirportInfo_FromICAO(poMainVM.Homebase.HomebaseCD);
            customleg = SetLegUserPrincipalSettings(customleg);
            customleg.LegNUM = 1;
            customleg.DepartureAirport = DepAirport;
            customleg.DepartICAOID = DepAirport.AirportID;
            //customleg.IsPrivate = pfMain.IsPrivate;
            customleg.ArrivalAirport = new AirportViewModel();
            customleg = SetDefaultDetails(customleg, poMainVM);
            customleg.IsDeleted = false;
            customleg.State = TripEntityState.Added;
            if (UserPrincipal._FuelBurnKiloLBSGallon != null)
            {
                customleg.FuelBurn = UserPrincipal._FuelBurnKiloLBSGallon;
            }
            if (customleg.FuelBurn == null || customleg.FuelBurn == 0)
                customleg.FuelBurn = 1;
           
            if  (string.IsNullOrEmpty(customleg.FedAviationRegNum))
                customleg.FedAviationRegNum = "91";

            customleg.DutyTYPE = 1;
            customleg.TechLog = poMainVM.TechLog;
            //customleg = ApplyCopiedValuesToLeg(customleg,"");
            customleg = SetPostflightLegProperty(customleg);
            return customleg;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> AddNewLeg(int LegNUM, PostflightLegViewModel CurrentLeg)
        {
            FSSOperationResult<Hashtable> RetObj = new FSSOperationResult<Hashtable>();
            Hashtable RetObject = new Hashtable();
            PostflightLegViewModel leg = new PostflightLegViewModel();

            if (RetObj.IsAuthorized() == false)
            {
                return RetObj;
            }
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();

            if (IsAuthorized(Permission.Preflight.AddPreflightLeg))
            {
                PostflightLogViewModel poViewModel = new PostflightLogViewModel();
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    poViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                    poViewModel.PostflightLegs.FirstOrDefault(p => p.LegNUM == CurrentLeg.LegNUM && p.IsDeleted == false && p.State != TripEntityState.Deleted).InjectFrom(CurrentLeg);
                    PostFlightValidator.ReCalculateCrewDutyHour();
                    PostflightLegViewModel LastLeg = poViewModel.PostflightLegs.OrderByDescending(p => p.LegNUM).FirstOrDefault(l => l.IsDeleted == false && l.State != TripEntityState.Deleted);
                    leg = SetLegUserPrincipalSettings(leg);
                    leg.LegNUM = GetANewLegNum("ADD");
                    leg.DepartureAirport = (AirportViewModel)new AirportViewModel().InjectFrom(LastLeg.ArrivalAirport);
                    leg.DepartICAOID = LastLeg.ArriveICAOID;
                    leg.ArrivalAirport = new AirportViewModel();
                    leg = SetDefaultDetails(leg, poViewModel.PostflightMain);
                    if (UserPrincipal._FuelBurnKiloLBSGallon != null)
                    {
                        leg.FuelBurn = UserPrincipal._FuelBurnKiloLBSGallon;
                    }
                    leg.DutyTYPE = 1;
                    leg.IsDeleted = false;
                    leg.State = TripEntityState.Added;

                    // Check if Remove End Duty checked or not. If Yes, remove end duty mark and deduct duty hours from previous legs.
                    if (UserPrincipal._IsEndDutyClient == true)
                    {
                        decimal dutyEnd = 0;
                        if (LastLeg != null)
                        {
                            if (LastLeg.CrewCurrency != null)
                            {
                                var crewDutyRule = DBCatalogsDataLoader.GetCrewDutyRulesInfo_FromFilter(0, LastLeg.CrewCurrency.Trim());
                                if (crewDutyRule != null)
                                    dutyEnd = Convert.ToDecimal(crewDutyRule.DutyDayEndTM);
                            }
                            LastLeg.IsDutyEnd = false;
                            if (LastLeg.DutyHrs != null)
                                LastLeg.DutyHrs = (LastLeg.DutyHrs - dutyEnd); 
                        }

                        leg.IsDutyEnd = true;
                    }

                    leg = CopyPreviousLegToNewLeg(LastLeg, leg);

                    if (string.IsNullOrEmpty(leg.FedAviationRegNum))
                        leg.FedAviationRegNum = "91";

                    //leg = ApplyCopiedValuesToLeg(leg,"");
                    leg = SetPostflightLegProperty(leg);
                    leg = AddPassengersInLeg(leg);
                    poViewModel.PostflightLegs.Add(leg);
                    RetObject["NewLeg"] = leg;
                    RetObject["Legs"] = poViewModel.PostflightLegs;
                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = poViewModel;

                    PostFlightValidator.ReCalculateLegDutyHour();

                    RetObj.Success = true;
                    RetObj.StatusCode = HttpStatusCode.OK;
                    RetObj.Result = RetObject;
                }
            }
            return RetObj;
        }

        private static PostflightLegViewModel AddPassengersInLeg(PostflightLegViewModel leg)
        {
            PostflightPaxLite paxlite = new PostflightPaxLite();
            var paxlist = paxlite.GetPaxListofGrid().Where(p=>p.State != TripEntityState.Deleted).ToList();
            if (leg.PostflightLegPassengers == null)
                leg.PostflightLegPassengers = new List<PostflightLegPassengerViewModel>();
            foreach (var pax in paxlist)
            {
                var newPax = (PostflightLegPassengerViewModel)new PostflightLegPassengerViewModel().InjectFrom(pax);
                newPax.PostflightPassengerListID=0;
                newPax.FlightPurposeID=null;
                leg.PostflightLegPassengers.Add(newPax);
            }
            return leg;

        }

        private static PostflightLegViewModel AddCrewInLeg(PostflightLegViewModel leg, PostflightLegViewModel currentleg)
        {
            if (leg.PostflightLegCrews == null)
                leg.PostflightLegCrews = new List<PostflightLegCrewViewModel>();
            foreach (var crew in currentleg.PostflightLegCrews)
            {
                var newCrew = (PostflightLegCrewViewModel)new PostflightLegCrewViewModel().InjectFrom(crew);
                newCrew.State = TripEntityState.Added;
                newCrew.PostflightCrewListID = 0;
                //newCrew.DutyTYPE = null;
                leg.PostflightLegCrews.Add(newCrew);
            }
            return leg;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> InsertNewLeg(int CurrentLegNum, PostflightLegViewModel CurrentLeg, bool isCopyCrew)
        {
            FSSOperationResult<Hashtable> RetObj = new FSSOperationResult<Hashtable>();
            Hashtable RetObject = new Hashtable();
            PostflightLegViewModel leg = new PostflightLegViewModel();

            if (RetObj.IsAuthorized() == false)
            {
                return RetObj;
            }
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();

            if (IsAuthorized(Permission.Preflight.AddPreflightLeg))
            {
                PostflightLogViewModel poViewModel = new PostflightLogViewModel();
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    poViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                    
                    poViewModel.PostflightLegs.FirstOrDefault(p => p.LegNUM == CurrentLeg.LegNUM && p.IsDeleted == false && p.State != TripEntityState.Deleted).InjectFrom(CurrentLeg);

                    
                    if (CurrentLeg == null || CurrentLeg.DepartICAOID.HasValue == false || CurrentLeg.ArriveICAOID.HasValue == false)
                    {
                        RetObj.Success = false;
                        RetObj.StatusCode = HttpStatusCode.OK;
                    }


                    PostFlightValidator.ReCalculateCrewDutyHour();
                    leg = SetLegUserPrincipalSettings(leg);
                    leg.LegNUM = GetANewLegNum("INSERT", Convert.ToString(CurrentLegNum));
                    if (leg.LegNUM > 1)
                    {
                        PostflightLegViewModel prevLeg = poViewModel.PostflightLegs.FirstOrDefault(p => p.LegNUM == leg.LegNUM -1 && p.IsDeleted == false && p.State != TripEntityState.Deleted);
                        leg.DepartureAirport = (AirportViewModel)new AirportViewModel().InjectFrom(prevLeg.ArrivalAirport);
                        leg.DepartICAOID = prevLeg.ArriveICAOID;
                    }
                    else
                    {
                        leg.ArrivalAirport = (AirportViewModel)new AirportViewModel().InjectFrom(CurrentLeg.DepartureAirport);
                        leg.ArriveICAOID = CurrentLeg.DepartICAOID;
                    }
                    //leg.IsPrivate = pfViewModel.PostflightMain.IsPrivate;
                    leg = SetDefaultDetails(leg, poViewModel.PostflightMain);
                    if (UserPrincipal._FuelBurnKiloLBSGallon != null)
                    {
                        leg.FuelBurn = UserPrincipal._FuelBurnKiloLBSGallon;
                    }
                    //CrewDutyRules Setting
                    if (UserPrincipal._CrewDutyID == null && leg.CrewDutyRulesID != null)
                    {
                        leg.CrewDutyRulesID = poViewModel.PostflightLegs[Convert.ToInt16(CurrentLegNum) - 1].CrewDutyRulesID;
                    }

                    if (leg.CrewDutyRule == null)
                    {
                        leg.CrewDutyRule = leg.CrewDutyRulesID.HasValue ? DBCatalogsDataLoader.GetCrewDutyRulesInfo_FromFilter(leg.CrewDutyRulesID.Value, string.Empty) : new CrewDutyRuleViewModel();
                        if (leg.CrewDutyRule != null && leg.CrewDutyRule.CrewDutyRulesID > 0)
                        {
                            leg.CrewCurrency = leg.CrewDutyRule.CrewDutyRuleCD;
                            leg.FedAviationRegNum = leg.CrewDutyRule.FedAviatRegNum.Trim();
                        }
                    }

                    if (leg.LegNUM > 1)
                    {
                        PostflightLegViewModel prevLeg = poViewModel.PostflightLegs.FirstOrDefault(p => p.LegNUM == leg.LegNUM - 1 && p.IsDeleted == false && p.State != TripEntityState.Deleted);
                        leg = CopyPreviousLegToNewLeg(prevLeg, leg);
                    }
                    else
                        leg = CopyPreviousLegToNewLeg(CurrentLeg, leg);

                    if (string.IsNullOrEmpty(leg.FedAviationRegNum))
                        leg.FedAviationRegNum = "91";

                    leg.DutyTYPE = 1;
                    leg = AddPassengersInLeg(leg);

                    if (isCopyCrew)
                    {
                        leg = AddCrewInLeg(leg, CurrentLeg);
                    }

                    leg.IsDeleted = false;
                    leg.State = TripEntityState.Added;
                    poViewModel.PostflightLegs.Insert(Convert.ToInt16(CurrentLegNum) - 1, leg);
                    RetObject["NewLeg"] = leg;
                    RetObject["Legs"] = poViewModel.PostflightLegs.Where(l => l.IsDeleted == false && l.State != TripEntityState.Deleted).OrderBy(l => l.LegNUM);

                    RetObj.Success = true;
                    RetObj.StatusCode = HttpStatusCode.OK;
                    RetObj.Result = RetObject;
                }
            }
            return RetObj;
        }

        private static PostflightLegViewModel CopyPreviousLegToNewLeg(PostflightLegViewModel LastLeg, PostflightLegViewModel leg)
        {
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();

            if (LastLeg != null)
            {
                if (LastLeg.TechLog != null)
                    leg.TechLog = LastLeg.TechLog;

                if (LastLeg.FlightPurpose != null)
                    leg.FlightPurpose = LastLeg.FlightPurpose;

                if (LastLeg.FlightNum != null)
                    leg.FlightNum = LastLeg.FlightNum;

                if (LastLeg.ClientID != null && LastLeg.ClientID > 0)
                {
                    leg.ClientID = LastLeg.ClientID;
                    if (LastLeg.Client != null)
                    {
                        leg.Client = LastLeg.Client;
                    }
                }
                if (LastLeg.PassengerRequestorID != null && LastLeg.PassengerRequestorID > 0)
                {
                    leg.PassengerRequestorID = LastLeg.PassengerRequestorID;
                    if (LastLeg.Passenger != null)
                    {
                        leg.Passenger = LastLeg.Passenger;
                    }
                }
                if (LastLeg.DepartmentID != null && LastLeg.DepartmentID > 0)
                {
                    leg.DepartmentID = LastLeg.DepartmentID;
                    if (LastLeg.Department != null)
                    {
                        leg.Department = LastLeg.Department;
                    }
                }
                if (LastLeg.AuthorizationID != null && LastLeg.AuthorizationID > 0)
                {
                    leg.AuthorizationID = LastLeg.AuthorizationID;
                    if (LastLeg.DepartmentAuthorization != null)
                    {
                        leg.DepartmentAuthorization = LastLeg.DepartmentAuthorization;
                    }
                }

                if (LastLeg.FlightCategoryID != null && LastLeg.FlightCategoryID > 0)
                {
                    leg.FlightCategoryID = LastLeg.FlightCategoryID;
                    if (LastLeg.FlightCatagory != null)
                    {
                        leg.FlightCatagory = LastLeg.FlightCatagory;
                    }
                }
                if (LastLeg.AccountID != null && LastLeg.AccountID > 0)
                {
                    leg.AccountID = LastLeg.AccountID;
                    if (LastLeg.Account != null)
                    {
                        leg.Account = LastLeg.Account;
                    }
                }

                //Add Ground Time based on Company Profile Settings in Preflight Tab (Domestic/International)
                DateTime? _prevInboundDTTM = null;
                decimal groundTime = 0;
                if (LastLeg.InboundDTTM != null)
                    _prevInboundDTTM = (DateTime)LastLeg.InboundDTTM;

                if (_prevInboundDTTM != null)
                {
                    if (LastLeg.DutyTYPE == WebConstants.LegDutyType.Domestic && UserPrincipal._GroundTM != null)
                        groundTime = (decimal)UserPrincipal._GroundTM;
                    else if (LastLeg.DutyTYPE == WebConstants.LegDutyType.International && UserPrincipal._GroundTMIntl != null)
                        groundTime = (decimal)UserPrincipal._GroundTMIntl;

                    leg.ScheduledTM = LastLeg.InboundDTTM.Value.AddHours(Convert.ToDouble(groundTime));
                }
          
                if ((UserPrincipal._CrewDutyID == null || UserPrincipal._CrewDutyID == 0) && (leg.CrewDutyRulesID == null || leg.CrewDutyRulesID == 0))
                {
                    leg.CrewDutyRulesID = LastLeg.CrewDutyRulesID ?? 0;
                }
                if (leg.CrewDutyRule == null || leg.CrewDutyRule.CrewDutyRulesID == 0)
                {
                    leg.CrewDutyRule = leg.CrewDutyRulesID.HasValue ? DBCatalogsDataLoader.GetCrewDutyRulesInfo_FromFilter(leg.CrewDutyRulesID.Value, string.Empty) : new CrewDutyRuleViewModel();
                    if (leg.CrewDutyRule != null && leg.CrewDutyRule.CrewDutyRulesID > 0)
                    {
                        leg.CrewCurrency = leg.CrewDutyRule.CrewDutyRuleCD.Trim();
                        leg.FedAviationRegNum = leg.CrewDutyRule.FedAviatRegNum.Trim();
                    }
                }
            }
            return leg;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> DeleteLeg(int CurrentLegNum)
        {
            FSSOperationResult<Hashtable> RetObj = new FSSOperationResult<Hashtable>();
            Hashtable RetObject = new Hashtable();
            PostflightLegViewModel leg = new PostflightLegViewModel();
            if (RetObj.IsAuthorized() == false)
            {
                return RetObj;
            }

            if (IsAuthorized(Permission.Preflight.DeletePreflightLeg))
            {
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    var poViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                    if (poViewModel.PostflightLegs != null && poViewModel.PostflightLegs.Where(p => p.IsDeleted == false && p.State != TripEntityState.Deleted).Count() > 1)
                    {
                        var objLeg = poViewModel.PostflightLegs.FirstOrDefault(p => p.LegNUM == CurrentLegNum && p.IsDeleted == false && p.State != TripEntityState.Deleted);
                        if (objLeg != null)
                        {
                            if (objLeg.POLegID != 0)
                            {
                                objLeg.IsDeleted = true;
                                objLeg.State = TripEntityState.Deleted;
                            }
                            else
                            {
                                PostflightLegViewModel legToBeRemoved = poViewModel.PostflightLegs.FirstOrDefault(l => l.LegNUM == CurrentLegNum);
                                if (legToBeRemoved != null)
                                {
                                    poViewModel.PostflightLegs.Remove(legToBeRemoved);
                                }
                            }
                        }
                        var Legs = poViewModel.PostflightLegs.Where(x => x.LegNUM >= CurrentLegNum && x.IsDeleted == false && x.State != TripEntityState.Deleted).OrderBy(x => x.LegNUM).ToList();
                        foreach (PostflightLegViewModel Leg in Legs)
                        {
                            Leg.LegNUM = Leg.LegNUM - 1;
                            if (Leg.POLegID > 0)
                                Leg.State = TripEntityState.Modified;
                        }

                        leg = poViewModel.PostflightLegs.FirstOrDefault(x => x.LegNUM == CurrentLegNum && x.IsDeleted == false && x.State != TripEntityState.Deleted);
                        if (leg == null)
                            leg = poViewModel.PostflightLegs.OrderByDescending(l => l.LegNUM).FirstOrDefault(x => x.IsDeleted == false && x.State != TripEntityState.Deleted);
                        PostFlightValidator.ReCalculateCrewDutyHour();
                        PostFlightValidator.ReCalculateLegDutyHour();
                        poViewModel.CurrentLegNUM = leg.LegNUM;
                        HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = poViewModel;
                        RetObject["NewLeg"] = leg;
                        RetObject["Legs"] = poViewModel.PostflightLegs.Where(l => l.IsDeleted == false && l.State != TripEntityState.Deleted);
                        RetObj.Success = true;
                    }
                    else
                    {
                        RetObj.Success = false;
                        RetObj.ErrorsList.Add(Microsoft.Security.Application.Encoder.HtmlEncode("Postflight Log must contain at least one leg")); 
                    }
                }
            }
            
            RetObj.StatusCode = HttpStatusCode.OK;
            RetObj.Result = RetObject;
            return RetObj;
        }

        private static long GetANewLegNum(string Mode, string SelectedLegNum = "")
        {
            int legNum = 0;
            int.TryParse(SelectedLegNum, out legNum);
            PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
            {
                pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                switch (Mode)
                {
                    case "ADD":
                        legNum = pfViewModel.PostflightLegs.Count(l => l.IsDeleted == false && l.State != TripEntityState.Deleted) + 1;
                        break;
                    case "INSERT":
                        List<PostflightLegViewModel> PrefLegs = new List<PostflightLegViewModel>();
                        PrefLegs = pfViewModel.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted && x.LegNUM >= legNum).OrderBy(x => x.LegNUM).ToList();
                        if (PrefLegs.Count > 0)
                        {
                            foreach (PostflightLegViewModel Leg in PrefLegs)
                            {
                                Leg.LegNUM = Leg.LegNUM + 1;
                                if (Leg.POLegID > 0)
                                    Leg.State = TripEntityState.Modified;
                            }
                        }
                        break;
                    case "DELETE":
                        int previndex = legNum - 1;

                        break;
                }
            }
            HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
            return legNum;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightLegViewModel> SaveCurrentLegDataAndReturn(int LegNUM, PostflightLegViewModel CurrentLegData)
        {
            FSSOperationResult<PostflightLegViewModel> RetObj = new FSSOperationResult<PostflightLegViewModel>();
            if (RetObj.IsAuthorized() == false)
            {
                return RetObj;
            }
            UserPrincipalViewModel userPrincipalVM = getUserPrincipal();
            CurrentLegData = SetLegUserPrincipalSettings(CurrentLegData);
            PostflightLegViewModel leg = new PostflightLegViewModel();
            PostflightLogViewModel pofViewModel = new PostflightLogViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
            {
                pofViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                var objCurrLeg = pofViewModel.PostflightLegs.FirstOrDefault(p => p.LegNUM == CurrentLegData.LegNUM && p.IsDeleted == false && p.State != TripEntityState.Deleted);
                if (objCurrLeg != null)
                {
                    var paxListOFLeg = new List<PostflightLegPassengerViewModel>();  // passenger list dont need to update while Leg page save call. Date vales are getting null from clientside
                    if (objCurrLeg.PostflightLegPassengers != null)
                        paxListOFLeg = MiscUtils.Clone<List<PostflightLegPassengerViewModel>>(objCurrLeg.PostflightLegPassengers);
                    objCurrLeg = (PostflightLegViewModel)objCurrLeg.InjectFrom(CurrentLegData);
                    objCurrLeg.PostflightLegPassengers = paxListOFLeg; // Resetting passenger list.
                }
                leg = pofViewModel.PostflightLegs.FirstOrDefault(l => l.LegNUM == LegNUM && l.IsDeleted == false && l.State != TripEntityState.Deleted);
                leg = SetLegUserPrincipalSettings(leg);
                HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pofViewModel;
            }

            RetObj.Success = true;
            RetObj.StatusCode = HttpStatusCode.OK;
            RetObj.Result = leg;

            return RetObj;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static void UpdateCurrentLegEntityState(int legNum)
        {
            var pofViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            if (pofViewModel != null && pofViewModel.PostflightLegs != null && pofViewModel.PostflightLegs.Count > 0)
            {
                var objLeg = pofViewModel.PostflightLegs.FirstOrDefault(p => p.LegNUM == legNum && p.IsDeleted == false && p.State != TripEntityState.Deleted);
                if (objLeg != null)
                {
                    if (pofViewModel.POLogID > 0)
                    {
                        if (objLeg.POLogID > 0)
                            objLeg.State = (objLeg.State == TripEntityState.Deleted) ? TripEntityState.Deleted : TripEntityState.Modified;
                        else
                            objLeg.State = TripEntityState.Added;

                        if (objLeg.State != TripEntityState.Deleted && objLeg.State != TripEntityState.Added)
                            objLeg.State = TripEntityState.Modified;
                    }
                    else
                        objLeg.State = TripEntityState.Added;
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pofViewModel;
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightLegViewModel> SavePrevLegAndReturnCurrentLegData(int LegNUM, PostflightLegViewModel PreviousLegData)
        {
            FSSOperationResult<PostflightLegViewModel> RetObj = new FSSOperationResult<PostflightLegViewModel>();
            if (RetObj.IsAuthorized() == false)
            {
                return RetObj;
            }

            PostflightLegViewModel leg = new PostflightLegViewModel();
            PostflightLogViewModel pofViewModel = new PostflightLogViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
            {
                pofViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                if (pofViewModel.PostflightMain.POEditMode)
                {
                    var preLeg = pofViewModel.PostflightLegs.FirstOrDefault(p => p.LegNUM == PreviousLegData.LegNUM && p.IsDeleted == false && p.State != TripEntityState.Deleted);
                    if (preLeg != null)
                    {
                        var paxListOFLeg = new List<PostflightLegPassengerViewModel>();  // passenger list dont need to update while Leg page save call. Date vales are getting null from clientside
                        if(preLeg.PostflightLegPassengers!=null)
                            paxListOFLeg = MiscUtils.Clone<List<PostflightLegPassengerViewModel>>(preLeg.PostflightLegPassengers);
                        preLeg = (PostflightLegViewModel)preLeg.InjectFrom(PreviousLegData);
                        preLeg.PostflightLegPassengers = paxListOFLeg; // Resetting passenger list.
                    }
                }

                leg = pofViewModel.PostflightLegs.FirstOrDefault(l => l.LegNUM == LegNUM && l.IsDeleted == false && l.State != TripEntityState.Deleted);
                leg = SetLegUserPrincipalSettings(leg);
                if (leg.CrewDutyRule == null || leg.CrewDutyRule.CrewDutyRulesID == 0)
                {
                    leg.CrewDutyRule = leg.CrewDutyRulesID.HasValue ? DBCatalogsDataLoader.GetCrewDutyRulesInfo_FromFilter(leg.CrewDutyRulesID.Value, string.Empty) : new CrewDutyRuleViewModel();
                    if (leg.CrewDutyRule != null && leg.CrewDutyRule.CrewDutyRulesID > 0)
                    {
                        leg.CrewCurrency = leg.CrewDutyRule.CrewDutyRuleCD;
                        leg.FedAviationRegNum = leg.CrewDutyRule.FedAviatRegNum.Trim();
                    }
                }

                pofViewModel.CurrentLegNUM = LegNUM;

                if(pofViewModel.PostflightMain.POEditMode)
                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pofViewModel;

            }

            RetObj.Success = true;
            RetObj.StatusCode = HttpStatusCode.OK;
            RetObj.Result = leg;

            return RetObj;
        }

        #endregion

        #region Expenses
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        ///Summary
        ///This method will return only the Legs without its child entities to avoid unnecessary amount of data being sent down
        public static FSSOperationResult<Hashtable> GetLegsLiteList()
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<Hashtable>>(() =>
            {
                FSSOperationResult<Hashtable> retObj = new FSSOperationResult<Hashtable>();

                PostflightLogViewModel pfViewModel = new PostflightLogViewModel();

                if (!retObj.IsAuthorized())
                {
                    return retObj;
                }
                retObj.StatusCode = HttpStatusCode.OK;
                retObj.Success = true;
                List<PostflightLegViewModel> legs = new List<PostflightLegViewModel>();
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

                    if (pfViewModel.PostflightLegs == null || pfViewModel.PostflightLegs.Count <= 0)
                    {
                        PostflightLegViewModel leg = new PostflightLegViewModel();
                        pfViewModel.PostflightLegs.Add(CreateFirstLeg(pfViewModel.PostflightMain));
                        HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                    }
                    foreach (PostflightLegViewModel leg in pfViewModel.PostflightLegs.Where(l => l.IsDeleted == false && l.State != TripEntityState.Deleted).OrderBy(o => o.LegNUM).ToList())
                    {
                        PostflightLegViewModel legLite = (PostflightLegViewModel)new PostflightLegViewModel().InjectFrom(leg);
                        legLite.PostflightLegPassengers = new List<PostflightLegPassengerViewModel>();
                        legLite.PostflightLegCrews = new List<PostflightLegCrewViewModel>();
                        legLite.PostflightLegExpensesList = new List<PostflightLegExpenseViewModel>();
                        legs.Add(legLite);
                    }
                    Hashtable legHashTable = new Hashtable();
                    legHashTable["Legs"] = legs;

                    PostflightLegViewModel currentLeg = legs.FirstOrDefault(l => l.IsDeleted == false && l.LegNUM == pfViewModel.CurrentLegNUM  && l.State != TripEntityState.Deleted);
                    if (currentLeg != null && currentLeg.LegNUM > 0)
                        legHashTable["CurrentLeg"] = currentLeg;
                
                    retObj.Result = legHashTable;
                }

                return retObj;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable GetAllPostflightLegExpensesForLeg(int LegNum)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Hashtable>(() =>
            {
                Hashtable data = new Hashtable();
                List<PostflightLegExpenseViewModel> returnResult = new List<PostflightLegExpenseViewModel>();

                PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
                UserPrincipalViewModel UserPrincipal = getUserPrincipal();

                if (UserPrincipal == null)
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
                    return data;
                }

                if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                    PostflightLegViewModel leg = pfViewModel.PostflightLegs.Where(l => l.IsDeleted == false && l.State != TripEntityState.Deleted && l.LegNUM == LegNum).FirstOrDefault();
                    if (leg != null)
                    {
                        returnResult.AddRange(leg.PostflightLegExpensesList.Where(ex => ex.IsDeleted == false && ex.State != TripEntityState.Deleted));
                    }

                    pfViewModel.CurrentLegNUM = LegNum;
                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                }
                data["meta"] = new PagingMetaData() { Page = 1, Size = returnResult.Count(), total_items = returnResult.Count() };
                data["results"] = returnResult;
                return data;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightLegExpenseViewModel> GetPostflightLegExpenses(int legNum, int rowNumber)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<PostflightLegExpenseViewModel>>(() =>
            {
                FSSOperationResult<PostflightLegExpenseViewModel> data = new FSSOperationResult<PostflightLegExpenseViewModel>();
                List<PostflightLegExpenseViewModel> returnResult = new List<PostflightLegExpenseViewModel>();

                PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
                PostflightNote note = new PostflightNote();
                if (!data.IsAuthorized())
                {
                    return data;
                }
                data.StatusCode = HttpStatusCode.OK;

                data.Success = false;
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                    PostflightLegViewModel leg = pfViewModel.PostflightLegs.Where(l => l.IsDeleted == false && l.State != TripEntityState.Deleted && l.LegNUM == legNum).FirstOrDefault();
                    if (leg != null)
                    {
                        PostflightLegExpenseViewModel expense = leg.PostflightLegExpensesList.Where(ex => ex.RowNumber == rowNumber).FirstOrDefault();
                        if (expense != null)
                        {
                            data.Success = true;
                            if (expense.Homebase == null || expense.Homebase.HomebaseID <= 0)
                            {
                                expense.Homebase = expense.HomebaseID.HasValue ? DBCatalogsDataLoader.GetHomebaseInfo_FromHomebaseId(expense.HomebaseID.Value) : new HomebaseViewModel();
                            }
                            if((expense.Airport == null || String.IsNullOrWhiteSpace(expense.Airport.AirportName)) && expense.AirportID.HasValue  )
                            {
                                expense.Airport = expense.AirportID.HasValue ? DBCatalogsDataLoader.GetAirportInfo_FromAirportId(expense.AirportID.Value) : new AirportViewModel();
                            }
                            if(expense.FBO == null || expense.FBO.FBOID == 0)
                            {
                                expense.FBO = expense.AirportID.HasValue && expense.FBOID.HasValue ? DBCatalogsDataLoader.GetFBOInfo_FromFBOId(expense.AirportID.Value,expense.FBOID.Value) : new FBOViewModel();
                            }
                            if(expense.Account == null || expense.Account.AccountID <= 0)
                            {
                                expense.Account = expense.AccountID.HasValue ? DBCatalogsDataLoader.GetAccountInfo_FromAccountId(expense.AccountID.Value) : new AccountViewModel();
                            }
                            if(expense.Crew == null || expense.Crew.CrewID <= 0)
                            {
                                expense.Crew = expense.CrewID.HasValue ? DBCatalogsDataLoader.GetCrewInfo_FromCrewId(expense.CrewID.Value) : new CrewViewModel();
                            }
                            if(expense.Fleet == null || expense.Fleet.FleetID <= 0)
                            {
                                expense.Fleet = expense.FleetID.HasValue ?  DBCatalogsDataLoader.GetFleetInfo_FromFleetId(expense.FleetID.Value) : new FleetViewModel();
                            }
                            if(expense.FlightCategory == null || expense.FlightCategory.FlightCategoryID <= 0)
                            {
                                expense.FlightCategory = expense.FlightCategoryID.HasValue ? DBCatalogsDataLoader.GetFlightCategoryInfo_FromId(expense.FlightCategoryID.Value) : new FlightCatagoryViewModel();
                            }

                            if(expense.FuelLocator == null || expense.FuelLocator.FuelLocatorID <= 0)
                            {
                                expense.FuelLocator = expense.FuelLocatorID.HasValue ? DBCatalogsDataLoader.GetFuelLocatorInfo_FromLocatorId(expense.FuelLocatorID.Value) : new FuelLocatorViewModel();
                            }

                            if(expense.PayableVendor == null || expense.PayableVendor.VendorID <= 0)
                            {
                                expense.PayableVendor = expense.PaymentVendorID.HasValue ? DBCatalogsDataLoader.GetPayableVendorInfo_FromVendorId(expense.PaymentVendorID.Value) : new PayableVendorViewModel();
                            }

                            if(expense.PaymentType == null || expense.PaymentType.PaymentTypeID <= 0)
                            {
                                expense.PaymentType = expense.PaymentTypeID.HasValue ? DBCatalogsDataLoader.GetPaymentTypeInfo_FromPaymentTypeId(expense.PaymentTypeID.Value) : new PaymentTypeViewModel();
                            }

                            data.Result = expense;
                        }
                    }
                }
                return data;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightLegExpenseViewModel> AddNewExpense(int legNum)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<PostflightLegExpenseViewModel>>(() =>
            {
                FSSOperationResult<PostflightLegExpenseViewModel> retObj = new FSSOperationResult<PostflightLegExpenseViewModel>();
                PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
                UserPrincipalViewModel UserPrincipal = getUserPrincipal();

                if (!retObj.IsAuthorized())
                {
                    return retObj;
                }
                retObj.StatusCode = HttpStatusCode.OK;
                retObj.Success = true;
                List<PostflightLegViewModel> legs = new List<PostflightLegViewModel>();
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

                    if (pfViewModel.PostflightLegs != null && pfViewModel.PostflightLegs.Count > 0)
                    {
                        PostflightLegExpenseViewModel Expense = new PostflightLegExpenseViewModel();
                        Expense.InvoiceNUM = String.Empty;
                        decimal dc = 0.00M;
                        Expense.FuelQTY = dc;
                        Expense.UnitPrice = dc;
                        Expense.PostFuelPrice = dc;
                        Expense.ExpenseAMT = dc;
                        Expense.SaleTAX = dc;
                        Expense.SateTAX = dc;
                        Expense.FederalTAX = dc;
                        Expense.HomebaseID = pfViewModel.PostflightMain.HomebaseID;

                        if (pfViewModel.PostflightMain.Company != null && pfViewModel.PostflightMain.Company.HomebaseAirportID != null)
                        {
                            Expense.HomebaseID = pfViewModel.PostflightMain.Company.HomebaseID;
                            Expense.Homebase = DBCatalogsDataLoader.GetHomebaseInfo_FromHomebaseId(Expense.HomebaseID.Value);
                        }

                        Expense.FleetID = pfViewModel.PostflightMain.FleetID;
                        if (Expense.FleetID.HasValue)
                        {
                            Expense.Fleet = DBCatalogsDataLoader.GetFleetInfo_FromFleetId(Expense.FleetID.Value);
                        }

                        PostflightLegViewModel leg = pfViewModel.PostflightLegs.Where(l => l.IsDeleted == false && l.LegNUM == legNum && l.State != TripEntityState.Deleted).FirstOrDefault();
                        if (leg !=null && leg.DepartICAOID.HasValue)
                        {
                            Expense.AirportID = leg.DepartICAOID;
                            Expense.Airport = DBCatalogsDataLoader.GetAirportInfo_FromAirportId(leg.DepartICAOID.Value);

                            if (leg.DepatureFBO.HasValue)
                            {
                                Expense.FBOID = leg.DepatureFBO;
                                Expense.FBO = DBCatalogsDataLoader.GetFBOInfo_FromFBOId(leg.DepartICAOID.Value, leg.DepatureFBO.Value);
                            }
                            else
                            {
                                Expense.FBO = DBCatalogsDataLoader.GetFBOChoice_ForAirportId(leg.DepartICAOID.Value);
                                Expense.FBOID = Expense.FBO.FBOID;
                            }
                        }

                        if (leg.InboundDTTM.HasValue)
                            Expense.PurchaseDT = leg.InboundDTTM;
                        else
                            Expense.PurchaseDT = DateTime.Today;

                        if (leg.FlightCategoryID != null && leg.FlightCatagory != null)
                        {
                            Expense.FlightCategoryID = leg.FlightCategoryID;
                            Expense.FlightCategory = DBCatalogsDataLoader.GetFlightCategoryInfo_FromId(leg.FlightCategoryID.Value);
                        }

                        Expense.DispatchNUM = pfViewModel.PostflightMain.DispatchNum;
                        Expense.AccountPeriod = DateTime.Now.Date.Year.ToString().Substring(2, 2).ToString() + "" + DateTime.Now.Date.Month.ToString("d2");
                        Expense.RowNumber = leg.PostflightLegExpensesList.Count() + 1;

                        PostflightLegCrewViewModel pfCrewVM = leg.PostflightLegCrews.Where(cdt => cdt.DutyTYPE == "P").FirstOrDefault();
                        if(pfCrewVM != null)
                        {
                            Expense.CrewID = pfCrewVM.CrewID;
                            Expense.Crew = pfCrewVM.CrewID.HasValue ? DBCatalogsDataLoader.GetCrewInfo_FromCrewId(pfCrewVM.CrewID.Value) : new CrewViewModel();
                        }

                        if (UserPrincipal._FuelPurchase != null)
                            Expense.FuelPurchase = UserPrincipal._FuelPurchase;
                        Expense.RowNumber = leg.PostflightLegExpensesList.Count() + 1;

                        leg.PostflightLegExpensesList.Add(Expense);

                        HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                        retObj.Result = Expense;
                    }
                }
                return retObj;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> SavePostflightLegExpense(int legNum, PostflightLegExpenseViewModel legExpense)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<bool>>(() =>
            {
                FSSOperationResult<bool> retObj = new FSSOperationResult<bool>();
                PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
                UserPrincipalViewModel UserPrincipal = getUserPrincipal();

                if (!retObj.IsAuthorized())
                {
                    return retObj;
                }
                retObj.StatusCode = HttpStatusCode.OK;
                retObj.Success = true;
                List<PostflightLegViewModel> legs = new List<PostflightLegViewModel>();
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

                    if (pfViewModel.PostflightLegs != null && pfViewModel.PostflightLegs.Count > 0)
                    {
                        PostflightLegViewModel leg = pfViewModel.PostflightLegs.Where(l => l.IsDeleted == false && l.LegNUM == legNum && l.State != TripEntityState.Deleted).FirstOrDefault();
                        if (leg != null)
                        {
                            var existingExpense = leg.PostflightLegExpensesList.Where(exp => exp.RowNumber == legExpense.RowNumber).FirstOrDefault();
                            if (existingExpense != null)
                            {
                                existingExpense = (PostflightLegExpenseViewModel)existingExpense.InjectFrom(legExpense);
                                if(leg.POLegID > 0 && existingExpense.PostflightExpenseID > 0)
                                {
                                    existingExpense.State = TripEntityState.Modified;
                                    if(existingExpense.PostflightNote != null)
                                    {
                                        if (existingExpense.PostflightNote.PostflightNoteID > 0)
                                            existingExpense.PostflightNote.State = TripEntityState.Modified;
                                        else if (existingExpense.PostflightNote.PostflightNoteID == 0)
                                            existingExpense.PostflightNote.State = TripEntityState.Added;
                                    }
                                }
                                else if(leg.POLegID == 0 || existingExpense.PostflightExpenseID == 0)
                                {
                                    existingExpense.State = TripEntityState.Added;
                                    if (existingExpense.PostflightNote != null && !String.IsNullOrWhiteSpace(existingExpense.PostflightNote.Notes))
                                        existingExpense.PostflightNote.State = TripEntityState.Added;
                                }
                                existingExpense.PostflightNote = existingExpense.PostflightNote;//This makes sure the list is updated
                            }
                            else
                            {
                                legExpense.State = TripEntityState.Added;
                                leg.PostflightLegExpensesList.Add(legExpense);
                            }

                            HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                        }
                        retObj.Result = true;
                    }
                }
                return retObj;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> DeletePostflightLegExpense(int legNum, int expenseRowNumber)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<bool>>(() =>
            {
                FSSOperationResult<bool> retObj = new FSSOperationResult<bool>();

                PostflightLogViewModel pfViewModel = new PostflightLogViewModel();

                if (!retObj.IsAuthorized())
                {
                    return retObj;
                }
                retObj.StatusCode = HttpStatusCode.OK;
                retObj.Success = true;
                List<PostflightLegViewModel> legs = new List<PostflightLegViewModel>();
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    pfViewModel = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

                    if (pfViewModel.PostflightLegs != null && pfViewModel.PostflightLegs.Count > 0)
                    {
                        PostflightLegViewModel leg = pfViewModel.PostflightLegs.Where(l => l.IsDeleted == false && l.LegNUM == legNum && l.State != TripEntityState.Deleted).FirstOrDefault();
                        PostflightLegExpenseViewModel legExpense= leg.PostflightLegExpensesList.Where(pl => pl.RowNumber == expenseRowNumber).FirstOrDefault();
                        if (legExpense != null)
                        {
                            if (legExpense.PostflightExpenseID <= 0)
                                leg.PostflightLegExpensesList.Remove(legExpense);
                            else
                            {
                                legExpense.State = TripEntityState.Deleted;
                                legExpense.IsDeleted = true;
                            }
                        }
                        HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = pfViewModel;
                        retObj.Result = true;
                    }
                }
                return retObj;

            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable GetPaymentTypes(bool showActiveOnly)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Hashtable>(() =>
            {
                Hashtable data = new Hashtable();
                List<PaymentTypeViewModel> returnResult = new List<PaymentTypeViewModel>();

                UserPrincipalViewModel UserPrincipal = getUserPrincipal();

                if (UserPrincipal == null)
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
                    return data;
                }
                returnResult.AddRange(DBCatalogsDataLoader.GetPaymentTypeInfo_List(showActiveOnly));
                data["meta"] = new PagingMetaData() { Page = 1, Size = returnResult.Count(), total_items = returnResult.Count() };
                data["results"] = returnResult;
                return data;
            }, FlightPak.Common.Constants.Policy.UILayer);
            
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> DeletePaymentType(long paymentIdToDelete)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<bool>>(() =>
            {
                FSSOperationResult<bool> retObj = new FSSOperationResult<bool>();
                if (!retObj.IsAuthorized())
                {
                    return retObj;
                }

                PaymentTypeViewModel paymentTypeVM = DBCatalogsDataLoader.GetPaymentTypeInfo_FromPaymentTypeId(paymentIdToDelete);
                retObj = DBCatalogsDataLoader.DeletePaymentType(paymentTypeVM);
                return retObj;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable GetPayableVendors(bool showActiveOnly, bool homebaseOnly)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Hashtable>(() =>
            {
                Hashtable data = new Hashtable();
                List<PayableVendorViewModel> returnResult = new List<PayableVendorViewModel>();

                UserPrincipalViewModel userPrincipal = getUserPrincipal();

                if (userPrincipal == null)
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
                    return data;
                }
                String homebase = homebaseOnly ? userPrincipal._homeBase : String.Empty;
                returnResult.AddRange(DBCatalogsDataLoader.GetPayableVendorList(showActiveOnly, homebase));
                data["meta"] = new PagingMetaData() { Page = 1, Size = returnResult.Count(), total_items = returnResult.Count() };
                data["results"] = returnResult;
                return data;
            }, FlightPak.Common.Constants.Policy.UILayer);

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> DeletePayableVendor(long vendorIdToDelete)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<bool>>(() =>
            {
                FSSOperationResult<bool> retObj = new FSSOperationResult<bool>();
                if (!retObj.IsAuthorized())
                {
                    return retObj;
                }
                PayableVendorViewModel paymentTypeVM = DBCatalogsDataLoader.GetPayableVendorInfo_FromVendorId(vendorIdToDelete);
                retObj = DBCatalogsDataLoader.DeletePayableVendor(paymentTypeVM);
                return retObj;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable GetFuelLocators(bool showActiveOnly)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Hashtable>(() =>
            {
                Hashtable data = new Hashtable();
                List<FuelLocatorViewModel> returnResult = new List<FuelLocatorViewModel>();

                UserPrincipalViewModel userPrincipal = getUserPrincipal();

                if (userPrincipal == null)
                {
                    data["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
                    return data;
                }
                returnResult.AddRange(DBCatalogsDataLoader.GetFuelLocatorList(showActiveOnly));
                data["meta"] = new PagingMetaData() { Page = 1, Size = returnResult.Count(), total_items = returnResult.Count() };
                data["results"] = returnResult;
                return data;
            }, FlightPak.Common.Constants.Policy.UILayer);

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> DeleteFuelLocator(long FuelLocatorID)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<bool>>(() =>
            {
                FSSOperationResult<bool> retObj = new FSSOperationResult<bool>();
                if (!retObj.IsAuthorized())
                {
                    return retObj;
                }
                FuelLocatorViewModel fuelLocVM = DBCatalogsDataLoader.GetFuelLocatorInfo_FromLocatorId(FuelLocatorID);
                retObj= DBCatalogsDataLoader.DeleteFuelLocator(fuelLocVM);
                return retObj;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }


        #endregion 

        #region PostFlightCrew
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> GetCurrentLegCrews(int legNum)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<Hashtable>>(() =>
            {
                FSSOperationResult<Hashtable> ResultObject = new FSSOperationResult<Hashtable>();
                if (!ResultObject.IsAuthorized())
                {
                    return ResultObject;
                }
                ResultObject.StatusCode = HttpStatusCode.OK;
                ResultObject.Success=false;
          
                Hashtable result = new System.Collections.Hashtable();
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                    if (TripLog != null)
                    {
                        var userPrincipal = getUserPrincipal();
                        if (TripLog.PostflightLegs != null && TripLog.PostflightLegs.Count > 0)
                        {
                            List<PostflightLegCrewViewModel> crews = new List<PostflightLegCrewViewModel>();
                            var currentLeg = TripLog.PostflightLegs.FirstOrDefault(x => x.LegNUM == legNum && x.IsDeleted == false && x.State != TripEntityState.Deleted);
                            if (currentLeg != null && currentLeg.PostflightLegCrews != null && currentLeg.PostflightLegCrews.Count > 0)
                            {
                                crews = currentLeg.PostflightLegCrews.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted).OrderBy(x => x.OrderNUM).ToList();
                                if (crews.Count > 0) // need to remove
                                {
                                    if (TripLog.PostflightMain.POEditMode)
                                    {
                                        PostflightCrewLite crewLite = new PostflightCrewLite(userPrincipal);

                                        foreach (var crew in crews)
                                        {
                                            if (string.IsNullOrWhiteSpace(crew.CrewCode) == true)
                                            {
                                                var objRetVal = crew.CrewID.HasValue ? DBCatalogsDataLoader.GetCrewInfo_FromCrewId((long)crew.CrewID) : new CrewViewModel();
                                                if (objRetVal != null && !String.IsNullOrWhiteSpace(objRetVal.CrewCD))
                                                    crew.CrewCode = objRetVal.CrewCD.ToUpper();
                                            }

                                            if (crew.IsRemainOverNightOverride != null && !(bool)crew.IsRemainOverNightOverride)
                                                crew.RemainOverNight = crewLite.RONCalculation((long)crew.CrewID, (long)currentLeg.LegNUM, userPrincipal, TripLog);
                                            if (crew.CrewID.HasValue && currentLeg.LegNUM.HasValue)
                                                crew.DaysAway = crewLite.DaysAwayCalculation(TripLog, crew.CrewID.Value, currentLeg.LegNUM.Value);
                                        }
                                        PostFlightValidator.ReCalculateCrewDutyHour();
                                    }
                                    result["currentSelectedCrew"] = crews[0];
                                    result["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
                                    result["results"] = crews;                                    
                                    HttpContext.Current.Session[WebSessionKeys.CurrentLegCrews] = crews;
                                }
                            }
							ResultObject.Success = true;
                            ResultObject.Result = result;
                        }
                        TripLog.CurrentLegNUM = legNum;
                        HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;
                    }
                }
                else
                {
                    result["currentSelectedCrew"] = new List<PostflightLegCrewViewModel>();
                    result["meta"] = new PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
                    result["results"] = new List<PostflightLegCrewViewModel>();
                    ResultObject.Result = result;
                }
                return ResultObject;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<PostflightLegCrewViewModel> GetSelectedCrewData(long CrewID, int currentLegNum)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<PostflightLegCrewViewModel>>(() =>
            {
                FSSOperationResult<PostflightLegCrewViewModel> ResultObject = new FSSOperationResult<PostflightLegCrewViewModel>();
                if (!ResultObject.IsAuthorized())
                {
                    return ResultObject;
                }
                ResultObject.StatusCode = HttpStatusCode.OK;
                ResultObject.Success = true;
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    PostflightLegCrewViewModel crewDetail = new PostflightLegCrewViewModel();
                    var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                    if (TripLog != null)
                    {
                        if (TripLog.PostflightLegs != null && TripLog.PostflightLegs.Count > 0)
                        {
                            var currentLeg = TripLog.PostflightLegs.FirstOrDefault(x => x.LegNUM == currentLegNum && x.IsDeleted == false && x.State != TripEntityState.Deleted);
                            if (currentLeg != null && currentLeg.PostflightLegCrews != null && currentLeg.PostflightLegCrews.Count > 0)
                                crewDetail = currentLeg.PostflightLegCrews.FirstOrDefault(x => x.IsDeleted == false && x.State != TripEntityState.Deleted && x.CrewID == CrewID);
                            if (crewDetail != null)
                                ResultObject.Result = crewDetail;
                        }
                    }
                }
                return ResultObject;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> AddCrewToSession(List<PostflightLegCrewViewModel> CrewsList, bool CopyToAllLegs, int currentLegNum)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<bool>>(() =>
            {
                FSSOperationResult<bool> ResultObject = new FSSOperationResult<bool>();
                if (!ResultObject.IsAuthorized())
                {
                    return ResultObject;
                }
                ResultObject.StatusCode = HttpStatusCode.OK;
                var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                if (TripLog != null)
                {
                    PostflightCrewLite poCrew = new PostflightCrewLite();
                    if (TripLog.PostflightLegs != null && TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted).Count() > 0)
                    {
                        var legs = TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted);
                        ResultObject.Success = poCrew.AddCrew(CopyToAllLegs, CrewsList, currentLegNum);
                        ResultObject.Result = true;
                    }
                }
                return ResultObject;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> SaveCrewCells(string ColName, long CrewID, string Value, long LegNUM)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<bool>>(() =>
            {
                FSSOperationResult<bool> ResultObject = new FSSOperationResult<bool>();
                if (!ResultObject.IsAuthorized())
                {
                    return ResultObject;
                }
                ResultObject.StatusCode = HttpStatusCode.OK;
                ResultObject.Success = true;
                var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                if (TripLog != null)
                {
                    if (TripLog.PostflightLegs != null && TripLog.PostflightLegs.Count > 0)
                    {
                        var currentLeg = TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted && x.LegNUM == LegNUM).OrderBy(y => y.LegNUM).FirstOrDefault();
                        if (currentLeg != null && currentLeg.PostflightLegCrews != null && currentLeg.PostflightLegCrews.Count > 0)
                        {
                            var modelRow = currentLeg.PostflightLegCrews.Where(x => x.CrewID == CrewID && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
                            if (modelRow != null)
                            {
                                if (ColName == "ddlCrewDutyType")
                                    modelRow.DutyTYPE = Value;
                                else if (ColName == "TakeOffDay")
                                    modelRow.TakeOffDay = Convert.ToDecimal(string.IsNullOrWhiteSpace(Value) ? "0" : Value);
                                else if (ColName == "TakeOffNight")
                                    modelRow.TakeOffNight = Convert.ToDecimal(string.IsNullOrWhiteSpace(Value) ? "0" : Value);
                                else if (ColName == "LandingDay")
                                    modelRow.LandingDay = Convert.ToDecimal(string.IsNullOrWhiteSpace(Value) ? "0" : Value);
                                else if (ColName == "LandingDayNight")
                                    modelRow.LandingNight = Convert.ToDecimal(string.IsNullOrWhiteSpace(Value) ? "0" : Value);
                                else if (ColName == "ApproachPrecision")
                                    modelRow.ApproachPrecision = Convert.ToDecimal(string.IsNullOrWhiteSpace(Value) ? "0" : Value);
                                else if (ColName == "ApproachNonPrecision")
                                    modelRow.ApproachNonPrecision = Convert.ToDecimal(string.IsNullOrWhiteSpace(Value) ? "0" : Value);
                                else if (ColName == "Instrument")
                                    modelRow.InstrumentStr = Value;
                                else if (ColName == "Night")
                                    modelRow.NightStr = Value;
                                else if (ColName == "DaysAway")
                                    modelRow.DaysAway = Convert.ToDecimal(string.IsNullOrWhiteSpace(Value) ? "0" : Value);
                                else if (ColName == "RemainOverNight")
                                {
                                    modelRow.RemainOverNight = Convert.ToDecimal(string.IsNullOrWhiteSpace(Value) ? "0" : Value);
                                    if (modelRow.RemainOverNight > 0)
                                        modelRow.IsRemainOverNightOverride = true;
                                }
                                else if (ColName == "CrewDutyStartTM")
                                    modelRow.CrewDutyStartTime = Value;
                                else if (ColName == "CrewDutyEndTM")
                                    modelRow.CrewDutyEndTime = Value;
                                else if (ColName == "DutyHours")
                                    modelRow.DutyHours = Convert.ToDecimal(string.IsNullOrWhiteSpace(Value) ? "0" : Value);
                                else if (ColName == "BlockHours")
                                {
                                    modelRow.BlockHoursTime = Value;
                                    if (modelRow.BlockHoursTime != currentLeg.BlockHoursTime)
                                        modelRow.IsAugmentCrew = true;
                                }
                                else if (ColName == "FlightHours")
                                {
                                    modelRow.FlightHoursTime = Value;
                                    if (modelRow.FlightHoursTime != currentLeg.FlightHoursTime)
                                        modelRow.IsAugmentCrew = true;
                                }
                                else if (ColName == "ddlCrewSeat")
                                    modelRow.Seat = Value;
                                else if (ColName == "Specification1")
                                    modelRow.Specification1 = Convert.ToBoolean(Value);
                                else if (ColName == "Specification2")
                                    modelRow.Sepcification2 = Convert.ToBoolean(Value);
                                else if (ColName == "Specification3")
                                    modelRow.Specification3 = Convert.ToDecimal(Convert.ToDecimal(string.IsNullOrWhiteSpace(Value) ? "0" : Value));
                                else if (ColName == "Specification4")
                                    modelRow.Specification4 = Convert.ToDecimal(Convert.ToDecimal(string.IsNullOrWhiteSpace(Value) ? "0" : Value));

                                if (modelRow.State != TripEntityState.Added && modelRow.State != TripEntityState.Deleted)
                                    modelRow.State = TripEntityState.Modified;
                            }
                        }
                        HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;
                        ResultObject.Result = true;
                    }
                }
                return ResultObject;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> DeleteCrewInfoFromSession(List<PostflightLegCrewViewModel> CrewsList, bool isDeleteAllCrew, int currentLegNum)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<bool>>(() =>
            {
                FSSOperationResult<bool> ResultObject = new FSSOperationResult<bool>();
                if (!ResultObject.IsAuthorized())
                {
                    return ResultObject;
                }
                ResultObject.StatusCode = HttpStatusCode.OK;
                ResultObject.Success = true;
                PostflightCrewLite crewLite = new PostflightCrewLite();
                var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                if (TripLog != null)
                {
                    if (TripLog.PostflightLegs != null && TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted).Count() > 0)
                    {
                        PostflightLegViewModel currentLeg = TripLog.PostflightLegs.Where(x => x.LegNUM == currentLegNum && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
                        if (CrewsList != null)
                        {
                            foreach (var crew in CrewsList)
                            {
                                long CrewId = 0;
                                if (crew.CrewID != null & crew.CrewID != 0)
                                    CrewId = (long)crew.CrewID;
                                if (isDeleteAllCrew)
                                {
                                    foreach (PostflightLegViewModel leg in TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted))
                                    {
                                        if (leg != null && CrewId > 0)
                                        {
                                            PostflightLegCrewViewModel objCrew = leg.PostflightLegCrews.Where(x => x.CrewID == CrewId && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
                                            if (objCrew != null)
                                            {
                                                if (objCrew.PostflightCrewListID > 0)
                                                {
                                                    objCrew.IsDeleted = true;
                                                    objCrew.State = TripEntityState.Deleted;
                                                }
                                                else
                                                    leg.PostflightLegCrews.Remove(objCrew);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    long CrewListID = 0;
                                    if (crew.PostflightCrewListID != 0)
                                        CrewListID = crew.PostflightCrewListID;

                                    if (currentLeg != null && CrewListID > 0)
                                    {
                                        PostflightLegCrewViewModel objCrew = currentLeg.PostflightLegCrews.Where(x => x.PostflightCrewListID == CrewListID && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
                                        if (objCrew != null)
                                        {
                                            objCrew.IsDeleted = true;
                                            objCrew.State = TripEntityState.Deleted;
                                        }
                                    }
                                    else
                                    {
                                        PostflightLegCrewViewModel objCrew = currentLeg.PostflightLegCrews.Where(x => x.CrewID == CrewId && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
                                        if (objCrew != null)
                                            currentLeg.PostflightLegCrews.Remove(objCrew);
                                    }
                                }
                            }
                        }
                    }

                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;
                    FlightPak.Web.Views.Transactions.PostFlightValidator.ReCalculateCrewDutyHour();
                    crewLite.BindValues(currentLegNum);
                    ResultObject.Result = true;
                }
                return ResultObject;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<decimal> RONCalculation(long CrewID, int currentLegNum)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<decimal>>(() =>
            {
                FSSOperationResult<decimal> ResultObject = new FSSOperationResult<decimal>();
                if (!ResultObject.IsAuthorized())
                {
                    return ResultObject;
                }
                ResultObject.StatusCode = HttpStatusCode.OK;
                ResultObject.Success = true;
                decimal RON = 0;
                var userPrincipal = getUserPrincipal();
                var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                if (TripLog != null)
                {
                    if (CrewID != 0 && currentLegNum > 0)
                    {
                        PostflightCrewLite pfCrewLite=new PostflightCrewLite();
                        RON = pfCrewLite.RONCalculation(CrewID, currentLegNum, userPrincipal, TripLog);
                        if (TripLog.PostflightLegs != null && TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted).Count() > 0)
                        {
                            PostflightLegViewModel currentLeg = TripLog.PostflightLegs.Where(x => x.LegNUM == currentLegNum && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
                            if (currentLeg != null && currentLeg.PostflightLegCrews != null && currentLeg.PostflightLegCrews.Count > 0)
                            {
                                var crewObj = currentLeg.PostflightLegCrews.Where(x => x.CrewID == CrewID && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
                                if (crewObj != null)
                                    crewObj.RemainOverNight = RON;
                            }
                        }
                    }
                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;
                    ResultObject.Result = RON;
                }
                return ResultObject;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> DutyAdjReset(long CrewID, int currentLegNum)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<bool>>(() =>
            {
                FSSOperationResult<bool> ResultObject = new FSSOperationResult<bool>();
                if (!ResultObject.IsAuthorized())
                {
                    return ResultObject;
                }
                ResultObject.StatusCode = HttpStatusCode.OK;
                ResultObject.Success = true;
                var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                if (TripLog != null)
                {
                    if (TripLog.PostflightLegs != null && TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted).Count() > 0)
                    {
                        UserPrincipalViewModel upViewModel = new UserPrincipalViewModel();
                        PostflightCrewLite crewLite = new PostflightCrewLite();
                        PostflightLegViewModel currentLeg = TripLog.PostflightLegs.Where(x => x.LegNUM == currentLegNum && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
                        if (currentLeg != null)
                        {
                            PostflightLegCrewViewModel objCrew = currentLeg.PostflightLegCrews.Where(x => x.CrewID == CrewID && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
                            if (objCrew != null)
                            {
                                objCrew.IsOverride = false;
                                if (objCrew.BeginningDuty == null)
                                    objCrew.BeginningDuty = "00:00";

                                if (objCrew.DutyEnd == null)
                                    objCrew.DutyEnd = "00:00";
                            }
                        }
                        PostFlightValidator.ReCalculateCrewDutyHour();
                        crewLite.BindValues((int)currentLeg.LegNUM);
                    }
                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;
                    ResultObject.Result = true;
                }
                return ResultObject;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> AugmentCrewReset(long CrewID, int currentLegNum)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<bool>>(() =>
            {
                FSSOperationResult<bool> ResultObject = new FSSOperationResult<bool>();
                if (!ResultObject.IsAuthorized())
                {
                    return ResultObject;
                }
                ResultObject.StatusCode = HttpStatusCode.OK;
                ResultObject.Success = true;
                var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                if (TripLog != null)
                {
                    if (TripLog.PostflightLegs != null && TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted).Count() > 0)
                    {
                        UserPrincipalViewModel upViewModel = new UserPrincipalViewModel();
                        PostflightLegViewModel currentLeg = TripLog.PostflightLegs.Where(x => x.LegNUM == currentLegNum && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
                        if (currentLeg != null)
                        {
                            var selectedCrew = currentLeg.PostflightLegCrews.Where(x => x.CrewID == CrewID && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
                            if (selectedCrew != null)
                            {
                                selectedCrew.IsAugmentCrew = false;

                                if ((upViewModel._TimeDisplayTenMin == null) || (upViewModel._TimeDisplayTenMin == POTenthMinuteFormat.Tenth))
                                {

                                    if (!string.IsNullOrWhiteSpace(selectedCrew.BlockHoursTime))
                                        selectedCrew.BlockHours = Math.Round(Convert.ToDecimal(currentLeg.BlockHours), 1);
                                    else
                                        selectedCrew.BlockHours = Convert.ToDecimal("0.0");

                                    if (!string.IsNullOrWhiteSpace(selectedCrew.FlightHoursTime))
                                        selectedCrew.FlightHours = Math.Round(Convert.ToDecimal(currentLeg.FlightHours), 1);
                                    else
                                        selectedCrew.FlightHours = Convert.ToDecimal("0.0");

                                    currentLeg.BlockHours = Math.Round(Convert.ToDecimal(currentLeg.BlockHours), 1);  //??
                                    currentLeg.FlightHours = Math.Round(Convert.ToDecimal(currentLeg.FlightHours), 1); //??
                                }
                                else
                                {
                                    if (!string.IsNullOrWhiteSpace(selectedCrew.BlockHoursTime))
                                        selectedCrew.BlockHours = Convert.ToDecimal(FPKConversionHelper.ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(currentLeg.BlockHours), 3).ToString()));
                                    else
                                        selectedCrew.BlockHoursTime = "00:00";

                                    if (!string.IsNullOrWhiteSpace(selectedCrew.FlightHoursTime))
                                        selectedCrew.FlightHours = Convert.ToDecimal(FPKConversionHelper.ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(currentLeg.FlightHours), 3).ToString()));
                                    else
                                        selectedCrew.FlightHoursTime = "00:00";

                                    currentLeg.BlockHours = Convert.ToDecimal(FPKConversionHelper.ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(currentLeg.BlockHours), 3).ToString())); //??
                                    currentLeg.FlightHours = Convert.ToDecimal(FPKConversionHelper.ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(currentLeg.FlightHours), 3).ToString())); //??
                                }
                            }
                        }
                    }
                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;
                    ResultObject.Result = true;
                }
                return ResultObject;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable CheckDutyHourOverride(long crewID, int currentLegNum)
        {
            PostflightCrewLite crew = new PostflightCrewLite();
            Hashtable retObj=new Hashtable();
            if (crewID != null)
            {
                UserPrincipalViewModel userPrincipleVM = (UserPrincipalViewModel)HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel];
                if (userPrincipleVM == null)
                {
                    userPrincipleVM = getUserPrincipal();
                }
                retObj["Result"] = FlightPak.Web.BusinessLite.Postflight.PostflightCrewLite.CheckDutyHourOverride(crewID, currentLegNum, userPrincipleVM);
                FlightPak.Web.Views.Transactions.PostFlightValidator.ReCalculateCrewDutyHour();
                crew.BindValues(currentLegNum);
            }
            return retObj;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> SaveEditedCrewInfoToSession(PostflightLegCrewViewModel CrewObj, int currentLegNum)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FSSOperationResult<bool>>(() =>
            {
                FSSOperationResult<bool> ResultObject = new FSSOperationResult<bool>();
                if (!ResultObject.IsAuthorized())
                {
                    return ResultObject;
                }
                ResultObject.StatusCode = HttpStatusCode.OK;
                ResultObject.Success = true;
                if (CrewObj != null)
                {
                    var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                    if (TripLog != null)
                    {
                        if (TripLog.PostflightLegs != null && TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted).Count() > 0)
                        {
                            PostflightLegViewModel currentLeg = TripLog.PostflightLegs.Where(x => x.LegNUM == currentLegNum && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
                            if (currentLeg != null && currentLeg.PostflightLegCrews != null && currentLeg.PostflightLegCrews.Count > 0)
                            {
                                var crewObj = currentLeg.PostflightLegCrews.Where(x => x.CrewID == CrewObj.CrewID && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
                                if (crewObj != null)
                                {
                                    crewObj.FlightHoursTime = CrewObj.FlightHoursTime;
                                    if (crewObj.FlightHoursTime != currentLeg.FlightHoursTime)
                                        crewObj.IsAugmentCrew = true;
                                    crewObj.BlockHoursTime = CrewObj.BlockHoursTime;
                                    if (crewObj.BlockHoursTime != currentLeg.BlockHoursTime)
                                        crewObj.IsAugmentCrew = true;
                                    crewObj.CrewDutyStartDate = CrewObj.CrewDutyStartDate;
                                    crewObj.CrewDutyStartTime = CrewObj.CrewDutyStartTime;
                                    crewObj.CrewDutyEndDate = CrewObj.CrewDutyEndDate;
                                    crewObj.CrewDutyEndTime = CrewObj.CrewDutyEndTime;
                                    crewObj.StartTime = CrewObj.StartTime;
                                    crewObj.EndTime = CrewObj.EndTime;
                                }
                            }
                        }
                        HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;
                        ResultObject.Result = true;
                    }
                }
                return ResultObject;
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        public static PostflightLegViewModel LoadLegDetails(PostflightLegViewModel postLeg)
        {
            UserPrincipalViewModel upViewModel = getUserPrincipal();
            PostflightCrewLite crew = new PostflightCrewLite();
            if (postLeg != null)
            {
                //if (postLeg.Distance != null && postLeg.Distance != decimal.MinValue)
                //    postLeg.Distance = crew.ConvertToKilomenterBasedOnCompanyProfile((decimal)postLeg.Distance);

                if (!string.IsNullOrWhiteSpace(postLeg.TimeOff))
                    postLeg.TimeOff = string.Format("{0:HH:mm}", postLeg.TimeOff);
                //else
                //    postLeg.TimeOff = "00:00";

                //if (string.IsNullOrWhiteSpace(postLeg.TimeOn))
                //    postLeg.TimeOn = "00:00";

                if (upViewModel._TimeDisplayTenMin == POTenthMinuteFormat.Minute)
                {
                    if (postLeg.BlockHours != null)
                        postLeg.BlockHoursTime = FPKConversionHelper.ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(postLeg.BlockHours), 3).ToString());
                    else
                        postLeg.BlockHoursTime = "00:00";

                    if (postLeg.FlightHours != null)
                        postLeg.FlightHoursTime = FPKConversionHelper.ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(postLeg.FlightHours), 3).ToString());
                    else
                        postLeg.FlightHoursTime = "00:00";

                    if (postLeg.DutyHrs != null)
                        postLeg.DutyHrsStr = FPKConversionHelper.ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal(postLeg.DutyHrs), 3).ToString());
                    else
                        postLeg.DutyHrsStr = "00:00";

                }
                else
                {
                    if (postLeg.BlockHours != null)
                        postLeg.BlockHoursTime = Math.Round(Convert.ToDecimal(postLeg.BlockHours.ToString()), 1).ToString();
                    else
                        postLeg.BlockHoursTime = "0.0";

                    if (postLeg.FlightHours != null)
                        postLeg.FlightHoursTime = Math.Round(Convert.ToDecimal(postLeg.FlightHours.ToString()), 1).ToString();
                    else
                        postLeg.FlightHoursTime = "0.0";

                    if (postLeg.DutyHrs != null)
                        postLeg.DutyHrsStr = Math.Round(Convert.ToDecimal(postLeg.FlightHours.ToString()), 1).ToString();
                    else
                        postLeg.DutyHrsStr = "0.0";

                }

                if (postLeg.FedAviationRegNum == null)
                    postLeg.FedAviationRegNum = "0";

                string rulesToolTip = string.Empty;
                if (postLeg.CrewCurrency != null)
                {
                    var RulesVal = DBCatalogsDataLoader.GetCrewDutyRulesInfo_FromFilter(0, postLeg.CrewCurrency.Trim());
                    if (RulesVal != null && !String.IsNullOrWhiteSpace(RulesVal.CrewDutyRuleCD))
                    {
                        postLeg.CrewCurrency = RulesVal.CrewDutyRuleCD.ToString().ToUpper();

                        if (RulesVal.CrewDutyRulesDescription != null)
                            postLeg.CrewDutyRulesDescription = System.Web.HttpUtility.HtmlEncode(RulesVal.CrewDutyRulesDescription.ToUpper());
                        //** need to check this tooltip in crewdutyruleobject
                        rulesToolTip = "Duty Day Start : " + (RulesVal.DutyDayBeingTM != null ? Math.Round((decimal)RulesVal.DutyDayBeingTM.Value, 1).ToString() : string.Empty);
                        rulesToolTip += "\nDuty Day End : " + (RulesVal.DutyDayEndTM != null ? Math.Round((decimal)RulesVal.DutyDayEndTM.Value, 1).ToString() : string.Empty);
                        rulesToolTip += "\nMax. Duty Hrs. Allowed : " + (RulesVal.MaximumDutyHrs != null ? Math.Round((decimal)RulesVal.MaximumDutyHrs.Value, 1).ToString() : string.Empty);
                        rulesToolTip += "\nMax. Flight Hrs. Allowed : " + (RulesVal.MaximumFlightHrs != null ? Math.Round((decimal)RulesVal.MaximumFlightHrs.Value, 1).ToString() : string.Empty);
                        rulesToolTip += "\nMin. Fixed Rest : " + (RulesVal.MinimumRestHrs != null ? Math.Round((decimal)RulesVal.MinimumRestHrs.Value, 1).ToString() : string.Empty);
                        postLeg.CrewRulesToolTipInfo = rulesToolTip;
                    }
                }
                else
                    postLeg.CrewCurrency = string.Empty;

                if (postLeg != null && postLeg.PostflightLegCrews != null && postLeg.PostflightLegCrews.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted).ToList().Count > 0)
                {
                    if (upViewModel != null && upViewModel._IsAutoPopulated == false)
                    {
                        foreach (PostflightLegCrewViewModel poCrew in postLeg.PostflightLegCrews)
                        {
                            if (poCrew.BlockHours != null && poCrew.BlockHours <= 0)
                                poCrew.BlockHours = postLeg.BlockHours;

                            if (poCrew.FlightHours != null && poCrew.FlightHours <= 0)
                                poCrew.FlightHours = postLeg.FlightHours;

                            if (poCrew.DutyHours != null && poCrew.DutyHours <= 0 && poCrew.CrewID != null)
                                poCrew.DutyHours = crew.DutyHourForCrew(postLeg, (long)poCrew.CrewID).DutyHours;

                            if (poCrew.CrewDutyStartTM == null && poCrew.CrewID != null)
                                //poCrew.CrewDutyStartTM = crew.DutyHourForCrew(postLeg, (long)poCrew.CrewID).CrewDutyStartTM;

                            if (poCrew.CrewDutyEndTM == null && poCrew.CrewID != null)
                                //poCrew.CrewDutyEndTM = crew.DutyHourForCrew(postLeg, (long)poCrew.CrewID).CrewDutyEndTM;

                            if (string.IsNullOrWhiteSpace(poCrew.BeginningDuty) && poCrew.CrewID != null)
                                poCrew.BeginningDuty = crew.DutyHourForCrew(postLeg, (long)poCrew.CrewID).BeginningDuty;

                            if (string.IsNullOrWhiteSpace(poCrew.DutyEnd) && poCrew.CrewID != null)
                                poCrew.DutyEnd = crew.DutyHourForCrew(postLeg, (long)poCrew.CrewID).DutyEnd;
                        }
                    }
                    crew.BindValues((int)postLeg.LegNUM);
                }
            }
            return postLeg;
        }
        #endregion

        #region Private Methods
        private static PostflightLegViewModel SetDefaultDetails(PostflightLegViewModel leg, PostflightMainViewModel poMainVM)
        {
            UserPrincipalViewModel userPrincipal = getUserPrincipal();
            leg.FlightNum = poMainVM.FlightNum;
            leg.FlightPurpose = poMainVM.POMainDescription;
            leg.AccountID = poMainVM.AccountID;
            if (leg.Account == null || leg.Account.AccountID == 0)
            {
                leg.Account = leg.AccountID.HasValue ? DBCatalogsDataLoader.GetAccountInfo_FromAccountId(leg.AccountID.Value) : new AccountViewModel();
            }
            leg.ClientID = poMainVM.ClientID;
            if (leg.Client == null || leg.Client.ClientID == 0)
            {
                leg.Client = leg.ClientID.HasValue ? DBCatalogsDataLoader.GetClientInfo_FromClientID(leg.ClientID.Value) : new ClientViewModel();
            }
            leg.DepartmentID = poMainVM.DepartmentID;
            if (leg.Department == null || leg.Department.DepartmentID == 0)
            {
                leg.Department = leg.DepartmentID.HasValue ? DBCatalogsDataLoader.GetDepartmentByIDorCD(leg.DepartmentID.Value, string.Empty) : new DepartmentViewModel();
            }
            leg.AuthorizationID = poMainVM.AuthorizationID;
            if (leg.Department != null && (leg.DepartmentAuthorization == null || leg.DepartmentAuthorization.AuthorizationID == 0))
            {
                leg.DepartmentAuthorization = leg.AuthorizationID.HasValue ? DBCatalogsDataLoader.GetDepartmentAuthorizationByDepIDnAuthorizationCDorID(string.Empty, leg.AuthorizationID.Value, leg.Department.DepartmentID) : new DepartmentAuthorizationViewModel();
            }
            leg.PassengerRequestorID = poMainVM.PassengerRequestorID;
            if (leg.Passenger == null || leg.Passenger.PassengerRequestorID == 0)
            {
                leg.Passenger = leg.PassengerRequestorID.HasValue ? DBCatalogsDataLoader.GetPassengerReqestorInfo_FromPassengerReqID(leg.PassengerRequestorID.Value) : new PostflightLegPassengerViewModel();
            }
            if (userPrincipal._DefaultFlightCatID != null)
                leg.FlightCategoryID = userPrincipal._DefaultFlightCatID;

            if (leg.FlightCatagory == null || leg.FlightCatagory.FlightCategoryID == 0)
            {
                leg.FlightCatagory = leg.FlightCategoryID.HasValue ? DBCatalogsDataLoader.GetFlightCategoryInfo_FromId(leg.FlightCategoryID.Value) : new FlightCatagoryViewModel();
            }


            leg.CrewDutyRulesID = userPrincipal._CrewDutyID ?? 0;
            if (leg.CrewDutyRule == null || leg.CrewDutyRule.CrewDutyRulesID == 0)
            {
                leg.CrewDutyRule = leg.CrewDutyRulesID.HasValue ? DBCatalogsDataLoader.GetCrewDutyRulesInfo_FromFilter(leg.CrewDutyRulesID.Value, string.Empty) : new CrewDutyRuleViewModel();
                if (leg.CrewDutyRule != null && leg.CrewDutyRule.CrewDutyRulesID > 0)
                {
                    leg.CrewCurrency = leg.CrewDutyRule.CrewDutyRuleCD;
                    leg.FedAviationRegNum = leg.CrewDutyRule.FedAviatRegNum.Trim();                    
                }
            }

            return leg;
        }
        public static PostflightLegViewModel SetLegUserPrincipalSettings(PostflightLegViewModel leg)
        {
            UserPrincipalViewModel userPrincipal = getUserPrincipal();
            if (leg == null)
                leg = new PostflightLegViewModel();

            leg.TimeDisplayTenMin = userPrincipal._TimeDisplayTenMin != null
                ? (decimal) userPrincipal._TimeDisplayTenMin
                : POTenthMinuteFormat.Tenth;

            leg.HoursMinutesCONV = userPrincipal._HoursMinutesCONV != null
                ? (int) userPrincipal._HoursMinutesCONV
                : 0;

            leg.IsKilometer = userPrincipal._IsKilometer;

            return leg;
        }
        #endregion
    }
}


