﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaxPassportVisa.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.PostFlight.PaxPassportVisa" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PAX Passport Visa</title>
  
</head>
<body>
   
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="radScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        
    <%=Styles.Render("~/bundles/jqgrid") %>
    <%=Scripts.Render("~/bundles/jquery") %>
    <%=Scripts.Render("~/bundles/sitecommon") %>
    <%=Scripts.Render("~/bundles/jqgridjs") %>


    <script type="text/javascript">
            var jqgridPassport = '#dgPassport';
            var jqgridVisa = '#dgVisa';
            var TitleSIFLAlert = "Postflight";

            $(document).ready(function () {

                $.extend(jQuery.jgrid.defaults, {
                    prmNames: {
                        page: "page", rows: "size", order: "dir", sort: "sort"
                    }
                });
                $("#btnSubmit").click(function () {
                    var selr = $(jqgridPassport).jqGrid('getGridParam', 'selrow');
                    if ((selr != null)) {
                        var rowData = $(jqgridPassport).getRowData(selr);
                        returnToParent(rowData, 0);
                    }
                    else {
                        jAlert('Please select a Passport.', popupTitle)
                    }
                    return false;
                });

                jQuery(jqgridPassport).jqGrid({
                    url: '/Views/Transactions/Postflight/PaxPassportVisa.aspx/GetAllPassports',
                    mtype: 'POST',
                    datatype: "json",
                    cache: false,
                    async: true,
                    viewrecords: true,
                    enableclear: true,
                    emptyrecords: "No records to view.",
                    rowNum: $("#rowNum").val(),                    
                    pager: "#pg_gridPager",
                    loadonce: true,
                    ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
                    serializeGridData: function (postData) {
                        if (postData._search == undefined || postData._search == false) {
                            if (postData.filters === undefined) postData.filters = null;
                        }                        
                        return JSON.stringify(postData);
                    },
                    height: 200,
                    width: 550,
                    ignoreCase: true,
                    autowidth: false,
                    shrinkToFit: false,
                    multiselect: false,
                    colNames: ['Passport No.', 'Choice', 'Expiration Date', 'Issuing City/Authority', 'Issue Date', 'Nationality', 'CountryCD', 'PassportID',''],
                    colModel: [                         
                         { name: 'PassportNum', index: 'PassportNum', width: 80, key: true },                         
                         { name: 'Choice', index: 'Choice', width: 40, sortable: true, formatter: "checkbox", classes: "text_centeralignment" },
                         { name: 'PassportExpiryDT', index: 'PassportExpiryDT', width: 60, sortable: true },
                         { name: 'IssueCity', index: 'IssueCity', width: 100, classes: "text_centeralignment" },
                         { name: 'IssueDT', index: 'IssueDT', width: 100, classes: "text_centeralignment" },
                         { name: 'CountryCD', index: 'CountryCD', width: 60, classes: "text_centeralignment" },
                         { name: 'CountryCD', index: 'CountryCD', width: 60,hidden:true },
                         { name: 'PassportID', index: 'PassportID', hidden: true },                         
                         { name: 'PassengerRequestorID', index: 'PassengerRequestorID', hidden: true }

                    ],
                    ondblClickRow: function (rowId) {
                        var rowData = jQuery(this).getRowData(rowId);
                        returnToParent(rowData, 0);
                    },
                    onSelectRow: function (id, status) {
                        if (status)
                            selectedRowData = $(this).getRowData(id);
                        else
                            selectedRowData = null;
                    }
                });

                $("#pagesizebox").insertBefore('.ui-paging-info');                

                jQuery(jqgridVisa).jqGrid({
                    url: '/Views/Transactions/Postflight/PaxPassportVisa.aspx/GetAllVisa',
                    mtype: 'POST',
                    datatype: "json",
                    cache: false,
                    async: true,
                    viewrecords: true,
                    enableclear: true,
                    emptyrecords: "No records to view.",
                    rowNum: $("#rowNumVisa").val(),
                    pager: "#pg_gridPagerVisa",
                    loadonce: true,
                    ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
                    serializeGridData: function (postData) {
                        if (postData._search == undefined || postData._search == false) {
                            if (postData.filters === undefined) postData.filters = null;
                        }
                        //postData.LegNUM = $("#hdnSelectedLegNum").val();
                        return JSON.stringify(postData);
                    },
                    height: 200,
                    width: 550,
                    ignoreCase: true,
                    autowidth: false,
                    shrinkToFit: false,
                    multiselect: false,
                    colNames: ['VisaID','Country', 'Visa No.', 'Expiration Date', 'Issuing City/Authority', 'Issue Date', 'Notes'],
                    colModel: [
                         { name: 'VisaID', index: 'VisaID', hidden: true },
                         { name: 'CountryCD', index: 'CountryCD', width: 60, key: true },
                         { name: 'VisaNum', index: 'VisaNum', width: 80, sortable: false },
                         { name: 'ExpiryDT', index: 'ExpiryDT', width: 80, sortable: false },
                         { name: 'IssuePlace', index: 'IssuePlace', width: 80 },
                         { name: 'IssueDate', index: 'IssueDate', width: 60 },
                         { name: 'Notes', index: 'Notes', width: 60 }                         
                    ]
                });

                $("#pagesizeboxVisa").insertBefore('.ui-paging-info');


            });

            //this function is used to close this window
            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshGrid(arg);
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                var oArg = new Object();
                oArg.PaxPassport = true;
                var rowData = null;
                var selr = $(jqgridPassport).jqGrid('getGridParam', 'selrow');                
                rowData = $(jqgridPassport).getRowData(selr);                
                if (rowData!=undefined) {
                    oArg.PassengerRequestorID = rowData.PassengerRequestorID;
                    oArg.PassportNum = rowData.PassportNum;
                    oArg.PassportID = rowData.PassportID;
                    oArg.PassportExpiryDT = rowData.PassportExpiryDT;
                    oArg.CountryCD = rowData.CountryCD;
                }
                else {
                    oArg.PassengerRequestorID = "";
                    oArg.PassportNum = "";
                    oArg.PassportExpiryDT = "";
                    oArg.CountryCD = "";
                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            <%--function RowDblClick() {
                var masterTable = $find("<%= dgPassport.ClientID %>").get_masterTableView();
                masterTable.fireCommand("InitInsert", "");
                return false;
            }

            function RowDblClick1() {
                var masterTable = $find("<%= dgVisa.ClientID %>").get_masterTableView();
                masterTable.fireCommand("InitInsert", "");
                return false;
            }--%>
            function rebindgrid() {
                $(jqgridPassport).setGridParam({ datatype: 'json', page: 1 }).trigger('reloadGrid');
                $(jqgridVisa).setGridParam({ datatype: 'json', page: 1 }).trigger('reloadGrid');
            }

            function PerformEditCommand(tab) {
                if (tab == "Passport") {
                    openWin2('/Views/Settings/People/PassengerCatalog.aspx?IsPopup=&IsPassport=true', 'radPaxInfoCRUDPopup');
                    return false;
                } else if (tab == "Visa")
                {
                    openWin2('/Views/Settings/People/PassengerCatalog.aspx?IsPopup=&IsPassport=false', 'radPaxInfoCRUDPopup');
                    return false;
                }
            }
            function PerformInsertCommand()
            {
                openWin2('/Views/Settings/People/PassengerCatalog.aspx?IsPopup=Add', 'radPaxInfoCRUDPopup');
            }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">        
        <table width="100%" cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td>
                                <table id="dgPassport" class="table table-striped table-hover table-bordered"></table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="grid_icon">
                                    <div role="group" id="pg_gridPager"></div>
                                    <a id="recordEdit" runat="server" clientidmode="Static" title="Edit" class="edit-icon-grid" href="" onclick="return PerformEditCommand('Passport');"></a>
                                    <div id="pagesizebox">
                                        <span>Page Size:</span>
                                        <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                        <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridPassport, $('#rowNum')); return false;" />
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <div style="padding: 5px 5px; text-align: right;">
                                    <input runat="server" clientidmode="Static" id="btnSubmit" class="button okButton" value="OK" type="button" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="nav-space">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                     <table width="100%">
                        <tr>
                            <td>
                                <table id="dgVisa" class="table table-striped table-hover table-bordered"></table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="grid_icon2">
                                    <div role="group" id="pg_gridPagerVisa"></div> 
                                    <a runat="server" clientidmode="Static" id="recordEditVisa" title="Edit" class="edit-icon-grid" href="" onclick="return PerformEditCommand('Visa');"></a>                                                                       
                                </div>
                                <div class="clear">
                                </div>
                                <div style="padding: 5px 5px; text-align: right;">
                                    
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
