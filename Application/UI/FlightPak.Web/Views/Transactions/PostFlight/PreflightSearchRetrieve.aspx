﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreflightSearchRetrieve.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.PostFlight.PreflightSearchRetrieve" %>
<%@ Import Namespace="System.Web.Optimization" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ MasterType VirtualPath="~/Framework/Masters/PostFlight.Master" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Preflight Retrieve</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="radScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <%=Scripts.Render("~/bundles/jquery") %>
        <%=Scripts.Render("~/bundles/jqgridjs") %>
        <%=Scripts.Render("~/bundles/sitecommon") %>
        <%=Styles.Render("~/bundles/jqgrid") %>
        <%=Scripts.Render("~/bundles/knockout") %>
        <%=Scripts.Render("~/bundles/postflight") %>
        <%=Scripts.Render("~/bundles/PreflightSearchRetrieve") %>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true"
        ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="RadClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="ClientCidePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <div id="DivExternalForm">
        <table cellspacing="0" cellpadding="0" class="box1">
            <tr>
                <td>
                    <table id="tblSearchForm">
                        <tr>
                            <td valign="top">
                                <fieldset>
                                    <legend>Search</legend>
                                    <table>
                                        <tr>
                                            <td class="tdLabel120">
                                                <input type="checkbox" id="chkHomebase" /><label for="chkHomebase">Home Base Only</label>
                                            </td>
                                            <td>
                                                Starting Depart Date
                                            </td>
                                            <td class="tdLabel120">
                                                <input type="text" id="tbDepDate" class="text80" data-bind="datepicker: DepDate" />
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdLabel120">
                                                <input type="checkbox" id="ChkExcLog" /><label for="ChkExcLog">Exclude Logged</label>
                                            </td>
                                            <td>
                                                Client Code
                                            </td>
                                            <td align="left">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <input type="text" id="tbClientCode" onkeypress="return fnAllowAlphaNumeric(this, event)" class="text80"
                                                                 onblur="return RemoveSpecialChars(this)" data-bind="value: PreflightRetrieveAll.ClientCD, event: { change: ClientValidator_KO }"/>
                                                            <input type="hidden" id="hdnClient" data-bind="value: PreflightRetrieveAll.ClientID" />
                                                        </td>
                                                        <td>
                                                            <input type="button" id="btnClientCode" class="browse-button" onclick="javascript: openWin('RadClientCodePopup'); return false;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td>
                                                            <label id="lbClientDesc" class="input_no_bg" data-bind="text: PreflightRetrieveAll.ClientDescription.kotouppercase()"></label>
                                                            <label id="lbcvClient" class="alert-text"></label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                            <td>
                                <fieldset>
                                    <legend>Status</legend>
                                    <table width="100%">
                                        <tr>
                                            <td class="tdLabel120">
                                                <input type="checkbox" id="ChkWorksheet" /><label for="ChkWorksheet">Worksheet</label>
                                            </td>
                                            <td class="tdLabel120">
                                                <input type="checkbox" id="ChkTripsheet" /><label for="ChkTripsheet">Tripsheet</label>
                                            </td>
                                            <td class="tdLabel120">
                                                <input type="checkbox" id="ChUnFillFilled" /><label for="ChUnFillFilled">Unfulfilled</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdLabel120">
                                                <input type="checkbox" id="ChkCancelled" /><label for="ChkCancelled">Canceled</label>
                                            </td>
                                            <td class="tdLabel120">
                                                <input type="checkbox" id="ChkHold" /><label for="ChkHold">Hold</label>
                                            </td>
                                            <td class="tdLabel120" style="display:none">
                                                <input type="checkbox" id="ChkSchedServ" /><label for="ChkSchedServ">Sched Serv</label>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                            <td>
                                <input type="button" id="btnSearch" class="button" value="Search"  onclick="reloadPage(); return false;" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <div class="divGridPanel">
                                    <div class="jqgrid">
                                        <div>
                                            <table class="box1">
                                                <tr>
                                                    <td>
                                                        <table id="dgPreflightRetrieve"></table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="grid_icon">
                                                            <div role="group" id="pg_gridPager"></div>
                                                            <div id="pagesizebox">
                                                                <span>Size:</span>
                                                                <input class="psize" id="rowNum" type="text" value="10" maxlength="5" />
                                                                <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(); return false;" />
                                                            </div>
                                                        </div>
                                                        <div style="padding: 5px 5px; text-align: right;">
                                                            <input id="btnSubmit" type="button" class="button okButton" value="OK" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <br />
                                    <label id="lbMessage" style="color:red;"></label>
                                    <input type="hidden" id="hdSelectedTripID" />
                                    <asp:HiddenField ID="hdnPFRetrieveInitializationSchemaObject" runat="server" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
