﻿using System;

using FlightPak.Web.ViewModels.Postflight;
using Newtonsoft.Json;
using FlightPak.Web.ViewModels;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.Views.Transactions.PostFlight
{
    public partial class PostFlightMain : BaseSecuredPage
    {
       
        /// <summary>
        /// Page Load Events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            PostflightTripManager.CheckSessionAndRedirect();
            if (!IsPostBack)
            {
                PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
                if (Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    pfViewModel = (PostflightLogViewModel)Session[WebSessionKeys.CurrentPostflightLog];
                }
                else
                {
                    pfViewModel.PostflightMain = new PostflightMainViewModel(PostflightTripManager.getUserPrincipal()._ApplicationDateFormat);
                    pfViewModel.PostflightMain = PostflightTripManager.CreateOrCancelTrip(pfViewModel.PostflightMain);
                }

                // Set dictionaries to null, It does not need on clientside. Makes issue of dictionary serialization from clientside
                pfViewModel.PostflightMain.DeletedCrewIDs = null;
                pfViewModel.PostflightMain.DeletedExceptionIDs = null;
                pfViewModel.PostflightMain.DeletedExpenseIDs = null;
                pfViewModel.PostflightMain.DeletedPAXIDs = null;
                pfViewModel.PostflightMain.DeletedSIFLIDs = null;

                if (Session["SearchItemPrimaryKeyValue"] != null)
                {
                    pfViewModel = PostflightTripManager.LogByTripID(Convert.ToInt64(Session["SearchItemPrimaryKeyValue"]));
                    Session.Remove("SearchItemPrimaryKeyValue");
                }

                hdnPostflightMainInitializationObject.Value =  Microsoft.Security.Application.Encoder.HtmlEncode(JsonConvert.SerializeObject(pfViewModel));
                UserPrincipalViewModel userPrincipal = PostflightTripManager.getUserPrincipal();
                hdnUserPrincipalInitializationObject.Value = Microsoft.Security.Application.Encoder.HtmlEncode(JsonConvert.SerializeObject(userPrincipal));
                PostflightTripManager.ExceptionLimitLevelUpdate(0);
            }
        }

    }
}
