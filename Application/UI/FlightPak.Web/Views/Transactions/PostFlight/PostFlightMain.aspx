﻿<%@ Import Namespace="System.Web.Optimization" %>

<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/PostFlight.Master"
    AutoEventWireup="true" CodeBehind="PostFlightMain.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.PostFlight.PostFlightMain"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/UserControls/Postflight/UCPostflightSearch.ascx" TagName="Search" TagPrefix="UCPostflight" %>
<%@ MasterType VirtualPath="~/Framework/Masters/PostFlight.Master" %>
<%@ Register Src="~/UserControls/Postflight/UCPostflightBottomButtonControls.ascx" TagPrefix="uc" TagName="UCPostflightBottomButtonControls" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PostFlightHeadContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <%= Scripts.Render("~/bundles/postflightmain") %>
        <style type="text/css">
            .prfl-nav-icons {
                left: -2px !important;
            }
        </style>
        <script type="text/javascript">
            
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PostFlightBodyContent" runat="server">

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true"
        ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="radCompanyMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="HomeBasePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCompanyCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="HomeBasePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="ClientCodePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFleetProfilePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientTailNoClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFleetProfileCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radDepartmentPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientDeptClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radDepartmentCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientDeptClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAccountMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientAccountClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAccountMasterCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAuthorizationPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientAuthClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAuthorizationCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientAuthClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="RequestorPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadSearchPopup" runat="server" Width="750px" OnClientResizeEnd="GetDimensions" OnClientClose="OnLogSearchClose"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PostFlight/PostFlightSearchAll.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadRetrievePopup" runat="server" Width="750px" OnClientResizeEnd="GetDimensions" OnClientClose="OnPFSearchRetrieveClose"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PostFlight/PreflightSearchRetrieve.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="History" NavigateUrl="~/Views/Transactions/History.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdFleetChargeHistory" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientFleetChargeHistory" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetChargeHistory.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" style="width: auto; float: left;">

        <asp:HiddenField ID="hdnPostflightMainInitializationObject" runat="server" />
        <asp:HiddenField ID="hdnUserPrincipalInitializationObject" runat="server" />
       
        <input type="hidden" id="hdShowRequestor" />
        <table cellpadding="0" cellspacing="0" width="100%" class="border-box">
            <tr id="123">
                <td>
                    <UCPostflight:Search ID="PostflightSearch" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                       <table width="100%" id="tblForm" runat="server">
            <tr>
                <td>
                    <table width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="65%">
                                <fieldset>
                                    <legend>Flight Details </legend>
                                    <table width="100%" border="0">
                                        <tr>
                                            <td valign="top">
                                                <table width="100%">
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel100">Dispatch No.
                                                                    </td>
                                                                    <td>
                                                                        <input id="tbDispatchNo" type="text" data-bind="enable: POEditMode, textInput: Postflight.PostflightMain.DispatchNum" class="text110" maxlength="15" onkeypress="return fnAllowAlphaNumeric(this, event)" /></input>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td valign="top">
                                                <table width="100%">
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel100">
                                                                        <asp:Label ID="lbHomeBaseTitle" runat="server" Text="Home Base"></asp:Label>
                                                                    </td>
                                                                    <td valign="top">
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <input type="text" id="tbHomeBase" maxlength="4" class="text80" onkeypress="return fnAllowAlphaNumeric(this, event)"
                                                                                        data-bind="enable: POEditMode, value: Postflight.PostflightMain.Homebase.HomebaseCD, event: { change: HomebaseValidator_KO_change }" />
                                                                                    <input type="hidden" id="hdnHomeBase" data-bind="value: Postflight.PostflightMain.HomebaseID" />

                                                                                </td>
                                                                                <td>
                                                                                    <input type="button" id="btnHomeBase" data-bind="enable: POEditMode, css: BrowseBtn" title="Search for Home Base" onclick="    javascript: openWin('radCompanyMasterPopup'); return false;" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" valign="top">
                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <label id="lbHomeBase" class="input_no_bg" data-bind="text: Postflight.PostflightMain.Homebase.BaseDescription.kotouppercase(), attr: { title: Postflight.PostflightMain.Homebase.HomebaseToolTip } "></label>
                                                                                    <label id="lbcvHomeBase" class="alert-text"></label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table width="100%">
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel100">Flight No.
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="tbFlightNumber" runat="server" CssClass="text90" MaxLength="15"
                                                                            onKeyPress="return fnAllowAlphaNumeric(this, event)" data-bind="enable:POEditMode, value:Postflight.PostflightMain.FlightNum">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <input type="button" id="btnFlightNumberCopy" data-bind="enable: POEditMode, css: CopyIcon, event: { click: CopyFlightNumber_click }" title="Copy Flight Number to all legs" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td valign="top">
                                                <table width="100%">
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel100">Client
                                                                    </td>
                                                                    <td valign="top">
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <input type="text" id="tbClient" data-bind="enable: POEditMode, value: Postflight.PostflightMain.Client.ClientCD, event: { change: ClientCodeValidation }" maxlength="10" class="text80" onkeypress="return fnAllowAlphaNumeric(this, event)" />
                                                                                    <input type="hidden" id="hdnClient" data-bind="value: Postflight.PostflightMain.ClientID" />
                                                                                </td>
                                                                                <td>
                                                                                    <input type="button" id="btnClientCode" data-bind="enable: POEditMode, css: BrowseBtn" title="View Clients" onclick="    javascript: openWin('RadClientCodePopup'); return false;" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" valign="top">
                                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="lbClientDesc" runat="server" CssClass="input_no_bg" data-bind="text:Postflight.PostflightMain.Client.ClientDescription"></asp:Label>
                                                                                    <label id="lbcvClient" class="alert-text"></label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left">
                                                <table border="0">
                                                    <tr>
                                                        <td valign="top">
                                                            <table cellspacing="0" cellpadding="0" border="0">
                                                                <tr>
                                                                    <td class="tdLabel100">Tech. Log
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:TextBox ID="tbTechLog" data-bind="enable:POEditMode, value:Postflight.PostflightMain.TechLog" runat="server" CssClass="text110" MaxLength="12" onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                            <td width="35%" valign="top">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <fieldset>
                                                <legend>Aircraft</legend>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="mnd_text tdLabel70" valign="top">Tail No.
                                                                    </td>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <input type="text" id="tbTailNumber" data-bind="enable: POEditMode, value: Postflight.PostflightMain.Fleet.TailNum, event: { change: self.TailNum_PostflightValidate }" class="text80" maxlength="9" />

                                                                                    <input type="hidden" id="hdnTailNo" data-bind="value: Postflight.PostflightMain.FleetID" />
                                                                                    <input type="hidden" id="hdnAircraftID" data-bind="value: Postflight.PostflightMain.AircraftID" />
                                                                                </td>
                                                                                <td>
                                                                                    <input type="button" id="btnTailNo" data-bind="enable: POEditMode, css: BrowseBtn" class="browse-button" title="Search for Tail No." onclick="    javascript: openWin('radFleetProfilePopup'); return false;" />
                                                                                </td>
                                                                                <td>
                                                                                    <input type="button" data-bind="css: self.CharterRateBtn" id="btnTailNoChargeHistory" onclick="    javascript: openFleetChargeHistory(); return false;" title="Fleet Charge History" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <label id="lbTailNumber" class="input_no_bg"></label>
                                                                        <label id="lbcvTailNumber" class="alert-text"></label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel70">Type Code
                                                                    </td>
                                                                    <td>
                                                                        <input type="text" id="tbTypeCode" data-bind="enable: POEditMode, value: Postflight.PostflightMain.Aircraft.AircraftCD" readonly="readonly" class="inpt_non_edit text80" />
                                                                    </td>
                                                                    <td>&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <div class="tblspace_25">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td valign="top">
                                <fieldset>
                                    <legend>Others</legend>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel100" valign="top">Requestor
                                                        </td>
                                                        <td class="tdLabel150">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <input type="text" id="tbRequestor" data-bind="enable: POEditMode, value: Postflight.PostflightMain.Passenger.PassengerRequestorCD, event: { change: self.RequestorValidator_KO_change }" maxlength="5" class="text110" onkeypress="return fnAllowAlphaNumeric(this, event)"></input>
                                                                        <input type="hidden" id="hdnRequestor" data-bind="value: Postflight.PostflightMain.PassengerRequestorID" />
                                                                    </td>
                                                                    <td>
                                                                        <input type="button" data-bind="enable: POEditMode, css: BrowseBtn" id="btnRequestor" title="View Requestors" onclick="    javascript: openWin('radPaxInfoPopup'); return false;" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130">
                                                            <input type="button" data-bind="enable: POEditMode, css: CopyIcon, event: { click: CopyRequestor_click }" id="btnRequestorCopy" title="Copy Requestor to all legs" />
                                                        </td>
                                                        <td class="tdLabel90">Phone
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbRequestorPhone" data-bind="enable:POEditMode,value:Postflight.PostflightMain.Passenger.AdditionalPhoneNum" runat="server" ReadOnly="true" CssClass="inpt_non_edit text110"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <label id="lbRequestorName" data-bind="text: Postflight.PostflightMain.Passenger.PassengerName.kotouppercase()" class="input_no_bg"></label>
                                                            <label id="lbRequestor" class="input_no_bg"></label>
                                                            <label id="lbcvRequestor" class="alert-text"></label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel100" valign="top">Account No.
                                                        </td>
                                                        <td class="tdLabel150">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <input type="text" id="tbAccountNumber" data-bind="enable: POEditMode, value: Postflight.PostflightMain.Account.AccountNum, event: { change: self.AccountValidate_KO_change }" maxlength="32" class="text110"
                                                                            onkeypress="return fnAllowNumericAndDot(this, event)" />
                                                                        <input type="hidden" id="hdnAccountNo" data-bind="value: Postflight.PostflightMain.AccountID" />
                                                                    </td>
                                                                    <td>
                                                                        <input type="button" id="btnAccountNo" data-bind="enable: POEditMode, css: BrowseBtn" title="View Account Numbers" onclick="    javascript: openWin('radAccountMasterPopup'); return false;" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <input type="button" id="btnAccountCopy" data-bind="enable: POEditMode, css: CopyIcon, event: { click: CopyAccount_click }" title="Copy Account No. to all legs" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <label id="lbAccountDesc" data-bind="enable: POEditMode, text: Postflight.PostflightMain.Account.AccountDescription.kotouppercase()" class="input_no_bg"></label>
                                                            <label id="lbcvAccountNumber" class="alert-text"></label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel100" valign="top">Department
                                                        </td>
                                                        <td class="tdLabel150">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <input type="text" id="tbDepartment" data-bind="enable: POEditMode, value: Postflight.PostflightMain.Department.DepartmentCD, event: { change: self.DepartmentValidator_KO_change }" maxlength="8" class="text110" onkeypress="return fnAllowAlphaNumeric(this, event)"></input>
                                                                        <input type="hidden" id="hdnDepartment" data-bind="value: Postflight.PostflightMain.DepartmentID" />
                                                                    </td>
                                                                    <td>
                                                                        <input type="button" id="btnDepartment" data-bind="enable: POEditMode, css: BrowseBtn" title="View Departments" onclick="    javascript: openWin('radDepartmentPopup'); return false;" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="tdLabel130">
                                                            <input type="button" id="btnDeparmentCopy" data-bind="enable: POEditMode, css: CopyIcon, event: { click: CopyDepartment_click }" title="Copy Department to all legs" />
                                                        </td>
                                                        <td class="tdLabel90">Authorization
                                                        </td>
                                                        <td class="tdLabel150">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <input type="text" id="tbAuthorization" data-bind="enable: POEditMode, value: Postflight.PostflightMain.DepartmentAuthorization.AuthorizationCD, event: { change: self.Authorization_KO_change }" maxlength="13" class="text110"
                                                                            onkeypress="return fnAllowAlphaNumeric(this, event)" />
                                                                        <input type="hidden" id="hdnAuthorization" data-bind="value: Postflight.PostflightMain.AuthorizationID" />
                                                                    </td>
                                                                    <td>
                                                                        <input type="button" id="btnAuthorization" data-bind="enable: POEditMode, css: BrowseBtn" title="View Authorizations" onclick="    javascript: fnValidateAuth(); return false;" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <input type="button" id="btnAuthCopy" data-bind="enable: POEditMode, css: CopyIcon, event: { click: CopyAuth_click }" title="Copy Authorization to all legs" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" valign="top">
                                                            <label id="lbDepartmentName" data-bind="text: Postflight.PostflightMain.Department.DepartmentName.kotouppercase()" class="input_no_bg"></label>
                                                            <label id="lbcvDepartment" class="alert-text"></label>
                                                        </td>
                                                        <td colspan="3">
                                                            <label id="lbAuthorizationName" data-bind="text: Postflight.PostflightMain.DepartmentAuthorization.DeptAuthDescription" class="input_no_bg"></label>
                                                            <label id="lbcvAuthorization" class="alert-text"></label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="tdLabel100">Trip Purpose
                                                        </td>
                                                        <td class="tdLabel490">
                                                            <input type="text" id="tbDescription" data-bind="enable: POEditMode, textInput: Postflight.PostflightMain.POMainDescription" class="text480" maxlength="50" />
                                                        </td>
                                                        <td>
                                                            <input type="button" id="btnDescriptionCopy" data-bind="enable: POEditMode, css: CopyIcon, event: { click: CopyDescription_click }" title="Copy Trip Purpose to all legs" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

                </td>
            </tr>
        </table>

     
        <UC:UCPostflightBottomButtonControls runat="server" ID="UCPostflightBottomButtonControls1" />
    </div>
</asp:Content>
