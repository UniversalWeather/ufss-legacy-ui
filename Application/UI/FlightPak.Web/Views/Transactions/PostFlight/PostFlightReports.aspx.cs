﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using FlightPak.Common.Constants;
using Telerik.Web.UI;


namespace FlightPak.Web.Views.Transactions.PostFlight
{
    public partial class PostFlightReports : BaseSecuredPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HaveModuleAccess(ModuleId.PostflightReports))
            {
                Response.Redirect("~/Views/Home.aspx?m=PostflightReports");
            }

            ContentPlaceHolder contentPlaceHolder = (ContentPlaceHolder)Master.Master.FindControl("MainContent");
            HtmlTable tbl = (HtmlTable)contentPlaceHolder.FindControl("tblTripHeader");
            tbl.Visible = false;

            HtmlGenericControl control = (HtmlGenericControl)contentPlaceHolder.FindControl("divartpostflightcontent");
            control.Attributes["class"] = "art-postflight-setting-content_rpt";
          
            Panel pnlFloater = (Panel)contentPlaceHolder.FindControl("pnlFloater");
            pnlFloater.Visible = false;

            RadPanelBar pnlTracker = (RadPanelBar)contentPlaceHolder.FindControl("pnlTracker");
            pnlTracker.Visible = false;

            RadPanelBar pnlException = (RadPanelBar)contentPlaceHolder.FindControl("pnlException");
            pnlException.Visible = false;

            RadPanelBar pnlTrip = (RadPanelBar)contentPlaceHolder.FindControl("pnlTrip");
            pnlTrip.Visible = false;

            Session["TabSelect"] = "PostFlight";
            
            //Panel pnlFloater = (Panel)contentPlaceHolder.FindControl("pnlFloater");
            //Label lblFloatLegNum = (Label)pnlFloater.FindControl("lblFloatLegNum");
            //lblFloatLegNum.Visible = false;
            //((Table)MainContent.FindControl("tblTripHeader")).Visible = false;

            //Menu mainMenu = (Menu)Page.Master.FindControl("NavigationMenu");

            //foreach (MenuItem m in mainMenu.Items)
            //{
            //    m.Enabled = false;
            //}
            //DivExternalForm 
            //Master  
            //HtmlTable master = (TableMaster)this.Master;
            ////master.ShowNavigationControls = false;

            //Master.FindControl(  

           // MasterPagename ms = this.Master;
 
            //Table tbl = (Table)Page.Master.FindControl("tblTripHeader");
            //Table tbl = (Table)Page.Master.FindControl("ctl00_ctl00_MainContent_tblTripHeader");

            //HtmlTable htable = (HtmlTable)this.Master.FindControl("tblTripHeader");
            //htable.Visible = false; 
            //Table tbl1 = (Table)this.Master.FindControl("tblTripHeader");
            //tbl1.Visible = false; 
            //mpTextBox = (TextBox)mpContentPlaceHolder.FindControl("TextBox1");

            //MasterPagename ms = Master as MasterPagename;
            //ms.NavigatorMenu.Items[0].disabled = true;

            //TableMaster master = (TableMaster)this.Master;
            //master.ShowNavigationControls = false;


            //MasterPageFile = "simple2.master";
            //MasterPageFile = "~/Framework/Masters/Site.master";
            //pnlFloater.vis
            //tblTripHeader.vis
            //Panel parentPanel = this.Parent.FindControl("pnlFloater") as Panel;
            //parentPanel.Visible = false;
            //Table parentTable = this.Parent.FindControl("tblTripHeader") as Table;
            //parentTable.Visible = false;

            //Panel panelHide = (Panel)this.Master.FindControl("pnlFloater");
            //panelHide.Visible = false;

            //Table tablelHide = (Table)this.Master.FindControl("tblTripHeader");
            //tablelHide.Visible = false;


            // Gets a reference to a TextBox control inside a ContentPlaceHolder
            //ContentPlaceHolder mpContentPlaceHolder;
            //TextBox mpTextBox;
            //mpContentPlaceHolder =
             //   (ContentPlaceHolder)Master.FindControl("MainContent");
            //if (mpContentPlaceHolder != null)
            //{
            //    Panel pp = Master.FindControl("pnlFloater") as Panel;
            //    if (pp != null)
            //    {
            //        pp.Visible = false;
            //    }

            //    //    mpTextBox = (TextBox)mpContentPlaceHolder.FindControl("TextBox1");
            //    //    if (mpTextBox != null)
            //    //    {
            //    //        mpTextBox.Text = "TextBox found!";
            //    //    }
            //}

            // Gets a reference to a Label control that is not in a 
            // ContentPlaceHolder control
            //Label mpLabel = (Label)Master.FindControl("masterPageLabel");
            //if (mpLabel != null)
            //{
            //    Label1.Text = "Master page label = " + mpLabel.Text;
            //}


           // ContentPlaceHolder cpHolder = this.Master.FindControl("MainContent") as ContentPlaceHolder;
            //Panel p = cpHolder.FindControl("pnlFloater") as Panel;
            //p.Visible = false;
            //Table tablelHide = cpHolder.FindControl("tblTripHeader") as Table;
            //tablelHide.Visible = false;
            //Table tablelHide = (Table)this.Master.FindControl("tblTripHeader");
            //tablelHide.Visible = false;
            

            //Panel panelHide = (Panel)this.Master.FindControl("pnlFloater");

            //panelHide.Visible = false;



            //Panel pp = Master.FindControl("pnlFloater") as Panel;
            //if (pp != null)
            //{
            //    pp.Visible = false;
            //}

            //ContentPlaceHolder cpHolder = this.Master.FindControl("SiteBodyContent") as ContentPlaceHolder;
            //Panel p = cpHolder.FindControl("pnlFloater") as Panel;
            //p.Visible = false;
            
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //MasterPageFile = "simple2.master";
           // MasterPageFile = "~/Framework/Masters/Site.master";
        }

    }
}