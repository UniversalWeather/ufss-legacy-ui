﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.Services;
using System.Web.Script.Services;
using System.Collections;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.ViewModels.Postflight;

namespace FlightPak.Web.Views.Transactions.PostFlight
{
    public partial class PaxPassportVisa : BaseSecuredPage
    {
        private static long PaxID;

        private ExceptionManager exManager;
        public PostflightLogViewModel TripLog = new PostflightLogViewModel();

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["PassengerRequestorID"]))
                        {
                            PaxID = Convert.ToInt64(Request.QueryString["PassengerRequestorID"]);
                            Session["PassengerRequestorID"] = PaxID;
                        }

                        if (!string.IsNullOrEmpty(Request.QueryString["PaxName"]))
                            Page.Title += " - " +  Request.QueryString["PaxName"];

                        if (Request.QueryString["flag"] == null)
                        {
                            if (Session[WebSessionKeys.CurrentPostflightLog] != null)
                            {
                                TripLog = (PostflightLogViewModel)Session[WebSessionKeys.CurrentPostflightLog];
                                if (TripLog != null && TripLog.PostflightMain.TripActionStatus != PostflightService.TripAction.RecordNowLockedForEdit && TripLog.PostflightMain.TripActionStatus != PostflightService.TripAction.NewRecord)
                                {
                                    recordEdit.Visible = false;
                                    recordEditVisa.Visible = false;
                                    btnSubmit.Visible = false;
                                }
                            }
                        }

                        //if (!IsPostBack)
                        //{
                        //    lbMessage.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        //}
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POPassenger);
                }
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable GetAllVisa()
        {
            Hashtable data = new Hashtable();
            data["meta"] = new FlightPak.Web.ViewModels.PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
            data["results"] = null;
            var userPrinciple= TripManagerBase.getUserPrincipal();

            using (MasterCatalogServiceClient Service = new MasterCatalogServiceClient())
            {
                CrewPassengerVisa PaxVisa = new CrewPassengerVisa();
                PaxVisa.PassengerRequestorID = PaxID;
                PaxVisa.CrewID = null;
                var CrewPaxVisaList = Service.GetCrewVisaListInfo(PaxVisa);

                if (CrewPaxVisaList.ReturnFlag == true)
                {
                    List<GetAllCrewPaxVisa> PaxPassengerVisaList = new List<GetAllCrewPaxVisa>();
                    PaxPassengerVisaList = CrewPaxVisaList.EntityList;
                    var paxVisaList = (from p in PaxPassengerVisaList
                                      select new {
                                            CountryCD= p.CountryCD,
                                            p.CountryID,
                                            p.CountryName,
                                            ExpiryDT = p.ExpiryDT.HasValue==true? p.ExpiryDT.Value.ToString(userPrinciple._ApplicationDateFormat):"",
                                            IssueDate = p.IssueDate.HasValue ==true ? p.IssueDate.Value.ToString(userPrinciple._ApplicationDateFormat):"",
                                            p.IssuePlace,
                                            p.Notes,
                                            p.PassengerRequestorID,
                                            p.TypeOfVisa,
                                            p.VisaExpireInDays,
                                            p.VisaID,
                                            p.VisaNum                                                                               
                                      }).ToList();
                    
                    data["meta"] = new FlightPak.Web.ViewModels.PagingMetaData() { Page = 1, Size = paxVisaList.Count, total_items = paxVisaList.Count };
                    data["results"] = paxVisaList;
                    return data;                    
                }                
            }
            return data;
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable GetAllPassports()
        {
            Hashtable data = new Hashtable();
            data["meta"] = new FlightPak.Web.ViewModels.PagingMetaData() { Page = 1, Size = 0, total_items = 0 };
            data["results"] = null;
            var userPrinciple = TripManagerBase.getUserPrincipal();

            using (MasterCatalogServiceClient Service = new MasterCatalogServiceClient())
            {
                List<GetAllCrewPassport> PassengerPassportList = new List<GetAllCrewPassport>();
                CrewPassengerPassport PassengerPassport = new CrewPassengerPassport();
                PassengerPassport.CrewID = null;
                PassengerPassport.PassengerRequestorID = Convert.ToInt64(PaxID);
                var objPassengerPassport = Service.GetCrewPassportListInfo(PassengerPassport);
                if (objPassengerPassport.ReturnFlag == true)
                {
                    PassengerPassportList = objPassengerPassport.EntityList.Where(x => x.PassengerRequestorID == Convert.ToInt64(PaxID) && x.IsDeleted == false).ToList();

                    var paxPassList = (from p in PassengerPassportList
                                      select new
                                      {
                                          CountryCD = p.CountryCD,
                                          CountryID= p.CountryID,
                                          CountryName=p.CountryName,
                                          Choice= p.Choice.HasValue==true? p.Choice.Value : false,
                                          IsDefaultPassport=p.IsDefaultPassport,
                                          IssueCity=p.IssueCity,
                                          IssueDT = p.IssueDT.HasValue==true ? p.IssueDT.Value.ToString(userPrinciple._ApplicationDateFormat):"",
                                          PassengerRequestorID=p.PassengerRequestorID,                                          
                                          PassportExpiryDT = p.PassportExpiryDT.HasValue == true ? p.PassportExpiryDT.Value.ToString(userPrinciple._ApplicationDateFormat) : "",
                                          PassportID=p.PassportID,
                                          PassportNum=p.PassportNum                                          
                                      }).ToList();
                    
                    data["meta"] = new FlightPak.Web.ViewModels.PagingMetaData() { Page = 1, Size = paxPassList.Count, total_items = paxPassList.Count };
                    data["results"] = paxPassList;                    
                }
            }
            return data;
        }
    }
}