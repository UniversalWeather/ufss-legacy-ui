﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PostFlightSearchAllExpenses.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.PostFlight.PostFlightSearchAllExpenses" %>

<%@ Import Namespace="System.Web.Optimization" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register TagPrefix="uc" TagName="ucSearch" Src="~/UserControls/UCSeeAllPostflightMain.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Postflight Search All Expenses</title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="radScriptManager1" runat="server">
        </telerik:RadScriptManager>
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <%=Scripts.Render("~/bundles/jquery") %>
            <%=Scripts.Render("~/bundles/jqgridjs") %>
            <%=Scripts.Render("~/bundles/sitecommon") %>
            <%=Styles.Render("~/bundles/jqgrid") %>
            <%=Scripts.Render("~/bundles/knockout") %>
            <%=Scripts.Render("~/bundles/postflight") %>
            <style>
                .ui-jqgrid .ui-jqgrid-hbox {
                    float: left;
                }
            </style>
            
            <script type="text/javascript">
                var SearchAllExpensesGrid = "#dgSearch";
                var POSearchAllExpensesViewModel = function () {
                    self.POSearchAllExpenses = {};
                    self.GotoDate = ko.observable();
                    var POAllExpense =htmlDecode($("[id$='hdnPOAllExpenseInitializationObject']").val());
                    self.POSearchAllExpenses = ko.mapping.fromJS(JSON.parse(POAllExpense));
                    self.POSearchAllExpenses.LogNum.subscribe(function () {
                        if (IsNullOrEmpty(POSearchAllExpenses.LogNum())) {
                            self.POSearchAllExpenses.POLogID(null);
                            self.POSearchAllExpenses.PostflightLeg(null);
                            $("#lbLogNo").text("");
                            return;
                        };
                        var paramters = { LogNum: POSearchAllExpenses.LogNum() };
                        var btn = document.getElementById("btnBrowseLogs");
                        Start_Loading(btn);
                        $.ajax({
                            type: "POST",
                            cache: false,
                            url: '/Views/Transactions/PostFlight/PostFlightValidator.aspx/LogNo_PostflightValidate_Retrieve_KO',
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify(paramters),
                            success: function (response) {
                                var returnResult = verifyReturnedResult(response);
                                if (returnResult == true && response.d.Success == true) {
                                    ko.mapping.fromJS(response.d.Result.PostflightLeg, {}, self.POSearchAllExpenses.PostflightLeg);
                                    $("#lbLogNo").text("");
                                    End_Loading(btn, true);
                                }
                                else {
                                    self.POSearchAllExpenses.POLogID(null);
                                    $("#lbLogNo").text("Invalid Log No !");
                                    End_Loading(btn, false);
                                }
                            },
                            complete: function (jqXHR, textStatus) {
                            },
                            error: function (xhr, textStatus, errorThrown) {
                                End_Loading('#btnBrowseLogs', false);
                                reportPostflightError(xhr, textStatus, errorThrown);
                            }
                        });
                    });
                };
                $(document).ready(function () {
                    SearchAllExpenses();
                    AppdateFormat = self.UserPrincipal._ApplicationDateFormat;
                    $("#btnSubmit").click(function () {
                        var selectedrows = $(SearchAllExpensesGrid).jqGrid('getGridParam', 'selrow');
                        if (selectedrows) {
                            var rowData = $(SearchAllExpensesGrid).getRowData(selectedrows);
                            var lastSel = rowData['POLogID'];
                            var SlipNUM = rowData['SlipNUM'];
                            $("#hdSelectedPOLogID").val(lastSel);
                            $("#hdSelectedSlipNUM").val(SlipNUM);
                            returnToParent();
                        }
                        else { showMessageBox('Please select a log.', popupTitle); }
                        return false;
                    });
                });
                function dataFormatter(cellvalue, options, rowObject) {
                    return moment(cellvalue).format(AppdateFormat.toUpperCase());
                }
                function reloadPageSize() {
                    var myGrid = $(SearchAllExpensesGrid);
                    var currentValue = $("#rowNum").val();
                    myGrid.setGridParam({ rowNum: currentValue });
                    myGrid.trigger('reloadGrid');
                }
                function AirportValidator_KO() {
                    var btn = document.getElementById("btnICAO");
                    var airportErrorLabel = "#lbICAO";
                    Airport_Validate_Retrieve_KO(self.POSearchAllExpenses.Airport, self.POSearchAllExpenses.AirportID, airportErrorLabel, btn);
                }
                function AccountValidator_KO() {
                    var btn = document.getElementById("btnAccountNo");
                    var accountErrorLabel = "lbAccountNumber";
                    Account_Validate_Retrieve_KO(self.POSearchAllExpenses.Account, self.POSearchAllExpenses.AccountID, accountErrorLabel, btn);
                }
                function CrewValidator_KO() {
                    var btn = document.getElementById("btnCrew");
                    var crewErrorLabel = "lbcvCrew";
                    Crew_Validate_Retrieve_KO(self.POSearchAllExpenses.Crew, self.POSearchAllExpenses.CrewID, crewErrorLabel, btn);
                }
                function PaymentVendorValidator_KO() {
                    var btn = document.getElementById("btnPayVendor");
                    var vendorErrorLabel = "lbPayVendor";
                    PayableVendor_Validate_Retrieve_KO(self.POSearchAllExpenses.PayableVendor, self.POSearchAllExpenses.PaymentVendorID, vendorErrorLabel, btn);
                }
                function PaymentTypeValidator_KO() {
                    var btn = document.getElementById("btnPaymentType");
                    var paymentTypeErrorLabel = "lbPaymentType";
                    PaymentType_Validate_Retrieve_KO(self.POSearchAllExpenses.PaymentType, self.POSearchAllExpenses.PaymentTypeID, paymentTypeErrorLabel, btn);
                }
                function FuelLocatorValidator_KO() {
                    var btn = document.getElementById("btnFuelLocator");
                    var fuelErrorLabel = "lbFuelLocator";
                    FuelLocator_Validate_Retrieve_KO(self.POSearchAllExpenses.FuelLocator, self.POSearchAllExpenses.FuelLocatorID, fuelErrorLabel, btn);
                }
                function TailNumValidator_KO() {
                    var btn = document.getElementById("btnTailNo");
                    var fleetErrorlabel = "lbcvTailNumber";
                    TailNum_Validate_Retrieve_KO(self.POSearchAllExpenses.Fleet, self.POSearchAllExpenses.FleetID, fleetErrorlabel, btn);
                }
                function SearchAllExpenses() {
                    jQuery(SearchAllExpensesGrid).jqGrid({
                        url: '/Views/Transactions/PostFlight/PostFlightSearchAllExpenses.aspx/SearchAllExpensesDataBind',
                        mtype: 'POST',
                        datatype: "json",
                        cache: false,
                        async: true,
                        loadonce: true,
                        ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                        serializeGridData: function (postData) {
                            if (postData._search == undefined || postData._search == false) {
                                if (postData.filters === undefined) postData.filters = null;
                            }
                            var homebaseID = false;
                            if ($('#chkHomebase').prop("checked")) {
                                homebaseID = true;
                            }
                            var goToDTTM = null;
                            if ($('#tbGoToDate').val()) {
                                goToDTTM = moment($('#tbGoToDate').val(), self.UserPrincipal._ApplicationDateFormat.toUpperCase()).format(isodateformat);
                            }
                            var fuelSelected = '0';
                            if (!IsNullOrEmpty($('input:radio[name=rblSearch]:checked'))) {
                                fuelSelected = $('input:radio[name=rblSearch]:checked').val();
                            }
                            var tailNum = '';
                            if (!IsNullOrEmpty($("#tbTailNumber").val())) {
                                tailNum = $("#tbTailNumber").val();
                            }
                            var icaoID = '';
                            if (!IsNullOrEmpty($("#tbICAO").val())) {
                                icaoID = $("#tbICAO").val();
                            }
                            var logID = null;
                            if (!IsNullOrEmpty($("#hdnPOLogID").val())) {
                                logID = $("#hdnPOLogID").val();
                            }
                            var legID = null;
                            if($('#ddlLegs').find('option:selected').val()!=null)
                            {
                                legID = $('#ddlLegs').find('option:selected').val();
                            }
                            var paymentTypeID = '';
                            if (!IsNullOrEmpty($("#tbPaymentType").val())) {
                                paymentTypeID = $("#tbPaymentType").val();
                            }
                            var payableVendorID = '';
                            if (!IsNullOrEmpty($("#tbPayVendor").val())) {
                                payableVendorID = $("#tbPayVendor").val();
                            }
                            var fuelLocID = '';
                            if (!IsNullOrEmpty($("#tbFuelLocator").val())) {
                                fuelLocID = $("#tbFuelLocator").val();
                            }
                            var accNoID = '';
                            if (!IsNullOrEmpty($("#tbAccountNumber").val())) {
                                accNoID = $("#tbAccountNumber").val();
                            }
                            var invoiceNo = '';
                            if (!IsNullOrEmpty($("#tbInvoiceNo").val())) {
                                invoiceNo = $("#tbInvoiceNo").val();
                            }
                            var expAmount = '';
                            if (!IsNullOrEmpty($("#tbExpAmount").val())) {
                                expAmount = $("#tbExpAmount").val();
                            }
                            var crewCD = '';
                            if (!IsNullOrEmpty($("#tbCrew").val())) {
                                crewCD = $("#tbCrew").val();
                            }
                            var crewID = '';
                            if (!IsNullOrEmpty($("#hdnCrew").val())) {
                                crewID = $("#hdnCrew").val();
                            }
                            postData.homeBaseOnly = homebaseID
                            postData.GoToDTTM = goToDTTM;
                            postData.FuelSelected = fuelSelected;
                            postData.TailNum = tailNum
                            postData.icaoID = icaoID;
                            postData.LogID = logID;
                            postData.legID = legID
                            postData.PaymentTypeID = paymentTypeID;
                            postData.payableVendorID = payableVendorID;
                            postData.FuelLocID = fuelLocID;
                            postData.AccNoID = accNoID
                            postData.InvoiceNo = invoiceNo;
                            postData.ExpAmount = expAmount;
                            postData.CrewCD = crewCD
                            postData.crewID = crewID;
                            return JSON.stringify(postData);
                        },
                        autowidth: false,
                        width: 950,
                        rowNum: $("#rowNum").val(),
                        pager: "#pg_gridPager",
                        shrinkToFit: false,
                        ignoreCase: true,
                        colNames: ['LogNo', 'PostflightExpenseID', 'Log No.', 'Leg No.', 'Invoice No.', 'Slip No.', 'Purchase Date', 'Tail No.'
                            , 'ICAO', 'Account Description', 'Expense Amount', 'Fuel Quantity', 'Unit Price', 'Posted Price'
                            , 'Fuel Locator', 'Home Base', 'Crew', 'CrewId'
                        ],
                        colModel: [
                            { name: 'POLogID', index: 'POLogID', hidden: true },
                            { name: 'PostflightExpenseID', index: 'PostflightExpenseID', hidden: true },
                            { name: 'LogNum', index: 'LogNum', width: 70, sorttype: 'number' },
                            { name: 'LegNUM', index: 'LegNUM', width: 70, sorttype: 'number' },
                            { name: 'InvoiceNUM', index: 'InvoiceNUM', width: 60 },
                            { name: 'SlipNUM', index: 'SlipNUM', width: 70, sorttype: 'number' },
                            {
                                name: 'PurchaseDT', index: 'PurchaseDT', width: 80, formatter: dataFormatter, sorttype: 'date', searchoptions: { sopt: ['eq'] },datefmt: self.UserPrincipal._ApplicationDateFormat.replace('yyyy', 'y').replace('MM', 'M').replace('dd', 'd')
                            },
                            { name: 'TailNum', index: 'TailNum', width: 70 },
                            { name: 'IcaoID', index: 'IcaoID', width: 70 },
                            { name: 'AccountDescription', index: 'AccountDescription', width: 170 },
                            { name: 'ExpenseAMT', index: 'ExpenseAMT', width: 70, sorttype: 'number' },
                            { name: 'FuelQTY', index: 'FuelQTY', width: 70, sorttype: 'number' },
                            { name: 'UnitPrice', index: 'UnitPrice', width: 53, sorttype: 'number' },
                            { name: 'PostFuelPrice', index: 'PostFuelPrice', width: 60, sorttype: 'number' },
                            { name: 'FuelLocatorCD', index: 'FuelLocatorCD', width: 68 },
                            { name: 'HomebaseCD', index: 'HomebaseCD', width: 60 },
                            { name: 'CrewCD', index: 'CrewCD', width: 60 },
                            { name: 'CrewID', index: 'CrewID', width: 90, sorttype: 'number' },
                        ],
                        ondblClickRow: function (rowId) {
                            var rowData = jQuery(this).getRowData(rowId);
                            var lastSel = rowData['POLogID'];
                            var SlipNUM = rowData['SlipNUM'];
                            $("#hdSelectedPOLogID").val(lastSel);
                            $("#hdSelectedSlipNUM").val(SlipNUM);
                            returnToParent();
                        },
                        onSelectRow: function (id) {
                            var rowData = $(this).getRowData(id);
                            var lastSel = rowData['POLogID'];

                            if (id !== lastSel) {
                                $(this).find(".selected").removeClass('selected');
                                $('#results_table').jqGrid('resetSelection', lastSel, true);
                                $(this).find('.ui-state-highlight').addClass('selected');
                                lastSel = id;
                            }
                        },
                        viewrecords: true
                    });
                    jQuery("#btnSearch").click(function () {
                        jQuery(SearchAllExpensesGrid).setGridParam({ datatype: 'json' });
                        jQuery(SearchAllExpensesGrid).trigger("reloadGrid");
                    });
                    $(SearchAllExpensesGrid).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
                    $("#pagesizebox").insertBefore('.ui-paging-info');
                }

                function returnToParent() {
                    oArg = new Object();
                    var selectedRows = $(SearchAllExpensesGrid).jqGrid('getGridParam', 'selrow');

                    if (selectedRows.length > 0) {
                        oArg.POLogID = $("#hdSelectedPOLogID").val();
                        oArg.SlipNUM = $("#hdSelectedSlipNUM").val();
                    }
                    else {
                        oArg.POLogID = "";
                        oArg.SlipNUM = "";
                    }
                    var oWnd = GetRadWindow();
                    if (oArg) {
                        oWnd.close(oArg);
                    }
                }

                //this function is used to close this window
                function CloseAndRebind(arg) {
                    GetRadWindow().Close();
                    GetRadWindow().BrowserWindow.refreshPage(arg);
                }

                //this function is used to navigate to pop up screen's with the selected code
                function openWin(radWin) {
                    var url = '';
                    if (radWin == "radFleetProfilePopup") {
                        url = '/Views/Settings/Fleet/FleetProfilePopup.aspx?TailNumber=' + document.getElementById('tbTailNumber').value + '&FromPage=postflight';
                    }
                    else if (radWin == "radAirportPopup") {
                        url = '/Views/Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('tbICAO').value;
                    }
                    else if (radWin == "radPaymentTypePopup") {
                        url = '/Views/Settings/Company/PaymentTypePopup.aspx';
                    }
                    else if (radWin == "radPayableVendorPopup") {
                        url = '/Views/Settings/Logistics/PayableVendorPopUp.aspx';
                    }
                    else if (radWin == "radFuelLocatorPopup") {
                        url = '/Views/Settings/Logistics/FuelLocatorPopup.aspx';
                    }
                    else if (radWin == "radAccountMasterPopup") {
                        url = '/Views/Settings/Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById('tbAccountNumber').value;
                    }
                    else if (radWin == "radLogPopup") {
                        url = '/Views/Transactions/PostFlight/PostFlightSearchAll.aspx?IsPopup=YES';
                    }
                    else if (radWin == "radCrewPopup") {
                        url = '/Views/Transactions/PostFlight/PostFligtCrewRosterPopUp.aspx?CrewCD=' + document.getElementById('tbCrew').value;
                    }

                    var oWnd = radopen(url, radWin);
                }

                // this function is used to display the value of selected TailNumber from popup
                function OnClientTailNoClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            var btn = document.getElementById("btnTailNo");
                            Start_Loading(btn);
                            self.POSearchAllExpenses.Fleet.TailNum(arg.TailNum);
                            self.POSearchAllExpenses.FleetID(arg.FleetId);
                            $('#lbcvTailNumber').text('');
                            End_Loading(btn, true);
                        }
                        else {
                            self.POSearchAllExpenses.Fleet.TailNum("");
                            self.POSearchAllExpenses.FleetID("");
                        }
                    }
                }

                function OnAirportClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            var btn = document.getElementById("btnICAO");
                            Start_Loading(btn);
                            self.POSearchAllExpenses.Airport.IcaoID(arg.ICAO);
                            self.POSearchAllExpenses.AirportID(arg.AirportID);
                            self.POSearchAllExpenses.Airport.AirportName(arg.AirportName);
                            $('#lbICAO').text('');
                            End_Loading(btn, true);
                        }
                        else {
                            self.POSearchAllExpenses.Airport.IcaoID("");
                            self.POSearchAllExpenses.AirportID("");
                            self.POSearchAllExpenses.Airport.AirportName("");
                        }
                    }
                }

                function OnPayTypeClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            var btn = document.getElementById("btnPaymentType");
                            Start_Loading(btn);
                            self.POSearchAllExpenses.PaymentType.PaymentTypeCD(arg.PaymentTypeCD);
                            self.POSearchAllExpenses.PaymentTypeID(arg.PaymentTypeID);
                            self.POSearchAllExpenses.PaymentType.PaymentTypeDescription(arg.PaymentTypeDescription);
                            $('#lbPaymentType').text('');
                            End_Loading(btn, true);
                        }
                        else {
                            self.POSearchAllExpenses.PaymentType.PaymentTypeCD("");
                            self.POSearchAllExpenses.PaymentTypeID("");
                            self.POSearchAllExpenses.PaymentType.PaymentTypeDescription("");
                        }
                    }
                }

                function OnVendorPayClose(oWnd, args) {

                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            var btn = document.getElementById("btnPayVendor");
                            Start_Loading(btn);
                            self.POSearchAllExpenses.PayableVendor.VendorCD(arg.VendorCD);
                            self.POSearchAllExpenses.PaymentVendorID(arg.VendorID);
                            self.POSearchAllExpenses.PayableVendor.Name(arg.Name);
                            $('#lbPayVendor').text('');
                            End_Loading(btn, true);
                        }
                        else {
                            self.POSearchAllExpenses.PayableVendor.VendorCD("");
                            self.POSearchAllExpenses.PaymentVendorID("");
                            self.POSearchAllExpenses.PayableVendor.Name("");
                        }
                    }
                }

                function OnFuelLocatorClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            var btn = document.getElementById("btnFuelLocator");
                            Start_Loading(btn);
                            self.POSearchAllExpenses.FuelLocator.FuelLocatorCD(arg.FuelLocatorCD);
                            self.POSearchAllExpenses.FuelLocatorID(arg.FuelLocatorID);
                            self.POSearchAllExpenses.FuelLocator.FuelLocatorDescription(arg.FuelLocatorDescription);
                            $('#lbFuelLocator').text('');
                            End_Loading(btn, true);
                        }
                        else {
                            self.POSearchAllExpenses.FuelLocator.FuelLocatorCD("");
                            self.POSearchAllExpenses.FuelLocatorID("");
                            self.POSearchAllExpenses.FuelLocator.FuelLocatorDescription("");
                        }
                    }
                }

                function OnCrewRosterClose(oWnd, args) {
                    //get the transferred arguments
                    if (args !== null && args.get_argument() != null) {
                        var arg = args.get_argument().val[0];
                        if (arg) {
                            var btn = document.getElementById("btnCrew");
                            Start_Loading(btn);
                            self.POSearchAllExpenses.Crew.CrewCD(arg.CrewCD);
                            self.POSearchAllExpenses.CrewID(arg.CrewID);
                            self.POSearchAllExpenses.Crew.DisplayName(arg.CrewFirstName);
                            $('#lbcvCrew').text('');
                            End_Loading(btn, true);
                        }
                        else {
                            self.POSearchAllExpenses.Crew.CrewCD("");
                            self.POSearchAllExpenses.CrewID("");
                            self.POSearchAllExpenses.Crew.DisplayName("");
                        }
                    }
                }

                function OnClientAccountClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            var btn = document.getElementById("btnAccountNo");
                            Start_Loading(btn);
                            self.POSearchAllExpenses.Account.AccountNum(arg.AccountNum);
                            self.POSearchAllExpenses.Account.AccountDescription(arg.AccountDescription);
                            self.POSearchAllExpenses.AccountID(arg.AccountId);
                            var isbilling = (arg.IsBilling != null && arg.IsBilling == "True");
                            self.POSearchAllExpenses.IsBilling(isbilling);
                            $('#lbAccountNumber').text('');
                            End_Loading(btn, true);
                        }
                        else {
                            self.POSearchAllExpenses.Account.AccountNum("");
                            self.POSearchAllExpenses.Account.AccountDescription("");
                            self.POSearchAllExpenses.AccountID("");
                            self.POSearchAllExpenses.IsBilling(false);
                        }
                    }
                }

                function OnClientLogClose(oWnd, args) {
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            self.POSearchAllExpenses.LogNum(arg.LogNum);
                            self.POSearchAllExpenses.POLogID(arg.POLogID);
                            $('#lbLogNo').text('');
                        }
                        else {
                            self.POSearchAllExpenses.LogNum("");
                            self.POSearchAllExpenses.POLogID("");
                        }
                    }
                }
            </script>
        </telerik:RadCodeBlock>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true"
            ShowContentDuringLoad="false">
            <Windows>
                <telerik:RadWindow ID="radFleetProfilePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientTailNoClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnAirportClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radPaymentTypePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnPayTypeClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/PaymentTypePopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radPayableVendorPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnVendorPayClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/PayableVendorPopUp.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radFuelLocatorPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnFuelLocatorClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Logistics/FuelLocatorPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radAccountMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientAccountClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radLogPopup" runat="server" Width="700px" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    NavigateUrl="~/Views/Transactions/PostFlight/PostFlightSearchAll.aspx" OnClientClose="OnClientLogClose">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radCrewPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnCrewRosterClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/PostFlight/PostFligtCrewRosterPopUp.aspx">
                </telerik:RadWindow>
            </Windows>
            <ConfirmTemplate>
                <div class="rwDialogPopup radconfirm">
                    <div class="rwDialogText">
                        {1}
                    </div>
                    <div>
                        <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                        <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    </div>
                </div>
            </ConfirmTemplate>
            <AlertTemplate>
                <div class="rwDialogPopup radalert">
                    <div class="rwDialogText">
                        {1}
                    </div>
                    <div>
                        <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                        </a>
                    </div>
                </div>
            </AlertTemplate>
        </telerik:RadWindowManager>
        <div id="DivExternalForm" runat="server">
            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
            <table cellspacing="0" cellpadding="0" class="box1">
                <tr>
                    <td>
                        <table id="tblSearchForm" runat="server" border="0">
                            <tr>
                                <td valign="top">
                                    <fieldset>
                                        <legend>Search</legend>
                                        <table border="0">
                                            <tr>
                                                <td valign="top" class="tdLabel200">
                                                    <table border="0">
                                                        <tr>
                                                            <td colspan="2">
                                                                <input type="checkbox" id="chkHomebase"/><label for="chkHomebase">Home Base Only</label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdLabel50">Go To
                                                            </td>
                                                            <td>
                                                                <input id="tbGoToDate" maxlength="10" class="text80" type="text" data-bind="datepicker: GotoDate" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <table id="rblSearch">
                                                                <tr>
                                                                    <td><input id="rblSearch_0" type="radio" name="rblSearch" value="0" checked="checked" /><label for="rblSearch_0">Include All</label></td>
                                                                    <td><input id="rblSearch_1" type="radio" name="rblSearch" value="1" /><label for="rblSearch_1">Fuel Only</label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input id="rblSearch_2" type="radio" name="rblSearch" value="2" /><label for="rblSearch_2">Exclude Fuel</label></td>
                                                                    <td></td>
                                                                </tr>
                                                                </table>                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top">
                                                    <table>
                                                        <tr>
                                                            <td valign="top">
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel50">Tail No.
                                                                        </td>
                                                                        <td class="tdLabel100">
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:HiddenField ID="hdnPOAllExpenseInitializationObject" runat="server" />
                                                                                        <input id="tbTailNumber" maxlength="9" type="text" class="text60" data-bind=" value: POSearchAllExpenses.Fleet.TailNum, event: { change: TailNumValidator_KO }" />
                                                                                        <input type="hidden" id="hdnTailNo" data-bind="value: POSearchAllExpenses.FleetID" />
                                                                                        <input type="hidden" id="hdnAircraftID" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="button" id="btnTailNo" class="browse-button" onclick="javascript: openWin('radFleetProfilePopup'); return false;" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <label id="lbcvTailNumber" class="alert-text"></label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel50">ICAO
                                                                        </td>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" id="tblICAO">
                                                                                <tr>
                                                                                    <td>
                                                                                        <input id="tbICAO" type="text" class="text60" maxlength="8" onkeypress="return fnAllowAlphaNumeric(this, event)" data-bind="value: POSearchAllExpenses.Airport.IcaoID, event: { change: AirportValidator_KO }" />
                                                                                        <input type="hidden" id="hdnICAO" data-bind="value: POSearchAllExpenses.AirportID" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="button" id="btnICAO" class="browse-button" onclick="javascript: openWin('radAirportPopup'); return false;" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <label id="lbICAODesc" class="input_no_bg" data-bind="text: POSearchAllExpenses.Airport.AirportName"></label>
                                                                                        <label id="lbICAO" class="alert-text"></label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel50">Log No.
                                                                        </td>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" id="Table1">
                                                                                <tr>
                                                                                    <td>
                                                                                        <input id="tbLogNo" type="text" class="text60" maxlength="8" onkeypress="return fnAllowNumeric(this, event)" data-bind="value: POSearchAllExpenses.LogNum" />
                                                                                        <input type="hidden" id="hdnPOLogID" data-bind="value: POSearchAllExpenses.POLogID" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="button" id="btnBrowseLogs" class="browse-button" onclick="javascript: openWin('radLogPopup'); return false;" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <label id="lbLogNo" class="alert-text"></label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="tdLabel50">Leg No.
                                                                        </td>
                                                                        <td>
                                                                            <select id="ddlLegs" style="width: 65px" data-bind="options: POSearchAllExpenses.PostflightLeg, optionsText: 'LegNUM', optionsValue: 'POLegID', value: POSearchAllExpenses.PostflightLeg.LegNUM"></select>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top">
                                                    <table>
                                                        <tr>
                                                            <td valign="top">
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel100">Payment Type
                                                                        </td>
                                                                        <td class="tdLabel100">
                                                                            <table cellpadding="0" cellspacing="0" id="tblPaymentType">
                                                                                <tr>
                                                                                    <td>
                                                                                        <input id="tbPaymentType" type="text" class="text60" maxlength="8" onkeypress="return fnAllowAlphaNumeric(this, event)" data-bind="value: POSearchAllExpenses.PaymentType.PaymentTypeCD, event: { change: PaymentTypeValidator_KO }" />
                                                                                        <input type="hidden" id="hdnPaymentType" data-bind="value: POSearchAllExpenses.PaymentTypeID" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="button" id="btnPaymentType" class="browse-button" onclick="javascript: openWin('radPaymentTypePopup'); return false;" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <label id="lbPaymentTypeDesc" class="input_no_bg" data-bind="text: POSearchAllExpenses.PaymentType.PaymentTypeDescription"></label>
                                                                                        <label id="lbPaymentType" class="alert-text"></label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="tdLabel100">Payables Vendor
                                                                        </td>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" id="tblPayVendor">
                                                                                <tr>
                                                                                    <td>
                                                                                        <input id="tbPayVendor" type="text" class="text60" maxlength="8" onkeypress="return fnAllowAlphaNumeric(this, event)" data-bind="value: POSearchAllExpenses.PayableVendor.VendorCD, event: { change: PaymentVendorValidator_KO }" />
                                                                                        <input type="hidden" id="hdnPayVendor" data-bind="value: POSearchAllExpenses.PaymentVendorID" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="button" id="btnPayVendor" class="browse-button" onclick="javascript: openWin('radPayableVendorPopup'); return false;" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <table cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <label id="lbPayVendorDesc" class="input_no_bg" data-bind="text: POSearchAllExpenses.PayableVendor.Name"></label>
                                                                                        <label id="lbPayVendor" class="alert-text"></label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel100">Fuel Locator
                                                                        </td>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" id="tblFuelLocator">
                                                                                <tr>
                                                                                    <td>
                                                                                        <input id="tbFuelLocator" type="text" class="text60" maxlength="8" onkeypress="return fnAllowAlphaNumeric(this, event)" data-bind="value: POSearchAllExpenses.FuelLocator.FuelLocatorCD, event: { change: FuelLocatorValidator_KO }" />
                                                                                        <input type="hidden" id="hdnFuelLocator" data-bind="value: POSearchAllExpenses.PaymentTypeID" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="button" id="btnFuelLocator" class="browse-button" onclick="javascript: openWin('radFuelLocatorPopup'); return false;" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <label id="lbFuelLocatorDesc" class="input_no_bg" data-bind="text: POSearchAllExpenses.FuelLocator.FuelLocatorDescription"></label>
                                                                            <label id="lbFuelLocator" class="alert-text"></label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel100">Account No.
                                                                        </td>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" id="tblAccount">
                                                                                <tr>
                                                                                    <td>
                                                                                        <input id="tbAccountNumber" type="text" class="text60" maxlength="8" onkeypress="return fnAllowNumericAndChar(this, event)" data-bind="value: POSearchAllExpenses.Account.AccountNum, event: { change: AccountValidator_KO }" />
                                                                                        <input type="hidden" id="hdnAccountNumber" data-bind="value: POSearchAllExpenses.AccountID" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="button" id="btnAccountNo" class="browse-button" onclick="javascript: openWin('radAccountMasterPopup'); return false;" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <label id="lbAccountNumber" class="alert-text"></label>
                                                                            <label id="lbAccDescription" class="input_no_bg" data-bind="text: POSearchAllExpenses.Account.AccountDescription"></label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top">
                                                    <table>
                                                        <tr>
                                                            <td valign="top">
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel100">Invoice No.
                                                                        </td>
                                                                        <td>
                                                                            <input id="tbInvoiceNo" type="text" class="text60" maxlength="25" onkeypress="return fnAllowAlphaNumeric(this, event)" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel100">Exp. Amount
                                                                        </td>
                                                                        <td>
                                                                            <input id="tbExpAmount" type="text" class="text60" maxlength="14" onkeypress="return fnAllowNumeric(this, event)" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top">
                                                                <table cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td class="tdLabel100">Crew
                                                                        </td>
                                                                        <td>
                                                                            <table cellpadding="0" cellspacing="0" id="Table2">
                                                                                <tr>
                                                                                    <td>
                                                                                        <input id="tbCrew" type="text" class="text60" maxlength="8" onkeypress="return fnAllowAlphaNumeric(this, event)" data-bind="value: POSearchAllExpenses.Crew.CrewCD, event: { change: CrewValidator_KO }" />
                                                                                        <input type="hidden" id="hdnCrew" data-bind="value: POSearchAllExpenses.CrewID" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="button" id="btnCrew" class="browse-button" onclick="javascript: openWin('radCrewPopup'); return false;" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <label id="lbCrewDesc" class="input_no_bg" data-bind="text: POSearchAllExpenses.Crew.DisplayName"></label>
                                                                            <label id="lbcvCrew" class="alert-text"></label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </td>
                                <td valign="middle">
                                    <input type="button" class="button" value="Search" id="btnSearch" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="postflight-custom-grid">
                                    <div class="jqgrid">
                                        <div>
                                            <table class="box1">
                                                <tr>
                                                    <td>
                                                        <table id="dgSearch"></table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input id="hdSelectedPOLogID" runat="server" type="hidden" />
                                                        <input id="hdSelectedSlipNUM" runat="server" type="hidden" />
                                                        <div class="grid_icon">
                                                            <div role="group" id="pg_gridPager"></div>
                                                            <div id="pagesizebox">
                                                                <span>Size:</span>
                                                                <input class="psize" id="rowNum" type="text" value="10" maxlength="5" />
                                                                <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(); return false;" />
                                                            </div>
                                                        </div>
                                                        <div style="padding: 5px 5px; text-align: right;">
                                                            <input id="btnSubmit" type="button" class="button okButton" value="OK" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <label id="lbMessage" class="alert-text"></label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
