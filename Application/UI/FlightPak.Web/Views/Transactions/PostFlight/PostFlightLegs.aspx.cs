﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Web.UI;
using System.Collections;
using FlightPak.Web.ViewModels;
using FlightPak.Web.ViewModels.Postflight;
using Newtonsoft.Json;
using FlightPak.Web.Framework.Constants;
using System.Web;

namespace FlightPak.Web.Views.Transactions.PostFlight
{
    public partial class PostFlightLegs : BaseSecuredPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PostflightTripManager.CheckSessionAndRedirect();
            if (!IsPostBack)
            {
                PostflightTripManager.ValidatePostflightSession("Leg");
                FSSOperationResult<Hashtable> legsHashtable = new FSSOperationResult<Hashtable>();
                PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
                if (Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    pfViewModel = (PostflightLogViewModel)Session[WebSessionKeys.CurrentPostflightLog];
                    legsHashtable = PostflightTripManager.InitializePostflightLegs(Convert.ToInt32(pfViewModel.CurrentLegNUM));
                }
                else
                {
                    pfViewModel.PostflightMain = new PostflightMainViewModel(PostflightTripManager.getUserPrincipal()._ApplicationDateFormat);
                    pfViewModel.PostflightMain = PostflightTripManager.CreateOrCancelTrip(pfViewModel.PostflightMain);
                    legsHashtable = PostflightTripManager.InitializePostflightLegs(1);
                }
                hdnPostflightMainInitializationObject.Value = Microsoft.Security.Application.Encoder.HtmlEncode(JsonConvert.SerializeObject(pfViewModel));
                UserPrincipalViewModel userPrincipal = PostflightTripManager.getUserPrincipal();
                hdnUserPrincipalInitializationObject.Value = Microsoft.Security.Application.Encoder.HtmlEncode(JsonConvert.SerializeObject(userPrincipal));

                hdnPostflightLegInitializationObject.Value = Microsoft.Security.Application.Encoder.HtmlEncode(JsonConvert.SerializeObject(legsHashtable));
                PostflightTripManager.ExceptionLimitLevelUpdate(1);
                
            }
        }        
    }
}


