﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.PostflightService;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Text.RegularExpressions;
using System.Text;
using FlightPak.Web.Views.Transactions.PostFlight;
using FlightPak.Web.ViewModels.Postflight;
using FlightPak.Web.Framework.Constants;
using Newtonsoft.Json;
using FlightPak.Web.ViewModels;
using System.Collections;

namespace FlightPak.Web.Views.Settings.PostFlight
{
    public partial class PostFlightCrew : BaseSecuredPage
    {       
         /// <summary>
        /// Page Load Events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            PostflightTripManager.CheckSessionAndRedirect();
            if (!IsPostBack)
            {
                PostflightTripManager.ExceptionLimitLevelUpdate(0);
            }

            PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
            if (Session[WebSessionKeys.CurrentPostflightLog] != null)
            {
                pfViewModel = (PostflightLogViewModel)Session[WebSessionKeys.CurrentPostflightLog];
                FSSOperationResult<Hashtable> ret = PostflightTripManager.InitializePostflightLegs(Convert.ToInt32(pfViewModel.CurrentLegNUM));
                //PostflightLegViewModel firstLeg = ret.Result["CurrentLeg"] as PostflightLegViewModel;
                hdnPostflightLegObject.Value = Microsoft.Security.Application.Encoder.HtmlEncode(ret != null ? JsonConvert.SerializeObject(ret) : "");
            }
            else
            {
                pfViewModel.PostflightMain = new PostflightMainViewModel(PostflightTripManager.getUserPrincipal()._ApplicationDateFormat);
                pfViewModel.PostflightMain = PostflightTripManager.CreateOrCancelTrip(pfViewModel.PostflightMain);
            }
            hdnPostflightMainInitializationObject.Value = Microsoft.Security.Application.Encoder.HtmlEncode(JsonConvert.SerializeObject(pfViewModel));
            UserPrincipalViewModel userPrincipal = PostflightTripManager.getUserPrincipal();
            hdnUserPrincipalInitializationObject.Value = Microsoft.Security.Application.Encoder.HtmlEncode(JsonConvert.SerializeObject(userPrincipal));
        }
    }
}
