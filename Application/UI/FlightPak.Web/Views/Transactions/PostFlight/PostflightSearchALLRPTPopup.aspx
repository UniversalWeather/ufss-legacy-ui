﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PostflightSearchALLRPTPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.PostFlight.PostflightSearchALLRPTPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="uc" TagName="ucSearch" Src="~/UserControls/UCSeeAllPostflightMain.ascx" %>
<%@ MasterType VirtualPath="~/Framework/Masters/PostFlight.Master" %>
<%@ Register TagPrefix="uc" TagName="DatePicker" Src="~/UserControls/UCDate.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Postflight Search All</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="radScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            //this function is used to navigate to pop up screen's with the selected code
            function openWin(radWin) {
                var url = '';
                if (radWin == "RadClientCodePopup") {
                    url = '/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById("<%=tbClientCode.ClientID%>").value;
                }
                else if (radWin == "radFleetProfilePopup") {
                    url = '/Views/Settings/Fleet/FleetProfilePopup.aspx';
                }
                var oWnd = radopen(url, radWin);
            }
            // this function is used to display the value of selected Client code from popup
            function ClientCidePopupClose(oWnd, args) {
                var combo = $find("<%= tbClientCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClientCode.ClientID%>").value = arg.ClientCD;
                        document.getElementById("<%=hdnClient.ClientID%>").value = arg.ClientID;
                    }
                    else {
                        document.getElementById("<%=tbClientCode.ClientID%>").value = "";
                        document.getElementById("<%=hdnClient.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            // this function is used to display the value of selected TailNumber from popup
            function OnClientTailNoClose(oWnd, args) {
                var combo = $find("<%= tbTailNumber.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbTailNumber.ClientID%>").value = arg.TailNum;

                    }
                    else {
                        document.getElementById("<%=tbTailNumber.ClientID%>").value = "";

                    }
                }


            }


            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgSearch.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];

                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "LogNum");

                        if (i == 0) {

                            oArg.LogNum = cell1.innerHTML + ",";

                        }
                        else {

                            oArg.LogNum += cell1.innerHTML + ",";

                        }
                    }
                }
                else {

                    oArg.LogNum = "";

                }
                if (oArg.LogNum != "")
                    oArg.LogNum = oArg.LogNum.substring(0, oArg.LogNum.length - 1)
                else
                    oArg.LogNum = "";

                oArg.Arg1 = oArg.LogNum;
                oArg.CallingButton = "LogNum";
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }


            // this function is used to get the dimensions
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }


            //this function is used to close this window
            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                //               GetRadWindow().BrowserWindow.refreshPage(arg);
            }

            //this function is used to set the frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                return oWindow;
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tblMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbSearchClientCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tbSearchClientCode" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel2" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="ClientCidePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFleetProfilePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientTailNoClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFleetProfileCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <table cellspacing="0" cellpadding="0" class="box1">
        <tr>
            <td>
                <table>
                    <tr>
                        <td valign="top">
                            <fieldset>
                                <legend>Search</legend>
                                <table width="100%">
                                    <tr>
                                        <td class="tdLabel120">
                                            <asp:CheckBox ID="chkHomebase" Text="Home Base Only" runat="server" />
                                        </td>
                                        <td>
                                            Strarting Depart Date
                                        </td>
                                        <td class="tdLabel120">
                                            <uc:DatePicker ID="ucstartDate" runat="server"></uc:DatePicker>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdLabel130">
                                            <asp:CheckBox ID="ChkPerTrav" Text="Personal travel only" runat="server" />
                                        </td>
                                        <td>
                                            Client Code
                                        </td>
                                        <td class="tdLabel120">
                                            <asp:TextBox ID="tbClientCode" runat="server"></asp:TextBox>
                                            <asp:HiddenField ID="hdnClient" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnClientCode" OnClientClick="javascript:openWin('RadClientCodePopup');return false;"
                                                CssClass="browse-button" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td>
                                        Tail No
                                    </td>
                                    <td class="tdLabel120">
                                        <asp:TextBox ID="tbTailNumber" runat="server"></asp:TextBox>
                                        <asp:HiddenField ID="hdnTailNo" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Button ID="Button1" OnClientClick="javascript:openWin('radFleetProfilePopup');return false;"
                                            CssClass="browse-button" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <telerik:RadGrid ID="dgSearch" runat="server" AllowMultiRowSelection="false" AllowSorting="true"
                                OnNeedDataSource="dgPostflightMainDetails_BindData" OnItemCommand="dgSearch_ItemCommand"
                                OnItemCreated="dgSearch_ItemCreated" OnInsertCommand="dgSearch_InsertCommand"
                                AutoGenerateColumns="false" PageSize="10" Width="960px" AllowPaging="true"
                                OnPreRender="dgSearch_PreRender"
                                >
                                <GroupingSettings CaseSensitive="false" />
                                <MasterTableView DataKeyNames="POLogID,TripID,RequestorName,HomebaseID" CommandItemDisplay="Bottom">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="POLogID" HeaderText="LogNo" CurrentFilterFunction="EqualTo"
                                             Display="false" FilterDelay="500" ShowFilterIcon="false" AutoPostBackOnFilter="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="LogNum" HeaderText="Log No." CurrentFilterFunction="EqualTo"
                                            FilterDelay="500" ShowFilterIcon="false" AutoPostBackOnFilter="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TripID" HeaderText="Trip No." CurrentFilterFunction="EqualTo"
                                            FilterDelay="500" ShowFilterIcon="false" AutoPostBackOnFilter="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="DispatchNum" HeaderText="Dispatch" CurrentFilterFunction="StartsWith"
                                            FilterDelay="500" ShowFilterIcon="false" AutoPostBackOnFilter="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail No." CurrentFilterFunction="StartsWith"
                                            FilterDelay="500" ShowFilterIcon="false" AutoPostBackOnFilter="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="EstDepartureDT" HeaderText="Depart" CurrentFilterFunction="EqualTo"
                                            FilterDelay="500" ShowFilterIcon="false" AutoPostBackOnFilter="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="RequestorName" HeaderText="Reqstr" CurrentFilterFunction="StartsWith"
                                            FilterDelay="500" ShowFilterIcon="false" AutoPostBackOnFilter="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="DepartmentDescription" HeaderText="Dept" CurrentFilterFunction="StartsWith"
                                            FilterDelay="500" ShowFilterIcon="false" AutoPostBackOnFilter="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="AuthorizationID" HeaderText="Auth" CurrentFilterFunction="EqualTo"
                                            FilterDelay="500" ShowFilterIcon="false" AutoPostBackOnFilter="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="POMainDescription" HeaderText="Description" CurrentFilterFunction="StartsWith"
                                            FilterDelay="500" ShowFilterIcon="false" AutoPostBackOnFilter="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="IsException" HeaderText="Excp" CurrentFilterFunction="StartsWith"
                                            FilterDelay="500" ShowFilterIcon="false" AutoPostBackOnFilter="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="IsPersonal" HeaderText="pers" CurrentFilterFunction="StartsWith"
                                            FilterDelay="500" ShowFilterIcon="false" AutoPostBackOnFilter="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="HomebaseID" HeaderText="Home Base" CurrentFilterFunction="EqualTo"
                                            FilterDelay="500" ShowFilterIcon="false" AutoPostBackOnFilter="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="FlightNum" HeaderText="FltNo" CurrentFilterFunction="StartsWith"
                                            FilterDelay="500" ShowFilterIcon="false" AutoPostBackOnFilter="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="IsCompleted" HeaderText="Completed" CurrentFilterFunction="StartsWith"
                                            FilterDelay="500" ShowFilterIcon="false" AutoPostBackOnFilter="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="LastUpdTS" HeaderText="Time" CurrentFilterFunction="StartsWith"
                                            FilterDelay="500" ShowFilterIcon="false" AutoPostBackOnFilter="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TechLog" HeaderText="TechLog" CurrentFilterFunction="StartsWith"
                                            FilterDelay="500" ShowFilterIcon="false" AutoPostBackOnFilter="false">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                        <div class="grd_ok">
                                            <asp:Button ID="btnSUB" runat="server" ToolTip="OK" OnClientClick="javascript:return returnToParent();"
                                                CausesValidation="false" CssClass="button" Text="OK"></asp:Button>
                                        </div>
                                        <div id="divMultiSelect" class="multiselect" runat="server" clientidmode="Static"
                                            visible="false">
                                            Use CTRL key to multi select</div>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings>
                                    <ClientEvents OnRowDblClick="returnToParent" />
                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lbMessage" runat="server" CssClass="alert-text" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
