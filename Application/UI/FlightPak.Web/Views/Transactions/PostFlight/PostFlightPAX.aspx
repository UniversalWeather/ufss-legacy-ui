﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/PostFlight.Master"
     CodeBehind="PostFlightPAX.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.PostFlight.PostFlightPAX"
     %>

<%@ Register Src="~/UserControls/Postflight/UCPostflightBottomButtonControls.ascx" TagPrefix="uc" TagName="UCPostflightBottomButtonControls" %>
<%@ Register Src="~/UserControls/Postflight/UCPostflightSearch.ascx" TagPrefix="uc" TagName="UCPostflightSearch" %>


<asp:Content ID="Content1" ContentPlaceHolderID="PostFlightHeadContent" runat="server">
        <style type="text/css">
            .prfl-nav-icons {
            left:-2px !important;
            }
        </style>                
    <%=Scripts.Render("~/bundles/postflightpax") %>

    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">        
        <script type="text/javascript">

            
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PostFlightBodyContent" runat="server">    
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true"
        ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="RadPassengerAssociatePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="PassengerAssociatePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerAssociatePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadSelectDepartPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="SelectDepartPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/PostFlight/PostFlightSIFLAirport.aspx?flag=Depart">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadSelectArrivalPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="SelectArrivalPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/PostFlight/PostFlightSIFLAirport.aspx?flag=Arrival">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadSelectPassengerPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="SelectPassengerPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/PostFlight/PostFlightSelectPassenger.aspx?rebind=1">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadSearchPopup" runat="server" OnClientResizeEnd="GetDimensions" OnClientClose="OnLogSearchClose"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/PostFlight/PostFlightSearchAll.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadRetrievePopup" runat="server" Width="750px" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" OnClientClose="OnPFSearchRetrieveClose" Behaviors="Close"
                 VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/PostFlight/PreflightSearchRetrieve.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions" OnClientClose="OnClientClosePaxInfoPopup"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" ReloadOnShow="false" Behaviors="Close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="History" NavigateUrl="~/Views/Transactions/History.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdPaxPage" runat="server" OnClientResizeEnd="GetDimensions"
                Width="830px" Height="550px" KeepInScreenBounds="true" Modal="true" ReloadOnShow="false"
                Behaviors="Close" VisibleStatusbar="false" Title="Passenger/Requestor">
            </telerik:RadWindow>

        </Windows>        
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" style="width: auto; float: left;">
        <asp:HiddenField ID="hdnPostflightMainInitializationObject" runat="server" />
        <asp:HiddenField ID="hdnUserPrincipalInitializationObject" runat="server" />
        
        <asp:HiddenField runat="server" ID="hdnPostflightPaxInitializationObject" ClientIDMode="Static" />
        
        <table width="100%" cellpadding="0" cellspacing="0" class="border-box">
            <tr>
                <td>
                    <UC:UCPostflightSearch runat="server" ID="UCPostflightSearch" />
                    
                    <div id="LoadingSpinner" style="display: none; background: rgba(255, 255, 255, 0.5); position: absolute; width: 100%; text-align: center; z-index: 999;">
                        <img src="/App_Themes/Default/images/loading.gif" style="position: absolute; left: 50%; top: 50%; margin-left: -32px; margin-top: -32px;" alt="Loading.." />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" id="tblPaxSearch" runat="server" cellspacing="0" cellpadding="0"
                        class="padtop_10">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" class="box_sc" width="100%">
                                    <tr>
                                        <td align="left">
                                            <table width="100%" cellspacing="0" cellpadding="0" class="nav_bg_preflight">
                                                <tr>
                                                    <td align="left" class="tdLabel180">
                                                        <div class="global-search-preflight">
                                                            <span>
                                                                <input type="button" data-bind="enable: self.POEditMode" onclick="PaxSearchClick();"  class="searchsubmitbutton"  id="btnSearchPAX" />
                                                                <input type="text" data-bind="enable:self.POEditMode" class="SearchBox_po_pax" id="tbSearchPAX" />
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td align="left">
                                                        <a id="lnkSearchPAX" onclick="javascript:return ShowPAXPopup(this);"
                                                            data-bind="visible:self.POEditMode" class="link_small">PAX Table</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="2" class="srch_category">(Search by Passenger Code)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Label ID="lbPAXSearchMsg" runat="server" CssClass="alert-text"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" id="tblPaxSummary" runat="server" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <telerik:RadPanelBar ID="pnlPaxSummary" Width="100%" ExpandAnimation-Type="None"
                                                            CollapseAnimation-Type="none" runat="server" CssClass="postflight-panel-bar716"
                                                            OnClientItemExpand="pnlPaxSummaryExpand" OnClientItemCollapse="pnlPaxSummaryCollapse">
                                                            <Items>
                                                                <telerik:RadPanelItem runat="server" Expanded="true" Text="PAX Summary" Value="pnlItemPaxSummary">
                                                                    <ContentTemplate>
                                                                        <table class="box-bg-normal" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td align="left">
                                                                                                <table cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <%--Total PAX Booked:--%>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <asp:Literal ID="lbBookedPax" runat="server" Visible="false"></asp:Literal>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td align="right">
                                                                                                <table cellpadding="0">
                                                                                                    <tr>
                                                                                                        <td>Override Flight Purpose for all Legs
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <select id="ddlFlightPurpose" data-bind="enable:self.POEditMode">
                                                                                                            </select>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>                                                                                            
                                                                                        </tr>
                                                                                    </table>
                                                                                    <table width="100%" cellpadding="0" id="tblPassenger" runat="server">
                                                                                        <tr>
                                                                                            <td style="width: 700px">
                                                                                                <table id="dgPassenger" class="table table-striped table-hover table-bordered"></table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>                                                                                                
                                                                                                <div class="grid_icon">
                                                                                                    
                                                                                                    <div role="group" id="pg_gridPagerPaxSelection" data-bind="enable: POEditMode">
                                                                                                    </div>
                                                                                                    
                                                                                                </div>                                                                                                
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td align="right">
                                                                                                <asp:Button ID="Button2" runat="server" Visible="false" Text="+/- Columns" CssClass="ui_nav" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <table width="100%" class="postflight-custom-grid">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <table cellpadding="0" cellspacing="0" id="dgPaxSummary" class="table table-striped table-hover table-bordered"></table>
                                                                                                <div role="group" id="pg_gridPagerdgPaxSummary">
                                                                                                    <div id="UpdateLoading" style="display: none;">
                                                                                                        <input type="button" id="btnPaxGroup" tooltip="Loading" class="browse-button-Validation-Loading" onclick="javascript: return false;" />
                                                                                                        Updating 
                                                                                                    </div>
                                                                                                </div>

                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </telerik:RadPanelItem>
                                                            </Items>
                                                        </telerik:RadPanelBar>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" cellpadding="0" cellspacing="0" class="padtop_10">
                                                <tr>
                                                    <td>
                                                        <telerik:RadPanelBar ID="pnlSIFL" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="none"
                                                            runat="server" CssClass="postflight-panel-bar716" OnClientItemExpand="pnlSIFLExpand" OnClientItemCollapse="pnlSIFLCollapse">
                                                            <Items>
                                                                <telerik:RadPanelItem runat="server" Expanded="true" Text="SIFL" Value="pnlItemSIFL">
                                                                    <ContentTemplate>
                                                                        <table class="box-bg-normal-pad" width="100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                                                        <tr>
                                                                                            <td width="50%">
                                                                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table>
                                                                                                                <tr>
                                                                                                                    <td class="mnd_text tdLabel60">PAX
                                                                                                                    </td>
                                                                                                                    <td align="left">
                                                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <input type="text" id="tbPax" data-bind="enable: self.POPaxSIFLEditMode,value: self.POSIFL.PassengerRequestorCD, event:{change: SIFLPaxCodeChangeEvent}" maxlength="5" ></input>
                                                                                                                                    <input type="hidden" id="hdnPax" data-bind="value:self.POSIFL.PassengerRequestorID" />
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <input type="button" id="btnPax" data-bind="enable: self.POPaxSIFLEditMode,css:self.BrowseBtnInSIFL" title="View Passengers" onclick="javascript:openWin('RadSelectPassengerPopup');return false;"
                                                                                                                                         />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td colspan="2">
                                                                                                                        <label id="lbPaxName" data-bind="text:self.POSIFL.Passenger1.PassengerName" class="input_no_bg"></label>
                                                                                                                        <label id="lbcvPaxName" class="alert-text" data-bind="text:self.POSIFL.alertlbcvPaxName"></label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <div>
                                                                                                                            <span>                                                                                                                                
                                                                                                                                <input type="radio" id="rbEmployeeType1" name="rdEmployeeType" data-bind='attr: { value: "C" }, checked: self.POSIFL.EmployeeTYPE.Trimmed(), enable: self.POPaxSIFLEditMode, event: { click: self.siflDistanceChange }' />
                                                                                                                                <label for="rbEmployeeType1">Control</label>
                                                                                                                            </span>
                                                                                                                            <span>                                                                                                                                
                                                                                                                                <input type="radio" id="rbEmployeeType2" name="rdEmployeeType" data-bind='attr: { value: "N" }, checked: self.POSIFL.EmployeeTYPE.Trimmed(), enable: self.POPaxSIFLEditMode, event: { click: self.siflDistanceChange }' />
                                                                                                                                <label for="rbEmployeeType2">Non-Control</label>
                                                                                                                            </span>
                                                                                                                            <span>                                                                                                                                
                                                                                                                                <input type="radio" id="rbEmployeeType3" name="rdEmployeeType" data-bind='attr: { value: "G" }, checked: self.POSIFL.EmployeeTYPE.Trimmed(), enable: self.POPaxSIFLEditMode, event: { click: self.siflDistanceChange }' />
                                                                                                                                <label for="rbEmployeeType3">Guest</label>
                                                                                                                            </span>
                                                                                                                        </div>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table class="border-box">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td class="tdLabel100" valign="top">Assoc. With
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                                                                        <tr>
                                                                                                                                            <td>
                                                                                                                                                <input type="text" id="tbAssocPax" data-bind="enable: self.POPaxSIFLAssociatePaxEditMode, value: self.POSIFL.AssocPassenger.AssociatedWithCD, event: { change: self.AssocPaxChange }" class="text70"  maxlength="5"></input>
                                                                                                                                                <input type="hidden" id="hdnAssocPax" data-bind="value: self.POSIFL.AssociatedPassengerID" />
                                                                                                                                            </td>
                                                                                                                                            <td>
                                                                                                                                                <input type="button" id="btnAssocPax" data-bind="enable: self.POPaxSIFLAssociatePaxEditMode, css: self.BrowseBtnPOPaxSIFLAssociatePax" title="View Passengers" onclick="    javascript: openWin('RadPassengerAssociatePopup'); return false;" />
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td colspan="2">
                                                                                                                                    <label id="lbAssocPaxName" class="input_no_bg" data-bind="text: self.POSIFL.AssocPassenger.PassengerName" ></label>
                                                                                                                                    <label id="lbcvAssoc" class="alert-text" ></label>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td class="mnd_text tdLabel100" valign="top">Departs
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                                                                        <tr>
                                                                                                                                            <td>
                                                                                                                                                <input type="text" id="tbDeparts" data-bind="enable: self.POPaxSIFLEditMode, value: self.POSIFL.DepartAirport.IcaoID, event: {change: siflDepartChange}" class="text70" maxlength="4"></input>
                                                                                                                                                <input type="hidden" id="hdnDeparts" data-bind="value: self.POSIFL.DepartICAOID" />
                                                                                                                                            </td>
                                                                                                                                            <td>
                                                                                                                                                <input type="button" id="btnDeparts" data-bind="enable: self.POPaxSIFLEditMode,css:self.BrowseBtnInSIFL" title="Display Airports"  onclick="javascript:openWin('RadSelectDepartPopup');return false;"
                                                                                                                                                     />
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td class="tdLabel100" valign="top"></td>
                                                                                                                                <td>
                                                                                                                                    <label id="lbcvDeparts" class="alert-text" ></label>
                                                                                                                                    <label id="lbDepart" class="input_no_bg" data-bind="text:self.POSIFL.DepartAirport.AirportName"></label>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td class="mnd_text tdLabel100" valign="top">Arrives
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                                                                        <tr>
                                                                                                                                            <td>
                                                                                                                                                <input type="text" data-bind="enable: self.POPaxSIFLEditMode, value: self.POSIFL.ArriveAirport.IcaoID, event: { change: siflArriveChange }" id="tbArrives" class="text70" maxlength="4"
                                                                                                                                                    ></input>
                                                                                                                                                <input type="hidden" id="hdnArrives" data-bind="value:self.POSIFL.ArriveICAOID" />
                                                                                                                                            </td>
                                                                                                                                            <td>
                                                                                                                                                <input type="button" id="btnArrives" data-bind="enable: self.POPaxSIFLEditMode,css:self.BrowseBtnInSIFL" title="Display Airports" onclick="javascript:openWin('RadSelectArrivalPopup');return false;"
                                                                                                                                                     />
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td class="tdLabel100" valign="top"></td>
                                                                                                                                <td>
                                                                                                                                    <label id="lbcvArrives" class="alert-text" ></label>
                                                                                                                                    <label id="lbArrives" data-bind="text:self.POSIFL.ArriveAirport.AirportName" class="input_no_bg"></label>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td class="tdLabel100" valign="top">Statute Miles
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <input type="text" id="tbStatueMiles" data-bind="enable: self.POPaxSIFLEditMode,value:self.POSIFL.Distance, event:{change:self.siflDistanceChange}" class="text70 rt_text" maxlength="5" onkeypress="return fnAllowNumeric(this, event)" >
                                                                                                                                    </input>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table cellspacing="0" cellpadding="0">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <div class="tblspace_5">
                                                                                                                        </div>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <table class="border-box" id="tblSifl" runat="server">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td class="tdLabel100" valign="top">Aircraft Multiplier
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <input type="text" id="tbAircraftMultiplier" data-bind="enable: self.POPaxSIFLEditMode, value: self.POSIFL.AircraftMultiplier, event: { change: self.siflDistanceChange }" class="text70 rt_text"  maxlength="5"
                                                                                                                                        onkeypress="return fnAllowNumericAndChar(this, event,'.')" ></input>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td class="tdLabel100">
                                                                                                                                    <span class="mnd_text">Mile Range</span>
                                                                                                                                </td>
                                                                                                                                <td class="tdLabel100">
                                                                                                                                    <span class="mnd_text">Miles</span>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <span class="mnd_text">Fare Level</span>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td class="tdLabel100">0 - 500
                                                                                                                                </td>
                                                                                                                                <td class="tdLabel100">
                                                                                                                                    <input type="text" id="tbMiles1" readonly="readonly" tabindex="-1" data-bind="enable: self.POPaxSIFLEditMode,value:self.POSIFL.Distance1" class="text70 inpt_non_edit rt_text" 
                                                                                                                                        onkeypress="return fnAllowNumeric(this, event)" onblur="return validateEmptyMilesTextbox(this, event)"></input>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <input type="text" id="tbRate1" data-bind="enable: self.POPaxSIFLEditMode, value: self.POSIFL.Rate1, event: { change: self.siflDistanceChange }" class="text70 rt_text" maxlength="8"
                                                                                                                                        
                                                                                                                                        ></input>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td class="tdLabel100">501 - 1500
                                                                                                                                </td>
                                                                                                                                <td class="tdLabel100">
                                                                                                                                    <input type="text"  id="tbMiles2" readonly="readonly" tabindex="-1" data-bind="enable: self.POPaxSIFLEditMode,value:self.POSIFL.Distance2" class="text70 inpt_non_edit rt_text" 
                                                                                                                                        onkeypress="return fnAllowNumeric(this, event)" onblur="return validateEmptyMilesTextbox(this, event)"></input>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <input type="text"  id="tbRate2" data-bind="enable: self.POPaxSIFLEditMode, value: self.POSIFL.Rate2, event: { change: self.siflDistanceChange }" class="text70 rt_text" maxlength="8"
                                                                                                                                        onkeypress="return fnAllowNumericAndChar(this, event,'.')" onblur="return validateEmptyRateTextbox(this, event)"
                                                                                                                                        ></input>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td class="tdLabel100">Over 1500
                                                                                                                                </td>
                                                                                                                                <td class="tdLabel100">
                                                                                                                                    <input type="text" id="tbMiles3" readonly="readonly" tabindex="-1" data-bind="enable: self.POPaxSIFLEditMode,value:self.POSIFL.Distance3" class="text70 inpt_non_edit rt_text"
                                                                                                                                        onkeypress="return fnAllowNumeric(this, event)" onblur="return validateEmptyMilesTextbox(this, event)"></input>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <input type="text" id="tbRate3" data-bind="enable: self.POPaxSIFLEditMode, value: self.POSIFL.Rate3, event: { change: self.siflDistanceChange }" class="text70 rt_text" maxlength="8"
                                                                                                                                        onkeypress="return fnAllowNumericAndChar(this, event,'.')" onblur="return validateEmptyRateTextbox(this, event)"
                                                                                                                                        ></input>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td class="tdLabel100">Terminal Charge
                                                                                                                                </td>
                                                                                                                                <td class="pr_radtextbox_76">
                                                                                                                                    <input type="text" id="tbTerminalCharge" class="text70 inpt_non_edit rt_text" tabindex="50" maxlength="6" value="0.00" data-bind="enable: self.POPaxSIFLEditMode, CostCurrencyElement: self.POSIFL.TeminalCharge, event: { change: self.siflDistanceChangeOnTerminalCharge }"/>
                                                                                                                                    
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td class="tdLabel100">SIFL Total
                                                                                                                                </td>
                                                                                                                                <td class="pr_radtextbox_76">
                                                                                                                                    <input type="text" id="tbSIFLTotal" maxlength="6" value="0.00" tabindex="-1" readonly="readonly" style="background-color:#EDEDED" class="text70 inpt_non_edit rt_text" disabled="disabled" data-bind="CostCurrencyElement: self.POSIFL.SIFLTotal">
                                                                                                                                    </input>                                                                                                                                    
                                                                                                                                </td>                                                                                                                                
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <table cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td class="tdLabel100">Grand Total
                                                                                                                                </td>
                                                                                                                                <td class="pr_radtextbox_76">
                                                                                                                                    <input type="text" id="tbGrandtotal" maxlength="6" value="0.00" tabindex="-1" readonly="readonly" style="background-color:#EDEDED;" class="text70 inpt_non_edit rt_text" data-bind="enable: self.POPaxSIFLEditMode, CostCurrencyElement: self.POSIFL.AmtTotal" disabled="disabled">
                                                                                                                                    </input>                                                                                                                                    
                                                                                                                                </td>
                                                                                                                                <td align="right">
                                                                                                                                    <input type="button" tabindex="51" id="btnUpdateSIFL" title="Update SIFL"
                                                                                                                                        value="Update" data-bind="enable: self.POPaxSIFLEditMode, css: self.CssPOPaxSIFLUpdateButton" onclick="    return Update_SiflClick();" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td width="50%" valign="top" align="left">
                                                                                                <table width="100%">
                                                                                                    <tr>
                                                                                                        <td align="right">
                                                                                                            <table cellspacing="0" cellpadding="0" class="box_alert">
                                                                                                                <tr>
                                                                                                                    <td align="left" valign="top">
                                                                                                                        <label id="lblSiflException" class="alert-text" >"Flight Scheduling Software has estimated the SIFL calculations for the trip. <br>Please review these records to ensure accuracy."</label>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="right">
                                                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td class="postflight-custom-grid" align="right">
                                                                                                                        <table id="dgSIFLPersonal" class="table table-striped table-hover table-bordered"></table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <div class="grid_icon">
                                                                                                                        <div role="group" id="pg_gridPagerSIFL" data-bind="enable: POEditMode">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="0" cellpadding="0" width="100%">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <div class="tblspace_5">
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table width="100%" cellpadding="0" cellspacing="0" class="postflight-custom-grid">
                                                                                        <tr>
                                                                                            <td align="left">
                                                                                                <table id="dgSIFL" class="table table-striped table-hover table-bordered"></table>
                                                                                            </td>
                                                                                        </tr>                                                                                        
                                                                                    </table>
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td align="left">
                                                                                                <input type="button" id="lnkSiflReport" class="print_preview_icon" title="Print SIFL Report" onclick="    return lnkSiflReport_Click();"></input>
                                                                                            </td>
                                                                                            <td align="right">
                                                                                                <input type="button" data-bind="enable: self.POEditMode, css: self.BlueButtonClass" id="btnReCalcSIFL" value="Recalc SIFL"
                                                                                                    title="SIFL Recalculation" onclick="return Recalculate_SiflLeg_Click();" />
                                                                                                <input type="button" data-bind="enable: self.POEditMode, css: self.BlueButtonClass, event: { click: AddSIFLLegClick }" id="btnAddSIFLLeg" value="Add SIFL Leg"
                                                                                                    title="Add SIFL Leg" />
                                                                                                <input type="button" data-bind="enable: self.POEditMode, css: self.BlueButtonClass" id="btnDeleteSIFLLeg" value="Delete SIFL Leg"
                                                                                                    title="Delete SIFL Leg" onclick="return DeleteSifl_LegClick();" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </telerik:RadPanelItem>
                                                            </Items>
                                                        </telerik:RadPanelBar>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <UC:UCPostflightBottomButtonControls runat="server" ID="UCPostflightBottomButtonControls2" />

        <input type="hidden" id="hdnPasspaxID" />
        <input type="hidden" id="hdnPassPaxLegID" />
        <input type="hidden" id="hdnInsert" />

    </div>
    
</asp:Content>
