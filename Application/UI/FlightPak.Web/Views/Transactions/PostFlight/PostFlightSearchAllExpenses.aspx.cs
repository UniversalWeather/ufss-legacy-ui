﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlightPak.Web.PostflightService;

//For Tracing and Exception Handling
using System.Web.Services;
using System.Collections;
using System.Web.Script.Services;
using FlightPak.Web.ViewModels;
using FlightPak.Web.ViewModels.Postflight;
using System.Web;
using FlightPak.Web.Framework.Constants;
namespace FlightPak.Web.Views.Transactions.PostFlight
{
    public partial class PostFlightSearchAllExpenses : BaseSecuredPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PostflightExpenseCatalogViewModel PostflightLegExpenseVM = new PostflightExpenseCatalogViewModel();
            var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(PostflightLegExpenseVM);
            hdnPOAllExpenseInitializationObject.Value = Microsoft.Security.Application.Encoder.HtmlEncode(json);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable SearchAllExpensesDataBind(bool homeBaseOnly, DateTime? GoToDTTM, string FuelSelected, string TailNum,
                string icaoID, Int64? LogID, Int64? legID, string PaymentTypeID, string payableVendorID, string FuelLocID, string AccNoID,
                string InvoiceNo, decimal? ExpAmount, string CrewCD, string crewID)
        {
            List<GetPostflightExpenseList> FilteredExpenseList = new List<GetPostflightExpenseList>();
            Hashtable RetObj = new Hashtable();
            PostflightService.PostflightServiceClient objService = new PostflightService.PostflightServiceClient();
            if (HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel] != null)
            {
                UserPrincipalViewModel userPrincipal = TripManagerBase.getUserPrincipal();
                var ObjRetval = objService.GetExpList().EntityList;
                if (ObjRetval != null)
                {
                    Int64? HomebaseID = null;
                    if (homeBaseOnly)
                        HomebaseID = userPrincipal._homeBaseId;

                    FilteredExpenseList = ObjRetval.Where(x => x.HomebaseID == (HomebaseID != null ? HomebaseID : x.HomebaseID)
                        && x.PurchaseDT >= (GoToDTTM != null ? GoToDTTM : x.PurchaseDT)
                        && x.TailNum == (TailNum != string.Empty ? TailNum : x.TailNum)
                        && x.IcaoID == (icaoID != string.Empty ? icaoID : x.IcaoID)
                        && x.POLogID == (LogID != null ? LogID : x.POLogID)
                        && x.POLegID == (legID != null ? legID : x.POLegID)
                        && x.PaymentTypeCD == (PaymentTypeID != string.Empty ? PaymentTypeID : x.PaymentTypeCD)
                        && string.Compare((x.VendorCD??"").Trim(),((payableVendorID??"").Trim() != string.Empty ? payableVendorID : (x.VendorCD??"")).Trim(),true)==0
                        && x.FuelLocatorCD == (FuelLocID != string.Empty ? FuelLocID : x.FuelLocatorCD)
                        && x.AccountNum == (AccNoID != string.Empty ? AccNoID : x.AccountNum)
                        && x.InvoiceNUM == (InvoiceNo != string.Empty ? InvoiceNo : x.InvoiceNUM)
                        && x.ExpenseAMT == (ExpAmount != null ? ExpAmount : x.ExpenseAMT)
                        && x.CrewCD == (!string.IsNullOrWhiteSpace(CrewCD) ? CrewCD : x.CrewCD)
                        && x.CrewID == (!string.IsNullOrWhiteSpace(crewID) ? Convert.ToInt64(crewID) : x.CrewID)
                        && x.IsDeleted == false
                        ).ToList();

                    if (FuelSelected == "1")
                        FilteredExpenseList = FilteredExpenseList.Where(x => x.IsAutomaticCalculation == true).ToList();
                    else if (FuelSelected == "2")
                        FilteredExpenseList = FilteredExpenseList.Where(x => x.IsAutomaticCalculation == false).ToList();
                }
            }
            RetObj["meta"] = new PagingMetaData() { Page = FilteredExpenseList.Count() / 10, Size = 10, total_items = FilteredExpenseList.Count() };
            RetObj["results"] = FilteredExpenseList;
            return RetObj;
        }
    }
}