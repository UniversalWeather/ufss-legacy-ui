﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Script.Services;
using System.Web.Services;
using FlightPak.Web.BusinessLite;
using FlightPak.Web.ViewModels;
using FlightPak.Common.Constants;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.Framework.Constants;
using Omu.ValueInjecter;
using System.Web.Security;
using System.Globalization;
using FlightPak.Web.CalculationService;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Transactions
{
    public class TripManagerBase : Page
    {
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static UserPrincipalViewModel getUserPrincipal()
        {
            UserPrincipalViewModel upViewModel = new UserPrincipalViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel] == null)
            {
                FPPrincipal UserPrincipal = (FPPrincipal)HttpContext.Current.Session[HttpContext.Current.Session.SessionID];
                upViewModel = (UserPrincipalViewModel)upViewModel.InjectFrom(UserPrincipal.Identity).InjectFrom(UserPrincipal.Identity._fpSettings);
                upViewModel.AllowAddTripManager = IsAuthorized(Permission.Preflight.AddTripManager);
                upViewModel.AllowEditTripManager = IsAuthorized(Permission.Preflight.EditTripManager);
                upViewModel.AllowDeleteTripManager = IsAuthorized(Permission.Preflight.DeleteTripManager);
                upViewModel.AddTripLog = IsAuthorized(Permission.Preflight.AddTripLog);
                upViewModel.AddTripSIFL = IsAuthorized(Permission.Preflight.AddTripSIFL);
                upViewModel.ViewPreFlightMain = IsAuthorized(Permission.Preflight.ViewPreFlightMain);
                upViewModel.ViewPreflightLeg = IsAuthorized(Permission.Preflight.ViewPreflightLeg);
                upViewModel.ViewPreflightCrew = IsAuthorized(Permission.Preflight.ViewPreflightCrew);
                upViewModel.ViewPreflightPassenger = IsAuthorized(Permission.Preflight.ViewPreflightPassenger);
                upViewModel.ViewPreflightCatering = IsAuthorized(Permission.Preflight.ViewPreflightCatering);
                upViewModel.ViewPreflightFBO = IsAuthorized(Permission.Preflight.ViewPreflightFBO);
                upViewModel.ViewPreflightFuel = IsAuthorized(Permission.Preflight.ViewPreflightFuel);
                upViewModel.ViewPreflightUWA = IsAuthorized(Permission.Preflight.ViewPreflightUWA);
                upViewModel.ViewPreflightReports = IsAuthorized(Permission.Preflight.ViewPreflightReports);
                upViewModel.ViewPreflightTripsheetReportWriter = IsAuthorized(Permission.Preflight.ViewPreflightTripsheetReportWriter);
                upViewModel._IsAPISSupport = UserPrincipal.Identity._fpSettings._IsAPISSupport;


                // Postflight Related Permission Variables

                upViewModel.AddPOLogManager= IsAuthorized(Permission.Postflight.AddPOLogManager);
                upViewModel.EditPOLogManager = IsAuthorized(Permission.Postflight.EditPOLogManager);
                upViewModel.DeletePOLogManager = IsAuthorized(Permission.Postflight.DeletePOLogManager);
                upViewModel.ViewPORetrieveLog = IsAuthorized(Permission.Postflight.ViewPORetrieveLog);
                upViewModel.ViewPOOtherCrewDutyLog = IsAuthorized(Permission.Postflight.ViewPOOtherCrewDutyLog);
                upViewModel.ViewPOExpenseCatalog = IsAuthorized(Permission.Postflight.ViewPOExpenseCatalog);
                upViewModel.ViewPOLogManager = IsAuthorized(Permission.Postflight.ViewPOLogManager);

                //Sometimes the Home Base ICAO is not populated
                if (upViewModel != null && String.IsNullOrEmpty(upViewModel._homeBase) && upViewModel._homeBaseId.HasValue && upViewModel._homeBaseId.Value > 0)
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objDstsvc.GetCompanyWithFilters(string.Empty, upViewModel._homeBaseId.Value, true, true);
                        List<FlightPakMasterService.GetCompanyWithFilters> CompanyList = new List<FlightPakMasterService.GetCompanyWithFilters>();
                        if (objRetVal.ReturnFlag)
                        {
                            CompanyList = objRetVal.EntityList;
                            if (CompanyList != null && CompanyList.Count > 0)
                            {
                                upViewModel._homeBase = CompanyList[0].HomebaseCD;
                            }
                        }
                    }
                }
                HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel] = upViewModel;

                if (UserPrincipal.Identity._homeBaseId != null)
                    SetHomebaseSettingsToSession((long) UserPrincipal.Identity._homeBaseId);
            }
            else
            {
                upViewModel = (UserPrincipalViewModel)HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel];
            }
            return upViewModel;
        }

        public static bool IsAuthorized(string ModuleName)
        {
            //Session Timeout fix
            if (((FPPrincipal)HttpContext.Current.Session[HttpContext.Current.Session.SessionID]) != null)
            {
                return ((FPPrincipal)HttpContext.Current.Session[HttpContext.Current.Session.SessionID]).IsInRole(ModuleName.ToLower());
            }
            return false;
        }

        public static void CheckSessionAndRedirect()
        {
            FlightPak.Web.Framework.Prinicipal.FPPrincipal UserPrincipal = (FlightPak.Web.Framework.Prinicipal.FPPrincipal)HttpContext.Current.Session[HttpContext.Current.Session.SessionID];
            if (UserPrincipal == null)
            {
                // Sign out from Forms Authentication
                FormsAuthentication.SignOut();
                HttpContext.Current.Session.Abandon();
                //Clear FormsAuthentication Cookie
                HttpCookie FormsAuthenticationCookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                FormsAuthenticationCookie.Expires = DateTime.Now.AddYears(-1);
                HttpContext.Current.Response.Cookies.Add(FormsAuthenticationCookie);

                //Clear ASP.NET_SessionId
                HttpCookie SessionCookie = new HttpCookie("ASP.NET_SessionId", "");
                SessionCookie.Expires = DateTime.Now.AddYears(-1);
                HttpContext.Current.Response.Cookies.Add(SessionCookie);

                HttpContext.Current.Response.Redirect("~/Account/Login.aspx");
            }
        }

        public static string ValidateTime(string timeVal)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(timeVal))
            {
                //Bug 2352 - #4723
                string result = string.Empty;
                if (timeVal.IndexOf(":") != -1)
                {
                    string[] timeArray = timeVal.Split(':');
                    string _hour = "00";
                    string _minute = "00";

                    if (!string.IsNullOrEmpty(timeArray[0]))
                        _hour = timeArray[0];
                    if (!string.IsNullOrEmpty(timeArray[1]))
                        _minute = timeArray[1];

                    Int32 hour = Convert.ToInt32(_hour);
                    Int32 minute = Convert.ToInt32(_minute);

                    if (hour > 23)
                    {
                        result = "Hour entry must be between 0 and 23. <br/>";
                    }
                    if (minute > 59)
                    {
                        result += "Minute entry must be between 0 and 59.";
                    }
                }
                else if (timeVal.IndexOf(":") < 0)
                {
                    result = "Invalid Time Format! <br/>";
                    result += "Hour entry must be between 0 and 23. <br/>";
                    result += "Minute entry must be between 0 and 59.";
                }
                return result;
            }
        }

        public static string FormatDate(string dt)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(dt))
            {
                string FormattedDate = string.Empty;
                if (!string.IsNullOrEmpty(dt))
                {
                    UserPrincipalViewModel UserPrincipal = getUserPrincipal();
                    DateTimeFormatInfo formatInfo = new DateTimeFormatInfo();
                    if (UserPrincipal._ApplicationDateFormat != null)
                        formatInfo.ShortDatePattern = UserPrincipal._ApplicationDateFormat;
                    else
                        formatInfo.ShortDatePattern = "MM/dd/yyyy"; // American, MDY

                    List<string> dtList = new List<string>();

                    char splitter;
                    if (dt.Contains("-"))
                        splitter = '-';
                    else if (dt.Contains("."))
                        splitter = '.';
                    else
                        splitter = '/';

                    switch (formatInfo.ShortDatePattern)
                    {
                        case "dd/MM/yyyy": // British, French, DMY
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[0] + "/" + dtList[2];
                            break;
                        case "dd.MM.yyyy": // German
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[0] + "/" + dtList[2];
                            break;
                        case "yyyy.MM.dd": // ANSI
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[2] + "/" + dtList[0];
                            break;
                        case "dd-MM-yyyy": // Italian
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[0] + "/" + dtList[2];
                            break;
                        case "yyyy/MM/dd": // Japan, Taiwan, YMD
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[2] + "/" + dtList[0];
                            break;
                        case "MM/dd/yyyy": // Default
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[0] + "/" + dtList[1] + "/" + dtList[2];
                            break;
                    }
                }
                return FormattedDate;
            }
        }

        public static DateTime FormatDate(string Date, string Format)
        {
            UserPrincipalViewModel UserPrincipal = getUserPrincipal();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Date, Format))
            {
                string DateFormat = UserPrincipal._ApplicationDateFormat;
                int dd = 0, mm = 0, yyyy = 0;

                if (!string.IsNullOrEmpty(Date) && Date != "0")
                {
                    string[] DateAndTime = Date.Split(' ');

                    string[] SplitDate = new string[3];
                    string[] SplitFormat = new string[3];

                    if (DateFormat.Contains("/"))
                    {
                        SplitDate = DateAndTime[0].Split('/');
                        SplitFormat = Format.Split('/');
                    }
                    else if (DateFormat.Contains("-"))
                    {
                        SplitDate = DateAndTime[0].Split('-');
                        SplitFormat = Format.Split('-');
                    }
                    else if (DateFormat.Contains("."))
                    {
                        SplitDate = DateAndTime[0].Split('.');
                        SplitFormat = Format.Split('.');
                    }

                    for (int Index = 0; Index < SplitFormat.Count(); Index++)
                    {
                        if (SplitFormat[Index].ToLower().Contains("d"))
                        {
                            dd = Convert.ToInt32(SplitDate[Index]);
                        }
                        if (SplitFormat[Index].ToLower().Contains("m"))
                        {
                            mm = Convert.ToInt32(SplitDate[Index]);
                        }
                        if (SplitFormat[Index].ToLower().Contains("y"))
                        {
                            if (SplitDate[Index] == "0000")
                                yyyy = 1;
                            else
                                yyyy = Convert.ToInt32(SplitDate[Index]);
                        }

                    }
                }
                return new DateTime(yyyy, mm, dd);
            }
        }

        public static string FormatValue(string tbValue)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tbValue))
            {
                UserPrincipalViewModel UserPrincipal = getUserPrincipal();
                string TextboxValue = "";
                if ((UserPrincipal._TimeDisplayTenMin != null) && ((UserPrincipal._TimeDisplayTenMin == 2)))
                {
                    TextboxValue = "00:00";
                    if (tbValue != "")
                    {
                        TextboxValue = tbValue;
                        if ((!TextboxValue.Contains('-')) && (TextboxValue.Trim() != "00:00") && (TextboxValue.Trim() != "") && (TextboxValue.Trim() != ":") && (TextboxValue.Trim() != ".") && (TextboxValue.Trim() != "0") && (TextboxValue.Trim() != "0:0") && (TextboxValue.Trim() != "0:00"))
                        {
                            if (TextboxValue.IndexOf(":") != -1)
                            {
                                string[] timeArray = TextboxValue.Split(':');
                                if (string.IsNullOrEmpty(timeArray[0]))
                                {
                                    timeArray[0] = "00";
                                    TextboxValue = timeArray[0] + ":" + timeArray[1];
                                }
                                if (!string.IsNullOrEmpty(timeArray[0]))
                                {
                                    if (timeArray[0].Length == 0)
                                    {
                                        timeArray[0] = "00";
                                        TextboxValue = timeArray[0] + ":" + timeArray[1];
                                    }
                                    if (timeArray[0].Length == 1)
                                    {
                                        timeArray[0] = "0" + timeArray[0];
                                        TextboxValue = timeArray[0] + ":" + timeArray[1];
                                    }
                                }
                                if (!string.IsNullOrEmpty(timeArray[1]))
                                {
                                    Int64 result = 0;
                                    if (timeArray[1].Length != 2)
                                    {
                                        result = (Convert.ToInt32(timeArray[1])) / 10;
                                        if (result < 1)
                                        {
                                            if (timeArray[0] != null)
                                            {
                                                if (timeArray[0].Trim() == "")
                                                {
                                                    timeArray[0] = "00";
                                                }
                                            }
                                            else
                                            {
                                                timeArray[0] = "00";
                                            }
                                            TextboxValue = timeArray[0] + ":" + "0" + timeArray[1];

                                        }
                                    }
                                }
                                else
                                {
                                    TextboxValue = TextboxValue + "00";
                                }
                            }
                            else
                            {
                                TextboxValue = TextboxValue + ":00";
                            }
                        }
                        else
                        {
                            TextboxValue = "00:00";
                        }

                    }
                    else
                    {
                        TextboxValue = "00:00";
                    }
                }
                else
                {
                    TextboxValue = "0.0";
                    if (tbValue != "")
                    {
                        TextboxValue = tbValue;
                        if ((!TextboxValue.Contains('-')) && (TextboxValue.Trim() != "00.00") && (TextboxValue.Trim() != "00.0") && (TextboxValue.Trim() != "") && (TextboxValue.Trim() != ":") && (TextboxValue.Trim() != ".") && (TextboxValue.Trim() != "0") && (TextboxValue.Trim() != "0.0") && (TextboxValue.Trim() != "0.00"))
                        {
                            //if (TextboxValue.IndexOf(".") != -1)
                            //{
                            //    string[] timeArray = TextboxValue.Split('.');
                            //    if (string.IsNullOrEmpty(timeArray[0]))
                            //    {
                            //        timeArray[0] = "00";
                            //        TextboxValue = timeArray[0] + "." + timeArray[1];
                            //    }
                            //    if (!string.IsNullOrEmpty(timeArray[0]))
                            //    {
                            //        if (timeArray[0].Length == 0)
                            //        {
                            //            timeArray[0] = "00";
                            //            TextboxValue = timeArray[0] + "." + timeArray[1];
                            //        }
                            //        if (timeArray[0].Length == 1)
                            //        {
                            //            timeArray[0] = "0" + timeArray[0];
                            //            TextboxValue = timeArray[0] + "." + timeArray[1];
                            //        }
                            //    }
                            //    if (!string.IsNullOrEmpty(timeArray[1]))
                            //    {
                            //        if (timeArray[1].Length == 0)
                            //        {
                            //            TextboxValue = timeArray[0] + "." + "00";
                            //        }
                            //        if (timeArray[1].Length == 1)
                            //        {
                            //            TextboxValue = "0" + timeArray[0] + "." + timeArray[1];
                            //        }
                            //    }
                            //    else
                            //    {
                            //        TextboxValue = TextboxValue + "00";
                            //    }
                            //}
                            //else
                            //{
                            //    TextboxValue = TextboxValue + ".00";
                            //}
                        }
                        else
                        {
                            TextboxValue = "0.0";
                        }

                    }
                    else
                    {
                        TextboxValue = "0.0";
                    }
                }
                return TextboxValue;
            }
        }

        public static TimeSpan ConvertDoubleToTimeSpan(double value, double blockMins)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(value, blockMins))
            {
                double hours = Math.Floor(value);
                double minutes = (value - hours) * 60.0;

                Int32 actualHours = (Int32)Math.Floor(hours);
                Int32 actualMinutes = (Int32)Math.Floor(minutes) + (Int32)Math.Floor(blockMins);

                TimeSpan nTime = new TimeSpan(actualHours, actualMinutes, 0);
                return nTime;
            }
        }

        public static TimeSpan ConvertDoubleToTimeSpan(double value)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(value))
            {
                double hours = Math.Floor(value);
                double minutes = (value - hours) * 60.0;

                Int32 actualHours = (Int32)Math.Floor(hours);
                Int32 actualMinutes = (Int32)Math.Floor(minutes);

                TimeSpan nTime = new TimeSpan(actualHours, actualMinutes, 0);
                return nTime;
            }
        }

        public static void SetHomebaseSettingsToSession(long homebaseID)
        {
            var exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            exManager.Process(() =>
               {
                   using (CalculationServiceClient CalcService = new CalculationServiceClient())
                   {
                       if (HttpContext.Current.Session[WebSessionKeys.POHomeBaseKey] == null)
                       {
                           if (homebaseID > 0)
                           {
                               var homeBaseInfo = CalcService.GetPostflightHomeBaseSetting(Convert.ToInt64(homebaseID, CultureInfo.InvariantCulture));
                               if(homeBaseInfo.ReturnFlag==true)
                                   HttpContext.Current.Session[WebSessionKeys.POHomeBaseKey] = homeBaseInfo.EntityList;
                           }
                       }
                   }
               }, FlightPak.Common.Constants.Policy.UILayer);
        }

        public static bool CheckPrincipalEmpty(Exception ex)
        {
            if (ex != null && ex.Message.Contains("Login session has expired or logged-in using other browser or other device."))
            {
                // Perform Logout
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}