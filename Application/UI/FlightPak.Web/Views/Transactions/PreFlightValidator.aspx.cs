﻿using FlightPak.Web.Framework.Constants;
using FlightPak.Web.PreflightService;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using FlightPak.Web.Views.Transactions.Preflight;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.Framework.Helpers.Validators;
using FlightPak.Web.Framework.Helpers.Preflight;
using System.Data;
using FlightPak.Web.ViewModels;
using FlightPak.Web.BusinessLite;
using Omu.ValueInjecter;
using FlightPak.Web.BusinessLite.Preflight;
using FlightPak.Web.CalculationService;

namespace FlightPak.Web.Views.Transactions
{
    public partial class PreFlightValidator : BaseSecuredPage
    {
        #region VARIABLE_DECLARATION
        public static FPKConversionHelper Utilities = new FPKConversionHelper();
        public static PreflightUtils PreUtilities=new PreflightUtils();

        
        #endregion

        #region PAGE_EVENT
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        #endregion

        #region UTILITIEY_METHOD

        private static void LegChangedSession(int LegNum)
        {
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                PreflightTripViewModel Trip = new PreflightTripViewModel();
                Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                if (Trip != null)
                {
                    if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                    {
                        if (Trip.PreflightLegs[LegNum] != null)
                        {
                            Trip.PreflightLegs[LegNum].DepartAirportChanged = true;
                            Trip.PreflightLegs[LegNum].DepartAirportChangedUpdateCrew = true;
                            Trip.PreflightLegs[LegNum].DepartAirportChangedUpdatePAX = true;
                        }
                    }
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }
        }

        private static List<object> setPowerValue(FlightPakMasterService.Aircraft retAircraft,decimal TenMin)
        {
            Hashtable PowerProp1 = new Hashtable();
            Hashtable PowerProp2 = new Hashtable();
            Hashtable PowerProp3 = new Hashtable();
            List<object> PowerValue = new List<object>();
           
            PowerProp1["Power"]= 1;
            PowerProp1["PowerSetting"]=retAircraft==null ?"": retAircraft.PowerSettings1Description;
            PowerProp1["TAS"] = retAircraft==null?0:retAircraft.PowerSettingNullCheck(retAircraft.PowerSettings1TrueAirSpeed, 0);
            PowerProp1["HrRange"] = retAircraft == null ? 0 : retAircraft.PowerSettingNullCheck(retAircraft.PowerSettings1HourRange, 1);
            PowerProp1["TOBias"] = retAircraft == null ? 0 : retAircraft.PowerSettingNullCheck(retAircraft.PowerSettings1TakeOffBias, 1);
            PowerProp1["LandBias"] = retAircraft == null ? 0 : retAircraft.PowerSettingNullCheck(retAircraft.PowerSettings1LandingBias, 1);

            if (TenthMinuteFormat.Minute == (TenthMinuteFormat)TenMin)
            {
                PowerProp1["ConvertTOBias"]=retAircraft!=null?Utilities.Preflight_ConvertTenthsToMins(Convert.ToString(retAircraft.PowerSettings1TakeOffBias), TenMin):"00:00";
                PowerProp1["ConvertLandBias"]=retAircraft != null ? Utilities.Preflight_ConvertTenthsToMins(Convert.ToString(retAircraft.PowerSettings1LandingBias), TenMin) : "00:00";
            }
            else { 
                PowerProp1["ConvertTOBias"]=retAircraft!=null?retAircraft.PowerSettings1TakeOffBias == null ?  "0.0" : Convert.ToString(Math.Round((decimal)retAircraft.PowerSettings1TakeOffBias, 1)):"0.0";
                PowerProp1["ConvertLandBias"]=retAircraft != null ? retAircraft.PowerSettings1LandingBias == null ? "0.0" : Convert.ToString(Math.Round((decimal)retAircraft.PowerSettings1LandingBias, 1)) : "0.0";
            }
            PowerValue.Add(PowerProp1);

            PowerProp2["Power"]=2;
            PowerProp2["PowerSetting"]=retAircraft == null ? "" : retAircraft.PowerSettings2Description;
            PowerProp2["TAS"] = retAircraft == null ? 0 : retAircraft.PowerSettingNullCheck(retAircraft.PowerSettings2TrueAirSpeed, 0);
            PowerProp2["HrRange"] = retAircraft == null ? 0 : retAircraft.PowerSettingNullCheck(retAircraft.PowerSettings2HourRange, 1);
            PowerProp2["TOBias"] = retAircraft == null ? 0 : retAircraft.PowerSettingNullCheck(retAircraft.PowerSettings2TakeOffBias, 1);
            PowerProp2["LandBias"] = retAircraft == null ? 0 : retAircraft.PowerSettingNullCheck(retAircraft.PowerSettings2LandingBias, 1);
            if (TenthMinuteFormat.Minute==(TenthMinuteFormat)TenMin)
            {
                PowerProp2["ConvertTOBias"]=retAircraft!=null? Utilities.Preflight_ConvertTenthsToMins(Convert.ToString(retAircraft.PowerSettings2TakeOffBias), TenMin):"00:00";
                PowerProp2["ConvertLandBias"]=retAircraft != null ? Utilities.Preflight_ConvertTenthsToMins(Convert.ToString(retAircraft.PowerSettings2LandingBias), TenMin) : "00:00";
            }
            else
            {
                PowerProp2["ConvertTOBias"]=retAircraft!=null?retAircraft.PowerSettings2TakeOffBias == null ? "0.0" : Convert.ToString(Math.Round((decimal)retAircraft.PowerSettings2TakeOffBias, 1)):"0.0";
                PowerProp2["ConvertLandBias"]=retAircraft != null ? retAircraft.PowerSettings2LandingBias == null ? "0.0": Convert.ToString(Math.Round((decimal)retAircraft.PowerSettings2LandingBias, 1)) : "0.0";
            }
            PowerValue.Add(PowerProp2);

            PowerProp3["Power"]= 3;
            PowerProp3["PowerSetting"]=retAircraft == null ? "" : retAircraft.PowerSettings3Description;
            PowerProp3["TAS"] = retAircraft == null ? 0 : retAircraft.PowerSettingNullCheck(retAircraft.PowerSettings3TrueAirSpeed, 0);
            PowerProp3["HrRange"] = retAircraft == null ? 0 : retAircraft.PowerSettingNullCheck(retAircraft.PowerSettings3HourRange, 1);
            PowerProp3["TOBias"] = retAircraft == null ? 0 : retAircraft.PowerSettingNullCheck(retAircraft.PowerSettings3TakeOffBias, 1);
            PowerProp3["LandBias"] = retAircraft == null ? 0 : retAircraft.PowerSettingNullCheck(retAircraft.PowerSettings3LandingBias, 1);
            if (TenthMinuteFormat.Minute == (TenthMinuteFormat)TenMin)
            {
                PowerProp3["ConvertTOBias"]=retAircraft!=null? Utilities.Preflight_ConvertTenthsToMins(Convert.ToString(retAircraft.PowerSettings3TakeOffBias), TenMin):"00:00";
                PowerProp3["ConvertLandBias"]=retAircraft != null ? retAircraft.PowerSettings3LandingBias == null ? 0 : Math.Round((decimal)retAircraft.PowerSettings3LandingBias, 1) : 0;
            }
            else
            {
                PowerProp3["ConvertTOBias"]=retAircraft!=null?retAircraft.PowerSettings3TakeOffBias == null ? "0.0" : Convert.ToString(Math.Round((decimal)retAircraft.PowerSettings3TakeOffBias, 1)):"0.0";
                PowerProp3["ConvertLandBias"]=retAircraft != null ? retAircraft.PowerSettings3LandingBias == null ? "0.0" : Convert.ToString(Math.Round((decimal)retAircraft.PowerSettings3LandingBias, 1)) : "0.0";
            }
            PowerValue.Add(PowerProp3);

            return PowerValue;
        }

        private static void CrewDutyRulesForAllLeg(long CrewDutyRuleId, string CrewDutyCD, string CrewDutyDesc, string FARRule, string LegNum, bool isEndDuty)
        {
            PreflightTripViewModel Trip = new PreflightTripViewModel();
            Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            int indexofLeg = Trip.GetPreflightLegIndex(LegNum);
            var legNum = Trip.PreflightLegs[indexofLeg].LegNUM;
            if(legNum != null) {
                Int64 CurrentLegNum =(long) legNum;
                List<PreflightLegViewModel> PrefLegs = new List<PreflightLegViewModel>();
                PrefLegs = Trip.PreflightLegs != null ? Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(p => p.LegNUM).ToList() : new List<PreflightLegViewModel>();

                for (int i = (int)CurrentLegNum-1; i >= 0; i--)
                {
                    PreflightLegViewModel LegtoUpdate = (from Leg in PrefLegs 
                                   where Leg.LegNUM == i && Leg.IsDeleted == false 
                                   select Leg).FirstOrDefault();
                    if (LegtoUpdate != null && !LegtoUpdate.IsLegDutyEnd())
                    {
                        if (LegtoUpdate.LegID != 0 && LegtoUpdate.IsDeleted != true)
                            Trip.PreflightMain.EditMode = true;

                        LegtoUpdate = LegtoUpdate.AddCrewDutyRuleToLeg(CrewDutyRuleId, CrewDutyCD, CrewDutyDesc, FARRule);
                    }
                    else
                        break;
                }

                if (!isEndDuty)
                {
                    for (int i = (int)CurrentLegNum ;  i<= PrefLegs.Count; i++)
                    {
                        PreflightLegViewModel LegtoUpdate = (from Leg in PrefLegs
                                       where Leg.LegNUM == i && Leg.IsDeleted == false
                                       select Leg).FirstOrDefault();

                        if (LegtoUpdate != null)
                        {
                            if (LegtoUpdate.LegID != 0 && LegtoUpdate.IsDeleted != true)
                                Trip.PreflightMain.EditMode = true;

                            LegtoUpdate = LegtoUpdate.AddCrewDutyRuleToLeg(CrewDutyRuleId, CrewDutyCD, CrewDutyDesc, FARRule);

                            if (LegtoUpdate.IsLegDutyEnd())
                               break;
                        }
                    }
                }
            }
            HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
        }

        #endregion

        #region VALIDATION_METHOD

        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> DepartDateValidation(string departDate) 
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            Hashtable RetObj = new Hashtable();
            PreflightTripViewModel Trip = new PreflightTripViewModel();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                if(Trip.PreflightMain.EditMode)
                {
                    if(!string.IsNullOrEmpty(departDate))
                    {
                        if(Trip.PreflightLegs != null && Trip.PreflightLegs.Where(x => x.IsDeleted == false).ToList().Count() > 0)
                        {
                            RetObj["Result"] = "true";
                        }
                        else
                        {
                            RetObj["Result"] = "false";
                        }
                    }
                    else
                    {
                        RetObj["Result"] = "false";
                    }
                }
                else
                {
                    RetObj["Result"] = "false";
                }
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static FSSOperationResult<object> LegDateChange(string departdate, string formate) 
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            PreflightTripViewModel Trip = new PreflightTripViewModel();

            DateTime? dt = null;

            dt = dt.FSSParseDateTime(departdate, formate);

            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                if (Trip.PreflightMain.EditMode == true)
                {
                    if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                    {
                        PreflightLegsLite preLegsLite = new PreflightLegsLite();
                        List<PreflightLegViewModel> flightLeg = Trip.PreflightLegs.Where(p => p.IsDeleted == false).OrderBy(p => p.LegNUM).ToList();
                        double dateDifference = 0;
                        foreach (PreflightLegViewModel Leg in flightLeg)
                        {
                            if (Leg.LegNUM >= 1)
                            {
                                if (Leg.LegNUM == 1)
                                {
                                    if (Leg.DepartureDTTMLocal != null)
                                    {
                                        DateTime? initialDatetime = null;
                                        initialDatetime = Leg.DepartureDTTMLocal!=null?((DateTime)Leg.DepartureDTTMLocal).Date:(DateTime?) null;
                                        if (dt != null && initialDatetime != null)
                                        {
                                            TimeSpan? t = dt - initialDatetime;
                                            dateDifference = t.Value.Days;
                                        }
                                    }
                                    else if (Leg.ArrivalDTTMLocal != null)
                                    {
                                        DateTime? initialDatetime = null;
                                        initialDatetime = Leg.ArrivalDTTMLocal!=null?((DateTime)Leg.ArrivalDTTMLocal).Date:(DateTime?) null;
                                        if (dt != null && initialDatetime != null)
                                        {
                                            TimeSpan? t = dt - initialDatetime;
                                            dateDifference = t.Value.Days;
                                        }
                                    }
                                    else
                                    {
                                        if (dt != null)
                                        {
                                            Leg.DepartureDTTMLocal = new DateTime(dt.Value.Year, dt.Value.Month, dt.Value.Day);
                                            DateTime ldGmtDep = DateTime.MinValue;
                                            using (CalculationServiceClient objDstsvc = new CalculationServiceClient())
                                            {
                                                ldGmtDep = objDstsvc.GetGMT(Leg.DepartureAirport.AirportID, dt.Value, true, false);
                                                Leg.DepartureGreenwichDTTM = ldGmtDep;
                                                if (Trip.PreflightMain.HomebaseAirportID != null)
                                                {
                                                    Leg.HomeDepartureDTTM = objDstsvc.GetGMT(Trip.PreflightMain.HomebaseAirportID.Value, ldGmtDep, false, false);
                                                }
                                            }
                                        }
                                    }



                                }
                                if (Leg.DepartureDTTMLocal.HasValue == false)
                                {
                                    preLegsLite.UpdateLegsDatesCalculation(Trip, Leg, TripManagerBase.getUserPrincipal());                                    
                                }
                                if (Leg.ArrivalDTTMLocal.HasValue == false)
                                {
                                    PreflightTripManager.UpdateAllLegsCalculation();
                                }
                                if (dateDifference != 0)
                                {
                                    if (Leg.LegID != 0 && Leg.State != TripEntityState.Modified)
                                        Leg.State = TripEntityState.Modified;
                                    Leg.DepartureDTTMLocal = Leg.DepartureDTTMLocal != null ? ((DateTime)Leg.DepartureDTTMLocal).AddDays(dateDifference) : Leg.DepartureDTTMLocal;
                                    Leg.DepartureGreenwichDTTM = Leg.DepartureGreenwichDTTM != null ? ((DateTime)Leg.DepartureGreenwichDTTM).AddDays(dateDifference) : Leg.DepartureGreenwichDTTM;
                                    Leg.HomeDepartureDTTM = Leg.HomeDepartureDTTM != null ? ((DateTime)Leg.HomeDepartureDTTM).AddDays(dateDifference) : Leg.HomeDepartureDTTM;
                                    Leg.ArrivalDTTMLocal = Leg.ArrivalDTTMLocal != null ? ((DateTime)Leg.ArrivalDTTMLocal).AddDays(dateDifference) : Leg.ArrivalDTTMLocal;
                                    Leg.ArrivalGreenwichDTTM = Leg.ArrivalGreenwichDTTM != null ? ((DateTime)Leg.ArrivalGreenwichDTTM).AddDays(dateDifference) : Leg.ArrivalGreenwichDTTM;
                                    Leg.HomeArrivalDTTM = Leg.HomeArrivalDTTM != null ? ((DateTime)Leg.HomeArrivalDTTM).AddDays(dateDifference) : Leg.HomeArrivalDTTM;
                                }
                            }
                        }

                        Trip.PreflightLegs = flightLeg;
                        HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;

                        PreflightCrewLite preflightCrewLite = new PreflightCrewLite();
                        preflightCrewLite.UpdateCrewHotelDates(dateDifference);
                        
                        PreflightPaxLite preflightPaxLite = new PreflightPaxLite();
                        preflightPaxLite.UpdatePaxHotelDates(dateDifference);
                        result.Result = "True";
                        return result;
                    }
                }
            }
            result.Result = "False";
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> DepartureIcao_Validate_Retrieve(string Departure, string LegNum, string TimeDisplayTenMin)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            Hashtable RetObj = new Hashtable();
            List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
            Dictionary<string, string> LegTabStrip = new Dictionary<string, string>();
            PreflightTripViewModel Trip = new PreflightTripViewModel();
            if (!string.IsNullOrWhiteSpace(Departure.Trim()))
            {
                using (
                    FlightPakMasterService.MasterCatalogServiceClient objDstsvc =
                        new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = objDstsvc.GetAirportByAirportICaoID(Departure.ToUpper().Trim());

                    if (objRetVal.ReturnFlag && objRetVal.EntityList != null && objRetVal.EntityList.Count > 0)
                    {
                        AirportLists = objRetVal.EntityList;
                        RetObj["AirportData"] = AirportLists.FirstOrDefault();
                        RetObj["Result"] = "1";
                        RetObj["DepartsTakeoffbias"] =
                            Utilities.Preflight_ConvertTenthsToMins(
                                AirportLists[0].TakeoffBIAS != null ? AirportLists[0].TakeoffBIAS.ToString() : "0.0",
                                Convert.ToDecimal(TimeDisplayTenMin));
                        RetObj["DepartsLandingbias"] =
                            Utilities.Preflight_ConvertTenthsToMins(
                                AirportLists[0].LandingBIAS != null ? AirportLists[0].LandingBIAS.ToString() : "0.0",
                                Convert.ToDecimal(TimeDisplayTenMin));
                        if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                        {
                            Trip =
                                (PreflightTripViewModel)
                                    HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                            int indexofLeg = Trip.GetPreflightLegIndex(LegNum);
                            Trip.PreflightLegs[indexofLeg].DepartureAirport =
                                (AirportViewModel) new AirportViewModel().InjectFrom(AirportLists.FirstOrDefault());
                            LegChangedSession(indexofLeg);
                            if (Trip.PreflightLegPreflightLogisticsList.ContainsKey(LegNum))
                            {
                                Trip.PreflightLegPreflightLogisticsList[LegNum].PreflightDepFboListViewModel =
                                    PreflightUtils.SetIsChoiceFBOToLogistics(
                                        Trip.PreflightLegPreflightLogisticsList[LegNum].PreflightDepFboListViewModel,
                                        Trip.PreflightLegs[indexofLeg].DepartICAOID);
                                Trip.PreflightLegPreflightLogisticsList[LegNum].PreflightDepFboListViewModel.State =
                                    Trip.PreflightLegPreflightLogisticsList[LegNum].PreflightDepFboListViewModel
                                        .PreflightFBOID > 0
                                        ? TripEntityState.Modified
                                        : TripEntityState.Added;
                            }
                        }
                    }
                    else
                    {
                        Trip = (PreflightTripViewModel) HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                        Trip.PreflightLegs[Trip.GetPreflightLegIndex(LegNum)].DepartureAirport.AirportID = 0;
                        RetObj["AitrportData"] = AirportLists;
                        RetObj["DepartsTakeoffbias"] = "00:00";
                        RetObj["DepartsLandingbias"] = "00:00";
                        RetObj["AirportType"] = "DOM";
                        RetObj["Result"] = "0";
                    }
                }
            }
            else
            {
                Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                Trip.PreflightLegs[Trip.GetPreflightLegIndex(LegNum)].DepartureAirport.AirportID = 0;
                RetObj["AitrportData"] = AirportLists;
                RetObj["DepartsTakeoffbias"] = "00:00";
                RetObj["DepartsLandingbias"] = "00:00";
                RetObj["AirportType"] = "DOM";
                RetObj["Result"] = "0";
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> ArrivalIcao_Validate_Retrieve(string arrival, string LegNUM, string TimeDisplayTenMin)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            Hashtable RetObj = new Hashtable();
            List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
            Dictionary<string,string> LegTabStrip=new Dictionary<string, string>();
            PreflightTripViewModel Trip = new PreflightTripViewModel();
            if (!string.IsNullOrWhiteSpace(arrival.Trim()))
            {
                using (
                    FlightPakMasterService.MasterCatalogServiceClient objDstsvc =
                        new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = objDstsvc.GetAirportByAirportICaoID(arrival.ToUpper().Trim());
                    if (objRetVal.ReturnFlag && objRetVal.EntityList != null && objRetVal.EntityList.Count > 0)
                    {
                        AirportLists = objRetVal.EntityList;
                        RetObj["AirportData"] = AirportLists.FirstOrDefault();
                        RetObj["Result"] = "1";
                        RetObj["ArrivesTakeoffbias"] =
                            Utilities.Preflight_ConvertTenthsToMins(
                                AirportLists[0].TakeoffBIAS != null ? AirportLists[0].TakeoffBIAS.ToString() : "0.0",
                                Convert.ToDecimal(TimeDisplayTenMin));
                        RetObj["ArrivesLandingbias"] =
                            Utilities.Preflight_ConvertTenthsToMins(
                                AirportLists[0].LandingBIAS != null ? AirportLists[0].LandingBIAS.ToString() : "0.0",
                                Convert.ToDecimal(TimeDisplayTenMin));

                        if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                        {
                            Trip =
                                (PreflightTripViewModel)
                                    HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                            int indexofLeg = Trip.GetPreflightLegIndex(LegNUM);
                            Trip.PreflightLegs[indexofLeg].ArrivalAirport =
                                (AirportViewModel) new AirportViewModel().InjectFrom(AirportLists.FirstOrDefault());
                            RetObj["AriportViewModel"] = Trip.PreflightLegs[indexofLeg].ArrivalAirport;
                            Trip.PreflightLegs[indexofLeg].CheckGroup = Convert.ToString(RetObj["AirportType"]);
                            if (Trip.PreflightLegPreflightLogisticsList.ContainsKey(LegNUM))
                            {
                                Trip.PreflightLegPreflightLogisticsList[LegNUM].PreflightArrFboListViewModel =
                                    PreflightUtils.SetIsChoiceFBOToLogistics(
                                        Trip.PreflightLegPreflightLogisticsList[LegNUM].PreflightArrFboListViewModel,
                                        Trip.PreflightLegs[indexofLeg].ArriveICAOID);
                                Trip.PreflightLegPreflightLogisticsList[LegNUM].PreflightArrFboListViewModel.State =
                                    Trip.PreflightLegPreflightLogisticsList[LegNUM].PreflightArrFboListViewModel
                                        .PreflightFBOID > 0
                                        ? TripEntityState.Modified
                                        : TripEntityState.Added;
                            }
                            LegChangedSession(indexofLeg);
                        }
                        HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
                    }
                    else
                    {
                        RetObj["AitrportData"] = AirportLists;
                        if (TimeDisplayTenMin == "1")
                        {
                            RetObj["ArrivesTakeoffbias"] = "0.0";
                            RetObj["ArrivesLandingbias"] = "0.0";
                        }
                        else
                        {
                            RetObj["ArrivesTakeoffbias"] = "00:00";
                            RetObj["ArrivesLandingbias"] = "00:00";
                        }

                        RetObj["AirportType"] = "DOM";
                        RetObj["Result"] = "0";
                    }
                }
            }
            else
            {
                Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                Trip.PreflightLegs[Trip.GetPreflightLegIndex(LegNUM)].ArrivalAirport.AirportID = 0;
                RetObj["AitrportData"] = AirportLists;
                if (TimeDisplayTenMin == "1")
                {
                    RetObj["ArrivesTakeoffbias"] = "0.0";
                    RetObj["ArrivesLandingbias"] = "0.0";
                }
                else
                {
                    RetObj["ArrivesTakeoffbias"] = "00:00";
                    RetObj["ArrivesLandingbias"] = "00:00";
                }

                RetObj["AirportType"] = "DOM";
                RetObj["Result"] = "0";
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> Category_Validate_Retrieve(string CategoryCD, string LegNum)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            Hashtable RetObj = new Hashtable();
            List<FlightPak.Web.FlightPakMasterService.GetAllFlightCategoryWithFilters> Category = new List<FlightPak.Web.FlightPakMasterService.GetAllFlightCategoryWithFilters>();
            PreflightTripViewModel Trip = new PreflightTripViewModel();

            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetAllFlightCategoryWithFilters(0, 0, CategoryCD.ToUpper().Trim(), true);
                if (objRetVal.ReturnFlag)
                {
                    Category = objRetVal.EntityList;
                    if (Category != null && Category.Count > 0)
                    {
                        if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                        {
                            Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                            int indexofLeg = Trip.GetPreflightLegIndex(LegNum);
                            Trip.PreflightLegs[indexofLeg].isDeadCategory = Category[0].IsDeadorFerryHead != null ? (bool)Category[0].IsDeadorFerryHead : false;

                            RetObj["isDeadCategory"] = Trip.PreflightLegs[indexofLeg].isDeadCategory;

                            if (Trip.PreflightLegs != null &&
                                Category[0].IsDeadorFerryHead==true &&
                                Trip.PreflightLegPassengers.ContainsKey(LegNum) &&
                                Trip.PreflightLegPassengers[LegNum] != null &&
                                Trip.PreflightLegPassengers[LegNum].Count > 0)
                            {
                                RetObj["Result"] = "1";
                                RetObj["DeadCategory"] = System.Web.HttpUtility.HtmlEncode(Category[0].FlightCategoryID.ToString());
                                RetObj["CategoryCD"] = System.Web.HttpUtility.HtmlEncode(Category[0].FlightCatagoryCD);
                                RetObj["hdCategoryID"] = null;
                                RetObj["ShowConfirmPopUp"] = true;
                            }
                            else
                            {
                                RetObj["Result"] = "1";
                                RetObj["CategoryCD"] = System.Web.HttpUtility.HtmlEncode(Category[0].FlightCatagoryCD);
                                RetObj["hdCategoryID"] = System.Web.HttpUtility.HtmlEncode(Category[0].FlightCategoryID.ToString());
                                RetObj["DeadCategory"] = null;
                                RetObj["ShowConfirmPopUp"] = false;
                            }
                        }
                    }
                    else
                    {
                        RetObj["Result"] = "0";
                        RetObj["CategoryCD"] = null;
                        RetObj["hdCategoryID"] = null;
                        RetObj["DeadCategory"] = null;
                        RetObj["ShowConfirmPopUp"] = false;
                    }
                }
                else
                {
                    RetObj["Result"] = "0";
                    RetObj["CategoryCD"] = null;
                    RetObj["hdCategoryID"] = null;
                    RetObj["DeadCategory"] = null;
                    RetObj["ShowConfirmPopUp"] = false;
                }
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> CategoryChange_PassengersDelete(string LegNum, bool isDelete)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            Hashtable RetObj = new Hashtable();
            PreflightTripViewModel Trip = new PreflightTripViewModel();
            int oldSet = 0;

            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                int indexofLeg = Trip.GetPreflightLegIndex(LegNum);
                if (Trip.PreflightLegPassengers.ContainsKey(LegNum) && Trip.PreflightLegs != null &&
                    Trip.PreflightLegPassengers.ContainsKey(LegNum) && Trip.PreflightLegPassengers[LegNum] != null && Trip.PreflightLegPassengers[LegNum].Count>0)
                {
                    if (isDelete == true)
                    {
                        oldSet = Trip.PreflightLegs[indexofLeg].PassengerTotal ?? 0;
                        foreach (PassengerViewModel PrePass in Trip.PreflightLegPassengers[LegNum].ToList())
                        {
                            if (PrePass.PreflightPassengerListID != 0)
                            {
                                PrePass.FlightPurposeID = 0;
                                PrePass.IsDeleted = true;
                                PrePass.State = TripEntityState.Deleted;
                            }
                            else
                            {
                                Trip.PreflightLegPassengers[LegNum].Remove(PrePass);
                            }
                        }
                        Trip.PreflightLegs[indexofLeg].PassengerTotal = 0;
                        Trip.PreflightLegs[indexofLeg].ReservationAvailable = Trip.PreflightLegs[indexofLeg].ReservationAvailable + oldSet;

                        RetObj["Result"] = "1";
                        RetObj["tbPax"] = System.Web.HttpUtility.HtmlEncode(Trip.PreflightLegs[indexofLeg].PassengerTotal.ToString());
                        RetObj["tbAvail"] = System.Web.HttpUtility.HtmlEncode(Trip.PreflightLegs[indexofLeg].ReservationAvailable.ToString());
                    }
                    else {
                        RetObj["Result"] = "1";
                        RetObj["FlightCatagoryCD"] = Trip.PreflightLegs[indexofLeg].FlightCatagory == null ? "" : Trip.PreflightLegs[indexofLeg].FlightCatagory.FlightCatagoryCD;
                        RetObj["FlightCatagoryID"] = System.Web.HttpUtility.HtmlEncode(Trip.PreflightLegs[indexofLeg].FlightCategoryID != null ? Trip.PreflightLegs[indexofLeg].FlightCategoryID.ToString() : "0");
                    }
                    HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
                }
                else
                {
                    RetObj["Result"]="0";
                }
            }
            else
            {
                RetObj["Result"] = "0";
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> CrewDutyRule_Validate_Retrieve(string CrewDutyRuleCD, string LegNum, bool isEndDuty)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            Hashtable RetObj = new Hashtable();
            List<FlightPak.Web.FlightPakMasterService.CrewDutyRules> CrewDutyRule = new List<FlightPak.Web.FlightPakMasterService.CrewDutyRules>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetCrewDutyRulesWithFilters(0, CrewDutyRuleCD.ToUpper().Trim(), false);
                if (objRetVal.ReturnFlag)
                {
                    CrewDutyRule = objRetVal.EntityList;
                    if (CrewDutyRule != null && CrewDutyRule.Count > 0 && string.IsNullOrEmpty(LegNum)==false)
                    {
                        CrewDutyRulesForAllLeg(CrewDutyRule[0].CrewDutyRulesID, CrewDutyRule[0].CrewDutyRuleCD, CrewDutyRule[0].CrewDutyRulesDescription, CrewDutyRule[0].FedAviatRegNum, LegNum, isEndDuty);
                        RetObj["Result"] = "1";
                        RetObj["CrewDutyRule"] = CrewDutyRule;
                    }
                    else if(CrewDutyRule != null && CrewDutyRule.Count > 0)
                    {
                        RetObj["Result"] = "1";
                        RetObj["CrewDutyRule"] = CrewDutyRule;
                    }
                    else
                    {
                        RetObj["Result"] = "0";
                    }
                }
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> PowerSetting_Validate_Retrieve(string Power, string AircraftID, string TimeDisplayTenMin)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            Hashtable RetObj = new Hashtable();
            List<FlightPakMasterService.Aircraft> retAircraft = new List<FlightPakMasterService.Aircraft>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objPowerSetting = objDstsvc.GetAircraftWithFilters(string.Empty, Convert.ToInt64(AircraftID), false);
                if (objPowerSetting.ReturnFlag)
                {
                    retAircraft = objPowerSetting.EntityList;
                    if (retAircraft != null && retAircraft.Count > 0)
                    {
                        switch (Power)
                        {
                            case "1":
                                RetObj["Result"] = "1";
                                RetObj["TAS"] = retAircraft[0].PowerSettings1TrueAirSpeed != null ? retAircraft[0].PowerSettings1TrueAirSpeed.ToString() : "0";
                                if (retAircraft[0].PowerSettings1TakeOffBias != null)
                                    RetObj["DepartsTakeoffbias"] = Utilities.Preflight_ConvertTenthsToMins(retAircraft[0].PowerSettings1TakeOffBias != null ? retAircraft[0].PowerSettings1TakeOffBias.ToString() : "00:00", Convert.ToDecimal(TimeDisplayTenMin));
                                if (retAircraft[0].PowerSettings1LandingBias != null)
                                    RetObj["DepartsLandingbias"] = Utilities.Preflight_ConvertTenthsToMins(retAircraft[0].PowerSettings1LandingBias != null ? retAircraft[0].PowerSettings1LandingBias.ToString() : "00:00", Convert.ToDecimal(TimeDisplayTenMin));
                                break;
                            case "2":
                                RetObj["Result"] = "1";
                                RetObj["TAS"] = retAircraft[0].PowerSettings2TrueAirSpeed != null ? retAircraft[0].PowerSettings2TrueAirSpeed.ToString() : "0";
                                if (retAircraft[0].PowerSettings2TakeOffBias != null)
                                    RetObj["DepartsTakeoffbias"] = Utilities.Preflight_ConvertTenthsToMins(retAircraft[0].PowerSettings2TakeOffBias != null ? retAircraft[0].PowerSettings2TakeOffBias.ToString() : "00:00", Convert.ToDecimal(TimeDisplayTenMin));
                                if (retAircraft[0].PowerSettings2LandingBias != null)
                                    RetObj["DepartsLandingbias"] = Utilities.Preflight_ConvertTenthsToMins(retAircraft[0].PowerSettings2LandingBias != null ? retAircraft[0].PowerSettings2LandingBias.ToString() : "00:00", Convert.ToDecimal(TimeDisplayTenMin));
                                break;
                            case "3":
                                RetObj["Result"] = "1";
                                RetObj["TAS"] = retAircraft[0].PowerSettings3TrueAirSpeed != null ? retAircraft[0].PowerSettings3TrueAirSpeed.ToString() : "0";
                                if (retAircraft[0].PowerSettings3TakeOffBias != null)
                                    RetObj["DepartsTakeoffbias"] = Utilities.Preflight_ConvertTenthsToMins(retAircraft[0].PowerSettings3TakeOffBias != null ? retAircraft[0].PowerSettings3TakeOffBias.ToString() : "00:00", Convert.ToDecimal(TimeDisplayTenMin));
                                if (retAircraft[0].PowerSettings3LandingBias != null)
                                    RetObj["DepartsLandingbias"] = Utilities.Preflight_ConvertTenthsToMins(retAircraft[0].PowerSettings3LandingBias != null ? retAircraft[0].PowerSettings3LandingBias.ToString() : "00:00", Convert.ToDecimal(TimeDisplayTenMin));
                                break;
                            default:
                                RetObj["Result"] = "0";
                                if (Convert.ToDecimal(TimeDisplayTenMin) == 2)
                                {
                                    RetObj["DepartsTakeoffbias"] = "00:00";
                                    RetObj["DepartsLandingbias"] = "00:00";
                                }
                                else
                                {
                                    RetObj["DepartsTakeoffbias"] = "0.0";
                                    RetObj["DepartsLandingbias"] = "0.0";
                                }
                                RetObj["TAS"] = "0.0";
                                break;
                        }
                    }
                    else
                    {
                        RetObj["Result"] = "0";
                        if (Convert.ToDecimal(TimeDisplayTenMin) == 2)
                        {
                            RetObj["DepartsTakeoffbias"] = "00:00";
                            RetObj["DepartsLandingbias"] = "00:00";
                        }
                        else
                        {
                            RetObj["DepartsTakeoffbias"] = "0.0";
                            RetObj["DepartsLandingbias"] = "0.0";
                        }
                        RetObj["TAS"] = "0.0";
                    }
                }
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> CalculateLegDateTime_Validate_Retrieve(string DateFormat, bool IsAutomaticCalcLegTimes, long DepartICAO, LegDateTime date)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            PreflightTripViewModel Trip = new PreflightTripViewModel();
            Hashtable Retobj = new Hashtable();
            DateTime? dt = null;
            //if (!string.IsNullOrEmpty(date.DepLocalDate))
            //{
                Trip = (PreflightTripViewModel) HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                dt = Trip.PreflightMain.EstDepartureDT;
                using (CalculationService.CalculationServiceClient ObjCalculation = new CalculationService.CalculationServiceClient())
                {
                    Retobj =
                        PreUtilities.CalculateLegDateTime(dt,
                            date.DepLocalTime, date.DepHomeTime, date.DepUtcTime, date.ArrLocalTime, date.ArrHomeTime,
                            date.ArrUtcTime,
                            DateFormat, DepartICAO.ToString(), true, ObjCalculation);
                }
            //}
                result.Result = Retobj;
                return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GMT_Validate_Retrieve(string date, string time, string Dateformat, string airportid, bool isGMT)
        {
            Dictionary<string,string> RetObj=new Dictionary<string, string>();
            DateTime ldGmtDep = new DateTime();
            if(!string.IsNullOrEmpty(airportid))
            {
                    string[] TimeArray = time.Split(':');
                    string _hour = "00";
                    string _minute = "00";
                    if (!string.IsNullOrWhiteSpace(TimeArray[0]))
                        _hour = TimeArray[0];
                    if (!string.IsNullOrWhiteSpace(TimeArray[1]))
                        _minute = TimeArray[1];

                    int StartHrs = Convert.ToInt16(_hour);
                    int StartMts = Convert.ToInt16(_minute);

                    RetObj["time"] = MiscUtils.CalculationTimeAdjustment(_hour + ":" + _minute);

                    DateTime? dt = new DateTime();
                    dt = dt.FSSParseDateTime(date, Dateformat);
                    dt = dt.Value.AddHours(StartHrs);
                    dt = dt.Value.AddMinutes(StartMts);

                    using (CalculationService.CalculationServiceClient ObjCalculation = new CalculationService.CalculationServiceClient())
                    {
                    ldGmtDep = ObjCalculation.GetGMT(Convert.ToInt64(airportid), dt.Value, true, isGMT);
                    }

                    string UtcDate = String.Format(CultureInfo.InvariantCulture, "{0:" + Dateformat + "}", ldGmtDep);
                    string UtcTime = String.Format("{0:00}:{0:00}", ldGmtDep.Hour, ldGmtDep.Minute);
                    RetObj.Add("UtcDate", UtcDate);
                    RetObj.Add("UtcTime", UtcTime);
                }
            return JsonConvert.SerializeObject(RetObj);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Dictionary<string, object>> PowersettingGrid_Validate_Retrieve(string aircraftId, decimal TenMin)
        {
            FSSOperationResult<Dictionary<string, object>> result = new FSSOperationResult<Dictionary<string, object>>();
            if (!result.IsAuthorized())
                return result;
            Dictionary<string, object> RetObj = new Dictionary<string, object>();
            FlightPakMasterService.Aircraft retAircraft = new FlightPakMasterService.Aircraft();
            Hashtable PowerProp = new Hashtable();
            List<object> PowerValue = new List<object>();
            if (!string.IsNullOrEmpty(aircraftId))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var PowerSetting = objDstsvc.GetAircraftWithFilters(string.Empty, Convert.ToInt64(aircraftId), false);
                    if (PowerSetting.ReturnFlag)
                    {
                        if (PowerSetting.EntityList != null && PowerSetting.EntityList.Count > 0)
                        {
                            retAircraft = PowerSetting.EntityList.FirstOrDefault();

                            RetObj["lbAircraft"] = System.Web.HttpUtility.HtmlEncode(retAircraft.AircraftDescription);
                            RetObj["PowerGrid"] = setPowerValue(retAircraft, TenMin);
                        }
                        else
                        {
                            RetObj["lbAircraft"] = "";
                            RetObj["PowerGrid"] = setPowerValue(null, TenMin);
                        }
                    }
                    else
                    {
                        RetObj["lbAircraft"] = "";
                        RetObj["PowerGrid"] = setPowerValue(null, TenMin);
                    }
                }
            }
            else {
                RetObj["lbAircraft"]="";
                RetObj["PowerGrid"]=setPowerValue(null, TenMin);
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Dictionary<string, object>> PreflightCalculation_Validate_Retrieve(DepartAirportDetails depAirport, ArrivalAirportDetails arrAirport, HomebaseAirportDetails homebaseAirport, AircraftDetails airecraft,
            LegDateTime date, decimal TenMin, bool Kilo, int WindRelibility, string localdate, string dateformat, string PowerSetting, decimal ElapseTime,
            string LegNum, long? CrewDutyRule, decimal? OverrideValue, bool IsEndDuty, bool isDepartureConfirmed, decimal? Distance, string EventChange, bool IsAutomaticCalcLegTimes)
        {
            FSSOperationResult<Dictionary<string, object>> result = new FSSOperationResult<Dictionary<string, object>>();
            if (!result.IsAuthorized())
                return result;
            PreflightTripViewModel Trip = new PreflightTripViewModel();
            Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            int indexOfLeg = Trip.GetPreflightLegIndex(LegNum);
            DateTime? dt = null;
            //dt = string.IsNullOrEmpty(date.Changed) ? (DateTime?)null : dt.FSSParseDateTime(date.Changed, dateformat);
            Dictionary<string, object> RetObj = new Dictionary<string, object>();
            double _wind, _landBias, _tas, _bias, _miles = 0;
            string _ete = null;

            Trip.PreflightLegs[indexOfLeg].IsDutyEnd = IsEndDuty;
            Trip.PreflightLegs[indexOfLeg].OverrideValue = OverrideValue;
            Trip.PreflightLegs[indexOfLeg].CrewDutyRulesID = CrewDutyRule;

            //Make sure we re-populate the Airport details again as the data from javascript cannot be trusted
            using (FlightPakMasterService.MasterCatalogServiceClient masterService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                long arrivalAirportId = 0;
                long.TryParse(arrAirport.ArrivalID, out arrivalAirportId);
                if(arrivalAirportId != 0){
                    AirportViewModel arrivalAirportVM = DBCatalogsDataLoader.GetAirportInfo_FromAirportId(arrivalAirportId);
                    if (arrivalAirportVM != null)
                    {
                        arrAirport.arrDayLightSavingStartDT = arrivalAirportVM.DayLightSavingStartDT;
                        arrAirport.arrDayLightSavingEndDT = arrivalAirportVM.DayLightSavingEndDT;
                    }
                }

                long departureAirportId = 0;
                long.TryParse(depAirport.DepID, out departureAirportId);
                if(departureAirportId != 0)
                {
                    AirportViewModel departureAirportVM = DBCatalogsDataLoader.GetAirportInfo_FromAirportId(departureAirportId);
                    if(departureAirportVM != null)
                    {
                        depAirport.depDayLightSavingStartDT = departureAirportVM.DayLightSavingStartDT;
                        depAirport.depDayLightSavingEndDT = departureAirportVM.DayLightSavingEndDT;
                    }
                }
            }
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            using (CalculationService.CalculationServiceClient ObjCalculation = new CalculationService.CalculationServiceClient())
            {
                switch (EventChange)
                {
                    case "Airport":
                        if (!string.IsNullOrEmpty(depAirport.DepID) && !string.IsNullOrEmpty(arrAirport.ArrivalID))
                        {
                            _miles =(double) PreUtilities.CalculateDistance(Kilo, depAirport.deplatdeg, depAirport.deplatmin,
                                depAirport.lcdeplatdir, depAirport.deplngdeg, depAirport.deplngmin,
                                depAirport.lcdeplngdir, arrAirport.arrlatdeg, arrAirport.arrlatmin,
                                arrAirport.lcArrLatDir, arrAirport.arrlngdeg, arrAirport.arrlngmin,
                                arrAirport.lcarrlngdir);
                        }
                        else
                        {
                            _miles = 0;
                        }
                        RetObj["Distance"] = PreUtilities.ConvertMilesToKilometer(Kilo, Convert.ToDecimal(_miles));
                        RetObj["Miles"] = Convert.ToString(_miles);
                        Trip.PreflightLegs[indexOfLeg].Distance = Convert.ToDecimal(_miles);

                        RetObj["CalculateBias"] = PreUtilities.CalculateBias(PowerSetting, airecraft.PowerSettings1TakeOffBias, airecraft.PowerSettings1LandingBias, airecraft.PowerSettings1TrueAirSpeed, airecraft.PowerSettings2TakeOffBias, airecraft.PowerSettings2LandingBias, airecraft.PowerSettings2TrueAirSpeed, airecraft.PowerSettings3TakeOffBias, airecraft.PowerSettings3LandingBias, airecraft.PowerSettings3TrueAirSpeed, depAirport.DepAirportTakeoffBIAS, arrAirport.ArrAirportLandingBIAS, TenMin, out _bias, out _landBias, out _tas);

                        foreach (var retBias in (Dictionary<string, string>)RetObj["CalculateBias"])
                        {
                            if (retBias.Key == "Bias")
                                Trip.PreflightLegs[indexOfLeg].StrTakeoffBIAS = retBias.Value;
                            if (retBias.Key == "LandBias")
                                Trip.PreflightLegs[indexOfLeg].StrLandingBias = retBias.Value;
                            if (retBias.Key == "TAS")
                                Trip.PreflightLegs[indexOfLeg].TrueAirSpeed = string.IsNullOrWhiteSpace(retBias.Value) ? 0 : Convert.ToDecimal(retBias.Value);
                        }

                        RetObj.Add("Wind", PreUtilities.CalculateWind(airecraft.aircraftWindAltitude, WindRelibility, depAirport.deplatdeg, depAirport.deplatmin, depAirport.lcdeplatdir, depAirport.deplngdeg, depAirport.deplngmin, depAirport.lcdeplngdir, depAirport.lnDepartWindZone, arrAirport.arrlatdeg, arrAirport.arrlatmin, arrAirport.lcArrLatDir, arrAirport.arrlngdeg, arrAirport.arrlngmin, arrAirport.lcarrlngdir, arrAirport.lnArrivWindZone, localdate, ObjCalculation, dateformat).ToString());
                        _wind = Convert.ToDouble(RetObj["Wind"]);
                        Trip.PreflightLegs[indexOfLeg].WindsBoeingTable = (decimal?)_wind;

                        _ete = PreUtilities.GetIcaoEte(_wind, _landBias, _tas, _bias, _miles, airecraft.IsFixedRotary, TenMin, ElapseTime);
                        RetObj["ETE"] = _ete.ToString();
                        Trip.PreflightLegs[indexOfLeg].ElapseTM = Convert.ToDecimal(TenMin == 2 ? Utilities.Preflight_ConvertMinstoTenths(_ete, TenMin) : _ete);

                       // date = PreUtilities.ClearAllDatesBasedonDeptOrArrival(depAirport, arrAirport, date, isDepartureConfirmed, dateformat);

                        Dictionary<string, string> LegDate = new Dictionary<string, string>();
                        LegDate = PreUtilities.CalculateDateTime(isDepartureConfirmed,depAirport.depIsDayLightSaving, depAirport.depDayLightSavingStartDT, depAirport.depDayLightSavingEndDT,
                        depAirport.depDaylLightSavingStartTM, depAirport.depDayLightSavingEndTM, depAirport.depOffsetToGMT, arrAirport.arrIsDayLightSaving, arrAirport.arrDayLightSavingStartDT,
                        arrAirport.arrDayLightSavingEndDT, arrAirport.arrDaylLightSavingStartTM, arrAirport.arrDayLightSavingEndTM, arrAirport.arrOffsetToGMT,
                        homebaseAirport.homeIsDayLightSaving, homebaseAirport.homeDayLightSavingStartDT, homebaseAirport.homeDayLightSavingEndDT, homebaseAirport.homeDaylLightSavingStartTM,
                        homebaseAirport.homeDayLightSavingEndTM, homebaseAirport.homeOffsetToGMT, date.DepLocalDate, dateformat, date.ArrLocalDate, date.ArrLocalTime,
                        depAirport.DepID, arrAirport.ArrivalID, _ete, Convert.ToString(TenMin), date.DepUtcTime, date.DepUtcDate, arrAirport.ArrivalCD, depAirport.DepCD, date.ArrUtcDate, date.ArrUtcTime, homebaseAirport.HomebaseId,
                        date.DepLocalTime, date.DepHomeDate, date.DepHomeTime, date.ArrHomeDate, date.ArrHomeTime, ElapseTime, date.Changed, IsAutomaticCalcLegTimes);
                        RetObj["LegDate"] = LegDate;

                        DateTime? LegDateTime = null;

                        Trip.PreflightLegs[indexOfLeg].DepartureDTTMLocal = string.IsNullOrEmpty(LegDate["DeptLocalDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptLocalDate"], LegDate["DeptLocalTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].DepartureGreenwichDTTM = string.IsNullOrEmpty(LegDate["DeptUtcDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptUtcDate"], LegDate["DeptUtcTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].HomeDepartureDTTM = string.IsNullOrEmpty(LegDate["DeptHomeDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptHomeDate"], LegDate["DeptHomeTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].ArrivalDTTMLocal = string.IsNullOrEmpty(LegDate["ArrivalLocalDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalLocalDate"], LegDate["ArrivalLocalTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].ArrivalGreenwichDTTM = string.IsNullOrEmpty(LegDate["ArrivalUtcDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalUtcDate"], LegDate["ArrivalUtcTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].HomeArrivalDTTM = string.IsNullOrEmpty(LegDate["ArrivalHomeDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalHomeDate"], LegDate["ArrivalHomeTime"]), dateformat + " HH:mm");

                        Dictionary<string, object> FARRules = PreUtilities.CalculatePreflightFARRules(Trip, Convert.ToInt16(LegNum), objDstsvc);
                        Trip = (PreflightTripViewModel)FARRules["Trip"];
                        RetObj["FARRules"] = FARRules["FARRules"];

                        Dictionary<string, object> FlightCost = new Dictionary<string, object>();
                        FlightCost = PreUtilities.CalculateFlightCost(airecraft.ChargeRate, airecraft.ChargeUnit, Trip, Convert.ToString(airecraft.AirCraftId), objDstsvc, LegNum);
                        Trip = (PreflightTripViewModel)FlightCost["Trip"];
                        RetObj["FlightCost"] = Convert.ToString(FlightCost["FlightCost"]);
                        Trip.PreflightMain.OldHomebaseID = Trip.PreflightMain.HomebaseID;
                        if (indexOfLeg == 0 && LegNum == "1" && Trip.PreflightLegs[indexOfLeg].DepartureDTTMLocal.HasValue)
                        {
                            Trip.PreflightMain.EstDepartureDT = new DateTime(Trip.PreflightLegs[indexOfLeg].DepartureDTTMLocal.Value.Ticks, DateTimeKind.Local);
                        }                      
                        break;
                    case "Miles":
                        if (Distance <= 0)
                        {
                            _miles = (double)PreUtilities.CalculateDistance(Kilo, depAirport.deplatdeg, depAirport.deplatmin,
                                depAirport.lcdeplatdir, depAirport.deplngdeg,
                                depAirport.deplngmin, depAirport.lcdeplngdir, arrAirport.arrlatdeg, arrAirport.arrlatmin,
                                arrAirport.lcArrLatDir,
                                arrAirport.arrlngdeg, arrAirport.arrlngmin, arrAirport.lcarrlngdir);
                            RetObj["Distance"] = PreUtilities.ConvertMilesToKilometer(Kilo, Convert.ToDecimal(_miles));
                            RetObj["Miles"] = Convert.ToString(_miles);
                            Trip.PreflightLegs[indexOfLeg].Distance = Convert.ToDecimal(_miles);
                        }
                        else
                        {
                            RetObj["Distance"] = Convert.ToString(Distance);
                            Trip.PreflightLegs[indexOfLeg].Distance = PreUtilities.ConvertKilometerToMiles(Kilo, Distance.Value);
                            _miles = (double) Trip.PreflightLegs[indexOfLeg].Distance;
                            RetObj["Miles"] = Convert.ToString(_miles);
                        }
                        
                         
                        RetObj.Add("Wind", PreUtilities.CalculateWind(airecraft.aircraftWindAltitude, WindRelibility, depAirport.deplatdeg, depAirport.deplatmin,
                        depAirport.lcdeplatdir, depAirport.deplngdeg, depAirport.deplngmin, depAirport.lcdeplngdir, depAirport.lnDepartWindZone, arrAirport.arrlatdeg,
                        arrAirport.arrlatmin, arrAirport.lcArrLatDir, arrAirport.arrlngdeg, arrAirport.arrlngmin, arrAirport.lcarrlngdir, arrAirport.lnArrivWindZone, localdate, ObjCalculation, dateformat).ToString());
                        _wind = Convert.ToDouble(RetObj["Wind"]);
                        Trip.PreflightLegs[indexOfLeg].WindsBoeingTable = (decimal?)_wind;

                        if (TenthMinuteFormat.Minute == (TenthMinuteFormat)TenMin)
                        {
                            Double.TryParse(Utilities.Preflight_ConvertMinstoTenths(airecraft.LandingBias, TenMin), out _landBias);
                            Double.TryParse(Utilities.Preflight_ConvertMinstoTenths(airecraft.Bias, TenMin), out _bias);
                        }
                        else
                        {
                            Double.TryParse(airecraft.LandingBias, out _landBias);
                            Double.TryParse(airecraft.Bias, out _bias);
                        }
                        if (String.IsNullOrWhiteSpace(Convert.ToString(airecraft.TAS)))
                            _tas = 0;
                        else
                            Double.TryParse(airecraft.TAS, out _tas);

                        _ete = PreUtilities.GetIcaoEte(_wind, _landBias, _tas, _bias, _miles, airecraft.IsFixedRotary, TenMin, ElapseTime);
                        RetObj["ETE"] = _ete.ToString();
                        Trip.PreflightLegs[indexOfLeg].ElapseTM = Convert.ToDecimal(TenMin == 2 ? Utilities.Preflight_ConvertMinstoTenths(_ete, TenMin) : _ete);

                        LegDate = new Dictionary<string, string>();
                        LegDate = PreUtilities.CalculateDateTime(isDepartureConfirmed, depAirport.depIsDayLightSaving, depAirport.depDayLightSavingStartDT, depAirport.depDayLightSavingEndDT,
                          depAirport.depDaylLightSavingStartTM, depAirport.depDayLightSavingEndTM, depAirport.depOffsetToGMT, arrAirport.arrIsDayLightSaving, arrAirport.arrDayLightSavingStartDT,
                          arrAirport.arrDayLightSavingEndDT, arrAirport.arrDaylLightSavingStartTM, arrAirport.arrDayLightSavingEndTM, arrAirport.arrOffsetToGMT,
                          homebaseAirport.homeIsDayLightSaving, homebaseAirport.homeDayLightSavingStartDT, homebaseAirport.homeDayLightSavingEndDT, homebaseAirport.homeDaylLightSavingStartTM,
                          homebaseAirport.homeDayLightSavingEndTM, homebaseAirport.homeOffsetToGMT, date.DepLocalDate, dateformat, date.ArrLocalDate, date.ArrLocalTime,
                          depAirport.DepID, arrAirport.ArrivalID, _ete, Convert.ToString(TenMin), date.DepUtcTime, date.DepUtcDate, arrAirport.ArrivalCD, depAirport.DepCD, date.ArrUtcDate, date.ArrUtcTime, homebaseAirport.HomebaseId,
                          date.DepLocalTime, date.DepHomeDate, date.DepHomeTime, date.ArrHomeDate, date.ArrHomeTime, ElapseTime, date.Changed, IsAutomaticCalcLegTimes);
                        RetObj["LegDate"] = LegDate;

                        LegDateTime = null;

                        Trip.PreflightLegs[indexOfLeg].DepartureDTTMLocal = string.IsNullOrEmpty(LegDate["DeptLocalDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptLocalDate"], LegDate["DeptLocalTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].DepartureGreenwichDTTM = string.IsNullOrEmpty(LegDate["DeptUtcDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptUtcDate"], LegDate["DeptUtcTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].HomeDepartureDTTM = string.IsNullOrEmpty(LegDate["DeptHomeDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptHomeDate"], LegDate["DeptHomeTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].ArrivalDTTMLocal = string.IsNullOrEmpty(LegDate["ArrivalLocalDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalLocalDate"], LegDate["ArrivalLocalTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].ArrivalGreenwichDTTM = string.IsNullOrEmpty(LegDate["ArrivalUtcDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalUtcDate"], LegDate["ArrivalUtcTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].HomeArrivalDTTM = string.IsNullOrEmpty(LegDate["ArrivalHomeDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalHomeDate"], LegDate["ArrivalHomeTime"]), dateformat + " HH:mm");

                        FARRules = PreUtilities.CalculatePreflightFARRules(Trip, Convert.ToInt16(LegNum), objDstsvc);
                        Trip = (PreflightTripViewModel)FARRules["Trip"];
                        RetObj["FARRules"] = FARRules["FARRules"];

                        FlightCost = new Dictionary<string, object>();
                        FlightCost = PreUtilities.CalculateFlightCost(airecraft.ChargeRate, airecraft.ChargeUnit, Trip, Convert.ToString(airecraft.AirCraftId), objDstsvc, LegNum);
                        Trip = (PreflightTripViewModel)FlightCost["Trip"];
                        RetObj["FlightCost"] = Convert.ToString(FlightCost["FlightCost"]);

                        break;
                    case "Power":

                        _miles = Convert.ToDouble(Trip.PreflightLegs[indexOfLeg].Distance);

                        RetObj["CalculateBias"] = PreUtilities.CalculateBias(PowerSetting, airecraft.PowerSettings1TakeOffBias, airecraft.PowerSettings1LandingBias, airecraft.PowerSettings1TrueAirSpeed,
                        airecraft.PowerSettings2TakeOffBias, airecraft.PowerSettings2LandingBias, airecraft.PowerSettings2TrueAirSpeed,
                        airecraft.PowerSettings3TakeOffBias, airecraft.PowerSettings3LandingBias, airecraft.PowerSettings3TrueAirSpeed,
                        depAirport.DepAirportTakeoffBIAS, arrAirport.ArrAirportLandingBIAS, TenMin, out _bias, out _landBias, out _tas);

                        RetObj.Add("Wind", PreUtilities.CalculateWind(airecraft.aircraftWindAltitude, WindRelibility, depAirport.deplatdeg, depAirport.deplatmin,
                        depAirport.lcdeplatdir, depAirport.deplngdeg, depAirport.deplngmin, depAirport.lcdeplngdir, depAirport.lnDepartWindZone, arrAirport.arrlatdeg,
                        arrAirport.arrlatmin, arrAirport.lcArrLatDir, arrAirport.arrlngdeg, arrAirport.arrlngmin, arrAirport.lcarrlngdir, arrAirport.lnArrivWindZone, localdate, ObjCalculation, dateformat).ToString());
                        _wind = Convert.ToDouble(RetObj["Wind"]);
                        Trip.PreflightLegs[indexOfLeg].WindsBoeingTable = (decimal?)_wind;

                        _ete = PreUtilities.GetIcaoEte(_wind, _landBias, _tas, _bias, _miles, airecraft.IsFixedRotary, TenMin, ElapseTime);
                        RetObj["ETE"] = _ete.ToString();
                        Trip.PreflightLegs[indexOfLeg].ElapseTM = Convert.ToDecimal(TenMin == 2 ? Utilities.Preflight_ConvertMinstoTenths(_ete, TenMin) : _ete);

                        LegDate = new Dictionary<string, string>();
                        LegDate = PreUtilities.CalculateDateTime(isDepartureConfirmed, depAirport.depIsDayLightSaving, depAirport.depDayLightSavingStartDT, depAirport.depDayLightSavingEndDT,
                        depAirport.depDaylLightSavingStartTM, depAirport.depDayLightSavingEndTM, depAirport.depOffsetToGMT, arrAirport.arrIsDayLightSaving, arrAirport.arrDayLightSavingStartDT,
                        arrAirport.arrDayLightSavingEndDT, arrAirport.arrDaylLightSavingStartTM, arrAirport.arrDayLightSavingEndTM, arrAirport.arrOffsetToGMT,
                        homebaseAirport.homeIsDayLightSaving, homebaseAirport.homeDayLightSavingStartDT, homebaseAirport.homeDayLightSavingEndDT, homebaseAirport.homeDaylLightSavingStartTM,
                        homebaseAirport.homeDayLightSavingEndTM, homebaseAirport.homeOffsetToGMT, date.DepLocalDate, dateformat, date.ArrLocalDate, date.ArrLocalTime,
                        depAirport.DepID, arrAirport.ArrivalID, _ete, Convert.ToString(TenMin), date.DepUtcTime, date.DepUtcDate, arrAirport.ArrivalCD, depAirport.DepCD, date.ArrUtcDate, date.ArrUtcTime, homebaseAirport.HomebaseId,
                        date.DepLocalTime, date.DepHomeDate, date.DepHomeTime, date.ArrHomeDate, date.ArrHomeTime, ElapseTime, date.Changed, IsAutomaticCalcLegTimes);
                        RetObj["LegDate"] = LegDate;

                        LegDateTime = null;

                        Trip.PreflightLegs[indexOfLeg].DepartureDTTMLocal = string.IsNullOrEmpty(LegDate["DeptLocalDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptLocalDate"], LegDate["DeptLocalTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].DepartureGreenwichDTTM = string.IsNullOrEmpty(LegDate["DeptUtcDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptUtcDate"], LegDate["DeptUtcTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].HomeDepartureDTTM = string.IsNullOrEmpty(LegDate["DeptHomeDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptHomeDate"], LegDate["DeptHomeTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].ArrivalDTTMLocal = string.IsNullOrEmpty(LegDate["ArrivalLocalDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalLocalDate"], LegDate["ArrivalLocalTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].ArrivalGreenwichDTTM = string.IsNullOrEmpty(LegDate["ArrivalUtcDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalUtcDate"], LegDate["ArrivalUtcTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].HomeArrivalDTTM = string.IsNullOrEmpty(LegDate["ArrivalHomeDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalHomeDate"], LegDate["ArrivalHomeTime"]), dateformat + " HH:mm");

                        FARRules = PreUtilities.CalculatePreflightFARRules(Trip, Convert.ToInt16(LegNum), objDstsvc);
                        Trip = (PreflightTripViewModel)FARRules["Trip"];
                        RetObj["FARRules"] = FARRules["FARRules"];

                        FlightCost = PreUtilities.CalculateFlightCost(airecraft.ChargeRate, airecraft.ChargeUnit, Trip, Convert.ToString(airecraft.AirCraftId), objDstsvc, LegNum);
                        Trip = (PreflightTripViewModel)FlightCost["Trip"];
                        RetObj["FlightCost"] = Convert.ToString(FlightCost["FlightCost"]);

                        break;

                    case "Bias":
                        _miles = Convert.ToDouble(Trip.PreflightLegs[indexOfLeg].Distance);
                        if (TenthMinuteFormat.Minute == (TenthMinuteFormat)TenMin)
                        {
                            Double.TryParse(Utilities.Preflight_ConvertMinstoTenths(airecraft.LandingBias, TenMin), out _landBias);
                            Double.TryParse(Utilities.Preflight_ConvertMinstoTenths(airecraft.Bias, TenMin), out _bias);
                        }
                        else
                        {
                            Double.TryParse(airecraft.LandingBias, out _landBias);
                            Double.TryParse(airecraft.Bias, out _bias);
                        }
                        if (String.IsNullOrWhiteSpace(Convert.ToString(airecraft.TAS)))
                            _tas = 0;
                        else
                            Double.TryParse(airecraft.TAS, out _tas);
                        _wind = (double)(Trip.PreflightLegs[indexOfLeg].WindsBoeingTable ?? 0);

                        _ete = PreUtilities.GetIcaoEte(_wind, _landBias, _tas, _bias, _miles, airecraft.IsFixedRotary, TenMin, ElapseTime);
                        RetObj["ETE"] = _ete.ToString();
                        Trip.PreflightLegs[indexOfLeg].ElapseTM = Convert.ToDecimal(TenMin == 2 ? Utilities.Preflight_ConvertMinstoTenths(_ete, TenMin) : _ete);

                        LegDate = new Dictionary<string, string>();
                        LegDate = PreUtilities.CalculateDateTime(isDepartureConfirmed, depAirport.depIsDayLightSaving, depAirport.depDayLightSavingStartDT, depAirport.depDayLightSavingEndDT,
                        depAirport.depDaylLightSavingStartTM, depAirport.depDayLightSavingEndTM, depAirport.depOffsetToGMT, arrAirport.arrIsDayLightSaving, arrAirport.arrDayLightSavingStartDT,
                        arrAirport.arrDayLightSavingEndDT, arrAirport.arrDaylLightSavingStartTM, arrAirport.arrDayLightSavingEndTM, arrAirport.arrOffsetToGMT,
                        homebaseAirport.homeIsDayLightSaving, homebaseAirport.homeDayLightSavingStartDT, homebaseAirport.homeDayLightSavingEndDT, homebaseAirport.homeDaylLightSavingStartTM,
                        homebaseAirport.homeDayLightSavingEndTM, homebaseAirport.homeOffsetToGMT, date.DepLocalDate, dateformat, date.ArrLocalDate, date.ArrLocalTime,
                        depAirport.DepID, arrAirport.ArrivalID, _ete, Convert.ToString(TenMin), date.DepUtcTime, date.DepUtcDate, arrAirport.ArrivalCD, depAirport.DepCD, date.ArrUtcDate, date.ArrUtcTime, homebaseAirport.HomebaseId,
                        date.DepLocalTime, date.DepHomeDate, date.DepHomeTime, date.ArrHomeDate, date.ArrHomeTime, ElapseTime, date.Changed, IsAutomaticCalcLegTimes);
                        RetObj["LegDate"] = LegDate;

                        LegDateTime = null;

                        Trip.PreflightLegs[indexOfLeg].DepartureDTTMLocal = string.IsNullOrEmpty(LegDate["DeptLocalDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptLocalDate"], LegDate["DeptLocalTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].DepartureGreenwichDTTM = string.IsNullOrEmpty(LegDate["DeptUtcDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptUtcDate"], LegDate["DeptUtcTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].HomeDepartureDTTM = string.IsNullOrEmpty(LegDate["DeptHomeDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptHomeDate"], LegDate["DeptHomeTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].ArrivalDTTMLocal = string.IsNullOrEmpty(LegDate["ArrivalLocalDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalLocalDate"], LegDate["ArrivalLocalTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].ArrivalGreenwichDTTM = string.IsNullOrEmpty(LegDate["ArrivalUtcDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalUtcDate"], LegDate["ArrivalUtcTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].HomeArrivalDTTM = string.IsNullOrEmpty(LegDate["ArrivalHomeDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalHomeDate"], LegDate["ArrivalHomeTime"]), dateformat + " HH:mm");

                        FARRules = PreUtilities.CalculatePreflightFARRules(Trip, Convert.ToInt16(LegNum), objDstsvc);
                        Trip = (PreflightTripViewModel)FARRules["Trip"];
                        RetObj["FARRules"] = FARRules["FARRules"];

                        FlightCost = PreUtilities.CalculateFlightCost(airecraft.ChargeRate, airecraft.ChargeUnit, Trip, Convert.ToString(airecraft.AirCraftId), objDstsvc, LegNum);
                        Trip = (PreflightTripViewModel)FlightCost["Trip"];
                        RetObj["FlightCost"] = Convert.ToString(FlightCost["FlightCost"]);
                        break;

                    case "Wind":
                        _miles = Convert.ToDouble(Trip.PreflightLegs[indexOfLeg].Distance);
                        if (TenthMinuteFormat.Minute == (TenthMinuteFormat)TenMin)
                        {
                            Double.TryParse(Utilities.Preflight_ConvertMinstoTenths(airecraft.LandingBias, TenMin), out _landBias);
                            Double.TryParse(Utilities.Preflight_ConvertMinstoTenths(airecraft.Bias, TenMin), out _bias);
                        }
                        else
                        {
                            Double.TryParse(airecraft.LandingBias, out _landBias);
                            Double.TryParse(airecraft.Bias, out _bias);
                        }
                        if (String.IsNullOrWhiteSpace(Convert.ToString(airecraft.TAS)))
                            _tas = 0;
                        else
                            Double.TryParse(airecraft.TAS, out _tas);
                        _wind = airecraft.Wind;

                        _ete = PreUtilities.GetIcaoEte(_wind, _landBias, _tas, _bias, _miles, airecraft.IsFixedRotary, TenMin, ElapseTime);
                        RetObj["ETE"] = _ete.ToString();
                        Trip.PreflightLegs[indexOfLeg].ElapseTM = Convert.ToDecimal(TenMin == 2 ? Utilities.Preflight_ConvertMinstoTenths(_ete, TenMin) : _ete);

                        LegDate = PreUtilities.CalculateDateTime(isDepartureConfirmed, depAirport.depIsDayLightSaving, depAirport.depDayLightSavingStartDT, depAirport.depDayLightSavingEndDT,
                        depAirport.depDaylLightSavingStartTM, depAirport.depDayLightSavingEndTM, depAirport.depOffsetToGMT, arrAirport.arrIsDayLightSaving, arrAirport.arrDayLightSavingStartDT,
                        arrAirport.arrDayLightSavingEndDT, arrAirport.arrDaylLightSavingStartTM, arrAirport.arrDayLightSavingEndTM, arrAirport.arrOffsetToGMT,
                        homebaseAirport.homeIsDayLightSaving, homebaseAirport.homeDayLightSavingStartDT, homebaseAirport.homeDayLightSavingEndDT, homebaseAirport.homeDaylLightSavingStartTM,
                        homebaseAirport.homeDayLightSavingEndTM, homebaseAirport.homeOffsetToGMT, date.DepLocalDate, dateformat, date.ArrLocalDate, date.ArrLocalTime,
                        depAirport.DepID, arrAirport.ArrivalID, _ete, Convert.ToString(TenMin), date.DepUtcTime, date.DepUtcDate, arrAirport.ArrivalCD, depAirport.DepCD, date.ArrUtcDate, date.ArrUtcTime, homebaseAirport.HomebaseId,
                        date.DepLocalTime, date.DepHomeDate, date.DepHomeTime, date.ArrHomeDate, date.ArrHomeTime, ElapseTime, date.Changed, IsAutomaticCalcLegTimes);
                        RetObj["LegDate"] = LegDate;

                        LegDateTime = null;

                        Trip.PreflightLegs[indexOfLeg].DepartureDTTMLocal = string.IsNullOrEmpty(LegDate["DeptLocalDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptLocalDate"], LegDate["DeptLocalTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].DepartureGreenwichDTTM = string.IsNullOrEmpty(LegDate["DeptUtcDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptUtcDate"], LegDate["DeptUtcTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].HomeDepartureDTTM = string.IsNullOrEmpty(LegDate["DeptHomeDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptHomeDate"], LegDate["DeptHomeTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].ArrivalDTTMLocal = string.IsNullOrEmpty(LegDate["ArrivalLocalDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalLocalDate"], LegDate["ArrivalLocalTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].ArrivalGreenwichDTTM = string.IsNullOrEmpty(LegDate["ArrivalUtcDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalUtcDate"], LegDate["ArrivalUtcTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].HomeArrivalDTTM = string.IsNullOrEmpty(LegDate["ArrivalHomeDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalHomeDate"], LegDate["ArrivalHomeTime"]), dateformat + " HH:mm");

                        FARRules = PreUtilities.CalculatePreflightFARRules(Trip, Convert.ToInt16(LegNum), objDstsvc);
                        Trip = (PreflightTripViewModel)FARRules["Trip"];
                        RetObj["FARRules"] = FARRules["FARRules"];

                        FlightCost = PreUtilities.CalculateFlightCost(airecraft.ChargeRate, airecraft.ChargeUnit, Trip, Convert.ToString(airecraft.AirCraftId), objDstsvc, LegNum);
                        Trip = (PreflightTripViewModel)FlightCost["Trip"];
                        RetObj["FlightCost"] = Convert.ToString(FlightCost["FlightCost"]);
                        break;

                    case "ETE":
                        LegDate = PreUtilities.CalculateDateTime(isDepartureConfirmed,depAirport.depIsDayLightSaving, depAirport.depDayLightSavingStartDT, depAirport.depDayLightSavingEndDT,
                        depAirport.depDaylLightSavingStartTM, depAirport.depDayLightSavingEndTM, depAirport.depOffsetToGMT, arrAirport.arrIsDayLightSaving, arrAirport.arrDayLightSavingStartDT,
                        arrAirport.arrDayLightSavingEndDT, arrAirport.arrDaylLightSavingStartTM, arrAirport.arrDayLightSavingEndTM, arrAirport.arrOffsetToGMT,
                        homebaseAirport.homeIsDayLightSaving, homebaseAirport.homeDayLightSavingStartDT, homebaseAirport.homeDayLightSavingEndDT, homebaseAirport.homeDaylLightSavingStartTM,
                        homebaseAirport.homeDayLightSavingEndTM, homebaseAirport.homeOffsetToGMT, date.DepLocalDate, dateformat, date.ArrLocalDate, date.ArrLocalTime,
                        depAirport.DepID, arrAirport.ArrivalID, airecraft.ETE, Convert.ToString(TenMin), date.DepUtcTime, date.DepUtcDate, arrAirport.ArrivalCD, depAirport.DepCD, date.ArrUtcDate, date.ArrUtcTime, homebaseAirport.HomebaseId,
                        date.DepLocalTime, date.DepHomeDate, date.DepHomeTime, date.ArrHomeDate, date.ArrHomeTime, ElapseTime, date.Changed, IsAutomaticCalcLegTimes);
                        RetObj["LegDate"] = LegDate;

                        _ete = airecraft.ETE;
                        Trip.PreflightLegs[indexOfLeg].ElapseTM = Convert.ToDecimal(TenMin == 2 ? Utilities.Preflight_ConvertMinstoTenths(_ete, TenMin) : _ete);

                        LegDateTime = null;

                        Trip.PreflightLegs[indexOfLeg].DepartureDTTMLocal = string.IsNullOrEmpty(LegDate["DeptLocalDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptLocalDate"], LegDate["DeptLocalTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].DepartureGreenwichDTTM = string.IsNullOrEmpty(LegDate["DeptUtcDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptUtcDate"], LegDate["DeptUtcTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].HomeDepartureDTTM = string.IsNullOrEmpty(LegDate["DeptHomeDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptHomeDate"], LegDate["DeptHomeTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].ArrivalDTTMLocal = string.IsNullOrEmpty(LegDate["ArrivalLocalDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalLocalDate"], LegDate["ArrivalLocalTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].ArrivalGreenwichDTTM = string.IsNullOrEmpty(LegDate["ArrivalUtcDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalUtcDate"], LegDate["ArrivalUtcTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].HomeArrivalDTTM = string.IsNullOrEmpty(LegDate["ArrivalHomeDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalHomeDate"], LegDate["ArrivalHomeTime"]), dateformat + " HH:mm");

                        FARRules = PreUtilities.CalculatePreflightFARRules(Trip, Convert.ToInt16(LegNum), objDstsvc);
                        Trip = (PreflightTripViewModel)FARRules["Trip"];
                        RetObj["FARRules"] = FARRules["FARRules"];

                        FlightCost = PreUtilities.CalculateFlightCost(airecraft.ChargeRate, airecraft.ChargeUnit, Trip, Convert.ToString(airecraft.AirCraftId), objDstsvc, LegNum);
                        Trip = (PreflightTripViewModel)FlightCost["Trip"];
                        RetObj["FlightCost"] = Convert.ToString(FlightCost["FlightCost"]);
                        break;
                    case "WindReliability":
                        _miles = Convert.ToDouble(Trip.PreflightLegs[indexOfLeg].Distance);
                        if (TenthMinuteFormat.Minute == (TenthMinuteFormat)TenMin)
                        {
                            Double.TryParse(Utilities.Preflight_ConvertMinstoTenths(airecraft.LandingBias, TenMin), out _landBias);
                            Double.TryParse(Utilities.Preflight_ConvertMinstoTenths(airecraft.Bias, TenMin), out _bias);
                        }
                        else
                        {
                            Double.TryParse(airecraft.LandingBias, out _landBias);
                            Double.TryParse(airecraft.Bias, out _bias);
                        }
                        if (String.IsNullOrWhiteSpace(Convert.ToString(airecraft.TAS)))
                            _tas = 0;
                        else
                            Double.TryParse(airecraft.TAS, out _tas);

                        RetObj.Add("Wind", PreUtilities.CalculateWind(airecraft.aircraftWindAltitude, WindRelibility, depAirport.deplatdeg, depAirport.deplatmin,
                      depAirport.lcdeplatdir, depAirport.deplngdeg, depAirport.deplngmin, depAirport.lcdeplngdir, depAirport.lnDepartWindZone, arrAirport.arrlatdeg,
                      arrAirport.arrlatmin, arrAirport.lcArrLatDir, arrAirport.arrlngdeg, arrAirport.arrlngmin, arrAirport.lcarrlngdir, arrAirport.lnArrivWindZone, localdate, ObjCalculation, dateformat).ToString());
                        _wind = Convert.ToDouble(RetObj["Wind"]);
                        Trip.PreflightLegs[indexOfLeg].WindsBoeingTable = (decimal?)_wind;

                        _ete = PreUtilities.GetIcaoEte(_wind, _landBias, _tas, _bias, _miles, airecraft.IsFixedRotary, TenMin, ElapseTime);
                        RetObj["ETE"] = _ete.ToString();
                        Trip.PreflightLegs[indexOfLeg].ElapseTM = Convert.ToDecimal(TenMin == 2 ? Utilities.Preflight_ConvertMinstoTenths(_ete, TenMin) : _ete);

                        LegDate = PreUtilities.CalculateDateTime(isDepartureConfirmed,depAirport.depIsDayLightSaving, depAirport.depDayLightSavingStartDT, depAirport.depDayLightSavingEndDT,
                        depAirport.depDaylLightSavingStartTM, depAirport.depDayLightSavingEndTM, depAirport.depOffsetToGMT, arrAirport.arrIsDayLightSaving, arrAirport.arrDayLightSavingStartDT,
                        arrAirport.arrDayLightSavingEndDT, arrAirport.arrDaylLightSavingStartTM, arrAirport.arrDayLightSavingEndTM, arrAirport.arrOffsetToGMT,
                        homebaseAirport.homeIsDayLightSaving, homebaseAirport.homeDayLightSavingStartDT, homebaseAirport.homeDayLightSavingEndDT, homebaseAirport.homeDaylLightSavingStartTM,
                        homebaseAirport.homeDayLightSavingEndTM, homebaseAirport.homeOffsetToGMT, date.DepLocalDate, dateformat, date.ArrLocalDate, date.ArrLocalTime,
                        depAirport.DepID, arrAirport.ArrivalID, _ete, Convert.ToString(TenMin), date.DepUtcTime, date.DepUtcDate, arrAirport.ArrivalCD, depAirport.DepCD, date.ArrUtcDate, date.ArrUtcTime, homebaseAirport.HomebaseId,
                        date.DepLocalTime, date.DepHomeDate, date.DepHomeTime, date.ArrHomeDate, date.ArrHomeTime, ElapseTime, date.Changed, IsAutomaticCalcLegTimes);
                        RetObj["LegDate"] = LegDate;

                        LegDateTime = null;

                         Trip.PreflightLegs[indexOfLeg].DepartureDTTMLocal = string.IsNullOrEmpty(LegDate["DeptLocalDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptLocalDate"], LegDate["DeptLocalTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].DepartureGreenwichDTTM = string.IsNullOrEmpty(LegDate["DeptUtcDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptUtcDate"], LegDate["DeptUtcTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].HomeDepartureDTTM = string.IsNullOrEmpty(LegDate["DeptHomeDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptHomeDate"], LegDate["DeptHomeTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].ArrivalDTTMLocal = string.IsNullOrEmpty(LegDate["ArrivalLocalDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalLocalDate"], LegDate["ArrivalLocalTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].ArrivalGreenwichDTTM = string.IsNullOrEmpty(LegDate["ArrivalUtcDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalUtcDate"], LegDate["ArrivalUtcTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].HomeArrivalDTTM = string.IsNullOrEmpty(LegDate["ArrivalHomeDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalHomeDate"], LegDate["ArrivalHomeTime"]), dateformat + " HH:mm");

                        FARRules = PreUtilities.CalculatePreflightFARRules(Trip, Convert.ToInt16(LegNum), objDstsvc);
                        Trip = (PreflightTripViewModel)FARRules["Trip"];
                        RetObj["FARRules"] = FARRules["FARRules"];

                        FlightCost = PreUtilities.CalculateFlightCost(airecraft.ChargeRate, airecraft.ChargeUnit, Trip, Convert.ToString(airecraft.AirCraftId), objDstsvc, LegNum);
                        Trip = (PreflightTripViewModel)FlightCost["Trip"];
                        RetObj["FlightCost"] = Convert.ToString(FlightCost["FlightCost"]);

                        break;
                    case "Date":
                        _miles = Convert.ToDouble(Trip.PreflightLegs[indexOfLeg].Distance);
                        if (TenthMinuteFormat.Minute == (TenthMinuteFormat)TenMin)
                        {
                            Double.TryParse(Utilities.Preflight_ConvertMinstoTenths(airecraft.LandingBias, TenMin), out _landBias);
                            Double.TryParse(Utilities.Preflight_ConvertMinstoTenths(airecraft.Bias, TenMin), out _bias);
                        }
                        else
                        {
                            Double.TryParse(airecraft.LandingBias, out _landBias);
                            Double.TryParse(airecraft.Bias, out _bias);
                        }
                        if (String.IsNullOrWhiteSpace(Convert.ToString(airecraft.TAS)))
                            _tas = 0;
                        else
                            Double.TryParse(airecraft.TAS, out _tas);

                        RetObj.Add("Wind", PreUtilities.CalculateWind(airecraft.aircraftWindAltitude, WindRelibility, depAirport.deplatdeg, depAirport.deplatmin,
                      depAirport.lcdeplatdir, depAirport.deplngdeg, depAirport.deplngmin, depAirport.lcdeplngdir, depAirport.lnDepartWindZone, arrAirport.arrlatdeg,
                      arrAirport.arrlatmin, arrAirport.lcArrLatDir, arrAirport.arrlngdeg, arrAirport.arrlngmin, arrAirport.lcarrlngdir, arrAirport.lnArrivWindZone, localdate, ObjCalculation, dateformat).ToString());
                        _wind = Convert.ToDouble(RetObj["Wind"]);
                        Trip.PreflightLegs[indexOfLeg].WindsBoeingTable = (decimal?)_wind;

                        _ete = PreUtilities.GetIcaoEte(_wind, _landBias, _tas, _bias, _miles, airecraft.IsFixedRotary, TenMin, ElapseTime);
                        RetObj["ETE"] = _ete.ToString();
                        Trip.PreflightLegs[indexOfLeg].ElapseTM = Convert.ToDecimal(TenMin == 2 ? Utilities.Preflight_ConvertMinstoTenths(_ete, TenMin) : _ete);

                        LegDate = PreUtilities.CalculateDateTime(isDepartureConfirmed, depAirport.depIsDayLightSaving, depAirport.depDayLightSavingStartDT, depAirport.depDayLightSavingEndDT,
                        depAirport.depDaylLightSavingStartTM, depAirport.depDayLightSavingEndTM, depAirport.depOffsetToGMT, arrAirport.arrIsDayLightSaving, arrAirport.arrDayLightSavingStartDT,
                        arrAirport.arrDayLightSavingEndDT, arrAirport.arrDaylLightSavingStartTM, arrAirport.arrDayLightSavingEndTM, arrAirport.arrOffsetToGMT,
                        homebaseAirport.homeIsDayLightSaving, homebaseAirport.homeDayLightSavingStartDT, homebaseAirport.homeDayLightSavingEndDT, homebaseAirport.homeDaylLightSavingStartTM,
                        homebaseAirport.homeDayLightSavingEndTM, homebaseAirport.homeOffsetToGMT, date.DepLocalDate, dateformat, date.ArrLocalDate, date.ArrLocalTime,
                        depAirport.DepID, arrAirport.ArrivalID, _ete, Convert.ToString(TenMin), date.DepUtcTime, date.DepUtcDate, arrAirport.ArrivalCD, depAirport.DepCD, date.ArrUtcDate, date.ArrUtcTime, homebaseAirport.HomebaseId,
                        date.DepLocalTime, date.DepHomeDate, date.DepHomeTime, date.ArrHomeDate, date.ArrHomeTime, ElapseTime, date.Changed, IsAutomaticCalcLegTimes);
                        RetObj["LegDate"] = LegDate;

                        LegDateTime = null;

                        Trip.PreflightLegs[indexOfLeg].DepartureDTTMLocal = string.IsNullOrEmpty(LegDate["DeptLocalDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptLocalDate"], LegDate["DeptLocalTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].DepartureGreenwichDTTM = string.IsNullOrEmpty(LegDate["DeptUtcDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptUtcDate"], LegDate["DeptUtcTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].HomeDepartureDTTM = string.IsNullOrEmpty(LegDate["DeptHomeDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["DeptHomeDate"], LegDate["DeptHomeTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].ArrivalDTTMLocal = string.IsNullOrEmpty(LegDate["ArrivalLocalDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalLocalDate"], LegDate["ArrivalLocalTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].ArrivalGreenwichDTTM = string.IsNullOrEmpty(LegDate["ArrivalUtcDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalUtcDate"], LegDate["ArrivalUtcTime"]), dateformat + " HH:mm");
                        Trip.PreflightLegs[indexOfLeg].HomeArrivalDTTM = string.IsNullOrEmpty(LegDate["ArrivalHomeDate"]) ? (DateTime?)null : LegDateTime.FSSParseDateTime(string.Format("{0} {1}", LegDate["ArrivalHomeDate"], LegDate["ArrivalHomeTime"]), dateformat + " HH:mm");

                        FARRules = PreUtilities.CalculatePreflightFARRules(Trip, Convert.ToInt16(LegNum), objDstsvc);
                        Trip = (PreflightTripViewModel)FARRules["Trip"];
                        RetObj["FARRules"] = FARRules["FARRules"];

                        FlightCost = PreUtilities.CalculateFlightCost(airecraft.ChargeRate, airecraft.ChargeUnit, Trip, Convert.ToString(airecraft.AirCraftId), objDstsvc, LegNum);
                        Trip = (PreflightTripViewModel)FlightCost["Trip"];
                        RetObj["FlightCost"] = Convert.ToString(FlightCost["FlightCost"]);
                        if (indexOfLeg == 0 && LegNum == "1")
                        {
                            Trip.PreflightMain.EstDepartureDT = Trip.PreflightLegs[indexOfLeg].DepartureDTTMLocal;
                        }
                        break;
                    case "CrewDutyRule":
                        FARRules = PreUtilities.CalculatePreflightFARRules(Trip, Convert.ToInt16(LegNum), objDstsvc);
                        Trip = (PreflightTripViewModel)FARRules["Trip"];
                        RetObj["FARRules"] = FARRules["FARRules"];

                        FlightCost = PreUtilities.CalculateFlightCost(airecraft.ChargeRate, airecraft.ChargeUnit, Trip, Convert.ToString(airecraft.AirCraftId), objDstsvc, LegNum);
                        Trip = (PreflightTripViewModel)FlightCost["Trip"];
                        RetObj["FlightCost"] = Convert.ToString(FlightCost["FlightCost"]);
                        break;

                }


                if (isDepartureConfirmed)
                {
                    Trip.PreflightLegs[indexOfLeg].IsDepartureConfirmed = true;
                    Trip.PreflightLegs[indexOfLeg].IsArrivalConfirmation = false;
                }
                else
                {
                    Trip.PreflightLegs[indexOfLeg].IsDepartureConfirmed = false;
                    Trip.PreflightLegs[indexOfLeg].IsArrivalConfirmation = true;
                }

                if (Trip.PreflightLegs[indexOfLeg].LegID != 0 && Trip.PreflightLegs[indexOfLeg].State != TripEntityState.Modified)
                    Trip.PreflightLegs[indexOfLeg].State = TripEntityState.Modified;

                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bool> Preflight_ReCalculation_Validate_Retrieve(string vTailNum)
        {
            var result = new FSSOperationResult<bool>();
            if (!result.IsAuthorized())
                return result;

            var userPrincipleVM = (UserPrincipalViewModel) HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel] ?? TripManagerBase.getUserPrincipal();

            decimal TenMin = userPrincipleVM._TimeDisplayTenMin ?? 0;
            bool Kilo = userPrincipleVM._IsKilometer;
            decimal ElapseTime = userPrincipleVM._ElapseTMRounding ?? 0;
            string dateformat = userPrincipleVM._ApplicationDateFormat;
            bool IsAutomaticCalcLegTimes = userPrincipleVM._IsAutomaticCalcLegTimes;

            var Trip = new PreflightTripViewModel();
            Trip = (PreflightTripViewModel) HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];

            var homebaseAirport = new HomebaseAirportDetails();
            var CompanyList = new List<FlightPakMasterService.GetCompanyWithFilters>();

            using (var objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetAllFleetWithFilters(0, false, "B", string.Empty, 0, vTailNum, 0).EntityList;
                if (objRetVal != null && objRetVal.Count > 0)
                {
                    var AircraftVal = objDstsvc.GetAircraftByAircraftID((long) objRetVal[0].AircraftID).EntityList;
                    if (objRetVal[0].HomebaseID != null)
                    {
                        var objRetVal3 = objDstsvc.GetCompanyWithFilters(string.Empty, (long) objRetVal[0].HomebaseID,true, true);
                        CompanyList = objRetVal3.EntityList.Where(x => x.HomebaseID == (long) objRetVal[0].HomebaseID).ToList();
                        if (CompanyList != null && CompanyList.Count > 0)
                        {
                            var objRetList = objDstsvc.GetAirportByAirportID((long) CompanyList[0].HomebaseAirportID);
                            if (objRetList != null && objRetList.EntityList != null)
                            {
                                var objRetGetAirport = objRetList.EntityList.FirstOrDefault();
                                if (objRetGetAirport != null && objRetGetAirport.AirportID != 0)
                                    homebaseAirport = setHomebaseDetails(objRetGetAirport);
                            }
                        }
                    }

                    foreach (PreflightLegViewModel preLegMain in Trip.PreflightLegs)
                    {
                        var RetObj = new Dictionary<string, object>();
                        var airecraft = new AircraftDetails();
                        if (AircraftVal != null && AircraftVal.Count > 0)
                        {
                            airecraft = setAircraftObj(preLegMain, AircraftVal[0]);
                        }

                        string localdate = preLegMain.DepartureLocalDate;
                        bool isDepartureConfirmed = true; 

                        string PowerSetting = string.IsNullOrWhiteSpace(preLegMain.PowerSetting) ? "" : preLegMain.PowerSetting;
                        int WindRelibility = string.IsNullOrWhiteSpace(preLegMain.StrWindReliability) ? 0 : Convert.ToInt32(preLegMain.StrWindReliability);
                      
                        var depAirport = new DepartAirportDetails();
                        var arrAirport = new ArrivalAirportDetails();

                        if (preLegMain.DepartureAirport != null)
                            depAirport = setDepartureAirportDetails(preLegMain.DepartureAirport);

                        if (preLegMain.ArrivalAirport != null)
                            arrAirport = setArrivalAirportDetails(preLegMain.ArrivalAirport);

                        long arrivalAirportId = preLegMain.ArrivalAirport != null ? preLegMain.ArrivalAirport.AirportID : 0;
                        if (arrivalAirportId != 0)
                        {
                            var arrivalAirportVM = DBCatalogsDataLoader.GetAirportInfo_FromAirportId(arrivalAirportId);
                            if (arrivalAirportVM != null)
                            {
                                arrAirport.arrDayLightSavingStartDT = arrivalAirportVM.DayLightSavingStartDT;
                                arrAirport.arrDayLightSavingEndDT = arrivalAirportVM.DayLightSavingEndDT;
                            }
                        }

                        long departureAirportId = preLegMain.DepartureAirport != null ? preLegMain.DepartureAirport.AirportID : 0;
                        if (departureAirportId != 0)
                        {
                            var departureAirportVM = DBCatalogsDataLoader.GetAirportInfo_FromAirportId(departureAirportId);
                            if (departureAirportVM != null)
                            {
                                depAirport.depDayLightSavingStartDT = departureAirportVM.DayLightSavingStartDT;
                                depAirport.depDayLightSavingEndDT = departureAirportVM.DayLightSavingEndDT;
                            }
                        }
                            
                        var date = new LegDateTime();
                        date.DepLocalDate = preLegMain.DepartureLocalDate;
                        date.DepLocalTime = preLegMain.DepartureLocalTime;
                        date.DepUtcDate = preLegMain.DepartureGreenwichDate;
                        date.DepUtcTime = preLegMain.DepartureGreenwichTime;
                        date.DepHomeDate = preLegMain.HomeDepartureDate;
                        date.DepHomeTime = preLegMain.HomeDepartureLocal;

                        date.ArrLocalDate = preLegMain.DepartureLocalDate;
                        date.ArrLocalTime = "";
                        date.ArrUtcDate = preLegMain.DepartureGreenwichDate;
                        date.ArrUtcTime = "";
                        date.ArrHomeDate = preLegMain.HomeDepartureDate;
                        date.ArrHomeTime = "";
                        date.Changed = "";

                        var objLegData = new FSSOperationResult<Dictionary<string, object>>();
                        objLegData = PreflightCalculation_Validate_Retrieve(depAirport, arrAirport, homebaseAirport, airecraft, date, TenMin, Kilo, WindRelibility, localdate, dateformat, PowerSetting, ElapseTime, preLegMain.LegNUM.ToString(), preLegMain.CrewDutyRulesID, preLegMain.OverrideValue, (preLegMain.IsDutyEnd ?? false), isDepartureConfirmed, (string.IsNullOrWhiteSpace(preLegMain.StrConvertedDistance) ? 0 : Convert.ToDecimal(preLegMain.StrConvertedDistance)), "Airport", IsAutomaticCalcLegTimes);
                        RetObj = objLegData.Result;
                    }
                }
            }
            
            result.Result = true;
            return result;
        }

        private static DepartAirportDetails setDepartureAirportDetails(AirportViewModel depAirport)
        {
            var departAirport = new DepartAirportDetails();
            departAirport = (DepartAirportDetails)departAirport.InjectFrom(depAirport);
            departAirport.deplatdeg = depAirport.LatitudeDegree == null ? 0 : (double) depAirport.LatitudeDegree;
            departAirport.deplatmin = depAirport.LatitudeMinutes == null ? 0 : (double) depAirport.LatitudeMinutes;
            departAirport.lcdeplatdir = string.IsNullOrWhiteSpace(depAirport.LatitudeNorthSouth) ? "" : depAirport.LatitudeNorthSouth;
            departAirport.deplngdeg = depAirport.LongitudeDegrees == null ? 0 : (double) depAirport.LongitudeDegrees;
            departAirport.deplngmin = depAirport.LongitudeMinutes == null ? 0 : (double) depAirport.LongitudeMinutes;
            departAirport.lcdeplngdir = string.IsNullOrWhiteSpace(depAirport.LongitudeEastWest) ? "" : depAirport.LongitudeEastWest;
            departAirport.depIsDayLightSaving = depAirport.IsDayLightSaving;
            departAirport.depDayLightSavingStartDT = depAirport.DayLightSavingStartDT;
            departAirport.depDayLightSavingEndDT = depAirport.DayLightSavingEndDT;
            departAirport.depDaylLightSavingStartTM = depAirport.DaylLightSavingStartTM ?? "";
            departAirport.depDayLightSavingEndTM = depAirport.DayLightSavingEndTM ?? "";
            departAirport.depOffsetToGMT = depAirport.OffsetToGMT;
            departAirport.lnDepartWindZone = depAirport.WindZone == null ? 0 : (long) depAirport.WindZone;
            departAirport.DepAirportTakeoffBIAS = depAirport.TakeoffBIAS == null ? 0 : (double) depAirport.TakeoffBIAS;
            departAirport.DepID = (depAirport.AirportID == 0) ? null : Convert.ToString(depAirport.AirportID);
            departAirport.DepCD = depAirport.IcaoID;
            return departAirport;
        }

        private static ArrivalAirportDetails setArrivalAirportDetails(AirportViewModel  ArrivalAirport)
        {
            var arrAirport = new ArrivalAirportDetails();
            arrAirport = (ArrivalAirportDetails)arrAirport.InjectFrom(ArrivalAirport);
            arrAirport.arrlatdeg = ArrivalAirport.LatitudeDegree == null ? 0 : (double) ArrivalAirport.LatitudeDegree;
            arrAirport.arrlatmin= ArrivalAirport.LatitudeMinutes == null ? 0 : (double) ArrivalAirport.LatitudeMinutes;
            arrAirport.lcArrLatDir = string.IsNullOrWhiteSpace(ArrivalAirport.LatitudeNorthSouth) ? "N" : ArrivalAirport.LatitudeNorthSouth;
            arrAirport.arrlngdeg= ArrivalAirport.LongitudeDegrees == null ? 0 : (double) ArrivalAirport.LongitudeDegrees;
            arrAirport.arrlngmin= ArrivalAirport.LongitudeMinutes == null ? 0 : (double) ArrivalAirport.LongitudeMinutes;
            arrAirport.lcarrlngdir = string.IsNullOrWhiteSpace(ArrivalAirport.LongitudeEastWest) ? "E" : ArrivalAirport.LongitudeEastWest;
            arrAirport.arrIsDayLightSaving= ArrivalAirport.IsDayLightSaving;
            arrAirport.arrDayLightSavingStartDT=ArrivalAirport.DayLightSavingStartDT;
            arrAirport.arrDayLightSavingEndDT=ArrivalAirport.DayLightSavingEndDT;
            arrAirport.arrDaylLightSavingStartTM= ArrivalAirport.DaylLightSavingStartTM ?? "";
            arrAirport.arrDayLightSavingEndTM= ArrivalAirport.DayLightSavingEndTM ?? "";
            arrAirport.arrOffsetToGMT= ArrivalAirport.OffsetToGMT;
            arrAirport.lnArrivWindZone = ArrivalAirport.WindZone == null ? 0 : (long) ArrivalAirport.WindZone;
            arrAirport.ArrAirportLandingBIAS = ArrivalAirport.LandingBIAS == null ? 0 : (double) ArrivalAirport.LandingBIAS;
            arrAirport.ArrivalID = ArrivalAirport.AirportID == 0 ? null : Convert.ToString(ArrivalAirport.AirportID);
            arrAirport.ArrivalCD= ArrivalAirport.IcaoID;
            return arrAirport;
        }

        private static AircraftDetails setAircraftObj(PreflightLegViewModel vLeg, FlightPakMasterService.Aircraft objAircraft)
        {
            var aircraft = new AircraftDetails();
            aircraft = (AircraftDetails)aircraft.InjectFrom(objAircraft);
            if (objAircraft != null)
            {
                aircraft.AirCraftId = objAircraft.AircraftID;
                aircraft.PowerSettings1TakeOffBias = objAircraft.PowerSettings1TakeOffBias == null ? 0 : (double) objAircraft.PowerSettings1TakeOffBias;
                aircraft.PowerSettings1LandingBias = objAircraft.PowerSettings1LandingBias == null ? 0 : (double)objAircraft.PowerSettings1LandingBias;
                aircraft.PowerSettings1TrueAirSpeed = objAircraft.PowerSettings1TrueAirSpeed == null ? 0 : (double)objAircraft.PowerSettings1TrueAirSpeed;
                aircraft.PowerSettings2TakeOffBias = objAircraft.PowerSettings2TakeOffBias == null ? 0 : (double)objAircraft.PowerSettings2TakeOffBias;
                aircraft.PowerSettings2LandingBias = objAircraft.PowerSettings2LandingBias == null ? 0 : (double)objAircraft.PowerSettings2LandingBias;
                aircraft.PowerSettings2TrueAirSpeed = objAircraft.PowerSettings2TrueAirSpeed == null ? 0 : (double)objAircraft.PowerSettings2TrueAirSpeed;
                aircraft.PowerSettings3TakeOffBias = objAircraft.PowerSettings3TakeOffBias == null ? 0 : (double)objAircraft.PowerSettings3TakeOffBias;
                aircraft.PowerSettings3LandingBias = objAircraft.PowerSettings3LandingBias == null ? 0 : (double)objAircraft.PowerSettings3LandingBias;
                aircraft.PowerSettings3TrueAirSpeed = objAircraft.PowerSettings3TrueAirSpeed == null ? 0 : (double)objAircraft.PowerSettings3TrueAirSpeed;
                aircraft.IsFixedRotary = objAircraft.IsFixedRotary;
                aircraft.aircraftWindAltitude = objAircraft.WindAltitude == null ? 0 : (double)objAircraft.WindAltitude;
                aircraft.ChargeRate = objAircraft.ChargeRate == null ? 0 : (double)objAircraft.ChargeRate;
                aircraft.ChargeUnit = objAircraft.ChargeUnit;
                aircraft.Distance = string.IsNullOrWhiteSpace(vLeg.StrConvertedDistance) ? 0 : Convert.ToDecimal(vLeg.StrConvertedDistance);
            }
            return aircraft;
        }

        private static HomebaseAirportDetails setHomebaseDetails(FlightPakMasterService.GetAllAirport objAirport)
        {
            var homebaseAirport = new HomebaseAirportDetails();
            if (objAirport != null)
            {
                homebaseAirport = (HomebaseAirportDetails)homebaseAirport.InjectFrom(objAirport);
                homebaseAirport.homeIsDayLightSaving= objAirport.IsDayLightSaving;
                homebaseAirport.homeDayLightSavingStartDT = objAirport.DayLightSavingStartDT ?? null;
                homebaseAirport.homeDayLightSavingEndDT = objAirport.DayLightSavingEndDT ?? null;
                homebaseAirport.homeDaylLightSavingStartTM= objAirport.DaylLightSavingStartTM;
                homebaseAirport.homeDayLightSavingEndTM= objAirport.DayLightSavingEndTM;
                homebaseAirport.homeOffsetToGMT= objAirport.OffsetToGMT;
                homebaseAirport.HomebaseId= objAirport.AirportID.ToString(CultureInfo.InvariantCulture);
                homebaseAirport.HomebaseCD= objAirport.IcaoID;
            }
            return homebaseAirport;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> CrewGroupValidation(string CrewGroupCD)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            Hashtable RetObj = new Hashtable();
            PreflightMain Trip = new PreflightMain();
            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                List<FlightPak.Web.FlightPakMasterService.GetAllCrewGroup> CrewGroupLists = new List<FlightPak.Web.FlightPakMasterService.GetAllCrewGroup>();
                var ObjRetVal = ObjService.GetCrewGroupInfo();
                CrewGroupLists = ObjRetVal.EntityList.Where(x => x.CrewGroupCD.ToLower().Trim().Equals(CrewGroupCD.ToLower().Trim()) && x.IsInActive == false).ToList();
                if (CrewGroupLists.Count > 0)
                {
                    RetObj["Result"] = "true";
                    RetObj["CrewGroupID"] = System.Web.HttpUtility.HtmlEncode(CrewGroupLists[0].CrewGroupID.ToString()); 
                }
                else
                {
                    RetObj["Result"] = "false";
                }

            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> CrewCodeValidation(string CrewCD, string CrewID, string _airportId, string hdnCrewGroupID)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            Hashtable RetObj = new Hashtable();
            using (PreflightService.PreflightServiceClient objService = new PreflightService.PreflightServiceClient())
            {

                Int64 CurrentTripID = 0;
                Int64 crewGroupID = 0;
                Int64 AircraftID = 0;
                Int64 FleetID = 0;
                Int64 ArriveICAOID = 0;
                DateTime? ddate = new DateTime();
                DateTime? adate = new DateTime();
                DateTime? maxArrivedate = new DateTime();
                DateTime? minDepartdate = new DateTime();
                DataTable table = null;
                var hdnLeg = "1";
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                    if (Trip != null)
                    {
                        if (Trip.PreflightMain.Fleet == null)
                        {
                            FleetID = 0;
                        }
                        else
                        {
                            FleetID = Convert.ToInt64(Trip.PreflightMain.FleetID);
                        }
                        if (Trip.PreflightMain.AircraftID == null)
                        {
                            AircraftID = 0;
                        }
                        else
                        {
                            AircraftID = Convert.ToInt64(Trip.PreflightMain.AircraftID);
                        }
                        if (string.IsNullOrEmpty(CrewID))
                        { CrewID = "0"; }
                        if (Trip.PreflightLegs == null || Trip.PreflightLegs.Count == 0)
                        {
                            CurrentTripID = Trip.PreflightMain.TripID;
                            ArriveICAOID = 0;
                            ddate = DateTime.Now;
                            adate = DateTime.Now;
                            maxArrivedate = DateTime.Now;
                            minDepartdate = DateTime.Now;
                            CurrentTripID = Trip.PreflightMain.TripID;
                        }
                        else
                        {
                            int LegtotCnt = Trip.PreflightLegs.Count;
                            CurrentTripID = Trip.PreflightMain.TripID;
                            ArriveICAOID = Convert.ToInt64(Trip.PreflightLegs[Convert.ToInt16(hdnLeg) - 1].ArriveICAOID);
                            if (Trip.PreflightLegs[Convert.ToInt16(hdnLeg) - 1].DepartureGreenwichDTTM != null)
                                ddate = Convert.ToDateTime(Trip.PreflightLegs[Convert.ToInt16(hdnLeg) - 1].DepartureGreenwichDTTM);
                            else
                                ddate = DateTime.Now;

                            if (Trip.PreflightLegs[LegtotCnt - 1].ArrivalGreenwichDTTM != null)
                                adate = Convert.ToDateTime(Trip.PreflightLegs[LegtotCnt - 1].ArrivalGreenwichDTTM);
                            else
                                adate = DateTime.Now;

                            var Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                            for (int i = 0; i < Preflegs.Count; i++)
                            {
                                if (Preflegs[i].ArrivalGreenwichDTTM != null)
                                {
                                    maxArrivedate = Convert.ToDateTime(Trip.PreflightLegs.Max(x => x.ArrivalGreenwichDTTM));
                                }
                                else
                                {
                                    maxArrivedate = DateTime.Now;
                                }
                                if (Preflegs[i].DepartureGreenwichDTTM != null)
                                {
                                    minDepartdate = Convert.ToDateTime(Trip.PreflightLegs.Min(x => x.DepartureGreenwichDTTM));
                                }
                                else
                                {
                                    minDepartdate = DateTime.Now;
                                }
                            }

                            CurrentTripID = Trip.PreflightMain.TripID;
                        }

                        if (!string.IsNullOrEmpty(hdnCrewGroupID))
                            crewGroupID = Convert.ToInt64(hdnCrewGroupID);


                        var objRetVal = objService.GetCrewListbyCrewIDORCrewCD(Convert.ToDateTime(ddate), Convert.ToDateTime(adate), crewGroupID, Convert.ToInt64(_airportId), FleetID, ArriveICAOID, CurrentTripID, Convert.ToDateTime(minDepartdate), Convert.ToDateTime(maxArrivedate), AircraftID, Convert.ToInt64(CrewID), CrewCD);
                        if (objRetVal.ReturnFlag == true&&objRetVal.EntityList.Count!=0)
                        {
                            RetObj["Results"] = "true";
                            RetObj["Crews"] = objRetVal.EntityList;
                        }
                        else
                        {
                            RetObj["Results"] = "false";
                        }
                    }
                }
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> PaxCodeValidation(string PaxCD)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            Hashtable RetObj = new Hashtable();
            List<FlightPak.Web.Framework.Helpers.Preflight.PassengerInLegs> List = new List<Framework.Helpers.Preflight.PassengerInLegs>();
            using (PreflightServiceClient prefsevice = new PreflightServiceClient())
            {
                GetAllPreFlightAvailablePaxList AvailableList = new GetAllPreFlightAvailablePaxList();
                var ListCD = new List<string>();
                if (PaxCD == "Popup")
                {
                    DataTable dtSelectedPax = new DataTable();
                    dtSelectedPax.Columns.Add("PassengerRequestorID");
                    dtSelectedPax.Columns.Add("PassengerRequestorCD");
                    dtSelectedPax.Columns.Add("PassengerName");
                    dtSelectedPax.Columns.Add("DepartmentID");
                    dtSelectedPax.Columns.Add("PassengerDescription");
                    dtSelectedPax.Columns.Add("PhoneNum");
                    dtSelectedPax.Columns.Add("HomebaseID");
                    dtSelectedPax.Columns.Add("IsActive");
                    dtSelectedPax.Columns.Add("IsRequestor");
                    dtSelectedPax.Columns.Add("FlightPurposeID");
                    dtSelectedPax.Columns.Add("FlightPurposeCD");
                    dtSelectedPax = (DataTable)HttpContext.Current.Session["SelectedPax"];
                    foreach (DataRow row in dtSelectedPax.Rows)
                    {
                        ListCD.Add(row[1].ToString());
                    }
                }
                else
                {
                    ListCD = PaxCD.Split(',').ToList();
                }
                foreach (var CD in ListCD)
                {
                    AvailableList = null;
                    var objretval = prefsevice.GetAllPreflightAvailablePaxListByIDORCD(0, CD.ToLower().Trim());
                    if (objretval.ReturnFlag)
                    {
                        var _list = objretval.EntityList;
                        if (_list != null && _list.Count > 0)
                            AvailableList = _list[0];
                    }
                    if (AvailableList != null)
                    {
                        FlightPak.Web.Framework.Helpers.Preflight.PassengerInLegs paxInfo = new FlightPak.Web.Framework.Helpers.Preflight.PassengerInLegs();
                        paxInfo.PassengerRequestorID = AvailableList.PassengerRequestorID;
                        paxInfo.Code = AvailableList.Code;
                        paxInfo.Name = AvailableList.Name;
                        paxInfo.Passport = AvailableList.Passport;
                        paxInfo.State = AvailableList.State;
                        paxInfo.Street = AvailableList.Street;
                        paxInfo.Status = AvailableList.TripStatus;
                        paxInfo.City = AvailableList.City;
                        paxInfo.Purpose = "0";
                        paxInfo.BillingCode = AvailableList.BillingCode;
                        paxInfo.Notes = AvailableList.Notes;
                        paxInfo.PassengerAlert = AvailableList.PassengerAlert;
                        paxInfo.PassportID = AvailableList.PassportID.ToString();
                        paxInfo.VisaID = AvailableList.VisaID.ToString();
                        paxInfo.Postal = AvailableList.Postal;
                        //if (!string.IsNullOrEmpty(_list[lstcnt].PassportExpiryDT.ToString()))
                        if (!string.IsNullOrEmpty(Convert.ToString(AvailableList.PassportExpiryDT)))
                        {
                            DateTime dtDate = Convert.ToDateTime(AvailableList.PassportExpiryDT);
                            if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                paxInfo.PassportExpiryDT = dtDate.ToString();
                            else
                                paxInfo.PassportExpiryDT = string.Empty;
                        }
                        paxInfo.Nation = AvailableList.Nation;
                        paxInfo.OrderNUM = 0;
                        List.Add(paxInfo);
                    }

                }
            }
            if (List.Count() > 0)
            {
                RetObj["Results"] = "true";
                RetObj["Paxs"] = List;
            }
            else
            {
                RetObj["Results"] = "false";
            }
            result.Result = RetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> DomesticInternational_Validate_Retrieve(string HomeBaseCountryId, string DepartureCountryId, string ArrivalCountryId, string LegNum)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;
            string LegType = string.Empty;

            if (HomeBaseCountryId != DepartureCountryId || HomeBaseCountryId != ArrivalCountryId)
            {
                LegType = "INT";
            }
            else if (HomeBaseCountryId == DepartureCountryId || HomeBaseCountryId == ArrivalCountryId)
            {
                LegType = "DOM";
            }
            else
            {
                LegType = "DOM";
            }
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                PreflightTripViewModel Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                int indexofLeg = Trip.GetPreflightLegIndex(LegNum);
                if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                {
                    Trip.PreflightLegs[indexofLeg].CheckGroup = LegType;
                    Trip.PreflightLegs[indexofLeg].DutyTYPE1 = LegType == "DOM" ? 1 : 2;
                    HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
                }
            }
            result.Result = LegType;
            return result;
        }
        #endregion

        #region SUMMARY_GRID

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable Summary_Validate_Retrieve(string DateFormat, decimal TenMin, decimal ElapseTM)
        {
            Hashtable RetObj = new Hashtable();
            List<PreflightLegViewModel> Dt = new List<PreflightLegViewModel>();
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                PreflightTripViewModel Trip = (PreflightTripViewModel) HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                if(Trip != null && Trip.PreflightLegs != null)
                {
                    Dt = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                    if(Dt.Count > 0)
                    {
                        var obj = (from u in Dt 
                                   where (u.ArrivalAirport.AirportID != 0 && u.DepartureAirport.AirportID != 0 && u.IsDeleted == false) 
                                   select new
                                   {
                                       LegNum = u.LegNUM, 
                                       DepartAir = u.DepartureAirport.IcaoID ?? string.Empty, 
                                       ArrivalAir = u.ArrivalAirport.IcaoID ?? string.Empty, 
                                       DepartAirportID = u.DepartICAOID, 
                                       ArrivalAirportID = u.ArriveICAOID, 
                                       DepartDate = u.DepartureDTTMLocal == null ? null : u.DepartureDTTMLocal.FSSParseDateTimeString(DateFormat), 
                                       ArrivalDate = u.ArrivalDTTMLocal == null ? null : u.ArrivalDTTMLocal.FSSParseDateTimeString(DateFormat), 
                                       DepartDateTime = u.DepartureDTTMLocal == null ? null : u.DepartureDTTMLocal.FSSParseDateTimeString(string.Format("{0} HH:mm", DateFormat)), 
                                       ArrivalDateTime = u.ArrivalDTTMLocal == null ? null : u.ArrivalDTTMLocal.FSSParseDateTimeString(string.Format("{0} HH:mm", DateFormat)), 
                                       TA = u.IsDepartureConfirmed != null ? ((bool) u.IsDepartureConfirmed ? "D" : "A") : "D", 
                                       ApproxTime = u.IsApproxTM != null && (bool) u.IsApproxTM,
                                       ETE = Utilities.Preflight_ConvertTenthsToMins(PreUtilities.RoundElpTime(Convert.ToDouble(u.ElapseTM ?? 0), TenMin, ElapseTM).ToString(), 
                                       TenMin).ToString(), 
                                       OS = u.IsScheduledServices != null && (bool) u.IsScheduledServices,
                                       Dept = u.Department != null ? u.Department.DepartmentCD : "", 
                                       Auth = u.DepartmentAuthorization != null ? u.DepartmentAuthorization.AuthorizationCD : "", 
                                       Cat = u.FlightCatagory != null ? u.FlightCatagory.FlightCatagoryCD : string.Empty, 
                                       EOD = u.IsDutyEnd != null && (bool) u.IsDutyEnd, 
                                       DutyErr = u.CrewDutyAlert,
                                       PAXNo = u.PassengerTotal ?? 0,
                                       SeatAvail = u.SeatTotal ?? 0, 
                                       FlightCost = u.FlightCost != null ? Math.Round(Convert.ToDecimal(u.FlightCost ?? 0), 2).ToString() : "0.00",
                                       ArrivalAirCityName = u.ArrivalAirport.CityName,
                                       DepartAirCityName=u.DepartureAirport.CityName,
                                   }).OrderBy(x => x.LegNum);
                        RetObj["meta"] = new PagingMetaData() {Page = obj.Count()/10, Size = 10, total_items = obj.Count()};
                        RetObj["results"] = obj;
                    }else
                    {
                        RetObj["meta"] = new PagingMetaData() { Page = Dt.Count() / 10, Size = 10, total_items = Dt.Count() };
                        RetObj["results"] = Dt;
                    }
                }
                else
                {
                    RetObj["meta"] = new PagingMetaData() {Page = Dt.Count()/10, Size = 10, total_items = Dt.Count()};
                    RetObj["results"] = Dt;
                }
            }
            else
            {
                RetObj["meta"] = new PagingMetaData() { Page = Dt.Count() / 10, Size = 10, total_items = Dt.Count() };
                RetObj["results"] = Dt;
            }
            return RetObj;
        }
        #endregion
    }
}