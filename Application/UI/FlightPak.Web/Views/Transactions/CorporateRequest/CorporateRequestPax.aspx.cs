﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using System.Collections;
using System.ComponentModel;
using FlightPak.Web.PreflightService;
using FlightPak.Web.CorporateRequestService;
using FlightPak.Web.FlightPakMasterService;
using System.Globalization;
using System.Collections.ObjectModel;
using System.Web.UI.HtmlControls;
using FlightPak.Web.UserControls;
using FlightPak.Web.Framework.Helpers.Preflight;
using FlightPak.Web.CalculationService;
//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using System.Drawing;

namespace FlightPak.Web.Views.Transactions.CorporateRequest
{
    [Serializable]
    public partial class CorporateRequestPax : BaseSecuredPage
    {
        public CRMain CorpRequest = new CRMain();
        private delegate void SaveSession();
        private delegate void CorpRequestEdit();
        private ExceptionManager exManager;
        public int IDcount = 0;
        private delegate void SavePAXSession();
        DataRow[] drarrayPaxSummary;
        string DateFormat = "MM/dd/yyyy";

        // Declaration for Exception
        List<FlightPak.Web.PreflightService.RulesException> ErrorList = new List<FlightPak.Web.PreflightService.RulesException>();

        private int paxCount = 0;
        private int ACount = 0;
        private static int? maxpassenger;
        public static int Availcount = 0;
        public static int footerCnt = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {

                SaveSession SaveCorpSession = new SaveSession(SaveCorpToSessionOnTabChange);
                Master.SaveCRToSession = SaveCorpSession;

                SaveSession SaveToSaveCorpRequestSession = new SaveSession(SaveToCorpRequestSession);
                Master.SaveToSessionPAX = SaveToSaveCorpRequestSession;

                Master.RadAjax_AjaxRequest += RadAjaxManager1_AjaxRequest;
                RadScriptManager.GetCurrent(Page).AsyncPostBackTimeout = 36000;

                //Replicate Save Cancel Delete Buttons in header
                Master.SaveClick += btnSave_Click;
                Master.CancelClick += btnCancel_Click;
                Master.DeleteClick += btnDelete_Click;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnEditTrip, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                        if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                            DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;

                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                        if (!IsPostBack)
                        {
                            Master.ValidateLegExists();

                            if (Session["CorpAvailablePaxList"] != null)
                                Session.Remove("CorpAvailablePaxList");
                            if (Session["CorpPaxSummaryList"] != null)
                                Session.Remove("CorpPaxSummaryList");


                            CalculateSeatTotal();
                            BindFlightPurpose(ddlFlightPurpose, 0, string.Empty);

                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                hdnLeg.Value = "1";
                                if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                                {
                                    LoadPaxfromCorpRequest();
                                    if (dgPaxSummary.Items.Count > 0)
                                    {
                                        dgPaxSummary.SelectedIndexes.Add(0);
                                        dgPaxSummary.Items[0].Selected = true;
                                    }
                                }
                            }
                        }
                        
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        /// <summary>
        /// show alert after ack req
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReload_Click(object sender, EventArgs e)
        {
            Master.RadAjaxManagerMaster.ResponseScripts.Add(@"window.location=window.location.href.split('?')[0];");
        }

        protected void SaveCorpToSessionOnTabChange()
        {
            string PaxpurposeErrorstr = string.Empty;
            PaxpurposeErrorstr = PaxPurposeError();

            if (!string.IsNullOrEmpty(PaxpurposeErrorstr))
                Session["CorpPAXnotAssigned"] = PaxpurposeErrorstr;
            else
            {
                Session.Remove("CorpPAXnotAssigned");
                SaveToCorpRequestSession();
            }
        }

        private void CalculateSeatTotal()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentCorporateRequest"] != null)
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                    using (FlightPakMasterService.MasterCatalogServiceClient FleetService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {

                        if (CorpRequest.FleetID != null && CorpRequest.FleetID != 0)
                        {
                            var FleetVal = FleetService.GetFleet().EntityList.Where(x => x.FleetID == (long)CorpRequest.FleetID).ToList();
                            if (FleetVal.Count > 0)
                            {
                                if (CorpRequest.CRLegs != null)
                                {
                                    for (int i = 0; i < CorpRequest.CRLegs.Count; i++)
                                    {
                                        if (FleetVal[0].MaximumPassenger != null)
                                        {
                                            CorpRequest.CRLegs[i].SeatTotal = Convert.ToInt32((decimal)FleetVal[0].MaximumPassenger);
                                            maxpassenger = Convert.ToInt32((decimal)FleetVal[0].MaximumPassenger);
                                        }
                                        else
                                        {
                                            CorpRequest.CRLegs[i].SeatTotal = 0;
                                            maxpassenger = 0;
                                        }
                                        CorpRequest.CRLegs[i].ReservationAvailable = CorpRequest.CRLegs[i].SeatTotal - CorpRequest.CRLegs[i].PassengerTotal;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (CorpRequest.CRLegs != null)
                            {
                                for (int i = 0; i < CorpRequest.CRLegs.Count; i++)
                                {
                                    CorpRequest.CRLegs[i].SeatTotal = 0;
                                }
                            }
                            maxpassenger = 0;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Method to Get Flight Purpose Details
        /// </summary>
        /// <param name="ddl"></param>
        private void BindFlightPurpose(DropDownList ddl, int LegNum, string Code)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ddl, LegNum, Code))
            {
                ddl.Items.Clear();
                using (FlightPakMasterService.MasterCatalogServiceClient PurposeService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var PurposeVal = PurposeService.GetFlightPurposeList();
                    if (PurposeVal.ReturnFlag == true)
                    {
                        foreach (FlightPakMasterService.FlightPurpose flightpurpose in PurposeVal.EntityList)
                        {
                            ddl.Items.Add(new ListItem(flightpurpose.FlightPurposeCD, flightpurpose.FlightPurposeID.ToString()));
                        }

                        if (ddl.ID == "ddlFlightPurpose")
                        {
                            ddl.Items.Insert(0, new ListItem("All Legs", "-1"));
                            ddl.Items.Insert(1, new ListItem("Select", "0"));
                        }
                        else
                            ddl.Items.Insert(0, new ListItem("Select", "0"));
                    }
                }
            }
        }

        protected void FlightPurpose_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool isdeadhead = false;
            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
            // Added for Set the Selected Value into the Dropdown list
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ddlFlightPurpose.SelectedIndex >= 0)
                        {
                            RadAjaxManager.GetCurrent(Page).FocusControl(dgAvailablePax);

                            CalculateSeatTotal();
                            List<PassengerInLegs> RetainList = (List<PassengerInLegs>)Session["CorpAvailablePaxList"];
                            int rowCount = 0;
                            foreach (GridDataItem dataItem in dgAvailablePax.MasterTableView.Items)
                            {
                                int legCount = 1;
                                foreach (PaxLegInfo PaxLeg in RetainList[rowCount].Legs)
                                {
                                    if (dataItem.Selected && Convert.ToInt64(dataItem["PassengerRequestorID"].Text) == RetainList[rowCount].PassengerRequestorID)
                                    {
                                        LinkButton lnkPaxCode = (LinkButton)dataItem.FindControl("lnkPaxCode");

                                        foreach (GridHeaderItem headerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Header))
                                        {
                                            if (CorpRequest.CRLegs != null)
                                            {
                                                List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                                                for (int legCnt = 0; legCnt < Preflegs.Count; legCnt++)
                                                {
                                                    if (Preflegs[legCnt].FlightCategoryID != null)
                                                    {
                                                        using (FlightPakMasterService.MasterCatalogServiceClient objFlightCategoryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                        {
                                                            List<FlightPak.Web.FlightPakMasterService.FlightCatagory> FlightCatagoryLists = new List<FlightPak.Web.FlightPakMasterService.FlightCatagory>();
                                                            var objRetVal = objFlightCategoryService.GetFlightCategoryList();
                                                            if (objRetVal.ReturnFlag == true)
                                                            {
                                                                FlightCatagoryLists = objRetVal.EntityList.Where(x => x.FlightCategoryID == Convert.ToInt64(Preflegs[legCnt].FlightCategoryID)).ToList();
                                                                if (FlightCatagoryLists != null && FlightCatagoryLists.Count > 0)
                                                                {
                                                                    if (FlightCatagoryLists[0].IsDeadorFerryHead == true)
                                                                    {
                                                                        isdeadhead = true;
                                                                    }
                                                                    else
                                                                    {
                                                                        isdeadhead = false;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    isdeadhead = false;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        isdeadhead = false;
                                                    }
                                                    DropDownList ddlFP = (DropDownList)dataItem["Leg" + Preflegs[legCnt].LegNUM.ToString()].FindControl("ddlLeg" + Preflegs[legCnt].LegNUM.ToString());
                                                    ddlFP.SelectedValue = ddlFlightPurpose.SelectedValue;

                                                    if (legCount == (legCnt + 1))
                                                    {
                                                        if ((legCnt + 1) == 1)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg1", "lblLeg1p1", "lblLeg1a1", "hdnLeg1Pax", "hdnLeg1Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 1);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 2)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg2", "lblLeg2p2", "lblLeg2a2", "hdnLeg2Pax", "hdnLeg2Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 2);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 3)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg3", "lblLeg3p3", "lblLeg3a3", "hdnLeg3Pax", "hdnLeg3Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 3);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 4)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg4", "lblLeg4p4", "lblLeg4a4", "hdnLeg4Pax", "hdnLeg4Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 4);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 5)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg5", "lblLeg5p5", "lblLeg5a5", "hdnLeg5Pax", "hdnLeg5Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 5);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 6)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg6", "lblLeg6p6", "lblLeg6a6", "hdnLeg6Pax", "hdnLeg6Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 6);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 7)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg7", "lblLeg7p7", "lblLeg7a7", "hdnLeg7Pax", "hdnLeg7Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 7);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 8)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg8", "lblLeg8p8", "lblLeg8a8", "hdnLeg8Pax", "hdnLeg8Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 8);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 9)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg9", "lblLeg9p9", "lblLeg9a9", "hdnLeg9Pax", "hdnLeg9Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 9);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 10)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg10", "lblLeg10p10", "lblLeg10a10", "hdnLeg10Pax", "hdnLeg10Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 10);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 11)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg11", "lblLeg11p11", "lblLeg11a11", "hdnLeg11Pax", "hdnLeg11Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 11);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 12)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg12", "lblLeg12p12", "lblLeg12a12", "hdnLeg12Pax", "hdnLeg12Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 12);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 13)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg13", "lblLeg13p13", "lblLeg13a13", "hdnLeg13Pax", "hdnLeg13Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 13);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 14)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg14", "lblLeg14p14", "lblLeg14a14", "hdnLeg14Pax", "hdnLeg14Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 14);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 15)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg15", "lblLeg15p15", "lblLeg15a15", "hdnLeg15Pax", "hdnLeg15Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 15);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 16)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg16", "lblLeg16p16", "lblLeg16a16", "hdnLeg16Pax", "hdnLeg16Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 16);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 17)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg17", "lblLeg17p17", "lblLeg17a17", "hdnLeg17Pax", "hdnLeg17Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 17);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 18)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg18", "lblLeg18p18", "lblLeg18a18", "hdnLeg18Pax", "hdnLeg18Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 18);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 19)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg19", "lblLeg19p19", "lblLeg19a19", "hdnLeg19Pax", "hdnLeg19Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 19);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 20)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg20", "lblLeg20p20", "lblLeg20a20", "hdnLeg20Pax", "hdnLeg20Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 20);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 21)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg21", "lblLeg21p21", "lblLeg21a21", "hdnLeg21Pax", "hdnLeg21Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 21);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 22)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg22", "lblLeg22p22", "lblLeg22a22", "hdnLeg22Pax", "hdnLeg22Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 22);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 23)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg23", "lblLeg23p23", "lblLeg23a23", "hdnLeg23Pax", "hdnLeg23Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 23);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 24)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg24", "lblLeg24p24", "lblLeg24a24", "hdnLeg24Pax", "hdnLeg24Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 24);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 25)
                                                        {
                                                            UpdateHeaderTemplate(ddlFP, "Leg25", "lblLeg25p25", "lblLeg25a25", "hdnLeg25Pax", "hdnLeg25Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 25);
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        legCount += 1;
                                    }
                                }
                                rowCount += 1;
                            }
                            //}
                            //ddlFlightPurpose.SelectedValue = "0";
                            dgAvailablePax.Rebind();
                            if (dgPaxSummary.Items.Count > 0)
                            {
                                dgPaxSummary.Items[0].Selected = true;
                                GridDataItem item = (GridDataItem)dgPaxSummary.SelectedItems[0];
                                Label lblLegID = (Label)item.FindControl("lblLegID");
                                Label lblPassengerRequestorID = (Label)item.FindControl("lblPassengerRequestorID");
                            }

                            // Fix for #8221
                            if (ddlFlightPurpose.Items.Count != null && ddlFlightPurpose.Items.Count >= 0)
                                ddlFlightPurpose.SelectedIndex = 0;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }



        #region common Methods

        public static DataTable ConvertToDataTable<T>(IList<T> list) where T : class
        {
            int count = 1;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DataTable table = CreateDataTable<T>();
                Type objType = typeof(T);
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(objType);
                foreach (T item in list)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor property in properties)
                    {
                        if (!CanUseType(property.PropertyType)) continue;
                        row[property.Name] = property.GetValue(item) ?? DBNull.Value;
                    }
                    row["ID"] = count;
                    table.Rows.Add(row);
                    count = count + 1;
                }

                return table;
            }
        }

        private static DataTable CreateDataTable<T>() where T : class
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Type objType = typeof(T);
                DataTable table = new DataTable(objType.Name);
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(objType);
                foreach (PropertyDescriptor property in properties)
                {
                    Type propertyType = property.PropertyType;
                    if (!CanUseType(propertyType)) continue;


                    //nullables must use underlying types
                    if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        propertyType = Nullable.GetUnderlyingType(propertyType);
                    //enums also need special treatment
                    if (propertyType.IsEnum)
                        propertyType = Enum.GetUnderlyingType(propertyType);
                    table.Columns.Add(property.Name, propertyType);
                }
                table.Columns.Add("ID");
                return table;
            }
        }

        private static bool CanUseType(Type propertyType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(propertyType))
            {
                //only strings and value types
                if (propertyType.IsArray) return false;
                if (!propertyType.IsValueType && propertyType != typeof(string)) return false;
                return true;
            }
        }

        /// <summary>
        /// Check the duplicate records in the Datatable
        /// </summary>
        /// <param name="dtDuplicate"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        protected DataTable DeleteDuplicateFromDataTable(DataTable dtDuplicate, string columnName)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Hashtable hashT = new Hashtable();
                ArrayList arrDuplicate = new ArrayList();
                foreach (DataRow row in dtDuplicate.Rows)
                {
                    if (hashT.Contains(row[columnName]))
                        arrDuplicate.Add(row);
                    else
                        hashT.Add(row[columnName], string.Empty);
                }
                foreach (DataRow row in arrDuplicate)
                    dtDuplicate.Rows.Remove(row);

                return dtDuplicate;
            }
        }

        #endregion

        #region Pax Available Grid
        private List<GetAllCorporateRequestAvailablePaxList> RetrievePaxList()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                using (CorporateRequestService.CorporateRequestServiceClient objService = new CorporateRequestService.CorporateRequestServiceClient())
                {
                    var objRetVal = objService.GetAllCorporateRequestAvailablePaxList();
                    if (objRetVal.ReturnFlag == true)
                    {
                        return objRetVal.EntityList;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Search Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Search_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveToCorpRequestSession();
                        SearchPaxAvailable();
                        SearchPaxSummary();
                        SearchBox.Text = string.Empty;
                        RadAjaxManager.GetCurrent(Page).FocusControl(Submit1);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        private void SearchPaxAvailable()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentCorporateRequest"] != null)
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                    bool alreadyExists = false;
                    List<PassengerInLegs> NewpassengerInLegs = new List<PassengerInLegs>();
                    List<PassengerInLegs> OldpassengerInLegs = new List<PassengerInLegs>();
                    List<PassengerInLegs> FinalpassengerInLegs = new List<PassengerInLegs>();
                    List<GetAllCorporateRequestAvailablePaxList> _list = null;

                    _list = RetrievePaxList();
                    if (Session["SelectedPax"] != null || !string.IsNullOrEmpty(SearchBox.Text))
                    {
                        DataTable dt = (DataTable)Session["SelectedPax"];
                        if (Session["CorpAvailablePaxList"] != null)
                        {
                            OldpassengerInLegs = (List<PassengerInLegs>)Session["CorpAvailablePaxList"];
                        }

                        if (_list != null)
                        {
                            //for (int lstcnt = 0; lstcnt < _list.Count; lstcnt++)
                            //{
                            if (!string.IsNullOrEmpty(SearchBox.Text))
                            {
                                Int32 MaxORderNum = 0;
                                if (OldpassengerInLegs != null)
                                {
                                    alreadyExists = OldpassengerInLegs.Any(x => x.Code.ToUpper().Trim() == SearchBox.Text.ToUpper().Trim());
                                    if (OldpassengerInLegs.Count > 0)
                                    {
                                        MaxORderNum = OldpassengerInLegs.Max(x => x.OrderNUM);



                                    }
                                    MaxORderNum = MaxORderNum + 1;
                                }

                                if (alreadyExists == false)
                                {
                                    GetAllCorporateRequestAvailablePaxList AvailableList = new GetAllCorporateRequestAvailablePaxList();
                                    AvailableList = _list.Where(x => ((x.Code != null) && (x.Code.ToLower().Trim() == SearchBox.Text.ToLower().Trim()))).SingleOrDefault();

                                    if (AvailableList != null)
                                    {

                                        PassengerInLegs paxInfo = new PassengerInLegs();
                                        paxInfo.PassengerRequestorID = AvailableList.PassengerRequestorID;
                                        paxInfo.Code = AvailableList.Code;
                                        paxInfo.Name = AvailableList.Name;
                                        paxInfo.Passport = AvailableList.Passport;
                                        paxInfo.State = AvailableList.State;
                                        paxInfo.Street = AvailableList.Street;
                                        paxInfo.Status = AvailableList.TripStatus;
                                        paxInfo.City = AvailableList.City;
                                        paxInfo.Purpose = "0";
                                        paxInfo.BillingCode = string.Empty;
                                        paxInfo.Notes = AvailableList.Notes;
                                        paxInfo.PassengerAlert = AvailableList.PassengerAlert;
                                        paxInfo.PassportID = AvailableList.PassportID.ToString();
                                        paxInfo.VisaID = AvailableList.VisaID.ToString();
                                        paxInfo.Postal = AvailableList.Postal;
                                        if (!string.IsNullOrEmpty(Convert.ToString(AvailableList.DateOfBirth)))
                                        {
                                            DateTime dtbirthDate = Convert.ToDateTime(AvailableList.DateOfBirth);
                                            paxInfo.DateOfBirth = dtbirthDate.ToShortDateString();
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(AvailableList.PassportExpiryDT)))
                                        {
                                            DateTime dtDate = Convert.ToDateTime(AvailableList.PassportExpiryDT);
                                            if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                paxInfo.PassportExpiryDT = dtDate.ToString();
                                            else
                                                paxInfo.PassportExpiryDT = string.Empty;
                                        }
                                        paxInfo.Nation = AvailableList.Nation;
                                        paxInfo.OrderNUM = MaxORderNum;
                                        OldpassengerInLegs.Add(paxInfo);
                                        SearchBox.Text = string.Empty;

                                    }

                                }
                            }
                            if (dt != null)
                            {
                                for (int rowCnt = 0; rowCnt < dt.Rows.Count; rowCnt++)
                                {
                                    GetAllCorporateRequestAvailablePaxList AvailableList = new GetAllCorporateRequestAvailablePaxList();
                                    AvailableList = _list.Where(x => x.PassengerRequestorID == Convert.ToInt64(dt.Rows[rowCnt]["PassengerRequestorID"])).SingleOrDefault();
                                    if (AvailableList != null)
                                    {
                                        Int32 MaxORderNum = 0;

                                        if (OldpassengerInLegs != null)
                                        {
                                            alreadyExists = OldpassengerInLegs.Any(x => x.PassengerRequestorID == Convert.ToInt64(dt.Rows[rowCnt]["PassengerRequestorID"]));
                                            if (OldpassengerInLegs.Count > 0)
                                            {
                                                MaxORderNum = OldpassengerInLegs.Max(x => x.OrderNUM);
                                            }
                                            MaxORderNum = MaxORderNum + 1;
                                        }

                                        if (alreadyExists == false && string.IsNullOrEmpty(SearchBox.Text))
                                        {
                                            PassengerInLegs paxInfo = new PassengerInLegs();
                                            paxInfo.PassengerRequestorID = AvailableList.PassengerRequestorID;
                                            paxInfo.Code = AvailableList.Code;
                                            paxInfo.Name = AvailableList.Name;
                                            paxInfo.Passport = AvailableList.Passport;
                                            paxInfo.State = AvailableList.State;
                                            paxInfo.Street = AvailableList.Street;
                                            paxInfo.Status = AvailableList.TripStatus;
                                            paxInfo.City = AvailableList.City;
                                            paxInfo.Purpose = string.Empty;
                                            paxInfo.BillingCode = string.Empty;
                                            paxInfo.Notes = AvailableList.Notes;
                                            paxInfo.PassengerAlert = AvailableList.PassengerAlert;
                                            paxInfo.PassportID = AvailableList.PassportID.ToString();
                                            paxInfo.VisaID = AvailableList.VisaID.ToString();
                                            paxInfo.Postal = AvailableList.Postal;
                                            if (!string.IsNullOrEmpty(Convert.ToString(AvailableList.DateOfBirth)))
                                            {
                                                DateTime dtbirthDate = Convert.ToDateTime(AvailableList.DateOfBirth);
                                                paxInfo.DateOfBirth = dtbirthDate.ToShortDateString();
                                            }
                                            if (!string.IsNullOrEmpty(Convert.ToString(AvailableList.PassportExpiryDT)))
                                            {
                                                DateTime dtDate = Convert.ToDateTime(AvailableList.PassportExpiryDT);
                                                if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                    paxInfo.PassportExpiryDT = dtDate.ToString();
                                                else
                                                    paxInfo.PassportExpiryDT = string.Empty;
                                            }
                                            paxInfo.Nation = AvailableList.Nation;
                                            paxInfo.OrderNUM = MaxORderNum;
                                            OldpassengerInLegs.Add(paxInfo);
                                        }
                                    }
                                }
                            }
                        }


                        if (OldpassengerInLegs != null)
                            FinalpassengerInLegs = NewpassengerInLegs.Concat(OldpassengerInLegs).ToList();

                        if (dt != null)
                        {
                            dt.Dispose();
                        }
                        Session["CorpAvailablePaxList"] = FinalpassengerInLegs;
                        dgAvailablePax.DataSource = FinalpassengerInLegs;
                        dgAvailablePax.DataBind();
                        BindDefaultPurpose(FinalpassengerInLegs);
                        Session.Remove("SelectedPax");
                    }
                }
            }
        }


        private void SearchPaxAvailableforInsert()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentCorporateRequest"] != null)
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                    bool alreadyExists = false;
                    List<PassengerInLegs> NewpassengerInLegs = new List<PassengerInLegs>();
                    List<PassengerInLegs> OldpassengerInLegs = new List<PassengerInLegs>();
                    List<PassengerInLegs> FinalpassengerInLegs = new List<PassengerInLegs>();
                    List<GetAllCorporateRequestAvailablePaxList> _list = null;

                    _list = RetrievePaxList();
                    if (Session["SelectedPax"] != null || !string.IsNullOrEmpty(SearchBox.Text))
                    {
                        DataTable dt = (DataTable)Session["SelectedPax"];
                        if (Session["CorpAvailablePaxList"] != null)

                            OldpassengerInLegs = (List<PassengerInLegs>)Session["CorpAvailablePaxList"];

                        List<PassengerInLegs> FirstPaxList = new List<PassengerInLegs>();
                        List<PassengerInLegs> RestPaxList = new List<PassengerInLegs>();

                        if (_list != null)
                        {

                            if (OldpassengerInLegs != null)
                            {

                                Int32 InsertBefore = 1;
                                if (dgAvailablePax.SelectedItems.Count > 0)
                                {
                                    InsertBefore = (Int32)((GridDataItem)dgAvailablePax.SelectedItems[0]).GetDataKeyValue("OrderNUM");
                                }

                                foreach (PassengerInLegs Paxinlegs in OldpassengerInLegs.OrderBy(x => x.OrderNUM).ToList())
                                {
                                    if (Paxinlegs.OrderNUM < InsertBefore)
                                        FirstPaxList.Add(Paxinlegs);
                                    else
                                        RestPaxList.Add(Paxinlegs);
                                }
                            }


                            if (dt != null)
                            {
                                for (int rowCnt = 0; rowCnt < dt.Rows.Count; rowCnt++)
                                {
                                    GetAllCorporateRequestAvailablePaxList AvailableList = new GetAllCorporateRequestAvailablePaxList();
                                    AvailableList = _list.Where(x => x.PassengerRequestorID == Convert.ToInt64(dt.Rows[rowCnt]["PassengerRequestorID"])).SingleOrDefault();

                                    if (AvailableList != null)
                                    {
                                        Int32 MaxORderNum = 0;
                                        if (FirstPaxList != null)
                                        {

                                            alreadyExists = OldpassengerInLegs.Any(x => x.PassengerRequestorID == Convert.ToInt64(dt.Rows[rowCnt]["PassengerRequestorID"]));
                                            if (FirstPaxList.Count > 0)
                                            {
                                                MaxORderNum = FirstPaxList.Max(x => x.OrderNUM);
                                            }
                                        }
                                        MaxORderNum = MaxORderNum + 1;
                                        if (alreadyExists == false && string.IsNullOrEmpty(SearchBox.Text))
                                        {
                                            PassengerInLegs paxInfo = new PassengerInLegs();
                                            paxInfo.PassengerRequestorID = AvailableList.PassengerRequestorID;
                                            paxInfo.Code = AvailableList.Code;
                                            paxInfo.Name = AvailableList.Name;
                                            paxInfo.Passport = AvailableList.Passport;
                                            paxInfo.State = AvailableList.State;
                                            paxInfo.Street = AvailableList.Street;
                                            paxInfo.Status = AvailableList.TripStatus;
                                            paxInfo.City = AvailableList.City;
                                            paxInfo.Purpose = string.Empty;
                                            paxInfo.BillingCode = string.Empty;
                                            paxInfo.Notes = AvailableList.Notes;
                                            paxInfo.PassengerAlert = AvailableList.PassengerAlert;
                                            paxInfo.PassportID = AvailableList.PassportID.ToString();
                                            paxInfo.VisaID = AvailableList.VisaID.ToString();
                                            paxInfo.Postal = AvailableList.Postal;
                                            if (!string.IsNullOrEmpty(Convert.ToString(AvailableList.DateOfBirth)))
                                            {
                                                DateTime dtbirthDate = Convert.ToDateTime(AvailableList.DateOfBirth);
                                                paxInfo.DateOfBirth = dtbirthDate.ToShortDateString();
                                            }
                                            if (!string.IsNullOrEmpty(Convert.ToString(AvailableList.PassportExpiryDT)))
                                            {
                                                DateTime dtDate = Convert.ToDateTime(AvailableList.PassportExpiryDT);
                                                if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                    paxInfo.PassportExpiryDT = dtDate.ToString();
                                                else
                                                    paxInfo.PassportExpiryDT = string.Empty;
                                            }
                                            paxInfo.Nation = AvailableList.Nation;
                                            paxInfo.OrderNUM = MaxORderNum;
                                            FirstPaxList.Add(paxInfo);
                                        }
                                    }
                                }
                            }
                        }

                        if (FirstPaxList != null)
                        {
                            if (RestPaxList != null)
                            {
                                foreach (PassengerInLegs nextpax in RestPaxList.OrderBy(x => x.OrderNUM).ToList())
                                {
                                    Int32 MaxORderNum = 0;
                                    if (FirstPaxList.Count > 0)
                                        MaxORderNum = FirstPaxList.Max(x => x.OrderNUM);
                                    MaxORderNum++;
                                    nextpax.OrderNUM = MaxORderNum;
                                    FirstPaxList.Add(nextpax);
                                }
                            }
                            List<PassengerSummary> Oldpassengerlist = new List<PassengerSummary>();
                            Oldpassengerlist = (List<PassengerSummary>)Session["CorpPaxSummaryList"];

                            if (Oldpassengerlist != null)
                            {
                                foreach (PassengerInLegs pax in FirstPaxList.OrderBy(x => x.OrderNUM).ToList())
                                {
                                    foreach (PassengerSummary PaxSum in Oldpassengerlist.Where(x => x.PaxID == pax.PassengerRequestorID).ToList())
                                    {
                                        PaxSum.OrderNum = pax.OrderNUM;
                                    }
                                }
                            }
                            Session["CorpPaxSummaryList"] = Oldpassengerlist;
                        }

                        if (OldpassengerInLegs != null)
                            FinalpassengerInLegs = NewpassengerInLegs.Concat(FirstPaxList).ToList();

                        if (dt != null)
                        {
                            dt.Dispose();
                        }
                        Session["CorpAvailablePaxList"] = FinalpassengerInLegs;
                        dgAvailablePax.DataSource = FinalpassengerInLegs;
                        dgAvailablePax.DataBind();
                        BindDefaultPurpose((FirstPaxList));
                        Session.Remove("SelectedPax");
                    }
                }
            }
        }


        private void BindDefaultPurpose(List<PassengerInLegs> NewpassengerInLegs)
        {
            bool isdeadhead = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(NewpassengerInLegs))
            {
                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                if (CorpRequest != null)
                {
                    if (CorpRequest.CRLegs != null)
                    {
                        List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        for (int newlistcnt = 0; newlistcnt < NewpassengerInLegs.Count; newlistcnt++)
                        {
                            if (NewpassengerInLegs[newlistcnt].Purpose == string.Empty)
                            {
                                for (int i = 1; i <= Preflegs.Count; i++)
                                {
                                    foreach (GridDataItem dataItem in dgAvailablePax.MasterTableView.Items)
                                    {
                                        LinkButton lnkPaxCode = (LinkButton)dataItem.FindControl("lnkPaxCode");
                                        using (FlightPakMasterService.MasterCatalogServiceClient PassengerService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var PassengerList = PassengerService.GetPassengerbyPassengerRequestorID(Convert.ToInt64(NewpassengerInLegs[newlistcnt].PassengerRequestorID));
                                            if (PassengerList.ReturnFlag == true)
                                            {
                                                if (Convert.ToInt64(dataItem["PassengerRequestorID"].Text) == Convert.ToInt64(NewpassengerInLegs[newlistcnt].PassengerRequestorID))
                                                {
                                                    if (Preflegs[i - 1].FlightCategoryID != null)
                                                    {
                                                        using (FlightPakMasterService.MasterCatalogServiceClient objFlightCategoryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                        {
                                                            List<FlightPak.Web.FlightPakMasterService.FlightCatagory> FlightCatagoryLists = new List<FlightPak.Web.FlightPakMasterService.FlightCatagory>();
                                                            var objRetVal = objFlightCategoryService.GetFlightCategoryList();
                                                            if (objRetVal.ReturnFlag == true)
                                                            {
                                                                FlightCatagoryLists = objRetVal.EntityList.Where(x => x.FlightCategoryID == Convert.ToInt64(Preflegs[i - 1].FlightCategoryID)).ToList();
                                                                if (FlightCatagoryLists != null && FlightCatagoryLists.Count > 0)
                                                                {
                                                                    if (FlightCatagoryLists[0].IsDeadorFerryHead == true)
                                                                    {
                                                                        isdeadhead = true;
                                                                    }
                                                                    else
                                                                    {
                                                                        isdeadhead = false;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    isdeadhead = false;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        isdeadhead = false;
                                                    }
                                                    if (Preflegs[i - 1].FlightCategoryID == null)
                                                    {
                                                        isdeadhead = false;
                                                    }
                                                    DropDownList ddlFP = (DropDownList)dataItem["Leg" + (Preflegs[i - 1].LegNUM).ToString()].FindControl("ddlLeg" + (Preflegs[i - 1].LegNUM).ToString());
                                                    HiddenField hdnFP = (HiddenField)dataItem["Leg" + (Preflegs[i - 1].LegNUM).ToString()].FindControl("hdnOldPurpose_" + (Preflegs[i - 1].LegNUM).ToString());
                                                    if (isdeadhead == false)
                                                    {
                                                        ddlFP.Enabled = true;
                                                        ddlFP.SelectedValue = PassengerList.EntityList[0].FlightPurposeID.ToString();
                                                        hdnFP.Value = PassengerList.EntityList[0].FlightPurposeID.ToString();
                                                        UpdateHeaderTemplate(ddlFP, ("Leg" + Preflegs[i - 1].LegNUM), ("lblLeg" + Preflegs[i - 1].LegNUM + "p" + Preflegs[i - 1].LegNUM), ("lblLeg" + Preflegs[i - 1].LegNUM + "a" + Preflegs[i - 1].LegNUM), ("hdnLeg" + Preflegs[i - 1].LegNUM + "Pax"), ("hdnLeg" + Preflegs[i - 1].LegNUM + "Avail"), dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, Convert.ToInt16(Preflegs[i - 1].LegNUM));
                                                        //flightpurposeoverride
                                                        if (Session["SelectedPax"] != null)
                                                        {
                                                            DataTable dt = (DataTable)Session["SelectedPax"];
                                                            if (dt.Rows.Count > 0)
                                                            {
                                                                for (int rowCount = 0; rowCount < dt.Rows.Count; rowCount++)
                                                                {
                                                                    if (Convert.ToInt64(dataItem["PassengerRequestorID"].Text) == Convert.ToInt64(dt.Rows[rowCount]["PassengerRequestorID"].ToString()) && !string.IsNullOrEmpty(dt.Rows[rowCount]["FlightPurposeID"].ToString()))
                                                                    {
                                                                        ddlFP.Enabled = true;
                                                                        ddlFP.SelectedValue = dt.Rows[rowCount]["FlightPurposeID"].ToString();
                                                                        hdnFP.Value = dt.Rows[rowCount]["FlightPurposeID"].ToString();
                                                                        UpdateHeaderTemplate(ddlFP, ("Leg" + Preflegs[i - 1].LegNUM), ("lblLeg" + Preflegs[i - 1].LegNUM + "p" + Preflegs[i - 1].LegNUM), ("lblLeg" + Preflegs[i - 1].LegNUM + "a" + Preflegs[i - 1].LegNUM), ("hdnLeg" + Preflegs[i - 1].LegNUM + "Pax"), ("hdnLeg" + Preflegs[i - 1].LegNUM + "Avail"), dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, Convert.ToInt16(Preflegs[i - 1].LegNUM));
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ddlFP.SelectedValue = "0";
                                                        hdnFP.Value = "0";
                                                        UpdateHeaderTemplate(ddlFP, ("Leg" + Preflegs[i - 1].LegNUM), ("lblLeg" + Preflegs[i - 1].LegNUM + "p" + Preflegs[i - 1].LegNUM), ("lblLeg" + Preflegs[i - 1].LegNUM + "a" + Preflegs[i - 1].LegNUM), ("hdnLeg" + Preflegs[i - 1].LegNUM + "Pax"), ("hdnLeg" + Preflegs[i - 1].LegNUM + "Avail"), dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, Convert.ToInt16(Preflegs[i - 1].LegNUM));
                                                        ddlFP.Enabled = false;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void BindAvailablePax()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (CorporateRequestService.CorporateRequestServiceClient objService = new CorporateRequestService.CorporateRequestServiceClient())
                {
                    var objRetVal = objService.GetAllCorporateRequestAvailablePaxList();
                    if (objRetVal.ReturnFlag == true)
                    {
                        DataTable table = ConvertToDataTable(objRetVal.EntityList);
                        DeleteDuplicateFromDataTable(table, "Code");
                        dgAvailablePax.DataSource = table;
                        dgAvailablePax.DataBind();
                        Session["SelectedPaxList"] = table;
                        Session["AvaialblePax"] = table;
                    }

                    if (dgAvailablePax.Items.Count > 0)
                    {
                        dgAvailablePax.Items[0].Selected = true;
                    }
                }
            }
        }


        protected void dgAvailablePax_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            List<PassengerInLegs> NewpassengerInLegs = new List<PassengerInLegs>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CorpAvailablePaxList"] != null)
                        {
                            NewpassengerInLegs = (List<PassengerInLegs>)Session["CorpAvailablePaxList"];
                            SaveToCorpRequestSession();
                            dgAvailablePax.VirtualItemCount = NewpassengerInLegs.Count;
                            dgAvailablePax.DataSource = NewpassengerInLegs;
                        }
                        
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void dgAvailablePax_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool isdeadhead = false;
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                            if (CorpRequest.CRLegs != null)
                            {
                                List<CRLeg> CRLegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                                for (int icount = 1; icount <= CRLegs.Count; icount++)
                                {
                                    if (CRLegs[icount - 1].IsDeleted == false)
                                    {
                                        (dgAvailablePax.MasterTableView.GetColumn("Leg" + icount) as GridTemplateColumn).Display = true;

                                        if (e.Item is GridDataItem)
                                        {
                                            GridDataItem item = (GridDataItem)e.Item;
                                            DropDownList ddl = new DropDownList();
                                            HiddenField hdn = new HiddenField();

                                            string strCode = item.GetDataKeyValue("PassengerRequestorID").ToString();
                                            ddl.ID = "ddl" + icount;
                                            ddl = (DropDownList)item["leg" + icount].FindControl("ddlLeg" + icount);
                                            BindFlightPurpose(ddl, icount, strCode);
                                        }
                                        if (e.Item is GridHeaderItem)
                                        {
                                            foreach (GridHeaderItem headerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Header))
                                            {
                                                Label lblpax = (Label)headerItem["Leg" + icount].FindControl("lblLeg" + icount + "p" + icount);// Get the header lable for pax                         
                                                Label lblavailable = (Label)headerItem["Leg" + icount].FindControl("lblLeg" + icount + "a" + icount); // Get the header lable for available 
                                                Label lblLegInfo = (Label)headerItem["Leg" + icount].FindControl("lblLeginfo" + icount);

                                                int? PasssengerTotal = CRLegs[icount - 1].PassengerTotal == null ? 0 : CRLegs[icount - 1].PassengerTotal;
                                                int? PassengerTotalWithPurpose = CRLegs[icount - 1].CRPassengers == null ? 0 : (CRLegs[icount - 1].CRPassengers.Where(x => x.IsDeleted == false)).ToList().Count;

                                                lblpax.Text = System.Web.HttpUtility.HtmlEncode(CRLegs[icount - 1].PassengerTotal == null ? "0" : CRLegs[icount - 1].PassengerTotal.ToString());
                                                //maxpassenger = CRLegs[icount - 1].ReservationAvailable == null ? 0 : CRLegs[icount - 1].SeatTotal - CRLegs[icount - 1].PassengerTotal;
                                                lblavailable.Text = System.Web.HttpUtility.HtmlEncode(CRLegs[icount - 1].ReservationAvailable == null ? "0" : (CRLegs[icount - 1].SeatTotal - CRLegs[icount - 1].PassengerTotal).ToString());


                                                //lblpax.Text = PassengerTotalWithPurpose.ToString();
                                                //lblavailable.Text = CRLegs[icount - 1].ReservationAvailable == null ? "0" : (CRLegs[icount - 1].SeatTotal - PassengerTotalWithPurpose).ToString();


                                                if (CRLegs[icount - 1].Airport1 != null && CRLegs[icount - 1].Airport != null)
                                                {
                                                    lblLegInfo.Text = System.Web.HttpUtility.HtmlEncode(CRLegs[icount - 1].Airport1.IcaoID + "-" + CRLegs[icount - 1].Airport.IcaoID);
                                                }
                                                if (CRLegs[icount - 1].Airport1 == null && CRLegs[icount - 1].Airport != null)
                                                {
                                                    lblLegInfo.Text = System.Web.HttpUtility.HtmlEncode(string.Empty + "-" + CRLegs[icount - 1].Airport.IcaoID);
                                                }
                                                if (CRLegs[icount - 1].Airport1 != null && CRLegs[icount - 1].Airport == null)
                                                {
                                                    lblLegInfo.Text = System.Web.HttpUtility.HtmlEncode(CRLegs[icount - 1].Airport1.IcaoID + "-" + string.Empty);
                                                }
                                            }
                                        }

                                        if (e.Item is GridFooterItem)
                                        {
                                            foreach (GridFooterItem footerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Footer))
                                            {
                                                TextBox txtBlockedSeats = (TextBox)footerItem["Leg" + icount].FindControl("tbLeg" + icount);// Get the header lable for pax
                                                HiddenField hdnBlockedSeats = (HiddenField)footerItem["Leg" + icount].FindControl("hdnLeg" + icount);// Get the header lable for pax

                                                int? PasssengerTotal = CRLegs[icount - 1].PassengerTotal == null ? 0 : CRLegs[icount - 1].PassengerTotal;
                                                int? PassengerTotalWithPurpose = CRLegs[icount - 1].CRPassengers == null ? 0 : (CRLegs[icount - 1].CRPassengers.Where(x => x.IsDeleted == false)).ToList().Count;
                                                if ((PasssengerTotal - PassengerTotalWithPurpose) > 0)
                                                {
                                                    txtBlockedSeats.Text = (PasssengerTotal - PassengerTotalWithPurpose).ToString();
                                                    hdnBlockedSeats.Value = (PasssengerTotal - PassengerTotalWithPurpose).ToString();
                                                }
                                                else
                                                {
                                                    txtBlockedSeats.Text = "0";
                                                    hdnBlockedSeats.Value = "0";
                                                }
                                            }
                                            
                                        }
                                        
                                    }

                                }
                            }
                            if (e.Item is GridDataItem)
                            {
                                GridDataItem dataItem = e.Item as GridDataItem;
                                GridColumn Column = dgAvailablePax.MasterTableView.GetColumn("Notes");
                                string Notes = dataItem["Notes"].Text.Trim();
                                string PassengerAlert = dataItem["PassengerAlert"].Text.Trim();
                                TableCell cellPaxCode = (TableCell)dataItem["PassengerRequestorID"];
                                TableCell cellName = (TableCell)dataItem["Name"];
                                //hdnTSA.Value = cellPaxCode.Text;

                                if (Notes != string.Empty && Notes != "&nbsp;" && !string.IsNullOrEmpty(hdnPaxNotes.Value) && Convert.ToBoolean(hdnPaxNotes.Value) == true)
                                {
                                    cellName.ForeColor = System.Drawing.Color.Red;
                                    cellName.ToolTip = "Addl Notes :" + Notes;

                                    if (PassengerAlert != string.Empty && PassengerAlert != "&nbsp;" && Convert.ToBoolean(hdnPaxNotes.Value) == true)
                                    {
                                        cellName.ToolTip = "Addl Notes :" + Notes + Environment.NewLine + "Alerts :" + PassengerAlert;
                                    }
                                }

                                if (CorpRequest.CRLegs != null)
                                {
                                    foreach (GridColumn col in dgAvailablePax.MasterTableView.Columns)
                                    {
                                        List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                                        for (int i = 0; i < Preflegs.Count; i++)
                                        {
                                            if (Preflegs[i].FlightCategoryID != null)
                                            {
                                                using (FlightPakMasterService.MasterCatalogServiceClient objFlightCategoryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    List<FlightPak.Web.FlightPakMasterService.FlightCatagory> FlightCatagoryLists = new List<FlightPak.Web.FlightPakMasterService.FlightCatagory>();
                                                    var objRetVal = objFlightCategoryService.GetFlightCategoryList();
                                                    if (objRetVal.ReturnFlag == true)
                                                    {
                                                        FlightCatagoryLists = objRetVal.EntityList.Where(x => x.FlightCategoryID == Convert.ToInt64(Preflegs[i].FlightCategoryID)).ToList();
                                                        if (FlightCatagoryLists != null && FlightCatagoryLists.Count > 0)
                                                        {
                                                            if (FlightCatagoryLists[0].IsDeadorFerryHead == true)
                                                            {
                                                                isdeadhead = true;
                                                            }
                                                            else
                                                            {
                                                                isdeadhead = false;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            isdeadhead = false;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                isdeadhead = false;
                                            }
                                            if (Preflegs[i].CRPassengers != null)
                                            {
                                                for (int j = 0; j < Preflegs[i].CRPassengers.Count; j++)
                                                {
                                                    if (Convert.ToInt64(cellPaxCode.Text) == Preflegs[i].CRPassengers[j].PassengerRequestorID && col.UniqueName == "Leg" + (Preflegs[i].LegNUM).ToString())
                                                    {
                                                        DropDownList ddlFP = (DropDownList)dataItem["Leg" + (Preflegs[i].LegNUM).ToString()].FindControl("ddlLeg" + (Preflegs[i].LegNUM).ToString());
                                                        if (Preflegs[i].CRPassengers[j].CRPassengerID != 0)
                                                        {
                                                            if (!ddlFP.Items.Contains(new ListItem(Preflegs[i].CRPassengers[j].FlightPurposeID.ToString())))
                                                            {
                                                                string strFlightPurposeCd = string.Empty;
                                                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                                                {
                                                                    var objflightRetVal = objDstsvc.GetFlightPurposebyFlightPurposeID(Convert.ToInt64(Preflegs[i].CRPassengers[j].FlightPurposeID));
                                                                    if (objflightRetVal.ReturnFlag == true)
                                                                    {
                                                                        if (objflightRetVal.EntityList.Count > 0)
                                                                        {
                                                                            strFlightPurposeCd = objflightRetVal.EntityList[0].FlightPurposeCD.ToString();
                                                                        }
                                                                    }
                                                                }
                                                                string encodedPurposeCd = Microsoft.Security.Application.Encoder.HtmlEncode(strFlightPurposeCd);
                                                                string encodedFlightPurposeID = Microsoft.Security.Application.Encoder.HtmlEncode(Preflegs[i].CRPassengers[j].FlightPurposeID.ToString());

                                                                ddlFP.Items.Add(new ListItem(encodedPurposeCd, encodedFlightPurposeID));
                                                            }
                                                        }
                                                        ddlFP.SelectedValue = Preflegs[i].CRPassengers[j].FlightPurposeID.ToString();

                                                        HiddenField hdnFP = (HiddenField)dataItem["Leg" + (Preflegs[i].LegNUM).ToString()].FindControl("hdnOldPurpose_" + (Preflegs[i].LegNUM).ToString());
                                                        hdnFP.Value = Preflegs[i].CRPassengers[j].FlightPurposeID.ToString();

                                                        if (isdeadhead)
                                                        {
                                                            ddlFP.Enabled = false;
                                                        }
                                                        else
                                                        {
                                                            ddlFP.Enabled = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (e.Item is GridFooterItem)
                        {
                            e.Item.Visible = false;
                            CRMain oCRMain = new CRMain();
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                oCRMain = (CRMain)Session["CurrentCorporateRequest"];
                                if (oCRMain != null)
                                {
                                    if (oCRMain.CorporateRequestStatus == "A")
                                    {
                                        e.Item.Visible = true;
                                    }
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }


        protected void tbLeg1_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg1", "lblLeg1p1", "lblLeg1a1", "hdnLeg1Pax", "hdnLeg1Avail", 1, "tbLeg1");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg2_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg2", "lblLeg2p2", "lblLeg2a2", "hdnLeg2Pax", "hdnLeg2Avail", 2, "tbLeg2");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg3_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg3", "lblLeg3p3", "lblLeg3a3", "hdnLeg3Pax", "hdnLeg3Avail", 3, "tbLeg3");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg4_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg4", "lblLeg4p4", "lblLeg4a4", "hdnLeg4Pax", "hdnLeg4Avail", 4, "tbLeg4");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg5_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg5", "lblLeg5p5", "lblLeg5a5", "hdnLeg5Pax", "hdnLeg5Avail", 5, "tbLeg5");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg6_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg6", "lblLeg6p6", "lblLeg6a6", "hdnLeg6Pax", "hdnLeg6Avail", 6, "tbLeg6");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg7_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg7", "lblLeg7p7", "lblLeg7a7", "hdnLeg7Pax", "hdnLeg7Avail", 7, "tbLeg7");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg8_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg8", "lblLeg8p8", "lblLeg8a8", "hdnLeg8Pax", "hdnLeg8Avail", 8, "tbLeg8");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg9_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg9", "lblLeg9p9", "lblLeg9a9", "hdnLeg9Pax", "hdnLeg9Avail", 9, "tbLeg9");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg10_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg10", "lblLeg10p10", "lblLeg10a10", "hdnLeg10Pax", "hdnLeg10Avail", 10, "tbLeg10");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg11_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg11", "lblLeg11p11", "lblLeg11a11", "hdnLeg11Pax", "hdnLeg11Avail", 11, "tbLeg11");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg12_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg12", "lblLeg12p12", "lblLeg12a12", "hdnLeg12Pax", "hdnLeg12Avail", 12, "tbLeg12");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg13_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg13", "lblLeg13p13", "lblLeg13a13", "hdnLeg13Pax", "hdnLeg13Avail", 13, "tbLeg13");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg14_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg14", "lblLeg14p14", "lblLeg14a14", "hdnLeg14Pax", "hdnLeg14Avail", 14, "tbLeg14");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg15_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg15", "lblLeg15p15", "lblLeg15a15", "hdnLeg15Pax", "hdnLeg15Avail", 15, "tbLeg15");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg16_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg16", "lblLeg16p16", "lblLeg16a16", "hdnLeg16Pax", "hdnLeg16Avail", 16, "tbLeg16");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg17_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg17", "lblLeg17p17", "lblLeg17a17", "hdnLeg17Pax", "hdnLeg17Avail", 17, "tbLeg17");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg18_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg18", "lblLeg18p18", "lblLeg18a18", "hdnLeg18Pax", "hdnLeg18Avail", 18, "tbLeg18");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg19_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg19", "lblLeg19p19", "lblLeg19a19", "hdnLeg19Pax", "hdnLeg19Avail", 19, "tbLeg19");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg20_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg20", "lblLeg20p20", "lblLeg20a20", "hdnLeg20Pax", "hdnLeg20Avail", 20, "tbLeg20");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg21_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg21", "lblLeg21p21", "lblLeg21a21", "hdnLeg21Pax", "hdnLeg21Avail", 21, "tbLeg21");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg22_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg22", "lblLeg22p22", "lblLeg22a22", "hdnLeg22Pax", "hdnLeg22Avail", 22, "tbLeg22");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg23_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg23", "lblLeg23p23", "lblLeg23a23", "hdnLeg23Pax", "hdnLeg23Avail", 23, "tbLeg23");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg24_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg24", "lblLeg24p24", "lblLeg24a24", "hdnLeg24Pax", "hdnLeg24Avail", 24, "tbLeg24");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void tbLeg25_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg25", "lblLeg25p25", "lblLeg25a25", "hdnLeg25Pax", "hdnLeg25Avail", 25, "tbLeg25");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void UpdateHeaderTemplateFromFooter(Object s, string LegNum, string paxNum, string maxNum, string hdnPax, string hdnAvail, int legnumber, string block)
        {
            TextBox tbBlock = (TextBox)s;
            if (!string.IsNullOrEmpty(tbBlock.Text))
            {
                List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                int? PasssengerTotal = Preflegs[legnumber - 1].PassengerTotal == null ? 0 : Preflegs[legnumber - 1].PassengerTotal;
                int? PassengerTotalWithPurpose = Preflegs[legnumber - 1].CRPassengers == null ? 0 : (Preflegs[legnumber - 1].CRPassengers.Where(x => x.IsDeleted == false && x.FlightPurposeID != 0)).ToList().Count;

                foreach (GridHeaderItem headerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Header))
                {
                    Label lblpax = (Label)headerItem[LegNum].FindControl(paxNum);// Get the header lable for pax                         
                    Label lblavailable = (Label)headerItem[LegNum].FindControl(maxNum); // Get the header lable for available 
                    HiddenField hdnLeg1P = (HiddenField)headerItem[LegNum].FindControl(hdnPax);
                    HiddenField hdnLeg1A = (HiddenField)headerItem[LegNum].FindControl(hdnAvail);

                    hdnLeg1P.Value = (Convert.ToInt16(tbBlock.Text) + PassengerTotalWithPurpose).ToString();
                    hdnLeg1A.Value = (Convert.ToInt16(maxpassenger) - (Convert.ToInt16(tbBlock.Text) + PassengerTotalWithPurpose)).ToString();

                    paxCount = Convert.ToInt32(hdnLeg1P.Value);
                    ACount = Convert.ToInt32(hdnLeg1A.Value);

                    lblpax.Text = System.Web.HttpUtility.HtmlEncode(paxCount.ToString());
                    lblavailable.Text = System.Web.HttpUtility.HtmlEncode(ACount.ToString());
                    hdnLeg1P.Value = paxCount.ToString();
                    hdnLeg1A.Value = ACount.ToString();
                    ((HiddenField)DivExternalForm.FindControl("hdnLeg" + legnumber + "p" + legnumber)).Value = paxCount.ToString();
                    ((HiddenField)DivExternalForm.FindControl("hdnLeg" + legnumber + "a" + legnumber)).Value = ACount.ToString();
                    Preflegs[legnumber - 1].PassengerTotal = paxCount;
                    Preflegs[legnumber - 1].ReservationAvailable = ACount;
                }
            }
            else
            {
                tbBlock.Text = "0";
                List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                int? PasssengerTotal = Preflegs[legnumber - 1].PassengerTotal == null ? 0 : Preflegs[legnumber - 1].PassengerTotal;
                int? PassengerTotalWithPurpose = Preflegs[legnumber - 1].CRPassengers == null ? 0 : (Preflegs[legnumber - 1].CRPassengers.Where(x => x.IsDeleted == false && x.FlightPurposeID != 0)).ToList().Count;

                foreach (GridHeaderItem headerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Header))
                {
                    Label lblpax = (Label)headerItem[LegNum].FindControl(paxNum);// Get the header lable for pax                         
                    Label lblavailable = (Label)headerItem[LegNum].FindControl(maxNum); // Get the header lable for available 
                    HiddenField hdnLeg1P = (HiddenField)headerItem[LegNum].FindControl(hdnPax);
                    HiddenField hdnLeg1A = (HiddenField)headerItem[LegNum].FindControl(hdnAvail);

                    hdnLeg1P.Value = (Convert.ToInt16(tbBlock.Text) + PassengerTotalWithPurpose).ToString();
                    hdnLeg1A.Value = (Convert.ToInt16(maxpassenger) - (Convert.ToInt16(tbBlock.Text) + PassengerTotalWithPurpose)).ToString();

                    paxCount = Convert.ToInt32(hdnLeg1P.Value);
                    ACount = Convert.ToInt32(hdnLeg1A.Value);

                    lblpax.Text = System.Web.HttpUtility.HtmlEncode(paxCount.ToString());
                    lblavailable.Text = System.Web.HttpUtility.HtmlEncode(ACount.ToString());
                    hdnLeg1P.Value = paxCount.ToString();
                    hdnLeg1A.Value = ACount.ToString();
                    ((HiddenField)DivExternalForm.FindControl("hdnLeg" + legnumber + "p" + legnumber)).Value = paxCount.ToString();
                    ((HiddenField)DivExternalForm.FindControl("hdnLeg" + legnumber + "a" + legnumber)).Value = ACount.ToString();
                    Preflegs[legnumber - 1].PassengerTotal = paxCount;
                    Preflegs[legnumber - 1].ReservationAvailable = ACount;
                }
            }
        }

        protected void ddlLeg1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg1 = editedItem.FindControl("ddlLeg1") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg1", "lblLeg1p1", "lblLeg1a1", "hdnLeg1Pax", "hdnLeg1Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 1);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void UpdateHeaderTemplate(Object s, string LegNum, string paxNum, string maxNum, string hdnPax, string hdnAvail, string Paxid, string PaxCode, string PaxName, int legnumber)
        {
            bool alreadyExists = false;
            string PassportNumber = string.Empty;
            string PaxPassportID = string.Empty;
            string street = string.Empty;
            string postal = string.Empty;
            string city = string.Empty;
            string state = string.Empty;
            string expirydt = string.Empty;
            string nation = string.Empty;
            string dateofbirth = string.Empty;

            int Index = 0;
            List<PassengerInLegs> passengerlist = new List<PassengerInLegs>();
            List<PassengerSummary> Newpassengerlist = new List<PassengerSummary>();
            List<PassengerSummary> Oldpassengerlist = new List<PassengerSummary>();
            List<PassengerSummary> Finalpassengerlist = new List<PassengerSummary>();
            DropDownList Currentddl = (DropDownList)s;
            CalculateSeatTotal();
            SaveToCorpRequestSession();
            if (Session["CorpAvailablePaxList"] != null)
            {
                passengerlist = (List<PassengerInLegs>)Session["CorpAvailablePaxList"];
            }

            if (Session["CorpPaxSummaryList"] != null)
            {
                Oldpassengerlist = (List<PassengerSummary>)Session["CorpPaxSummaryList"];
            }

            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objflightRetVal = objDstsvc.GetPassengerPassportByPassengerRequestorID(Convert.ToInt64(Paxid));
                if (objflightRetVal.ReturnFlag == true)
                {
                    if (objflightRetVal.EntityList.Count > 0)
                    {
                        PaxPassportID = objflightRetVal.EntityList[0].PassportID.ToString();
                        PassportNumber = objflightRetVal.EntityList[0].PassportNum;
                        if (!string.IsNullOrEmpty(Convert.ToString(objflightRetVal.EntityList[0].PassportExpiryDT)))
                        {
                            DateTime dtDate = Convert.ToDateTime(objflightRetVal.EntityList[0].PassportExpiryDT);
                            if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                expirydt = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ((DateTime)dtDate).ToShortDateString());
                            else
                                expirydt = string.Empty;
                        }

                        using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objCountryVal = CountryService.GetCountryMasterList();
                            if (objCountryVal.ReturnFlag == true)
                            {
                                var objCountryVal1 = objCountryVal.EntityList.Where(x => x.CountryID == objflightRetVal.EntityList[0].CountryID).ToList();
                                if (objCountryVal1.Count > 0)
                                {
                                    nation = objCountryVal1[0].CountryCD;
                                }
                            }
                        }
                    }
                }
                var objPax = objDstsvc.GetPassengerbyPassengerRequestorID(Convert.ToInt64(Paxid));
                if (objPax.ReturnFlag == true)
                {
                    if (objPax.EntityList.Count > 0)
                    {
                        street = objPax.EntityList[0].Addr1 + objPax.EntityList[0].Addr2 + objPax.EntityList[0].Addr3;
                        postal = objPax.EntityList[0].PostalZipCD;
                        city = objPax.EntityList[0].City;
                        state = objPax.EntityList[0].StateName;
                        if (!string.IsNullOrEmpty(Convert.ToString(objPax.EntityList[0].DateOfBirth)))
                        {
                            DateTime dtbirthDate = Convert.ToDateTime(objPax.EntityList[0].DateOfBirth);
                            dateofbirth = dtbirthDate.ToShortDateString();
                        }
                        else
                        {
                            dateofbirth = string.Empty;
                        }
                    }
                }
            }

            foreach (GridFooterItem footerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Footer))
            {
                TextBox tbLeg = (TextBox)footerItem[LegNum].FindControl("tbLeg" + legnumber);
                if (!string.IsNullOrEmpty(tbLeg.Text))
                {
                    footerCnt = Convert.ToInt16(tbLeg.Text);
                }
                else
                {
                    footerCnt = 0;
                }
                break;

            }
            foreach (GridHeaderItem headerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Header))
            {
                #region legs
                if (CorpRequest.CRLegs != null)
                {
                    List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                    for (int legCount = 0; legCount < Preflegs.Count; legCount++)
                    {
                        int aliaslegCount = legCount;
                        if ((aliaslegCount + 1) == legnumber)
                        {
                            if (Oldpassengerlist != null && Oldpassengerlist.Count > 0)
                            {
                                for (int Cnt = 0; Cnt < Oldpassengerlist.Count; Cnt++)
                                {
                                    if (Oldpassengerlist[Cnt].PaxID == Convert.ToInt64(Paxid) && Oldpassengerlist[Cnt].LegId == Convert.ToInt64(legnumber))
                                    {
                                        Index = Cnt;
                                        alreadyExists = true;
                                        break;
                                    }
                                }

                                if (alreadyExists && Currentddl.SelectedIndex == 0)
                                {
                                    Oldpassengerlist.RemoveAt(Index);
                                    if (Preflegs[legCount].CRPassengers != null)
                                    {
                                        for (int Paxcnt = 0; Paxcnt < Preflegs[legCount].CRPassengers.Count; Paxcnt++)
                                        {

                                            if (Preflegs[legCount].CRPassengers[Paxcnt].PassengerRequestorID == Convert.ToInt64(Paxid) && Preflegs[legCount].CRPassengers[Paxcnt].CRPassengerID == 0)
                                            {
                                                Preflegs[legCount].CRPassengers.Remove(Preflegs[legCount].CRPassengers[Paxcnt]);
                                                break;
                                            }
                                            if (Preflegs[legCount].CRPassengers[Paxcnt].PassengerRequestorID == Convert.ToInt64(Paxid) && Preflegs[legCount].CRPassengers[Paxcnt].CRPassengerID != 0)
                                            {
                                                Preflegs[legCount].CRPassengers[Paxcnt].State = CorporateRequestTripEntityState.Deleted;
                                                Preflegs[legCount].CRPassengers[Paxcnt].IsDeleted = true;
                                            }
                                        }
                                    }

                                }
                                if (!alreadyExists && Currentddl.SelectedIndex != 0)
                                {
                                    PassengerSummary pax = new PassengerSummary();
                                    Newpassengerlist.Add(pax);
                                    pax.PaxID = Convert.ToInt64(Paxid);
                                    pax.PaxCode = PaxCode;
                                    pax.PaxName = PaxName;
                                    pax.LegId = Preflegs[legCount].LegNUM;
                                    pax.Street = street;
                                    pax.Postal = postal;
                                    pax.City = city;
                                    pax.State = state;
                                    pax.Purpose = string.Empty;
                                    pax.Passport = PassportNumber;
                                    pax.BillingCode = string.Empty;
                                    pax.VisaID = string.Empty;
                                    pax.PassportID = PaxPassportID;
                                    pax.Nation = nation;
                                    pax.PassportExpiryDT = expirydt;
                                    pax.DateOfBirth = dateofbirth;
                                }
                                if (alreadyExists && Currentddl.SelectedIndex != 0)
                                {
                                    for (int Cnts = 0; Cnts < Oldpassengerlist.Count; Cnts++)
                                    {
                                        if (Oldpassengerlist[Cnts].PaxID == Convert.ToInt64(Paxid) && Oldpassengerlist[Cnts].LegId == Convert.ToInt64(legnumber))
                                        {
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].Passport))
                                                Oldpassengerlist[Cnts].Passport = PassportNumber;
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].PassportID))
                                                Oldpassengerlist[Cnts].PassportID = PaxPassportID;
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].Street))
                                                Oldpassengerlist[Cnts].Street = street;
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].Postal))
                                                Oldpassengerlist[Cnts].Postal = postal;
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].City))
                                                Oldpassengerlist[Cnts].City = city;
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].State))
                                                Oldpassengerlist[Cnts].State = state;
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].Nation))
                                                Oldpassengerlist[Cnts].Nation = nation;
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].PassportExpiryDT))
                                                Oldpassengerlist[Cnts].PassportExpiryDT = expirydt;
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].DateOfBirth))
                                                Oldpassengerlist[Cnts].DateOfBirth = dateofbirth;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (Currentddl.SelectedValue != "0" && Oldpassengerlist.Count == 0)
                                {
                                    PassengerSummary pax = new PassengerSummary();
                                    Newpassengerlist.Add(pax);
                                    pax.PaxID = Convert.ToInt64(Paxid);
                                    pax.PaxCode = PaxCode;
                                    pax.PaxName = PaxName;
                                    pax.LegId = Preflegs[legCount].LegNUM;
                                    pax.Street = street;
                                    pax.Postal = postal;
                                    pax.City = city;
                                    pax.State = state;
                                    pax.Purpose = string.Empty;
                                    pax.Passport = PassportNumber;
                                    pax.BillingCode = string.Empty;
                                    pax.VisaID = string.Empty;
                                    pax.PassportID = PaxPassportID;
                                    pax.Nation = nation;
                                    pax.PassportExpiryDT = expirydt;
                                    pax.DateOfBirth = dateofbirth;
                                }
                            }
                        }
                    }

                    if (Oldpassengerlist != null)
                        Finalpassengerlist = Newpassengerlist.Concat(Oldpassengerlist).ToList();

                    Session["CorpPaxSummaryList"] = Finalpassengerlist;
                    dgPaxSummary.DataSource = Finalpassengerlist;
                    dgPaxSummary.DataBind();
                    SaveToCorpRequestSession();

                #endregion

                    int? PasssengerTotal = Preflegs[legnumber - 1].PassengerTotal == null ? 0 : Preflegs[legnumber - 1].PassengerTotal;
                    int? PassengerTotalWithPurpose = Preflegs[legnumber - 1].CRPassengers == null ? 0 : (Preflegs[legnumber - 1].CRPassengers.Where(x => x.IsDeleted == false && x.FlightPurposeID != 0)).ToList().Count;
                    Label lblpax = (Label)headerItem[LegNum].FindControl(paxNum);// Get the header lable for pax                         
                    Label lblavailable = (Label)headerItem[LegNum].FindControl(maxNum); // Get the header lable for available 
                    HiddenField hdnLeg1P = (HiddenField)headerItem[LegNum].FindControl(hdnPax);
                    HiddenField hdnLeg1A = (HiddenField)headerItem[LegNum].FindControl(hdnAvail);

                    hdnLeg1P.Value = "0";

                    if (!string.IsNullOrEmpty(maxpassenger.ToString()))
                    {
                        hdnLeg1A.Value = maxpassenger.ToString();
                    }
                    else
                    {
                        hdnLeg1A.Value = "0";
                    }

                    if (footerCnt != 0)
                    {
                        hdnLeg1P.Value = (Convert.ToInt16(hdnLeg1P.Value) + footerCnt + PassengerTotalWithPurpose).ToString();
                        hdnLeg1A.Value = (Convert.ToInt16(hdnLeg1A.Value) - (footerCnt + PassengerTotalWithPurpose)).ToString();
                    }
                    else
                    {
                        hdnLeg1P.Value = (Convert.ToInt16(hdnLeg1P.Value) + PassengerTotalWithPurpose).ToString();
                        hdnLeg1A.Value = (Convert.ToInt16(hdnLeg1A.Value) - PassengerTotalWithPurpose).ToString();
                    }
                    paxCount = Convert.ToInt32(hdnLeg1P.Value);
                    ACount = Convert.ToInt32(hdnLeg1A.Value);
                    int Total = Convert.ToInt16(PasssengerTotal - PassengerTotalWithPurpose);
                    //if (passengerlist.Count > Total)
                    //{
                    //paxCount++;

                    //ACount = Convert.ToInt32(hdnLeg1A.Value);
                    //ACount--;

                    lblpax.Text = System.Web.HttpUtility.HtmlEncode(paxCount.ToString());
                    lblavailable.Text = System.Web.HttpUtility.HtmlEncode(ACount.ToString());
                    hdnLeg1P.Value = paxCount.ToString();
                    hdnLeg1A.Value = ACount.ToString();
                    ((HiddenField)DivExternalForm.FindControl("hdnLeg" + legnumber + "p" + legnumber)).Value = paxCount.ToString();
                    ((HiddenField)DivExternalForm.FindControl("hdnLeg" + legnumber + "a" + legnumber)).Value = ACount.ToString();
                    Preflegs[legnumber - 1].PassengerTotal = paxCount;
                    Preflegs[legnumber - 1].ReservationAvailable = ACount;
                }
            }
            if (dgPaxSummary.Items.Count > 0)
            {
                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                dgPaxSummary.Items[0].Selected = true;
                GridDataItem item = (GridDataItem)dgPaxSummary.SelectedItems[0];
                Label lblLegID = (Label)item.FindControl("lblLegID");
                Label lblPassengerRequestorID = (Label)item.FindControl("lblPassengerRequestorID");
                Label lblName = (Label)item.FindControl("lblName");
            }
        }


        protected void ddlLeg2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg2 = editedItem.FindControl("ddlLeg2") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg2", "lblLeg2p2", "lblLeg2a2", "hdnLeg2Pax", "hdnLeg2Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 2);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void ddlLeg3_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg3 = editedItem.FindControl("ddlLeg3") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg3", "lblLeg3p3", "lblLeg3a3", "hdnLeg3Pax", "hdnLeg3Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 3);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void ddlLeg4_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg4 = editedItem.FindControl("ddlLeg4") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg4", "lblLeg4p4", "lblLeg4a4", "hdnLeg4Pax", "hdnLeg4Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 4);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }
        protected void ddlLeg5_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg5 = editedItem.FindControl("ddlLeg5") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg5", "lblLeg5p5", "lblLeg5a5", "hdnLeg5Pax", "hdnLeg5Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 5);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }
        protected void ddlLeg6_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg6 = editedItem.FindControl("ddlLeg6") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg6", "lblLeg6p6", "lblLeg6a6", "hdnLeg6Pax", "hdnLeg6Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 6);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void ddlLeg7_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg7 = editedItem.FindControl("ddlLeg7") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg7", "lblLeg7p7", "lblLeg7a7", "hdnLeg7Pax", "hdnLeg7Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 7);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }
        protected void ddlLeg8_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg8 = editedItem.FindControl("ddlLeg8") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg8", "lblLeg8p8", "lblLeg8a8", "hdnLeg8Pax", "hdnLeg8Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 8);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }
        protected void ddlLeg9_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg9 = editedItem.FindControl("ddlLeg9") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg9", "lblLeg9p9", "lblLeg9a9", "hdnLeg9Pax", "hdnLeg9Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 9);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }
        protected void ddlLeg10_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg10 = editedItem.FindControl("ddlLeg10") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg10", "lblLeg10p10", "lblLeg10a10", "hdnLeg10Pax", "hdnLeg10Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 10);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }

        }
        protected void ddlLeg11_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg11 = editedItem.FindControl("ddlLeg11") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg11", "lblLeg11p11", "lblLeg11a11", "hdnLeg11Pax", "hdnLeg11Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 11);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }
        protected void ddlLeg12_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg12 = editedItem.FindControl("ddlLeg12") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg12", "lblLeg12p12", "lblLeg12a12", "hdnLeg12Pax", "hdnLeg12Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 12);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }
        protected void ddlLeg13_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg13 = editedItem.FindControl("ddlLeg13") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg13", "lblLeg13p13", "lblLeg13a13", "hdnLeg13Pax", "hdnLeg13Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 13);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void ddlLeg14_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg14 = editedItem.FindControl("ddlLeg14") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg14", "lblLeg14p14", "lblLeg14a14", "hdnLeg14Pax", "hdnLeg14Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 14);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void ddlLeg15_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg15 = editedItem.FindControl("ddlLeg15") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg15", "lblLeg15p15", "lblLeg15a15", "hdnLeg15Pax", "hdnLeg15Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 15);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void ddlLeg16_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg16 = editedItem.FindControl("ddlLeg16") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg16", "lblLeg16p16", "lblLeg16a16", "hdnLeg16Pax", "hdnLeg16Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 16);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void ddlLeg17_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg17 = editedItem.FindControl("ddlLeg17") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg17", "lblLeg17p17", "lblLeg17a17", "hdnLeg17Pax", "hdnLeg17Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 17);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void ddlLeg18_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg18 = editedItem.FindControl("ddlLeg18") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg18", "lblLeg18p18", "lblLeg18a18", "hdnLeg18Pax", "hdnLeg18Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 18);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void ddlLeg19_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg19 = editedItem.FindControl("ddlLeg19") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg19", "lblLeg19p19", "lblLeg19a19", "hdnLeg19Pax", "hdnLeg19Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 19);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void ddlLeg20_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg20 = editedItem.FindControl("ddlLeg20") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg20", "lblLeg20p20", "lblLeg20a20", "hdnLeg20Pax", "hdnLeg20Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 20);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void ddlLeg21_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg21 = editedItem.FindControl("ddlLeg21") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg21", "lblLeg21p21", "lblLeg21a21", "hdnLeg21Pax", "hdnLeg21Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 21);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void ddlLeg22_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg22 = editedItem.FindControl("ddlLeg22") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg22", "lblLeg22p22", "lblLeg22a22", "hdnLeg22Pax", "hdnLeg22Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 22);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void ddlLeg23_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg23 = editedItem.FindControl("ddlLeg23") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg23", "lblLeg23p23", "lblLeg23a23", "hdnLeg23Pax", "hdnLeg23Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 23);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void ddlLeg24_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg24 = editedItem.FindControl("ddlLeg24") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg24", "lblLeg24p24", "lblLeg24a24", "hdnLeg24Pax", "hdnLeg24Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 24);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void ddlLeg25_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg25 = editedItem.FindControl("ddlLeg25") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg25", "lblLeg25p25", "lblLeg25a25", "hdnLeg25Pax", "hdnLeg25Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 25);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        private int RetrieveAvailableCount()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<PassengerInLegs> passengerInLegsList = new List<PassengerInLegs>();
                int AvailableCnt = 0;

                if (Session["CorpAvailablePaxList"] != null)
                {
                    passengerInLegsList = (List<PassengerInLegs>)Session["CorpAvailablePaxList"];
                    for (int Cnt = 0; Cnt < passengerInLegsList.Count; Cnt++)
                    {
                        if (!string.IsNullOrEmpty(passengerInLegsList[Cnt].Purpose))
                        {
                            AvailableCnt = passengerInLegsList.Count;
                        }
                    }
                }
                return AvailableCnt;
            }
        }
        protected void dgAvailablePax_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveToCorpRequestSession();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void dgAvailablePax_ItemCommand(object sender, GridCommandEventArgs e)
        {
            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.CommandName == "RowClick")
                        {
                            GridDataItem test = (GridDataItem)e.Item;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        #endregion

        /// <summary>
        /// Rebind the popup values in the corresponding grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.Argument)
                        {
                            case "Rebind":
                                dgAvailablePax.MasterTableView.SortExpressions.Clear();
                                dgAvailablePax.MasterTableView.GroupByExpressions.Clear();
                                dgAvailablePax.MasterTableView.CurrentPageIndex = dgAvailablePax.MasterTableView.PageCount - 1;
                                dgAvailablePax.Rebind();
                                break;
                            case "RebindAndNavigate":
                                SaveToCorpRequestSession();
                                SearchPaxAvailable();
                                if (Session["PassSelectedPax"] != null && Session["PaxPassSelectedLeg"] != null)
                                {
                                    dgPaxSummary.Rebind();
                                }
                                break;
                            case "RebindAndNavigateInsert":
                                SaveToCorpRequestSession();
                                SearchPaxAvailableforInsert();
                                break;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
        }

        /// <summary>
        /// Define the method of IPostBackEventHandler that raises change events.
        /// </summary>
        /// <param name="sourceControl"></param>
        /// <param name="eventArgument"></param>
        protected override void RaisePostBackEvent(IPostBackEventHandler sourceControl, string eventArgument)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sourceControl, eventArgument))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.RaisePostBackEvent(sourceControl, eventArgument);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        /// <summary>
        /// LoadPaxfromCorpRequest
        /// </summary>
        /// <param name="dtNewAssign"></param>
        private void LoadPaxfromCorpRequest()
        {
            bool alreadyExists = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                List<PassengerInLegs> NewpassengerInLegs = new List<PassengerInLegs>();
                List<PassengerSummary> Newpassengerlist = new List<PassengerSummary>();
                if (Session["CurrentCorporateRequest"] != null)
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                    if (CorpRequest.CRLegs != null)
                    {
                        Session.Remove("CorpAvailablePaxList");
                    }
                    if (Session["CorpAvailablePaxList"] == null && CorpRequest.CRLegs != null)
                    {
                        // CorpRequest.CRLegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                        for (int LegCnt = 0; LegCnt < Preflegs.Count; LegCnt++)
                        {
                            if (Preflegs[LegCnt].IsDeleted == false)
                            {
                                if (Preflegs[LegCnt].CRPassengers != null)
                                {
                                    for (int PaxCnt = 0; PaxCnt < Preflegs[LegCnt].CRPassengers.Count; PaxCnt++)
                                    {
                                        if (Preflegs[LegCnt].CRPassengers[PaxCnt].IsDeleted == false)
                                        {
                                            alreadyExists = NewpassengerInLegs.Any(X => X.PassengerRequestorID == Preflegs[LegCnt].CRPassengers[PaxCnt].PassengerRequestorID);

                                            if (alreadyExists == false)
                                            {
                                                PassengerInLegs paxInfo = new PassengerInLegs();
                                                paxInfo.Legs = new List<PaxLegInfo>();

                                                NewpassengerInLegs.Add(paxInfo);
                                                PaxLegInfo PaxLeg = new PaxLegInfo();
                                                PaxLeg.LegID = Convert.ToInt64(Preflegs[LegCnt].CRLegID);

                                                paxInfo.PassengerRequestorID = Convert.ToInt64(Preflegs[LegCnt].CRPassengers[PaxCnt].PassengerRequestorID);

                                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    var objPaxRetVal = objDstsvc.GetPassengerbyPassengerRequestorID(Convert.ToInt64(Preflegs[LegCnt].CRPassengers[PaxCnt].PassengerRequestorID));

                                                    if (objPaxRetVal.ReturnFlag == true)
                                                    {
                                                        paxInfo.Code = objPaxRetVal.EntityList[0].PassengerRequestorCD;
                                                        paxInfo.Notes = objPaxRetVal.EntityList[0].Notes;
                                                        paxInfo.PassengerAlert = objPaxRetVal.EntityList[0].PassengerAlert;
                                                        paxInfo.Name = objPaxRetVal.EntityList[0].FirstName + "," + objPaxRetVal.EntityList[0].LastName + "," + objPaxRetVal.EntityList[0].MiddleInitial;
                                                        if (!string.IsNullOrEmpty(Convert.ToString(objPaxRetVal.EntityList[0].DateOfBirth)))
                                                        {
                                                            DateTime dtBirthdate = (DateTime)objPaxRetVal.EntityList[0].DateOfBirth;
                                                            paxInfo.DateOfBirth = dtBirthdate.ToShortDateString();
                                                        }
                                                        else
                                                        {
                                                            paxInfo.DateOfBirth = string.Empty;
                                                        }
                                                    }

                                                    //paxInfo.Name = Preflegs[LegCnt].CRPassengers[PaxCnt].PassengerName;
                                                    paxInfo.OrderNUM = (Int32)Preflegs[LegCnt].CRPassengers[PaxCnt].OrderNUM;
                                                    using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                                                    {

                                                        var objPassRetVal = Service.GetPassengerPassportByPassportID(Convert.ToInt64(Preflegs[LegCnt].CRPassengers[PaxCnt].PassportID));
                                                        if (objPassRetVal.ReturnFlag == true)
                                                        {
                                                            if (objPassRetVal.EntityList.Count > 0)
                                                            {
                                                                paxInfo.PassportID = objPassRetVal.EntityList[0].PassportID.ToString();
                                                                paxInfo.Passport = objPassRetVal.EntityList[0].PassportNum;
                                                            }
                                                        }
                                                    }


                                                    var objflightRetVal = objDstsvc.GetFlightPurposebyFlightPurposeID(Convert.ToInt64(Preflegs[LegCnt].CRPassengers[PaxCnt].FlightPurposeID));

                                                    if (objflightRetVal.ReturnFlag == true)
                                                    {
                                                        paxInfo.Purpose = objflightRetVal.EntityList[0].FlightPurposeID.ToString();
                                                    }
                                                }
                                                if (Preflegs[LegCnt].CRPassengers[PaxCnt].Billing != null)
                                                    paxInfo.BillingCode = Preflegs[LegCnt].CRPassengers[PaxCnt].Billing.ToString();
                                            }
                                        }
                                    }

                                    for (int PaxCnt = 0; PaxCnt < Preflegs[LegCnt].CRPassengers.Count; PaxCnt++)
                                    {
                                        if (Preflegs[LegCnt].CRPassengers[PaxCnt].IsDeleted == false)
                                        {

                                            PassengerSummary paxSum = new PassengerSummary();
                                            Newpassengerlist.Add(paxSum);

                                            paxSum.PaxID = Convert.ToInt64(Preflegs[LegCnt].CRPassengers[PaxCnt].PassengerRequestorID);
                                            paxSum.OrderNum = (Int32)Preflegs[LegCnt].CRPassengers[PaxCnt].OrderNUM;
                                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                var objPaxRetVal = objDstsvc.GetPassengerbyPassengerRequestorID(Convert.ToInt64(Preflegs[LegCnt].CRPassengers[PaxCnt].PassengerRequestorID));

                                                if (objPaxRetVal.ReturnFlag == true)
                                                {
                                                    paxSum.PaxCode = objPaxRetVal.EntityList[0].PassengerRequestorCD;
                                                    paxSum.PaxName = objPaxRetVal.EntityList[0].FirstName + "," + objPaxRetVal.EntityList[0].LastName + "," + objPaxRetVal.EntityList[0].MiddleInitial;
                                                    if (!string.IsNullOrEmpty(Convert.ToString(objPaxRetVal.EntityList[0].DateOfBirth)))
                                                    {
                                                        DateTime dtBirthdate = (DateTime)objPaxRetVal.EntityList[0].DateOfBirth;
                                                        paxSum.DateOfBirth = dtBirthdate.ToShortDateString();
                                                    }
                                                    else
                                                    {
                                                        paxSum.DateOfBirth = string.Empty;
                                                    }
                                                }
                                                // paxSum.PaxName = Preflegs[LegCnt].CRPassengers[PaxCnt].PassengerName;
                                                paxSum.LegId = Preflegs[LegCnt].LegNUM;
                                                if (Preflegs[LegCnt].CRPassengers[PaxCnt].PassportID != null)
                                                    paxSum.PassportID = Preflegs[LegCnt].CRPassengers[PaxCnt].PassportID.ToString();

                                                using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                                                {

                                                    var objPassRetVal = Service.GetPassengerPassportByPassportID(Convert.ToInt64(Preflegs[LegCnt].CRPassengers[PaxCnt].PassportID));
                                                    if (objPassRetVal.ReturnFlag == true)
                                                    {
                                                        if (objPassRetVal.EntityList.Count > 0)
                                                        {
                                                            paxSum.PassportID = objPassRetVal.EntityList[0].PassportID.ToString();
                                                            paxSum.Passport = objPassRetVal.EntityList[0].PassportNum;
                                                            //if (!string.IsNullOrEmpty(objPassRetVal.EntityList[0].PassportExpiryDT.ToString()))
                                                            if (!string.IsNullOrEmpty(Convert.ToString(objPassRetVal.EntityList[0].PassportExpiryDT)))
                                                            {
                                                                DateTime dtDate = Convert.ToDateTime(objPassRetVal.EntityList[0].PassportExpiryDT);
                                                                if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                                    paxSum.PassportExpiryDT = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ((DateTime)dtDate).ToShortDateString());
                                                                else
                                                                    paxSum.PassportExpiryDT = string.Empty;
                                                            }

                                                            using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                            {
                                                                var objCountryVal = CountryService.GetCountryMasterList();
                                                                if (objCountryVal.ReturnFlag == true)
                                                                {
                                                                    var objCountryVal1 = objCountryVal.EntityList.Where(x => x.CountryID == objPassRetVal.EntityList[0].CountryID).ToList();
                                                                    if (objCountryVal1.Count > 0)
                                                                    {
                                                                        paxSum.Nation = objCountryVal1[0].CountryCD;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                var objflightRetVal = objDstsvc.GetFlightPurposebyFlightPurposeID(Convert.ToInt64(Preflegs[LegCnt].CRPassengers[PaxCnt].FlightPurposeID));
                                                if (objflightRetVal.ReturnFlag == true)
                                                {
                                                    paxSum.Purpose = objflightRetVal.EntityList[0].FlightPurposeID.ToString();
                                                }
                                            }

                                            if (Preflegs[LegCnt].CRPassengers[PaxCnt].PassportID != null)
                                                paxSum.PassportID = Preflegs[LegCnt].CRPassengers[PaxCnt].PassportID.ToString();
                                            if (Preflegs[LegCnt].CRPassengers[PaxCnt].Billing != null)
                                                paxSum.BillingCode = Preflegs[LegCnt].CRPassengers[PaxCnt].Billing.ToString();
                                        }
                                    }

                                }
                            }
                        }

                        Newpassengerlist = Newpassengerlist.Where(x => x.LegId != null).OrderBy(x => x.LegId).ToList();
                        Int32 MaxOrderNUM = 0;
                        foreach (PassengerInLegs PaxinLegs in NewpassengerInLegs.OrderBy(x => x.OrderNUM).ToList())
                        {
                            MaxOrderNUM++;
                            PaxinLegs.OrderNUM = MaxOrderNUM;

                            foreach (PassengerSummary PaxSum in Newpassengerlist.Where(x => x.PaxID == PaxinLegs.PassengerRequestorID).ToList())
                            {
                                PaxSum.OrderNum = PaxinLegs.OrderNUM;
                            }
                        }
                        Session["CorpAvailablePaxList"] = NewpassengerInLegs;
                        dgAvailablePax.DataSource = NewpassengerInLegs;
                        dgAvailablePax.DataBind();
                        Session["CorpPaxSummaryList"] = Newpassengerlist;
                        dgPaxSummary.DataSource = Newpassengerlist;
                        dgPaxSummary.DataBind();

                        Session["CorpAvailablePaxList"] = RetainPAXInfoGrid(dgAvailablePax);
                        Session["CorpPaxSummaryList"] = RetainPaxSummaryGrid(dgPaxSummary);
                        if (dgPaxSummary.Items.Count > 0)
                            dgPaxSummary.Items[0].Selected = true;
                    }
                    else
                    {
                        List<PassengerInLegs> paxInfoList = (List<PassengerInLegs>)Session["CorpAvailablePaxList"];
                        dgAvailablePax.DataSource = paxInfoList;
                        dgAvailablePax.DataBind();
                        FillFlightPurpose(paxInfoList);
                    }
                }
            }
        }

        private void FillFlightPurpose(List<PassengerInLegs> paxInfoList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                int itemCount = 0;
                foreach (GridDataItem Item in dgAvailablePax.MasterTableView.Items)
                {
                    int counter = 1;
                    for (int leg = 0; leg < paxInfoList[itemCount].Legs.Count; leg++)
                    {
                        foreach (GridColumn col in dgAvailablePax.MasterTableView.Columns)
                        {
                            if (col.UniqueName == "Leg" + counter.ToString())
                            {
                                // Label lblLeg1p1 = (Label) 
                                DropDownList ddlFlightPurpose = (DropDownList)Item["Leg" + counter.ToString()].FindControl("ddlLeg" + counter.ToString());
                                ddlFlightPurpose.SelectedValue = paxInfoList[itemCount].Legs[leg].FlightPurposeID.ToString();
                                break;
                            }
                        }
                        counter += 1;
                    }
                    itemCount += 1;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string PaxpurposeErrorstr = string.Empty;
                        PaxpurposeErrorstr = PaxPurposeError();

                        if (!string.IsNullOrEmpty(PaxpurposeErrorstr))
                            RadWindowManager1.RadConfirm(PaxpurposeErrorstr, "confirmPaxRemoveCallBackFn", 330, 100, null, "Confirmation!");
                        else
                        {
                            if (IsAuthorized(Permission.CorporateRequest.AddCRPAXManifest) || IsAuthorized(Permission.CorporateRequest.AddCRPAXManifest))
                            {
                                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                SaveToCorpRequestSession();
                            }
                            Master.Save(CorpRequest);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void btnPaxRemoveYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Session["CurrentCorporateRequest"] != null)
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                    if (CorpRequest.Mode == CorporateRequestTripActionMode.Edit)
                    {
                        Session["CorpAvailablePaxList"] = RetainPAXInfoGrid(dgAvailablePax);
                        Session["CorpPaxSummaryList"] = RetainPaxSummaryGrid(dgPaxSummary);
                        List<PassengerInLegs> passengerList = (List<PassengerInLegs>)Session["CorpAvailablePaxList"];
                        List<PassengerSummary> passengerSummaryList = RetainPaxSummaryGrid(dgPaxSummary);
                        SavePassengers(passengerList, passengerSummaryList);
                        Session["CurrentCorporateRequest"] = CorpRequest;

                        Master.Save(CorpRequest);
                    }
                }
            }
        }

        protected void btnPaxRemoveNo_Click(object sender, EventArgs e)
        {
            RadAjaxManager.GetCurrent(Page).FocusControl(btnSave);
        }


        protected void btnPaxNextYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                SaveToCorpRequestSession();
                Master.Next("Pax");
            }
        }

        protected void btnPaxNextNo_Click(object sender, EventArgs e)
        {
            RadAjaxManager.GetCurrent(Page).FocusControl(btnSave);
        }


        private string PaxPurposeError()
        {
            string desc = string.Empty;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Session["CorpAvailablePaxList"] = RetainPAXInfoGrid(dgAvailablePax);
                List<PassengerInLegs> passengerList = (List<PassengerInLegs>)Session["CorpAvailablePaxList"];
                List<PassengerInLegs> nolegsPAXlist = new List<PassengerInLegs>();


                bool nopurposeassigned = false;
                foreach (PassengerInLegs paxlegs in passengerList)
                {
                    List<PaxLegInfo> legsPAXlist = new List<PaxLegInfo>();

                    legsPAXlist = (from paxleg in paxlegs.Legs
                                   where paxleg.FlightPurposeID != 0
                                   select paxleg).ToList();
                    if (legsPAXlist == null || legsPAXlist.Count == 0)
                        nopurposeassigned = true;
                }

                if (nopurposeassigned)
                    desc = "Warning - Passenger Not Assigned To Any Legs And Will Not Be Saved. Continue Without Saving Your Entry";
            }
            return desc;
        }

        protected void btnCancelYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                        {
                            CRMain oCRMain = new CRMain();
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                oCRMain = (CRMain)Session["CurrentCorporateRequest"];
                                var RetTrip = Service.GetCRHistory(Convert.ToInt64(oCRMain.CustomerID), Convert.ToInt64(oCRMain.CRMainID));
                                StringBuilder HistoryDescription = new System.Text.StringBuilder();
                                string OldDescription = string.Empty;
                                CRHistory TripHistory = new CRHistory();
                                if (oCRMain.TripID != null || oCRMain.TripID != 0)
                                {
                                    oCRMain.CorporateRequestStatus = "X";
                                    var Result = Service.CancelRequest(oCRMain);
                                    if (Result.ReturnFlag == true)
                                    {
                                        if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                        {
                                            //TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                            TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                            TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                            TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                            TripHistory.LastUpdTS = DateTime.UtcNow;
                                            HistoryDescription.AppendLine(string.Format("Existing Trip Cancel Request Submitted To The Change Queue."));
                                            //OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                            TripHistory.LogisticsHistory = HistoryDescription.ToString();
                                            TripHistory.IsDeleted = false;
                                            var ReturnHistory = Service.AddCRHistory(TripHistory);
                                        }
                                        RadWindowManager1.RadAlert("Request canceled successfully.", 360, 50, "Corporate request", "alertCancelCallBackFn", null);
                                    }
                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("Trip Was Never Submitted To Dispatch. Please Use The Delete Button To Delete The Trip.", 360, 50, "Corporate request", "");
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }
        protected void btnCancellationReq_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CRMain oCRMain = new CRMain();
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            oCRMain = (CRMain)Session["CurrentCorporateRequest"];

                            if (oCRMain.TripID != null)
                            {
                                RadWindowManager1.RadConfirm("Submit Cancel Request To Change Queue ?", "confirmCancelRequestCallBackFn", 400, 30, null, "Corporate request");
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Trip Was Never Submitted To Dispatch. Please Use The Delete Button To Delete The Trip.", 360, 50, "Corporate request", "");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }
        protected void btnSubmitYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                        {
                            CRMain oCRMain = new CRMain();
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                oCRMain = (CRMain)Session["CurrentCorporateRequest"];
                                var RetTrip = Service.GetCRHistory(Convert.ToInt64(oCRMain.CustomerID), Convert.ToInt64(oCRMain.CRMainID));
                                StringBuilder HistoryDescription = new System.Text.StringBuilder();
                                string OldDescription = string.Empty;
                                CRHistory TripHistory = new CRHistory();
                                ////Prakash
                                //if (oCRMain.CorporateRequestStatus != null && oCRMain.CorporateRequestStatus == "T")
                                //{
                                //    oCRMain.CorporateRequestStatus = "A";
                                //}
                                ////Prakash
                                if (oCRMain.CorporateRequestStatus != "S" && oCRMain.CorporateRequestStatus != "A")
                                {
                                    string status = "W";
                                    if (oCRMain.CorporateRequestStatus != null && oCRMain.CorporateRequestStatus.ToString().Trim() != string.Empty)
                                        status = oCRMain.CorporateRequestStatus;
                                    oCRMain.CorporateRequestStatus = "S";
                                    var Result = Service.UpdateCRMainCorpStatus(oCRMain);
                                    if (Result.ReturnFlag == true)
                                    {
                                        if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                        {
                                            // TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                            TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                            TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                            TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                            TripHistory.LastUpdTS = DateTime.UtcNow;
                                            if (status != "W")
                                                HistoryDescription.AppendLine(string.Format("Existing Trip Request Resubmitted To The Change Queue."));
                                            else
                                                HistoryDescription.AppendLine(string.Format("New Trip Request Submitted To The Change Queue."));
                                            // OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                            TripHistory.LogisticsHistory = HistoryDescription.ToString();
                                            TripHistory.IsDeleted = false;
                                            var ReturnHistory = Service.AddCRHistory(TripHistory);
                                        }
                                        RadWindowManager1.RadAlert("Request submitted successfully.", 360, 50, "Corporate request", "alertsubmitCallBackFn", null);
                                    }
                                }
                                else if (oCRMain.CorporateRequestStatus == "A")
                                {
                                    oCRMain.CorporateRequestStatus = "S";
                                    var Result = Service.UpdateCRMainCorpStatus(oCRMain);
                                    if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                    {
                                        //TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                        TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                        TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                        TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                        TripHistory.LastUpdTS = DateTime.UtcNow;
                                        HistoryDescription.AppendLine(string.Format("Existing Trip Request Resubmitted To The Change Queue."));
                                        // OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                        TripHistory.LogisticsHistory = HistoryDescription.ToString();
                                        TripHistory.IsDeleted = false;
                                        var ReturnHistory = Service.AddCRHistory(TripHistory);
                                    }
                                    RadWindowManager1.RadAlert("Request submitted successfully.", 360, 50, "Corporate request", "alertsubmitCallBackFn", null);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }
        protected void btnSubmitReq_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                        {
                            bool DatCheck = true;
                            DateTime CompanyTime = System.DateTime.Now;
                            double CompanySetting = 0;

                            if (UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinHours != null && UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinHours != 0)
                            {
                                CompanySetting = Convert.ToDouble(UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinHours);
                            }
                            CompanyTime = CompanyTime.AddHours(CompanySetting);
                            CRMain oCRMain = new CRMain();
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                oCRMain = (CRMain)Session["CurrentCorporateRequest"];
                                if (oCRMain.CRLegs != null && oCRMain.CRLegs.Count() > 0)
                                {
                                    foreach (CRLeg legs in oCRMain.CRLegs)
                                    {
                                        if (legs.DepartureDTTMLocal != null && legs.LegNUM == 1)
                                        {
                                            if (Convert.ToDateTime(legs.DepartureDTTMLocal) <= CompanyTime)
                                            {
                                                DatCheck = false;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            if (DatCheck == false)
                            {
                                if (UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinWeekend != null && UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinWeekend == true)
                                {
                                    if (System.DateTime.Now.DayOfWeek == DayOfWeek.Saturday || System.DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        DatCheck = true;
                                    }
                                }
                            }

                            if (DatCheck == false && CompanySetting != 0)
                            {
                                RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"confirmExcludeOnSubmitMain();");
                            }
                            else
                            {

                                if (Session["CurrentCorporateRequest"] != null)
                                {
                                    oCRMain = (CRMain)Session["CurrentCorporateRequest"];

                                    if (oCRMain.CorporateRequestStatus != "S")
                                    {
                                        RadWindowManager1.RadConfirm("Submit Travel Request To Change Queue ?", "confirmSubmitCallBackFn", 400, 30, null, "Corporate request");
                                    }
                                    else if (oCRMain.CorporateRequestStatus == "S")
                                    {
                                        RadWindowManager1.RadAlert("Trip Request Is Already Submitted To The Change Queue.", 360, 50, "Corporate request", "");

                                    }
                                }
                            }



                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }
        protected void btnAcknowledge_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                        {
                            CRMain oCRMain = new CRMain();
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                oCRMain = (CRMain)Session["CurrentCorporateRequest"];
                                var RetTrip = Service.GetCRHistory(Convert.ToInt64(oCRMain.CustomerID), Convert.ToInt64(oCRMain.CRMainID));
                                StringBuilder HistoryDescription = new System.Text.StringBuilder();
                                string OldDescription = string.Empty;
                                CRHistory TripHistory = new CRHistory();
                                if (oCRMain.AcknowledgementStatus == "A")
                                {
                                    string strLastuid = oCRMain.LastUpdUID;
                                    var ObjRetval = Service.RequestAcknowledge((long)oCRMain.CRMainID, (long)oCRMain.TripID, (long)oCRMain.CustomerID, strLastuid, Convert.ToDateTime(oCRMain.LastUpdTS));
                                    if (ObjRetval.ReturnFlag)
                                    {
                                        oCRMain = ObjRetval.EntityInfo;
                                        Session["CurrentCorporateRequest"] = oCRMain;
                                        if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                        {
                                            //TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                            TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                            TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                            TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                            TripHistory.LastUpdTS = DateTime.UtcNow;
                                            HistoryDescription.AppendLine(string.Format("Acknowledged Changes Made By Dispatch."));
                                            // OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                            TripHistory.LogisticsHistory = HistoryDescription.ToString();
                                            TripHistory.IsDeleted = false;
                                            var ReturnHistory = Service.AddCRHistory(TripHistory);
                                        }
                                        RadWindowManager1.RadAlert("Request Acknowledged successfully.", 360, 50, "Corporate request", "RedirectPage");
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string PaxpurposeErrorstr = string.Empty;
                        PaxpurposeErrorstr = PaxPurposeError();

                        if (!string.IsNullOrEmpty(PaxpurposeErrorstr))
                            RadWindowManager1.RadConfirm(PaxpurposeErrorstr, "confirmPaxNextCallBackFn", 330, 100, null, "Confirmation!");
                        else
                        {
                            SaveToCorpRequestSession();
                            Master.Next("Pax");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                            Master.Cancel(CorpRequest);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        /// <summary>
        /// btnDelete Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                            if (CorpRequest.CorporateRequestStatus == "W")
                            {
                                Master.Delete(CorpRequest.CRMainID);
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Trip Request has been transferred to Preflight. Please submit Cancel request to Change Queue.", 450, 50, "Corporate request", "");

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void SaveToCorpRequestSessionOnTabChange()
        {
            string PaxpurposeErrorstr = string.Empty;
            PaxpurposeErrorstr = PaxPurposeError();

            if (!string.IsNullOrEmpty(PaxpurposeErrorstr))
                Session["CorpPAXnotAssigned"] = PaxpurposeErrorstr;
            else
            {
                Session.Remove("CorpPAXnotAssigned");
                SaveToCorpRequestSession();
            }
        }

        /// <summary>
        /// Method to Save Corp Passenger form fields saved into session
        /// </summary>
        protected void SaveToCorpRequestSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentCorporateRequest"] != null)
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                    if (CorpRequest.Mode == CorporateRequestTripActionMode.Edit)
                    {

                        Session["CorpAvailablePaxList"] = RetainPAXInfoGrid(dgAvailablePax);

                        Session["CorpPaxSummaryList"] = RetainPaxSummaryGrid(dgPaxSummary);
                        //}
                        List<PassengerInLegs> passengerList = (List<PassengerInLegs>)Session["CorpAvailablePaxList"];
                        List<PassengerSummary> passengerSummaryList = RetainPaxSummaryGrid(dgPaxSummary);

                        SavePassengers(passengerList, passengerSummaryList);

                        Session["CurrentCorporateRequest"] = CorpRequest;

                        Master.BindException();
                    }
                }
            }
        }

        /// <summary>
        /// Method to Retain PAX Info Grid Items into List
        /// </summary>
        /// <param name="Grid"></param>
        /// <returns>Returns PAX Info List</returns>
        private List<PassengerInLegs> RetainPAXInfoGrid(RadGrid grid)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                List<PassengerInLegs> paxInfoList = new List<PassengerInLegs>();

                if (Session["CurrentCorporateRequest"] != null)
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                foreach (GridDataItem Item in dgAvailablePax.MasterTableView.Items)
                {
                    PassengerInLegs PaxInfo = new PassengerInLegs();
                    LinkButton lbtnCode = (LinkButton)Item["Code"].FindControl("lnkPaxCode");
                    PaxInfo.Code = lbtnCode.Text;
                    PaxInfo.PassengerRequestorID = Convert.ToInt64(Item["PassengerRequestorID"].Text);
                    PaxInfo.Name = Item["Name"].Text;
                    if (CorpRequest != null)
                    {
                        if (CorpRequest.CRLegs.Count > 0)
                        {
                            List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                            PaxInfo.Legs = new List<PaxLegInfo>();
                            int counter = 1;
                            for (int leg = 0; leg < Preflegs.Count; leg++)
                            {
                                PaxLegInfo PaxLeg = new PaxLegInfo();
                                PaxLeg.LegID = Convert.ToInt64(Preflegs[leg].CRLegID);
                                if (Preflegs[leg].Airport != null)
                                {
                                    if (Preflegs[leg].Airport.IcaoID != null)
                                        PaxLeg.Depart = Preflegs[leg].Airport.IcaoID;
                                }
                                if (Preflegs[leg].Airport1 != null)
                                {
                                    if (Preflegs[leg].Airport1.IcaoID != null)
                                        PaxLeg.Arrive = Preflegs[leg].Airport1.IcaoID;
                                }

                                foreach (GridColumn col in dgAvailablePax.MasterTableView.Columns)
                                {
                                    PaxInfo.OrderNUM = (Int32)Item.GetDataKeyValue("OrderNUM");
                                    if (col.UniqueName == "Leg" + counter.ToString())
                                    {
                                        DropDownList ddlFlightPurpose = (DropDownList)Item["Leg" + counter.ToString()].FindControl("ddlLeg" + counter.ToString());
                                        if (ddlFlightPurpose.SelectedItem != null)
                                            PaxLeg.FlightPurposeCD = ddlFlightPurpose.SelectedItem.ToString();
                                        if (!string.IsNullOrEmpty(ddlFlightPurpose.SelectedValue))
                                            PaxLeg.FlightPurposeID = Convert.ToInt64(ddlFlightPurpose.SelectedValue);
                                        PaxInfo.Purpose = ddlFlightPurpose.SelectedValue;

                                        break;
                                    }
                                }
                                PaxLeg.LegOrder = counter;
                                counter += 1;
                                PaxInfo.Legs.Add(PaxLeg);
                            }
                        }
                    }
                    paxInfoList.Add(PaxInfo);
                }
                return paxInfoList;
            }
        }

        /// <summary>
        /// page prerender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void page_prerender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                            if (CorpRequest.Mode == CorporateRequestTripActionMode.Edit)
                            {
                                btnSubmitReq.Enabled = false;
                                btnCancellationReq.Enabled = false;
                                btnAcknowledge.Enabled = false;
                                btnSave.Enabled = true;
                                btnCancel.Enabled = true;
                                btnDeleteTrip.Enabled = false;
                                btnEditTrip.Enabled = false;

                                btnSave.CssClass = "button";
                                btnCancel.CssClass = "button";
                                btnDeleteTrip.CssClass = "button-disable";
                                btnEditTrip.CssClass = "button-disable";
                                btnSubmitReq.CssClass = "button-disable";
                                btnCancellationReq.CssClass = "button-disable";
                                btnAcknowledge.CssClass = "button-disable";
                                disableenablefields(true);
                                if (IsAuthorized(Permission.CorporateRequest.AddCRPAXManifest) || IsAuthorized(Permission.CorporateRequest.EditCRPAXManifest))
                                {
                                    foreach (GridDataItem dataItem in dgAvailablePax.MasterTableView.Items)
                                    {
                                        // ((Button)dataItem["DeleteColumn"].Controls[0]).Enabled = true;
                                        if (CorpRequest.CRLegs != null)
                                        {
                                            List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                                            bool isdeadhead = false;
                                            for (int i = 0; i < Preflegs.Count; i++)
                                            {
                                                if (Preflegs[i].IsDeleted == false)
                                                {
                                                    if (Preflegs[i].FlightCategoryID != null)
                                                    {
                                                        using (FlightPakMasterService.MasterCatalogServiceClient objFlightCategoryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                        {
                                                            List<FlightPak.Web.FlightPakMasterService.FlightCatagory> FlightCatagoryLists = new List<FlightPak.Web.FlightPakMasterService.FlightCatagory>();
                                                            var objRetVal = objFlightCategoryService.GetFlightCategoryList();
                                                            if (objRetVal.ReturnFlag == true)
                                                            {
                                                                FlightCatagoryLists = objRetVal.EntityList.Where(x => x.FlightCategoryID == Convert.ToInt64(Preflegs[i].FlightCategoryID)).ToList();
                                                                if (FlightCatagoryLists != null && FlightCatagoryLists.Count > 0)
                                                                {
                                                                    if (FlightCatagoryLists[0].IsDeadorFerryHead == true)
                                                                    {
                                                                        isdeadhead = true;
                                                                    }
                                                                    else
                                                                    {
                                                                        isdeadhead = false;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    isdeadhead = false;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                        isdeadhead = false;
                                                    DropDownList ddlFP = (DropDownList)dataItem["Leg" + (Preflegs[i].LegNUM).ToString()].FindControl("ddlLeg" + (Preflegs[i].LegNUM).ToString());
                                                    if (!isdeadhead)
                                                    {
                                                        ddlFP.Enabled = true;
                                                    }
                                                    else
                                                    {
                                                        ddlFP.Enabled = false;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    foreach (GridDataItem dataItem in dgPaxSummary.MasterTableView.Items)
                                    {
                                        TextBox tbPassport = (TextBox)dataItem.FindControl("tbPassport");
                                        tbPassport.Enabled = false;
                                        TextBox tbBillingCode = (TextBox)dataItem.FindControl("tbBillingCode");
                                        tbBillingCode.Enabled = true;
                                    }
                                }
                                else
                                {
                                    foreach (GridDataItem dataItem in dgAvailablePax.MasterTableView.Items)
                                    {
                                        ((Button)dataItem["DeleteColumn"].Controls[0]).Enabled = false;
                                        if (CorpRequest.CRLegs != null)
                                        {
                                            for (int i = 0; i < CorpRequest.CRLegs.Count; i++)
                                            {
                                                if (CorpRequest.CRLegs[i].IsDeleted == false)
                                                {
                                                    DropDownList ddlFP = (DropDownList)dataItem["Leg" + (CorpRequest.CRLegs[i].LegNUM).ToString()].FindControl("ddlLeg" + (CorpRequest.CRLegs[i].LegNUM).ToString());
                                                    ddlFP.Enabled = false;
                                                }
                                            }
                                        }
                                    }
                                    foreach (GridDataItem dataItem in dgPaxSummary.MasterTableView.Items)
                                    {
                                        TextBox txtStreet = (TextBox)dataItem.FindControl("txtStreet");
                                        txtStreet.Enabled = false;
                                        TextBox txtPostal = (TextBox)dataItem.FindControl("txtPostal");
                                        txtPostal.Enabled = false;
                                        TextBox txtCity = (TextBox)dataItem.FindControl("txtCity");
                                        txtCity.Enabled = false;
                                        TextBox txtState = (TextBox)dataItem.FindControl("txtState");
                                        txtState.Enabled = false;
                                        TextBox tbPurpose = (TextBox)dataItem.FindControl("tbPurpose");
                                        tbPurpose.Enabled = false;
                                        TextBox tbPassport = (TextBox)dataItem.FindControl("tbPassport");
                                        tbPassport.Enabled = false;
                                        TextBox tbBillingCode = (TextBox)dataItem.FindControl("tbBillingCode");
                                        tbBillingCode.Enabled = false;
                                    }
                                }
                            }
                            else
                            {
                                btnSave.Enabled = false;
                                btnCancel.Enabled = false;
                                btnDeleteTrip.Enabled = true;
                                btnEditTrip.Enabled = true;

                                btnSave.CssClass = "button-disable";
                                btnCancel.CssClass = "button-disable";
                                btnDeleteTrip.CssClass = "button";
                                btnEditTrip.CssClass = "button";
                                disableenablefields(false);
                            }
                        }
                        else
                        {
                            btnSubmitReq.Enabled = false;
                            btnCancellationReq.Enabled = false;
                            btnAcknowledge.Enabled = false;
                            btnSave.Enabled = false;
                            btnCancel.Enabled = false;
                            btnDeleteTrip.Enabled = false;
                            btnEditTrip.Enabled = false;
                            btnNext.Enabled = false;

                            btnSubmitReq.CssClass = "button-disable";
                            btnCancellationReq.CssClass = "button-disable";
                            btnAcknowledge.CssClass = "button-disable";
                            btnSave.CssClass = "button-disable";
                            btnCancel.CssClass = "button-disable";
                            btnDeleteTrip.CssClass = "button-disable";
                            btnEditTrip.CssClass = "button-disable";
                            btnNext.CssClass = "button-disable";
                            disableenablefields(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        /// <summary>
        /// disableenablefields
        /// </summary>
        /// <param name="status"></param>
        private void disableenablefields(bool status)
        {
            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(status))
            {
                lnkPaxRoaster.Visible = status;
                btnAddremoveColumns.Enabled = status;
                ddlFlightPurpose.Enabled = status;
                SearchBox.Enabled = status;


                foreach (GridNoRecordsItem item in dgAvailablePax.MasterTableView.GetItems(GridItemType.NoRecordsItem))
                {
                    GridCommandItem commandItem = (GridCommandItem)dgAvailablePax.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                    LinkButton lnkButton = (LinkButton)commandItem.FindControl("lnkPaxDelete");
                    lnkButton.Enabled = status;
                    System.Web.UI.HtmlControls.HtmlAnchor insertlink = (System.Web.UI.HtmlControls.HtmlAnchor)commandItem.FindControl("aInsertCrew");
                    if (insertlink != null)
                    {
                        if (status)
                            insertlink.Attributes.Add("onclick", "ShowPaxInfoPopup('insert')");
                        else
                            insertlink.Attributes.Add("onclick", string.Empty);

                    }
                }
                foreach (GridDataItem dataItem in dgAvailablePax.MasterTableView.Items)
                {

                    GridCommandItem commandItem = (GridCommandItem)dgAvailablePax.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                    LinkButton lnkButton = (LinkButton)commandItem.FindControl("lnkPaxDelete");
                    //((Button)dataItem["DeleteColumn"].Controls[0]).Enabled = status;
                    lnkButton.Enabled = status;
                    System.Web.UI.HtmlControls.HtmlAnchor insertlink = (System.Web.UI.HtmlControls.HtmlAnchor)commandItem.FindControl("aInsertCrew");
                    if (insertlink != null)
                    {
                        if (status)
                            insertlink.Attributes.Add("onclick", "ShowPaxInfoPopup('insert')");
                        else
                            insertlink.Attributes.Add("onclick", string.Empty);

                    }
                    if (CorpRequest != null)
                    {
                        if (CorpRequest.CRLegs != null)
                        {
                            List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                            bool isdeadhead = false;
                            for (int i = 0; i < Preflegs.Count; i++)
                            {
                                if (Preflegs[i].FlightCategoryID != null)
                                {
                                    using (FlightPakMasterService.MasterCatalogServiceClient objFlightCategoryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        List<FlightPak.Web.FlightPakMasterService.FlightCatagory> FlightCatagoryLists = new List<FlightPak.Web.FlightPakMasterService.FlightCatagory>();
                                        var objRetVal = objFlightCategoryService.GetFlightCategoryList();
                                        if (objRetVal.ReturnFlag == true)
                                        {
                                            FlightCatagoryLists = objRetVal.EntityList.Where(x => x.FlightCategoryID == Convert.ToInt64(Preflegs[i].FlightCategoryID)).ToList();
                                            if (FlightCatagoryLists != null && FlightCatagoryLists.Count > 0)
                                            {
                                                if (FlightCatagoryLists[0].IsDeadorFerryHead == true)
                                                {
                                                    isdeadhead = true;
                                                }
                                                else
                                                {
                                                    isdeadhead = false;
                                                }
                                            }
                                            else
                                            {
                                                isdeadhead = false;
                                            }
                                        }
                                    }
                                }
                                else
                                    isdeadhead = false;
                                DropDownList ddlFP = (DropDownList)dataItem["Leg" + (Preflegs[i].LegNUM).ToString()].FindControl("ddlLeg" + (Preflegs[i].LegNUM).ToString());
                                if (!isdeadhead)
                                {
                                    ddlFP.Enabled = status;
                                }
                                else
                                {
                                    ddlFP.Enabled = false;
                                }
                            }
                        }
                    }
                }

                foreach (GridFooterItem footerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Footer))
                {
                    if (CorpRequest != null)
                    {
                        if (CorpRequest.CRLegs != null)
                        {
                            List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                            for (int i = 0; i < Preflegs.Count; i++)
                            {
                                if (Preflegs[i].IsDeleted == false)
                                {
                                    TextBox txtBlockedSeats = (TextBox)footerItem["Leg" + (Preflegs[i].LegNUM).ToString()].FindControl("tbLeg" + (Preflegs[i].LegNUM).ToString());
                                    txtBlockedSeats.Enabled = status;
                                }
                            }
                        }
                    }
                }
                foreach (GridDataItem dataItem in dgPaxSummary.MasterTableView.Items)
                {
                    TextBox tbPassport = (TextBox)dataItem.FindControl("tbPassport");
                    tbPassport.Enabled = false;//status;
                    TextBox tbBillingCode = (TextBox)dataItem.FindControl("tbBillingCode");
                    tbBillingCode.Enabled = status;
                }
            }
        }



        /// <summary>
        /// Method to Retain Passenger Summary Grid
        /// </summary>
        /// <param name="gridPaxSummary">Pass Grid Control</param>
        /// <returns>Returns Passenger Summary List</returns>
        private List<PassengerSummary> RetainPaxSummaryGrid(RadGrid gridPaxSummary)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(gridPaxSummary))
            {
                List<PassengerSummary> paxSummaryList = new List<PassengerSummary>();


                foreach (GridDataItem Item in gridPaxSummary.MasterTableView.Items)
                {
                    long legId = Convert.ToInt64(((Label)Item.FindControl("lblLegID")).Text);
                    long paxID = Convert.ToInt64(((Label)Item.FindControl("lblPassengerRequestorID")).Text);
                    string paxCode = ((Label)Item.FindControl("lblCode")).Text;
                    string paxName = ((Label)Item.FindControl("lblName")).Text;
                    string dateofbirth = ((Label)Item.FindControl("lbDateOfBirth")).Text;

                    //string Street = ((TextBox)Item.FindControl("txtStreet")).Text;
                    //string Postal = ((TextBox)Item.FindControl("txtPostal")).Text;
                    //string City = ((TextBox)Item.FindControl("txtCity")).Text;
                    //string State = ((TextBox)Item.FindControl("txtState")).Text;

                    //string Purpose = ((TextBox)Item.FindControl("tbPurpose")).Text;
                    string Passport = ((TextBox)Item.FindControl("tbPassport")).Text;
                    string BillingCode = ((TextBox)Item.FindControl("tbBillingCode")).Text;
                    string VisaID = ((HiddenField)Item.FindControl("hdnVisaID")).Value;
                    string PassportID = ((HiddenField)Item.FindControl("hdnPassID")).Value;
                    string PassportExpiryDT = Item["PassportExpiryDT"].Text;
                    string Nation = Item["Nation"].Text;
                    Int32 OrderNUM = (Int32)Item.GetDataKeyValue("OrderNUM");
                    paxSummaryList.Add(new PassengerSummary()
                    {
                        LegId = legId,
                        PaxID = paxID,
                        PaxName = paxName,
                        PaxCode = paxCode,
                        Street = string.Empty,
                        Postal = string.Empty,
                        City = string.Empty,
                        State = string.Empty,
                        Purpose = string.Empty,
                        Passport = Passport,
                        BillingCode = BillingCode,
                        IsNonPassenger = false,
                        OrderNum = OrderNUM,
                        WaitList = string.Empty,
                        VisaID = VisaID,
                        PassportID = PassportID,
                        PassportExpiryDT = PassportExpiryDT,
                        Nation = Nation,
                        DateOfBirth = dateofbirth
                    });
                }

                return paxSummaryList;
            }
        }

        private void SavePassengers(List<PassengerInLegs> paxlist, List<PassengerSummary> sumlist)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paxlist, sumlist))
            {
                int LegNum = 0;
                if (Session["CurrentCorporateRequest"] != null)
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                    if (CorpRequest.CRLegs != null)
                    {
                        List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                        //for (int k = 0; k < sumlist.Count; k++)
                        //{
                        for (int i = 0; i < paxlist.Count; i++)
                        {
                            if (paxlist[i].Legs != null)
                            {
                                for (int j = 0; j < paxlist[i].Legs.Count; j++)
                                {
                                    if (!string.IsNullOrEmpty(paxlist[i].Legs[j].FlightPurposeID.ToString()) && paxlist[i].Legs[j].FlightPurposeID != 0)
                                    {
                                        #region "Updating PAX"
                                        for (int k = 0; k < sumlist.Count; k++)
                                        {
                                            if (paxlist[i].PassengerRequestorID == sumlist[k].PaxID && sumlist[k].LegId == paxlist[i].Legs[j].LegOrder)
                                            {
                                                LegNum = Convert.ToInt16(paxlist[i].Legs[j].LegOrder);
                                                foreach (CRLeg Leg in Preflegs)
                                                {
                                                    if (Leg.IsDeleted == false)
                                                    {
                                                        if (Leg.LegNUM == LegNum)
                                                        {
                                                            if (Leg.CRLegID != 0 && Leg.State != CorporateRequestTripEntityState.Deleted)
                                                            {
                                                                Leg.State = CorporateRequestTripEntityState.Modified;
                                                            }

                                                            Preflegs[LegNum - 1].ReservationTotal = 0;
                                                            Preflegs[LegNum - 1].WaitNUM = 0;
                                                            #region Leg reservation
                                                            if (LegNum == 1)
                                                            {
                                                                string strpax = hdnLeg1p1.Value;
                                                                string stravailable = hdnLeg1a1.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 2)
                                                            {
                                                                string strpax = hdnLeg2p2.Value;
                                                                string stravailable = hdnLeg2a2.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 3)
                                                            {
                                                                string strpax = hdnLeg3p3.Value;
                                                                string stravailable = hdnLeg3a3.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 4)
                                                            {
                                                                string strpax = hdnLeg4p4.Value;
                                                                string stravailable = hdnLeg4a4.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 5)
                                                            {
                                                                string strpax = hdnLeg5p5.Value;
                                                                string stravailable = hdnLeg5a5.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 6)
                                                            {
                                                                string strpax = hdnLeg6p6.Value;
                                                                string stravailable = hdnLeg6a6.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 7)
                                                            {
                                                                string strpax = hdnLeg7p7.Value;
                                                                string stravailable = hdnLeg7a7.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 8)
                                                            {
                                                                string strpax = hdnLeg8p8.Value;
                                                                string stravailable = hdnLeg8a8.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 9)
                                                            {
                                                                string strpax = hdnLeg9p9.Value;
                                                                string stravailable = hdnLeg9a9.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 10)
                                                            {
                                                                string strpax = hdnLeg10p10.Value;
                                                                string stravailable = hdnLeg10a10.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 11)
                                                            {
                                                                string strpax = hdnLeg11p11.Value;
                                                                string stravailable = hdnLeg11a11.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 12)
                                                            {
                                                                string strpax = hdnLeg12p12.Value;
                                                                string stravailable = hdnLeg12a12.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 13)
                                                            {
                                                                string strpax = hdnLeg13p13.Value;
                                                                string stravailable = hdnLeg13a13.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 14)
                                                            {
                                                                string strpax = hdnLeg14p14.Value;
                                                                string stravailable = hdnLeg14a14.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 15)
                                                            {
                                                                string strpax = hdnLeg15p15.Value;
                                                                string stravailable = hdnLeg15a15.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 16)
                                                            {
                                                                string strpax = hdnLeg16p16.Value;
                                                                string stravailable = hdnLeg16a16.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 17)
                                                            {
                                                                string strpax = hdnLeg17p17.Value;
                                                                string stravailable = hdnLeg17a17.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 18)
                                                            {
                                                                string strpax = hdnLeg18p18.Value;
                                                                string stravailable = hdnLeg18a18.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 19)
                                                            {
                                                                string strpax = hdnLeg19p19.Value;
                                                                string stravailable = hdnLeg19a19.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 20)
                                                            {
                                                                string strpax = hdnLeg20p20.Value;
                                                                string stravailable = hdnLeg20a20.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 21)
                                                            {
                                                                string strpax = hdnLeg21p21.Value;
                                                                string stravailable = hdnLeg21a21.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 22)
                                                            {
                                                                string strpax = hdnLeg22p22.Value;
                                                                string stravailable = hdnLeg22a22.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 23)
                                                            {
                                                                string strpax = hdnLeg23p23.Value;
                                                                string stravailable = hdnLeg23a23.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 24)
                                                            {
                                                                string strpax = hdnLeg24p24.Value;
                                                                string stravailable = hdnLeg24a24.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }
                                                            if (LegNum == 25)
                                                            {
                                                                string strpax = hdnLeg25p25.Value;
                                                                string stravailable = hdnLeg25a25.Value;
                                                                if (!string.IsNullOrEmpty(strpax))
                                                                    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                                if (!string.IsNullOrEmpty(stravailable))
                                                                    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                            }

                                                            #endregion

                                                            Int64 GridPaxID = 0;
                                                            GridPaxID = Convert.ToInt64(paxlist[i].PassengerRequestorID);

                                                            //if (Leg.LegID > 0)
                                                            //{
                                                            if (Leg.CRPassengers != null && Leg.CRPassengers.Count > 0)
                                                            {
                                                                bool PassFound = false;
                                                                foreach (CRPassenger PrefPass in Leg.CRPassengers)
                                                                {
                                                                    if (PrefPass.PassengerRequestorID == GridPaxID)
                                                                    {
                                                                        #region Passenge Modfiy Mode Already Exists
                                                                        if (PrefPass.CRPassengerID != 0)
                                                                            PrefPass.State = CorporateRequestTripEntityState.Modified;
                                                                        else
                                                                            PrefPass.State = CorporateRequestTripEntityState.Added;
                                                                        PrefPass.CRLegID = Leg.CRLegID;
                                                                        PrefPass.IsDeleted = false;

                                                                        PrefPass.PassengerRequestorID = Convert.ToInt64(paxlist[i].PassengerRequestorID);
                                                                        PrefPass.PassengerName = paxlist[i].Name.ToString();
                                                                        //paxlist[i].Name.ToString().Replace(',', ' ').Trim();

                                                                        PrefPass.OrderNUM = paxlist[i].OrderNUM;
                                                                        if (!string.IsNullOrEmpty(sumlist[k].PassportID))
                                                                            PrefPass.PassportID = Convert.ToInt64(sumlist[k].PassportID);



                                                                        PrefPass.FlightPurposeID = Convert.ToInt64(paxlist[i].Legs[j].FlightPurposeID);
                                                                        if (!string.IsNullOrEmpty(sumlist[k].BillingCode))
                                                                            PrefPass.Billing = sumlist[k].BillingCode;

                                                                        PrefPass.IsNonPassenger = false;
                                                                        PrefPass.IsBlocked = false;
                                                                        PrefPass.IsDeleted = false;
                                                                        PrefPass.LastUpdTS = DateTime.UtcNow;
                                                                        PassFound = true;

                                                                        #endregion
                                                                        break;
                                                                    }
                                                                }
                                                                //Add here 
                                                                if (!PassFound)
                                                                {
                                                                    #region Passnot found
                                                                    //it is a new Pax for this leg
                                                                    CRPassenger NewPaxList = new CRPassenger();
                                                                    NewPaxList.State = CorporateRequestTripEntityState.Added;
                                                                    NewPaxList.CRLegID = Leg.CRLegID;
                                                                    NewPaxList.IsDeleted = false;

                                                                    //add the passenger list here
                                                                    NewPaxList.PassengerRequestorID = Convert.ToInt64(paxlist[i].PassengerRequestorID);
                                                                    NewPaxList.PassengerName = paxlist[i].Name.ToString();
                                                                    //paxlist[i].Name.ToString().Replace(',', ' ').Trim();


                                                                    if (!string.IsNullOrEmpty(sumlist[k].PassportID))
                                                                        NewPaxList.PassportID = Convert.ToInt64(sumlist[k].PassportID);


                                                                    NewPaxList.FlightPurposeID = Convert.ToInt64(paxlist[i].Legs[j].FlightPurposeID);
                                                                    if (!string.IsNullOrEmpty(sumlist[k].BillingCode))
                                                                        NewPaxList.Billing = sumlist[k].BillingCode;
                                                                    NewPaxList.OrderNUM = paxlist[i].OrderNUM;
                                                                    NewPaxList.IsNonPassenger = false;
                                                                    NewPaxList.IsBlocked = false;
                                                                    NewPaxList.IsDeleted = false;
                                                                    NewPaxList.LastUpdTS = DateTime.UtcNow;


                                                                    Leg.CRPassengers.Add(NewPaxList);
                                                                    #endregion
                                                                }
                                                            }
                                                            else
                                                            {
                                                                #region noPassenger in legs
                                                                //it is a new crew for this leg
                                                                if (Leg.CRPassengers == null)
                                                                {
                                                                    Leg.CRPassengers = new List<CRPassenger>();
                                                                }
                                                                CRPassenger NewPaxList = new CRPassenger();
                                                                NewPaxList.State = CorporateRequestTripEntityState.Added;
                                                                NewPaxList.CRLegID = Leg.CRLegID;
                                                                NewPaxList.IsDeleted = false;
                                                                //add pax here 
                                                                NewPaxList.PassengerRequestorID = Convert.ToInt64(paxlist[i].PassengerRequestorID);
                                                                NewPaxList.PassengerName = paxlist[i].Name.ToString();
                                                                //paxlist[i].Name.ToString().Replace(',', ' ').Trim();

                                                                if (!string.IsNullOrEmpty(sumlist[k].PassportID))
                                                                    NewPaxList.PassportID = Convert.ToInt64(sumlist[k].PassportID);


                                                                NewPaxList.FlightPurposeID = Convert.ToInt64(paxlist[i].Legs[j].FlightPurposeID);
                                                                if (!string.IsNullOrEmpty(sumlist[k].BillingCode))
                                                                    NewPaxList.Billing = sumlist[k].BillingCode;
                                                                NewPaxList.OrderNUM = paxlist[i].OrderNUM;
                                                                NewPaxList.IsNonPassenger = false;
                                                                NewPaxList.IsBlocked = false;
                                                                NewPaxList.IsDeleted = false;
                                                                NewPaxList.LastUpdTS = DateTime.UtcNow;

                                                                Leg.CRPassengers.Add(NewPaxList);
                                                                #endregion
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        #endregion
                                    }
                                    else
                                    {
                                        #region Tochange flightpurpose during edit when previously set
                                        if (Preflegs != null)
                                        {
                                            for (int legcnt = 0; legcnt < Preflegs.Count; legcnt++)
                                            {
                                                LegNum = Convert.ToInt16(paxlist[i].Legs[j].LegOrder);
                                                if (Preflegs[legcnt].LegNUM == LegNum)
                                                {
                                                    if (Preflegs[legcnt].CRPassengers != null)
                                                    {
                                                        for (int passcnt = 0; passcnt < Preflegs[legcnt].CRPassengers.Count; passcnt++)
                                                        {
                                                            if (Preflegs[legcnt].CRPassengers[passcnt].PassengerRequestorID == paxlist[i].PassengerRequestorID && Preflegs[legcnt].LegNUM == paxlist[i].Legs[j].LegOrder)
                                                            {
                                                                if (Preflegs[legcnt].CRPassengers[passcnt].CRPassengerID != 0)
                                                                {
                                                                    Preflegs[legcnt].CRPassengers[passcnt].State = CorporateRequestTripEntityState.Deleted;
                                                                    Preflegs[legcnt].CRPassengers[passcnt].IsDeleted = true;
                                                                    Preflegs[legcnt].CRPassengers[passcnt].FlightPurposeID = 0;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                }
                            }
                        }

                        if (paxlist != null && paxlist.Count == 0)
                        {
                            if (Preflegs != null)
                            {
                                for (int legscnt = 0; legscnt < Preflegs.Count; legscnt++)
                                {
                                    if (Preflegs[legscnt].CRLegID != 0)
                                    {
                                        Preflegs[legscnt].State = CorporateRequestTripEntityState.Modified;
                                    }

                                    Preflegs[legscnt].ReservationTotal = 0;
                                    Preflegs[legscnt].WaitNUM = 0;

                                    #region blocked count

                                    if (legscnt + 1 == 1)
                                    {
                                        string strpax = hdnLeg1p1.Value;
                                        string stravailable = hdnLeg1a1.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 2)
                                    {
                                        string strpax = hdnLeg2p2.Value;
                                        string stravailable = hdnLeg2a2.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 3)
                                    {
                                        string strpax = hdnLeg3p3.Value;
                                        string stravailable = hdnLeg3a3.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 4)
                                    {
                                        string strpax = hdnLeg4p4.Value;
                                        string stravailable = hdnLeg4a4.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 5)
                                    {
                                        string strpax = hdnLeg5p5.Value;
                                        string stravailable = hdnLeg5a5.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 6)
                                    {
                                        string strpax = hdnLeg6p6.Value;
                                        string stravailable = hdnLeg6a6.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 7)
                                    {
                                        string strpax = hdnLeg7p7.Value;
                                        string stravailable = hdnLeg7a7.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 8)
                                    {
                                        string strpax = hdnLeg8p8.Value;
                                        string stravailable = hdnLeg8a8.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 9)
                                    {
                                        string strpax = hdnLeg9p9.Value;
                                        string stravailable = hdnLeg9a9.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 10)
                                    {
                                        string strpax = hdnLeg10p10.Value;
                                        string stravailable = hdnLeg10a10.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 11)
                                    {
                                        string strpax = hdnLeg11p11.Value;
                                        string stravailable = hdnLeg11a11.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 12)
                                    {
                                        string strpax = hdnLeg12p12.Value;
                                        string stravailable = hdnLeg12a12.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 13)
                                    {
                                        string strpax = hdnLeg13p13.Value;
                                        string stravailable = hdnLeg13a13.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 14)
                                    {
                                        string strpax = hdnLeg14p14.Value;
                                        string stravailable = hdnLeg14a14.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 15)
                                    {
                                        string strpax = hdnLeg15p15.Value;
                                        string stravailable = hdnLeg15a15.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 16)
                                    {
                                        string strpax = hdnLeg16p16.Value;
                                        string stravailable = hdnLeg16a16.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 17)
                                    {
                                        string strpax = hdnLeg17p17.Value;
                                        string stravailable = hdnLeg17a17.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 18)
                                    {
                                        string strpax = hdnLeg18p18.Value;
                                        string stravailable = hdnLeg18a18.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 19)
                                    {
                                        string strpax = hdnLeg19p19.Value;
                                        string stravailable = hdnLeg19a19.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 20)
                                    {
                                        string strpax = hdnLeg20p20.Value;
                                        string stravailable = hdnLeg20a20.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 21)
                                    {
                                        string strpax = hdnLeg21p21.Value;
                                        string stravailable = hdnLeg21a21.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 22)
                                    {
                                        string strpax = hdnLeg22p22.Value;
                                        string stravailable = hdnLeg22a22.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 23)
                                    {
                                        string strpax = hdnLeg23p23.Value;
                                        string stravailable = hdnLeg23a23.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 24)
                                    {
                                        string strpax = hdnLeg24p24.Value;
                                        string stravailable = hdnLeg24a24.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }
                                    if (legscnt + 1 == 25)
                                    {
                                        string strpax = hdnLeg25p25.Value;
                                        string stravailable = hdnLeg25a25.Value;
                                        if (!string.IsNullOrEmpty(strpax))
                                            Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                        if (!string.IsNullOrEmpty(stravailable))
                                            Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                    }


                                    #endregion
                                }
                            }

                        }
                    }
                }
            }

        }


        #region Pax Summary Grid


        private DataTable FinalPaxSummarylist(string Code)
        {
            DataTable table = null;
            DataTable dtFinal = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                table = dtFinal.Clone();
                drarrayPaxSummary = dtFinal.Select("Code = " + "'" + Code + "'");

                if (drarrayPaxSummary != null)
                {
                    foreach (DataRow drpaxsum in drarrayPaxSummary)
                        table.ImportRow(drpaxsum);
                }
                table.Merge(table);
                return table;
            }
        }

        protected void lnkPaxDelete_OnClick(object source, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsAuthorized(Permission.CorporateRequest.DeleteCRPAXManifest))
                        {
                            if (dgAvailablePax.SelectedItems.Count > 0)
                            {
                                foreach (GridDataItem dataItem in dgAvailablePax.SelectedItems)
                                {
                                    Session["SelectedPaxitems"] += dataItem["PassengerRequestorID"].Text + ",";
                                }

                                RadWindowManager1.RadConfirm("Are you sure you want to delete this record", "confirmDeletePaxCallBackFn", 330, 100, null, "Confirmation!");
                            }
                            else
                            {
                                if (dgAvailablePax.Items != null && dgAvailablePax.Items.Count > 0)
                                {
                                    RadWindowManager1.RadConfirm("Please select the record", "confirmDeletePaxCallBackFn", 330, 100, null, "Confirmation!");
                                }
                                else
                                {
                                    RadWindowManager1.RadConfirm("No Passenger is found for delete,Please add the Passenger", "confirmDeletePaxCallBackFn", 330, 100, null, "Confirmation!");
                                }
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }
        protected void btnDeletePaxYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                Paxdelete();
            }
        }
        protected void btnDeletePaxNo_Click(object sender, EventArgs e)
        {
        }

        protected void Paxdelete()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentCorporateRequest"] != null)
                {
                    if (IsAuthorized(Permission.CorporateRequest.DeleteCRPAXManifest))
                    {
                        if (dgAvailablePax.SelectedItems.Count == 0)
                        {
                            if (Session["SelectedPaxitems"] != null)
                            {
                                foreach (GridDataItem dataItem in dgAvailablePax.Items)
                                {
                                    string[] Paxitems = Session["SelectedPaxitems"].ToString().Split(',');
                                    foreach (string paxitem in Paxitems)
                                    {
                                        if (!string.IsNullOrEmpty(paxitem))
                                        {
                                            if (Convert.ToInt64(dataItem["PassengerRequestorID"].Text) == Convert.ToInt64(paxitem))
                                            {
                                                dataItem.Selected = true;
                                            }
                                        }
                                    }
                                }

                            }
                        }
                        if (dgAvailablePax.SelectedItems.Count > 0)
                        {
                            foreach (GridDataItem dataItem in dgAvailablePax.SelectedItems)
                            {
                                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                SaveToCorpRequestSession();
                                List<PassengerInLegs> FinalpassengerInLegs = new List<PassengerInLegs>();
                                List<PassengerSummary> Finalpassengerlist = new List<PassengerSummary>();
                                FinalpassengerInLegs = (List<PassengerInLegs>)Session["CorpAvailablePaxList"];
                                Finalpassengerlist = (List<PassengerSummary>)Session["CorpPaxSummaryList"];
                                Int64 PaxID = Convert.ToInt64(dataItem["PassengerRequestorID"].Text);
                                //Convert.ToInt64((e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["PassengerRequestorID"]);
                                DataTable dtHotel = new DataTable();

                                if (Session["PaxHotel"] != null)
                                {
                                    dtHotel = (DataTable)Session["PaxHotel"];
                                }
                                if (CorpRequest.CRLegs != null)
                                {
                                    List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                                    for (int Legcnt = 0; Legcnt < Preflegs.Count; Legcnt++)
                                    {
                                        if (Preflegs[Legcnt].CRPassengers != null)
                                        {
                                            if (Preflegs[Legcnt].CRPassengers.Count > 0)
                                            {
                                                for (int PaxCnt = 0; PaxCnt < Preflegs[Legcnt].CRPassengers.Count; PaxCnt++)
                                                {
                                                    if (Preflegs[Legcnt].CRPassengers[PaxCnt].IsDeleted == false)
                                                    {
                                                        if (PaxID == Preflegs[Legcnt].CRPassengers[PaxCnt].PassengerRequestorID)
                                                        {
                                                            if (Preflegs[Legcnt].CRPassengers[PaxCnt].CRPassengerID == 0)
                                                            {

                                                                Preflegs[Legcnt].PassengerTotal = Preflegs[Legcnt].PassengerTotal - 1;
                                                                Preflegs[Legcnt].ReservationAvailable = Preflegs[Legcnt].SeatTotal - Preflegs[Legcnt].PassengerTotal + 1;

                                                                if (Preflegs[Legcnt].CRPassengers[PaxCnt].FlightPurposeID != 0)
                                                                {
                                                                    HiddenField hdnPax = ((HiddenField)DivExternalForm.FindControl("hdnLeg" + Preflegs[Legcnt].LegNUM + "p" + Preflegs[Legcnt].LegNUM));
                                                                    hdnPax.Value = Preflegs[Legcnt].PassengerTotal.ToString();
                                                                    ((HiddenField)DivExternalForm.FindControl("hdnLeg" + Preflegs[Legcnt].LegNUM + "a" + Preflegs[Legcnt].LegNUM)).Value = Preflegs[Legcnt].ReservationAvailable.ToString();
                                                                }
                                                                Preflegs[Legcnt].CRPassengers.Remove(Preflegs[Legcnt].CRPassengers[PaxCnt]);
                                                            }
                                                            else
                                                            {
                                                                Preflegs[Legcnt].PassengerTotal = Preflegs[Legcnt].PassengerTotal - 1;
                                                                Preflegs[Legcnt].ReservationAvailable = Preflegs[Legcnt].SeatTotal - Preflegs[Legcnt].PassengerTotal + 1;
                                                                Preflegs[Legcnt].CRPassengers[PaxCnt].FlightPurposeID = 0;

                                                                ((HiddenField)DivExternalForm.FindControl("hdnLeg" + Preflegs[Legcnt].LegNUM + "p" + Preflegs[Legcnt].LegNUM)).Value = Preflegs[Legcnt].PassengerTotal.ToString();
                                                                ((HiddenField)DivExternalForm.FindControl("hdnLeg" + Preflegs[Legcnt].LegNUM + "a" + Preflegs[Legcnt].LegNUM)).Value = Preflegs[Legcnt].ReservationAvailable.ToString();

                                                                Preflegs[Legcnt].CRPassengers[PaxCnt].State = CorporateRequestTripEntityState.Deleted;
                                                                Preflegs[Legcnt].CRPassengers[PaxCnt].IsDeleted = true;
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }

                                if (dtHotel != null && dtHotel.Rows.Count > 0)
                                {
                                    for (int hotCnt = 0; hotCnt < dtHotel.Rows.Count; hotCnt++)
                                    {
                                        if (Convert.ToInt64(dtHotel.Rows[hotCnt]["PassengerRequestorID"]) == PaxID)
                                        {
                                            dtHotel.Rows[hotCnt].Delete();
                                            hotCnt = hotCnt - 1;
                                        }
                                    }
                                    Session["PaxHotel"] = dtHotel;
                                }


                                for (int FinalAvlCnt = 0; FinalAvlCnt < FinalpassengerInLegs.Count; FinalAvlCnt++)
                                {
                                    if (FinalpassengerInLegs[FinalAvlCnt].PassengerRequestorID == PaxID)
                                    {
                                        FinalpassengerInLegs.Remove(FinalpassengerInLegs[FinalAvlCnt]);
                                    }
                                }


                                for (int FinalAssCnt = 0; FinalAssCnt < Finalpassengerlist.Count; FinalAssCnt++)
                                {
                                    if (Finalpassengerlist[FinalAssCnt].PaxID == PaxID)
                                    {
                                        Finalpassengerlist.Remove(Finalpassengerlist[FinalAssCnt]);
                                        FinalAssCnt = FinalAssCnt - 1;
                                    }
                                }

                                Int32 MAXOrderNUM = 0;
                                foreach (PassengerInLegs Pax in FinalpassengerInLegs.OrderBy(x => x.OrderNUM).ToList())
                                {
                                    MAXOrderNUM++;
                                    Pax.OrderNUM = MAXOrderNUM;

                                    foreach (PassengerSummary PaxSum in Finalpassengerlist.Where(x => x.PaxID == Pax.PassengerRequestorID).ToList())
                                    {
                                        PaxSum.OrderNum = Pax.OrderNUM;
                                    }
                                }

                                Session.Remove("SelectedPaxitems");
                                Session["CurrentCorporateRequest"] = CorpRequest;
                                Session["CorpAvailablePaxList"] = FinalpassengerInLegs;
                                if (Session["CorpAvailablePaxList"] != null)
                                {
                                    dgAvailablePax.DataSource = Session["CorpAvailablePaxList"];
                                    dgAvailablePax.DataBind();
                                }
                                Session["CorpPaxSummaryList"] = Finalpassengerlist;
                                if (Session["CorpPaxSummaryList"] != null)
                                {
                                    dgPaxSummary.DataSource = Session["CorpPaxSummaryList"];
                                    dgPaxSummary.DataBind();
                                }
                                if (dgPaxSummary.Items.Count > 0)
                                {
                                    dgPaxSummary.Items[0].Selected = true;
                                    GridDataItem item = (GridDataItem)dgPaxSummary.SelectedItems[0];
                                    Label lblName = (Label)item.FindControl("lblName");
                                    Label lblLegID = (Label)item.FindControl("lblLegID");
                                }

                            }
                        }
                    }

                }
            }
        }


        private void SearchPaxSummary()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool alreadyExists = false;
                if (Session["CurrentCorporateRequest"] != null)
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                    List<PassengerInLegs> passengerlist = new List<PassengerInLegs>();
                    List<PassengerSummary> Newpassengerlist = new List<PassengerSummary>();
                    List<PassengerSummary> Oldpassengerlist = new List<PassengerSummary>();
                    List<PassengerSummary> Finalpassengerlist = new List<PassengerSummary>();

                    passengerlist = (List<PassengerInLegs>)Session["CorpAvailablePaxList"];

                    if (Session["CorpPaxSummaryList"] != null)
                    {
                        Oldpassengerlist = (List<PassengerSummary>)Session["CorpPaxSummaryList"];
                    }

                    if (CorpRequest.CRLegs != null)
                    {
                        for (int legCount = 1; legCount <= CorpRequest.CRLegs.Count; legCount++)
                        {
                            if (passengerlist != null)
                            {
                                if (Oldpassengerlist != null && Oldpassengerlist.Count > 0)
                                {
                                    for (int paxCnt = 0; paxCnt < passengerlist.Count; paxCnt++)
                                    {

                                        for (int Cnt = 0; Cnt < Oldpassengerlist.Count; Cnt++)
                                        {
                                            alreadyExists = Oldpassengerlist.Any(x => x.PaxID == passengerlist[paxCnt].PassengerRequestorID);
                                        }
                                        if (alreadyExists == false)
                                        {
                                            PassengerSummary pax = new PassengerSummary();
                                            Newpassengerlist.Add(pax);
                                            pax.PaxID = passengerlist[paxCnt].PassengerRequestorID;
                                            pax.PaxCode = passengerlist[paxCnt].Code;
                                            pax.PaxName = passengerlist[paxCnt].Name;
                                            pax.LegId = legCount;
                                            pax.Street = passengerlist[paxCnt].Street;
                                            pax.Postal = passengerlist[paxCnt].Postal;
                                            pax.City = passengerlist[paxCnt].City;
                                            pax.State = passengerlist[paxCnt].State;
                                            pax.Purpose = passengerlist[paxCnt].Purpose;
                                            pax.Passport = passengerlist[paxCnt].Passport;
                                            pax.BillingCode = passengerlist[paxCnt].BillingCode;
                                            pax.VisaID = passengerlist[paxCnt].VisaID;
                                            pax.PassportID = passengerlist[paxCnt].PassportID;
                                            if (!string.IsNullOrEmpty(Convert.ToString(passengerlist[paxCnt].DateOfBirth)))
                                            {
                                                DateTime dtbirthDate = Convert.ToDateTime(passengerlist[paxCnt].DateOfBirth);
                                                pax.DateOfBirth = dtbirthDate.ToShortDateString();
                                            }
                                            //if (!string.IsNullOrEmpty(passengerlist[paxCnt].PassportExpiryDT.ToString()))
                                            if (!string.IsNullOrEmpty(Convert.ToString(passengerlist[paxCnt].PassportExpiryDT)))
                                            {
                                                DateTime dtDate = Convert.ToDateTime(passengerlist[paxCnt].PassportExpiryDT);
                                                if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                    pax.PassportExpiryDT = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ((DateTime)dtDate).ToShortDateString());
                                                else
                                                    pax.PassportExpiryDT = string.Empty;
                                            }
                                            pax.Nation = passengerlist[paxCnt].Nation;
                                        }
                                    }
                                }
                                else
                                {
                                    for (int paxCnt = 0; paxCnt < passengerlist.Count; paxCnt++)
                                    {
                                        PassengerSummary pax = new PassengerSummary();
                                        Newpassengerlist.Add(pax);
                                        pax.PaxID = passengerlist[paxCnt].PassengerRequestorID;
                                        pax.PaxCode = passengerlist[paxCnt].Code;
                                        pax.PaxName = passengerlist[paxCnt].Name;
                                        pax.LegId = legCount;
                                        pax.Street = passengerlist[paxCnt].Street;
                                        pax.Postal = passengerlist[paxCnt].Postal;
                                        pax.City = passengerlist[paxCnt].City;
                                        pax.State = passengerlist[paxCnt].State;
                                        pax.Purpose = passengerlist[paxCnt].Purpose;
                                        pax.Passport = passengerlist[paxCnt].Passport;
                                        pax.BillingCode = passengerlist[paxCnt].BillingCode;
                                        pax.VisaID = passengerlist[paxCnt].VisaID;
                                        pax.PassportID = passengerlist[paxCnt].PassportID;
                                        if (!string.IsNullOrEmpty(Convert.ToString(passengerlist[paxCnt].DateOfBirth)))
                                        {
                                            DateTime dtbirthDate = Convert.ToDateTime(passengerlist[paxCnt].DateOfBirth);
                                            pax.DateOfBirth = dtbirthDate.ToShortDateString();
                                        }
                                        //if (!string.IsNullOrEmpty(passengerlist[paxCnt].PassportExpiryDT.ToString()))
                                        if (!string.IsNullOrEmpty(Convert.ToString(passengerlist[paxCnt].PassportExpiryDT)))
                                        {
                                            DateTime dtDate = Convert.ToDateTime(passengerlist[paxCnt].PassportExpiryDT);
                                            if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                pax.PassportExpiryDT = String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ((DateTime)dtDate).ToShortDateString());
                                            else
                                                pax.PassportExpiryDT = string.Empty;
                                        }
                                        pax.Nation = passengerlist[paxCnt].Nation;
                                    }
                                }
                            }
                        }
                    }

                    if (Oldpassengerlist != null)
                        Finalpassengerlist = Newpassengerlist.Concat(Oldpassengerlist).ToList();

                    Session["CorpPaxSummaryList"] = Finalpassengerlist;
                    dgPaxSummary.DataSource = Finalpassengerlist;
                    dgPaxSummary.DataBind();
                    if (dgPaxSummary.Items.Count > 0)
                    {
                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                        List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        dgPaxSummary.SelectedIndexes.Add(0);
                        dgPaxSummary.Items[0].Selected = true;
                        GridDataItem item = (GridDataItem)dgPaxSummary.SelectedItems[0];
                        Label lblName = (Label)item.FindControl("lblName");
                        Label lblLegID = (Label)item.FindControl("lblLegID");
                    }
                }
            }
        }

        private List<PassengerSummary> ConvertDataTabletoList(DataTable dt)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<PassengerSummary> paxSummaryList = new List<PassengerSummary>();
                foreach (DataRow row in dt.Rows)
                {
                    PassengerSummary pax = new PassengerSummary();
                    pax.LegId = Convert.ToInt64(row["LegID"].ToString());
                    pax.PaxID = Convert.ToInt64(row["PassengerRequestorID"].ToString());
                    pax.PaxCode = row["Code"].ToString();
                    pax.PaxName = row["Name"].ToString();

                    if (!string.IsNullOrEmpty(row["Postal"].ToString()))
                    {
                        pax.Postal = row["Postal"].ToString();
                    }
                    else
                    {
                        pax.Postal = string.Empty;
                    }

                    if (!string.IsNullOrEmpty(row["Street"].ToString()))
                    {
                        pax.Street = row["Street"].ToString();
                    }
                    else
                    {
                        pax.Street = string.Empty;
                    }

                    if (!string.IsNullOrEmpty(row["StateName"].ToString()))
                    {
                        pax.State = row["StateName"].ToString();
                    }
                    else
                    {
                        pax.State = string.Empty;
                    }

                    if (!string.IsNullOrEmpty(row["City"].ToString()))
                    {
                        pax.City = row["City"].ToString();
                    }
                    else
                    {
                        pax.City = string.Empty;
                    }

                    if (!string.IsNullOrEmpty(row["Purpose"].ToString()))
                    {
                        pax.Purpose = row["Purpose"].ToString();
                    }
                    else
                    {
                        pax.Purpose = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(row["Passport"].ToString()))
                    {
                        pax.Passport = row["Passport"].ToString();
                    }
                    else
                    {
                        pax.Passport = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(row["BillingCode"].ToString()))
                    {
                        pax.BillingCode = row["BillingCode"].ToString();
                    }
                    else
                    {
                        pax.BillingCode = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(row["VisaID"].ToString()))
                    {
                        pax.VisaID = row["VisaID"].ToString();
                    }
                    else
                    {
                        pax.VisaID = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(row["PassportID"].ToString()))
                    {
                        pax.PassportID = row["PassportID"].ToString();
                    }
                    else
                    {
                        pax.PassportID = string.Empty;
                    }
                    //if (!string.IsNullOrEmpty(row["PassportExpiryDT"].ToString()))
                    if (!string.IsNullOrEmpty(Convert.ToString(row["PassportExpiryDT"])))
                    {
                        pax.PassportExpiryDT = row["PassportExpiryDT"].ToString();
                    }
                    else
                    {
                        pax.PassportExpiryDT = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(row["Nation"].ToString()))
                    {
                        pax.Nation = row["Nation"].ToString();
                    }
                    else
                    {
                        pax.Nation = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(row["DateOfBirth"].ToString()))
                    {
                        pax.DateOfBirth = row["DateOfBirth"].ToString();
                    }
                    else
                    {
                        pax.DateOfBirth = string.Empty;
                    }
                    paxSummaryList.Add(pax);
                }
                return paxSummaryList;
            }
        }

        /// <summary>
        /// dgAssignedCrew ItemCommand
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgPaxSummary_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                            if (e.CommandName == "RowClick")
                            {
                                GridDataItem test = (GridDataItem)e.Item;
                                int index = e.Item.ItemIndex;
                                e.Item.Selected = true;
                                hdnPassPax.Value = test.GetDataKeyValue("PaxID").ToString();
                                int legid = Convert.ToInt16(test.GetDataKeyValue("LegID"));
                                List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                                hdnArrivalICao.Value = Preflegs[legid - 1].AAirportID.ToString();

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void dgPaxSummary_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                            foreach (GridDataItem dataItem in dgPaxSummary.Items)
                            {
                                if (dataItem.Selected)
                                {
                                    Label lblPassengerRequestorID = (Label)dataItem.FindControl("lblPassengerRequestorID");
                                    hdnPassPax.Value = System.Web.HttpUtility.HtmlEncode(lblPassengerRequestorID.Text);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void dgPaxSummary_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridPagerItem)
                        {
                            GridPagerItem pager = (GridPagerItem)e.Item;
                            Label lbl = (Label)pager.FindControl("ChangePageSizeLabel");
                            lbl.Visible = false;

                            RadComboBox combo = (RadComboBox)pager.FindControl("PageSizeComboBox");
                            combo.Visible = false;
                        }
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem dataItem = e.Item as GridDataItem;
                            Label lblPassengerRequestorID = (Label)dataItem.FindControl("lblPassengerRequestorID");
                            Label lblLegID = (Label)dataItem.FindControl("lblLegID");

                            //if (e.Item.Selected)
                            //    hdnPassPax.Value = lblPassengerRequestorID.Text;
                            TextBox tbPassport = (TextBox)dataItem.FindControl("tbPassport");
                            HiddenField hdnPassID = (HiddenField)dataItem.FindControl("hdnPassID");
                            HiddenField hdnVisaID = (HiddenField)dataItem.FindControl("hdnVisaID");
                            if (tbPassport != null && hdnPassPax != null && hdnPassPax.Value != null && lblPassengerRequestorID.Text == hdnPassPax.Value && hdnAssignPassLeg.Value != null && lblLegID.Text == hdnAssignPassLeg.Value)
                            {
                                if (Session["SelectedPassport"] != null)
                                    tbPassport.Text = Session["SelectedPassport"].ToString();
                                if (Session["SelectedPassportID"] != null)
                                    hdnPassID.Value = Session["SelectedPassportID"].ToString();
                                if (Session["SelectedVisaID"] != null)
                                    hdnVisaID.Value = Session["SelectedVisaID"].ToString();
                                if (Session["SelectedPrefPaxPassportExpiryDT"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Session["SelectedPrefPaxPassportExpiryDT"].ToString()))
                                    {
                                        DateTime dtDate = Convert.ToDateTime(Session["SelectedPrefPaxPassportExpiryDT"]);
                                        if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                            dataItem["PassportExpiryDT"].Text = System.Web.HttpUtility.HtmlEncode( String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", ((DateTime)dtDate)));
                                        else
                                            dataItem["PassportExpiryDT"].Text = string.Empty;
                                    }
                                }
                                if (Session["SelectedPrefPaxCountryCD"] != null)
                                {
                                    dataItem["Nation"].Text = System.Web.HttpUtility.HtmlEncode( Session["SelectedPrefPaxCountryCD"].ToString());
                                }
                                Session.Remove("SelectedPrefPaxCountryCD");
                                Session.Remove("SelectedPrefPaxPassportExpiryDT");

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void dgPaxSummary_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            List<PassengerSummary> paxSummaryList = new List<PassengerSummary>();
            try
            {
                if (Session["CorpPaxSummaryList"] != null)
                {
                    paxSummaryList = (List<PassengerSummary>)Session["CorpPaxSummaryList"];
                    dgPaxSummary.VirtualItemCount = paxSummaryList.Count;
                    dgPaxSummary.DataSource = paxSummaryList;
                    // dgPaxSummary.DataBind();
                }
            }
            catch (Exception ex)
            { }
        }
        #endregion

        /// <summary>
        /// Method for showing Alert
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Alert_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ViewState["AlertType"] != null)
                        {
                            switch (ViewState["AlertType"].ToString())
                            {
                                case "EmpType_Changed":
                                    break;
                                case "dgAvailablePax_DeleteCommand":
                                    break;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestPAX);
                }
            }
        }

        protected void btnEditTrip_Click(object sender, EventArgs e)
        {
            Master.EditRequestEvent();
        }

    }
}
