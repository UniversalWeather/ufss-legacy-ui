﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CopyRequest.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.CorporateRequest.CopyRequest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Copy CR</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadScriptManager ID="radScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" DateFormat="MM/dd/yyyy" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                    ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var currentTextBox = null;
            var currentDatePicker = null;

            //This method is called to handle the onclick and onfocus client side events for the texbox
            function showPopup(sender, e) {
                //this is a reference to the texbox which raised the event
                //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                //this gets a reference to the datepicker, which will be shown, to facilitate
                //the selection of a date
                var datePicker = $find("<%= RadDatePicker1.ClientID %>");

                //this variable is used to store a reference to the date picker, which is currently
                //active
                currentDatePicker = datePicker;

                //this method first parses the date, that the user entered or selected, and then
                //sets it as a selected date to the picker
                datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));

                //the code lines below show the calendar, which is used to select a date. The showPopup
                //function takes three arguments - the x and y coordinates where to show the calendar, as
                //well as its height, derived from the offsetHeight property of the textbox
                var position = datePicker.getElementPosition(currentTextBox);
                datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
            }

            //this handler is used to set the text of the TextBox to the value of selected from the popup
            function dateSelected(sender, args) {
                if (currentTextBox != null) {
                    //currentTextBox is the currently selected TextBox. Its value is set to the newly selected
                    //value of the picker
                    currentTextBox.value = args.get_newValue();
                }
            }

            //this function is used to parse the date entered or selected by the user
            function parseDate(sender, e) {
                if (currentDatePicker != null) {
                    var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                    var dateInput = currentDatePicker.get_dateInput();
                    var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                    sender.value = formattedDate;
                }
            }
            function tbDate_OnKeyDown(sender, event) {
                if (event.keyCode == 9) {
                    var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                    datePicker.hidePopup();
                    return true;
                }
            }

            function alertCallBackFn(arg) {
                document.getElementById("<%=tbNewDepartureDate.ClientID%>").focus();
            }
           
        </script>
        <script type="text/javascript">

            function openWin(radWin) {
                var url = '';

                if (url != "") {
                    var oBrowserWnd = GetRadWindow().BrowserWindow;
                    setTimeout(function () {
                        oBrowserWnd.radopen(url, radWin);
                    }, 0);
                }
            }

            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            function CloseAndRebind(arg) {
                var win = radalert("Request Copied Successfully.", 330, 100, "Copy Alert", null);
                win.add_close(function () {
                    GetRadWindow().Close();
                    GetRadWindow().BrowserWindow.location.reload();
                });
            }
            function CloseWindow() {
                GetRadWindow().Close();
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCopy">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <div id="DivExternalForm" runat="server">
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table width="100%" cellpadding="0" cellspacing="0" class="border-box">
                        <tr>
                            <td class="tdLabel120">
                                <span class="mnd_text">New Departure Date</span>
                            </td>
                            <td>
                                <asp:Label ID="InjectScript" runat="server"></asp:Label>
                                <asp:TextBox ID="tbNewDepartureDate" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                    onclick="showPopup(this, event);" AutoPostBack="true" onBlur="parseDate(this, event);"
                                    onkeydown="return tbDate_OnKeyDown(this, event);" CssClass="text70" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBox ID="chkAll" runat="server" Text="All"  AutoPostBack="true" OnCheckedChanged="chkAll_OnCheckedChanged"/>
                            </td>
                        </tr>
                         <tr>
                            <td colspan="2">
                                <asp:CheckBox ID="chkLegs" runat="server" Text="Include Legs" AutoPostBack="true" OnCheckedChanged="CRLogisticsCheckbox_OnCheckedChanged" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBox ID="chkIncludeLogistics" runat="server" Text="Include Legs and Logistics"  AutoPostBack="true" OnCheckedChanged="CRLogisticsCheckbox_OnCheckedChanged" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBox ID="chkIncludePassengerManifest" runat="server" Text="Include Legs and PAX Manifest" AutoPostBack="true" OnCheckedChanged="CRLogisticsCheckbox_OnCheckedChanged"  />
                            </td>
                        </tr>                       
                    </table>
                </td>
            </tr>
            <tr>
                <td class="nav-6">
                </td>
            </tr>
            <tr>
                <td align="right">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnCopy" runat="server" Text="Copy" CssClass="button" OnClick="bntCopyNewDeptDate_Click" />
                            </td>
                            <td align="right">
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:CloseWindow();return false;"
                                    CssClass="button" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="tblspace_48">
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
