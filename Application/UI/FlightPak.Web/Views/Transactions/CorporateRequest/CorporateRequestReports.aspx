﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/CorporateRequest.Master"
    AutoEventWireup="true" CodeBehind="CorporateRequestReports.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.CorporateRequest.CorporateRequestReports" %>

<%@ MasterType VirtualPath="~/Framework/Masters/CorporateRequest.Master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js">
    </script>
    <telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadScriptBlock runat="server" ID="scriptBlock">
        <script type="text/javascript">

            function openReport(radWin) {
                var url = '';
                //Company
                if (radWin == "CorAirAppForm") {
                    url = '../../Reports/ReportViewerModule.aspx?xmlFilename=CRCorporateAircraftApprovalForm.xml';
                }
                else if (radWin == "AppAuditSum") {
                    url = '../../Reports/ReportViewerModule.aspx?xmlFilename=CRApprovalAuditSummary.xml';
                }
                else if (radWin == "PassITI") {
                    url = '../../Reports/ReportViewerModule.aspx?xmlFilename=CRPassengerItinerary.xml';
                }
                else if (radWin == "TripITI") {
                    url = '../../Reports/ReportViewerModule.aspx?xmlFilename=CRTripItinerary.xml';
                }                
                var oWnd = radopen(url, "RadCRReportsPopup");
            }
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function handleDropDownEvents(ddlPAXPurpose) {

                //alert("test");
                //alert(ddlPAXPurpose.prevSelectedIndex);
            }
            function openWin(radWin) {
                var url = '';

                if (radWin == "RadRetrievePopup") {
                    url = '/Views/Transactions/CorporateRequest/CorporateRequestSearch.aspx';
                }

                if (radWin == "rdCopyTrip") {
                    url = '/Views/Transactions/CorporateRequest/CopyRequest.aspx';
                }

                if (radWin == "rdHistory") {
                    url = "../../Transactions/CRLogisticsHistory.aspx";
                }
                else if (radWin == "rdChangeQueue") {
                    url = '../../Transactions/CorporateRequest/CorporateChangeQueue.aspx';
                }

                if (url != '') {
                    var oWnd = radopen(url, radWin);
                }
            }



            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }
            // this function is used to display the PaxInfo popup 
            function ShowPaxInfoPopup(ParameterCall) {

                return false;
            }

            // this function is used to display the details of a particular crew
            function ShowPaxDetailsPopup(Paxid) {
                window.radopen("/Views/Transactions/Preflight/PreflightPassengerPopup.aspx?PaxID=" + Paxid, "rdPaxPage");
                return false;
            }

            // this function is used to display the Pax passport visa type popup 
            function ShowPaxPassportPopup(Paxid, LegID) {
                //window.radopen("/Views/Transactions/Preflight/PaxPassportVisa.aspx?PassengerRequestorID=" + Paxid, "rdPaxPassport");
//               
                return false;
            }

            // this function is used to the refresh the currency grid
            function refreshGrid(arg) {

                $find("<%= Master.RadAjaxManagerMaster.ClientID %>").ajaxRequest('Rebind');
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function OnTextFocus(txtPax) {
                document.getElementById("hdnPAXPurposeOnFocus").value = txtPax.value;

            }

            String.prototype.trim = function () {
                return this.replace(/^\s+|\s+$/g, "");
            }

            function allownumbers(e) {
                var key = window.event ? e.keyCode : e.which;
                var keychar = String.fromCharCode(key);
                var reg = new RegExp("[0-9.]")
                if (key == 8) {
                    keychar = String.fromCharCode(key);
                }
                if (key == 13) {
                    key = 8;
                    keychar = String.fromCharCode(key);
                }
                return reg.test(keychar);
            }
            function ShowAirportInfoPopup(Airportid) {
                window.radopen("/Views/Transactions/Preflight/PreflightAirportInfo.aspx?AirportID=" + Airportid, "rdAirportPage");
                return false;
            }

            function RequestStart(sender, args) {
                currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                currentUpdatedControl = "<%= DivExternalForm.ClientID %>";
                //show the loading panel over the updated control
                currentLoadingPanel.show(currentUpdatedControl);
            }
            function ResponseEnd() {
                //hide the loading panel and clean up the global variables
                if (currentLoadingPanel != null)
                    currentLoadingPanel.hide(currentUpdatedControl);
                currentUpdatedControl = null;
                currentLoadingPanel = null;
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadCRReportsPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/CorporateRequest/CorporateRequestReports.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" width="250px" class="reports_new">
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkCorAirAppForm" runat="server" Text="Corporate Aircraft Approval Form"
                                                OnClientClick="javascript:openReport('CorAirAppForm');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkAppAuditSum" runat="server" Text="(Corporate) Approval Audit Summary"
                                                OnClientClick="javascript:openReport('AppAuditSum');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkPassITI" runat="server" Text="Passenger Itinerary" OnClientClick="javascript:openReport('PassITI');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkTripITI" runat="server" Text="Trip Itinerary" OnClientClick="javascript:openReport('TripITI');return false;"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
