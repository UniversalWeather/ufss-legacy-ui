﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/CorporateRequest.Master"
    AutoEventWireup="true" CodeBehind="CorporateRequestLogistics.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.CorporateRequest.CorporateRequestLogistics" %>

<%@ MasterType VirtualPath="~/Framework/Masters/CorporateRequest.Master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">

            var Popup = '';
            function openWinFBO(type) {

                var url = '';
                Popup = type;
                if (type == "DEPART") {
                    url = '/Views/Settings/Logistics/FBOPopup.aspx?IcaoID=' + document.getElementById('<%=hdDepartIcaoID.ClientID%>').value;
                    var oWnd = radopen(url, 'RadFBOCodePopup');
                    oWnd.add_close(RadFBOPopupClose);
                }
                else {
                    url = '/Views/Settings/Logistics/FBOPopup.aspx?IcaoID=' + document.getElementById('<%=hdArriveIcaoID.ClientID%>').value;
                    var oWnd = radopen(url, 'RadFBOCodePopup');
                    oWnd.add_close(RadFBOPopupClose);
                }
            }

            function validateEmptyRadRateTextbox(sender, eventArgs) {
                if (sender.get_textBoxValue().length < 1) {
                    sender.set_value('0.0');
                }
            }

            function alertsubmitCallBackFn(arg) {
                window.location.reload();
            }

            function RedirectPage(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnReload.ClientID%>').click();
                }
            }

            function alertCancelCallBackFn(arg) {
                window.location.reload();
            }


            function confirmSubmitCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnSubmitYes.ClientID%>').click();
                }
            }

            function confirmCancelRequestCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnCancelYes.ClientID%>').click();
                }
            }

            function confirmExcludeOnSubmitMain(arg) {
                var oWnd = radalert("Warning - Trip Departure Has Exceeded Limits Set By Dispatch.<br> Trip Request Cannot Be Submitted To The Change Queue.<br> Please Contact Dispatch To Request Any Additional Changes.", 440, 110, "Request Alert");
                oWnd.add_close(function () { window.location.reload(); });
            }

            function openMaintHotel() {
                var url = '';
                url = '/Views/Settings/Logistics/HotelPopup.aspx?IcaoID=' + document.getElementById('<%=hdArriveIcaoID.ClientID%>').value + '&PeopleType=Crew' + '&HotelCD=' + document.getElementById('<%=tbMaintHotelCode.ClientID%>').value;;
                var oWnd = radopen(url, 'radHotelPopup');
                oWnd.add_close(OnClientMaintHotelClose);
            }

            function openPaxHotel() {
                var url = '';
                url = '/Views/Settings/Logistics/HotelPopup.aspx?IcaoID=' + document.getElementById('<%=hdArriveIcaoID.ClientID%>').value + '&PeopleType=Pax' + '&HotelCD=' + document.getElementById('<%=tbPaxHotelCode.ClientID%>').value;;
                var oWnd = radopen(url, 'radHotelPopup');
                oWnd.add_close(OnClientPaxHotelClose);
            }

            function openCrewHotel() {
                var url = '';
                url = '/Views/Settings/Logistics/HotelPopup.aspx?IcaoID=' + document.getElementById('<%=hdArriveIcaoID.ClientID%>').value + '&PeopleType=Crew' + '&HotelCD=' + document.getElementById('<%=tbCrewHotelCode.ClientID%>').value;;
                var oWnd = radopen(url, 'radHotelPopup');
                oWnd.add_close(OnClientCrewHotelClose);
            }

            var TransPopup = '';
            function openWinTransport(type) {

                var url = '';
                TransPopup = type;
                if (type == "CREW") {
                    url = '/Views/Settings/Logistics/TransportCatalogPopup.aspx?IcaoID=' + document.getElementById('<%=hdArriveIcaoID.ClientID%>').value + '&PeopleType=Crew' + '&TransportCD=' + document.getElementById('<%=tbCrewTransportCode.ClientID%>').value;
                    var oWnd = radopen(url, 'radTransportCodePopup');
                    oWnd.add_close(RadTransportPopupClose);
                }
                else {
                    url = '/Views/Settings/Logistics/TransportCatalogPopup.aspx?IcaoID=' + document.getElementById('<%=hdArriveIcaoID.ClientID%>').value + '&PeopleType=Pax' + '&TransportCD=' + document.getElementById('<%=tbCrewTransportCode.ClientID%>').value;
                    var oWnd = radopen(url, 'radTransportCodePopup');
                    oWnd.add_close(RadTransportPopupClose);
                }
            }


            var CaterinPopup = '';
            function openWinCater(type) {

                var url = '';
                CaterinPopup = type;
                if (type == "DEPART") {
                    url = '/Views/Settings/Logistics/CaterPopup.aspx?IcaoID=' + document.getElementById('<%=hdDepartIcaoID.ClientID%>').value;
                    var oWnd = radopen(url, 'radCateringPopup');
                    oWnd.add_close(RadCaterPopupClose);
                }
                else {
                    url = '/Views/Settings/Logistics/CaterPopup.aspx?IcaoID=' + document.getElementById('<%=hdArriveIcaoID.ClientID%>').value;
                    var oWnd = radopen(url, 'radCateringPopup');
                    oWnd.add_close(RadCaterPopupClose);
                }
            }

            function RadCaterPopupClose(oWnd, args) {

                if (CaterinPopup == 'DEPART') {

                    var combo = $find("<%= tbDeptCateringCode.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=hdDeptCatAirID.ClientID%>").value = arg.AirportID;
                            document.getElementById("<%=hdDeptCateringID.ClientID%>").value = arg.CateringID;
                            document.getElementById("<%=tbDeptCateringCode.ClientID%>").value = arg.CateringCD;
                            document.getElementById("<%=tbDeptCateringName.ClientID%>").value = arg.CateringVendor;
                            if (arg.PhoneNum != null && arg.PhoneNum != "&nbsp;")
                                document.getElementById("<%=tbDeptCateringPhone.ClientID%>").value = arg.PhoneNum;
                            if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                                document.getElementById("<%=tbDeptCateringFax.ClientID%>").value = arg.FaxNum;
                            if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                                document.getElementById("<%=tbDeptCateringEmail.ClientID%>").value = arg.ContactEmail;
                            if (arg.NegotiatedRate != null && arg.NegotiatedRate != "&nbsp;")
                                document.getElementById("<%=tbDeptCateringRate.ClientID%>").value = arg.NegotiatedRate;
                            else
                                document.getElementById("<%=tbDeptCateringRate.ClientID%>").value = "0.00";
                            if (arg.CateringCD != null)
                                document.getElementById("<%=lbcvDeptCateringCode.ClientID%>").innerHTML = "";
                            var step = "DeptCatering_TextChanged";
                            var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }
                        else {
                            document.getElementById("<%=hdDeptCatAirID.ClientID%>").value = "";
                            document.getElementById("<%=hdDeptCateringID.ClientID%>").value = "";
                            document.getElementById("<%=tbDeptCateringCode.ClientID%>").value = "";
                            document.getElementById("<%=tbDeptCateringName.ClientID%>").value = "";
                            document.getElementById("<%=tbDeptCateringPhone.ClientID%>").value = "";
                            document.getElementById("<%=tbDeptCateringFax.ClientID%>").value = "";
                            document.getElementById("<%=tbDeptCateringRate.ClientID%>").value = "";
                            document.getElementById("<%=tbDeptCateringEmail.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }

                }
                if (CaterinPopup == 'ARRIVAL') {
                    var combo = $find("<%= tbArriveCateringCode.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=hdArriveCatAirID.ClientID%>").value = arg.AirportID;
                            document.getElementById("<%=hdArriveCateringID.ClientID%>").value = arg.CateringID;
                            document.getElementById("<%=tbArriveCateringCode.ClientID%>").value = arg.CateringCD;
                            document.getElementById("<%=tbArriveCateringName.ClientID%>").value = arg.CateringVendor;
                            if (arg.PhoneNum != null && arg.PhoneNum != "&nbsp;")
                                document.getElementById("<%=tbArriveCateringPhone.ClientID%>").value = arg.PhoneNum;
                            if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                                document.getElementById("<%=tbArriveCateringFax.ClientID%>").value = arg.FaxNum;
                            if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                                document.getElementById("<%=tbArriveCateringEmail.ClientID%>").value = arg.ContactEmail;
                            if (arg.NegotiatedRate != null && arg.NegotiatedRate != "&nbsp;")
                                document.getElementById("<%=tbArriveCateringRate.ClientID%>").value = arg.NegotiatedRate;
                            else
                                document.getElementById("<%=tbArriveCateringRate.ClientID%>").value = "0.00";
                            if (arg.CateringCD != null)
                                document.getElementById("<%=lbcvArriveCateringCode.ClientID%>").innerHTML = "";
                            var step = "ArriveCatering_TextChanged";
                            var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }
                        else {
                            document.getElementById("<%=hdArriveCatAirID.ClientID%>").value = "";
                            document.getElementById("<%=hdArriveCateringID.ClientID%>").value = "";
                            document.getElementById("<%=tbArriveCateringCode.ClientID%>").value = "";
                            document.getElementById("<%=tbArriveCateringName.ClientID%>").value = "";
                            document.getElementById("<%=tbArriveCateringPhone.ClientID%>").value = "";
                            document.getElementById("<%=tbArriveCateringFax.ClientID%>").value = "";
                            document.getElementById("<%=tbArriveCateringRate.ClientID%>").value = "";
                            document.getElementById("<%=tbArriveCateringEmail.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
            }



            function RadFBOPopupClose(oWnd, args) {

                if (Popup == 'DEPART') {

                    var combo = $find("<%= tbDepartFBO.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=hdDepartFBOAirID.ClientID%>").value = arg.AirportID;
                            document.getElementById("<%=hdDepartFBOID.ClientID%>").value = arg.FBOID;
                            document.getElementById("<%=tbDepartFBO.ClientID%>").value = arg.FBOCD;
                            document.getElementById("<%=tbDepartFBOName.ClientID%>").value = arg.FBOVendor;
                            if (arg.PhoneNUM1 != null && arg.PhoneNUM1 != "&nbsp;")
                                document.getElementById("<%=tbDepartFBOPhone.ClientID%>").value = arg.PhoneNUM1;
                            if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                                document.getElementById("<%=tbDepartFBOFax.ClientID%>").value = arg.FaxNum;
                            if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                                document.getElementById("<%=tbDepartFBOEmail.ClientID%>").value = arg.ContactEmail;
                            if (arg.FBOID != null)
                                document.getElementById("<%=lbcvDepartFBOCode.ClientID%>").innerHTML = "";
                            var step = "DepartFBO_TextChanged";
                            var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }
                        else {
                            document.getElementById("<%=hdDepartFBOAirID.ClientID%>").value = "";
                            document.getElementById("<%=hdDepartFBOID.ClientID%>").value = "";
                            document.getElementById("<%=tbDepartFBO.ClientID%>").value = "";
                            document.getElementById("<%=tbDepartFBOName.ClientID%>").value = "";
                            document.getElementById("<%=tbDepartFBOPhone.ClientID%>").value = "";
                            document.getElementById("<%=tbDepartFBOFax.ClientID%>").value = "";
                            document.getElementById("<%=tbDepartFBOEmail.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
                if (Popup == 'ARRIVAL') {
                    var combo = $find("<%= tbArriveFBO.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=hdArriveFBOAirID.ClientID%>").value = arg.AirportID;
                            document.getElementById("<%=hdArriveFBOID.ClientID%>").value = arg.FBOID;
                            document.getElementById("<%=tbArriveFBO.ClientID%>").value = arg.FBOCD;
                            document.getElementById("<%=tbArriveFBOName.ClientID%>").value = arg.FBOVendor;
                            if (arg.PhoneNUM1 != null && arg.PhoneNUM1 != "&nbsp;")
                                document.getElementById("<%=tbArriveFBOPhone.ClientID%>").value = arg.PhoneNUM1;
                            if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                                document.getElementById("<%=tbArriveFBOFax.ClientID%>").value = arg.FaxNum;
                            if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                                document.getElementById("<%=tbArriveFBOEmail.ClientID%>").value = arg.ContactEmail;
                            if (arg.FBOCD != null)
                                document.getElementById("<%=lbcvArriveFBOCode.ClientID%>").innerHTML = "";
                            var step = "ArriveFBO_TextChanged";
                            var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }
                        else {
                            document.getElementById("<%=hdArriveFBOAirID.ClientID%>").value = "";
                            document.getElementById("<%=hdArriveFBOID.ClientID%>").value = "";
                            document.getElementById("<%=tbArriveFBO.ClientID%>").value = "";
                            document.getElementById("<%=tbArriveFBOName.ClientID%>").value = "";
                            document.getElementById("<%=tbArriveFBOPhone.ClientID%>").value = "";
                            document.getElementById("<%=tbArriveFBOFax.ClientID%>").value = "";
                            document.getElementById("<%=tbArriveFBOEmail.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
            }



            function RadTransportPopupClose(oWnd, args) {

                if (TransPopup == 'CREW') {

                    var combo = $find("<%= tbCrewTransportCode.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=hdCrewTransAirID.ClientID%>").value = arg.AirportID;
                            document.getElementById("<%=hdCrewTransportID.ClientID%>").value = arg.TransportID;
                            document.getElementById("<%=tbCrewTransportCode.ClientID%>").value = arg.TransportCD;
                            document.getElementById("<%=tbCrewTransportName.ClientID%>").value = arg.TransportationVendor;
                            if (arg.PhoneNum != null && arg.PhoneNum != "&nbsp;")
                                document.getElementById("<%=tbCrewTransportPhone.ClientID%>").value = arg.PhoneNUM;
                            if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                                document.getElementById("<%=tbCrewTransportFax.ClientID%>").value = arg.FaxNUM;
                            if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                                document.getElementById("<%=tbCrewTransportEmail.ClientID%>").value = arg.ContactEmail;
                            if (arg.NegotiatedRate != null && arg.NegotiatedRate != "&nbsp;")
                                document.getElementById("<%=tbCrewTransportRate.ClientID%>").value = arg.NegotiatedRate;
                            else
                                document.getElementById("<%=tbCrewTransportRate.ClientID%>").value = "0.00";
                            if (arg.CateringCD != null)
                                document.getElementById("<%=lbcvCrewTransportCode.ClientID%>").innerHTML = "";
                            var step = "CrewTransportCode_TextChanged";
                            var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }
                        else {
                            document.getElementById("<%=hdCrewTransAirID.ClientID%>").value = "";
                            document.getElementById("<%=hdCrewTransportID.ClientID%>").value = "";
                            document.getElementById("<%=tbCrewTransportCode.ClientID%>").value = "";
                            document.getElementById("<%=tbCrewTransportName.ClientID%>").value = "";
                            document.getElementById("<%=tbCrewTransportPhone.ClientID%>").value = "";
                            document.getElementById("<%=tbCrewTransportFax.ClientID%>").value = "";
                            document.getElementById("<%=tbCrewTransportRate.ClientID%>").value = "";
                            document.getElementById("<%=tbCrewTransportEmail.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }

                if (TransPopup == 'PAX') {
                    var combo = $find("<%= tbPaxTransportCode.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=hdPaxTransAirID.ClientID%>").value = arg.AirportID;
                            document.getElementById("<%=hdPaxTransportID.ClientID%>").value = arg.TransportID;
                            document.getElementById("<%=tbPaxTransportCode.ClientID%>").value = arg.TransportCD;
                            document.getElementById("<%=tbPaxTransportName.ClientID%>").value = arg.TransportationVendor;
                            if (arg.PhoneNum != null && arg.PhoneNum != "&nbsp;")
                                document.getElementById("<%=tbPaxTransportPhone.ClientID%>").value = arg.PhoneNUM;
                            if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                                document.getElementById("<%=tbPaxTransportFax.ClientID%>").value = arg.FaxNUM;
                            if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                                document.getElementById("<%=tbPaxTransportEmail.ClientID%>").value = arg.ContactEmail;
                            if (arg.NegotiatedRate != null && arg.NegotiatedRate != "&nbsp;")
                                document.getElementById("<%=tbPaxTransportRate.ClientID%>").value = arg.NegotiatedRate;
                            else
                                document.getElementById("<%=tbPaxTransportRate.ClientID%>").value = "0.00";
                            if (arg.CateringCD != null)
                                document.getElementById("<%=lbcvPaxTransportCode.ClientID%>").innerHTML = "";
                            var step = "PaxTransportCode_TextChanged";
                            var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }
                        else {
                            document.getElementById("<%=hdPaxTransAirID.ClientID%>").value = "";
                            document.getElementById("<%=hdPaxTransportID.ClientID%>").value = "";
                            document.getElementById("<%=tbPaxTransportCode.ClientID%>").value = "";
                            document.getElementById("<%=tbPaxTransportName.ClientID%>").value = "";
                            document.getElementById("<%=tbPaxTransportPhone.ClientID%>").value = "";
                            document.getElementById("<%=tbPaxTransportFax.ClientID%>").value = "";
                            document.getElementById("<%=tbPaxTransportRate.ClientID%>").value = "";
                            document.getElementById("<%=tbPaxTransportEmail.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
            }




            function openWin(radWin) {
                var url = '';

                if (radWin == "rdCopyTrip") {
                    url = '/Views/Transactions/CorporateRequest/CopyRequest.aspx';
                }
                else if (radWin == "RadRetrievePopup") {
                    url = '../../Transactions/CorporateRequest/CorporateRequestSearch.aspx';
                }
                else if (radWin == "rdChangeQueue") {
                    url = '../../Transactions/CorporateRequest/CorporateChangeQueue.aspx';
                }
                else if (radWin == "rdHistory") {
                    url = "../../Transactions/CRLogisticsHistory.aspx";
                }

                if (url != '') {
                    var oWnd = radopen(url, radWin);
                }
            }
            function OpenCQSearch() {
                var oWnd = radopen('../../Transactions/CharterQuote/CharterQuoteSearchAll.aspx', "RadCQSearch");
            }
            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }



            function OnClientDeptCaterClose(oWnd, args) {
                var combo = $find("<%= tbDeptCateringCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=hdDeptCatAirID.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%=hdDeptCateringID.ClientID%>").value = arg.CateringID;
                        document.getElementById("<%=tbDeptCateringCode.ClientID%>").value = arg.CateringCD;
                        document.getElementById("<%=tbDeptCateringName.ClientID%>").value = arg.CateringVendor;
                        if (arg.PhoneNum != null && arg.PhoneNum != "&nbsp;")
                            document.getElementById("<%=tbDeptCateringPhone.ClientID%>").value = arg.PhoneNum;
                        if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                            document.getElementById("<%=tbDeptCateringFax.ClientID%>").value = arg.FaxNum;
                        if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                            document.getElementById("<%=tbDeptCateringEmail.ClientID%>").value = arg.ContactEmail;
                        if (arg.NegotiatedRate != null && arg.NegotiatedRate != "&nbsp;")
                            document.getElementById("<%=tbDeptCateringRate.ClientID%>").value = arg.NegotiatedRate;
                        else
                            document.getElementById("<%=tbDeptCateringRate.ClientID%>").value = "0.00";
                        if (arg.CateringCD != null)
                            document.getElementById("<%=lbcvDeptCateringCode.ClientID%>").innerHTML = "";
                        var step = "DeptCatering_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=hdDeptCatAirID.ClientID%>").value = "";
                        document.getElementById("<%=hdDeptCateringID.ClientID%>").value = "";
                        document.getElementById("<%=tbDeptCateringCode.ClientID%>").value = "";
                        document.getElementById("<%=tbDeptCateringName.ClientID%>").value = "";
                        document.getElementById("<%=tbDeptCateringPhone.ClientID%>").value = "";
                        document.getElementById("<%=tbDeptCateringFax.ClientID%>").value = "";
                        document.getElementById("<%=tbDeptCateringRate.ClientID%>").value = "";
                        document.getElementById("<%=tbDeptCateringEmail.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientArriveCaterClose(oWnd, args) {
                var combo = $find("<%= tbArriveCateringCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=hdArriveCatAirID.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%=hdArriveCateringID.ClientID%>").value = arg.CateringID;
                        document.getElementById("<%=tbArriveCateringCode.ClientID%>").value = arg.CateringCD;
                        document.getElementById("<%=tbArriveCateringName.ClientID%>").value = arg.CateringVendor;
                        if (arg.PhoneNum != null && arg.PhoneNum != "&nbsp;")
                            document.getElementById("<%=tbArriveCateringPhone.ClientID%>").value = arg.PhoneNum;
                        if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                            document.getElementById("<%=tbArriveCateringFax.ClientID%>").value = arg.FaxNum;
                        if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                            document.getElementById("<%=tbArriveCateringEmail.ClientID%>").value = arg.ContactEmail;
                        if (arg.NegotiatedRate != null && arg.NegotiatedRate != "&nbsp;")
                            document.getElementById("<%=tbArriveCateringRate.ClientID%>").value = arg.NegotiatedRate;
                        else
                            document.getElementById("<%=tbArriveCateringRate.ClientID%>").value = "0.00";
                        if (arg.CateringCD != null)
                            document.getElementById("<%=lbcvArriveCateringCode.ClientID%>").innerHTML = "";
                        var step = "ArriveCatering_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=hdArriveCatAirID.ClientID%>").value = "";
                        document.getElementById("<%=hdArriveCateringID.ClientID%>").value = "";
                        document.getElementById("<%=tbArriveCateringCode.ClientID%>").value = "";
                        document.getElementById("<%=tbArriveCateringName.ClientID%>").value = "";
                        document.getElementById("<%=tbArriveCateringPhone.ClientID%>").value = "";
                        document.getElementById("<%=tbArriveCateringFax.ClientID%>").value = "";
                        document.getElementById("<%=tbArriveCateringRate.ClientID%>").value = "";
                        document.getElementById("<%=tbArriveCateringEmail.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }



            function OnClientPaxHotelClose(oWnd, args) {
                var combo = $find("<%= tbPaxHotelCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=hdPaxHotelAirID.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%=hdPaxHotelID.ClientID%>").value = arg.HotelID;
                        document.getElementById("<%=tbPaxHotelCode.ClientID%>").value = arg.HotelCD;
                        document.getElementById("<%=tbPaxHotelName.ClientID%>").value = arg.Name;
                        if (arg.PhoneNum != null && arg.PhoneNum != "&nbsp;")
                            document.getElementById("<%=tbPaxHotelPhone.ClientID%>").value = arg.PhoneNum;
                        if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                            document.getElementById("<%=tbPaxHotelFax.ClientID%>").value = arg.FaxNum;
                        if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                            document.getElementById("<%=tbPaxHotelEmail.ClientID%>").value = arg.ContactEmail;
                        if (arg.NegotiatedRate != null && arg.NegotiatedRate != "&nbsp;")
                            document.getElementById("<%=tbPaxHotelRate.ClientID%>").value = arg.NegociatedRate;
                        else
                            document.getElementById("<%=tbPaxHotelRate.ClientID%>").value = "0.00";
                        if (arg.CateringCD != null)
                            document.getElementById("<%=lbcvPaxHotelCode.ClientID%>").innerHTML = "";
                        var step = "PaxHotelCode_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=hdPaxHotelAirID.ClientID%>").value = "";
                        document.getElementById("<%=hdPaxHotelID.ClientID%>").value = "";
                        document.getElementById("<%=tbPaxHotelCode.ClientID%>").value = "";
                        document.getElementById("<%=tbPaxHotelName.ClientID%>").value = "";
                        document.getElementById("<%=tbPaxHotelPhone.ClientID%>").value = "";
                        document.getElementById("<%=tbPaxHotelFax.ClientID%>").value = "";
                        document.getElementById("<%=tbPaxHotelRate.ClientID%>").value = "";
                        document.getElementById("<%=tbPaxHotelEmail.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCrewHotelClose(oWnd, args) {
                var combo = $find("<%= tbCrewHotelCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=hdCrewHotelAirID.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%=hdCrewHotelID.ClientID%>").value = arg.HotelID;
                        document.getElementById("<%=tbCrewHotelCode.ClientID%>").value = arg.HotelCD;
                        document.getElementById("<%=tbCrewHotelName.ClientID%>").value = arg.Name;
                        if (arg.PhoneNum != null && arg.PhoneNum != "&nbsp;")
                            document.getElementById("<%=tbCrewHotelPhone.ClientID%>").value = arg.PhoneNum;
                        if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                            document.getElementById("<%=tbCrewHotelFax.ClientID%>").value = arg.FaxNum;
                        if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                            document.getElementById("<%=tbCrewHotelEmail.ClientID%>").value = arg.ContactEmail;
                        if (arg.NegotiatedRate != null && arg.NegotiatedRate != "&nbsp;")
                            document.getElementById("<%=tbCrewHotelRate.ClientID%>").value = arg.NegociatedRate;
                        else
                            document.getElementById("<%=tbCrewHotelRate.ClientID%>").value = "0.00";
                        if (arg.CateringCD != null)
                            document.getElementById("<%=lbcvCrewHotelCode.ClientID%>").innerHTML = "";
                        var step = "CrewHotelCode_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=hdCrewHotelAirID.ClientID%>").value = "";
                        document.getElementById("<%=hdCrewHotelID.ClientID%>").value = "";
                        document.getElementById("<%=tbCrewHotelCode.ClientID%>").value = "";
                        document.getElementById("<%=tbCrewHotelName.ClientID%>").value = "";
                        document.getElementById("<%=tbCrewHotelPhone.ClientID%>").value = "";
                        document.getElementById("<%=tbCrewHotelFax.ClientID%>").value = "";
                        document.getElementById("<%=tbCrewHotelRate.ClientID%>").value = "";
                        document.getElementById("<%=tbCrewHotelEmail.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientMaintHotelClose(oWnd, args) {
                var combo = $find("<%= tbMaintHotelCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=hdMaintHotelAirID.ClientID%>").value = arg.AirportID;
                        document.getElementById("<%=hdMaintHotelID.ClientID%>").value = arg.HotelID;
                        document.getElementById("<%=tbMaintHotelCode.ClientID%>").value = arg.HotelCD;
                        document.getElementById("<%=tbMaintHotelName.ClientID%>").value = arg.Name;
                        if (arg.PhoneNum != null && arg.PhoneNum != "&nbsp;")
                            document.getElementById("<%=tbMaintHotelPhone.ClientID%>").value = arg.PhoneNum;
                        if (arg.FaxNum != null && arg.FaxNum != "&nbsp;")
                            document.getElementById("<%=tbMaintHotelFax.ClientID%>").value = arg.FaxNum;
                        if (arg.ContactEmail != null && arg.ContactEmail != "&nbsp;")
                            document.getElementById("<%=tbMaintHotelEmail.ClientID%>").value = arg.ContactEmail;
                        if (arg.NegotiatedRate != null && arg.NegotiatedRate != "&nbsp;")
                            document.getElementById("<%=tbMaintHotelRate.ClientID%>").value = arg.NegociatedRate;
                        else
                            document.getElementById("<%=tbMaintHotelRate.ClientID%>").value = "0.00";
                        if (arg.CateringCD != null)
                            document.getElementById("<%=lbcvMaintHotelCode.ClientID%>").innerHTML = "";
                        var step = "MaintHotelCode_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=hdMaintHotelAirID.ClientID%>").value = "";
                        document.getElementById("<%=hdMaintHotelID.ClientID%>").value = "";
                        document.getElementById("<%=tbMaintHotelCode.ClientID%>").value = "";
                        document.getElementById("<%=tbMaintHotelName.ClientID%>").value = "";
                        document.getElementById("<%=tbMaintHotelPhone.ClientID%>").value = "";
                        document.getElementById("<%=tbMaintHotelFax.ClientID%>").value = "";
                        document.getElementById("<%=tbMaintHotelRate.ClientID%>").value = "";
                        document.getElementById("<%=tbMaintHotelEmail.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function OnClientChangeQueueClose() {
                window.location.reload();
            }

            function ValidateRateFormat(sender, args) {
                var ReturnValue = true;
                var TitleName = "";
                var CallBackFn = Function.createDelegate(sender, function (shouldSubmit) {
                    sender.focus();
                    return ReturnValue;
                });
                var RateValue = sender.value;
                var SplitRateValue;
                if (RateValue != "") {
                    if (RateValue.indexOf(".") != -1) {
                        SplitRateValue = RateValue.split(".");
                        if (SplitRateValue.length > 2) {
                            ReturnValue = false;
                            radalert("You entered: " + RateValue + ". Invalid format. Required Format is NNNNNNNNNN.NN", 360, 50, TitleName, CallBackFn);
                            RateValue = "0.00";
                            args.cancel = true;
                            //return false;
                        }
                        else {

                            if (SplitRateValue[0].length > 11) {
                                ReturnValue = false;
                                radalert("You entered: " + RateValue + ". Invalid format. Required Format is NNNNNNNNNN.NN", 360, 50, TitleName, CallBackFn);
                                RateValue = "0.00";
                                args.cancel = true;
                                //return false;
                            }
                            else if (SplitRateValue[1].length > 2) {
                                ReturnValue = false;
                                radalert("You entered: " + RateValue + ". Invalid format. Required Format is NNNNNNNNNN.NN", 360, 50, TitleName, CallBackFn);
                                RateValue = "0.00";
                                args.cancel = true;
                                //return false;
                            }

                        }
                    }
                    else {
                        if (RateValue.length <= 11) {
                            RateValue = RateValue + ".00";
                        }
                        else {
                            ReturnValue = false;
                            radalert("You entered: " + RateValue + ". Invalid format. Required Format is NNNNNNNNNN.NN", 360, 50, TitleName, CallBackFn);
                            RateValue = "0.00";
                            args.cancel = true;
                            //return false;
                        }
                    }
                }
                else {
                    RateValue = "0.00";
                }
                sender.value = RateValue;
                return ReturnValue;
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadCQSearch" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="../../Transactions/CharterQuote/CharterQuoteSearchAll.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadRetrievePopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/CorporateRequest/CorporateRequestSearch.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadFBOCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Logistics/FBOPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFBOCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Logistics/FBOPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCateringCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Logistics/CaterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCateringPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Logistics/CaterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radTransportCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Logistics/TransportCatalogPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radTransportCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Logistics/TransportCatalogPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radHotelCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Logistics/HotelPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radHotelPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Logistics/HotelPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Width="400px" Height="236px" Title="Copy Trip" NavigateUrl="~/Views/Transactions/CorporateRequest/CopyRequest.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdChangeQueue" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientChangeQueueClose" AutoSize="true" KeepInScreenBounds="true"
                Behaviors="Close" Modal="true" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/CorporateRequest/CorporateChangeQueue.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="History" NavigateUrl="~/Views/Transactions/CRLogisticsHistory.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <div style="width: 712px;">
            <div style="width: 712px; padding: 10px 0px 5px 0px;">
                <asp:Label ID="lbLeg" runat="server" Style="display: none;"></asp:Label>
                <telerik:RadTabStrip Skin="Windows7" OnTabClick="rtsLegs_TabClick" AutoPostBack="true"
                    Width="718px" ID="rtsLegs" runat="server" ReorderTabsOnSelect="true">
                    <Tabs>
                        <telerik:RadTab Text="Leg1" Selected="true">
                        </telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
            </div>
            <div style="width: 710px; float: left;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <fieldset>
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="tdLabel130">
                                            Flight Time (HH:MM)
                                        </td>
                                        <td class="tdLabel150">
                                            <asp:TextBox ID="tbElapsedTime" runat="server" CssClass="text80" Enabled="false"
                                                MaxLength="5" />
                                        </td>
                                        <td class="tdLabel90">
                                            Departure Date
                                        </td>
                                        <td>
                                            <asp:Label ID="lbDepartDate" runat="server" Text="" />
                                        </td>
                                        <td class="tdLabel90">
                                            Departure ICAO
                                        </td>
                                        <td>
                                            <asp:Label ID="lbDepartICAO" runat="server" Text="" />
                                            <asp:HiddenField ID="hdnDepartAirportID" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="tblspace_10">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Distance
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbDistance" runat="server" CssClass="text80" Enabled="false" MaxLength="5" />
                                        </td>
                                        <td>
                                            Arrival Date
                                        </td>
                                        <td>
                                            <asp:Label ID="lbArriveDate" runat="server" Text="" />
                                        </td>
                                        <td>
                                            Arrival ICAO
                                        </td>
                                        <td>
                                            <asp:Label ID="lbArriveICAO" runat="server" Text="" />
                                            <asp:HiddenField ID="hdnArriveAirportID" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="tblspace_10">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="div_712">
                <div style="float: left; width: 356px;">
                    <asp:Panel ID="pnlDepartFBO" runat="server">
                        <fieldset>
                            <legend>Departure FBO</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel50">
                                        <asp:Label ID="lbDepartFBO" runat="server" Text="Code"></asp:Label>
                                    </td>
                                    <td class="tdLabel250">
                                        <asp:TextBox ID="tbDepartFBO" runat="server" CssClass="text40" OnTextChanged="DepartFBO_TextChanged"
                                            onKeyPress="return fnAllowAlphaNumeric(this, event)" MaxLength="4" AutoPostBack="true"></asp:TextBox>
                                        <asp:HiddenField ID="hdDepartFBOID" runat="server" />
                                        <asp:Button ID="btnDepartFBO" OnClientClick="javascript:openWinFBO('DEPART');return false;"
                                            runat="server" CssClass="browse-button" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lbcvDepartFBOCode" runat="server" CssClass="alert-text"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbDepartFBOName" runat="server" Text="Name"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbDepartFBOName" runat="server" CssClass="text230" MaxLength="10"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbDepartFBOPhone" runat="server" Text="Phone">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbDepartFBOPhone" runat="server" CssClass="text230" MaxLength="10">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbDepartFBOFax" runat="server" Text="Fax">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbDepartFBOFax" runat="server" CssClass="text230" MaxLength="10">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbDepartFBOEmail" runat="server" Text="E-mail">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbDepartFBOEmail" runat="server" CssClass="text230" MaxLength="10">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </asp:Panel>
                </div>
                <div style="float: right; width: 356px;">
                    <asp:Panel ID="pnlArriveFBO" runat="server">
                        <fieldset>
                            <legend>Arrival FBO</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel50">
                                        <asp:Label ID="lbArriveFBO" runat="server" Text="Code"></asp:Label>
                                    </td>
                                    <td class="tdLabel250">
                                        <asp:TextBox ID="tbArriveFBO" runat="server" CssClass="text40" OnTextChanged="ArriveFBO_TextChanged"
                                            onKeyPress="return fnAllowAlphaNumeric(this, event)" MaxLength="4" AutoPostBack="true"></asp:TextBox>
                                        <asp:Button ID="btnArriveFBO" CssClass="browse-button" runat="server" OnClientClick="javascript:openWinFBO('ARRIVAL');return false;" />
                                        <asp:HiddenField ID="hdArriveFBOID" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="lbcvArriveFBOCode" runat="server" CssClass="alert-text"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbArriveFBOName" runat="server" Text="Name"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbArriveFBOName" runat="server" CssClass="text230" MaxLength="10"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbArriveFBOPhone" runat="server" Text="Phone">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbArriveFBOPhone" runat="server" CssClass="text230" MaxLength="10">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbArriveFBOFax" runat="server" Text="Fax">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbArriveFBOFax" runat="server" CssClass="text230" MaxLength="10">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbArriveFBOEmail" runat="server" Text="E-mail">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbArriveFBOEmail" runat="server" CssClass="text230" MaxLength="10">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </asp:Panel>
                </div>
            </div>
            <div class="div_712">
                <div style="float: left; width: 356px;">
                    <asp:Panel ID="pnlDeptCatering" runat="server">
                        <fieldset>
                            <legend>Departure Catering</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel52">
                                        <asp:Label ID="lbDeptCateringCode" runat="server" Text="Code">
                                        </asp:Label>
                                    </td>
                                    <td class="tdLabel75">
                                        <asp:TextBox ID="tbDeptCateringCode" runat="server" CssClass="text40" OnTextChanged="DeptCatering_TextChanged"
                                            onKeyPress="return fnAllowAlphaNumeric(this, event)" MaxLength="4" AutoPostBack="true">
                                        </asp:TextBox>
                                        <asp:Button ID="btnDeptCateringCode" CssClass="browse-button" runat="server" OnClientClick="javascript:openWinCater('DEPART');return false;" />
                                        <asp:HiddenField ID="hdDeptCateringID" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbDeptCateringRate" runat="server" Text="Rate">
                                        </asp:Label>
                                    </td>
                                    <td class="pr_radtextbox_117" align="left">
                                        <%--<asp:TextBox ID="tbDeptCateringRate" runat="server" CssClass="text40" MaxLength="14"
                                            onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onblur="ValidateRateFormat(this,event)">
                                        </asp:TextBox>--%>
                                        <telerik:RadNumericTextBox ID="tbDeptCateringRate" runat="server" Type="Currency"
                                            Culture="en-US" ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="10"
                                            Value="0.00" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                        </telerik:RadNumericTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lbcvDeptCateringCode" runat="server" CssClass="alert-text"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbDeptCateringName" runat="server" Text="Name">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbDeptCateringName" runat="server" CssClass="text230" MaxLength="40">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbDeptCateringPhone" runat="server" Text="Phone">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbDeptCateringPhone" runat="server" CssClass="text230" MaxLength="25">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbDeptCateringFax" runat="server" Text="Fax">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbDeptCateringFax" runat="server" CssClass="text230" MaxLength="25">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbDeptCateringEmail" runat="server" Text="E-mail">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbDeptCateringEmail" runat="server" CssClass="text230" MaxLength="100">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:RegularExpressionValidator ID="rgDeptCateringEmail" runat="server" ErrorMessage="Invalid E-mail format"
                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbDeptCateringEmail"
                                            ValidationGroup="Save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </asp:Panel>
                </div>
                <div style="float: right; width: 356px;">
                    <asp:Panel ID="pnlArriveCatering" runat="server">
                        <fieldset>
                            <legend>Arrival Catering</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel52">
                                        <asp:Label ID="lbArriveCateringCode" runat="server" Text="Code">
                                        </asp:Label>
                                    </td>
                                    <td class="tdLabel75">
                                        <asp:TextBox ID="tbArriveCateringCode" runat="server" CssClass="text40" AutoPostBack="true"
                                            OnTextChanged="ArriveCatering_TextChanged" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                            MaxLength="4">
                                        </asp:TextBox>
                                        <asp:Button ID="btnArriveCateringCode" CssClass="browse-button" runat="server" OnClientClick="javascript:openWinCater('ARRIVAL');return false;" />
                                        <asp:HiddenField ID="hdArriveCateringID" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbArriveCateringRate" runat="server" Text="Rate">
                                        </asp:Label>
                                    </td>
                                    <td class="pr_radtextbox_117" align="left">
                                        <%-- <asp:TextBox ID="tbArriveCateringRate" runat="server" CssClass="text40" MaxLength="14"
                                            onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onblur="ValidateRateFormat(this,event)">
                                        </asp:TextBox>--%>
                                        <telerik:RadNumericTextBox ID="tbArriveCateringRate" runat="server" Type="Currency"
                                            Culture="en-US" ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="10"
                                            Value="0.00" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                        </telerik:RadNumericTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lbcvArriveCateringCode" runat="server" CssClass="alert-text"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbArriveCateringName" runat="server" Text="Name">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbArriveCateringName" runat="server" CssClass="text230" MaxLength="40">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbArriveCateringPhone" runat="server" Text="Phone">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbArriveCateringPhone" runat="server" CssClass="text230" MaxLength="25">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbArriveCateringFax" runat="server" Text="Fax">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbArriveCateringFax" runat="server" CssClass="text230" MaxLength="25">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbArriveCateringEmail" runat="server" Text="E-mail">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbArriveCateringEmail" runat="server" CssClass="text230" MaxLength="100">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:RegularExpressionValidator ID="rgArriveCateringemail" runat="server" ErrorMessage="Invalid E-mail format"
                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbArriveCateringEmail"
                                            ValidationGroup="Save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </asp:Panel>
                </div>
            </div>
            <div class="div_712">
                <div style="float: left; width: 356px;">
                    <asp:Panel ID="pnlPaxHotel" runat="server">
                        <fieldset>
                            <legend>PAX Hotel</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel52">
                                        <asp:Label ID="lbPaxHotelCode" runat="server" Text="Code">
                                        </asp:Label>
                                    </td>
                                    <td class="tdLabel75">
                                        <asp:TextBox ID="tbPaxHotelCode" runat="server" CssClass="text40" OnTextChanged="PaxHotelCode_TextChanged"
                                            onKeyPress="return fnAllowAlphaNumeric(this, event)" MaxLength="4" AutoPostBack="true">
                                        </asp:TextBox>
                                        <asp:Button ID="btnPaxHotelCode" CssClass="browse-button" runat="server" OnClientClick="javascript:openPaxHotel();return false;" />
                                        <asp:HiddenField ID="hdPaxHotelID" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbPaxHotelRate" runat="server" Text="Rate">
                                        </asp:Label>
                                    </td>
                                    <td class="pr_radtextbox_117" align="left">
                                        <%--<asp:TextBox ID="tbPaxHotelRate" runat="server" CssClass="text40" MaxLength="14"
                                            onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onblur="ValidateRateFormat(this,event)">
                                        </asp:TextBox>--%>
                                        <telerik:RadNumericTextBox ID="tbPaxHotelRate" runat="server" Type="Currency" Culture="en-US"
                                            ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="10" Value="0.00"
                                            NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                        </telerik:RadNumericTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lbcvPaxHotelCode" runat="server" CssClass="alert-text"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbPaxHotelnName" runat="server" Text="Name">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbPaxHotelName" runat="server" CssClass="text230" MaxLength="40">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbPaxHotelPhone" runat="server" Text="Phone">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbPaxHotelPhone" runat="server" CssClass="text230" MaxLength="25">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbPaxHotelFax" runat="server" Text="Fax">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbPaxHotelFax" runat="server" CssClass="text230" MaxLength="25">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbPaxHotelEmail" runat="server" Text="E-mail">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbPaxHotelEmail" runat="server" CssClass="text230" MaxLength="100">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid E-mail format"
                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbPaxHotelEmail" ValidationGroup="Save"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </asp:Panel>
                </div>
                <div style="float: right; width: 356px;">
                    <asp:Panel ID="pnlPaxTransport" runat="server">
                        <fieldset>
                            <legend>PAX Transport</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel52">
                                        <asp:Label ID="lbPaxTransportCode" runat="server" Text="Code">
                                        </asp:Label>
                                    </td>
                                    <td class="tdLabel75">
                                        <asp:TextBox ID="tbPaxTransportCode" runat="server" CssClass="text40" AutoPostBack="true"
                                            OnTextChanged="PaxTransportCode_TextChanged" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                            MaxLength="4">
                                        </asp:TextBox>
                                        <asp:Button ID="btnPaxTransportCode" CssClass="browse-button" runat="server" OnClientClick="javascript:openWinTransport('PAX');return false;" />
                                        <asp:HiddenField ID="hdPaxTransportID" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbPaxTransportRate" runat="server" Text="Rate">
                                        </asp:Label>
                                    </td>
                                    <td class="pr_radtextbox_117" align="left">
                                        <%-- <asp:TextBox ID="tbPaxTransportRate" runat="server" CssClass="text40" MaxLength="14"
                                            onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onblur="ValidateRateFormat(this,event)">
                                        </asp:TextBox>--%>
                                        <telerik:RadNumericTextBox ID="tbPaxTransportRate" runat="server" Type="Currency"
                                            Culture="en-US" ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="10"
                                            Value="0.00" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                        </telerik:RadNumericTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lbcvPaxTransportCode" runat="server" CssClass="alert-text"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbPaxTransportName" runat="server" Text="Name">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbPaxTransportName" runat="server" CssClass="text230" MaxLength="40">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbPaxTransportPhone" runat="server" Text="Phone">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbPaxTransportPhone" runat="server" CssClass="text230" MaxLength="25">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbPaxTransportFax" runat="server" Text="Fax">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbPaxTransportFax" runat="server" CssClass="text230" MaxLength="25">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbPaxTransportEmail" runat="server" Text="E-mail">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbPaxTransportEmail" runat="server" CssClass="text230" MaxLength="100">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:RegularExpressionValidator ID="rePaxtransportEmail" runat="server" ErrorMessage="Invalid E-mail format"
                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbPaxTransportEmail"
                                            ValidationGroup="Save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </asp:Panel>
                </div>
            </div>
            <div class="div_712">
                <div style="float: left; width: 356px;">
                    <asp:Panel ID="pnlCrewHotel" runat="server">
                        <fieldset>
                            <legend>Crew Hotel</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel52">
                                        <asp:Label ID="lbCrewHotelCode" runat="server" Text="Code">
                                        </asp:Label>
                                    </td>
                                    <td class="tdLabel75">
                                        <asp:TextBox ID="tbCrewHotelCode" runat="server" CssClass="text40" AutoPostBack="true"
                                            OnTextChanged="CrewHotelCode_TextChanged" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                            MaxLength="4">
                                        </asp:TextBox>
                                        <asp:Button ID="btnCrewHotelCode" CssClass="browse-button" runat="server" OnClientClick="javascript:openCrewHotel();return false;" />
                                        <asp:HiddenField ID="hdCrewHotelID" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbCrewHotelRate" runat="server" Text="Rate">
                                        </asp:Label>
                                    </td>
                                    <td class="pr_radtextbox_117" align="left">
                                        <%--<asp:TextBox ID="tbCrewHotelRate" runat="server" CssClass="text40" MaxLength="14"
                                            onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onblur="ValidateRateFormat(this,event)">
                                        </asp:TextBox>--%>
                                        <telerik:RadNumericTextBox ID="tbCrewHotelRate" runat="server" Type="Currency" Culture="en-US"
                                            ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="10" Value="0.00"
                                            NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                        </telerik:RadNumericTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lbcvCrewHotelCode" runat="server" CssClass="alert-text"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbCrewHotelName" runat="server" Text="Name">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbCrewHotelName" runat="server" CssClass="text230" MaxLength="40">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbCrewHotelPhone" runat="server" Text="Phone">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbCrewHotelPhone" runat="server" CssClass="text230" MaxLength="25">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbCrewHotelFax" runat="server" Text="Fax">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbCrewHotelFax" runat="server" CssClass="text230" MaxLength="25">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbCrewHotelEmail" runat="server" Text="E-mail">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbCrewHotelEmail" runat="server" CssClass="text230" MaxLength="100">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:RegularExpressionValidator ID="rgCrewhotelEmail" runat="server" ErrorMessage="Invalid E-mail format"
                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbCrewHotelEmail"
                                            ValidationGroup="Save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </asp:Panel>
                </div>
                <div style="float: right; width: 356px;">
                    <asp:Panel ID="pnlCrewTransport" runat="server">
                        <fieldset>
                            <legend>Crew Transport</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel52">
                                        <asp:Label ID="lbCrewTransportCode" runat="server" Text="Code">
                                        </asp:Label>
                                    </td>
                                    <td class="tdLabel75">
                                        <asp:TextBox ID="tbCrewTransportCode" runat="server" CssClass="text40" AutoPostBack="true"
                                            OnTextChanged="CrewTransportCode_TextChanged" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                            MaxLength="4">
                                        </asp:TextBox>
                                        <asp:Button ID="btnCrewTransportCode" CssClass="browse-button" runat="server" OnClientClick="javascript:openWinTransport('CREW');return false;" />
                                        <asp:HiddenField ID="hdCrewTransportID" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lbCrewTransportRate" runat="server" Text="Rate">
                                        </asp:Label>
                                    </td>
                                    <td class="pr_radtextbox_117" align="left">
                                        <%--<asp:TextBox ID="tbCrewTransportRate" runat="server" CssClass="text40" MaxLength="14"
                                            onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onblur="ValidateRateFormat(this,event)">
                                        </asp:TextBox>--%>
                                        <telerik:RadNumericTextBox ID="tbCrewTransportRate" runat="server" Type="Currency"
                                            Culture="en-US" ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="10"
                                            Value="0.00" NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                        </telerik:RadNumericTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lbcvCrewTransportCode" runat="server" CssClass="alert-text"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbCrewTransportName" runat="server" Text="Name">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbCrewTransportName" runat="server" CssClass="text230" MaxLength="40">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbCrewTransportPhone" runat="server" Text="Phone">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbCrewTransportPhone" runat="server" CssClass="text230" MaxLength="25">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbCrewTransportFax" runat="server" Text="Fax">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbCrewTransportFax" runat="server" CssClass="text230" MaxLength="25">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbCrewTransportEmail" runat="server" Text="E-mail">
                                        </asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="tbCrewTransportEmail" runat="server" CssClass="text230" MaxLength="100">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:RegularExpressionValidator ID="rgCrewTransportEmail" runat="server" ErrorMessage="Invalid E-mail format"
                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbCrewTransportEmail"
                                            ValidationGroup="Save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </asp:Panel>
                </div>
            </div>
            <div class="div_712">
                <div style="float: left; width: 100%">
                    <asp:Panel ID="pnlMaintHotel" runat="server">
                        <fieldset>
                            <legend>Maint Hotel</legend>
                            <table width="100%">
                                <tr>
                                    <td class="tdLabel52">
                                        <asp:Label ID="lbMaintHotelCode" runat="server" Text="Code">
                                        </asp:Label>
                                    </td>
                                    <td class="tdLabel300">
                                        <asp:TextBox ID="tbMaintHotelCode" runat="server" CssClass="text40" AutoPostBack="true"
                                            OnTextChanged="MaintHotelCode_TextChanged" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                            MaxLength="4">
                                        </asp:TextBox>
                                        <asp:Button ID="btnMaintHotelCode" CssClass="browse-button" runat="server" OnClientClick="javascript:openMaintHotel();return false;" />
                                        <asp:HiddenField ID="hdMaintHotelID" runat="server" />
                                    </td>
                                    <td class="tdLabel50">
                                        <asp:Label ID="lbMaintHotelRate" runat="server" Text="Rate">
                                        </asp:Label>
                                    </td>
                                    <td class="pr_radtextbox_117" align="left">
                                        <%--<asp:TextBox ID="tbMaintHotelRate" runat="server" CssClass="text40" MaxLength="14"
                                            onKeyPress="return fnAllowNumericAndChar(this, event,'.')" onblur="ValidateRateFormat(this,event)">
                                        </asp:TextBox>--%>
                                        <telerik:RadNumericTextBox ID="tbMaintHotelRate" runat="server" Type="Currency" Culture="en-US"
                                            ClientEvents-OnBlur="validateEmptyRadRateTextbox" MaxLength="10" Value="0.00"
                                            NumberFormat-DecimalSeparator="." EnabledStyle-HorizontalAlign="Right">
                                        </telerik:RadNumericTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lbcvMaintHotelCode" runat="server" CssClass="alert-text"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbMaintHotelName" runat="server" Text="Name">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbMaintHotelName" runat="server" CssClass="text230" MaxLength="40">
                                        </asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbMaintHotelPhone" runat="server" Text="Phone">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbMaintHotelPhone" runat="server" CssClass="text230" MaxLength="25">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbMaintHotelFax" runat="server" Text="Fax">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbMaintHotelFax" runat="server" CssClass="text230" MaxLength="25">
                                        </asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbMaintHotelEmail" runat="server" Text="E-mail">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbMaintHotelEmail" runat="server" CssClass="text230" MaxLength="100">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:RegularExpressionValidator ID="rgMainthotelEmail" runat="server" ErrorMessage="Invalid E-mail format"
                                            Display="Dynamic" CssClass="alert-text" ControlToValidate="tbMaintHotelEmail"
                                            ValidationGroup="Save" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </asp:Panel>
                </div>
                <div style="float: right; width: 356px;">
                    <asp:Panel ID="Panel2" runat="server">
                    </asp:Panel>
                </div>
            </div>
            <table style="width: 712px; padding: 0px 0 5px 0;">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Passenger Leg Notes</legend>
                            <asp:TextBox ID="tbPaxNotes" CssClass="textarea675" TextMode="MultiLine" runat="server"></asp:TextBox>
                        </fieldset>
                    </td>
                </tr>
            </table>
            <div style="float: right; padding: 0px 1px 0px 0;">
                <table width="718px" cellspacing="6">
                    <tr>
                        <td align="left">
                            <asp:Button ID="btnCancellationReq" runat="server" ToolTip="Cancellation Request"
                                Text="Cancellation Request" OnClick="btnCancellationReq_Click" CssClass="button" />
                            <asp:Button ID="btnSubmitReq" runat="server" ToolTip="Submit Request" Text="Submit Request"
                                OnClick="btnSubmitReq_Click" CssClass="button" />
                            <asp:Button ID="btnAcknowledge" runat="server" ToolTip="Acknowledge" Text="Acknowledge"
                                OnClick="btnAcknowledge_Click" CssClass="button" />
                        </td>
                        <td align="right">
                            <asp:Button ID="btnDeleteTrip" runat="server" ToolTip="Delete Selected Record" Text="Delete Request"
                                OnClick="btnDelete_Click" CssClass="button" />
                            <asp:Button ID="btnEditTrip" runat="server" ToolTip="Edit Selected Record" Text="Edit Request"
                                OnClick="btnEditTrip_Click" CssClass="button" />
                            <asp:Button ID="btnCancel" runat="server" ToolTip="Cancel All Changes" Text="Cancel"
                                OnClick="btnCancel_Click" CssClass="button" />
                            <asp:Button ID="btnReload" runat="server" Text="Button" OnClick="btnReload_Click"
                                Style="display: none;" />
                            <asp:Button ID="btnSave" runat="server" ToolTip="Save Changes" Text="Save" OnClick="btnSave_Click"
                                ValidationGroup="Save" CssClass="button" />
                            <asp:Button ID="btnNext" runat="server" Text="Next" Visible="false" OnClick="btnNext_Click"
                                CssClass="button" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hdDepartFBOAirID" runat="server" />
                <asp:HiddenField ID="hdArriveFBOAirID" runat="server" />
                <asp:HiddenField ID="hdDeptCatAirID" runat="server" />
                <asp:HiddenField ID="hdArriveCatAirID" runat="server" />
                <asp:HiddenField ID="hdPaxTransAirID" runat="server" />
                <asp:HiddenField ID="hdCrewTransAirID" runat="server" />
                <asp:HiddenField ID="hdPaxHotelAirID" runat="server" />
                <asp:HiddenField ID="hdCrewHotelAirID" runat="server" />
                <asp:HiddenField ID="hdMaintHotelAirID" runat="server" />
                <asp:HiddenField ID="hdDepartIcaoID" runat="server" />
                <asp:HiddenField ID="hdArriveIcaoID" runat="server" />
                <asp:HiddenField ID="hdnLegNum" runat="server" />
                <asp:Button ID="btnSubmitYes" runat="server" Text="Button" OnClick="btnSubmitYes_Click"
                    Style="display: none;" />
                <asp:Button ID="btnCancelYes" runat="server" Text="Button" OnClick="btnCancelYes_Click"
                    Style="display: none;" />
            </div>
            <div style="float: left; width: 712px; display: none;">
                <div style="float: left;">
                    <telerik:RadTabStrip ID="RadTabStrip1" runat="server" Skin="Black" ReorderTabsOnSelect="true"
                        Align="Justify">
                        <Tabs>
                            <telerik:RadTab Text="Alert" Value="main" Selected="true">
                            </telerik:RadTab>
                            <telerik:RadTab Text="Recent Activity" Value="legs">
                            </telerik:RadTab>
                            <telerik:RadTab Text="Calendar" Value="Quote On File">
                            </telerik:RadTab>
                        </Tabs>
                    </telerik:RadTabStrip>
                </div>
            </div>
            <div style="float: left; width: 712px; padding: 0px 0 10px 0;">
                <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" Width="100%"
                    Visible="false">
                    <telerik:RadPageView ID="RadPageView1" runat="server">
                        <telerik:RadPanelBar ID="pnlbarcompanyInformation" Width="100%" ExpandAnimation-Type="None"
                            CollapseAnimation-Type="none" runat="server">
                            <Items>
                                <telerik:RadPanelItem runat="server" Expanded="false" Text=" " CssClass="PanelHeaderStyle">
                                    <Items>
                                        <telerik:RadPanelItem>
                                            <ContentTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelBar>
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </div>
        </div>
    </div>
</asp:Content>
