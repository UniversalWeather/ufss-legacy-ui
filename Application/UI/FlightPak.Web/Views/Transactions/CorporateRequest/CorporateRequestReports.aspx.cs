﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Common;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;

namespace FlightPak.Web.Views.Transactions.CorporateRequest
{
    public partial class CorporateRequestReports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ContentPlaceHolder contentPlaceHolder = (ContentPlaceHolder)Master.Master.FindControl("MainContent");

            UserControl ucCRHeader = (UserControl)contentPlaceHolder.FindControl("CorpRequestHeader");
            HtmlTable tbl = (HtmlTable)ucCRHeader.FindControl("tblHeader");
            tbl.Visible = false;

            Panel pnlFloater = (Panel)contentPlaceHolder.FindControl("pnlFloater");
            pnlFloater.Visible = false;

            Label lblLastModified = (Label)ucCRHeader.FindControl("lblLastModified");
            lblLastModified.Visible = false;            
           
            //RadPanelBar pnlTracker = (RadPanelBar)contentPlaceHolder.FindControl("pnlTracker");
            //pnlTracker.Visible = false;

            RadPanelBar pnlException = (RadPanelBar)contentPlaceHolder.FindControl("pnlException");
            pnlException.Visible = false;

            RadPanelBar RadPanelBar1 = (RadPanelBar)contentPlaceHolder.FindControl("RadPanelBar1");
            RadPanelBar1.Visible = false;


            RadPanelBar pnlbarPreflightInfo = (RadPanelBar)contentPlaceHolder.FindControl("pnlbarPreflightInfo");
            pnlbarPreflightInfo.Visible = false;

            //RadPanelBar pnlbarPreflightInfo = (RadPanelBar)contentPlaceHolder.FindControl("pnlbarPreflightInfo");
            //RadPanelItem rpiLegSummary = (RadPanelItem)pnlbarPreflightInfo.FindControl("rpiLegSummary");
            //HtmlTable tblReqPrivate = (HtmlTable)rpiLegSummary.FindControl("tblReqPrivate");
            //tblReqPrivate.Visible = false;
            
            UserControl ucCRSearch = (UserControl)contentPlaceHolder.FindControl("CorpRequestSearch");
            ucCRSearch.Visible = false; 

            //Panel pnlFloater = (Panel)contentPlaceHolder.FindControl("pnlFloater");
            //pnlFloater.Visible = false;
            Session["TabSelect"] = "CorpReq";            
            if (!Page.IsPostBack)
            {
                //LoadData();
            }
        }

   //     private void LoadData()
   //     {
   //         using (CommonService.CommonServiceClient common = new CommonService.CommonServiceClient())
   //         {
   //             var LockData = common.GetAllLocks();
   //             if (LockData.ReturnFlag == true)
   //             {
   //                 gvLock.DataSource = LockData.EntityList.ToList();
   //                 gvLock.DataBind();
   //             }
   //         }
   //     }


   //     protected void gvLock_RowDataBound(object sender, GridViewRowEventArgs e)
   //     {
   //         if (e.Row.RowType == DataControlRowType.Header)
   //         {
   //             LinkButton lnkSelected = (LinkButton)e.Row.FindControl("lnkDeleteSelected");
   //             lnkSelected.Attributes.Add("onclick", "javascript:return " +
   //             "confirm('Are you sure you want to delete lock for these selected records." +
   //             DataBinder.Eval(e.Row.DataItem, "0") + "')");
   //         }

   //         //if (e.Row.RowType == DataControlRowType.DataRow)
   //         //{
   //         //    LinkButton l = (LinkButton)e.Row.FindControl("LinkButton1");
   //         //    l.Attributes.Add("onclick", "javascript:return " +
   //         //    "confirm('Are you sure you want to delete this Lock " +
   //         //    DataBinder.Eval(e.Row.DataItem, "FPLockID") + "')");
   //         //}

   //         if (e.Row.RowType == DataControlRowType.DataRow &&
   //(e.Row.RowState == DataControlRowState.Normal ||
   // e.Row.RowState == DataControlRowState.Alternate))
   //         {
   //             CheckBox chkBxSelect = (CheckBox)e.Row.Cells[1].FindControl("chkBxSelect");
   //             CheckBox chkBxHeader = (CheckBox)this.gvLock.HeaderRow.FindControl("chkBxHeader");
   //             chkBxSelect.Attributes["onclick"] = string.Format
   //                                                    (
   //                                                       "javascript:ChildClick(this,'{0}');",
   //                                                       chkBxHeader.ClientID
   //                                                    );
   //         }


   //     }

   //     protected void gvLock_RowCommand(object sender, GridViewCommandEventArgs e)
   //     {
   //         if (e.CommandName == "Delete")
   //         {
   //             Int64 FPLockID = Convert.ToInt64(e.CommandArgument);
   //             DeleteLockedRecord(FPLockID);
   //         }
   //         if (e.CommandName == "DeleteSelected")
   //         {
   //             foreach (GridViewRow row in gvLock.Rows)
   //             {
   //                 CheckBox checkbox = (CheckBox)row.FindControl("chkBxSelect");

   //                 //Check if the checkbox is checked.
   //                 //value in the HtmlInputCheckBox's Value property is set as the //value of the delete command's parameter.
   //                 if (checkbox.Checked)
   //                 {
   //                     // Retreive the Employee ID
   //                     Int64 FPLockID = Convert.ToInt32(gvLock.DataKeys[row.RowIndex].Value);
   //                     using (CommonService.CommonServiceClient common = new CommonService.CommonServiceClient())
   //                     {
   //                         var DeleteRecord = common.DeleteLock(FPLockID);
   //                     }
   //                 }
   //             }
   //             LoadData();
   //         }

   //     }

   //     protected void gvLock_RowDeleting(object sender, GridViewDeleteEventArgs e)
   //     {
   //         //  int FPLockID = (int)gvLock.DataKeys[e.RowIndex].Value;
   //         //DeleteRecordByID(FPLockID);
   //     }

   //     private void DeleteLockedRecord(Int64 FPLockID)
        //{
        //    using (CommonService.CommonServiceClient common = new CommonService.CommonServiceClient())
        //    {
        //        var DeleteRecord = common.DeleteLock(FPLockID);
        //        if (DeleteRecord.ReturnFlag == true)
        //        {
        //            LoadData();
        //        }
        //    }
        //}

    }
}