﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/CorporateRequest.Master"
    AutoEventWireup="true" CodeBehind="CorporateRequestPax.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.CorporateRequest.CorporateRequestPax" %>

<%@ MasterType VirtualPath="~/Framework/Masters/CorporateRequest.Master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadScriptBlock runat="server" ID="scriptBlock">
        <script type="text/javascript">
                <!--

            function confirmDeletePaxCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnDeletePaxYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnDeletePaxNo.ClientID%>').click();
                }
            }

            function OnClientChangeQueueClose() {
                window.location.reload();
            }

            function RedirectPage(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnReload.ClientID%>').click();
                }
            }

            function alertsubmitCallBackFn(arg) {
                window.location.reload();
            }

            function confirmSubmitCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnSubmitYes.ClientID%>').click();
                }
            }

            function confirmCancelRequestCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnCancelYes.ClientID%>').click();
                }
            }


            function alertCancelCallBackFn(arg) {
                window.location.reload();
            }

            function confirmPaxRemoveCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnPaxRemoveYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnPaxRemoveNo.ClientID%>').click();
                }
            }

            function confirmExcludeOnSubmitMain(arg) {
                var oWnd = radalert("Warning - Trip Departure Has Exceeded Limits Set By Dispatch.<br> Trip Request Cannot Be Submitted To The Change Queue.<br> Please Contact Dispatch To Request Any Additional Changes.", 440, 110, "Request Alert");
                oWnd.add_close(function () { window.location.reload(); });
            }

            function confirmPaxNextCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnPaxNextYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnPaxNextNo.ClientID%>').click();
                }
            }

            function alertPaxCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnAlert.ClientID%>').click();
                }
            }
            //            function onRowDropping(sender, args) {
            //                if (sender.get_id() == "<%=dgAvailablePax.ClientID %>") {
            //                    var node = args.get_destinationHtmlElement();
            //                    if (!isChildOf('<%=dgAvailablePax.ClientID %>', node)) {
            //                        args.set_cancel(true);
            //                    }
            //                }
            //                else {
            //                    var node = args.get_destinationHtmlElement();
            //                    if (!isChildOf('trashCan', node)) {
            //                        args.set_cancel(true);
            //                    }
            //                    else {
            //                        if (confirm("Are you sure you want to delete this order?"))
            //                            args.set_destinationHtmlElement($get('trashCan'));
            //                        else
            //                            args.set_cancel(true);
            //                    }
            //                }
            //            }

            function isChildOf(parentId, element) {
                while (element) {
                    if (element.id && element.id.indexOf(parentId) > -1) {
                        return true;
                    }
                    element = element.parentNode;
                }
                return false;
            }
            function maxLength(field, maxChars) {
                if (field.value.length >= maxChars) {
                    event.returnValue = false;
                    return false;
                }
            }

            function openWinTransport(type) {
                var url = '';
                if (type == "DEPART") {
                    url = '/Views/Settings/Logistics/TransportCatalogPopup.aspx?IcaoID=' + document.getElementById('<%=hdnDepartIcao.ClientID%>').value;
                    var oWnd = radopen(url, 'radTransportCodePopup');
                    oWnd.add_close(OnClientTransportClose);
                }
                else {
                    url = '/Views/Settings/Logistics/TransportCatalogPopup.aspx?IcaoID=' + document.getElementById('<%=hdnArrivalICao.ClientID%>').value;
                    var oWnd = radopen(url, 'radTransportCodePopup');
                    oWnd.add_close(OnClientArriveTransportClose);
                }

            }

            function maxLengthPaste(field, maxChars) {
                event.returnValue = false;
                if ((field.value.length + window.clipboardData.getData("Text").length) > maxChars) {
                    return false;
                }
                event.returnValue = true;
            }

            function checknumericValue(myfield) {
                var postbackval = true;
                if (myfield.value.length > 0) {
                    if (myfield.value.indexOf(".") >= 0) {
                        if (myfield.value.indexOf(".") > 0) {
                            var strarr = myfield.value.split(".");
                            if (strarr[0].length > 3) {
                                alert("Invalid format, Required format is ###.## ");
                                myfield.value = "0.00";
                                myfield.focus();
                                postbackval = false;

                            }
                            if (strarr[1].length > 2) {
                                alert("Invalid format, Required format is ###.## ");
                                myfield.value = "0.00";
                                myfield.focus();
                                postbackval = false;
                            }
                        }
                    }
                    else {
                        if (myfield.value.length > 3) {
                            alert("Invalid format, Required format is ###.## ");
                            myfield.value = "0.00";
                            myfield.focus();
                            postbackval = false;
                        }
                    }
                }

                var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                ajaxManager.ajaxRequest("tbPaxHotelRate_Text_Changed");
                return postbackval;
            }

            function checkMaxcap(myfield) {
                var postbackval = true;
                if (myfield.value.length > 0) {
                    if (myfield.value.indexOf(".") >= 0) {
                        if (myfield.value.indexOf(".") > 0) {
                            var strarr = myfield.value.split(".");
                            if (strarr[0].length > 7) {
                                alert("Invalid format, Required format is #######.## ");
                                myfield.value = "0.00";
                                myfield.focus();
                                postbackval = false;

                            }
                            if (strarr[1].length > 2) {
                                alert("Invalid format, Required format is #######.## ");
                                myfield.value = "0.00";
                                myfield.focus();
                                postbackval = false;
                            }
                        }
                    }
                    else {
                        if (myfield.value.length > 7) {
                            alert("Invalid format, Required format is #######.## ");
                            myfield.value = "0.00";
                            myfield.focus();
                            postbackval = false;
                        }
                    }
                }

                var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                ajaxManager.ajaxRequest("tbMaxcapamt_Text_Changed");
                return postbackval;
            }

            function checkTransRatenumericValue(myfield) {
                var postbackval = true;
                if (myfield.value.length > 0) {
                    if (myfield.value.indexOf(".") >= 0) {
                        if (myfield.value.indexOf(".") > 0) {
                            var strarr = myfield.value.split(".");
                            if (strarr[0].length > 3) {
                                alert("Invalid format, Required format is ###.## ");
                                myfield.value = "0.00";
                                myfield.focus();
                                postbackval = false;

                            }
                            if (strarr[1].length > 2) {
                                alert("Invalid format, Required format is ###.## ");
                                myfield.value = "0.00";
                                myfield.focus();
                                postbackval = false;
                            }
                        }
                    }
                    else {
                        if (myfield.value.length > 3) {
                            alert("Invalid format, Required format is ###.## ");
                            myfield.value = "0.00";
                            myfield.focus();
                            postbackval = false;
                        }
                    }
                }

                var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                ajaxManager.ajaxRequest("tbArriveRate_Text_Changed");
                return postbackval;
            }
            function fnAllowNumericAndOneDecimal(fieldname, myfield, e) {
                var key;
                var keychar;
                var allowedString = "0123456789" + ".";

                if (window.event)
                    key = window.event.keyCode;
                else if (e)
                    key = e.which;
                else
                    return true;
                keychar = String.fromCharCode(key);

                if (keychar == ".") {

                    if (myfield.value.indexOf(keychar) > -1) {
                        return false;
                    }
                }

                if (fieldname == "override") {
                    if (myfield.value.length > 0) {
                        if (myfield.value.indexOf(".") > myfield.value.length - 1)
                            return false;
                    }
                }
                // control keys
                if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
                    return true;
                // numeric values and allow slash("/")
                else if ((allowedString).indexOf(keychar) > -1)
                    return true;
                else
                    return false;
            }        

                    -->
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function handleDropDownEvents(ddlPAXPurpose) {

                //alert("test");
                //alert(ddlPAXPurpose.prevSelectedIndex);
            }
            function openWin(radWin) {
                var url = '';

                if (radWin == "RadRetrievePopup") {
                    url = '/Views/Transactions/CorporateRequest/CorporateRequestSearch.aspx';
                }

                if (radWin == "rdCopyTrip") {
                    url = '/Views/Transactions/CorporateRequest/CopyRequest.aspx';
                }

                if (radWin == "rdHistory") {
                    url = "../../Transactions/CRLogisticsHistory.aspx";
                }
                else if (radWin == "rdChangeQueue") {
                    url = '../../Transactions/CorporateRequest/CorporateChangeQueue.aspx';
                }

                if (url != '') {
                    var oWnd = radopen(url, radWin);
                }
            }



            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }
            // this function is used to display the PaxInfo popup 
            function ShowPaxInfoPopup(ParameterCall) {
                if (ParameterCall == 'insert') {
                    document.getElementById('<%=hdnInsert.ClientID%>').value = "true";
                }
                else
                    document.getElementById('<%=hdnInsert.ClientID%>').value = "false";
                var oWnd = radopen("/Views/Transactions/Preflight/PaxInfoPopup.aspx?ShowFillPopUp=TRUE", "radPaxInfoPopup"); // Added for Preflight Pax Tab
                return false;
            }

            // this function is used to display the details of a particular crew
            function ShowPaxDetailsPopup(Paxid) {
                window.radopen("/Views/Transactions/Preflight/PreflightPassengerPopup.aspx?PaxID=" + Paxid, "rdPaxPage");
                return false;
            }

            // this function is used to display the Pax passport visa type popup 
            function ShowPaxPassportPopup(Paxid, LegID, PaxName) {
                //window.radopen("/Views/Transactions/Preflight/PaxPassportVisa.aspx?PassengerRequestorID=" + Paxid, "rdPaxPassport");
                window.radopen("/Views/Transactions/CorporateRequest/PaxPassportVisa.aspx?PassengerRequestorID=" + Paxid + "&PaxLeg=" + LegID + "&PaxName=" + PaxName, "radPaxInfoPopup");
                document.getElementById("<%=hdnPassPax.ClientID%>").value = Paxid;
                document.getElementById('<%=hdnAssignPassLeg.ClientID%>').value = LegID;
                return false;
            }

            // this function is used to the refresh the currency grid
            function refreshGrid(arg) {

                $find("<%= Master.RadAjaxManagerMaster.ClientID %>").ajaxRequest('Rebind');
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function OnTextFocus(txtPax) {
                document.getElementById("hdnPAXPurposeOnFocus").value = txtPax.value;

            }

            String.prototype.trim = function () {
                return this.replace(/^\s+|\s+$/g, "");
            }

            function allownumbers(e) {
                var key = window.event ? e.keyCode : e.which;
                var keychar = String.fromCharCode(key);
                var reg = new RegExp("[0-9.]")
                if (key == 8) {
                    keychar = String.fromCharCode(key);
                }
                if (key == 13) {
                    key = 8;
                    keychar = String.fromCharCode(key);
                }
                return reg.test(keychar);
            }
            function ShowAirportInfoPopup(Airportid) {
                window.radopen("/Views/Transactions/Preflight/PreflightAirportInfo.aspx?AirportID=" + Airportid, "rdAirportPage");
                return false;
            }

            function RequestStart(sender, args) {
                currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                currentUpdatedControl = "<%= DivExternalForm.ClientID %>";
                //show the loading panel over the updated control
                currentLoadingPanel.show(currentUpdatedControl);
            }
            function ResponseEnd() {
                //hide the loading panel and clean up the global variables
                if (currentLoadingPanel != null)
                    currentLoadingPanel.hide(currentUpdatedControl);
                currentUpdatedControl = null;
                currentLoadingPanel = null;
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript">
            function refreshGrid(arg) {
                if (!arg) {
                    $find("<%= Master.RadAjaxManagerMaster.ClientID %>").ajaxRequest("Rebind");
                }
                else {
                    if (document.getElementById('<%=hdnInsert.ClientID%>').value == "true")
                        $find("<%= Master.RadAjaxManagerMaster.ClientID %>").ajaxRequest("RebindAndNavigateInsert");
                    else
                        $find("<%= Master.RadAjaxManagerMaster.ClientID %>").ajaxRequest("RebindAndNavigate");
                }
            }
         
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="false" Width="960px" Height="430px" KeepInScreenBounds="true" Modal="true"
                ReloadOnShow="true" Behaviors="Close" VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <%--<telerik:RadWindow ID="rdPaxPassport" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" ReloadOnShow="false" Behaviors="Close"
                VisibleStatusbar="false">
            </telerik:RadWindow>--%>
            <telerik:RadWindow ID="RadRetrievePopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/CorporateRequest/CorporateRequestSearch.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Width="400px" Height="236px" Title="Copy Trip" NavigateUrl="~/Views/Transactions/CorporateRequest/CopyRequest.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdAirportPage" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" AutoSize="true" Modal="true" ReloadOnShow="false" Behaviors="Close"
                VisibleStatusbar="false" Title="Airport Information">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdPaxPage" runat="server" OnClientResizeEnd="GetDimensions"
                Width="830px" Height="550px" KeepInScreenBounds="true" Modal="true" ReloadOnShow="false"
                Behaviors="Close" VisibleStatusbar="false" Title="Passenger/Requestor">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="History" NavigateUrl="~/Views/Transactions/CRLogisticsHistory.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdChangeQueue" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientChangeQueueClose" AutoSize="true" KeepInScreenBounds="true"
                Behaviors="Close" Modal="true" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/CorporateRequest/CorporateChangeQueue.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <asp:HiddenField ID="hdnCtryID" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="hdnMetroID" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="hdnAssignPassLeg" runat="server"></asp:HiddenField>
                    <input type="hidden" id="hdnPAXPurposeOnFocus" value="0" />
                    <asp:HiddenField ID="hdnPaxNotes" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="hdnLeg1" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="hdnPassPax" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="hdnStreet" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="hdnCity" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="hdnPostal" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="hdnState" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="hdnHotelID" runat="server" />
                    <asp:HiddenField ID="hdnHotelAirID" runat="server" />
                    <asp:HiddenField ID="hdnTransportID" runat="server" />
                    <asp:HiddenField ID="hdnTransAirID" runat="server" />
                    <asp:HiddenField ID="hdnArriveTransportID" runat="server" />
                    <asp:HiddenField ID="hdnArriveTransAirID" runat="server" />
                    <asp:HiddenField ID="hdnArrivalICao" runat="server" />
                    <asp:HiddenField ID="hdnDepartIcao" runat="server" />
                    <asp:HiddenField ID="hdnLeg1p1" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="hdnLeg1a1" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="hdnLeg2p2" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="hdnLeg2a2" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg3p3" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg3a3" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg4p4" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg4a4" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg5p5" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg5a5" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg6p6" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg6a6" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg7p7" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg7a7" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg8p8" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg8a8" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg9p9" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg9a9" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg10p10" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg10a10" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg11p11" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg11a11" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg12p12" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg12a12" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg13p13" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg13a13" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg14p14" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg14a14" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg15p15" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg15a15" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg16p16" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg16a16" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg17p17" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg17a17" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg18p18" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg18a18" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg19p19" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg19a19" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg20p20" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg20a20" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg21p21" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg21a21" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg22p22" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg22a22" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg23p23" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg23a23" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg24p24" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg24a24" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg25p25" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnLeg25a25" runat="server" ClientIDMode="Static" />
                    <asp:Panel ID="pnlPax" runat="server" Visible="true">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left">
                                    <div class="nav-space">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" class="box_sc" width="100%">
                                        <tr>
                                            <td align="left">
                                                <table width="100%" class="nav_bg_preflight">
                                                    <tr>
                                                        <td align="left" class="tdLabel180">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox runat="server" CssClass="SearchBox_sc_crew" AutoPostBack="true" OnTextChanged="Search_Click"
                                                                            ID="SearchBox"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button runat="server" CssClass="searchsubmitbutton" CausesValidation="false"
                                                                            OnClick="Search_Click" ID="Submit1" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td align="left">
                                                            <asp:LinkButton ID="lnkPaxRoaster" runat="server" CausesValidation="false" OnClientClick="javascript:return ShowPaxInfoPopup('all');"
                                                                Text="PAX Table" CssClass="link_small"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" colspan="2" class="srch_category">
                                                            (Search by Passenger Code)
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_5">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" class="box1">
                                                    <tr>
                                                        <td>
                                                            <fieldset>
                                                                <legend>PAX</legend>
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="right">
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td class="tdLabel60">
                                                                                        <%--All Legs--%>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="ddlFlightPurpose" runat="server" AppendDataBoundItems="true"
                                                                                            AutoPostBack="true" OnSelectedIndexChanged="FlightPurpose_SelectedIndexChanged">
                                                                                            <asp:ListItem Selected="True" Text="Select" Value=""></asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                    <td align="right">
                                                                                        <asp:Button ID="btnAddremoveColumns" runat="server" Visible="false" CssClass="button"
                                                                                            Text="+/- Columns" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="nav-6">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <telerik:RadGrid ID="dgAvailablePax" runat="server" OnNeedDataSource="dgAvailablePax_BindData"
                                                                                EnableAJAX="True" AutoGenerateColumns="false" OnSelectedIndexChanged="dgAvailablePax_SelectedIndexChanged"
                                                                                OnItemCommand="dgAvailablePax_ItemCommand" OnItemDataBound="dgAvailablePax_ItemDataBound"
                                                                                Width="680px" Height="250px" AllowMultiRowSelection="true" HeaderStyle-HorizontalAlign="Center"
                                                                                AllowPaging="false">
                                                                                <MasterTableView DataKeyNames="OrderNUM,Code,PassengerRequestorID" CommandItemDisplay="None"
                                                                                    AllowFilteringByColumn="false" ShowFooter="true" ItemStyle-HorizontalAlign="Left"
                                                                                    AllowPaging="false" AllowSorting="false">
                                                                                    <SortExpressions>
                                                                                        <telerik:GridSortExpression FieldName="OrderNUM" SortOrder="Ascending" />
                                                                                    </SortExpressions>
                                                                                    <Columns>
                                                                                        <telerik:GridBoundColumn UniqueName="PassengerRequestorID" SortExpression="PassengerRequestorID"
                                                                                            HeaderText="PassengerRequestorID" DataField="PassengerRequestorID" Display="false" />
                                                                                        <telerik:GridTemplateColumn HeaderText="Code" CurrentFilterFunction="StartsWith"
                                                                                            FilterDelay="4000" ShowFilterIcon="false" UniqueName="Code" AllowFiltering="false"
                                                                                            HeaderStyle-Width="60px">
                                                                                            <ItemTemplate>
                                                                                                <asp:LinkButton ID="lnkPaxCode" runat="server" Text='<%# Eval("Code") %>' CssClass="tdtext100"
                                                                                                    CausesValidation="false" OnClientClick='<%#Eval("PassengerRequestorID", "return ShowPaxDetailsPopup(\"{0}\")")%>'>
                                                                                                </asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:Label ID="lblBlockedSeats" runat="server" Text="Blocked Seats"></asp:Label>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridBoundColumn UniqueName="Name" HeaderText="Name" DataField="Name" HeaderStyle-Width="220px" />
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg1" UniqueName="Leg1" Display="false" HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg1" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg1PaxHeader" class="small_text" runat="server" ClientIDMode="Static"
                                                                                                                Text="PAX:"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg1p1" ClientIDMode="Static" runat="server" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg1AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg1a1" ClientIDMode="Static" runat="server" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg1:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo1" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg1Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg1Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_1_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg1" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg1_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_1" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg1" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg1_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg1" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg2" UniqueName="Leg2" Display="false" HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg2" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg2PaxHeader" ClientIDMode="Static" runat="server" Text="PAX:"
                                                                                                                class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg2p2" runat="server" ClientIDMode="Static" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg2AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg2a2" runat="server" ClientIDMode="Static" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg2:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo2" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg2Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg2Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_2_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg2" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg2_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_2" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg2" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg2_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg2" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg3" UniqueName="Leg3" Display="false" HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg3" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg3PaxHeader" ClientIDMode="Static" runat="server" Text="PAX:"
                                                                                                                class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg3p3" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg3AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg3a3" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg3:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo3" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg3Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg3Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_3_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg3" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg3_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_3" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg3" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg3_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg3" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg4" UniqueName="Leg4" Display="false" HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg4" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg4PaxHeader" ClientIDMode="Static" runat="server" Text="PAX:"
                                                                                                                class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg4p4" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg4AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg4a4" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg4:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo4" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg4Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg4Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_4_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg4" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg4_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_4" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg4" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg4_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg4" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg5" UniqueName="Leg5" Display="false" HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg5" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg5PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg5p5" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg5AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg5a5" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg5:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo5" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg5Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg5Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_5_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg5" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg5_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_5" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg5" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg5_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg5" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg6" UniqueName="Leg6" Display="false" HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg6" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg6PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg6p6" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg6AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg6a6" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg6:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo6" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg6Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg6Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_6_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg6" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg6_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_6" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg6" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg6_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg6" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg7" UniqueName="Leg7" Display="false" HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg7" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg7PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg7p7" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg7AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg7a7" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg7:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo7" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg7Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg7Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_7_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg7" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg7_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_7" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg7" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg7_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg7" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg8" UniqueName="Leg8" Display="false" HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg8" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg8PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg8p8" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg8AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg8a8" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg8:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo8" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg8Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg8Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_8_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg8" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg8_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_8" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg8" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg8_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg8" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg9" UniqueName="Leg9" Display="false" HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg9" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg9PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg9p9" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg9AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg9a9" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg9:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo9" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg9Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg9Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_9_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg9" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg9_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_9" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg9" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg9_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg9" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg10" UniqueName="Leg10" Display="false"
                                                                                            HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg10" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg10PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg10P10" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg10AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg10a10" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg10:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo10" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg10Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg10Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_10_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg10" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg10_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_10" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg10" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg10_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg10" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg11" UniqueName="Leg11" Display="false"
                                                                                            HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg11" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg11PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg11p11" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg11AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg11a11" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg11:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo11" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg11Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg11Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_11_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg11" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg11_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_11" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg11" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg11_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg11" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg12" UniqueName="Leg12" Display="false"
                                                                                            HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg12" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg12PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg12p12" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg12AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg12a12" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg12:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo12" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg12Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg12Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_12_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg12" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg12_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_12" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg12" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg12_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg12" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg13" UniqueName="Leg13" Display="false"
                                                                                            HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg13" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg13PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg13p13" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg13AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg13a13" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg13:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo13" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg13Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg13Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_13_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg13" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg13_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_13" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg13" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg13_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg13" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg14" UniqueName="Leg14" Display="false"
                                                                                            HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg14" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg14PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg14p14" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg14AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg14a14" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg14:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo14" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg14Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg14Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_14_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg14" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg14_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_14" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg14" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg14_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg14" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg15" UniqueName="Leg15" Display="false"
                                                                                            HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg15" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg15PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg15p15" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg15AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg15a15" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg15:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo15" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg15Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg15Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_15_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg15" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg15_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_15" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg15" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg15_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg15" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg16" UniqueName="Leg16" Display="false"
                                                                                            HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg16" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg16PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg16p16" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg16AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg16a16" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg16:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo16" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg16Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg16Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_16_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg16" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg16_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_16" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg16" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg16_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg16" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg17" UniqueName="Leg17" Display="false"
                                                                                            HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg17" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg17PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg17p17" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg17AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg17a17" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg17:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo17" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg17Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg17Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_17_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg17" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg17_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_17" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg17" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg17_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg17" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg18" UniqueName="Leg18" Display="false"
                                                                                            HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg18" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg18PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg18p18" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg18AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg18a18" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg18:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo18" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg18Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg18Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_18_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg18" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg18_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_18" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg18" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg18_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg18" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Le19" UniqueName="Leg19" Display="false"
                                                                                            HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg19" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg19PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg19p19" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg19AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg19a19" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg19:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo19" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg19Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg19Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_19_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg19" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg19_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_19" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg19" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg19_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg19" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg20" UniqueName="Leg20" Display="false"
                                                                                            HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg20" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg20PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg20p20" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg20AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg20a20" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg20:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo20" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg20Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg20Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_20_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg20" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg20_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_20" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg20" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg20_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg20" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg21" UniqueName="Leg21" Display="false"
                                                                                            HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg21" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg21PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg21p21" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg21AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg21a21" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg21:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo21" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg21Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg21Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_21_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg21" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg21_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_21" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg21" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg21_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg21" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg22" UniqueName="Leg22" Display="false"
                                                                                            HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg22" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg22PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg22p22" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg22AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg22a22" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg22:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo22" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg22Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg22Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_22_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg22" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg22_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_22" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg22" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg22_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg22" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg23" UniqueName="Leg23" Display="false"
                                                                                            HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg23" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg23PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg23p23" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg23AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg23a23" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg23:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo23" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg23Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg23Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_23_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg23" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg23_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_23" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg23" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg23_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg23" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg24" UniqueName="Leg24" Display="false"
                                                                                            HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg24" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg24PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg24p24" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg24AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg24a24" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg24:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo24" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg24Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg24Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_24_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg24" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg24_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_24" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg24" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg24_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg24" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn HeaderText="Leg25" UniqueName="Leg25" Display="false"
                                                                                            HeaderStyle-Width="160px">
                                                                                            <HeaderTemplate>
                                                                                                <table id="tblLeg25" cellpadding="1" cellspacing="1" width="160px">
                                                                                                    <tr>
                                                                                                        <td class="tdLabel60" align="left">
                                                                                                            <asp:Label ID="lblLeg25PaxHeader" runat="server" Text="PAX:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg25p25" runat="server" ClientIDMode="Static" class="small_text">10</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg25AvailableHeader" runat="server" Text="Available:" class="small_text"></asp:Label>
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeg25a25" runat="server" ClientIDMode="Static" class="small_text">5</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" class="small_text">
                                                                                                            Leg25:
                                                                                                        </td>
                                                                                                        <td align="left">
                                                                                                            <asp:Label ID="lblLeginfo25" runat="server" Text=""></asp:Label>
                                                                                                            <asp:HiddenField ID="hdnLeg25Pax" runat="server" />
                                                                                                            <asp:HiddenField ID="hdnLeg25Avail" runat="server" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <div id='<%# "div_25_" + Eval("PassengerRequestorID") %>'>
                                                                                                    <asp:DropDownList ID="ddlLeg25" runat="server" AutoPostBack="true" CausesValidation="false"
                                                                                                        OnSelectedIndexChanged="ddlLeg25_OnSelectedIndexChanged">
                                                                                                    </asp:DropDownList>
                                                                                                    <asp:HiddenField ID="hdnOldPurpose_25" runat="server" ClientIDMode="Static" Value="0" />
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                <asp:TextBox ID="tbLeg25" MaxLength="3" CssClass="tdLabel60" runat="server" AutoPostBack="true"
                                                                                                    OnTextChanged="tbLeg25_OnTextChanged" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hdnLeg25" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                                            </FooterTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridBoundColumn UniqueName="Notes" SortExpression="Notes" HeaderText="Notes"
                                                                                            DataField="Notes" Display="false" />
                                                                                        <telerik:GridBoundColumn UniqueName="PassengerAlert" SortExpression="PassengerAlert"
                                                                                            HeaderText="PAX Alert" DataField="PassengerAlert" Display="false" />
                                                                                        <telerik:GridBoundColumn UniqueName="OrderNUM" HeaderText="OrderNUM" Display="false"
                                                                                            DataField="OrderNUM" />
                                                                                    </Columns>
                                                                                    <CommandItemTemplate>
                                                                                        <div class="grid_icon">
                                                                                            <asp:LinkButton ID="lnkPaxDelete" runat="server" CommandName="DeleteSelected" CssClass="delete-icon-grid"
                                                                                                ToolTip="Delete" OnClick="lnkPaxDelete_OnClick"></asp:LinkButton>
                                                                                            <a href="#" onclick="ShowPaxInfoPopup('insert')" id="aInsertCrew" title="Insert PAX"
                                                                                                runat="server" class="insert-icon-grid"></a>
                                                                                        </div>
                                                                                        <div id="divMultiSelect" class="multiselect" runat="server" clientidmode="Static">
                                                                                            Use CTRL key to multi select</div>
                                                                                    </CommandItemTemplate>
                                                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                                </MasterTableView>
                                                                                <ClientSettings EnablePostBackOnRowClick="true" AllowRowsDragDrop="true" AllowColumnsReorder="false"
                                                                                    ReorderColumnsOnClient="true">
                                                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="true" FrozenColumnsCount="3" />
                                                                                    <ClientEvents />
                                                                                    <Selecting AllowRowSelect="true" EnableDragToSelectRows="false" />
                                                                                </ClientSettings>
                                                                                <GroupingSettings CaseSensitive="false" />
                                                                            </telerik:RadGrid>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <div class="nav-6">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="tblspace_5">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" class="box1">
                                                    <tr>
                                                        <td>
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td class="tdLabel616">
                                                                                </td>
                                                                                <td align="center">
                                                                                    <asp:Button ID="btnaddColumns" runat="server" Visible="false" CssClass="button" Text="+/- Columns" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="nav-6">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        <telerik:RadPanelBar ID="pnlPaxSummary" Width="100%" ExpandAnimation-Type="None"
                                                                            CollapseAnimation-Type="None" runat="server">
                                                                            <Items>
                                                                                <telerik:RadPanelItem runat="server" Expanded="false" Text="Other info">
                                                                                    <Items>
                                                                                        <telerik:RadPanelItem>
                                                                                            <ContentTemplate>
                                                                                                <telerik:RadGrid ID="dgPaxSummary" runat="server" GridLines="None" AutoGenerateColumns="false"
                                                                                                    AllowSorting="true" AllowPaging="false" OnNeedDataSource="dgPaxSummary_NeedDataSource"
                                                                                                    ShowStatusBar="true" OnItemDataBound="dgPaxSummary_ItemDataBound" Width="698px"
                                                                                                    Height="250px" OnItemCommand="dgPaxSummary_ItemCommand" OnSelectedIndexChanged="dgPaxSummary_OnSelectedIndexChanged"
                                                                                                    Skin="Office2010Silver" HorizontalAlign="Left" AllowMultiRowSelection="false">
                                                                                                    <SortingSettings SortedBackColor="" />
                                                                                                    <MasterTableView DataKeyNames="OrderNUM,PaxCode,PaxName,PaxID,LegID" CommandItemDisplay="None"
                                                                                                        AllowFilteringByColumn="false" ItemStyle-HorizontalAlign="Left" AllowPaging="false">
                                                                                                        <SortExpressions>
                                                                                                            <telerik:GridSortExpression FieldName="PaxCode" SortOrder="Ascending" />
                                                                                                            <telerik:GridSortExpression FieldName="LegID" SortOrder="Ascending" />
                                                                                                        </SortExpressions>
                                                                                                        <Columns>
                                                                                                            <telerik:GridTemplateColumn UniqueName="PaxID" SortExpression="PassengerRequestorID"
                                                                                                                HeaderText="PassengerRequestorID" Display="false" HeaderStyle-Width="100px">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:HiddenField ID="hdnPassengerRequestorID" runat="server" Value='<%# Bind("PaxID") %>' />
                                                                                                                    <asp:Label ID="lblPassengerRequestorID" runat="server" Text='<%# Bind("PaxID") %>'
                                                                                                                        Width="95%" />
                                                                                                                </ItemTemplate>
                                                                                                            </telerik:GridTemplateColumn>
                                                                                                            <telerik:GridTemplateColumn UniqueName="PaxCode" HeaderText="Code" HeaderStyle-Width="60px">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblCode" runat="server" Text='<%# Bind("PaxCode") %>' />
                                                                                                                </ItemTemplate>
                                                                                                            </telerik:GridTemplateColumn>
                                                                                                            <telerik:GridTemplateColumn UniqueName="PaxName" HeaderText="Name" HeaderStyle-Width="200px">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblName" runat="server" Text='<%# Bind("PaxName") %>' />
                                                                                                                </ItemTemplate>
                                                                                                            </telerik:GridTemplateColumn>
                                                                                                            <telerik:GridTemplateColumn UniqueName="LegID" HeaderText="Leg No." HeaderStyle-Width="60px">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblLegID" runat="server" Text='<%# Bind("LegID") %>' />
                                                                                                                </ItemTemplate>
                                                                                                            </telerik:GridTemplateColumn>
                                                                                                            <telerik:GridTemplateColumn UniqueName="BillingCode" HeaderText="Billing Code" HeaderStyle-Width="95px">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:TextBox ID="tbBillingCode" runat="server" Text='<%# Bind("BillingCode") %>'
                                                                                                                        Width="70%" />
                                                                                                                </ItemTemplate>
                                                                                                            </telerik:GridTemplateColumn>
                                                                                                            <telerik:GridTemplateColumn UniqueName="DateOfBirth" HeaderText="Date Of Birth" HeaderStyle-Width="95px">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lbDateOfBirth" runat="server" Text='<%# Bind("DateOfBirth") %>' Width="95%" />
                                                                                                                </ItemTemplate>
                                                                                                            </telerik:GridTemplateColumn>
                                                                                                            <telerik:GridTemplateColumn UniqueName="Passport" HeaderText="Passport" Display="true"
                                                                                                                HeaderStyle-Width="140px">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:HiddenField ID="hdnVisaID" runat="server" Value='<%# Eval("VisaID") %>' />
                                                                                                                    <asp:HiddenField ID="hdnPassID" runat="server" Value='<%# Eval("PassportID") %>' />
                                                                                                                    <asp:TextBox ID="tbPassport" runat="server" Text='<%# Bind("Passport") %>' CssClass="tdLabel80"
                                                                                                                        Enabled="false" />
                                                                                                                    <asp:LinkButton ID="lnkPassport" runat="server" CssClass="browse-button" Width="16px"
                                                                                                                        Height="16px" CausesValidation="false" OnClientClick='<%#String.Format("return ShowPaxPassportPopup(\"{0}\",\"{1}\",\"{2}\");",Eval("PaxID"),Eval("LegID"),Eval("PaxName")) %>'></asp:LinkButton>
                                                                                                                </ItemTemplate>
                                                                                                            </telerik:GridTemplateColumn>
                                                                                                            <telerik:GridBoundColumn UniqueName="Nation" HeaderText="Nationality" DataField="Nation"
                                                                                                                AllowSorting="false" AllowFiltering="false" HeaderStyle-Width="80px" />
                                                                                                            <telerik:GridBoundColumn UniqueName="PassportExpiryDT" HeaderText="Expiration Date"
                                                                                                                DataField="PassportExpiryDT" AllowSorting="false" AllowFiltering="false" HeaderStyle-Width="80px" />
                                                                                                        </Columns>
                                                                                                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                                                                    </MasterTableView>
                                                                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                                                                        <ClientEvents />
                                                                                                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                                                                                        <Selecting AllowRowSelect="true" />
                                                                                                    </ClientSettings>
                                                                                                    <GroupingSettings CaseSensitive="false" />
                                                                                                </telerik:RadGrid>
                                                                                            </ContentTemplate>
                                                                                        </telerik:RadPanelItem>
                                                                                    </Items>
                                                                                </telerik:RadPanelItem>
                                                                            </Items>
                                                                        </telerik:RadPanelBar>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left">
                                <asp:HiddenField ID="hdnLeg" runat="server" />
                                <asp:Button ID="btnCancellationReq" runat="server" ToolTip="Cancellation Request"
                                    Text="Cancellation Request" OnClick="btnCancellationReq_Click" CssClass="button" />
                                <asp:Button ID="btnSubmitReq" runat="server" ToolTip="Submit Request" Text="Submit Request"
                                    OnClick="btnSubmitReq_Click" CssClass="button" />
                                <asp:Button ID="btnAcknowledge" runat="server" ToolTip="Acknowledge" Text="Acknowledge"
                                    OnClick="btnAcknowledge_Click" CssClass="button" />
                            </td>
                            <td align="right">
                                <asp:Button ID="btnDeleteTrip" runat="server" ToolTip="Delete Selected Record" Text="Delete Request"
                                    OnClick="btnDelete_Click" CssClass="button" />
                                <asp:Button ID="btnEditTrip" runat="server" ToolTip="Edit Selected Record" Text="Edit Request"
                                    OnClick="btnEditTrip_Click" CssClass="button" />
                                <asp:Button ID="btnCancel" runat="server" ToolTip="Cancel All Changes" Text="Cancel"
                                    OnClick="btnCancel_Click" CssClass="button" />
                                <asp:Button ID="btnSave" runat="server" ToolTip="Save Changes" Text="Save" OnClick="btnSave_Click"
                                    ValidationGroup="Save" CssClass="button" />
                                <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" CssClass="button" />
                                <asp:HiddenField runat="server" ID="hdnInsert" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table id="tblHidden" style="display: none;">
            <tr>
                <td>
                    <asp:Button ID="btnAlert" runat="server" Text="Button" OnClick="Alert_Click" />
                </td>
            </tr>
        </table>
        <table id="Table1" style="display: none;">
            <tr>
                <td>
                    <asp:Button ID="btnPaxRemoveYes" runat="server" Text="Button" OnClick="btnPaxRemoveYes_Click" />
                    <asp:Button ID="btnPaxRemoveNo" runat="server" Text="Button" OnClick="btnPaxRemoveNo_Click" />
                    <asp:Button ID="btnPaxNextYes" runat="server" Text="Button" OnClick="btnPaxNextYes_Click" />
                    <asp:Button ID="btnPaxNextNo" runat="server" Text="Button" OnClick="btnPaxNextNo_Click" />
                    <asp:Button ID="btnDeletePaxYes" runat="server" Text="Button" OnClick="btnDeletePaxYes_Click" />
                    <asp:Button ID="btnDeletePaxNo" runat="server" Text="Button" OnClick="btnDeletePaxNo_Click" />
                    <asp:Button ID="btnSubmitYes" runat="server" Text="Button" OnClick="btnSubmitYes_Click"
                        Style="display: none;" />
                    <asp:Button ID="btnReload" runat="server" Text="Button" OnClick="btnReload_Click"
                        Style="display: none;" />
                    <asp:Button ID="btnCancelYes" runat="server" Text="Button" OnClick="btnCancelYes_Click"
                        Style="display: none;" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
