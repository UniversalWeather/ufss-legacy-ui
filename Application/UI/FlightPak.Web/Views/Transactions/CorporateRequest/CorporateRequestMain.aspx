﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/CorporateRequest.Master"
    ValidateRequest="false" AutoEventWireup="true" CodeBehind="CorporateRequestMain.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.CorporateRequest.CorporateRequestMain" %>

<%@ MasterType VirtualPath="~/Framework/Masters/CorporateRequest.Master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="../../../Scripts/Common.js"></script>
        <script type="text/javascript">

            function confirmCopyCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnCopyYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnCopyNo.ClientID%>').click();
                }
            }

            function fnTextCount(text, long) {
                var maxlength = new Number(long);
                if (text.value.length >= maxlength) {
                    //                    text.value = text.value.substring(0, maxlength);
                    return false;
                }
            }

            function RedirectPage(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnReload.ClientID%>').click();
                }
            }

            function alertsubmitCallBackFn(arg) {
                window.location.reload();
            }

            function alertCancelCallBackFn(arg) {
                window.location.reload();
            }

            function confirmCancelRequestCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnCancelYes.ClientID%>').click();
                }
            }

            function confirmSubmitCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnSubmitYes.ClientID%>').click();
                }
            }

            function confirmExcludeOnSubmitMain(arg) {
                var oWnd = radalert("Warning - Trip Departure Has Exceeded Limits Set By Dispatch.<br> Trip Request Cannot Be Submitted To The Change Queue.<br> Please Contact Dispatch To Request Any Additional Changes.", 440, 110, "Request Alert");
                oWnd.add_close(function () { window.location.reload(); });
            }

            function openWin(url, value, radWin) {
                var oWnd = radopen(url + value, radWin);
            }

            function openWin(radWin) {
                var url = '';
                if (radWin == "radFleetProfilePopup") {
                    url = "../../Settings/Fleet/FleetProfilePopup.aspx?fromPage1=CRMain&FromPage=preflight&TailNumber=" + document.getElementById('<%=tbTailNo.ClientID%>').value;
                }

                else if (radWin == "rdType") {
                    if (document.getElementById("<%=hdTailNo.ClientID%>").value == "") {
                        url = "../../Settings/Fleet/AircraftPopup.aspx?AircraftCD=" + document.getElementById('<%=tbType.ClientID%>').value + "&tailNoAirCraftID=" + document.getElementById("<%=hdType.ClientID%>").value;
                    }
                }
                else if (radWin == "radPaxInfoPopup") {
                    var requestorCd = '';
                    if ($.trim($('#<%=lbcvName.ClientID%>').text()) == '')
                        requestorCd = document.getElementById("<%=tbName.ClientID%>").value;
                    url = "../../Settings/People/PassengerRequestorsPopup.aspx?fromPage=CRMain&PassengerName=" + document.getElementById('<%=tbName.ClientID%>').value + "&ShowRequestor=" + document.getElementById("<%=hdShowRequestor.ClientID%>").value +  "&PassengerRequestorCD=" + requestorCd;
                }

                else if (radWin == "rdClientCodePopup") {
                    url = '../../Settings/Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('<%=tbClientCode.ClientID%>').value;
                }
                else if (radWin == "radDepartmentPopup") {
                    url = "../../Settings/Company/DepartmentAuthorizationPopup.aspx?DepartmentCD=" + document.getElementById('<%=tbDepartment.ClientID%>').value
                }
                else if (radWin == "radCompanyMasterPopup") {
                    url = '../../Settings/Company/CompanyMasterPopup.aspx?HomeBase=' + document.getElementById('<%=tbHomeBase.ClientID%>').value;
                }
                else if (radWin == "radAuthorizationPopup") {

                    if (document.getElementById("<%=hdDepartment.ClientID%>").value != "") {
                        url = "../../Settings/Company/AuthorizationPopup.aspx?AuthorizationCD=" + document.getElementById('<%=tbAuthorization.ClientID%>').value + "&deptId=" + document.getElementById("<%=hdDepartment.ClientID%>").value;
                    }
                    else {
                        var OAlert = radalert("Please enter the Department Code before entering the Authorization Code.", 330, 100, "Corporate Request", "");
                        OAlert.add_close(function () { document.getElementById("<%=tbDepartment.ClientID%>").focus(); });
                    }
                }
                if (radWin == "rdCopyTrip") {
                    url = '/Views/Transactions/CorporateRequest/CopyRequest.aspx';
                }
                else if (radWin == "radTravelCoordinator") {
                    url = '../../Settings/People/TravelCoordinatorPopup.aspx?TravelCoordCD=' + document.getElementById('<%=tbTravelCoordinator.ClientID%>').value;
                }
                else if (radWin == "RadRetrievePopup") {
                    url = '../../Transactions/CorporateRequest/CorporateRequestSearch.aspx';
                }
                else if (radWin == "rdChangeQueue") {
                    url = '../../Transactions/CorporateRequest/CorporateChangeQueue.aspx';
                }
                else if (radWin == "rdHistory") {
                    url = "../../Transactions/CRLogisticsHistory.aspx";
                }

                if (url != "")
                    var oWnd = radopen(url, radWin);
            }
            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

            function OnClientClose(oWnd, args) {
                var combo = $find("<%= tbHomeBase.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = arg.HomeBase;
                        document.getElementById("<%=lbHomeBase.ClientID%>").innerHTML = arg.BaseDescription;
                        if (document.getElementById("<%=hdHomeBase.ClientID%>").value != "") {
                            document.getElementById("<%=hdOldHomeBase.ClientID%>").value = document.getElementById("<%=hdHomeBase.ClientID%>").value
                        }
                        document.getElementById("<%=hdHomeBase.ClientID%>").value = arg.HomebaseID;

                        var step = "tbHomeBase_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                        if (arg.HomeBase != null)
                            document.getElementById("<%=lbcvHomeBase.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = "";
                        document.getElementById("<%=hdHomeBase.ClientID%>").value = "";
                        document.getElementById("<%=lbHomeBase.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientTailNoClose(oWnd, args) {
                var combo = $find("<%= tbTailNo.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbTailNo.ClientID%>").value = arg.TailNum;
                        if (document.getElementById("<%=hdTailNo.ClientID%>").value != "") {
                            document.getElementById("<%=hdOldTailNo.ClientID%>").value = document.getElementById("<%=hdTailNo.ClientID%>").value
                        }
                        document.getElementById("<%=hdTailNo.ClientID%>").value = arg.FleetId;
                        var step = "tbTailNo_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbTailNo.ClientID%>").value = "";
                        document.getElementById("<%=hdTailNo.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientTypeClose(oWnd, args) {
                var combo = $find("<%= tbType.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbType.ClientID%>").value = arg.AircraftCD;
                        document.getElementById("<%=lbType.ClientID%>").innerHTML = arg.AircraftDescription;
                        document.getElementById("<%=hdType.ClientID%>").value = arg.AircraftID;
                        var step = "tbType_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }

                        if (arg.AircraftCD != null)
                            document.getElementById("<%=lbcvType.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbType.ClientID%>").value = "";
                        document.getElementById("<%=lbType.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdType.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientNameClose(oWnd, args) {
                var combo = $find("<%= tbName.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbName.ClientID%>").value = arg.PassengerRequestorCD;
                        if (arg.PhoneNum != "&nbsp;")
                            document.getElementById("<%=tbPhone.ClientID%>").value = arg.PhoneNum;
                        document.getElementById("<%=lbName.ClientID%>").innerHTML = arg.PassengerName;
                        document.getElementById("<%=hdName.ClientID%>").value = arg.PassengerRequestorID;
                        var step = "tbName_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                        if (arg.PassengerRequestorCD != null)
                            document.getElementById("<%=lbcvName.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbName.ClientID%>").value = "";
                        document.getElementById("<%=tbPhone.ClientID%>").value = "";
                        document.getElementById("<%=lbName.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdName.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCodeClose(oWnd, args) {
                var combo = $find("<%= tbClientCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbClientCode.ClientID%>").value = arg.ClientCD;
                        document.getElementById("<%=lbClientCode.ClientID%>").innerHTML = arg.ClientDescription;
                        document.getElementById("<%=hdClientCode.ClientID%>").value = arg.ClientID;
                        if (arg.ClientCD != null)
                            document.getElementById("<%=lbcvClientCode.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbClientCode.ClientID%>").value = "";
                        document.getElementById("<%=hdClientCode.ClientID%>").value = "";
                        document.getElementById("<%=lbClientCode.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientAuthClose(oWnd, args) {

                var combo = $find("<%= tbAuthorization.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbAuthorization.ClientID%>").value = arg.AuthorizationCD;
                        document.getElementById("<%=lbAuthorization.ClientID%>").innerHTML = arg.DeptAuthDescription;
                        document.getElementById("<%=hdAuthorization.ClientID%>").value = arg.AuthorizationID;
                        //document.getElementById("").value = arg.DepartmentID;
                        var step = "tbAuthorization_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                        if (arg.AuthorizationCD != null)
                            document.getElementById("<%=lbcvAuthorization.ClientID%>").innerHTML = "";

                    }
                    else {
                        document.getElementById("<%=tbAuthorization.ClientID%>").value = "";
                        document.getElementById("<%=lbAuthorization.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdAuthorization.ClientID%>").value = "";
                        //                        document.getElementById("").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientTravelCoordinatorClose(oWnd, args) {
                var combo = $find("<%= tbTravelCoordinator.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbTravelCoordinator.ClientID%>").value = arg.TravelCoordCD;
                        document.getElementById("<%=lbTravelCoordinator.ClientID%>").innerHTML = arg.FirstName;
                        document.getElementById("<%=hdnTravelCoordinatorID.ClientID%>").value = arg.TravelCoordinatorID;
                        if (arg.PhoneNum != undefined && arg.PhoneNum != "&nbsp;")
                            document.getElementById("<%=tbTravelCoPhone.ClientID%>").value = arg.PhoneNum;

                        if (arg.DepartmentCD != null)
                            document.getElementById("<%=lbcvTravelCoordinator.ClientID%>").innerHTML = "";

                        var step = "tbTravelCoordinator_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbTravelCoordinator.ClientID%>").value = "";
                        document.getElementById("<%=lbTravelCoordinator.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnTravelCoordinatorID.ClientID%>").value = "";
                        document.getElementById("<%=tbTravelCoPhone.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientChangeQueueClose() {
                window.location.reload();
            }

            function OnClientDeptClose(oWnd, args) {
                var combo = $find("<%= tbDepartment.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbDepartment.ClientID%>").value = arg.DepartmentCD;
                        document.getElementById("<%=lbDepartment.ClientID%>").innerHTML = arg.DepartmentName;
                        document.getElementById("<%=hdDepartment.ClientID%>").value = arg.DepartmentID;
                        if (arg.DepartmentCD != null)
                            document.getElementById("<%=lbcvDepartment.ClientID%>").innerHTML = "";
                        var step = "tbDepartment_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbDepartment.ClientID%>").value = "";
                        document.getElementById("<%=lbDepartment.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdDepartment.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="radAuthorizationPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientAuthClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AuthorizationPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radCompanyMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radFleetProfilePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientTailNoClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdType" runat="server" OnClientResizeEnd="GetDimensions" OnClientClose="OnClientTypeClose"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    NavigateUrl="~/Views/Settings/Fleet/AircraftPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientNameClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCodeClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radDepartmentPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientDeptClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radTravelCoordinator" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientTravelCoordinatorClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/TravelCoordinatorPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                    KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    Width="400px" Height="236px" Title="Copy Trip" NavigateUrl="~/Views/Transactions/CorporateRequest/CopyRequest.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdChangeQueue" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientChangeQueueClose" Behaviors="Close" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/CorporateRequest/CorporateChangeQueue.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadRetrievePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    NavigateUrl="~/Views/Transactions/CorporateRequest/CorporateRequestSearch.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                    KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    Title="History" NavigateUrl="~/Views/Transactions/CRLogisticsHistory.aspx">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <asp:HiddenField ID="hdIsDepartAuthReq" runat="server" />
        <asp:HiddenField ID="hdnTravelCoordinatorID" runat="server" />
        <asp:HiddenField ID="hdHighlightTailno" runat="server" />
        <div style="width: 718px;">
            <div style="width: 718px; padding: 5px 0 0 0px;">
                <fieldset>
                    <table width="100%">
                        <tr>
                            <td class="tdLabel110">
                                Request No.
                            </td>
                            <td class="tdLabel130">
                                <asp:TextBox ID="tbCrNumber" runat="server" CssClass="text70"></asp:TextBox>
                            </td>
                            <td class="tdLabel80">
                                Approver
                            </td>
                            <td class="tdLabel150">
                                <asp:TextBox ID="tbApprover" runat="server" CssClass="text70"></asp:TextBox>
                            </td>
                            <td class="tdLabel90">
                                Request Status
                            </td>
                            <td>
                                <asp:TextBox ID="tbCorpStatus" runat="server" CssClass="text80"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Trip No.
                            </td>
                            <td>
                                <asp:TextBox ID="tbTripNumber" runat="server" CssClass="text70"></asp:TextBox>
                            </td>
                            <td>
                                Trip Status
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="tbTripStatus" runat="server" CssClass="text70"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </div>
            <div style="width: 718px; padding: 5px 0 0 0px;">
                <fieldset>
                    <table width="100%">
                        <tr>
                            <td class="tdLabel130">
                                Travel Coordinator
                            </td>
                            <td class="tdLabel130">
                                <asp:TextBox ID="tbTravelCoordinator" runat="server" CssClass="text70" OnTextChanged="tbTravelCoordinator_TextChanged"
                                    AutoPostBack="true"></asp:TextBox>
                                <asp:Button runat="server" ToolTip="View Travel Coordinator" ID="btnTravelCoordinator"
                                    OnClientClick="javascript:openWin('radTravelCoordinator');return false;" />
                            </td>
                            <td class="tdLabel80">
                                Phone/Fax
                            </td>
                            <td class="tdLabel150">
                                <asp:TextBox ID="tbTravelCoPhone" ReadOnly="true" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdLabel80">
                                E-mail
                            </td>
                            <td class="tdLabel150">
                                <asp:TextBox ID="tbTravelCoEmail" ReadOnly="true" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lbTravelCoordinator" runat="server" CssClass="input_no_bg" Text="Label"></asp:Label>
                                <asp:Label ID="lbcvTravelCoordinator" CssClass="alert-text" runat="server"></asp:Label>
                            </td>
                            <td colspan="4">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Tail No.
                            </td>
                            <td>
                                <asp:TextBox ID="tbTailNo" runat="server" CssClass="text70" MaxLength="9" OnTextChanged="tbTailNo_TextChanged"
                                    AutoPostBack="true"></asp:TextBox>
                                <asp:HiddenField ID="hdTailNo" runat="server" />
                                <asp:HiddenField ID="hdOldTailNo" runat="server" />
                                <asp:HiddenField ID="hdMaximumPassenger" Value="0" runat="server" />
                                <asp:Button runat="server" ToolTip="Search for Tail No." ID="btnTailNo" OnClientClick="javascript:openWin('radFleetProfilePopup');return false;" />
                            </td>
                            <td>
                                <span class="mnd_text">Type Code</span>
                            </td>
                            <td>
                                <asp:TextBox ID="tbType" runat="server" CssClass="text70" MaxLength="10" OnTextChanged="tbType_TextChanged"
                                    AutoPostBack="true"></asp:TextBox>
                                <asp:HiddenField ID="hdType" runat="server" />
                                <asp:Button runat="server" ToolTip="Search for Type Code" ID="btnType" OnClientClick="javascript:openWin('rdType');return false;" />
                            </td>
                            <td class="tdLabel90">
                                Departure
                            </td>
                            <td>
                                <asp:TextBox ID="tbDepartDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                    onclick="showPopup(this, event);" MaxLength="10" onkeydown="return tbDate_OnKeyDown(this, event);"
                                    onchange="parseDate(this, event);" OnTextChanged="tbDepartDate_TextChanged"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lbcvTailNo" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                            </td>
                            <td colspan="4">
                                <asp:Label ID="lbType" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                <asp:Label ID="lbcvType" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                <asp:RequiredFieldValidator ID="rfvType" runat="server" ControlToValidate="tbType"
                                    ValidationGroup="Save" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">Type is Required.</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </div>
            <div style="width: 718px; padding: 5px 0 0 0px;">
                <fieldset>
                    <table width="100%">
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="tdLabel105">
                                            Requestor
                                        </td>
                                        <td class="tdLabel110">
                                            <asp:HiddenField ID="hdShowRequestor" runat="server" />
                                            <asp:TextBox ID="tbName" runat="server" CssClass="text70" MaxLength="5" OnTextChanged="tbName_TextChanged"
                                                AutoPostBack="true"></asp:TextBox>
                                            <asp:HiddenField ID="hdName" runat="server" />
                                            <asp:Button runat="server" ToolTip="View Requestors" ID="btnName" OnClientClick="javascript:openWin('radPaxInfoPopup');return false;" />
                                        </td>
                                        <td class="tdLabel70">
                                            <asp:Button ID="btnNameAllLegs" runat="server" ToolTip="Copy Requestor to all legs"
                                                OnClick="btnNameAllLegs_Click" />
                                        </td>
                                        <td class="tdLabel80">
                                            Phone
                                        </td>
                                        <td class="tdLabel150">
                                            <asp:TextBox ID="tbPhone" runat="server" CssClass="text120"></asp:TextBox>
                                        </td>
                                        <td class="tdLabel90">
                                            Request Date
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbRequestDate" runat="server" CssClass="text70" OnTextChanged="tbRequestDate_TextChanged"
                                                onKeyPress="return fnAllowNumericAndChar(this, event,'/')" onkeydown="return tbDate_OnKeyDown(this, event);"
                                                onclick="showPopup(this, event);" onchange="parseDate(this, event);javascript:return ValidateDate(this);"
                                                MaxLength="10"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbName" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                            <asp:Label ID="lbcvName" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="tdLabel105">
                                            Department
                                        </td>
                                        <td class="tdLabel110">
                                            <asp:TextBox ID="tbDepartment" runat="server" CssClass="text70" MaxLength="8" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                OnTextChanged="tbDepartment_TextChanged" AutoPostBack="true"></asp:TextBox>
                                            <asp:HiddenField ID="hdDepartment" runat="server" />
                                            <asp:Button runat="server" ToolTip="View Departments" ID="btnDepartment" OnClientClick="javascript:openWin('radDepartmentPopup');return false;" />
                                        </td>
                                        <td class="tdLabel70">
                                            <asp:Button ID="btnDepartmentAllLegs" runat="server" ToolTip="Copy Department to all legs"
                                                OnClick="btnDepartmentAllLegs_Click" />
                                        </td>
                                        <td class="tdLabel80">
                                            Client Code
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbClientCode" runat="server" MaxLength="5" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                CssClass="text70" ValidationGroup="Save" onBlur="return RemoveSpecialChars(this)"
                                                OnTextChanged="tbClientCode_TextChanged" AutoPostBack="true"></asp:TextBox>
                                            <asp:HiddenField ID="hdClientCode" runat="server" />
                                            <asp:Button runat="server" ToolTip="View Clients" ID="btnClientCode" OnClientClick="javascript:openWin('rdClientCodePopup');return false;" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="tdLabel290">
                                            <asp:Label ID="lbDepartment" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                            <asp:Label ID="lbcvDepartment" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lbClientCode" CssClass="input_no_bg" runat="server" Visible="true"></asp:Label>
                                            <asp:Label ID="lbcvClientCode" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="tdLabel105">
                                            Authorization
                                        </td>
                                        <td class="tdLabel110">
                                            <asp:TextBox ID="tbAuthorization" runat="server" CssClass="text70" MaxLength="8"
                                                onKeyPress="return fnAllowAlphaNumeric(this,event)" OnTextChanged="tbAuthorization_TextChanged"
                                                AutoPostBack="true"></asp:TextBox>
                                            <asp:HiddenField ID="hdAuthorization" runat="server" />
                                            <asp:Button runat="server" ToolTip="View Authorizations" ID="btnAuth" OnClientClick="javascript:openWin('radAuthorizationPopup');return false;" />
                                        </td>
                                        <td valign="top" class="tdLabel70">
                                            <asp:Button ID="btnAuthorizationAllLegs" runat="server" ToolTip="Copy Authorization to all legs"
                                                OnClick="btnAuthorizationAllLegs_Click" />
                                        </td>
                                        <td class="tdLabel80">
                                            <asp:Label ID="lbHomeBaseIcao" runat="server" Text="Home Base"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbHomeBase" runat="server" MaxLength="4" CssClass="text70" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                OnTextChanged="tbHomeBase_TextChanged" AutoPostBack="true" onBlur="return RemoveSpecialChars(this)"></asp:TextBox>
                                            <asp:HiddenField ID="hdHomeBase" runat="server" />
                                            <asp:HiddenField ID="hdOldHomeBase" runat="server" />
                                            <asp:HiddenField ID="hdHomeBaseAirportID" runat="server" />
                                            <asp:Button runat="server" ToolTip="Search for Home Base" ID="btnHomeBase" OnClientClick="javascript:openWin('radCompanyMasterPopup');return false;" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="tdLabel290">
                                            <asp:Label ID="lbAuthorization" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                            <asp:Label ID="lbcvAuthorization" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lbHomeBase" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                            <asp:Label ID="lbcvHomeBase" runat="server" CssClass="alert-text" Visible="true"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="tdLabel105">
                                            Description
                                        </td>
                                        <td class="tdLabel380">
                                            <asp:TextBox ID="tbDescription" runat="server" CssClass="text370" MaxLength="40"></asp:TextBox>
                                        </td>
                                        <td class="tdLabel50">
                                            <asp:Button ID="btnDescriptionAllLegs" runat="server" ToolTip="Copy Description to all legs"
                                                OnClick="btnDescriptionAllLegs_Click" />
                                        </td>
                                        <td class="tdLabel90">
                                            <asp:CheckBox ID="chkPrivate" Text="Private Trip" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnPrivateAllLegs" runat="server" ToolTip="Copy Private to all legs"
                                                OnClick="btnPrivateAllLegs_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </div>
            <div style="width: 718px; margin: 5px 0 0 0;" class="Dispatch_fieldset">
                <telerik:RadPanelBar ID="pnlLegNotes" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                    runat="server">
                    <Items>
                        <telerik:RadPanelItem runat="server" Expanded="false" Text="Dispatch Messaging">
                            <Items>
                                <telerik:RadPanelItem>
                                    <ContentTemplate>
                                        <table width="100%" class="box1">
                                            <tr>
                                                <td class="tdLabel250">
                                                    Requestor Notes
                                                </td>
                                                <td>
                                                    Dispatcher Notes
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="pr_main_radmask">
                                                    <telerik:RadTextBox ID="tbRequestorNotes" runat="server" TextMode="MultiLine" onKeyPress="return fnAllowAlpha(this,event)" />
                                                </td>
                                                <td class="pr_main_radmask">
                                                    <telerik:RadTextBox ID="tbDispatcherNotes" runat="server" TextMode="MultiLine" onKeyPress="return fnAllowAlpha(this,event)" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </telerik:RadPanelItem>
                            </Items>
                        </telerik:RadPanelItem>
                    </Items>
                </telerik:RadPanelBar>
            </div>
        </div>
        <div style="width: 718px; text-align: right; padding: 5px 0 0 0;">
            <table width="718px" cellspacing="6">
                <tr>
                    <td align="left">
                        <asp:Button ID="btnCancellationReq" runat="server" ToolTip="Cancellation Request"
                            Text="Cancellation Request" OnClick="btnCancellationReq_Click" CssClass="button" />
                        <asp:Button ID="btnSubmitReq" runat="server" ToolTip="Submit Request" Text="Submit Request"
                            OnClick="btnSubmitReq_Click" CssClass="button" />
                        <asp:Button ID="btnAcknowledge" runat="server" ToolTip="Acknowledge" Text="Acknowledge"
                            OnClick="btnAcknowledge_Click" CssClass="button" />
                    </td>
                    <td align="right">
                        <asp:Button ID="btnDeleteTrip" runat="server" ToolTip="Delete Selected Record" Text="Delete Request"
                            OnClick="btnDelete_Click" CssClass="button" />
                        <asp:Button ID="btnEditTrip" runat="server" ToolTip="Edit Selected Record" Text="Edit Request"
                            OnClick="btnEditTrip_Click" CssClass="button" />
                        <asp:Button ID="btnCancel" runat="server" ToolTip="Cancel All Changes" Text="Cancel"
                            OnClick="btnCancel_Click" CssClass="button" />
                        <asp:Button ID="btnSave" runat="server" ToolTip="Save Changes" Text="Save" OnClick="btnSave_Click"
                            ValidationGroup="Save" CssClass="button" />
                        <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" CssClass="button" />
                        <asp:Button ID="btnSubmitYes" runat="server" Text="Button" OnClick="btnSubmitYes_Click"
                            Style="display: none;" />
                            <asp:Button ID="btnReload" runat="server" Text="Button" OnClick="btnReload_Click"
                            Style="display: none;" />
                            
                        <asp:Button ID="btnCancelYes" runat="server" Text="Button" OnClick="btnCancelYes_Click"
                            Style="display: none;" />
                        <asp:HiddenField ID="hdCalculateLeg" runat="server" Value="false" />
                    </td>
                </tr>
            </table>
            <table id="tblHidden" style="display: none;">
                <tr>
                    <td>
                        <asp:Button ID="btnCopyYes" runat="server" Text="Button" OnClick="btnCopyYes_Click" />
                        <asp:Button ID="btnCopyNo" runat="server" Text="Button" OnClick="btnCopyNo_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
