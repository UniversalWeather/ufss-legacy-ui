﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CorporateLegsPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.CorporateRequest.CorporateLegsPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CR Legs</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgLegSummary.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgLegSummary.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "LegNum");
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "CRLegID");
                        if (i == 0) {
                            oArg.LegNUM = cell1.innerHTML + ",";
                            //oArg.LegID = cell2.innerHTML + ",";
                        }
                        else {
                            oArg.LegNUM += cell1.innerHTML + ",";
                            //oArg.LegID += cell2.innerHTML + ",";
                        }
                    }
                }
                else {
                    oArg.LegNUM = "";
                    //oArg.LegID = "";

                }
                if (oArg.LegNUM != "") {
                    oArg.LegNUM = oArg.LegNUM.substring(0, oArg.LegNUM.length - 1)
                    //oArg.LegID = oArg.LegID.substring(0, oArg.LegID.length - 1)
                } else {
                    oArg.LegNUM = "";
                    //oArg.LegID = "";
                }

                oArg.Arg1 = oArg.LegNUM;
                oArg.CallingButton = "CRLegNum";

                var oWnd = GetRadWindow();
                //                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgLegSummary">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgLegSummary" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgLegSummary" OnNeedDataSource="dgLegSummary_BindData" runat="server"
            AllowSorting="false" AutoGenerateColumns="false" PageSize="25" Width="722px"
            AllowPaging="false" AllowFilteringByColumn="false" PagerStyle-AlwaysVisible="true"
             OnItemCommand="dgLegSummary_ItemCommand" OnPreRender="dgLegSummary_PreRender" 
            >
            <MasterTableView DataKeyNames="DepartAir,ArrivalAir" PageSize="25" AllowPaging="false"
                AllowFilteringByColumn="false" CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" DataTextField="LegNum" />
                    <telerik:GridTemplateColumn CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="false"
                        ShowFilterIcon="false" HeaderText="Description" HeaderStyle-HorizontalAlign="Left"
                        DataField="LegNum" FilterDelay="500">
                        <ItemTemplate>
                            <asp:Label ID="lbLegNum" runat="server" Text="Leg:  "></asp:Label>
                            <asp:Label ID="lbLeg" runat="server" Text='<%#Eval("LegNum")%>'></asp:Label>
                            <asp:Label ID="Label1" runat="server" Text=", "></asp:Label>
                            <asp:Label ID="Label2" runat="server" Text="Departs :"></asp:Label>
                            <asp:Label ID="lbFirstName" runat="server" Text='<%#Eval("DepartAir")%>'></asp:Label>
                            <asp:Label ID="Label3" runat="server" Text="Arrives :"></asp:Label>
                            <asp:Label ID="lbInitial" runat="server" Text='<%#Eval("ArrivalAir")%>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn UniqueName="LegNum" DataField="LegNum" HeaderText="Leg" CurrentFilterFunction="EqualTo" Display="false"
                         FilterDelay="500" ShowFilterIcon="false" HeaderStyle-Width="40px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="CRLegID" DataField="CRLegID" HeaderText="Leg" CurrentFilterFunction="EqualTo" Display="false"
                         FilterDelay="500" ShowFilterIcon="false" HeaderStyle-Width="40px">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; float: left; clear: both;">
                        <%--   <asp:LinkButton ID="lnkInitInsert" runat="server" CommandName="InitInsert"><img style="border:0px;vertical-align:middle;" alt="" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                            CausesValidation="true" runat="server" CommandName="DeleteSelected"><img style="border:0px;vertical-align:middle;" alt="" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>--%>
                        <asp:Button ID="btnOK" runat="server" ToolTip="OK" OnClientClick="javascript:return returnToParent();"
                            CausesValidation="false" CssClass="button" Text="OK"></asp:Button>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
        <asp:HiddenField ID="hdName" runat="server" />
    </div>
    </form>
</body>
</html>
