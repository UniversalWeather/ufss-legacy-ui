﻿using System;
using Telerik.Web.UI;
//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web;
using System.Web.UI;
using FlightPak.Common.Constants;
using System.Collections.Generic;
using System.Linq;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Transactions.CorporateRequest
{
    public partial class AirportCityPopup : BaseSecuredPage
    {
        private string CityName = "";
        public bool showCommandItems = true;
        private ExceptionManager exManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["AirportCityName"] != null)
            {
                CityName = Request.QueryString["AirportCityName"];
            }
        }



        /// <summary>
        /// Bind Airport Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgAirport_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPakMasterService.MasterCatalogServiceClient AirportService = new FlightPakMasterService.MasterCatalogServiceClient();
                        List<GetAllAirportListForGrid> FinalList = new List<GetAllAirportListForGrid>();
                        var ReturnValue = AirportService.GetAllAirportListForGrid(false);
                        if (ReturnValue.ReturnFlag == true)
                        {
                            if (Request.QueryString["AirportCityName"] != null)
                            {
                                CityName = Request.QueryString["AirportCityName"];
                            }
                            FinalList = ReturnValue.EntityList.Where( x => x.CityName != null && x.CityName.Trim() != "").ToList() ;
                            if (CityName != null && CityName.Trim() != "")
                            {
                                FinalList = FinalList.Where(x => x.CityName.ToString().ToUpper().Trim() == CityName.ToString().ToUpper().Trim()).ToList();
                            }
                            dgAirport.DataSource = FinalList;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Airport);
                }
            }

        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgAirport;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Airport);
                }
            }

        }

        protected void dgAirport_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;
                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
            }
        }

        protected void dgAirport_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgAirport, Page.Session);
        }
       
    }
}