﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CorporateRequestSearch.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.CorporateRequest.CorporateRequestSearch" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CR Search</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="radScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript" src="../../../Scripts/Common.js"></script>
        <script type="text/javascript">
            function confirmCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnNo.ClientID%>').click();
                }
            }

            //this function is used to navigate to pop up screen's with the selected code
            function openWin(radWin) {
                var url = '';
                if (radWin == "RadClientCodePopup") {
                    url = '/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('<%=tbSearchClientCode.ClientID%>').value;
                }
                else if (radWin == "rdHomebasePopup") {
                    url = '../../Settings/Company/CompanyMasterPopup.aspx?&MultiSelect=yes&HomeBase=' + document.getElementById('<%=tbHomeBase.ClientID%>').value;
                }
                else if (radWin == "radTravelCoordinator") {
                    url = '../../Settings/People/TravelCoordinatorPopup.aspx?TravelCoordCD=' + document.getElementById('<%=tbTravelCoordinator.ClientID%>').value;
                }
                var oWnd = radopen(url, radWin);
            }

            // this function is used to display the value of selected Client code from popup
            function ClientCidePopupClose(oWnd, args) {
                var combo = $find("<%= tbSearchClientCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbSearchClientCode.ClientID%>").value = arg.ClientCD;
                        document.getElementById("<%=lbClientCode.ClientID%>").innerHTML = arg.ClientDescription;
                        document.getElementById("<%=hdClientCodeID.ClientID%>").value = arg.ClientID;
                        if (arg.ClientCD != null)
                            document.getElementById("<%=lbcvClientCode.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbSearchClientCode.ClientID%>").value = "";
                        document.getElementById("<%=hdClientCodeID.ClientID%>").value = "";
                        document.getElementById("<%=lbClientCode.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }


            // this function is used to get the dimensions
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            //this function is used to close this window
            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.location.reload(false);
            }

            function CloseRadWindow(arg) {
                GetRadWindow().Close();

            }


            //this function is used to set the frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                return oWindow;
            }
            var currentTextBox = null;
            var currentDatePicker = null;

            //This method is called to handle the onclick and onfocus client side events for the texbox
            function showPopup(sender, e) {
                //this is a reference to the texbox which raised the event
                //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);



                //this gets a reference to the datepicker, which will be shown, to facilitate
                //the selection of a date
                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                //this variable is used to store a reference to the date picker, which is currently
                //active
                currentDatePicker = datePicker;

                //this method first parses the date, that the user entered or selected, and then
                //sets it as a selected date to the picker


                datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));


                //the code lines below show the calendar, which is used to select a date. The showPopup
                //function takes three arguments - the x and y coordinates where to show the calendar, as
                //well as its height, derived from the offsetHeight property of the textbox
                var position = datePicker.getElementPosition(currentTextBox);
                datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
            }

            //this handler is used to set the text of the TextBox to the value of selected from the popup
            function dateSelected(sender, args) {

                if (currentTextBox != null) {
                    if (currentTextBox.value != args.get_newValue()) {
                        currentTextBox.value = args.get_newValue();
                    }
                }
            }

            function clearhidden() {
                document.getElementById("<%=hdHomeBase.ClientID%>").value = "";
                return true;
            }

            function OnClientHomebaseClose(oWnd, args) {
                //get the transferred arguments               
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        if (arg.HomebaseID.split(',').length > 1) {

                            var lableID = '<%= lblMultiHomebase.ClientID %>';
                            var lable = document.getElementById(lableID);
                            if (lable) {
                                lable.style.display = 'inherit';
                                lable.style.color = "#FF3300";
                            }
                            document.getElementById("<%=lbHomeBase.ClientID%>").innerHTML = "";
                        }
                        else {
                            var lableID = '<%= lblMultiHomebase.ClientID %>';
                            var lable = document.getElementById(lableID);
                            if (lable) {
                                lable.style.display = 'none';
                            }
                            document.getElementById("<%=lbHomeBase.ClientID%>").innerHTML = arg.BaseDescription;
                        }
                        document.getElementById("<%=hdHomeBase.ClientID%>").value = arg.HomebaseID;
                        var temp = arg.HomeBase.split(',');
                        document.getElementById("<%=tbHomeBase.ClientID%>").value = temp[0];
                    }
                    else {
                        document.getElementById("<%=hdHomeBase.ClientID%>").value = "";
                    }
                }
            }

            function OnClientTravelCoordinatorClose(oWnd, args) {
                var combo = $find("<%= tbTravelCoordinator.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbTravelCoordinator.ClientID%>").value = arg.TravelCoordCD;
                        document.getElementById("<%=lbTravelCoordinator.ClientID%>").innerHTML = arg.FirstName;
                        document.getElementById("<%=hdnTravelCoordinatorID.ClientID%>").value = arg.TravelCoordinatorID;
                        if (arg.DepartmentCD != null)
                            document.getElementById("<%=lbcvTravelCoordinator.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbTravelCoordinator.ClientID%>").value = "";
                        document.getElementById("<%=lbTravelCoordinator.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnTravelCoordinatorID.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function tbDate_OnKeyDown(sender, event) {
                if (event.keyCode == 9) {
                    var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                    datePicker.hidePopup();
                    return true;
                }
            }


            //this function is used to parse the date entered or selected by the user
            function parseDate(sender, e) {
                if (currentDatePicker != null) {
                    var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                    var dateInput = currentDatePicker.get_dateInput();
                    var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());

                    sender.value = formattedDate;
                }
            }
            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgCorpRequestRetrieve.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "CRTripNUM");
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "CRMainID");
                        if (i == 0) {
                            oArg.CRTripNUM = cell1.innerHTML + ",";
                            oArg.CRMainID = cell2.innerHTML + ",";
                        }
                        else {

                            oArg.CRTripNUM += cell1.innerHTML + ",";
                            oArg.CRMainID += cell2.innerHTML + ",";
                        }
                    }
                }
                else {
                    oArg.CRTripNUM = "";
                    oArg.CRMainID = "";
                }

                if (oArg.CRTripNUM != "") {
                    oArg.CRTripNUM = oArg.CRTripNUM.substring(0, oArg.CRTripNUM.length - 1)
                    oArg.CRMainID = oArg.CRMainID.substring(0, oArg.CRMainID.length - 1)
                }
                else {
                    oArg.CRTripNUM = "";
                    oArg.CRMainID = "";
                }
                oArg.Arg1 = oArg.CRTripNUM;
                oArg.Arg2 = oArg.CRMainID;
                oArg.CallingButton = "<%= Microsoft.Security.Application.Encoder.HtmlEncode(Param) %>";
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);

                }
            }

            function RowDblClick() {
                var frompagestr = "";

                frompagestr = document.getElementById("<%= hdnPageRequested.ClientID%>").value;
                if (frompagestr.toLowerCase() == "move" || frompagestr.toLowerCase() == "report") {
                    returnToParent();
                }
                else {
                    var masterTable = $find("<%= dgCorpRequestRetrieve.ClientID %>").get_masterTableView();
                    masterTable.fireCommand("InitInsert", "");
                    return false;
                }
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                    ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tblMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbDepDate" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbSearchClientCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lbClientCode" />
                    <telerik:AjaxUpdatedControl ControlID="hdClientCodeID" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="ClientCidePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHomebasePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientHomebaseClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radTravelCoordinator" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientTravelCoordinatorClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/TravelCoordinatorPopup.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server">
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <asp:HiddenField ID="hdClientCodeID" runat="server" />
        <table class="preflight-search-box">
            <tr>
                <td valign="top" width="90%">
                    <fieldset>
                        <legend>Search</legend>
                        <table>
                            <tr>
                                <td class="tdLabel140">
                                    <asp:CheckBox ID="chkHomebase" Text="Home Base Only" runat="server" />
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="tdLabel130">
                                                Starting Departure Date
                                            </td>
                                            <td class="tdLabel150">
                                                <asp:TextBox ID="tbDepDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                    onkeydown="return tbDate_OnKeyDown(this, event);" onclick="showPopup(this, event);"
                                                    MaxLength="10" onBlur="parseDate(this, event);"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="tdLabel70">
                                                Client Code
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbSearchClientCode" runat="server" MaxLength="5" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                    CssClass="text50" ValidationGroup="Save" onBlur="return RemoveSpecialChars(this)"
                                                    OnTextChanged="tbSearchClientCode_TextChanged" AutoPostBack="true"> 
                                                                                                    
                                                </asp:TextBox><asp:Button ID="btnClientCode" OnClientClick="javascript:openWin('RadClientCodePopup');return false;"
                                                    CssClass="browse-button" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="lbClientCode" CssClass="input_no_bg" runat="server" Visible="true"></asp:Label>
                                                <asp:Label ID="lbcvClientCode" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="tdLabel105">
                                                Travel Coordinator
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbTravelCoordinator" runat="server" CssClass="text150" OnTextChanged="tbTravelCoordinator_TextChanged"
                                                    AutoPostBack="true"></asp:TextBox><asp:Button runat="server" ToolTip="View Travel Coordinator"
                                                        ID="btnTravelCoordinator" CssClass="browse-button" OnClientClick="javascript:openWin('radTravelCoordinator');return false;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbTravelCoordinator" runat="server" CssClass="input_no_bg" Text="Label"></asp:Label>
                                                <asp:Label ID="lbcvTravelCoordinator" CssClass="alert-text" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:HiddenField ID="hdnTravelCoordinatorID" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
                <td width="10%">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="RetrieveSearch_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="tdLabel80">
                                Home Base:
                            </td>
                            <td>
                                <asp:TextBox ID="tbHomeBase" runat="server" MaxLength="4" CssClass="text50" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                    OnTextChanged="tbHomeBase_TextChanged" AutoPostBack="true" onBlur="return clearhidden();"></asp:TextBox>
                                <asp:HiddenField ID="hdHomeBase" runat="server" />
                                <asp:Button runat="server" ToolTip="Search for Home Base" CssClass="browse-button"
                                    ID="btnHomeBase" OnClientClick="javascript:openWin('rdHomebasePopup');return false;" />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Label ID="lbHomeBase" runat="server" Visible="true" CssClass="input_no_bg"></asp:Label>
                                <asp:Label ID="lbcvHomeBase" runat="server" CssClass="alert-text" Visible="true"></asp:Label>
                                <asp:Label ID="lblMultiHomebase" runat="server" Text="(Multiple)" ForeColor="Red"
                                    Style="display: none;" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3" valign="top">
                    <asp:HiddenField ID="hdnPageRequested" runat="server" />
                    <telerik:RadGrid ID="dgCorpRequestRetrieve" runat="server" AllowMultiRowSelection="true" OnPageIndexChanged="MetroCity_PageIndexChanged"
                        OnItemDataBound="dgCorpRequestRetrieve_ItemDataBound" AllowSorting="true" OnNeedDataSource="dgCorpRequestRetrieve_BindData"
                        OnItemCommand="dgCorpRequestRetrieve_ItemCommand" AutoGenerateColumns="false"
                        OnItemCreated="dgCorpRequestRetrieve_ItemCreated" PageSize="10" AllowPaging="true"
                        Width="880px" Height="350px" OnPreRender="dgCorpRequestRetrieve_PreRender">
                        <MasterTableView CommandItemDisplay="Bottom" AllowFilteringByColumn="true" DataKeyNames="CRMainID">
                            <Columns>
                                <telerik:GridBoundColumn DataField="CRTripNUM" HeaderText="CR No." UniqueName="CRTripNUM"
                                    HeaderStyle-Width="50px" AutoPostBackOnFilter="false" CurrentFilterFunction="EqualTo"
                                    FilterControlWidth="20px" ShowFilterIcon="false" FilterDelay="500" />
                                <telerik:GridBoundColumn DataField="EstDepartureDT" HeaderText="Departure Date" UniqueName="EstDepartureDT"
                                    HeaderStyle-Width="80px" AutoPostBackOnFilter="false" CurrentFilterFunction="EqualTo"
                                    ShowFilterIcon="false" FilterDelay="500" />
                                <telerik:GridBoundColumn DataField="Rqstr" HeaderText="Requestor" UniqueName="Rqstr"
                                    HeaderStyle-Width="65px" AutoPostBackOnFilter="false" CurrentFilterFunction="StartsWith"
                                    ShowFilterIcon="false" />
                                <telerik:GridBoundColumn DataField="TripDescription" HeaderText="Trip Description"
                                    UniqueName="TripDescription" HeaderStyle-Width="85px" AutoPostBackOnFilter="false"
                                    CurrentFilterFunction="StartsWith" ShowFilterIcon="false" FilterDelay="500" />
                                <telerik:GridBoundColumn DataField="CorporateRequestStatus" HeaderText="Status" UniqueName="CorporateRequestStatus"
                                    HeaderStyle-Width="70px" AutoPostBackOnFilter="false" CurrentFilterFunction="StartsWith"
                                    ShowFilterIcon="false" FilterDelay="500" />
                                <telerik:GridBoundColumn DataField="RequestDT" HeaderText="Request Date" UniqueName="RequestDT"
                                    HeaderStyle-Width="80px" AutoPostBackOnFilter="false" CurrentFilterFunction="EqualTo"
                                    ShowFilterIcon="false" FilterDelay="500" />
                                <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail No." UniqueName="TailNum"
                                    HeaderStyle-Width="70px" AutoPostBackOnFilter="false" CurrentFilterFunction="StartsWith"
                                    ShowFilterIcon="false" FilterDelay="500" />
                                <telerik:GridBoundColumn DataField="AircraftCD" HeaderText="Type Code" UniqueName="AircraftCD"
                                    HeaderStyle-Width="65px" AutoPostBackOnFilter="false" CurrentFilterFunction="StartsWith"
                                    ShowFilterIcon="false" FilterDelay="500" />
                                <telerik:GridBoundColumn DataField="AcknowledgementStatus" HeaderText="Acknowledgement"
                                    UniqueName="AcknowledgementStatus" HeaderStyle-Width="110px" AutoPostBackOnFilter="false"
                                    CurrentFilterFunction="StartsWith" ShowFilterIcon="false" FilterDelay="500" />
                                <telerik:GridBoundColumn DataField="TripNUM" HeaderText="Trip No." UniqueName="TripNUM"
                                    HeaderStyle-Width="56px" AutoPostBackOnFilter="false" CurrentFilterFunction="EqualTo"
                                    ShowFilterIcon="false" FilterDelay="500" />
                                <telerik:GridBoundColumn DataField="TripSheetStatus" HeaderText="Trip Status" UniqueName="TripSheetStatus"
                                    HeaderStyle-Width="100px" AutoPostBackOnFilter="false" CurrentFilterFunction="StartsWith"
                                    ShowFilterIcon="false" FilterDelay="500" />
                                <telerik:GridBoundColumn DataField="CRMainID" HeaderText="CRMainID" UniqueName="CRMainID"
                                    HeaderStyle-Width="100px" Display="false" ShowFilterIcon="false" />
                            </Columns>
                            <CommandItemTemplate>
                                <div style="padding: 5px 5px; text-align: right;">
                                    <asp:Button ID="lbtnInitInsert" runat="server" ToolTip="OK" CommandName="InitInsert"
                                        CausesValidation="false" CssClass="button" Text="OK"></asp:Button>
                                    <asp:Button ID="btnOK" runat="server" ToolTip="OK" OnClientClick="javascript:return returnToParent();"
                                        CausesValidation="false" CssClass="button" Text="OK"></asp:Button>
                                    <asp:Button ID="btnSUB" runat="server" ToolTip="OK" OnClientClick="javascript:return returnToParent();"
                                        CausesValidation="false" CssClass="button" Text="OK"></asp:Button>
                                </div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="RowDblClick" />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                    <asp:Label ID="InjectScript" runat="server"></asp:Label>
                    <%--<asp:Label ID="lbMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>--%>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <table id="tblHidden" style="display: none; position: relative">
                        <tr>
                            <td>
                                <asp:Button ID="btnYes" runat="server" Text="Button" OnClick="Yes_Click" />
                                <asp:Button ID="btnNo" runat="server" Text="Button" OnClick="No_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
