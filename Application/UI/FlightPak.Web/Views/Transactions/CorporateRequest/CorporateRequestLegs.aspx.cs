﻿using System;
using System.Text;
using System.Data;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;

using FlightPak.Web.PreflightService;
using FlightPak.Web.CorporateRequestService;
using System.Drawing;
using FlightPak.Web.CalculationService;
//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using FlightPak.Web.Framework.Helpers;
using System.Web;

namespace FlightPak.Web.Views.Transactions.CorporateRequest
{
    public partial class CorporateRequestLegs : BaseSecuredPage
    {
        private ExceptionManager exManager;
        public CRMain CorpRequest = new CRMain();
        private delegate void SaveSession();
        string CompDateFormat = "MM/dd/yyyy";


        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveSession SaveToSaveCorpRequestSession = new SaveSession(SaveCRToSession);
                        Master.SaveCRToSession = SaveToSaveCorpRequestSession;
                        Master.RadAjax_AjaxRequest += RadAjaxManager1_AjaxRequest;
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(Master.MasterForm.FindControl("DivMasterForm"), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(DivExternalForm, DivExternalForm, RadAjaxLoadingPanel1);

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnEditTrip, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);


                        #region "Ajax Distortion issues Fix"

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepartCity, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepartsIcao, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepartAirportName, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbArrivalCity, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbArrivalIcao, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepartsLocal, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbPassenger, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbAuthorizationCode, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbPassengerName, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbAuthName, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepartment, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbFlightCategoryCode, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbDepartmentName, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbFlightCategoryName, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbArrivalLocal, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDistance, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkPrivate, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbPaxNumbers, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbElapsedTime, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rmbArrivalLocalTime, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rmbDepartsLocal, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnAuthorizationCode, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnClosestArrivalICAO, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDepartment, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDepartsICAO, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnFlightCategory, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnPassenger, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDeleteLeg, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancellationReq, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnNext, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancel, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnAddLeg1, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDeleteLeg, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnInsertLeg, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSubmitReq, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);



                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepartCity, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepartsIcao, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepartAirportName, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbArrivalCity, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbArrivalIcao, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepartsLocal, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbPassenger, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbAuthorizationCode, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbPassengerName, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbAuthName, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepartment, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbFlightCategoryCode, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbDepartmentName, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbFlightCategoryName, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbArrivalLocal, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDistance, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(chkPrivate, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbPaxNumbers, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbElapsedTime, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rmbArrivalLocalTime, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rmbDepartsLocal, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnAuthorizationCode, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnClosestArrivalICAO, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDepartment, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDepartsICAO, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnFlightCategory, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnPassenger, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDeleteLeg, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancellationReq, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnNext, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancel, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnAddLeg1, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDeleteLeg, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnInsertLeg, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSubmitReq, DivExternalForm, RadAjaxLoadingPanel1);

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rtsLegs, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rtsLegs, DivExternalForm, RadAjaxLoadingPanel1);

                        #endregion

                        hdnRunway.Value = "";
                        if (UserPrincipal.Identity._fpSettings._CorpReqMinimumRunwayLength != null && UserPrincipal.Identity._fpSettings._CorpReqMinimumRunwayLength.ToString() != "")
                        {
                            hdnRunway.Value = UserPrincipal.Identity._fpSettings._CorpReqMinimumRunwayLength.ToString();
                        }
                        if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                            CompDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                        hdTimeDisplayTenMin.Value = UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == null ? "1" : UserPrincipal.Identity._fpSettings._TimeDisplayTenMin.ToString();
                        hdHomeCategory.Value = (UserPrincipal.Identity._fpSettings._DefaultFlightCatID == null ? 0 : UserPrincipal.Identity._fpSettings._DefaultFlightCatID).ToString();
                        //Replicate Save Cancel Delete Buttons in header
                        Master.SaveClick += btnSave_Click;
                        Master.CancelClick += btnCancel_Click;
                        Master.DeleteClick += btnDelete_Click;
                        //Load US Country for DOM/Internation calculation 
                        using (FlightPakMasterService.MasterCatalogServiceClient FleetService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPakMasterService.GetFleet> GetFleetList = new List<GetFleet>();
                            var fleetretval = FleetService.GetFleet();
                            if (fleetretval.ReturnFlag)
                            {
                                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                if (CorpRequest != null)
                                {
                                    if (CorpRequest.FleetID != 0 && CorpRequest.FleetID != null)
                                    {
                                        GetFleetList = fleetretval.EntityList.Where(x => x.FleetID == (long)CorpRequest.FleetID).ToList();

                                        if (GetFleetList != null && GetFleetList.Count > 0)
                                        {
                                            if (CorpRequest.Fleet != null)
                                            {
                                                CorpRequest.Fleet.MaximumPassenger = GetFleetList[0].MaximumPassenger != null ? GetFleetList[0].MaximumPassenger : 0;

                                                if (CorpRequest.CRLegs != null)
                                                {
                                                    foreach (CRLeg Leg in CorpRequest.CRLegs)
                                                    {

                                                        Leg.SeatTotal = Convert.ToInt32((decimal)CorpRequest.Fleet.MaximumPassenger);
                                                        if (Leg.PassengerTotal != null)
                                                            Leg.ReservationAvailable = Leg.SeatTotal - Leg.PassengerTotal;
                                                        else
                                                            Leg.ReservationAvailable = Leg.SeatTotal;
                                                    }
                                                }
                                                Session["CurrentCorporateRequest"] = CorpRequest;

                                                FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId Fleetcompany = new GetAllCompanyMasterbyHomeBaseId();
                                                if (GetFleetList[0].HomebaseID != null)
                                                {
                                                    Fleetcompany = GetCompany((long)GetFleetList[0].HomebaseID);
                                                    FlightPakMasterService.GetAllAirport Fleetcompanyairport = new GetAllAirport();
                                                    if (Fleetcompany != null && Fleetcompany.HomebaseAirportID != null)
                                                    {
                                                        Fleetcompanyairport = GetAirport((long)Fleetcompany.HomebaseAirportID);
                                                        if (Fleetcompanyairport != null)
                                                            hdnCountryID.Value = Fleetcompanyairport.CountryID.ToString();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }


                        if (!IsPostBack)
                        {

                            #region Authorization
                            CheckAutorization(Permission.CorporateRequest.ViewCRLeg);

                            if (!IsAuthorized(Permission.CorporateRequest.ViewCRManager))
                            {
                                btnNext.Visible = false;
                            }

                            if (!IsAuthorized(Permission.CorporateRequest.DeleteCRManager))
                            {
                                btnDeleteTrip.Visible = false;
                            }

                            if (!IsAuthorized(Permission.CorporateRequest.AddCRLeg))
                            {
                                btnAddLeg1.Visible = false;
                                btnInsertLeg.Visible = false;
                            }

                            if (!IsAuthorized(Permission.CorporateRequest.DeleteCRLeg))
                            {
                                btnDeleteLeg.Visible = false;
                            }

                            #endregion


                            #region "Kilometer/Miles"
                            if (UserPrincipal.Identity._fpSettings._IsKilometer == null ? false : (bool)UserPrincipal.Identity._fpSettings._IsKilometer)
                                lbMiles.Text = "Kilometers";
                            else
                                lbMiles.Text = "Miles(N)";


                            #endregion


                            Session["StandardFlightpakConversion"] = LoadStandardFPConversion();
                            TimeDisplay();
                            hdnLeg.Value = "1";

                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                                string StatusStr = string.Empty;
                                switch (CorpRequest.TripStatus)
                                {
                                    case "W": StatusStr = "W-Worksheet"; break;
                                    case "T": StatusStr = "T-Tripsheet"; break;
                                    case "U": StatusStr = "U-Unfulfilled"; break;
                                    case "X": StatusStr = "X-Cancelled"; break;
                                    case "H": StatusStr = "H-Hold"; break;
                                }

                                BindCRTabs(CorpRequest, "pageload");
                                hdnLeg.Value = (Convert.ToInt16(rtsLegs.Tabs[0].Value) + 1).ToString();

                                if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count() > 0)
                                {
                                    //Modify Mode
                                    LoadLegDetails(CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1]);
                                }
                                else
                                {
                                    //Add Mode Set the default values
                                    #region Addmode Set default values
                                    if (!IsAuthorized(Permission.CorporateRequest.AddCRLeg))
                                    {
                                        btnSave.Visible = false;
                                        btnCancel.Visible = false;
                                    }

                                    if (UserPrincipal.Identity._fpSettings._CrewDutyID != null)
                                        hdCrewRules.Value = Convert.ToString((long)UserPrincipal.Identity._fpSettings._CrewDutyID);
                                    hdSetFarRules.Value = (UserPrincipal.Identity._fpSettings._FedAviationRegNum == null ? string.Empty : UserPrincipal.Identity._fpSettings._FedAviationRegNum).ToString();
                                    hdCategory.Value = (UserPrincipal.Identity._fpSettings._DefaultFlightCatID == null ? 0 : UserPrincipal.Identity._fpSettings._DefaultFlightCatID).ToString();

                                    if (Session["CurrentCorporateRequest"] != null)
                                    {
                                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                        hdAircraft.Value = CorpRequest.AircraftID.ToString();
                                        Int64 HomebaseID = 0;
                                        Int64 DepartmentID = 0;


                                        if (CorpRequest.ClientID != null && CorpRequest.ClientID != 0)
                                        {
                                            hdClient.Value = CorpRequest.ClientID.ToString();
                                        }




                                        if (CorpRequest.HomebaseID != null && CorpRequest.HomebaseID > 0)
                                        {
                                            HomebaseID = (Int64)CorpRequest.HomebaseID;


                                            FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId Comp = GetCompany(HomebaseID);
                                            if (Comp != null)
                                            {
                                                FlightPakMasterService.GetAllAirport HomebaseAirport = GetAirport((long)Comp.HomebaseAirportID);
                                                if (HomebaseAirport != null)
                                                {
                                                    tbDepartsIcao.Text = HomebaseAirport.IcaoID;
                                                    tbDepartAirportName.Text = HomebaseAirport.AirportName != null ? System.Web.HttpUtility.HtmlEncode(HomebaseAirport.AirportName) : string.Empty;
                                                    tbDepartCity.Text = HomebaseAirport.CityName != null ? System.Web.HttpUtility.HtmlEncode(HomebaseAirport.CityName) : string.Empty;
                                                    hdDepart.Value = HomebaseAirport.AirportID.ToString();
                                                    hdHomebaseAirport.Value = HomebaseAirport.AirportID.ToString();


                                                    //lbDepartsICAO.Text = HomebaseAirport.IcaoID == null ? string.Empty : System.Web.HttpUtility.HtmlEncode(HomebaseAirport.IcaoID.ToString());
                                                    //lbDepartsCity.Text = HomebaseAirport.CityName == null ? string.Empty : System.Web.HttpUtility.HtmlEncode(HomebaseAirport.CityName.ToString());
                                                    //lbDepartsState.Text = HomebaseAirport.StateName == null ? string.Empty : System.Web.HttpUtility.HtmlEncode(HomebaseAirport.StateName.ToString());
                                                    //lbDepartsCountry.Text = HomebaseAirport.CountryName == null ? string.Empty : System.Web.HttpUtility.HtmlEncode(HomebaseAirport.CountryName.ToString());
                                                    //lbDepartsAirport.Text = HomebaseAirport.AirportName == null ? string.Empty : System.Web.HttpUtility.HtmlEncode(HomebaseAirport.AirportName.ToString());
                                                    //lbDepartsTakeoffbias.Text = HomebaseAirport.TakeoffBIAS != null ? System.Web.HttpUtility.HtmlEncode(HomebaseAirport.TakeoffBIAS.ToString()) : "0";
                                                    //lbDepartsLandingbias.Text = HomebaseAirport.LandingBIAS != null ? System.Web.HttpUtility.HtmlEncode(HomebaseAirport.LandingBIAS.ToString()) : "0";

                                                    #region Tooltip for AirportDetails

                                                    string builder = string.Empty;

                                                    builder = "ICAO : " + (HomebaseAirport.IcaoID != null ? HomebaseAirport.IcaoID : string.Empty)
                                                        + "\n" + "City : " + (HomebaseAirport.CityName != null ? HomebaseAirport.CityName : string.Empty)
                                                        + "\n" + "State/Province : " + (HomebaseAirport.StateName != null ? HomebaseAirport.StateName : string.Empty)
                                                        + "\n" + "Country : " + (HomebaseAirport.CountryName != null ? HomebaseAirport.CountryName : string.Empty)
                                                        + "\n" + "Airport : " + (HomebaseAirport.AirportName != null ? HomebaseAirport.AirportName : string.Empty)
                                                        + "\n" + "DST Region : " + (HomebaseAirport.DSTRegionCD != null ? HomebaseAirport.DSTRegionCD : string.Empty)
                                                        + "\n" + "UTC+/- : " + (HomebaseAirport.OffsetToGMT != null ? HomebaseAirport.OffsetToGMT.ToString() : string.Empty)
                                                        + "\n" + "Longest Runway : " + (HomebaseAirport.LongestRunway != null ? HomebaseAirport.LongestRunway.ToString() : string.Empty)
                                                        + "\n" + "IATA : " + (HomebaseAirport.Iata != null ? HomebaseAirport.Iata.ToString() : string.Empty);

                                                    tbDepartAirportName.ToolTip = builder;
                                                    //lbDepartIcao.ToolTip = builder;

                                                    #endregion
                                                }
                                                else
                                                {
                                                    tbDepartsIcao.Text = "";
                                                    tbDepartAirportName.Text = "";
                                                    tbDepartCity.Text = "";
                                                    hdDepart.Value = null;
                                                    hdHomebaseAirport.Value = null;
                                                }
                                            }
                                        }

                                        if (CorpRequest.DepartmentID != null && CorpRequest.DepartmentID > 0)
                                        {
                                            DepartmentID = (Int64)CorpRequest.HomebaseID;


                                            FlightPakMasterService.GetAllDepartments Comp = GetAllDepartment(DepartmentID);
                                            if (Comp != null)
                                            {
                                                FlightPakMasterService.GetAllDepartments HomebaseAirport = GetAllDepartment((long)Comp.DepartmentID);
                                                if (HomebaseAirport != null)
                                                {
                                                    tbDepartment.Text = HomebaseAirport.DepartmentCD;
                                                    lbDepartmentName.Text = HomebaseAirport.DepartmentName != null ? System.Web.HttpUtility.HtmlEncode(HomebaseAirport.DepartmentName) : string.Empty;
                                                    hdDepartment.Value = HomebaseAirport.DepartmentID.ToString();

                                                }
                                                else
                                                {
                                                    tbDepartment.Text = "";
                                                    lbDepartmentName.Text = "";
                                                    hdDepartment.Value = null;
                                                }
                                            }
                                        }

                                        if (CorpRequest.AuthorizationID != null && CorpRequest.AuthorizationID > 0)
                                        {
                                            DepartmentID = (Int64)CorpRequest.HomebaseID;


                                            FlightPakMasterService.GetAllDepartments Comp = GetAllDepartment(DepartmentID);
                                            if (Comp != null)
                                            {
                                                FlightPakMasterService.GetAllDepartments HomebaseAirport = GetAllDepartment((long)Comp.DepartmentID);
                                                if (HomebaseAirport != null)
                                                {
                                                    tbDepartment.Text = HomebaseAirport.DepartmentCD;
                                                    lbDepartmentName.Text = HomebaseAirport.DepartmentName != null ? System.Web.HttpUtility.HtmlEncode(HomebaseAirport.DepartmentName) : string.Empty;
                                                    hdDepartment.Value = HomebaseAirport.DepartmentID.ToString();

                                                }
                                                else
                                                {
                                                    tbDepartment.Text = "";
                                                    lbDepartmentName.Text = "";
                                                    hdDepartment.Value = null;
                                                }
                                            }
                                        }

                                        if (CorpRequest.IsPrivate != null && CorpRequest.IsPrivate == true)
                                        {
                                            chkPrivate.Checked = true;
                                        }


                                        if (UserPrincipal.Identity._fpSettings._CorpReqStartTM != null && UserPrincipal.Identity._fpSettings._CorpReqStartTM.Trim() != string.Empty)
                                        {
                                            string startHrs = UserPrincipal.Identity._fpSettings._CorpReqStartTM;
                                            rmbDepartsLocal.Text = startHrs;
                                            rmbArrivalLocalTime.Text = startHrs;
                                        }

                                        if (CorpRequest.EstDepartureDT != null)
                                        {
                                            DateTime dt = new DateTime();
                                            DateTime ldGmtDep = new DateTime();
                                            string StartTime = rmbDepartsLocal.Text.Trim();
                                            if (StartTime.Trim() == "")
                                            {
                                                StartTime = "0000";
                                            }
                                            int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                            int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                                            tbDepartsLocal.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", CorpRequest.EstDepartureDT);
                                            tbUtcDate.Text = tbDepartsLocal.Text;
                                            tbHomeDate.Text = tbDepartsLocal.Text;

                                            if (!string.IsNullOrEmpty(hdDepart.Value))
                                            {
                                                CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient();
                                                if (UserPrincipal.Identity._fpSettings._IsAutomaticCalcLegTimes != null && (bool)UserPrincipal.Identity._fpSettings._IsAutomaticCalcLegTimes == true)
                                                {
                                                    dt = DateTime.ParseExact(tbDepartsLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);

                                                    dt = dt.AddHours(StartHrs);
                                                    dt = dt.AddMinutes(StartMts);
                                                    //GMTDept
                                                    ldGmtDep = objDstsvc.GetGMT(Convert.ToInt64(hdDepart.Value), dt, true, false);

                                                    string startHrs = "0000" + ldGmtDep.Hour.ToString();
                                                    string startMins = "0000" + ldGmtDep.Minute.ToString();
                                                    tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldGmtDep);
                                                    rmtUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                                }
                                            }
                                        }
                                    }


                                    if (UserPrincipal.Identity._clientId != null)
                                    {

                                        hdClient.Value = UserPrincipal.Identity._clientId.ToString();
                                        FlightPakMasterService.Client client = new FlightPakMasterService.Client();
                                        client = GetClient((long)UserPrincipal.Identity._clientId);
                                    }

                                    if (CorpRequest.Fleet != null)
                                    {
                                        if (CorpRequest.Fleet.MaximumPassenger != null && CorpRequest.Fleet.MaximumPassenger > 0)
                                        {
                                            tbPaxNumbers.Text = "0";
                                            if (CorpRequest.Fleet.MaximumPassenger < 0)
                                            {
                                                ///tbAvail.ForeColor = Color.Red;
                                                // tbAvail.Text = CorpRequest.Fleet.MaximumPassenger.ToString();
                                            }
                                            //tbAvail.Text = CorpRequest.Fleet.MaximumPassenger.ToString();
                                        }
                                        else
                                        {
                                            tbPaxNumbers.Text = "0";
                                            //tbAvail.Text = CorpRequest.Fleet.MaximumPassenger.ToString();
                                        }
                                    }

                                    SetCrewDutyTypeAndFARRules();
                                    SetCategory();
                                    setAircraftSettings();
                                    SetWindReliability();
                                    CopyCRMainDetails(CorpRequest);
                                    SetCopyInfo(CorpRequest);


                                    #endregion
                                }
                            }
                            //this.tbPaxNumbers.ReadOnly = true;
                            this.tbDistance.ReadOnly = true;
                            this.tbElapsedTime.ReadOnly = true;

                        }
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                            hdAircraft.Value = CorpRequest.AircraftID.ToString();
                            Int64 HomebaseID = 0;

                            if (CorpRequest != null)
                            {
                                if (CorpRequest.AircraftID != null && CorpRequest.AircraftID != 0)
                                {
                                    FlightPakMasterService.Aircraft TripAircraft = new FlightPakMasterService.Aircraft();

                                    TripAircraft = GetAircraft((long)CorpRequest.AircraftID);

                                    if (TripAircraft != null)
                                    {
                                        if (TripAircraft.IsFixedRotary != null && TripAircraft.IsFixedRotary.ToUpper() == "R")
                                        {
                                        }
                                    }
                                }
                            }

                            if (CorpRequest.HomebaseID != null && CorpRequest.HomebaseID > 0)
                            {
                                HomebaseID = (Int64)CorpRequest.HomebaseID;
                                if (UserPrincipal.Identity._homeBaseId != null)
                                    hdHomebaseAirport.Value = UserPrincipal.Identity._airportId.ToString();
                                else
                                    hdHomebaseAirport.Value = null;

                                FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId Comp = GetCompany(HomebaseID);
                                if (Comp != null)
                                {
                                    FlightPakMasterService.GetAllAirport HomebaseAirport = GetAirport((long)Comp.HomebaseAirportID);
                                    if (HomebaseAirport != null)
                                        hdHomebaseAirport.Value = HomebaseAirport.AirportID.ToString();
                                    else
                                        hdHomebaseAirport.Value = null;
                                }
                            }
                        }
                        LoadHomebaseSettings(!IsPostBack);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }





        protected void page_prerender(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat != null && UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 2)
                        {
                            ArrTimeForm.Visible = false;
                            DepTimeForm.Visible = false;
                        }
                        else
                        {
                            ArrTimeForm.Visible = true;
                            DepTimeForm.Visible = true;

                        }
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                            #region Changes for Legnum in CRHeader
                            Master.FloatLegNum.Visible = true;
                            #endregion

                            if (CorpRequest.Mode == CorporateRequestTripActionMode.Edit)
                            {
                                btnSubmitReq.Enabled = false;
                                btnCancellationReq.Enabled = false;
                                btnAcknowledge.Enabled = false;
                                btnSave.Enabled = true;
                                btnCancel.Enabled = true;
                                btnDeleteTrip.Enabled = false;
                                btnEditTrip.Enabled = false;

                                btnSave.CssClass = "button";
                                btnCancel.CssClass = "button";
                                btnDeleteTrip.CssClass = "button-disable";
                                btnEditTrip.CssClass = "button-disable";
                                btnSubmitReq.CssClass = "button-disable";
                                btnCancellationReq.CssClass = "button-disable";
                                btnAcknowledge.CssClass = "button-disable";
                                EnableForm(true);
                            }
                            else
                            {
                                btnSave.Enabled = false;
                                btnCancel.Enabled = false;
                                btnDeleteTrip.Enabled = true;
                                btnEditTrip.Enabled = true;

                                btnSave.CssClass = "button-disable";
                                btnCancel.CssClass = "button-disable";
                                btnDeleteTrip.CssClass = "button";
                                btnEditTrip.CssClass = "button";
                                EnableForm(false);
                            }
                        }
                        else
                        {
                            btnSubmitReq.Enabled = false;
                            btnCancellationReq.Enabled = false;
                            btnAcknowledge.Enabled = false;
                            btnSave.Enabled = false;
                            btnCancel.Enabled = false;
                            btnDeleteTrip.Enabled = false;
                            btnEditTrip.Enabled = false;
                            btnNext.Enabled = false;

                            btnSubmitReq.CssClass = "button-disable";
                            btnCancellationReq.CssClass = "button-disable";
                            btnAcknowledge.CssClass = "button-disable";
                            btnSave.CssClass = "button-disable";
                            btnCancel.CssClass = "button-disable";
                            btnDeleteTrip.CssClass = "button-disable";
                            btnEditTrip.CssClass = "button-disable";
                            btnNext.CssClass = "button-disable";
                            EnableForm(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }

        }


        #region "Text changed Events"

        #region "Main Events"

        protected void tbDepart_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        RadAjaxManager.GetCurrent(Page).FocusControl(btnDepartsICAO);
                        tbDepartAirportName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        tbDepartCity.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvDepartIcao.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        tbDepartsIcao.Text = tbDepartsIcao.Text.ToUpper();
                        if (!string.IsNullOrEmpty(tbDepartsIcao.Text))
                        {
                            //if (tbDepart.Text != tbArrivalLocal.Text)
                            //{
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {

                                var objRetVal = objDstsvc.GetAirportByAirportICaoID(tbDepartsIcao.Text.ToUpper().Trim());
                                List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();

                                if (objRetVal.ReturnFlag)
                                {
                                    AirportLists = objRetVal.EntityList;
                                    if (AirportLists != null && AirportLists.Count > 0)
                                    {
                                        tbDepartAirportName.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].AirportName != null ? AirportLists[0].AirportName.ToUpper() : string.Empty);
                                        tbDepartCity.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].CityName != null ? AirportLists[0].CityName.ToUpper() : string.Empty);

                                        hdDepart.Value = AirportLists[0].AirportID.ToString();

                                        //lbDepartsICAO.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].IcaoID == null ? string.Empty : AirportLists[0].IcaoID.ToString());
                                        //lbDepartsCity.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].CityName == null ? string.Empty : AirportLists[0].CityName.ToString());
                                        //lbDepartsState.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].StateName == null ? string.Empty : AirportLists[0].StateName.ToString());
                                        //lbDepartsCountry.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].CountryName == null ? string.Empty : AirportLists[0].CountryName.ToString());
                                        //lbDepartsAirport.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].AirportName == null ? string.Empty : AirportLists[0].AirportName.ToString());
                                        //lbDepartsTakeoffbias.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].TakeoffBIAS != null ? AirportLists[0].TakeoffBIAS.ToString() : "0");
                                        //lbDepartsLandingbias.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].LandingBIAS != null ? AirportLists[0].LandingBIAS.ToString() : "0");

                                        lbcvDepartIcao.Text = System.Web.HttpUtility.HtmlEncode("");

                                        #region Tooltip for AirportDetails

                                        string builder = string.Empty;

                                        builder = "ICAO : " + (AirportLists[0].IcaoID != null ? AirportLists[0].IcaoID : string.Empty)
                                            + "\n" + "City : " + (AirportLists[0].CityName != null ? AirportLists[0].CityName : string.Empty)
                                            + "\n" + "State/Province : " + (AirportLists[0].StateName != null ? AirportLists[0].StateName : string.Empty)
                                            + "\n" + "Country : " + (AirportLists[0].CountryName != null ? AirportLists[0].CountryName : string.Empty)
                                            + "\n" + "Airport : " + (AirportLists[0].AirportName != null ? AirportLists[0].AirportName : string.Empty)
                                            + "\n" + "DST Region : " + (AirportLists[0].DSTRegionCD != null ? AirportLists[0].DSTRegionCD : string.Empty)
                                            + "\n" + "UTC+/- : " + (AirportLists[0].OffsetToGMT != null ? AirportLists[0].OffsetToGMT.ToString() : string.Empty)
                                            + "\n" + "Longest Runway : " + (AirportLists[0].LongestRunway != null ? AirportLists[0].LongestRunway.ToString() : string.Empty)
                                            + "\n" + "IATA : " + (AirportLists[0].Iata != null ? AirportLists[0].Iata.ToString() : string.Empty);


                                        tbDepartAirportName.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);
                                        //lbDepart.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);

                                        #endregion



                                        if (Session["CurrentCorporateRequest"] != null)
                                        {
                                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                            if (CorpRequest != null)
                                            {
                                                if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                                                {
                                                    if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1] != null)
                                                    {
                                                        CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DepartAirportChanged = true;
                                                        //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DepartAirportChangedUpdateCrew = true;
                                                        //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DepartAirportChangedUpdatePAX = true;
                                                    }
                                                }
                                            }
                                            Session["CurrentCorporateRequest"] = CorpRequest;
                                        }

                                        hdnDomestic.Value = "DOM";
                                        if (!string.IsNullOrEmpty(hdArrival.Value))
                                        {
                                            var objArrRetVal = objDstsvc.GetAirportByAirportID(Convert.ToInt64(hdArrival.Value));

                                            if (objArrRetVal.ReturnFlag)
                                            {
                                                List<FlightPak.Web.FlightPakMasterService.GetAllAirport> ArrAirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                                                ArrAirportLists = objArrRetVal.EntityList;

                                                if (ArrAirportLists != null && ArrAirportLists.Count > 0)
                                                {

                                                    if (!string.IsNullOrEmpty(hdnCountryID.Value) && hdnCountryID.Value != "0.0")
                                                    {
                                                        if (ArrAirportLists[0].CountryID == null || AirportLists[0].CountryID == null)
                                                        {
                                                            hdnDomestic.Value = "DOM";
                                                        }
                                                        else
                                                        {
                                                            if (ArrAirportLists[0].CountryID != null || AirportLists[0].CountryID != null)
                                                            {
                                                                if (hdnCountryID.Value != ArrAirportLists[0].CountryID.ToString())
                                                                {
                                                                    hdnDomestic.Value = "INT";
                                                                }

                                                                if (hdnCountryID.Value != AirportLists[0].CountryID.ToString())
                                                                {
                                                                    hdnDomestic.Value = "INT";
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }


                                        #region Company Profile-Color changes in Airport textbox if Notes & Alerts are present

                                        //if (AirportLists[0].AirportID == Convert.ToInt64(hdDepart.Value))
                                        //{
                                        //    if (!string.IsNullOrEmpty(AirportLists[0].Alerts) || !string.IsNullOrEmpty(AirportLists[0].GeneralNotes))
                                        //    {
                                        //        if (UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert != null && UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert == true)
                                        //        {
                                        //            tbDepartsIcao.ForeColor = Color.Red;
                                        //            if (AirportLists[0].Alerts != null)
                                        //                tbDepartsIcao.ToolTip = AirportLists[0].Alerts;
                                        //            //if (AirportLists[0].GeneralNotes != null)
                                        //            //    tbDepartsIcao.ToolTip = AirportLists[0].GeneralNotes;
                                        //        }
                                        //        else
                                        //        {
                                        //            tbDepartsIcao.ForeColor = Color.Black;
                                        //        }
                                        //    }
                                        //    else
                                        //    {
                                        //        tbDepartsIcao.ForeColor = Color.Black;
                                        //    }
                                        //}
                                        // Fix for UW-1165(8179)
                                        string tooltipStr = string.Empty;
                                        if (UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert != null && UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert == true)
                                        {
                                            if (!string.IsNullOrEmpty(AirportLists[0].Alerts))
                                            {
                                                //TxtBx.ToolTip = AirportList[0].Alerts.ToString();
                                                string alertStr = "Alerts : \n";
                                                alertStr += AirportLists[0].Alerts;
                                                tooltipStr = alertStr;
                                                tbDepartsIcao.ForeColor = Color.Red;
                                            }
                                            if (!string.IsNullOrEmpty(AirportLists[0].GeneralNotes))
                                            {
                                                string noteStr = string.Empty;
                                                if (!string.IsNullOrEmpty(tooltipStr))
                                                    noteStr = "\n\nNotes : \n";
                                                else
                                                    noteStr = "Notes : \n";
                                                noteStr += AirportLists[0].GeneralNotes;
                                                tooltipStr += noteStr;
                                            }
                                            tbDepartsIcao.ToolTip = tooltipStr;
                                        }
                                        else
                                        {
                                            tbDepartsIcao.ForeColor = Color.Black;
                                        }
                                        #endregion
                                        if (hdnPower.Value != null)
                                            hdnPower.Value = "1";
                                        CommonCRCalculation("Airport", true, null);
                                    }
                                    else
                                    {
                                        lbcvDepartIcao.Text = "Airport Code Does Not Exist";
                                        tbDepartAirportName.Text = "";
                                        tbDepartCity.Text = "";
                                        hdDepart.Value = "";
                                        //lbDepartsICAO.Text = "";
                                        //lbDepartsCity.Text = "";
                                        //lbDepartsState.Text = "";
                                        //lbDepartsCountry.Text = "";
                                        //lbDepartsAirport.Text = "";
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartsIcao);
                                    }
                                }
                                else
                                {
                                    lbcvDepartIcao.Text = "Airport Code Does Not Exist";
                                    tbDepartAirportName.Text = "";
                                    tbDepartCity.Text = "";
                                    hdDepart.Value = "";
                                    //lbDepartsICAO.Text = "";
                                    //lbDepartsCity.Text = "";
                                    //lbDepartsState.Text = "";
                                    //lbDepartsCountry.Text = "";
                                    //lbDepartsAirport.Text = "";
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartsIcao);
                                }
                            }
                            //}
                            //else
                            //{
                            //    tbDepart.Text = "";
                            //    lbDepart.Text = "";
                            //    hdDepart.Value = "";
                            //    lbDepartsICAO.Text = string.Empty;
                            //    lbDepartsCity.Text = string.Empty;
                            //    lbDepartsState.Text = string.Empty;
                            //    lbDepartsCountry.Text = string.Empty;
                            //    lbDepartsAirport.Text = string.Empty;
                            //}
                        }
                        else
                        {
                            tbDepartAirportName.Text = "";
                            hdDepart.Value = "";
                            tbDepartCity.Text = "";
                            //lbDepartsICAO.Text = "";
                            //lbDepartsCity.Text = "";
                            //lbDepartsState.Text = "";
                            //lbDepartsCountry.Text = "";
                            //lbDepartsAirport.Text = "";
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }

        }
        protected void tbArrival_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        RadAjaxManager.GetCurrent(Page).FocusControl(btnClosestArrivalICAO);
                        lbArrivalAirportName.Text = string.Empty;
                        tbArrivalCity.Text = string.Empty;
                        lbcvArrivalIcao.Text = string.Empty;
                        tbArrivalIcao.Text = tbArrivalIcao.Text.ToUpper();
                        if (!string.IsNullOrEmpty(tbArrivalIcao.Text))
                        {

                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetAirportByAirportICaoID(tbArrivalIcao.Text.ToUpper().Trim());
                                List<FlightPakMasterService.GetAllAirport> getAllAirportList = new List<GetAllAirport>();

                                if (objRetVal.ReturnFlag)
                                {
                                    getAllAirportList = objRetVal.EntityList;

                                    if (getAllAirportList != null && getAllAirportList.Count > 0)
                                    {
                                        lbArrivalAirportName.Text = getAllAirportList[0].AirportName != null ? System.Web.HttpUtility.HtmlEncode(getAllAirportList[0].AirportName.ToUpper()) : string.Empty;
                                        tbArrivalCity.Text = getAllAirportList[0].CityName != null ? System.Web.HttpUtility.HtmlEncode(getAllAirportList[0].CityName.ToUpper()) : string.Empty;
                                        hdArrival.Value = getAllAirportList[0].AirportID.ToString();
                                        lbcvArrivalIcao.Text = "";

                                        #region Tooltip for AirportDetails

                                        string builder = string.Empty;

                                        builder = "ICAO : " + (getAllAirportList[0].IcaoID != null ? getAllAirportList[0].IcaoID : string.Empty)
                                            + "\n" + "City : " + (getAllAirportList[0].CityName != null ? getAllAirportList[0].CityName : string.Empty)
                                            + "\n" + "State/Province : " + (getAllAirportList[0].StateName != null ? getAllAirportList[0].StateName : string.Empty)
                                            + "\n" + "Country : " + (getAllAirportList[0].CountryName != null ? getAllAirportList[0].CountryName : string.Empty)
                                            + "\n" + "Airport : " + (getAllAirportList[0].AirportName != null ? getAllAirportList[0].AirportName : string.Empty)
                                            + "\n" + "DST Region : " + (getAllAirportList[0].DSTRegionCD != null ? getAllAirportList[0].DSTRegionCD : string.Empty)
                                            + "\n" + "UTC+/- : " + (getAllAirportList[0].OffsetToGMT != null ? getAllAirportList[0].OffsetToGMT.ToString() : string.Empty)
                                            + "\n" + "Longest Runway : " + (getAllAirportList[0].LongestRunway != null ? getAllAirportList[0].LongestRunway.ToString() : string.Empty)
                                             + "\n" + "IATA : " + (getAllAirportList[0].Iata != null ? getAllAirportList[0].Iata.ToString() : string.Empty);

                                        lbArrivalAirportName.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);
                                        //lbArrival.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);

                                        #endregion


                                        hdnDomestic.Value = "DOM";
                                        if (!string.IsNullOrEmpty(hdDepart.Value))
                                        {
                                            var objArrRetVal = objDstsvc.GetAirportByAirportID(Convert.ToInt64(hdDepart.Value));

                                            if (objArrRetVal.ReturnFlag)
                                            {
                                                List<FlightPak.Web.FlightPakMasterService.GetAllAirport> DepAirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                                                DepAirportLists = objArrRetVal.EntityList;

                                                if (DepAirportLists != null && DepAirportLists.Count > 0)
                                                {

                                                    if (!string.IsNullOrEmpty(hdnCountryID.Value) && hdnCountryID.Value != "0.0")
                                                    {
                                                        if (DepAirportLists[0].CountryID == null || getAllAirportList[0].CountryID == null)
                                                        {
                                                            hdnDomestic.Value = "DOM";
                                                        }
                                                        else
                                                        {
                                                            if (DepAirportLists[0].CountryID != null || getAllAirportList[0].CountryID != null)
                                                            {
                                                                if (hdnCountryID.Value != DepAirportLists[0].CountryID.ToString())
                                                                {
                                                                    hdnDomestic.Value = "INT";
                                                                }

                                                                if (hdnCountryID.Value != getAllAirportList[0].CountryID.ToString())
                                                                {
                                                                    hdnDomestic.Value = "INT";
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }


                                        #region Company Profile-Color changes in Airport textbox if Notes & Alerts are present
                                        //if (getAllAirportList[0].AirportID == Convert.ToInt64(hdArrival.Value))
                                        //{
                                        //    if (!string.IsNullOrEmpty(getAllAirportList[0].Alerts) || !string.IsNullOrEmpty(getAllAirportList[0].GeneralNotes))
                                        //    {
                                        //        if (UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert != null && UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert == true)
                                        //        {
                                        //            tbArrivalIcao.ForeColor = Color.Red;
                                        //            if (getAllAirportList[0].Alerts != null)
                                        //                tbArrivalIcao.ToolTip = getAllAirportList[0].Alerts;
                                        //            //if (getAllAirportList[0].GeneralNotes != null)
                                        //            //    tbArrivalIcao.ToolTip = getAllAirportList[0].GeneralNotes;
                                        //        }
                                        //        else
                                        //        {
                                        //            tbArrivalIcao.ForeColor = Color.Black;
                                        //        }
                                        //    }
                                        //    else
                                        //    {
                                        //        tbArrivalIcao.ForeColor = Color.Black;
                                        //    }
                                        //}

                                        // Fix for UW-1165(8179)
                                        string tooltipStr = string.Empty;
                                        if (UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert != null && UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert == true)
                                        {
                                            if (!string.IsNullOrEmpty(getAllAirportList[0].Alerts))
                                            {
                                                //TxtBx.ToolTip = AirportList[0].Alerts.ToString();
                                                string alertStr = "Alerts : \n";
                                                alertStr += getAllAirportList[0].Alerts;
                                                tooltipStr = alertStr;
                                                tbArrivalIcao.ForeColor = Color.Red;
                                            }
                                            if (!string.IsNullOrEmpty(getAllAirportList[0].GeneralNotes))
                                            {
                                                string noteStr = string.Empty;
                                                if (!string.IsNullOrEmpty(tooltipStr))
                                                    noteStr = "\n\nNotes : \n";
                                                else
                                                    noteStr = "Notes : \n";
                                                noteStr += getAllAirportList[0].GeneralNotes;
                                                tooltipStr += noteStr;
                                            }
                                            tbArrivalIcao.ToolTip = tooltipStr;
                                        }
                                        else
                                        {
                                            tbArrivalIcao.ForeColor = Color.Black;
                                        }
                                        #endregion
                                        CommonCRCalculation("Airport", true, null);

                                        if (Session["CurrentCorporateRequest"] != null)
                                        {
                                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                            if (CorpRequest != null)
                                            {
                                                if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                                                {
                                                    if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1] != null)
                                                    {
                                                        CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ArrivalAirportChanged = true;
                                                        //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ArrivalAirportChangedUpdateCrew = true;
                                                        //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ArrivalAirportChangedUpdatePAX = true;
                                                    }
                                                }
                                            }
                                            Session["CurrentCorporateRequest"] = CorpRequest;
                                        }

                                    }
                                    else
                                    {
                                        lbcvArrivalIcao.Text = System.Web.HttpUtility.HtmlEncode("Airport Code Does Not Exist");
                                        lbArrivalAirportName.Text = "";
                                        tbArrivalCity.Text = "";
                                        hdArrival.Value = "";
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbArrivalIcao);
                                        //lbArrivesICAO.Text = string.Empty;
                                        //lbArrivesCity.Text = string.Empty;
                                        //lbArrivesState.Text = string.Empty;
                                        //lbArrivesCountry.Text = string.Empty;
                                        //lbArrivesAirport.Text = string.Empty;

                                    }
                                }
                                else
                                {
                                    lbcvArrivalIcao.Text = System.Web.HttpUtility.HtmlEncode("Airport Code Does Not Exist");
                                    lbArrivalAirportName.Text = "";
                                    tbArrivalCity.Text = "";
                                    hdArrival.Value = "";
                                    //lbArrivesICAO.Text = string.Empty;
                                    //lbArrivesCity.Text = string.Empty;
                                    //lbArrivesState.Text = string.Empty;
                                    //lbArrivesCountry.Text = string.Empty;
                                    //lbArrivesAirport.Text = string.Empty;
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbArrivalIcao);
                                }
                            }
                            //}
                            //else
                            //{
                            //    tbArrivalLocal.Text = "";
                            //    lbArrivesICAO.Text = string.Empty;
                            //    lbArrivesCity.Text = string.Empty;
                            //    lbArrivesState.Text = string.Empty;
                            //    lbArrivesCountry.Text = string.Empty;
                            //    lbArrivesAirport.Text = string.Empty;
                            //}
                        }
                        else
                        {
                            lbArrivalAirportName.Text = "";
                            tbArrivalCity.Text = "";
                            hdArrival.Value = "";
                            //lbArrivesICAO.Text = string.Empty;
                            //lbArrivesCity.Text = string.Empty;
                            //lbArrivesState.Text = string.Empty;
                            //lbArrivesCountry.Text = string.Empty;
                            //lbArrivesAirport.Text = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }

        }

        protected void tbDepartCity_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnDepartsICAO);
                        tbDepartAirportName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvDepartIcao.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        tbDepartsIcao.Text = tbDepartsIcao.Text.ToUpper();
                        if (!string.IsNullOrEmpty(tbDepartCity.Text))
                        {

                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {

                                var objRetVal = objDstsvc.GetAirportByCityName(tbDepartCity.Text.ToUpper().Trim());
                                List<FlightPak.Web.FlightPakMasterService.GetAirportByCityName> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAirportByCityName>();

                                if (objRetVal.ReturnFlag)
                                {
                                    AirportLists = objRetVal.EntityList;
                                    if (AirportLists != null && AirportLists.Count == 1)
                                    {
                                        tbDepartAirportName.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].AirportName != null ? AirportLists[0].AirportName.ToUpper() : string.Empty);
                                        tbDepartCity.Text = System.Web.HttpUtility.HtmlEncode(AirportLists[0].CityName != null ? AirportLists[0].CityName.ToUpper() : string.Empty);
                                        tbDepartsIcao.Text = AirportLists[0].IcaoID != null ? System.Web.HttpUtility.HtmlEncode(AirportLists[0].IcaoID.ToUpper()) : string.Empty;
                                        hdDepart.Value = AirportLists[0].AirportID.ToString();
                                        lbcvDepartIcao.Text = System.Web.HttpUtility.HtmlEncode("");
                                        #region Tooltip for AirportDetails

                                        string builder = string.Empty;

                                        builder = "ICAO : " + (AirportLists[0].IcaoID != null ? AirportLists[0].IcaoID : string.Empty)
                                            + "\n" + "City : " + (AirportLists[0].CityName != null ? AirportLists[0].CityName : string.Empty)
                                            + "\n" + "State/Province : " + (AirportLists[0].StateName != null ? AirportLists[0].StateName : string.Empty)
                                            + "\n" + "Country : " + (AirportLists[0].CountryName != null ? AirportLists[0].CountryName : string.Empty)
                                            + "\n" + "Airport : " + (AirportLists[0].AirportName != null ? AirportLists[0].AirportName : string.Empty)
                                            + "\n" + "DST Region : " + (AirportLists[0].DSTRegionCD != null ? AirportLists[0].DSTRegionCD : string.Empty)
                                            + "\n" + "UTC+/- : " + (AirportLists[0].OffsetToGMT != null ? AirportLists[0].OffsetToGMT.ToString() : string.Empty)
                                            + "\n" + "Longest Runway : " + (AirportLists[0].LongestRunway != null ? AirportLists[0].LongestRunway.ToString() : string.Empty)
                                            + "\n" + "IATA : " + (AirportLists[0].Iata != null ? AirportLists[0].Iata.ToString() : string.Empty);


                                        tbDepartAirportName.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);
                                        //lbDepart.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);

                                        #endregion
                                        if (Session["CurrentCorporateRequest"] != null)
                                        {
                                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                            if (CorpRequest != null)
                                            {
                                                if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                                                {
                                                    if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1] != null)
                                                    {
                                                        CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DepartAirportChanged = true;
                                                        //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DepartAirportChangedUpdateCrew = true;
                                                        //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DepartAirportChangedUpdatePAX = true;
                                                    }
                                                }
                                            }
                                            Session["CurrentCorporateRequest"] = CorpRequest;
                                        }

                                        hdnDomestic.Value = "DOM";
                                        if (!string.IsNullOrEmpty(hdArrival.Value))
                                        {
                                            var objArrRetVal = objDstsvc.GetAirportByAirportID(Convert.ToInt64(hdArrival.Value));

                                            if (objArrRetVal.ReturnFlag)
                                            {
                                                List<FlightPak.Web.FlightPakMasterService.GetAllAirport> ArrAirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                                                ArrAirportLists = objArrRetVal.EntityList;

                                                if (ArrAirportLists != null && ArrAirportLists.Count > 0)
                                                {

                                                    if (!string.IsNullOrEmpty(hdnCountryID.Value) && hdnCountryID.Value != "0.0")
                                                    {
                                                        if (ArrAirportLists[0].CountryID == null || AirportLists[0].CountryID == null)
                                                        {
                                                            hdnDomestic.Value = "DOM";
                                                        }
                                                        else
                                                        {
                                                            if (ArrAirportLists[0].CountryID != null || AirportLists[0].CountryID != null)
                                                            {
                                                                if (hdnCountryID.Value != ArrAirportLists[0].CountryID.ToString())
                                                                {
                                                                    hdnDomestic.Value = "INT";
                                                                }

                                                                if (hdnCountryID.Value != AirportLists[0].CountryID.ToString())
                                                                {
                                                                    hdnDomestic.Value = "INT";
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }


                                        #region Company Profile-Color changes in Airport textbox if Notes & Alerts are present

                                        if (AirportLists[0].AirportID == Convert.ToInt64(hdDepart.Value))
                                        {
                                            if (!string.IsNullOrEmpty(AirportLists[0].Alerts) || !string.IsNullOrEmpty(AirportLists[0].GeneralNotes))
                                            {
                                                if (UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert != null && UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert == true)
                                                {
                                                    tbDepartsIcao.ForeColor = Color.Red;
                                                    if (AirportLists[0].Alerts != null)
                                                        tbDepartsIcao.ToolTip = AirportLists[0].Alerts;
                                                    //if (AirportLists[0].GeneralNotes != null)
                                                    //    tbDepartsIcao.ToolTip = AirportLists[0].GeneralNotes;
                                                }
                                                else
                                                {
                                                    tbDepartsIcao.ForeColor = Color.Black;
                                                }
                                            }
                                            else
                                            {
                                                tbDepartsIcao.ForeColor = Color.Black;
                                            }
                                        }

                                        #endregion
                                        if (hdnPower.Value != null)
                                            hdnPower.Value = "1";
                                        CommonCRCalculation("Airport", true, null);
                                    }
                                    else if (AirportLists != null && AirportLists.Count() > 1)
                                    {
                                        if (hdDepart.Value != null && hdDepart.Value.ToString().Trim() != "")
                                        {
                                            AirportLists = AirportLists.Where(x => x.AirportID == Convert.ToInt64(hdDepart.Value)).ToList();
                                            if (AirportLists.Count > 0)
                                            {
                                                if (tbDepartCity.Text.Trim() == AirportLists[0].CityName.ToString().Trim())
                                                {
                                                    tbDepart_TextChanged(null, null);
                                                    RadAjaxManager.GetCurrent(Page).FocusControl(btnClosestIcao);
                                                }
                                                else
                                                {
                                                    tbDepartAirportName.Text = "There are Cities with the Same Name.Please Choose from the lookup";
                                                    hdDepart.Value = "";
                                                    tbDepartsIcao.Text = "";
                                                    RadAjaxManager.GetCurrent(Page).FocusControl(btnClosestIcao);
                                                }
                                            }
                                            else
                                            {
                                                tbDepartAirportName.Text = "There are Cities with the Same Name.Please Choose from the lookup";
                                                hdDepart.Value = "";
                                                tbDepartsIcao.Text = "";
                                                RadAjaxManager.GetCurrent(Page).FocusControl(btnClosestIcao);
                                            }
                                        }
                                        else
                                        {
                                            tbDepartAirportName.Text = "There are Cities with the Same Name.Please Choose from the lookup";
                                            hdDepart.Value = "";
                                            tbDepartsIcao.Text = "";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(btnClosestIcao);
                                        }
                                    }
                                    else
                                    {
                                        lbcvDepartIcao.Text = "Airport City Does Not Exist";
                                        tbDepartAirportName.Text = "";
                                        hdDepart.Value = "";
                                        tbDepartsIcao.Text = "";
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartCity);
                                    }
                                }
                                else
                                {
                                    lbcvDepartIcao.Text = "Airport City Does Not Exist";
                                    tbDepartAirportName.Text = "";
                                    hdDepart.Value = "";
                                    tbDepartsIcao.Text = "";
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartCity);
                                }
                            }
                        }
                        else
                        {
                            tbDepartAirportName.Text = "";
                            hdDepart.Value = "";
                            tbDepartCity.Text = "";
                            tbDepartsIcao.Text = "";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }
        protected void tbArrivalCity_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        RadAjaxManager.GetCurrent(Page).FocusControl(btnClosestArrivalICAO);
                        lbArrivalAirportName.Text = string.Empty;
                        //tbArrivalCity.Text = string.Empty;
                        lbcvArrivalIcao.Text = string.Empty;
                        tbArrivalIcao.Text = tbArrivalIcao.Text.ToUpper();
                        if (!string.IsNullOrEmpty(tbArrivalCity.Text))
                        {
                            //if (tbArrivalLocal.Text != tbDepart.Text)
                            //{
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetAirportByCityName(tbArrivalCity.Text.ToUpper().Trim());
                                List<FlightPakMasterService.GetAirportByCityName> getAllAirportList = new List<FlightPakMasterService.GetAirportByCityName>();

                                if (objRetVal.ReturnFlag)
                                {
                                    getAllAirportList = objRetVal.EntityList;

                                    if (getAllAirportList != null && getAllAirportList.Count == 1)
                                    {
                                        lbArrivalAirportName.Text = getAllAirportList[0].AirportName != null ? System.Web.HttpUtility.HtmlEncode(getAllAirportList[0].AirportName.ToUpper()) : string.Empty;
                                        tbArrivalCity.Text = getAllAirportList[0].CityName != null ? System.Web.HttpUtility.HtmlEncode(getAllAirportList[0].CityName.ToUpper()) : string.Empty;
                                        tbArrivalIcao.Text = getAllAirportList[0].IcaoID != null ? System.Web.HttpUtility.HtmlEncode(getAllAirportList[0].IcaoID.ToUpper()) : string.Empty;
                                        hdArrival.Value = getAllAirportList[0].AirportID.ToString();
                                        lbcvArrivalIcao.Text = "";

                                        #region Tooltip for AirportDetails

                                        string builder = string.Empty;

                                        builder = "ICAO : " + (getAllAirportList[0].IcaoID != null ? getAllAirportList[0].IcaoID : string.Empty)
                                            + "\n" + "City : " + (getAllAirportList[0].CityName != null ? getAllAirportList[0].CityName : string.Empty)
                                            + "\n" + "State/Province : " + (getAllAirportList[0].StateName != null ? getAllAirportList[0].StateName : string.Empty)
                                            + "\n" + "Country : " + (getAllAirportList[0].CountryName != null ? getAllAirportList[0].CountryName : string.Empty)
                                            + "\n" + "Airport : " + (getAllAirportList[0].AirportName != null ? getAllAirportList[0].AirportName : string.Empty)
                                            + "\n" + "DST Region : " + (getAllAirportList[0].DSTRegionCD != null ? getAllAirportList[0].DSTRegionCD : string.Empty)
                                            + "\n" + "UTC+/- : " + (getAllAirportList[0].OffsetToGMT != null ? getAllAirportList[0].OffsetToGMT.ToString() : string.Empty)
                                            + "\n" + "Longest Runway : " + (getAllAirportList[0].LongestRunway != null ? getAllAirportList[0].LongestRunway.ToString() : string.Empty)
                                             + "\n" + "IATA : " + (getAllAirportList[0].Iata != null ? getAllAirportList[0].Iata.ToString() : string.Empty);

                                        lbArrivalAirportName.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);
                                        //lbArrival.ToolTip = System.Web.HttpUtility.HtmlEncode(builder);

                                        #endregion


                                        hdnDomestic.Value = "DOM";
                                        if (!string.IsNullOrEmpty(hdDepart.Value))
                                        {
                                            var objArrRetVal = objDstsvc.GetAirportByAirportID(Convert.ToInt64(hdDepart.Value));

                                            if (objArrRetVal.ReturnFlag)
                                            {
                                                List<FlightPak.Web.FlightPakMasterService.GetAllAirport> DepAirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                                                DepAirportLists = objArrRetVal.EntityList;

                                                if (DepAirportLists != null && DepAirportLists.Count > 0)
                                                {

                                                    if (!string.IsNullOrEmpty(hdnCountryID.Value) && hdnCountryID.Value != "0.0")
                                                    {
                                                        if (DepAirportLists[0].CountryID == null || getAllAirportList[0].CountryID == null)
                                                        {
                                                            hdnDomestic.Value = "DOM";
                                                        }
                                                        else
                                                        {
                                                            if (DepAirportLists[0].CountryID != null || getAllAirportList[0].CountryID != null)
                                                            {
                                                                if (hdnCountryID.Value != DepAirportLists[0].CountryID.ToString())
                                                                {
                                                                    hdnDomestic.Value = "INT";
                                                                }

                                                                if (hdnCountryID.Value != getAllAirportList[0].CountryID.ToString())
                                                                {
                                                                    hdnDomestic.Value = "INT";
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }


                                        #region Company Profile-Color changes in Airport textbox if Notes & Alerts are present
                                        if (getAllAirportList[0].AirportID == Convert.ToInt64(hdArrival.Value))
                                        {
                                            if (!string.IsNullOrEmpty(getAllAirportList[0].Alerts) || !string.IsNullOrEmpty(getAllAirportList[0].GeneralNotes))
                                            {
                                                if (UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert != null && UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert == true)
                                                {
                                                    tbArrivalIcao.ForeColor = Color.Red;
                                                    if (getAllAirportList[0].Alerts != null)
                                                        tbArrivalIcao.ToolTip = getAllAirportList[0].Alerts;
                                                    //if (getAllAirportList[0].GeneralNotes != null)
                                                    //    tbArrivalIcao.ToolTip = getAllAirportList[0].GeneralNotes;
                                                }
                                                else
                                                {
                                                    tbArrivalIcao.ForeColor = Color.Black;
                                                }
                                            }
                                            else
                                            {
                                                tbArrivalIcao.ForeColor = Color.Black;
                                            }
                                        }
                                        #endregion
                                        CommonCRCalculation("Airport", true, null);

                                        if (Session["CurrentCorporateRequest"] != null)
                                        {
                                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                            if (CorpRequest != null)
                                            {
                                                if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                                                {
                                                    if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1] != null)
                                                    {
                                                        CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ArrivalAirportChanged = true;
                                                        //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ArrivalAirportChangedUpdateCrew = true;
                                                        //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ArrivalAirportChangedUpdatePAX = true;
                                                    }
                                                }
                                            }
                                            Session["CurrentCorporateRequest"] = CorpRequest;
                                        }

                                    }
                                    else if (getAllAirportList != null && getAllAirportList.Count() > 1)
                                    {
                                        if (hdArrival.Value != null && hdArrival.Value.ToString().Trim() != "")
                                        {
                                            getAllAirportList = getAllAirportList.Where(x => x.AirportID == Convert.ToInt64(hdArrival.Value)).ToList();
                                            if (getAllAirportList.Count > 0)
                                            {
                                                if (tbArrivalCity.Text.Trim() == getAllAirportList[0].CityName.ToString().Trim())
                                                {
                                                    tbArrival_TextChanged(null, null);
                                                    RadAjaxManager.GetCurrent(Page).FocusControl(btnClosestIcao);
                                                }
                                                else
                                                {
                                                    lbArrivalAirportName.Text = "There are Cities with the Same Name.Please Choose from the lookup";
                                                    hdArrival.Value = "";
                                                    tbArrivalIcao.Text = "";
                                                    RadAjaxManager.GetCurrent(Page).FocusControl(btnArrivalAirportCity);
                                                }
                                            }
                                            else
                                            {
                                                lbArrivalAirportName.Text = "There are Cities with the Same Name.Please Choose from the lookup";
                                                hdArrival.Value = "";
                                                tbArrivalIcao.Text = "";
                                                RadAjaxManager.GetCurrent(Page).FocusControl(btnArrivalAirportCity);
                                            }
                                        }
                                        else
                                        {
                                            lbArrivalAirportName.Text = "There are Cities with the Same Name.Please Choose from the lookup";
                                            hdArrival.Value = "";
                                            tbArrivalIcao.Text = "";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(btnArrivalAirportCity);
                                        }
                                    }
                                    else
                                    {
                                        lbcvArrivalIcao.Text = System.Web.HttpUtility.HtmlEncode("Airport City Does Not Exist");
                                        lbArrivalAirportName.Text = "";
                                        hdArrival.Value = "";
                                        tbArrivalIcao.Text = "";
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbArrivalCity);
                                    }
                                }
                                else
                                {
                                    lbcvArrivalIcao.Text = System.Web.HttpUtility.HtmlEncode("Airport City Does Not Exist");
                                    lbArrivalAirportName.Text = "";
                                    hdArrival.Value = "";
                                    tbArrivalIcao.Text = "";
                                    //lbArrivesICAO.Text = string.Empty;
                                    //lbArrivesCity.Text = string.Empty;
                                    //lbArrivesState.Text = string.Empty;
                                    //lbArrivesCountry.Text = string.Empty;
                                    //lbArrivesAirport.Text = string.Empty;
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbArrivalCity);
                                }
                            }
                            //}
                            //else
                            //{
                            //    tbArrivalLocal.Text = "";
                            //    lbArrivesICAO.Text = string.Empty;
                            //    lbArrivesCity.Text = string.Empty;
                            //    lbArrivesState.Text = string.Empty;
                            //    lbArrivesCountry.Text = string.Empty;
                            //    lbArrivesAirport.Text = string.Empty;
                            //}
                        }
                        else
                        {
                            lbArrivalAirportName.Text = "";
                            hdArrival.Value = "";
                            tbArrivalCity.Text = "";
                            tbArrivalIcao.Text = "";
                            //lbArrivesICAO.Text = string.Empty;
                            //lbArrivesCity.Text = string.Empty;
                            //lbArrivesState.Text = string.Empty;
                            //lbArrivesCountry.Text = string.Empty;
                            //lbArrivesAirport.Text = string.Empty;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
            //lbcvArrivalCity.Text = string.Empty;
            //lbArrivalAirportName.Text = string.Empty;
            //if (!string.IsNullOrEmpty(tbArrivalCity.Text.Trim()))
            //{
            //    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            //    {
            //        var objRetVal = objDstsvc.GetAllAirportList().EntityList.Where(x => x.CityName != null && x.CityName.ToString().Trim().ToUpper() == (tbArrivalCity.Text.ToString().Trim().ToUpper())).ToList();
            //        if (objRetVal != null && objRetVal.Count() > 0)
            //        {
            //            tbArrivalIcao.Text = ((FlightPakMasterService.GetAllAirport)objRetVal[0]).IcaoID.ToString();
            //            tbArrivalCity.Text = ((FlightPakMasterService.GetAllAirport)objRetVal[0]).CityName.ToString();
            //            hdArrival.Value = ((FlightPakMasterService.GetAllAirport)objRetVal[0]).AirportID.ToString();

            //            if (((FlightPakMasterService.GetAllAirport)objRetVal[0]).AirportName != null)
            //                lbArrivalAirportName.Text = System.Web.HttpUtility.HtmlEncode(((FlightPakMasterService.GetAllAirport)objRetVal[0]).AirportName.ToString().ToUpper());
            //        }
            //        else
            //        {
            //            lbcvArrivalCity.Text = System.Web.HttpUtility.HtmlEncode("Invalid Airport City");
            //            tbArrivalCity.Focus();
            //        }
            //    }
            //}
        }

        #endregion


        protected void tbDepartLocalDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(hdDepart.Value))
                        {
                            ValidateDateFormat(tbDepartsLocal);
                            if (!string.IsNullOrEmpty(tbDepartsLocal.Text))
                            {

                                if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat != null && UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 2)
                                {
                                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                    {


                                        DateTime dt = new DateTime();
                                        DateTime ldGmtDep = new DateTime();
                                        //rmbDepartsLocal.Text = "0000";
                                        string StartTime = rmbDepartsLocal.Text.Trim();
                                        if (StartTime.Trim() == "")
                                        {
                                            StartTime = "0000";
                                        }
                                        int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                        int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                                        bool calculate = true;
                                        if (StartHrs > 23)
                                        {
                                            RadWindowManager1.RadAlert("Hour entry must be between 0 and 23", 330, 100, "Request Alert", null);
                                            calculate = false;
                                            rmbDepartsLocal.Text = "0000";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmbDepartsLocal);

                                        }
                                        else if (StartMts > 59)
                                        {
                                            RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, "Request Alert", null);
                                            calculate = false;
                                            rmbDepartsLocal.Text = "0000";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmbDepartsLocal);
                                        }

                                        if (calculate)
                                        {
                                            dt = DateTime.ParseExact(tbDepartsLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);

                                            if (sender is TextBox)
                                            {
                                                if (((TextBox)sender).ID.ToLower() == "tbdepartslocal")
                                                    Master.UpdateLegDates((int)CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM, dt);
                                            }

                                            dt = dt.AddHours(StartHrs);
                                            dt = dt.AddMinutes(StartMts);


                                            //GMTDept
                                            ldGmtDep = objDstsvc.GetGMT(Convert.ToInt64(hdDepart.Value), dt, true, false);

                                            string startHrs = "0000" + ldGmtDep.Hour.ToString();
                                            string startMins = "0000" + ldGmtDep.Minute.ToString();
                                            tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldGmtDep);
                                            rmtUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                            CommonCRCalculation("Date", true, dt);
                                            if (sender is TextBox)
                                            {
                                                if (((TextBox)sender).ID.ToLower() == "tbdepartslocal")
                                                    RadAjaxManager.GetCurrent(Page).FocusControl(rmbDepartsLocal);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                    {


                                        DateTime dt = new DateTime();
                                        DateTime ldGmtDep = new DateTime();
                                        //rmbDepartsLocal.Text = "0000";
                                        string StartTime = rmbDepartsLocal.Text.Trim();
                                        if (StartTime.Trim() == "")
                                        {
                                            StartTime = "0000";
                                        }
                                        int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                        int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                                        bool calculate = true;
                                        if (StartHrs > 12)
                                        {
                                            RadWindowManager1.RadAlert("Hour entry must be between 0 and 12", 330, 100, "Request Alert", null);
                                            calculate = false;
                                            rmbDepartsLocal.Text = "0000";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmbDepartsLocal);

                                        }
                                        else if (StartMts > 59)
                                        {
                                            RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, "Request Alert", null);
                                            calculate = false;
                                            rmbDepartsLocal.Text = "0000";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(rmbDepartsLocal);
                                        }
                                        //else if (StartHrs > 11 && StartMts > 0)
                                        //{
                                        //    RadWindowManager1.RadAlert("Hour should not exceed 12", 330, 100, "Request Alert", null);
                                        //    calculate = false;
                                        //    rmbDepartsLocal.Text = "0000";
                                        //    RadAjaxManager.GetCurrent(Page).FocusControl(rmbDepartsLocal);
                                        //}
                                        if (calculate)
                                        {
                                            dt = DateTime.ParseExact(tbDepartsLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);

                                            if (sender is TextBox)
                                            {
                                                if (((TextBox)sender).ID.ToLower() == "tbdepartslocal")
                                                    Master.UpdateLegDates((int)CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM, dt);
                                            }
                                            if (DepTimeForm.SelectedValue == "2")
                                            {
                                                if (StartHrs != 12)
                                                    dt = dt.AddHours(StartHrs + 12);
                                                else
                                                    dt = dt.AddHours(StartHrs);
                                            }
                                            else
                                            {

                                                if (StartHrs != 12)
                                                    dt = dt.AddHours(StartHrs);
                                                else
                                                    dt = dt.AddHours(0);
                                            }
                                            dt = dt.AddMinutes(StartMts);
                                            //GMTDept
                                            ldGmtDep = objDstsvc.GetGMT(Convert.ToInt64(hdDepart.Value), dt, true, false);

                                            string startHrs = "0000" + ldGmtDep.Hour.ToString();
                                            string startMins = "0000" + ldGmtDep.Minute.ToString();
                                            tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldGmtDep);
                                            rmtUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                            CommonCRCalculation("Date", true, dt);
                                            if (sender is TextBox)
                                            {
                                                if (((TextBox)sender).ID.ToLower() == "tbdepartslocal")
                                                    RadAjaxManager.GetCurrent(Page).FocusControl(rmbDepartsLocal);
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }
        protected void tbArrivalLocalDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //if (!string.IsNullOrEmpty(hdArrival.Value))
                        //{
                        ValidateDateFormat(tbArrivalLocal);
                        if (!string.IsNullOrEmpty(tbArrivalLocal.Text))
                        {
                            if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat != null && UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 2)
                            {
                                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                {


                                    DateTime dt = new DateTime();
                                    DateTime ldGmtArr = new DateTime();
                                    string StartTime = rmbArrivalLocalTime.Text.Trim();
                                    if (StartTime.Trim() == "")
                                    {
                                        StartTime = "0000";
                                    }
                                    int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                    int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                                    bool calculate = true;
                                    if (StartHrs > 23)
                                    {
                                        RadWindowManager1.RadAlert("Hour entry must be between 0 and 23", 330, 100, "Request Alert", null);
                                        calculate = false;
                                        rmbArrivalLocalTime.Text = "0000";
                                        RadAjaxManager.GetCurrent(Page).FocusControl(rmbArrivalLocalTime);

                                    }
                                    else if (StartMts > 59)
                                    {
                                        RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, "Request Alert", null);
                                        calculate = false;
                                        rmbArrivalLocalTime.Text = "0000";
                                        RadAjaxManager.GetCurrent(Page).FocusControl(rmbArrivalLocalTime);
                                    }
                                    if (calculate)
                                    {
                                        dt = DateTime.ParseExact(tbArrivalLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                        if (sender is TextBox)
                                        {
                                            if (((TextBox)sender).ID.ToLower() == "tbarrivallocal")
                                                Master.UpdateLegDates((int)CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM, dt);
                                        }
                                        dt = dt.AddHours(StartHrs);
                                        dt = dt.AddMinutes(StartMts);
                                        //GMTDept
                                        if (hdArrival.Value != string.Empty)
                                            ldGmtArr = objDstsvc.GetGMT(Convert.ToInt64(hdArrival.Value), dt, true, false);

                                        string startHrs = "0000" + ldGmtArr.Hour.ToString();
                                        string startMins = "0000" + ldGmtArr.Minute.ToString();
                                        tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldGmtArr);
                                        rmtArrivalUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                        CommonCRCalculation("Date", false, dt);

                                        if (sender is TextBox)
                                        {
                                            if (((TextBox)sender).ID.ToLower() == "tbarrivallocal")
                                            {
                                                RadAjaxManager.GetCurrent(Page).FocusControl(rmbArrivalLocalTime);
                                            }
                                        }

                                        //if (sender is RadMaskedTextBox)
                                        //{
                                        //    if (((RadMaskedTextBox)sender).ID.ToLower() == "rmtarrivaltime")
                                        //    {
                                        //        RadAjaxManager.GetCurrent(Page).FocusControl(tbArrivalUtcDate);
                                        //    }
                                        //}
                                    }
                                }
                            }
                            else
                            {
                                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                {


                                    DateTime dt = new DateTime();

                                    DateTime ldGmtArr = new DateTime();
                                    string StartTime = rmbArrivalLocalTime.Text.Trim();
                                    if (StartTime.Trim() == "")
                                    {
                                        StartTime = "0000";
                                    }
                                    int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                    int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                                    bool calculate = true;
                                    if (StartHrs > 12)
                                    {
                                        RadWindowManager1.RadAlert("Hour entry must be between 0 and 12", 330, 100, "Request Alert", null);
                                        calculate = false;
                                        rmbArrivalLocalTime.Text = "0000";
                                        RadAjaxManager.GetCurrent(Page).FocusControl(rmbArrivalLocalTime);

                                    }
                                    else if (StartMts > 59)
                                    {
                                        RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, "Request Alert", null);
                                        calculate = false;
                                        rmbArrivalLocalTime.Text = "0000";
                                        RadAjaxManager.GetCurrent(Page).FocusControl(rmbArrivalLocalTime);
                                    }
                                    if (calculate)
                                    {
                                        dt = DateTime.ParseExact(tbArrivalLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                        if (sender is TextBox)
                                        {
                                            if (((TextBox)sender).ID.ToLower() == "tbarrivallocal")
                                                Master.UpdateLegDates((int)CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM, dt);
                                        }

                                        if (ArrTimeForm.SelectedValue == "2")
                                        {
                                            if (StartHrs != 12)
                                                dt = dt.AddHours(StartHrs + 12);
                                            else
                                                dt = dt.AddHours(StartHrs);
                                        }
                                        else
                                        {

                                            if (StartHrs != 12)
                                                dt = dt.AddHours(StartHrs);
                                            else
                                                dt = dt.AddHours(0);
                                        }

                                        //GMTDept
                                        if (hdArrival.Value != string.Empty)
                                            ldGmtArr = objDstsvc.GetGMT(Convert.ToInt64(hdArrival.Value), dt, true, false);

                                        string startHrs = "0000" + ldGmtArr.Hour.ToString();
                                        string startMins = "0000" + ldGmtArr.Minute.ToString();
                                        tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldGmtArr);
                                        rmtArrivalUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                        CommonCRCalculation("Date", false, dt);

                                        if (sender is TextBox)
                                        {
                                            if (((TextBox)sender).ID.ToLower() == "tbarrivallocal")
                                            {
                                                RadAjaxManager.GetCurrent(Page).FocusControl(rmbArrivalLocalTime);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //}
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }
        protected void tbPassenger_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnPassenger);
                        lbPassengerName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvPaxValidator.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        if (!string.IsNullOrEmpty(tbPassenger.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.Passenger> passengerLists = new List<FlightPak.Web.FlightPakMasterService.Passenger>();

                                var objRetVal = objDstsvc.GetPassengerRequestorList();

                                if (objRetVal.ReturnFlag)
                                {
                                    passengerLists = objRetVal.EntityList.Where(x => x.PassengerRequestorCD.ToString().ToUpper().Trim().Equals(tbPassenger.Text.ToUpper().Trim())).ToList();
                                    if (passengerLists != null && passengerLists.Count > 0)
                                    {
                                        lbPassengerName.Text = System.Web.HttpUtility.HtmlEncode(((FlightPak.Web.FlightPakMasterService.Passenger)passengerLists[0]).PassengerName);
                                        hdnPaxId.Value = ((FlightPak.Web.FlightPakMasterService.Passenger)passengerLists[0]).PassengerRequestorID.ToString();
                                        tbPassenger.Text = ((FlightPak.Web.FlightPakMasterService.Passenger)passengerLists[0]).PassengerRequestorCD;

                                        lbcvPaxValidator.Text = "";

                                        Int64 DepartmentID = 0;
                                        Int64 DepartmentAuthorizationID = 0;

                                        if (!string.IsNullOrEmpty(hdIsDepartAuthReq.Value))
                                        {
                                            if (hdIsDepartAuthReq.Value.ToLower() == "true")
                                            {
                                                DepartmentID = Convert.ToInt64(((FlightPak.Web.FlightPakMasterService.Passenger)passengerLists[0]).DepartmentID);
                                                DepartmentAuthorizationID = Convert.ToInt64(((FlightPak.Web.FlightPakMasterService.Passenger)passengerLists[0]).AuthorizationID);

                                                if (DepartmentID != 0)
                                                {
                                                    FlightPakMasterService.Department dept = GetDepartment(DepartmentID);
                                                    if (dept != null)
                                                    {
                                                        tbDepartment.Text = dept.DepartmentCD;
                                                        lbDepartmentName.Text = System.Web.HttpUtility.HtmlEncode(dept.DepartmentName);
                                                        hdDepartment.Value = dept.DepartmentID.ToString();
                                                    }
                                                    else
                                                    {
                                                        tbDepartment.Text = string.Empty;
                                                        lbDepartmentName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                                        hdDepartment.Value = string.Empty;
                                                    }
                                                }
                                                else
                                                {
                                                    tbDepartment.Text = string.Empty;
                                                    lbDepartmentName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                                    hdDepartment.Value = string.Empty;
                                                }

                                                if (DepartmentAuthorizationID != 0)
                                                {
                                                    FlightPakMasterService.DepartmentAuthorization deptAuth = GetAuthorization(DepartmentAuthorizationID);
                                                    if (deptAuth != null)
                                                    {
                                                        tbAuthorizationCode.Text = deptAuth.AuthorizationCD;
                                                        lbAuthName.Text = System.Web.HttpUtility.HtmlEncode(deptAuth.DeptAuthDescription);
                                                        hdnAuthorizationId.Value = deptAuth.AuthorizationID.ToString();
                                                    }
                                                    else
                                                    {
                                                        tbAuthorizationCode.Text = string.Empty;
                                                        lbAuthName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                                        hdnAuthorizationId.Value = string.Empty;
                                                    }
                                                }
                                                else
                                                {
                                                    tbAuthorizationCode.Text = string.Empty;
                                                    lbAuthName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                                    hdnAuthorizationId.Value = string.Empty;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        lbcvPaxValidator.Text = System.Web.HttpUtility.HtmlEncode("Passenger/Requestor Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbPassenger);
                                    }
                                }
                                else
                                {
                                    lbcvPaxValidator.Text = System.Web.HttpUtility.HtmlEncode("Passenger/Requestor Code Does Not Exist");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbPassenger);
                                }
                            }
                        }
                        else
                        {
                            lbPassengerName.Text = System.Web.HttpUtility.HtmlEncode("");
                            hdnPaxId.Value = "";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }
        private FlightPak.Web.FlightPakMasterService.Department GetDepartment(Int64 DepartmentID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPak.Web.FlightPakMasterService.Department Dept = new FlightPakMasterService.Department();

                    var objDepartment = objDstsvc.GetDepartmentList();
                    if (objDepartment.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.Department> departmentcode = (from department in objDepartment.EntityList
                                                                                                where department.DepartmentID == DepartmentID
                                                                                                select department).ToList();
                        if (departmentcode != null && departmentcode.Count > 0)
                            Dept = departmentcode[0];
                        else
                            Dept = null;
                    }
                    return Dept;
                }
            }

        }
        private FlightPak.Web.FlightPakMasterService.DepartmentAuthorization GetAuthorization(Int64 AuthorizationID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AuthorizationID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.DepartmentAuthorization Auth = new FlightPakMasterService.DepartmentAuthorization();


                    var objAuthorization = objDstsvc.GetDepartmentAuthorizationList();
                    if (objAuthorization.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.DepartmentAuthorization> authorizationcode = (from Authorization in objAuthorization.EntityList
                                                                                                                where Authorization.AuthorizationID == AuthorizationID
                                                                                                                select Authorization).ToList();
                        if (authorizationcode != null && authorizationcode.Count > 0)
                            Auth = authorizationcode[0];
                        else
                            Auth = null;
                    }
                    return Auth;
                }
            }

        }
        protected void tbAuthorization_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (!string.IsNullOrEmpty(hdDepartment.Value))
                        {
                            RadAjaxManager.GetCurrent(Page).FocusControl(btnAuthorizationCode);
                            lbAuthName.Text = string.Empty;
                            lbcvAuthID.Text = string.Empty;
                            if (!string.IsNullOrEmpty(tbAuthorizationCode.Text))
                            {
                                List<FlightPak.Web.FlightPakMasterService.DepartmentAuthorization> DepartmentAuthorizationList = new List<FlightPakMasterService.DepartmentAuthorization>();
                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objDstsvc.GetDepartmentAuthorizationList();
                                    if (objRetVal.ReturnFlag)
                                    {
                                        DepartmentAuthorizationList = objRetVal.EntityList.Where(x => x.AuthorizationCD.ToString().ToUpper().Trim().Equals(tbAuthorizationCode.Text.ToUpper().Trim()) && x.DepartmentID == Convert.ToInt64(hdDepartment.Value)).ToList();

                                        if (DepartmentAuthorizationList != null && DepartmentAuthorizationList.Count > 0)
                                        {
                                            lbAuthName.Text = System.Web.HttpUtility.HtmlEncode(DepartmentAuthorizationList[0].DeptAuthDescription);
                                            hdnAuthorizationId.Value = DepartmentAuthorizationList[0].AuthorizationID.ToString();
                                            lbcvAuthID.Text = System.Web.HttpUtility.HtmlEncode("");
                                        }
                                        else
                                        {
                                            lbcvAuthID.Text = System.Web.HttpUtility.HtmlEncode("Invalid Authorization Code");
                                            hdnAuthorizationId.Value = "";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbAuthorizationCode);
                                        }
                                    }
                                    else
                                    {
                                        lbcvAuthID.Text = System.Web.HttpUtility.HtmlEncode("Invalid Authorization Code");
                                        hdnAuthorizationId.Value = "";
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbAuthorizationCode);
                                    }
                                }
                            }
                            else
                            {
                                lbAuthName.Text = "";
                                hdnAuthorizationId.Value = "";
                                //hdDept.Value = "";
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(tbAuthorizationCode.Text))
                                lbcvAuthID.Text = "Department Not Selected";
                            else
                                lbcvAuthID.Text = "";
                            hdnAuthorizationId.Value = "";
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbAuthorizationCode);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }


        }
        protected void tbDepartmentId_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnDepartment);
                        lbDepartmentName.Text = string.Empty;
                        lbcvDepartmentName.Text = string.Empty;
                        if (!string.IsNullOrEmpty(tbDepartment.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.GetAllDeptAuth> DepartmentAuthorizationLists = new List<FlightPak.Web.FlightPakMasterService.GetAllDeptAuth>();
                                var objRetVal = objDstsvc.GetAllDeptAuthorizationList();

                                if (objRetVal.ReturnFlag)
                                {
                                    DepartmentAuthorizationLists = objRetVal.EntityList.Where(x => x.DepartmentCD.ToString().ToUpper().Trim().Equals(tbDepartment.Text.ToUpper().Trim())).ToList();
                                    if (DepartmentAuthorizationLists != null && DepartmentAuthorizationLists.Count > 0)
                                    {
                                        lbDepartmentName.Text = System.Web.HttpUtility.HtmlEncode(((FlightPak.Web.FlightPakMasterService.GetAllDeptAuth)DepartmentAuthorizationLists[0]).DepartmentName);
                                        hdDepartment.Value = ((FlightPak.Web.FlightPakMasterService.GetAllDeptAuth)DepartmentAuthorizationLists[0]).DepartmentID.ToString();
                                        tbDepartment.Text = ((FlightPak.Web.FlightPakMasterService.GetAllDeptAuth)DepartmentAuthorizationLists[0]).DepartmentCD;
                                        lbcvDepartmentName.Text = "";
                                        tbAuthorizationCode.Text = "";
                                        lbAuthName.Text = "";
                                        hdnAuthorizationId.Value = "";
                                        lbcvAuthID.Text = "";
                                    }
                                    else
                                    {
                                        lbcvDepartmentName.Text = System.Web.HttpUtility.HtmlEncode("Invalid Department Code");
                                        lbDepartmentName.Text = "";
                                        hdDepartment.Value = "";
                                        tbAuthorizationCode.Text = "";
                                        lbAuthName.Text = "";
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartment);
                                    }
                                }
                                else
                                {
                                    lbcvDepartmentName.Text = System.Web.HttpUtility.HtmlEncode("Invalid Department Code");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartment);
                                    lbDepartmentName.Text = "";
                                    hdDepartment.Value = "";
                                    tbAuthorizationCode.Text = "";
                                    lbAuthName.Text = "";
                                    hdnAuthorizationId.Value = "";
                                }
                            }
                        }
                        else
                        {
                            lbDepartmentName.Text = "";
                            hdDepartment.Value = "";
                            tbAuthorizationCode.Text = "";
                            lbAuthName.Text = "";
                            lbcvAuthID.Text = "";
                            lbcvDepartmentName.Text = "";
                            hdnAuthorizationId.Value = "";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }

        }
        protected void tbFlightCategory_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnFlightCategory);
                        lbcvflightCategory.Text = string.Empty;
                        if (!string.IsNullOrEmpty(tbFlightCategoryCode.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.FlightCatagory> CrewDtyRuleLists = new List<FlightPak.Web.FlightPakMasterService.FlightCatagory>();
                                // var objRetVal = objDstsvc.GetFlightCategoryList().EntityList.Where(x => x.FlightCatagoryCD.ToString().ToUpper().Trim().Equals(tbFlightCategoryCode.Text.ToUpper().Trim())).ToList();
                                var objRetVal = objDstsvc.GetFlightCategoryList();
                                if (objRetVal.ReturnFlag)
                                {
                                    CrewDtyRuleLists = objRetVal.EntityList.Where(x => x.FlightCatagoryCD.ToString().ToUpper().Trim().Equals(tbFlightCategoryCode.Text.ToUpper().Trim())).ToList();
                                }

                                if (CrewDtyRuleLists != null && CrewDtyRuleLists.Count > 0)
                                {
                                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                    if (CorpRequest.CRLegs != null)
                                    {
                                        if (CrewDtyRuleLists[0].IsDeadorFerryHead == true)
                                        {
                                            if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CRPassengers != null)
                                            {
                                                if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CRPassengers.Count > 0)
                                                {
                                                    hdnDeadCategory.Value = ((FlightPak.Web.FlightPakMasterService.FlightCatagory)CrewDtyRuleLists[0]).FlightCategoryID.ToString();
                                                    lbcvflightCategory.Text = "";
                                                    lbFlightCategoryName.Text = System.Web.HttpUtility.HtmlEncode(((FlightPak.Web.FlightPakMasterService.FlightCatagory)CrewDtyRuleLists[0]).FlightCatagoryDescription.ToString());
                                                    tbFlightCategoryCode.Text = ((FlightPak.Web.FlightPakMasterService.FlightCatagory)CrewDtyRuleLists[0]).FlightCatagoryCD;
                                                    RadWindowManager1.RadConfirm("Passengers exists for this leg will be deleted, Do you want to continue changing the Flight Category?", "confirmDeadHeadLegCallBackFn", 330, 100, null, "Confirmation!");

                                                }
                                            }
                                        }
                                        else
                                        {
                                            hdCategory.Value = ((FlightPak.Web.FlightPakMasterService.FlightCatagory)CrewDtyRuleLists[0]).FlightCategoryID.ToString();
                                            lbFlightCategoryName.Text = Microsoft.Security.Application.Encoder.HtmlEncode(((FlightPak.Web.FlightPakMasterService.FlightCatagory)CrewDtyRuleLists[0]).FlightCatagoryDescription.ToString());
                                            lbcvflightCategory.Text = "";
                                        }
                                    }
                                    else
                                    {
                                        hdCategory.Value = ((FlightPak.Web.FlightPakMasterService.FlightCatagory)CrewDtyRuleLists[0]).FlightCategoryID.ToString();
                                        lbFlightCategoryName.Text = System.Web.HttpUtility.HtmlEncode(((FlightPak.Web.FlightPakMasterService.FlightCatagory)CrewDtyRuleLists[0]).FlightCatagoryDescription.ToString());
                                        lbcvflightCategory.Text = "";
                                    }
                                }
                                else
                                {
                                    lbcvflightCategory.Text = "Invalid Flight Category Code";
                                    lbFlightCategoryName.Text = "";
                                    hdCategory.Value = "";
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbFlightCategoryCode);
                                }
                            }
                        }
                        else
                        {
                            lbcvflightCategory.Text = "";
                            hdCategory.Value = "";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }

        }
        #endregion

        #region "Check Changed Events"
        protected void chkPrivate_CheckedChanged(object sender, EventArgs e)
        {

        }
        #endregion

        #region "Button Click Events"
        protected void btnEditTrip_Click(object sender, EventArgs e)
        {
            Master.EditRequestEvent();
        }
        protected void btnAddLeg_Click(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (IsAuthorized(Permission.CorporateRequest.AddCRLeg))
                        {

                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                SaveCRToSession();
                                validateandrebindmaster();
                                ClearFields();
                                CRLeg CurrLeg = new CRLeg();
                                CurrLeg.State = CorporateRequestTripEntityState.Added;

                                CurrLeg.LegNUM = GetLegnumToUpdate(CorpRequest, "Add");
                                GetCurrentOrNextLeg(CorpRequest, "Add");

                                CorporateRequestService.Airport DepTArr = new CorporateRequestService.Airport();
                                if (!string.IsNullOrEmpty(hdDepart.Value))
                                {
                                    Int64 AirportID = 0;
                                    if (Int64.TryParse(hdDepart.Value, out AirportID))
                                    {
                                        DepTArr.AirportID = AirportID;
                                        DepTArr.IcaoID = tbDepartsIcao.Text;
                                        DepTArr.AirportName = tbDepartAirportName.Text;
                                        DepTArr.CityName = tbDepartCity.Text;
                                        CurrLeg.Airport1 = DepTArr;
                                        CurrLeg.DAirportID = AirportID;
                                    }
                                    else
                                    {
                                        CurrLeg.Airport1 = null;
                                        CurrLeg.DAirportID = null;
                                    }
                                }
                                else
                                {
                                    CurrLeg.Airport1 = null;
                                    CurrLeg.DAirportID = null;
                                }

                                CorporateRequestService.Airport ArrvArr = new CorporateRequestService.Airport();
                                if (!string.IsNullOrEmpty(hdArrival.Value))
                                {
                                    Int64 AirportArriveID = 0;
                                    if (Int64.TryParse(hdArrival.Value, out AirportArriveID))
                                    {
                                        ArrvArr.AirportID = AirportArriveID;
                                        ArrvArr.IcaoID = tbArrivalIcao.Text;
                                        ArrvArr.AirportName = lbArrivalAirportName.Text;
                                        ArrvArr.CityName = tbArrivalCity.Text;
                                        CurrLeg.Airport = ArrvArr;
                                        CurrLeg.AAirportID = AirportArriveID;

                                    }
                                    else
                                    {
                                        CurrLeg.Airport = null;
                                        CurrLeg.AAirportID = null;
                                    }
                                }
                                else
                                {
                                    CurrLeg.Airport = null;
                                    CurrLeg.AAirportID = null;
                                }


                                if (!string.IsNullOrEmpty(tbDepartsLocal.Text))
                                {
                                    string StartTime = rmbDepartsLocal.Text.Trim();
                                    if (StartTime.Trim() == "")
                                    {
                                        StartTime = "0000";
                                    }
                                    int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                    int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                                    DateTime dt = DateTime.ParseExact(tbDepartsLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat != null && UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 2)
                                    {

                                    }
                                    else
                                    {
                                        ////if (DepTimeForm.SelectedValue == "2")
                                        ////{
                                        ////    StartHrs = StartHrs + 12;
                                        ////}
                                    }
                                    dt = dt.AddHours(StartHrs);
                                    dt = dt.AddMinutes(StartMts);
                                    CurrLeg.DepartureDTTMLocal = dt;
                                }


                                //CurrLeg.DepartsLocalhhmm = tbLocaltime.Text;

                                if (!string.IsNullOrEmpty(tbUtcDate.Text))
                                {
                                    string Timestr = rmtUtctime.Text.Trim();
                                    int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                    int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                    DateTime dt = DateTime.ParseExact(tbUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    dt = dt.AddHours(Hrst);
                                    dt = dt.AddMinutes(Mtstr);
                                    CurrLeg.DepartureGreenwichDTTM = dt;
                                }
                                //CurrLeg.DepartsUTChhmm = tbUtctime.Text;

                                if (!string.IsNullOrEmpty(tbHomeDate.Text))
                                {
                                    string Timestr = rmtHomeTime.Text.Trim();
                                    int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                    int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                    DateTime dt = DateTime.ParseExact(tbHomeDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    dt = dt.AddHours(Hrst);
                                    dt = dt.AddMinutes(Mtstr);
                                    CurrLeg.HomeDepartureDTTM = dt;
                                }

                                if (!string.IsNullOrEmpty(tbArrivalLocal.Text))
                                {
                                    string Timestr = rmbArrivalLocalTime.Text.Trim();
                                    int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                    int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                    DateTime dt = DateTime.ParseExact(tbArrivalLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat != null && UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 2)
                                    {

                                    }
                                    else
                                    {
                                        ////if (ArrTimeForm.SelectedValue == "2")
                                        ////{
                                        ////    Hrst = Hrst + 12;
                                        ////}
                                    }
                                    dt = dt.AddHours(Hrst);
                                    dt = dt.AddMinutes(Mtstr);
                                    CurrLeg.ArrivalDTTMLocal = dt;
                                    CurrLeg.NextLocalDTTM = dt;
                                }


                                if (!string.IsNullOrEmpty(tbArrivalUtcDate.Text))
                                {
                                    string Timestr = rmtArrivalUtctime.Text.Trim();
                                    int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                    int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                    DateTime dt = DateTime.ParseExact(tbArrivalUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    dt = dt.AddHours(Hrst);
                                    dt = dt.AddMinutes(Mtstr);
                                    CurrLeg.ArrivalGreenwichDTTM = dt;
                                }

                                //CurrLeg.ArrivesUTChhmm = tbArrivalUtctime.Text;

                                if (!string.IsNullOrEmpty(tbArrivalHomeDate.Text))
                                {
                                    string Timestr = rmtArrivalHomeTime.Text.Trim();
                                    int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                    int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                    DateTime dt = DateTime.ParseExact(tbArrivalHomeDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    dt = dt.AddHours(Hrst);
                                    dt = dt.AddMinutes(Mtstr);
                                    CurrLeg.HomeArrivalDTTM = dt;
                                }





                                if (CorpRequest.ClientID != null && CorpRequest.ClientID != 0)
                                {

                                    CorporateRequestService.Client PrefClient = new CorporateRequestService.Client();
                                    PrefClient.ClientID = CorpRequest.Client.ClientID;
                                    PrefClient.ClientCD = CorpRequest.Client.ClientCD;
                                    PrefClient.ClientDescription = CorpRequest.Client.ClientDescription;
                                    CurrLeg.ClientID = CorpRequest.Client.ClientID;
                                }

                                if (CorpRequest.Fleet != null)
                                {
                                    if (CorpRequest.Fleet.MaximumPassenger != null && CorpRequest.Fleet.MaximumPassenger > 0)
                                    {
                                        if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                                        {

                                            CurrLeg.SeatTotal = Convert.ToInt32(CorpRequest.Fleet.MaximumPassenger);
                                            CurrLeg.ReservationTotal = 0;
                                            CurrLeg.PassengerTotal = 0;
                                            CurrLeg.ReservationAvailable = CurrLeg.SeatTotal - CurrLeg.PassengerTotal;
                                            CurrLeg.WaitNUM = 0;
                                        }
                                    }

                                }

                                if (CorpRequest.IsPrivate != null)
                                {
                                    if (CorpRequest.IsPrivate != null && CorpRequest.IsPrivate == true)
                                    {
                                        if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                                        {

                                            CurrLeg.IsPrivate = true;
                                        }
                                    }

                                }

                                if (UserPrincipal.Identity._clientId != null)
                                {
                                    FlightPakMasterService.Client client = new FlightPakMasterService.Client();
                                    client = GetClient((long)UserPrincipal.Identity._clientId);
                                    if (client != null)
                                    {
                                        CorporateRequestService.Client PrefClient = new CorporateRequestService.Client();
                                        PrefClient.ClientID = client.ClientID;
                                        PrefClient.ClientCD = client.ClientCD;
                                        PrefClient.ClientDescription = client.ClientDescription;
                                        CurrLeg.ClientID = client.ClientID;
                                        //CurrLeg.ClientID = PrefClient;
                                        //tbClient.ReadOnly = true;
                                    }
                                    else
                                    {
                                        //tbClient.ReadOnly = false;
                                    }
                                }



                                CorpRequest.CRLegs.Add(CurrLeg);
                                hdnLeg.Value = CorpRequest.CRLegs.Count().ToString();
                                LoadLegDetails(CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1]);

                                BindCRTabs(CorpRequest, "AddLeg");


                                Session["CurrentCorporateRequest"] = CorpRequest;
                                Master.TrackerLegs.Rebind();
                                ClearLabels();
                                if (UserPrincipal.Identity._fpSettings._CrewDutyID != null)
                                    hdCrewRules.Value = Convert.ToString((long)UserPrincipal.Identity._fpSettings._CrewDutyID);
                                SetCrewDutyTypeAndFARRules();
                                SetCategory();
                                setAircraftSettings();
                                SetWindReliability();
                                CopyPrevLegDetails((long)CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM, true);
                                SetCopyInfo(CorpRequest);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }


        private void ClearLabels()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //lbcvAccountNo.Text = "";
                lbcvArrivalCity.Text = "";
                lbcvAuthID.Text = "";
                lbcvflightCategory.Text = "";
                lbcvDepartCity.Text = "";
                lbcvDepartmentName.Text = "";
                lbcvPaxValidator.Text = "";
                lbcvArrivalIcao.Text = "";
                lbcvDepartIcao.Text = "";

                lbAuthName.Text = "";
                lbDepartmentName.Text = "";
                lbPassengerName.Text = "";
                lbFlightCategoryName.Text = "";
            }
        }

        protected void SetCrewDutyTypeAndFARRules()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string CrewCode = string.Empty;
                if (hdSetCrewRules.Value.ToLower() == "true" && !string.IsNullOrEmpty(hdCrewRules.Value))
                    if (!string.IsNullOrEmpty(hdCrewRules.Value))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPak.Web.FlightPakMasterService.CrewDutyRules> CrewDtyRuleList = new List<FlightPak.Web.FlightPakMasterService.CrewDutyRules>();
                            var objRetVal = objDstsvc.GetCrewDutyRuleList();
                            if (objRetVal.ReturnFlag)
                            {
                                CrewDtyRuleList = objRetVal.EntityList.Where(x => x.CrewDutyRulesID.ToString().ToUpper().Trim().Equals(hdCrewRules.Value.Trim())).ToList();
                                if (CrewDtyRuleList != null && CrewDtyRuleList.Count > 0)
                                {
                                    CrewCode = CrewDtyRuleList[0].CrewDutyRuleCD == null ? string.Empty : CrewDtyRuleList[0].CrewDutyRuleCD;

                                    tbCrewRules.Value = CrewDtyRuleList[0].CrewDutyRuleCD == null ? string.Empty : CrewDtyRuleList[0].CrewDutyRuleCD;

                                    hdCrewRules.Value = CrewDtyRuleList[0].CrewDutyRulesID == null ? string.Empty : CrewDtyRuleList[0].CrewDutyRulesID.ToString();
                                    this.lbFar.Value = CrewDtyRuleList[0].FedAviatRegNum == null ? string.Empty : System.Web.HttpUtility.HtmlEncode(CrewDtyRuleList[0].FedAviatRegNum.ToString());

                                }
                                else
                                {
                                    tbCrewRules.Value = string.Empty;
                                    this.lbFar.Value = string.Empty;
                                    hdCrewRules.Value = string.Empty;
                                }
                            }
                            else
                            {
                                tbCrewRules.Value = string.Empty;
                                this.lbFar.Value = string.Empty;
                                hdCrewRules.Value = string.Empty;
                            }
                        }

                    }




            }



        }

        protected void setAircraftSettings()
        {
            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
            if (CorpRequest.AircraftID != null)
                hdAircraft.Value = CorpRequest.AircraftID.ToString();

            hdnPower.Value = "1";
            tbTAS.Value = "0.0";
            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
            {
                tbBias.Value = "00:00";
                tbLandBias.Value = "00:00";
            }
            else
            {
                tbBias.Value = "0.0";
                tbLandBias.Value = "0.0";
            }
            Int64 AircraftID = Convert.ToInt64(hdAircraft.Value == string.Empty ? "0" : hdAircraft.Value);
            FlightPakMasterService.Aircraft Aircraft = GetAircraft(AircraftID);



            if (Aircraft != null)
            {

                hdnPower.Value = Aircraft.PowerSetting == null ? "1" : Aircraft.PowerSetting;

                switch (hdnPower.Value)
                {
                    case "1":
                        tbTAS.Value = Aircraft.PowerSettings1TrueAirSpeed != null ? Aircraft.PowerSettings1TrueAirSpeed.ToString() : "0.0";
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {


                            tbBias.Value = Aircraft.PowerSettings1TakeOffBias != null ? ConvertTenthsToMins(Math.Round((decimal)Aircraft.PowerSettings1TakeOffBias, 1).ToString()) : "00:00";
                            tbLandBias.Value = Aircraft.PowerSettings1LandingBias != null ? ConvertTenthsToMins(Math.Round((decimal)Aircraft.PowerSettings1LandingBias, 1).ToString()) : "00:00";

                        }
                        else
                        {
                            tbBias.Value = Aircraft.PowerSettings1TakeOffBias != null ? Math.Round((decimal)Aircraft.PowerSettings1TakeOffBias, 1).ToString() : "0.0";
                            tbLandBias.Value = Aircraft.PowerSettings1LandingBias != null ? Math.Round((decimal)Aircraft.PowerSettings1LandingBias, 1).ToString() : "0.0";
                        }
                        break;
                    case "2":
                        tbTAS.Value = Aircraft.PowerSettings2TrueAirSpeed != null ? Aircraft.PowerSettings2TrueAirSpeed.ToString() : "0.0";
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {

                            tbBias.Value = Aircraft.PowerSettings2TakeOffBias != null ? ConvertTenthsToMins(Math.Round((decimal)Aircraft.PowerSettings2TakeOffBias, 1).ToString()) : "00:00";
                            tbLandBias.Value = Aircraft.PowerSettings2LandingBias != null ? ConvertTenthsToMins(Math.Round((decimal)Aircraft.PowerSettings2LandingBias, 1).ToString()) : "00:00";
                        }
                        else
                        {
                            tbBias.Value = Aircraft.PowerSettings2TakeOffBias != null ? Math.Round((decimal)Aircraft.PowerSettings2TakeOffBias, 1).ToString() : "0.0";
                            tbLandBias.Value = Aircraft.PowerSettings2LandingBias != null ? Math.Round((decimal)Aircraft.PowerSettings2LandingBias, 1).ToString() : "0.0";
                        }
                        break;
                    case "3":
                        tbTAS.Value = Aircraft.PowerSettings3TrueAirSpeed != null ? Aircraft.PowerSettings3TrueAirSpeed.ToString() : "0";
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {


                            tbBias.Value = Aircraft.PowerSettings3TakeOffBias != null ? ConvertTenthsToMins(Math.Round((decimal)Aircraft.PowerSettings3TakeOffBias, 1).ToString()) : "00:00";
                            tbLandBias.Value = Aircraft.PowerSettings3LandingBias != null ? ConvertTenthsToMins(Math.Round((decimal)Aircraft.PowerSettings3LandingBias, 1).ToString()) : "00:00";

                        }
                        else
                        {
                            tbBias.Value = Aircraft.PowerSettings3TakeOffBias != null ? Math.Round((decimal)Aircraft.PowerSettings3TakeOffBias, 1).ToString() : "0.0";
                            tbLandBias.Value = Aircraft.PowerSettings3LandingBias != null ? Math.Round((decimal)Aircraft.PowerSettings3LandingBias, 1).ToString() : "0.0";
                        }
                        break;

                    default:

                        hdnPower.Value = "1";
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            tbLandBias.Value = "00:00";
                            tbBias.Value = "00:00";
                        }
                        else
                        {
                            tbBias.Value = "0.0";
                            tbLandBias.Value = "0.0";
                        }
                        tbTAS.Value = "0.0";
                        break;
                }
                // To check whether the Aircraft has 0 due to 'convert to decimal' method & return to it's 0.0 default format
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                {
                    if (!string.IsNullOrEmpty(tbBias.Value))
                    {
                        if (tbBias.Value.IndexOf(".") < 0)
                        {
                            tbBias.Value = tbBias.Value + ".0";
                        }

                    }

                    if (!string.IsNullOrEmpty(tbLandBias.Value))
                    {
                        if (tbLandBias.Value.IndexOf(".") < 0)
                        {
                            tbLandBias.Value = tbLandBias.Value + ".0";
                        }

                    }

                }
            }

        }

        protected void SetWindReliability()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(hdSetWindReliability.Value))
                {
                    hdnWindReliability.Value = UserPrincipal.Identity._fpSettings._WindReliability == null ? "3" : UserPrincipal.Identity._fpSettings._WindReliability.ToString();
                }
                else
                    hdnWindReliability.Value = "3";
            }

        }


        protected void btnInsertLeg_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (IsAuthorized(Permission.CorporateRequest.AddCRLeg))
                        {

                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                SaveCRToSession();
                                validateandrebindmaster();
                                ClearFields();
                                CRLeg CurrLeg = new CRLeg();
                                CurrLeg.State = CorporateRequestTripEntityState.Added;

                                CurrLeg.LegNUM = GetLegnumToUpdate(CorpRequest, "Insert");
                                GetCurrentOrNextLeg(CorpRequest, "Insert");

                                CorporateRequestService.Airport DepTArr = new CorporateRequestService.Airport();
                                if (!string.IsNullOrEmpty(hdDepart.Value))
                                {
                                    Int64 AirportID = 0;
                                    if (Int64.TryParse(hdDepart.Value, out AirportID))
                                    {
                                        DepTArr.AirportID = AirportID;
                                        DepTArr.IcaoID = tbDepartsIcao.Text;
                                        DepTArr.AirportName = tbDepartAirportName.Text;
                                        DepTArr.CityName = tbDepartCity.Text;
                                        CurrLeg.Airport1 = DepTArr;
                                        CurrLeg.DAirportID = AirportID;
                                        CurrLeg.DepartAirportChanged = true;
                                        //CurrLeg.DepartAirportChangedUpdateCrew = true;
                                        //CurrLeg.DepartAirportChangedUpdatePAX = true;
                                    }
                                    else
                                    {
                                        CurrLeg.Airport1 = null;
                                        CurrLeg.DAirportID = null;
                                    }
                                }
                                else
                                {
                                    CurrLeg.Airport1 = null;
                                    CurrLeg.DAirportID = null;
                                }



                                if (!string.IsNullOrEmpty(tbDepartsLocal.Text))
                                {
                                    string StartTime = rmbDepartsLocal.Text.Trim();
                                    if (StartTime.Trim() == "")
                                    {
                                        StartTime = "0000";
                                    }
                                    int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                    int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                                    DateTime dt = DateTime.ParseExact(tbDepartsLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat != null && UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 2)
                                    {

                                    }
                                    else
                                    {
                                        if (DepTimeForm.SelectedValue == "2")
                                        {
                                            StartHrs = StartHrs + 12;
                                        }
                                    }
                                    dt = dt.AddHours(StartHrs);
                                    dt = dt.AddMinutes(StartMts);
                                    CurrLeg.DepartureDTTMLocal = dt;
                                }

                                if (!string.IsNullOrEmpty(tbUtcDate.Text))
                                {
                                    string Timestr = rmtUtctime.Text.Trim();
                                    int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                    int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                    DateTime dt = DateTime.ParseExact(tbUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    dt = dt.AddHours(Hrst);
                                    dt = dt.AddMinutes(Mtstr);
                                    CurrLeg.DepartureGreenwichDTTM = dt;
                                }
                                //CurrLeg.DepartsUTChhmm = tbUtctime.Text;

                                if (!string.IsNullOrEmpty(tbHomeDate.Text))
                                {
                                    string Timestr = rmtHomeTime.Text.Trim();
                                    int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                    int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                    DateTime dt = DateTime.ParseExact(tbHomeDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    dt = dt.AddHours(Hrst);
                                    dt = dt.AddMinutes(Mtstr);
                                    CurrLeg.HomeDepartureDTTM = dt;
                                }


                                if (!string.IsNullOrEmpty(tbArrivalLocal.Text))
                                {
                                    string Timestr = rmbArrivalLocalTime.Text.Trim();
                                    int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                    int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                    DateTime dt = DateTime.ParseExact(tbArrivalLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat != null && UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 2)
                                    {

                                    }
                                    else
                                    {
                                        if (ArrTimeForm.SelectedValue == "2")
                                        {
                                            Hrst = Hrst + 12;
                                        }
                                    }
                                    dt = dt.AddHours(Hrst);
                                    dt = dt.AddMinutes(Mtstr);
                                    CurrLeg.ArrivalDTTMLocal = dt;
                                    CurrLeg.NextLocalDTTM = dt;
                                }



                                CorporateRequestService.Airport ArrvArr = new CorporateRequestService.Airport();
                                if (!string.IsNullOrEmpty(hdArrival.Value))
                                {
                                    Int64 AirportArriveID = 0;
                                    if (Int64.TryParse(hdArrival.Value, out AirportArriveID))
                                    {
                                        ArrvArr.AirportID = AirportArriveID;
                                        ArrvArr.IcaoID = tbArrivalIcao.Text;
                                        ArrvArr.AirportName = lbArrivalAirportName.Text;
                                        ArrvArr.CityName = tbArrivalCity.Text;
                                        CurrLeg.Airport = ArrvArr;
                                        CurrLeg.AAirportID = AirportArriveID;
                                        CurrLeg.ArrivalAirportChanged = true;
                                        //CurrLeg.ArrivalAirportChangedUpdateCrew = true;
                                        //CurrLeg.ArrivalAirportChangedUpdatePAX = true;
                                    }
                                    else
                                    {
                                        CurrLeg.Airport = null;
                                        CurrLeg.AAirportID = null;
                                    }
                                }
                                else
                                {
                                    CurrLeg.Airport = null;
                                    CurrLeg.AAirportID = null;
                                }

                                //if (CorpRequest.FlightNUM != null)
                                //{
                                //    CurrLeg.FlightNUM = CorpRequest.FlightNUM;
                                //}

                                if (CorpRequest.ClientID != null && CorpRequest.ClientID != 0)
                                {

                                    CorporateRequestService.Client PrefClient = new CorporateRequestService.Client();
                                    PrefClient.ClientID = CorpRequest.Client.ClientID;
                                    PrefClient.ClientCD = CorpRequest.Client.ClientCD;
                                    PrefClient.ClientDescription = CorpRequest.Client.ClientDescription;
                                    CurrLeg.ClientID = CorpRequest.Client.ClientID;
                                    //CurrLeg.Client = PrefClient;
                                }

                                if (CorpRequest.Fleet != null)
                                {
                                    if (CorpRequest.Fleet.MaximumPassenger != null && CorpRequest.Fleet.MaximumPassenger > 0)
                                    {
                                        if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                                        {

                                            CurrLeg.SeatTotal = Convert.ToInt32(CorpRequest.Fleet.MaximumPassenger);
                                            CurrLeg.ReservationTotal = 0;
                                            CurrLeg.PassengerTotal = 0;
                                            CurrLeg.ReservationAvailable = CurrLeg.SeatTotal - CurrLeg.PassengerTotal;
                                            CurrLeg.WaitNUM = 0;
                                        }
                                    }

                                }

                                if (UserPrincipal.Identity._clientId != null)
                                {
                                    FlightPakMasterService.Client client = new FlightPakMasterService.Client();
                                    client = GetClient((long)UserPrincipal.Identity._clientId);
                                    if (client != null)
                                    {
                                        CorporateRequestService.Client PrefClient = new CorporateRequestService.Client();
                                        PrefClient.ClientID = client.ClientID;
                                        PrefClient.ClientCD = client.ClientCD;
                                        PrefClient.ClientDescription = client.ClientDescription;
                                        CurrLeg.ClientID = client.ClientID;
                                        //CurrLeg.ClientID = PrefClient;
                                        //tbClient.ReadOnly = true;
                                    }
                                    else
                                    {
                                        //tbClient.ReadOnly = false;
                                    }
                                }


                                CorpRequest.CRLegs.Insert(Convert.ToInt16(hdnLeg.Value) - 1, CurrLeg);
                                hdnLeg.Value = (CorpRequest.CRLegs.IndexOf(CurrLeg) + 1).ToString();
                                LoadLegDetails(CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1]);
                                BindCRTabs(CorpRequest, "InsertLeg");

                                Session["CurrentCorporateRequest"] = CorpRequest;
                                Master.TrackerLegs.Rebind();
                                ClearLabels();
                                if (UserPrincipal.Identity._fpSettings._CrewDutyID != null)
                                    hdCrewRules.Value = Convert.ToString((long)UserPrincipal.Identity._fpSettings._CrewDutyID);
                                SetCrewDutyTypeAndFARRules();
                                SetCategory();
                                setAircraftSettings();
                                SetWindReliability();
                                CopyPrevLegDetails((long)CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM, false);
                                SetCopyInfo(CorpRequest);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }

        }
        protected void btnDeleteLeg_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsAuthorized(Permission.CorporateRequest.DeleteCRLeg))
                        {
                            RadAjaxManager.GetCurrent(Page).FocusControl(rtsLegs);
                            if (rtsLegs.Tabs.Count != 1)
                            {
                                RadWindowManager1.RadConfirm("Delete Corporate Leg?", "confirmDeleteLegCallBackFn", 330, 100, null, "Confirmation!");
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Cannot Delete - Request should have atleast one leg", 330, 100, "Request Alert", null);
                            }
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }



        }
        protected void btnCancelYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                        {
                            CRMain oCRMain = new CRMain();
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                oCRMain = (CRMain)Session["CurrentCorporateRequest"];
                                var RetTrip = Service.GetCRHistory(Convert.ToInt64(oCRMain.CustomerID), Convert.ToInt64(oCRMain.CRMainID));
                                StringBuilder HistoryDescription = new System.Text.StringBuilder();
                                string OldDescription = string.Empty;
                                CRHistory TripHistory = new CRHistory();
                                if (oCRMain.TripID != null || oCRMain.TripID != 0)
                                {
                                    oCRMain.CorporateRequestStatus = "X";
                                    var Result = Service.CancelRequest(oCRMain);
                                    if (Result.ReturnFlag == true)
                                    {
                                        if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                        {
                                            //TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                            TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                            TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                            TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                            TripHistory.LastUpdTS = DateTime.UtcNow;
                                            HistoryDescription.AppendLine(string.Format("Existing Trip Cancel Request Submitted To The Change Queue."));
                                            //OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                            TripHistory.LogisticsHistory = HistoryDescription.ToString();
                                            TripHistory.IsDeleted = false;
                                            var ReturnHistory = Service.AddCRHistory(TripHistory);
                                        }
                                        RadWindowManager1.RadAlert("Request canceled successfully.", 360, 50, "Corporate request", "alertCancelCallBackFn", null);
                                    }
                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("Trip Was Never Submitted To Dispatch. Please Use The Delete Button To Delete The Trip.", 360, 50, "Corporate request", "");
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }
        protected void btnCancellationReq_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CRMain oCRMain = new CRMain();
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            oCRMain = (CRMain)Session["CurrentCorporateRequest"];

                            if (oCRMain.TripID != null)
                            {
                                RadWindowManager1.RadConfirm("Submit Cancel Request To Change Queue ?", "confirmCancelRequestCallBackFn", 400, 30, null, "Corporate request");
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Trip Was Never Submitted To Dispatch. Please Use The Delete Button To Delete The Trip.", 360, 50, "Corporate request", "");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }

        /// <summary>
        /// show alert after ack req
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReload_Click(object sender, EventArgs e)
        {
            Master.RadAjaxManagerMaster.ResponseScripts.Add(@"window.location=window.location.href.split('?')[0];");
        }

        protected void btnSubmitYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                        {
                            CRMain oCRMain = new CRMain();
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                oCRMain = (CRMain)Session["CurrentCorporateRequest"];
                                var RetTrip = Service.GetCRHistory(Convert.ToInt64(oCRMain.CustomerID), Convert.ToInt64(oCRMain.CRMainID));
                                StringBuilder HistoryDescription = new System.Text.StringBuilder();
                                string OldDescription = string.Empty;
                                CRHistory TripHistory = new CRHistory();

                                //if (Session["CRStatus"] != null && Session["CRStatus"].ToString() == "Submitted")
                                //{
                                //    oCRMain.CorporateRequestStatus = "S";
                                //    Session["CRStatus"] = null;
                                //}
                                //if (Session["CRStatus"] != null && Session["CRStatus"].ToString() == "Accepted")
                                //{
                                //    oCRMain.CorporateRequestStatus = "A";
                                //    Session["CRStatus"] = null;
                                //}

                                if (oCRMain.CorporateRequestStatus != "S" && oCRMain.CorporateRequestStatus != "A")
                                {
                                    string status = "W";
                                    if (oCRMain.CorporateRequestStatus != null && oCRMain.CorporateRequestStatus.ToString().Trim() != string.Empty)
                                        status = oCRMain.CorporateRequestStatus;
                                    oCRMain.CorporateRequestStatus = "S";
                                    var Result = Service.UpdateCRMainCorpStatus(oCRMain);
                                    if (Result.ReturnFlag == true)
                                    {
                                        if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                        {
                                            //TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                            TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                            TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                            TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                            TripHistory.LastUpdTS = DateTime.UtcNow;
                                            if (status != "W")
                                                HistoryDescription.AppendLine(string.Format("Existing Trip Request Resubmitted To The Change Queue."));
                                            else
                                                HistoryDescription.AppendLine(string.Format("New Trip Request Submitted To The Change Queue."));
                                            //OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                            TripHistory.LogisticsHistory = HistoryDescription.ToString();
                                            TripHistory.IsDeleted = false;
                                            var ReturnHistory = Service.AddCRHistory(TripHistory);
                                        }
                                        RadWindowManager1.RadAlert("Request submitted successfully.", 360, 50, "Corporate request", "alertsubmitCallBackFn", null);
                                    }
                                }
                                else if (oCRMain.CorporateRequestStatus == "A")
                                {
                                    oCRMain.CorporateRequestStatus = "S";
                                    var Result = Service.UpdateCRMainCorpStatus(oCRMain);
                                    if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                    {
                                        //TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                        TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                        TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                        TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                        TripHistory.LastUpdTS = DateTime.UtcNow;
                                        HistoryDescription.AppendLine(string.Format("Existing Trip Request Resubmitted To The Change Queue."));
                                        //OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                        TripHistory.LogisticsHistory = HistoryDescription.ToString();
                                        TripHistory.IsDeleted = false;
                                        var ReturnHistory = Service.AddCRHistory(TripHistory);
                                    }
                                    RadWindowManager1.RadAlert("Request submitted successfully.", 360, 50, "Corporate request", "alertsubmitCallBackFn", null);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }
        protected void btnSubmitReq_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                        {
                            bool DatCheck = true;
                            DateTime CompanyTime = System.DateTime.Now;
                            double CompanySetting = 0;

                            if (UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinHours != null && UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinHours != 0)
                            {
                                CompanySetting = Convert.ToDouble(UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinHours);
                            }
                            CompanyTime = CompanyTime.AddHours(CompanySetting);
                            CRMain oCRMain = new CRMain();
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                oCRMain = (CRMain)Session["CurrentCorporateRequest"];
                                if (oCRMain.CRLegs != null && oCRMain.CRLegs.Count() > 0)
                                {
                                    foreach (CRLeg legs in oCRMain.CRLegs)
                                    {
                                        if (legs.DepartureDTTMLocal != null && legs.LegNUM == 1)
                                        {
                                            if (Convert.ToDateTime(legs.DepartureDTTMLocal) <= CompanyTime)
                                            {
                                                DatCheck = false;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            if (DatCheck == false)
                            {
                                if (UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinWeekend != null && UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinWeekend == true)
                                {
                                    if (System.DateTime.Now.DayOfWeek == DayOfWeek.Saturday || System.DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        DatCheck = true;
                                    }
                                }
                            }

                            if (DatCheck == false && CompanySetting != 0)
                            {
                                RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"confirmExcludeOnSubmitMain();");
                            }
                            else
                            {

                                if (Session["CurrentCorporateRequest"] != null)
                                {
                                    oCRMain = (CRMain)Session["CurrentCorporateRequest"];

                                    if (oCRMain.CorporateRequestStatus != "S")
                                    {
                                        RadWindowManager1.RadConfirm("Submit Travel Request To Change Queue ?", "confirmSubmitCallBackFn", 400, 30, null, "Corporate request");
                                    }
                                    else if (oCRMain.CorporateRequestStatus == "S")
                                    {
                                        RadWindowManager1.RadAlert("Trip Request Is Already Submitted To The Change Queue.", 360, 50, "Corporate request", "");

                                    }
                                }
                            }



                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }
        protected void btnAcknowledge_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                        {
                            CRMain oCRMain = new CRMain();
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                oCRMain = (CRMain)Session["CurrentCorporateRequest"];
                                var RetTrip = Service.GetCRHistory(Convert.ToInt64(oCRMain.CustomerID), Convert.ToInt64(oCRMain.CRMainID));
                                StringBuilder HistoryDescription = new System.Text.StringBuilder();
                                string OldDescription = string.Empty;
                                CRHistory TripHistory = new CRHistory();
                                if (oCRMain.AcknowledgementStatus == "A")
                                {
                                    string strLastuid = oCRMain.LastUpdUID;
                                    var ObjRetval = Service.RequestAcknowledge((long)oCRMain.CRMainID, (long)oCRMain.TripID, (long)oCRMain.CustomerID, strLastuid, Convert.ToDateTime(oCRMain.LastUpdTS));
                                    if (ObjRetval.ReturnFlag)
                                    {
                                        oCRMain = ObjRetval.EntityInfo;
                                        Session["CurrentCorporateRequest"] = oCRMain;
                                        if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                        {
                                            //TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                            TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                            TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                            TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                            TripHistory.LastUpdTS = DateTime.UtcNow;
                                            HistoryDescription.AppendLine(string.Format("Acknowledged Changes Made By Dispatch."));
                                            //OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                            TripHistory.LogisticsHistory = HistoryDescription.ToString();
                                            TripHistory.IsDeleted = false;
                                            var ReturnHistory = Service.AddCRHistory(TripHistory);
                                        }
                                        RadWindowManager1.RadAlert("Request Acknowledged successfully.", 360, 50, "Corporate request", "RedirectPage");

                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                            if (CorpRequest.CorporateRequestStatus == "W")
                            {
                                Master.Delete(CorpRequest.CRMainID);
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Trip Request has been transferred to Preflight. Please submit Cancel request to Change Queue.", 450, 50, "Corporate request", "");

                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }




        protected void btnDeleteLegYes_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsAuthorized(Permission.CorporateRequest.DeleteCRLeg))
                        {
                            RadAjaxManager.GetCurrent(Page).FocusControl(rtsLegs);
                            if (rtsLegs.Tabs.Count != 1)
                            {
                                //rtsLegs.Tabs.RemoveAt(rtsLegs.Tabs.Count() - 1);
                                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                                int currentleg = 0;
                                currentleg = CorpRequest.CRLegs.IndexOf(CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1]);

                                Int64 currentlegnum = (long)CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM;



                                long dummynum = 0;
                                dummynum = GetLegnumToUpdate(CorpRequest, "Delete");


                                if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CRLegID != 0)
                                {
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].State = CorporateRequestTripEntityState.Deleted;
                                    //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM = 9999;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].IsDeleted = true;

                                }
                                else
                                {
                                    CorpRequest.CRLegs.RemoveAt(Convert.ToInt16(hdnLeg.Value) - 1);
                                }



                                CRLeg LegToLoad = CorpRequest.CRLegs.Where(x => x.LegNUM == currentlegnum && x.IsDeleted == false).FirstOrDefault();

                                if (LegToLoad == null)
                                    LegToLoad = CorpRequest.CRLegs.Where(x => x.LegNUM == currentlegnum - 1 && x.IsDeleted == false).FirstOrDefault();


                                hdnLeg.Value = (CorpRequest.CRLegs.IndexOf(LegToLoad) + 1).ToString();

                                hdnLeg.Value = getNextActiveLegIndex(CorpRequest, currentleg);


                                Session["CurrentCorporateRequest"] = CorpRequest;
                                Master.DoLegCalculationsForTrip();
                                rtsLegs.Tabs.FindTabByValue((Convert.ToInt16(hdnLeg.Value) - 1).ToString()).Selected = true;
                                validateandrebindmaster();
                                ClearLabels();
                                LoadLegDetails(LegToLoad);
                                BindCRTabs(CorpRequest, "DeleteLeg");
                                Session["CurrentCorporateRequest"] = CorpRequest;
                                Master.TrackerLegs.Rebind();

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }

        }

        protected void btnDeleteLegNo_Click(object sender, EventArgs e)
        {
            RadAjaxManager.GetCurrent(Page).FocusControl(rtsLegs);
        }

        protected void btnPassengerCopy_Click(object sender, EventArgs e)
        {

        }
        protected void btnAuthorizationCopy_Click(object sender, EventArgs e)
        {

        }

        protected void btnCategoryLegs_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnCategoryLegs);
                        if (!string.IsNullOrEmpty(hdCategory.Value))
                        {
                            ViewState["CopyType"] = "FlightCategory";
                            RadWindowManager1.RadConfirm("Copy FlightCategory To All Legs?", "confirmCopyCallBackFn", 330, 100, null, "Confirmation!");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightLeg);
                }
            }
        }

        protected void btnDepartmentCopy_Click(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveCRToSession();
                        //validateandrebindmaster();
                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                        //BindCRTabs(CorpRequest, "SaveButton");
                        Master.Save(CorpRequest);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }

        }

        protected void btnNext_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveCRToSession();
                        validateandrebindmaster();
                        BindCRTabs(CorpRequest, "NextButton");

                        //Trip = (PreflightMain)Session["CurrentCorporateRequest"];
                        //if (Trip != null)
                        //{
                        //    if (CorpRequest.AllowTripToNavigate)
                        //    {
                        Master.Next("Legs");
                        //    }
                        //}

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                        Master.Cancel(CorpRequest);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }

        }

        #region Legs Tab Click
        protected void rtsLegs_TabClick(object sender, RadTabStripEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveCRToSession();
                        validateandrebindmaster();
                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                        //hdnLeg.Value = (rtsLegs.Tabs.FindTabByText(e.Tab.Text).Index + 1).ToString();
                        hdnLeg.Value = (Convert.ToInt16(e.Tab.Value) + 1).ToString();
                        ClearFields();
                        // ClearLabels();
                        LoadLegDetails(CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1]);
                        BindCRTabs(CorpRequest, "Tabclick");
                        rtsLegs.Focus();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }

        }
        #endregion
        #endregion

        #region"Methods"


        protected void btnDeadHeadLegYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdCategory.Value = hdnDeadCategory.Value;
                        lbcvflightCategory.Text = "";
                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                        if (CorpRequest != null)
                        {
                            int OldSeatTot = 0;
                            if (CorpRequest.CRLegs != null)
                            {
                                if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CRPassengers != null)
                                {
                                    OldSeatTot = (int)CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].PassengerTotal;
                                    foreach (CRPassenger PrefPass in CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CRPassengers.ToList())
                                    {
                                        if (PrefPass.CRPassengerID != 0)
                                        {
                                            PrefPass.FlightPurposeID = 0;
                                            PrefPass.IsDeleted = true;
                                            PrefPass.State = CorporateRequestTripEntityState.Deleted;
                                        }
                                        else
                                        {
                                            CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CRPassengers.Remove(PrefPass);
                                        }
                                    }
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].PassengerTotal = 0;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ReservationAvailable = CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ReservationAvailable + OldSeatTot;
                                    tbPaxNumbers.Text = CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].PassengerTotal.ToString();
                                    //tbAvail.Text = CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ReservationAvailable.ToString();
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }

        protected void btnDeadHeadLegNo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                        if (CorpRequest.CRLegs != null)
                        {
                            if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].FlightCatagory != null)
                            {
                                tbFlightCategoryCode.Text = CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].FlightCatagory.FlightCatagoryCD;
                                hdCategory.Value = System.Web.HttpUtility.HtmlEncode(CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].FlightCategoryID.ToString());
                                lbFlightCategoryName.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].FlightCatagory.FlightCatagoryDescription.ToString());
                            }
                            else
                            {
                                tbFlightCategoryCode.Text = string.Empty;
                                hdCategory.Value = string.Empty;
                                lbFlightCategoryName.Text = string.Empty;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }

        protected void btnCopyYes_Click(object sender, EventArgs e)
        {
            int OldSeatTot = 0;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                            if (CorpRequest.CopyInfoToAllLegs == null)
                                CorpRequest.CopyInfoToAllLegs = new Dictionary<CorporateRequestCopyFunctionalty, CorporateRequestCopyInfo>();
                        }
                        if (ViewState["CopyType"] != null)
                        {
                            Dictionary<string, string> AddlCopyDetails = new Dictionary<string, string>();
                            if (ViewState["CopyType"].Equals("FlightCategory"))
                            {
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbDistance);
                                CorporateRequestService.FlightCatagory flightCatagory = new CorporateRequestService.FlightCatagory();
                                if (!string.IsNullOrEmpty(hdCategory.Value))
                                {
                                    Int64 FlightCategoryID = 0;
                                    if (Int64.TryParse(hdCategory.Value, out FlightCategoryID))
                                    {
                                        flightCatagory.FlightCategoryID = FlightCategoryID;
                                        flightCatagory.FlightCatagoryCD = tbFlightCategoryCode.Text;

                                        if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                                        {
                                            foreach (CRLeg corpLegitem in CorpRequest.CRLegs)
                                            {
                                                corpLegitem.FlightCatagory = flightCatagory;
                                                corpLegitem.FlightCategoryID = flightCatagory.FlightCategoryID;
                                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    List<FlightPak.Web.FlightPakMasterService.FlightCatagory> flightCategoryList = new List<FlightPak.Web.FlightPakMasterService.FlightCatagory>();
                                                    var objRetVal = objDstsvc.GetFlightCategoryList();
                                                    if (objRetVal.ReturnFlag)
                                                    {
                                                        flightCategoryList = objRetVal.EntityList.Where(x => x.FlightCatagoryCD.ToString().ToUpper().Trim().Equals(tbFlightCategoryCode.Text.ToUpper().Trim())).ToList();
                                                    }
                                                    if (flightCategoryList != null && flightCategoryList.Count > 0)
                                                    {
                                                        if (flightCategoryList[0].IsDeadorFerryHead == true)
                                                        {
                                                            if (corpLegitem.CRPassengers != null)
                                                            {
                                                                if (corpLegitem.CRPassengers.Count > 0)
                                                                {
                                                                    OldSeatTot = (int)corpLegitem.PassengerTotal;
                                                                    foreach (CRPassenger PrefPass in corpLegitem.CRPassengers.ToList())
                                                                    {
                                                                        if (PrefPass.CRPassengerID != 0)
                                                                        {
                                                                            PrefPass.FlightPurposeID = 0;
                                                                            PrefPass.IsDeleted = true;
                                                                            PrefPass.State = CorporateRequestTripEntityState.Deleted;
                                                                        }
                                                                        else
                                                                        {
                                                                            corpLegitem.CRPassengers.Remove(PrefPass);
                                                                        }
                                                                    }
                                                                    corpLegitem.PassengerTotal = 0;
                                                                    corpLegitem.ReservationAvailable = corpLegitem.ReservationAvailable + OldSeatTot;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            if (CorpRequest.CopyInfoToAllLegs.Keys.Contains(CorporateRequestCopyFunctionalty.FlightCategory))
                                                CorpRequest.CopyInfoToAllLegs.Remove(CorporateRequestCopyFunctionalty.FlightCategory);
                                            CorpRequest.CopyInfoToAllLegs.Add(CorporateRequestCopyFunctionalty.FlightCategory, new CorporateRequestCopyInfo()
                                            {
                                                IsCopied = true,
                                                CDToCopy = tbFlightCategoryCode.Text,
                                                IDToCopy = Convert.ToInt64(FlightCategoryID),
                                            });
                                        }

                                    }
                                }
                            }
                            Session["CurrentCorporateRequest"] = CorpRequest;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }

        }

        protected void btnCopyNo_Click(object sender, EventArgs e)
        {
            if (ViewState["CopyType"] != null)
            {
                RadAjaxManager.GetCurrent(Page).FocusControl(btnCategoryLegs);
            }
        }



        private string getNextActiveLegNum(CRMain CRMainRequest, int currIndex)
        {
            return "";
        }

        private string getNextActiveLegIndex(CRMain CRMainRequest, int currIndex)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CRMainRequest, currIndex))
            {
                string indexstring = string.Empty;
                if (currIndex == CRMainRequest.CRLegs.Count)
                {
                    currIndex--;
                }

                for (int i = currIndex; i <= CRMainRequest.CRLegs.Count() - 1; i++)
                {
                    if (CRMainRequest.CRLegs[i].State != CorporateRequestTripEntityState.Deleted)
                    {
                        indexstring = (i + 1).ToString();
                        break;
                    }

                }
                if (indexstring == string.Empty)
                {
                    for (int i = currIndex; i >= 0; i--)
                    {
                        if (CRMainRequest.CRLegs[i].State != CorporateRequestTripEntityState.Deleted)
                        {
                            indexstring = (i + 1).ToString();
                            break;
                        }

                    }
                }
                return indexstring;

            }

        }




        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            //if (e.Argument.ToString() == "tbDepartCity_TextChanged")
            //{
            //    if (hdnDepartBool.Value == "0")
            //    {
            //        hdnDepartBool.Value = "1";
            //        tbDepartCity_TextChanged(sender, e);
            //    }
            //    else
            //    {
            //        tbDepart_TextChanged(sender, e);
            //    }
            //}
            //if (e.Argument.ToString() == "tbArrivalCity_TextChanged")
            //{
            //    if (hdnArrivalBool.Value == "0")
            //    {
            //        hdnArrivalBool.Value = "1";
            //        tbArrivalCity_TextChanged(sender, e);
            //    }
            //    else
            //    {
            //        tbArrival_TextChanged(sender, e);
            //    }
            //}
            //if (e.Argument.ToString() == "tbDepart_TextChanged")
            //    tbDepart_TextChanged(sender, e);
            //if (e.Argument.ToString() == "tbArrival_TextChanged")
            //    tbArrival_TextChanged(sender, e);

        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            //if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
            //{

            //}
        }

        private void CopyPrevLegDetails(Int64 Legnum, bool Add)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Legnum))
            {

                CRMain corprequest = (CRMain)Session["CurrentCorporateRequest"];

                CRLeg LegDetailstobeCopied = new CRLeg();

                if (Legnum == 1)
                {
                    if (Add)
                        LegDetailstobeCopied = (from Leg in corprequest.CRLegs.Where(x => x.IsDeleted == false)
                                                where Leg.LegNUM == Legnum
                                                select Leg).FirstOrDefault();
                    else
                        LegDetailstobeCopied = (from Leg in corprequest.CRLegs.Where(x => x.IsDeleted == false)
                                                where Leg.LegNUM == Legnum + 1
                                                select Leg).FirstOrDefault();
                }
                else
                {
                    LegDetailstobeCopied = (from Leg in corprequest.CRLegs.Where(x => x.IsDeleted == false)
                                            where Leg.LegNUM == Legnum - 1
                                            select Leg).FirstOrDefault();
                }

                if (LegDetailstobeCopied != null)
                {

                    if (LegDetailstobeCopied.PassengerRequestorID != 0 && LegDetailstobeCopied.Passenger != null)
                    {
                        hdnPaxId.Value = LegDetailstobeCopied.PassengerRequestorID.ToString();
                        tbPassenger.Text = LegDetailstobeCopied.Passenger.PassengerRequestorCD;
                        lbPassengerName.Text = System.Web.HttpUtility.HtmlEncode(LegDetailstobeCopied.Passenger.FirstName);
                    }


                    if (LegDetailstobeCopied.DepartmentID != 0 && LegDetailstobeCopied.Department != null)
                    {
                        hdDepartment.Value = LegDetailstobeCopied.DepartmentID.ToString();
                        tbDepartment.Text = LegDetailstobeCopied.Department.DepartmentCD;
                        lbDepartmentName.Text = System.Web.HttpUtility.HtmlEncode(LegDetailstobeCopied.Department.DepartmentName);
                    }

                    if (LegDetailstobeCopied.AuthorizationID != 0 && LegDetailstobeCopied.DepartmentAuthorization != null)
                    {
                        hdnAuthorizationId.Value = LegDetailstobeCopied.AuthorizationID.ToString();
                        tbAuthorizationCode.Text = LegDetailstobeCopied.DepartmentAuthorization.AuthorizationCD;
                        lbAuthName.Text = System.Web.HttpUtility.HtmlEncode(LegDetailstobeCopied.DepartmentAuthorization.DeptAuthDescription);
                    }


                    //if (LegDetailstobeCopied.AccountID != 0 && LegDetailstobeCopied.Account != null)
                    //{
                    //    hdAccountNo.Value = LegDetailstobeCopied.AccountID.ToString();
                    //    tbAccountNo.Text = LegDetailstobeCopied.Account.AccountNum;
                    //    lbAccountNo.Text = System.Web.HttpUtility.HtmlEncode(LegDetailstobeCopied.Account.AccountDescription);
                    //}


                    if (LegDetailstobeCopied.ClientID != 0 && LegDetailstobeCopied.Client != null)
                    {
                        hdClient.Value = LegDetailstobeCopied.ClientID.ToString();
                        //tbClient.Text = LegDetailstobeCopied.Client.ClientCD;
                        //lbClient.Text = System.Web.HttpUtility.HtmlEncode(LegDetailstobeCopied.Client.ClientDescription);
                    }

                    if (!string.IsNullOrEmpty(LegDetailstobeCopied.FlightPurpose))
                    {
                        hdnFlightPurpose.Value = LegDetailstobeCopied.FlightPurpose;
                    }

                    //if (!string.IsNullOrEmpty(LegDetailstobeCopied.FlightNUM))
                    //{
                    //    tbFlightNumber.Text = LegDetailstobeCopied.FlightNUM;
                    //}

                    // Added Code to Flight Categry
                    if (LegDetailstobeCopied.FlightCategoryID != 0 && LegDetailstobeCopied.FlightCatagory != null)
                    {
                        hdCategory.Value = LegDetailstobeCopied.FlightCategoryID.ToString();
                        tbFlightCategoryCode.Text = LegDetailstobeCopied.FlightCatagory.FlightCatagoryCD;
                        lbFlightCategoryName.Text = System.Web.HttpUtility.HtmlEncode(LegDetailstobeCopied.FlightCatagory.FlightCatagoryDescription);
                    }


                    if (LegDetailstobeCopied.IsPrivate != null)
                        chkPrivate.Checked = LegDetailstobeCopied.IsPrivate == true ? true : false;

                    else
                        chkPrivate.Checked = false;

                    //if (string.IsNullOrEmpty(tbCrewRules.Text))
                    //{
                    //    if (LegDetailstobeCopied.IsDutyEnd != null && !(bool)LegDetailstobeCopied.IsDutyEnd)
                    //    {
                    //        if (LegDetailstobeCopied.CrewDutyRule != null)
                    //        {
                    //            tbCrewRules.Text = LegDetailstobeCopied.CrewDutyRule.CrewDutyRuleCD;
                    //            hdCrewRules.Value = LegDetailstobeCopied.CrewDutyRule.CrewDutyRulesID.ToString();
                    //            lbFar.Text = LegDetailstobeCopied.CrewDutyRule.FedAviatRegNum == null ? string.Empty : System.Web.HttpUtility.HtmlEncode(LegDetailstobeCopied.CrewDutyRule.FedAviatRegNum.ToString());
                    //        }
                    //    }
                    //    else
                    //    {
                    //        CRLeg NextLegDetailstobeCopied = new CRLeg();
                    //        NextLegDetailstobeCopied = (from Leg in corprequest.CRLegs.Where(x => x.IsDeleted == false)
                    //                                    where Leg.LegNUM == Legnum + 1
                    //                                    select Leg).FirstOrDefault();
                    //        if (NextLegDetailstobeCopied != null)
                    //        {
                    //            if (NextLegDetailstobeCopied.CrewDutyRule != null)
                    //            {
                    //                tbCrewRules.Text = NextLegDetailstobeCopied.CrewDutyRule.CrewDutyRuleCD;
                    //                hdCrewRules.Value = NextLegDetailstobeCopied.CrewDutyRule.CrewDutyRulesID.ToString();
                    //                lbFar.Text = NextLegDetailstobeCopied.CrewDutyRule.FedAviatRegNum == null ? string.Empty : System.Web.HttpUtility.HtmlEncode(NextLegDetailstobeCopied.CrewDutyRule.FedAviatRegNum.ToString());
                    //            }
                    //        }

                    //    }
                    //}
                }
            }

        }



        private void SetCopyInfo(CRMain corprequest)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(corprequest))
            {
                if (corprequest.CRLegs == null || corprequest.CRLegs.Count == 0)
                {
                    CRLeg firstleg = new CRLeg();
                    firstleg.CRMainID = corprequest.CRMainID;
                    firstleg.CRMain = corprequest;
                    firstleg.LegNUM = 1;
                    firstleg.PassengerTotal = 0;
                    CorpRequest.CRLegs = new List<CRLeg>();

                    if (!string.IsNullOrEmpty(hdDepart.Value))
                    {
                        firstleg.DepartAirportChanged = true;
                    }
                    if (!string.IsNullOrEmpty(hdArrival.Value))
                    {
                        firstleg.ArrivalAirportChanged = true;
                    }

                    corprequest.CRLegs.Add(firstleg);
                }
                if (corprequest.CopyInfoToAllLegs != null && corprequest.CopyInfoToAllLegs.Count > 0)
                {
                    if (corprequest.CopyInfoToAllLegs.Keys.Contains(CorporateRequestCopyFunctionalty.Requestor) && corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Requestor].IsCopied == true)
                    {
                        hdnPaxId.Value = corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Requestor].IDToCopy.ToString();
                        tbPassenger.Text = corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Requestor].CDToCopy.ToString();
                        lbPassengerName.Text = System.Web.HttpUtility.HtmlEncode(corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Requestor].DescToCopy);
                    }

                    if (corprequest.CopyInfoToAllLegs.Keys.Contains(CorporateRequestCopyFunctionalty.Department) && corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Department].IsCopied == true)
                    {
                        hdDepartment.Value = corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Department].IDToCopy.ToString();
                        tbDepartment.Text = corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Department].CDToCopy.ToString();
                        lbDepartmentName.Text = System.Web.HttpUtility.HtmlEncode(corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Department].DescToCopy);
                    }

                    if (corprequest.CopyInfoToAllLegs.Keys.Contains(CorporateRequestCopyFunctionalty.Authorization) && corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Authorization].IsCopied == true)
                    {
                        hdnAuthorizationId.Value = corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Authorization].IDToCopy.ToString();
                        tbAuthorizationCode.Text = corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Authorization].CDToCopy;
                        lbAuthName.Text = System.Web.HttpUtility.HtmlEncode(corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Authorization].DescToCopy);
                    }


                    if (corprequest.CopyInfoToAllLegs.Keys.Contains(CorporateRequestCopyFunctionalty.Account) && corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Account].IsCopied == true)
                    {
                        hdAccountnum.Value = corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Account].IDToCopy.ToString();
                        //tbAccountNo.Text = corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Account].CDToCopy;
                        //lbAccountNo.Text = System.Web.HttpUtility.HtmlEncode(corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Account].DescToCopy);
                    }

                    if (corprequest.CopyInfoToAllLegs.Keys.Contains(CorporateRequestCopyFunctionalty.FlightCategory) && corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.FlightCategory].IsCopied == true)
                    {
                        hdCategory.Value = corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.FlightCategory].IDToCopy.ToString();
                        tbFlightCategoryCode.Text = corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.FlightCategory].CDToCopy;
                        lbFlightCategoryName.Text = System.Web.HttpUtility.HtmlEncode(corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.FlightCategory].DescToCopy);
                    }

                    //if (corprequest.CopyInfoToAllLegs.Keys.Contains(CorporateRequestCopyFunctionalty.CrewDutyRule) && corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.CrewDutyRule].IsCopied == true)
                    //{
                    //    hdCrewRules.Value = corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.CrewDutyRule].IDToCopy.ToString();
                    //    tbCrewRules.Text = corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.CrewDutyRule].CDToCopy;
                    //}

                    if (corprequest.CopyInfoToAllLegs.Keys.Contains(CorporateRequestCopyFunctionalty.Description) && corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Description].IsCopied == true)
                    {
                        hdnFlightPurpose.Value = corprequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Description].DescToCopy;
                    }

                }

                Session["CurrentCorporateRequest"] = corprequest;

            }


        }

        protected void SetCategory()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string CategoryCode = string.Empty;


                if (!string.IsNullOrEmpty(hdCategory.Value))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPak.Web.FlightPakMasterService.FlightCatagory> FlightCatagoryLists = new List<FlightPak.Web.FlightPakMasterService.FlightCatagory>();
                        var objRetVal = objDstsvc.GetFlightCategoryList();
                        if (objRetVal.ReturnFlag)
                        {
                            FlightCatagoryLists = objRetVal.EntityList.Where(x => x.FlightCategoryID == Convert.ToInt64(hdCategory.Value)).ToList();
                            if (FlightCatagoryLists != null && FlightCatagoryLists.Count > 0)
                            {

                                tbFlightCategoryCode.Text = FlightCatagoryLists[0].FlightCatagoryCD;
                                lbFlightCategoryName.Text = System.Web.HttpUtility.HtmlEncode(FlightCatagoryLists[0].FlightCatagoryDescription);
                                hdCategory.Value = FlightCatagoryLists[0].FlightCategoryID.ToString();
                            }
                            else
                            {
                                tbFlightCategoryCode.Text = string.Empty;
                                lbcvflightCategory.Text = string.Empty;
                                lbFlightCategoryName.Text = string.Empty;
                                hdCategory.Value = string.Empty;
                            }
                        }
                        else
                        {
                            tbFlightCategoryCode.Text = string.Empty;
                            lbcvflightCategory.Text = string.Empty;
                            lbFlightCategoryName.Text = string.Empty;
                            hdCategory.Value = string.Empty;
                        }
                    }
                }
            }


        }
        private FlightPak.Web.FlightPakMasterService.Client GetClient(Int64 ClientID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPak.Web.FlightPakMasterService.Client client = new FlightPakMasterService.Client();
                    var objRetVal = objDstsvc.GetClientCodeList();
                    List<FlightPakMasterService.Client> ClientList = new List<FlightPakMasterService.Client>();

                    if (objRetVal.ReturnFlag)
                    {

                        ClientList = objRetVal.EntityList.Where(x => x.ClientID == ClientID).ToList();
                        if (ClientList != null && ClientList.Count > 0)
                        {
                            client = ClientList[0];

                        }
                        else
                            client = null;

                    }
                    return client;
                }
            }
        }
        public void GetDeparturetAiportCity()
        {
            lbcvDepartCity.Text = string.Empty;
            tbDepartAirportName.Text = string.Empty;
            if (!string.IsNullOrEmpty(tbDepartCity.Text.Trim()))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = objDstsvc.GetAllAirportList().EntityList.Where(x => x.CityName != null && x.CityName.ToString().Trim().ToUpper() == (tbDepartCity.Text.ToString().Trim().ToUpper())).ToList();
                    if (objRetVal != null && objRetVal.Count() == 1)
                    {
                        tbDepartsIcao.Text = ((FlightPakMasterService.GetAllAirport)objRetVal[0]).IcaoID.ToString();
                        tbDepartCity.Text = ((FlightPakMasterService.GetAllAirport)objRetVal[0]).CityName.ToString();
                        hdDepart.Value = ((FlightPakMasterService.GetAllAirport)objRetVal[0]).AirportID.ToString();

                        if (((FlightPakMasterService.GetAllAirport)objRetVal[0]).AirportName != null)
                            tbDepartAirportName.Text = System.Web.HttpUtility.HtmlEncode(((FlightPakMasterService.GetAllAirport)objRetVal[0]).AirportName.ToString().ToUpper());
                    }
                    else if (objRetVal != null && objRetVal.Count() > 1)
                    {
                        radAirportPopup.VisibleOnPageLoad = true;
                        //tbDepartsIcao.Text = ((FlightPakMasterService.GetAllAirport)objRetVal[0]).IcaoID.ToString();
                        //tbDepartCity.Text = ((FlightPakMasterService.GetAllAirport)objRetVal[0]).CityName.ToString();
                        //hdDepart.Value = ((FlightPakMasterService.GetAllAirport)objRetVal[0]).AirportID.ToString();

                        //if (((FlightPakMasterService.GetAllAirport)objRetVal[0]).AirportName != null)
                        //    tbDepartAirportName.Text = System.Web.HttpUtility.HtmlEncode(((FlightPakMasterService.GetAllAirport)objRetVal[0]).AirportName.ToString().ToUpper());
                    }
                    else
                    {
                        lbcvDepartCity.Text = System.Web.HttpUtility.HtmlEncode("Invalid Airport City");
                        tbDepartCity.Focus();
                    }
                }
            }
        }
        protected void BindCRTabs(CRMain CorpReq, string eventraised)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                rtsLegs.Tabs.Clear();


                if (CorpReq.CRLegs != null && CorpReq.CRLegs.Count() > 0)
                {
                    List<CRLeg> CorpLegList = CorpReq.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                    int Legid = 0;
                    int counter = 1;
                    foreach (CRLeg Leg in CorpLegList)
                    {
                        if (Leg.State != CorporateRequestTripEntityState.Deleted)
                        {
                            RadTab Rt = new RadTab();
                            Rt.Text = "Leg" + Leg.LegNUM.ToString();
                            Rt.Value = CorpReq.CRLegs.IndexOf(Leg).ToString();


                            rtsLegs.Tabs.Add(Rt);

                            if (Leg.AAirportID != null && Leg.DAirportID != null)
                            {
                                if (Leg.Airport1 != null && Leg.Airport != null)
                                    rtsLegs.Tabs[Legid].Text = "Leg" + (Legid + 1).ToString() + " (" + Leg.Airport1.IcaoID + "-" + Leg.Airport.IcaoID + ")";
                                else
                                    rtsLegs.Tabs[Legid].Text = "Leg" + (Legid + 1).ToString();
                            }
                            else
                                rtsLegs.Tabs[Legid].Text = "Leg" + (Legid + 1).ToString();

                            Legid = Legid + 1;
                            counter = counter + 1;
                        }
                    }
                }
                else
                {
                    RadTab Rt = new RadTab();
                    Rt.Text = "Leg1";
                    Rt.Value = "0";
                    rtsLegs.Tabs.Add(Rt);
                }
                if (rtsLegs.Tabs.Count > 1)
                {
                    if (eventraised.ToLower() == "pageload")
                        rtsLegs.Tabs[0].Selected = true;
                    else
                        rtsLegs.Tabs.FindTabByValue((Convert.ToInt16(hdnLeg.Value) - 1).ToString()).Selected = true;
                }
                else
                {
                    rtsLegs.Tabs[0].Selected = true;
                }

            }

        }
        protected void GetCurrentOrNextLeg(CRMain CorpReq, string Mode)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CorpReq, Mode))
            {
                PreflightLeg leg = new PreflightLeg();
                switch (Mode)
                {
                    case "Add":
                        {
                            int previndex = 0;
                            previndex = rtsLegs.Tabs[rtsLegs.Tabs.Count - 1].Index;
                            if (CorpReq.CRLegs[Convert.ToInt16(rtsLegs.Tabs[previndex].Value)].Airport != null)
                            {
                                hdDepart.Value = CorpReq.CRLegs[Convert.ToInt16(rtsLegs.Tabs[previndex].Value)].Airport.AirportID.ToString();
                                tbDepartsIcao.Text = CorpReq.CRLegs[Convert.ToInt16(rtsLegs.Tabs[previndex].Value)].Airport.IcaoID;
                                tbDepartAirportName.Text = CorpReq.CRLegs[Convert.ToInt16(rtsLegs.Tabs[previndex].Value)].Airport.AirportName != null ? System.Web.HttpUtility.HtmlEncode(CorpReq.CRLegs[Convert.ToInt16(rtsLegs.Tabs[previndex].Value)].Airport.AirportName) : string.Empty;
                                tbDepartCity.Text = CorpReq.CRLegs[Convert.ToInt16(rtsLegs.Tabs[previndex].Value)].Airport.CityName != null ? System.Web.HttpUtility.HtmlEncode(CorpReq.CRLegs[Convert.ToInt16(rtsLegs.Tabs[previndex].Value)].Airport.CityName) : string.Empty;

                                #region AutoCalulateLegtimes
                                if (CorpReq.CRLegs[Convert.ToInt16(rtsLegs.Tabs[previndex].Value)].ArrivalDTTMLocal != null)
                                {
                                    DateTime dt = (DateTime)CorpReq.CRLegs[Convert.ToInt16(rtsLegs.Tabs[previndex].Value)].ArrivalDTTMLocal;


                                    double hourtoadd = 0;
                                    if (CorpReq.CRLegs[Convert.ToInt16(rtsLegs.Tabs[previndex].Value)].CheckGroup != null && CorpReq.CRLegs[Convert.ToInt16(rtsLegs.Tabs[previndex].Value)].CheckGroup == "DOM")
                                        hourtoadd = UserPrincipal.Identity._fpSettings._GroundTM == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTM;
                                    else
                                        hourtoadd = UserPrincipal.Identity._fpSettings._GroundTMIntl == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTMIntl;

                                    dt = dt.AddHours(hourtoadd);

                                    string startHrs = "0000" + dt.Hour.ToString();
                                    string startMins = "0000" + dt.Minute.ToString();
                                    tbDepartsLocal.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", dt);
                                    rmbDepartsLocal.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);


                                    DateTime ldGmtDep = DateTime.MinValue;
                                    DateTime ldHomDep = DateTime.MinValue;
                                    using (CalculationServiceClient objDstsvc = new CalculationServiceClient())
                                    {
                                        ldGmtDep = objDstsvc.GetGMT(Convert.ToInt64(hdDepart.Value), dt, true, false);

                                        startHrs = "0000" + ldGmtDep.Hour.ToString();
                                        startMins = "0000" + ldGmtDep.Minute.ToString();
                                        tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldGmtDep);
                                        rmtUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                        if (!string.IsNullOrEmpty(hdHomebaseAirport.Value))
                                        {
                                            ldHomDep = objDstsvc.GetGMT(Convert.ToInt64(hdHomebaseAirport.Value), ldGmtDep, false, false);
                                        }
                                        if (!string.IsNullOrEmpty(hdHomebaseAirport.Value))
                                        {
                                            // DateTime Homedt = ldHomDep;
                                            string HomedtstartHrs = "0000" + ldHomDep.Hour.ToString();
                                            string HomedtstartMins = "0000" + ldHomDep.Minute.ToString();
                                            tbHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldHomDep);
                                            rmtHomeTime.Text = HomedtstartHrs.Substring(HomedtstartHrs.Length - 2, 2) + ":" + HomedtstartMins.Substring(HomedtstartMins.Length - 2, 2);
                                        }
                                    }


                                }
                                #endregion

                                RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartsIcao);
                            }
                        }
                        break;
                    case "Insert":
                        {


                            long setLegNum;
                            setLegNum = (long)CorpReq.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM - 1;
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartsIcao);
                            if (setLegNum == 1)
                            {
                                if (CorpReq.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Airport1 != null)
                                {
                                    hdArrival.Value = CorpReq.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Airport1.AirportID.ToString();
                                    tbArrivalIcao.Text = CorpReq.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Airport1.IcaoID;
                                    lbArrivalAirportName.Text = System.Web.HttpUtility.HtmlEncode(CorpReq.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Airport1.AirportName != null ? System.Web.HttpUtility.HtmlEncode(CorpReq.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Airport1.AirportName) : string.Empty);
                                    tbArrivalCity.Text = System.Web.HttpUtility.HtmlEncode(CorpReq.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Airport1.CityName != null ? System.Web.HttpUtility.HtmlEncode(CorpReq.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Airport1.CityName) : string.Empty);

                                    #region "Auto Calculate Leg times"
                                    if (CorpReq.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DepartureDTTMLocal != null)
                                    {
                                        DateTime dt = (DateTime)CorpReq.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DepartureDTTMLocal;
                                        double hourtoadd = 0;
                                        if ((!string.IsNullOrEmpty(CorpReq.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CheckGroup)) && CorpReq.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CheckGroup == "DOM")
                                            hourtoadd = UserPrincipal.Identity._fpSettings._GroundTM == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTM;
                                        else
                                            hourtoadd = UserPrincipal.Identity._fpSettings._GroundTMIntl == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTMIntl;

                                        dt = dt.AddHours(-1 * hourtoadd);
                                        string startHrs = "0000" + dt.Hour.ToString();
                                        string startMins = "0000" + dt.Minute.ToString();
                                        tbArrivalLocal.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", dt);
                                        rmbArrivalLocalTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);


                                        if (!string.IsNullOrEmpty(hdArrival.Value))
                                        {
                                            DateTime ldGmtArr = DateTime.MinValue;
                                            DateTime ldHomArr = DateTime.MinValue;
                                            using (CalculationServiceClient objDstsvc = new CalculationServiceClient())
                                            {
                                                ldGmtArr = objDstsvc.GetGMT(Convert.ToInt64(hdArrival.Value), dt, true, false);

                                                startHrs = "0000" + ldGmtArr.Hour.ToString();
                                                startMins = "0000" + ldGmtArr.Minute.ToString();
                                                tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldGmtArr);
                                                rmtArrivalUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                                if (!string.IsNullOrEmpty(hdHomebaseAirport.Value))
                                                {
                                                    ldHomArr = objDstsvc.GetGMT(Convert.ToInt64(hdHomebaseAirport.Value), ldGmtArr, false, false);
                                                    string HomeArrivalstartHrs = "0000" + ldHomArr.Hour.ToString();
                                                    string HomeArrivalstartMins = "0000" + ldHomArr.Minute.ToString();
                                                    tbArrivalHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldHomArr);
                                                    rmtArrivalHomeTime.Text = HomeArrivalstartHrs.Substring(HomeArrivalstartHrs.Length - 2, 2) + ":" + HomeArrivalstartMins.Substring(HomeArrivalstartMins.Length - 2, 2);
                                                }
                                            }
                                        }

                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                CRLeg PrevorNextLeg = new CRLeg();
                                PrevorNextLeg = CorpReq.CRLegs.Where(x => x.LegNUM == setLegNum - 1).FirstOrDefault();

                                if (PrevorNextLeg != null)
                                {
                                    if (PrevorNextLeg.Airport != null)
                                    {
                                        hdDepart.Value = PrevorNextLeg.Airport.AirportID.ToString();
                                        tbDepartsIcao.Text = PrevorNextLeg.Airport.IcaoID;
                                        tbDepartAirportName.Text = System.Web.HttpUtility.HtmlEncode(PrevorNextLeg.Airport.AirportName != null ? PrevorNextLeg.Airport.AirportName : string.Empty);
                                        tbDepartCity.Text = PrevorNextLeg.Airport.CityName != null ? PrevorNextLeg.Airport.CityName : string.Empty;

                                        #region "Auto Calculate Leg times"
                                        if (PrevorNextLeg.ArrivalDTTMLocal != null)
                                        {
                                            DateTime dt = (DateTime)PrevorNextLeg.ArrivalDTTMLocal;



                                            double hourtoadd = 0;
                                            if (PrevorNextLeg.CheckGroup != null && PrevorNextLeg.CheckGroup == "DOM")
                                                hourtoadd = UserPrincipal.Identity._fpSettings._GroundTM == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTM;
                                            else
                                                hourtoadd = UserPrincipal.Identity._fpSettings._GroundTMIntl == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTMIntl;

                                            dt = dt.AddHours(hourtoadd);


                                            string startHrs = "0000" + dt.Hour.ToString();
                                            string startMins = "0000" + dt.Minute.ToString();
                                            tbDepartsLocal.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", dt);
                                            rmbDepartsLocal.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);


                                            DateTime ldGmtDep = DateTime.MinValue;
                                            DateTime ldHomDep = DateTime.MinValue;
                                            using (CalculationServiceClient objDstsvc = new CalculationServiceClient())
                                            {
                                                ldGmtDep = objDstsvc.GetGMT(Convert.ToInt64(hdDepart.Value), dt, true, false);

                                                startHrs = "0000" + ldGmtDep.Hour.ToString();
                                                startMins = "0000" + ldGmtDep.Minute.ToString();
                                                tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldGmtDep);
                                                rmtUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                                if (!string.IsNullOrEmpty(hdHomebaseAirport.Value))
                                                {
                                                    ldHomDep = objDstsvc.GetGMT(Convert.ToInt64(hdHomebaseAirport.Value), ldGmtDep, false, false);
                                                }
                                                if (!string.IsNullOrEmpty(hdHomebaseAirport.Value))
                                                {
                                                    // DateTime Homedt = ldHomDep;
                                                    string HomedtstartHrs = "0000" + ldHomDep.Hour.ToString();
                                                    string HomedtstartMins = "0000" + ldHomDep.Minute.ToString();
                                                    tbHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldHomDep);
                                                    rmtHomeTime.Text = HomedtstartHrs.Substring(HomedtstartHrs.Length - 2, 2) + ":" + HomedtstartMins.Substring(HomedtstartMins.Length - 2, 2);
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                }
                            }




                        }
                        break;
                }

            }


        }
        public void GetArrivalAiportCity()
        {
            // if(
        }
        protected void SaveCRToSession()
        {
            ClearLabels();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentCorporateRequest"] != null)
                {

                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                    if (CorpRequest.Mode == CorporateRequestTripActionMode.Edit)
                    {

                        IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                        if (CorpRequest.CRLegs == null || CorpRequest.CRLegs.Count() == 0)
                        {
                            CRLeg firstleg = new CRLeg();
                            firstleg.CRMainID = CorpRequest.CRMainID;
                            firstleg.CRMain = CorpRequest;
                            firstleg.LegNUM = 1;
                            firstleg.PassengerTotal = 0;
                            CorpRequest.CRLegs = new List<CRLeg>();

                            if (!string.IsNullOrEmpty(hdDepart.Value))
                            {
                                firstleg.DepartAirportChanged = true;
                                //firstleg.DepartAirportChangedUpdateCrew = true;
                                //firstleg.DepartAirportChangedUpdatePAX = true;

                            }
                            if (!string.IsNullOrEmpty(hdArrival.Value))
                            {
                                firstleg.ArrivalAirportChanged = true;
                                //firstleg.ArrivalAirportChangedUpdateCrew = true;
                                //firstleg.ArrivalAirportChangedUpdatePAX = true;
                            }
                            CorpRequest.CRLegs.Add(firstleg);

                        }


                        CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CRMainID = CorpRequest.CRMainID;
                        //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CRMain = CorpRequest;

                        //Check whether it is added/ modify
                        if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CRLegID == 0)
                            CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].State = CorporateRequestTripEntityState.Added;
                        else
                            CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].State = CorporateRequestTripEntityState.Modified;



                        bool SaveToSession = false;
                        if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == CorporateRequestTripEntityState.Modified)
                        {
                            if (IsAuthorized(Permission.CorporateRequest.EditCRLeg))
                            {
                                SaveToSession = true;
                            }
                        }
                        else if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == CorporateRequestTripEntityState.Added)
                        {
                            if (IsAuthorized(Permission.CorporateRequest.AddCRLeg))
                            {
                                SaveToSession = true;
                            }
                        }

                        if (SaveToSession)
                        {

                            //CorpRequest.RevisionDescriptioin = Master.RevisionDescription;

                            #region Departs

                            CorporateRequestService.Airport DepTArr = new CorporateRequestService.Airport();
                            if (!string.IsNullOrEmpty(hdDepart.Value))
                            {
                                Int64 AirportID = 0;
                                if (Int64.TryParse(hdDepart.Value, out AirportID))
                                {
                                    DepTArr.AirportID = AirportID;
                                    DepTArr.IcaoID = tbDepartsIcao.Text;
                                    DepTArr.AirportName = tbDepartAirportName.Text;
                                    DepTArr.CityName = tbDepartCity.Text;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Airport1 = DepTArr;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DAirportID = AirportID;
                                }
                                else
                                {
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Airport1 = null;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DAirportID = null;
                                }
                            }
                            else
                            {
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Airport1 = null;
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DAirportID = null;
                            }


                            if (!string.IsNullOrEmpty(tbDepartsLocal.Text))
                            {
                                string StartTime = rmbDepartsLocal.Text.Trim();
                                if (StartTime.Trim() == "")
                                {
                                    StartTime = "0000";
                                }
                                int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                                DateTime dt = DateTime.ParseExact(tbDepartsLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat != null && UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 2)
                                {

                                }
                                else
                                {
                                    if (DepTimeForm.SelectedValue == "2")
                                    {
                                        if (StartHrs != 12)
                                            StartHrs = StartHrs + 12;
                                    }
                                    else
                                    {
                                        if (StartHrs == 12)
                                            StartHrs = 0;
                                    }
                                }
                                dt = dt.AddHours(StartHrs);
                                dt = dt.AddMinutes(StartMts);
                                //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DepartureDTTMLocal = DateTime.ParseExact(tbDepartsLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DepartureDTTMLocal = dt;

                            }

                            if (!string.IsNullOrEmpty(tbUtcDate.Text))
                            {
                                string Timestr = rmtUtctime.Text.Trim();
                                int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                DateTime dt = DateTime.ParseExact(tbUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                dt = dt.AddHours(Hrst);
                                dt = dt.AddMinutes(Mtstr);
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DepartureGreenwichDTTM = dt;
                            }

                            if (!string.IsNullOrEmpty(tbHomeDate.Text))
                            {
                                string Timestr = rmtHomeTime.Text.Trim();
                                int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                DateTime dt = DateTime.ParseExact(tbHomeDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                dt = dt.AddHours(Hrst);
                                dt = dt.AddMinutes(Mtstr);
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].HomeDepartureDTTM = dt;
                            }
                            #endregion

                            #region Arrives

                            CorporateRequestService.Airport ArrvArr = new CorporateRequestService.Airport();
                            if (!string.IsNullOrEmpty(hdArrival.Value))
                            {
                                Int64 AirportArriveID = 0;
                                if (Int64.TryParse(hdArrival.Value, out AirportArriveID))
                                {
                                    ArrvArr.AirportID = AirportArriveID;
                                    ArrvArr.IcaoID = tbArrivalIcao.Text;
                                    ArrvArr.AirportName = lbArrivalAirportName.Text;
                                    ArrvArr.CityName = tbArrivalCity.Text;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Airport = ArrvArr;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].AAirportID = AirportArriveID;
                                }
                                else
                                {
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Airport = null;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].AAirportID = null;
                                }
                            }
                            else
                            {
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Airport = null;
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].AAirportID = null;
                            }

                            if (!string.IsNullOrEmpty(tbArrivalLocal.Text))
                            {
                                string Timestr = rmbArrivalLocalTime.Text.Trim();
                                int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                DateTime dt = DateTime.ParseExact(tbArrivalLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat != null && UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 2)
                                {

                                }
                                else
                                {
                                    if (ArrTimeForm.SelectedValue == "2")
                                    {
                                        if (Hrst != 12)
                                            Hrst = Hrst + 12;
                                    }
                                    else
                                    {
                                        if (Hrst == 12)
                                            Hrst = 0;
                                    }
                                }
                                dt = dt.AddHours(Hrst);
                                dt = dt.AddMinutes(Mtstr);
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ArrivalDTTMLocal = dt;
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].NextLocalDTTM = dt;
                            }


                            if (!string.IsNullOrEmpty(tbArrivalUtcDate.Text))
                            {
                                string Timestr = rmtArrivalUtctime.Text.Trim();
                                int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                DateTime dt = DateTime.ParseExact(tbArrivalUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                dt = dt.AddHours(Hrst);
                                dt = dt.AddMinutes(Mtstr);
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ArrivalGreenwichDTTM = dt;
                            }


                            if (!string.IsNullOrEmpty(tbArrivalHomeDate.Text))
                            {
                                string Timestr = rmtArrivalHomeTime.Text.Trim();
                                int Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                int Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));
                                DateTime dt = DateTime.ParseExact(tbArrivalHomeDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                dt = dt.AddHours(Hrst);
                                dt = dt.AddMinutes(Mtstr);
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].HomeArrivalDTTM = dt;
                            }

                            #endregion

                            #region Distance & Time

                            decimal distance = 0;
                            if (decimal.TryParse(tbDistance.Text, out distance))
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Distance = distance;
                            else
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Distance = 0;

                            if (decimal.TryParse(hdnMiles.Value, out distance))
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Distance = distance;
                            else
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Distance = 0;




                            CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].IsPrivate = chkPrivate.Checked;

                            decimal tobias = 0;
                            string TakeOffBiasStr = string.Empty;

                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                TakeOffBiasStr = ConvertMinToTenths(hdnTakeoffBIAS.Value, true, "3");
                            }
                            else
                                TakeOffBiasStr = hdnTakeoffBIAS.Value;
                            if (decimal.TryParse(TakeOffBiasStr, out tobias))
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].TakeoffBIAS = tobias;
                            else
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].TakeoffBIAS = 0;


                            decimal toTas = 0;
                            if (decimal.TryParse(hdnTrueAirSpeed.Value, out toTas))
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].TrueAirSpeed = toTas;
                            else
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].TrueAirSpeed = 0;


                            int windreliability = 0;
                            if (int.TryParse(hdnWindReliability.Value, out windreliability))
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].WindReliability = windreliability;
                            else
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].WindReliability = null;

                            CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].PowerSetting = hdnPower.Value;


                            decimal LandingBias = 0;
                            string LandingBiasStr = string.Empty;

                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                LandingBiasStr = ConvertMinToTenths(hdnLandingBIAS.Value, true, "1");
                                // LandingBiasStr = directMinstoTenths(hdnLandingBIAS.Value);
                            }
                            else
                                LandingBiasStr = hdnLandingBIAS.Value;

                            if (decimal.TryParse(LandingBiasStr, out LandingBias))
                            {
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].LandingBIAS = LandingBias;
                            }
                            else
                            {
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].LandingBIAS = 0;
                            }

                            decimal WindsBoeingTable = 0;
                            if (decimal.TryParse(hdnWindBoeingTable.Value, out WindsBoeingTable))
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].WindsBoeingTable = WindsBoeingTable;
                            else
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].WindsBoeingTable = 0;


                            CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ElapseTM = 0;

                            decimal ElapseTime = 0;
                            if (!string.IsNullOrEmpty(tbElapsedTime.Text))
                            {
                                string ETEStr = "0";
                                //if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                //{
                                //ETEStr = ConvertMinToTenths(tbETE.TextWithLiterals, true, "1");
                                if (tbElapsedTime.Text.ToString().Contains(':') == true)
                                    ETEStr = directMinstoTenths(tbElapsedTime.Text);
                                //}
                                //else
                                //    ETEStr = tbElapsedTime.Text;
                                if (decimal.TryParse(ETEStr, out ElapseTime))
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ElapseTM = ElapseTime;

                            }

                            #endregion

                            #region Duty

                            //////CorporateRequestService.CrewDutyRule CrewDtyRule = new PreflightService.CrewDutyRule();
                            //////if (!string.IsNullOrEmpty(hdCrewRules.Value) && hdCrewRules.Value != "0")
                            //////{
                            //////    Int64 CrewDutyRulesID = 0;
                            //////    if (Int64.TryParse(hdCrewRules.Value, out CrewDutyRulesID))
                            //////    {
                            //////        CrewDtyRule.CrewDutyRuleCD = tbCrewRules.Text;
                            //////        CrewDtyRule.CrewDutyRulesID = CrewDutyRulesID;
                            //////        CrewDtyRule.CrewDutyRulesDescription = " ";
                            //////        CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CrewDutyRule = CrewDtyRule;
                            //////        CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CrewDutyRulesID = CrewDutyRulesID;
                            //////    }
                            //////    else
                            //////    {
                            //////        CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CrewDutyRule = null;
                            //////        CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CrewDutyRulesID = null;
                            //////    }
                            //////}
                            //////else
                            //////{
                            //////    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CrewDutyRule = null;
                            //////    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CrewDutyRulesID = null;
                            //////}
                            //////CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].IsDutyEnd = chkEndDuty.Checked;

                            decimal Overrideval = 0;
                            string overrideValue = "0";

                            if (!string.IsNullOrEmpty(hdnCROverRide.Value))
                            {
                                //overrideValue = tbOverride.TextWithLiterals;
                                overrideValue = hdnCROverRide.Value;
                            }
                            if (decimal.TryParse(overrideValue, out Overrideval))
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CROverRide = Overrideval;
                            else
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CROverRide = 0;


                            CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].FedAviationRegNUM = hdnFAR.Value;




                            decimal DutyHours = 0;
                            string DutyStr = "0";
                            CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DutyHours = 0;
                            if (!string.IsNullOrEmpty(hdnDutyHours.Value))
                            {
                                //if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                //{
                                //    DutyStr = ConvertMinToTenths(lbTotalDuty.Text, true, "1");
                                //}
                                //else
                                DutyStr = hdnDutyHours.Value;

                                if (decimal.TryParse(DutyStr, out DutyHours))
                                {

                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DutyHours = Math.Round(DutyHours, 1);
                                }
                            }



                            decimal FlightHours = 0;
                            string FlightHourstr = "0";
                            CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].FlightHours = 0;
                            if (!string.IsNullOrEmpty(hdnFlightHours.Value))
                            {
                                //if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                //{
                                //    FlightHourstr = ConvertMinToTenths(lbTotalFlight.Text, true, "1");
                                //}
                                //else
                                FlightHourstr = hdnFlightHours.Value;
                                if (decimal.TryParse(FlightHourstr, out FlightHours))
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].FlightHours = Math.Round(FlightHours, 1);
                            }


                            decimal RestHours = 0;
                            string RestHoursrstr = "0";
                            CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].RestHours = 0;
                            if (!string.IsNullOrEmpty(hdnRestHours.Value))
                            {
                                //if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                //{
                                //    RestHoursrstr = ConvertMinToTenths(lbRest.Text, true, "1");
                                //}
                                //else
                                RestHoursrstr = hdnRestHours.Value;
                                if (decimal.TryParse(RestHoursrstr, out RestHours))
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].RestHours = Math.Round(RestHours, 2);
                            }


                            #endregion

                            #region FlightDetails
                            //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].FlightNUM = tbFlightNumber.Text.Trim();

                            //if (cnklist.Items[0].Selected)
                            //    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].IsScheduledServices = true;
                            //else
                            //    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].IsScheduledServices = false;

                            //if (cnklist.Items[1].Selected)
                            //    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].IsPrivate = true;
                            //else
                            //    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].IsPrivate = false;

                            //if (cnklist.Items[2].Selected)
                            //    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].IsApproxTM = true;
                            //else
                            //    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].IsApproxTM = false;

                            //decimal FlightCost = 0;
                            //if (decimal.TryParse(tbCost.Text, out FlightCost))
                            //    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].FlightCost = FlightCost;
                            //else
                            //    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].FlightCost = 0;

                            //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].UWAID = tbUwaID.Text;
                            //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].USCrossing = tbBorderCrossing.Text;
                            //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ConfirmID = tbCBPConfirmNumber.Text;

                            #endregion

                            #region Requestor


                            CorporateRequestService.Passenger Pass = new CorporateRequestService.Passenger();
                            if (!string.IsNullOrEmpty(hdnPaxId.Value))
                            {
                                Int64 PassengerRequestorID = 0;
                                if (Int64.TryParse(hdnPaxId.Value, out PassengerRequestorID))
                                {
                                    Pass.PassengerRequestorID = PassengerRequestorID;
                                    Pass.PassengerRequestorCD = tbPassenger.Text;
                                    Pass.FirstName = lbPassengerName.Text;
                                    // CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].pa = lbRequestor.Text;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Passenger = Pass;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].PassengerRequestorID = PassengerRequestorID;
                                }
                                else
                                {
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Passenger = null;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].PassengerRequestorID = null;
                                }
                            }
                            else
                            {
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Passenger = null;
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].PassengerRequestorID = null;
                            }
                            //While binding get the Account ID base on the Accountnum
                            //CorpRequest.Account.AccountNum = tbAcctNo.Text;
                            //CorporateRequestService.Account Accountsample = new CorporateRequestService.Account();
                            //if (!string.IsNullOrEmpty(hdAccountNo.Value))
                            //{
                            //    Int64 AccountID = 0;
                            //    if (Int64.TryParse(hdAccountNo.Value, out AccountID))
                            //    {
                            //        Accountsample.AccountID = AccountID;
                            //        Accountsample.AccountNum = tbAccountNo.Text;
                            //        Accountsample.AccountDescription = lbAccountNo.Text;
                            //        CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].AccountID = AccountID;
                            //        CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Account = Accountsample;
                            //    }
                            //    else
                            //    {
                            //        CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].AccountID = null;
                            //        CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Account = null;
                            //    }

                            //}
                            //else
                            //{
                            //    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].AccountID = null;
                            //    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Account = null;
                            //}

                            CorporateRequestService.Department MainDepartment = new CorporateRequestService.Department();
                            if (!string.IsNullOrEmpty(hdDepartment.Value))
                            {
                                Int64 DepartmentID = 0;
                                if (Int64.TryParse(hdDepartment.Value, out DepartmentID))
                                {
                                    MainDepartment.DepartmentID = DepartmentID;
                                    MainDepartment.DepartmentCD = tbDepartment.Text;
                                    MainDepartment.DepartmentName = lbDepartmentName.Text;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Department = MainDepartment;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DepartmentID = DepartmentID;
                                    // CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DepartmentName = lbDepartment.Text;
                                }
                                else
                                {
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Department = null;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DepartmentID = null;
                                }
                            }
                            else
                            {
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Department = null;
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DepartmentID = null;
                            }


                            CorporateRequestService.DepartmentAuthorization MainDeptAuth = new CorporateRequestService.DepartmentAuthorization();
                            if (!string.IsNullOrEmpty(hdnAuthorizationId.Value))
                            {
                                Int64 AuthorizationID = 0;
                                if (Int64.TryParse(hdnAuthorizationId.Value, out AuthorizationID))
                                {
                                    MainDeptAuth.AuthorizationID = AuthorizationID;
                                    MainDeptAuth.AuthorizationCD = tbAuthorizationCode.Text;
                                    MainDeptAuth.DeptAuthDescription = lbAuthName.Text;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DepartmentAuthorization = MainDeptAuth;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].AuthorizationID = AuthorizationID;
                                    // CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].AuthorizationName = lbAuthorization.Text;
                                }
                                else
                                {
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].AuthorizationID = null;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DepartmentAuthorization = null;
                                }
                            }
                            else
                            {
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].AuthorizationID = null;
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DepartmentAuthorization = null;
                            }

                            CorporateRequestService.Client Clientsample = new CorporateRequestService.Client();
                            //if (!string.IsNullOrWhiteSpace(tbClient.Text))
                            //{
                            Int64 ClientID = 0;
                            if (Int64.TryParse(hdClient.Value, out ClientID))
                            {
                                Clientsample.ClientID = ClientID;
                                /// Clientsample.ClientCD = tbClient.Text;
                                //  Clientsample.ClientDescription = lbClient.Text;
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ClientID = ClientID;
                                //  CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Client = Clientsample;
                            }
                            else
                            {
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ClientID = null;
                                // CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ClientID = null;
                            }
                            //}
                            //else
                            //{
                            //    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ClientID = null;
                            //    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Client = null;
                            //}

                            CorporateRequestService.FlightCatagory FlightCategory = new CorporateRequestService.FlightCatagory();
                            if (!string.IsNullOrWhiteSpace(hdCategory.Value))
                            {
                                Int64 FlightCategoryID = 0;
                                if (Int64.TryParse(hdCategory.Value, out FlightCategoryID))
                                {
                                    FlightCategory.FlightCategoryID = FlightCategoryID;
                                    FlightCategory.FlightCatagoryCD = tbFlightCategoryCode.Text;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].FlightCategoryID = FlightCategoryID;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].FlightCatagory = FlightCategory;
                                }
                                else
                                {
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].FlightCategoryID = null;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].FlightCatagory = null;

                                }
                            }
                            else
                            {
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].FlightCategoryID = null;
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].FlightCatagory = null;

                            }

                            #endregion

                            #region Last Part

                            int PAXtotal = 0;
                            if (int.TryParse(tbPaxNumbers.Text, out PAXtotal))
                                CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].PassengerTotal = PAXtotal;

                            //int Availtotal = 0;
                            //if (int.TryParse(tbAvail.Text, out Availtotal))
                            //    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ReservationAvailable = Availtotal;


                            bool AddorUpdateLegnotes = false;

                            //if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == CorporateRequestTripEntityState.Added)
                            //{
                            //    if (IsAuthorized(Permission.Preflight.AddPreflightLegNotes))
                            //        AddorUpdateLegnotes = true;
                            //}

                            if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == CorporateRequestTripEntityState.Modified)
                            {
                                if (IsAuthorized(Permission.CorporateRequest.EditCRLeg))
                                    AddorUpdateLegnotes = true;
                            }

                            if (AddorUpdateLegnotes)
                            {
                                //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].Notes = tbPaxNotes.Text;
                                //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CrewNotes = tbCrewNotes.Text;


                                //if (!string.IsNullOrEmpty(lblCrewlastupatedhdn.Text))
                                //{

                                //    string datestr = lblCrewlastupatedhdn.Text;
                                //    datestr = datestr.Replace(" ", string.Empty);

                                //    string timestr = datestr.Substring(datestr.Length - 5, 5);
                                //    datestr = datestr.Replace(timestr, string.Empty);
                                //    int Hrst = Convert.ToInt16(timestr.Substring(0, 2));
                                //    int Mtstr = Convert.ToInt16(timestr.Substring(3, 2));


                                //    DateTime dt = DateTime.ParseExact(datestr, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                //    dt = dt.AddHours(Hrst);
                                //    dt = dt.AddMinutes(Mtstr);

                                //    // DateTime dtCrew = Convert.ToDateTime(lblCrewlastupated.Text);
                                //   // CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].LastUpdTSCrewNotes = dt;

                                //}

                                //if (!string.IsNullOrEmpty(lblPaxlastupatedhdn.Text))
                                //{
                                //    string datestr = lblPaxlastupatedhdn.Text;
                                //    datestr = datestr.Replace(" ", string.Empty);

                                //    string timestr = datestr.Substring(datestr.Length - 5, 5);
                                //    datestr = datestr.Replace(timestr, string.Empty);


                                //    int Hrst = Convert.ToInt16(timestr.Substring(0, 2));
                                //    int Mtstr = Convert.ToInt16(timestr.Substring(3, 2));


                                //    DateTime dt = DateTime.ParseExact(datestr, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                //    dt = dt.AddHours(Hrst);
                                //    dt = dt.AddMinutes(Mtstr);


                                //    // DateTime dtPax = Convert.ToDateTime(lblPaxlastupated.Text);
                                //    //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].LastUpdTSPaxNotes = dt;
                                //}
                            }
                            //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].FlightPurpose = tbPurpose.Text;
                            //CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].WaitList = tbWaitlist.Text;
                            CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].CheckGroup = hdnDomestic.Value;
                            //if (rbDomestic.SelectedItem.Value.ToUpper() == "DOM")
                            //    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DutyTYPE1 = 1;
                            //else
                            //    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].DutyTYPE1 = 2;

                            //if (!string.IsNullOrEmpty(tbNogo.Text))
                            //    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].GoTime = tbNogo.Text;
                            //else
                            //    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].GoTime = null;

                            #endregion

                            if (CorpRequest.Fleet != null)
                            {
                                if (CorpRequest.Fleet.MaximumPassenger != null && CorpRequest.Fleet.MaximumPassenger > 0)
                                {
                                    if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == CorporateRequestTripEntityState.Added)
                                    {
                                        CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].SeatTotal = Convert.ToInt32((decimal)CorpRequest.Fleet.MaximumPassenger);
                                        CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ReservationTotal = 0;
                                        CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].ReservationAvailable = CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].SeatTotal - CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].PassengerTotal;
                                        CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].WaitNUM = 0;
                                    }
                                }
                            }

                            Session["CurrentCorporateRequest"] = CorpRequest;
                        }
                    }
                }
            }

        }

        protected void LoadLegDetails(CRLeg Leg)
        {

            #region Authorization - Check for Add/Update access and make save cancel visible

            if (Leg.State == CorporateRequestTripEntityState.Modified)
            {
                if (!IsAuthorized(Permission.CorporateRequest.EditCRLeg))
                {
                    btnSave.Visible = false;
                    btnCancel.Visible = false;
                }
            }

            if (Leg.State == CorporateRequestTripEntityState.Added)
            {
                if (!IsAuthorized(Permission.CorporateRequest.AddCRLeg))
                {
                    btnSave.Visible = false;
                    btnCancel.Visible = false;
                }
            }

            #endregion

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Leg))
            {
                ClearFields();

                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];



                if (CorpRequest != null)
                {
                    hdnLeg.Value = System.Web.HttpUtility.HtmlEncode((CorpRequest.CRLegs.IndexOf(Leg) + 1).ToString());
                }

                #region Changes for Legnum in CRHeader
                Master.FloatLegNum.Text = System.Web.HttpUtility.HtmlEncode(Leg.LegNUM.ToString());
                #endregion

                #region Departs
                if (Leg.Airport1 != null)
                {
                    tbDepartsIcao.Text = Leg.Airport1.IcaoID;
                    tbDepartAirportName.Text = Leg.Airport1.AirportName != null ? System.Web.HttpUtility.HtmlEncode(Leg.Airport1.AirportName) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                    tbDepartCity.Text = Leg.Airport1.CityName != null ? System.Web.HttpUtility.HtmlEncode(Leg.Airport1.CityName) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                    hdDepart.Value = System.Web.HttpUtility.HtmlEncode(Leg.Airport1.AirportID.ToString());
                    using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
                        var objRetVal = objDstsvc.GetAirportByAirportICaoID(Leg.Airport1.IcaoID);
                        if (objRetVal.ReturnFlag)
                        {
                            AirportLists = objRetVal.EntityList;
                            if (AirportLists != null && AirportLists.Count > 0)
                            {

                                //if (!string.IsNullOrEmpty(AirportLists[0].Alerts) || !string.IsNullOrEmpty(AirportLists[0].GeneralNotes))
                                //{
                                //    if (UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert != null && UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert == true)
                                //    {
                                //        tbDepartsIcao.ForeColor = Color.Red;
                                //        if (AirportLists[0].Alerts != null)
                                //            tbDepartsIcao.ToolTip = AirportLists[0].Alerts;
                                //        //if (AirportLists[0].GeneralNotes != null)
                                //        //    tbDepartsIcao.ToolTip = tbDepartsIcao.ToolTip + AirportLists[0].GeneralNotes;
                                //    }
                                //    else
                                //    {
                                //        tbDepartsIcao.ForeColor = Color.Black;
                                //    }
                                //}
                                //else
                                //{
                                //    tbDepartsIcao.ForeColor = Color.Black;
                                //}

                                // Fix for UW-1165(8179)
                                string tooltipStr = string.Empty;

                                if (UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert != null && UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert == true)
                                {
                                    if (!string.IsNullOrEmpty(AirportLists[0].Alerts))
                                    {
                                        //TxtBx.ToolTip = AirportList[0].Alerts.ToString();
                                        string alertStr = "Alerts : \n";
                                        alertStr += AirportLists[0].Alerts;
                                        tooltipStr = alertStr;
                                        tbDepartsIcao.ForeColor = Color.Red;
                                    }
                                    if (!string.IsNullOrEmpty(AirportLists[0].GeneralNotes))
                                    {
                                        string noteStr = string.Empty;
                                        if (!string.IsNullOrEmpty(tooltipStr))
                                            noteStr = "\n\nNotes : \n";
                                        else
                                            noteStr = "Notes : \n";
                                        noteStr += AirportLists[0].GeneralNotes;
                                        tooltipStr += noteStr;
                                    }
                                    tbDepartsIcao.ToolTip = tooltipStr;
                                }
                                else
                                {
                                    tbDepartsIcao.ForeColor = Color.Black;
                                }                                                           
                            }
                        }
                    }

                }
                else
                {
                    tbDepartsIcao.Text = "";
                    tbDepartAirportName.Text = System.Web.HttpUtility.HtmlEncode("");
                    tbDepartCity.Text = System.Web.HttpUtility.HtmlEncode("");
                    hdDepart.Value = System.Web.HttpUtility.HtmlEncode("");
                }

                if (Leg.DepartureDTTMLocal != DateTime.MinValue && Leg.DepartureDTTMLocal != null)
                {
                    if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat != null && UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 2)
                    {
                        DateTime dt = (DateTime)Leg.DepartureDTTMLocal;
                        string startHrs = "0000" + dt.Hour.ToString();
                        string startMins = "0000" + dt.Minute.ToString();
                        tbDepartsLocal.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", Leg.DepartureDTTMLocal);
                        rmbDepartsLocal.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                        DepTimeForm.Visible = false;
                    }
                    else
                    {
                        DateTime dt = (DateTime)Leg.DepartureDTTMLocal;
                        string startHrs = "0000" + dt.Hour.ToString();
                        string startMins = "0000" + dt.Minute.ToString();
                        tbDepartsLocal.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", Leg.DepartureDTTMLocal);
                        rmbDepartsLocal.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                        DepTimeForm.Visible = true;
                        string AMPM = dt.ToString("tt");
                        if (AMPM == "AM" || AMPM == "am")
                        {
                            DepTimeForm.SelectedValue = "1";
                        }
                        else
                        {
                            int Hour = dt.Hour - 12;
                            string startHour = "0000" + Hour.ToString();
                            string startMinutes = "0000" + dt.Minute.ToString();
                            rmbDepartsLocal.Text = startHour.Substring(startHour.Length - 2, 2) + ":" + startMinutes.Substring(startMinutes.Length - 2, 2);

                            DepTimeForm.SelectedValue = "2";
                        }
                    }
                }
                else
                {
                    tbDepartsLocal.Text = "";
                    rmbDepartsLocal.Text = "0000";
                }

                //tbLocaltime.Text = Leg.DepartsLocalhhmm;

                if (Leg.DepartureGreenwichDTTM != DateTime.MinValue && Leg.DepartureGreenwichDTTM != null)
                {
                    DateTime dt = (DateTime)Leg.DepartureGreenwichDTTM;
                    string startHrs = "0000" + dt.Hour.ToString();
                    string startMins = "0000" + dt.Minute.ToString();
                    tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", Leg.DepartureGreenwichDTTM);
                    rmtUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                }
                else
                {
                    tbUtcDate.Text = "";
                    rmtUtctime.Text = "0000";
                }

                //tbUtctime.Text = Leg.DepartsUTChhmm;

                if (Leg.HomeDepartureDTTM != DateTime.MinValue && Leg.HomeDepartureDTTM != null)
                {
                    DateTime dt = (DateTime)Leg.HomeDepartureDTTM;
                    string startHrs = "0000" + dt.Hour.ToString();
                    string startMins = "0000" + dt.Minute.ToString();
                    tbHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", Leg.HomeDepartureDTTM);
                    rmtHomeTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                }
                else
                {
                    tbHomeDate.Text = "";
                    rmtHomeTime.Text = "0000";
                }

                //tbHomeTime.Text = Leg.DepartsHomehhmm;

                #endregion

                #region Arrives

                if (Leg.Airport != null)
                {
                    tbArrivalIcao.Text = Leg.Airport.IcaoID;
                    lbArrivalAirportName.Text = Leg.Airport.AirportName != null ? System.Web.HttpUtility.HtmlEncode(Leg.Airport.AirportName) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                    tbArrivalCity.Text = Leg.Airport.CityName != null ? System.Web.HttpUtility.HtmlEncode(Leg.Airport.CityName) : System.Web.HttpUtility.HtmlEncode(string.Empty);
                    hdArrival.Value = System.Web.HttpUtility.HtmlEncode(Leg.Airport.AirportID.ToString());
                    using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objDstsvc.GetAirportByAirportICaoID(Leg.Airport.IcaoID);
                        List<FlightPakMasterService.GetAllAirport> getAllAirportList = new List<GetAllAirport>();

                        if (objRetVal.ReturnFlag)
                        {
                            getAllAirportList = objRetVal.EntityList;

                            if (getAllAirportList != null && getAllAirportList.Count > 0)
                            {
                                //if (!string.IsNullOrEmpty(getAllAirportList[0].Alerts) || !string.IsNullOrEmpty(getAllAirportList[0].GeneralNotes))
                                //{
                                //    if (UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert != null && UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert == true)
                                //    {
                                //        tbArrivalIcao.ForeColor = Color.Red;
                                //        if (getAllAirportList[0].Alerts != null)
                                //            tbArrivalIcao.ToolTip = getAllAirportList[0].Alerts;
                                //        //if (getAllAirportList[0].GeneralNotes != null)
                                //        //    tbArrivalIcao.ToolTip = tbArrivalIcao.ToolTip + getAllAirportList[0].GeneralNotes;
                                //    }
                                //    else
                                //    {
                                //        tbArrivalIcao.ForeColor = Color.Black;
                                //    }
                                //}
                                //else
                                //{
                                //    tbArrivalIcao.ForeColor = Color.Black;
                                //}
                                // Fix for UW-1165(8179)
                                string tooltipStr = string.Empty;

                                if (UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert != null && UserPrincipal.Identity._fpSettings._IsCorpReqActivateAlert == true)
                                {
                                    if (!string.IsNullOrEmpty(getAllAirportList[0].Alerts))
                                    {
                                        //TxtBx.ToolTip = AirportList[0].Alerts.ToString();
                                        string alertStr = "Alerts : \n";
                                        alertStr += getAllAirportList[0].Alerts;
                                        tooltipStr = alertStr;
                                        tbArrivalIcao.ForeColor = Color.Red;
                                    }
                                    if (!string.IsNullOrEmpty(getAllAirportList[0].GeneralNotes))
                                    {
                                        string noteStr = string.Empty;
                                        if (!string.IsNullOrEmpty(tooltipStr))
                                            noteStr = "\n\nNotes : \n";
                                        else
                                            noteStr = "Notes : \n";
                                        noteStr += getAllAirportList[0].GeneralNotes;
                                        tooltipStr += noteStr;
                                    }
                                    tbArrivalIcao.ToolTip = tooltipStr;
                                }
                                else
                                {
                                    tbDepartsIcao.ForeColor = Color.Black;
                                }      
                            }
                        }
                    }
                }
                else
                {
                    tbArrivalIcao.Text = "";
                    lbArrivalAirportName.Text = System.Web.HttpUtility.HtmlEncode("");
                    tbArrivalCity.Text = System.Web.HttpUtility.HtmlEncode("");
                    hdArrival.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                }

                if (Leg.ArrivalDTTMLocal != DateTime.MinValue && Leg.ArrivalDTTMLocal != null)
                {
                    if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat != null && UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 2)
                    {
                        DateTime dt = (DateTime)Leg.ArrivalDTTMLocal;
                        string startHrs = "0000" + dt.Hour.ToString();
                        string startMins = "0000" + dt.Minute.ToString();
                        tbArrivalLocal.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", Leg.ArrivalDTTMLocal);
                        rmbArrivalLocalTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                        ArrTimeForm.Visible = false;
                    }
                    else
                    {
                        DateTime dt = (DateTime)Leg.ArrivalDTTMLocal;
                        string startHrs = "0000" + dt.Hour.ToString();
                        string startMins = "0000" + dt.Minute.ToString();
                        tbArrivalLocal.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", Leg.ArrivalDTTMLocal);
                        rmbArrivalLocalTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                        ArrTimeForm.Visible = true;
                        string AMPM = dt.ToString("tt");
                        if (AMPM == "AM" || AMPM == "am")
                        {
                            ArrTimeForm.SelectedValue = "1";
                        }
                        else
                        {
                            int Hour = dt.Hour - 12;
                            string startHour = "0000" + Hour.ToString();
                            string startMinutes = "0000" + dt.Minute.ToString();
                            rmbArrivalLocalTime.Text = startHour.Substring(startHour.Length - 2, 2) + ":" + startMinutes.Substring(startMinutes.Length - 2, 2);

                            ArrTimeForm.SelectedValue = "2";
                        }
                    }
                }
                else
                {
                    tbArrivalLocal.Text = "";
                    rmbArrivalLocalTime.Text = "0000";
                }


                ////tbArrivalTime.Text = Leg.ArrivesLocalhhmm;

                if (Leg.ArrivalGreenwichDTTM != DateTime.MinValue && Leg.ArrivalGreenwichDTTM != null)
                {
                    DateTime dt = (DateTime)Leg.ArrivalGreenwichDTTM;
                    string startHrs = "0000" + dt.Hour.ToString();
                    string startMins = "0000" + dt.Minute.ToString();
                    tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", Leg.ArrivalGreenwichDTTM);
                    rmtArrivalUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                }
                else
                {
                    tbArrivalUtcDate.Text = "";
                    rmtArrivalUtctime.Text = "0000";
                }
                //tbArrivalUtctime.Text = Leg.ArrivesUTChhmm;

                if (Leg.HomeArrivalDTTM != DateTime.MinValue && Leg.HomeArrivalDTTM != null)
                {
                    DateTime dt = (DateTime)Leg.HomeArrivalDTTM;
                    string startHrs = "0000" + dt.Hour.ToString();
                    string startMins = "0000" + dt.Minute.ToString();
                    tbArrivalHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", Leg.HomeArrivalDTTM);
                    rmtArrivalHomeTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                }
                else
                {
                    tbArrivalHomeDate.Text = "";
                    rmtArrivalHomeTime.Text = "0000";
                }

                //tbArrivalHomeTime.Text = Leg.ArrivesHomehhmm;

                #endregion

                #region Distance & Time
                tbDistance.Text = "0";
                if (Leg.Distance != null)
                {
                    tbDistance.Text = ConvertToKilomenterBasedOnCompanyProfile((decimal)Leg.Distance).ToString();
                    hdnMiles.Value = Leg.Distance.ToString();
                }


                if ((CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].IsPrivate != null) && (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].IsPrivate == true))
                {
                    chkPrivate.Checked = true;
                }
                else
                {
                    chkPrivate.Checked = false;
                }
                hdnDomestic.Value = Leg.WindReliability.ToString();
                hdnPower.Value = Leg.PowerSetting;
                if (Leg.TakeoffBIAS != null)
                {
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                    {
                        hdnTakeoffBIAS.Value = Math.Round((decimal)Leg.TakeoffBIAS, 2).ToString();
                        hdnTakeoffBIAS.Value = "00:00";

                        hdnTakeoffBIAS.Value = ConvertTenthsToMins(Leg.TakeoffBIAS.ToString());
                    }
                    else
                    {
                        hdnTakeoffBIAS.Value = "0.0";
                        hdnTakeoffBIAS.Value = Math.Round((decimal)Leg.TakeoffBIAS, 1).ToString();
                    }
                }
                hdnTrueAirSpeed.Value = Leg.TrueAirSpeed.ToString();

                if (Leg.LandingBIAS != null)
                {
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                    {
                        hdnLandingBIAS.Value = "00:00";
                        hdnLandingBIAS.Value = ConvertTenthsToMins((Leg.LandingBIAS).ToString());
                    }
                    else
                    {
                        hdnLandingBIAS.Value = "0.0";
                        hdnLandingBIAS.Value = Math.Round((decimal)Leg.LandingBIAS, 1).ToString();
                    }
                }
                // To check whether the Aircraft has 0 due to 'convert to decimal' method & return to it's 0.0 default format
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                {
                    if (!string.IsNullOrEmpty(hdnTakeoffBIAS.Value))
                    {
                        if (hdnTakeoffBIAS.Value.IndexOf(".") < 0)
                        {
                            hdnTakeoffBIAS.Value = hdnTakeoffBIAS.Value + ".0";
                        }

                    }

                    if (!string.IsNullOrEmpty(hdnLandingBIAS.Value))
                    {
                        if (hdnLandingBIAS.Value.IndexOf(".") < 0)
                        {
                            hdnLandingBIAS.Value = hdnLandingBIAS.Value + ".0";
                        }

                    }
                }

                hdnWindBoeingTable.Value = Leg.WindsBoeingTable.ToString();

                hdnWindReliability.Value = Leg.WindReliability != null ? Leg.WindReliability.ToString() : "3";
                double ETE = 0.0;
                if (Leg.ElapseTM != null)
                {
                    ETE = RoundElpTime((double)Leg.ElapseTM);
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        tbElapsedTime.Text = ConvertTenthsToMins(ETE.ToString());
                    else
                    {

                        tbElapsedTime.Text = Math.Round(ETE, 1).ToString();
                        if (tbElapsedTime.Text.IndexOf(".") < 0)
                            tbElapsedTime.Text = tbElapsedTime.Text + ".0";

                    }
                }




                #endregion

                #region Duty

                //if (Leg.CrewDutyRule != null)
                //{
                //    tbCrewRules.Text = Leg.CrewDutyRule.CrewDutyRuleCD;
                //    hdCrewRules.Value = System.Web.HttpUtility.HtmlEncode(Leg.CrewDutyRulesID.ToString());
                //}

                //string lcCdAlert = string.Empty;
                //if (Leg.CrewDutyAlert != null)
                //    lcCdAlert = Leg.CrewDutyAlert;
                //if (lcCdAlert.LastIndexOf('D') >= 0)
                //{
                //    lbTotalDuty.CssClass = System.Web.HttpUtility.HtmlEncode("infored");
                //    //lbTotalDuty.ForeColor = Color.White;
                //    //lbTotalDuty.BackColor = Color.Red;
                //}
                //else
                //{
                //    lbTotalDuty.CssClass = System.Web.HttpUtility.HtmlEncode("infoash");
                //    //lbTotalDuty.ForeColor = Color.Black;
                //    //lbTotalDuty.BackColor = Color.Transparent;
                //}
                //if (lcCdAlert.LastIndexOf('F') >= 0)
                //{
                //    lbTotalFlight.CssClass = System.Web.HttpUtility.HtmlEncode("infored");
                //    //lbTotalFlight.ForeColor = Color.White;
                //    //lbTotalFlight.BackColor = Color.Red;
                //}
                //else
                //{
                //    lbTotalFlight.CssClass = System.Web.HttpUtility.HtmlEncode("infoash");
                //    //lbTotalFlight.ForeColor = Color.Black;
                //    //lbTotalFlight.BackColor = Color.Transparent;
                //}
                //if (lcCdAlert.LastIndexOf('R') >= 0)
                //{
                //    lbRest.CssClass = System.Web.HttpUtility.HtmlEncode("infored");
                //    //lbRest.ForeColor = Color.White;
                //    //lbRest.BackColor = Color.Red;
                //}
                //else
                //{
                //    lbRest.CssClass = System.Web.HttpUtility.HtmlEncode("infoash");
                //    //lbRest.ForeColor = Color.Black;
                //    //lbRest.BackColor = Color.Transparent;
                //}

                //chkEndDuty.Checked = Leg.IsDutyEnd == true ? true : false;
                ////ETEStr = tbETE.TextWithLiterals;
                //tbOverride.Text = "0.0";

                //if (Leg.OverrideValue != null)
                //{
                //    if ((decimal)Leg.OverrideValue != 0)
                //        tbOverride.Text = Math.Round((decimal)Leg.OverrideValue, 1).ToString();
                //}


                if (Leg.FedAviationRegNUM != null)
                    hdnFAR.Value = System.Web.HttpUtility.HtmlEncode(Leg.FedAviationRegNUM.ToString());


                hdnDutyHours.Value = System.Web.HttpUtility.HtmlEncode("0.0");
                if (Leg.DutyHours != null && Leg.DutyHours != 0)
                {
                    //if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                    //    lbTotalDuty.Text = ConvertTenthsToMins(Leg.DutyHours.ToString());
                    //else
                    //{

                    double dutyHours = Math.Round((double)Leg.DutyHours, 1);
                    hdnDutyHours.Value = System.Web.HttpUtility.HtmlEncode(dutyHours.ToString());

                    if (hdnDutyHours.Value.IndexOf(".") < 0)
                    {
                        hdnDutyHours.Value = System.Web.HttpUtility.HtmlEncode(hdnDutyHours.Value + ".0");
                    }
                    //}

                }
                else
                    hdnDutyHours.Value = System.Web.HttpUtility.HtmlEncode("0.0");

                hdnDutyHours.Value = System.Web.HttpUtility.HtmlEncode("0.0");
                if (Leg.FlightHours != null && Leg.FlightHours != 0)
                {
                    //if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                    //    lbTotalFlight.Text = ConvertTenthsToMins(Leg.FlightHours.ToString());
                    //else
                    //{
                    double totalFlightHours = Math.Round((double)Leg.FlightHours, 1);
                    hdnDutyHours.Value = System.Web.HttpUtility.HtmlEncode(totalFlightHours.ToString());
                    if (hdnDutyHours.Value.IndexOf(".") < 0)
                    {
                        hdnDutyHours.Value = System.Web.HttpUtility.HtmlEncode(hdnDutyHours.Value + ".0");
                    }
                    //}
                }
                else
                    hdnDutyHours.Value = System.Web.HttpUtility.HtmlEncode("0.0");
                hdnRestHours.Value = System.Web.HttpUtility.HtmlEncode("0.0");
                if (Leg.RestHours != null && Leg.RestHours != 0)
                {
                    //if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                    //    lbRest.Text = ConvertTenthsToMins(Leg.RestHours.ToString());
                    //else
                    //{
                    double restHours = Math.Round((double)Leg.RestHours, 1);
                    hdnRestHours.Value = System.Web.HttpUtility.HtmlEncode(restHours.ToString());
                    if (hdnRestHours.Value.IndexOf(".") < 0)
                    {
                        hdnRestHours.Value = System.Web.HttpUtility.HtmlEncode(hdnRestHours.Value + ".0");
                    }
                    //}
                }
                else
                    hdnRestHours.Value = System.Web.HttpUtility.HtmlEncode("0.0");

                #endregion

                #region Flight
                //if (!string.IsNullOrEmpty(Leg.FlightNUM))
                //    tbFlightNumber.Text = Leg.FlightNUM.Trim();
                //else
                //    if (!string.IsNullOrEmpty(CorpRequest.FlightNUM))
                //        tbFlightNumber.Text = CorpRequest.FlightNUM.Trim();


                //cnklist.Items[0].Selected = Leg.IsScheduledServices == true ? true : false;
                //cnklist.Items[1].Selected = Leg.IsPrivate == true ? true : false;
                //cnklist.Items[2].Selected = Leg.IsApproxTM == true ? true : false;

                //tbCost.Text = "0.00";

                //if (Leg.FlightCost != null)
                //{
                //    if ((decimal)Leg.FlightCost != 0)
                //        tbCost.Text = Math.Round((decimal)Leg.FlightCost, 2).ToString();
                //}
                //tbUwaID.Text = Leg.UWAID;
                //tbCBPConfirmNumber.Text = Leg.ConfirmID;
                //tbBorderCrossing.Text = Leg.USCrossing;


                hdnDomestic.Value = Leg.CheckGroup;



                ////foreach (ListItem item in rbDomestic.Items)
                ////{
                ////    if (item.Value == Leg.CheckGroup)
                ////    {
                ////        item.Selected = true;
                ////        break;
                ////    }
                ////}
                ////if (Leg.DutyTYPE1 != null)
                ////{
                ////    if (Leg.DutyTYPE1 == 1)
                ////        rbDomestic.SelectedValue = "DOM";
                ////    else
                ////        rbDomestic.SelectedValue = "INT";
                ////}
                #endregion

                #region  Requestor

                if (Leg.Passenger != null)
                {
                    tbPassenger.Text = Leg.Passenger.PassengerRequestorCD;
                    lbPassengerName.Text = System.Web.HttpUtility.HtmlEncode(Leg.Passenger.FirstName) + System.Web.HttpUtility.HtmlEncode((string.IsNullOrWhiteSpace(Leg.Passenger.MiddleInitial)) ? System.Web.HttpUtility.HtmlEncode(string.Empty) : System.Web.HttpUtility.HtmlEncode((" " + Leg.Passenger.MiddleInitial))) + System.Web.HttpUtility.HtmlEncode((string.IsNullOrWhiteSpace(Leg.Passenger.LastName)) ? System.Web.HttpUtility.HtmlEncode(string.Empty) : System.Web.HttpUtility.HtmlEncode((" " + Leg.Passenger.LastName)));
                    hdnPaxId.Value = System.Web.HttpUtility.HtmlEncode(Leg.PassengerRequestorID.ToString());
                }

                if (Leg.Department != null)
                {
                    tbDepartment.Text = Leg.Department.DepartmentCD;
                    lbDepartmentName.Text = System.Web.HttpUtility.HtmlEncode(Leg.Department.DepartmentName);
                    hdDepartment.Value = System.Web.HttpUtility.HtmlEncode(Leg.DepartmentID.ToString());
                }

                if (Leg.DepartmentAuthorization != null)
                {
                    tbAuthorizationCode.Text = Leg.DepartmentAuthorization.AuthorizationCD;
                    lbAuthName.Text = System.Web.HttpUtility.HtmlEncode(Leg.DepartmentAuthorization.DeptAuthDescription);
                    hdnAuthorizationId.Value = System.Web.HttpUtility.HtmlEncode(Leg.AuthorizationID.ToString());
                }

                if (Leg.ClientID != null)
                {
                    hdClient.Value = System.Web.HttpUtility.HtmlEncode(Leg.ClientID.ToString());
                }


                if (Leg.FlightCatagory != null)
                {
                    tbFlightCategoryCode.Text = Leg.FlightCatagory.FlightCatagoryCD;
                    hdCategory.Value = System.Web.HttpUtility.HtmlEncode(Leg.FlightCategoryID.ToString());
                }
                #endregion

                #region LastPart


                if (CorpRequest.Fleet != null)
                {
                    if (CorpRequest.Fleet.MaximumPassenger != null && CorpRequest.Fleet.MaximumPassenger > 0)
                    {
                        if (Leg.State == CorporateRequestTripEntityState.Added)
                        {
                            Leg.SeatTotal = Convert.ToInt32((decimal)CorpRequest.Fleet.MaximumPassenger);
                            Leg.ReservationTotal = 0;
                            Leg.ReservationAvailable = Leg.SeatTotal - Leg.PassengerTotal;
                            Leg.WaitNUM = 0;
                        }
                    }
                }

                //if (Leg.GoTime != null)
                //    tbNogo.Text = Leg.GoTime;

                if (Leg.PassengerTotal != null)
                    tbPaxNumbers.Text = Leg.PassengerTotal.ToString();
                //if (Leg.ReservationAvailable != null)
                //    if (Leg.ReservationAvailable < 0)
                //    {
                //        tbAvail.ForeColor = Color.Red;
                //        tbAvail.Text = Leg.ReservationAvailable.ToString();
                //    }
                //    else
                //        tbAvail.Text = Leg.ReservationAvailable.ToString();
                //    tbLegNotes.Text = Leg.Notes;

                //if (IsAuthorized(Permission.Preflight.ViewPreflightLegNotes))
                //{
                //    tbCrewNotes.Text = Leg.CrewNotes;
                //    if (string.IsNullOrEmpty(tbCrewNotes.Text))
                //    {
                //        lblCrewlastupated.Text = string.Empty;
                //        lblCrewlastupatedhdn.Text = string.Empty;
                //    }
                //    if (Leg.LastUpdTSCrewNotes != null)
                //    {
                //        lblCrewlastupated.Visible = true;

                //        DateTime date = (DateTime)Leg.LastUpdTSCrewNotes;
                //        if (date != null)
                //        {
                //            DateTime LastModifiedUTCDate = DateTime.Now;
                //            if (UserPrincipal.Identity._airportId != null)
                //            {
                //                using (CalculationServiceClient CalcSvc = new CalculationServiceClient())
                //                {
                //                    LastModifiedUTCDate = CalcSvc.GetGMT(UserPrincipal.Identity._airportId, (DateTime)date, false, false);
                //                }
                //            }
                //            else
                //                LastModifiedUTCDate = (DateTime)date;
                //            lblCrewlastupated.Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + " HH:mm}", LastModifiedUTCDate));
                //            lblCrewlastupatedhdn.Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + " HH:mm}", (DateTime)Leg.LastUpdTSCrewNotes));
                //        }
                //    }
                //    tbPaxNotes.Text = Leg.Notes;
                //    if (string.IsNullOrEmpty(tbPaxNotes.Text))
                //    {
                //        lblPaxlastupated.Text = string.Empty;
                //        lblPaxlastupatedhdn.Text = string.Empty;
                //    }
                //    if (Leg.LastUpdTSPaxNotes != null)
                //    {
                //        lblPaxlastupated.Visible = true;
                //        DateTime paxdate = (DateTime)Leg.LastUpdTSPaxNotes;
                //        if (paxdate != null)
                //        {
                //            DateTime LastModifiedUTCDate = DateTime.Now;
                //            if (UserPrincipal.Identity._airportId != null)
                //            {
                //                using (CalculationServiceClient CalcSvc = new CalculationServiceClient())
                //                {
                //                    LastModifiedUTCDate = CalcSvc.GetGMT(UserPrincipal.Identity._airportId, (DateTime)paxdate, false, false);
                //                }
                //            }
                //            else
                //                LastModifiedUTCDate = (DateTime)paxdate;
                //            lblPaxlastupated.Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + " HH:mm}", LastModifiedUTCDate));
                //            lblPaxlastupatedhdn.Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + " HH:mm}", (DateTime)Leg.LastUpdTSPaxNotes));
                //        }
                //    }
                //}
                //tbPurpose.Text = Leg.FlightPurpose;
                ////tbWaitlist.Text = Leg.WaitList;

                #endregion

            }


        }
        protected int GetLegnumToUpdate(CRMain Trip, string Mode)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip, Mode))
            {
                Trip = (CRMain)Session["CurrentCorporateRequest"];

                int setLegNum = 0;
                switch (Mode)
                {
                    case "Add":
                        {
                            int previndex = 0;
                            previndex = rtsLegs.Tabs[rtsLegs.Tabs.Count - 1].Index;
                            if (previndex == 0)
                                setLegNum = 2;
                            else
                            {
                                setLegNum = (int)CorpRequest.CRLegs[Convert.ToInt32(rtsLegs.Tabs[previndex].Value)].LegNUM + 1;
                            }
                        }
                        break;
                    case "Insert":
                        {

                            int previndex = 0;
                            //previndex = rtsLegs.Tabs[Convert.ToInt16(hdnLeg.Value) - 1].Index;
                            previndex = rtsLegs.SelectedIndex;

                            if (previndex == 0)
                                setLegNum = 1;
                            else
                                setLegNum = (int)CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM;


                            List<CRLeg> PrefLegs = new List<CRLeg>();
                            PrefLegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                            if (PrefLegs != null && PrefLegs.Count > 0)
                            {
                                foreach (CRLeg Leg in PrefLegs)
                                {
                                    if (Leg.LegNUM >= CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM)
                                    {
                                        Leg.LegNUM = Leg.LegNUM + 1;
                                        if (Leg.CRLegID != 0 && Leg.State != CorporateRequestTripEntityState.Deleted)
                                            Leg.State = CorporateRequestTripEntityState.Modified;
                                    }
                                }
                            }


                        }
                        break;
                    case "Delete":
                        {
                            int previndex = 0;
                            //previndex = rtsLegs.Tabs[Convert.ToInt16(hdnLeg.Value) - 1].Index;
                            previndex = rtsLegs.SelectedIndex;

                            if (previndex != rtsLegs.Tabs.Count - 1)
                            {

                                List<CRLeg> PrefLegs = new List<CRLeg>();
                                PrefLegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                                if (PrefLegs != null && PrefLegs.Count > 0)
                                {
                                    foreach (CRLeg Leg in PrefLegs)
                                    {
                                        if (Leg.LegNUM > CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM)
                                        {
                                            Leg.LegNUM = Leg.LegNUM - 1;
                                            if (Leg.CRLegID != 0 && Leg.State != CorporateRequestTripEntityState.Deleted)
                                                Leg.State = CorporateRequestTripEntityState.Modified;
                                        }
                                    }
                                }
                            }
                        }
                        break;
                }
                Session["CurrentCorporateRequest"] = Trip;
                return setLegNum;
            }

        }
        public void EnableForm(bool status)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(status))
            {
                tbDepartCity.Enabled = status;
                //tbDepartsIcao.Enabled = status;
                //Fix for IE
                tbArrivalIcao.ReadOnly = !status;
                //lbCr.Text = "";
                tbDepartAirportName.Enabled = status;
                tbArrivalCity.Enabled = status;
                btnCategoryLegs.Enabled = status;
                //tbArrivalIcao.Enabled = status;
                //Fix for IE
                tbArrivalIcao.ReadOnly = !status;
                lbArrivalAirportName.Enabled = status;
                tbDepartsLocal.Enabled = status;
                tbPassenger.Enabled = status;
                tbAuthorizationCode.Enabled = status;
                lbPassengerName.Enabled = status;
                lbAuthName.Enabled = status;
                tbDepartment.Enabled = status;
                tbFlightCategoryCode.Enabled = status;
                lbDepartmentName.Enabled = status;
                lbFlightCategoryName.Enabled = status;
                tbArrivalLocal.Enabled = status;
                tbDistance.Enabled = status;
                chkPrivate.Enabled = status;
                tbPaxNumbers.Enabled = status;
                tbElapsedTime.Enabled = status;

                rmbArrivalLocalTime.Enabled = status;
                rmbDepartsLocal.Enabled = status;

                //btnArrivalAirportAlerts.Enabled = status;
                //btnArrivalAirportCity.Enabled = status;
                btnAuthorizationCode.Enabled = status;
                //btnClosestIcao.Enabled = status;
                btnClosestArrivalICAO.Enabled = status;
                //btnDepartAirportAlerts.Enabled = status;
                btnDepartment.Enabled = status;
                btnDepartsICAO.Enabled = status;
                btnClosestIcao.Enabled = status;
                btnArrivalAirportCity.Enabled = status;
                btnFlightCategory.Enabled = status;
                btnPassenger.Enabled = status;
                DepTimeForm.Enabled = status;
                ArrTimeForm.Enabled = status;
                //btnDeleteTrip.Enabled = status;
                btnDeleteLeg.Enabled = status;
                //btnCancellationReq.Enabled = status;
                //btnNext.Enabled = status;
                btnSave.Enabled = status;
                btnCancel.Enabled = status;
                btnAddLeg1.Enabled = status;
                btnDeleteLeg.Enabled = status;
                btnInsertLeg.Enabled = status;
                //btnSubmitReq.Enabled = status;

                if (status)
                {
                    btnDeleteLeg.CssClass = "ui_nav";
                    btnAddLeg1.CssClass = "ui_nav";
                    btnDeleteLeg.CssClass = "ui_nav";
                    btnInsertLeg.CssClass = "ui_nav";

                    btnAuthorizationCode.CssClass = "browse-button";
                    btnClosestArrivalICAO.CssClass = "browse-button";
                    btnDepartment.CssClass = "browse-button";
                    btnDepartsICAO.CssClass = "browse-button";
                    btnFlightCategory.CssClass = "browse-button";
                    btnPassenger.CssClass = "browse-button";
                    btnClosestIcao.CssClass = "browse-button";
                    btnArrivalAirportCity.CssClass = "browse-button";
                    btnCategoryLegs.CssClass = "copy-icon";
                }
                else
                {

                    btnDeleteLeg.CssClass = "ui_nav_disable";
                    //btnCancellationReq.CssClass = "ui_nav_disable";
                    //btnNext.CssClass = "ui_nav_disable";
                    btnSave.CssClass = "ui_nav_disable";
                    btnCancel.CssClass = "ui_nav_disable";
                    btnAddLeg1.CssClass = "ui_nav_disable";
                    btnDeleteLeg.CssClass = "ui_nav_disable";
                    btnInsertLeg.CssClass = "ui_nav_disable";

                    btnAuthorizationCode.CssClass = "browse-button-disabled";
                    btnClosestArrivalICAO.CssClass = "browse-button-disabled";
                    btnDepartment.CssClass = "browse-button-disabled";
                    btnClosestIcao.CssClass = "browse-button-disabled";
                    btnArrivalAirportCity.CssClass = "browse-button-disabled";
                    btnDepartsICAO.CssClass = "browse-button-disabled";
                    btnFlightCategory.CssClass = "browse-button-disabled";
                    btnPassenger.CssClass = "browse-button-disabled";
                    btnCategoryLegs.CssClass = "copy-icon-disabled";
                }
            }
        }
        public void ClearFields()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbDepartCity.Text = "";
                tbDepartsIcao.Text = "";
                //lbCr.Text = "";
                tbDepartAirportName.Text = "";
                tbArrivalCity.Text = "";
                tbArrivalIcao.Text = "";
                lbArrivalAirportName.Text = "";
                tbDepartsLocal.Text = "";
                tbPassenger.Text = "";
                tbAuthorizationCode.Text = "";
                lbPassengerName.Text = "";
                lbAuthName.Text = "";
                tbDepartment.Text = "";
                tbFlightCategoryCode.Text = "";
                lbDepartmentName.Text = "";
                lbFlightCategoryName.Text = "";
                tbArrivalLocal.Text = "";
                tbDistance.Text = "0";
                chkPrivate.Checked = false;
                tbPaxNumbers.Text = "0";

                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                {
                    tbElapsedTime.Text = "00:00";
                }
                else
                {
                    tbElapsedTime.Text = "0.0";
                }

                rmbArrivalLocalTime.Text = "0000";
                rmbDepartsLocal.Text = "0000";

                hdDepart.Value = "";
                hdArrival.Value = "";
                hdnAuthorizationId.Value = "";
                hdDepartment.Value = "";
                hdnPaxId.Value = "";
                hdCategory.Value = "";

                tbUtcDate.Text = "";
                rmtUtctime.Text = "0000";

                tbArrivalUtcDate.Text = "";
                rmtArrivalUtctime.Text = "0000";

                tbHomeDate.Text = "";
                rmtHomeTime.Text = "0000";

                tbArrivalHomeDate.Text = "";
                rmtArrivalHomeTime.Text = "0000";


            }
        }

        private FlightPak.Web.FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId GetCompany(Int64 HomeBaseID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomeBaseID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    GetAllCompanyMasterbyHomeBaseId Companymaster = new GetAllCompanyMasterbyHomeBaseId();
                    var objCompany = objDstsvc.GetListInfoByHomeBaseId(HomeBaseID);

                    if (objCompany.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId> Company = (from Comp in objCompany.EntityList
                                                                                                              where Comp.HomebaseID == HomeBaseID
                                                                                                              select Comp).ToList();
                        if (Company != null && Company.Count > 0)
                            Companymaster = Company[0];

                        else
                        {
                            Companymaster = null;
                        }
                    }
                    return Companymaster;
                }
            }

        }

        private FlightPak.Web.FlightPakMasterService.GetAllDepartments GetAllDepartment(Int64 DepartmentID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    GetAllDepartments Departmentmaster = new GetAllDepartments();
                    var objCompany = objDstsvc.GetAllDepartmentList();

                    if (objCompany.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllDepartments> Company = (from Comp in objCompany.EntityList
                                                                                                where Comp.DepartmentID == DepartmentID
                                                                                                select Comp).ToList();
                        if (Company != null && Company.Count > 0)
                            Departmentmaster = Company[0];

                        else
                        {
                            Departmentmaster = null;
                        }
                    }
                    return Departmentmaster;
                }
            }

        }

        private FlightPak.Web.FlightPakMasterService.GetAllAirport GetAirportbyICaoID(string ICaoID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ICaoID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    GetAllAirport Airportmaster = new GetAllAirport();
                    var objAirport = objDstsvc.GetAirportByAirportICaoID(ICaoID);
                    if (objAirport.ReturnFlag)
                    {
                        //List<FlightPak.Web.FlightPakMasterService.GetAllAirport> DepAirport = (from Arpt in objAirport.EntityList
                        //                                                                       where Arpt.IcaoID == DepartIcaoID
                        //                                                                       select Arpt).ToList();

                        List<FlightPak.Web.FlightPakMasterService.GetAllAirport> DepAirport = objAirport.EntityList;

                        if (DepAirport != null && DepAirport.Count > 0)
                            Airportmaster = DepAirport[0];
                        else
                            Airportmaster = null;
                    }
                    return Airportmaster;
                }
            }

        }

        private FlightPak.Web.FlightPakMasterService.GetAllAirport GetAirport(Int64 DepartIcaoID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartIcaoID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    GetAllAirport Airportmaster = new GetAllAirport();
                    var objAirport = objDstsvc.GetAirportByAirportID(DepartIcaoID);
                    if (objAirport.ReturnFlag)
                    {
                        //List<FlightPak.Web.FlightPakMasterService.GetAllAirport> DepAirport = (from Arpt in objAirport.EntityList
                        //                                                                       where Arpt.IcaoID == DepartIcaoID
                        //                                                                       select Arpt).ToList();

                        List<FlightPak.Web.FlightPakMasterService.GetAllAirport> DepAirport = objAirport.EntityList;

                        if (DepAirport != null && DepAirport.Count > 0)
                            Airportmaster = DepAirport[0];
                        else
                            Airportmaster = null;
                    }
                    return Airportmaster;

                }
            }
        }

        private FlightPak.Web.FlightPakMasterService.Aircraft GetAircraft(Int64 AircraftID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AircraftID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.Aircraft retAircraft = new FlightPakMasterService.Aircraft();
                    var objPowerSetting = objDstsvc.GetAircraftByAircraftID(AircraftID);

                    if (objPowerSetting.ReturnFlag)
                    {
                        //List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = (from aircraft in objPowerSetting.EntityList
                        //                                                                    where aircraft.AircraftID == AircraftID
                        //                                                                    select aircraft).ToList();

                        List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = objPowerSetting.EntityList;


                        if (AircraftList != null && AircraftList.Count > 0)
                            retAircraft = AircraftList[0];
                        else
                            retAircraft = null;
                    }
                    return retAircraft;

                }
            }
        }


        private void CopyCRMainDetails(CRMain trip)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(trip))
            {

                if (CorpRequest.PassengerRequestorID != 0 && CorpRequest.Passenger != null)
                {
                    hdnPaxId.Value = CorpRequest.PassengerRequestorID.ToString();
                    tbPassenger.Text = CorpRequest.Passenger.PassengerRequestorCD;
                    lbPassengerName.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.Passenger.FirstName);
                }


                if (CorpRequest.DepartmentID != 0 && CorpRequest.Department != null)
                {
                    hdDepartment.Value = CorpRequest.DepartmentID.ToString();
                    tbDepartment.Text = CorpRequest.Department.DepartmentCD;
                    lbDepartmentName.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.Department.DepartmentName);
                }

                if (CorpRequest.AuthorizationID != 0 && CorpRequest.DepartmentAuthorization != null)
                {
                    hdnAuthorizationId.Value = CorpRequest.AuthorizationID.ToString();
                    tbAuthorizationCode.Text = CorpRequest.DepartmentAuthorization.AuthorizationCD;
                    lbAuthName.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.DepartmentAuthorization.DeptAuthDescription);
                }

                if (CorpRequest.ClientID != 0 && CorpRequest.Client != null)
                {
                    hdClient.Value = CorpRequest.ClientID.ToString();
                }

                if (CorpRequest.IsPrivate != null && CorpRequest.IsPrivate == true)
                {
                    chkPrivate.Checked = true;
                }
            }
        }

        private void validateandrebindmaster()
        {
            if (CorpRequest.CRMainID == 0 && CorpRequest.State == CorporateRequestTripEntityState.Added)
            {
                if (CorpRequest.Mode == CorporateRequestTripActionMode.Edit)
                {
                    Master.ValidationRequest(CorporateRequestService.Group.Legs);
                    Master.BindException();
                    Master.Exceptions.Rebind();
                    Master.TrackerLegs.Rebind();
                    Master.LegSummary.Rebind();
                }
            }
        }



        protected void LoadHomebaseSettings(bool InitialPageReq)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(InitialPageReq))
            {
                //ResetHomebaseSettings();


                // if not is postback and if it is a new trip 
                if (Session["CurrentCorporateRequest"] != null)
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                    if (InitialPageReq)
                    {
                        if (CorpRequest.CRMainID == 0)
                        {


                            //if (UserPrincipal.Identity._fpSettings._CorpReqStartTM != null && UserPrincipal.Identity._fpSettings._CorpReqStartTM.Trim() != string.Empty)
                            //{
                            //    string startHrs = UserPrincipal.Identity._fpSettings._CorpReqStartTM;
                            //    rmbDepartsLocal.Text = startHrs;
                            //    rmbArrivalLocalTime.Text = startHrs;
                            //}
                        }
                    }

                }
            }
        }
        #endregion


        #region Preflight Calculations

        private void SetChangedCrewDutyRuleforOtherLegs()
        {
            if (Session["CurrentCorporateRequest"] != null)
            {
                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                if (CorpRequest != null)
                {
                    if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                    {
                        Int64 Legnum = 0;
                        Legnum = (long)CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].LegNUM;
                        List<CRLeg> PrefLegs = new List<CRLeg>();

                        PrefLegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                        //Update previous legs

                        for (int i = (int)Legnum - 1; i >= 0; i--)
                        {
                            CRLeg LegtoUpdate = new CRLeg();
                            LegtoUpdate = (from Leg in PrefLegs
                                           where Leg.LegNUM == i
                                           select Leg).SingleOrDefault();

                            if (LegtoUpdate != null && ((LegtoUpdate.IsDutyEnd != null && !(bool)LegtoUpdate.IsDutyEnd) || LegtoUpdate.IsDutyEnd == null))
                            {
                                if (LegtoUpdate.CRLegID != 0 && LegtoUpdate.State != CorporateRequestTripEntityState.Deleted)
                                    LegtoUpdate.State = CorporateRequestTripEntityState.Modified;

                                PreflightService.CrewDutyRule CrewDtyRule = new PreflightService.CrewDutyRule();
                                //if (!string.IsNullOrEmpty(hdCrewRules.Value) && hdCrewRules.Value != "0")
                                //{
                                //    Int64 CrewDutyRulesID = 0;
                                //    if (Int64.TryParse(hdCrewRules.Value, out CrewDutyRulesID))
                                //    {
                                //        CrewDtyRule.CrewDutyRuleCD = hdCrewRules.Value;
                                //        CrewDtyRule.CrewDutyRulesID = CrewDutyRulesID;
                                //        CrewDtyRule.CrewDutyRulesDescription = " ";
                                //        //LegtoUpdate.CrewDutyRule = CrewDtyRule;
                                //        //LegtoUpdate.CrewDutyRulesID = CrewDutyRulesID;
                                //    }
                                //    else
                                //    {
                                //        //LegtoUpdate.CrewDutyRule = null;
                                //        //LegtoUpdate.CrewDutyRulesID = null;
                                //    }
                                //}
                                LegtoUpdate.FedAviationRegNUM = hdCrewRules.Value;
                            }
                            else
                                break;

                        }

                        //Update Next legs
                        //if (!chkEndDuty.Checked)
                        //{
                        for (int i = (int)Legnum + 1; i <= PrefLegs.Count; i++)
                        {
                            CRLeg LegtoUpdate = new CRLeg();
                            LegtoUpdate = (from Leg in PrefLegs
                                           where Leg.LegNUM == i
                                           select Leg).SingleOrDefault();

                            if (LegtoUpdate != null)
                            {
                                if (LegtoUpdate.CRLegID != 0 && LegtoUpdate.State != CorporateRequestTripEntityState.Deleted)
                                    LegtoUpdate.State = CorporateRequestTripEntityState.Modified;

                                //PreflightService.CrewDutyRule CrewDtyRule = new PreflightService.CrewDutyRule();
                                //if (!string.IsNullOrEmpty(hdCrewRules.Value) && hdCrewRules.Value != "0")
                                //{
                                //    Int64 CrewDutyRulesID = 0;
                                //    if (Int64.TryParse(hdCrewRules.Value, out CrewDutyRulesID))
                                //    {
                                //        CrewDtyRule.CrewDutyRuleCD = tbCrewRules.Text;
                                //        CrewDtyRule.CrewDutyRulesID = CrewDutyRulesID;
                                //        CrewDtyRule.CrewDutyRulesDescription = " ";
                                //        LegtoUpdate.CrewDutyRule = CrewDtyRule;
                                //        LegtoUpdate.CrewDutyRulesID = CrewDutyRulesID;
                                //    }
                                //    else
                                //    {
                                //        LegtoUpdate.CrewDutyRule = null;
                                //        LegtoUpdate.CrewDutyRulesID = null;
                                //    }
                                //}
                                LegtoUpdate.FedAviationRegNUM = hdCrewRules.Value;
                                if (LegtoUpdate.IsDutyEnd != null && (bool)LegtoUpdate.IsDutyEnd)
                                    break;

                            }



                        }
                        //}
                    }
                }

                Session["CurrentCorporateRequest"] = CorpRequest;
            }
        }

        private void ClearAllDatesBasedonDeptOrArrival()
        {
            if (!string.IsNullOrEmpty(hdDepart.Value))
            {
                if (!string.IsNullOrEmpty(tbDepartsLocal.Text))
                {
                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                    {
                        DateTime dt = new DateTime();
                        DateTime ldGmtDep = new DateTime();
                        string StartTime = rmbDepartsLocal.Text.Trim();
                        int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                        int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                        dt = DateTime.ParseExact(tbDepartsLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                        dt = dt.AddHours(StartHrs);
                        dt = dt.AddMinutes(StartMts);
                        //GMTDept
                        ldGmtDep = objDstsvc.GetGMT(Convert.ToInt64(hdDepart.Value), dt, true, false);

                        string startHrs = "0000" + ldGmtDep.Hour.ToString();
                        string startMins = "0000" + ldGmtDep.Minute.ToString();
                        tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldGmtDep);
                        rmtUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                        tbHomeDate.Text = string.Empty;
                        rmtHomeTime.Text = "00:00";

                        tbArrivalHomeDate.Text = string.Empty;
                        rmtArrivalHomeTime.Text = "00:00";
                        tbArrivalLocal.Text = string.Empty;
                        rmbArrivalLocalTime.Text = "00:00";
                        tbArrivalUtcDate.Text = string.Empty;
                        rmtArrivalUtctime.Text = "00:00";

                    }
                }
            }
            if (!string.IsNullOrEmpty(hdArrival.Value))
            {
                if (!string.IsNullOrEmpty(tbArrivalLocal.Text))
                {
                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                    {
                        DateTime dt = new DateTime();
                        DateTime ldGmtArr = new DateTime();
                        string StartTime = rmbArrivalLocalTime.Text.Trim();
                        int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                        int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                        dt = DateTime.ParseExact(tbArrivalLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                        dt = dt.AddHours(StartHrs);
                        dt = dt.AddMinutes(StartMts);
                        //GMTDept
                        ldGmtArr = objDstsvc.GetGMT(Convert.ToInt64(hdArrival.Value), dt, true, false);

                        string startHrs = "0000" + ldGmtArr.Hour.ToString();
                        string startMins = "0000" + ldGmtArr.Minute.ToString();
                        tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldGmtArr);
                        rmtArrivalUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                        tbArrivalHomeDate.Text = string.Empty;
                        rmtArrivalHomeTime.Text = "00:00";

                        tbHomeDate.Text = string.Empty;
                        rmtHomeTime.Text = "00:00";

                        tbDepartsLocal.Text = string.Empty;
                        rmbDepartsLocal.Text = "00:00";
                        tbUtcDate.Text = string.Empty;
                        rmtUtctime.Text = "00:00";
                    }
                }
            }

        }

        private int GetQtr(DateTime Dateval)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                int lnqtr = 0;

                if (Dateval.Month == 12 || Dateval.Month == 1 || Dateval.Month == 2) //Must retrieve from TripLeg table in DB
                {
                    lnqtr = 1;
                }
                else if (Dateval.Month == 3 || Dateval.Month == 4 || Dateval.Month == 5)
                {
                    lnqtr = 2;
                }
                else if (Dateval.Month == 6 || Dateval.Month == 7 || Dateval.Month == 8)
                {
                    lnqtr = 3;
                }
                else if (Dateval.Month == 9 || Dateval.Month == 10 || Dateval.Month == 11)
                {
                    lnqtr = 4;
                }
                else
                    lnqtr = 0;


                return lnqtr;

            }

        }


        private void CalculateDateTime(bool isDepartureConfirmed, DateTime? ChangedDate)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isDepartureConfirmed))
            {
                double x10, x11;
                int DeptUTCHrst, DeptUTCMtstr, Hrst, Mtstr, ArrivalUTChrs, ArrivalUTCmts;
                DateTime Arrivaldt;


                if (ChangedDate == null)
                {
                    if (!string.IsNullOrEmpty(tbDepartsLocal.Text))
                    {
                        using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                        {
                            DateTime dt = new DateTime();
                            //string StartTime = rmbDepartsLocal.Text.Trim();  //Commented in Preflight
                            string StartTime = "0000";
                            int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                            int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                            if (StartHrs > 23)
                            {
                                RadWindowManager1.RadConfirm("Hour entry must be between 0 and 23", "confirmCrewRemoveCallBackFn", 330, 100, null, "Confirmation!");
                            }
                            else if (StartMts > 59)
                            {
                                RadWindowManager1.RadConfirm("Minute entry must be between 0 and 59", "confirmCrewRemoveCallBackFn", 330, 100, null, "Confirmation!");
                            }
                            dt = DateTime.ParseExact(tbDepartsLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                            dt = dt.AddHours(StartHrs);
                            dt = dt.AddMinutes(StartMts);
                            ChangedDate = dt;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(tbArrivalLocal.Text))
                        {
                            using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                            {
                                DateTime dt = new DateTime();
                                string StartTime = rmbArrivalLocalTime.Text.Trim();
                                int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                                dt = DateTime.ParseExact(tbArrivalLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                dt = dt.AddHours(StartHrs);
                                dt = dt.AddMinutes(StartMts);
                                ChangedDate = dt;
                                isDepartureConfirmed = false;
                            }
                        }
                    }
                }


                if (ChangedDate != null)
                {


                    DateTime EstDepartDate = (DateTime)ChangedDate;
                    try
                    {
                        if ((!string.IsNullOrEmpty(hdDepart.Value) && hdDepart.Value != "0")
                            && (!string.IsNullOrEmpty(hdArrival.Value) && hdArrival.Value != "0")
                            )
                        {

                            if (!string.IsNullOrEmpty(tbElapsedTime.Text))
                            {
                                string ETEstr = "0";
                                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                                {

                                    ETEstr = ConvertMinToTenths(tbElapsedTime.Text, true, "1");
                                }
                                else
                                {
                                    if (tbElapsedTime.Text.ToString().Contains(':') == false)
                                        ETEstr = Math.Round(Convert.ToDecimal(tbElapsedTime.Text.ToString().Trim()), 1).ToString();
                                    else
                                        ETEstr = ConvertMinToTenths(tbElapsedTime.Text, true, "1");
                                    //ETEstr = Math.Round(Convert.ToDecimal(tbElapsedTime.Text.ToString().Trim()), 1).ToString(); //tbElapsedTime.Text; karthik Issue Time
                                }


                                double tripleg_elp_time = RoundElpTime(Convert.ToDouble(ETEstr));

                                x10 = (tripleg_elp_time * 60 * 60);
                                x11 = (tripleg_elp_time * 60 * 60);
                            }
                            else
                            {
                                x10 = 0;
                                x11 = 0;
                            }
                            DateTime DeptUTCdt = DateTime.MinValue;
                            DateTime ArrUTCdt = DateTime.MinValue;

                            if (isDepartureConfirmed)
                            {
                                //DeptUTCDate
                                if (!string.IsNullOrEmpty(rmtUtctime.Text))
                                {
                                    string DeptUTCTimestr = rmtUtctime.Text.Trim();
                                    DeptUTCHrst = Convert.ToInt16(DeptUTCTimestr.Substring(0, 2));
                                    DeptUTCMtstr = Convert.ToInt16(DeptUTCTimestr.Substring(2, 2));
                                    if (!string.IsNullOrEmpty(tbUtcDate.Text))
                                    {
                                        DeptUTCdt = DateTime.ParseExact(tbUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                        DeptUTCdt = DeptUTCdt.AddHours(DeptUTCHrst);
                                        DeptUTCdt = DeptUTCdt.AddMinutes(DeptUTCMtstr);
                                    }
                                }
                                if (!string.IsNullOrWhiteSpace(this.tbArrivalIcao.Text))
                                {
                                    string Timestr = rmtUtctime.Text.Trim();
                                    Hrst = Convert.ToInt16(Timestr.Substring(0, 2));
                                    Mtstr = Convert.ToInt16(Timestr.Substring(2, 2));

                                    Arrivaldt = DateTime.ParseExact(tbUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    Arrivaldt = Arrivaldt.AddHours(Hrst);
                                    Arrivaldt = Arrivaldt.AddMinutes(Mtstr);
                                    Arrivaldt = Arrivaldt.AddSeconds(x10);
                                    ArrUTCdt = Arrivaldt;

                                    tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", Arrivaldt);
                                    string startHrs = "0000" + Arrivaldt.Hour.ToString();
                                    string startMins = "0000" + Arrivaldt.Minute.ToString();
                                    rmtArrivalUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                }
                                else
                                {
                                    tbArrivalUtcDate.Text = string.Empty;
                                    rmtArrivalUtctime.Text = string.Empty;
                                }
                            }

                            else
                            {
                                //Arrival utc date
                                string ArrUTCTimestr = rmtArrivalUtctime.Text.Trim();
                                int ArrUTCHrst = Convert.ToInt16(ArrUTCTimestr.Substring(0, 2));
                                int ArrUTCMtstr = Convert.ToInt16(ArrUTCTimestr.Substring(2, 2));
                                ArrUTCdt = DateTime.ParseExact(tbArrivalUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                ArrUTCdt = ArrUTCdt.AddHours(ArrUTCHrst);
                                ArrUTCdt = ArrUTCdt.AddMinutes(ArrUTCMtstr);

                                if (!string.IsNullOrWhiteSpace(this.tbDepartsIcao.Text))
                                {
                                    // tbUtcDate.Text = Convert.ToString(Convert.ToDateTime(this.tbArrivalUtcDate.Text).AddSeconds(-(x11)));

                                    string Timestr = rmtArrivalUtctime.Text.Trim();
                                    ArrivalUTChrs = Convert.ToInt16(Timestr.Substring(0, 2));
                                    ArrivalUTCmts = Convert.ToInt16(Timestr.Substring(2, 2));
                                    DateTime dt = DateTime.ParseExact(tbArrivalUtcDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    dt = dt.AddHours(ArrivalUTChrs);
                                    dt = dt.AddMinutes(ArrivalUTCmts);
                                    dt = dt.AddSeconds(-(x11));
                                    DeptUTCdt = dt;
                                    tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", dt);
                                    string startHrs = "0000" + dt.Hour.ToString();
                                    string startMins = "0000" + dt.Minute.ToString();
                                    rmtUtctime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
                                }
                                else
                                {
                                    tbUtcDate.Text = string.Empty; //DepartsUTC
                                    rmtUtctime.Text = "";
                                }
                            }




                            using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                            {
                                DateTime ldLocDep = DateTime.MinValue;
                                DateTime ldLocArr = DateTime.MinValue;
                                DateTime ldHomDep = DateTime.MinValue;
                                DateTime ldHomArr = DateTime.MinValue;
                                if (!string.IsNullOrEmpty(hdDepart.Value))
                                {
                                    ldLocDep = objDstsvc.GetGMT(Convert.ToInt64(hdDepart.Value), DeptUTCdt, false, false);
                                }

                                if (!string.IsNullOrEmpty(hdHomebaseAirport.Value))
                                {
                                    ldHomDep = objDstsvc.GetGMT(Convert.ToInt64(hdHomebaseAirport.Value), DeptUTCdt, false, false);
                                }

                                if (!string.IsNullOrEmpty(hdArrival.Value))
                                {
                                    ldLocArr = objDstsvc.GetGMT(Convert.ToInt64(hdArrival.Value), ArrUTCdt, false, false);
                                }

                                if (!string.IsNullOrEmpty(hdHomebaseAirport.Value))
                                {
                                    ldHomArr = objDstsvc.GetGMT(Convert.ToInt64(hdHomebaseAirport.Value), ArrUTCdt, false, false);
                                }

                                //DateTime ltBlank = EstDepartDate;

                                if (!string.IsNullOrEmpty(this.tbDepartsIcao.Text))
                                {
                                    // DateTime Localdt = ldLocDep;
                                    string startHrs = "0000" + ldLocDep.Hour.ToString();
                                    string startMins = "0000" + ldLocDep.Minute.ToString();
                                    tbDepartsLocal.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldLocDep);
                                    rmbDepartsLocal.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);



                                    if (!string.IsNullOrEmpty(hdHomebaseAirport.Value))
                                    {
                                        // DateTime Homedt = ldHomDep;
                                        string HomedtstartHrs = "0000" + ldHomDep.Hour.ToString();
                                        string HomedtstartMins = "0000" + ldHomDep.Minute.ToString();
                                        tbHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldHomDep);
                                        rmtHomeTime.Text = HomedtstartHrs.Substring(HomedtstartHrs.Length - 2, 2) + ":" + HomedtstartMins.Substring(HomedtstartMins.Length - 2, 2);
                                    }
                                }
                                else
                                {
                                    DateTime Localdt = EstDepartDate;
                                    string startHrs = "0000" + Localdt.Hour.ToString();
                                    string startMins = "0000" + Localdt.Minute.ToString();

                                    tbDepartsLocal.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", EstDepartDate);
                                    rmbDepartsLocal.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);


                                    tbUtcDate.Text = tbDepartsLocal.Text;
                                    rmtUtctime.Text = rmbDepartsLocal.Text;

                                    if (!string.IsNullOrEmpty(hdHomebaseAirport.Value))
                                    {
                                        tbHomeDate.Text = tbDepartsLocal.Text;
                                        rmtHomeTime.Text = rmbDepartsLocal.Text;
                                    }
                                }

                                if (!string.IsNullOrEmpty(this.tbArrivalIcao.Text))
                                {
                                    // DateTime dt = ldLocArr;
                                    string startHrs = "0000" + ldLocArr.Hour.ToString();
                                    string startMins = "0000" + ldLocArr.Minute.ToString();
                                    tbArrivalLocal.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldLocArr);
                                    rmbArrivalLocalTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);


                                    if (!string.IsNullOrEmpty(hdHomebaseAirport.Value))
                                    {
                                        // DateTime HomeArrivaldt = ldHomArr;
                                        string HomeArrivalstartHrs = "0000" + ldHomArr.Hour.ToString();
                                        string HomeArrivalstartMins = "0000" + ldHomArr.Minute.ToString();
                                        tbArrivalHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", ldHomArr);
                                        rmtArrivalHomeTime.Text = HomeArrivalstartHrs.Substring(HomeArrivalstartHrs.Length - 2, 2) + ":" + HomeArrivalstartMins.Substring(HomeArrivalstartMins.Length - 2, 2);
                                    }
                                }
                                else
                                {
                                    DateTime Localdt = EstDepartDate;
                                    string startHrs = "0000" + Localdt.Hour.ToString();
                                    string startMins = "0000" + Localdt.Minute.ToString();

                                    tbArrivalLocal.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", EstDepartDate);
                                    rmbArrivalLocalTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                    tbArrivalUtcDate.Text = tbArrivalLocal.Text;
                                    rmtArrivalUtctime.Text = rmbArrivalLocalTime.Text;

                                    if (!string.IsNullOrEmpty(hdHomebaseAirport.Value))
                                    {
                                        tbArrivalHomeDate.Text = tbArrivalLocal.Text;
                                        rmtArrivalHomeTime.Text = rmbDepartsLocal.Text;
                                    }

                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        //Manually handled
                        if (!string.IsNullOrEmpty(this.tbArrivalLocal.Text))
                        {
                            tbArrivalLocal.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", EstDepartDate);
                            rmbArrivalLocalTime.Text = "00:00";
                        }
                        if (!string.IsNullOrEmpty(hdHomebaseAirport.Value))
                        {

                            if (!string.IsNullOrEmpty(tbArrivalHomeDate.Text))
                            {
                                tbArrivalHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", EstDepartDate);
                                rmtArrivalHomeTime.Text = "00:00";
                            }
                        }

                        if (!string.IsNullOrEmpty(this.tbArrivalUtcDate.Text))
                        {
                            tbArrivalUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", EstDepartDate);
                            rmtArrivalUtctime.Text = "00:00";
                        }

                        if (!string.IsNullOrEmpty(this.tbDepartsLocal.Text))
                        {
                            tbDepartsLocal.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", EstDepartDate);
                            rmbDepartsLocal.Text = "00:00";
                        }

                        if (!string.IsNullOrEmpty(this.tbUtcDate.Text))
                        {
                            tbUtcDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", EstDepartDate);
                            rmtUtctime.Text = "00:00";
                        }
                        if (!string.IsNullOrEmpty(hdHomebaseAirport.Value))
                        {
                            if (!string.IsNullOrEmpty(this.tbHomeDate.Text))
                            {
                                tbHomeDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", EstDepartDate);
                                rmtHomeTime.Text = "00:00";
                            }
                        }
                    }

                }

            }

        }

        private void CalculateMiles()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //try
                //{
                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                {

                    //CalculationManager CM = new CalculationManager();
                    //Function Miles Calculation
                    double Miles = 0;
                    hdnMiles.Value = "0";
                    tbDistance.Text = "0";
                    if ((!string.IsNullOrEmpty(hdDepart.Value) && hdDepart.Value != "0") && (!string.IsNullOrEmpty(hdArrival.Value) && hdArrival.Value != "0"))
                    {
                        Miles = objDstsvc.GetDistance(Convert.ToInt64(hdDepart.Value), Convert.ToInt64(hdArrival.Value));
                        tbDistance.Text = ConvertToKilomenterBasedOnCompanyProfile((decimal)Miles).ToString();
                        hdnMiles.Value = Miles.ToString();
                    }
                }
                //}
                //catch (Exception) 
                //{
                //    tbMiles.Text = "0.0";
                //}

            }


        }

        private void CalculateDomORInt()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                {
                    string dutyType = "";
                    if (!string.IsNullOrEmpty(hdDepart.Value) && !string.IsNullOrEmpty(hdArrival.Value) && !string.IsNullOrEmpty(hdHomebaseAirport.Value))
                    {

                        dutyType = objDstsvc.CalcDomIntl(Convert.ToInt64(hdDepart.Value), Convert.ToInt64(hdArrival.Value), Convert.ToInt64(hdHomebaseAirport.Value));
                        hdnDomestic.Value = dutyType;
                    }
                }

            }

        }

        private void CalculateETE()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                {
                    double ETE = 0;

                    double Wind = 0;
                    double LandBias = 0;
                    double TAS = 0;
                    double Bias = 0;
                    double Miles = 0;

                    if (!string.IsNullOrEmpty(tbWind.Value))
                    {
                        Wind = Convert.ToDouble(tbWind.Value);
                    }

                    double LandingBias = 0;
                    string LandingBiasStr = string.Empty;

                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        LandingBiasStr = ConvertMinToTenths(tbLandBias.Value, true, "1");
                    else
                        LandingBiasStr = tbLandBias.Value;

                    if (double.TryParse(LandingBiasStr, out LandingBias))
                        LandBias = LandingBias;
                    else
                        LandBias = 0;

                    double tobias = 0;
                    string TakeOffBiasStr = string.Empty;

                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                    {
                        TakeOffBiasStr = ConvertMinToTenths(tbBias.Value, true, "1");
                    }
                    else
                        TakeOffBiasStr = tbBias.Value;
                    if (double.TryParse(TakeOffBiasStr, out tobias))
                        Bias = tobias;
                    else
                        Bias = 0;


                    if (!string.IsNullOrEmpty(tbTAS.Value))
                    {
                        TAS = Convert.ToDouble(tbTAS.Value);
                    }



                    if (!string.IsNullOrEmpty(hdnMiles.Value))
                    {
                        Miles = Convert.ToDouble(hdnMiles.Value);
                    }

                    DateTime dt = DateTime.Now;

                    if (!string.IsNullOrEmpty(tbDepartsLocal.Text))
                    {
                        string StartTime = rmbDepartsLocal.Text.Trim();
                        int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                        int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                        dt = DateTime.ParseExact(tbDepartsLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                        dt = dt.AddHours(StartHrs);
                        dt = dt.AddMinutes(StartMts);
                    }
                    else
                    {

                        if (!string.IsNullOrEmpty(tbArrivalLocal.Text))
                        {
                            string StartTime = rmbArrivalLocalTime.Text.Trim();
                            int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                            int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                            dt = DateTime.ParseExact(tbArrivalLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                            dt = dt.AddHours(StartHrs);
                            dt = dt.AddMinutes(StartMts);
                        }
                    }

                    ETE = objDstsvc.GetIcaoEte(Wind, LandBias, TAS, Bias, Miles, dt);

                    ETE = RoundElpTime(ETE);

                    if (ETE == 0)
                    {
                        Int64 AircraftID = Convert.ToInt64(hdAircraft.Value == string.Empty ? "0" : hdAircraft.Value);
                        FlightPakMasterService.Aircraft Aircraft = GetAircraft(AircraftID);

                        if (Aircraft != null)
                            if (Aircraft.IsFixedRotary != null && Aircraft.IsFixedRotary.ToUpper() == "R")
                                ETE = 0.1;
                    }

                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                    {
                        tbElapsedTime.Text = "00:00";
                        tbElapsedTime.Text = ConvertTenthsToMins(ETE.ToString());
                    }
                    else
                    {
                        tbElapsedTime.Text = "0.0";
                        tbElapsedTime.Text = Math.Round(ETE, 1).ToString();
                        if (tbElapsedTime.Text.IndexOf(".") < 0)
                            tbElapsedTime.Text = tbElapsedTime.Text + ".0";
                    }
                }

            }
        }

        private void CalculateWind()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                if (!string.IsNullOrEmpty(tbDepartsLocal.Text))
                {
                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                    {

                        string StartTime = "0000";
                        int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                        int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));

                        DateTime dt = DateTime.ParseExact(tbDepartsLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                        dt = dt.AddHours(StartHrs);
                        dt = dt.AddMinutes(StartMts);
                        //Function Wind Calculation
                        double Wind = 0;
                        if (
                            (!string.IsNullOrEmpty(hdDepart.Value) && hdDepart.Value != "0") &&
                            (!string.IsNullOrEmpty(hdArrival.Value) && hdArrival.Value != "0") &&
                            (!string.IsNullOrEmpty(hdAircraft.Value) && hdAircraft.Value != "0")
                            )
                        {
                            Wind = objDstsvc.GetWind(Convert.ToInt64(hdDepart.Value), Convert.ToInt64(hdArrival.Value), Convert.ToInt32(hdnWindReliability.Value), Convert.ToInt64(hdAircraft.Value), GetQtr(dt).ToString());
                            tbWind.Value = Wind.ToString();
                        }
                        else
                            tbWind.Value = "0.0";
                    }
                }
                else
                    tbWind.Value = "0.0";
            }
        }

        private void CalcualteBias()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                {
                    List<double> BiasList = new List<double>();
                    if (hdnPower.Value == string.Empty)
                        hdnPower.Value = "1";

                    if (
                        (!string.IsNullOrEmpty(hdDepart.Value) && hdDepart.Value != "0")
                        &&
                        (!string.IsNullOrEmpty(hdArrival.Value) && hdArrival.Value != "0")
                        &&
                        (!string.IsNullOrEmpty(hdAircraft.Value) && hdAircraft.Value != "0")
                        )
                    {
                        BiasList = objDstsvc.CalcBiasTas(Convert.ToInt64(hdDepart.Value), Convert.ToInt64(hdArrival.Value), Convert.ToInt64(hdAircraft.Value), hdnPower.Value);

                        if (BiasList != null)
                        {
                            // lnToBias, lnLndBias, lnTas
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                tbBias.Value = "00:00";
                                tbBias.Value = ConvertTenthsToMins(BiasList[0].ToString());
                            }
                            else
                            {
                                tbBias.Value = "0.0";
                                tbBias.Value = Math.Round((decimal)BiasList[0], 1).ToString();

                            }
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                tbLandBias.Value = "00:00";
                                tbLandBias.Value = ConvertTenthsToMins(BiasList[1].ToString());
                            }
                            else
                            {
                                tbLandBias.Value = "0.0";
                                tbLandBias.Value = Math.Round((decimal)BiasList[1], 1).ToString();
                            }
                            tbTAS.Value = BiasList[2].ToString();
                        }
                    }
                    else
                    {
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            tbBias.Value = "00:00";
                            tbLandBias.Value = "00:00";
                        }
                        else
                        {
                            tbBias.Value = "0.0";
                            tbLandBias.Value = "0.0";
                        }
                        tbTAS.Value = "0.0";
                    }

                }
                // To check whether the Aircraft has 0 due to 'convert to decimal' method & return to it's 0.0 default format
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                {
                    if (!string.IsNullOrEmpty(tbBias.Value))
                    {
                        if (tbBias.Value.IndexOf(".") < 0)
                        {
                            tbBias.Value = tbBias.Value + ".0";
                        }

                    }

                    if (!string.IsNullOrEmpty(tbLandBias.Value))
                    {
                        if (tbLandBias.Value.IndexOf(".") < 0)
                        {
                            tbLandBias.Value = tbLandBias.Value + ".0";
                        }
                    }
                }
            }
        }

        private void CalculateCRFARRules()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //try
                //{
                double lnLeg_Num, lnEndLegNum, lnOrigLeg_Num, lnDuty_Hrs, lnRest_Hrs, lnFlt_Hrs, lnMaxDuty, lnMaxFlt, lnMinRest, lnRestMulti, lnRestPlus, lnOldDuty_Hrs, lnDayBeg, lnDayEnd,
                    lnNeededRest, lnWorkArea;

                lnEndLegNum = lnNeededRest = lnWorkArea = lnLeg_Num = lnOrigLeg_Num = lnDuty_Hrs = lnRest_Hrs = lnFlt_Hrs = lnMaxDuty = lnMaxFlt = lnMinRest = lnRestMulti = lnRestPlus = lnOldDuty_Hrs = lnDayBeg = lnDayEnd = 0.0;

                bool llRProblem, llFProblem, llDutyBegin, llDProblem;
                llRProblem = llFProblem = llDutyBegin = llDProblem = false;

                DateTime ltGmtDep, ltGmtArr, llDutyend;
                Int64 lnDuty_RulesID = 0;
                string lcDuty_Rules = string.Empty;

                if ((!string.IsNullOrEmpty(hdDepart.Value) && hdDepart.Value != "0") && (!string.IsNullOrEmpty(hdArrival.Value) && hdArrival.Value != "0"))
                {
                    SaveCRToSession();
                    BindCRTabs(CorpRequest, "FAR Rules");
                }
                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];


                if (CorpRequest.CRLegs != null)
                {
                    List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                    lnOrigLeg_Num = (double)Preflegs[0].LegNUM;
                    lnEndLegNum = (double)Preflegs[Preflegs.Count - 1].LegNUM;

                    lnDuty_Hrs = 0;
                    lnRest_Hrs = 0;
                    lnFlt_Hrs = 0;

                    if (Preflegs[0].DepartureGreenwichDTTM != null)
                    {
                        ltGmtDep = (DateTime)Preflegs[0].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[0].DepartureGreenwichDTTM;
                        ltGmtArr = (DateTime)Preflegs[0].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[0].DepartureGreenwichDTTM;

                        llDutyBegin = true;
                        double lnOldDuty_hrs = -1;

                        int legcounter = 0;
                        foreach (CRLeg Leg in Preflegs)
                        {
                            if (Leg.ArrivalGreenwichDTTM != null && Leg.DepartureGreenwichDTTM != null)
                            {

                                double dbElapseTime = 0.0;
                                long dbCrewDutyRulesID = 0;

                                if (Leg.ElapseTM != null)
                                    dbElapseTime = (double)Leg.ElapseTM;


                                double lnOverRide = 0;
                                double lnDutyLeg_Num = 0;

                                bool llDutyEnd = false;
                                string FARNum = "";

                                //if (CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].CrewDutyRules != null)            // Sincr No CrewDutyrule in CRLEG
                                //    lnDuty_RulesID = (Int64)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].CrewDutyRulesID;
                                //else
                                lnDuty_RulesID = 0;
                                if (CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].IsDutyEnd == null)
                                    llDutyEnd = false;
                                else
                                    llDutyEnd = (bool)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].IsDutyEnd;

                                //if it is last leg then  llDutyEnd = true;
                                if (lnEndLegNum == CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].LegNUM)
                                    llDutyEnd = true;

                                lnOverRide = (double)(CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].CROverRide == null ? 0 : CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].CROverRide);



                                lnDutyLeg_Num = (double)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].LegNUM;



                                using (MasterCatalogServiceClient objectDstsvc = new MasterCatalogServiceClient())
                                {
                                    var objRetVal = objectDstsvc.GetCrewDutyRuleList();
                                    if (objRetVal.ReturnFlag)
                                    {
                                        List<FlightPakMasterService.CrewDutyRules> CrewDtyRule = (from CrewDutyRl in objRetVal.EntityList
                                                                                                  where CrewDutyRl.CrewDutyRulesID == lnDuty_RulesID
                                                                                                  select CrewDutyRl).ToList();

                                        if (CrewDtyRule != null && CrewDtyRule.Count > 0)
                                        {
                                            FARNum = CrewDtyRule[0].FedAviatRegNum;
                                            lnDayBeg = lnOverRide == 0 ? Convert.ToDouble(CrewDtyRule[0].DutyDayBeingTM == null ? 0 : CrewDtyRule[0].DutyDayBeingTM) : lnOverRide;
                                            lnDayEnd = Convert.ToDouble(CrewDtyRule[0].DutyDayEndTM == null ? 0 : CrewDtyRule[0].DutyDayEndTM);
                                            lnMaxDuty = Convert.ToDouble(CrewDtyRule[0].MaximumDutyHrs == null ? 0 : CrewDtyRule[0].MaximumDutyHrs);
                                            lnMaxFlt = Convert.ToDouble(CrewDtyRule[0].MaximumFlightHrs == null ? 0 : CrewDtyRule[0].MaximumFlightHrs);
                                            lnMinRest = Convert.ToDouble(CrewDtyRule[0].MinimumRestHrs == null ? 0 : CrewDtyRule[0].MinimumRestHrs);
                                            lnRestMulti = Convert.ToDouble(CrewDtyRule[0].RestMultiple == null ? 0 : CrewDtyRule[0].RestMultiple);
                                            lnRestPlus = Convert.ToDouble(CrewDtyRule[0].RestMultipleHrs == null ? 0 : CrewDtyRule[0].RestMultipleHrs);
                                        }
                                        else
                                        {
                                            lnDayBeg = 0;
                                            lnDayEnd = 0;
                                            lnMaxDuty = 0;
                                            lnMaxFlt = 0;
                                            lnMinRest = 8;
                                            lnRestMulti = 0;
                                            lnRestPlus = 0;
                                        }
                                    }
                                }
                                TimeSpan? Hoursdiff = (DateTime)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].DepartureGreenwichDTTM - ltGmtArr;
                                if (Leg.ElapseTM != null && Leg.ElapseTM != 0)
                                {
                                    lnFlt_Hrs = lnFlt_Hrs + (double)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].ElapseTM;
                                    lnDuty_Hrs = lnDuty_Hrs + (double)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].ElapseTM + Hoursdiff.Value.TotalHours;  //(ROUND(ROUND((tripleg.gmtdep - ltGmtArr),0)/3600,1))
                                }
                                else
                                {

                                    lnDuty_Hrs = lnDuty_Hrs + Hoursdiff.Value.TotalHours;  //(ROUND(ROUND((tripleg.gmtdep - ltGmtArr),0)/3600,1))
                                }

                                llRProblem = false;

                                if (llDutyBegin && lnOldDuty_hrs != -1)
                                {

                                    lnDuty_Hrs = lnDuty_Hrs + lnDayBeg;

                                    if (lnRestMulti > 0)
                                    {

                                        //Here is the Multiple and Plus hours usage
                                        lnNeededRest = ((lnOldDuty_hrs * lnRestMulti) + lnRestPlus > lnMinRest + lnRestPlus) ? ((lnOldDuty_hrs * lnRestMulti) + lnRestPlus) : lnMinRest + lnRestPlus;
                                        if (lnRest_Hrs < lnNeededRest && lnRest_Hrs > 0)
                                        {

                                            llRProblem = true;
                                        }
                                    }
                                    else
                                    {
                                        if (lnRest_Hrs < (lnMinRest + lnRestPlus) && lnRest_Hrs > 0)
                                        {

                                            llRProblem = true;
                                        }

                                    }
                                }
                                else
                                {
                                    if (llDutyBegin)
                                    {
                                        lnDuty_Hrs = lnDuty_Hrs + lnDayBeg;
                                    }
                                }

                                if (llDutyEnd)
                                {
                                    lnDuty_Hrs = lnDuty_Hrs + lnDayEnd;
                                }

                                if (lnRest_Hrs < 0)
                                    llRProblem = true;

                                string lcCdAlert = "";

                                if (lnFlt_Hrs > lnMaxFlt) //if flight hours is greater than tripleg.maxflt
                                    lcCdAlert = "F";// Flight time violation and F is stored in tripleg.cdalert as a Flight time error
                                else
                                    lcCdAlert = "";



                                if (lnDuty_Hrs > lnMaxDuty) //If Duty Hours is greater than Maximum Duty
                                    lcCdAlert = lcCdAlert + "D"; //Duty type violation and D is stored in tripleg.cdalert as a Duty time error



                                if (llRProblem)
                                    lcCdAlert = lcCdAlert + "R";// Rest time violation and R is stored in tripleg.cdalert as a Rest time error 
                                if (CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].CRLegID != 0 && CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].State != CorporateRequestTripEntityState.Deleted)
                                    CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].State = CorporateRequestTripEntityState.Modified;
                                else
                                    CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].State = CorporateRequestTripEntityState.Added;
                                CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].FlightHours = (decimal)lnFlt_Hrs;
                                CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].DutyHours = (decimal)lnDuty_Hrs;
                                CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].RestHours = (decimal)lnRest_Hrs;


                                if (hdnLeg.Value == (CorpRequest.CRLegs.IndexOf(Leg) + 1).ToString())
                                {

                                    lbFar.Value = System.Web.HttpUtility.HtmlEncode(FARNum);



                                    if (lnDuty_Hrs != 0)
                                    {
                                        lbTotalDuty.Value = System.Web.HttpUtility.HtmlEncode(Math.Round(lnDuty_Hrs, 1).ToString());
                                        if (lbTotalDuty.Value.IndexOf(".") < 0)
                                        {
                                            lbTotalDuty.Value = System.Web.HttpUtility.HtmlEncode(lbTotalDuty.Value) + ".0";
                                        }
                                    }
                                    else
                                        lbTotalDuty.Value = System.Web.HttpUtility.HtmlEncode("0.0");
                                    if (lnFlt_Hrs != 0)
                                    {
                                        lbTotalFlight.Value = System.Web.HttpUtility.HtmlEncode(Math.Round(lnFlt_Hrs, 1).ToString());
                                        if (lbTotalFlight.Value.IndexOf(".") < 0)
                                        {
                                            lbTotalFlight.Value = System.Web.HttpUtility.HtmlEncode(lbTotalFlight.Value) + ".0";
                                        }
                                    }
                                    else
                                        lbTotalFlight.Value = System.Web.HttpUtility.HtmlEncode("0.0");

                                    if (lnRest_Hrs != 0)
                                    {
                                        lbRest.Value = Math.Round(lnRest_Hrs, 1).ToString();
                                        if (lbRest.Value.IndexOf(".") < 0)
                                        {
                                            lbRest.Value = System.Web.HttpUtility.HtmlEncode(lbRest.Value) + ".0";
                                        }
                                    }
                                    else
                                        lbRest.Value = System.Web.HttpUtility.HtmlEncode("0.0");
                                }

                                lnLeg_Num = (double)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].LegNUM;

                                if (CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].IsDutyEnd != null)
                                    llDutyEnd = (bool)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].IsDutyEnd;
                                else
                                    CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].IsDutyEnd = false;

                                ltGmtArr = (DateTime)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].ArrivalGreenwichDTTM;

                                if ((bool)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].IsDutyEnd)
                                {
                                    llDutyBegin = true;
                                    lnFlt_Hrs = 0;
                                }
                                else
                                {
                                    llDutyBegin = false;
                                }

                                lnOldDuty_hrs = lnDuty_Hrs;

                                legcounter++;
                                if (legcounter < Preflegs.Count)
                                {
                                    if (llDutyEnd)
                                    {
                                        lnDuty_Hrs = 0;

                                        if (Preflegs[legcounter].DepartureGreenwichDTTM != null)
                                        {
                                            TimeSpan? hrsdiff = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM - ltGmtArr;

                                            lnRest_Hrs = ((hrsdiff.Value.Days * 24) + hrsdiff.Value.Hours + (double)((double)hrsdiff.Value.Minutes / 60)) - lnDayBeg - lnDayEnd;

                                            ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;
                                        }
                                    }
                                    else
                                    {
                                        lnRest_Hrs = 0;
                                    }
                                }
                            }
                        }
                    }
                }
                Session["CurrentCorporateRequest"] = CorpRequest;
            }


        }

        private void CalculateFlightCost()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                if ((!string.IsNullOrEmpty(hdDepart.Value) && hdDepart.Value != "0") && (!string.IsNullOrEmpty(hdArrival.Value) && hdArrival.Value != "0"))
                {
                    SaveCRToSession();
                    BindCRTabs(CorpRequest, "Flight Cost");
                }
                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                if (CorpRequest.CRLegs != null)
                {
                    List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                    decimal TotalCost = 0.0M;

                    foreach (CRLeg Leg in Preflegs)
                    {

                        decimal lnChrg_Rate = 0.0M;
                        decimal lnCost = 0.0M;
                        string lcChrg_Unit = string.Empty;

                        if (CorpRequest.FleetID != null && CorpRequest.FleetID != 0)
                        {
                            using (MasterCatalogServiceClient MasterClient = new MasterCatalogServiceClient())
                            {
                                var objRetval = MasterClient.GetFleetChargeRateList((long)CorpRequest.FleetID);
                                if (Leg.DepartureDTTMLocal != null)
                                {
                                    if (objRetval.ReturnFlag)
                                    {
                                        List<FlightPakMasterService.FleetChargeRate> FleetchargeRate = (from Fleetchrrate in objRetval.EntityList
                                                                                                        where (Fleetchrrate.BeginRateDT <= Leg.ArrivalDTTMLocal
                                                                                                        && Fleetchrrate.EndRateDT >= Leg.DepartureDTTMLocal)
                                                                                                        where Fleetchrrate.FleetID == CorpRequest.FleetID
                                                                                                        select Fleetchrrate).ToList();
                                        if (FleetchargeRate.Count > 0)
                                        {
                                            lnChrg_Rate = FleetchargeRate[0].ChargeRate == null ? 0.0M : (decimal)FleetchargeRate[0].ChargeRate;
                                            lcChrg_Unit = FleetchargeRate[0].ChargeUnit;
                                        }
                                    }
                                }
                            }
                        }

                        if (lnChrg_Rate == 0.0M)
                        {

                            if (!string.IsNullOrEmpty(hdAircraft.Value) && hdAircraft.Value != "0")
                            {
                                Int64 AircraftID = Convert.ToInt64(hdAircraft.Value);
                                FlightPakMasterService.Aircraft Aircraft = GetAircraft(AircraftID);
                                if (Aircraft != null)
                                {
                                    lnChrg_Rate = Aircraft.ChargeRate == null ? 0.0M : (decimal)Aircraft.ChargeRate;
                                    lcChrg_Unit = Aircraft.ChargeUnit;
                                }
                            }

                        }

                        switch (lcChrg_Unit)
                        {
                            case "N":
                                {
                                    lnCost = (Leg.Distance == null ? 0 : (decimal)Leg.Distance) * lnChrg_Rate;
                                    break;
                                }
                            case "K":
                                {
                                    lnCost = (Leg.Distance == null ? 0 : (decimal)Leg.Distance) * lnChrg_Rate * 1.852M;
                                    break;
                                }
                            case "S":
                                {
                                    lnCost = (Leg.Distance == null ? 0 : (decimal)Leg.Distance) * lnChrg_Rate * 1.1508M;
                                    break;
                                }
                            case "H":
                                {
                                    lnCost = (Leg.ElapseTM == null ? 0 : (decimal)Leg.ElapseTM) * lnChrg_Rate;
                                    break;
                                }
                            default: lnCost = 0.0M; break;

                        }

                        TotalCost = TotalCost + lnCost;
                    }


                    Session["CurrentCorporateRequest"] = CorpRequest;
                }
            }



        }

        #endregion

        #region "Calculations"
        private string Convert12To24Hrs(string DateAndTime)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                return string.Format("{0:" + CompDateFormat + " HH:mm}", Convert.ToDateTime(DateAndTime));
            }
        }

        //added for datetime formatting
        private void SplitDateAndTime(string DateTimeValue, ref string Date, ref string Time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DateTimeValue = Convert12To24Hrs(DateTimeValue);
                Date = DateTimeValue.Substring(0, DateTimeValue.IndexOf(' ')).Trim();
                Time = DateTimeValue.Substring(DateTimeValue.IndexOf(' '), DateTimeValue.Length - DateTimeValue.IndexOf(' ')).Trim();
            }
        }

        //public void SetDepartDate()
        //{
        //    if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat != null && UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 2)
        //    {
        //        string startHrs = "0000" + Convert.ToDateTime(rmbDepartsLocal.TextWithLiterals).Hour.ToString();
        //        string startMins = "0000" + Convert.ToDateTime(rmbDepartsLocal.TextWithLiterals).Minute.ToString();
        //        DepTimeForm.Visible = false;
        //        string Date = string.Empty;
        //        string Time = string.Empty;
        //        SplitDateAndTime(Convert.ToDateTime(rmbDepartsLocal.TextWithLiterals).ToString(), ref Date, ref Time);
        //        if (!string.IsNullOrEmpty(Time))
        //        {
        //            rmbDepartsLocal.Text = Time;
        //        }
        //        else
        //        {
        //            rmbDepartsLocal.Text = "00:00";
        //        }
        //    }
        //    else
        //    {

        //        string startHrs = "0000" + Convert.ToDateTime(rmbDepartsLocal.TextWithLiterals).Hour.ToString();
        //        string startMins = "0000" + Convert.ToDateTime(rmbDepartsLocal.TextWithLiterals).Minute.ToString();
        //        tbDepartsLocal.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", Convert.ToDateTime(rmbDepartsLocal.TextWithLiterals));
        //        rmbDepartsLocal.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);
        //        DepTimeForm.Visible = true;
        //        string AMPM = Convert.ToDateTime(rmbDepartsLocal.TextWithLiterals).ToString("tt");
        //        if (AMPM == "AM" || AMPM == "am")
        //        {
        //            DepTimeForm.SelectedValue = "1";
        //        }
        //        else
        //        {
        //            int Hour = Convert.ToDateTime(rmbDepartsLocal.TextWithLiterals).Hour - 12;
        //            string startHour = "0000" + Hour.ToString();
        //            string startMinutes = "0000" + Convert.ToDateTime(rmbDepartsLocal.TextWithLiterals).Minute.ToString();
        //            rmbDepartsLocal.Text = startHour.Substring(startHour.Length - 2, 2) + ":" + startMinutes.Substring(startMinutes.Length - 2, 2);
        //            DepTimeForm.SelectedValue = "2";
        //        }
        //    }
        //}
        private void CalculateAll(bool CalcDistance, bool CalcETE)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (CalcDistance)
                {
                    CalculateDistance();
                }

                if (CalcETE)
                {
                    CalculateETE();
                }

            }
        }
        private void CalculateDistance()
        {

            //DateTime DateAndTime;
            //string DateString = string.Empty;
            //string Date = string.Empty, Time = string.Empty;
            //if (!string.IsNullOrEmpty(tbDepartsLocal.Text))
            //{
            //    DateString = string.Empty; Date = string.Empty; Time = string.Empty;
            //    Date = tbDepartsLocal.Text;
            //    Time = rmbDepartsLocal.TextWithLiterals;
            //    DateString = string.Format("{0} {1}", Date, Time, CultureInfo.InvariantCulture);
            //    DateAndTime = Convert.ToDateTime(DateString);
            //}
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((!string.IsNullOrEmpty(tbDepartsLocal.Text)) &&
                    (!string.IsNullOrEmpty(hdDepart.Value)) &&
                    (!string.IsNullOrEmpty(tbArrivalLocal.Text)) &&
                    (!string.IsNullOrEmpty(hdArrival.Value)))
                {
                    using (CalculationService.CalculationServiceClient CalculationService = new CalculationService.CalculationServiceClient())
                    {
                        double Miles = 0;
                        Miles = CalculationService.GetDistance(Convert.ToInt64(hdDepart.Value), Convert.ToInt64(hdArrival.Value));
                        tbDistance.Text = CalculationService.GetDistance(Convert.ToInt64(hdDepart.Value), Convert.ToInt64(hdArrival.Value)).ToString();
                        //hdnIPLMiles.Value = Miles.ToString();
                    }
                }
                else
                {
                    tbDistance.Text = "0";
                    //hdnIPLMiles.Value = "0";
                }
            }
        }



        private void CommonCRCalculation(string EventChange, bool isDepartOrArrival, DateTime? ChangedDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(EventChange, isDepartOrArrival))
            {
                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                    if (CorpRequest != null)
                    {
                        if ((!string.IsNullOrEmpty(hdDepart.Value) && hdDepart.Value != "0") && (!string.IsNullOrEmpty(hdArrival.Value) && hdArrival.Value != "0"))
                            SaveCRToSession();

                        if (tbDepartsLocal.Text.Trim() != "")
                        {
                            if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat != null && UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 2)
                            {

                            }
                            else
                            {
                                if (DepTimeForm.SelectedValue == "2")
                                {
                                    DateTime dt = DateTime.MinValue;
                                    string StartTime = rmbDepartsLocal.Text.Trim();
                                    if (StartTime.Trim() == "")
                                    {
                                        StartTime = "0000";
                                    }
                                    int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                    if (StartHrs != 12)
                                        StartHrs = StartHrs + 12;
                                    int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                                    string startHour = "0000" + StartHrs.ToString();
                                    string startMinutes = "0000" + StartMts.ToString();
                                    rmbDepartsLocal.Text = startHour.Substring(startHour.Length - 2, 2) + ":" + startMinutes.Substring(startMinutes.Length - 2, 2);

                                    DepTimeForm.SelectedValue = "2";
                                }
                            }
                        }

                        if (tbArrivalLocal.Text.Trim() != "")
                        {
                            if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat != null && UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 2)
                            {

                            }
                            else
                            {
                                if (ArrTimeForm.SelectedValue == "2")
                                {
                                    DateTime dt = DateTime.MinValue;
                                    string StartTime = rmbArrivalLocalTime.Text.Trim();
                                    if (StartTime.Trim() == "")
                                    {
                                        StartTime = "0000";
                                    }
                                    int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                                    if (StartHrs != 12)
                                        StartHrs = StartHrs + 12;
                                    int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                                    string startHour = "0000" + StartHrs.ToString();
                                    string startMinutes = "0000" + StartMts.ToString();
                                    rmbArrivalLocalTime.Text = startHour.Substring(startHour.Length - 2, 2) + ":" + startMinutes.Substring(startMinutes.Length - 2, 2);

                                    ArrTimeForm.SelectedValue = "2";
                                }
                            }
                        }
                        switch (EventChange)
                        {
                            case "Airport":
                                if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == CorporateRequestTripEntityState.Modified)
                                {
                                    // CorpRequest.resetLogisticsChecklist = true;  // Leela
                                    CorpRequest.State = CorporateRequestTripEntityState.Modified;
                                }
                                CalculateMiles();
                                CalcualteBias();
                                CalculateWind();
                                CalculateETE();
                                ClearAllDatesBasedonDeptOrArrival();
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateCRFARRules();
                                CalculateFlightCost();
                                break;
                            case "Miles":
                                if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == CorporateRequestTripEntityState.Modified)
                                {
                                    //CorpRequest.resetLogisticsChecklist = true;
                                    CorpRequest.State = CorporateRequestTripEntityState.Modified;
                                }
                                CalculateWind();
                                CalculateETE();
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateCRFARRules();
                                CalculateFlightCost();
                                break;
                            case "Power":
                                if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == CorporateRequestTripEntityState.Modified)
                                {
                                    // CorpRequest.resetLogisticsChecklist = true;
                                    CorpRequest.State = CorporateRequestTripEntityState.Modified;
                                }
                                CalcualteBias();
                                CalculateWind();
                                CalculateETE();
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateCRFARRules();
                                CalculateFlightCost();
                                break;
                            case "Bias":
                                if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == CorporateRequestTripEntityState.Modified)
                                {
                                    //CorpRequest.resetLogisticsChecklist = true;
                                    CorpRequest.State = CorporateRequestTripEntityState.Modified;
                                }
                                CalculateETE();
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateCRFARRules();
                                CalculateFlightCost();
                                break;
                            case "Wind":
                                if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == CorporateRequestTripEntityState.Modified)
                                {
                                    //CorpRequest.resetLogisticsChecklist = true;
                                    CorpRequest.State = CorporateRequestTripEntityState.Modified;
                                }
                                CalculateETE();
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateCRFARRules();
                                CalculateFlightCost();
                                break;
                            case "ETE":
                                if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == CorporateRequestTripEntityState.Modified)
                                {
                                    //CorpRequest.resetLogisticsChecklist = true;
                                    CorpRequest.State = CorporateRequestTripEntityState.Modified;
                                }
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateCRFARRules();
                                CalculateFlightCost();
                                break;
                            case "WindReliability":
                                if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == CorporateRequestTripEntityState.Modified)
                                {
                                    //CorpRequest.resetLogisticsChecklist = true;
                                    CorpRequest.State = CorporateRequestTripEntityState.Modified;
                                }
                                CalculateWind();
                                CalculateETE();
                                CalculateDateTime(isDepartOrArrival, null);
                                CalculateCRFARRules();
                                CalculateFlightCost();
                                break;
                            case "Date":
                                if (CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].State == CorporateRequestTripEntityState.Modified)
                                {
                                    //CorpRequest.resetLogisticsChecklist = true;
                                    CorpRequest.State = CorporateRequestTripEntityState.Modified;
                                }
                                CalculateDateTime(isDepartOrArrival, ChangedDate);
                                CalculateWind();
                                CalculateETE();
                                if (isDepartOrArrival)
                                {
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].IsDepartureConfirmed = true;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].IsArrivalConfirmation = false;
                                }
                                else
                                {
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].IsDepartureConfirmed = false;
                                    CorpRequest.CRLegs[Convert.ToInt16(hdnLeg.Value) - 1].IsArrivalConfirmation = true;
                                }
                                CalculateDateTime(isDepartOrArrival, ChangedDate);
                                CalculateCRFARRules();
                                CalculateFlightCost();
                                break;
                            case "CrewDutyRule":
                                CalculateCRFARRules();
                                CalculateFlightCost();
                                break;
                        }


                        if (!string.IsNullOrEmpty(tbDepartsLocal.Text))
                        {
                            string StartTime = rmbDepartsLocal.Text.Trim();
                            if (StartTime.Trim() == "")
                            {
                                StartTime = "0000";
                            }
                            int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                            int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                            DateTime dt = DateTime.ParseExact(tbDepartsLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                            dt = dt.AddHours(StartHrs);
                            dt = dt.AddMinutes(StartMts);
                            if (dt != null && dt != DateTime.MinValue)
                            {
                                if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat != null && UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 2)
                                {
                                    string startHrs = "0000" + dt.Hour.ToString();
                                    string startMins = "0000" + dt.Minute.ToString();
                                    rmbDepartsLocal.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                    DepTimeForm.Visible = false;
                                }
                                else
                                {
                                    string startHrs = "0000" + dt.Hour.ToString();
                                    string startMins = "0000" + dt.Minute.ToString();
                                    rmbDepartsLocal.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                    DepTimeForm.Visible = true;
                                    string AMPM = dt.ToString("tt");
                                    if (AMPM == "AM" || AMPM == "am")
                                    {
                                        //if (rmbDepartsLocal.TextWithLiterals.Contains("00:"))
                                        //{
                                        //    rmbDepartsLocal.Text = rmbDepartsLocal.TextWithLiterals.Replace("00:", "12:");
                                        //}
                                        DepTimeForm.SelectedValue = "1";
                                    }
                                    else
                                    {
                                        int Hour = dt.Hour - 12;
                                        string startHour = "0000" + Hour.ToString();
                                        string startMinutes = "0000" + dt.Minute.ToString();
                                        rmbDepartsLocal.Text = startHour.Substring(startHour.Length - 2, 2) + ":" + startMinutes.Substring(startMinutes.Length - 2, 2);
                                        //if (rmbDepartsLocal.TextWithLiterals.Contains("00:"))
                                        //{
                                        //    rmbDepartsLocal.Text = rmbDepartsLocal.TextWithLiterals.Replace("00:", "12:");
                                        //}
                                        DepTimeForm.SelectedValue = "2";
                                    }
                                }
                            }
                        }


                        if (!string.IsNullOrEmpty(tbArrivalLocal.Text))
                        {
                            string StartTime = rmbArrivalLocalTime.Text.Trim();
                            if (StartTime.Trim() == "")
                            {
                                StartTime = "0000";
                            }
                            int StartHrs = Convert.ToInt16(StartTime.Substring(0, 2));
                            int StartMts = Convert.ToInt16(StartTime.Substring(2, 2));
                            DateTime dt = DateTime.ParseExact(tbArrivalLocal.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                            dt = dt.AddHours(StartHrs);
                            dt = dt.AddMinutes(StartMts);
                            if (dt != null && dt != DateTime.MinValue)
                            {
                                if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat != null && UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 2)
                                {
                                    string startHrs = "0000" + dt.Hour.ToString();
                                    string startMins = "0000" + dt.Minute.ToString();
                                    rmbArrivalLocalTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                    ArrTimeForm.Visible = false;
                                }
                                else
                                {
                                    string startHrs = "0000" + dt.Hour.ToString();
                                    string startMins = "0000" + dt.Minute.ToString();
                                    rmbArrivalLocalTime.Text = startHrs.Substring(startHrs.Length - 2, 2) + ":" + startMins.Substring(startMins.Length - 2, 2);

                                    ArrTimeForm.Visible = true;
                                    string AMPM = dt.ToString("tt");
                                    if (AMPM == "AM" || AMPM == "am")
                                    {
                                        //if (rmbArrivalLocalTime.TextWithLiterals.Contains("00:"))
                                        //{
                                        //    rmbArrivalLocalTime.Text = rmbArrivalLocalTime.TextWithLiterals.Replace("00:", "12:");
                                        //}
                                        ArrTimeForm.SelectedValue = "1";
                                    }
                                    else
                                    {
                                        int Hour = dt.Hour - 12;
                                        string startHour = "0000" + Hour.ToString();
                                        string startMinutes = "0000" + dt.Minute.ToString();
                                        rmbArrivalLocalTime.Text = startHour.Substring(startHour.Length - 2, 2) + ":" + startMinutes.Substring(startMinutes.Length - 2, 2);
                                        //if (rmbArrivalLocalTime.TextWithLiterals.Contains("00:"))
                                        //{
                                        //    rmbArrivalLocalTime.Text =  rmbArrivalLocalTime.TextWithLiterals.Replace("00:", "12:");
                                        //}
                                        ArrTimeForm.SelectedValue = "2";
                                    }
                                }
                            }
                        }


                        if ((!string.IsNullOrEmpty(hdDepart.Value) && hdDepart.Value != "0") && (!string.IsNullOrEmpty(hdArrival.Value) && hdArrival.Value != "0"))
                            SaveCRToSession();
                    }

                }
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == null || UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 1)
            {
                if (rmbArrivalLocalTime.TextWithLiterals.Contains("00:"))
                {
                    rmbArrivalLocalTime.Text = rmbArrivalLocalTime.TextWithLiterals.Replace("00:", "12:");
                }

                if (rmbDepartsLocal.TextWithLiterals.Contains("00:"))
                {
                    rmbDepartsLocal.Text = rmbDepartsLocal.TextWithLiterals.Replace("00:", "12:");
                }
            }
            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == null || UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
            {
                if (tbElapsedTime.Text.ToString().Contains(':') == false)
                    tbElapsedTime.Text = ConvertTenthsToMins(tbElapsedTime.Text);
            }
        }

        #endregion

        #region CompanyProfile DateTimeCalculation

        /// <summary>
        /// Method to Set Default Standard and Flightpak Conversion into List
        /// </summary>
        public List<StandardFlightpakConversion> LoadStandardFPConversion()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<StandardFlightpakConversion> conversionList = new List<StandardFlightpakConversion>();
                // Set Standard Conversions
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.0M, StartMinutes = 0, EndMinutes = 5, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.1M, StartMinutes = 6, EndMinutes = 11, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.2M, StartMinutes = 12, EndMinutes = 17, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.3M, StartMinutes = 18, EndMinutes = 23, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.4M, StartMinutes = 24, EndMinutes = 29, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.5M, StartMinutes = 30, EndMinutes = 35, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.6M, StartMinutes = 36, EndMinutes = 41, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.7M, StartMinutes = 42, EndMinutes = 47, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.8M, StartMinutes = 48, EndMinutes = 53, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.9M, StartMinutes = 54, EndMinutes = 59, ConversionType = 1 });

                // Set Flightpak Conversions
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.0M, StartMinutes = 0, EndMinutes = 2, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.1M, StartMinutes = 3, EndMinutes = 8, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.2M, StartMinutes = 9, EndMinutes = 14, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.3M, StartMinutes = 15, EndMinutes = 21, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.4M, StartMinutes = 22, EndMinutes = 27, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.5M, StartMinutes = 28, EndMinutes = 32, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.6M, StartMinutes = 33, EndMinutes = 39, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.7M, StartMinutes = 40, EndMinutes = 44, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.8M, StartMinutes = 45, EndMinutes = 50, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.9M, StartMinutes = 51, EndMinutes = 57, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 1.0M, StartMinutes = 58, EndMinutes = 59, ConversionType = 2 });

                return conversionList;
            }
        }

        /// <summary>
        /// Method to Set Time Format, based on Company Profile
        /// </summary>
        public void TimeDisplay()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (UserPrincipal.Identity != null)
                {
                    // Display Time Fields based on Tenths / Minutes - Company Profile
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 1)
                    {
                        SetMask("0.0", "<0..99>.<0..9>");
                    }
                    else
                    {
                        SetMask("00:00", "<00..99>:<00..99>");
                    }
                }
                else
                {
                    SetMask("00:00", "<00..99>:<00..99>");
                }
            }
        }

        /// <summary>
        /// Method to Set Mask Fields
        /// </summary>
        /// <param name="display">Pass Display Type</param>
        /// <param name="mask">Pass Mask Type</param>
        private void SetMask(string display, string mask)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(display, mask))
            {
                //tbETE.Text = display;
                //tbETE.Mask = mask;
                //hdnTakeoffBIAS.Value = display;
                //tbBias.Mask = mask;
                //hdnLandingBIAS.Value = display;
                //tbLandBias.Mask = mask;
                //if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != 2)
                //{

                //    //tbETE.Mask = "n";
                //    //LiteralMaskPart literalMaskPart = new LiteralMaskPart();
                //    //if (literalMaskPart.Text == "-")
                //    //{
                //    //    literalMaskPart.Text = "-";
                //    //}
                //    //else
                //    //    literalMaskPart.Text = "";
                //    //tbETE.MaskParts.Add(literalMaskPart);
                //}
            }
        }


        //protected override void OnKeyDown(object sender,EventArgs e)
        //{

        //}
        /// <summary>
        /// Method to Get Tenths Conversion Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <param name="isTenths">Pass Boolean Value</param>
        /// <param name="conversionType">Pass Conversion Type</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        protected string ConvertMinToTenths(String time, Boolean isTenths, String conversionType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time, isTenths, conversionType))
            {
                string result = "0.0";

                if (time.IndexOf(":") != -1)
                {
                    string[] timeArray = time.Split(':');
                    decimal hour = Convert.ToDecimal(timeArray[0]);
                    decimal minute = Convert.ToDecimal(timeArray[1]);

                    if (isTenths && (conversionType == "1" || conversionType == "2")) // Standard and Flightpak Conversion
                    {
                        if (Session["StandardFlightpakConversion"] != null)
                        {
                            List<StandardFlightpakConversion> StandardFlightpakConversionList = (List<StandardFlightpakConversion>)Session["StandardFlightpakConversion"];
                            var standardList = StandardFlightpakConversionList.ToList().Where(x => minute >= x.StartMinutes && minute <= x.EndMinutes && x.ConversionType.ToString() == conversionType).ToList();

                            if (standardList.Count > 0)
                            {
                                result = Convert.ToString(hour + standardList[0].Tenths);
                            }
                        }
                    }
                    //else if (isTenths && conversionType == "3") // Tenths - Min Conversion : Others
                    //{
                    //    List<GetPOHomeBaseSetting> homeBaseSetting = (List<GetPOHomeBaseSetting>)Session[Master.POHomeBaseKey];
                    //    List<GetPOTenthsConversion> tenthConvList = homeBaseSetting[0].TenthConversions.ToList().Where(x => minute >= x.StartMinimum && minute <= x.EndMinutes).ToList();

                    //    if (tenthConvList.Count > 0)
                    //    {
                    //        result = Convert.ToString(hour + tenthConvList[0].Tenths);
                    //    }
                    //}
                    else
                    {
                        decimal decimalOfMin = 0;
                        if (minute > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = minute / 60;

                        result = Convert.ToString(hour + decimalOfMin);
                    }
                }
                return result;
            }
        }

        protected string directMinstoTenths(string time)
        {
            string result = "0.0";

            if (time.IndexOf(":") != -1)
            {
                string[] timeArray = time.Split(':');
                decimal hour = Convert.ToDecimal(timeArray[0]);
                decimal minute = Convert.ToDecimal(timeArray[1]);

                decimal decimalOfMin = 0;
                if (minute > 0) // Added conditon to avoid divide by zero error.
                    decimalOfMin = minute / 60;

                result = Convert.ToString(hour + decimalOfMin);
            }

            return result;
        }

        /// <summary>
        /// Method to Get Minutes from decimal Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        protected string ConvertTenthsToMins(String time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time))
            {

                string result = "00:00";
                int val;
                if (!string.IsNullOrEmpty(time))
                {
                    if (time.IndexOf(".") < 0)

                        time = time + ".0";

                    if (time.IndexOf(".") != -1)
                    {
                        string[] timeArray = time.Split('.');
                        decimal hour = Convert.ToDecimal(timeArray[0]);
                        decimal decimal_min = Convert.ToDecimal(String.Concat("0.", timeArray[1]));
                        decimal decimalOfMin = 0;
                        if (decimal_min > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = (decimal_min * 60 / 100);
                        decimal finalval = Math.Round(hour + decimalOfMin, 2);
                        result = Convert.ToString(finalval).Replace(".", ":");
                        if (result.IndexOf(":") < 0)
                            result = result + ":00";

                        timeArray = result.Split(':');
                        if (timeArray[0].Length == 1)
                        {
                            result = "0" + result;
                        }
                        if (timeArray[1].Length == 1)
                        {
                            result = result + "0";
                        }
                    }
                    else if (int.TryParse(time, out val))
                    {
                        result = time;
                    }
                }

                return result;
            }
        }


        protected decimal ConvertToKilomenterBasedOnCompanyProfile(decimal Miles)
        {

            if (UserPrincipal.Identity._fpSettings._IsKilometer == null ? false : (bool)UserPrincipal.Identity._fpSettings._IsKilometer)
                return Math.Round(Miles * 1.852M, 0);
            else
                return Miles;
        }

        protected decimal ConvertToMilesBasedOnCompanyProfile(decimal Kilometers)
        {

            if (UserPrincipal.Identity._fpSettings._IsKilometer == null ? false : (bool)UserPrincipal.Identity._fpSettings._IsKilometer)
                return Math.Round(Kilometers / 1.852M, 0);
            else
                return Kilometers;
        }

        protected double RoundElpTime(double lnElp_Time)
        {
            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
            {

                decimal ElapseTMRounding = 0;

                //lnEteRound=(double)CompanyLists[0].ElapseTMRounding~lnRound;
                double lnElp_Min = 0.0;
                double lnEteRound = 0.0;

                if (UserPrincipal.Identity._fpSettings._ElapseTMRounding != null)
                    ElapseTMRounding = (decimal)UserPrincipal.Identity._fpSettings._ElapseTMRounding;

                if (ElapseTMRounding == 1)
                    lnEteRound = 0;
                else if (ElapseTMRounding == 2)
                    lnEteRound = 5;
                else if (ElapseTMRounding == 3)
                    lnEteRound = 10;
                else if (ElapseTMRounding == 4)
                    lnEteRound = 15;
                else if (ElapseTMRounding == 5)
                    lnEteRound = 30;
                else if (ElapseTMRounding == 6)
                    lnEteRound = 60;

                if (lnEteRound > 0)
                {

                    lnElp_Min = (lnElp_Time - Math.Floor(lnElp_Time)) * 60;
                    //lnElp_Min = lnElp_Min % lnEteRound;
                    if (((lnElp_Min % lnEteRound) / lnEteRound) <= 0.5)
                        lnElp_Min = lnElp_Min - (lnElp_Min % lnEteRound);
                    else
                        lnElp_Min = lnElp_Min + (lnEteRound - (lnElp_Min % lnEteRound));


                    if (lnElp_Min > 0 && lnElp_Min < 60)
                        lnElp_Time = Math.Floor(lnElp_Time) + lnElp_Min / 60;

                    if (lnElp_Min == 0)
                        lnElp_Time = Math.Floor(lnElp_Time);


                    if (lnElp_Min == 60)
                        lnElp_Time = Math.Ceiling(lnElp_Time);

                }
            }
            return lnElp_Time;
        }
        //lcEteTimeValue = DHM2THM(lnElapTime, lnElpMaxHrsChars);



        //Decimal hour minute DD.DD to time hour minute HHHHH:MM 
        protected string DecimalHourMinuteToTimeHourMinute(double lnElp_Time)
        {
            int lnPos = 0, lnLengthOfTimeStr = 0, lnLength = 2;
            string lcEteTimeValue = string.Empty, lcHoursMinutesWithDecimal = string.Empty, Lchour = string.Empty, Lcmin = string.Empty;

            lcHoursMinutesWithDecimal = lnElp_Time.ToString(); //lnHoursMinutesWithDecimal.ToString().Substring(4, 12);
            lnPos = lcHoursMinutesWithDecimal.IndexOf('.');
            lnLengthOfTimeStr = lcHoursMinutesWithDecimal.Length;
            Lchour = lcHoursMinutesWithDecimal.Substring(0, lnPos - 1);
            Lchour = Lchour.PadLeft(lnLength, ' ');
            Lcmin = lcHoursMinutesWithDecimal.Substring(lnPos + 1, lnLengthOfTimeStr - lnPos);
            return Lchour + ":" + DecimalMinutesToTimeMinutes(Lcmin.Substring(0, 2));
        }


        protected string DecimalMinutesToTimeMinutes(string lcMinutes)
        {
            string lcD = string.Empty;
            double lnMin = 0.0, lnC = 0.0;
            //int TenthsToMin = 0;
            //oApp.TenthsToMin // take it from UI page
            //if (TenthsToMin == 2)//Time Display is set to Minute in Preflight tab of Company Profile
            //{
            //lnC = Convert.ToDouble(lcMinutes) * 60;
            //lcD = (Math.Round((lnC / 100), -1)).ToString().PadLeft(2, '0'); //lcD = PADL(ALLTRIM(STR(ROUND(lnC/100,-1))),2,"0")
            //lnC = Convert.ToDouble(lcMinutes.Substring(lnPos, -2)); //lnC = ROUND((VAL(lcMinutes)/1000)*60,0)
            lnC = (Convert.ToDouble("." + lcMinutes)) * 60;
            //}
            //else
            //{
            //    lnMin = Convert.ToDouble(lcMinutes);
            //    lnMin = lnMin - (lnMin % 100);
            //    lnC = Math.Round((lnMin / 1000) * 60, 0);
            //}

            //lcD = lnC.ToString().Substring(0, 7).PadLeft(2, '0');
            lcD = lnC.ToString();
            if (lcD.IndexOf('.') > 0)
            {
                lcD = lcD.Substring(0, lcD.IndexOf('.') - 1);
            }
            return lcD;
        }


        protected bool ValidateTenthMinBasedonCompanyprofile(TextBox cntrlToset)
        {
            bool ValidationSucceeded = true;

            string ValtoCheck = cntrlToset.Text;

            decimal companyprofileSetting = 1;
            companyprofileSetting = UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == null ? 1 : (decimal)UserPrincipal.Identity._fpSettings._TimeDisplayTenMin;
            if (companyprofileSetting == 1)
            {
                var count = 0;
                var strtext = cntrlToset.Text;
                for (var i = 0; i < strtext.Length; ++i)
                {
                    if (strtext[i] == '.')
                        count++;
                }
                if (count > 1)
                {
                    RadWindowManager1.RadAlert("Invalid format, Required format is NN.N ", 330, 100, "Request Alert", null);
                    ValtoCheck = "0.0";
                    ValidationSucceeded = false;
                }
                if (ValtoCheck.Length > 0)
                {
                    if (ValtoCheck.IndexOf(".") >= 0)
                    {
                        string[] strarr = ValtoCheck.Split('.');
                        if (strarr[0].Length > 2)
                        {
                            RadWindowManager1.RadAlert("Invalid format, Required format is NN.N ", 330, 100, "Request Alert", null);
                            ValtoCheck = "0.0";
                            ValidationSucceeded = false;
                        }
                        if (strarr[1].Length > 1)
                        {
                            RadWindowManager1.RadAlert("Invalid format, Required format is NN.N ", 330, 100, "Request Alert", null);
                            ValtoCheck = "0.0";
                            ValidationSucceeded = false;
                        }

                    }
                    else
                    {
                        if (ValtoCheck.Length > 2)
                        {
                            RadWindowManager1.RadAlert("Invalid format, Required format is NN.N ", 330, 100, "Request Alert", null);
                            ValtoCheck = "0.0";
                            ValidationSucceeded = false;
                        }
                        else
                        {
                            ValtoCheck = ValtoCheck + ".0";
                        }
                    }
                }


            }
            else
            {

                var hourvalue = "00";
                var minvalue = "00";
                if (ValtoCheck.Length > 0)
                {
                    if (ValtoCheck.IndexOf(":") >= 0)
                    {
                        var strarr = ValtoCheck.Split(':');
                        if (strarr[0].Length <= 2)
                        {
                            hourvalue = "00" + strarr[0];
                            hourvalue = hourvalue.Substring(hourvalue.Length - 2, 2);
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Invalid format, Required format is NN:NN ", 330, 100, "Request Alert", null);
                            ValtoCheck = "00:00";
                            ValidationSucceeded = false;
                        }
                        if (ValidationSucceeded)
                        {
                            if (strarr[1].Length <= 2)
                            {
                                if (Convert.ToInt16(strarr[1]) > 59)
                                {
                                    RadWindowManager1.RadAlert("Minute entry must be between 0 and 59", 330, 100, "Request Alert", null);
                                    hourvalue = "00";
                                    minvalue = "00";
                                    ValidationSucceeded = false;
                                }
                                else
                                {
                                    minvalue = "00" + strarr[1];
                                    minvalue = minvalue.Substring(minvalue.Length - 2, 2);
                                }
                            }

                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Invalid format, Required format is NN:NN ", 330, 100, "Request Alert", null);
                            ValtoCheck = "00:00";
                            ValidationSucceeded = false;
                        }
                    }
                    else
                    {

                        if (ValtoCheck.Length <= 2)
                        {
                            hourvalue = "00" + ValtoCheck;
                            hourvalue = hourvalue.Substring(hourvalue.Length - 2, 2);
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Invalid format, Required format is NN:NN ", 330, 100, "Request Alert", null);
                            ValtoCheck = "00:00";
                            ValidationSucceeded = false;
                        }
                    }


                }

                ValtoCheck = hourvalue + ":" + minvalue;



            }

            cntrlToset.Text = ValtoCheck;

            return ValidationSucceeded;
        }


        private bool ValidateDateFormat(TextBox TxtDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TxtDate))
            {
                bool ReturnValue = true;
                if (!string.IsNullOrEmpty(TxtDate.Text))
                {
                    DateTime DDate = new DateTime();
                    if (CompDateFormat != null)
                    {
                        DDate = Convert.ToDateTime(FormatDate(TxtDate.Text, CompDateFormat, false));
                    }
                    else
                    {
                        DDate = Convert.ToDateTime(TxtDate.Text);
                    }
                    if ((DDate.Year < 1900) || (DDate.Year > 2100))
                    {
                        RadWindowManager1.RadAlert("Please enter / select Date between 01/01/1900 and 12/31/2100", 330, 100, "Request Alert", null);
                        TxtDate.Text = string.Empty;
                        TxtDate.Focus();
                        ReturnValue = false;
                    }
                }
                return ReturnValue;
            }
        }
        public string FormatDate(string Date, string Format, bool AppendTime)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Date, Format))
            {
                string[] DateAndTime = Date.Split(' ');
                string[] SplitDate = new string[3];
                string[] SplitFormat = new string[3];
                if (Format.Contains("/"))
                {
                    SplitFormat = Format.Split('/');
                }
                else if (Format.Contains("-"))
                {
                    SplitFormat = Format.Split('-');
                }
                else if (Format.Contains("."))
                {
                    SplitFormat = Format.Split('.');
                }
                if (DateAndTime[0].Contains("/"))
                {
                    SplitDate = DateAndTime[0].Split('/');
                }
                else if (DateAndTime[0].Contains("-"))
                {
                    SplitDate = DateAndTime[0].Split('-');
                }
                else if (DateAndTime[0].Contains("."))
                {
                    SplitDate = DateAndTime[0].Split('.');
                }
                int dd = 0, mm = 0, yyyy = 0;
                for (int Index = 0; Index < SplitFormat.Count(); Index++)
                {
                    if (SplitFormat[Index].ToLower().Contains("d"))
                    {
                        dd = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("m"))
                    {
                        mm = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("y"))
                    {
                        yyyy = Convert.ToInt16(SplitDate[Index]);
                    }
                }
                //return new DateTime(yyyy, mm, dd);
                if (AppendTime == true)
                {
                    return (yyyy + "/" + mm + "/" + dd + ' ' + DateAndTime[1]);
                }
                else
                {
                    return (yyyy + "/" + mm + "/" + dd);
                }
            }
        }

        #endregion
    }
}
