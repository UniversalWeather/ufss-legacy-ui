﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaxPassportVisa.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.CorporateRequest.PaxPassportVisa" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PAX Passport Visa</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jquery.alerts.js") %>"></script>
        <script type="text/javascript">
            //            var oArg = new Object();
            //            var grid = $find("<%= dgPassport.ClientID %>");

            //this function is used to close this window
            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshGrid(arg);
            }
            function rebindgrid() {
                var masterTable = $find("<%= dgPassport.ClientID %>").get_masterTableView();
                var masterTable1 = $find("<%= dgVisa.ClientID %>").get_masterTableView();
                masterTable.rebind();
                // masterTable1.rebind();
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function RowDblClick() {
                var masterTable = $find("<%= dgPassport.ClientID %>").get_masterTableView();
                masterTable.fireCommand("InitInsert", "");
                return false;
            }

            function RowDblClick1() {
                var masterTable = $find("<%= dgVisa.ClientID %>").get_masterTableView();
                masterTable.fireCommand("InitInsert", "");
                return false;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgPassport.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "PassengerRequestorID");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "PassportNum");
                }
                if (selectedRows.length > 0) {
                    oArg.PassengerRequestorID = cell1.innerHTML;
                    oArg.PassportNum = cell2.innerHTML;

                }
                else {
                    oArg.PassengerRequestorID = "";
                    oArg.PassportNum = "";
                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function GetGridId() {
                return $find("<%= dgPassport.ClientID %>");
            }

            function GetGridId1() {
                return $find("<%= dgVisa.ClientID %>");
            }      
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table width="100%" cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <telerik:RadGrid ID="dgPassport" runat="server" OnNeedDataSource="Passport_BindData"
                        EnableAJAX="True" AutoGenerateColumns="false" AllowFilteringByColumn="false"
                        Height="200px" Width="850px" OnItemDataBound="dgPassport_ItemDataBound" OnItemCommand="dgPassport_ItemCommand"
                        OnInsertCommand="dgPassport_InsertCommand" OnItemCreated="dgPassport_ItemCreated">
                        <MasterTableView AutoGenerateColumns="False" EditMode="InPlace" TableLayout="Fixed"
                            DataKeyNames="CrewID,PassportID,PassengerRequestorID,Choice,IssueCity,PassportNum,PassportExpiryDT,IsDefaultPassport,PilotLicenseNum ,LastUpdUID
                             ,LastUpdTS,IssueDT,IsDeleted,CountryID,CountryCD" CommandItemDisplay="None"
                            ShowFooter="false" AllowFilteringByColumn="false">
                            <Columns>
                                <telerik:GridBoundColumn DataField="PassportNum" HeaderText="Passport No." ShowFilterIcon="false"
                                    Display="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn DataField="Choice" HeaderText="Choice" ShowFilterIcon="false"
                                    Display="true">
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridBoundColumn DataField="PassportExpiryDT" HeaderText="Expiration Date"
                                    ShowFilterIcon="false" Display="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="IssueCity" HeaderText="Issuing City/Authority"
                                    ShowFilterIcon="false" Display="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="IssueDT" HeaderText="Issue Date" ShowFilterIcon="false"
                                    Display="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CountryCD" HeaderText="Nationality" ShowFilterIcon="false"
                                    Display="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PassportID" HeaderText="Passport ID" ShowFilterIcon="false"
                                    Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="PassengerRequestorID" DataField="PassengerRequestorID"
                                    HeaderText="PAX ID" ShowFilterIcon="false" Display="false">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div class="grid_icon">
                                    <%--  <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                        CommandName="PerformInsert" ></asp:LinkButton>--%>
                                    <%-- Visible='<%# IsAuthorized(Permission.Database.AddPassengerRequestor)%>'--%>
                                    <asp:LinkButton ID="lnkInitEdit" runat="server" ToolTip="Edit" CssClass="edit-icon-grid"
                                        CommandName="Edit" OnClientClick="return GetGridId();"></asp:LinkButton>
                                    <%--Visible='<%# IsAuthorized(Permission.Database.EditPassengerRequestor)%>' --%>
                                    <%--   <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDeletePopup('dgPassport', 'radPaxInfoPopup');"
                                        CssClass="delete-icon-grid" ToolTip="Delete" CommandName="DeleteSelected" ></asp:LinkButton>--%>
                                    <%-- Visible='<%# IsAuthorized(Permission.Database.DeletePassengerRequestor)%>' --%>
                                </div>
                                <div style="padding: 5px 5px; text-align: right;">
                                    <asp:Button ID="btnSubmit" OnClientClick="returnToParent(); return false;" runat="server"
                                        Text="OK" CssClass="button" />
                                    <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                        CausesValidation="false" CssClass="button" Text="OK"></asp:LinkButton>
                                </div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="RowDblClick" />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                    </telerik:RadGrid>
                    <br />
                    <asp:Label ID="lbMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="nav-space">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadGrid ID="dgVisa" runat="server" OnNeedDataSource="Visa_BindData" OnItemDataBound="Visa_ItemDataBound"
                        OnItemCommand="dgVisa_ItemCommand" OnItemCreated="Visa_ItemCreated" EnableAJAX="True"
                        Height="200px" Width="850px">
                        <MasterTableView AutoGenerateColumns="False" EditMode="InPlace" TableLayout="Fixed"
                            DataKeyNames="VisaID,CrewID,PassengerRequestorID,CustomerID,VisaTYPE,VisaNum,LastUpdUID,LastUpdTS,CountryID,ExpiryDT,Notes,IssuePlace,IssueDate,IsDeleted,CountryCD,CountryName"
                            CommandItemDisplay="None" ShowFooter="false" AllowFilteringByColumn="false">
                            <Columns>
                                <telerik:GridBoundColumn DataField="CountryCD" UniqueName="CountryCD" HeaderText="Country"
                                    ShowFilterIcon="false" Display="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="VisaNum" UniqueName="VisaNum" HeaderText="Visa No."
                                    ShowFilterIcon="false" Display="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ExpiryDT" UniqueName="ExpiryDT" HeaderText="Expiration Date"
                                    ShowFilterIcon="false" Display="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="IssuePlace" UniqueName="IssuePlace" HeaderText="Issuing City/Authority"
                                    ShowFilterIcon="false" Display="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="IssueDate" UniqueName="IssueDate" HeaderText="Issue Date"
                                    ShowFilterIcon="false" Display="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Notes" UniqueName="Notes" HeaderText="Notes"
                                    ShowFilterIcon="false" Display="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="VisaID" UniqueName="VisaID" HeaderText="VisaID"
                                    ShowFilterIcon="false" Display="false">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                            <CommandItemTemplate>
                                <div class="grid_icon">
                                    <%--     <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                        CommandName="PerformInsert" ></asp:LinkButton>--%>
                                    <%-- Visible='<%# IsAuthorized(Permission.Database.AddPassengerRequestor)%>'--%>
                                    <asp:LinkButton ID="lnkInitEdit" runat="server" ToolTip="Edit" CssClass="edit-icon-grid"
                                        CommandName="Edit" OnClientClick="return GetGridId1();"></asp:LinkButton>
                                    <%--Visible='<%# IsAuthorized(Permission.Database.EditPassengerRequestor)%>' --%>
                                    <%--   <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDeletePopup('dgVisa', 'radPaxInfoPopup');"
                                        CssClass="delete-icon-grid" ToolTip="Delete" CommandName="DeleteSelected" ></asp:LinkButton>--%>
                                    <%-- Visible='<%# IsAuthorized(Permission.Database.DeletePassengerRequestor)%>' --%>
                                </div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="RowDblClick1" />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
