﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AirportCityPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.CorporateRequest.AirportCityPopup" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Airport</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <link href="../../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jquery.alerts.js") %>"></script>
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgAirport.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgAirport.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "IcaoID");
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "AirportName");
                        var cell3 = MasterTable.getCellByColumnUniqueName(row, "AirportID");
                        var cell4 = MasterTable.getCellByColumnUniqueName(row, "CountryCD");
                        var cell5 = MasterTable.getCellByColumnUniqueName(row, "CityName");


                        if (i == 0) {
                            oArg.ICAO = cell1.innerHTML + ",";
                            oArg.AirportName = cell2.innerHTML;
                            oArg.AirportID = cell3.innerHTML;
                            oArg.CountryName = cell4.innerHTML;
                            oArg.CityName = cell5.innerHTML;
                        }
                        else {
                            oArg.ICAO += cell1.innerHTML + ",";
                            oArg.AirportName += cell2.innerHTML + ",";
                            oArg.AirportID += cell3.innerHTML + ",";
                            oArg.CountryName += cell4.innerHTML + ",";
                            oArg.CityName += cell5.innerHTML + ",";
                        }

                    }

                }
                else {
                    oArg.ICAO = "";
                    oArg.AirportName = "";
                    oArg.AirportID = "";
                    oArg.CountryName = "";
                    oArg.CityName = "";
                }
                if (oArg.ICAO != "")
                    oArg.ICAO = oArg.ICAO.substring(0, oArg.ICAO.length - 1)
                else
                    oArg.ICAO = "";

                oArg.Arg1 = oArg.ICAO;
                oArg.CallingButton = "IcaoID";

                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function rebindgrid() {
                var masterTable = $find("<%= dgAirport.ClientID %>").get_masterTableView();
                masterTable.rebind();
            }
            function GetGridId() {
                return $find("<%= dgAirport.ClientID %>");
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgAirport">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAirport" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table class="box1">
            <tr>
                <td>
                    <telerik:RadGrid ID="dgAirport" runat="server" AllowMultiRowSelection="true" AllowSorting="true"
                        OnNeedDataSource="dgAirport_BindData" AutoGenerateColumns="false" Height="341px"
                        PageSize="10" AllowPaging="true" Width="865px" OnItemCommand="dgAirport_ItemCommand" OnPreRender="dgAirport_PreRender">
                        <MasterTableView DataKeyNames="AirportID" CommandItemDisplay="Bottom" PageSize="10">
                            <Columns>
                                <telerik:GridBoundColumn DataField="IcaoID" HeaderText="ICAO" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CityName" HeaderText="City" AutoPostBackOnFilter="false"
                                    HeaderStyle-Width="150px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="StateName" HeaderText="State" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CountryCD" HeaderText="Country" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AirportName" HeaderText="Airport" AutoPostBackOnFilter="false"
                                    HeaderStyle-Width="150px" ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AirportID" HeaderText="AirportID" CurrentFilterFunction="EqualTo"
                                    Display="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Iata" HeaderText="IATA" AutoPostBackOnFilter="false"
                                    Display="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterDelay="500">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div style="padding: 5px 5px; text-align: right;">
                                    <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                                        Ok</button>
                                    <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                                </div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="returnToParent"  />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
