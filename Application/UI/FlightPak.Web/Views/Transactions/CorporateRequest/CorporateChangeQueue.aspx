﻿<%@ Page Title="Corporate Request Change Queue" Language="C#" AutoEventWireup="true"
    CodeBehind="CorporateChangeQueue.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.CorporateRequest.CorporateChangeQueue" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="FlightPak.Common.Constants" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Corporate Request Change Queue</title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="../../../Scripts/Common.js"></script>
        <script type="text/javascript">

            function confirmSubmitCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnSubmitYes.ClientID%>').click();
                }
            }

            function openWin(radWin) {
                var url = '';
                if (radWin == "RadFilterClientPopup") {
                    url = "../../Settings/Company/ClientCodePopup.aspx?ClientCD=" + document.getElementById("<%=tbClientCodeFilter.ClientID%>").value;
                }
                else if (radWin == "RadTripManagerPopup") {
                    url = "../Preflight/PreFlightMain.aspx";
                }
                else if (radWin == "RadCorpRequestPopup") {
                    url = "CorporateRequestMain.aspx";
                }

                var oWnd = radopen(url, radWin);
            }

            // this function is used to get the dimensions
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            function ClientCodeFilterPopupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbClientCodeFilter.ClientID%>").value = arg.ClientCD;
                    }
                }
            }
            function OnClientCloseTripManagerPopup(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                    }
                }
            }
            function OnClientCloseCorpRequestPopup(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                    }
                }
            }
            function btnMarkAccepted_OnClientClick(sender, e) {
                var ReturnValue = false;
                var grid = $find("<%=dgChangeQueueList.ClientID %>");
                var MasterTable = grid.get_masterTableView().get_selectedItems();
                var length = MasterTable.length;
                var AcknowledgementStatus = MasterTable[0].getDataKeyValue("AcknowledgementStatus");
                AcknowledgementStatus = AcknowledgementStatus.replace(/^\s+|\s+$/g, '')
                var CorporateRequestStatus = MasterTable[0].getDataKeyValue("CorporateRequestStatus");
                CorporateRequestStatus = CorporateRequestStatus.replace(/^\s+|\s+$/g, '')
                if (length != 0) {
                    if ((AcknowledgementStatus == "A") && (CorporateRequestStatus == "S")) {
                        var Msg = "Warning - No Trip Request Information Will Be <br>Transferred to Tripsheet. Any Tripsheet Changes <br>Must Be Done Manually. Continue Marking Trip <br>Request As Accepted (Y/N)?";
                        var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                            if (shouldSubmit) {
                                __doPostBack('btnMarkAccepted', '');
                            }
                        });
                        radconfirm(Msg, callBackFunction, 400, 100, null, "Corporate Request Change Queue");
                        e.cancel = true;
                    }
                    else if ((AcknowledgementStatus == "") && (CorporateRequestStatus == "S")) {
                        var Msg = "Warning - The Selected Corporate Request Trip Has Never Been Transferred To The Tripsheet Catalog.  Please Use The Transfer Button To Move Trip Request Over to Tripsheet.";
                        radalert(Msg, 400, 100, "Corporate Request Change Queue");
                        e.cancel = true;
                    }
                    else {
                        __doPostBack('btnMarkAccepted', '');
                    }
                }
                return ReturnValue;
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadScriptManager ID="ScriptManager" runat="server" EnableScriptCombine="false" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ReloadOnShow="true"
        ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="RadFilterClientPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="ClientCodeFilterPopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadTripManagerPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseTripManagerPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/PreFlightMain.aspx">
            </telerik:RadWindow>
        </Windows>
        <Windows>
            <telerik:RadWindow ID="RadCorpRequestPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseCorpRequestPopup" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/CorporateRequest/CorporateRequestMain.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <table class="ocd_log_popup">
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" style="width: 100%;">
                    <tr>
                        <td align="left">
                            <div class="nav-space">
                            </div>
                        </td>
                    </tr>
                </table>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left">
                            <div class="tab-nav-top">
                                <span class="head-title">Corporate Request Change Queue</span>
                            </div>
                        </td>
                    </tr>
                </table>
                <table width="100%" cellpadding="0" cellspacing="0" class="head-sub-menu">
                    <tr>
                        <td>
                            <div class="status-list">
                                <table width="100%">
                                    <tr>
                                        <td class="tdLabel150">
                                            <asp:CheckBox ID="chkHomebase" runat="server" Text="Home Base Only" />
                                        </td>
                                        <td>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <table id="Table1" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="tdLabel90">
                                                                    <asp:Label runat="server" ID="lbClientCode">Client Code :</asp:Label>
                                                                </td>
                                                                <td class="tdLabel100">
                                                                    <asp:TextBox ID="tbClientCodeFilter" runat="server" MaxLength="5" ValidationGroup="Save"
                                                                        AutoPostBack="true" CssClass="text50"></asp:TextBox>
                                                                    <asp:Button ID="btnClientCodeFilter" OnClientClick="javascript:openWin('RadFilterClientPopup');return false;"
                                                                        CssClass="browse-button" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <asp:Label runat="server" ID="lbClientCodeFilter" Text="Invalid ClientCode" Visible="false"
                                                                        CssClass="alert-text"></asp:Label>
                                                                    <asp:CustomValidator ID="cvClientCodeFilter" runat="server" ControlToValidate="tbClientCodeFilter"
                                                                        ErrorMessage="Invalid ClientCode" Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td class="postflight-custom-grid">
                        </td>
                    </tr>
                </table>
                <div id="DivExternalForm" runat="server" class="ExternalForm">
                    <div style="width: 710px; float: left;">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <telerik:RadGrid ID="dgChangeQueueList" runat="server" AllowMultiRowSelection="true"
                                        OnSelectedIndexChanged="dgChangeQueueList_SelectedIndexChanged" OnItemDataBound="dgChangeQueueList_ItemDataBound"
                                        AllowSorting="true" OnNeedDataSource="dgChangeQueueList_BindData" OnItemCommand="dgChangeQueueList_ItemCommand"
                                        AutoGenerateColumns="false" PageSize="10" AllowPaging="true" Width="700px" Height="350px">
                                        <MasterTableView CommandItemDisplay="None" AllowFilteringByColumn="true" DataKeyNames="CRMainID,CRTripNUM,TripStatus,TripNUM,TripSheetStatus,CorporateRequestStatus,AcknowledgementStatus"
                                            ClientDataKeyNames="CRMainID,CRTripNUM,TripStatus,TripNUM,TripSheetStatus,CorporateRequestStatus,AcknowledgementStatus">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="CRTripNUM" HeaderText="CR No." UniqueName="CRTripNUM"
                                                    AutoPostBackOnFilter="true" CurrentFilterFunction="EqualTo" ShowFilterIcon="false"
                                                    HeaderStyle-Width="60px" FilterControlWidth="40px" />
                                                <telerik:GridBoundColumn DataField="EstDepartureDT" HeaderText="Departure Date" UniqueName="EstDepartureDT"
                                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                                    AllowFiltering="false" HeaderStyle-Width="80px" />
                                                <telerik:GridBoundColumn DataField="PassengerRequestorCD" HeaderText="Requestor"
                                                    UniqueName="PassengerRequestorCD" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                                    ShowFilterIcon="false" HeaderStyle-Width="80px" FilterControlWidth="60px" />
                                                <telerik:GridBoundColumn DataField="CRMainDescription" HeaderText="Trip Description"
                                                    UniqueName="CRMainDescription" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                                    ShowFilterIcon="false" HeaderStyle-Width="200px" FilterControlWidth="180px" />
                                                <telerik:GridBoundColumn DataField="CorporateRequestStatus" HeaderText="Status" UniqueName="CorporateRequestStatus"
                                                    ShowFilterIcon="false" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                                    FilterDelay="1000" HeaderStyle-Width="80px" FilterControlWidth="60px" />
                                                <telerik:GridBoundColumn DataField="Approver" HeaderText="Approved" UniqueName="Approver"
                                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                                    HeaderStyle-Width="80px" FilterControlWidth="60px" />
                                                <telerik:GridBoundColumn DataField="RequestDT" HeaderText="Request Date" UniqueName="RequestDT"
                                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                                    HeaderStyle-Width="80px" FilterControlWidth="60px" />
                                                <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail No." UniqueName="TailNum"
                                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                                    HeaderStyle-Width="80px" FilterControlWidth="60px" />
                                                <telerik:GridBoundColumn DataField="AircraftCD" HeaderText="Type Code" UniqueName="AircraftCD"
                                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                                    HeaderStyle-Width="80px" FilterControlWidth="60px" />
                                                <%--  <telerik:GridBoundColumn DataField="AcknowledgementStatus" HeaderText="Ack" UniqueName="AcknowledgementStatus"
                                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false" />--%>
                                                <telerik:GridBoundColumn DataField="TripNUM" HeaderText="Trip No." UniqueName="TripNUM"
                                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                                    HeaderStyle-Width="80px" FilterControlWidth="60px" />
                                                <telerik:GridBoundColumn DataField="TripStatus" HeaderText="Trip Status" UniqueName="TripStatus"
                                                    AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                                    HeaderStyle-Width="80px" FilterControlWidth="60px" />
                                                <telerik:GridBoundColumn DataField="TravelCoordCD" HeaderText="Travel Coordinator"
                                                    UniqueName="TravelCoordCD" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                                    ShowFilterIcon="false" HeaderStyle-Width="80px" FilterControlWidth="60px" />
                                                <telerik:GridBoundColumn DataField="CRMainID" HeaderText="CRMainID" UniqueName="CRMainID"
                                                    Display="false" ShowFilterIcon="false" />
                                            </Columns>
                                            <CommandItemTemplate>
                                                <%--<div style="padding: 5px 5px; float: left; clear: both;">
                                                    <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                                        Visible='<%# IsAuthorized(Permission.Database.AddCrewRoster)%>'><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                                                    <asp:LinkButton ID="lbtnInitEdit" runat="server" ToolTip="Edit" CommandName="Edit"
                                                        Visible='<%# IsAuthorized("Permission.Database.EditCrewRoster")%>'><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="DeleteSelected" ToolTip="Delete"
                                                        Visible='<%# IsAuthorized("Permission.Database.DeleteCrewRoster")%>'><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                                                </div>--%>
                                                <div>
                                                    <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                                                </div>
                                            </CommandItemTemplate>
                                        </MasterTableView>
                                        <ClientSettings EnablePostBackOnRowClick="true">
                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                            <Selecting AllowRowSelect="true" />
                                        </ClientSettings>
                                        <GroupingSettings CaseSensitive="false" />
                                    </telerik:RadGrid>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="tblspace_10">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="tblspace_10">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <telerik:RadPanelBar ID="RadPanelBarItineraryPlan" Width="100%" ExpandAnimation-Type="None"
                                        CollapseAnimation-Type="none" runat="server">
                                        <Items>
                                            <telerik:RadPanelItem runat="server" Expanded="true" Text="Dispatch Messaging" CssClass="PanelHeaderStyle">
                                                <Items>
                                                    <telerik:RadPanelItem>
                                                        <ContentTemplate>
                                                            <div style="float: left; width: 712px; padding: 0px 0 5px 0;">
                                                                <fieldset>
                                                                    <legend>Logistics History:</legend>
                                                                    <asp:TextBox ID="tbLogisticHistory" CssClass="textarea675" TextMode="MultiLine" runat="server"
                                                                        Enabled="false"></asp:TextBox>
                                                                </fieldset>
                                                            </div>
                                                            <div style="float: left; width: 712px; padding: 0px 0 5px 0;">
                                                                <fieldset>
                                                                    <legend>Requestor Notes:</legend>
                                                                    <asp:TextBox ID="tbPaxRequestorNotes" CssClass="textarea675" TextMode="MultiLine"
                                                                        runat="server" Enabled="false"></asp:TextBox>
                                                                </fieldset>
                                                            </div>
                                                            <div style="float: left; width: 712px; padding: 0px 0 5px 0;">
                                                                <fieldset>
                                                                    <legend>Dispatcher Notes:</legend>
                                                                    <asp:TextBox ID="tbDispatchNotes" CssClass="textarea675" TextMode="MultiLine" runat="server"
                                                                        Enabled="false"></asp:TextBox>
                                                                </fieldset>
                                                            </div>
                                                        </ContentTemplate>
                                                    </telerik:RadPanelItem>
                                                </Items>
                                            </telerik:RadPanelItem>
                                        </Items>
                                    </telerik:RadPanelBar>
                                </td>
                            </tr>
                        </table>
                        <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                            <tr align="left">
                                <td>
                                    <asp:Button ID="btnTripManager" Text="Preflight" runat="server" CssClass="button"
                                        OnClientClick="javascript:openWin('RadTripManagerPopup'); return false;" Style="display: none;" />
                                    <asp:Button ID="btnCorpRequest" Text="Corp Request" runat="server" CssClass="button"
                                        OnClientClick="javascript:openWin('RadCorpRequestPopup'); return false;" Style="display: none;" />
                                    <asp:Button ID="btnTransfer" Text="Transfer" runat="server" CssClass="button" OnClick="btnTransfer_OnClick" />
                                    <asp:Button ID="btnDenyRequest" Text="Deny Request" runat="server" CssClass="button"
                                        OnClick="btnDenyRequest_OnClick" />
                                    <asp:Button ID="btnMarkAccepted" Text="Mark Accepted" runat="server" CssClass="button"
                                        OnClick="btnMarkAccepted_OnClick" />
                                    <asp:Button ID="btnApproved" Text="Approved" runat="server" CssClass="button" OnClick="btnApproved_OnClick" />
                                    <asp:Button ID="btnSubmitYes" runat="server" Text="Button" OnClick="btnSubmitYes_Click"
                                        Style="display: none;" />
                                </td>
                            </tr>
                        </table>
                        <%--<table cellspacing="0" cellpadding="0" class="tblButtonArea">
                            <tr align="right">
                                <td>
                                    <asp:Button ID="btnSaveChanges" Text="Save" runat="server" CssClass="button" OnClick="Save_Click"
                                        ValidationGroup="Save" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancel" Text="Cancel" OnClick="Cancel_Click" runat="server" CssClass="button"
                                        CausesValidation="false" />
                                </td>
                            </tr>
                        </table>--%>
                        <%--<table id="tblHidden" style="display: none;">
                            <tr>
                                <td>
                                    <asp:Button ID="btnDeleteNo" runat="server" Text="Button" OnClick="DeleteNo_Click" />
                                    <asp:Button ID="btnDeleteYes" runat="server" Text="Button" OnClick="DeleteYes_Click" />
                                    <asp:Button ID="btnCancelYes" runat="server" Text="Button" OnClick="CancelYes_Click" />
                                    <asp:Button ID="btnCancelNo" runat="server" Text="Button" OnClick="CancelNo_Click" />
                                    <asp:HiddenField ID="hdnTempHoursTxtbox" runat="server" />
                                </td>
                            </tr>
                        </table>--%>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
