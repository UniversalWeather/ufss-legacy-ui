﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.CorporateRequestService;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Transactions.CorporateRequest
{
    public partial class CorporateRequestSearch : BaseSecuredPage
    {
        private ExceptionManager exManager;
        public CorporateRequestService.CRMain CorpRequest = new CorporateRequestService.CRMain();
        public Int64 CRMainID = 0;
        public string Param = string.Empty;
        bool Filtering = false;
        string strFilterVal = string.Empty;

        protected void page_prerender(object sender, EventArgs e)
        {
            if (hdHomeBase.Value.IndexOf(',') > 0)
            {
                lbHomeBase.Text = "(Multiple)";
                lbHomeBase.CssClass = "";
                lbHomeBase.ForeColor = System.Drawing.Color.Red;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (Request.QueryString["FromPage"] != null)
                        {

                            hdnPageRequested.Value = Request.QueryString["FromPage"];

                            if (Request.QueryString["FromPage"].ToString() == "1")
                            {
                                dgCorpRequestRetrieve.AllowMultiRowSelection = true;
                            }
                            else
                            {
                                dgCorpRequestRetrieve.AllowMultiRowSelection = false;
                            }
                        }
                        if (Request.QueryString["ControlName"] != null)
                        {
                            Param = Request.QueryString["ControlName"];
                        }
                        if (!IsPostBack)
                        {
                            if (UserPrincipal.Identity._clientId == null)
                                btnClientCode.Enabled = true;
                            else
                                btnClientCode.Enabled = false;
                            if (UserPrincipal.Identity._clientId != null)
                            {
                                tbSearchClientCode.Text = UserPrincipal.Identity._clientCd;
                                hdClientCodeID.Value = UserPrincipal.Identity._clientId.ToString();
                                FlightPakMasterService.Client client = new FlightPakMasterService.Client();
                                client = GetClient((long)UserPrincipal.Identity._clientId);
                                if (client != null)
                                {
                                    lbClientCode.Text = System.Web.HttpUtility.HtmlEncode(client.ClientDescription);
                                }
                                tbSearchClientCode.ReadOnly = true;
                            }
                            else
                            {
                                tbSearchClientCode.ReadOnly = false;
                            }

                            if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                            {
                                RadDatePicker1.DateInput.DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                                RadDatePicker1.DateInput.DisplayDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                                DatePicker.DateInput.DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                                DatePicker.DateInput.DisplayDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            }

                            if (!IsPostBack)
                            {
                                if (UserPrincipal.Identity._fpSettings._IsDeactivateHomeBaseFilter == false)
                                {
                                    chkHomebase.Checked = true;
                                }
                            }

                            DateTime estDepart = DateTime.Now;
                            if (UserPrincipal.Identity._fpSettings._CRSearchBack != null)
                            {
                                estDepart = estDepart.AddDays(-((double)UserPrincipal.Identity._fpSettings._CRSearchBack));
                            }
                            else
                            {
                                estDepart = estDepart.AddDays(-(30));
                            }


                            tbDepDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", estDepart);
                            lbTravelCoordinator.Text = string.Empty;
                            SetTravelCoordinator();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }



        protected void dgCorpRequestRetrieve_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridFilteringItem)
            {
                GridFilteringItem filteringItem = e.Item as GridFilteringItem;
                //set dimensions for the filter textbox  
                TextBox box = filteringItem["CRTripNUM"].Controls[0] as TextBox;
                box.Width = Unit.Pixel(30);
                TextBox EstDepartureDT = filteringItem["EstDepartureDT"].Controls[0] as TextBox;
                EstDepartureDT.Width = Unit.Pixel(58);
                TextBox Rqstr = filteringItem["Rqstr"].Controls[0] as TextBox;
                Rqstr.Width = Unit.Pixel(47);
                TextBox TripDescription = filteringItem["TripDescription"].Controls[0] as TextBox;
                TripDescription.Width = Unit.Pixel(60);
                TextBox CorporateRequestStatus = filteringItem["CorporateRequestStatus"].Controls[0] as TextBox;
                CorporateRequestStatus.Width = Unit.Pixel(45);
                TextBox RequestDT = filteringItem["RequestDT"].Controls[0] as TextBox;
                RequestDT.Width = Unit.Pixel(45);
                TextBox TailNum = filteringItem["TailNum"].Controls[0] as TextBox;
                TailNum.Width = Unit.Pixel(50);
                TextBox AircraftCD = filteringItem["AircraftCD"].Controls[0] as TextBox;
                AircraftCD.Width = Unit.Pixel(40);
                TextBox TripNUM = filteringItem["TripNUM"].Controls[0] as TextBox;
                TripNUM.Width = Unit.Pixel(40);

            }
        }


        protected void SetTravelCoordinator()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //if (UserPrincipal.Identity._homeBaseId != null)
                //{
                //    using (AdminService.AdminServiceClient objService = new AdminService.AdminServiceClient())
                //    {
                //        var objUserVal = objService.GetUserMasterList();
                //        List<AdminService.GetAllUserMasterResult> lstUserMaster = new List<AdminService.GetAllUserMasterResult>();
                //        if (objUserVal.ReturnFlag == true)
                //        {
                //            lstUserMaster = objUserVal.EntityList.Where(x => x.HomebaseID == UserPrincipal.Identity._homeBaseId && x.CustomerID == UserPrincipal.Identity._customerID && x.UserName.ToLower().Trim() == UserPrincipal.Identity._name.ToLower().Trim()).ToList();
                //            if (lstUserMaster.Count > 0)
                //            {
                //                if (lstUserMaster[0].TravelCoordCD != null)
                //                {
                //                    tbTravelCoordinator.Text = lstUserMaster[0].TravelCoordCD.ToString();
                //                    hdnTravelCoordinatorID.Value = lstUserMaster[0].TravelCoordID.ToString();
                //                    lbTravelCoordinator.Text = System.Web.HttpUtility.HtmlEncode(lstUserMaster[0].FirstName.ToString());
                //                    lbcvTravelCoordinator.Text = string.Empty;
                //                    tbTravelCoordinator.Enabled = false;
                //                    btnTravelCoordinator.Enabled = false;
                //                }
                //            }
                //        }
                //    }
                //}

                string FirstName = "";
                if (UserPrincipal.Identity._travelCoordID != null)
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var query = objService.GetTravelCoordinatorList().EntityList.Where(x => x.TravelCoordinatorID == Convert.ToInt64(UserPrincipal.Identity._travelCoordID)).ToList();
                        if (query != null && query.Count > 0)
                        {
                            FirstName = query[0].FirstName;
                        }
                    }
                }

                if (UserPrincipal.Identity._isSysAdmin == false && UserPrincipal.Identity._travelCoordID != null)
                {
                    tbTravelCoordinator.Text = UserPrincipal.Identity._travelCoordCD.ToString();
                    hdnTravelCoordinatorID.Value = UserPrincipal.Identity._travelCoordID.ToString();
                    lbTravelCoordinator.Text = System.Web.HttpUtility.HtmlEncode(FirstName);
                    lbcvTravelCoordinator.Text = string.Empty;
                    tbTravelCoordinator.Enabled = false;
                    btnTravelCoordinator.Enabled = false;
                }
                else
                {
                    if (UserPrincipal.Identity._travelCoordCD != null)
                    {
                        tbTravelCoordinator.Text = UserPrincipal.Identity._travelCoordCD.ToString();
                    }
                    else
                    {
                        tbTravelCoordinator.Text = string.Empty;
                    }
                    if (UserPrincipal.Identity._travelCoordID != null)
                    {
                        hdnTravelCoordinatorID.Value = UserPrincipal.Identity._travelCoordID.ToString();
                    }
                    else
                    {
                        hdnTravelCoordinatorID.Value = string.Empty;
                    }
                    lbTravelCoordinator.Text = System.Web.HttpUtility.HtmlEncode(FirstName);
                    lbcvTravelCoordinator.Text = string.Empty;
                    tbTravelCoordinator.Enabled = true;
                    btnTravelCoordinator.Enabled = true;
                }
            }
        }

        protected void tbHomeBase_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (!string.IsNullOrEmpty(hdHomeBase.Value) && hdHomeBase.Value.IndexOf(",") > 0)
                        {
                            lbHomeBase.Text = string.Empty;
                        }
                        else
                        {
                            lbHomeBase.Text = string.Empty;
                            lbcvHomeBase.Text = string.Empty;
                            if (!string.IsNullOrEmpty(tbHomeBase.Text))
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objDstsvc.GetCompanyMasterListInfo();
                                    List<FlightPakMasterService.GetAllCompanyMaster> CompanyList = new List<FlightPakMasterService.GetAllCompanyMaster>();
                                    if (objRetVal.ReturnFlag)
                                    {

                                        CompanyList = objRetVal.EntityList.Where(x => x.HomebaseCD != null && x.HomebaseCD.ToString().ToUpper().Trim().Equals(tbHomeBase.Text.ToUpper().Trim())).ToList();
                                        if (CompanyList != null && CompanyList.Count > 0)
                                        {
                                            lbHomeBase.Text = System.Web.HttpUtility.HtmlEncode(CompanyList[0].BaseDescription);
                                            hdHomeBase.Value = CompanyList[0].HomebaseID.ToString();
                                            tbHomeBase.Text = CompanyList[0].HomebaseCD;
                                            lbcvHomeBase.Text = System.Web.HttpUtility.HtmlEncode("");
                                        }
                                        else
                                        {
                                            lbcvHomeBase.Text = System.Web.HttpUtility.HtmlEncode("Homebase Code Does Not Exist");
                                            hdHomeBase.Value = "";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbHomeBase);
                                        }
                                    }
                                    else
                                    {
                                        lbcvHomeBase.Text = System.Web.HttpUtility.HtmlEncode("Homebase Code Does Not Exist");
                                        hdHomeBase.Value = "";
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbHomeBase);
                                    }
                                }
                            }
                            else
                            {
                                lbHomeBase.Text = "";
                                hdHomeBase.Value = "";
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }


        protected void dgCorpRequestRetrieve_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!Filtering)
                        {
                            dgCorpRequestRetrieve.DataSource = DoSearchFilter();
                        }
                        else
                        {
                            List<View_CorporateRequestMainList> lst = DoSearchFilter();
                            if (strFilterVal != "")
                            {
                                List<View_CorporateRequestMainList> chkFilter = (from x in lst where x.CorporateRequestStatus.ToLower() == strFilterVal.ToLower() select x).ToList();
                                dgCorpRequestRetrieve.DataSource = chkFilter;
                            }
                            else
                            {
                                dgCorpRequestRetrieve.DataSource = lst;
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }


        protected void dgCorpRequestRetrieve_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                        {
                            if (e.Item is GridDataItem)
                            {

                                GridDataItem item = (GridDataItem)e.Item;
                                if (item["EstDepartureDT"].Text != "&nbsp;")
                                {
                                    DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "EstDepartureDT");
                                    if (date != null)
                                        item["EstDepartureDT"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", date));
                                }
                                if (item["RequestDT"].Text != "&nbsp;")
                                {
                                    DateTime date1 = (DateTime)DataBinder.Eval(item.DataItem, "RequestDT");
                                    if (date1 != null)
                                        item["RequestDT"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", date1));
                                }




                                //if (item["CorporateRequestStatus"].Text != null && item["CorporateRequestStatus"].Text.ToUpper() == "W")
                                //{
                                //    item["CorporateRequestStatus"].Text = "Worksheet";
                                //}
                                //else if (item["CorporateRequestStatus"].Text != null && item["CorporateRequestStatus"].Text.ToUpper() == "A")
                                //{
                                //    item["CorporateRequestStatus"].Text = "Accepted";
                                //}
                                //else if (item["CorporateRequestStatus"].Text != null && item["CorporateRequestStatus"].Text.ToUpper() == "T")
                                //{
                                //    item["CorporateRequestStatus"].Text = "Accepted";
                                //}
                                //else if (item["CorporateRequestStatus"].Text != null && item["CorporateRequestStatus"].Text.ToUpper() == "D")
                                //{
                                //    item["CorporateRequestStatus"].Text = "Denied";
                                //}
                                //else if (item["CorporateRequestStatus"].Text != null && item["CorporateRequestStatus"].Text.ToUpper() == "C")
                                //{
                                //    item["CorporateRequestStatus"].Text = "Canceled";
                                //}
                                //else if (item["CorporateRequestStatus"].Text != null && item["CorporateRequestStatus"].Text.ToUpper() == "X")
                                //{
                                //    item["CorporateRequestStatus"].Text = "Submitted";
                                //}
                                //else if (item["CorporateRequestStatus"].Text != null && item["CorporateRequestStatus"].Text.ToUpper() == "M")
                                //{
                                //    item["CorporateRequestStatus"].Text = "Modified";
                                //}
                                //else if (item["CorporateRequestStatus"].Text != null && item["CorporateRequestStatus"].Text.ToUpper() == "S")
                                //{
                                //    item["CorporateRequestStatus"].Text = "Submitted";
                                //}



                                //if (item["TripSheetStatus"].Text != null && item["TripSheetStatus"].Text.ToUpper() == "W")
                                //{
                                //    item["TripSheetStatus"].Text = "Worksheet";
                                //}
                                //else if (item["TripSheetStatus"].Text != null && item["TripSheetStatus"].Text.ToUpper() == "T")
                                //{
                                //    item["TripSheetStatus"].Text = "Tripsheet";
                                //}
                                //else if (item["TripSheetStatus"].Text != null && item["TripSheetStatus"].Text.ToUpper() == "C")
                                //{
                                //    item["TripSheetStatus"].Text = "Canceled";
                                //}
                                //else if (item["TripSheetStatus"].Text != null && item["TripSheetStatus"].Text.ToUpper() == "H")
                                //{
                                //    item["TripSheetStatus"].Text = "Hold";
                                //}

                            }

                            if (e.Item is GridCommandItem)
                            {
                                GridCommandItem Item = (GridCommandItem)e.Item;

                                Button Insert = (Button)Item.FindControl("lbtnInitInsert");
                                Button Ok = (Button)Item.FindControl("btnOK");
                                Button Submit = (Button)Item.FindControl("btnSUB");
                                if (Insert != null && Ok != null && Submit != null)
                                {
                                    if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"] == "Move")
                                    {
                                        Insert.Visible = false;
                                        Ok.Visible = true;
                                        Submit.Visible = false;
                                    }
                                    else if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"] == "Report")
                                    {
                                        Insert.Visible = false;
                                        Ok.Visible = false;
                                        Submit.Visible = true;
                                    }
                                    else
                                    {
                                        Insert.Visible = true;
                                        Ok.Visible = false;
                                        Submit.Visible = false;
                                    }
                                }
                            }

                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgCorpRequestRetrieve;

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
            // SelectItem();
        }
        protected void dgCorpRequestRetrieve_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.CommandName == RadGrid.InitInsertCommandName)
                        {
                            e.Canceled = true;
                            _LoadTriptoSession();
                        }
                        else if (e.CommandName == RadGrid.FilterCommandName)
                        {
                            Filtering = true;
                            Pair filterPair = (Pair)e.CommandArgument;
                            if (filterPair.First.ToString() == "Custom")
                            {
                                string colName = filterPair.Second.ToString();
                                TextBox tbPattern = (e.Item as GridFilteringItem)[colName].Controls[0] as TextBox;
                                string[] values = tbPattern.Text.Split(' ');
                                if (values.Length == 1)
                                {
                                    e.Canceled = true;
                                    strFilterVal = values[0].ToString();
                                    dgCorpRequestRetrieve.Rebind();
                                }
                            }
                            Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }


        protected void _LoadTriptoSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgCorpRequestRetrieve.SelectedItems.Count > 0)
                        {
                            GridDataItem item = (GridDataItem)dgCorpRequestRetrieve.SelectedItems[0];
                            CRMainID = Convert.ToInt64(item["CRMainID"].Text);
                            if (dgCorpRequestRetrieve.SelectedItems.Count > 0)
                            {
                                Int64 convTripNum = 0;
                                Int64 Tripnum = 0;
                                if (Int64.TryParse(item["CRTripNUM"].Text, out convTripNum))
                                    Tripnum = convTripNum;

                                if (Session["CurrentCorporateRequest"] != null)
                                {
                                    CRMain CurrCorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                    if (CurrCorpRequest.Mode == CorporateRequestTripActionMode.Edit && CurrCorpRequest.CRMainID != CRMainID)
                                    {

                                        RadWindowManager1.RadConfirm("Request: " + CurrCorpRequest.CRTripNUM.ToString() + "Unsaved, ignore changes and load the selected Trip: " + Tripnum + " ?", "confirmCallBackFn", 330, 100, null, "Confirmation!");
                                    }
                                    else
                                    {
                                        using (CorporateRequestServiceClient CorpSvc = new CorporateRequestServiceClient())
                                        {
                                            var objRetVal = CorpSvc.GetRequest(CRMainID);
                                            if (objRetVal.ReturnFlag)
                                            {
                                                CorpRequest = objRetVal.EntityList[0];

                                                //SetTripModeToNoChange(ref Trip);
                                                CorpRequest.Mode = CorporateRequestTripActionMode.NoChange;
                                                CorpRequest.State = CorporateRequestTripEntityState.NoChange;
                                                Session["CurrentCorporateRequest"] = CorpRequest;

                                                var ObjretVal = CorpSvc.GetRequestExceptionList(CorpRequest.CRMainID);
                                                if (ObjretVal.ReturnFlag)
                                                {
                                                    Session["CorporateRequestException"] = ObjretVal.EntityList;
                                                }
                                                else
                                                    Session.Remove("CorporateRequestException");
                                                CorpRequest.AllowTripToNavigate = true;
                                                CorpRequest.AllowTripToSave = true;
                                                RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    using (CorporateRequestServiceClient CorpSvc = new CorporateRequestServiceClient())
                                    {
                                        var objRetVal = CorpSvc.GetRequest(CRMainID);
                                        if (objRetVal.ReturnFlag)
                                        {
                                            CorpRequest = objRetVal.EntityList[0];
                                            CorpRequest.Mode = CorporateRequestTripActionMode.NoChange;
                                            CorpRequest.State = CorporateRequestTripEntityState.NoChange;
                                            CorpRequest.AllowTripToNavigate = true;
                                            CorpRequest.AllowTripToSave = true;
                                            Session["CurrentCorporateRequest"] = CorpRequest;
                                            var ObjretVal = CorpSvc.GetRequestExceptionList(CorpRequest.CRMainID);
                                            if (ObjretVal.ReturnFlag)
                                            {
                                                Session["CorporateRequestException"] = ObjretVal.EntityList;
                                            }
                                            else
                                                Session.Remove("CorporateRequestException");
                                            RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                                        }
                                    }
                                }

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }


        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
        }
        private FlightPak.Web.FlightPakMasterService.Client GetClient(Int64 ClientID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<FlightPakMasterService.Client> ClientList;
                FlightPak.Web.FlightPakMasterService.Client client = new FlightPakMasterService.Client();
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            client = new FlightPakMasterService.Client();
                            var objRetVal = objDstsvc.GetClientCodeList();
                            ClientList = new List<FlightPakMasterService.Client>();

                            if (objRetVal.ReturnFlag)
                            {

                                ClientList = objRetVal.EntityList.Where(x => x.ClientID == ClientID).ToList();
                                if (ClientList != null && ClientList.Count > 0)
                                {
                                    client = ClientList[0];

                                }
                                else
                                    client = null;

                            }

                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
                return client;
            }

        }
        protected void tbSearchClientCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        btnClientCode.Focus();
                        lbClientCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvClientCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        if (!string.IsNullOrEmpty(tbSearchClientCode.Text))
                        {
                            List<FlightPak.Web.FlightPakMasterService.Client> ClientList = new List<FlightPakMasterService.Client>();
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetClientCodeList();

                                if (objRetVal.ReturnFlag)
                                {
                                    ClientList = objRetVal.EntityList.Where(x => x.ClientCD.ToString().ToUpper().Trim().Equals(tbSearchClientCode.Text.ToUpper().Trim())).ToList();

                                    if (ClientList != null && ClientList.Count > 0)
                                    {
                                        lbClientCode.Text = System.Web.HttpUtility.HtmlEncode(ClientList[0].ClientDescription);
                                        hdClientCodeID.Value = ClientList[0].ClientID.ToString();
                                        lbcvClientCode.Text = System.Web.HttpUtility.HtmlEncode("");
                                    }
                                    else
                                    {
                                        lbcvClientCode.Text = System.Web.HttpUtility.HtmlEncode("Client Code Does Not Exist");
                                        hdClientCodeID.Value = "";
                                        tbSearchClientCode.Focus();
                                    }
                                }
                                else
                                {
                                    lbcvClientCode.Text = System.Web.HttpUtility.HtmlEncode("Client Code Does Not Exist");
                                    hdClientCodeID.Value = "";
                                    tbSearchClientCode.Focus();
                                }

                            }
                        }
                        else
                        {
                            lbClientCode.Text = System.Web.HttpUtility.HtmlEncode("");
                            hdClientCodeID.Value = "";
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }

        protected void RetrieveSearch_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        dgCorpRequestRetrieve.DataSource = DoSearchFilter();
                        dgCorpRequestRetrieve.DataBind();
                        dgCorpRequestRetrieve.Focus();

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }
        public RadDatePicker DatePicker { get { return this.RadDatePicker1; } }
        public List<View_CorporateRequestMainList> DoSearchFilter()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<View_CorporateRequestMainList> crLists = new List<View_CorporateRequestMainList>();
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        Int64 ClientID = 0;
                        Int64 homeBaseID = 0;
                        Int64 travelCoordinator = 0;
                        string homebaseIDCSVStr = string.Empty;
                        string travelCoordinatorIDCSVStr = string.Empty;
                        if (UserPrincipal != null)
                        {
                            homeBaseID = (long)UserPrincipal.Identity._homeBaseId;
                        }

                        if (!string.IsNullOrEmpty(hdHomeBase.Value))
                        {
                            homebaseIDCSVStr = hdHomeBase.Value;
                            chkHomebase.Checked = false;
                        }
                        else
                        {

                        }


                        using (CorporateRequestService.CorporateRequestServiceClient objService = new CorporateRequestService.CorporateRequestServiceClient())
                        {

                            DateTime? estDepart;
                            if (!string.IsNullOrEmpty(tbDepDate.Text))
                                estDepart = DateTime.ParseExact(tbDepDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                            else
                                //    estDepart = DateTime.Now.AddDays(-1000);
                                estDepart = DateTime.MinValue;


                            crLists = new List<View_CorporateRequestMainList>();

                            if (!string.IsNullOrEmpty(hdClientCodeID.Value))
                            {
                                ClientID = Convert.ToInt64(hdClientCodeID.Value);
                            }
                            else
                                ClientID = 0;
                            if (!string.IsNullOrEmpty(hdnTravelCoordinatorID.Value))
                            {
                                travelCoordinator = Convert.ToInt64(hdnTravelCoordinatorID.Value);
                            }
                            else
                                travelCoordinator = 0;


                            bool ChkTripsheet = false;

                            var ObjPrefmainlist = objService.GetAllCorporateRequestMainList(chkHomebase.Checked ? homeBaseID : 0,
                                ClientID,
                             ChkTripsheet ? "T" : null,
                             ChkTripsheet ? "W" : null,
                             ChkTripsheet ? "H" : null,
                             ChkTripsheet ? "X" : null,
                             ChkTripsheet ? "S" : null,
                             ChkTripsheet ? "U" : null,
                             ChkTripsheet ? true : false,
                            (DateTime)estDepart, hdHomeBase.Value,
                            travelCoordinator
                            );

                            if (ObjPrefmainlist.ReturnFlag)
                            {
                                crLists = ObjPrefmainlist.EntityList;
                            }
                            if (crLists != null && crLists.Count > 0)
                            {
                                foreach (View_CorporateRequestMainList List in crLists)
                                {
                                    if (List.CorporateRequestStatus != null && List.CorporateRequestStatus.ToUpper() == "W")
                                    {
                                        List.CorporateRequestStatus = "Worksheet";
                                    }
                                    else if (List.CorporateRequestStatus != null && List.CorporateRequestStatus.ToUpper() == "A")
                                    {
                                        List.CorporateRequestStatus = "Accepted";
                                    }
                                    else if (List.CorporateRequestStatus != null && List.CorporateRequestStatus.ToUpper() == "T")
                                    {
                                        List.CorporateRequestStatus = "Accepted";
                                    }
                                    else if (List.CorporateRequestStatus != null && List.CorporateRequestStatus.ToUpper() == "D")
                                    {
                                        List.CorporateRequestStatus = "Denied";
                                    }
                                    else if (List.CorporateRequestStatus != null && List.CorporateRequestStatus.ToUpper() == "C")
                                    {
                                        List.CorporateRequestStatus = "Canceled";
                                    }
                                    else if (List.CorporateRequestStatus != null && List.CorporateRequestStatus.ToUpper() == "X")
                                    {
                                        List.CorporateRequestStatus = "Submitted";
                                    }
                                    else if (List.CorporateRequestStatus != null && List.CorporateRequestStatus.ToUpper() == "M")
                                    {
                                        List.CorporateRequestStatus = "Modified";
                                    }
                                    else if (List.CorporateRequestStatus != null && List.CorporateRequestStatus.ToUpper() == "S")
                                    {
                                        List.CorporateRequestStatus = "Submitted";
                                    }



                                    if (List.TripSheetStatus != null && List.TripSheetStatus.ToUpper() == "W")
                                    {
                                        List.TripSheetStatus = "Worksheet";
                                    }
                                    else if (List.TripSheetStatus != null && List.TripSheetStatus.ToUpper() == "T")
                                    {
                                        List.TripSheetStatus = "Tripsheet";
                                    }
                                    else if (List.TripSheetStatus != null && List.TripSheetStatus.ToUpper() == "C")
                                    {
                                        List.TripSheetStatus = "Canceled";
                                    }
                                    else if (List.TripSheetStatus != null && List.TripSheetStatus.ToUpper() == "H")
                                    {
                                        List.TripSheetStatus = "Hold";
                                    }
                                }
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
                return crLists;
            }

        }

        protected void tbTravelCoordinator_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        RadAjaxManager.GetCurrent(Page).FocusControl(btnTravelCoordinator);
                        lbcvTravelCoordinator.Text = string.Empty;
                        hdnTravelCoordinatorID.Value = string.Empty;
                        if (!string.IsNullOrEmpty(tbTravelCoordinator.Text))
                        {
                            List<FlightPakMasterService.GetTravelCoordinator> TravelCoordinatorList = new List<FlightPakMasterService.GetTravelCoordinator>();
                            using (FlightPakMasterService.MasterCatalogServiceClient TravelCoordinatorService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = TravelCoordinatorService.GetTravelCoordinatorList();
                                if (objRetVal.ReturnFlag)
                                {

                                    TravelCoordinatorList = objRetVal.EntityList.Where(x => x.TravelCoordCD.ToUpper().Trim() == tbTravelCoordinator.Text.ToUpper().Trim()).ToList();

                                    if (TravelCoordinatorList != null && TravelCoordinatorList.Count > 0)
                                    {
                                        lbTravelCoordinator.Text = System.Web.HttpUtility.HtmlEncode(TravelCoordinatorList[0].FirstName);
                                        hdnTravelCoordinatorID.Value = TravelCoordinatorList[0].TravelCoordinatorID.ToString();
                                        lbcvTravelCoordinator.Text = "";
                                    }
                                    else
                                    {
                                        lbcvTravelCoordinator.Text = System.Web.HttpUtility.HtmlEncode("Travel Coordinator Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbTravelCoordinator);
                                    }
                                }
                                else
                                {
                                    lbcvTravelCoordinator.Text = System.Web.HttpUtility.HtmlEncode("Travel Coordinator Does Not Exist");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbTravelCoordinator);
                                }
                            }
                        }
                        else
                        {
                            lbTravelCoordinator.Text = "";
                            hdnTravelCoordinatorID.Value = "";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }

        }


        protected void Yes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridDataItem item = (GridDataItem)dgCorpRequestRetrieve.SelectedItems[0];
                        CRMainID = Convert.ToInt64(item["CRMainID"].Text);

                        Int64 convTripNum = 0;
                        Int64 Tripnum = 0;
                        if (Int64.TryParse(item["CRTripNUM"].Text, out convTripNum))
                            Tripnum = convTripNum;


                        using (CorporateRequestServiceClient CorpSvc = new CorporateRequestServiceClient())
                        {
                            var objRetVal = CorpSvc.GetRequest(CRMainID);
                            if (objRetVal.ReturnFlag)
                            {
                                CorpRequest = objRetVal.EntityList[0];


                                CorpRequest.Mode = CorporateRequestTripActionMode.NoChange;
                                CorpRequest.State = CorporateRequestTripEntityState.NoChange;
                                Session["CurrentCorporateRequest"] = CorpRequest;
                                var ObjretVal = CorpSvc.GetRequestExceptionList(CorpRequest.CRMainID);
                                if (ObjretVal.ReturnFlag)
                                {
                                    Session["CorporateRequestException"] = ObjretVal.EntityList;
                                }
                                else
                                    Session.Remove("CorporateRequestException");
                                CorpRequest.AllowTripToNavigate = true;
                                CorpRequest.AllowTripToSave = true;
                                RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }

        protected void MetroCity_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgCorpRequestRetrieve.ClientSettings.Scrolling.ScrollTop = "0";
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Metro);
                }
            }

        }


        protected void No_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager1.ResponseScripts.Add(@"CloseRadWindow('CloseWindow');");

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }

        protected void dgCorpRequestRetrieve_PreRender(object sender, EventArgs e)
        {
            RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCorpRequestRetrieve, 
                Page.Session);
        }
    }
}
