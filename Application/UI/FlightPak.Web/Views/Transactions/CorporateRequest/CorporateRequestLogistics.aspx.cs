﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//For Tracing and Exception Handling
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.CorporateRequestService;
using Telerik.Web.UI;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Collections;
using System.Globalization;
using System.Text;

namespace FlightPak.Web.Views.Transactions.CorporateRequest
{
    public partial class CorporateRequestLogistics : BaseSecuredPage
    {
        string DateFormat;
        public CRMain CorpRequest = new CRMain();
        private ExceptionManager exManager;
        private bool selectchangefired = false;
        private delegate void SaveSession();

        private string ModuleNameCRLogistics = ModuleNameConstants.CorporateRequest.CorporateRequestLogistics;
        private string PermissionViewCRLogistics = Permission.CorporateRequest.ViewCRLogistics;
        private string PermissionAddCRLogistics = Permission.CorporateRequest.AddCRLogistics;
        private string PermissionEditCRLogistics = Permission.CorporateRequest.EditCRLogistics;
        private string PermissionDeleteCRLogistics = Permission.CorporateRequest.DeleteCRLogistics;

        /// <summary>
        /// Wire up the SaveToCorpRequestSession to the event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveSession SaveToSaveCorpRequestSession = new SaveSession(SaveToCorpRequestSession);
                        Master.SaveCRToSession = SaveToSaveCorpRequestSession;
                        Master.RadAjax_AjaxRequest += RadAjaxManager1_AjaxRequest;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }
        }

        /// <summary>
        /// show alert after ack req
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReload_Click(object sender, EventArgs e)
        {
            Master.RadAjaxManagerMaster.ResponseScripts.Add(@"window.location=window.location.href.split('?')[0];");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(Master.MasterForm.FindControl("DivMasterForm"), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(DivExternalForm, DivExternalForm, RadAjaxLoadingPanel1);

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rtsLegs, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbEstimatedGallons, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepartFBOCode, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbArriveFBOCode, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepartCaterCode, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbArriveCaterCode, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDeleteTrip, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnEditTrip, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancel, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(rtsLegs, DivExternalForm, RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbEstimatedGallons, DivExternalForm, RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(radlstAmt, DivExternalForm, RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepartFBOCode, DivExternalForm, RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbArriveFBOCode, DivExternalForm, RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepartCaterCode, DivExternalForm, RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbArriveCaterCode, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDeleteTrip, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnEditTrip, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancel, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, DivExternalForm, RadAjaxLoadingPanel1);

                        //Replicate Save Cancel Delete Buttons in header
                        Master.SaveClick += btnSave_Click;
                        Master.CancelClick += btnCancel_Click;
                        Master.DeleteClick += btnDelete_Click;

                        if (!IsPostBack)
                        {
                            //ajss - commented
                            ApplyPermissions();
                            //ajse   - commented
                            hdnLegNum.Value = "1";

                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                CheckCRLegExist(CorpRequest);
                                BindCRTabs(CorpRequest, "pageload");

                                rtsLegs.Tabs[Convert.ToInt16(hdnLegNum.Value) - 1].Selected = true;

                                if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                                {
                                    List<CRLeg> Corplegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                                    foreach (CRLeg crleginfo in Corplegs.ToList())
                                    {
                                        if (crleginfo.LegNUM == Convert.ToInt16(hdnLegNum.Value))
                                        {
                                            LoadLegDetails(CorpRequest.CRLegs[Convert.ToInt16(rtsLegs.Tabs[Convert.ToInt16(hdnLegNum.Value) - 1].Value)]);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                        }
                        //ajss - commented
                        SetCompanyProfileSetting();
                        //ajse   - commented
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }
        }

        protected void page_prerender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                            #region Changes for Legnum in CRHeader
                            Master.FloatLegNum.Visible = true;
                            #endregion

                            if (CorpRequest.Mode == CorporateRequestTripActionMode.Edit)
                            {
                                btnSubmitReq.Enabled = false;
                                btnCancellationReq.Enabled = false;
                                btnAcknowledge.Enabled = false;
                                btnSave.Enabled = true;
                                btnCancel.Enabled = true;
                                btnDeleteTrip.Enabled = false;
                                btnEditTrip.Enabled = false;

                                btnSave.CssClass = "button";
                                btnCancel.CssClass = "button";
                                btnDeleteTrip.CssClass = "button-disable";
                                btnEditTrip.CssClass = "button-disable";
                                btnSubmitReq.CssClass = "button-disable";
                                btnCancellationReq.CssClass = "button-disable";
                                btnAcknowledge.CssClass = "button-disable";
                                EnableForm(true);
                            }
                            else
                            {
                                btnSave.Enabled = false;
                                btnCancel.Enabled = false;
                                btnDeleteTrip.Enabled = true;
                                btnEditTrip.Enabled = true;

                                btnSave.CssClass = "button-disable";
                                btnCancel.CssClass = "button-disable";
                                btnDeleteTrip.CssClass = "button";
                                btnEditTrip.CssClass = "button";

                                EnableForm(false);
                            }
                        }
                        else
                        {
                            btnSubmitReq.Enabled = false;
                            btnCancellationReq.Enabled = false;
                            btnAcknowledge.Enabled = false;
                            btnSave.Enabled = false;
                            btnCancel.Enabled = false;
                            btnDeleteTrip.Enabled = false;
                            btnNext.Enabled = false;
                            btnEditTrip.Enabled = false;

                            btnSubmitReq.CssClass = "button-disable";
                            btnCancellationReq.CssClass = "button-disable";
                            btnAcknowledge.CssClass = "button-disable";
                            btnSave.CssClass = "button-disable";
                            btnCancel.CssClass = "button-disable";
                            btnDeleteTrip.CssClass = "button-disable";
                            btnEditTrip.CssClass = "button-disable";
                            btnNext.CssClass = "button-disable";

                            EnableForm(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }

        }

        /// <summary>
        /// Load data from company profile settings
        /// </summary>
        private void SetCompanyProfileSetting()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if ((UserPrincipal.Identity != null) && (UserPrincipal.Identity._fpSettings != null))
                {
                    if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                    {
                        DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                    }
                    else
                    {
                        DateFormat = "MM/dd/yyyy";
                    }
                    if (UserPrincipal.Identity._fpSettings._IsCorpReqAllowLogUpdFBOInfo != null)
                    {
                        pnlDepartFBO.Enabled = UserPrincipal.Identity._fpSettings._IsCorpReqAllowLogUpdFBOInfo;
                        pnlArriveFBO.Enabled = UserPrincipal.Identity._fpSettings._IsCorpReqAllowLogUpdFBOInfo;
                    }
                    if (UserPrincipal.Identity._fpSettings._IsCorpReqAllowLogUpdPaxInfo != null)
                    {
                        pnlPaxHotel.Enabled = UserPrincipal.Identity._fpSettings._IsCorpReqAllowLogUpdPaxInfo;
                        pnlPaxTransport.Enabled = UserPrincipal.Identity._fpSettings._IsCorpReqAllowLogUpdPaxInfo;
                    }
                    if (UserPrincipal.Identity._fpSettings._IsCorpReqAllowLogUpdCrewInfo != null)
                    {
                        pnlCrewHotel.Enabled = UserPrincipal.Identity._fpSettings._IsCorpReqAllowLogUpdCrewInfo;
                        pnlCrewTransport.Enabled = UserPrincipal.Identity._fpSettings._IsCorpReqAllowLogUpdCrewInfo;
                        pnlMaintHotel.Enabled = UserPrincipal.Identity._fpSettings._IsCorpReqAllowLogUpdCrewInfo;
                    }
                    if (UserPrincipal.Identity._fpSettings._IsCorpReqAllowLogUpdCateringInfo != null)
                    {
                        pnlDeptCatering.Enabled = UserPrincipal.Identity._fpSettings._IsCorpReqAllowLogUpdCateringInfo;
                        pnlArriveCatering.Enabled = UserPrincipal.Identity._fpSettings._IsCorpReqAllowLogUpdCateringInfo;
                    }
                }
            }
        }

        private void CheckCRLegExist(CRMain CorpRequest)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string RedirectTo = string.Empty;
                if (CorpRequest == null)
                {
                    RedirectTo = "CorporateRequestMain.aspx";
                }
                else if (CorpRequest.CRLegs == null)
                {
                    RedirectTo = "CorporateRequestLegs.aspx";
                }
                else if (CorpRequest.CRLegs.Count == 0)
                {
                    RedirectTo = "CorporateRequestLegs.aspx";
                }
                else if (CorpRequest.CRLegs.Count > 0)
                {
                    for (int Index = 0; Index < CorpRequest.CRLegs.Count; Index++)
                    {
                        if ((CorpRequest.CRLegs[Index].DAirportID == null) || (CorpRequest.CRLegs[Index].AAirportID == null))
                        {
                            RedirectTo = "CorporateRequestLegs.aspx";
                            break;
                        }
                    }
                }
                if (RedirectTo != string.Empty)
                {
                    Response.Redirect(RedirectTo);
                    return;
                }
            }
        }

        private void LoadDataFromLeg(CRLeg Leg)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Leg))
            {

                if (Leg != null)
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                    CRLeg oCRLeg = new CRLeg();
                    oCRLeg = Leg;

                    if ((oCRLeg.ElapseTM != null) && (oCRLeg.ElapseTM.Value != null))
                    {
                        //if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        //{
                        tbElapsedTime.Text = "00:00";
                        tbElapsedTime.Text = ConvertTenthsToMins(oCRLeg.ElapseTM.Value.ToString());
                        //}
                        //else
                        //{
                        //    tbElapsedTime.Text = "0.0";
                        //    tbElapsedTime.Text = Math.Round(oCRLeg.ElapseTM.Value, 1).ToString();
                        //    if (tbElapsedTime.Text.IndexOf(".") < 0)
                        //        tbElapsedTime.Text = tbElapsedTime.Text + ".0";
                        //}
                        //tbElapsedTime.Text = String.Format("{0:" + "##:##" + "}", oCRLeg.ElapseTM.Value.ToString());     //oCRLeg.ElapseTM.Value.ToString();
                    }
                    else
                    {
                        //if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        //{
                        tbElapsedTime.Text = "00:00";
                        //}
                        //else
                        //{
                        //    tbElapsedTime.Text = "0.0";
                        //}
                        //tbElapsedTime.Text = String.Format("{0:" + "##:##" + "}", "00:00");     //string.Empty;
                    }
                    if ((oCRLeg.Distance != null) && (oCRLeg.Distance.Value != null))
                    {
                        tbDistance.Text = oCRLeg.Distance.Value.ToString();
                    }
                    else
                    {
                        tbDistance.Text = string.Empty;
                    }
                    if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat != null && UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 2)
                    {
                        if ((oCRLeg.DepartureDTTMLocal != null) && (oCRLeg.DepartureDTTMLocal.Value != null))
                        {
                            lbDepartDate.Text = System.Web.HttpUtility.HtmlEncode(Convert12To24Hrs(oCRLeg.DepartureDTTMLocal.Value.ToString()));
                        }
                        else
                        {
                            lbDepartDate.Text = string.Empty;
                        }
                        if ((oCRLeg.ArrivalDTTMLocal != null) && (oCRLeg.ArrivalDTTMLocal.Value != null))
                        {
                            lbArriveDate.Text = System.Web.HttpUtility.HtmlEncode(Convert12To24Hrs(oCRLeg.ArrivalDTTMLocal.Value.ToString()));
                        }
                        else
                        {
                            lbArriveDate.Text = string.Empty;
                        }
                    }
                    else //if (UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat != null && UserPrincipal.Identity._fpSettings._CorpReqTMEntryFormat == 1)
                    {
                        if ((oCRLeg.DepartureDTTMLocal != null) && (oCRLeg.DepartureDTTMLocal.Value != null))
                        {
                            //string tmDepart = oCRLeg.DepartureDTTMLocal.ToString().Substring(10, oCRLeg.DepartureDTTMLocal.ToString().Length - 10);
                            //tmDepart = tmDepart.Substring(1, tmDepart.LastIndexOf(':') - 1) + ' ' + tmDepart.Substring(10, 2);
                            // lbDepartDate.Text = string.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", oCRLeg.DepartureDTTMLocal.Value.ToString());
                            lbDepartDate.Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + " HH:mm tt}", oCRLeg.DepartureDTTMLocal));// +tmDepart;
                        }
                        else
                        {
                            lbDepartDate.Text = string.Empty;
                        }
                        if ((oCRLeg.ArrivalDTTMLocal != null) && (oCRLeg.ArrivalDTTMLocal.Value != null))
                        {
                            //string tmArrive = oCRLeg.ArrivalDTTMLocal.ToString().Substring(10, oCRLeg.ArrivalDTTMLocal.ToString().Length - 10);
                            //tmArrive = tmArrive.Substring(1, tmArrive.LastIndexOf(':') - 1) + ' ' + tmArrive.Substring(10, 2);
                            // lbArriveDate.Text = string.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", oCRLeg.ArrivalDTTMLocal.Value.ToString());
                            lbArriveDate.Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "  HH:mm tt}", oCRLeg.ArrivalDTTMLocal));// +tmArrive;
                        }
                        else
                        {
                            lbArriveDate.Text = string.Empty;
                        }
                    }

                    if ((oCRLeg.DAirportID != null) && (oCRLeg.DAirportID.Value != null))
                    {
                        hdnDepartAirportID.Value = oCRLeg.DAirportID.Value.ToString();
                        GetAirportData(lbDepartICAO, hdnDepartAirportID);
                    }
                    else
                    {
                        hdnDepartAirportID.Value = string.Empty;
                        GetAirportData(lbDepartICAO, hdnDepartAirportID);
                    }
                    if ((oCRLeg.AAirportID != null) && (oCRLeg.AAirportID.Value != null))
                    {
                        hdnArriveAirportID.Value = oCRLeg.AAirportID.Value.ToString();
                        GetAirportData(lbArriveICAO, hdnArriveAirportID);
                    }
                    else
                    {
                        hdnArriveAirportID.Value = string.Empty;
                        GetAirportData(lbArriveICAO, hdnArriveAirportID);
                    }
                }
            }
        }

        protected void LoadLegDetails(CRLeg crLeg)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crLeg))
            {

                ClearFields();

                //CRchoice(crLeg);

                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                if (CorpRequest != null)
                {
                    //hdnLegNum.Value = crLeg.LegNUM.ToString();
                    // code for getting depart and arrive airport id from leg
                    if (crLeg.DAirportID != null)
                    {
                        hdDepartIcaoID.Value = crLeg.DAirportID.Value.ToString();
                        hdnDepartAirportID.Value = crLeg.DAirportID.Value.ToString();
                    }

                    if (crLeg.AAirportID != null)
                    {
                        hdArriveIcaoID.Value = crLeg.AAirportID.Value.ToString();
                        hdnArriveAirportID.Value = crLeg.AAirportID.Value.ToString();
                    }

                    #region Changes for Legnum in CRHeader
                    Master.FloatLegNum.Text = System.Web.HttpUtility.HtmlEncode(crLeg.LegNUM.ToString());
                    #endregion

                    LoadDataFromLeg(crLeg);

                    // crLeg = CorpRequest.CRLegs.Where(x => x.LegNUM == Convert.ToInt16(lbLeg.Text) && x.IsDeleted == false).FirstOrDefault();

                    if (crLeg != null)
                    {
                        //hdnLegNum.Value = crLeg.LegNUM.ToString();

                        CRchoice(crLeg);
                        SetTransferFBO(Convert.ToInt32(hdnLegNum.Value) - 1);

                        if (crLeg.CRFBOLists != null && crLeg.CRFBOLists.Count > 0)
                        {
                            foreach (CRFBOList crfbo in crLeg.CRFBOLists)
                            {
                                UpdateScreenDetailsFromCRFBO(crfbo);
                            }
                        }

                        if (crLeg.CRCateringLists != null && crLeg.CRCateringLists.Count > 0)
                        {
                            foreach (CRCateringList crcatering in crLeg.CRCateringLists)
                            {
                                UpdateScreenDetailsFromCRCatering(crcatering);
                            }
                        }

                        if (crLeg.CRTransportLists != null && crLeg.CRTransportLists.Count > 0)
                        {
                            foreach (CRTransportList crtransport in crLeg.CRTransportLists)
                            {
                                UpdateScreenDetailsFromCRTransport(crtransport);
                            }
                        }

                        if (crLeg.CRHotelLists != null && crLeg.CRHotelLists.Count > 0)
                        {
                            foreach (CRHotelList crhotel in crLeg.CRHotelLists)
                            {
                                UpdateScreenDetailsFromCRHotel(crhotel);
                            }
                        }

                        if (crLeg.Notes != null)
                        {
                            tbPaxNotes.Text = crLeg.Notes.ToString();
                        }
                        else
                        {
                            tbPaxNotes.Text = string.Empty;
                        }
                    }
                }

                // BindCRTabs(CorpRequest, "pageload");
            }
        }

        private void GetTransferFBO()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                ArrayList TransferFBO = new ArrayList();
                if (!string.IsNullOrEmpty(tbArriveFBO.Text))
                {
                    TransferFBO.Add(tbArriveFBO.Text);
                }
                Session["TransferFBO"] = TransferFBO;
            }
        }
        private void SetTransferFBO(Int32 LegIndex)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(LegIndex))
            {
                if (LegIndex > 0)
                {
                    if (Session["CurrentCorporateRequest"] != null)
                    {
                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                        if (CorpRequest.CRLegs[LegIndex - 1].AAirportID == CorpRequest.CRLegs[LegIndex].DAirportID)
                        {
                            if ((CorpRequest.CRLegs[LegIndex - 1].CRFBOLists != null) && (CorpRequest.CRLegs[LegIndex - 1].CRFBOLists.Count != 0))
                            {
                                for (int Index = 0; Index < CorpRequest.CRLegs[LegIndex - 1].CRFBOLists.Count; Index++)
                                {
                                    if (CorpRequest.CRLegs[LegIndex - 1].CRFBOLists[Index].RecordType.Trim() == "A")
                                    {
                                        ArrayList TransferFBO = new ArrayList();
                                        TransferFBO = (ArrayList)Session["TransferFBO"];
                                        if (TransferFBO.Count != 0)
                                        {
                                            if (TransferFBO[0] != null)
                                            {
                                                tbDepartFBO.Text = TransferFBO[0].ToString();
                                                DepartFBO_TextChanged(tbDepartFBO, EventArgs.Empty);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        #region MasterCatalog Popup text box change events

        private bool GetAirportData(Label lbAirportICAOID, HiddenField HdnFld)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lbAirportICAOID, HdnFld))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    bool ReturnValue = true;
                    if (!string.IsNullOrEmpty(HdnFld.Value))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objMasterService.GetAirportByAirportID(Convert.ToInt64(HdnFld.Value)).EntityList;
                            if (objRetVal.Count != 0)
                            {
                                List<FlightPakMasterService.GetAllAirport> AirportList = new List<FlightPakMasterService.GetAllAirport>();
                                AirportList = (List<FlightPakMasterService.GetAllAirport>)objRetVal.ToList();
                                HdnFld.Value = AirportList[0].AirportID.ToString();
                                lbAirportICAOID.Text = System.Web.HttpUtility.HtmlEncode(AirportList[0].IcaoID.ToString());
                                string builder = string.Empty;
                                builder = "ICAO Id : " + (AirportList[0].IcaoID != null ? AirportList[0].IcaoID : string.Empty)
                                    + "\n" + "City : " + (AirportList[0].CityName != null ? AirportList[0].CityName : string.Empty)
                                    + "\n" + "State/Province : " + (AirportList[0].StateName != null ? AirportList[0].StateName : string.Empty)
                                    + "\n" + "Country : " + (AirportList[0].CountryName != null ? AirportList[0].CountryName : string.Empty)
                                    + "\n" + "Airport : " + (AirportList[0].AirportName != null ? AirportList[0].AirportName : string.Empty)
                                    + "\n" + "DST Region : " + (AirportList[0].DSTRegionCD != null ? AirportList[0].DSTRegionCD : string.Empty)
                                    + "\n" + "UTC+/- : " + (AirportList[0].OffsetToGMT != null ? AirportList[0].OffsetToGMT.ToString() : string.Empty)
                                    + "\n" + "Longest Runway : " + (AirportList[0].LongestRunway != null ? AirportList[0].LongestRunway.ToString() : string.Empty)
                                    + "\n" + "IATA : " + (AirportList[0].Iata != null ? AirportList[0].Iata.ToString() : string.Empty);
                                lbAirportICAOID.ToolTip = builder;
                                ReturnValue = true;
                            }
                            else
                            {
                                HdnFld.Value = string.Empty;
                                lbAirportICAOID.Text = string.Empty;
                                ReturnValue = false;
                            }
                        }
                    }
                    return ReturnValue;
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// To check unique Depart FBO Code exists or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DepartFBO_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnDepartFBO);
                        tbDepartFBOName.Text = string.Empty;
                        tbDepartFBOPhone.Text = string.Empty;
                        tbDepartFBOFax.Text = string.Empty;
                        tbDepartFBOEmail.Text = string.Empty;
                        lbcvDepartFBOCode.Text = string.Empty;
                        hdDepartFBOID.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbDepartFBO.Text))
                        {
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(hdDepartIcaoID.Value))
                                {
                                    var ServCall = objDstsvc.GetAllFBOByAirportID(Convert.ToInt64(hdDepartIcaoID.Value));
                                    if (ServCall.ReturnFlag)
                                    {

                                        var objRetVal = ServCall.EntityList.Where(x => x.FBOCD == tbDepartFBO.Text.Trim()).ToList();

                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            // RadWindowManager1.RadConfirm("Do you want to use the current Arrival FBO to update the next leg's Departure FBO?", "confirmNextFBOCallBackFn", 330, 100, null, "Confirmation!");
                                            if (!string.IsNullOrEmpty(objRetVal[0].FBOCD))
                                                tbDepartFBO.Text = objRetVal[0].FBOCD.ToString();
                                            hdDepartFBOID.Value = objRetVal[0].FBOID.ToString();

                                            if (objRetVal[0].FBOVendor != null && !string.IsNullOrEmpty(objRetVal[0].FBOVendor))
                                                tbDepartFBOName.Text = objRetVal[0].FBOVendor.ToString();
                                            if (objRetVal[0].PhoneNUM1 != null && !string.IsNullOrEmpty(objRetVal[0].PhoneNUM1))
                                                tbDepartFBOPhone.Text = objRetVal[0].PhoneNUM1.ToString();
                                            if (objRetVal[0].FaxNum != null && !string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                tbDepartFBOFax.Text = objRetVal[0].FaxNum.ToString();
                                            if (objRetVal[0].ContactEmail != null && !string.IsNullOrEmpty(objRetVal[0].ContactEmail))
                                                tbDepartFBOEmail.Text = objRetVal[0].ContactEmail.ToString();
                                        }
                                        else
                                        {
                                            lbcvDepartFBOCode.Text = System.Web.HttpUtility.HtmlEncode("FBO/Handler Code Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartFBO);
                                        }
                                    }
                                    else
                                    {
                                        lbcvDepartFBOCode.Text = System.Web.HttpUtility.HtmlEncode("FBO/Handler Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartFBO);
                                    }
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }
        }

        /// <summary>
        /// To check unique Arrive FBO Code exists or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ArriveFBO_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnArriveFBO);
                        tbArriveFBOName.Text = string.Empty;
                        tbArriveFBOPhone.Text = string.Empty;
                        tbArriveFBOFax.Text = string.Empty;
                        tbArriveFBOEmail.Text = string.Empty;
                        lbcvArriveFBOCode.Text = string.Empty;
                        hdArriveFBOID.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbArriveFBO.Text))
                        {
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                                {
                                    var ServCall = objDstsvc.GetAllFBOByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                                    if (ServCall.ReturnFlag)
                                    {
                                        var objRetVal = ServCall.EntityList.Where(x => x.FBOCD == tbArriveFBO.Text.Trim()).ToList();

                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            // RadWindowManager1.RadConfirm("Do you want to update the next leg’s Arriveure FBO?", "confirmNextFBOCallBackFn", 330, 100, null, "Confirmation!");
                                            if (!string.IsNullOrEmpty(objRetVal[0].FBOCD))
                                                tbArriveFBO.Text = objRetVal[0].FBOCD.ToString();
                                            hdArriveFBOID.Value = objRetVal[0].FBOID.ToString();

                                            if (objRetVal[0].FBOVendor != null && !string.IsNullOrEmpty(objRetVal[0].FBOVendor))
                                                tbArriveFBOName.Text = objRetVal[0].FBOVendor.ToString();
                                            if (objRetVal[0].PhoneNUM1 != null && !string.IsNullOrEmpty(objRetVal[0].PhoneNUM1))
                                                tbArriveFBOPhone.Text = objRetVal[0].PhoneNUM1.ToString();
                                            if (objRetVal[0].FaxNum != null && !string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                tbArriveFBOFax.Text = objRetVal[0].FaxNum.ToString();
                                            if (objRetVal[0].ContactEmail != null && !string.IsNullOrEmpty(objRetVal[0].ContactEmail))
                                                tbArriveFBOEmail.Text = objRetVal[0].ContactEmail.ToString();
                                        }
                                        else
                                        {
                                            lbcvArriveFBOCode.Text = System.Web.HttpUtility.HtmlEncode("FBO/Handler Code Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveFBO);
                                        }
                                    }
                                    else
                                    {
                                        lbcvArriveFBOCode.Text = System.Web.HttpUtility.HtmlEncode("FBO/Handler Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveFBO);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }
        }

        /// <summary>
        /// To check unique Maintainence Catering Code exists or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeptCatering_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnDeptCateringCode);
                        tbDeptCateringName.Text = string.Empty;
                        tbDeptCateringPhone.Text = string.Empty;
                        tbDeptCateringFax.Text = string.Empty;
                        tbDeptCateringRate.Text = string.Empty;
                        tbDeptCateringEmail.Text = string.Empty;
                        lbcvDeptCateringCode.Text = string.Empty;
                        hdDeptCateringID.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbDeptCateringCode.Text))
                        {
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(hdDepartIcaoID.Value))
                                {
                                    var ServCall = objDstsvc.GetAllCateringByAirportID(Convert.ToInt64(hdDepartIcaoID.Value));
                                    if (ServCall.ReturnFlag)
                                    {
                                        var objRetVal = ServCall.EntityList.Where(x => x.CateringCD == tbDeptCateringCode.Text.Trim()).ToList();

                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            if (objRetVal[0].CateringCD != null && !string.IsNullOrEmpty(objRetVal[0].CateringCD))
                                                tbDeptCateringCode.Text = objRetVal[0].CateringCD.ToString();
                                            hdDeptCateringID.Value = objRetVal[0].CateringID.ToString();

                                            if (objRetVal[0].CateringVendor != null && !string.IsNullOrEmpty(objRetVal[0].CateringVendor))
                                                tbDeptCateringName.Text = objRetVal[0].CateringVendor.ToString();
                                            if (objRetVal[0].PhoneNum != null && !string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                                tbDeptCateringPhone.Text = objRetVal[0].PhoneNum.ToString();
                                            if (objRetVal[0].FaxNum != null && !string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                tbDeptCateringFax.Text = objRetVal[0].FaxNum.ToString();
                                            if (objRetVal[0].ContactEmail != null && !string.IsNullOrEmpty(objRetVal[0].ContactEmail))
                                                tbDeptCateringEmail.Text = objRetVal[0].ContactEmail.ToString();
                                            if (objRetVal[0].NegotiatedRate != null)
                                                tbDeptCateringRate.Text = objRetVal[0].NegotiatedRate.ToString();
                                            else
                                                tbDeptCateringRate.Text = "0.00";
                                        }
                                        else
                                        {
                                            lbcvDeptCateringCode.Text = System.Web.HttpUtility.HtmlEncode("Catering Code Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbDeptCateringCode);
                                        }
                                    }
                                    else
                                    {
                                        lbcvArriveFBOCode.Text = System.Web.HttpUtility.HtmlEncode("Catering Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbDeptCateringCode);
                                    }
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }
        }

        protected void ArriveCatering_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnArriveCateringCode);
                        tbArriveCateringName.Text = string.Empty;
                        tbArriveCateringPhone.Text = string.Empty;
                        tbArriveCateringFax.Text = string.Empty;
                        tbArriveCateringRate.Text = string.Empty;
                        tbArriveCateringEmail.Text = string.Empty;
                        lbcvArriveCateringCode.Text = string.Empty;
                        hdArriveCateringID.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbArriveCateringCode.Text))
                        {
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                                {
                                    var ServCall = objDstsvc.GetAllCateringByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                                    if (ServCall.ReturnFlag)
                                    {
                                        var objRetVal = ServCall.EntityList.Where(x => x.CateringCD == tbArriveCateringCode.Text.Trim()).ToList();

                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            if (objRetVal[0].CateringCD != null && !string.IsNullOrEmpty(objRetVal[0].CateringCD))
                                                tbArriveCateringCode.Text = objRetVal[0].CateringCD.ToString();
                                            hdArriveCateringID.Value = objRetVal[0].CateringID.ToString();

                                            if (objRetVal[0].CateringVendor != null && !string.IsNullOrEmpty(objRetVal[0].CateringVendor))
                                                tbArriveCateringName.Text = objRetVal[0].CateringVendor.ToString();
                                            if (objRetVal[0].PhoneNum != null && !string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                                tbArriveCateringPhone.Text = objRetVal[0].PhoneNum.ToString();
                                            if (objRetVal[0].FaxNum != null && !string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                tbArriveCateringFax.Text = objRetVal[0].FaxNum.ToString();
                                            if (objRetVal[0].ContactEmail != null && !string.IsNullOrEmpty(objRetVal[0].ContactEmail))
                                                tbArriveCateringEmail.Text = objRetVal[0].ContactEmail.ToString();
                                            if (objRetVal[0].NegotiatedRate != null)
                                                tbArriveCateringRate.Text = objRetVal[0].NegotiatedRate.ToString();
                                            else
                                                tbArriveCateringRate.Text = "0.00";
                                        }
                                        else
                                        {
                                            lbcvArriveCateringCode.Text = System.Web.HttpUtility.HtmlEncode("Catering Code Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveCateringCode);
                                        }
                                    }
                                    else
                                    {
                                        lbcvArriveFBOCode.Text = System.Web.HttpUtility.HtmlEncode("Catering Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbArriveCateringCode);
                                    }
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }
        }

        /// <summary>
        /// To check unique Pax Transport Code exists or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PaxTransportCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnPaxTransportCode);
                        tbPaxTransportName.Text = string.Empty;
                        tbPaxTransportPhone.Text = string.Empty;
                        tbPaxTransportFax.Text = string.Empty;
                        tbPaxTransportRate.Text = string.Empty;
                        tbPaxTransportEmail.Text = string.Empty;
                        lbcvPaxTransportCode.Text = string.Empty;
                        hdPaxTransportID.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbPaxTransportCode.Text))
                        {
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                                {
                                    var ServCall = objDstsvc.GetTransportByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                                    if (ServCall.ReturnFlag)
                                    {
                                        var objRetVal = ServCall.EntityList.Where(x => x.TransportCD == tbPaxTransportCode.Text.Trim()).ToList();

                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            if (objRetVal[0].TransportCD != null && !string.IsNullOrEmpty(objRetVal[0].TransportCD))
                                                tbPaxTransportCode.Text = objRetVal[0].TransportCD.ToString();
                                            hdPaxTransportID.Value = objRetVal[0].TransportID.ToString();

                                            if (objRetVal[0].TransportationVendor != null && !string.IsNullOrEmpty(objRetVal[0].TransportationVendor))
                                                tbPaxTransportName.Text = objRetVal[0].TransportationVendor.ToString();
                                            if (objRetVal[0].PhoneNum != null && !string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                                tbPaxTransportPhone.Text = objRetVal[0].PhoneNum.ToString();
                                            if (objRetVal[0].FaxNum != null && !string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                tbPaxTransportFax.Text = objRetVal[0].FaxNum.ToString();
                                            if (objRetVal[0].ContactEmail != null && !string.IsNullOrEmpty(objRetVal[0].ContactEmail))
                                                tbPaxTransportEmail.Text = objRetVal[0].ContactEmail.ToString();
                                            if (objRetVal[0].NegotiatedRate != null)
                                                tbPaxTransportRate.Text = objRetVal[0].NegotiatedRate.ToString();
                                            else
                                                tbPaxTransportRate.Text = "0.00";
                                        }
                                        else
                                        {
                                            lbcvPaxTransportCode.Text = System.Web.HttpUtility.HtmlEncode("Transport Code Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbPaxTransportCode);
                                        }
                                    }
                                    else
                                    {
                                        lbcvPaxTransportCode.Text = System.Web.HttpUtility.HtmlEncode("Transport Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbPaxTransportCode);
                                    }
                                }
                            }
                        }
                        else
                        {
                            hdPaxTransportID.Value = "";
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }
        }

        /// <summary>
        /// To check unique Crew Transport Code exists or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewTransportCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnCrewTransportCode);
                        tbCrewTransportName.Text = string.Empty;
                        tbCrewTransportPhone.Text = string.Empty;
                        tbCrewTransportFax.Text = string.Empty;
                        tbCrewTransportRate.Text = string.Empty;
                        tbCrewTransportEmail.Text = string.Empty;
                        lbcvCrewTransportCode.Text = string.Empty;
                        hdCrewTransportID.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbCrewTransportCode.Text))
                        {
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                                {
                                    var ServCall = objDstsvc.GetTransportByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                                    if (ServCall.ReturnFlag)
                                    {
                                        var objRetVal = ServCall.EntityList.Where(x => x.TransportCD == tbCrewTransportCode.Text.Trim()).ToList();

                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            if (objRetVal[0].TransportCD != null && !string.IsNullOrEmpty(objRetVal[0].TransportCD))
                                                tbCrewTransportCode.Text = objRetVal[0].TransportCD.ToString();
                                            hdCrewTransportID.Value = objRetVal[0].TransportID.ToString();

                                            if (objRetVal[0].TransportationVendor != null && !string.IsNullOrEmpty(objRetVal[0].TransportationVendor))
                                                tbCrewTransportName.Text = objRetVal[0].TransportationVendor.ToString();
                                            if (objRetVal[0].PhoneNum != null && !string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                                tbCrewTransportPhone.Text = objRetVal[0].PhoneNum.ToString();
                                            if (objRetVal[0].FaxNum != null && !string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                tbCrewTransportFax.Text = objRetVal[0].FaxNum.ToString();
                                            if (objRetVal[0].ContactEmail != null && !string.IsNullOrEmpty(objRetVal[0].ContactEmail))
                                                tbCrewTransportEmail.Text = objRetVal[0].ContactEmail.ToString();
                                            if (objRetVal[0].NegotiatedRate != null)
                                                tbCrewTransportRate.Text = objRetVal[0].NegotiatedRate.ToString();
                                            else
                                                tbCrewTransportRate.Text = "0.00";
                                        }
                                        else
                                        {
                                            lbcvCrewTransportCode.Text = System.Web.HttpUtility.HtmlEncode("Transport Code Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCrewTransportCode);
                                        }
                                    }
                                    else
                                    {
                                        lbcvCrewTransportCode.Text = System.Web.HttpUtility.HtmlEncode("Transport Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbCrewTransportCode);
                                    }
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }
        }

        /// <summary>
        /// To check unique Pax Hotel Code exists or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PaxHotelCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnPaxHotelCode);
                        tbPaxHotelName.Text = string.Empty;
                        tbPaxHotelPhone.Text = string.Empty;
                        tbPaxHotelFax.Text = string.Empty;
                        tbPaxHotelRate.Text = string.Empty;
                        tbPaxHotelEmail.Text = string.Empty;
                        lbcvPaxHotelCode.Text = string.Empty;
                        hdPaxHotelID.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbPaxHotelCode.Text))
                        {
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                                {
                                    var ServCall = objDstsvc.GetHotelsByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                                    if (ServCall.ReturnFlag)
                                    {
                                        var objRetVal = ServCall.EntityList.Where(x => x.HotelCD == tbPaxHotelCode.Text.Trim()).ToList();

                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            if (objRetVal[0].HotelCD != null && !string.IsNullOrEmpty(objRetVal[0].HotelCD))
                                                tbPaxHotelCode.Text = objRetVal[0].HotelCD.ToString();
                                            hdPaxHotelID.Value = objRetVal[0].HotelID.ToString();

                                            if (objRetVal[0].Name != null && !string.IsNullOrEmpty(objRetVal[0].Name))
                                                tbPaxHotelName.Text = objRetVal[0].Name.ToString();
                                            if (objRetVal[0].PhoneNum != null && !string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                                tbPaxHotelPhone.Text = objRetVal[0].PhoneNum.ToString();
                                            if (objRetVal[0].FaxNum != null && !string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                tbPaxHotelFax.Text = objRetVal[0].FaxNum.ToString();
                                            if (objRetVal[0].ContactEmail != null && !string.IsNullOrEmpty(objRetVal[0].ContactEmail))
                                                tbPaxHotelEmail.Text = objRetVal[0].ContactEmail.ToString();
                                            if (objRetVal[0].NegociatedRate != null)
                                                tbPaxHotelRate.Text = objRetVal[0].NegociatedRate.ToString();
                                            else
                                                tbPaxHotelRate.Text = "0.00";
                                        }
                                        else
                                        {
                                            lbcvPaxHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbPaxHotelCode);
                                        }
                                    }
                                    else
                                    {
                                        lbcvPaxHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbPaxHotelCode);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }
        }

        /// <summary>
        /// To check unique Crew Hotel Code exists or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CrewHotelCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnCrewHotelCode);
                        tbCrewHotelName.Text = string.Empty;
                        tbCrewHotelPhone.Text = string.Empty;
                        tbCrewHotelFax.Text = string.Empty;
                        tbCrewHotelRate.Text = string.Empty;
                        tbCrewHotelEmail.Text = string.Empty;
                        lbcvCrewHotelCode.Text = string.Empty;
                        hdCrewHotelID.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbCrewHotelCode.Text))
                        {
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                                {
                                    var ServCall = objDstsvc.GetHotelsByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                                    if (ServCall.ReturnFlag)
                                    {
                                        var objRetVal = ServCall.EntityList.Where(x => x.HotelCD == tbCrewHotelCode.Text.Trim()).ToList();

                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            if (objRetVal[0].HotelCD != null && !string.IsNullOrEmpty(objRetVal[0].HotelCD))
                                                tbCrewHotelCode.Text = objRetVal[0].HotelCD.ToString();
                                            hdCrewHotelID.Value = objRetVal[0].HotelID.ToString();

                                            if (objRetVal[0].Name != null && !string.IsNullOrEmpty(objRetVal[0].Name))
                                                tbCrewHotelName.Text = objRetVal[0].Name.ToString();
                                            if (objRetVal[0].PhoneNum != null && !string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                                tbCrewHotelPhone.Text = objRetVal[0].PhoneNum.ToString();
                                            if (objRetVal[0].FaxNum != null && !string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                tbCrewHotelFax.Text = objRetVal[0].FaxNum.ToString();
                                            if (objRetVal[0].ContactEmail != null && !string.IsNullOrEmpty(objRetVal[0].ContactEmail))
                                                tbCrewHotelEmail.Text = objRetVal[0].ContactEmail.ToString();
                                            if (objRetVal[0].NegociatedRate != null)
                                                tbCrewHotelRate.Text = objRetVal[0].NegociatedRate.ToString();
                                            else
                                                tbCrewHotelRate.Text = "0.00";
                                        }
                                        else
                                        {
                                            lbcvCrewHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbCrewHotelCode);
                                        }
                                    }
                                    else
                                    {
                                        lbcvCrewHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbCrewHotelCode);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }
        }

        /// <summary>
        /// To check unique Maint Hotel Code exists or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void MaintHotelCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnMaintHotelCode);
                        tbMaintHotelName.Text = string.Empty;
                        tbMaintHotelPhone.Text = string.Empty;
                        tbMaintHotelFax.Text = string.Empty;
                        tbMaintHotelRate.Text = string.Empty;
                        tbMaintHotelEmail.Text = string.Empty;
                        lbcvMaintHotelCode.Text = string.Empty;
                        hdMaintHotelID.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbMaintHotelCode.Text))
                        {
                            using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                                {
                                    var ServCall = objDstsvc.GetHotelsByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                                    if (ServCall.ReturnFlag)
                                    {
                                        var objRetVal = ServCall.EntityList.Where(x => x.HotelCD == tbMaintHotelCode.Text.Trim()).ToList();

                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            if (objRetVal[0].HotelCD != null && !string.IsNullOrEmpty(objRetVal[0].HotelCD))
                                                tbMaintHotelCode.Text = objRetVal[0].HotelCD.ToString();
                                            hdMaintHotelID.Value = objRetVal[0].HotelID.ToString();

                                            if (objRetVal[0].Name != null && !string.IsNullOrEmpty(objRetVal[0].Name))
                                                tbMaintHotelName.Text = objRetVal[0].Name.ToString();
                                            if (objRetVal[0].PhoneNum != null && !string.IsNullOrEmpty(objRetVal[0].PhoneNum))
                                                tbMaintHotelPhone.Text = objRetVal[0].PhoneNum.ToString();
                                            if (objRetVal[0].FaxNum != null && !string.IsNullOrEmpty(objRetVal[0].FaxNum))
                                                tbMaintHotelFax.Text = objRetVal[0].FaxNum.ToString();
                                            if (objRetVal[0].ContactEmail != null && !string.IsNullOrEmpty(objRetVal[0].ContactEmail))
                                                tbMaintHotelEmail.Text = objRetVal[0].ContactEmail.ToString();
                                            if (objRetVal[0].NegociatedRate != null)
                                                tbMaintHotelRate.Text = objRetVal[0].NegociatedRate.ToString();
                                            else
                                                tbMaintHotelRate.Text = "0.00";
                                        }
                                        else
                                        {
                                            lbcvMaintHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbMaintHotelCode);
                                        }
                                    }
                                    else
                                    {
                                        lbcvMaintHotelCode.Text = System.Web.HttpUtility.HtmlEncode("Hotel Code Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbMaintHotelCode);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }
        }

        #endregion

        #region "Commented dgLeg Grid events"
        //protected void Legs_BindData(object sender, GridNeedDataSourceEventArgs e)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                //ajss - commented
        //                //Master.BindLegs(dgLegs, false);
        //                //ajse - commented
        //            }, FlightPak.Common.Constants.Policy.UILayer);
        //        }
        //        catch (Exception ex)
        //        {
        //            ProcessErrorMessage(ex, ModuleNameCRLogistics);
        //        }
        //    }
        //}

        //protected void Legs_ItemDataBound(object sender, GridItemEventArgs e)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                if (dgLegs.MasterTableView.Items.Count > 0)
        //                {
        //                    dgLegs.MasterTableView.Items[0].Selected = true;

        //                    GridDataItem dataItem = (GridDataItem)dgLegs.SelectedItems[0];
        //                    if (dataItem.GetDataKeyValue("DepartAirportID") != null)
        //                        hdDepartIcaoID.Value = dataItem.GetDataKeyValue("DepartAirportID").ToString();
        //                    if (dataItem.GetDataKeyValue("ArrivalAirportID") != null)
        //                        hdArriveIcaoID.Value = dataItem.GetDataKeyValue("ArrivalAirportID").ToString();
        //                }
        //            }, FlightPak.Common.Constants.Policy.UILayer);
        //        }
        //        catch (Exception ex)
        //        {
        //            ProcessErrorMessage(ex, ModuleNameCRLogistics);
        //        }
        //    }
        //}

        //protected void Legs_ItemCommand(object sender, GridCommandEventArgs e)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                switch (e.CommandName)
        //                {
        //                    case RadGrid.InitInsertCommandName:
        //                        e.Canceled = true;
        //                        break;

        //                    case RadGrid.PerformInsertCommandName:
        //                        e.Canceled = true;
        //                        break;

        //                    case RadGrid.DeleteSelectedCommandName:
        //                        break;

        //                    case "RowClick":
        //                        GridDataItem Item = (GridDataItem)dgLegs.SelectedItems[0];
        //                        hdDepartIcaoID.Value = Item.GetDataKeyValue("DepartAirportID").ToString();
        //                        hdArriveIcaoID.Value = Item.GetDataKeyValue("ArrivalAirportID").ToString();
        //                        if (!selectchangefired)
        //                        {
        //                            SaveToCorpRequestSession();
        //                        }

        //                        break;

        //                    default:
        //                        break;

        //                }

        //            }, FlightPak.Common.Constants.Policy.UILayer);
        //        }
        //        catch (Exception ex)
        //        {
        //            //The exception will be handled, logged and replaced by our custom exception. 
        //            ProcessErrorMessage(ex, ModuleNameCRLogistics);
        //        }
        //    }
        //}

        //protected void Legs_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
        //    {
        //        try
        //        {
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                SaveToCorpRequestSession();
        //                LoadLegDetails();
        //                selectchangefired = true;
        //            }, FlightPak.Common.Constants.Policy.UILayer);
        //        }
        //        catch (Exception ex)
        //        {
        //            ProcessErrorMessage(ex, ModuleNameCRLogistics);
        //        }
        //    }
        //}
        #endregion

        private void ClearFields()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbDepartFBO.Text = string.Empty;
                tbDepartFBOEmail.Text = string.Empty;
                tbDepartFBOFax.Text = string.Empty;
                tbDepartFBOName.Text = string.Empty;
                tbDepartFBOPhone.Text = string.Empty;

                tbArriveFBO.Text = string.Empty;
                tbArriveFBOEmail.Text = string.Empty;
                tbArriveFBOFax.Text = string.Empty;
                tbArriveFBOName.Text = string.Empty;
                tbArriveFBOPhone.Text = string.Empty;

                tbCrewHotelCode.Text = string.Empty;
                tbCrewHotelName.Text = string.Empty;
                tbCrewHotelPhone.Text = string.Empty;
                tbCrewHotelRate.Text = string.Empty;
                tbCrewHotelFax.Text = string.Empty;
                tbCrewHotelEmail.Text = string.Empty;

                tbCrewTransportCode.Text = string.Empty;
                tbCrewTransportName.Text = string.Empty;
                tbCrewTransportPhone.Text = string.Empty;
                tbCrewTransportRate.Text = string.Empty;
                tbCrewTransportFax.Text = string.Empty;
                tbCrewTransportEmail.Text = string.Empty;

                tbPaxHotelCode.Text = string.Empty;
                tbPaxHotelName.Text = string.Empty;
                tbPaxHotelPhone.Text = string.Empty;
                tbPaxHotelRate.Text = string.Empty;
                tbPaxHotelFax.Text = string.Empty;
                tbPaxHotelEmail.Text = string.Empty;

                tbPaxTransportCode.Text = string.Empty;
                tbPaxTransportName.Text = string.Empty;
                tbPaxTransportPhone.Text = string.Empty;
                tbPaxTransportRate.Text = string.Empty;
                tbPaxTransportFax.Text = string.Empty;
                tbPaxTransportEmail.Text = string.Empty;

                tbPaxNotes.Text = string.Empty;

                tbMaintHotelCode.Text = string.Empty;
                tbMaintHotelName.Text = string.Empty;
                tbMaintHotelPhone.Text = string.Empty;
                tbMaintHotelRate.Text = string.Empty;
                tbMaintHotelFax.Text = string.Empty;
                tbMaintHotelEmail.Text = string.Empty;

                tbDeptCateringCode.Text = string.Empty;
                tbDeptCateringFax.Text = string.Empty;
                tbDeptCateringName.Text = string.Empty;
                tbDeptCateringPhone.Text = string.Empty;
                tbDeptCateringRate.Text = string.Empty;
                tbDeptCateringFax.Text = string.Empty;
                tbDeptCateringEmail.Text = string.Empty;

                tbArriveCateringCode.Text = string.Empty;
                tbArriveCateringFax.Text = string.Empty;
                tbArriveCateringName.Text = string.Empty;
                tbArriveCateringPhone.Text = string.Empty;
                tbArriveCateringRate.Text = string.Empty;
                tbArriveCateringFax.Text = string.Empty;
                tbArriveCateringEmail.Text = string.Empty;

                // Clear Label Validation Messages
                lbcvArriveFBOCode.Text = string.Empty;
                lbcvCrewHotelCode.Text = string.Empty;
                lbcvCrewTransportCode.Text = string.Empty;
                lbcvDepartFBOCode.Text = string.Empty;
                lbcvDeptCateringCode.Text = string.Empty;
                lbcvArriveCateringCode.Text = string.Empty;
                lbcvMaintHotelCode.Text = string.Empty;
                lbcvPaxHotelCode.Text = string.Empty;
                lbcvPaxTransportCode.Text = string.Empty;
            }
        }

        protected void SaveLogistics(CRLeg crLeg)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crLeg))
            {
                if (Session["CurrentCorporateRequest"] != null)
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                    List<CRLeg> Corplegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                    foreach (CRLeg crleginfo in Corplegs.ToList())
                    {
                        if (crleginfo.LegNUM == crLeg.LegNUM)
                        {

                            #region "Depart FBO and Arrive FBO"

                            CRFBOList crDepartFBOList = null;
                            CRFBOList crArriveFBOList = null;

                            if (crLeg.CRFBOLists != null && crLeg.CRFBOLists.Count > 0)
                                crDepartFBOList = crLeg.CRFBOLists.Where(x => x.IsDeleted == false && x.RecordType.Trim() == "D").FirstOrDefault();

                            if (crLeg.CRFBOLists != null && crLeg.CRFBOLists.Count > 0)
                                crArriveFBOList = crLeg.CRFBOLists.Where(x => x.IsDeleted == false && x.RecordType.Trim() == "A").FirstOrDefault();

                            if (crDepartFBOList == null)
                            {
                                if (IsAuthorized(PermissionAddCRLogistics))
                                {
                                    if (!string.IsNullOrEmpty(tbDepartFBO.Text))
                                    {
                                        CRFBOList departFBOList = new CRFBOList();
                                        departFBOList.State = CorporateRequestTripEntityState.Added;
                                        departFBOList.RecordType = "D";
                                        departFBOList.CRLegID = crLeg.CRLegID;
                                        UpdateScreenDetailstoCRFBO(ref departFBOList, departFBOList.RecordType.ToString());
                                        if (crLeg.CRFBOLists == null)
                                            crLeg.CRFBOLists = new List<CRFBOList>();
                                        crLeg.CRFBOLists.Add(departFBOList);
                                    }
                                }
                            }
                            else
                            {
                                if (IsAuthorized(PermissionEditCRLogistics))
                                {
                                    // CRFBOList departFBOList = new CRFBOList();
                                    if (crDepartFBOList.State != CorporateRequestTripEntityState.Deleted && crDepartFBOList.State != CorporateRequestTripEntityState.Added)
                                    {
                                        if (crDepartFBOList.CRFBOListID != 0)
                                        {
                                            crDepartFBOList.State = CorporateRequestTripEntityState.Modified;
                                        }
                                        else
                                        {
                                            crDepartFBOList.State = CorporateRequestTripEntityState.Added;
                                        }
                                    }
                                    crDepartFBOList.CRLegID = crLeg.CRLegID;
                                    UpdateScreenDetailstoCRFBO(ref crDepartFBOList, crDepartFBOList.RecordType.ToString());
                                    //  crLeg.CRFBOLists[0] = departFBOList;
                                }
                            }

                            if (crArriveFBOList == null)
                            {
                                if (IsAuthorized(PermissionAddCRLogistics))
                                {
                                    if (!string.IsNullOrEmpty(tbArriveFBO.Text))
                                    {
                                        CRFBOList arriveFBOList = new CRFBOList();
                                        arriveFBOList.State = CorporateRequestTripEntityState.Added;
                                        arriveFBOList.RecordType = "A";
                                        arriveFBOList.CRLegID = crLeg.CRLegID;
                                        UpdateScreenDetailstoCRFBO(ref arriveFBOList, arriveFBOList.RecordType.ToString());
                                        if (crLeg.CRFBOLists == null)
                                            crLeg.CRFBOLists = new List<CRFBOList>();
                                        crLeg.CRFBOLists.Add(arriveFBOList);
                                    }
                                }
                            }
                            else
                            {
                                if (IsAuthorized(PermissionEditCRLogistics))
                                {
                                    //CRFBOList arriveFBOList = new CRFBOList();
                                    if (crArriveFBOList.State != CorporateRequestTripEntityState.Deleted && crArriveFBOList.State != CorporateRequestTripEntityState.Added)
                                    {
                                        if (crArriveFBOList.CRFBOListID != 0)
                                        {
                                            crArriveFBOList.State = CorporateRequestTripEntityState.Modified;
                                        }
                                        else
                                        {
                                            crArriveFBOList.State = CorporateRequestTripEntityState.Added;
                                        }
                                    }
                                    //crArriveFBOList.State = CorporateRequestTripEntityState.Modified;
                                    crArriveFBOList.CRLegID = crLeg.CRLegID;
                                    UpdateScreenDetailstoCRFBO(ref crArriveFBOList, crArriveFBOList.RecordType.ToString());
                                    // crLeg.CRFBOLists[1] = arriveFBOList;
                                }
                            }

                            #endregion

                            #region "Depart Catering"

                            CRCateringList crCateringList = null;

                            if (crLeg.CRCateringLists != null && crLeg.CRCateringLists.Count > 0)
                                crCateringList = crLeg.CRCateringLists.Where(x => x.IsDeleted == false && x.RecordType.Trim() == "D").FirstOrDefault();

                            if (crCateringList == null)
                            {
                                if (IsAuthorized(PermissionAddCRLogistics))
                                {
                                    if (!string.IsNullOrEmpty(tbDeptCateringName.Text))
                                    {
                                        CRCateringList departCatering = new CRCateringList();
                                        departCatering.RecordType = "D";
                                        departCatering.State = CorporateRequestTripEntityState.Added;
                                        departCatering.CRLegID = crLeg.CRLegID;
                                        UpdateScreenDetailstoCRCatering(ref departCatering);
                                        if (crLeg.CRCateringLists == null)
                                            crLeg.CRCateringLists = new List<CRCateringList>();
                                        crLeg.CRCateringLists.Add(departCatering);
                                    }
                                }
                            }
                            else
                            {
                                if (IsAuthorized(PermissionEditCRLogistics))
                                {
                                    //CRCateringList departCatering = new CRCateringList();
                                    crCateringList.RecordType = "D";
                                    if (crCateringList.State != CorporateRequestTripEntityState.Added || crCateringList.State != CorporateRequestTripEntityState.Deleted)
                                    {
                                        if (crCateringList.CRCateringListID != 0)
                                        {
                                            crCateringList.State = CorporateRequestTripEntityState.Modified;
                                        }
                                        else
                                        {
                                            crCateringList.State = CorporateRequestTripEntityState.Added;
                                        }
                                    }
                                    //crCateringList.State = CorporateRequestTripEntityState.Modified;
                                    crCateringList.CRLegID = crLeg.CRLegID;
                                    UpdateScreenDetailstoCRCatering(ref crCateringList);
                                    //crLeg.CRCateringLists[0] = departCatering;
                                }
                            }

                            #endregion

                            #region "Arrive Catering"

                            CRCateringList crArriveCateringList = null;

                            if (crLeg.CRCateringLists != null && crLeg.CRCateringLists.Count > 0)
                                crArriveCateringList = crLeg.CRCateringLists.Where(x => x.IsDeleted == false && x.RecordType.Trim() == "A").FirstOrDefault();

                            if (crArriveCateringList == null)
                            {
                                if (IsAuthorized(PermissionAddCRLogistics))
                                {
                                    if (!string.IsNullOrEmpty(tbArriveCateringName.Text))
                                    {
                                        CRCateringList ArriveCatering = new CRCateringList();
                                        ArriveCatering.RecordType = "A";
                                        ArriveCatering.State = CorporateRequestTripEntityState.Added;
                                        ArriveCatering.CRLegID = crLeg.CRLegID;
                                        UpdateScreenDetailstoCRCatering(ref ArriveCatering);
                                        if (crLeg.CRCateringLists == null)
                                            crLeg.CRCateringLists = new List<CRCateringList>();
                                        crLeg.CRCateringLists.Add(ArriveCatering);
                                    }
                                }
                            }
                            else
                            {
                                if (IsAuthorized(PermissionEditCRLogistics))
                                {
                                    //CRCateringList ArriveCatering = new CRCateringList();
                                    crArriveCateringList.RecordType = "A";
                                    if (crArriveCateringList.State != CorporateRequestTripEntityState.Added || crArriveCateringList.State != CorporateRequestTripEntityState.Deleted)
                                    {
                                        if (crArriveCateringList.CRCateringListID != 0)
                                        {
                                            crArriveCateringList.State = CorporateRequestTripEntityState.Modified;
                                        }
                                        else
                                        {
                                            crArriveCateringList.State = CorporateRequestTripEntityState.Added;
                                        }
                                    }
                                    //   crArriveCateringList.State = CorporateRequestTripEntityState.Modified;
                                    crArriveCateringList.CRLegID = crLeg.CRLegID;
                                    UpdateScreenDetailstoCRCatering(ref crArriveCateringList);
                                    //crLeg.CRCateringLists[0] = ArriveCatering;
                                }
                            }

                            #endregion

                            #region "Pax Transport and Crew Transport"

                            CRTransportList crPaxTransportList = null;
                            CRTransportList crCrewTransportList = null;

                            if (crLeg.CRTransportLists != null && crLeg.CRTransportLists.Count > 0)
                                crPaxTransportList = crLeg.CRTransportLists.Where(x => x.IsDeleted == false && x.RecordType.Trim() == "P").FirstOrDefault();

                            if (crLeg.CRTransportLists != null && crLeg.CRTransportLists.Count > 0)
                                crCrewTransportList = crLeg.CRTransportLists.Where(x => x.IsDeleted == false && x.RecordType.Trim() == "C").FirstOrDefault();

                            if (crPaxTransportList == null)
                            {
                                if (IsAuthorized(PermissionAddCRLogistics))
                                {
                                    if (!string.IsNullOrEmpty(tbPaxTransportName.Text))
                                    {
                                        CRTransportList paxTransportList = new CRTransportList();
                                        paxTransportList.State = CorporateRequestTripEntityState.Added;
                                        paxTransportList.RecordType = "P";
                                        paxTransportList.CRLegID = crLeg.CRLegID;
                                        UpdateScreenDetailstoCRTransport(ref paxTransportList, paxTransportList.RecordType);
                                        if (crLeg.CRTransportLists == null)
                                            crLeg.CRTransportLists = new List<CRTransportList>();
                                        crLeg.CRTransportLists.Add(paxTransportList);
                                    }
                                }
                            }
                            else
                            {
                                if (IsAuthorized(PermissionEditCRLogistics))
                                {
                                    //CRTransportList paxTransportList = new CRTransportList();
                                    if (crPaxTransportList.State != CorporateRequestTripEntityState.Added && crPaxTransportList.State != CorporateRequestTripEntityState.Deleted)
                                    {
                                        if (crPaxTransportList.CRTransportListID != 0)
                                        {
                                            crPaxTransportList.State = CorporateRequestTripEntityState.Modified;
                                        }
                                        else
                                        {
                                            crPaxTransportList.State = CorporateRequestTripEntityState.Added;
                                        }
                                    }
                                    //crPaxTransportList.State = CorporateRequestTripEntityState.Modified;
                                    crPaxTransportList.CRLegID = crLeg.CRLegID;
                                    UpdateScreenDetailstoCRTransport(ref crPaxTransportList, crPaxTransportList.RecordType);
                                    //crLeg.CRTransportLists[0] = paxTransportList;
                                }
                            }

                            if (crCrewTransportList == null)
                            {
                                if (IsAuthorized(PermissionAddCRLogistics))
                                {
                                    if (!string.IsNullOrEmpty(tbCrewTransportName.Text))
                                    {
                                        CRTransportList crewTransportList = new CRTransportList();
                                        crewTransportList.State = CorporateRequestTripEntityState.Added;
                                        crewTransportList.RecordType = "C";
                                        crewTransportList.CRLegID = crLeg.CRLegID;
                                        UpdateScreenDetailstoCRTransport(ref crewTransportList, crewTransportList.RecordType);
                                        crLeg.CRTransportLists.Add(crewTransportList);
                                    }
                                }
                            }
                            else
                            {
                                if (IsAuthorized(PermissionEditCRLogistics))
                                {
                                    //CRTransportList crewTransportList = new CRTransportList();
                                    if (crCrewTransportList.State != CorporateRequestTripEntityState.Added && crCrewTransportList.State != CorporateRequestTripEntityState.Deleted)
                                    {
                                        if (crCrewTransportList.CRTransportListID != 0)
                                        {
                                            crCrewTransportList.State = CorporateRequestTripEntityState.Modified;
                                        }
                                        else
                                        {
                                            crCrewTransportList.State = CorporateRequestTripEntityState.Added;
                                        }
                                    }
                                    //crCrewTransportList.State = CorporateRequestTripEntityState.Modified;
                                    crCrewTransportList.CRLegID = crLeg.CRLegID;
                                    UpdateScreenDetailstoCRTransport(ref crCrewTransportList, crCrewTransportList.RecordType);
                                    //crLeg.CRTransportLists[1] = crewTransportList;
                                }
                            }

                            #endregion

                            #region "Pax Hotel , Crew Hotel and Maintenance Hotel"

                            CRHotelList crPaxHotelList = null;
                            CRHotelList crCrewHotelList = null;
                            CRHotelList crMaintHotelList = null;

                            if (crLeg.CRHotelLists != null && crLeg.CRHotelLists.Count > 0)
                                crPaxHotelList = crLeg.CRHotelLists.Where(x => x.IsDeleted == false && x.RecordType.Trim() == "P").FirstOrDefault();

                            if (crLeg.CRHotelLists != null && crLeg.CRHotelLists.Count > 0)
                                crCrewHotelList = crLeg.CRHotelLists.Where(x => x.IsDeleted == false && x.RecordType.Trim() == "C").FirstOrDefault();

                            if (crLeg.CRHotelLists != null && crLeg.CRHotelLists.Count > 0)
                                crMaintHotelList = crLeg.CRHotelLists.Where(x => x.IsDeleted == false && x.RecordType.Trim() == "A").FirstOrDefault();

                            if (crPaxHotelList == null)
                            {
                                if (IsAuthorized(PermissionAddCRLogistics))
                                {
                                    if (!string.IsNullOrEmpty(tbPaxHotelName.Text))
                                    {
                                        CRHotelList paxHotelList = new CRHotelList();
                                        paxHotelList.State = CorporateRequestTripEntityState.Added;
                                        paxHotelList.RecordType = "P";
                                        paxHotelList.CRLegID = crLeg.CRLegID;
                                        UpdateScreenDetailstoCRHotel(ref paxHotelList, paxHotelList.RecordType);
                                        if (crLeg.CRHotelLists == null)
                                            crLeg.CRHotelLists = new List<CRHotelList>();
                                        crLeg.CRHotelLists.Add(paxHotelList);
                                    }
                                }
                            }
                            else
                            {
                                if (IsAuthorized(PermissionEditCRLogistics))
                                {
                                    //CRHotelList paxHotelList = new CRHotelList();
                                    if (crPaxHotelList.State != CorporateRequestTripEntityState.Added && crPaxHotelList.State != CorporateRequestTripEntityState.Deleted)
                                    {
                                        if (crPaxHotelList.CRHotelListID != 0)
                                        {
                                            crPaxHotelList.State = CorporateRequestTripEntityState.Modified;
                                        }
                                        else
                                        {
                                            crPaxHotelList.State = CorporateRequestTripEntityState.Added;
                                        }
                                    }
                                    //crPaxHotelList.State = CorporateRequestTripEntityState.Modified;
                                    crPaxHotelList.CRLegID = crLeg.CRLegID;
                                    UpdateScreenDetailstoCRHotel(ref crPaxHotelList, crPaxHotelList.RecordType);
                                    //crLeg.CRHotelLists[0] = paxHotelList;
                                }
                            }

                            if (crCrewHotelList == null)
                            {
                                if (IsAuthorized(PermissionAddCRLogistics))
                                {
                                    if (!string.IsNullOrEmpty(tbCrewHotelName.Text))
                                    {
                                        CRHotelList CrewHotelList = new CRHotelList();
                                        CrewHotelList.State = CorporateRequestTripEntityState.Added;
                                        CrewHotelList.RecordType = "C";
                                        CrewHotelList.CRLegID = crLeg.CRLegID;
                                        UpdateScreenDetailstoCRHotel(ref CrewHotelList, CrewHotelList.RecordType);
                                        if (crLeg.CRHotelLists == null)
                                            crLeg.CRHotelLists = new List<CRHotelList>();
                                        crLeg.CRHotelLists.Add(CrewHotelList);
                                    }
                                }
                            }
                            else
                            {
                                if (IsAuthorized(PermissionEditCRLogistics))
                                {
                                    //CRHotelList CrewHotelList = new CRHotelList();
                                    if (crCrewHotelList.State != CorporateRequestTripEntityState.Added && crCrewHotelList.State != CorporateRequestTripEntityState.Deleted)
                                    {
                                        if (crCrewHotelList.CRHotelListID != 0)
                                        {
                                            crCrewHotelList.State = CorporateRequestTripEntityState.Modified;
                                        }
                                        else
                                        {
                                            crCrewHotelList.State = CorporateRequestTripEntityState.Added;
                                        }
                                    }
                                    //   crCrewHotelList.State = CorporateRequestTripEntityState.Modified;
                                    crCrewHotelList.CRLegID = crLeg.CRLegID;
                                    UpdateScreenDetailstoCRHotel(ref crCrewHotelList, crCrewHotelList.RecordType);
                                    // crLeg.CRHotelLists[1] = CrewHotelList;
                                }
                            }

                            if (crMaintHotelList == null)
                            {
                                if (IsAuthorized(PermissionAddCRLogistics))
                                {
                                    if (!string.IsNullOrEmpty(tbMaintHotelName.Text))
                                    {
                                        CRHotelList MaintHotelList = new CRHotelList();
                                        MaintHotelList.State = CorporateRequestTripEntityState.Added;
                                        MaintHotelList.RecordType = "A";
                                        MaintHotelList.CRLegID = crLeg.CRLegID;
                                        UpdateScreenDetailstoCRHotel(ref MaintHotelList, MaintHotelList.RecordType);
                                        if (crLeg.CRHotelLists == null)
                                            crLeg.CRHotelLists = new List<CRHotelList>();
                                        crLeg.CRHotelLists.Add(MaintHotelList);
                                    }
                                }
                            }
                            else
                            {
                                if (IsAuthorized(PermissionEditCRLogistics))
                                {
                                    //CRHotelList MaintHotelList = new CRHotelList();
                                    if (crMaintHotelList.State != CorporateRequestTripEntityState.Added && crMaintHotelList.State != CorporateRequestTripEntityState.Deleted)
                                    {
                                        if (crMaintHotelList.CRHotelListID != 0)
                                        {
                                            crMaintHotelList.State = CorporateRequestTripEntityState.Modified;
                                        }
                                        else
                                        {
                                            crMaintHotelList.State = CorporateRequestTripEntityState.Added;
                                        }
                                    }
                                    //   crMaintHotelList.State = CorporateRequestTripEntityState.Modified;
                                    crMaintHotelList.CRLegID = crLeg.CRLegID;
                                    UpdateScreenDetailstoCRHotel(ref crMaintHotelList, crMaintHotelList.RecordType);
                                    // crLeg.CRHotelLists[2] = MaintHotelList;
                                }
                            }

                            #endregion

                            if (crLeg.CRLegID != 0 && crLeg.State != CorporateRequestTripEntityState.Deleted)
                            {
                                crLeg.State = CorporateRequestTripEntityState.Modified;
                            }
                            if (!string.IsNullOrEmpty(tbPaxNotes.Text.Trim()))
                            {
                                crLeg.Notes = tbPaxNotes.Text.Trim();
                            }
                            else
                            {
                                crLeg.Notes = string.Empty;
                            }
                        }
                    }
                    Session["CurrentCorporateRequest"] = CorpRequest;
                }
            }
        }

        protected void SaveToCorpRequestSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentCorporateRequest"] != null)
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                    if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                    {
                        List<CRLeg> Corplegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        foreach (CRLeg crleginfo in Corplegs.ToList())
                        {
                            if (crleginfo.LegNUM == Convert.ToInt16(hdnLegNum.Value))
                            {
                                SaveLogistics(crleginfo);
                            }
                        }
                    }
                    Session["CurrentCorporateRequest"] = CorpRequest;
                }
            }
        }

        private void UpdateScreenDetailstoCRFBO(ref CRFBOList crfbo, string arriveDepart)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crfbo, arriveDepart))
            {
                if (arriveDepart.Trim().Equals("D"))
                {
                    if (!string.IsNullOrEmpty(hdDepartFBOID.Value))
                        crfbo.FBOID = Convert.ToInt64(hdDepartFBOID.Value);
                    else
                        crfbo.FBOID = null;

                    if (!string.IsNullOrEmpty(hdDepartIcaoID.Value))
                        crfbo.AirportID = Convert.ToInt64(hdDepartIcaoID.Value);
                    else
                        crfbo.AirportID = null;

                    crfbo.RecordType = "D";
                    crfbo.LastUpdTS = DateTime.UtcNow;
                    crfbo.IsDeleted = false;
                }
                else if (arriveDepart.Trim().Equals("A"))
                {
                    if (!string.IsNullOrEmpty(hdArriveFBOID.Value))
                        crfbo.FBOID = Convert.ToInt64(hdArriveFBOID.Value);
                    else
                        crfbo.FBOID = null;

                    if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                        crfbo.AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                    else
                        crfbo.AirportID = null;

                    crfbo.RecordType = "A";
                    crfbo.LastUpdTS = DateTime.UtcNow;
                    crfbo.IsDeleted = false;
                }
            }
        }

        private void UpdateScreenDetailsFromCRFBO(CRFBOList crfbo)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crfbo))
            {
                if (crfbo.RecordType.Trim() == "D")
                {
                    if (hdDepartIcaoID.Value == crfbo.AirportID.ToString())
                    {
                        if (!string.IsNullOrEmpty(crfbo.FBOID.ToString()))
                        {
                            hdDepartFBOID.Value = crfbo.FBOID.ToString();
                        }
                        else
                        {
                            hdDepartFBOID.Value = string.Empty;
                        }

                        using (FlightPakMasterService.MasterCatalogServiceClient FBOService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (!string.IsNullOrEmpty(hdDepartIcaoID.Value))
                            {
                                var FBOValue = FBOService.GetAllFBOByAirportID(Convert.ToInt64(hdDepartIcaoID.Value));
                                if (FBOValue.ReturnFlag == true)
                                {
                                    if (FBOValue.EntityList.Count > 0)
                                    {
                                        if (!string.IsNullOrEmpty(hdDepartFBOID.Value))
                                        {
                                            var FBOValue1 = FBOValue.EntityList.Where(x => x.FBOID == Convert.ToInt64(hdDepartFBOID.Value)).ToList();
                                            if (FBOValue1.Count > 0)
                                            {
                                                if (FBOValue1[0].FBOCD != null && !string.IsNullOrEmpty(FBOValue1[0].FBOCD.ToString()))
                                                    tbDepartFBO.Text = FBOValue1[0].FBOCD;
                                                if (FBOValue1[0].FBOVendor != null && !string.IsNullOrEmpty(FBOValue1[0].FBOVendor.ToString()))
                                                    tbDepartFBOName.Text = FBOValue1[0].FBOVendor.ToString();
                                                if (FBOValue1[0].PhoneNUM1 != null && !string.IsNullOrEmpty(FBOValue1[0].PhoneNUM1.ToString()))
                                                    tbDepartFBOPhone.Text = FBOValue1[0].PhoneNUM1.ToString();
                                                if (FBOValue1[0].FaxNum != null && !string.IsNullOrEmpty(FBOValue1[0].FaxNum.ToString()))
                                                    tbDepartFBOFax.Text = FBOValue1[0].FaxNum.ToString();
                                                if (FBOValue1[0].ContactEmail != null && !string.IsNullOrEmpty(FBOValue1[0].ContactEmail.ToString()))
                                                    tbDepartFBOEmail.Text = FBOValue1[0].ContactEmail.ToString();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (crfbo.RecordType.Trim() == "A")
                {
                    if (hdArriveIcaoID.Value == crfbo.AirportID.ToString())
                    {
                        if (!string.IsNullOrEmpty(crfbo.FBOID.ToString()))
                        {
                            hdArriveFBOID.Value = crfbo.FBOID.ToString();
                        }
                        else
                        {
                            hdArriveFBOID.Value = string.Empty;
                        }

                        using (FlightPakMasterService.MasterCatalogServiceClient FBOService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                            {
                                var FBOValue = FBOService.GetAllFBOByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                                if (FBOValue.ReturnFlag == true)
                                {
                                    if (FBOValue.EntityList.Count > 0)
                                    {
                                        if (!string.IsNullOrEmpty(hdArriveFBOID.Value))
                                        {
                                            var FBOValue1 = FBOValue.EntityList.Where(x => x.FBOID == Convert.ToInt64(hdArriveFBOID.Value)).ToList();
                                            if (FBOValue1.Count > 0)
                                            {
                                                if (FBOValue1[0].FBOCD != null && !string.IsNullOrEmpty(FBOValue1[0].FBOCD.ToString()))
                                                    tbArriveFBO.Text = FBOValue1[0].FBOCD;
                                                if (FBOValue1[0].FBOVendor != null && !string.IsNullOrEmpty(FBOValue1[0].FBOVendor.ToString()))
                                                    tbArriveFBOName.Text = FBOValue1[0].FBOVendor.ToString();
                                                if (FBOValue1[0].PhoneNUM1 != null && !string.IsNullOrEmpty(FBOValue1[0].PhoneNUM1.ToString()))
                                                    tbArriveFBOPhone.Text = FBOValue1[0].PhoneNUM1.ToString();
                                                if (FBOValue1[0].FaxNum != null && !string.IsNullOrEmpty(FBOValue1[0].FaxNum.ToString()))
                                                    tbArriveFBOFax.Text = FBOValue1[0].FaxNum.ToString();
                                                if (FBOValue1[0].ContactEmail != null && !string.IsNullOrEmpty(FBOValue1[0].ContactEmail.ToString()))
                                                    tbArriveFBOEmail.Text = FBOValue1[0].ContactEmail.ToString();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void UpdateScreenDetailstoCRCatering(ref CRCateringList crcatering)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crcatering))
            {
                if (crcatering.RecordType.Trim().Equals("D"))
                {
                    if (!string.IsNullOrEmpty(hdDeptCateringID.Value))
                        crcatering.CateringID = Convert.ToInt64(hdDeptCateringID.Value);
                    else
                        crcatering.CateringID = null;

                    if (!string.IsNullOrEmpty(hdDepartIcaoID.Value))
                        crcatering.AirportID = Convert.ToInt64(hdDepartIcaoID.Value);
                    else
                        crcatering.AirportID = null;

                    crcatering.RecordType = "D";
                    crcatering.LastUpdTS = DateTime.UtcNow;
                    crcatering.IsDeleted = false;
                    if (!string.IsNullOrEmpty(tbDeptCateringRate.Text))
                        crcatering.Rate = Convert.ToDecimal(tbDeptCateringRate.Text);
                    crcatering.Email = tbDeptCateringEmail.Text;
                    crcatering.FaxNum = tbDeptCateringFax.Text;
                    crcatering.PhoneNUM = tbDeptCateringPhone.Text;
                    crcatering.CRCateringListDescription = tbDeptCateringName.Text;
                }
                else if (crcatering.RecordType.Trim().Equals("A"))
                {
                    if (!string.IsNullOrEmpty(hdArriveCateringID.Value))
                        crcatering.CateringID = Convert.ToInt64(hdArriveCateringID.Value);
                    else
                        crcatering.CateringID = null;

                    if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                        crcatering.AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                    else
                        crcatering.AirportID = null;

                    crcatering.RecordType = "A";
                    crcatering.LastUpdTS = DateTime.UtcNow;
                    crcatering.IsDeleted = false;
                    if (!string.IsNullOrEmpty(tbArriveCateringRate.Text))
                        crcatering.Rate = Convert.ToDecimal(tbArriveCateringRate.Text);
                    crcatering.Email = tbArriveCateringEmail.Text;
                    crcatering.FaxNum = tbArriveCateringFax.Text;
                    crcatering.PhoneNUM = tbArriveCateringPhone.Text;
                    crcatering.CRCateringListDescription = tbArriveCateringName.Text;
                }
            }
        }

        private void UpdateScreenDetailsFromCRCatering(CRCateringList crcatering)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crcatering))
            {
                if (crcatering.RecordType.Trim().Equals("D"))
                {
                    if (hdDepartIcaoID.Value == crcatering.AirportID.ToString())
                    {
                        if (!string.IsNullOrEmpty(crcatering.CateringID.ToString()))
                        {
                            hdDeptCateringID.Value = crcatering.CateringID.ToString();
                        }
                        else
                        {
                            hdDeptCateringID.Value = string.Empty;
                        }
                        if (crcatering.CRCateringListDescription != null && !string.IsNullOrEmpty(crcatering.CRCateringListDescription.ToString()))
                            tbDeptCateringName.Text = crcatering.CRCateringListDescription.ToString();
                        if (crcatering.PhoneNUM != null && !string.IsNullOrEmpty(crcatering.PhoneNUM.ToString()))
                            tbDeptCateringPhone.Text = crcatering.PhoneNUM.ToString();
                        if (crcatering.FaxNum != null && !string.IsNullOrEmpty(crcatering.FaxNum.ToString()))
                            tbDeptCateringFax.Text = crcatering.FaxNum.ToString();
                        if (crcatering.Rate != null)
                            tbDeptCateringRate.Text = crcatering.Rate.ToString();
                        if (crcatering.Email != null && !string.IsNullOrEmpty(crcatering.Email.ToString()))
                            tbDeptCateringEmail.Text = crcatering.Email.ToString();
                        using (FlightPakMasterService.MasterCatalogServiceClient CateringService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var CateringValue = CateringService.GetAllCateringByAirportID(Convert.ToInt64(hdDepartIcaoID.Value));
                            if (CateringValue.ReturnFlag == true)
                            {
                                if (CateringValue.EntityList.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(hdDeptCateringID.Value))
                                    {
                                        var CateringValue1 = CateringValue.EntityList.Where(x => x.CateringID == Convert.ToInt64(hdDeptCateringID.Value)).ToList();
                                        if (CateringValue1.Count > 0)
                                        {
                                            if (!string.IsNullOrEmpty(CateringValue1[0].CateringCD.ToString()))
                                                tbDeptCateringCode.Text = CateringValue1[0].CateringCD;
                                            //if (CateringValue1[0].CateringVendor != null && !string.IsNullOrEmpty(CateringValue1[0].CateringVendor.ToString()))
                                            //    tbDeptCateringName.Text = CateringValue1[0].CateringVendor.ToString();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (crcatering.CRCateringListDescription != null && !string.IsNullOrEmpty(crcatering.CRCateringListDescription.ToString()))
                            tbDeptCateringName.Text = crcatering.CRCateringListDescription.ToString();
                        if (crcatering.PhoneNUM != null && !string.IsNullOrEmpty(crcatering.PhoneNUM.ToString()))
                            tbDeptCateringPhone.Text = crcatering.PhoneNUM.ToString();
                        if (crcatering.FaxNum != null && !string.IsNullOrEmpty(crcatering.FaxNum.ToString()))
                            tbDeptCateringFax.Text = crcatering.FaxNum.ToString();
                        if (crcatering.Rate != null)
                            tbDeptCateringRate.Text = crcatering.Rate.ToString();
                        if (crcatering.Email != null && !string.IsNullOrEmpty(crcatering.Email.ToString()))
                            tbDeptCateringEmail.Text = crcatering.Email.ToString();
                    }
                }
                else if (crcatering.RecordType.Trim().Equals("A"))
                {
                    if (hdArriveIcaoID.Value == crcatering.AirportID.ToString())
                    {
                        if (!string.IsNullOrEmpty(crcatering.CateringID.ToString()))
                        {
                            hdArriveCateringID.Value = crcatering.CateringID.ToString();
                        }
                        else
                        {
                            hdArriveCateringID.Value = string.Empty;
                        }
                        if (crcatering.CRCateringListDescription != null && !string.IsNullOrEmpty(crcatering.CRCateringListDescription.ToString()))
                            tbArriveCateringName.Text = crcatering.CRCateringListDescription.ToString();
                        if (crcatering.PhoneNUM != null && !string.IsNullOrEmpty(crcatering.PhoneNUM.ToString()))
                            tbArriveCateringPhone.Text = crcatering.PhoneNUM.ToString();
                        if (crcatering.FaxNum != null && !string.IsNullOrEmpty(crcatering.FaxNum.ToString()))
                            tbArriveCateringFax.Text = crcatering.FaxNum.ToString();
                        if (crcatering.Rate != null)
                            tbArriveCateringRate.Text = crcatering.Rate.ToString();
                        if (crcatering.Email != null && !string.IsNullOrEmpty(crcatering.Email.ToString()))
                            tbArriveCateringEmail.Text = crcatering.Email.ToString();
                        using (FlightPakMasterService.MasterCatalogServiceClient CateringService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var CateringValue = CateringService.GetAllCateringByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                            if (CateringValue.ReturnFlag == true)
                            {
                                if (CateringValue.EntityList.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(hdArriveCateringID.Value))
                                    {
                                        var CateringValue1 = CateringValue.EntityList.Where(x => x.CateringID == Convert.ToInt64(hdArriveCateringID.Value)).ToList();
                                        if (CateringValue1.Count > 0)
                                        {
                                            if (!string.IsNullOrEmpty(CateringValue1[0].CateringCD.ToString()))
                                                tbArriveCateringCode.Text = CateringValue1[0].CateringCD;
                                            //if (CateringValue1[0].CateringVendor != null && !string.IsNullOrEmpty(CateringValue1[0].CateringVendor.ToString()))
                                            //    tbArriveCateringName.Text = CateringValue1[0].CateringVendor.ToString();                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (crcatering.CRCateringListDescription != null && !string.IsNullOrEmpty(crcatering.CRCateringListDescription.ToString()))
                            tbArriveCateringName.Text = crcatering.CRCateringListDescription.ToString();
                        if (crcatering.PhoneNUM != null && !string.IsNullOrEmpty(crcatering.PhoneNUM.ToString()))
                            tbArriveCateringPhone.Text = crcatering.PhoneNUM.ToString();
                        if (crcatering.FaxNum != null && !string.IsNullOrEmpty(crcatering.FaxNum.ToString()))
                            tbArriveCateringFax.Text = crcatering.FaxNum.ToString();
                        if (crcatering.Rate != null)
                            tbArriveCateringRate.Text = crcatering.Rate.ToString();
                        if (crcatering.Email != null && !string.IsNullOrEmpty(crcatering.Email.ToString()))
                            tbArriveCateringEmail.Text = crcatering.Email.ToString();
                    }
                }
            }
        }

        private void UpdateScreenDetailstoCRTransport(ref CRTransportList crtransport, string paxcrew)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crtransport, paxcrew))
            {
                if (paxcrew.Trim().Equals("P"))
                {
                    if (!string.IsNullOrEmpty(hdPaxTransportID.Value))
                        crtransport.TransportID = Convert.ToInt64(hdPaxTransportID.Value);
                    else
                        crtransport.TransportID = null;

                    if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                        crtransport.AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                    else
                        crtransport.AirportID = null;

                    crtransport.RecordType = "P";
                    crtransport.LastUpdTS = DateTime.UtcNow;
                    crtransport.IsDeleted = false;
                    crtransport.CRTransportListDescription = tbPaxTransportName.Text;
                    if (!string.IsNullOrEmpty(tbPaxTransportRate.Text))
                        crtransport.Rate = Convert.ToDecimal(tbPaxTransportRate.Text);
                    crtransport.PhoneNUM = tbPaxTransportPhone.Text;
                    crtransport.FaxNum = tbPaxTransportFax.Text;
                    crtransport.Email = tbPaxTransportEmail.Text;
                }
                else if (paxcrew.Trim().Equals("C"))
                {
                    if (!string.IsNullOrEmpty(hdCrewTransportID.Value))
                        crtransport.TransportID = Convert.ToInt64(hdCrewTransportID.Value);
                    else
                        crtransport.TransportID = null;

                    if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                        crtransport.AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                    else
                        crtransport.AirportID = null;

                    crtransport.RecordType = "C";
                    crtransport.LastUpdTS = DateTime.UtcNow;
                    crtransport.IsDeleted = false;
                    crtransport.CRTransportListDescription = tbCrewTransportName.Text;
                    if (!string.IsNullOrEmpty(tbCrewTransportRate.Text))
                        crtransport.Rate = Convert.ToDecimal(tbCrewTransportRate.Text);
                    crtransport.PhoneNUM = tbCrewTransportPhone.Text;
                    crtransport.FaxNum = tbCrewTransportFax.Text;
                    crtransport.Email = tbCrewTransportEmail.Text;
                }
            }
        }

        private void UpdateScreenDetailsFromCRTransport(CRTransportList crtransport)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crtransport))
            {
                if (crtransport.RecordType.Trim() == "P")
                {
                    if (hdArriveIcaoID.Value == crtransport.AirportID.ToString())
                    {
                        if (!string.IsNullOrEmpty(crtransport.TransportID.ToString()))
                        {
                            hdPaxTransportID.Value = crtransport.TransportID.ToString();
                        }
                        else
                        {
                            hdPaxTransportID.Value = string.Empty;
                        }
                        if (crtransport.CRTransportListDescription != null && !string.IsNullOrEmpty(crtransport.CRTransportListDescription.ToString()))
                            tbPaxTransportName.Text = crtransport.CRTransportListDescription.ToString();
                        if (crtransport.PhoneNUM != null && !string.IsNullOrEmpty(crtransport.PhoneNUM.ToString()))
                            tbPaxTransportPhone.Text = crtransport.PhoneNUM.ToString();
                        if (crtransport.FaxNum != null && !string.IsNullOrEmpty(crtransport.FaxNum.ToString()))
                            tbPaxTransportFax.Text = crtransport.FaxNum.ToString();
                        if (crtransport.Email != null && !string.IsNullOrEmpty(crtransport.Email.ToString()))
                            tbPaxTransportEmail.Text = crtransport.Email.ToString();
                        if (crtransport.Rate != null)
                            tbPaxTransportRate.Text = crtransport.Rate.ToString();
                        using (FlightPakMasterService.MasterCatalogServiceClient TransportService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var TransportValue = TransportService.GetTransportByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                            if (TransportValue.ReturnFlag == true)
                            {
                                if (TransportValue.EntityList.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(hdPaxTransportID.Value))
                                    {
                                        var TransportValue1 = TransportValue.EntityList.Where(x => x.TransportID == Convert.ToInt64(hdPaxTransportID.Value)).ToList();
                                        if (TransportValue1.Count > 0)
                                        {
                                            if (!string.IsNullOrEmpty(TransportValue1[0].TransportCD.ToString()))
                                                tbPaxTransportCode.Text = TransportValue1[0].TransportCD;
                                            //if (!string.IsNullOrEmpty(TransportValue1[0].TransportationVendor.ToString()))
                                            //    tbPaxTransportName.Text = TransportValue1[0].TransportationVendor.ToString();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (crtransport.CRTransportListDescription != null && !string.IsNullOrEmpty(crtransport.CRTransportListDescription.ToString()))
                            tbPaxTransportName.Text = crtransport.CRTransportListDescription.ToString();
                        if (crtransport.PhoneNUM != null && !string.IsNullOrEmpty(crtransport.PhoneNUM.ToString()))
                            tbPaxTransportPhone.Text = crtransport.PhoneNUM.ToString();
                        if (crtransport.FaxNum != null && !string.IsNullOrEmpty(crtransport.FaxNum.ToString()))
                            tbPaxTransportFax.Text = crtransport.FaxNum.ToString();
                        if (crtransport.Email != null && !string.IsNullOrEmpty(crtransport.Email.ToString()))
                            tbPaxTransportEmail.Text = crtransport.Email.ToString();
                        if (crtransport.Rate != null)
                            tbPaxTransportRate.Text = crtransport.Rate.ToString();
                    }
                }
                else if (crtransport.RecordType.Trim() == "C")
                {
                    if (hdArriveIcaoID.Value == crtransport.AirportID.ToString())
                    {
                        if (!string.IsNullOrEmpty(crtransport.TransportID.ToString()))
                        {
                            hdCrewTransportID.Value = crtransport.TransportID.ToString();
                        }
                        else
                        {
                            hdCrewTransportID.Value = string.Empty;
                        }
                        if (crtransport.CRTransportListDescription != null && !string.IsNullOrEmpty(crtransport.CRTransportListDescription.ToString()))
                            tbCrewTransportName.Text = crtransport.CRTransportListDescription.ToString();
                        if (crtransport.PhoneNUM != null && !string.IsNullOrEmpty(crtransport.PhoneNUM.ToString()))
                            tbCrewTransportPhone.Text = crtransport.PhoneNUM.ToString();
                        if (crtransport.FaxNum != null && !string.IsNullOrEmpty(crtransport.FaxNum.ToString()))
                            tbCrewTransportFax.Text = crtransport.FaxNum.ToString();
                        if (crtransport.Email != null && !string.IsNullOrEmpty(crtransport.Email.ToString()))
                            tbCrewTransportEmail.Text = crtransport.Email.ToString();
                        if (crtransport.Rate != null && !string.IsNullOrEmpty(crtransport.Rate.ToString()))
                            tbCrewTransportRate.Text = crtransport.Rate.ToString();
                        using (FlightPakMasterService.MasterCatalogServiceClient TransportService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var TransportValue = TransportService.GetTransportByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                            if (TransportValue.ReturnFlag == true)
                            {
                                if (TransportValue.EntityList.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(hdCrewTransportID.Value))
                                    {
                                        var TransportValue1 = TransportValue.EntityList.Where(x => x.TransportID == Convert.ToInt64(hdCrewTransportID.Value)).ToList();
                                        if (TransportValue1.Count > 0)
                                        {
                                            if (!string.IsNullOrEmpty(TransportValue1[0].TransportCD.ToString()))
                                                tbCrewTransportCode.Text = TransportValue1[0].TransportCD;
                                            //if (!string.IsNullOrEmpty(TransportValue1[0].TransportationVendor.ToString()))
                                            //    tbCrewTransportName.Text = TransportValue1[0].TransportationVendor.ToString();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (crtransport.CRTransportListDescription != null && !string.IsNullOrEmpty(crtransport.CRTransportListDescription.ToString()))
                            tbCrewTransportName.Text = crtransport.CRTransportListDescription.ToString();
                        if (crtransport.PhoneNUM != null && !string.IsNullOrEmpty(crtransport.PhoneNUM.ToString()))
                            tbCrewTransportPhone.Text = crtransport.PhoneNUM.ToString();
                        if (crtransport.FaxNum != null && !string.IsNullOrEmpty(crtransport.FaxNum.ToString()))
                            tbCrewTransportFax.Text = crtransport.FaxNum.ToString();
                        if (crtransport.Email != null && !string.IsNullOrEmpty(crtransport.Email.ToString()))
                            tbCrewTransportEmail.Text = crtransport.Email.ToString();
                        if (crtransport.Rate != null && !string.IsNullOrEmpty(crtransport.Rate.ToString()))
                            tbCrewTransportRate.Text = crtransport.Rate.ToString();
                    }
                }
            }
        }

        private void UpdateScreenDetailstoCRHotel(ref CRHotelList crhotel, string paxcrewmaint)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crhotel, paxcrewmaint))
            {
                if (paxcrewmaint.Trim().Equals("P"))
                {
                    if (!string.IsNullOrEmpty(hdPaxHotelID.Value))
                        crhotel.HotelID = Convert.ToInt64(hdPaxHotelID.Value);
                    else
                        crhotel.HotelID = null;

                    if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                        crhotel.AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                    else
                        crhotel.AirportID = null;

                    crhotel.RecordType = "P";
                    crhotel.LastUpdTS = DateTime.UtcNow;
                    crhotel.IsDeleted = false;
                    if (!string.IsNullOrEmpty(tbPaxHotelRate.Text))
                        crhotel.Rate = Convert.ToDecimal(tbPaxHotelRate.Text);
                    crhotel.Email = tbPaxHotelEmail.Text;
                    crhotel.FaxNum = tbPaxHotelFax.Text;
                    crhotel.PhoneNUM = tbPaxHotelPhone.Text;
                    crhotel.CRHotelListDescription = tbPaxHotelName.Text;
                }
                else if (paxcrewmaint.Trim().Equals("C"))
                {
                    if (!string.IsNullOrEmpty(hdCrewHotelID.Value))
                        crhotel.HotelID = Convert.ToInt64(hdCrewHotelID.Value);
                    else
                        crhotel.HotelID = null;

                    if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                        crhotel.AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                    else
                        crhotel.AirportID = null;

                    crhotel.RecordType = "C";
                    crhotel.LastUpdTS = DateTime.UtcNow;
                    crhotel.IsDeleted = false;
                    if (!string.IsNullOrEmpty(tbCrewHotelRate.Text))
                        crhotel.Rate = Convert.ToDecimal(tbCrewHotelRate.Text);
                    crhotel.Email = tbCrewHotelEmail.Text;
                    crhotel.FaxNum = tbCrewHotelFax.Text;
                    crhotel.PhoneNUM = tbCrewHotelPhone.Text;
                    crhotel.CRHotelListDescription = tbCrewHotelName.Text;
                }
                else if (paxcrewmaint.Trim().Equals("A"))
                {
                    if (!string.IsNullOrEmpty(hdMaintHotelID.Value))
                        crhotel.HotelID = Convert.ToInt64(hdMaintHotelID.Value);
                    else
                        crhotel.HotelID = null;

                    if (!string.IsNullOrEmpty(hdArriveIcaoID.Value))
                        crhotel.AirportID = Convert.ToInt64(hdArriveIcaoID.Value);
                    else
                        crhotel.AirportID = null;

                    crhotel.RecordType = "A";
                    crhotel.LastUpdTS = DateTime.UtcNow;
                    crhotel.IsDeleted = false;
                    if (!string.IsNullOrEmpty(tbMaintHotelRate.Text))
                        crhotel.Rate = Convert.ToDecimal(tbMaintHotelRate.Text);
                    crhotel.Email = tbMaintHotelEmail.Text;
                    crhotel.FaxNum = tbMaintHotelFax.Text;
                    crhotel.PhoneNUM = tbMaintHotelPhone.Text;
                    crhotel.CRHotelListDescription = tbMaintHotelName.Text;
                }
            }
        }

        private void UpdateScreenDetailsFromCRHotel(CRHotelList crhotel)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crhotel))
            {
                if (crhotel.RecordType.Trim() == "P")
                {
                    if (hdArriveIcaoID.Value == crhotel.AirportID.ToString())
                    {
                        if (!string.IsNullOrEmpty(crhotel.HotelID.ToString()))
                        {
                            hdPaxHotelID.Value = crhotel.HotelID.ToString();
                        }
                        else
                        {
                            hdPaxHotelID.Value = string.Empty;
                        }
                        if (crhotel.CRHotelListDescription != null && !string.IsNullOrEmpty(crhotel.CRHotelListDescription.ToString()))
                            tbPaxHotelName.Text = crhotel.CRHotelListDescription.ToString();
                        if (crhotel.PhoneNUM != null && !string.IsNullOrEmpty(crhotel.PhoneNUM.ToString()))
                            tbPaxHotelPhone.Text = crhotel.PhoneNUM.ToString();
                        if (crhotel.FaxNum != null && !string.IsNullOrEmpty(crhotel.FaxNum.ToString()))
                            tbPaxHotelFax.Text = crhotel.FaxNum.ToString();
                        if (crhotel.Email != null && !string.IsNullOrEmpty(crhotel.Email.ToString()))
                            tbPaxHotelEmail.Text = crhotel.Email.ToString();
                        if (crhotel.Rate != null)
                            tbPaxHotelRate.Text = crhotel.Rate.ToString();
                        using (FlightPakMasterService.MasterCatalogServiceClient HotelService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var HotelValue = HotelService.GetHotelsByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                            if (HotelValue.ReturnFlag == true)
                            {
                                if (HotelValue.EntityList.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(hdPaxHotelID.Value))
                                    {
                                        var HotelValue1 = HotelValue.EntityList.Where(x => x.HotelID == Convert.ToInt64(hdPaxHotelID.Value)).ToList();
                                        if (HotelValue1.Count > 0)
                                        {
                                            if (!string.IsNullOrEmpty(HotelValue1[0].HotelCD.ToString()))
                                                tbPaxHotelCode.Text = HotelValue1[0].HotelCD;
                                            //if (!string.IsNullOrEmpty(HotelValue1[0].Name.ToString()))
                                            //    tbPaxHotelName.Text = HotelValue1[0].Name.ToString();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (crhotel.CRHotelListDescription != null && !string.IsNullOrEmpty(crhotel.CRHotelListDescription.ToString()))
                            tbPaxHotelName.Text = crhotel.CRHotelListDescription.ToString();
                        if (crhotel.PhoneNUM != null && !string.IsNullOrEmpty(crhotel.PhoneNUM.ToString()))
                            tbPaxHotelPhone.Text = crhotel.PhoneNUM.ToString();
                        if (crhotel.FaxNum != null && !string.IsNullOrEmpty(crhotel.FaxNum.ToString()))
                            tbPaxHotelFax.Text = crhotel.FaxNum.ToString();
                        if (crhotel.Email != null && !string.IsNullOrEmpty(crhotel.Email.ToString()))
                            tbPaxHotelEmail.Text = crhotel.Email.ToString();
                        if (crhotel.Rate != null)
                            tbPaxHotelRate.Text = crhotel.Rate.ToString();
                    }
                }
                else if (crhotel.RecordType.Trim() == "C")
                {
                    if (hdArriveIcaoID.Value == crhotel.AirportID.ToString())
                    {
                        if (!string.IsNullOrEmpty(crhotel.HotelID.ToString()))
                        {
                            hdCrewHotelID.Value = crhotel.HotelID.ToString();
                        }
                        else
                        {
                            hdCrewHotelID.Value = string.Empty;
                        }
                        if (crhotel.CRHotelListDescription != null && !string.IsNullOrEmpty(crhotel.CRHotelListDescription.ToString()))
                            tbCrewHotelName.Text = crhotel.CRHotelListDescription.ToString();
                        if (crhotel.PhoneNUM != null && !string.IsNullOrEmpty(crhotel.PhoneNUM.ToString()))
                            tbCrewHotelPhone.Text = crhotel.PhoneNUM.ToString();
                        if (crhotel.FaxNum != null && !string.IsNullOrEmpty(crhotel.FaxNum.ToString()))
                            tbCrewHotelFax.Text = crhotel.FaxNum.ToString();
                        if (crhotel.Email != null && !string.IsNullOrEmpty(crhotel.Email.ToString()))
                            tbCrewHotelEmail.Text = crhotel.Email.ToString();
                        if (crhotel.Rate != null)
                            tbCrewHotelRate.Text = crhotel.Rate.ToString();
                        using (FlightPakMasterService.MasterCatalogServiceClient HotelService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var HotelValue = HotelService.GetHotelsByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                            if (HotelValue.ReturnFlag == true)
                            {
                                if (HotelValue.EntityList.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(hdCrewHotelID.Value))
                                    {
                                        var HotelValue1 = HotelValue.EntityList.Where(x => x.HotelID == Convert.ToInt64(hdCrewHotelID.Value)).ToList();
                                        if (HotelValue1.Count > 0)
                                        {
                                            if (!string.IsNullOrEmpty(HotelValue1[0].HotelCD.ToString()))
                                                tbCrewHotelCode.Text = HotelValue1[0].HotelCD;
                                            //if (!string.IsNullOrEmpty(HotelValue1[0].Name.ToString()))
                                            //    tbCrewHotelName.Text = HotelValue1[0].Name.ToString();
                                            //if (HotelValue1[0].PhoneNum != null && !string.IsNullOrEmpty(HotelValue1[0].PhoneNum.ToString()))
                                            //    tbCrewHotelPhone.Text = HotelValue1[0].PhoneNum.ToString();
                                            //if (HotelValue1[0].FaxNum != null && !string.IsNullOrEmpty(HotelValue1[0].FaxNum.ToString()))
                                            //    tbCrewHotelFax.Text = HotelValue1[0].FaxNum.ToString();
                                            //if (HotelValue1[0].ContactEmail != null && !string.IsNullOrEmpty(HotelValue1[0].ContactEmail.ToString()))
                                            //    tbCrewHotelEmail.Text = HotelValue1[0].ContactEmail.ToString();
                                            //if (HotelValue1[0].NegociatedRate != null && !string.IsNullOrEmpty(HotelValue1[0].NegociatedRate.ToString()))
                                            //    tbCrewHotelRate.Text = HotelValue1[0].NegociatedRate.ToString();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (crhotel.CRHotelListDescription != null && !string.IsNullOrEmpty(crhotel.CRHotelListDescription.ToString()))
                            tbCrewHotelName.Text = crhotel.CRHotelListDescription.ToString();
                        if (crhotel.PhoneNUM != null && !string.IsNullOrEmpty(crhotel.PhoneNUM.ToString()))
                            tbCrewHotelPhone.Text = crhotel.PhoneNUM.ToString();
                        if (crhotel.FaxNum != null && !string.IsNullOrEmpty(crhotel.FaxNum.ToString()))
                            tbCrewHotelFax.Text = crhotel.FaxNum.ToString();
                        if (crhotel.Email != null && !string.IsNullOrEmpty(crhotel.Email.ToString()))
                            tbCrewHotelEmail.Text = crhotel.Email.ToString();
                        if (crhotel.Rate != null)
                            tbCrewHotelRate.Text = crhotel.Rate.ToString();
                    }
                }
                else if (crhotel.RecordType.Trim() == "A")
                {
                    if (hdArriveIcaoID.Value == crhotel.AirportID.ToString())
                    {
                        if (!string.IsNullOrEmpty(crhotel.HotelID.ToString()))
                        {
                            hdMaintHotelID.Value = crhotel.HotelID.ToString();
                        }
                        else
                        {
                            hdMaintHotelID.Value = string.Empty;
                        }
                        if (crhotel.CRHotelListDescription != null && !string.IsNullOrEmpty(crhotel.CRHotelListDescription.ToString()))
                            tbMaintHotelName.Text = crhotel.CRHotelListDescription.ToString();
                        if (crhotel.PhoneNUM != null && !string.IsNullOrEmpty(crhotel.PhoneNUM.ToString()))
                            tbMaintHotelPhone.Text = crhotel.PhoneNUM.ToString();
                        if (crhotel.FaxNum != null && !string.IsNullOrEmpty(crhotel.FaxNum.ToString()))
                            tbMaintHotelFax.Text = crhotel.FaxNum.ToString();
                        if (crhotel.Email != null && !string.IsNullOrEmpty(crhotel.Email.ToString()))
                            tbMaintHotelEmail.Text = crhotel.Email.ToString();
                        if (crhotel.Rate != null)
                            tbMaintHotelRate.Text = crhotel.Rate.ToString();
                        using (FlightPakMasterService.MasterCatalogServiceClient HotelService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var HotelValue = HotelService.GetHotelsByAirportID(Convert.ToInt64(hdArriveIcaoID.Value));
                            if (HotelValue.ReturnFlag == true)
                            {
                                if (HotelValue.EntityList.Count > 0)
                                {
                                    if (!string.IsNullOrEmpty(hdMaintHotelID.Value))
                                    {
                                        var HotelValue1 = HotelValue.EntityList.Where(x => x.HotelID == Convert.ToInt64(hdMaintHotelID.Value)).ToList();
                                        if (HotelValue1.Count > 0)
                                        {
                                            if (!string.IsNullOrEmpty(HotelValue1[0].HotelCD.ToString()))
                                                tbMaintHotelCode.Text = HotelValue1[0].HotelCD;
                                            //if (!string.IsNullOrEmpty(HotelValue1[0].Name.ToString()))
                                            //    tbMaintHotelName.Text = HotelValue1[0].Name.ToString();
                                            //if (HotelValue1[0].PhoneNum != null && !string.IsNullOrEmpty(HotelValue1[0].PhoneNum.ToString()))
                                            //    tbMaintHotelPhone.Text = HotelValue1[0].PhoneNum.ToString();
                                            //if (HotelValue1[0].FaxNum != null && !string.IsNullOrEmpty(HotelValue1[0].FaxNum.ToString()))
                                            //    tbMaintHotelFax.Text = HotelValue1[0].FaxNum.ToString();
                                            //if (HotelValue1[0].ContactEmail != null && !string.IsNullOrEmpty(HotelValue1[0].ContactEmail.ToString()))
                                            //    tbMaintHotelEmail.Text = HotelValue1[0].ContactEmail.ToString();
                                            //if (HotelValue1[0].NegociatedRate != null && !string.IsNullOrEmpty(HotelValue1[0].NegociatedRate.ToString()))
                                            //    tbMaintHotelRate.Text = HotelValue1[0].NegociatedRate.ToString();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (crhotel.CRHotelListDescription != null && !string.IsNullOrEmpty(crhotel.CRHotelListDescription.ToString()))
                            tbMaintHotelName.Text = crhotel.CRHotelListDescription.ToString();
                        if (crhotel.PhoneNUM != null && !string.IsNullOrEmpty(crhotel.PhoneNUM.ToString()))
                            tbMaintHotelPhone.Text = crhotel.PhoneNUM.ToString();
                        if (crhotel.FaxNum != null && !string.IsNullOrEmpty(crhotel.FaxNum.ToString()))
                            tbMaintHotelFax.Text = crhotel.FaxNum.ToString();
                        if (crhotel.Email != null && !string.IsNullOrEmpty(crhotel.Email.ToString()))
                            tbMaintHotelEmail.Text = crhotel.Email.ToString();
                        if (crhotel.Rate != null)
                            tbMaintHotelRate.Text = crhotel.Rate.ToString();
                    }
                }
            }
        }

        private void CRchoice(CRLeg crLeg)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crLeg))
            {
                if (crLeg.DepartAirportChanged)
                {
                    #region "Depart FBO"
                    if (IsAuthorized(PermissionViewCRLogistics))
                    {
                        if (crLeg.CRFBOLists != null && crLeg.CRFBOLists.Count() > 0)
                        {
                            CRFBOList Departfbo = (from fbo in crLeg.CRFBOLists.ToList()
                                                   where fbo.IsDeleted == false && fbo.RecordType.Trim() == "D"
                                                   select fbo).SingleOrDefault();

                            if (Departfbo != null)
                            {
                                if (Departfbo.CRFBOListID == 0)
                                    crLeg.CRFBOLists.Remove(Departfbo);
                                else
                                {

                                    Departfbo.IsDeleted = true;
                                    Departfbo.State = CorporateRequestTripEntityState.Deleted;
                                }
                            }
                        }

                        #region "Add Depart FBO"


                        if (crLeg.DAirportID != null && crLeg.DAirportID > 0)
                        {
                            MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                            var ObjRetValGroup = ObjService.GetAllFBOByAirportID(Convert.ToInt64(crLeg.DAirportID)).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                            if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                            {
                                CRFBOList Fbo = new CRFBOList();

                                CorporateRequestService.FBO DepFbo = new CorporateRequestService.FBO();

                                DepFbo.FBOID = ObjRetValGroup[0].FBOID;
                                DepFbo.FBOCD = ObjRetValGroup[0].FBOCD;
                                Fbo.FBO = DepFbo;
                                Fbo.AirportID = crLeg.DAirportID;
                                Fbo.FBOID = ObjRetValGroup[0].FBOID;
                                Fbo.RecordType = "D";
                                Fbo.LastUpdTS = DateTime.UtcNow;
                                Fbo.IsDeleted = false;
                                Fbo.State = CorporateRequestTripEntityState.Added;
                                if (crLeg.CRFBOLists == null)
                                    crLeg.CRFBOLists = new List<CRFBOList>();
                                crLeg.CRFBOLists.Add(Fbo);
                            }
                        }
                        #endregion

                    }
                    #endregion

                    //#region "Depart Catering"
                    //if (IsAuthorized(PermissionViewCRLogistics))
                    //{
                    //    if (crLeg.CRCateringLists != null && crLeg.CRCateringLists.Count() > 0)
                    //    {
                    //        CRCateringList DepartCatering = (from catering in crLeg.CRCateringLists.ToList()
                    //                                         where catering.IsDeleted == false && catering.RecordType.Trim() == "D"
                    //                                         select catering).SingleOrDefault();

                    //        if (DepartCatering != null)
                    //        {
                    //            if (DepartCatering.CRCateringListID == 0)
                    //                crLeg.CRCateringLists.Remove(DepartCatering);
                    //            else
                    //            {

                    //                DepartCatering.IsDeleted = true;
                    //                DepartCatering.State = CorporateRequestTripEntityState.Deleted;
                    //            }
                    //        }
                    //    }

                    //    #region "Add Depart Catering"


                    //    if (crLeg.DAirportID != null && crLeg.DAirportID > 0)
                    //    {
                    //        MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                    //        var ObjRetValGroup = ObjService.GetAllCateringByAirportID(Convert.ToInt64(crLeg.DAirportID)).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                    //        if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                    //        {
                    //            CRCateringList catering = new CRCateringList();

                    //            CorporateRequestService.Catering departCatering = new CorporateRequestService.Catering();

                    //            departCatering.CateringID = ObjRetValGroup[0].CateringID;
                    //            departCatering.CateringCD = ObjRetValGroup[0].CateringCD;
                    //            catering.Catering = departCatering;

                    //            catering.AirportID = crLeg.DAirportID;
                    //            catering.CateringID = ObjRetValGroup[0].CateringID;
                    //            catering.RecordType = "D";
                    //            catering.LastUpdTS = DateTime.UtcNow;
                    //            catering.IsDeleted = false;
                    //            catering.State = CorporateRequestTripEntityState.Added;
                    //            if (crLeg.CRCateringLists == null)
                    //                crLeg.CRCateringLists = new List<CRCateringList>();
                    //            crLeg.CRCateringLists.Add(catering);
                    //        }
                    //    }
                    //    #endregion

                    //}
                    //#endregion

                    crLeg.DepartAirportChanged = false;
                }

                if (crLeg.ArrivalAirportChanged)
                {
                    #region "Arrive FBO"
                    if (IsAuthorized(PermissionViewCRLogistics))
                    {
                        if (crLeg.CRFBOLists != null && crLeg.CRFBOLists.Count() > 0)
                        {
                            CRFBOList Arrivefbo = (from fbo in crLeg.CRFBOLists.ToList()
                                                   where fbo.IsDeleted == false && fbo.RecordType.Trim() == "A"
                                                   select fbo).SingleOrDefault();

                            if (Arrivefbo != null)
                            {
                                if (Arrivefbo.CRFBOListID == 0)
                                    crLeg.CRFBOLists.Remove(Arrivefbo);
                                else
                                {

                                    Arrivefbo.IsDeleted = true;
                                    Arrivefbo.State = CorporateRequestTripEntityState.Deleted;
                                }
                            }
                        }

                        #region "Add Arrive FBO"


                        if (crLeg.AAirportID != null && crLeg.AAirportID > 0)
                        {
                            MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                            var ObjRetValGroup = ObjService.GetAllFBOByAirportID(Convert.ToInt64(crLeg.AAirportID)).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                            if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                            {
                                CRFBOList Fbo = new CRFBOList();

                                CorporateRequestService.FBO PrefFbo = new CorporateRequestService.FBO();

                                PrefFbo.FBOID = ObjRetValGroup[0].FBOID;
                                PrefFbo.FBOCD = ObjRetValGroup[0].FBOCD;
                                Fbo.FBO = PrefFbo;
                                Fbo.AirportID = crLeg.AAirportID;
                                Fbo.FBOID = ObjRetValGroup[0].FBOID;
                                Fbo.RecordType = "A";
                                Fbo.LastUpdTS = DateTime.UtcNow;
                                Fbo.IsDeleted = false;
                                Fbo.State = CorporateRequestTripEntityState.Added;
                                if (crLeg.CRFBOLists == null)
                                    crLeg.CRFBOLists = new List<CRFBOList>();
                                crLeg.CRFBOLists.Add(Fbo);
                            }
                        }
                        #endregion

                    }
                    #endregion

                    //#region "Arrive Catering"
                    //if (IsAuthorized(PermissionViewCRLogistics))
                    //{
                    //    if (crLeg.CRCateringLists != null && crLeg.CRCateringLists.Count() > 0)
                    //    {
                    //        CRCateringList ArriveCatering = (from catering in crLeg.CRCateringLists.ToList()
                    //                                         where catering.IsDeleted == false && catering.RecordType.Trim() == "A"
                    //                                         select catering).SingleOrDefault();

                    //        if (ArriveCatering != null)
                    //        {
                    //            if (ArriveCatering.CRCateringListID == 0)
                    //                crLeg.CRCateringLists.Remove(ArriveCatering);
                    //            else
                    //            {

                    //                ArriveCatering.IsDeleted = true;
                    //                ArriveCatering.State = CorporateRequestTripEntityState.Deleted;
                    //            }
                    //        }
                    //    }

                    //    #region "Add Arrive Catering"


                    //    if (crLeg.AAirportID != null && crLeg.AAirportID > 0)
                    //    {
                    //        MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                    //        var ObjRetValGroup = ObjService.GetAllCateringByAirportID(Convert.ToInt64(crLeg.AAirportID)).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                    //        if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                    //        {
                    //            CRCateringList catering = new CRCateringList();

                    //            CorporateRequestService.Catering ArriveCatering = new CorporateRequestService.Catering();

                    //            ArriveCatering.CateringID = ObjRetValGroup[0].CateringID;
                    //            ArriveCatering.CateringCD = ObjRetValGroup[0].CateringCD;
                    //            catering.Catering = ArriveCatering;

                    //            catering.AirportID = crLeg.AAirportID;
                    //            catering.CateringID = ObjRetValGroup[0].CateringID;
                    //            catering.RecordType = "A";
                    //            catering.LastUpdTS = DateTime.UtcNow;
                    //            catering.IsDeleted = false;
                    //            catering.State = CorporateRequestTripEntityState.Added;
                    //            if (crLeg.CRCateringLists == null)
                    //                crLeg.CRCateringLists = new List<CRCateringList>();
                    //            crLeg.CRCateringLists.Add(catering);
                    //        }
                    //    }
                    //    #endregion

                    //}
                    //#endregion

                    //#region "Transport"
                    //if (IsAuthorized(PermissionViewCRLogistics))
                    //{
                    //    if (crLeg.CRTransportLists != null && crLeg.CRTransportLists.Count() > 0)
                    //    {
                    //        CRTransportList paxTrans = (from paxtransport in crLeg.CRTransportLists.ToList()
                    //                                    where paxtransport.IsDeleted == false && paxtransport.RecordType.Trim() == "P"
                    //                                    select paxtransport).SingleOrDefault();

                    //        CRTransportList crewTrans = (from paxtransport in crLeg.CRTransportLists.ToList()
                    //                                     where paxtransport.IsDeleted == false && paxtransport.RecordType.Trim() == "C"
                    //                                     select paxtransport).SingleOrDefault();

                    //        if (paxTrans != null)
                    //        {
                    //            if (paxTrans.CRTransportListID == 0)
                    //                crLeg.CRTransportLists.Remove(paxTrans);
                    //            else
                    //            {
                    //                paxTrans.IsDeleted = true;
                    //                paxTrans.State = CorporateRequestTripEntityState.Deleted;
                    //            }
                    //        }

                    //        if (crewTrans != null)
                    //        {
                    //            if (crewTrans.CRTransportListID == 0)
                    //                crLeg.CRTransportLists.Remove(crewTrans);
                    //            else
                    //            {
                    //                crewTrans.IsDeleted = true;
                    //                crewTrans.State = CorporateRequestTripEntityState.Deleted;
                    //            }
                    //        }
                    //    }

                    //    #region "Add Transport for Pax and Crew"

                    //    if (crLeg.AAirportID != null && crLeg.AAirportID > 0)
                    //    {
                    //        MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                    //        var ObjRetValGroup = ObjService.GetTransportByAirportID(Convert.ToInt64(crLeg.AAirportID)).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                    //        if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                    //        {
                    //            #region "Pax"
                    //            CRTransportList PaxTransport = new CRTransportList();

                    //            CorporateRequestService.Transport paxTrans = new CorporateRequestService.Transport();

                    //            paxTrans.TransportID = ObjRetValGroup[0].TransportID;
                    //            paxTrans.TransportCD = ObjRetValGroup[0].TransportCD;
                    //            PaxTransport.Transport = paxTrans;

                    //            PaxTransport.AirportID = crLeg.AAirportID;
                    //            PaxTransport.TransportID = ObjRetValGroup[0].TransportID;
                    //            PaxTransport.RecordType = "P";
                    //            PaxTransport.LastUpdTS = DateTime.UtcNow;
                    //            PaxTransport.IsDeleted = false;
                    //            PaxTransport.State = CorporateRequestTripEntityState.Added;
                    //            if (crLeg.CRTransportLists == null)
                    //                crLeg.CRTransportLists = new List<CRTransportList>();
                    //            crLeg.CRTransportLists.Add(PaxTransport);
                    //            #endregion

                    //            #region "Crew"
                    //            CRTransportList CrewTransport = new CRTransportList();

                    //            CorporateRequestService.Transport crewTrans = new CorporateRequestService.Transport();

                    //            crewTrans.TransportID = ObjRetValGroup[0].TransportID;
                    //            crewTrans.TransportCD = ObjRetValGroup[0].TransportCD;
                    //            CrewTransport.Transport = crewTrans;

                    //            CrewTransport.AirportID = crLeg.AAirportID;
                    //            CrewTransport.TransportID = ObjRetValGroup[0].TransportID;
                    //            CrewTransport.RecordType = "C";
                    //            CrewTransport.LastUpdTS = DateTime.UtcNow;
                    //            CrewTransport.IsDeleted = false;
                    //            CrewTransport.State = CorporateRequestTripEntityState.Added;
                    //            if (crLeg.CRTransportLists == null)
                    //                crLeg.CRTransportLists = new List<CRTransportList>();
                    //            crLeg.CRTransportLists.Add(CrewTransport);
                    //            #endregion
                    //        }
                    //    }
                    //    #endregion
                    //}
                    //#endregion

                    crLeg.ArrivalAirportChanged = false;
                }

                Session["CurrentCorporateRequest"] = CorpRequest;
            }
        }

        protected void rtsLegs_TabClick(object sender, RadTabStripEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                        if (CorpRequest.CRLegs != null)
                        {
                            List<CRLeg> Corplegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                            if (IsAuthorized(Permission.CorporateRequest.AddCRLogistics) || IsAuthorized(Permission.CorporateRequest.EditCRLogistics))
                            {
                                SaveLogistics(Corplegs[Convert.ToInt16(hdnLegNum.Value) - 1]);
                            }
                            hdnLegNum.Value = (Convert.ToInt16(e.Tab.Index) + 1).ToString();
                            foreach (CRLeg crleginfo in Corplegs.ToList())
                            {
                                if (crleginfo.LegNUM == Convert.ToInt16(hdnLegNum.Value))
                                {
                                    GetTransferFBO();
                                    ClearFields();
                                    LoadLegDetails(Corplegs[Convert.ToInt16(hdnLegNum.Value) - 1]);
                                }
                            }
                            rtsLegs.Focus();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }

        }

        protected void BindCRTabs(CRMain CorpReq, string eventraised)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CorpReq, eventraised))
            {
                rtsLegs.Tabs.Clear();


                if (CorpReq.CRLegs != null && CorpReq.CRLegs.Count() > 0)
                {
                    List<CRLeg> CorpLegList = CorpReq.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                    int Legid = 0;
                    int counter = 1;
                    foreach (CRLeg Leg in CorpLegList)
                    {
                        if (Leg.State != CorporateRequestTripEntityState.Deleted)
                        {
                            RadTab Rt = new RadTab();
                            Rt.Text = "Leg" + Leg.LegNUM.ToString();
                            Rt.Value = CorpReq.CRLegs.IndexOf(Leg).ToString();


                            rtsLegs.Tabs.Add(Rt);

                            if (Leg.AAirportID != null && Leg.DAirportID != null)
                            {
                                if (Leg.Airport1 != null && Leg.Airport != null)
                                    rtsLegs.Tabs[Legid].Text = "Leg" + (Legid + 1).ToString() + " (" + Leg.Airport1.IcaoID + "-" + Leg.Airport.IcaoID + ")";
                                else
                                    rtsLegs.Tabs[Legid].Text = "Leg" + (Legid + 1).ToString();
                            }
                            else
                                rtsLegs.Tabs[Legid].Text = "Leg" + (Legid + 1).ToString();

                            Legid = Legid + 1;
                            counter = counter + 1;
                        }
                    }
                }
                else
                {
                    RadTab Rt = new RadTab();
                    Rt.Text = "Leg1";
                    Rt.Value = "0";
                    rtsLegs.Tabs.Add(Rt);
                }
                if (rtsLegs.Tabs.Count > 1)
                {
                    if (eventraised.ToLower() == "pageload")
                        rtsLegs.Tabs[0].Selected = true;
                    else
                        rtsLegs.Tabs.FindTabByValue((Convert.ToInt16(hdnLegNum.Value) - 1).ToString()).Selected = true;
                }
                else
                {
                    rtsLegs.Tabs[0].Selected = true;
                }

            }

        }

        #region AJAX Events
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //if (e.Argument.ToString() == "Vendor_TextChanged")
                        //    Vendor_TextChanged(sender, e);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }
        }
        #endregion

        #region Permissions

        /// <summary>
        /// Method to Apply Permissions
        /// </summary>
        private void ApplyPermissions()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                // Check for Page Level Permissions
                CheckAutorization(PermissionViewCRLogistics);

                // Check for Field Level Permissions
                if (IsAuthorized(PermissionViewCRLogistics))
                {
                    // Show read-only form
                    EnableForm(false);

                    // Check Page Controls - Visibility
                    if (IsAuthorized(PermissionAddCRLogistics) || IsAuthorized(PermissionEditCRLogistics))
                    {
                        ControlVisibility("Button", btnSave, true);
                        ControlVisibility("Button", btnCancel, true);
                    }
                    if (IsAuthorized(PermissionDeleteCRLogistics))
                    {
                        ControlVisibility("Button", btnDeleteTrip, true);
                    }

                    // Check Page Controls - Disable/Enable
                    if (Session["CurrentCorporateRequest"] != null)
                    {
                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                        if (CorpRequest != null && CorpRequest.State == CorporateRequestTripEntityState.Added || CorpRequest.State == CorporateRequestTripEntityState.Modified) // Add or Edit Mode
                        {
                            EnableForm(true);

                            if (IsAuthorized(PermissionAddCRLogistics) || IsAuthorized(PermissionEditCRLogistics))
                            {
                                ControlEnable("Button", btnSave, true, "button");
                                ControlEnable("Button", btnCancel, true, "button");
                            }
                            ControlEnable("Button", btnDeleteTrip, false, "button-disable");
                        }
                        else
                        {
                            ControlEnable("Button", btnSave, false, "button-disable");
                            ControlEnable("Button", btnCancel, false, "button-disable");
                            ControlEnable("Button", btnDeleteTrip, false, "button-disable");
                        }
                    }
                    else
                    {
                        ControlEnable("Button", btnSave, false, "button-disable");
                        ControlEnable("Button", btnCancel, false, "button-disable");
                        ControlEnable("Button", btnDeleteTrip, false, "button-disable");
                    }
                }
            }
        }

        /// <summary>
        /// Method for Controls Visibility
        /// </summary>
        /// <param name="ctrlType">Pass Control Type</param>
        /// <param name="ctrlName">Pass Control Name</param>
        /// <param name="isEnable">Pass Visibility Action</param>
        private void ControlVisibility(string ctrlType, Control ctrlName, bool isEnable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ctrlType, ctrlName, isEnable))
            {
                switch (ctrlType)
                {
                    case "Button":
                        Button btn = (Button)ctrlName;
                        btn.Visible = isEnable;
                        break;
                    case "RadPanelBar":
                        RadPanelBar pnl = (RadPanelBar)ctrlName;
                        pnl.Visible = isEnable;
                        break;
                    case "TextBox":
                        TextBox txt = (TextBox)ctrlName;
                        txt.Visible = isEnable;
                        break;
                    default: break;
                }
            }
        }

        /// <summary>
        /// Method for Controls Enable / Disable
        /// </summary>
        /// <param name="ctrlType">Pass Control Type</param>
        /// <param name="ctrlName">Pass Control Name</param>
        /// <param name="isEnable">Pass Enable Action</param>
        /// <param name="isEnable">Pass CSS Class Name</param>
        private void ControlEnable(string ctrlType, Control ctrlName, bool isEnable, string cssClass)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ctrlType, ctrlName, isEnable))
            {
                switch (ctrlType)
                {
                    case "Button":
                        Button btn = (Button)ctrlName;
                        btn.Enabled = isEnable;
                        if (!string.IsNullOrEmpty(cssClass))
                            btn.CssClass = cssClass;
                        break;
                    case "RadPanelBar":
                        RadPanelBar pnl = (RadPanelBar)ctrlName;
                        if (!string.IsNullOrEmpty(cssClass))
                            pnl.CssClass = cssClass;
                        pnl.Enabled = isEnable;
                        break;
                    case "TextBox":
                        TextBox txt = (TextBox)ctrlName;
                        txt.Enabled = isEnable;
                        if (!string.IsNullOrEmpty(cssClass))
                            txt.CssClass = cssClass;
                        break;
                    default: break;
                }
            }
        }

        /// <summary>
        /// Method to Enable and Disable Form
        /// </summary>
        /// <param name="isEnable">Boolean</param>
        protected void EnableForm(bool isEnable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isEnable))
            {
                bool Enable = false;
                tbDepartFBO.Enabled = isEnable;
                tbDepartFBOEmail.Enabled = Enable;
                tbDepartFBOFax.Enabled = Enable;
                tbDepartFBOName.Enabled = Enable;
                tbDepartFBOPhone.Enabled = Enable;

                tbArriveFBO.Enabled = isEnable;
                tbArriveFBOEmail.Enabled = Enable;
                tbArriveFBOFax.Enabled = Enable;
                tbArriveFBOName.Enabled = Enable;
                tbArriveFBOPhone.Enabled = Enable;

                tbCrewHotelCode.Enabled = isEnable;
                tbCrewHotelName.Enabled = isEnable;
                tbCrewHotelPhone.Enabled = isEnable;
                tbCrewHotelRate.Enabled = isEnable;
                tbCrewHotelFax.Enabled = isEnable;
                tbCrewHotelEmail.Enabled = isEnable;

                tbCrewTransportCode.Enabled = isEnable;
                tbCrewTransportName.Enabled = isEnable;
                tbCrewTransportPhone.Enabled = isEnable;
                tbCrewTransportRate.Enabled = isEnable;
                tbCrewTransportFax.Enabled = isEnable;
                tbCrewTransportEmail.Enabled = isEnable;

                tbPaxHotelCode.Enabled = isEnable;
                tbPaxHotelName.Enabled = isEnable;
                tbPaxHotelPhone.Enabled = isEnable;
                tbPaxHotelRate.Enabled = isEnable;
                tbPaxHotelFax.Enabled = isEnable;
                tbPaxHotelEmail.Enabled = isEnable;

                tbPaxTransportCode.Enabled = isEnable;
                tbPaxTransportName.Enabled = isEnable;
                tbPaxTransportPhone.Enabled = isEnable;
                tbPaxTransportRate.Enabled = isEnable;
                tbPaxTransportFax.Enabled = isEnable;
                tbPaxTransportEmail.Enabled = isEnable;

                tbPaxNotes.Enabled = isEnable;

                tbMaintHotelCode.Enabled = isEnable;
                tbMaintHotelName.Enabled = isEnable;
                tbMaintHotelPhone.Enabled = isEnable;
                tbMaintHotelRate.Enabled = isEnable;
                tbMaintHotelFax.Enabled = isEnable;
                tbMaintHotelEmail.Enabled = isEnable;

                tbDeptCateringCode.Enabled = isEnable;
                tbDeptCateringFax.Enabled = isEnable;
                tbDeptCateringName.Enabled = isEnable;
                tbDeptCateringPhone.Enabled = isEnable;
                tbDeptCateringRate.Enabled = isEnable;
                tbDeptCateringFax.Enabled = isEnable;
                tbDeptCateringEmail.Enabled = isEnable;

                tbArriveCateringCode.Enabled = isEnable;
                tbArriveCateringFax.Enabled = isEnable;
                tbArriveCateringName.Enabled = isEnable;
                tbArriveCateringPhone.Enabled = isEnable;
                tbArriveCateringRate.Enabled = isEnable;
                tbArriveCateringFax.Enabled = isEnable;
                tbArriveCateringEmail.Enabled = isEnable;

                //btnAddColumns.Enabled = isEnable;
                //btnCalender.Enabled = isEnable;
                btnArriveFBO.Enabled = isEnable;
                btnCrewHotelCode.Enabled = isEnable;
                btnCrewTransportCode.Enabled = isEnable;
                btnDepartFBO.Enabled = isEnable;
                btnDeptCateringCode.Enabled = isEnable;
                btnArriveCateringCode.Enabled = isEnable;
                btnMaintHotelCode.Enabled = isEnable;
                btnPaxHotelCode.Enabled = isEnable;
                btnPaxTransportCode.Enabled = isEnable;

                if (isEnable)
                {
                    btnArriveFBO.CssClass = "browse-button";
                    btnCrewHotelCode.CssClass = "browse-button";
                    btnCrewTransportCode.CssClass = "browse-button";
                    btnDepartFBO.CssClass = "browse-button";
                    btnDeptCateringCode.CssClass = "browse-button";
                    btnArriveCateringCode.CssClass = "browse-button";
                    btnMaintHotelCode.CssClass = "browse-button";
                    btnPaxHotelCode.CssClass = "browse-button";
                    btnPaxTransportCode.CssClass = "browse-button";
                }
                else
                {
                    btnArriveFBO.CssClass = "browse-button-disabled";
                    btnCrewHotelCode.CssClass = "browse-button-disabled";
                    btnCrewTransportCode.CssClass = "browse-button-disabled";
                    btnDepartFBO.CssClass = "browse-button-disabled";
                    btnDeptCateringCode.CssClass = "browse-button-disabled";
                    btnArriveCateringCode.CssClass = "browse-button-disabled";
                    btnMaintHotelCode.CssClass = "browse-button-disabled";
                    btnPaxHotelCode.CssClass = "browse-button-disabled";
                    btnPaxTransportCode.CssClass = "browse-button-disabled";
                }
            }
        }

        #endregion

        #region "Button Click Events"
        protected void btnCancelYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                        {
                            CRMain oCRMain = new CRMain();
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                oCRMain = (CRMain)Session["CurrentCorporateRequest"];
                                var RetTrip = Service.GetCRHistory(Convert.ToInt64(oCRMain.CustomerID), Convert.ToInt64(oCRMain.CRMainID));
                                StringBuilder HistoryDescription = new System.Text.StringBuilder();
                                string OldDescription = string.Empty;
                                CRHistory TripHistory = new CRHistory();
                                if (oCRMain.TripID != null || oCRMain.TripID != 0)
                                {
                                    oCRMain.CorporateRequestStatus = "X";
                                    var Result = Service.CancelRequest(oCRMain);
                                    if (Result.ReturnFlag == true)
                                    {
                                        if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                        {
                                            //TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                            TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                            TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                            TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                            TripHistory.LastUpdTS = DateTime.UtcNow;
                                            HistoryDescription.AppendLine(string.Format("Existing Trip Cancel Request Submitted To The Change Queue."));
                                            //OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                            TripHistory.LogisticsHistory = HistoryDescription.ToString();
                                            TripHistory.IsDeleted = false;
                                            var ReturnHistory = Service.AddCRHistory(TripHistory);
                                        }
                                        RadWindowManager1.RadAlert("Request canceled successfully.", 360, 50, "Corporate request", "alertCancelCallBackFn", null);
                                    }
                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("Trip Was Never Submitted To Dispatch. Please Use The Delete Button To Delete The Trip.", 360, 50, "Corporate request", "");
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLogistics);
                }
            }
        }
        protected void btnCancellationReq_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CRMain oCRMain = new CRMain();
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            oCRMain = (CRMain)Session["CurrentCorporateRequest"];

                            if (oCRMain.TripID != null)
                            {
                                RadWindowManager1.RadConfirm("Submit Cancel Request To Change Queue ?", "confirmCancelRequestCallBackFn", 400, 30, null, "Corporate request");
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Trip Was Never Submitted To Dispatch. Please Use The Delete Button To Delete The Trip.", 360, 50, "Corporate request", "");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLogistics);
                }
            }
        }
        protected void btnSubmitYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                        {
                            CRMain oCRMain = new CRMain();
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                oCRMain = (CRMain)Session["CurrentCorporateRequest"];
                                var RetTrip = Service.GetCRHistory(Convert.ToInt64(oCRMain.CustomerID), Convert.ToInt64(oCRMain.CRMainID));
                                StringBuilder HistoryDescription = new System.Text.StringBuilder();
                                string OldDescription = string.Empty;
                                CRHistory TripHistory = new CRHistory();
                                ////Prakash
                                //if (oCRMain.CorporateRequestStatus != null && oCRMain.CorporateRequestStatus == "T")
                                //{
                                //    oCRMain.CorporateRequestStatus = "A";
                                //}
                                ////Prakash
                                if (oCRMain.CorporateRequestStatus != "S" && oCRMain.CorporateRequestStatus != "A")
                                {
                                    string status = "W";
                                    if (oCRMain.CorporateRequestStatus != null && oCRMain.CorporateRequestStatus.ToString().Trim() != string.Empty)
                                        status = oCRMain.CorporateRequestStatus;
                                    oCRMain.CorporateRequestStatus = "S";
                                    var Result = Service.UpdateCRMainCorpStatus(oCRMain);
                                    if (Result.ReturnFlag == true)
                                    {
                                        if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                        {
                                            //TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                            TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                            TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                            TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                            TripHistory.LastUpdTS = DateTime.UtcNow;
                                            if (status != "W")
                                                HistoryDescription.AppendLine(string.Format("Existing Trip Request Resubmitted To The Change Queue."));
                                            else
                                                HistoryDescription.AppendLine(string.Format("New Trip Request Submitted To The Change Queue."));
                                            //OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                            TripHistory.LogisticsHistory = HistoryDescription.ToString();
                                            TripHistory.IsDeleted = false;
                                            var ReturnHistory = Service.AddCRHistory(TripHistory);

                                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                            CorpRequest.CorporateRequestStatus = "S";
                                            Session["CurrentCorporateRequest"] = CorpRequest;
                                        }
                                        RadWindowManager1.RadAlert("Request submitted successfully.", 360, 50, "Corporate request", "alertsubmitCallBackFn", null);
                                    }
                                }
                                else if (oCRMain.CorporateRequestStatus == "A")
                                {
                                    oCRMain.CorporateRequestStatus = "S";
                                    var Result = Service.UpdateCRMainCorpStatus(oCRMain);
                                    if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                    {
                                        //TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                        TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                        TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                        TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                        TripHistory.LastUpdTS = DateTime.UtcNow;
                                        HistoryDescription.AppendLine(string.Format("Existing Trip Request Resubmitted To The Change Queue."));
                                        //OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                        TripHistory.LogisticsHistory = HistoryDescription.ToString();
                                        TripHistory.IsDeleted = false;
                                        var ReturnHistory = Service.AddCRHistory(TripHistory);
                                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                        CorpRequest.CorporateRequestStatus = "S";
                                        Session["CurrentCorporateRequest"] = CorpRequest;
                                    }
                                    RadWindowManager1.RadAlert("Request submitted successfully.", 360, 50, "Corporate request", "alertsubmitCallBackFn", null);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }
        protected void btnSubmitReq_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                        {
                            bool DatCheck = true;
                            DateTime CompanyTime = System.DateTime.Now;
                            double CompanySetting = 0;

                            if (UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinHours != null && UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinHours != 0)
                            {
                                CompanySetting = Convert.ToDouble(UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinHours);
                            }
                            CompanyTime = CompanyTime.AddHours(CompanySetting);
                            CRMain oCRMain = new CRMain();
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                oCRMain = (CRMain)Session["CurrentCorporateRequest"];
                                if (oCRMain.CRLegs != null && oCRMain.CRLegs.Count() > 0)
                                {
                                    foreach (CRLeg legs in oCRMain.CRLegs)
                                    {
                                        if (legs.DepartureDTTMLocal != null && legs.LegNUM == 1)
                                        {
                                            if (Convert.ToDateTime(legs.DepartureDTTMLocal) <= CompanyTime)
                                            {
                                                DatCheck = false;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            if (DatCheck == false)
                            {
                                if (UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinWeekend != null && UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinWeekend == true)
                                {
                                    if (System.DateTime.Now.DayOfWeek == DayOfWeek.Saturday || System.DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        DatCheck = true;
                                    }
                                }
                            }

                            if (DatCheck == false && CompanySetting != 0)
                            {
                                RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"confirmExcludeOnSubmitMain();");
                            }
                            else
                            {

                                if (Session["CurrentCorporateRequest"] != null)
                                {
                                    oCRMain = (CRMain)Session["CurrentCorporateRequest"];

                                    if (oCRMain.CorporateRequestStatus != "S")
                                    {
                                        RadWindowManager1.RadConfirm("Submit Travel Request To Change Queue ?", "confirmSubmitCallBackFn", 400, 30, null, "Corporate request");
                                    }
                                    else if (oCRMain.CorporateRequestStatus == "S")
                                    {
                                        RadWindowManager1.RadAlert("Trip Request Is Already Submitted To The Change Queue.", 360, 50, "Corporate request", "");

                                    }
                                }
                            }



                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }
        protected void btnAcknowledge_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                        {
                            CRMain oCRMain = new CRMain();
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                oCRMain = (CRMain)Session["CurrentCorporateRequest"];
                                var RetTrip = Service.GetCRHistory(Convert.ToInt64(oCRMain.CustomerID), Convert.ToInt64(oCRMain.CRMainID));
                                StringBuilder HistoryDescription = new System.Text.StringBuilder();
                                string OldDescription = string.Empty;
                                CRHistory TripHistory = new CRHistory();
                                if (oCRMain.AcknowledgementStatus == "A")
                                {
                                    string strLastuid = oCRMain.LastUpdUID;
                                    var ObjRetval = Service.RequestAcknowledge((long)oCRMain.CRMainID, (long)oCRMain.TripID, (long)oCRMain.CustomerID, strLastuid, Convert.ToDateTime(oCRMain.LastUpdTS));
                                    if (ObjRetval.ReturnFlag)
                                    {
                                        oCRMain = ObjRetval.EntityInfo;
                                        Session["CurrentCorporateRequest"] = oCRMain;
                                        if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                        {
                                            // TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                            TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                            TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                            TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                            TripHistory.LastUpdTS = DateTime.UtcNow;
                                            HistoryDescription.AppendLine(string.Format("Acknowledged Changes Made By Dispatch."));
                                            //OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                            TripHistory.LogisticsHistory = HistoryDescription.ToString();
                                            TripHistory.IsDeleted = false;
                                            var ReturnHistory = Service.AddCRHistory(TripHistory);
                                        }
                                        RadWindowManager1.RadAlert("Request Acknowledged successfully.", 360, 50, "Corporate request", "RedirectPage");

                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLogistics);
                }
            }
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                            if (CorpRequest.CorporateRequestStatus == "W")
                            {
                                Master.Delete(CorpRequest.CRMainID);
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Trip Request has been transferred to Preflight. Please submit Cancel request to Change Queue.", 450, 50, "Corporate request", "");

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                        Master.Cancel(CorpRequest);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }
        }
        protected void btnEditTrip_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Master.EditRequestEvent();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveToCorpRequestSession();
                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                        Master.Save(CorpRequest);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveToCorpRequestSession();
                        Master.Next("Logistics");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameCRLogistics);
                }
            }
        }


        protected string ConvertTenthsToMins(String time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time))
            {

                string result = "00:00";
                int val;
                if (!string.IsNullOrEmpty(time))
                {
                    if (time.IndexOf(".") < 0)

                        time = time + ".0";

                    if (time.IndexOf(".") != -1)
                    {
                        string[] timeArray = time.Split('.');
                        decimal hour = Convert.ToDecimal(timeArray[0]);
                        decimal decimal_min = Convert.ToDecimal(String.Concat("0.", timeArray[1]));
                        decimal decimalOfMin = 0;
                        if (decimal_min > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = (decimal_min * 60 / 100);
                        decimal finalval = Math.Round(hour + decimalOfMin, 2);
                        result = Convert.ToString(finalval).Replace(".", ":");
                        if (result.IndexOf(":") < 0)
                            result = result + ":00";

                        timeArray = result.Split(':');
                        if (timeArray[0].Length == 1)
                        {
                            result = "0" + result;
                        }
                        if (timeArray[1].Length == 1)
                        {
                            result = result + "0";
                        }
                    }
                    else if (int.TryParse(time, out val))
                    {
                        result = time;
                    }
                }

                return result;
            }
        }
        #endregion

        #region "Formattings"
        private string Convert12To24Hrs(string DateAndTime)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DateAndTime))
            {
                return string.Format("{0:" + Master.DatePicker.DateInput.DateFormat + " HH:mm}", Convert.ToDateTime(DateAndTime));
            }
        }
        #endregion
    }
}