﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/CorporateRequest.Master"
    AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="CorporateRequestLegs.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.CorporateRequest.CorporateRequestLegs" %>

<%@ MasterType VirtualPath="~/Framework/Masters/CorporateRequest.Master" %>
<asp:Content ID="Content3" ContentPlaceHolderID="PostFlightHeadContent" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript" src="../../../Scripts/Common.js"></script>
        <script type="text/javascript">

            var Popup = '';
            function openWinAirport(type) {
                var url = '';
                Popup = type;
                if (type == "DEPART") {

                    url = '../../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbDepartsIcao.ClientID%>').value + '&Runway=' + document.getElementById('<%=hdnRunway.ClientID%>').value;
                    var oWnd = radopen(url, 'radAirportPopup');
                    oWnd.add_close(RadPopupClose);
                }
                else {

                    url = '../../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('<%=tbArrivalIcao.ClientID%>').value + '&Runway=' + document.getElementById('<%=hdnRunway.ClientID%>').value;
                    var oWnd = radopen(url, 'radAirportPopup');
                    oWnd.add_close(RadPopupClose);
                }

            }

            function alertsubmitCallBackFn(arg) {
                window.location.reload();
            }

            function alertCancelCallBackFn(arg) {
                window.location.reload();
            }

            function confirmCancelRequestCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnCancelYes.ClientID%>').click();
                }
            }

            function RedirectPage(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnReload.ClientID%>').click();
                }
            }

            function confirmSubmitCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnSubmitYes.ClientID%>').click();
                }
            }

            function confirmDeadHeadLegCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnDeadHeadLegYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnDeadHeadLegNo.ClientID%>').click();
                }
            }

            function confirmExcludeOnSubmitMain(arg) {
                var oWnd = radalert("Warning - Trip Departure Has Exceeded Limits Set By Dispatch.<br> Trip Request Cannot Be Submitted To The Change Queue.<br> Please Contact Dispatch To Request Any Additional Changes.", 440, 110, "Request Alert");
                oWnd.add_close(function () { window.location.reload(); });
            }

            function confirmDeleteLegCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnDeleteLegYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnDeleteLegNo.ClientID%>').click();
                }
            }


            function confirmCopyCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnCopyYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnCopyNo.ClientID%>').click();
                }
            }

            function RadPopupClose(oWnd, args) {
                if (Popup == 'DEPART') {

                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbDepartCity.ClientID%>").value = arg.CityName;
                            document.getElementById("<%=tbDepartsIcao.ClientID%>").value = arg.ICAO;
                            document.getElementById("<%=hdDepart.ClientID%>").value = arg.AirportID;
                            if (arg.AirportName != null)
                                document.getElementById("<%=tbDepartAirportName.ClientID%>").innerHTML = arg.AirportName;

                            if (arg.ICAO != null) {
                                document.getElementById("<%=lbcvDepartIcao.ClientID%>").innerHTML = "";
                            }
                            var step = "tbDepart_TextChanged";
                            var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }
                        else {
                            document.getElementById("<%=tbDepartCity.ClientID%>").value = "";
                            document.getElementById("<%=tbDepartsIcao.ClientID%>").value = "";
                            document.getElementById("<%=tbDepartAirportName.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdDepart.ClientID%>").value = "";
                        }
                    }
                }
                if (Popup == 'ARRIVAL') {
                    var combo = $find("<%= tbArrivalIcao.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbArrivalCity.ClientID%>").value = arg.CityName;
                            document.getElementById("<%=tbArrivalIcao.ClientID%>").value = arg.ICAO;
                            document.getElementById("<%=hdArrival.ClientID%>").value = arg.AirportID;
                            if (arg.AirportName != null)
                                document.getElementById("<%=lbArrivalAirportName.ClientID%>").innerHTML = arg.AirportName;

                            if (arg.ICAO != null) {
                                document.getElementById("<%=lbcvArrivalIcao.ClientID%>").innerHTML = "";
                            }
                            var step = "tbArrival_TextChanged";
                            var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }
                        else {
                            document.getElementById("<%=tbArrivalCity.ClientID%>").value = "";
                            document.getElementById("<%=tbArrivalIcao.ClientID%>").value = "";
                            document.getElementById("<%=lbArrivalAirportName.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdArrival.ClientID%>").value = "";
                        }
                    }

                }
            }

            var Mode = '';
            function openWinAirportCity(type) {

                var url = '';
                Mode = type;
                if (type == "DEPART") {
                    url = '../CorporateRequest/AirportCityPopup.aspx?AirportCityName=' + document.getElementById('<%=tbDepartCity.ClientID%>').value;
                    var oWnd = radopen(url, 'radAirportCityPopup');
                    oWnd.add_close(RadCityPopupClose);
                }
                else {
                    url = '../CorporateRequest/AirportCityPopup.aspx?AirportCityName=' + document.getElementById('<%=tbArrivalCity.ClientID%>').value;
                    var oWnd = radopen(url, 'radAirportCityPopup');
                    oWnd.add_close(RadCityPopupClose);
                }

            }

            function RadCityPopupClose(oWnd, args) {

                if (Mode == 'DEPART') {

                    var combo = $find("<%= tbDepartCity.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {

                            document.getElementById("<%=tbDepartCity.ClientID%>").value = arg.CityName;
                            document.getElementById("<%=tbDepartsIcao.ClientID%>").value = arg.ICAO;
                            document.getElementById("<%=hdDepart.ClientID%>").value = arg.AirportID;
                            if (arg.AirportName != null)
                                document.getElementById("<%=tbDepartAirportName.ClientID%>").innerHTML = arg.AirportName;
                            if (arg.ICAO != null) {
                                document.getElementById("<%=lbcvDepartIcao.ClientID%>").innerHTML = "";
                            }

                            var step = "tbDepart_TextChanged";
                            var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                            return false;
                        }
                        else {
                            document.getElementById("<%=tbDepartCity.ClientID%>").value = "";
                            document.getElementById("<%=tbDepartsIcao.ClientID%>").value = "";
                            document.getElementById("<%=tbDepartAirportName.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdDepart.ClientID%>").value = "";
                        }
                    }
                }
                if (Mode == 'ARRIVAL') {
                    var combo = $find("<%= tbArrivalCity.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbArrivalCity.ClientID%>").value = arg.CityName;
                            document.getElementById("<%=tbArrivalIcao.ClientID%>").value = arg.ICAO;
                            document.getElementById("<%=hdArrival.ClientID%>").value = arg.AirportID;
                            if (arg.AirportName != null)
                                document.getElementById("<%=lbArrivalAirportName.ClientID%>").innerHTML = arg.AirportName;

                            if (arg.ICAO != null) {
                                document.getElementById("<%=lbcvArrivalIcao.ClientID%>").innerHTML = "";
                            }


                            var step = "tbArrival_TextChanged";
                            var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                            return false;
                        }
                        else {
                            document.getElementById("<%=tbArrivalCity.ClientID%>").value = "";
                            document.getElementById("<%=tbArrivalIcao.ClientID%>").value = "";
                            document.getElementById("<%=lbArrivalAirportName.ClientID%>").innerHTML = "";
                            document.getElementById("<%=hdArrival.ClientID%>").value = "";
                        }
                    }

                }
            }





            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }


            function openWin(radWin) {
                var url = '';

                if (radWin == "radPaxInfoPopup") {
                    url = "../../Settings/People/PassengerRequestorsPopup.aspx?PassengerRequestorCD=" + document.getElementById('<%=tbPassenger.ClientID%>').value;
                }
                else if (radWin == "radDepartmentPopup") {
                    url = "../../Settings/Company/DepartmentAuthorizationPopup.aspx?DepartmentCD=" + document.getElementById('<%=tbDepartment.ClientID%>').value;
                }
                else if (radWin == "rdCopyTrip") {
                    url = '/Views/Transactions/CorporateRequest/CopyRequest.aspx';
                }
                else if (radWin == "RadRetrievePopup") {
                    url = '../../Transactions/CorporateRequest/CorporateRequestSearch.aspx';
                }
                else if (radWin == "radAuthorizationPopup") {

                    if (document.getElementById("<%=hdDepartment.ClientID%>").value != "") {
                        url = "../../Settings/Company/AuthorizationPopup.aspx?AuthorizationCD=" + document.getElementById('<%=tbAuthorizationCode.ClientID%>').value + "&deptId=" + document.getElementById("<%=hdDepartment.ClientID%>").value;
                    }
                    else {
                        var OAlert = radalert("Please enter the Department Code before entering the Authorization Code.", 330, 100, "Corporate Legs", "");
                        OAlert.add_close(function () { document.getElementById("<%=tbDepartment.ClientID%>").focus(); });
                    }

                    if (document.getElementById("<%=hdDepartment.ClientID%>").value != "") {
                        url = "../../Settings/Company/AuthorizationPopup.aspx?AuthorizationCD=" + document.getElementById('<%=tbAuthorizationCode.ClientID%>').value + "&deptId=" + document.getElementById("<%=hdDepartment.ClientID%>").value
                    }
                }
                else if (radWin == "radFlightCategoryPopup") {
                    url = "../../Settings/Fleet/FlightCategoryPopup.aspx?FlightCatagoryCD=" + document.getElementById('<%=tbFlightCategoryCode.ClientID%>').value;
                }
                else if (radWin == "rdChangeQueue") {
                    url = '../../Transactions/CorporateRequest/CorporateChangeQueue.aspx';
                }
                else if (radWin == "rdHistory") {
                    url = "../../Transactions/CRLogisticsHistory.aspx";
                }
                if (url != "") {
                    var oWnd = radopen(url, radWin);
                }
            }






            function OnClientDeptClose(oWnd, args) {
                var combo = $find("<%= tbDepartment.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbDepartment.ClientID%>").value = arg.DepartmentCD;
                        document.getElementById("<%=lbDepartmentName.ClientID%>").innerHTML = arg.DepartmentName;
                        document.getElementById("<%=hdDepartment.ClientID%>").value = arg.DepartmentID;
                        if (arg.DepartmentCD != null)
                            document.getElementById("<%=lbcvDepartmentName.ClientID%>").innerHTML = "";
                        var step = "tbDepartmentId_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbDepartment.ClientID%>").value = "";
                        document.getElementById("<%=lbDepartmentName.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdDepartment.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientAuthClose(oWnd, args) {
                var combo = $find("<%= tbAuthorizationCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbAuthorizationCode.ClientID%>").value = arg.AuthorizationCD;

                        document.getElementById("<%=hdnAuthorizationId.ClientID%>").value = arg.AuthorizationID;
                        if (arg.AuthorizationCD != null)
                            document.getElementById("<%=lbcvAuthID.ClientID%>").innerHTML = "";
                        var step = "tbAuthorization_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbAuthorizationCode.ClientID%>").value = "";

                        document.getElementById("<%=hdnAuthorizationId.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientRqstrClose(oWnd, args) {
                var combo = $find("<%= tbPassenger.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbPassenger.ClientID%>").value = arg.PassengerRequestorCD;
                        document.getElementById("<%=lbPassengerName.ClientID%>").innerHTML = arg.PassengerName;
                        document.getElementById("<%=hdnPaxId.ClientID%>").value = arg.PassengerRequestorID;
                        if (arg.PassengerRequestorCD != null)
                            document.getElementById("<%=lbcvPaxValidator.ClientID%>").innerHTML = "";
                        var step = "tbPassenger_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbPassenger.ClientID%>").value = "";
                        document.getElementById("<%=lbPassengerName.ClientID%>").innerHTML = "";
                        document.getElementById("<%=hdnPaxId.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function OnClientCategoryClose(oWnd, args) {
                var combo = $find("<%= tbFlightCategoryCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbFlightCategoryCode.ClientID%>").value = arg.FlightCatagoryCD;
                        document.getElementById("<%=hdCategory.ClientID%>").value = arg.FlightCategoryID;
                        if (arg.FlightCatagoryCD != null)
                            document.getElementById("<%=lbcvflightCategory.ClientID%>").innerHTML = "";
                        var step = "tbFlightCategory_TextChanged";
                        var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbFlightCategoryCode.ClientID%>").value = "";
                        document.getElementById("<%=hdCategory.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function CloseRadWindow(arg) {
                GetRadWindow().Close();

            }

            function ValidatetbAirportCity(ctrlID, e) {

                var step = "tbFlightCategory_TextChanged";
                var ajaxManager = $find("<%= Master.RadAjaxManagerMaster.ClientID %>");
                if (step) {
                    ajaxManager.ajaxRequest(step);
                }
            }

            function ValidateDate(control) {
                var MinDate = new Date("01/01/1900");
                var MaxDate = new Date("12/31/2100");
                var SelectedDate;
                if (control.value != "") {
                    SelectedDate = new Date(control.value);
                    if ((SelectedDate < MinDate) || (SelectedDate > MaxDate)) {
                        control.value = "";
                        alert("Please enter/select Date between 01/01/1900 and 12/31/2100");

                    }
                }
                return false;
            }

            function OnClientChangeQueueClose() {
                window.location.reload();
            }

            function confirmDeleteLegCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnDeleteLegYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnDeleteLegNo.ClientID%>').click();
                }
            }
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="radDepartmentPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientDeptClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radAuthorizationPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientAuthClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AuthorizationPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radDepartmentCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientDeptClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radAuthorizationCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientAuthClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AuthorizationPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                    KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    Width="400px" Height="236px" Title="Copy Trip" NavigateUrl="~/Views/Transactions/CorporateRequest/CopyRequest.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" VisibleStatusbar="false"
                    NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" Height="800px" Width="1100px"
                    ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                    VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radAirportCityPopup" runat="server" Height="400px" Width="900px"
                    ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                    VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/CorporateRequest/AirportCityPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientRqstrClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                    NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx" ReloadOnShow="true"
                    KeepInScreenBounds="true" Modal="true" Behaviors="close" OnClientClose="OnClientRqstrClose"
                    VisibleStatusbar="false">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radFlightCategoryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCategoryClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radFlightCategoryCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCategoryClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadRetrievePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    NavigateUrl="~/Views/Transactions/CorporateRequest/CorporateRequestSearch.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdChangeQueue" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientChangeQueueClose" AutoSize="true" KeepInScreenBounds="true"
                    Behaviors="Close" Modal="true" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/CorporateRequest/CorporateChangeQueue.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                    KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    Title="History" NavigateUrl="~/Views/Transactions/CRLogisticsHistory.aspx">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <div style="float: right; text-align: right; width: 314px; padding-top: 5px;">
            <asp:HiddenField ID="hdnLeg" runat="server" />
            <asp:Button ID="btnAddLeg1" runat="server" ToolTip="Add New Leg" Text="Add Leg" OnClick="btnAddLeg_Click"
                CssClass="ui_nav" />
            <asp:Button ID="btnInsertLeg" runat="server" ToolTip="Insert New Leg Prior to the Highlighted Leg" Text="Insert Leg"
                OnClick="btnInsertLeg_Click" CssClass="ui_nav" />
            <asp:Button ID="btnDeleteLeg" runat="server" ToolTip="Delete Existing Leg" Text="Delete Leg"
                OnClick="btnDeleteLeg_Click" CssClass="ui_nav" />
            <asp:HiddenField ID="previousTabHidden" runat="Server" />
        </div>
        <telerik:RadTabStrip Skin="Windows7" AutoPostBack="true" Width="718px" ID="rtsLegs"
            OnTabClick="rtsLegs_TabClick" runat="server" ReorderTabsOnSelect="true">
            <Tabs>
                <telerik:RadTab Text="Leg1" Selected="true">
                </telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
        <div style="width: 718px;">
            <table width="100%">
                <tr>
                    <td>
                        <div style="width: 710px; float: left;">
                            <fieldset>
                                <table width="100%" cellpadding="1" cellspacing="2">
                                    <tr>
                                        <td class="tdLabel60">
                                            <asp:Label ID="lbDepartCity" Font-Bold="true" runat="server" Text="Departure"></asp:Label>
                                        </td>
                                        <td class="tdLabel200">
                                            <asp:TextBox ID="tbDepartCity" MaxLength="25" runat="server" CssClass="text150" OnTextChanged="tbDepartCity_TextChanged"
                                                AutoPostBack="true">
                                            </asp:TextBox>
                                            <asp:Button runat="server" CssClass="browse-button" ToolTip="Display Airports" ID="btnClosestIcao"
                                                OnClientClick="javascript:openWinAirportCity('DEPART');return false;" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbDepartsIcao" MaxLength="4" runat="server" CssClass="text150" OnTextChanged="tbDepart_TextChanged"
                                                AutoPostBack="true">
                                            </asp:TextBox>
                                            <asp:HiddenField ID="hdDepart" runat="server" />
                                            <asp:Button runat="server" CssClass="browse-button" ToolTip="Display Airports" ID="btnDepartsICAO"
                                                OnClientClick="javascript:openWinAirport('DEPART');return false;" />
                                            <%--<asp:Button runat="server" CssClass="browse-button" ToolTip="Display Departing Airports Alerts"
                                                ID="btnDepartAirportAlerts" OnClientClick="javascript:openWinAirport('DEPART');return false;" />--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="left">
                                            <asp:Label ID="tbDepartAirportName" CssClass="input_no_bg" runat="server"></asp:Label>
                                            <asp:Label ID="lbcvDepartCity" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                            <asp:Label ID="lbcvDepartIcao" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbArrivalCity" Font-Bold="true" runat="server" Text="Arrival"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbArrivalCity" MaxLength="25" runat="server" CssClass="text150"
                                                OnTextChanged="tbArrivalCity_TextChanged" AutoPostBack="true">
                                            </asp:TextBox>
                                            <asp:Button runat="server" CssClass="browse-button" ToolTip="Display Airports" ID="btnArrivalAirportCity"
                                                OnClientClick="javascript:openWinAirportCity('ARRIVAL');return false;" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbArrivalIcao" MaxLength="4" runat="server" CssClass="text150" OnTextChanged="tbArrival_TextChanged"
                                                AutoPostBack="true">
                                            </asp:TextBox>
                                            <asp:HiddenField ID="hdArrival" runat="server" />
                                            <asp:Button runat="server" CssClass="browse-button" ToolTip="Display Airports" ID="btnClosestArrivalICAO"
                                                OnClientClick="javascript:openWinAirport('ARRIVAL');return false;" />
                                            <%--<asp:Button runat="server" CssClass="browse-button" ToolTip="Display Arrival Airports Alerts"
                                                ID="btnArrivalAirportAlerts" OnClientClick="javascript:openWinAirport('DEPART');return false;" />--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="left">
                                            <asp:Label ID="lbcvArrivalCity" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                            <asp:Label ID="lbArrivalAirportName" MaxLength="12" CssClass="input_no_bg" runat="server"></asp:Label>
                                            <asp:Label ID="lbcvArrivalIcao" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 710px; float: left;">
                            <div style="width: 30%; float: left; vertical-align: top;">
                                <table width="100%" cellpadding="1" cellspacing="2">
                                    <tr>
                                        <td>
                                            <fieldset>
                                                <legend>Departure</legend>
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left" colspan="2" class="mnd_text">
                                                                        Local
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top" class="tdLabel80">
                                                                        <asp:TextBox ID="tbDepartsLocal" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                            onclick="showPopup(this, event);" OnTextChanged="tbDepartLocalDate_TextChanged"
                                                                            AutoPostBack="true" onkeydown="return tbDate_OnKeyDown(this, event);" CssClass="text70"
                                                                            runat="server" onBlur="parseDate(this, event);"></asp:TextBox>
                                                                    </td>
                                                                    <td class="pr_leg_radmask">
                                                                        <telerik:RadMaskedTextBox ID="rmbDepartsLocal" runat="server" OnTextChanged="tbDepartLocalDate_TextChanged"
                                                                            AutoPostBack="true" SelectionOnFocus="SelectAll" Mask="<0..99>:<0..99>">
                                                                        </telerik:RadMaskedTextBox>
                                                                        <asp:DropDownList runat="server" ID="DepTimeForm" AutoPostBack="true" OnSelectedIndexChanged="tbDepartLocalDate_TextChanged">
                                                                            <asp:ListItem Text="AM" Value="1"></asp:ListItem>
                                                                            <asp:ListItem Text="PM" Value="2"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="width: 70%; float: right;">
                                <fieldset>
                                    <table width="100%" cellpadding="1" cellspacing="2">
                                        <tr>
                                            <td class="tdLabel110">
                                                Requestor
                                            </td>
                                            <td class="tdLabel100">
                                                <asp:TextBox ID="tbPassenger" runat="server" CssClass="text60" MaxLength="5" OnTextChanged="tbPassenger_TextChanged"
                                                    AutoPostBack="true"></asp:TextBox>
                                                <asp:HiddenField ID="hdnPaxId" runat="server" />
                                                <asp:Button runat="server" ToolTip="View Requestors" ID="btnPassenger" OnClientClick="javascript:openWin('radPaxInfoPopup');return false;" />
                                            </td>
                                            <td class="tdLabel30">
                                                <%-- <asp:Button ID="btnPassengerCopy" CssClass="copy-icon" runat="server" ToolTip="Copy Passenger to all Legs"
                                                    OnClick="btnPassengerCopy_Click" />--%>
                                            </td>
                                            <td>
                                                Flight Category
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbFlightCategoryCode" runat="server" CssClass="text60" MaxLength="4"
                                                    OnTextChanged="tbFlightCategory_TextChanged" AutoPostBack="true">
                                                </asp:TextBox>
                                                <asp:HiddenField ID="hdCategory" runat="server" />
                                                <asp:Button runat="server" CssClass="browse-button" ToolTip="Display Flight Category"
                                                    ID="btnFlightCategory" OnClientClick="javascript:openWin('radFlightCategoryPopup');return false;" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnCategoryLegs" CssClass="copy-icon" runat="server" ToolTip="Copy Category to all Legs"
                                                    OnClick="btnCategoryLegs_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="left">
                                                <asp:Label ID="lbPassengerName" CssClass="input_no_bg" runat="server" Visible="true"></asp:Label>
                                                <asp:Label ID="lbcvPaxValidator" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                            </td>
                                            <td colspan="3" align="left">
                                                <asp:Label ID="lbFlightCategoryName" CssClass="input_no_bg" runat="server" Visible="true"></asp:Label>
                                                <asp:Label ID="lbcvflightCategory" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Department
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbDepartment" runat="server" CssClass="text60" MaxLength="25" OnTextChanged="tbDepartmentId_TextChanged"
                                                    AutoPostBack="true">
                                                </asp:TextBox>
                                                <asp:HiddenField ID="hdDepartment" runat="server" />
                                                <asp:Button runat="server" CssClass="browse-button" ToolTip="Display Department"
                                                    ID="btnDepartment" OnClientClick="javascript:openWin('radDepartmentPopup');return false;" />
                                            </td>
                                            <td>
                                                <%-- <asp:Button ID="btnDepartmentCopy" CssClass="copy-icon" runat="server" ToolTip="Copy Department to all Legs"
                                                    OnClick="btnDepartmentCopy_Click" />--%>
                                            </td>
                                            <td class="tdLabel100">
                                                Authorization
                                            </td>
                                            <td class="tdLabel100">
                                                <asp:TextBox ID="tbAuthorizationCode" runat="server" CssClass="text60" MaxLength="8"
                                                    OnTextChanged="tbAuthorization_TextChanged" AutoPostBack="true">
                                                </asp:TextBox>
                                                <asp:HiddenField ID="hdnAuthorizationId" runat="server" />
                                                <asp:Button runat="server" CssClass="browse-button" ToolTip="Display Authorization"
                                                    ID="btnAuthorizationCode" OnClientClick="javascript:openWin('radAuthorizationPopup');return false;" />
                                            </td>
                                            <td>
                                                <%--<asp:Button ID="btnAuthorizationCopy" CssClass="copy-icon" runat="server" ToolTip="Copy Authorization to all Legs"
                                                    OnClick="btnAuthorizationCopy_Click" />--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="left">
                                                <asp:Label ID="lbDepartmentName" CssClass="input_no_bg" runat="server" Visible="true"></asp:Label>
                                                <asp:Label ID="lbcvDepartmentName" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                            </td>
                                            <td colspan="3" align="left">
                                                <asp:Label ID="lbAuthName" CssClass="input_no_bg" runat="server" Visible="true"></asp:Label>
                                                <asp:Label ID="lbcvAuthID" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 710px; float: left;">
                            <div style="width: 30%; float: left;">
                                <table width="100%" cellpadding="1" cellspacing="2">
                                    <tr>
                                        <td>
                                            <fieldset>
                                                <legend>Arrival</legend>
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left" colspan="2" class="mnd_text">
                                                                        Local
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top" class="tdLabel80">
                                                                        <asp:TextBox ID="tbArrivalLocal" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                            onclick="showPopup(this, event);" OnTextChanged="tbArrivalLocalDate_TextChanged"
                                                                            AutoPostBack="true" onkeydown="return tbDate_OnKeyDown(this, event);" CssClass="text70"
                                                                            runat="server" onBlur="parseDate(this, event);"></asp:TextBox>
                                                                    </td>
                                                                    <td class="pr_leg_radmask">
                                                                        <telerik:RadMaskedTextBox ID="rmbArrivalLocalTime" runat="server" OnTextChanged="tbArrivalLocalDate_TextChanged"
                                                                            AutoPostBack="true" SelectionOnFocus="SelectAll" Mask="<0..99>:<0..99>">
                                                                        </telerik:RadMaskedTextBox>
                                                                        <asp:DropDownList runat="server" ID="ArrTimeForm" AutoPostBack="true" OnTextChanged="tbArrivalLocalDate_TextChanged">
                                                                            <asp:ListItem Text="AM" Value="1"></asp:ListItem>
                                                                            <asp:ListItem Text="PM" Value="2"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="width: 70%; float: right;">
                                <fieldset>
                                    <table width="100%" cellpadding="1" cellspacing="2">
                                        <tr>
                                            <td class="tdLabel110">
                                                <asp:Label ID="lbMiles" runat="server"></asp:Label>
                                            </td>
                                            <td class="tdLabel90">
                                                <asp:TextBox ID="tbDistance" runat="server" ReadOnly="true" CssClass="text60 charter_readonly_textbox"
                                                    MaxLength="4" AutoPostBack="true">
                                                </asp:TextBox>
                                            </td>
                                            <td class="tdLabel130">
                                                <asp:CheckBox ID="chkPrivate" AutoPostBack="true" OnCheckedChanged="chkPrivate_CheckedChanged"
                                                    runat="server" Text="Private" />
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                No. of Passengers
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbPaxNumbers" runat="server" CssClass="text60" MaxLength="4">
                                                </asp:TextBox>
                                            </td>
                                            <td>
                                                Elapsed Time (HH:MM)
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbElapsedTime" runat="server" ReadOnly="true" CssClass="text60 charter_readonly_textbox"
                                                    MaxLength="4" AutoPostBack="true">
                                                </asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="left">
                                            </td>
                                            <td colspan="2" align="left">
                                                <%-- <asp:Label ID="Label7" CssClass="alert-text" runat="server" Visible="true"></asp:Label>--%>
                                                <asp:Label ID="Label8" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div style="width: 718px; text-align: right; padding: 5px 0 0 0;">
            <table width="718px" cellspacing="6">
                <tr>
                    <td align="left">
                        <asp:Button ID="btnCancellationReq" runat="server" ToolTip="Cancellation Request"
                            Text="Cancellation Request" OnClick="btnCancellationReq_Click" CssClass="button" />
                        <asp:Button ID="btnSubmitReq" runat="server" ToolTip="Submit Request" Text="Submit Request"
                            OnClick="btnSubmitReq_Click" CssClass="button" />
                        <asp:Button ID="btnAcknowledge" runat="server" ToolTip="Acknowledge" Text="Acknowledge"
                            OnClick="btnAcknowledge_Click" CssClass="button" />
                    </td>
                    <td align="right">
                        <asp:Button ID="btnDeleteTrip" runat="server" ToolTip="Delete Selected Record" Text="Delete Request"
                            OnClick="btnDelete_Click" CssClass="button" />
                        <asp:Button ID="btnEditTrip" runat="server" ToolTip="Edit Selected Record" Text="Edit Request"
                            OnClick="btnEditTrip_Click" CssClass="button" />
                        <asp:Button ID="btnCancel" runat="server" ToolTip="Cancel All Changes" Text="Cancel"
                            OnClick="btnCancel_Click" CssClass="button" />
                        <asp:Button ID="btnSave" runat="server" ToolTip="Save Changes" Text="Save" OnClick="btnSave_Click"
                            ValidationGroup="Save" CssClass="button" />
                        <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" CssClass="button" />
                    </td>
                </tr>
            </table>
        </div>
        <table id="tblHidden" style="display: none;">
            <tr>
                <td>
                    <asp:HiddenField ID="hdnDepartCity" runat="server" />
                    <asp:HiddenField ID="hdnArrivalcity" runat="server" />
                    <asp:HiddenField ID="hdClient" runat="server" />
                    <asp:HiddenField ID="hdnMiles" runat="server" />
                    <asp:HiddenField ID="hdnCountryID" runat="server" />
                    <asp:HiddenField ID="hdTimeDisplayTenMin" runat="server" />
                    <asp:HiddenField ID="hdHomebaseAirport" runat="server" />
                    <asp:HiddenField ID="hdnRunway" runat="server" />
                    <%-- <asp:HiddenField ID="hdnAutoCalDUtcDate" runat="server" />
                    <asp:HiddenField ID="hdnAutoCalDUtcTime" runat="server" />
                    <asp:HiddenField ID="hdnAutoCalDHomeDate" runat="server" />
                    <asp:HiddenField ID="hdnAutoCalDHomeTime" runat="server" />
                    <asp:HiddenField ID="hdnAutoCalAUtcDate" runat="server" />
                    <asp:HiddenField ID="hdnAutoCalAUtcTime" runat="server" />
                    <asp:HiddenField ID="hdnAutoCalAHomeDate" runat="server" />
                    <asp:HiddenField ID="hdnAutoCalAHomeTime" runat="server" />
                    <asp:HiddenField ID="hdnAutoCalUtcDate" runat="server" />
                    <asp:HiddenField ID="hdnAutoCalUtcTime" runat="server" />
                    <asp:HiddenField ID="hdnAutoCalHomeDate" runat="server" />
                    <asp:HiddenField ID="hdnAutoCalHomeTime" runat="server" />--%>
                    <asp:HiddenField ID="hdCrewRules" runat="server" />
                    <asp:HiddenField ID="hdnDomestic" runat="server" />
                    <asp:HiddenField ID="hdnPower" runat="server" />
                    <asp:HiddenField ID="hdnWindReliability" runat="server" />
                    <asp:HiddenField ID="hdnLandingBIAS" runat="server" />
                    <asp:HiddenField ID="hdnTakeoffBIAS" runat="server" />
                    <asp:HiddenField ID="hdnTrueAirSpeed" runat="server" />
                    <asp:HiddenField ID="hdAircraft" runat="server" />
                    <asp:HiddenField ID="hdHomeCategory" runat="server" />
                    <asp:HiddenField ID="hdnWindBoeingTable" runat="server" />
                    <asp:HiddenField ID="hdnFlightHours" runat="server" />
                    <asp:HiddenField ID="hdnRestHours" runat="server" />
                    <asp:HiddenField ID="hdnCROverRide" runat="server" />
                    <asp:HiddenField ID="hdnFAR" runat="server" />
                    <asp:HiddenField ID="hdnDutyHours" runat="server" />
                    <asp:HiddenField ID="hdnFlightPurpose" runat="server" />
                    <asp:HiddenField ID="hdAccountnum" runat="server" />
                    <asp:HiddenField ID="hdIsDepartAuthReq" runat="server" />
                    <asp:HiddenField ID="hdnDeadCategory" runat="server" />
                    <asp:HiddenField ID="hdSetFarRules" runat="server" />
                    <asp:HiddenField ID="hdSetCrewRules" runat="server" />
                    <asp:HiddenField ID="tbCrewRules" runat="server" />
                    <asp:HiddenField ID="lbFar" runat="server" />
                    <asp:HiddenField ID="tbTAS" runat="server" />
                    <asp:HiddenField ID="tbBias" runat="server" />
                    <asp:HiddenField ID="tbLandBias" runat="server" />
                    <asp:HiddenField ID="hdSetWindReliability" runat="server" />
                    <asp:HiddenField ID="tbWind" runat="server" />
                    <asp:HiddenField ID="lbTotalDuty" runat="server" />
                    <asp:HiddenField ID="lbTotalFlight" runat="server" />
                    <asp:HiddenField ID="lbRest" runat="server" />
                    <asp:TextBox ID="tbUtcDate" CssClass="text70" onkeydown="return tbDate_OnKeyDown(this, event);"
                        AutoPostBack="true" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                        onclick="showPopup(this, event);" onchange="parseDate(this, event); javascript:return ValidateDate(this);"
                        runat="server"></asp:TextBox>
                    <telerik:RadMaskedTextBox ID="rmtUtctime" AutoPostBack="true" runat="server" SelectionOnFocus="SelectAll"
                        Mask="<0..99>:<0..99>">
                    </telerik:RadMaskedTextBox>
                    <asp:TextBox ID="tbArrivalUtcDate" AutoPostBack="true" onkeydown="return tbDate_OnKeyDown(this, event);"
                        CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                        onchange="parseDate(this, event);javascript:return ValidateDate(this);" onclick="showPopup(this, event);"
                        runat="server"></asp:TextBox>
                    <telerik:RadMaskedTextBox ID="rmtArrivalUtctime" AutoPostBack="true" runat="server"
                        SelectionOnFocus="SelectAll" Mask="<0..99>:<0..99>">
                    </telerik:RadMaskedTextBox>
                    <asp:TextBox ID="tbHomeDate" AutoPostBack="true" onkeydown="return tbDate_OnKeyDown(this, event);"
                        onKeyPress="return fnAllowNumericAndChar(this, event,'/')" onclick="showPopup(this, event)"
                        CssClass="text70" onchange="parseDate(this, event);javascript:return ValidateDate(this);"
                        runat="server"></asp:TextBox>
                    <telerik:RadMaskedTextBox ID="rmtHomeTime" AutoPostBack="true" runat="server" SelectionOnFocus="SelectAll"
                        Mask="<0..99>:<0..99>">
                    </telerik:RadMaskedTextBox>
                    <asp:TextBox ID="tbArrivalHomeDate" onkeydown="return tbDate_OnKeyDown(this, event);"
                        AutoPostBack="true" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                        onchange="parseDate(this, event);javascript:return ValidateDate(this);" onclick="showPopup(this, event)"
                        runat="server"></asp:TextBox>
                    <telerik:RadMaskedTextBox ID="rmtArrivalHomeTime" AutoPostBack="true" runat="server"
                        SelectionOnFocus="SelectAll" Mask="<0..99>:<0..99>">
                    </telerik:RadMaskedTextBox>
                    <asp:Button ID="btnCopyYes" runat="server" Text="Button" OnClick="btnCopyYes_Click" />
                    <asp:Button ID="btnCopyNo" runat="server" Text="Button" OnClick="btnCopyNo_Click" />
                    <asp:Button ID="btnDeleteLegYes" runat="server" Text="Button" OnClick="btnDeleteLegYes_Click" />
                    <asp:Button ID="btnDeleteLegNo" runat="server" Text="Button" OnClick="btnDeleteLegNo_Click" />
                    <asp:Button ID="btnSubmitYes" runat="server" Text="Button" OnClick="btnSubmitYes_Click" />
                    <asp:Button ID="btnReload" runat="server" Text="Button" OnClick="btnReload_Click"
                        Style="display: none;" />
                    <asp:Button ID="btnDeadHeadLegYes" runat="server" Text="Button" OnClick="btnDeadHeadLegYes_Click" />
                    <asp:Button ID="btnDeadHeadLegNo" runat="server" Text="Button" OnClick="btnDeadHeadLegNo_Click" />
                    <asp:Button ID="btnCancelYes" runat="server" Text="Button" OnClick="btnCancelYes_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
