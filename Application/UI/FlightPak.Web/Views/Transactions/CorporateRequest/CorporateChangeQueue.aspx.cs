﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using Telerik.Web.UI;
using System.Drawing;
using FlightPak.Web.PostflightService;
using FlightPak.Web.CorporateRequestService;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Text.RegularExpressions;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;


//Added for Other crew duty log popup - Ramesh
using FlightPak.Web.Framework.Helpers;
using System.Text;
using FlightPak.Web.PreflightService;

namespace FlightPak.Web.Views.Transactions.CorporateRequest
{
    public partial class CorporateChangeQueue : BaseSecuredPage
    {
        private ExceptionManager exManager;
        public string DateFormat = "MM/dd/yyyy";
        protected void Page_Load(object sender, EventArgs e)
        {
            bool TransferVisible = IsAuthorized(Permission.CorporateRequest.ViewCRTransfer);
            bool DenyVisible = IsAuthorized(Permission.CorporateRequest.ViewCRDeny);
            bool MarkVisible = IsAuthorized(Permission.CorporateRequest.ViewCRMarkAccepted);

            btnTransfer.Visible = TransferVisible;
            btnDenyRequest.Visible = DenyVisible;
            btnMarkAccepted.Visible = MarkVisible;

            if (UserPrincipal != null)
            {
                if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                {
                    DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();

                }
                else
                {
                    DateFormat = "MM/dd/yyyy";

                }
                //Company Profile and User Setting to approve button visibility starts here
                btnApproved.Visible = false;
                if (UserPrincipal.Identity._fpSettings._IsCorpReqActivateApproval != null)
                {
                    if (UserPrincipal.Identity._fpSettings._IsCorpReqActivateApproval == true)
                    {
                        //if (UserPrincipal.Identity._isCorpRequestApproval != null)
                        //{
                        //    if (UserPrincipal.Identity._isCorpRequestApproval == true)
                        //    {
                        btnApproved.Visible = UserPrincipal.Identity._fpSettings._IsCorpReqActivateApproval;
                        //    }
                        //}
                    }
                }
                //Company Profile and User Setting to approve button visibility starts here
                if (!IsPostBack)
                {
                    if (UserPrincipal.Identity._fpSettings._IsDeactivateHomeBaseFilter == false)
                    {
                        chkHomebase.Checked = true;
                    }
                }
                //// Grid Control could be ajaxified when the page is initially loaded.
                //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgChangeQueueList, dgChangeQueueList, RadAjaxLoadingPanel1);
                //// Store the clientID of the grid to reference it later on the client
                //RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgChangeQueueList.ClientID));

            }
        }

        protected void dgChangeQueueList_BindData(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestService.CorporateRequestServiceClient ObjService = new CorporateRequestService.CorporateRequestServiceClient())
                        {
                            var ObjRetVal = ObjService.GetCRChangeQueue();
                            List<CorporateRequestService.GetAllCRMain> lstAccount = new List<CorporateRequestService.GetAllCRMain>();
                            if (ObjRetVal.ReturnFlag == true)
                            {
                                lstAccount = ObjRetVal.EntityList.Where(x => x.IsDeleted == false).ToList();
                                if (lstAccount.Count != null)
                                {
                                    dgChangeQueueList.DataSource = (from u in lstAccount
                                                                    where (u.IsDeleted == false && u.CRTripNUM != null && u.CorporateRequestStatus == "S")
                                                                    select
                                                                     new
                                                                     {
                                                                         CRTripNUM = u.CRTripNUM,
                                                                         EstDepartureDT = u.EstDepartureDT,
                                                                         PassengerRequestorCD = u.PassengerRequestorCD,
                                                                         CRMainDescription = u.CRMainDescription,
                                                                         TripSheetStatus = u.TripSheetStatus,
                                                                         AcknowledgementStatus = u.AcknowledgementStatus,
                                                                         CorporateRequestStatus = u.CorporateRequestStatus != null ? ((string)u.CorporateRequestStatus == "S" ? "Submitted" : null) : null,
                                                                         Approver = u.Approver,
                                                                         RequestDT = u.RequestDT,
                                                                         TailNum = u.TailNum,
                                                                         AircraftCD = u.AircraftCD,
                                                                         TripNUM = u.TripNUM,
                                                                         TripStatus = u.TripStatus,
                                                                         TravelCoordCD = u.TravelCoordCD,
                                                                         CRMainID = u.CRMainID,
                                                                     });
                                }
                            }
                            Session["CorporateRequestChangeQueue"] = lstAccount;
                            DoSearchfilter();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessagePopup(ex, ModuleNameConstants.Database.Account);
                }
            }
        }

        protected void dgChangeQueueList_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ClearForm();
                        LoadControlData();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.ChangeQueue);
                }
            }
        }
        //public void ClearForm()
        //{
        //    //tbCrNum.Text = string.Empty;
        //    //tbChangeQueue.Text = string.Empty;
        //    //tbTripNo.Text = string.Empty;
        //    //tbTripStatus.Text = string.Empty;
        //}
        //public void ReadOnlyForm()
        //{
        //    GridDataItem item = dgChangeQueueList.SelectedItems[0] as GridDataItem;
        //    if (dgChangeQueueList.SelectedItems.Count > 0)
        //    {
        //        if (item.GetDataKeyValue("CRTripNUM") != null)
        //        {
        //            tbCrNum.Text = item.GetDataKeyValue("CRTripNUM").ToString();
        //        }
        //        if (item.GetDataKeyValue("TripSheetStatus") != null)
        //        {
        //            tbChangeQueue.Text = item.GetDataKeyValue("TripSheetStatus").ToString();
        //        }
        //        if (item.GetDataKeyValue("TripNUM") != null)
        //        {
        //            tbTripNo.Text = item.GetDataKeyValue("TripNUM").ToString();
        //        }

        //        if (item.GetDataKeyValue("TripStatus") != null)
        //        {
        //            tbTripStatus.Text = item.GetDataKeyValue("TripStatus").ToString();
        //        }
        //    }
        //}

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.ChangeQueue);
                }
            }
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.ChangeQueue);
                }
            }
        }

        protected void dgChangeQueueList_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

                GridDataItem item = (GridDataItem)e.Item;
                if (item["EstDepartureDT"].Text != "&nbsp;")
                {
                    DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "EstDepartureDT");
                    if (date != null)
                        item["EstDepartureDT"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", date));
                }
                if (item["RequestDT"].Text != "&nbsp;")
                {
                    DateTime date1 = (DateTime)DataBinder.Eval(item.DataItem, "RequestDT");
                    if (date1 != null)
                        item["RequestDT"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", date1));
                }

                ////if (item["CorporateRequestStatus"].Text != "&nbsp;")
                ////{
                ////    if (item["CorporateRequestStatus"].Text == "S")

                ////        item["CorporateRequestStatus"].Text = "Submitted";

                ////}
            }
        }


        protected void dgChangeQueueList_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;
                if (filterPair.First.ToString() == "Custom")
                {
                    
                    string colName = filterPair.Second.ToString();
                    TextBox tbPattern = (e.Item as GridFilteringItem)[colName].Controls[0] as TextBox;
                    string[] values = tbPattern.Text.Split(' ');
                    if (values.Length == 1)
                    {
                        e.Canceled = true;
                        //string newFilter = "(([" + filterPair.Second + "] >='" + values[0] + "') AND ([" + filterPair.Second + "] <='" + values[1] + "'))";
                        string newFilter = "(([" + filterPair.Second + "] >='" + values[0] + "')";
                        if (dgChangeQueueList.MasterTableView.FilterExpression == "")
                        {
                            dgChangeQueueList.MasterTableView.FilterExpression = newFilter;
                        }
                        else
                        {
                            //dgChangeQueueList.MasterTableView.FilterExpression = "((" + dgChangeQueueList.MasterTableView.FilterExpression + ") AND (" + newFilter + "))";
                            dgChangeQueueList.MasterTableView.FilterExpression = "((" + newFilter + "))";
                        }
                        dgChangeQueueList.Rebind();
                    }
                }
            }
        }
        protected void ClientCodeFilter_TextChanged(object sender, EventArgs e)
        {

            if (CheckClientFilterCodeExist())
            {
                lbClientCodeFilter.Visible = true;
                lbClientCodeFilter.ForeColor = System.Drawing.Color.Red;
                //RadAjaxManager.GetCurrent(Page).FocusControl(tbClientCodeFilter);
            }
            else
            {
                lbClientCodeFilter.Visible = false;
                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    if ((tbClientCodeFilter.Text.Trim() != string.Empty))
                    {
                        //DoSearchfilter();
                        //DefaultSelection(true);
                    }
                    if ((tbClientCodeFilter.Text.Trim() == string.Empty))
                    {
                        //DoSearchfilter();
                        //DefaultSelection(true);
                    }
                }
            }

        }


        private bool CheckClientFilterCodeExist()
        {
            bool RetVal = false;
            if ((tbClientCodeFilter.Text.Trim() != string.Empty) && (tbClientCodeFilter.Text != null))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient ClientService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var ClientValueFilter = ClientService.GetClientCodeList().EntityList.Where(x => (x.ClientCD != null));

                    var ClientValue = ClientValueFilter.Where(x => x.ClientCD.Trim().ToUpper().Equals(tbClientCodeFilter.Text.ToString().ToUpper().Trim()));
                    if (ClientValue.Count() == 0 || ClientValue == null)
                    {
                        RetVal = true;
                    }
                    else
                    {
                        RetVal = false;
                    }
                }

            }
            return RetVal;
        }


        protected void Save_Click(object sender, EventArgs e)
        {

        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            //RadWindowManager1.RadConfirm("Are you sure you want to Cancel this record?", "confirmCallBackFnCancel", 330, 100, null, "Confirmmation!");
        }

        protected void CancelYes_Click(object sender, EventArgs e)
        {

        }

        protected void CancelNo_Click(object sender, EventArgs e)
        {
        }

        protected void DeleteYes_Click(object sender, EventArgs e)
        {


        }

        protected void DeleteNo_Click(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                DoSearchfilter();
                DefaultSelection(true);
            }
        }

        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (BindDataSwitch)
                    {
                        dgChangeQueueList.Rebind();
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }
        public void DoSearchfilter()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (CorporateRequestService.CorporateRequestServiceClient Service1 = new CorporateRequestService.CorporateRequestServiceClient())
                {
                    List<GetAllCRMain> FilterChangeQueue = new List<GetAllCRMain>();
                    FilterChangeQueue = (List<GetAllCRMain>)Session["CorporateRequestChangeQueue"];
                    CheckClientFilterCodeExist();
                    if (chkHomebase.Checked)
                    {
                        FilterChangeQueue = FilterChangeQueue.Where(x => (x.HomebaseID != null)).ToList();
                        FilterChangeQueue = FilterChangeQueue.Where(x => (x.HomebaseID == Convert.ToInt64(UserPrincipal.Identity._homeBaseId))).ToList();
                    }

                    if (lbClientCodeFilter.Visible == false)
                    {
                        if (tbClientCodeFilter.Text.Trim() != "")
                        {
                            FilterChangeQueue = FilterChangeQueue.Where(x => (x.ClientCD != null)).ToList();
                            FilterChangeQueue = FilterChangeQueue.Where(x => (x.ClientCD.Trim().ToUpper().Equals(tbClientCodeFilter.Text.ToString().ToUpper().Trim()))).ToList();
                        }
                    }

                    if (FilterChangeQueue.Count > 0)
                    {
                        dgChangeQueueList.DataSource = (from u in FilterChangeQueue
                                                        where (u.IsDeleted == false && u.CRTripNUM != null && u.CorporateRequestStatus == "S")
                                                        select
                                                         new
                                                         {
                                                             CRTripNUM = u.CRTripNUM,
                                                             EstDepartureDT = u.EstDepartureDT,
                                                             PassengerRequestorCD = u.PassengerRequestorCD,
                                                             CRMainDescription = u.CRMainDescription,
                                                             TripSheetStatus = u.TripSheetStatus,
                                                             AcknowledgementStatus = u.AcknowledgementStatus,
                                                             CorporateRequestStatus = u.CorporateRequestStatus != null ? ((string)u.CorporateRequestStatus == "S" ? "Submitted" : null) : null,
                                                             Approver = u.Approver,
                                                             RequestDT = u.RequestDT,
                                                             TailNum = u.TailNum,
                                                             AircraftCD = u.AircraftCD,
                                                             TripNUM = u.TripNUM,
                                                             TripStatus = u.TripStatus,
                                                             TravelCoordCD = u.TravelCoordCD,
                                                             CRMainID = u.CRMainID,
                                                         });
                    }

                }
            }
        }

        #region "Button Events"
        protected void btnTransfer_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgChangeQueueList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgChangeQueueList.SelectedItems[0];
                            CRMain oCRMain = new CRMain();
                            if (Item.GetDataKeyValue("CRMainID") != null)
                            {
                                oCRMain.CRMainID = Convert.ToInt64(Item.GetDataKeyValue("CRMainID").ToString());
                                using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                                {
                                    var RetTrip = Service.GetCRHistory(Convert.ToInt64(oCRMain.CustomerID), Convert.ToInt64(oCRMain.CRMainID));
                                    StringBuilder HistoryDescription = new System.Text.StringBuilder();
                                    string OldDescription = string.Empty;
                                    CRHistory TripHistory = new CRHistory();
                                    if (Session["CurrentCorporateRequest"] != null)
                                    {
                                        CRMain tempCRMain = new CRMain();
                                        tempCRMain = (CRMain)Session["CurrentCorporateRequest"];
                                        if (tempCRMain.CRMainID == oCRMain.CRMainID)
                                        {
                                            tempCRMain.TripStatus = UserPrincipal.Identity._fpSettings._TripMGRDefaultStatus;
                                        }
                                        Session["CurrentCorporateRequest"] = tempCRMain;
                                    }
                                    var Result = Service.QueueTransfer(oCRMain);
                                    if (Result.ReturnFlag == true)
                                    {
                                        var ReqCR = Service.GetRequest(oCRMain.CRMainID);
                                        if (ReqCR.ReturnFlag == true)
                                        {
                                            CRMain tempMain = new CRMain();
                                            if (Session["CurrentCorporateRequest"] != null)
                                            {
                                                tempMain = (CRMain)Session["CurrentCorporateRequest"];
                                                tempMain.TripID = ReqCR.EntityList[0].TripID;
                                                tempMain.TripStatus = ReqCR.EntityList[0].TripStatus;
                                                tempMain.CorporateRequestStatus = ReqCR.EntityList[0].CorporateRequestStatus;
                                                Session["CurrentCorporateRequest"] = tempMain;
                                                //Session["CRStatus"] = "Accepted";
                                            }
                                        }
                                        RadWindowManager1.RadAlert("Request Transferred successfully.", 360, 50, "Change Queue - Corporate request", "");
                                        dgChangeQueueList.Rebind();
                                        if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                        {
                                            // TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                            TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                            TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                            TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                            TripHistory.LastUpdTS = DateTime.UtcNow;
                                            HistoryDescription.AppendLine(string.Format("Confirmed Transfer of Corp Request to Preflight."));
                                            //OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                            TripHistory.LogisticsHistory = HistoryDescription.ToString();
                                            TripHistory.IsDeleted = false;
                                            var ReturnHistory = Service.AddCRHistory(TripHistory);
                                        }
                                    }
                                }
                            }
                            dgChangeQueueList.Rebind();
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Please select the request.", 360, 50, "Change Queue - Corporate request", "");
                            return;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.ChangeQueue);
                }
            }
        }
        protected void btnDenyRequest_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgChangeQueueList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgChangeQueueList.SelectedItems[0];
                            CRMain oCRMain = new CRMain();
                            if (Item.GetDataKeyValue("CRMainID") != null)
                            {
                                oCRMain.CRMainID = Convert.ToInt64(Item.GetDataKeyValue("CRMainID").ToString());
                                using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                                {
                                    var RetTrip = Service.GetCRHistory(Convert.ToInt64(oCRMain.CustomerID), Convert.ToInt64(oCRMain.CRMainID));
                                    StringBuilder HistoryDescription = new System.Text.StringBuilder();
                                    string OldDescription = string.Empty;
                                    CRHistory TripHistory = new CRHistory();
                                    var Result = Service.DenyRequest(oCRMain);
                                    if (Result.ReturnFlag == true)
                                    {
                                        RadWindowManager1.RadAlert("Request denied successfully.", 360, 50, "Change Queue - Corporate request", "");
                                        dgChangeQueueList.Rebind();
                                        if (Session["CurrentCorporateRequest"] != null)
                                        {
                                            CRMain CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                            if (CorpRequest.CRMainID == oCRMain.CRMainID)
                                            {
                                                CorpRequest.CorporateRequestStatus = "D";
                                            }
                                            Session["CurrentCorporateRequest"] = CorpRequest;
                                        }
                                        if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                        {
                                            // TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                            TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                            TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                            TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                            TripHistory.LastUpdTS = DateTime.UtcNow;
                                            HistoryDescription.AppendLine(string.Format("Denied Corp Request to Tripsheet Catalog."));
                                            // OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                            TripHistory.LogisticsHistory = HistoryDescription.ToString();
                                            TripHistory.IsDeleted = false;
                                            var ReturnHistory = Service.AddCRHistory(TripHistory);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Please select the request.", 360, 50, "Change Queue - Corporate request", "");
                            return;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.ChangeQueue);
                }
            }
        }
        protected void btnSubmitYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgChangeQueueList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgChangeQueueList.SelectedItems[0];
                            CRMain oCRMain = new CRMain();
                            if (Item.GetDataKeyValue("CRMainID") != null)
                            {
                                oCRMain.CRMainID = Convert.ToInt64(Item.GetDataKeyValue("CRMainID").ToString());
                                using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                                {
                                    var RetTrip = Service.GetCRHistory(Convert.ToInt64(oCRMain.CustomerID), Convert.ToInt64(oCRMain.CRMainID));
                                    StringBuilder HistoryDescription = new System.Text.StringBuilder();
                                    string OldDescription = string.Empty;
                                    CRHistory TripHistory = new CRHistory();
                                    var Result = Service.AcceptRequest(oCRMain);
                                    if (Result.ReturnFlag == true)
                                    {
                                        RadWindowManager1.RadAlert("Request accepted successfully.", 360, 50, "Change Queue - Corporate request", "");
                                        dgChangeQueueList.Rebind();
                                        if (Session["CurrentCorporateRequest"] != null)
                                        {
                                            CRMain CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                            if (CorpRequest.CRMainID == oCRMain.CRMainID)
                                            {
                                                CorpRequest.CorporateRequestStatus = "A";
                                            }
                                            Session["CurrentCorporateRequest"] = CorpRequest;
                                            if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                            {
                                                //TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                                TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                                TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                                TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                                TripHistory.LastUpdTS = DateTime.UtcNow;
                                                HistoryDescription.AppendLine(string.Format("Accepted Corp Request to Tripsheet Catalog."));
                                                // OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                                TripHistory.LogisticsHistory = HistoryDescription.ToString();
                                                TripHistory.IsDeleted = false;
                                                var ReturnHistory = Service.AddCRHistory(TripHistory);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Please select the request.", 360, 50, "Change Queue - Corporate request", "");
                            return;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.ChangeQueue);
                }
            }
        }
        protected void btnMarkAccepted_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgChangeQueueList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgChangeQueueList.SelectedItems[0];
                            CRMain oCRMain = new CRMain();
                            if (Item.GetDataKeyValue("CRMainID") != null)
                            {
                                oCRMain.CRMainID = Convert.ToInt64(Item.GetDataKeyValue("CRMainID").ToString());
                                using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                                {
                                    var objRetVal = Service.GetRequest(oCRMain.CRMainID);
                                    if (objRetVal.ReturnFlag)
                                    {
                                        oCRMain = objRetVal.EntityList[0];
                                    }

                                    if (oCRMain.TripID != null && oCRMain.TripID != 0 && oCRMain.CorporateRequestStatus == "S")
                                    {
                                        RadWindowManager1.RadConfirm("Warning - No Trip Request Information Will Be <br>Transferred to Tripsheet. Any Tripsheet Changes <br>Must Be Done Manually. Continue Marking Trip <br>Request As Accepted (Y/N)?", "confirmSubmitCallBackFn", 400, 30, null, "Corporate request");
                                    }
                                    else if (string.IsNullOrEmpty(oCRMain.AcknowledgementStatus) && oCRMain.CorporateRequestStatus == "S" && oCRMain.TripID == null)
                                    {
                                        RadWindowManager1.RadAlert("Warning - The Selected Corporate Request Trip Has Never Been Transferred To The Tripsheet Catalog.  Please Use The Transfer Button To Move Trip Request Over to Tripsheet.", 360, 50, "Change Queue - Corporate request", "");
                                    }
                                }
                            }
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Please select the request.", 360, 50, "Change Queue - Corporate request", "");
                            return;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.ChangeQueue);
                }
            }
        }
        protected void btnApproved_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgChangeQueueList.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgChangeQueueList.SelectedItems[0];
                            CRMain oCRMain = new CRMain();
                            if (Item.GetDataKeyValue("CRMainID") != null)
                            {
                                oCRMain.CRMainID = Convert.ToInt64(Item.GetDataKeyValue("CRMainID").ToString());
                                using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                                {
                                    var Result = Service.ApproveRequest(oCRMain);
                                    if (Result.ReturnFlag == true)
                                    {
                                        RadWindowManager1.RadAlert("Request approved successfully.", 360, 50, "Change Queue - Corporate request", "");
                                        dgChangeQueueList.Rebind();
                                    }
                                }
                            }
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Please select the request.", 360, 50, "Change Queue - Corporate request", "");
                            return;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.ChangeQueue);
                }
            }
        }
        #endregion

        #region "Methods"
        private void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                string EmptyString = string.Empty;
                tbDispatchNotes.Text = EmptyString;
                tbLogisticHistory.Text = EmptyString;
                tbPaxRequestorNotes.Text = EmptyString;
            }
        }
        private void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (dgChangeQueueList.SelectedItems.Count > 0)
                {
                    GridDataItem Item = (GridDataItem)dgChangeQueueList.SelectedItems[0];
                    using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                    {
                        if (Item.GetDataKeyValue("CRMainID") != null)
                        {
                            CRMain objCRMain = new CRMain();
                            var objresult = Service.GetRequest(Convert.ToInt64(Item.GetDataKeyValue("CRMainID").ToString().Trim()));
                            if ((objresult.ReturnFlag == true) && (objresult.EntityList.Count > 0))
                            {
                                objCRMain = objresult.EntityList[0];
                            }
                            if ((objCRMain.CRDispatchNotes != null) && (objCRMain.CRDispatchNotes.Count != 0))
                            {
                                if (objCRMain.CRDispatchNotes[0].CRDispatchNotes != null)
                                {
                                    //Name is mentioned wrong but this is correct. Do not change this
                                    tbPaxRequestorNotes.Text = objCRMain.CRDispatchNotes[0].CRDispatchNotes;
                                }
                            }
                            if ((objCRMain.CRHistories != null) && (objCRMain.CRHistories.Count != 0))
                            {
                                if (objCRMain.CRHistories[0].LogisticsHistory != null)
                                {
                                    tbLogisticHistory.Text = objCRMain.CRHistories[0].LogisticsHistory;
                                }
                            }
                            if (objCRMain.TripID != null)
                            {
                                using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                                {
                                    var objRetVal = PrefSvc.GetTrip((long)objCRMain.TripID);
                                    if (objRetVal.ReturnFlag)
                                    {
                                        if (objRetVal.EntityList[0].Notes != null)
                                        {
                                            tbDispatchNotes.Text = objRetVal.EntityList[0].Notes;
                                        }
                                    }
                                }
                                //using (MasterCatalogServiceClient MasterService = new MasterCatalogServiceClient())
                                //{
                                //    var objresult1 = MasterService.GetPassengerbyPassengerRequestorID(objCRMain.PassengerRequestorID.Value);
                                //    if ((objresult1.ReturnFlag == true) && (objresult1.EntityList.Count > 0))
                                //    {
                                //        if (objresult1.EntityList[0].Notes != null)
                                //        {
                                //            //Name is mentioned wrong but this is correct. Do not change this
                                //            tbDispatchNotes.Text = objresult1.EntityList[0].Notes;
                                //        }
                                //    }
                                //}
                            }
                        }
                    }
                }
            }
        }
        #endregion
    }
}
