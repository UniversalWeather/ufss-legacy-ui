﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.CorporateRequestService;
using FlightPak.Web.CalculationService;
//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using Telerik.Web.UI;
using System.Globalization;
using System.Drawing;
using FlightPak.Web.PreflightService;
using System.Text;

namespace FlightPak.Web.Views.Transactions.CorporateRequest
{
    public partial class CorporateRequestMain : BaseSecuredPage
    {
        private ExceptionManager exManager;
        public CRMain CorpRequest = new CRMain();
        private delegate void SaveSession();
        private delegate void TripEdit();
        List<CRException> ExceptionList = new List<CRException>();

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(Master.MasterForm.FindControl("DivMasterForm"), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(DivExternalForm, DivExternalForm, RadAjaxLoadingPanel1);

                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnEditTrip, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);


                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbHomeBase, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbClientCode, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbTailNo, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepartDate, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbType, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbTravelCoordinator, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbName, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnNameAllLegs, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepartment, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDepartmentAllLegs, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDescription, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbPhone, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbRequestDate, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbAuthorization, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnAuthorizationAllLegs, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDescriptionAllLegs, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnPrivateAllLegs, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnClientCode, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnHomeBase, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnType, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnName, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDepartment, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnAuth, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbName, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbClientCode, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbHomeBaseIcao, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbHomeBase, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbType, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbDepartment, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbAuthorization, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    // RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(DivExternalForm, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDeleteTrip, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancellationReq, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnNext, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancel, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnEditTrip, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnAcknowledge, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSubmitReq, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);


                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbHomeBase, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbClientCode, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbTailNo, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepartDate, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbType, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbTravelCoordinator, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbName, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDepartment, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbDescription, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbPhone, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbRequestDate, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(tbAuthorization, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnClientCode, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnHomeBase, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnPrivateAllLegs, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnType, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnName, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnNameAllLegs, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDepartment, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnAuth, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnAuthorizationAllLegs, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDescriptionAllLegs, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbName, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbClientCode, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbHomeBaseIcao, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbHomeBase, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbType, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbDepartment, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbAuthorization, DivExternalForm, RadAjaxLoadingPanel1);
                    // RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(DivExternalForm, DivExternalForm, RadAjaxLoadingPanel1);

                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDeleteTrip, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancellationReq, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnNext, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancel, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnEditTrip, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnAcknowledge, DivExternalForm, RadAjaxLoadingPanel1);
                    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSubmitReq, DivExternalForm, RadAjaxLoadingPanel1);


                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnEditTrip, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                        SaveSession SaveCorpSession = new SaveSession(SaveCorpToSessionOnTabChange);
                        Master.SaveCRToSession = SaveCorpSession;
                        SaveSession SaveToSaveCorpRequestSession = new SaveSession(SaveToCorpRequestSession);
                        Master.SaveToSessionmain = SaveToSaveCorpRequestSession;

                        Master.RadAjax_AjaxRequest += RadAjaxManager1_AjaxRequest;

                        //Replicate Save Cancel Delete Buttons in header
                        Master.SaveClick += btnSave_Click;
                        Master.CancelClick += btnCancel_Click;
                        Master.DeleteClick += btnDelete_Click;

                        diableFields();
                        if (!IsPostBack)
                        {
                            lbTravelCoordinator.Text = string.Empty;
                            BindCorporateRequestFromSession();
                            this.tbPhone.ReadOnly = true;

                            #region Authorization
                            CheckAutorization(Permission.CorporateRequest.ViewCRManager);

                            if (!IsAuthorized(Permission.CorporateRequest.ViewCRManager))
                            {
                                btnNext.Visible = false;
                            }

                            if (!IsAuthorized(Permission.CorporateRequest.DeleteCRManager))
                            {
                                btnDeleteTrip.Visible = false;
                            }


                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                                if (CorpRequest.State == CorporateRequestTripEntityState.Added)
                                {
                                    if (!IsAuthorized(Permission.CorporateRequest.AddCRManager))
                                    {
                                        if (!IsAuthorized(Permission.CorporateRequest.AddCRManager))
                                        {

                                            btnSave.Visible = false;
                                            btnCancel.Visible = false;
                                        }
                                    }
                                    else if (CorpRequest.State == CorporateRequestTripEntityState.Modified)
                                    {
                                        if (!IsAuthorized(Permission.CorporateRequest.EditCRManager))
                                        {
                                            btnSave.Visible = false;
                                            btnCancel.Visible = false;
                                        }
                                    }

                                }
                            }
                            else
                            {

                                if (!IsAuthorized(Permission.CorporateRequest.AddCRManager))
                                {
                                    btnSave.Visible = false;
                                    btnCancel.Visible = false;
                                    disableenablefields(false);
                                }
                                else if (!IsAuthorized(Permission.CorporateRequest.EditCRManager))
                                {
                                    btnSave.Visible = false;
                                    btnCancel.Visible = false;
                                    disableenablefields(false);
                                }

                            }
                            #endregion
                            //if (CorpRequest.State == CorporateRequestTripEntityState.Added || CorpRequest.State == CorporateRequestTripEntityState.NoChange)
                            //    SetTravelCoordinator();
                        }
                        if (!IsAuthorized(Permission.CorporateRequest.AddCRManager))
                        {
                            btnSave.Visible = false;
                            btnCancel.Visible = false;
                            disableenablefields(false);
                        }
                        else if (!IsAuthorized(Permission.CorporateRequest.EditCRManager))
                        {
                            btnSave.Visible = false;
                            btnCancel.Visible = false;
                            disableenablefields(false);
                        }
                        LoadHomebaseSettings(!IsPostBack);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["SearchItemPrimaryKeyValue"] != null)
            {
                long CRMainID = Convert.ToInt64(Session["SearchItemPrimaryKeyValue"].ToString());
                CorporateRequestService.CRMain Trip = null;
                using (CorporateRequestServiceClient CReqSvc = new CorporateRequestServiceClient())
                {
                    Trip = new CorporateRequestService.CRMain();
                    var objPrefObj = CReqSvc.GetRequest(CRMainID);
                    if (objPrefObj.ReturnFlag)
                    {
                        CorpRequest = objPrefObj.EntityList[0];

                        Session["CurrentCorporateRequest"] = CorpRequest;
                        var ObjRetval = CReqSvc.GetRequestExceptionList(CRMainID);
                        if (ObjRetval.ReturnFlag)
                        {
                            Session["CoporateRequestException"] = ObjRetval.EntityList;
                        }
                        else
                            Session.Remove("CoporateRequestException");
                    }
                    else
                    {
                        CorpRequest = null;
                        Session.Remove("CoporateRequestException");
                    }
                }
            }
            Session.Remove("SearchItemPrimaryKeyValue");
        }


        protected void btnNameAllLegs_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnNameAllLegs);
                        if (!string.IsNullOrEmpty(hdName.Value))
                        {
                            ViewState["CopyType"] = "Requestor";
                            RadWindowManager1.RadConfirm("Copy Requestor To All Legs?", "confirmCopyCallBackFn", 330, 100, null, "Confirmation!");
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }

        }

        protected void btnDepartmentAllLegs_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnDepartmentAllLegs);
                        if (!string.IsNullOrEmpty(hdDepartment.Value))
                        {
                            ViewState["CopyType"] = "Department";
                            RadWindowManager1.RadConfirm("Copy Department To All Legs?", "confirmCopyCallBackFn", 330, 100, null, "Confirmation!");
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }

        }

        protected void btnAuthorizationAllLegs_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnAuthorizationAllLegs);
                        if (!string.IsNullOrEmpty(hdAuthorization.Value))
                        {

                            ViewState["CopyType"] = "Authorization";
                            RadWindowManager1.RadConfirm("Warning,Both the current Department and Authorization will be copied to All Legs?", "confirmCopyCallBackFn", 330, 100, null, "Confirmation!");
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }


        }

        protected void btnDescriptionAllLegs_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnDescriptionAllLegs);
                        if (!string.IsNullOrEmpty(tbDescription.Text))
                        {
                            ViewState["CopyType"] = "Description";
                            RadWindowManager1.RadConfirm("Copy Trip Purpose To All Legs?", "confirmCopyCallBackFn", 330, 100, null, "Confirmation!");
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }



        }


        protected void btnPrivateAllLegs_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnPrivateAllLegs);
                        ViewState["CopyType"] = "Private";
                        RadWindowManager1.RadConfirm("Copy Private To All Legs?", "confirmCopyCallBackFn", 330, 100, null, "Confirmation!");


                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }


        }


        protected void btnCopyYes_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                            if (CorpRequest.CopyInfoToAllLegs == null)
                                CorpRequest.CopyInfoToAllLegs = new Dictionary<CorporateRequestCopyFunctionalty, CorporateRequestCopyInfo>();

                        }
                        if (ViewState["CopyType"] != null)
                        {
                            Dictionary<string, string> AddlCopyDetails = new Dictionary<string, string>();
                            if (ViewState["CopyType"].Equals("Department"))
                            {
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbAuthorization);
                                if (!string.IsNullOrEmpty(hdDepartment.Value))
                                {
                                    Int64 DepartmentId = 0;
                                    if (Int64.TryParse(hdDepartment.Value, out DepartmentId))
                                    {
                                        if (CorpRequest.CopyInfoToAllLegs.ContainsKey(CorporateRequestCopyFunctionalty.Department))
                                            CorpRequest.CopyInfoToAllLegs.Remove(CorporateRequestCopyFunctionalty.Department);
                                        CorpRequest.CopyInfoToAllLegs.Add(CorporateRequestCopyFunctionalty.Department, new CorporateRequestCopyInfo()
                                        {
                                            IsCopied = true,
                                            CDToCopy = tbDepartment.Text,
                                            DescToCopy = lbDepartment.Text,
                                            IDToCopy = Convert.ToInt64(hdDepartment.Value),
                                        });
                                    }
                                }
                            }
                            else if (ViewState["CopyType"].Equals("Authorization"))
                            {
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbDescription);
                                if (!string.IsNullOrEmpty(hdAuthorization.Value))
                                {
                                    Int64 authorizationId = 0;
                                    Int64 DepartmentId = 0;
                                    if (Int64.TryParse(hdAuthorization.Value, out authorizationId))
                                    {
                                        if (CorpRequest.CopyInfoToAllLegs.ContainsKey(CorporateRequestCopyFunctionalty.Authorization))
                                            CorpRequest.CopyInfoToAllLegs.Remove(CorporateRequestCopyFunctionalty.Authorization);
                                        CorpRequest.CopyInfoToAllLegs.Add(CorporateRequestCopyFunctionalty.Authorization, new CorporateRequestCopyInfo()
                                        {
                                            IsCopied = true,
                                            CDToCopy = tbAuthorization.Text,
                                            DescToCopy = lbAuthorization.Text,
                                            IDToCopy = Convert.ToInt64(authorizationId),
                                        });
                                    }
                                    if (Int64.TryParse(hdDepartment.Value, out DepartmentId))
                                    {
                                        if (CorpRequest.CopyInfoToAllLegs.ContainsKey(CorporateRequestCopyFunctionalty.Department))
                                            CorpRequest.CopyInfoToAllLegs.Remove(CorporateRequestCopyFunctionalty.Department);
                                        CorpRequest.CopyInfoToAllLegs.Add(CorporateRequestCopyFunctionalty.Department, new CorporateRequestCopyInfo()
                                        {
                                            IsCopied = true,
                                            CDToCopy = tbDepartment.Text,
                                            DescToCopy = lbDepartment.Text,
                                            IDToCopy = Convert.ToInt64(hdDepartment.Value),
                                        });
                                    }
                                }
                            }
                            else if (ViewState["CopyType"].Equals("Requestor"))
                            {
                                //RadAjaxManager.GetCurrent(Page).FocusControl(tbAccountNo);
                                if (!string.IsNullOrEmpty(hdName.Value))
                                {
                                    Int64 PassengerRequestorId = 0;
                                    if (Int64.TryParse(hdName.Value, out PassengerRequestorId))
                                    {
                                        if (CorpRequest.CopyInfoToAllLegs.ContainsKey(CorporateRequestCopyFunctionalty.Requestor))
                                            CorpRequest.CopyInfoToAllLegs.Remove(CorporateRequestCopyFunctionalty.Requestor);
                                        CorpRequest.CopyInfoToAllLegs.Add(CorporateRequestCopyFunctionalty.Requestor, new CorporateRequestCopyInfo()
                                        {
                                            IsCopied = true,
                                            CDToCopy = tbName.Text,
                                            DescToCopy = lbName.Text,
                                            IDToCopy = Convert.ToInt64(PassengerRequestorId),
                                        });
                                    }
                                }
                            }
                            else if (ViewState["CopyType"].Equals("Description"))
                            {
                                //RadAjaxManager.GetCurrent(Page).FocusControl(radStatus);
                                if (!string.IsNullOrEmpty(tbDescription.Text))
                                {
                                    if (CorpRequest.CopyInfoToAllLegs.ContainsKey(CorporateRequestCopyFunctionalty.Description))
                                        CorpRequest.CopyInfoToAllLegs.Remove(CorporateRequestCopyFunctionalty.Description);
                                    CorpRequest.CopyInfoToAllLegs.Add(CorporateRequestCopyFunctionalty.Description, new CorporateRequestCopyInfo()
                                    {
                                        IsCopied = true,
                                        DescToCopy = tbDescription.Text,
                                    });

                                }
                            }
                            else if (ViewState["CopyType"].Equals("Private"))
                            {
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbTailNo);

                                if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                                {
                                    foreach (CRLeg Leg in CorpRequest.CRLegs)
                                    {
                                        if (!(bool)Leg.IsDeleted && Leg.LegNUM > 0)
                                        {
                                            CorpRequest.IsPrivate = chkPrivate.Checked;
                                            Leg.IsPrivate = CorpRequest.IsPrivate;
                                        }

                                    }
                                }

                            }

                            Session["CurrentCorporateRequest"] = CorpRequest;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }

        }
        protected void btnCopyNo_Click(object sender, EventArgs e)
        {
            if (ViewState["CopyType"] != null)
            {
                if (ViewState["CopyType"].Equals("Department"))
                {
                    RadAjaxManager.GetCurrent(Page).FocusControl(btnDepartmentAllLegs);
                }
                else if (ViewState["CopyType"].Equals("Authorization"))
                {
                    RadAjaxManager.GetCurrent(Page).FocusControl(btnAuthorizationAllLegs);
                }
                else if (ViewState["CopyType"].Equals("Requestor"))
                {
                    RadAjaxManager.GetCurrent(Page).FocusControl(btnNameAllLegs);
                }
                else if (ViewState["CopyType"].Equals("Description"))
                {
                    RadAjaxManager.GetCurrent(Page).FocusControl(btnDescriptionAllLegs);
                }
            }
        }


        protected void SaveCorpToSessionOnTabChange()
        {
            string DatecheckErrorstr = string.Empty;
            DatecheckErrorstr = DatecheckError();

            if (!string.IsNullOrEmpty(DatecheckErrorstr))
                Session["CorpMainDateCheck"] = DatecheckErrorstr;
            else
            {
                Session.Remove("CorpMainDateCheck");
                SaveToCorpRequestSession();
            }
        }

        private string DatecheckError()
        {
            string desc = string.Empty;
            ////using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            ////{
            ////    bool DatCheck = true;
            ////    DateTime DepartureDate = DateTime.MinValue;
            ////    DateTime RequestDate = DateTime.MinValue;
            ////    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
            ////    if (!string.IsNullOrEmpty(tbDepartDate.Text))
            ////        DepartureDate = DateTime.ParseExact(tbDepartDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);

            ////    if (!string.IsNullOrEmpty(tbRequestDate.Text))
            ////        RequestDate = DateTime.ParseExact(tbRequestDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);

            ////    if (DepartureDate != DateTime.MinValue && RequestDate != DateTime.MinValue)
            ////    {
            ////        if (DepartureDate < RequestDate)
            ////        {
            ////            DatCheck = false;
            ////        }
            ////    }

            ////    if (!DatCheck)
            ////    {
            ////        //RadWindowManager1.RadAlert("Departure Date must be equal to or greater than Request Date", 360, 50, "Request Alert", "");
            ////        desc = "Departure Date must be equal to or greater than Request Date";
            ////    }
            ////}
            return desc;
        }

        protected void SetTravelCoordinator()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (UserPrincipal.Identity._homeBaseId != null)
                {
                    using (AdminService.AdminServiceClient objService = new AdminService.AdminServiceClient())
                    {
                        var objUserVal = objService.GetUserMasterList();
                        List<AdminService.GetAllUserMasterResult> lstUserMaster = new List<AdminService.GetAllUserMasterResult>();
                        if (objUserVal.ReturnFlag == true)
                        {
                            lstUserMaster = objUserVal.EntityList.Where(x => x.HomebaseID == UserPrincipal.Identity._homeBaseId && x.CustomerID == UserPrincipal.Identity._customerID && x.UserName.ToLower().Trim() == UserPrincipal.Identity._name.ToLower().Trim()).ToList();
                            if (lstUserMaster.Count > 0)
                            {
                                if (lstUserMaster[0].TravelCoordCD != null)
                                {
                                    tbTravelCoordinator.Text = lstUserMaster[0].TravelCoordCD.ToString();
                                    hdnTravelCoordinatorID.Value = lstUserMaster[0].TravelCoordID.ToString();
                                    lbTravelCoordinator.Text = System.Web.HttpUtility.HtmlEncode( lstUserMaster[0].FirstName.ToString());
                                    lbcvTravelCoordinator.Text = string.Empty;
                                    hdShowRequestor.Value = UserPrincipal.Identity._fpSettings._IsReqOnly.ToString();
                                    hdHighlightTailno.Value = UserPrincipal.Identity._fpSettings._IsANotes.ToString();
                                    hdIsDepartAuthReq.Value = UserPrincipal.Identity._fpSettings._IsDepartAuthDuringReqSelection.ToString();
                                    tbTravelCoordinator_TextChanged(null, null);

                                }
                            }
                        }
                    }
                }
            }
        }

        private void diableFields()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                tbCrNumber.Enabled = false;
                tbApprover.Enabled = false;
                tbCorpStatus.Enabled = false;
                tbTripNumber.Enabled = false;
                tbTripStatus.Enabled = false;
                tbDispatcherNotes.Enabled = false;
            }
        }


        protected void SaveToCorpRequestSession()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];


                            if (CorpRequest.Mode == CorporateRequestTripActionMode.Edit)
                            {
                                if (CorpRequest.CRMainID > 0)
                                    CorpRequest.State = CorporateRequestTripEntityState.Modified;
                                else
                                    CorpRequest.State = CorporateRequestTripEntityState.Added;


                                bool SaveToSession = false;
                                if (CorpRequest.State == CorporateRequestTripEntityState.Modified)
                                {
                                    if (IsAuthorized(Permission.CorporateRequest.EditCRManager))
                                    {
                                        SaveToSession = true;
                                    }
                                }
                                else if (CorpRequest.State == CorporateRequestTripEntityState.Added)
                                {
                                    if (IsAuthorized(Permission.CorporateRequest.AddCRManager))
                                    {
                                        SaveToSession = true;
                                    }
                                }


                                if (SaveToSession)
                                {
                                    #region Departure
                                    CorporateRequestService.Company HomebaseSample = new CorporateRequestService.Company();
                                    if (!string.IsNullOrWhiteSpace(tbHomeBase.Text))
                                    {
                                        Int64 HomebaseID = 0;
                                        if (Int64.TryParse(hdHomeBase.Value, out HomebaseID))
                                        {
                                            HomebaseSample.HomebaseID = HomebaseID;
                                            if (string.IsNullOrEmpty(hdHomeBaseAirportID.Value))
                                            {
                                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    var objRetVal = objDstsvc.GetListInfoByHomeBaseId(HomebaseID);
                                                    List<FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId> CompanyList = new List<FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId>();

                                                    if (objRetVal.ReturnFlag)
                                                    {

                                                        CompanyList = objRetVal.EntityList.Where(x => x.HomebaseID == HomebaseID).ToList();
                                                        if (CompanyList != null && CompanyList.Count > 0)
                                                        {
                                                            hdHomeBaseAirportID.Value = CompanyList[0].HomebaseAirportID.ToString();
                                                            HomebaseSample.HomebaseAirportID = CompanyList[0].HomebaseAirportID;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                                HomebaseSample.HomebaseAirportID = Convert.ToInt64(hdHomeBaseAirportID.Value);

                                            HomebaseSample.BaseDescription = lbHomeBase.Text;
                                            HomebaseSample.BaseDescription = lbHomeBase.Text;
                                            CorpRequest.HomebaseID = HomebaseID;
                                            CorpRequest.HomeBaseAirportICAOID = tbHomeBase.Text;
                                            CorpRequest.HomeBaseAirportID = Convert.ToInt64(hdHomeBaseAirportID.Value);
                                            CorpRequest.Company = HomebaseSample;
                                        }
                                    }
                                    else
                                    {
                                        CorpRequest.HomebaseID = null;
                                        CorpRequest.Company = null;
                                    }


                                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                                    if (!string.IsNullOrEmpty(tbDepartDate.Text))

                                        CorpRequest.EstDepartureDT = DateTime.ParseExact(tbDepartDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    else
                                        CorpRequest.EstDepartureDT = null;

                                    CorporateRequestService.Client ClientSample = new CorporateRequestService.Client();
                                    if (!string.IsNullOrWhiteSpace(tbClientCode.Text))
                                    {
                                        Int64 ClientID = 0;
                                        if (Int64.TryParse(hdClientCode.Value, out ClientID))
                                        {
                                            ClientSample.ClientID = ClientID;
                                            ClientSample.ClientCD = tbClientCode.Text;
                                            ClientSample.ClientDescription = lbClientCode.Text;
                                            CorpRequest.Client = ClientSample;
                                            CorpRequest.ClientID = ClientID;
                                        }
                                    }
                                    else
                                    {
                                        CorpRequest.Client = null;
                                        CorpRequest.ClientID = null;
                                    }

                                    CorpRequest.IsPrivate = chkPrivate.Checked;
                                    #endregion

                                    #region Aircraft
                                    CorporateRequestService.Fleet fleetsample = new CorporateRequestService.Fleet();
                                    if (!string.IsNullOrWhiteSpace(tbTailNo.Text))
                                    {
                                        Int64 FleetID = 0;
                                        if (Int64.TryParse(hdTailNo.Value, out FleetID))
                                        {
                                            fleetsample.FleetID = FleetID;
                                            fleetsample.TailNum = tbTailNo.Text;
                                            if (!string.IsNullOrEmpty(hdMaximumPassenger.Value))
                                                fleetsample.MaximumPassenger = Convert.ToDecimal(hdMaximumPassenger.Value.ToString());

                                            if (fleetsample.MaximumPassenger != null && fleetsample.MaximumPassenger > 0)
                                            {
                                                if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                                                {
                                                    foreach (CRLeg Leg in CorpRequest.CRLegs)
                                                    {
                                                        Leg.SeatTotal = Convert.ToInt32((decimal)fleetsample.MaximumPassenger);
                                                        Leg.ReservationTotal = 0;
                                                        if (Leg.PassengerTotal != null)
                                                            Leg.ReservationAvailable = Leg.SeatTotal - Leg.PassengerTotal;
                                                        else
                                                            Leg.ReservationAvailable = Leg.SeatTotal;
                                                        Leg.WaitNUM = 0;
                                                    }
                                                }
                                            }

                                            CorpRequest.Fleet = fleetsample;
                                            CorpRequest.FleetID = FleetID;
                                        }
                                    }
                                    else
                                    {
                                        CorpRequest.Fleet = null;
                                        CorpRequest.FleetID = null;
                                    }

                                    CorporateRequestService.Aircraft aircraftSample = new CorporateRequestService.Aircraft();
                                    if (!string.IsNullOrWhiteSpace(tbType.Text))
                                    {
                                        Int64 AircraftID = 0;
                                        if (Int64.TryParse(hdType.Value, out AircraftID))
                                        {
                                            aircraftSample.AircraftID = AircraftID;
                                            aircraftSample.AircraftCD = tbType.Text;
                                            aircraftSample.AircraftDescription = lbType.Text;
                                            CorpRequest.Aircraft = aircraftSample;
                                            CorpRequest.AircraftID = AircraftID;
                                        }


                                        #region Reset Leg's Passenger total if there is not fleet and only aircarft is left

                                        if (CorpRequest.FleetID == null)
                                        {
                                            if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                                            {
                                                foreach (CRLeg Leg in CorpRequest.CRLegs)
                                                {
                                                    if (Leg.CRLegID != 0 && Leg.State != CorporateRequestTripEntityState.Deleted)
                                                    {
                                                        Leg.State = CorporateRequestTripEntityState.Modified;
                                                    }
                                                    Leg.SeatTotal = 0;
                                                    Leg.ReservationTotal = 0;
                                                    if (Leg.PassengerTotal != null)
                                                        Leg.ReservationAvailable = Leg.SeatTotal - Leg.PassengerTotal;
                                                    else
                                                        Leg.ReservationAvailable = Leg.SeatTotal;
                                                    Leg.WaitNUM = 0;
                                                }
                                            }
                                        }

                                        #endregion
                                    }
                                    else
                                    {
                                        CorpRequest.Aircraft = null;
                                        CorpRequest.AircraftID = null;
                                    }
                                    #endregion

                                    #region Requestor

                                    if (!string.IsNullOrEmpty(tbRequestDate.Text))
                                    {
                                        CorpRequest.RequestDT = DateTime.ParseExact(tbRequestDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        CorpRequest.RequestDT = null;
                                    }

                                    CorporateRequestService.Passenger Pass = new CorporateRequestService.Passenger();
                                    if (!string.IsNullOrEmpty(tbName.Text))
                                    {
                                        Int64 PassengerRequestorID = 0;
                                        if (Int64.TryParse(hdName.Value, out PassengerRequestorID))
                                        {
                                            Pass.PassengerRequestorID = PassengerRequestorID;
                                            Pass.PassengerRequestorCD = tbName.Text;
                                            Pass.FirstName = lbName.Text;
                                            Pass.AdditionalPhoneNum = tbPhone.Text;
                                            CorpRequest.Passenger = Pass;
                                            CorpRequest.PassengerRequestorID = PassengerRequestorID;
                                        }
                                    }
                                    else
                                    {
                                        CorpRequest.Passenger = null;
                                        CorpRequest.PassengerRequestorID = null;
                                    }

                                    CorporateRequestService.Department DepartmentSample = new CorporateRequestService.Department();
                                    if (!string.IsNullOrEmpty(tbDepartment.Text))
                                    {
                                        Int64 DepartmentID = 0;
                                        if (Int64.TryParse(hdDepartment.Value, out DepartmentID))
                                        {
                                            DepartmentSample.DepartmentID = DepartmentID;
                                            DepartmentSample.DepartmentCD = tbDepartment.Text;
                                            DepartmentSample.DepartmentName = lbDepartment.Text;
                                            CorpRequest.Department = DepartmentSample;
                                            CorpRequest.DepartmentID = DepartmentID;
                                        }
                                    }
                                    else
                                    {
                                        CorpRequest.Department = null;
                                        CorpRequest.DepartmentID = null;
                                    }


                                    CorporateRequestService.DepartmentAuthorization MainDeptAuth = new CorporateRequestService.DepartmentAuthorization();
                                    if (!string.IsNullOrEmpty(tbAuthorization.Text))
                                    {
                                        Int64 AuthorizationID = 0;
                                        if (Int64.TryParse(hdAuthorization.Value, out AuthorizationID))
                                        {
                                            MainDeptAuth.AuthorizationID = AuthorizationID;
                                            MainDeptAuth.AuthorizationCD = tbAuthorization.Text;
                                            MainDeptAuth.DeptAuthDescription = lbAuthorization.Text;
                                            CorpRequest.DepartmentAuthorization = MainDeptAuth;
                                            CorpRequest.AuthorizationID = AuthorizationID;
                                        }
                                    }
                                    else
                                    {
                                        CorpRequest.DepartmentAuthorization = null;
                                        CorpRequest.AuthorizationID = null;
                                    }

                                    #endregion

                                    #region Status
                                    if (!string.IsNullOrEmpty(tbCorpStatus.Text) && tbCorpStatus.Text == "Worksheet")
                                        CorpRequest.CorporateRequestStatus = "W";
                                    else if (!string.IsNullOrEmpty(tbCorpStatus.Text) && tbCorpStatus.Text == "Canceled")
                                        CorpRequest.CorporateRequestStatus = "X";
                                    else if (!string.IsNullOrEmpty(tbCorpStatus.Text) && tbCorpStatus.Text == "Submitted")
                                        CorpRequest.CorporateRequestStatus = "S";
                                    else if (!string.IsNullOrEmpty(tbCorpStatus.Text) && tbCorpStatus.Text == "Accepted")
                                        CorpRequest.CorporateRequestStatus = "A";
                                    else if (!string.IsNullOrEmpty(tbCorpStatus.Text) && tbCorpStatus.Text == "Modified")
                                        CorpRequest.CorporateRequestStatus = "M";

                                    #endregion

                                    #region TravelCoordinator
                                    CorporateRequestService.TravelCoordinator MainTravelCoordinator = new CorporateRequestService.TravelCoordinator();
                                    if (!string.IsNullOrEmpty(tbTravelCoordinator.Text))
                                    {
                                        Int64 TravelCoordinatorID = 0;
                                        if (Int64.TryParse(hdnTravelCoordinatorID.Value, out TravelCoordinatorID))
                                        {
                                            MainTravelCoordinator.TravelCoordinatorID = TravelCoordinatorID;
                                            MainTravelCoordinator.TravelCoordCD = tbTravelCoordinator.Text;
                                            MainTravelCoordinator.FirstName = lbTravelCoordinator.Text;
                                            MainTravelCoordinator.PhoneNum = tbTravelCoPhone.Text;
                                            CorpRequest.TravelCoordinator = MainTravelCoordinator;
                                            CorpRequest.TravelCoordinatorID = TravelCoordinatorID;
                                        }
                                    }
                                    else
                                    {
                                        CorpRequest.TravelCoordinator = null;
                                        CorpRequest.TravelCoordinatorID = null;
                                    }

                                    #endregion

                                    #region Description
                                    CorpRequest.CRMainDescription = tbDescription.Text;
                                    #endregion

                                    CorpRequest.RecordType = "W";

                                    if (CorpRequest.CRDispatchNotes == null)
                                    {
                                        CorpRequest.CRDispatchNotes = new List<CRDispatchNote>();
                                    }

                                    if (CorpRequest.CRDispatchNotes != null && CorpRequest.CRDispatchNotes.Count > 0)
                                    {
                                        if (CorpRequest.CRDispatchNotes[0].CRDispatchNotesID == 0)
                                            CorpRequest.CRDispatchNotes[0].State = CorporateRequestTripEntityState.Added;
                                        else
                                            CorpRequest.CRDispatchNotes[0].State = CorporateRequestTripEntityState.Modified;

                                        CorpRequest.CRDispatchNotes[0].CRDispatchNotes = tbRequestorNotes.Text;
                                        CorpRequest.CRDispatchNotes[0].CRMainID = CorpRequest.CRMainID;
                                        CorpRequest.CRDispatchNotes[0].CustomerID = CorpRequest.CustomerID;
                                    }
                                    else
                                    {
                                        CRDispatchNote dispatchNote = new CRDispatchNote();
                                        if (dispatchNote.CRDispatchNotesID == 0)
                                            dispatchNote.State = CorporateRequestTripEntityState.Added;

                                        dispatchNote.CRDispatchNotes = tbRequestorNotes.Text;
                                        dispatchNote.CRMainID = CorpRequest.CRMainID;
                                        dispatchNote.CustomerID = CorpRequest.CustomerID;
                                        CorpRequest.CRDispatchNotes.Add(dispatchNote);
                                    }


                                    if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                                    {
                                        foreach (CRLeg preflightLegitem in CorpRequest.CRLegs)
                                        {
                                            if (preflightLegitem.CRLegID != 0 && preflightLegitem.State != CorporateRequestTripEntityState.Deleted)
                                                preflightLegitem.State = CorporateRequestTripEntityState.Modified;

                                            if (CorpRequest.CopyInfoToAllLegs != null && CorpRequest.CopyInfoToAllLegs.Count > 0)
                                            {
                                                if (CorpRequest.CopyInfoToAllLegs.Keys.Contains(CorporateRequestCopyFunctionalty.Requestor) && CorpRequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Requestor].IsCopied == true)
                                                {
                                                    if (Pass != null)
                                                    {
                                                        preflightLegitem.Passenger = Pass;
                                                        preflightLegitem.PassengerRequestorID = Pass.PassengerRequestorID;
                                                    }

                                                }

                                                if (CorpRequest.CopyInfoToAllLegs.Keys.Contains(CorporateRequestCopyFunctionalty.Department) && CorpRequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Department].IsCopied == true)
                                                {
                                                    if (DepartmentSample != null)
                                                    {
                                                        preflightLegitem.Department = DepartmentSample;
                                                        preflightLegitem.DepartmentID = DepartmentSample.DepartmentID;
                                                        //preflightLegitem.DepartmentName = DepartmentSample.DepartmentName;
                                                    }
                                                }

                                                if (CorpRequest.CopyInfoToAllLegs.Keys.Contains(CorporateRequestCopyFunctionalty.Authorization) && CorpRequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Authorization].IsCopied == true)
                                                {
                                                    if (MainDeptAuth != null)
                                                    {
                                                        preflightLegitem.DepartmentAuthorization = MainDeptAuth;
                                                        preflightLegitem.AuthorizationID = MainDeptAuth.AuthorizationID;
                                                    }

                                                }

                                                if (CorpRequest.CopyInfoToAllLegs.Keys.Contains(CorporateRequestCopyFunctionalty.Description) && CorpRequest.CopyInfoToAllLegs[CorporateRequestCopyFunctionalty.Description].IsCopied == true)
                                                {
                                                    preflightLegitem.FlightPurpose = tbDescription.Text;
                                                }
                                            }
                                        }
                                    }



                                    Session["CurrentCorporateRequest"] = CorpRequest;

                                    if (hdCalculateLeg.Value.ToLower() == "true")
                                    {
                                        Master.DoLegCalculationsForTrip();
                                    }


                                    ExceptionList = (List<CRException>)Session["CorporateRequestException"];
                                    if (CorpRequest.CRMainID == 0 && CorpRequest.State == CorporateRequestTripEntityState.Added)
                                    {
                                        Master.ValidationRequest(CorporateRequestService.Group.Legs);
                                        Master.BindException();
                                    }

                                    Master.TrackerLegs.Rebind();



                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }

        protected void BindCorporateRequestFromSession()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentCorporateRequest"] != null)
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                    ClearFields();

                    tbCrNumber.Text = CorpRequest.CRTripNUM.ToString();
                    if (!string.IsNullOrEmpty(CorpRequest.Approver))
                        tbApprover.Text = CorpRequest.Approver.ToString();
                    //Curently there is not filed for Depart Airport
                    #region Depart
                    if (CorpRequest.Company != null)
                    {

                        if (CorpRequest.HomebaseID > 0 && string.IsNullOrEmpty(CorpRequest.HomeBaseAirportICAOID))
                        {
                            FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId TripCompany = new FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId();
                            TripCompany = GetCompany((long)CorpRequest.HomebaseID);
                            if (TripCompany != null)
                            {
                                CorpRequest.HomeBaseAirportICAOID = TripCompany.HomebaseCD;
                                CorpRequest.HomeBaseAirportID = (long)TripCompany.HomebaseAirportID;

                            }
                        }

                        lbHomeBase.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.Company.BaseDescription);
                        hdHomeBase.Value = CorpRequest.HomebaseID.ToString();
                        tbHomeBase.Text = CorpRequest.HomeBaseAirportICAOID;
                        hdHomeBaseAirportID.Value = CorpRequest.HomeBaseAirportID.ToString();
                        #region Tooltip for AirportDetails
                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetValue = objDstsvc.GetAirportByAirportICaoID(tbHomeBase.Text.ToUpper().Trim());
                            List<FlightPakMasterService.GetAllAirport> getAllAirportList = new List<FlightPakMasterService.GetAllAirport>();
                            if (objRetValue.ReturnFlag)
                            {
                                getAllAirportList = objRetValue.EntityList;
                                if ((getAllAirportList != null && getAllAirportList.Count > 0))
                                {
                                    string builder = string.Empty;
                                    builder = "ICAO : " + (getAllAirportList[0].IcaoID != null ? getAllAirportList[0].IcaoID : string.Empty)
                                        + "\n" + "City : " + (getAllAirportList[0].CityName != null ? getAllAirportList[0].CityName : string.Empty)
                                        + "\n" + "State/Province : " + (getAllAirportList[0].StateName != null ? getAllAirportList[0].StateName : string.Empty)
                                        + "\n" + "Country : " + (getAllAirportList[0].CountryName != null ? getAllAirportList[0].CountryName : string.Empty)
                                        + "\n" + "Airport : " + (getAllAirportList[0].AirportName != null ? getAllAirportList[0].AirportName : string.Empty)
                                        + "\n" + "DST Region : " + (getAllAirportList[0].DSTRegionCD != null ? getAllAirportList[0].DSTRegionCD : string.Empty)
                                        + "\n" + "UTC+/- : " + (getAllAirportList[0].OffsetToGMT != null ? getAllAirportList[0].OffsetToGMT.ToString() : string.Empty)
                                        + "\n" + "Longest Runway : " + (getAllAirportList[0].LongestRunway != null ? getAllAirportList[0].LongestRunway.ToString() : string.Empty)
                                         + "\n" + "IATA : " + (getAllAirportList[0].Iata != null ? getAllAirportList[0].Iata.ToString() : string.Empty);

                                    //lbArriveIcao.ToolTip = builder;
                                    lbHomeBase.ToolTip = builder;
                                    lbHomeBaseIcao.ToolTip = builder;
                                }


                            }
                        }
                        #endregion
                    }

                    if (CorpRequest.EstDepartureDT != DateTime.MinValue && CorpRequest.EstDepartureDT != null)
                        tbDepartDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", CorpRequest.EstDepartureDT);
                    else
                        tbDepartDate.Text = "";

                    chkPrivate.Checked = CorpRequest.IsPrivate == true ? true : false;

                    if (CorpRequest.Client != null)
                    {
                        tbClientCode.Text = CorpRequest.Client.ClientCD;
                        lbClientCode.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.Client.ClientDescription);
                        hdClientCode.Value = CorpRequest.ClientID.ToString();
                    }

                    #endregion

                    #region Aircraft

                    if (CorpRequest.Fleet != null)
                    {
                        tbTailNo.Text = CorpRequest.Fleet.TailNum;
                        hdMaximumPassenger.Value = CorpRequest.Fleet.MaximumPassenger.ToString();
                        hdTailNo.Value = CorpRequest.FleetID.ToString();


                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            if (UserPrincipal.Identity._fpSettings._IsANotes)
                            {
                                List<FlightPak.Web.FlightPakMasterService.Fleet> Fleetlist = new List<FlightPakMasterService.Fleet>();
                                var objRetVal = objDstsvc.GetFleetProfileList();
                                if (objRetVal.ReturnFlag)
                                {

                                    Fleetlist = objRetVal.EntityList.Where(x => x.TailNum.ToString().ToUpper().Trim().Equals(tbTailNo.Text.ToUpper().Trim())).ToList();

                                    if (Fleetlist != null && Fleetlist.Count > 0)
                                    {
                                        if (Fleetlist[0].Notes != null && Fleetlist[0].Notes != string.Empty)
                                        {
                                            tbTailNo.ForeColor = Color.Red;
                                            //this.tbTailNo.BackColor = System.Drawing.ColorTranslator.FromHtml("#c00000");
                                            //this.tbTailNo.BorderColor =  System.Drawing.ColorTranslator.FromHtml("#a30303");
                                            this.tbTailNo.ToolTip = Fleetlist[0].Notes;
                                            tbTailNo.ToolTip = Fleetlist[0].Notes != null ? Fleetlist[0].Notes : string.Empty;
                                        }
                                        else
                                        {
                                            tbTailNo.ForeColor = Color.Black;
                                            tbTailNo.ToolTip = string.Empty;
                                        }
                                    }
                                }
                            }
                        }

                    }


                    if (CorpRequest.Aircraft != null)
                    {
                        tbType.Text = CorpRequest.Aircraft.AircraftCD;
                        lbType.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.Aircraft.AircraftDescription);
                        hdType.Value = CorpRequest.AircraftID.ToString();
                    }

                    #endregion

                    #region  Requestor

                    if (CorpRequest.RequestDT != DateTime.MinValue && CorpRequest.RequestDT != null)
                        tbRequestDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", CorpRequest.RequestDT);
                    else
                        tbRequestDate.Text = "";

                    if (CorpRequest.Passenger != null)
                    {
                        tbName.Text = CorpRequest.Passenger.PassengerRequestorCD;
                        lbName.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.Passenger.FirstName) + (string.IsNullOrWhiteSpace(System.Web.HttpUtility.HtmlEncode(CorpRequest.Passenger.MiddleInitial)) ? string.Empty : (" " + System.Web.HttpUtility.HtmlEncode(CorpRequest.Passenger.MiddleInitial))) + (string.IsNullOrWhiteSpace(System.Web.HttpUtility.HtmlEncode(CorpRequest.Passenger.LastName)) ? string.Empty : (" " + System.Web.HttpUtility.HtmlEncode(CorpRequest.Passenger.LastName)));
                        hdName.Value = CorpRequest.PassengerRequestorID.ToString();
                        tbPhone.Text = CorpRequest.Passenger.AdditionalPhoneNum;
                    }

                    if (CorpRequest.Department != null)
                    {
                        tbDepartment.Text = CorpRequest.Department.DepartmentCD;
                        lbDepartment.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.Department.DepartmentName);
                        hdDepartment.Value = CorpRequest.DepartmentID.ToString();
                    }

                    if (CorpRequest.DepartmentAuthorization != null)
                    {
                        tbAuthorization.Text = CorpRequest.DepartmentAuthorization.AuthorizationCD;
                        lbAuthorization.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.DepartmentAuthorization.DeptAuthDescription);
                        hdAuthorization.Value = CorpRequest.AuthorizationID.ToString();
                    }


                    #endregion

                    #region Status
                    if (CorpRequest.CorporateRequestStatus != null && CorpRequest.CorporateRequestStatus.ToUpper() == "W")
                    {
                        tbCorpStatus.Text = "Worksheet";
                    }
                    else if (CorpRequest.CorporateRequestStatus != null && CorpRequest.CorporateRequestStatus.ToUpper() == "T")
                    {
                        tbCorpStatus.Text = "Accepted";
                    }
                    else if (CorpRequest.CorporateRequestStatus != null && CorpRequest.CorporateRequestStatus.ToUpper() == "A")
                    {
                        tbCorpStatus.Text = "Accepted";
                    }
                    else if (CorpRequest.CorporateRequestStatus != null && CorpRequest.CorporateRequestStatus.ToUpper() == "D")
                    {
                        tbCorpStatus.Text = "Denied";
                    }
                    else if (CorpRequest.CorporateRequestStatus != null && CorpRequest.CorporateRequestStatus.ToUpper() == "X")
                    {
                        tbCorpStatus.Text = "Submitted";
                    }
                    else if (CorpRequest.CorporateRequestStatus != null && CorpRequest.CorporateRequestStatus.ToUpper() == "M")
                    {
                        tbCorpStatus.Text = "Modified";
                    }
                    else if (CorpRequest.CorporateRequestStatus != null && CorpRequest.CorporateRequestStatus.ToUpper() == "S")
                    {
                        tbCorpStatus.Text = "Submitted";
                    }
                    else if (CorpRequest.CorporateRequestStatus != null && CorpRequest.CorporateRequestStatus.ToUpper() == "C")
                    {
                        tbCorpStatus.Text = "Canceled";
                    }
                    #endregion

                    #region "Trip status"
                    if (CorpRequest.TripStatus != null && CorpRequest.TripStatus.ToUpper() == "W")
                    {
                        tbTripStatus.Text = "Worksheet";
                    }
                    else if (CorpRequest.TripStatus != null && CorpRequest.TripStatus.ToUpper() == "T")
                    {
                        tbTripStatus.Text = "Tripsheet";
                    }
                    else if (CorpRequest.TripStatus != null && CorpRequest.TripStatus.ToUpper() == "C")
                    {
                        tbTripStatus.Text = "Canceled";
                        //tbCorpStatus.Text = "Canceled";
                    }
                    else if (CorpRequest.TripStatus != null && CorpRequest.TripStatus.ToUpper() == "H")
                    {
                        tbTripStatus.Text = "Hold";
                    }

                    if (CorpRequest.TripID != null)
                    {
                        using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                        {
                            var objRetVal = PrefSvc.GetTrip((long)CorpRequest.TripID);
                            if (objRetVal.ReturnFlag)
                            {
                                tbTripNumber.Text = objRetVal.EntityList[0].TripNUM.ToString();
                                if (objRetVal.EntityList[0].DispatchNotes != null)
                                    tbDispatcherNotes.Text = objRetVal.EntityList[0].DispatchNotes.ToString();
                            }
                        }
                    }

                    #endregion

                    #region  TravelCoordinator

                    if (CorpRequest.TravelCoordinator != null)
                    {
                        tbTravelCoordinator.Text = CorpRequest.TravelCoordinator.TravelCoordCD;
                        lbTravelCoordinator.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.TravelCoordinator.FirstName);
                        hdnTravelCoordinatorID.Value = CorpRequest.TravelCoordinatorID.ToString();
                        if (CorpRequest.TravelCoordinator.BusinessPhone != null)
                            tbTravelCoPhone.Text = CorpRequest.TravelCoordinator.BusinessPhone;
                        if (CorpRequest.TravelCoordinator.BusinessEmail != null)
                            tbTravelCoEmail.Text = CorpRequest.TravelCoordinator.BusinessEmail.ToString();
                        if (CorpRequest.TravelCoordinator.FaxNum != null && CorpRequest.TravelCoordinator.FaxNum.ToString().Trim() != "")
                        {
                            tbTravelCoPhone.Text += " / " + CorpRequest.TravelCoordinator.FaxNum.ToString();
                        }
                    }
                    #endregion


                    #region Dispatcher
                    if (CorpRequest.CRDispatchNotes != null)
                    {
                        if (CorpRequest.CRDispatchNotes.Count > 0)
                            tbRequestorNotes.Text = CorpRequest.CRDispatchNotes[0].CRDispatchNotes;
                    }
                    #endregion

                    #region Description
                    tbDescription.Text = CorpRequest.CRMainDescription;
                    #endregion

                }
                else
                    disableenablefields(false);
            }
        }

        private FlightPak.Web.FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId GetCompany(Int64 HomeBaseID)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomeBaseID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPak.Web.FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId Company = new FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId();
                    var objRetVal = objDstsvc.GetListInfoByHomeBaseId(HomeBaseID);
                    List<FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId> CompanyList = new List<FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId>();

                    if (objRetVal.ReturnFlag)
                    {

                        CompanyList = objRetVal.EntityList.Where(x => x.HomebaseID == HomeBaseID).ToList();
                        if (CompanyList != null && CompanyList.Count > 0)
                        {
                            Company = CompanyList[0];

                        }
                        else
                            Company = null;

                    }
                    return Company;
                }
            }

        }

        protected void ResetHomebaseSettings()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                hdShowRequestor.Value = "false";
                hdHighlightTailno.Value = "false";
                hdIsDepartAuthReq.Value = "false";
            }
        }

        protected void LoadHomebaseSettings(bool InitialPageReq)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(InitialPageReq))
            {
                ResetHomebaseSettings();

                hdShowRequestor.Value = UserPrincipal.Identity._fpSettings._IsReqOnly.ToString();
                hdHighlightTailno.Value = UserPrincipal.Identity._fpSettings._IsANotes.ToString();
                hdIsDepartAuthReq.Value = UserPrincipal.Identity._fpSettings._IsDepartAuthDuringReqSelection.ToString();
                // if not is postback and if it is a new trip 
                if (Session["CurrentCorporateRequest"] != null)
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                    if (InitialPageReq)
                    {
                        if (CorpRequest.CRMainID == 0)
                        {


                            //if (UserPrincipal.Identity._fpSettings._TripMGRDefaultStatus != null)
                            //{
                            //    tbCorpStatus.Text = UserPrincipal.Identity._fpSettings._TripMGRDefaultStatus == null ? "T" : UserPrincipal.Identity._fpSettings._TripMGRDefaultStatus;
                            //    if (tbCorpStatus.Text.Trim() == "W")
                            //    {
                            tbCorpStatus.Text = "Worksheet";
                            //    }
                            //}

                            tbRequestDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", DateTime.Now);
                            tbDepartDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", DateTime.Now);

                            if (UserPrincipal.Identity._clientId != null)
                            {
                                tbClientCode.Text = UserPrincipal.Identity._clientCd;
                                hdClientCode.Value = UserPrincipal.Identity._clientId.ToString();
                                FlightPakMasterService.Client client = new FlightPakMasterService.Client();
                                client = GetClient((long)UserPrincipal.Identity._clientId);
                                if (client != null)
                                {
                                    lbClientCode.Text = System.Web.HttpUtility.HtmlEncode(client.ClientDescription);
                                }
                                tbClientCode.ReadOnly = true;
                            }
                            else
                            {
                                tbClientCode.ReadOnly = false;
                            }
                        }
                    }

                }
            }
        }

        protected void page_prerender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                            if (CorpRequest.Mode == CorporateRequestTripActionMode.Edit)
                            {
                                btnSubmitReq.Enabled = false;
                                btnCancellationReq.Enabled = false;
                                btnAcknowledge.Enabled = false;
                                btnSave.Enabled = true;
                                btnCancel.Enabled = true;
                                btnDeleteTrip.Enabled = false;
                                btnEditTrip.Enabled = false;

                                btnSave.CssClass = "button";
                                btnCancel.CssClass = "button";
                                btnDeleteTrip.CssClass = "button-disable";
                                btnEditTrip.CssClass = "button-disable";
                                btnSubmitReq.CssClass = "button-disable";
                                btnCancellationReq.CssClass = "button-disable";
                                btnAcknowledge.CssClass = "button-disable";
                                disableenablefields(true);
                            }
                            else
                            {
                                btnSave.Enabled = false;
                                btnCancel.Enabled = false;
                                btnDeleteTrip.Enabled = true;
                                btnEditTrip.Enabled = true;

                                btnSave.CssClass = "button-disable";
                                btnCancel.CssClass = "button-disable";
                                btnDeleteTrip.CssClass = "button";
                                btnEditTrip.CssClass = "button";
                                disableenablefields(false);
                            }
                        }
                        else
                        {
                            btnSubmitReq.Enabled = false;
                            btnCancellationReq.Enabled = false;
                            btnAcknowledge.Enabled = false;
                            btnSave.Enabled = false;
                            btnCancel.Enabled = false;
                            btnDeleteTrip.Enabled = false;
                            btnNext.Enabled = false;
                            btnEditTrip.Enabled = false;

                            btnSubmitReq.CssClass = "button-disable";
                            btnCancellationReq.CssClass = "button-disable";
                            btnAcknowledge.CssClass = "button-disable";
                            btnSave.CssClass = "button-disable";
                            btnCancel.CssClass = "button-disable";
                            btnDeleteTrip.CssClass = "button-disable";
                            btnEditTrip.CssClass = "button-disable";
                            btnNext.CssClass = "button-disable";
                            disableenablefields(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }

        }

        private void ClearFields()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                lbHomeBase.Text = "";
                lbClientCode.Text = "";
                lbType.Text = "";
                lbName.Text = "";
                lbDepartment.Text = "";
                lbAuthorization.Text = "";
                tbHomeBase.Text = "";
                tbClientCode.Text = "";
                tbTailNo.Text = "";
                tbType.Text = "";
                tbName.Text = "";
                tbDepartment.Text = "";
                tbAuthorization.Text = "";
                tbDepartDate.Text = "";
                tbRequestDate.Text = "";
                tbPhone.Text = "";
                tbDescription.Text = "";
                lbTravelCoordinator.Text = "";
                tbTravelCoordinator.Text = "";
                tbTravelCoPhone.Text = "";
                tbTravelCoEmail.Text = "";
                chkPrivate.Checked = false;
                chkPrivate.Checked = false;
                hdHomeBase.Value = "";
                hdClientCode.Value = "";
                hdTailNo.Value = "";
                hdType.Value = "";
                hdName.Value = "";
                hdDepartment.Value = "";
                hdAuthorization.Value = "";
                lbcvTravelCoordinator.Text = "";
                lbTravelCoordinator.Text = "";
                hdnTravelCoordinatorID.Value = "";
                tbRequestorNotes.Text = "";
            }

        }


        private void ClearLabels()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                lbHomeBase.Text = "";
                lbClientCode.Text = "";
                lbType.Text = "";
                lbTravelCoordinator.Text = "";
                lbName.Text = "";
                lbDepartment.Text = "";
                lbAuthorization.Text = "";
                lbcvAuthorization.Text = "";
                lbcvClientCode.Text = "";
                lbcvDepartment.Text = "";
                lbcvTravelCoordinator.Text = "";
                lbcvHomeBase.Text = "";
                lbcvName.Text = "";
                lbcvTailNo.Text = "";
                lbcvType.Text = "";
            }
        }


        private void disableenablefields(bool status)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(status))
            {
                lbHomeBase.Enabled = status;
                lbClientCode.Enabled = status;
                lbType.Enabled = status;
                lbName.Enabled = status;
                lbDepartment.Enabled = status;
                lbAuthorization.Enabled = status;
                tbHomeBase.Enabled = status;
                tbClientCode.Enabled = status;
                tbTailNo.Enabled = status;
                tbType.Enabled = status;
                tbTravelCoordinator.Enabled = status;
                tbTravelCoPhone.Enabled = status;
                tbTravelCoEmail.Enabled = status;
                tbName.Enabled = status;
                tbDepartment.Enabled = status;
                tbAuthorization.Enabled = status;
                tbDepartDate.Enabled = status;
                tbRequestDate.Enabled = status;
                tbPhone.Enabled = status;
                tbDescription.Enabled = status;
                lbTravelCoordinator.Enabled = status;
                chkPrivate.Enabled = status;
                chkPrivate.Enabled = status;
                btnHomeBase.Enabled = status;
                if (status)
                {
                    if (UserPrincipal.Identity._clientId == null)
                        btnClientCode.Enabled = status;
                    else
                        btnClientCode.Enabled = false;
                }
                else
                    btnClientCode.Enabled = status;
                btnTailNo.Enabled = status;
                btnType.Enabled = status;
                btnName.Enabled = status;
                btnTravelCoordinator.Enabled = status;
                btnDepartment.Enabled = status;
                btnAuth.Enabled = status;
                tbRequestorNotes.Enabled = status;
                btnNameAllLegs.Enabled = status;
                btnPrivateAllLegs.Enabled = status;
                btnDepartmentAllLegs.Enabled = status;
                btnAuthorizationAllLegs.Enabled = status;
                btnDescriptionAllLegs.Enabled = status;


                if (status)
                {
                    btnHomeBase.CssClass = "browse-button";//enabled
                    btnClientCode.CssClass = "browse-button";
                    btnTailNo.CssClass = "browse-button";
                    btnType.CssClass = "browse-button";
                    btnTravelCoordinator.CssClass = "browse-button";
                    btnName.CssClass = "browse-button";
                    btnDepartment.CssClass = "browse-button";
                    btnAuth.CssClass = "browse-button";
                    btnNameAllLegs.CssClass = "copy-icon";
                    btnPrivateAllLegs.CssClass = "copy-icon";
                    btnDepartmentAllLegs.CssClass = "copy-icon";
                    btnAuthorizationAllLegs.CssClass = "copy-icon";
                    btnDescriptionAllLegs.CssClass = "copy-icon";
                }
                else
                {
                    btnHomeBase.CssClass = "browse-button-disabled";//disabled
                    btnClientCode.CssClass = "browse-button-disabled";
                    btnTailNo.CssClass = "browse-button-disabled";
                    btnType.CssClass = "browse-button-disabled";
                    btnTravelCoordinator.CssClass = "browse-button-disabled";
                    btnName.CssClass = "browse-button-disabled";
                    btnDepartment.CssClass = "browse-button-disabled";
                    btnAuth.CssClass = "browse-button-disabled";
                    btnNameAllLegs.CssClass = "copy-icon-disabled";
                    btnPrivateAllLegs.CssClass = "copy-icon-disabled";
                    btnDepartmentAllLegs.CssClass = "copy-icon-disabled";
                    btnAuthorizationAllLegs.CssClass = "copy-icon-disabled";
                    btnDescriptionAllLegs.CssClass = "copy-icon-disabled";
                }
            }
        }


        private FlightPak.Web.FlightPakMasterService.Client GetClient(Int64 ClientID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPak.Web.FlightPakMasterService.Client client = new FlightPakMasterService.Client();
                    var objRetVal = objDstsvc.GetClientCodeList();
                    List<FlightPakMasterService.Client> ClientList = new List<FlightPakMasterService.Client>();

                    if (objRetVal.ReturnFlag)
                    {

                        ClientList = objRetVal.EntityList.Where(x => x.ClientID == ClientID).ToList();
                        if (ClientList != null && ClientList.Count > 0)
                        {
                            client = ClientList[0];

                        }
                        else
                            client = null;

                    }
                    return client;
                }
            }
        }

        #region "Button Event"

        protected void btnCancelYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                        {
                            CRMain oCRMain = new CRMain();
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                oCRMain = (CRMain)Session["CurrentCorporateRequest"];
                                var RetTrip = Service.GetCRHistory(Convert.ToInt64(oCRMain.CustomerID), Convert.ToInt64(oCRMain.CRMainID));
                                StringBuilder HistoryDescription = new System.Text.StringBuilder();
                                string OldDescription = string.Empty;
                                CRHistory TripHistory = new CRHistory();
                                if (oCRMain.TripID != null || oCRMain.TripID != 0)
                                {
                                    oCRMain.CorporateRequestStatus = "X";
                                    var Result = Service.CancelRequest(oCRMain);
                                    if (Result.ReturnFlag == true)
                                    {
                                        if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                        {
                                            //TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                            TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                            TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                            TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                            TripHistory.LastUpdTS = DateTime.UtcNow;
                                            HistoryDescription.AppendLine(string.Format("Existing Trip Cancel Request Submitted To The Change Queue."));
                                            //OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                            TripHistory.LogisticsHistory = HistoryDescription.ToString();
                                            TripHistory.IsDeleted = false;
                                            var ReturnHistory = Service.AddCRHistory(TripHistory);
                                        }
                                        RadWindowManager1.RadAlert("Request canceled successfully.", 360, 50, "Corporate request", "alertCancelCallBackFn", null);
                                    }
                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("Trip Was Never Submitted To Dispatch. Please Use The Delete Button To Delete The Trip.", 360, 50, "Corporate request", "");
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }
        protected void btnCancellationReq_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CRMain oCRMain = new CRMain();
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            oCRMain = (CRMain)Session["CurrentCorporateRequest"];

                            if (oCRMain.TripID != null)
                            {
                                RadWindowManager1.RadConfirm("Submit Cancel Request To Change Queue ?", "confirmCancelRequestCallBackFn", 400, 30, null, "Corporate request");
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Trip Was Never Submitted To Dispatch. Please Use The Delete Button To Delete The Trip.", 360, 50, "Corporate request", "");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }

        /// <summary>
        /// show alert after ack req
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReload_Click(object sender, EventArgs e)
        {
            Master.RadAjaxManagerMaster.ResponseScripts.Add(@"window.location=window.location.href.split('?')[0];");
        }

        protected void btnSubmitYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                        {
                            CRMain oCRMain = new CRMain();
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                oCRMain = (CRMain)Session["CurrentCorporateRequest"];
                                var RetTrip = Service.GetCRHistory(Convert.ToInt64(oCRMain.CustomerID), Convert.ToInt64(oCRMain.CRMainID));
                                StringBuilder HistoryDescription = new System.Text.StringBuilder();
                                string OldDescription = string.Empty;
                                CRHistory TripHistory = new CRHistory();

                                //if (Session["CRStatus"] != null && Session["CRStatus"].ToString() == "Submitted")
                                //{
                                //    oCRMain.CorporateRequestStatus = "S";
                                //    Session["CRStatus"] = null;
                                //}
                                //if (Session["CRStatus"] != null && Session["CRStatus"].ToString() == "Accepted")
                                //{
                                //    oCRMain.CorporateRequestStatus = "A";
                                //    Session["CRStatus"] = null;
                                //}

                                if (oCRMain.CorporateRequestStatus != "S" && oCRMain.CorporateRequestStatus != "A")
                                {
                                    string status = "W";
                                    if (oCRMain.CorporateRequestStatus != null && oCRMain.CorporateRequestStatus.ToString().Trim() != string.Empty)
                                        status = oCRMain.CorporateRequestStatus;
                                    oCRMain.CorporateRequestStatus = "S";
                                    var Result = Service.UpdateCRMainCorpStatus(oCRMain);
                                    if (Result.ReturnFlag == true)
                                    {
                                        if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                        {
                                            //TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                            TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                            TripHistory.CustomerID = Convert.ToInt64(RetTrip.EntityList[0].CustomerID);
                                            TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                            TripHistory.LastUpdTS = DateTime.UtcNow;
                                            if (status != "W")
                                                HistoryDescription.AppendLine(string.Format("Existing Trip Request Resubmitted To The Change Queue."));
                                            else
                                                HistoryDescription.AppendLine(string.Format("New Trip Request Submitted To The Change Queue."));
                                            //OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                            TripHistory.LogisticsHistory = HistoryDescription.ToString();//OldDescription + HistoryDescription.ToString();
                                            TripHistory.IsDeleted = false;
                                            var ReturnHistory = Service.AddCRHistory(TripHistory);
                                            //Session["CRStatus"] = "Submitted";

                                            Session["CurrentCorporateRequest"] = oCRMain;
                                            tbCorpStatus.Text = "Submitted";
                                        }
                                        //string Path = this.Request.Path;
                                        //Response.Redirect(Path, false);
                                        RadWindowManager1.RadAlert("Request submitted successfully.", 360, 50, "Corporate request", "alertsubmitCallBackFn", null);
                                    }
                                }
                                else if (oCRMain.CorporateRequestStatus == "A")
                                {
                                    oCRMain.CorporateRequestStatus = "S";
                                    var Result = Service.UpdateCRMainCorpStatus(oCRMain);
                                    if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                    {
                                        // TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                        TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                        TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                        TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                        TripHistory.LastUpdTS = DateTime.UtcNow;
                                        HistoryDescription.AppendLine(string.Format("Existing Trip Request Resubmitted To The Change Queue."));
                                        //OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                        TripHistory.LogisticsHistory = HistoryDescription.ToString();//OldDescription + HistoryDescription.ToString();
                                        TripHistory.IsDeleted = false;
                                        var ReturnHistory = Service.AddCRHistory(TripHistory);
                                        Session["CurrentCorporateRequest"] = oCRMain;
                                        tbCorpStatus.Text = "Submitted";
                                    }

                                    RadWindowManager1.RadAlert("Request submitted successfully.", 360, 50, "Corporate request", "alertsubmitCallBackFn", null);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }
        protected void btnSubmitReq_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                        {
                            bool DatCheck = true;
                            DateTime CompanyTime = System.DateTime.Now;
                            double CompanySetting = 0;

                            if (UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinHours != null && UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinHours != 0)
                            {
                                CompanySetting = Convert.ToDouble(UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinHours);
                            }
                            CompanyTime = CompanyTime.AddHours(CompanySetting);
                            CRMain oCRMain = new CRMain();
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                oCRMain = (CRMain)Session["CurrentCorporateRequest"];
                                if (oCRMain.CRLegs != null && oCRMain.CRLegs.Count() > 0)
                                {
                                    foreach (CRLeg legs in oCRMain.CRLegs)
                                    {
                                        if (legs.DepartureDTTMLocal != null && legs.LegNUM == 1)
                                        {
                                            if (Convert.ToDateTime(legs.DepartureDTTMLocal) <= CompanyTime)
                                            {
                                                DatCheck = false;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            if (DatCheck == false)
                            {
                                if (UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinWeekend != null && UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinWeekend == true)
                                {
                                    if (System.DateTime.Now.DayOfWeek == DayOfWeek.Saturday || System.DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                                    {
                                        DatCheck = true;
                                    }
                                }
                            }

                            if (DatCheck == false && CompanySetting != 0)
                            {
                                RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"confirmExcludeOnSubmitMain();");
                            }
                            else
                            {

                                if (Session["CurrentCorporateRequest"] != null)
                                {
                                    oCRMain = (CRMain)Session["CurrentCorporateRequest"];

                                    if (oCRMain.CorporateRequestStatus != "S")
                                    {
                                        RadWindowManager1.RadConfirm("Submit Travel Request To Change Queue ?", "confirmSubmitCallBackFn", 400, 30, null, "Corporate request");
                                    }
                                    else if (oCRMain.CorporateRequestStatus == "S")
                                    {
                                        RadWindowManager1.RadAlert("Trip Request Is Already Submitted To The Change Queue.", 360, 50, "Corporate request", "");

                                    }
                                }
                            }



                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestLeg);
                }
            }
        }
        protected void btnAcknowledge_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                        {
                            CRMain oCRMain = new CRMain();
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                oCRMain = (CRMain)Session["CurrentCorporateRequest"];
                                var RetTrip = Service.GetCRHistory(Convert.ToInt64(oCRMain.CustomerID), Convert.ToInt64(oCRMain.CRMainID));
                                StringBuilder HistoryDescription = new System.Text.StringBuilder();
                                string OldDescription = string.Empty;
                                CRHistory TripHistory = new CRHistory();
                                if (oCRMain.AcknowledgementStatus == "A")
                                {
                                    string strLastuid = oCRMain.LastUpdUID;
                                    var ObjRetval = Service.RequestAcknowledge((long)oCRMain.CRMainID, (long)oCRMain.TripID, (long)oCRMain.CustomerID, strLastuid, Convert.ToDateTime(oCRMain.LastUpdTS));
                                    if (ObjRetval.ReturnFlag)
                                    {
                                        oCRMain = ObjRetval.EntityInfo;
                                        Session["CurrentCorporateRequest"] = oCRMain;
                                        if (RetTrip.ReturnFlag == true && RetTrip.EntityList.Count > 0)
                                        {
                                            // TripHistory.CRHistoryID = RetTrip.EntityList[0].CRHistoryID;
                                            TripHistory.CRMainID = RetTrip.EntityList[0].CRMainID;
                                            TripHistory.CustomerID = RetTrip.EntityList[0].CustomerID;
                                            TripHistory.LastUpdUID = UserPrincipal.Identity._name;
                                            TripHistory.LastUpdTS = DateTime.UtcNow;
                                            HistoryDescription.AppendLine(string.Format("Acknowledged Changes Made By Dispatch."));
                                            //OldDescription = RetTrip.EntityList[0].LogisticsHistory;
                                            TripHistory.LogisticsHistory = HistoryDescription.ToString();
                                            TripHistory.IsDeleted = false;
                                            var ReturnHistory = Service.AddCRHistory(TripHistory);
                                        }
                                        // karthik - Commented this two line to show alert message.

                                        RadWindowManager1.RadAlert("Request Acknowledged successfully.", 360, 50, "Corporate request", "RedirectPage");
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                            if (CorpRequest.CorporateRequestStatus == "W")
                            {
                                Master.Delete(CorpRequest.CRMainID);
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Trip Request has been transferred to Preflight. Please submit Cancel request to Change Queue.", 450, 50, "Corporate request", "");

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }

        protected void btnEditTrip_Click(object sender, EventArgs e)
        {
            Master.EditRequestEvent();
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
            Master.Cancel(CorpRequest);
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //bool DatCheck = true;
                        //DateTime DepartureDate = DateTime.MinValue;
                        //DateTime RequestDate = DateTime.MinValue;
                        //IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                        //if (!string.IsNullOrEmpty(tbDepartDate.Text))
                        //    DepartureDate = DateTime.ParseExact(tbDepartDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);

                        //if (!string.IsNullOrEmpty(tbRequestDate.Text))
                        //    RequestDate = DateTime.ParseExact(tbRequestDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);

                        //if (DepartureDate != DateTime.MinValue && RequestDate != DateTime.MinValue)
                        //{
                        //    if (DepartureDate < RequestDate)
                        //    {
                        //        DatCheck = false;
                        //    }
                        //}

                        //if (DatCheck)
                        //{
                        SaveToCorpRequestSession();
                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                        Master.Save(CorpRequest);
                        //}
                        //else
                        //{
                        //    RadWindowManager1.RadAlert("Departure Date must be equal to or greater than Request Date", 360, 50, "Request Alert", "");
                        //}
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ////bool DatCheck = true;
                        ////DateTime DepartureDate = DateTime.MinValue;
                        ////DateTime RequestDate = DateTime.MinValue;
                        ////IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                        ////if (!string.IsNullOrEmpty(tbDepartDate.Text))
                        ////    DepartureDate = DateTime.ParseExact(tbDepartDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);

                        ////if (!string.IsNullOrEmpty(tbRequestDate.Text))
                        ////    RequestDate = DateTime.ParseExact(tbRequestDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);

                        ////if (DepartureDate != DateTime.MinValue && RequestDate != DateTime.MinValue)
                        ////{
                        ////    if (DepartureDate < RequestDate)
                        ////    {
                        ////        DatCheck = false;
                        ////    }
                        ////}

                        ////if (DatCheck)
                        ////{
                        SaveToCorpRequestSession();
                        Master.Next("Request");
                        ////}
                        ////else
                        ////{
                        ////    RadWindowManager1.RadAlert("Departure Date must be equal to or greater than Request Date", 360, 50, "Request Alert", "");
                        ////}

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }

        }
        #endregion




        protected void tbTravelCoordinator_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        RadAjaxManager.GetCurrent(Page).FocusControl(btnTravelCoordinator);
                        lbcvTravelCoordinator.Text = string.Empty;
                        lbTravelCoordinator.Text = string.Empty;
                        hdnTravelCoordinatorID.Value = string.Empty;
                        lbcvName.Text = string.Empty;
                        lbName.Text = string.Empty;
                        tbTravelCoEmail.Text = string.Empty;
                        tbTravelCoPhone.Text = string.Empty;
                        tbDepartment.Text = string.Empty;
                        hdDepartment.Value = string.Empty;
                        lbcvDepartment.Text = string.Empty;
                        lbDepartment.Text = string.Empty;
                        tbAuthorization.Text = string.Empty;
                        hdAuthorization.Value = string.Empty;
                        lbAuthorization.Text = string.Empty;
                        lbcvAuthorization.Text = string.Empty;
                        tbName.Text = "";
                        hdName.Value = "";
                        lbName.Text = "";
                        lbcvName.Text = "";
                        tbTailNo.Text = "";
                        hdTailNo.Value = "";
                        lbcvTailNo.Text = "";
                        tbType.Text = "";
                        hdType.Value = "";
                        lbType.Text = "";
                        lbcvType.Text = "";

                        if (UserPrincipal.Identity._isSysAdmin == false && UserPrincipal.Identity._travelCoordID != null)
                        {
                            if (!string.IsNullOrEmpty(tbTravelCoordinator.Text))
                            {
                                List<FlightPakMasterService.GetTravelCoordinator> TravelCoordinatorList = new List<FlightPakMasterService.GetTravelCoordinator>();
                                using (FlightPakMasterService.MasterCatalogServiceClient TravelCoordinatorService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = TravelCoordinatorService.GetTravelCoordinatorList();
                                    if (objRetVal.ReturnFlag)
                                    {

                                        TravelCoordinatorList = objRetVal.EntityList.Where(x => x.TravelCoordCD.ToUpper().Trim() == tbTravelCoordinator.Text.ToUpper().Trim() && x.TravelCoordinatorID == UserPrincipal.Identity._travelCoordID).ToList();

                                        if (TravelCoordinatorList != null && TravelCoordinatorList.Count > 0)
                                        {
                                            lbTravelCoordinator.Text = System.Web.HttpUtility.HtmlEncode(TravelCoordinatorList[0].FirstName);
                                            hdnTravelCoordinatorID.Value = TravelCoordinatorList[0].TravelCoordinatorID.ToString();
                                            tbTravelCoordinator.Text = TravelCoordinatorList[0].TravelCoordCD;
                                            if (TravelCoordinatorList[0].BusinessEmail != null)
                                            {
                                                tbTravelCoEmail.Text = TravelCoordinatorList[0].BusinessEmail.ToString();
                                            }
                                            if (TravelCoordinatorList[0].BusinessPhone != null)
                                            {
                                                tbTravelCoPhone.Text = TravelCoordinatorList[0].BusinessPhone.ToString();
                                            }
                                            if (TravelCoordinatorList[0].FaxNum != null && TravelCoordinatorList[0].FaxNum.ToString().Trim() != "")
                                            {
                                                tbTravelCoPhone.Text += " / " + TravelCoordinatorList[0].FaxNum.ToString();
                                            }
                                            lbcvTravelCoordinator.Text = System.Web.HttpUtility.HtmlEncode("");

                                            FlightPakMasterService.TravelCoordinatorRequestor Travel = new FlightPakMasterService.TravelCoordinatorRequestor();
                                            Travel.TravelCoordinatorID = Convert.ToInt64(TravelCoordinatorList[0].TravelCoordinatorID);
                                            Travel.IsDeleted = false;
                                            var objTravelReq = TravelCoordinatorService.GetTravelCoordinatorRequestorInfo(Travel).EntityList.Where(y => (y.IsChoice == true) && (y.IsDeleted == false) && (y.TravelCoordinatorID == Convert.ToInt64(TravelCoordinatorList[0].TravelCoordinatorID))).ToList();
                                            if (objTravelReq.Count() > 0)
                                            {
                                                if (objTravelReq[0].PassengerRequestorID != null)
                                                {
                                                    var Paxlist = TravelCoordinatorService.GetPassengerbyPassengerRequestorID(Convert.ToInt64(objTravelReq[0].PassengerRequestorID)).EntityList.ToList();

                                                    if (Paxlist.Count > 0)
                                                    {
                                                        tbName.Text = Paxlist[0].PassengerRequestorCD.ToString();
                                                        hdName.Value = Paxlist[0].PassengerRequestorID.ToString();
                                                        lbName.Text = System.Web.HttpUtility.HtmlEncode(Paxlist[0].PassengerName.ToString());
                                                        lbcvName.Text = "";
                                                        tbName_TextChanged(null, null);
                                                    }
                                                }
                                                else
                                                {
                                                    tbName.Text = "";
                                                    hdName.Value = "";
                                                    lbName.Text = "";
                                                }
                                            }
                                            else
                                            {
                                                tbName.Text = "";
                                                hdName.Value = "";
                                                lbName.Text = "";
                                            }


                                            FlightPakMasterService.TravelCoordinatorFleet TravelCo = new FlightPakMasterService.TravelCoordinatorFleet();
                                            TravelCo.TravelCoordinatorID = Convert.ToInt64(TravelCoordinatorList[0].TravelCoordinatorID);
                                            TravelCo.IsDeleted = false;
                                            var objTravelReqAircraft = TravelCoordinatorService.GetTravelCoordinatorFleetInfo(TravelCo).EntityList.Where(y => (y.IsChoice == true) && (y.IsDeleted == false) && (y.TravelCoordinatorID == Convert.ToInt64(TravelCoordinatorList[0].TravelCoordinatorID))).ToList();
                                            if (objTravelReqAircraft.Count() > 0)
                                            {
                                                if (objTravelReqAircraft[0].FleetID != null)
                                                {
                                                    var Paxlist = TravelCoordinatorService.GetFleetIDInfo(Convert.ToInt64(objTravelReqAircraft[0].FleetID)).EntityList.ToList();

                                                    if (Paxlist.Count > 0)
                                                    {
                                                        tbTailNo.Text = Paxlist[0].TailNum.ToString();
                                                        hdTailNo.Value = Paxlist[0].FleetID.ToString();
                                                        lbcvTailNo.Text = "";
                                                        tbTailNo_TextChanged(null, null);
                                                    }
                                                }
                                                else
                                                {
                                                    tbTailNo.Text = "";
                                                    hdTailNo.Value = "";
                                                    lbcvTailNo.Text = "";
                                                }
                                            }
                                            else
                                            {
                                                tbTailNo.Text = "";
                                                hdTailNo.Value = "";
                                                lbcvTailNo.Text = "";
                                            }
                                        }
                                        else
                                        {
                                            lbcvTravelCoordinator.Text = System.Web.HttpUtility.HtmlEncode("Travel Coordinator Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbTravelCoordinator);
                                        }
                                    }
                                    else
                                    {
                                        lbcvTravelCoordinator.Text = System.Web.HttpUtility.HtmlEncode("Travel Coordinator Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbTravelCoordinator);
                                    }
                                }
                            }
                            else
                            {
                                lbTravelCoordinator.Text = "";
                                hdnTravelCoordinatorID.Value = "";
                                tbName.Text = "";
                                hdName.Value = "";
                                lbName.Text = "";
                                lbcvName.Text = "";
                                tbTailNo.Text = "";
                                hdTailNo.Value = "";
                                lbcvTailNo.Text = "";
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(tbTravelCoordinator.Text))
                            {
                                List<FlightPakMasterService.GetTravelCoordinator> TravelCoordinatorList = new List<FlightPakMasterService.GetTravelCoordinator>();
                                using (FlightPakMasterService.MasterCatalogServiceClient TravelCoordinatorService = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = TravelCoordinatorService.GetTravelCoordinatorList();
                                    if (objRetVal.ReturnFlag)
                                    {

                                        TravelCoordinatorList = objRetVal.EntityList.Where(x => x.TravelCoordCD.ToUpper().Trim() == tbTravelCoordinator.Text.ToUpper().Trim()).ToList();

                                        if (TravelCoordinatorList != null && TravelCoordinatorList.Count > 0)
                                        {
                                            lbTravelCoordinator.Text = System.Web.HttpUtility.HtmlEncode(TravelCoordinatorList[0].FirstName);
                                            hdnTravelCoordinatorID.Value = TravelCoordinatorList[0].TravelCoordinatorID.ToString();
                                            if (TravelCoordinatorList[0].BusinessEmail != null)
                                            {
                                                tbTravelCoEmail.Text = TravelCoordinatorList[0].BusinessEmail.ToString();
                                            }
                                            if (TravelCoordinatorList[0].BusinessPhone != null)
                                            {
                                                tbTravelCoPhone.Text = TravelCoordinatorList[0].BusinessPhone.ToString();
                                            }
                                            if (TravelCoordinatorList[0].FaxNum != null && TravelCoordinatorList[0].FaxNum.ToString().Trim() != "")
                                            {
                                                tbTravelCoPhone.Text += " / " + TravelCoordinatorList[0].FaxNum.ToString();
                                            }
                                            lbcvTravelCoordinator.Text = string.Empty;

                                            FlightPakMasterService.TravelCoordinatorRequestor Travel = new FlightPakMasterService.TravelCoordinatorRequestor();
                                            Travel.TravelCoordinatorID = Convert.ToInt64(TravelCoordinatorList[0].TravelCoordinatorID);
                                            Travel.IsDeleted = false;
                                            var objTravelReq = TravelCoordinatorService.GetTravelCoordinatorRequestorInfo(Travel).EntityList.Where(y => (y.IsChoice == true) && (y.IsDeleted == false) && (y.TravelCoordinatorID == Convert.ToInt64(TravelCoordinatorList[0].TravelCoordinatorID))).ToList();
                                            if (objTravelReq.Count() > 0)
                                            {
                                                if (objTravelReq[0].PassengerRequestorID != null)
                                                {
                                                    var Paxlist = TravelCoordinatorService.GetPassengerbyPassengerRequestorID(Convert.ToInt64(objTravelReq[0].PassengerRequestorID)).EntityList.ToList();

                                                    if (Paxlist.Count > 0)
                                                    {
                                                        tbName.Text = Paxlist[0].PassengerRequestorCD.ToString();
                                                        hdName.Value = Paxlist[0].PassengerRequestorID.ToString();
                                                        lbName.Text = System.Web.HttpUtility.HtmlEncode(Paxlist[0].PassengerName.ToString());
                                                        lbcvName.Text = string.Empty;
                                                        tbName_TextChanged(null, null);
                                                    }
                                                }
                                                else
                                                {
                                                    tbName.Text = "";
                                                    hdName.Value = "";
                                                    lbName.Text = "";
                                                }
                                            }
                                            else
                                            {
                                                tbName.Text = "";
                                                hdName.Value = "";
                                                lbName.Text = "";
                                            }


                                            FlightPakMasterService.TravelCoordinatorFleet TravelCo = new FlightPakMasterService.TravelCoordinatorFleet();
                                            TravelCo.TravelCoordinatorID = Convert.ToInt64(TravelCoordinatorList[0].TravelCoordinatorID);
                                            TravelCo.IsDeleted = false;
                                            var objTravelReqAircraft = TravelCoordinatorService.GetTravelCoordinatorFleetInfo(TravelCo).EntityList.Where(y => (y.IsChoice == true) && (y.IsDeleted == false) && (y.TravelCoordinatorID == Convert.ToInt64(TravelCoordinatorList[0].TravelCoordinatorID))).ToList();
                                            if (objTravelReqAircraft.Count() > 0)
                                            {
                                                if (objTravelReqAircraft[0].FleetID != null)
                                                {
                                                    var Paxlist = TravelCoordinatorService.GetFleetIDInfo(Convert.ToInt64(objTravelReqAircraft[0].FleetID)).EntityList.ToList();

                                                    if (Paxlist.Count > 0)
                                                    {
                                                        tbTailNo.Text = Paxlist[0].TailNum.ToString();
                                                        hdTailNo.Value = Paxlist[0].FleetID.ToString();
                                                        lbcvTailNo.Text = "";
                                                        tbTailNo_TextChanged(null, null);
                                                    }
                                                }
                                                else
                                                {
                                                    tbTailNo.Text = "";
                                                    hdTailNo.Value = "";
                                                    lbcvTailNo.Text = "";
                                                }
                                            }
                                            else
                                            {
                                                tbTailNo.Text = "";
                                                hdTailNo.Value = "";
                                                lbcvTailNo.Text = "";
                                            }
                                        }
                                        else
                                        {
                                            lbcvTravelCoordinator.Text = System.Web.HttpUtility.HtmlEncode("Travel Coordinator Does Not Exist");
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbTravelCoordinator);
                                        }
                                    }
                                    else
                                    {
                                        lbcvTravelCoordinator.Text = System.Web.HttpUtility.HtmlEncode("Travel Coordinator Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbTravelCoordinator);
                                    }
                                }
                            }
                            else
                            {
                                lbTravelCoordinator.Text = "";
                                hdnTravelCoordinatorID.Value = "";
                                tbName.Text = "";
                                hdName.Value = "";
                                lbName.Text = "";
                                lbcvName.Text = "";
                                tbTailNo.Text = "";
                                hdTailNo.Value = "";
                                lbcvTailNo.Text = "";
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }

        }

        /// <summary>
        /// To validate Tail number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbTailNo_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnTailNo);
                        lbcvType.Text = string.Empty;
                        lbcvTailNo.Text = string.Empty;
                        hdTailNo.Value = string.Empty;
                        if (UserPrincipal.Identity._isSysAdmin == false && UserPrincipal.Identity._travelCoordID != null)
                        {
                            if (!string.IsNullOrEmpty(tbTailNo.Text))
                            {
                                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    using (CorporateRequestService.CorporateRequestServiceClient CService = new CorporateRequestService.CorporateRequestServiceClient())
                                    {
                                        List<CorporateRequestService.GetAllFleetForCR> Fleetlist = new List<CorporateRequestService.GetAllFleetForCR>();
                                        var objRetVal = CService.GetCRFleet(Convert.ToInt64(UserPrincipal.Identity._travelCoordID)).EntityList.Where(x => x.TailNum.ToString().ToUpper().Trim().Equals(tbTailNo.Text.ToUpper().Trim())).ToList();
                                        if (objRetVal != null && objRetVal.Count > 0)
                                        {
                                            Fleetlist = objRetVal;
                                            if (Fleetlist != null && Fleetlist.Count > 0)
                                            {
                                                hdCalculateLeg.Value = "true";

                                                hdTailNo.Value = Fleetlist[0].FleetID.ToString();
                                                hdType.Value = Fleetlist[0].AircraftID.ToString();
                                                tbTailNo.Text = Fleetlist[0].TailNum;
                                                tbType.Text = "";
                                                lbType.Text = "";

                                                hdMaximumPassenger.Value = Fleetlist[0].MaximumPassenger != null ? Fleetlist[0].MaximumPassenger.ToString() : "0";


                                                tbTailNo.ToolTip = Fleetlist[0].Notes != null ? Fleetlist[0].Notes : string.Empty;


                                                if (hdTailNo.Value != null)
                                                {
                                                    tbType.ReadOnly = true;
                                                }
                                                else
                                                {
                                                    tbType.ReadOnly = false;
                                                }

                                                lbcvTailNo.Text = "";


                                                #region Aircraft
                                                List<FlightPak.Web.FlightPakMasterService.GetAllAircraft> AircraftList = new List<FlightPakMasterService.GetAllAircraft>();

                                                var objRetVal1 = objDstsvc.GetAircraftList();
                                                if (objRetVal1.ReturnFlag)
                                                {
                                                    AircraftList = objRetVal1.EntityList.Where(x => x.AircraftID.ToString() == hdType.Value).ToList();

                                                    if (AircraftList != null && AircraftList.Count > 0)
                                                    {
                                                        //.EntityList.Where(x => x.AircraftID.ToString() == hdType.Value).ToList();
                                                        tbType.Text = AircraftList[0].AircraftCD.ToString();
                                                        lbType.Text = System.Web.HttpUtility.HtmlEncode(AircraftList[0].AircraftDescription.ToString());
                                                        hdType.Value = AircraftList[0].AircraftID.ToString();
                                                    }
                                                }
                                                #endregion

                                                #region Homebase

                                                if (Fleetlist[0].HomebaseID != null && Fleetlist[0].HomebaseID != 0)
                                                {
                                                    Int64 HomebaseID = (long)Fleetlist[0].HomebaseID;
                                                    hdHomeBase.Value = Fleetlist[0].HomebaseID.ToString();

                                                    using (FlightPakMasterService.MasterCatalogServiceClient ObjmasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                    {
                                                        //var objCompanyRetVal = ObjmasterService.GetCompanyMasterListInfo();
                                                        var objCompanyRetVal = ObjmasterService.GetListInfoByHomeBaseId((long)Fleetlist[0].HomebaseID);
                                                        List<FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId> CompanyList = new List<FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId>();

                                                        if (objCompanyRetVal.ReturnFlag)
                                                        {

                                                            CompanyList = objCompanyRetVal.EntityList.Where(x => x.HomebaseID == HomebaseID).ToList();
                                                            if (CompanyList != null && CompanyList.Count > 0)
                                                            {
                                                                tbHomeBase.Text = CompanyList[0].HomebaseCD;
                                                                hdHomeBaseAirportID.Value = CompanyList[0].HomebaseAirportID.ToString();
                                                                lbHomeBase.Text = System.Web.HttpUtility.HtmlEncode(CompanyList[0].BaseDescription);

                                                                #region check firstleg and if details are not filled replace dep leg
                                                                if (CorpRequest != null && CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count() > 0)
                                                                {
                                                                    CRLeg firstleg = CorpRequest.CRLegs.Where(x => x.IsDeleted == false && x.LegNUM == 1 && (x.AAirportID == null || x.AAirportID == 0)).SingleOrDefault();
                                                                    if (firstleg != null)
                                                                    {

                                                                        FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId Comp = GetCompany(HomebaseID);
                                                                        if (Comp != null)
                                                                        {
                                                                            FlightPakMasterService.GetAllAirport HomebaseAirport = Master.GetAirport((long)Comp.HomebaseAirportID);
                                                                            if (HomebaseAirport != null)
                                                                            {
                                                                                CorporateRequestService.Airport DepTArr = new CorporateRequestService.Airport();
                                                                                DepTArr.AirportID = HomebaseAirport.AirportID;
                                                                                DepTArr.IcaoID = HomebaseAirport.IcaoID;
                                                                                DepTArr.AirportName = HomebaseAirport.AirportName != null ? System.Web.HttpUtility.HtmlEncode(HomebaseAirport.AirportName) : string.Empty;
                                                                                DepTArr.CityName = HomebaseAirport.CityName != null ? System.Web.HttpUtility.HtmlEncode(HomebaseAirport.CityName) : string.Empty;
                                                                                firstleg.Airport1 = DepTArr;
                                                                                firstleg.DAirportID = HomebaseAirport.AirportID;
                                                                            }
                                                                        }
                                                                    }

                                                                }
                                                                #endregion
                                                            }
                                                        }
                                                    }
                                                }

                                                #endregion
                                            }
                                            else
                                            {
                                                this.tbTailNo.ToolTip = "";
                                                lbcvTailNo.Text = System.Web.HttpUtility.HtmlEncode("Tail No. Does Not Exist");
                                                hdTailNo.Value = "";
                                                tbType.Text = "";
                                                hdType.Value = "";
                                                lbType.Text = "";
                                                tbType.ReadOnly = false;
                                                RadAjaxManager.GetCurrent(Page).FocusControl(tbTailNo);
                                            }
                                        }
                                        else
                                        {
                                            this.tbTailNo.ToolTip = "";
                                            lbcvTailNo.Text = System.Web.HttpUtility.HtmlEncode("Tail No. Does Not Exist");
                                            hdTailNo.Value = "";
                                            tbType.Text = "";
                                            hdType.Value = "";
                                            lbType.Text = "";
                                            tbType.ReadOnly = false;
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbTailNo);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                this.tbTailNo.ToolTip = "";
                                lbcvTailNo.Text = "";
                                hdTailNo.Value = "";
                                tbType.Text = "";
                                hdType.Value = "";
                                lbType.Text = "";
                                tbType.ReadOnly = false;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(tbTailNo.Text))
                            {
                                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    List<FlightPak.Web.FlightPakMasterService.Fleet> Fleetlist = new List<FlightPakMasterService.Fleet>();
                                    var objRetVal = objDstsvc.GetFleetProfileList();
                                    if (objRetVal.ReturnFlag)
                                    {
                                        Fleetlist = objRetVal.EntityList.Where(x => x.TailNum.ToString().ToUpper().Trim().Equals(tbTailNo.Text.ToUpper().Trim())).ToList();

                                        if (Fleetlist != null && Fleetlist.Count > 0)
                                        {
                                            hdCalculateLeg.Value = "true";

                                            hdTailNo.Value = Fleetlist[0].FleetID.ToString();
                                            hdType.Value = Fleetlist[0].AircraftID.ToString();
                                            tbType.Text = "";
                                            lbType.Text = "";

                                            hdMaximumPassenger.Value = Fleetlist[0].MaximumPassenger != null ? Fleetlist[0].MaximumPassenger.ToString() : "0";


                                            tbTailNo.ToolTip = Fleetlist[0].Notes != null ? Fleetlist[0].Notes : string.Empty;


                                            if (hdTailNo.Value != null)
                                            {
                                                tbType.ReadOnly = true;
                                            }
                                            else
                                            {
                                                tbType.ReadOnly = false;
                                            }

                                            lbcvTailNo.Text = "";


                                            #region Aircraft
                                            List<FlightPak.Web.FlightPakMasterService.GetAllAircraft> AircraftList = new List<FlightPakMasterService.GetAllAircraft>();

                                            var objRetVal1 = objDstsvc.GetAircraftList();
                                            if (objRetVal1.ReturnFlag)
                                            {
                                                AircraftList = objRetVal1.EntityList.Where(x => x.AircraftID.ToString() == hdType.Value).ToList();

                                                if (AircraftList != null && AircraftList.Count > 0)
                                                {
                                                    //.EntityList.Where(x => x.AircraftID.ToString() == hdType.Value).ToList();
                                                    tbType.Text = AircraftList[0].AircraftCD.ToString();
                                                    lbType.Text = System.Web.HttpUtility.HtmlEncode(AircraftList[0].AircraftDescription.ToString());
                                                    hdType.Value = AircraftList[0].AircraftID.ToString();
                                                }
                                            }
                                            #endregion

                                            #region Homebase

                                            if (Fleetlist[0].HomebaseID != null && Fleetlist[0].HomebaseID != 0)
                                            {
                                                Int64 HomebaseID = (long)Fleetlist[0].HomebaseID;
                                                hdHomeBase.Value = Fleetlist[0].HomebaseID.ToString();

                                                using (FlightPakMasterService.MasterCatalogServiceClient ObjmasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    //var objCompanyRetVal = ObjmasterService.GetCompanyMasterListInfo();
                                                    var objCompanyRetVal = ObjmasterService.GetListInfoByHomeBaseId((long)Fleetlist[0].HomebaseID);
                                                    List<FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId> CompanyList = new List<FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId>();

                                                    if (objCompanyRetVal.ReturnFlag)
                                                    {

                                                        CompanyList = objCompanyRetVal.EntityList.Where(x => x.HomebaseID == HomebaseID).ToList();
                                                        if (CompanyList != null && CompanyList.Count > 0)
                                                        {
                                                            tbHomeBase.Text = CompanyList[0].HomebaseCD;
                                                            hdHomeBaseAirportID.Value = CompanyList[0].HomebaseAirportID.ToString();
                                                            lbHomeBase.Text = System.Web.HttpUtility.HtmlEncode(CompanyList[0].BaseDescription);

                                                            #region check firstleg and if details are not filled replace dep leg
                                                            if (CorpRequest != null && CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count() > 0)
                                                            {
                                                                CRLeg firstleg = CorpRequest.CRLegs.Where(x => x.IsDeleted == false && x.LegNUM == 1 && (x.AAirportID == null || x.AAirportID == 0)).SingleOrDefault();
                                                                if (firstleg != null)
                                                                {

                                                                    FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId Comp = GetCompany(HomebaseID);
                                                                    if (Comp != null)
                                                                    {
                                                                        FlightPakMasterService.GetAllAirport HomebaseAirport = Master.GetAirport((long)Comp.HomebaseAirportID);
                                                                        if (HomebaseAirport != null)
                                                                        {
                                                                            CorporateRequestService.Airport DepTArr = new CorporateRequestService.Airport();
                                                                            DepTArr.AirportID = HomebaseAirport.AirportID;
                                                                            DepTArr.IcaoID = HomebaseAirport.IcaoID;
                                                                            DepTArr.AirportName = HomebaseAirport.AirportName != null ? System.Web.HttpUtility.HtmlEncode(HomebaseAirport.AirportName) : string.Empty;
                                                                            DepTArr.CityName = HomebaseAirport.CityName != null ? System.Web.HttpUtility.HtmlEncode(HomebaseAirport.CityName) : string.Empty;
                                                                            firstleg.Airport1 = DepTArr;
                                                                            firstleg.DAirportID = HomebaseAirport.AirportID;
                                                                        }
                                                                    }
                                                                }

                                                            }
                                                            #endregion
                                                        }
                                                    }
                                                }
                                            }

                                            #endregion
                                        }
                                        else
                                        {
                                            this.tbTailNo.ToolTip = "";
                                            lbcvTailNo.Text = System.Web.HttpUtility.HtmlEncode("Tail No. Does Not Exist");
                                            hdTailNo.Value = "";
                                            tbType.Text = "";
                                            hdType.Value = "";
                                            lbType.Text = "";
                                            tbType.ReadOnly = false;
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbTailNo);
                                        }
                                    }
                                    else
                                    {
                                        this.tbTailNo.ToolTip = "";
                                        lbcvTailNo.Text = System.Web.HttpUtility.HtmlEncode("Tail No. Does Not Exist");
                                        hdTailNo.Value = "";
                                        tbType.Text = "";
                                        hdType.Value = "";
                                        lbType.Text = "";
                                        tbType.ReadOnly = false;
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbTailNo);
                                    }
                                }
                            }
                            else
                            {
                                this.tbTailNo.ToolTip = "";
                                lbcvTailNo.Text = "";
                                hdTailNo.Value = "";
                                tbType.Text = "";
                                hdType.Value = "";
                                lbType.Text = "";
                                tbType.ReadOnly = false;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }

        }


        /// <summary>
        /// To validate the Aircraft type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbType_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnType);
                        lbType.Text = string.Empty;
                        lbcvType.Text = string.Empty;
                        if (!string.IsNullOrEmpty(tbType.Text))
                        {
                            List<FlightPak.Web.FlightPakMasterService.GetAllAircraft> AircraftList = new List<FlightPakMasterService.GetAllAircraft>();
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetAircraftList();
                                if (objRetVal.ReturnFlag)
                                {

                                    AircraftList = objRetVal.EntityList.Where(x => x.AircraftCD.ToUpper() == tbType.Text.ToUpper()).ToList();

                                    if (AircraftList != null && AircraftList.Count > 0)
                                    {
                                        hdCalculateLeg.Value = "true";
                                        lbType.Text = System.Web.HttpUtility.HtmlEncode(AircraftList[0].AircraftDescription);
                                        hdType.Value = AircraftList[0].AircraftID.ToString();
                                        tbType.Text = AircraftList[0].AircraftCD;
                                        lbcvType.Text = "";
                                    }
                                    else
                                    {
                                        lbcvType.Text = System.Web.HttpUtility.HtmlEncode("Aircraft Type Does Not Exist");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbType);
                                    }
                                }
                                else
                                {
                                    lbcvType.Text = System.Web.HttpUtility.HtmlEncode("Aircraft Type Does Not Exist");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbType);
                                }
                            }
                        }
                        else
                        {
                            lbType.Text = "";
                            hdType.Value = "";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }

        }


        /// <summary>
        /// To validate Name 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbName_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnName);
                        lbName.Text = string.Empty;
                        lbcvName.Text = string.Empty;
                        tbPhone.Text = string.Empty;

                        if (UserPrincipal.Identity._isSysAdmin == false && UserPrincipal.Identity._travelCoordID != null)
                        {
                            if (!string.IsNullOrEmpty(tbName.Text))
                            {
                                List<FlightPak.Web.CorporateRequestService.GetPassengerByPassengerRequestorIDforCR> PassengerList = new List<CorporateRequestService.GetPassengerByPassengerRequestorIDforCR>();
                                using (CorporateRequestService.CorporateRequestServiceClient objDstsvc = new CorporateRequestService.CorporateRequestServiceClient())
                                {
                                    var objRetVal = objDstsvc.GetCRPaxbyPaxID(Convert.ToInt64(UserPrincipal.Identity._travelCoordID));
                                    if (objRetVal.ReturnFlag)
                                    {
                                        PassengerList = objRetVal.EntityList.Where(x => x.PassengerRequestorCD.ToString().ToUpper().Trim().Equals(tbName.Text.ToUpper().Trim())).ToList();
                                        if (PassengerList != null && PassengerList.Count > 0)
                                        {

                                            lbName.Text = System.Web.HttpUtility.HtmlEncode(PassengerList[0].PassengerName);
                                            hdName.Value = PassengerList[0].PassengerRequestorID.ToString();
                                            tbName.Text = PassengerList[0].PassengerRequestorCD;

                                            /////********Included to remove &nbsp ********///////////
                                            if (PassengerList[0].AdditionalPhoneNum != null)
                                            {
                                                string convPhoneNumber = string.Empty;
                                                string PhoneNumber = string.Empty;
                                                convPhoneNumber = PassengerList[0].AdditionalPhoneNum.Trim();
                                                tbPhone.Text = convPhoneNumber;
                                            }
                                            /////********Included to remove &nbsp ********///////////

                                            lbcvName.Text = "";

                                            Int64 DepartmentID = 0;
                                            Int64 DepartmentAuthorizationID = 0;

                                            if (!string.IsNullOrEmpty(hdIsDepartAuthReq.Value))
                                            {
                                                if (hdIsDepartAuthReq.Value.ToLower() == "true")
                                                {
                                                    DepartmentID = PassengerList[0].DepartmentID == null ? 0 : (long)PassengerList[0].DepartmentID;
                                                    DepartmentAuthorizationID = PassengerList[0].AuthorizationID == null ? 0 : (long)PassengerList[0].AuthorizationID;

                                                    if (DepartmentID != 0)
                                                    {
                                                        FlightPakMasterService.Department dept = GetDepartment(DepartmentID);
                                                        if (dept != null)
                                                        {
                                                            tbDepartment.Text = dept.DepartmentCD;
                                                            lbDepartment.Text = System.Web.HttpUtility.HtmlEncode(dept.DepartmentName);
                                                            hdDepartment.Value = dept.DepartmentID.ToString();
                                                        }
                                                        else
                                                        {
                                                            tbDepartment.Text = string.Empty;
                                                            lbDepartment.Text = string.Empty;
                                                            hdDepartment.Value = string.Empty;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        tbDepartment.Text = string.Empty;
                                                        lbDepartment.Text = string.Empty;
                                                        hdDepartment.Value = string.Empty;
                                                    }

                                                    if (DepartmentAuthorizationID != 0)
                                                    {
                                                        FlightPakMasterService.DepartmentAuthorization deptAuth = GetAuthorization(DepartmentAuthorizationID);
                                                        if (deptAuth != null)
                                                        {
                                                            tbAuthorization.Text = deptAuth.AuthorizationCD;
                                                            lbAuthorization.Text = System.Web.HttpUtility.HtmlEncode(deptAuth.DeptAuthDescription);
                                                            hdAuthorization.Value = deptAuth.AuthorizationID.ToString();
                                                        }
                                                        else
                                                        {
                                                            tbAuthorization.Text = string.Empty;
                                                            lbAuthorization.Text = string.Empty;
                                                            hdAuthorization.Value = string.Empty;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        tbAuthorization.Text = string.Empty;
                                                        lbAuthorization.Text = string.Empty;
                                                        hdAuthorization.Value = string.Empty;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            lbcvName.Text = System.Web.HttpUtility.HtmlEncode("Passenger/Requestor Code Does Not Exist");
                                            hdName.Value = "";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbName);
                                        }
                                    }
                                    else
                                    {
                                        lbcvName.Text = System.Web.HttpUtility.HtmlEncode("Passenger/Requestor Code Does Not Exist");
                                        hdName.Value = "";
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbName);
                                    }
                                }
                            }
                            else
                            {
                                lbName.Text = "";
                                tbPhone.Text = string.Empty;
                                hdName.Value = "";
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(tbName.Text))
                            {
                                List<FlightPak.Web.FlightPakMasterService.Passenger> PassengerList = new List<FlightPakMasterService.Passenger>();
                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objDstsvc.GetPassengerRequestorList();
                                    if (objRetVal.ReturnFlag)
                                    {
                                        PassengerList = objRetVal.EntityList.Where(x => x.PassengerRequestorCD.ToString().ToUpper().Trim().Equals(tbName.Text.ToUpper().Trim())).ToList();
                                        if (PassengerList != null && PassengerList.Count > 0)
                                        {

                                            lbName.Text = System.Web.HttpUtility.HtmlEncode(PassengerList[0].PassengerName);
                                            hdName.Value = PassengerList[0].PassengerRequestorID.ToString();

                                            /////********Included to remove &nbsp ********///////////
                                            if (PassengerList[0].AdditionalPhoneNum != null)
                                            {
                                                string convPhoneNumber = string.Empty;
                                                string PhoneNumber = string.Empty;
                                                convPhoneNumber = PassengerList[0].AdditionalPhoneNum.Trim();
                                                tbPhone.Text = convPhoneNumber;
                                            }
                                            /////********Included to remove &nbsp ********///////////

                                            lbcvName.Text = "";

                                            Int64 DepartmentID = 0;
                                            Int64 DepartmentAuthorizationID = 0;

                                            if (!string.IsNullOrEmpty(hdIsDepartAuthReq.Value))
                                            {
                                                if (hdIsDepartAuthReq.Value.ToLower() == "true")
                                                {
                                                    DepartmentID = PassengerList[0].DepartmentID == null ? 0 : (long)PassengerList[0].DepartmentID;
                                                    DepartmentAuthorizationID = PassengerList[0].AuthorizationID == null ? 0 : (long)PassengerList[0].AuthorizationID;

                                                    if (DepartmentID != 0)
                                                    {
                                                        FlightPakMasterService.Department dept = GetDepartment(DepartmentID);
                                                        if (dept != null)
                                                        {
                                                            tbDepartment.Text = dept.DepartmentCD;
                                                            lbDepartment.Text = System.Web.HttpUtility.HtmlEncode(dept.DepartmentName);
                                                            hdDepartment.Value = dept.DepartmentID.ToString();
                                                        }
                                                        else
                                                        {
                                                            tbDepartment.Text = string.Empty;
                                                            lbDepartment.Text = string.Empty;
                                                            hdDepartment.Value = string.Empty;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        tbDepartment.Text = string.Empty;
                                                        lbDepartment.Text = string.Empty;
                                                        hdDepartment.Value = string.Empty;
                                                    }

                                                    if (DepartmentAuthorizationID != 0)
                                                    {
                                                        FlightPakMasterService.DepartmentAuthorization deptAuth = GetAuthorization(DepartmentAuthorizationID);
                                                        if (deptAuth != null)
                                                        {
                                                            tbAuthorization.Text = deptAuth.AuthorizationCD;
                                                            lbAuthorization.Text = System.Web.HttpUtility.HtmlEncode(deptAuth.DeptAuthDescription);
                                                            hdAuthorization.Value = deptAuth.AuthorizationID.ToString();
                                                        }
                                                        else
                                                        {
                                                            tbAuthorization.Text = string.Empty;
                                                            lbAuthorization.Text = string.Empty;
                                                            hdAuthorization.Value = string.Empty;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        tbAuthorization.Text = string.Empty;
                                                        lbAuthorization.Text = string.Empty;
                                                        hdAuthorization.Value = string.Empty;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            lbcvName.Text = System.Web.HttpUtility.HtmlEncode("Passenger/Requestor Code Does Not Exist");
                                            hdName.Value = "";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbName);
                                        }
                                    }
                                    else
                                    {
                                        lbcvName.Text = System.Web.HttpUtility.HtmlEncode("Passenger/Requestor Code Does Not Exist");
                                        hdName.Value = "";
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbName);
                                    }
                                }
                            }
                            else
                            {
                                lbName.Text = "";
                                tbPhone.Text = string.Empty;
                                hdName.Value = "";
                            }
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }

        }

        private FlightPak.Web.FlightPakMasterService.Department GetDepartment(Int64 DepartmentID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentID))
            {

                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {

                    FlightPak.Web.FlightPakMasterService.Department dept = new FlightPakMasterService.Department();
                    var objDepartment = objDstsvc.GetDepartmentList();
                    if (objDepartment.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.Department> departmentcode = (from department in objDepartment.EntityList
                                                                                                where department.DepartmentID == DepartmentID
                                                                                                select department).ToList();
                        if (departmentcode != null && departmentcode.Count > 0)
                            dept = departmentcode[0];
                    }
                    return dept;
                }
            }

        }


        private FlightPak.Web.FlightPakMasterService.DepartmentAuthorization GetAuthorization(Int64 AuthorizationID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AuthorizationID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.DepartmentAuthorization Auth = new FlightPakMasterService.DepartmentAuthorization();
                    var objAuthorization = objDstsvc.GetDepartmentAuthorizationList();
                    if (objAuthorization.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.DepartmentAuthorization> authorizationcode = (from Authorization in objAuthorization.EntityList
                                                                                                                where Authorization.AuthorizationID == AuthorizationID
                                                                                                                select Authorization).ToList();
                        if (authorizationcode != null && authorizationcode.Count > 0)
                            Auth = authorizationcode[0];
                    }
                    return Auth;
                }
            }

        }

        /// <summary>
        /// To validate the client code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbClientCode_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnClientCode);
                        lbClientCode.Text = string.Empty;
                        lbcvClientCode.Text = string.Empty;
                        if (!string.IsNullOrEmpty(tbClientCode.Text))
                        {

                            List<FlightPak.Web.FlightPakMasterService.Client> ClientList = new List<FlightPakMasterService.Client>();
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetClientCodeList();

                                if (objRetVal.ReturnFlag)
                                {

                                    ClientList = objRetVal.EntityList.Where(x => x.ClientCD.ToString().ToUpper().Trim().Equals(tbClientCode.Text.ToUpper().Trim())).ToList();


                                    if (ClientList != null && ClientList.Count > 0)
                                    {
                                        lbClientCode.Text = System.Web.HttpUtility.HtmlEncode(ClientList[0].ClientDescription);
                                        hdClientCode.Value = ClientList[0].ClientID.ToString();
                                        tbClientCode.Text = ClientList[0].ClientCD;
                                        lbcvClientCode.Text = "";
                                    }
                                    else
                                    {
                                        lbcvClientCode.Text = System.Web.HttpUtility.HtmlEncode("Client Code Does Not Exist");
                                        hdClientCode.Value = "";
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbClientCode);
                                    }
                                }
                                else
                                {
                                    lbcvClientCode.Text = System.Web.HttpUtility.HtmlEncode("Client Code Does Not Exist");
                                    hdClientCode.Value = "";
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbClientCode);
                                }
                            }
                        }
                        else
                        {
                            lbClientCode.Text = "";
                            hdClientCode.Value = "";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }

        }

        protected void tbDepartDate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                ValidateDateFormat(tbDepartDate);
                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];


                if (CorpRequest.Mode == CorporateRequestTripActionMode.Edit)
                {
                    if (!string.IsNullOrEmpty(tbDepartDate.Text))
                    {
                        DateTime dt = DateTime.ParseExact(tbDepartDate.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);
                        Master.UpdateLegDates(1, dt);
                    }
                }
                if (CorpRequest.State == CorporateRequestTripEntityState.Modified || CorpRequest.State == CorporateRequestTripEntityState.NoChange)
                {
                    CorpRequest.State = CorporateRequestTripEntityState.Modified;
                    RadAjaxManager.GetCurrent(Page).FocusControl(chkPrivate);
                }
                Session["CurrentCorporateRequest"] = CorpRequest;

                RadScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "hide()", true);

            }
        }

        protected void tbRequestDate_TextChanged(object sender, EventArgs e)
        {
            ValidateDateFormat(tbRequestDate);
            RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartment);
        }

        /// <summary>
        /// To validate Department
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbDepartment_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                RadAjaxManager.GetCurrent(Page).FocusControl(btnDepartment);
                lbDepartment.Text = string.Empty;
                lbcvDepartment.Text = string.Empty;
                if (!string.IsNullOrEmpty(tbDepartment.Text))
                {
                    List<FlightPak.Web.FlightPakMasterService.GetAllDeptAuth> GetAllDeptAuthList = new List<FlightPakMasterService.GetAllDeptAuth>();
                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var objRetVal = objDstsvc.GetAllDeptAuthorizationList();
                        if (objRetVal.ReturnFlag)
                        {
                            GetAllDeptAuthList = objRetVal.EntityList.Where(x => x.DepartmentCD.ToString().ToUpper().Trim().Equals(tbDepartment.Text.ToUpper().Trim())).ToList();
                            if (GetAllDeptAuthList != null && GetAllDeptAuthList.Count > 0)
                            {
                                lbDepartment.Text = System.Web.HttpUtility.HtmlEncode(GetAllDeptAuthList[0].DepartmentName);
                                hdDepartment.Value = GetAllDeptAuthList[0].DepartmentID.ToString();
                                tbDepartment.Text = GetAllDeptAuthList[0].DepartmentCD;
                                lbcvDepartment.Text = "";
                                tbAuthorization.Text = "";
                                lbAuthorization.Text = "";
                                hdAuthorization.Value = "";
                                lbcvAuthorization.Text = "";
                            }
                            else
                            {
                                lbcvDepartment.Text = System.Web.HttpUtility.HtmlEncode("Invalid Department Code");
                                hdDepartment.Value = "";
                                lbDepartment.Text = "";
                                hdDepartment.Value = "";
                                tbAuthorization.Text = "";
                                lbAuthorization.Text = "";
                                hdAuthorization.Value = "";
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartment);
                            }
                        }
                        else
                        {
                            lbcvDepartment.Text = System.Web.HttpUtility.HtmlEncode("Invalid Department Code");
                            hdDepartment.Value = "";
                            lbAuthorization.Text = "";
                            hdAuthorization.Value = "";
                            tbAuthorization.Text = "";
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbDepartment);
                        }
                    }
                }
                else
                {
                    lbDepartment.Text = "";
                    hdDepartment.Value = "";
                    tbAuthorization.Text = "";
                    lbAuthorization.Text = "";
                    hdAuthorization.Value = "";
                }

            }

        }

        /// <summary>
        /// To validate Home base
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbHomeBase_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnHomeBase);
                        hdHomeBase.Value = "";
                        lbHomeBase.Text = string.Empty;
                        lbcvHomeBase.Text = string.Empty;
                        if (!string.IsNullOrEmpty(tbHomeBase.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetCompanyMasterListInfo();
                                List<FlightPakMasterService.GetAllCompanyMaster> CompanyList = new List<FlightPakMasterService.GetAllCompanyMaster>();
                                var objRetValue = objDstsvc.GetAirportByAirportICaoID(tbHomeBase.Text.ToUpper().Trim());
                                List<FlightPakMasterService.GetAllAirport> getAllAirportList = new List<FlightPakMasterService.GetAllAirport>();
                                if (objRetVal.ReturnFlag)
                                {

                                    CompanyList = objRetVal.EntityList.Where(x => x.HomebaseCD != null && x.HomebaseCD.ToString().ToUpper().Trim().Equals(tbHomeBase.Text.ToUpper().Trim())).ToList();
                                    if (CompanyList != null && CompanyList.Count > 0)
                                    {
                                        lbHomeBase.Text = System.Web.HttpUtility.HtmlEncode(CompanyList[0].BaseDescription);
                                        hdHomeBase.Value = CompanyList[0].HomebaseID.ToString();
                                        if (hdHomeBase.Value != hdOldHomeBase.Value && hdOldHomeBase.Value != string.Empty)
                                        {
                                            hdCalculateLeg.Value = "true";
                                        }
                                        else if (hdOldHomeBase.Value == string.Empty && (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0))
                                        {
                                            hdCalculateLeg.Value = "true";
                                        }

                                        tbHomeBase.Text = CompanyList[0].HomebaseCD;
                                        hdHomeBaseAirportID.Value = CompanyList[0].HomebaseAirportID.ToString();
                                        lbcvHomeBase.Text = System.Web.HttpUtility.HtmlEncode("");

                                        #region Tooltip for AirportDetails
                                        if (objRetValue.ReturnFlag)
                                        {
                                            getAllAirportList = objRetValue.EntityList;
                                            if (getAllAirportList[0] != null && getAllAirportList.Count > 0)
                                            {

                                                #region check firstleg and if details are not filled replace dep leg
                                                if (Session["CurrentCorporateRequest"] != null)
                                                {
                                                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];


                                                    if (CorpRequest != null && CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count() > 0)
                                                    {
                                                        CRLeg firstleg = CorpRequest.CRLegs.Where(x => x.IsDeleted == false && x.LegNUM == 1 && (x.AAirportID == null || x.AAirportID == 0)).SingleOrDefault();
                                                        if (firstleg != null)
                                                        {
                                                            CorporateRequestService.Airport DepTArr = new CorporateRequestService.Airport();
                                                            DepTArr.AirportID = getAllAirportList[0].AirportID;
                                                            DepTArr.IcaoID = getAllAirportList[0].IcaoID;
                                                            DepTArr.AirportName = getAllAirportList[0].AirportName != null ? System.Web.HttpUtility.HtmlEncode(getAllAirportList[0].AirportName) : string.Empty;
                                                            DepTArr.CityName = getAllAirportList[0].CityName != null ? System.Web.HttpUtility.HtmlEncode(getAllAirportList[0].CityName) : string.Empty;
                                                            firstleg.Airport1 = DepTArr;
                                                            firstleg.DAirportID = getAllAirportList[0].AirportID;
                                                        }

                                                    }
                                                    Session["CurrentCorporateRequest"] = CorpRequest;
                                                }
                                                #endregion
                                                string builder = string.Empty;
                                                builder = "ICAO : " + (getAllAirportList[0].IcaoID != null ? getAllAirportList[0].IcaoID : string.Empty)
                                                    + "\n" + "City : " + (getAllAirportList[0].CityName != null ? getAllAirportList[0].CityName : string.Empty)
                                                    + "\n" + "State/Province : " + (getAllAirportList[0].StateName != null ? getAllAirportList[0].StateName : string.Empty)
                                                    + "\n" + "Country : " + (getAllAirportList[0].CountryName != null ? getAllAirportList[0].CountryName : string.Empty)
                                                    + "\n" + "Airport : " + (getAllAirportList[0].AirportName != null ? getAllAirportList[0].AirportName : string.Empty)
                                                    + "\n" + "DST Region : " + (getAllAirportList[0].DSTRegionCD != null ? getAllAirportList[0].DSTRegionCD : string.Empty)
                                                    + "\n" + "UTC+/- : " + (getAllAirportList[0].OffsetToGMT != null ? getAllAirportList[0].OffsetToGMT.ToString() : string.Empty)
                                                    + "\n" + "Longest Runway : " + (getAllAirportList[0].LongestRunway != null ? getAllAirportList[0].LongestRunway.ToString() : string.Empty)
                                                     + "\n" + "IATA : " + (getAllAirportList[0].Iata != null ? getAllAirportList[0].Iata.ToString() : string.Empty);

                                                //lbArriveIcao.ToolTip = builder;
                                                lbHomeBase.ToolTip = builder;
                                                lbHomeBaseIcao.ToolTip = builder;
                                            }
                                        }
                                        #endregion

                                    }
                                    else
                                    {
                                        lbcvHomeBase.Text = System.Web.HttpUtility.HtmlEncode("Homebase Code Does Not Exist");
                                        hdHomeBase.Value = "";
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbHomeBase);
                                    }
                                }
                                else
                                {
                                    lbcvHomeBase.Text = System.Web.HttpUtility.HtmlEncode("Homebase Code Does Not Exist");
                                    hdHomeBase.Value = "";
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbHomeBase);
                                }
                            }
                        }
                        else
                        {
                            lbHomeBase.Text = "";
                            hdHomeBase.Value = "";
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }

        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.ToString() == "tbHomeBase_TextChanged")
                tbHomeBase_TextChanged(sender, e);
            if (e.Argument.ToString() == "tbTailNo_TextChanged")
                tbTailNo_TextChanged(sender, e);
            if (e.Argument.ToString() == "tbDepartment_TextChanged")
                tbDepartment_TextChanged(sender, e);
            if (e.Argument.ToString() == "tbName_TextChanged")
                tbName_TextChanged(sender, e);
            if (e.Argument.ToString() == "tbAuthorization_TextChanged")
                tbAuthorization_TextChanged(sender, e);
            if (e.Argument.ToString() == "tbType_TextChanged")
                tbType_TextChanged(sender, e);

        }


        /// <summary>
        /// To validate Auth
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbAuthorization_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(hdDepartment.Value))
                        {
                            RadAjaxManager.GetCurrent(Page).FocusControl(btnAuth);
                            lbAuthorization.Text = string.Empty;
                            lbcvAuthorization.Text = string.Empty;
                            if (!string.IsNullOrEmpty(tbAuthorization.Text))
                            {
                                List<FlightPak.Web.FlightPakMasterService.DepartmentAuthorization> DepartmentAuthorizationList = new List<FlightPakMasterService.DepartmentAuthorization>();
                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objDstsvc.GetDepartmentAuthorizationList();
                                    if (objRetVal.ReturnFlag)
                                    {
                                        DepartmentAuthorizationList = objRetVal.EntityList.Where(x => x.AuthorizationCD.ToString().ToUpper().Trim().Equals(tbAuthorization.Text.ToUpper().Trim()) && x.DepartmentID == Convert.ToInt64(hdDepartment.Value)).ToList();

                                        if (DepartmentAuthorizationList != null && DepartmentAuthorizationList.Count > 0)
                                        {
                                            lbAuthorization.Text = System.Web.HttpUtility.HtmlEncode(DepartmentAuthorizationList[0].DeptAuthDescription);
                                            hdAuthorization.Value = DepartmentAuthorizationList[0].AuthorizationID.ToString();
                                            lbcvAuthorization.Text = System.Web.HttpUtility.HtmlEncode("");
                                            tbAuthorization.Text = DepartmentAuthorizationList[0].AuthorizationCD;
                                        }
                                        else
                                        {
                                            lbcvAuthorization.Text = System.Web.HttpUtility.HtmlEncode("Invalid Authorization Code");
                                            hdAuthorization.Value = "";
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbAuthorization);
                                        }
                                    }
                                    else
                                    {
                                        lbcvAuthorization.Text = System.Web.HttpUtility.HtmlEncode("Invalid Authorization Code");
                                        hdAuthorization.Value = "";
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbAuthorization);
                                    }
                                }
                            }
                            else
                            {
                                lbAuthorization.Text = "";
                                hdAuthorization.Value = "";
                                //hdDept.Value = "";
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(tbAuthorization.Text))
                                lbcvAuthorization.Text = "Department Not Selected";
                            else
                                lbcvAuthorization.Text = "";
                            hdAuthorization.Value = "";
                            RadAjaxManager.GetCurrent(Page).FocusControl(tbAuthorization);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }

        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
        }

        private bool ValidateDateFormat(TextBox TxtDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TxtDate))
            {
                bool ReturnValue = true;
                if (!string.IsNullOrEmpty(TxtDate.Text))
                {
                    DateTime DDate = new DateTime();
                    if (Master.DatePicker.DateInput.DateFormat != null)
                    {
                        DDate = Convert.ToDateTime(FormatDate(TxtDate.Text, Master.DatePicker.DateInput.DateFormat, false));
                    }
                    else
                    {
                        DDate = Convert.ToDateTime(TxtDate.Text);
                    }
                    if ((DDate.Year < 1900) || (DDate.Year > 2100))
                    {
                        RadWindowManager1.RadAlert("Please enter / select Date between 01/01/1900 and 12/31/2100", 330, 100, "Request Alert", null);
                        TxtDate.Text = string.Empty;
                        TxtDate.Focus();
                        ReturnValue = false;
                    }
                }
                return ReturnValue;
            }
        }
        public string FormatDate(string Date, string Format, bool AppendTime)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Date, Format))
            {
                string[] DateAndTime = Date.Split(' ');
                string[] SplitDate = new string[3];
                string[] SplitFormat = new string[3];
                if (Format.Contains("/"))
                {
                    SplitFormat = Format.Split('/');
                }
                else if (Format.Contains("-"))
                {
                    SplitFormat = Format.Split('-');
                }
                else if (Format.Contains("."))
                {
                    SplitFormat = Format.Split('.');
                }
                if (DateAndTime[0].Contains("/"))
                {
                    SplitDate = DateAndTime[0].Split('/');
                }
                else if (DateAndTime[0].Contains("-"))
                {
                    SplitDate = DateAndTime[0].Split('-');
                }
                else if (DateAndTime[0].Contains("."))
                {
                    SplitDate = DateAndTime[0].Split('.');
                }
                int dd = 0, mm = 0, yyyy = 0;
                for (int Index = 0; Index < SplitFormat.Count(); Index++)
                {
                    if (SplitFormat[Index].ToLower().Contains("d"))
                    {
                        dd = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("m"))
                    {
                        mm = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("y"))
                    {
                        yyyy = Convert.ToInt16(SplitDate[Index]);
                    }
                }
                //return new DateTime(yyyy, mm, dd);
                if (AppendTime == true)
                {
                    return (yyyy + "/" + mm + "/" + dd + ' ' + DateAndTime[1]);
                }
                else
                {
                    return (yyyy + "/" + mm + "/" + dd);
                }
            }
        }
    }
}
