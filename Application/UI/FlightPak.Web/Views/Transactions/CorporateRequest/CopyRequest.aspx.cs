﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.CorporateRequestService;
using System.Globalization;
using FlightPak.Web.Framework.Prinicipal;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Text.RegularExpressions;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common;

namespace FlightPak.Web.Views.Transactions.CorporateRequest
{
    public partial class CopyRequest : BaseSecuredPage
    {
        private ExceptionManager exManager;
        private Delegate _openCorpCopyClick;

        public Delegate OpenClick
        {
            set { _openCorpCopyClick = value; }
        }

        protected void Open_click(object sender, EventArgs e)
        {
            _openCorpCopyClick.DynamicInvoke();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
            {
                RadDatePicker1.DateInput.DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                RadDatePicker1.DateInput.DisplayDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;                
            }
        }

        protected void bntCopyNewDeptDate_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbNewDepartureDate.Text))
            {


                List<DateTime> dtList = new List<DateTime>();

                DateTime dateDepartTime = new DateTime();

                dateDepartTime = DateTime.ParseExact(tbNewDepartureDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);


                CRMain CorpRequest = new CRMain();
                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                if (CorpRequest != null && CorpRequest.CRMainID != 0)
                {


                    using (CorporateRequestServiceClient objCorpClient = new CorporateRequestServiceClient())
                    {

                        bool varLogistics = chkIncludeLogistics.Checked;
                        bool varPAXManifest = chkIncludePassengerManifest.Checked;
                        bool varLegs = chkLegs.Checked;
                        bool varAll = chkAll.Checked;


                        dtList.Add(dateDepartTime);

                        var ObjRetval = objCorpClient.CopyRequestDetails(dateDepartTime, CorpRequest.CRMainID, (long)CorpRequest.CustomerID, varLogistics, varPAXManifest, varLegs, varAll);
                        if (ObjRetval.ReturnFlag)
                        {
                            CorpRequest = ObjRetval.EntityInfo;

                           
                            CorpRequest.Mode = CorporateRequestTripActionMode.NoChange;
                            CorpRequest.State = CorporateRequestTripEntityState.NoChange;
                            Session["CurrentCorporateRequest"] = CorpRequest;

                            var ObjRetvalExp = objCorpClient.GetRequestExceptionList(CorpRequest.CRMainID);
                            if (ObjRetvalExp.ReturnFlag)
                            {
                                Session["CoporateRequestException"] = ObjRetvalExp.EntityList;
                            }
                            else
                                Session.Remove("CoporateRequestException");

                            CorpRequest.AllowTripToNavigate = true;
                            CorpRequest.AllowTripToSave = true;

                            RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                        }
                    }
                }
            }
            else
            {
                RadWindowManager1.RadAlert("New Depart Date is Mandatory", 330, 100, "Advance Copy Alert", "alertCallBackFn", null);               
            }

        }

        protected void chkAll_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkAll.Checked)
                        {
                            chkLegs.Checked = false;
                            chkIncludeLogistics.Checked = false;
                            chkIncludePassengerManifest.Checked = false;
                            chkLegs.Enabled = false;
                            chkIncludeLogistics.Enabled = false;
                            chkIncludePassengerManifest.Enabled = false;
                        }
                        if (!chkAll.Checked)
                        {
                            chkLegs.Enabled = true;
                            chkIncludeLogistics.Enabled = true;
                            chkIncludePassengerManifest.Enabled = true;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }


        protected void CRLogisticsCheckbox_OnCheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkLegs.Checked && chkIncludeLogistics.Checked && chkIncludePassengerManifest.Checked)
                        {
                            chkAll.Checked = false;
                            //chkAll.Enabled = false;
                        }
                        else
                        {
                            chkAll.Enabled = true;
                        }

                        if (chkIncludeLogistics.Checked)
                        {
                            chkLegs.Checked = false;
                        }
                        else if (chkIncludePassengerManifest.Checked)
                        {
                            chkLegs.Checked = false;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }

    }
}