﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.PreflightService;
using FlightPak.Web.CorporateRequestService;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Views.Transactions.CorporateRequest
{
    public partial class PaxPassportVisa : BaseSecuredPage
    {
        private string PaxID;
        private string Leg;
        private string PaxName;
        private ExceptionManager exManager;
        public CRMain Trip = new CRMain();

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["PassengerRequestorID"]))
                        {
                            PaxID = Request.QueryString["PassengerRequestorID"];
                        }
                        if (!string.IsNullOrEmpty(Request.QueryString["PaxLeg"]))
                        {
                            Leg = Request.QueryString["PaxLeg"];
                        }
                        if (!string.IsNullOrEmpty(Request.QueryString["PaxName"]))
                        {
                            PaxName = Request.QueryString["PaxName"];
                            Page.Title += " - " + PaxName;
                        }
                        if (!IsPostBack)
                        {
                            lbMessage.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                //catch (System.NullReferenceException ex) { }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }

        protected void dgPassport_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            LinkButton lbtnInitEdit = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            LinkButton lbtnInsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            Button btnSubmit = (e.Item as GridCommandItem).FindControl("btnSubmit") as Button;

                            if (Request.QueryString["flag"] == null)
                            {
                                btnSubmit.Visible = false;
                                lbtnInsertButton.Visible = true;

                                if (Session["CurrentCorporateRequest"] != null)
                                {
                                    Trip = (CRMain)Session["CurrentCorporateRequest"];
                                    if (Trip != null && Trip.Mode != CorporateRequestTripActionMode.Edit)
                                    {
                                        lbtnInsertButton.Visible = false;
                                        lbtnInitEdit.Visible = false;
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }

        protected void Visa_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            LinkButton lbtnInitEdit = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;

                            if (Request.QueryString["flag"] == null)
                            {
                                if (Session["CurrentCorporateRequest"] != null)
                                {
                                    Trip = (CRMain)Session["CurrentCorporateRequest"];
                                    if (Trip != null && Trip.Mode != CorporateRequestTripActionMode.Edit)
                                    {
                                        lbtnInitEdit.Visible = false;
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }

        /// <summary>
        /// Method to trigger Grid Item Command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgVisa_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                try
                {
                    string resolvedurl = string.Empty;
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                break;
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;


                                Session["PassengerRequestorID"] = PaxID;
                                TryResolveUrl("/Views/Settings/People/PassengerCatalog.aspx?IsPopup=", out resolvedurl);
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'rdPaxInfo');", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radPaxInfoCRUDPopup');", true);
                                break;
                            case RadGrid.PerformInsertCommandName:
                                e.Canceled = true;
                                TryResolveUrl("/Views/Settings/People/PassengerCatalog.aspx?IsPopup=Add", out resolvedurl);
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'rdPaxInfo');", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radPaxInfoCRUDPopup');", true);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    column.CurrentFilterValue = string.Empty;
                                    column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }

        /// <summary>
        /// Method to trigger Grid Item Command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgPassport_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                try
                {
                    string resolvedurl = string.Empty;
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                InsertSelectedRow();
                                break;
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;


                                Session["PassengerRequestorID"] = PaxID;
                                TryResolveUrl("/Views/Settings/People/PassengerCatalog.aspx?IsPopup=", out resolvedurl);
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'rdPaxInfo');", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radPaxInfoCRUDPopup');", true);
                                break;
                            case RadGrid.PerformInsertCommandName:
                                e.Canceled = true;
                                TryResolveUrl("/Views/Settings/People/PassengerCatalog.aspx?IsPopup=Add", out resolvedurl);
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'rdPaxInfo');", true);
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Modal Window", "openWin2('" + resolvedurl + "', 'radPaxInfoCRUDPopup');", true);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    column.CurrentFilterValue = string.Empty;
                                    column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }

        /// <summary>
        /// Method to call Insert Selected Row
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgPassport_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        InsertSelectedRow();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }

        protected void dgPassport_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;

                            if (Item["PassportExpiryDT"].Text != "&nbsp;")
                            {
                                DateTime PassportExpiryDT = (DateTime)DataBinder.Eval(Item.DataItem, "PassportExpiryDT");
                                Item["PassportExpiryDT"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", PassportExpiryDT));
                            }
                            if (Item["IssueDT"].Text != "&nbsp;")
                            {
                                DateTime IssueDT = (DateTime)DataBinder.Eval(Item.DataItem, "IssueDT");
                                Item["IssueDT"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", IssueDT));
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }

        /// <summary>
        /// Method to Bind into to session, and 
        /// Call "CloseAndRebind" javascript function to Close and
        /// Rebind the selected record into parent page.
        /// </summary>
        private void InsertSelectedRow()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentCorporateRequest"] != null)
                {
                    Trip = (CRMain)Session["CurrentCorporateRequest"];
                    if (Trip != null && Trip.Mode == CorporateRequestTripActionMode.Edit)
                    {

                        if (dgPassport.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgPassport.SelectedItems[0];

                            foreach (GridDataItem dataItem in dgPassport.SelectedItems)
                            {
                                if (dataItem.Selected)
                                {
                                    Session["PassSelectedPax"] = PaxID;
                                    Session["PaxPassSelectedLeg"] = Leg;
                                    Session["SelectedPassport"] = dataItem.GetDataKeyValue("PassportNum").ToString();
                                    Session["SelectedPassportID"] = dataItem.GetDataKeyValue("PassportID").ToString();
                                    Session["SelectedPrefPaxPassportExpiryDT"] = dataItem.GetDataKeyValue("PassportExpiryDT").ToString();
                                    Session["SelectedPrefPaxCountryCD"] = dataItem.GetDataKeyValue("CountryCD").ToString();
                                }
                            }

                            foreach (GridDataItem visaItem in dgVisa.SelectedItems)
                            {
                                if (visaItem.Selected)
                                {
                                    Session["SelectedVisaID"] = visaItem.GetDataKeyValue("VisaID").ToString();
                                }
                            }

                            RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                        }
                    }
                }
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Passport_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                using (MasterCatalogServiceClient Service = new MasterCatalogServiceClient())
                {
                    List<GetAllCrewPassport> PassengerPassportList = new List<GetAllCrewPassport>();
                    FlightPakMasterService.CrewPassengerPassport PassengerPassport = new FlightPakMasterService.CrewPassengerPassport();
                    PassengerPassport.CrewID = null;
                    PassengerPassport.PassengerRequestorID = Convert.ToInt64(PaxID);
                    var objPassengerPassport = Service.GetCrewPassportListInfo(PassengerPassport);
                    if (objPassengerPassport.ReturnFlag == true)
                    {
                        PassengerPassportList = objPassengerPassport.EntityList.Where(x => x.PassengerRequestorID == Convert.ToInt64(PaxID) && x.IsDeleted == false).ToList();
                        dgPassport.DataSource = PassengerPassportList;
                        //dgPassport.DataBind();
                        Session["PassengerPassport"] = PassengerPassportList;
                    }
                }
            }
        }


        /// <summary>
        /// Method to Bind Visa Details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Visa_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                using (MasterCatalogServiceClient Service = new MasterCatalogServiceClient())
                {
                    CrewPassengerVisa PaxVisa = new CrewPassengerVisa();
                    PaxVisa.PassengerRequestorID = Convert.ToInt64(PaxID);
                    PaxVisa.CrewID = null;
                    var CrewPaxVisaList = Service.GetCrewVisaListInfo(PaxVisa);

                    if (CrewPaxVisaList.ReturnFlag == true)
                    {
                        List<GetAllCrewPaxVisa> PaxPassengerVisaList = new List<GetAllCrewPaxVisa>();
                        PaxPassengerVisaList = CrewPaxVisaList.EntityList;
                        dgVisa.DataSource = PaxPassengerVisaList;
                        Session["PaxVisa"] = PaxPassengerVisaList;
                    }
                }
            }
        }

        protected void Visa_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;

                            if (Item["ExpiryDT"].Text != "&nbsp;")
                            {
                                DateTime ExpiryDT = (DateTime)DataBinder.Eval(Item.DataItem, "ExpiryDT");
                                Item["ExpiryDT"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", ExpiryDT));
                            }


                            if (Item["IssueDate"].Text != "&nbsp;")
                            {
                                DateTime IssueDate = (DateTime)DataBinder.Eval(Item.DataItem, "IssueDate");
                                Item["IssueDate"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", IssueDate));
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightPAX);
                }
            }
        }
    }
}