﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.CorporateRequestService;
using FlightPak.Web.FlightPakMasterService;
using System.Text.RegularExpressions;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Transactions.CorporateRequest
{
    public partial class CorporateLegsPopup : BaseSecuredPage
    {
        private string PreflightLeg;
        public Int64 CRMainID = 0;
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["MultiSelect"] != null)
            {
                if (Request.QueryString["MultiSelect"].ToString() == "true")
                {
                    dgLegSummary.AllowMultiRowSelection = true;
                }
                else
                {
                    dgLegSummary.AllowMultiRowSelection = false;
                }
            }
            else
            {
                dgLegSummary.AllowMultiRowSelection = false;
            }
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Request.QueryString["CRMainID"] != null)
                        {
                            CRMainID = Convert.ToInt64(Request.QueryString["CRMainID"]);
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }

        }

        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgLegSummary_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        using (CorporateRequestServiceClient ObjService = new CorporateRequestServiceClient())
                        {
                            var objRetVal = ObjService.GetRequest(CRMainID);
                            List<CRLeg> Dt = new List<CRLeg>();
                            Dt = objRetVal.EntityList[0].CRLegs;
                            if (Dt.Count > 0)
                            {
                                dgLegSummary.DataSource = (from u in Dt
                                                           where (u.Airport != null && u.Airport1 != null && u.IsDeleted == false)
                                                           select
                                                            new
                                                            {
                                                                LegNum = u.LegNUM,
                                                                CRLegID = u.CRLegID,
                                                                DepartAir = u.Airport.IcaoID == null ? string.Empty : u.Airport1.IcaoID,
                                                                ArrivalAir = u.Airport1.IcaoID == null ? string.Empty : u.Airport.IcaoID,
                                                                Departcity = u.Airport1.CityName,
                                                                Arrivecity = u.Airport.CityName,
                                                                DepartAirportID = u.DAirportID,
                                                                ArrivalAirportID = u.AAirportID,
                                                            }).OrderBy(x => x.LegNum);

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }

        }




        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgLegSummary;
                        SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Company);
                }
            }

        }

        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (PreflightLeg != null)
                {
                    foreach (GridDataItem Item in dgLegSummary.MasterTableView.Items)
                    {
                        if (Item["LegNUM"].Text.Trim() == PreflightLeg)
                        {
                            Item.Selected = true;
                            break;
                        }
                    }
                }

            }

        }

        protected void dgLegSummary_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;
                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
            }
        }

        protected void dgLegSummary_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgLegSummary, Page.Session);
        }

    }
}