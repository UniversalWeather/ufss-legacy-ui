﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrewActivityPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.PreFlight.CrewActivityPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Crew Activity</title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="divGridPanel">
            <asp:ScriptManager ID="scr1" runat="server">
            </asp:ScriptManager>
            <telerik:RadDatePicker ID="RadDatePicker3" Style="display: none;" runat="server">
                <ClientEvents OnDateSelected="dateSelected" />
                <DateInput ID="DateInput3" DateFormat="MM/dd/yyyy" runat="server">
                </DateInput>
                <Calendar ID="Calendar3" runat="server">
                    <SpecialDays>
                        <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                            ItemStyle-BorderStyle="Solid">
                        </telerik:RadCalendarDay>
                    </SpecialDays>
                </Calendar>
            </telerik:RadDatePicker>
            <telerik:RadDatePicker ID="RadDatePicker4" Style="display: none;" runat="server">
                <ClientEvents OnDateSelected="dateSelected" />
                <DateInput ID="DateInput4" DateFormat="MM/dd/yyyy" runat="server">
                </DateInput>
                <Calendar ID="Calendar4" runat="server">
                    <SpecialDays>
                        <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                            ItemStyle-BorderStyle="Solid">
                        </telerik:RadCalendarDay>
                    </SpecialDays>
                </Calendar>
            </telerik:RadDatePicker>
            <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
                <script type="text/javascript">
                    var currentTextBox = null;
                    var currentDatePicker = null;

                    //This method is called to handle the onclick and onfocus client side events for the texbox
                    function showPopup(sender, e) {
                        //this is a reference to the texbox which raised the event
                        //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                        currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                        //this gets a reference to the datepicker, which will be shown, to facilitate
                        //the selection of a date
                        var datePicker = $find("<%= RadDatePicker3.ClientID %>");

                    //this variable is used to store a reference to the date picker, which is currently
                    //active
                    currentDatePicker = datePicker;

                    //this method first parses the date, that the user entered or selected, and then
                    //sets it as a selected date to the picker
                    datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));

                    //the code lines below show the calendar, which is used to select a date. The showPopup
                    //function takes three arguments - the x and y coordinates where to show the calendar, as
                    //well as its height, derived from the offsetHeight property of the textbox
                    var position = datePicker.getElementPosition(currentTextBox);
                    datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
                }

                //this handler is used to set the text of the TextBox to the value of selected from the popup
                function dateSelected(sender, args) {
                    if (currentTextBox != null) {
                        //currentTextBox is the currently selected TextBox. Its value is set to the newly selected
                        //value of the picker
                        currentTextBox.value = args.get_newValue();
                    }
                }

                //this function is used to parse the date entered or selected by the user
                function parseDate(sender, e) {                   
                    if (currentDatePicker != null) {
                        var date = currentDatePicker.get_dateInput().parseDate(sender.value);                       
                        var dateInput = currentDatePicker.get_dateInput();                      
                        var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());                       
                        sender.value = formattedDate;
                    }
                }
                function tbDate_OnKeyDown(sender, event) {
                    if (event.keyCode == 9) {
                        var datePicker = $find("<%= RadDatePicker3.ClientID %>");
                        datePicker.hidePopup();
                        return true;
                    }
                }

                function alertCallBackFn(arg) {
                    document.getElementById("<%=tbHistory.ClientID%>").focus();
                }

                </script>
                <script type="text/javascript">
                    var currentTextBox = null;
                    var currentDatePicker = null;

                    //This method is called to handle the onclick and onfocus client side events for the texbox
                    function showfuturePopup(sender, e) {
                        //this is a reference to the texbox which raised the event
                        //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                        currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                        //this gets a reference to the datepicker, which will be shown, to facilitate
                        //the selection of a date
                        var datePicker = $find("<%= RadDatePicker4.ClientID %>");

                    //this variable is used to store a reference to the date picker, which is currently
                    //active
                    currentDatePicker = datePicker;

                    //this method first parses the date, that the user entered or selected, and then
                    //sets it as a selected date to the picker
                    datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));

                    //the code lines below show the calendar, which is used to select a date. The showPopup
                    //function takes three arguments - the x and y coordinates where to show the calendar, as
                    //well as its height, derived from the offsetHeight property of the textbox
                    var position = datePicker.getElementPosition(currentTextBox);
                    datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
                }

                //this handler is used to set the text of the TextBox to the value of selected from the popup
                function dateSelected(sender, args) {
                    if (currentTextBox != null) {
                        //currentTextBox is the currently selected TextBox. Its value is set to the newly selected
                        //value of the picker
                        currentTextBox.value = args.get_newValue();
                    }
                }

                //this function is used to parse the date entered or selected by the user
                function parseDate(sender, e) {
                    if (currentDatePicker != null) {
                        var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                        var dateInput = currentDatePicker.get_dateInput();
                        var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                        sender.value = formattedDate;
                    }
                }
                function tbDate_OnKeyDown(sender, event) {
                    if (event.keyCode == 9) {
                        var datePicker = $find("<%= RadDatePicker4.ClientID %>");
                        datePicker.hidePopup();
                        return true;
                    }
                }

                function alertCallBackFn(arg) {
                    document.getElementById("<%=tbFuture.ClientID%>").focus();
                }

                </script>
                <script type="text/javascript">
                    function alertFuturedateCallFn(arg) {
                        document.getElementById("<%=tbFuture.ClientID%>").focus();
                }
                function alertHistorydateCallFn(arg) {
                    document.getElementById("<%=tbHistory.ClientID%>").focus();
                }

                </script>
            </telerik:RadCodeBlock>
            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
            <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            </telerik:RadWindowManager>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>Future:
                                </td>
                                <%-- <td>
                                <asp:TextBox ID="tbDateRange" runat="server" Enabled="false" Text="01/26/1990"></asp:TextBox>
                            </td>--%>
                                <td>
                                    <asp:Label ID="Label1" runat="server"></asp:Label>
                                    <asp:TextBox ID="tbFuture" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                        onclick="showfuturePopup(this, event);" onblur="parseDate(this, event);"
                                        onkeydown="return tbDate_OnKeyDown(this, event);" CssClass="text70" runat="server"></asp:TextBox>
                                    <asp:Button ID="btnFutureSearch" CssClass="button" runat="server" Text="Search" OnClick="btnFutureSearch_OnClick" />
                                </td>
                                <%--    <td>
                                <telerik:RadDatePicker ID="RadDatePicker2" runat="server" ShowPopupOnFocus="True"
                                    Skin="Vista" EnableTyping="False" OnSelectedDateChanged="RadDatePicker2_SelectedDateChanged">
                                    <DateInput ID="DateInput2" runat="server" ReadOnly="True">
                                    </DateInput>
                                    <Calendar ID="Calendar2" runat="server" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                        ViewSelectorText="x" Skin="Vista" OnDayRender="CustomizefutureDay">
                                        <SpecialDays>
                                            <telerik:RadCalendarDay Date="2009-06-24" IsDisabled="True" IsSelectable="False">
                                            </telerik:RadCalendarDay>
                                        </SpecialDays>
                                        <DisabledDayStyle Font-Bold="True" Font-Strikeout="True" />
                                    </Calendar>
                                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                </telerik:RadDatePicker>
                            </td>--%>
                                <td class="tdLabel160" align="center">Activity for Crew Member
                                </td>
                                <td class="tdLabel180">
                                    <asp:TextBox ID="tbCrewMember" runat="server" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td>
                                    <div class="tblspace_10">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Button ID="btnaddColumns" runat="server" Text="+/- Columns" Visible="false"
                            CssClass="button" />
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <telerik:RadGrid ID="dgCrewActivity" runat="server" OnNeedDataSource="CrewActivity_BindData"
                            OnItemDataBound="CrewActivity_ItemDataBound" EnableAJAX="True" AutoGenerateColumns="false"
                            Height="200px" Width="930px" AllowPaging="False" PagerStyle-AlwaysVisible="false"
                            AllowScroll="true">
                            <MasterTableView DataKeyNames="" CommandItemDisplay="None" AllowFilteringByColumn="false"
                                TableLayout="Fixed" ItemStyle-HorizontalAlign="Left" AllowSorting="false" AllowPaging="false"
                                Font-Size="8" HeaderStyle-HorizontalAlign="Left">
                                <Columns>
                                    <telerik:GridBoundColumn UniqueName="DtyPeriod" HeaderText="Duty Period" HeaderStyle-HorizontalAlign="Left"
                                        DataField="DtyPeriod">
                                        <HeaderStyle Width="67px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="TailNo" HeaderText="Tail No." DataField="TailNo">
                                        <HeaderStyle Width="85px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="Duty" HeaderText="Duty" DataField="Duty">
                                        <HeaderStyle Width="30px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="LocDep" HeaderText="Local Dep Date/Time" DataField="LocDep">
                                        <HeaderStyle Width="63px" />
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridBoundColumn UniqueName="UtcDep" HeaderText="UTC.Dep Dt/Tm" DataField="UtcDep">
                                    <HeaderStyle Width="55px" />
                                </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn UniqueName="DepIcao" HeaderText="Dep ICAO" DataField="DepIcao">
                                        <HeaderStyle Width="53px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="ArrIcao" HeaderText="Arr ICAO" DataField="ArrIcao">
                                        <HeaderStyle Width="52px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="LocArr" HeaderText="Local Arr Date/Time" DataField="LocArr">
                                        <HeaderStyle Width="65px" />
                                    </telerik:GridBoundColumn>
                                    <%-- <telerik:GridBoundColumn UniqueName="UtcArr" HeaderText="UTC.Arr Dt/Tm" DataField="UtcArr">
                                    <HeaderStyle Width="60px" />
                                </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn UniqueName="FHrs" HeaderText="Flight Hrs." HeaderStyle-HorizontalAlign="Left"
                                        DataField="FHrs">
                                        <HeaderStyle Width="35px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="DHrs" HeaderText="Duty Hrs." HeaderStyle-HorizontalAlign="Left"
                                        DataField="DHrs">
                                        <HeaderStyle Width="35px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="RHrs" HeaderText="Rest Hrs." HeaderStyle-HorizontalAlign="Left"
                                        DataField="RHrs">
                                        <HeaderStyle Width="35px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="CDays" HeaderText="Calendar Days" DataField="CDays">
                                        <HeaderStyle Width="50px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="Pos" HeaderText="Pos" DataField="Pos">
                                        <HeaderStyle Width="27px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="CrwCnt" HeaderText="Crew Count" HeaderStyle-HorizontalAlign="Left"
                                        DataField="CrwCnt">
                                        <HeaderStyle Width="40px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="Rules" HeaderText="Rules" DataField="Rules">
                                        <HeaderStyle Width="35px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="Intl" HeaderText="Intl." HeaderStyle-HorizontalAlign="Left"
                                        DataField="Intl">
                                        <HeaderStyle Width="25px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="FAR" HeaderText="FAR" HeaderStyle-HorizontalAlign="Left"
                                        DataField="FAR">
                                        <HeaderStyle Width="27px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="TypeCode" HeaderText="Type Code" DataField="TypeCode">
                                        <HeaderStyle Width="40px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="TSNumber" HeaderText="Trip No." DataField="TSNumber">
                                        <HeaderStyle Width="50px" />
                                    </telerik:GridBoundColumn>
                                    <%--   <telerik:GridBoundColumn UniqueName="LogNumber" HeaderText="Log No." DataField="LogNumber">
                                    <HeaderStyle Width="50px" />
                                </telerik:GridBoundColumn>--%>
                                </Columns>
                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                            </MasterTableView>
                            <ClientSettings EnablePostBackOnRowClick="true">
                                <Scrolling AllowScroll="true" UseStaticHeaders="false" />
                                <Selecting AllowRowSelect="false" />
                            </ClientSettings>
                            <GroupingSettings CaseSensitive="false" />
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td>
                                    <div class="tblspace_10">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" valign="top">
                        <table width="60%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>History:
                                </td>
                                <td>
                                    <asp:TextBox ID="tbHistory" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                        onclick="showPopup(this, event);" onblur="parseDate(this, event);"
                                        onkeydown="return tbDate_OnKeyDown(this, event);" CssClass="text70" runat="server"></asp:TextBox>
                                    <asp:Button ID="btnSearchHistory" CssClass="button" runat="server" Text="Search"
                                        OnClick="btnSearchHistory_OnClick" />
                                </td>
                                <%--<td>
                                <telerik:RadDatePicker ID="RadDatePicker1" runat="server" ShowPopupOnFocus="True"
                                    Skin="Vista" EnableTyping="False" OnSelectedDateChanged="RadDatePicker1_SelectedDateChanged">
                                    <DateInput ID="DateInput1" runat="server" ReadOnly="True">
                                    </DateInput>
                                    <Calendar ID="Calendar1" runat="server" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                        ViewSelectorText="x" Skin="Vista" OnDayRender="CustomizeDay">
                                        <SpecialDays>
                                            <telerik:RadCalendarDay Date="2009-06-24" IsDisabled="True" IsSelectable="False">
                                            </telerik:RadCalendarDay>
                                        </SpecialDays>
                                        <DisabledDayStyle Font-Bold="True" Font-Strikeout="True" />
                                    </Calendar>
                                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                </telerik:RadDatePicker>
                            </td>--%>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td>
                                    <div class="tblspace_10">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <telerik:RadGrid ID="dgCrewHistoryActivity" runat="server" OnNeedDataSource="CrewHistoryActivity_BindData"
                            OnItemDataBound="CrewHistoryActivity_ItemDataBound" EnableAJAX="True" AutoGenerateColumns="false"
                            PageSize="5" Height="200px" Width="930px" AllowPaging="False" PagerStyle-AlwaysVisible="false"
                            AllowScroll="true">
                            <MasterTableView DataKeyNames="" CommandItemDisplay="None" AllowFilteringByColumn="false"
                                TableLayout="Fixed" ItemStyle-HorizontalAlign="Left" AllowSorting="false" AllowPaging="false"
                                Font-Size="8" HeaderStyle-HorizontalAlign="Left">
                                <Columns>
                                    <telerik:GridBoundColumn UniqueName="DtyPeriod" HeaderText="Duty Period" HeaderStyle-HorizontalAlign="Left"
                                        DataField="DtyPeriod">
                                        <HeaderStyle Width="75px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="TailNo" HeaderText="Tail No." DataField="TailNo">
                                        <HeaderStyle Width="85px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="Duty" HeaderText="Duty" DataField="Duty">
                                        <HeaderStyle Width="30px" />
                                    </telerik:GridBoundColumn>
                                    <%-- <telerik:GridBoundColumn UniqueName="LocDep" HeaderText="Loc.Dep Dt/Tm" DataField="LocDep">
                                    <HeaderStyle Width="54px" />
                                </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn UniqueName="UtcDep" HeaderText="UTC Dep Date/Time" DataField="UtcDep">
                                        <HeaderStyle Width="60px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="DepIcao" HeaderText="Dep ICAO" DataField="DepIcao">
                                        <HeaderStyle Width="53px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="ArrIcao" HeaderText="Arr ICAO" DataField="ArrIcao">
                                        <HeaderStyle Width="52px" />
                                    </telerik:GridBoundColumn>
                                    <%-- <telerik:GridBoundColumn UniqueName="LocArr" HeaderText="Loc.Arr Dt/Tm" DataField="LocArr">
                                    <HeaderStyle Width="60px" />
                                </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn UniqueName="UtcArr" HeaderText="UTC Arr Date/Time" DataField="UtcArr">
                                        <HeaderStyle Width="60px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="FHrs" HeaderText="Flight Hrs." HeaderStyle-HorizontalAlign="Left"
                                        DataField="FHrs">
                                        <HeaderStyle Width="35px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="DHrs" HeaderText="Duty Hrs." HeaderStyle-HorizontalAlign="Left"
                                        DataField="DHrs">
                                        <HeaderStyle Width="35px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="RHrs" HeaderText="Rest Hrs." HeaderStyle-HorizontalAlign="Left"
                                        DataField="RHrs">
                                        <HeaderStyle Width="35px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="CDays" HeaderText="Calendar Days" DataField="CDays">
                                        <HeaderStyle Width="50px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="Pos" HeaderText="Pos" DataField="Pos">
                                        <HeaderStyle Width="27px" />
                                    </telerik:GridBoundColumn>
                                    <%--  <telerik:GridBoundColumn UniqueName="CrwCnt" HeaderText="CrwCnt" HeaderStyle-HorizontalAlign="Left"
                                    DataField="CrwCnt">
                                    <HeaderStyle Width="40px" />
                                </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn UniqueName="Rules" HeaderText="Rules" DataField="Rules">
                                        <HeaderStyle Width="35px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="Intl" HeaderText="Intl." HeaderStyle-HorizontalAlign="Left"
                                        DataField="Intl">
                                        <HeaderStyle Width="25px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="FAR" HeaderText="FAR" HeaderStyle-HorizontalAlign="Left"
                                        DataField="FAR">
                                        <HeaderStyle Width="27px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn UniqueName="TypeCode" HeaderText="Type Code" DataField="TypeCode">
                                        <HeaderStyle Width="40px" />
                                    </telerik:GridBoundColumn>
                                    <%-- <telerik:GridBoundColumn UniqueName="TSNumber" HeaderText="TS No." DataField="TSNumber">
                                    <HeaderStyle Width="50px" />
                                </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn UniqueName="LogNumber" HeaderText="Log No." DataField="LogNumber">
                                        <HeaderStyle Width="50px" />
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                            </MasterTableView>
                            <ClientSettings EnablePostBackOnRowClick="false">
                                <Scrolling AllowScroll="true" UseStaticHeaders="false" />
                                <Selecting AllowRowSelect="false" />
                            </ClientSettings>
                            <GroupingSettings CaseSensitive="false" />
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
