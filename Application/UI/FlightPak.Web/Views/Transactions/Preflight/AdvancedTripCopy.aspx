﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdvancedTripCopy.aspx.cs"
    Inherits="FlightPak.Web.Views.Settings.PreFlight.AdvancedTripCopy" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Advanced Trip Copy</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="radScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" DateFormat="MM/dd/yyyy" runat="server">
        </DateInput>
        <Calendar ID="Calendar1" runat="server">
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                    ItemStyle-BorderStyle="Solid">
                </telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="../../../Scripts/Common.js"></script>
        <script type="text/javascript">
            var currentTextBox = null;
            var currentDatePicker = null;

            //This method is called to handle the onclick and onfocus client side events for the texbox
            function showPopup(sender, e) {
                //this is a reference to the texbox which raised the event
                //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                //this gets a reference to the datepicker, which will be shown, to facilitate
                //the selection of a date
                var datePicker = $find("<%= RadDatePicker1.ClientID %>");

                //this variable is used to store a reference to the date picker, which is currently
                //active
                currentDatePicker = datePicker;

                //this method first parses the date, that the user entered or selected, and then
                //sets it as a selected date to the picker
                datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));

                //the code lines below show the calendar, which is used to select a date. The showPopup
                //function takes three arguments - the x and y coordinates where to show the calendar, as
                //well as its height, derived from the offsetHeight property of the textbox
                var position = datePicker.getElementPosition(currentTextBox);
                datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
            }

            //this handler is used to set the text of the TextBox to the value of selected from the popup
            function dateSelected(sender, args) {
                if (currentTextBox != null) {
                    //currentTextBox is the currently selected TextBox. Its value is set to the newly selected
                    //value of the picker

                    if (currentTextBox.value != args.get_newValue()) {
                        var step = "date_changed";
                        currentTextBox.value = args.get_newValue();
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }


                }
            }

            //this function is used to parse the date entered or selected by the user
            function parseDate(sender, e) {
                if (currentDatePicker != null) {
                    var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                    var dateInput = currentDatePicker.get_dateInput();
                    var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                    sender.value = formattedDate;
                }
            }
            function tbDate_OnKeyDown(sender, event) {
                if (event.keyCode == 9) {
                    var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                    datePicker.hidePopup();
                    return true;
                }
            }

            function OneCheckBox(oBox) {

                var el, i = 0, f = oBox.form;
                while (el = f[i++]) if (el.type == 'checkbox') el.checked = (el == oBox && el.checked);

            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function CloseWindow() {
                GetRadWindow().Close();
            }

            function alertCallBackFn(arg) {
                document.getElementById("<%=tbEndAfter.ClientID%>").focus();
            }

            function alertStartGreatFn(arg) {
                document.getElementById("<%=tbEndDate.ClientID%>").focus();
            }

            function alertStartDateFn(arg) {
                document.getElementById("<%=tbStartDate.ClientID%>").focus();
            }

            function alertAddDatesFn(arg) {
                document.getElementById("<%=bntAddDateGrid.ClientID%>").focus();
            }

        </script>
        <script type="text/javascript">

            function confirmCopyCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnCopyYes.ClientID%>').click();
                }                
            }

            function alertEnddateCallFn(arg) {
                document.getElementById("<%=tbEndDate.ClientID%>").focus();
            }           
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    </telerik:RadWindowManager>
    <div id="DivCopyForm" runat="server">
        <table cellpadding="0" cellspacing="0" width="100%" class="box1">
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="65%">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <div id="copy" runat="server">
                                                <fieldset>
                                                    <legend>Include In Copy</legend>
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="tdLabel100">
                                                                <asp:CheckBox ID="chkLogistics" runat="server" Text="Logistics" />
                                                            </td>
                                                            <td class="tdLabel150">
                                                                <asp:CheckBox ID="chkPassManifest" runat="server" Text="PAX Manifest" />
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="chkCrewManifest" runat="server" Text="Crew Manifest" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="nav-6">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <fieldset>
                                                <legend>Recurrence Pattern</legend>
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0" style="border-right: 1px solid #ccc;">
                                                                <tr>
                                                                    <td class="tdLabel100">
                                                                        <asp:RadioButton ID="radDaily" OnCheckedChanged="radOption_Changed" AutoPostBack="true"
                                                                            runat="server" Text="Daily" GroupName="OccurFreq" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RadioButton ID="radWeekly" OnCheckedChanged="radOption_Changed" AutoPostBack="true"
                                                                            runat="server" Text="Weekly" GroupName="OccurFreq" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RadioButton ID="radBiWeekly" runat="server" Text="Bi-Weekly" GroupName="OccurFreq"
                                                                            OnCheckedChanged="radOption_Changed" AutoPostBack="true" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RadioButton ID="radMonthly" runat="server" Text="Monthly" GroupName="OccurFreq"
                                                                            OnCheckedChanged="radOption_Changed" AutoPostBack="true" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td>
                                                                        Recur Every
                                                                    </td>
                                                                    <td class="tdLabel87">
                                                                        <asp:TextBox ID="tbRecurEvery" Enabled="false" runat="server" CssClass="tdtext50"
                                                                            onKeyPress="return fnAllowNumeric(this, event)" MaxLength="3"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        Week(s) on:
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkSunday" runat="server" Text="Sunday" GroupName="WeekDays" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="nav-3">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td class="tdLabel83">
                                                                                    <asp:CheckBox ID="chkMonday" runat="server" Text="Monday" GroupName="WeekDays" />
                                                                                </td>
                                                                                <td class="tdLabel83">
                                                                                    <asp:CheckBox ID="chkTuesday" runat="server" Text="Tuesday" GroupName="WeekDays" />
                                                                                </td>
                                                                                <td colspan="2">
                                                                                    <asp:CheckBox ID="chkWednesday" runat="server" Text="Wednesday" GroupName="WeekDays" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="nav-3">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkThursday" runat="server" Text="Thursday" GroupName="WeekDays" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkFriday" runat="server" Text="Friday" GroupName="WeekDays" />
                                                                                </td>
                                                                                <td colspan="2">
                                                                                    <asp:CheckBox ID="chkSaturday" runat="server" Text="Saturday" GroupName="WeekDays" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="nav-6">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <fieldset>
                                                <legend>Range of Recurrence</legend>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            Start:
                                                        </td>
                                                        <td class="tdLabel136">
                                                            <asp:TextBox ID="tbStartDate" AutoPostBack="true" OnTextChanged="tbStartDate_TextChanged"
                                                                onKeyPress="return fnAllowNumericAndChar(this, event,'/')" onclick="showPopup(this, event);"
                                                                onkeydown="return tbDate_OnKeyDown(this, event);" onBlur="parseDate(this, event);"
                                                                CssClass="text70" runat="server"></asp:TextBox>
                                                           
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0" class="border-box">
                                                                <tr>
                                                                    <td class="tdLabel80">
                                                                        <asp:RadioButton ID="radEndAfter" runat="server" Text="End After" GroupName="ChkAfterBy" />
                                                                    </td>
                                                                    <td class="tdLabel50">
                                                                        <asp:TextBox ID="tbEndAfter" runat="server" CssClass="tdtext35" MaxLength="3" onKeyPress="return fnAllowNumeric(this, event)"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        Occurrences
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="nav-3">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RadioButton ID="radEndBy" runat="server" Text="End By" GroupName="ChkAfterBy" />
                                                                    </td>
                                                                    <td colspan="2">
                                                                        <asp:TextBox ID="tbEndDate" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                                            onclick="showPopup(this, event);" AutoPostBack="true" onBlur="parseDate(this, event);"
                                                                            onkeydown="return tbDate_OnKeyDown(this, event);" CssClass="text70" runat="server"
                                                                            OnTextChanged="tbEndDate_OnTextChanged"></asp:TextBox>
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" width="35%">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="tdLabel10">
                                        </td>
                                        <td>
                                            Dates To Create
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdLabel10">
                                        </td>
                                        <td class="preflight-custom-grid-fuel">
                                            <telerik:RadGrid ID="dgDateCreate" runat="server" Skin="Office2007" AutoGenerateColumns="false"
                                                EnableAJAX="True" AllowPaging="true" Width="270px" Height="230px">
                                                <MasterTableView ClientDataKeyNames="AdvCopyDay,DepartDate" TableLayout="Auto" CommandItemDisplay="None"
                                                    AllowPaging="false" AllowFilteringByColumn="false" AllowSorting="false" ItemStyle-HorizontalAlign="Left">
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="AdvCopyDay" HeaderText="Day" HeaderStyle-Width="75px" />
                                                        <telerik:GridBoundColumn DataField="DepartDate" HeaderText="Depart Date" HeaderStyle-Width="140px" />
                                                    </Columns>
                                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                </MasterTableView>
                                                <ClientSettings>
                                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True">
                                                    </Scrolling>
                                                    <Selecting AllowRowSelect="true" EnableDragToSelectRows="false" />
                                                </ClientSettings>
                                                <GroupingSettings CaseSensitive="false" />
                                            </telerik:RadGrid>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <div class="tblspace_5">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="left">
                                <asp:Button ID="bntAddDateGrid" Text="Add To Date Grid" runat="server" CssClass="button"
                                    OnClick="bntAddDateGrid_Click" />
                            </td>
                            <td align="center">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnCopy" Text="Copy" runat="server" CssClass="button" OnClick="bntCopyDateGrid_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCancel" Text="Cancel" OnClientClick="javascript:CloseWindow();return false;"
                                                runat="server" CssClass="button" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="right">
                                <asp:Button ID="btnResetDateGrid" Text="Reset Date Grid" runat="server" CssClass="button"
                                    OnClick="bntResetDateGrid_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <div class="tblspace_5">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <telerik:RadGrid ID="dgCopyGrid" runat="server" Skin="Office2007" AutoGenerateColumns="false"
                                    EnableAJAX="True" AllowPaging="true" Width="766px" Height="150px">
                                    <MasterTableView ClientDataKeyNames="TripID" TableLayout="Auto" CommandItemDisplay="None"
                                        AllowSorting="false" AllowPaging="false" AllowFilteringByColumn="false" ItemStyle-HorizontalAlign="Left">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="TripNUM" HeaderText="Trip No." AutoPostBackOnFilter="true"
                                                ShowFilterIcon="false" CurrentFilterFunction="Contains" AllowFiltering="false"
                                                HeaderStyle-Width="50px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EstDepartureDT" HeaderText="Departure Date" DataFormatString="{0:dd/MM/yyyy}"
                                                AllowFiltering="false" AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                                                HeaderStyle-Width="90px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TripDescription" HeaderText="Trip Description"
                                                ShowFilterIcon="false" FilterDelay="4000" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                                AllowFiltering="false" HeaderStyle-Width="150px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TripStatus" HeaderText="Status" AutoPostBackOnFilter="true"
                                                ShowFilterIcon="false" FilterDelay="4000" CurrentFilterFunction="Contains" AllowFiltering="false"
                                                HeaderStyle-Width="60px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="RequestDT" HeaderText="Request Date" AutoPostBackOnFilter="true"
                                                AllowFiltering="false" DataFormatString="{0:dd/MM/yyyy}" ShowFilterIcon="false"
                                                FilterDelay="4000" CurrentFilterFunction="Contains" HeaderStyle-Width="80px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Except" HeaderText="Exception" AutoPostBackOnFilter="true"
                                                AllowFiltering="false" ShowFilterIcon="false" ItemStyle-ForeColor="Red" ItemStyle-HorizontalAlign="Center"
                                                FilterDelay="4000" CurrentFilterFunction="Contains" HeaderStyle-Width="80px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TripID" HeaderText="TripID" Display="false" AutoPostBackOnFilter="false"
                                                ShowFilterIcon="false" CurrentFilterFunction="Contains" AllowFiltering="false"
                                                HeaderStyle-Width="100px">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                    </MasterTableView>
                                    <ClientSettings>
                                        <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True">
                                        </Scrolling>
                                        <Selecting AllowRowSelect="true" EnableDragToSelectRows="false" />
                                    </ClientSettings>
                                    <GroupingSettings CaseSensitive="false" />
                                </telerik:RadGrid>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadGrid ID="dgCopyCalender" runat="server" Skin="Office2007" AutoGenerateColumns="false"
                                    EnableAJAX="True" AllowPaging="true" Width="766px" Height="150px" Visible="false">
                                    <MasterTableView ClientDataKeyNames="TripID" TableLayout="Auto" CommandItemDisplay="None"
                                        AllowSorting="false" AllowPaging="false" AllowFilteringByColumn="false" ItemStyle-HorizontalAlign="Left">
                                        <Columns>
                                            
                                            <telerik:GridBoundColumn DataField="EstDepartureDT" HeaderText="Start Date/Time"
                                                DataFormatString="{0:dd/MM/yyyy}" AllowFiltering="false" AutoPostBackOnFilter="true"
                                                ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="90px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EstArrivalDT" HeaderText="End Date/Time" DataFormatString="{0:dd/MM/yyyy}"
                                                AllowFiltering="false" AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                                                HeaderStyle-Width="90px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DepICAO" HeaderText="Dep.Icao" AutoPostBackOnFilter="true"
                                                ShowFilterIcon="false" CurrentFilterFunction="Contains" AllowFiltering="false"
                                                HeaderStyle-Width="50px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ArrICAO" HeaderText="Arr.Icao" AutoPostBackOnFilter="true"
                                                ShowFilterIcon="false" CurrentFilterFunction="Contains" AllowFiltering="false"
                                                HeaderStyle-Width="50px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Crew" HeaderText="Crew" AutoPostBackOnFilter="true"
                                                ShowFilterIcon="false" CurrentFilterFunction="Contains" AllowFiltering="false"
                                                HeaderStyle-Width="50px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Duty" HeaderText="Duty" AutoPostBackOnFilter="true"
                                                ShowFilterIcon="false" CurrentFilterFunction="Contains" AllowFiltering="false"
                                                HeaderStyle-Width="50px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TripID" HeaderText="TripID" Display="false" AutoPostBackOnFilter="false"
                                                ShowFilterIcon="false" CurrentFilterFunction="Contains" AllowFiltering="false"
                                                HeaderStyle-Width="100px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TripNUM" HeaderText="Trip No." AutoPostBackOnFilter="true"
                                                ShowFilterIcon="false" CurrentFilterFunction="Contains" AllowFiltering="false"
                                                HeaderStyle-Width="50px" Display="false">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                    </MasterTableView>
                                    <ClientSettings>
                                        <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True">
                                        </Scrolling>
                                        <Selecting AllowRowSelect="true" EnableDragToSelectRows="false" />
                                    </ClientSettings>
                                    <GroupingSettings CaseSensitive="false" />
                                </telerik:RadGrid>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadGrid ID="dgCopyFleetCalender" runat="server" Skin="Office2007" AutoGenerateColumns="false"
                                    EnableAJAX="True" AllowPaging="true" Width="766px" Height="150px" Visible="false">
                                    <MasterTableView ClientDataKeyNames="TripID" TableLayout="Auto" CommandItemDisplay="None"
                                        AllowSorting="false" AllowPaging="false" AllowFilteringByColumn="false" ItemStyle-HorizontalAlign="Left">
                                        <Columns>
                                            
                                            <telerik:GridBoundColumn DataField="EstDepartureDT" HeaderText="Start Date/Time"
                                                DataFormatString="{0:dd/MM/yyyy}" AllowFiltering="false" AutoPostBackOnFilter="true"
                                                ShowFilterIcon="false" CurrentFilterFunction="Contains" HeaderStyle-Width="90px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EstArrivalDT" HeaderText="End Date/Time" DataFormatString="{0:dd/MM/yyyy}"
                                                AllowFiltering="false" AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"
                                                HeaderStyle-Width="90px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DepICAO" HeaderText="Dep.Icao" AutoPostBackOnFilter="true"
                                                ShowFilterIcon="false" CurrentFilterFunction="Contains" AllowFiltering="false"
                                                HeaderStyle-Width="50px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ArrICAO" HeaderText="Arr.Icao" AutoPostBackOnFilter="true"
                                                ShowFilterIcon="false" CurrentFilterFunction="Contains" AllowFiltering="false"
                                                HeaderStyle-Width="50px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail No." AutoPostBackOnFilter="true"
                                                ShowFilterIcon="false" CurrentFilterFunction="Contains" AllowFiltering="false"
                                                HeaderStyle-Width="50px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Duty" HeaderText="Duty" AutoPostBackOnFilter="true"
                                                ShowFilterIcon="false" CurrentFilterFunction="Contains" AllowFiltering="false"
                                                HeaderStyle-Width="50px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TripID" HeaderText="TripID" Display="false" AutoPostBackOnFilter="false"
                                                ShowFilterIcon="false" CurrentFilterFunction="Contains" AllowFiltering="false"
                                                HeaderStyle-Width="100px">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TripNUM" HeaderText="Trip No." AutoPostBackOnFilter="true"
                                                ShowFilterIcon="false" CurrentFilterFunction="Contains" AllowFiltering="false"
                                                HeaderStyle-Width="50px" Display="false">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                    </MasterTableView>
                                    <ClientSettings>
                                        <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True">
                                        </Scrolling>
                                        <Selecting AllowRowSelect="true" EnableDragToSelectRows="false" />
                                    </ClientSettings>
                                    <GroupingSettings CaseSensitive="false" />
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
          
        </table>
        <table id="tblHidden" style="display: none;">
            <tr>
                <td>
                    <asp:Button ID="btnCopyYes" runat="server" Text="Button" OnClick="btnCopyYes_Click" />
                    <asp:Button ID="btnCopyNo" runat="server" Text="Button" OnClick="btnCopyNo_Click" />
                </td>
            </tr>
        </table>
    </div>
    <telerik:RadAjaxManager ID="RadAjaxManager1" OnAjaxRequest="RadAjaxManager1_AjaxRequest"
        runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivCopyForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="DivCopyForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivCopyForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
           
        </AjaxSettings>
    </telerik:RadAjaxManager>
    </form>
</body>
</html>
