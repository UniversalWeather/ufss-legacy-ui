﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.PreflightService;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Transactions.Preflight
{
    public partial class PreflightSearchRPTPopup : BaseSecuredPage
    {

        // private string TripID;
        public PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
        public Int64 TripID = 0;
        public string Param = string.Empty;
        string DateFormat = string.Empty;
        private ExceptionManager exManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Request.QueryString["FromPage"] != null)
            //{
            //    if (Request.QueryString["FromPage"].ToString().Contains("Report|"))
            //    {
            //        dgPreflightRetrieve.AllowMultiRowSelection = true;
            //    }
            //    else
            //    {
            //        dgPreflightRetrieve.AllowMultiRowSelection = false;
            //    }
            //}

            //else
            //{
            //    dgPreflightRetrieve.AllowMultiRowSelection = false;
            //}

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Request.QueryString["ControlName"] != null)
                        {
                            Param = Request.QueryString["ControlName"];
                        }
                        if (!IsPostBack)
                        {
                            if (UserPrincipal.Identity._clientId == null)
                                btnClientCode.Enabled = true;
                            else
                                btnClientCode.Enabled = false;
                            if (UserPrincipal.Identity._clientId != null)
                            {
                                tbSearchClientCode.Text = UserPrincipal.Identity._clientCd;
                                hdClientCodeID.Value = UserPrincipal.Identity._clientId.ToString();
                                FlightPakMasterService.Client client = new FlightPakMasterService.Client();
                                client = GetClient((long)UserPrincipal.Identity._clientId);
                                if (client != null)
                                {
                                    lbClientCode.Text = System.Web.HttpUtility.HtmlEncode(client.ClientDescription);
                                }
                                tbSearchClientCode.ReadOnly = true;
                            }
                            else
                            {
                                tbSearchClientCode.ReadOnly = false;
                            }

                            if (UserPrincipal != null && UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                            {
                                RadDatePicker1.DateInput.DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                                RadDatePicker1.DateInput.DisplayDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                                DatePicker.DateInput.DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                                DatePicker.DateInput.DisplayDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            }

                            DateTime estDepart = DateTime.Now;
                            if (UserPrincipal.Identity._fpSettings._TripsheetSearchBack != null)
                            {
                                estDepart = estDepart.AddDays(-((double)UserPrincipal.Identity._fpSettings._TripsheetSearchBack));
                            }
                            else
                            {
                                estDepart = estDepart.AddDays(-(30));
                            }


                            tbDepDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", estDepart);
                        }

                        if (Session["XMLFileName"] != null && Session["XMLFileName"].ToString() == "PRETripExceptions.xml" && Request.QueryString["multiselect"] != null && Request.QueryString["multiselect"].ToString() == "1")
                        {
                            dgPreflightRetrieve.AllowMultiRowSelection = true;
                            GridClientSelectColumn selCol = new GridClientSelectColumn();
                            selCol = (GridClientSelectColumn)dgPreflightRetrieve.MasterTableView.GetColumn("ClientSelectColumn");
                            selCol.Display = true;
                            selCol.HeaderText = "Select All";
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }



        protected void dgPreflightRetrieve_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgPreflightRetrieve.DataSource = DoSearchFilter();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        //protected void dgPreflightRetrieve_ItemCreated(object sender, GridItemEventArgs e)
        //{

        //    if (e.Item is GridCommandItem)
        //    {
        //        // To find the edit button in the grid
        //        Button EditButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as Button;
        //        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(EditButton, DivExternalForm, RadAjaxLoadingPanel1);

        //        // To find the insert button in the grid
        //        Button InsertButton = (e.Item as GridCommandItem).FindControl("btnOK") as Button;
        //        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(InsertButton, DivExternalForm, RadAjaxLoadingPanel1);


        //    }

        //}

        protected void dgPreflightRetrieve_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
                        {

                            if (e.Item is GridCommandItem)
                            {
                                GridCommandItem Item = (GridCommandItem)e.Item;

                                Button Insert = (Button)Item.FindControl("lbtnInitInsert");
                                Button Ok = (Button)Item.FindControl("btnOK");
                                Button Submit = (Button)Item.FindControl("btnSUB");
                                if (Insert != null && Ok != null && Submit != null)
                                {
                                    if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"] == "Move")
                                    {
                                        Insert.Visible = false;
                                        Ok.Visible = true;
                                        Submit.Visible = false;
                                    }
                                    else if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"].Contains("Report|"))
                                    {
                                        Insert.Visible = false;
                                        Ok.Visible = false;
                                        Submit.Visible = true;
                                    }
                                    else if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"] == "Report")
                                    {
                                        Insert.Visible = false;
                                        Ok.Visible = false;
                                        Submit.Visible = true;
                                    }
                                    else
                                    {
                                        Insert.Visible = true;
                                        Ok.Visible = false;
                                        Submit.Visible = false;
                                    }
                                }

                            }

                            if (e.Item is GridDataItem)
                            {
                                GridColumn Column = dgPreflightRetrieve.MasterTableView.GetColumn("EstDepartureDT");
                                GridDataItem DataItem = e.Item as GridDataItem;
                                string UwaValue = DataItem["EstDepartureDT"].Text.Trim();
                                GridDataItem Item1 = (GridDataItem)e.Item;
                                TableCell cell = (TableCell)Item1["EstDepartureDT"];
                                TableCell cell1 = (TableCell)Item1["RequestDT"];
                                string encodedDate = "";
                                if ((!string.IsNullOrEmpty(cell.Text)) && (cell.Text != "&nbsp;"))
                                {
                                    encodedDate = Microsoft.Security.Application.Encoder.HtmlEncode(String.Format("{0: " + RadDatePicker1.DateInput.DateFormat + "}", Convert.ToDateTime(cell.Text)));
                                    cell.Text = encodedDate;
                                }
                                if ((!string.IsNullOrEmpty(cell1.Text)) && (cell1.Text != "&nbsp;"))
                                {
                                    encodedDate =   Microsoft.Security.Application.Encoder.HtmlEncode(String.Format("{0: " + RadDatePicker1.DateInput.DateFormat + "}", Convert.ToDateTime(cell1.Text)));
                                    cell1.Text = encodedDate;
                                }
                            }
                            if (e.Item is GridHeaderItem)
                            {
                                GridHeaderItem item = (GridHeaderItem)e.Item;
                                Label lbl = new Label();
                                lbl.ID = "Label1";
                                lbl.Text = "Select All";
                                item["ClientSelectColumn"].Controls.Add(lbl);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }


        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Updated = dgPreflightRetrieve;
                        // SelectItem();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }
        protected void dgPreflightRetrieve_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                //InsertSelectedRow();
                                _LoadTriptoSession();
                                break;
                            case RadGrid.FilterCommandName:
                                Pair filterPair = (Pair)e.CommandArgument;
                                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }


        protected void _LoadTriptoSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                if (dgPreflightRetrieve.SelectedItems.Count > 0)
                {
                    GridDataItem item = (GridDataItem)dgPreflightRetrieve.SelectedItems[0];
                    TripID = Convert.ToInt64(item["TripID"].Text);
                    if (Request.QueryString["FromPage"] == "Move")
                    {
                        using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                        {
                            var objRetVal = PrefSvc.GetTrip(TripID);
                            if (objRetVal.ReturnFlag)
                            {
                                Trip = objRetVal.EntityList[0];

                                //SetTripModeToNoChange(ref Trip);
                                Trip.Mode = TripActionMode.NoChange;
                                Trip.State = TripEntityState.NoChange;
                                Session["MovePreFlightTrip"] = Trip;
                                Trip.AllowTripToNavigate = true;
                                Trip.AllowTripToSave = true;
                                // InjectScript.Text = "<script type='text/javascript'>returnToParent(); return false;</script>";
                                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "returnToParent()", true);
                            }
                        }
                    }
                    else
                    {
                        if (dgPreflightRetrieve.SelectedItems.Count > 0)
                        {
                            Int64 convTripNum = 0;
                            Int64 Tripnum = 0;
                            if (Int64.TryParse(item["TripNUM"].Text, out convTripNum))
                                Tripnum = convTripNum;

                            if (Session["CurrentPreFlightTrip"] != null)
                            {
                                PreflightMain CurrTrip = (PreflightMain)Session["CurrentPreFlightTrip"];
                                if (CurrTrip.Mode == TripActionMode.Edit && CurrTrip.TripID != TripID)
                                {
                                    //this.PreflightHeader.HeaderMessage.ForeColor = System.Drawing.Color.Red;
                                    //this.PreflightHeader.HeaderMessage.Text = "Trip Unsaved, please Save or Cancel Trip";
                                    //RadWindowManager1.RadAlert("Trip Unsaved, please Save or Cancel Trip", 330, 100, "Trip Alert", null);

                                    RadWindowManager1.RadConfirm("Trip: " + CurrTrip.TripNUM.ToString() + "Unsaved, ignore changes and load the selected Trip: " + Tripnum + " ?", "confirmCallBackFn", 330, 100, null, "Confirmation!");
                                }
                                else
                                {
                                    using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                                    {
                                        var objRetVal = PrefSvc.GetTrip(TripID);
                                        if (objRetVal.ReturnFlag)
                                        {
                                            Trip = objRetVal.EntityList[0];

                                            //SetTripModeToNoChange(ref Trip);
                                            Trip.Mode = TripActionMode.NoChange;
                                            Trip.State = TripEntityState.NoChange;
                                            Session["CurrentPreFlightTrip"] = Trip;
                                            Trip.AllowTripToNavigate = true;
                                            Trip.AllowTripToSave = true;
                                            RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                                            //InjectScript.Text = "<script type='text/javascript'>CloseAndRebind('navigateToInserted')</script>";
                                        }
                                    }
                                }
                            }
                            else
                            {
                                using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                                {
                                    var objRetVal = PrefSvc.GetTrip(TripID);
                                    if (objRetVal.ReturnFlag)
                                    {
                                        Trip = objRetVal.EntityList[0];
                                        Trip.Mode = TripActionMode.NoChange;
                                        Trip.State = TripEntityState.NoChange;
                                        Trip.AllowTripToNavigate = true;
                                        Trip.AllowTripToSave = true;
                                        Session["CurrentPreFlightTrip"] = Trip;
                                        RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                                        //InjectScript.Text = "<script type='text/javascript'>CloseAndRebind('navigateToInserted')</script>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
        }

        private FlightPak.Web.FlightPakMasterService.Client GetClient(Int64 ClientID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientID))
            {
                FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient();
                FlightPak.Web.FlightPakMasterService.Client client = new FlightPakMasterService.Client();
                var objRetVal = objDstsvc.GetClientWithFilters (ClientID,string.Empty,false);
                List<FlightPakMasterService.Client> ClientList = new List<FlightPakMasterService.Client>();

                if (objRetVal.ReturnFlag)
                {

                    ClientList = objRetVal.EntityList;
                    if (ClientList != null && ClientList.Count > 0)
                    {
                        client = ClientList[0];

                    }
                    else
                        client = null;

                }
                return client;
            }
        }

        protected void tbSearchClientCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbClientCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbcvClientCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        if (!string.IsNullOrEmpty(tbSearchClientCode.Text))
                        {

                            List<FlightPak.Web.FlightPakMasterService.Client> ClientList = new List<FlightPakMasterService.Client>();
                            FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient();
                            var objRetVal = objDstsvc.GetClientWithFilters(0,tbSearchClientCode.Text.Trim(),false);

                            if (objRetVal.ReturnFlag)
                            {

                                ClientList = objRetVal.EntityList;


                                if (ClientList != null && ClientList.Count > 0)
                                {
                                    tbSearchClientCode.Text = ClientList[0].ClientCD;
                                    lbClientCode.Text = System.Web.HttpUtility.HtmlEncode(ClientList[0].ClientDescription);
                                    hdClientCodeID.Value = ClientList[0].ClientID.ToString();
                                    lbcvClientCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                }
                                else
                                {
                                    lbcvClientCode.Text = System.Web.HttpUtility.HtmlEncode("Client Code Does Not Exist");
                                    hdClientCodeID.Value = System.Web.HttpUtility.HtmlEncode("");
                                    tbSearchClientCode.Focus();
                                }
                            }
                            else
                            {
                                lbcvClientCode.Text = System.Web.HttpUtility.HtmlEncode("Client Code Does Not Exist");
                                hdClientCodeID.Value = System.Web.HttpUtility.HtmlEncode("");
                                tbSearchClientCode.Focus();
                            }


                        }
                        else
                        {
                            lbClientCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            hdClientCodeID.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void RetrieveSearch_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgPreflightRetrieve.DataSource = DoSearchFilter();
                        dgPreflightRetrieve.DataBind();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        public RadDatePicker DatePicker { get { return this.RadDatePicker1; } }

        public List<View_PreflightMainList> DoSearchFilter()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Int64 ClientID = 0;
                Int64 homeBaseID = 0;
                if (UserPrincipal != null)
                {

                    homeBaseID = (long)UserPrincipal.Identity._homeBaseId;


                }
                using (PreflightService.PreflightServiceClient objService = new PreflightService.PreflightServiceClient())
                {

                    DateTime? estDepart;
                    if (!string.IsNullOrEmpty(tbDepDate.Text))
                        estDepart = DateTime.ParseExact(tbDepDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                    else
                        //    estDepart = DateTime.Now.AddDays(-1000);
                        estDepart = DateTime.MinValue;


                    List<View_PreflightMainList> preFlightLists = new List<View_PreflightMainList>();

                    if (!string.IsNullOrEmpty(hdClientCodeID.Value))
                    {
                        ClientID = Convert.ToInt64(hdClientCodeID.Value);
                    }
                    else
                        ClientID = 0;

                    var ObjPrefmainlist = objService.GetAllPreflightMainList(chkHomebase.Checked ? homeBaseID : 0,
                        ClientID,
                    ChkTripsheet.Checked ? "T" : null,
                    ChkWorksheet.Checked ? "W" : null,
                    ChkHold.Checked ? "H" : null,
                    ChkCancelled.Checked ? "C" : null,
                    ChkSchedServ.Checked ? "S" : null,
                    ChUnFillFilled.Checked ? "U" : null,
                    ChkExcLog.Checked ? true : false,
                    (DateTime)estDepart, string.Empty);

                    //var ObjRetval = objService.GetAllPreflightList();

                    if (ObjPrefmainlist.ReturnFlag)
                    {
                        preFlightLists = ObjPrefmainlist.EntityList;
                    }
                    return preFlightLists;
                }
            }
        }


        protected void Yes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridDataItem item = (GridDataItem)dgPreflightRetrieve.SelectedItems[0];
                        TripID = Convert.ToInt64(item["TripID"].Text);

                        Int64 convTripNum = 0;
                        Int64 Tripnum = 0;
                        if (Int64.TryParse(item["TripNUM"].Text, out convTripNum))
                            Tripnum = convTripNum;


                        using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                        {
                            var objRetVal = PrefSvc.GetTrip(TripID);
                            if (objRetVal.ReturnFlag)
                            {
                                Trip = objRetVal.EntityList[0];

                                //SetTripModeToNoChange(ref Trip);
                                Trip.Mode = TripActionMode.NoChange;
                                Trip.State = TripEntityState.NoChange;
                                Session["CurrentPreFlightTrip"] = Trip;
                                Trip.AllowTripToNavigate = true;
                                Trip.AllowTripToSave = true;
                                RadAjaxManager1.ResponseScripts.Add(@"CloseAndRebind('navigateToInserted');");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void No_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager1.ResponseScripts.Add(@"CloseRadWindow('CloseWindow');");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void dgPreflightRetrieve_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgPreflightRetrieve, Page.Session);

        }
    }
}