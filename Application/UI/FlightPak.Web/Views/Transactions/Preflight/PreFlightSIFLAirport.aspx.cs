﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using FlightPak.Web.PreflightService;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.Views.Transactions.PreFlight
{
    public partial class PreFlightSIFLAirport : BaseSecuredPage
    {
        public PreflightMain Trip = new PreflightMain();
        private ExceptionManager exManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            BindData();
                            lbMessage.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        public void BindData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("LegID");
                dt.Columns.Add("ICAOID");
                dt.Columns.Add("IcaoCity");
                dt.Columns.Add("AirportID");
                dt.Columns.Add("AirportName");
                // int DepartsCount = 0;
                // int ArrivesCount = 0;

                if (Session[WebSessionKeys.UnchangedCurrentTrip] != null)
                {
                    PreflightMain Trip = (PreflightMain)Session[WebSessionKeys.UnchangedCurrentTrip];
                    if (Request.QueryString["flag"] != null && Request.QueryString["flag"] == "Depart")
                    {

                        Telerik.Web.UI.GridColumn hdr1 = dgPostFlightSIFLAirport.MasterTableView.GetColumnSafe("IcaoCity");
                        hdr1.HeaderText = "Dep ICAO - City";
                        Page.Title = "Select Departure ICAO";

                        if (Trip.PreflightLegs != null)
                        {
                            var departsCount = Trip.PreflightLegs.Select(x => x.Airport1).Count();
                            foreach (var tripLeg in Trip.PreflightLegs.OrderBy(x => x.LegNUM))
                            {
                                if (string.IsNullOrEmpty(Request.QueryString["Departs"]) || departsCount > 0)
                                {
                                    DataRow oItem = dt.NewRow();
                                    oItem[0] = tripLeg.LegNUM.ToString();
                                    oItem[1] = tripLeg.Airport1.IcaoID;
                                    if (!string.IsNullOrEmpty(tripLeg.Airport1.CityName))
                                    {
                                        oItem[2] = tripLeg.Airport1.IcaoID + " - " + tripLeg.Airport1.CityName;
                                    }
                                    else
                                    {
                                        oItem[2] = tripLeg.Airport1.IcaoID;
                                    }

                                    oItem[3] = tripLeg.Airport1.AirportID.ToString();

                                    oItem[4] = tripLeg.Airport1.AirportName;
                                    dt.Rows.Add(oItem);
                                }

                            }
                        }

                    }
                    else if (Request.QueryString["flag"] != null && Request.QueryString["flag"] == "Arrival")
                    {

                        Telerik.Web.UI.GridColumn hdr2 = dgPostFlightSIFLAirport.MasterTableView.GetColumnSafe("IcaoCity");
                        hdr2.HeaderText = "Arr ICAO - City";
                        Page.Title = "Select Arrival ICAO";
                        if (Trip.PreflightLegs != null)
                        {
                            var arrivalCount = Trip.PreflightLegs.Select(x => x.Airport).Count();
                            foreach (var tripLeg in Trip.PreflightLegs.OrderBy(x => x.LegNUM))
                            {
                                if (string.IsNullOrEmpty(Request.QueryString["Arrives"]) || arrivalCount > 0)
                                {
                                    DataRow oItem = dt.NewRow();
                                    oItem[0] = tripLeg.LegNUM.ToString();
                                    oItem[1] = tripLeg.Airport.IcaoID;
                                    if (!string.IsNullOrEmpty(tripLeg.Airport.CityName))
                                    {
                                        oItem[2] = tripLeg.Airport.IcaoID + " - " + tripLeg.Airport.CityName;
                                    }
                                    else
                                    {
                                        oItem[2] = tripLeg.Airport.IcaoID;
                                    }
                                    oItem[3] = tripLeg.Airport.AirportID.ToString();
                                    oItem[4] = tripLeg.Airport.AirportName;
                                    dt.Rows.Add(oItem);
                                }

                            }
                        }

                    }
                    dgPostFlightSIFLAirport.DataSource = dt;
                    dgPostFlightSIFLAirport.DataBind();
                }
            }
        }

        protected void PostFlightSIFLAirport_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                InsertSelectedRow();
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        protected void PostFlightSIFLAirport_InsertCommand(object source, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        InsertSelectedRow();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightSIFL);
                }
            }
        }

        private void InsertSelectedRow()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    POSIFLAirport selAirport = new POSIFLAirport();
                    GridDataItem item = (GridDataItem)dgPostFlightSIFLAirport.SelectedItems[0];
                    Session["AirportFlag"] = Request.QueryString["flag"].ToString();
                    if (item["LegID"] != null)
                        selAirport.AirportID = Convert.ToInt64(item["LegID"].Text);
                    if (item["ICAOID"] != null)
                        selAirport.ICAOID = item["ICAOID"].Text;
                    if (item["IcaoCity"] != null)
                        selAirport.IcaoCity = item["IcaoCity"].Text;
                    if (item["AirportID"] != null)
                        selAirport.AirportID = Convert.ToInt64(item["AirportID"].Text);
                    if (item["AirportName"] != null)
                        selAirport.AirportName = item["AirportName"].Text;

                    Session["SelectedAirport"] = selAirport;
                }
                catch (Exception ex)
                {
                    Session.Remove("AirportFlag");
                    Session.Remove("SelectedAirport");
                }
            }
        }
    }
}