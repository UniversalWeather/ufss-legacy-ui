﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Preflight.Master"
    AutoEventWireup="true" CodeBehind="PreflightLegs.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.PreFlight.PreflightLegs"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/UserControls/UCPreflightFooterButton.ascx" TagName="Footer" TagPrefix="UCPreflight" %>
<%@ MasterType VirtualPath="~/Framework/Masters/Preflight.Master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <%=Scripts.Render("~/bundles/preflightLeg") %>
    <script type="text/javascript">

        function checklegnotes() {
            if (self.CurrentLegData.CrewNotes() != null && self.CurrentLegData.CrewNotes().trim()!='' || self.CurrentLegData.Notes() != null && self.CurrentLegData.Notes().trim()!='') {
                $("#<%= pnlLegNotes.ClientID %> span .rpText").css('color', 'red');
            }
            else {
                $("#<%= pnlLegNotes.ClientID %> span .rpText").css('color', 'black');
            }
        }
        
        
    </script>
    
    <input type="hidden" id="hdCommonLabel"/>
    <input type="hidden" id="HomebaseAirpotObj"/>
    <input type="hidden" id="ArriAirpotObj"/>
    <input type="hidden" id="DepAirpotObj"/>
    <input type="hidden" id="AircraftObj"/>
    <input type="hidden" id="DateFormat" data-bind="value: UserPrincipal._ApplicationDateFormat"/>
    <asp:HiddenField runat="server" ID="hdnPreflightLegInitializationObject"/>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div id="DivExternalForm" class="ExternalForm">
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="rdPreflightEMAIL" runat="server" OnClientResizeEnd="GetDimensions"
                    KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    AutoSize="true" Width="780px" Height="650px" Title="E-mail Notification" NavigateUrl="~/Views/Transactions/Preflight/EmailTrip.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" OnClientClose="OnClientICAOClose1" KeepInScreenBounds="true" Modal="true" Width="450px" Height="400px"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radArrivalAirportPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" OnClientClose="OnClientICAOClose2" KeepInScreenBounds="true" Modal="true" Width="450px" Height="400px"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radAirportCRUDPopup" runat="server" Height="800px" Width="1100px"
                    ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                    VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Airports/AirportMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCodeClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx"
                    VisibleStatusbar="false">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdDeptPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientDeptClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                    Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radAccountMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientAccountClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radAccountMasterCRUDPopup" runat="server" Height="800px" Width="1100px"
                    ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                    VisibleStatusbar="false">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdAuth" runat="server" OnClientResizeEnd="GetDimensions" OnClientClose="OnClientAuthClose"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    NavigateUrl="~/Views/Settings/Company/AuthorizationPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radCrewDutyRulesPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCrewRuleClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewDutyRulesPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radCrewDutyRulesCRUDPopup" runat="server" Height="800px" Width="1100px"
                    ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                    VisibleStatusbar="false">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientRqstrClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                    ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                    VisibleStatusbar="false">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radFlightCategoryPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCategoryClose" AutoSize="true" KeepInScreenBounds="true" 
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
               </telerik:RadWindow>
                <telerik:RadWindow ID="radFlightCategoryCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCategoryClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightCategoryPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radCQCPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="radCQCPopups" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseCharterQuotCustomerPopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/CharterQuote/CharterQuoteCustomerPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdPower" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientPowerClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/AircraftPopup.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdPowerSetting" runat="server" VisibleOnPageLoad="false" Height="230px"
                    Width="500px" Modal="true" BackColor="#DADADA" Behaviors="Close" VisibleStatusbar="false"
                    Title="Aircraft Power Settings">
                    <ContentTemplate>
                        <table width="100%" class="box1">
                            <tr>
                                <td>
                                    Aircraft:
                                    <asp:Label ID="lbAircraft" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="preflight-custom-grid">
                                    <table id="dgPowerSetting"></table>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" valign="bottom">
                                    <input type="button" id="btPowerSelect" onclick="return GetSelectedPowerSetting();" class="button" value="Select" />
                                    <input type="button" id="btPowerCancel" onclick="return ClosePowerSettingPopup();" class="button" value="Cancel"/>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdTOBias" runat="server" VisibleOnPageLoad="false" Width="600px"
                    Height="180px" Behaviors="Close" Modal="true" VisibleStatusbar="false" Title="Airport Bias">
                    <ContentTemplate>
                        <table class="box1">
                            <tr>
                                <td class="tdLabel80">
                                    ICAO
                                </td>
                                <td class="tdLabel120">
                                    City
                                </td>
                                <td class="tdLabel80">
                                    State
                                </td>
                                <td class="tdLabel150">
                                    Country
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <Label id="lbDepartsICAO" data-bind="text: CurrentLegData.DepartureAirport.IcaoID" style="display: block"></Label>
                                </td>
                                <td>
                                    <Label id="lbDepartsCity" data-bind="text: CurrentLegData.DepartureAirport.CityName" style="display: block"></Label>
                                </td>
                                <td>
                                    <Label id="lbDepartsState" data-bind="text: CurrentLegData.DepartureAirport.StateName" style="display: block"></Label>
                                </td>
                                <td>
                                    <Label id="lbDepartsCountry" data-bind="text: CurrentLegData.DepartureAirport.CountryName" style="display: block"></Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <div class="nav-space">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    Airport
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <label id="lbDepartsAirport" data-bind="text: CurrentLegData.DepartureAirport.AirportName" style="display: block" class="text150"></label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <div class="nav-space">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table cellpadding="0" width="100%" cellspacing="0" class="preflight-box-noedit">
                                        <tr>
                                            <td class="tdLabel100">
                                                Takeoff Bias
                                            </td>
                                            <td class="tdLabel40">
                                                <label id="lbDepartsTakeoffbias" data-bind="text: CurrentLegData.DepartureAirport.StrTakeoffBIAS" style="display: block" class="text90"></label>
                                            </td>
                                            <td class="tdLabel100">
                                                Landing Bias
                                            </td>
                                            <td>
                                                <label ID="lbDepartsLandingbias" data-bind="text: CurrentLegData.DepartureAirport.StrLandingBIAS" style="display: block" class="text90"></label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdLandBias" runat="server" VisibleOnPageLoad="false" Width="600px"
                    Height="180px" Behaviors="Close" Modal="true" VisibleStatusbar="false" Title="Airport Bias">
                    <ContentTemplate>
                        <table class="box1">
                            <tr>
                                <td class="tdLabel80">
                                    ICAO
                                </td>
                                <td class="tdLabel120">
                                    City
                                </td>
                                <td class="tdLabel80">
                                    State
                                </td>
                                <td class="tdLabel150">
                                    Country
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label id="lbArrivesICAO" data-bind="text: CurrentLegData.ArrivalAirport.IcaoID" style="display: block"></label>
                                </td>
                                <td>
                                    <label id="lbArrivesCity" data-bind="text: CurrentLegData.ArrivalAirport.CityName" style="display: block"></label>
                                </td>
                                <td>
                                    <label id="lbArrivesState" data-bind="text: CurrentLegData.ArrivalAirport.StateName" style="display: block"></label>
                                </td>
                                <td>
                                    <label id="lbArrivesCountry" data-bind="text: CurrentLegData.ArrivalAirport.CountryName" style="display: block"></label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <div class="nav-space">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    Airport
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <label id="lbArrivesAirport" data-bind="text: CurrentLegData.ArrivalAirport.AirportName" style="display: block" class="text150"></label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <div class="nav-space">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table cellpadding="0" width="100%" cellspacing="0" class="preflight-box-noedit">
                                        <tr>
                                            <td class="tdLabel100">
                                                Takeoff Bias
                                            </td>
                                            <td class="tdLabel40">
                                                <label id="lbArrivesTakeoffbias" data-bind="text: CurrentLegData.ArrivalAirport.StrTakeoffBIAS" style="display: block" class="text90"></label>
                                            </td>
                                            <td class="tdLabel100">
                                                Landing Bias
                                            </td>
                                            <td>
                                                <label id="lbArrivesLandingbias" data-bind="text: CurrentLegData.ArrivalAirport.StrLandingBIAS" style="display: block" class="text90"></label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </telerik:RadWindow>
                <telerik:RadWindow ID="RadRetrievePopup" runat="server" OnClientResizeEnd="GetDimensions" OnClientClose="OnTripSearchClose"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    NavigateUrl="~/Views/Transactions/Preflight/PreflightSearch.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdOutBoundInstructions" runat="server" VisibleOnPageLoad="false" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" KeepInScreenBounds="true" Modal="true" BackColor="#DADADA" VisibleStatusbar="false" Behaviors="Close"
                    NavigateUrl="~/Views/Transactions/Preflight/PreflightOutboundInstruction.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdChecklist" runat="server" OnClientResizeEnd="GetDimensions" VisibleOnPageLoad="false" Width="700px" Height="500px"
                    Modal="true" BackColor="#DADADA" VisibleStatusbar="false" Behaviors="Close"
                    NavigateUrl="~/Views/Transactions/Preflight/PreflightChecklist.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdSIFL" runat="server" OnClientResizeEnd="GetDimensions" AutoSize="true"
                    KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    NavigateUrl="~/Views/Transactions/Preflight/PreflightSIFL.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                    KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    Width="400px" Height="236px" Title="Copy Trip" NavigateUrl="~/Views/Transactions/Preflight/CopyTrip.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdAdvanceCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="true" Title="Copy Trip" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                    Behaviors="Close" NavigateUrl="~/Views/Transactions/Preflight/AdvancedTripCopy.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdMoveTrip" runat="server" OnClientResizeEnd="GetDimensions"
                    AutoSize="false" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    Width="990px" Height="190px" Title="Move Trip" NavigateUrl="~/Views/Transactions/Preflight/MoveTrip.aspx">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdAirportPage" runat="server" OnClientResizeEnd="GetDimensions"
                    KeepInScreenBounds="true" AutoSize="true" Modal="true" ReloadOnShow="false" Behaviors="Close"
                    VisibleStatusbar="false" Title="Airport Information">
                </telerik:RadWindow>
                <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                    KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    Title="History" NavigateUrl="~/Views/Transactions/History.aspx">
                </telerik:RadWindow>
            </Windows>
            <ConfirmTemplate>
                <div class="rwDialogPopup radconfirm">
                    <div class="rwDialogText">
                        {1}
                    </div>
                    <div>
                        <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                        <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    </div>
                </div>
            </ConfirmTemplate>
            <AlertTemplate>
                <div class="rwDialogPopup radalert">
                    <div class="rwDialogText">
                        {1}
                    </div>
                    <div>
                        <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                            <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                        </a>
                    </div>
                </div>
            </AlertTemplate>
        </telerik:RadWindowManager>
        <%--<UCPreflight:Header ID="PreflightHeader" runat="server" />--%>
        <input type="hidden" id="hdTimeDisplayTenMin" data-bind="value: UserPrincipal._TimeDisplayTenMin == null ? 1 : UserPrincipal._TimeDisplayTenMin"/>
        <input type="hidden" id="hdShowRequestor" data-bind="value: UserPrincipal._IsReqOnly"/>
        <input type="hidden" id="hdHighlightTailno" data-bind="value: UserPrincipal._IsANotes" />
        <input type="hidden" id="hdIsDepartAuthReq" data-bind="value: UserPrincipal._IsDepartAuthDuringReqSelection"/>
        <input type="hidden" id="hdSetFarRules" data-bind="value: UserPrincipal._FedAviationRegNum == null ? '' : UserPrincipal._FedAviationRegNum"/>
        <input type="hidden" id="hdSetCrewRules" data-bind="value: UserPrincipal._IsAutoCreateCrewAvailInfo"/>
        <input type="hidden" id="hdSetWindReliability" data-bind="value: UserPrincipal._WindReliability == null ? 3 : UserPrincipal._WindReliability"/>
        <input type="hidden" id="hdHomeCategory" data-bind="value: UserPrincipal._DefaultFlightCatID == null ? 0 : UserPrincipal._DefaultFlightCatID"/>
        <input type="hidden" id="hdHomeChkListGroup"/>
        <input type="hidden" id="hdnHomeBaseCountry"/>
        <input type="hidden" id="hdnKilo" data-bind="value: UserPrincipal._IsKilometer"/>
        <input type="hidden" id="hdElapseTMRounding" data-bind="value: UserPrincipal._ElapseTMRounding"/>
        <input type="hidden" id="hdIsAutomaticCalcLegTimes" data-bind="value: UserPrincipal._IsAutomaticCalcLegTimes == null ? false : UserPrincipal._IsAutomaticCalcLegTimes"/>
        <div style="float: left; width: 400px; padding-top: 5px;">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <label id="lblfieldStatus">Status: </label>
                    </td>
                    <td>
                        <label id="lbStatus" data-bind="text: Status" class="inpt_non_edit"></label>
                    </td>
                    <td class="tblspace_25">
                        &nbsp;&nbsp;&nbsp;
                    </td>
                    <td>
                        <label id="lblfieldDesc">Description: </label>
                    </td>
                    <td>
                        <label id="lbDesc" data-bind="text: Preflight.PreflightMain.TripDescription" class="inpt_non_edit" ></label>
                    </td>
                </tr>
            </table>
        </div>
        <div style="float: right; text-align: right; width: 314px; padding-top: 5px;">
            <input type="hidden" id="hdnLeg" data-bind="value: CurrentLegNum"/>
            <input type="button" id="btnAddLeg" data-bind="attr: { 'data-leg': CurrentLegNum }, enable: EditMode(), css: EditMode() == true ? 'ui_nav' : 'ui_nav_disable'" onclick="AddLeg()" title="Add New Leg" value="Add Leg" />
            <input type="button" id="btnInsertLeg" data-bind="attr: { 'data-leg': CurrentLegNum }, enable: EditMode(), css: EditMode() == true ? 'ui_nav' : 'ui_nav_disable'" onclick="InsertLeg()" title="Insert New Leg Prior to the Highlighted Leg" value="Insert Leg" />
            <input type="button" id="btnDeleteLeg" data-bind="attr: { 'data-leg': CurrentLegNum }, enable: EditMode(), css: EditMode() == true ? 'ui_nav' : 'ui_nav_disable'" onclick="DeleteLeg()" title="Delete Existing Leg" value="Delete Leg"/>
            <input type="hidden" id="previousTabHidden"/>
        </div>
        <div runat="server" id="ChecklistOutboundTable" style="padding-top: 8px; padding-bottom: 5px">
            <asp:LinkButton CssClass="link_small" ID="lnkbtnChecklist" Text="Checklist" runat="server"
                OnClientClick="javascript:openWin('rdChecklist');return false;"></asp:LinkButton>
            |
            <asp:LinkButton CssClass="link_small" ID="lnkbtnOutbound" Text="Outbound Instructions"
                runat="server" OnClientClick="javascript:openWin('rdOutBoundInstructions');return false;"></asp:LinkButton>
        </div>
         <ul id="rtsLegs" data-bind="foreach: Legs" class="tabStrip">
             <li class="tabStripItem" data-bind="attr: { 'id': 'Leg'+LegNUM()}, event: { click: LegTab_Click }" >
                 Leg <span data-bind="text: LegNUM"></span>(<span data-bind="text: DepartureAirport.IcaoID"></span>-<span data-bind="text: ArrivalAirport.IcaoID"></span>)
             </li>
         </ul>
        <div style="width: 718px;">
            <fieldset style="margin: 6px 0 0 0;">
                <legend>Destination</legend>
                <div style="width: 550px; float: left">
                    <div style="width: 550px; float: left;">
                        <table width="100%" cellpadding="1" cellspacing="2">
                            <tr>
                                <td class="tdLabel110">
                                    <label id="lbDepartIcao" style="font-weight: bold" data-bind="attr: { title: CurrentLegData.DepartureAirport.tooltipInfo }" >Departure ICAO</label>
                                </td>
                                <td class="tdLabel200">
                                    <input type="text" id="tbDepart"  data-bind="value: CurrentLegData.DepartureAirport.IcaoID, enable: EditMode, event: { change: departureChanged }"  class="text60" maxlength="4" />
                                    <input type="hidden" data-bind="value: CurrentLegData.DepartureAirport.AirportID" id="hdDepart"/>
                                    <input type="hidden" id="hdAircraft" data-bind="value: Preflight.PreflightMain.Aircraft.AircraftID"/>
                                    <input type="hidden" id="hdHomebaseAirport"/>
                                    <input type="button" title="Display Airports" data-bind="enable: EditMode, css: BrowseBtn" ID="btnClosestIcao" onclick="    javascript: openWinAirport('DEPART'); return false;" />
                                </td>
                                <td class="tdLabel80">
                                    Flight No.
                                </td>
                                <td class="tdLabel90">
                                    <input type="text" id="tbFlightNumber" data-bind="value: CurrentLegData.FlightNUM.Trimmed(), enable: EditMode" maxlength="12" class="text112" />
                                </td>
                                <td rowspan="4" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="left">
                                    <label id="lbDepart" data-bind="text: CurrentLegData.DepartureAirport.AirportName, attr: { title: CurrentLegData.DepartureAirport.tooltipInfo }" style="display: block" class="input_no_bg"></label>
                                    <label id="lbcvDepart" class="alert-text" style="display: block"></label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel110">
                                    <label id="lbArriveIcao" style="font-weight: bold" data-bind="attr: { title: CurrentLegData.ArrivalAirport.tooltipInfo }">Arrival ICAO</label>
                                </td>
                                <td class="tdLabel200">
                                    <input type="text" id="tbArrival" data-bind="value: CurrentLegData.ArrivalAirport.IcaoID, enable: EditMode, event: { change: ArrivalChanged }" maxlength="4" class="text60" />
                                    <input type="hidden" id="hdArrival" data-bind="value: CurrentLegData.ArrivalAirport.AirportID"/>
                                    <input type="button" title="Display Airports" data-bind="enable: EditMode, css: BrowseBtn" id="btnArrival" onclick="javascript: openWinAirport('ARRIVAL'); return false;" />
                                </td>
                                <td class="tdLabel80">
                                    Cost
                                </td>
                                <td class="pr_radtextbox_117" align="left">
                                    <input type="text" id="tbCost" data-bind="formatCurrency: CurrentLegData.FlightCost, enable: false" class="formatCurrency"  />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="left">
                                    <label id="lbArrival" data-bind="text: CurrentLegData.ArrivalAirport.AirportName, attr: { title: CurrentLegData.ArrivalAirport.tooltipInfo }" style="font-weight: bold" class="input_no_bg"></label>
                                    <label id="lbcvArrival" class="alert-text" style="display: block"></label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div id="divTimeValidateMessage" style="color: red; padding-left:115px; display:none;"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="legs_destination_timewrape"  style="width: 550px; float: left;">
                        <div style="width: 114px; float: left;">
                            <table width="100%" cellpadding="1" cellspacing="2">
                                <tr>
                                    <td style="height: 50px;">
                                        <label id="lbDepartureDate">Departure</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 40px;">
                                        <label id="lbArrivalDate">Arrival</label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="width: 143px; float: left;">
                            <table width="100%" cellpadding="1" cellspacing="2">
                                <tr>
                                    <td>
                                        <table class="preflight-box" cellpadding="4" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" colspan="2" class="mnd_text">
                                                    Local
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="tdLabel80">
                                                    <input ID="tbLocalDate" type="text" data-bind="value: CurrentLegData.DepartureLocalDate, enable: EditMode, event: { change: DateTimeChanged }" class="textDatePicker" />
                                                </td>
                                                <td class="pr_leg_radmask">
                                                    <input ID="rmtbLocaltime" type="text" data-bind="value: CurrentLegData.DepartureLocalTime, enable: EditMode, event: { change: DateTimeChanged }" onblur="return RemoveTimeValidationMessage();" class="datetimechange" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="preflight-box" cellpadding="4" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" colspan="2" class="tdLabel120">
                                                    <span class="mnd_text">Local</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdLabel80">
                                                    <input ID="tbArrivalDate" type="text" data-bind="value: CurrentLegData.ArrivalLocalDate, enable: EditMode, event: { change: DateTimeChanged }" class="textDatePicker" />
                                                </td>
                                                <td class="pr_leg_radmask">
                                                    <input ID="rmtArrivalTime" type="text" data-bind="value: CurrentLegData.ArrivalLocalTime, enable: EditMode, event: { change: DateTimeChanged }" onblur="return RemoveTimeValidationMessage();" class="datetimechange" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="width: 143px; float: left;">
                            <table width="100%" cellpadding="1" cellspacing="2">
                                <tr>
                                    <td>
                                        <table class="preflight-box" cellpadding="4" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" colspan="2" class="mnd_text">
                                                    UTC
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdLabel80">
                                                    <input ID="tbUtcDate" type="text" data-bind="value: CurrentLegData.DepartureGreenwichDate, enable: EditMode, event: { change: DateTimeChanged }" class="textDatePicker"/>
                                                </td>
                                                <td class="pr_leg_radmask">
                                                     <input ID="rmtUtctime" type="text" data-bind="value: CurrentLegData.DepartureGreenwichTime, enable: EditMode, event: { change: DateTimeChanged }" onblur="return RemoveTimeValidationMessage();" class="datetimechange" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="preflight-box" cellpadding="4" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" colspan="2" class="mnd_text">
                                                    UTC
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdLabel80">
                                                    <input ID="tbArrivalUtcDate" type="text" data-bind="value: CurrentLegData.ArrivalGreenwichDate, enable: EditMode, event: { change: DateTimeChanged }" class="textDatePicker"/>
                                                </td>
                                                <td class="pr_leg_radmask">
                                                    <input ID="rmtArrivalUtctime" type="text" data-bind="value: CurrentLegData.ArrivalGreenwichTime, enable: EditMode, event: { change: DateTimeChanged }" onblur="return RemoveTimeValidationMessage();" class="datetimechange" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="width:143px; float: left;">
                            <table width="100%" cellpadding="1" cellspacing="2">
                                <tr>
                                    <td>
                                        <table class="preflight-box" cellpadding="4" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" colspan="2" class="mnd_text">
                                                    Home
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdLabel80">
                                                    <input ID="tbHomeDate"  type="text" data-bind="value: CurrentLegData.HomeDepartureDate, enable: EditMode, event: { change: DateTimeChanged }" class="textDatePicker" />
                                                </td>
                                                <td class="pr_leg_radmask">
                                                    <input ID="rmtHomeTime" type="text" data-bind="value: CurrentLegData.HomeDepartureLocal, enable: EditMode, event: { change: DateTimeChanged }" onblur="return RemoveTimeValidationMessage();" class="datetimechange" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="preflight-box" cellpadding="4" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" colspan="2" class="mnd_text">
                                                    Home
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdLabel80">
                                                    <input ID="tbArrivalHomeDate" type="text" data-bind="value: CurrentLegData.HomeArrivalDate, enable: EditMode, event: { change: DateTimeChanged }" class="textDatePicker"/> 
                                                </td>
                                                <td class="pr_leg_radmask"> 
                                                    <input ID="rmtArrivalHomeTime" type="text" data-bind="value: CurrentLegData.HomeArrivalTime, enable: EditMode, event: { change: DateTimeChanged }" onblur="return RemoveTimeValidationMessage();" class="datetimechange" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div style="width: 140px; float: left">
                    <label><input type="radio" id="rbDomestic" name="LegType" value="DOM" data-bind="checked: CurrentLegData.CheckGroup, enable: EditMode" />Domestic</label><br/>
                    <label><input type="radio" id="rbInternational" name="LegType" value="INT" data-bind="checked: CurrentLegData.CheckGroup, enable: EditMode" />International</label>
                    <br />
                    <br />
                    <label><input type="checkbox" data-bind="checked: CurrentLegData.IsScheduledServices, enable: EditMode" ID="chkOpenSeating" />Open Seating</label><br/>
                    <label><input type="checkbox" data-bind="checked: CurrentLegData.IsPrivate, enable: EditMode" ID="chkPrivate" />Confidential</label><br/>
                    <label><input type="checkbox" data-bind="checked: CurrentLegData.IsApproxTM, enable: EditMode" ID="chkApproxTime" />Approx.Time</label><br/>
                    <label><input type="checkbox" data-bind="checked: CurrentLegData.IsDutyEnd, enable: EditMode" ID="chkEndOfDuty" />End of Duty Day</label>
                </div>
                <div style="width: 695px; float: left;">
                    <table width="100%">
                        <tr>
                            <td class="tdLabel110">
                                Purpose
                            </td>
                            <td class="tdLabel300">
                                <input type="text" data-bind="value: CurrentLegData.FlightPurpose, enable: EditMode" ID="tbPurpose" maxlength="40" class="text280"></input>
                            </td>
                            <td class="tdLabel60">
                                Category
                            </td>
                            <td class="tdLabel40">
                                <input type="text" data-bind="value: CurrentLegData.FlightCatagory.FlightCatagoryCD, enable: EditMode, event: { change: CategoryValidation }" id="tbCategory" maxlength="4" onkeypress="return fnAllowAlphaNumeric(this, event)" class="text30" onblur="return RemoveSpecialChars(this)"/>
                                <input type="hidden" data-bind="value: CurrentLegData.FlightCategoryID" id="hdCategory"/>
                                <input type="hidden" data-bind="value: self.CurrentLegData.isDeadCategory" id="hdnDeadCategory"/>
                            </td>
                            <td class="tdLabel30">
                                <input type="button" data-bind="enable: EditMode, css: BrowseBtn" title="View Flight Categories" id="btnCategory" onclick="    javascript: openWin('radFlightCategoryPopup'); return false;" />
                            </td>
                            <td align="left">
                                <%-- <asp:CheckBox ID="chkLeg" runat="server" />All Legs--%>
                                <input type="button" data-bind="enable: EditMode, css: CopyIcon, event: { click: confirmCopyFlightCategoryToAllLeg }" id="btnCategoryLegs" title="Copy Category to all Legs"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                            </td>
                            <td colspan="3">
                                <label id="lbcvCategory" class="alert-text" style="display: block"></label>
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
            <fieldset style="margin: 6px 0 0 0;">
                <legend>Distance & ETE</legend>
                <table cellpadding="1" cellspacing="2">
                    <tr>
                        <td class="tdLabel90">
                            <label id="lbMiles" data-bind="text: UserPrincipal._IsKilometer == true ? 'Kilometers' : 'Miles(N)'"></label>
                        </td>
                        <td class="tdLabel100">
                            <input type="text" data-bind="value: CurrentLegData.StrConvertedDistance, enable: EditMode, event: { change: Miles_Validate_Retrieve }" id="tbMiles" maxlength="5" onkeypress="return fnAllowNumericAndOneDecimal('miles',this, event)" class="text60" />
                            <%--Text="618"--%>
                        </td>
                        <td class="tdLabel60">
                            T/O Bias
                        </td>
                        <td class="tdLabel100">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="tdLabel50">
                                        <input type="text" data-bind="value: CurrentLegData.StrTakeoffBIAS, enable: EditMode, event: { change: CheckTenthorHourMinValuebias }" id="tbBias" onfocus="this.select();" class="text40"
                                            onkeypress="return fnAllowNumericAndChar(this, event,': .')"  maxlength="5"
                                            onclick="this.select();" />
                                        <%--Text="-1"--%>
                                    </td>
                                    <td>
                                        <input type="button" id="imgbtnTOBias" title="Display Airport or Aircraft Bias"
                                            onclick="javascript:openWin('rdTOBias');return false;" class="calc-icon" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="tdLabel80">
                            TAS
                        </td>
                        <td colspan="2" class="tdLabel70">
                            <input type="text" data-bind="value: CurrentLegData.TrueAirSpeed, enable: EditMode, event: { change: TASValidation }" id="tbTAS" onfocus="this.select();"
                                   maxlength="3" onkeypress="return fnAllowNumeric(this, event)"
                                   class="text60"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel90">
                            Power Setting
                        </td>
                        <td class="tdLabel100">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <input type="text" data-bind="value: CurrentLegData.PowerSetting, enable: EditMode, event: { change: PowersettingValidation }" id="tbPower" onfocus="this.select();"
                                           class="text60" maxlength="1" onblur="return RemoveSpecialChars(this)"></input>
                                        <%--Text="1"--%>
                                        <input type="hidden" id="hdPower"/>
                                        <input type="button" data-bind="enable: EditMode, css: BrowseBtn" title="Display Aircraft Power Settings" id="btnPower"
                                            onclick="javascript:openWin('rdPowerSetting');return false;" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="tdLabel60">
                            Land Bias
                        </td>
                        <td class="tdLabel100">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="tdLabel50">
                                        <input type="text" data-bind="value: CurrentLegData.StrLandingBias, enable: EditMode, event: { change: CheckTenthorHourMinValueLandbias }" id="tbLandBias" class="text40" onfocus="this.select();"
                                               onkeypress="return fnAllowNumericAndChar(this, event,': .')"
                                               maxlength="5" />
                                        <%-- Text="-1" --%>
                                    </td>
                                    <td>
                                        <input type="button" id="imgbtnLandBias" title="Display Airport or Aircraft Bias"
                                            onclick="javascript:openWin('rdLandBias');return false;" class="calc-icon" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="tdLabel80">
                            Wind Factor
                        </td>
                        <td class="tdLabel90">
                            <input type="text" data-bind="value: CurrentLegData.WindsBoeingTable, enable: EditMode, event: { change: checkNegativeValue }" id="tbWind"  onfocus="this.select();"
                                   onkeypress="return fnAllowNumericAndChar(this, event,'-')"
                                   maxlength="5" class="text60" /><%-- Text="-91"--%>
                        </td>
                        <td align="left">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left" class="tdLabel50">
                                        ETE
                                    </td>
                                    <td align="left">
                                        <input type="text" data-bind="formatSingleDecimal: CurrentLegData.StrElapseTM, enable: EditMode, event: { change: CheckTenthorHourMinValueEte }" id="tbETE" class="text40" onfocus="this.select();"
                                               onkeypress="return fnAllowNumericAndChar(this, event,': .')"
                                               maxlength="5" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <label id="lbcvPower" class="alert-text" style="display: block"></label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Wind Reliability
                        </td>
                        <td align="left" colspan="6">
                            <table class="legs_wind_wraper" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="middle" style="padding:0 0 0 10px;">
                                        <table style="height:35px;" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="height:35px;">
                                                    <input type="radio" id="rbWindReliability1" name="rblistWindReliability" value="1" data-bind="checked: CurrentLegData.StrWindReliability, enable: EditMode" /> 
                                                </td>
                                                <td style="height:35px;line-height:35px;">
                                                    50%
                                                </td>
                                            </tr>
                                        </table>                                        
                                    </td>
                                    <td valign="middle" style="height:50px;padding:0 0 0 10px;">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <input type="radio" id="rbWindReliability2" name="rblistWindReliability" value="2" data-bind="checked: CurrentLegData.StrWindReliability, enable: EditMode" /> 
                                                </td>
                                                 <td>
                                                     75%
                                                </td>
                                            </tr>
                                        </table>                                        
                                    </td>
                                    <td valign="middle" style="height:50px;padding:0 0 0 10px;">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <input type="radio" id="rbWindReliability3" name="rblistWindReliability" value="3" data-bind="checked: CurrentLegData.StrWindReliability, enable: EditMode" /> 
                                                </td>
                                                 <td>
                                                     85%
                                                </td>
                                            </tr>
                                        </table>                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <fieldset style="margin: 6px 0 0 0;">
                <legend>Duty</legend>
                <div style="width: 550px; float: left">
                    <table>
                        <tr>
                            <td valign="top" class="tdLabel90">
                                <label id="lbCrewRules" data-bind="attr: { title: CrewDutyRuleTitle }" style="font-weight: bold">Crew Rules</label>
                            </td>
                            <td valign="top" class="tdLabel150">
                                <input type="text" data-bind="value: CurrentLegData.CrewDutyRule.CrewDutyRuleCD, enable: EditMode, attr: { title: CrewDutyRuleTitle }, event: { change: CrewDutyRuleValidation }" id="tbCrewRules" class="text90" maxlength="4"
                                       onkeypress="return fnAllowAlphaNumeric(this, event)" onBlur="return RemoveSpecialChars(this)"/>
                                <input type="hidden" data-bind="value: CurrentLegData.CrewDutyRulesID" id="hdCrewRules"/>
                                <input type="button" data-bind="enable: EditMode, css: BrowseBtn" title="View Crew Duty Rules" id="btnCrewRules" onclick="    javascript: openWin('radCrewDutyRulesPopup'); return false;" />
                            </td>
                            <td class="tdLabel60" valign="top">
                                <input type="button" data-bind="enable: EditMode, css: CopyIcon, event: {click:copyCrewDutyRuleToAllLeg}" id="btnCrewRulesAllLegs" title="Copy Crew Duty Rules to all legs"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label id="lbcvCrewRules" class="alert-text" style="display: block"></label>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Override Duty Day Start
                            </td>
                            <td valign="top">
                                <input type="text" data-bind="formatSingleDecimal: CurrentLegData.OverrideValue, enable: EditMode, event: { change: checknumericValue }" id="tbOverride" title="Required format is NN.N" maxlength="4" class="text60"/>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width: 140px; float: left">
                    <table class="preflight-box-noedit" width="100%" cellpadding="2">
                        <tr>
                            <td class="tdLabel80" valign="top">
                                FAR
                            </td>
                            <td>
                                <label id="lbFar" data-bind="text: CurrentLegData.FedAviationRegNUM" class="infoash" ></label>
                                <input type="hidden" id="hdnFar"/>
                                <%--Text="91"--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel80" valign="top">
                                Total Duty
                            </td>
                            <td valign="top">
                                <label id="lbTotalDuty" data-bind="MathRound: CurrentLegData.DutyHours == null ? 0.0 : CurrentLegData.DutyHours, e: 1, css: DutyHoursAlert">0.0</label>
                                <input type="hidden" id="hdnTotalDuty"/>
                                <%--Text="20.0"--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel80" valign="top">
                                Total Flight
                            </td>
                            <td valign="top">
                                <label id="lbTotalFlight" data-bind="MathRound: CurrentLegData.FlightHours == null ? 0.0 : CurrentLegData.FlightHours, e: 1, css: FlightHoursAlert" >0.0</label>
                                 <input type="hidden" id="hdnTotalFlight"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel80" valign="top">
                                Rest
                            </td>
                            <td valign="top">
                                <label id="lbRest" data-bind="MathRound: CurrentLegData.RestHours == null ? 0.0 : CurrentLegData.RestHours, e: 1, css: RestHoursAlert">0.0</label>
                                <input type="hidden" id="hdnRest"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
            <fieldset style="margin: 6px 0 0 0;">
                <legend>Other Info</legend>
                <table width="100%">
                    <tr>
                        <td class="tdLabel90">
                            Passenger
                        </td>
                        <td class="tdLabel130">
                            <input type="text" data-bind="value: CurrentLegData.PassengerTotal, enable: false" id="tbPax" maxlength="4" onkeypress="return fnAllowNumeric(this, event)"
                                   value="0" class="inpt_non_edit text40"/>
                        </td>
                        <td class="tdLabel80">
                            Available
                        </td>
                        <td class="tdLabel130">
                            <input type="text" data-bind="value: CurrentLegData.ReservationAvailable, style: { color: CurrentLegData.ReservationAvailable < 0?'red':'#111'}, enable: false" id="tbAvail" maxlength="4" onkeypress="return fnAllowNumeric(this, event)"
                                   value="0" class="inpt_non_edit text40"/>
                        </td>
                        <td class="tdLabel100">
                            <label id="lbNogo">No Go</label>
                        </td>
                        <td>
                            <input type="text" data-bind="value: CurrentLegData.GoTime, enable: EditMode" id="tbNogo" class="text80" onkeypress="return fnAllowAlphaNumeric(this, event)"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel90">
                            Requestor
                        </td>
                        <td class="tdLabel130">
                            <input type="text" data-bind="value: CurrentLegData.Passenger.PassengerRequestorCD, enable: EditMode, event: { change: RequestorValidator }" id="tbRequestor" class="text80" maxlength="5"onkeypress="return fnAllowAlphaNumeric(this, event)"
                                   onblur="return RemoveSpecialChars(this)"/>
                            <input type="hidden" data-bind="value: CurrentLegData.Passenger.PassengerRequestorID" id="hdRequestor"/>
                            <input type="button" data-bind="enable: EditMode, css: BrowseBtn" title="View Requestors" id="btnRequestor" onclick="    javascript: openWin('radPaxInfoPopup'); return false;" />
                        </td>
                        <td class="tdLabel80">
                            Department
                        </td>
                        <td class="tdLabel130">
                            <input type="text" data-bind="value: CurrentLegData.Department.DepartmentCD, enable: EditMode, event: { change: DepartmentValidator }" id="tbDepartment" maxlength="25" class="text80" onkeypress="return fnAllowAlphaNumeric(this, event)"
                                   onblur="return RemoveSpecialChars(this)"/>
                            <input type="hidden" data-bind="value: CurrentLegData.Department.DepartmentID" id="hdDepartment"/>
                            <input type="button" data-bind="enable: EditMode, css: BrowseBtn" title="View Departments" id="btnDepartment" onclick="    javascript: openWin('rdDeptPopup'); return false;" />
                        </td>
                        <td class="tdLabel100">
                            Authorization
                        </td>
                        <td>
                            <input type="text" data-bind="value: CurrentLegData.DepartmentAuthorization.AuthorizationCD, enable: EditMode, event: { change: AuthorizationValidator }" id="tbAuthorization" maxlength="8" class="text80"
                                   onkeypress="return fnAllowAlphaNumeric(this, event)" onblur="return RemoveSpecialChars(this)"/>
                            <input type="hidden" data-bind="value: CurrentLegData.DepartmentAuthorization.AuthorizationID" id="hdAuthorization"/>
                            <input type="button" data-bind="enable: EditMode, css: BrowseBtn" title="View Authorizations" id="btnAuthorization" onclick=" return fnValidateAuth();" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="tdLabel220">
                            <label id="lbRequestor" data-bind="text: CurrentLegData.Passenger.RequestorDesc" style="display: block" class="input_no_bg"></label>
                            <label id="lbcvRequestor" class="alert-text" style="display: block"></label>
                        </td>
                        <td colspan="2" class="tdLabel210">
                            <label id="lbDepartment" data-bind="text: CurrentLegData.Department.DepartmentName" style="display: block" class="input_no_bg"></label>
                            <label id="lbcvDepartment" class="alert-text" style="display: block"></label>
                        </td>
                        <td colspan="2" class="tdLabel210">
                            <label id="lbAuthorization" data-bind="text: CurrentLegData.DepartmentAuthorization.DeptAuthDescription" style="display: block" class="input_no_bg"></label>
                            <label id="lbcvAuthorization" style="display: block" class="alert-text"></label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel90">
                            Account No.
                        </td>
                        <td class="tdLabel130">
                            <input type="text" data-bind="value: CurrentLegData.Account.AccountNum, enable: EditMode, event: { change: AccountValidator }" id="tbAccountNo" maxlength="32" class="text80" onkeypress="return fnAllowNumericAndChar(this, event,'.')"
                                   onblur="return RemoveSpecialChars(this)"/>
                            <input type="hidden" data-bind="value: CurrentLegData.Account.AccountID" id="hdAccountNo"/>
                            <input type="button" data-bind="enable: EditMode, css: BrowseBtn" title="View Account Numbers" id="btnAccountNo" onclick="    javascript: openWin('radAccountMasterPopup'); return false;" />
                        </td>
                        <td class="tdLabel80">
                            Client
                        </td>
                        <td class="tdLabel130">
                            <input type="text" data-bind="value: CurrentLegData.Client.ClientCD, enable: EditMode, event: { change: ClientCodeValidation }" id="tbClient" maxlength="5" class="text80"
                                   onkeypress="return fnAllowAlphaNumeric(this, event)" onblur="return RemoveSpecialChars(this)"/>
                            <input type="hidden" data-bind="value: CurrentLegData.Client.ClientID" id="hdClient"/>
                            <input type="button" data-bind="enable: EditMode, css: BrowseBtn" title="View Clients" id="btnClient" onclick="    javascript: openWin('rdClientCodePopup'); return false;" />
                        </td>
                        <td class="tdLabel100">
                            Charter Customer
                        </td>
                        <td class="tdLabel120" valign="top">
                            <input type="text" data-bind="value: CurrentLegData.CQCustomer.CQCustomerCD, CQCustomerEnableDisable: EditMode,CQCheckFor:'Leg', event: { change: CQCustomerValidator }" id="tbCQCustomer" class="text80" maxlength="8" onkeypress="return fnAllowAlphaNumeric(this, event)"/>
                            <input type="hidden" data-bind="value: CurrentLegData.CQCustomer.CQCustomerID" id="hdCQCustomer"/>
                            <input type="button" data-bind="enable: EditMode, css: BrowseBtn" title="View CQCustomer" id="btCQcustomerPopup" onclick="    javascript: openWin('radCQCPopups'); return false;" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="tdLabel220">
                            <label id="lbAccountNo" data-bind="text: CurrentLegData.Account.AccountDescription" class="input_no_bg" style="display: block"></label>
                            <label id="lbcvAccountNo" class="alert-text" style="display: block"></label>
                        </td>
                        <td colspan="2" class="tdLabel220">
                            <label id="lbClient" data-bind="text: CurrentLegData.Client.ClientDescription" class="input_no_bg" style="display: block"></label>
                            <label id="lbcvClient" class="alert-text" style="display: block"></label>
                        </td>
                        <td colspan="2" class="tdLabel270">
                            <label id="lbCQCustomer" data-bind="text: CurrentLegData.CQCustomer.CQCustomerName" style="display: block" class="input_no_bg"></label>
                            <label id="lbcvCQCustomer" class="alert-text" style="display: block"></label>
                        </td>
                    </tr>
                </table>
                <div style="width: 430px; float: left; padding: 5px 0 5px 0">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="pr_leg_usa_radmask" rowspan="2">
                                USA Border Crossing
                                <br />
                                <textarea id="tbBorderCrossing" title="This value shall be the local estimated time and location of U.S. border crossing." data-bind="value: CurrentLegData.USCrossing, enable: EditMode" maxlength="75"  rows="2" style="width:398px;"></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width:208px; float: left; padding: 5px 0 5px 29px">
                    <table width="100%">
                        <tr>
                            <td>
                                <label id="lbCBPConfirmNo">CBP Confirm No.</label>
                            </td>
                            <td>
                                <input type="text" data-bind="enable: false" id="tbCBPConfirmNumber" maxlength="25" class="text80" onkeypress="return fnAllowAlphaNumeric(this, event)"/>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width:698px;" class="leg_fieldset">
                    <telerik:RadPanelBar ID="pnlLegNotes" Width="100%" ExpandAnimation-Type="None" CollapseAnimation-Type="None"
                        runat="server">
                        <Items>
                            <telerik:RadPanelItem runat="server" Expanded="false" Text="Leg Notes">
                                <Items>
                                    <telerik:RadPanelItem>
                                        <ContentTemplate>
                                            <table class="preflight_legnotes" width="100%" cellpadding="0" cellspacing="0" class="box1">
                                                <tr>
                                                    <td class="pr_main_radmask left_radmask" style="line-height:24px;">
                                                        Crew Notes
                                                    </td>
                                                    <td></td>
                                                    <td class="pr_main_radmask right_radmask">
                                                        PAX Notes
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="pr_main_radmask left_radmask">
                                                        <textarea id="tbCrewNotes" data-bind="enable: EditMode, value: CurrentLegData.CrewNotes" rows="3"></textarea>
                                                    </td>
                                                    <td class="tdLabel60" valign="top">
                                                         <input type="button" data-bind="enable: EditMode, css: CopyIcon, event: { click : copyCrewNotesToAllLeg }" id="btnCrewNotesAllLegs" title="Copy Crew Notes to all legs"/>
                                                    </td>
                                                    <td class="pr_main_radmask right_radmask">
                                                        <textarea id="tbPaxNotes" data-bind="enable: EditMode, value: CurrentLegData.Notes" rows="3"></textarea>
                                                    </td>
                                                    <td class="tdLabel60" valign="top">
                                                        <input type="button" data-bind="enable: EditMode, css: CopyIcon, event: { click : copyPaxNotesToAllLeg }" id="btnPaxNotesAllLegs" title="Copy PAX Notes to all legs"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label id="lblCrewlastupatedhdn" style="display:none"></label>
                                                        <label id="lblCrewlastupated" style="display:none"></label>
                                                    </td>
                                                    <td>
                                                        <label id="lblPaxlastupatedhdn" style="display:none"></label>
                                                        <label id="lblPaxlastupated" style="display:none"></label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </telerik:RadPanelItem>
                                </Items>
                            </telerik:RadPanelItem>
                        </Items>
                    </telerik:RadPanelBar>
                </div>
                <div style="float: right; text-align: right; width: 314px; padding-top: 5px;" > 
                    <input type="button" id="btnAddLeg1" data-bind="attr: { 'data-leg': CurrentLegNum }, enable: EditMode(), css: EditMode() == true ? 'ui_nav' : 'ui_nav_disable'" onclick="setFocusOnTopOfScreen('#DivExternalForm'); AddLeg();" title="Add New Leg" value="Add Leg" />
                    <input type="button" id="btnInsertLeg1" data-bind="attr: { 'data-leg': CurrentLegNum }, enable: EditMode(), css: EditMode() == true ? 'ui_nav' : 'ui_nav_disable'" onclick="setFocusOnTopOfScreen('#DivExternalForm'); InsertLeg();" title="Insert New Leg Prior to the Highlighted Leg" value="Insert Leg"  />
                    <input type="button" id="btnDeleteLeg1" data-bind="attr: { 'data-leg': CurrentLegNum }, enable: EditMode(), css: EditMode() == true ? 'ui_nav' : 'ui_nav_disable'" onclick="setFocusOnTopOfScreen('#DivExternalForm'); DeleteLeg();" title="Delete Existing Leg" value="Delete Leg" />
                </div>
            </fieldset>
            <div style="text-align:right;padding-top:11px;padding-right:5px;padding-bottom:7px;">
                 <UCPreflight:Footer ID="PreflightHeader" runat="server" />
            </div>
        </div>
        <table id="tblHidden" style="display: none;">
            <tr>
                <td>
                    <input type="button" id="btnCopyYes" value="Button"  />
                    <input type="button" id="btnCopyNo" value="Button" />
                    <input type="button" id="btnDeleteLegYes" value="Button" />
                    <input type="button" id="btnDeleteLegNo" value="Button" />
                    <input type="button" id="btnDeadHeadLegYes" value="Button" />
                    <input type="button" id="btnDeadHeadLegNo" value="Button" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>


