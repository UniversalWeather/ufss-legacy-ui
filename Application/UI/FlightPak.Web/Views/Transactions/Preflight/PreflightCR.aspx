﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/PreflightLegacy.Master"
    AutoEventWireup="true" CodeBehind="PreflightCR.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.Preflight.PreflightCR" %>

<%@ MasterType VirtualPath="~/Framework/Masters/PreflightLegacy.Master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function openWin(radWin) {

                var url = '';
                if (radWin == "RadRetrievePopup") {
                    url = 'PreflightSearch.aspx';
                }
                if (radWin == "rdSIFL") {
                    url = '/Views/Transactions/Preflight/PreflightSIFL.aspx';
                }
                if (radWin == "rdCopyTrip") {
                    url = '/Views/Transactions/Preflight/CopyTrip.aspx';
                }
                if (radWin == "rdMoveTrip") {
                    url = '/Views/Transactions/Preflight/MoveTrip.aspx';
                }
                if (radWin == "rdPreflightAPIS") {
                    url = "/Views/Transactions/Preflight/PreflightAPIS.aspx?APIS=" + "APIS";
                }
                if (radWin == "rdHistory") {
                    url = "../../Transactions/History.aspx?FromPage=" + "Preflight";
                }
                // PROD-38
                if (radWin == "rdPreflightEMAIL") {
                    url = '/Views/Transactions/Preflight/EmailTrip.aspx';
                }
                if (url != "")
                    var oWnd = radopen(url, radWin);

                if (radWin == "rdTravelSense") {
                    var left = (screen.width / 2) - (2 / 2);
                    var top = (screen.height / 2) - (2 / 2);
                    var test = window.open("/Views/Transactions/Preflight/PreflightTravelSense.aspx", "Travelsense", "width=2px,height=2px,top=" + top + ", left=" + left);
                }
            }

            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

            function RequestStart(sender, args) {
                currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
                currentUpdatedControl = "<%= DivExternalForm.ClientID %>";
                //show the loading panel over the updated control
                currentLoadingPanel.show(currentUpdatedControl);
            }
            function ResponseEnd() {
                //hide the loading panel and clean up the global variables
                if (currentLoadingPanel != null)
                    currentLoadingPanel.hide(currentUpdatedControl);
                currentUpdatedControl = null;
                currentLoadingPanel = null;
            }

            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
            // this function is used to the refresh the  grid
            function refreshGrid(arg) {
                $find("<%= Master.RadAjaxManagerMaster.ClientID %>").ajaxRequest('Rebind');

            }

            function RowDblClick(sender, eventArgs) {
                sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
            }

            function gridCreated(sender, args) {
                if (sender.get_editIndexes && sender.get_editIndexes().length > 0) {
                    document.getElementById("OutPut").innerHTML = sender.get_editIndexes().join();
                }
                else {
                    document.getElementById("OutPut").innerHTML = "";
                }
            }



            function fnCloseRadWindow() {
                GetRadWindow().Close();
            }            

        </script>
    </telerik:RadCodeBlock>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="rdPreflightEMAIL" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false" AutoSize="true"
                Width="780px" Height="650px" Title="E-mail Notification" NavigateUrl="~/Views/Transactions/Preflight/EmailTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadRetrievePopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/PreflightSearch.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdSIFL" runat="server" OnClientResizeEnd="GetDimensions" AutoSize="true"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/PreflightSIFL.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Width="400px" Height="236px" Title="Copy Trip" NavigateUrl="~/Views/Transactions/Preflight/CopyTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdAdvanceCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" Title="Copy Trip" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                Behaviors="Close" NavigateUrl="~/Views/Transactions/Preflight/AdvancedTripCopy.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdMoveTrip" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="false" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Width="990px" Height="590px" Title="Move Trip" NavigateUrl="~/Views/Transactions/Preflight/MoveTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdPreflightAPIS" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="APIS" NavigateUrl="~/Views/Transactions/Preflight/PreflightAPIS.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdAirportPage" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" AutoSize="true" Modal="true" ReloadOnShow="false" Behaviors="Close"
                VisibleStatusbar="false" Title="Airport Information">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdTravelSense" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="TravelSense" NavigateUrl="~/Views/Transactions/Preflight/PreflightTravelSense.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="History" NavigateUrl="~/Views/Transactions/History.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <div style="width: 718px;">
            <asp:Panel runat="server" ID="pnlPreflight" Visible="true">
                <div style="width: 718px; padding: 5px 0 0 0px;">
                    <div style="width: 718px; float: left">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    Dispatcher Notes
                                </td>
                            </tr>
                            <tr>
                                <td class="pr_radtextbox_710x100">
                                    <telerik:RadTextBox ID="tbDispatcherNotes" runat="server" TextMode="MultiLine" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="tblspace_10">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Requestor Notes
                                </td>
                            </tr>
                            <tr>
                                <td class="pr_radtextbox_710x100">
                                    <telerik:RadTextBox ID="tbRequestorNotes" runat="server" TextMode="MultiLine" Enabled="false" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <div style="text-align: right; padding: 5px 0 0 0;">
            <table width="718px" cellspacing="6">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnDeleteTrip" runat="server" ToolTip="Delete Selected Record" Text="Delete Trip"
                            OnClick="btnDelete_Click" CssClass="button" />
                        <asp:Button ID="btnEditTrip" runat="server" ToolTip="Edit Selected Record" Text="Edit Trip"
                            OnClick="btnEditTrip_Click" CssClass="button" />
                        <asp:Button ID="btnCancel" runat="server" ToolTip="Cancel All Changes" Text="Cancel"
                            OnClick="btnCancel_Click" CssClass="button" />
                        <asp:Button ID="btnSave" runat="server" ToolTip="Save Changes" Text="Save" OnClick="btnSave_Click"
                            ValidationGroup="Save" CssClass="button" />
                        <asp:Button ID="btnNext" runat="server" Text="Next" Visible="false" OnClick="btnNext_Click"
                            CssClass="button" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
