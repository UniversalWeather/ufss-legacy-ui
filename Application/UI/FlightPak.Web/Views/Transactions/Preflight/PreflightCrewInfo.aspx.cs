﻿using System;
using FlightPak.Web.Views.Transactions.Preflight;
using FlightPak.Web.ViewModels;
using Newtonsoft.Json;

namespace FlightPak.Web.Views.Transactions.PreFlight
{
    public partial class PreflightCrewInfo : BaseSecuredPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PreflightTripManager.CheckSessionAndRedirect();
            if (!IsPostBack)
            {
                BusinessLite.Preflight.PreflightTripManagerLite.UpdatePreflightTripViewModelAsPerCalenderSelection();
                PreflightUtils.ValidatePreflightSession();
                dynamic preflightCrewInitializer = new { Hotel = new PreflightLegCrewHotelViewModel(), Transport = new PreflightLegCrewTransportationViewModel() };
                hdnPreflightCrewInitializationSchema.Value = JsonConvert.SerializeObject(preflightCrewInitializer);                
            }
        }
    }
}
