﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreflightSearchRPTPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.Preflight.PreflightSearchRPTPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Select Tripsheet</title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="radScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
        <script type="text/javascript" src="../../../Scripts/Common.js"></script>
        <script type="text/javascript">
            function confirmCallBackFn(arg) {
                if (arg == true) {
                    document.getElementById('<%=btnYes.ClientID%>').click();
                }
                else {
                    document.getElementById('<%=btnNo.ClientID%>').click();
                }
            }

            //this function is used to navigate to pop up screen's with the selected code
            function openWin(radWin) {
                var url = '';
                if (radWin == "RadClientCodePopup") {
                    url = '/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('<%=tbSearchClientCode.ClientID%>').value;
                }
                var oWnd = radopen(url, radWin);
            }

            // this function is used to display the value of selected Client code from popup
            function ClientCidePopupClose(oWnd, args) {
                var combo = $find("<%= tbSearchClientCode.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg != null) {
                    if (arg) {
                        document.getElementById("<%=tbSearchClientCode.ClientID%>").value = arg.ClientCD;
                        document.getElementById("<%=lbClientCode.ClientID%>").innerHTML = arg.ClientDescription;
                        document.getElementById("<%=hdClientCodeID.ClientID%>").value = arg.ClientID;
                        if (arg.ClientCD != null)
                            document.getElementById("<%=lbcvClientCode.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=tbSearchClientCode.ClientID%>").value = "";
                        document.getElementById("<%=hdClientCodeID.ClientID%>").value = "";
                        document.getElementById("<%=lbClientCode.ClientID%>").innerHTML = "";
                        combo.clearSelection();
                    }
                }
            }


            // this function is used to get the dimensions
            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }

            //this function is used to close this window
            function CloseAndRebind(arg) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.location.reload();
            }

            function CloseRadWindow(arg) {
                GetRadWindow().Close();

            }


            //this function is used to set the frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                return oWindow;
            }
            var currentTextBox = null;
            var currentDatePicker = null;

            //This method is called to handle the onclick and onfocus client side events for the texbox
            function showPopup(sender, e) {
                //this is a reference to the texbox which raised the event
                //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);



                //this gets a reference to the datepicker, which will be shown, to facilitate
                //the selection of a date
                var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                //this variable is used to store a reference to the date picker, which is currently
                //active
                currentDatePicker = datePicker;

                //this method first parses the date, that the user entered or selected, and then
                //sets it as a selected date to the picker


                datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));


                //the code lines below show the calendar, which is used to select a date. The showPopup
                //function takes three arguments - the x and y coordinates where to show the calendar, as
                //well as its height, derived from the offsetHeight property of the textbox
                var position = datePicker.getElementPosition(currentTextBox);
                datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
            }

            //this handler is used to set the text of the TextBox to the value of selected from the popup
            function dateSelected(sender, args) {

                if (currentTextBox != null) {
                    if (currentTextBox.value != args.get_newValue()) {
                        currentTextBox.value = args.get_newValue();
                    }
                }
            }

            function tbDate_OnKeyDown(sender, event) {
                if (event.keyCode == 9) {
                    var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                    datePicker.hidePopup();
                    return true;
                }
            }
            //this function is used to parse the date entered or selected by the user
            function parseDate(sender, e) {
                if (currentDatePicker != null) {
                    var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                    var dateInput = currentDatePicker.get_dateInput();
                    var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());

                    sender.value = formattedDate;
                }
            }
            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgPreflightRetrieve.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "TripNUM");
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "TripID");
                        if (i == 0) {
                            oArg.TripNUM = cell1.innerHTML + ",";
                            oArg.TripID = cell2.innerHTML + ","; 
                        }
                        else {
                            oArg.TripNUM += cell1.innerHTML + ",";
                            oArg.TripID += cell2.innerHTML + ",";
                        }
                    }
                }
                else {
                    oArg.TripNUM = "";
                    oArg.TripID = "";
                }

                if (oArg.TripNUM != "")
                    oArg.TripNUM = oArg.TripNUM.substring(0, oArg.TripNUM.length - 1)
                else
                    oArg.TripNUM = "";

                if (oArg.TripID != "")
                    oArg.TripID = oArg.TripID.substring(0, oArg.TripID.length - 1)
                else
                    oArg.TripID = "";

                oArg.Arg1 = oArg.TripNUM;
                oArg.Arg2 = oArg.TripID;
                oArg.CallingButton = "<%=Microsoft.Security.Application.Encoder.HtmlEncode(Param) %>";
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);

                }
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
        <ClientEvents OnDateSelected="dateSelected" />
        <DateInput ID="DateInput1" runat="server">
        </DateInput>
    </telerik:RadDatePicker>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tblMain" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tbDepDate" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tbSearchClientCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lbClientCode" />
                    <telerik:AjaxUpdatedControl ControlID="hdClientCodeID" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="RadClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="ClientCidePopupClose" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server">
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <asp:HiddenField ID="hdClientCodeID" runat="server" />
        <table class="preflight-search-box">
            <tr>
                <td valign="top" width="50%">
                    <fieldset>
                        <legend>Search</legend>
                        <table width="100%">
                            <tr>
                                <td class="tdLabel140">
                                    <asp:CheckBox ID="chkHomebase" Text="Home Base Only" runat="server" />
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="tdLabel110">
                                                Start Depart Date
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbDepDate" runat="server" CssClass="text70" onKeyPress="return fnAllowNumericAndChar(this, event,'/')"
                                                    onkeydown="return tbDate_OnKeyDown(this, event);" onclick="showPopup(this, event);"
                                                    onfocus="showPopup(this, event);" MaxLength="10" onBlur="parseDate(this, event);"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel140" valign="top">
                                    <asp:CheckBox ID="ChkExcLog" Text="Exclude Logged" runat="server" />
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="tdLabel110">
                                                            Client Code
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbSearchClientCode" runat="server" MaxLength="5" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                                CssClass="text50" ValidationGroup="Save" onBlur="return RemoveSpecialChars(this)"
                                                                OnTextChanged="tbSearchClientCode_TextChanged" AutoPostBack="true"> 
                                                                                                    
                                                            </asp:TextBox><asp:Button ID="btnClientCode" OnClientClick="javascript:openWin('RadClientCodePopup');return false;"
                                                                CssClass="browse-button" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lbClientCode" CssClass="input_no_bg" runat="server" Visible="true"></asp:Label>
                                                            <asp:Label ID="lbcvClientCode" CssClass="alert-text" runat="server" Visible="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
                <td width="40%" valign="top">
                    <fieldset>
                        <legend>Status</legend>
                        <table width="100%">
                            <tr>
                                <td class="tdLabel120">
                                    <asp:CheckBox ID="ChkWorksheet" Text="Worksheet" runat="server" />
                                </td>
                                <td class="tdLabel120">
                                    <asp:CheckBox ID="ChkTripsheet" Text="Tripsheet" runat="server" />
                                </td>
                                <td class="tdLabel120">
                                    <asp:CheckBox ID="ChUnFillFilled" Text="Unfulfilled" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel120">
                                    <asp:CheckBox ID="ChkCancelled" Text="Canceled" runat="server" />
                                </td>
                                <td class="tdLabel120">
                                    <asp:CheckBox ID="ChkHold" Text="Hold" runat="server" />
                                </td>
                                <td class="tdLabel120">
                                    <asp:CheckBox ID="ChkSchedServ" Text="Sched Serv" runat="server" Visible="false" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
                <td width="10%">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="RetrieveSearch_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <telerik:RadGrid ID="dgPreflightRetrieve" runat="server" AllowMultiRowSelection="false"
                        OnItemDataBound="dgPreflightRetrieve_ItemDataBound" AllowSorting="true" OnNeedDataSource="dgPreflightRetrieve_BindData"
                        OnItemCommand="dgPreflightRetrieve_ItemCommand" AutoGenerateColumns="false" PageSize="10" AllowFilteringByColumn="true"
                        AllowPaging="true" Width="880px" Height="330px" OnPreRender="dgPreflightRetrieve_PreRender">
                        <MasterTableView CommandItemDisplay="Bottom" DataKeyNames="TripID">
                            <Columns>
                                <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" DataTextField="TripSheet"
                                    Display="false" HeaderStyle-Width="100px" />
                                <telerik:GridBoundColumn DataField="TripNUM" HeaderText="Trip No." AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="EqualTo" AllowFiltering="true"
                                    HeaderStyle-Width="50px" FilterDelay="500" FilterControlWidth="35px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="EstDepartureDT" HeaderText="Departure Date"
                                    AllowFiltering="true" AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                                    HeaderStyle-Width="90px" FilterDelay="500" FilterControlWidth="70px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="RequestorFirstName" HeaderText="Requestor" AllowFiltering="true"
                                    AutoPostBackOnFilter="false" ShowFilterIcon="false" FilterDelay="500" CurrentFilterFunction="StartsWith"
                                    HeaderStyle-Width="80px" FilterControlWidth="60px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="TripDescription" HeaderText="Trip Description"
                                    ShowFilterIcon="false" FilterDelay="500" AutoPostBackOnFilter="false" CurrentFilterFunction="StartsWith"
                                    AllowFiltering="true" HeaderStyle-Width="150px" FilterControlWidth="130px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="TripStatus" HeaderText="Status" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" FilterDelay="500" CurrentFilterFunction="StartsWith" AllowFiltering="true"
                                    HeaderStyle-Width="60px" FilterControlWidth="50px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="RequestDT" HeaderText="Request Date" AutoPostBackOnFilter="false"
                                    AllowFiltering="true" ShowFilterIcon="false" FilterDelay="500" CurrentFilterFunction="EqualTo"
                                    HeaderStyle-Width="80px" FilterControlWidth="70px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="TailNum" HeaderText="Tail No." AutoPostBackOnFilter="false"
                                    AllowFiltering="true" ShowFilterIcon="false" FilterDelay="500" CurrentFilterFunction="StartsWith"
                                    HeaderStyle-Width="60px" FilterControlWidth="50px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="LogNum" HeaderText="Log" AutoPostBackOnFilter="false"
                                    AllowFiltering="true" ShowFilterIcon="false" FilterDelay="500" CurrentFilterFunction="EqualTo"
                                    HeaderStyle-Width="40px" FilterControlWidth="25px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Acknowledge" HeaderText="Acknowledge" AutoPostBackOnFilter="false"
                                    AllowFiltering="true" ShowFilterIcon="false" FilterDelay="500" CurrentFilterFunction="StartsWith"
                                    HeaderStyle-Width="100px" FilterControlWidth="90px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="WaitList" HeaderText="WaitList" AutoPostBackOnFilter="false"
                                    AllowFiltering="false" ShowFilterIcon="false" FilterDelay="500" CurrentFilterFunction="StartsWith"
                                     Display="false" HeaderStyle-Width="70px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="HomebaseCD" HeaderText="Home Base" AutoPostBackOnFilter="false"
                                    AllowFiltering="true" ShowFilterIcon="false" FilterDelay="500" CurrentFilterFunction="StartsWith"
                                    HeaderStyle-Width="80px" FilterControlWidth="70px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="TripID" HeaderText="TripID" Display="false" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="EqualTo" AllowFiltering="false"
                                    HeaderStyle-Width="100px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div class="grd_ok">
                                    <asp:Button ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                        CausesValidation="false" CssClass="button" Text="OK"></asp:Button>
                                    <asp:Button ID="btnOK" runat="server" ToolTip="OK" OnClientClick="javascript:return returnToParent();"
                                        CausesValidation="false" CssClass="button" Text="OK"></asp:Button>
                                    <asp:Button ID="btnSUB" runat="server" ToolTip="OK" OnClientClick="javascript:return returnToParent();"
                                        CausesValidation="false" CssClass="button" Text="OK"></asp:Button>
                                </div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="returnToParent" />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                    <asp:Label ID="InjectScript" runat="server"></asp:Label>
                    <%--<asp:Label ID="lbMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>--%>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <table id="tblHidden" style="display: none; position: relative">
                        <tr>
                            <td>
                                <asp:Button ID="btnYes" runat="server" Text="Button" OnClick="Yes_Click" />
                                <asp:Button ID="btnNo" runat="server" Text="Button" OnClick="No_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
