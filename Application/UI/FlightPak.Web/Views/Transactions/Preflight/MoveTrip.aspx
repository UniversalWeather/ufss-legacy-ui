﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MoveTrip.aspx.cs" Inherits="FlightPak.Web.Views.Settings.PreFlight.MoveTrip" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Move Trip Legs</title>
</head>
<body style="background-color:#fff">
    <form id="form1" runat="server">
    <div class="divGridPanel">
        <telerik:RadScriptManager ID="radScriptManager1" runat="server">
        </telerik:RadScriptManager>
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript" src="../../../Scripts/Common.js"></script>
            <script type="text/javascript">

                function alertCallBackFn(arg) {
                    document.getElementById("<%=tbDestinationTrip.ClientID%>").focus();
                }

                function alertInvalidTripFn(arg) {
                    document.getElementById("<%=tbDestinationTrip.ClientID%>").focus();
                }

                function alertNoTripFn(arg) {
                    document.getElementById("<%=btnDestinationTrip.ClientID%>").focus();
                }

                function openWin(radWin) {

                    var url = '';
                    url = '../../Transactions/Preflight/PreflightSearch.aspx?FromPage=Move';
                    if (url != "") {
                        oWindow = window.GetRadWindow();
                        openSizedWindowAllTrips(url, oWindow, 1135, 475, OnClientCloseTrip);
                    }
                }
                function openSizedWindowAllTrips(url, radwindow, w, h, CallbackClose) {
                    var parentWin = GetRadWindowPopup().BrowserWindow;
                    var parentRadWinManager = parentWin.GetRadWindowManager();
                    var oWndRad = parentRadWinManager.open(url, radwindow);
                    oWndRad.setSize(w, h);
                    try { oWndRad.add_pageLoad(OnPageLoadComplete); } catch (e) { }
                    try { oWndRad.add_close(CallbackClose); } catch (e) { }
                    oWndRad.set_modal(true);
                    oWndRad.set_visibleStatusbar(false);
                    oWndRad.set_behaviors(4);
                    oWndRad.center();
                    oWndRad.set_status('');
                }

                function GetRadWindow() {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow;
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                    return oWindow;
                }
                function CloseAndRebind(arg) {
                    GetRadWindow().Close();
                    GetRadWindow().BrowserWindow.location.reload();
                }

                function ConfirmClose(WinName) {
                    var oManager = GetRadWindowManager();
                    var oWnd = oManager.GetWindowByName(WinName);
                    //Find the Close button on the page and attach to the 
                    //onclick event
                    var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                    CloseButton.onclick = function () {
                        CurrentWinName = oWnd.Id;
                        //radconfirm is non-blocking, so you will need to provide a callback function
                        radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                    }
                }
                function CloseRadWindow(arg) {
                    GetRadWindow().Close();
                }

                function OnClientCloseTrip(oWnd, args) {

                    var combo = $find("<%= tbDestinationTrip.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();

                    if (arg !== null) {

                        if (arg) {
                            document.getElementById("<%=tbDestinationTrip.ClientID%>").value = arg.TripNUM;
                            var step = "DestinationTrip_TextChanged";
                            var ajaxManager = $find("<%= RadAjaxManager.ClientID %>");
                            if (step) {
                                ajaxManager.ajaxRequest(step);
                            }
                        }
                        else {
                            document.getElementById("<%=tbDestinationTrip.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }
                function GetDimensions(sender, args) {
                    var bounds = sender.getWindowBounds();
                    return;
                }
                

            </script>
        </telerik:RadCodeBlock>
        <telerik:RadAjaxManager ID="RadAjaxManager" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating"
            OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgDestination" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbDestinationTrip">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgDestination" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="bntInsert">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgDestination"  />
                        <telerik:AjaxUpdatedControl ControlID="tbDestinationTrip"  />
                        <telerik:AjaxUpdatedControl ControlID="btnDestinationTrip"  />
                        <telerik:AjaxUpdatedControl ControlID="chkNewTrip"  />
                        <telerik:AjaxUpdatedControl ControlID="dgViewSource" />
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1"  />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkNewTrip">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgDestination"  />
                        <telerik:AjaxUpdatedControl ControlID="btnDestinationTrip"  />
                        <telerik:AjaxUpdatedControl ControlID="tbDestinationTrip"  />
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1"  />
                        
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="bntAppend">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgDestination" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgViewSource" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="btnDestinationTrip" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="tbDestinationTrip" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="chkNewTrip" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnSave">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnCancel">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <%--<telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
            <DateInput ID="DateInput1" runat="server">
            </DateInput>
            <Calendar ID="Calendar1" runat="server">
                <SpecialDays>
                    <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a"
                        ItemStyle-BorderStyle="Solid">
                    </telerik:RadCalendarDay>
                </SpecialDays>
            </Calendar>
        </telerik:RadDatePicker>--%>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="RadRetrievePopup" runat="server" Width="960px" Height="520px"
                    OnClientResizeEnd="GetDimensions" AutoSize="false" OnClientClose="OnClientCloseTrip"
                    KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                    NavigateUrl="~/Views/Transactions/Preflight/PreflightSearch.aspx">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <div runat="server" id="DivExternalForm" class="ExternalForm">
            <table cellpadding="0" cellspacing="0" class="box1">
                <tr>
                    <td>
                        <table width="100%" class="movetrip_grid">
                            <tr>
                                
                                <td colspan="2">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td  class="tdLabel100" >
                                                <asp:Label ID="lbTrip" class="mnd_text" runat="server" Text="Source Trip No. : "></asp:Label>
                                            </td>
                                            <td class="tdLabel120">
                                                <asp:Label ID="lblTripno" runat="server"></asp:Label>
                                            </td>
                                            <td class="tdLabel60" >
                                                <asp:Label ID="lbTail" class="mnd_text" runat="server" Text="Tail No. : "></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblTailno" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <telerik:RadGrid ID="dgViewSource" runat="server" Skin="Office2007" AutoGenerateColumns="true"
                                        OnPageIndexChanged="ViewSource_PageIndexChanged" Width="100%" AllowMultiRowSelection="true"
                                        ShowHeader="true" ShowFooter="false" AllowPaging="false" AllowSorting="false"
                                        AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="false" ShowColumnHeaders="true">
                                        <MasterTableView AllowFilteringByColumn="false" CommandItemDisplay="Bottom" AllowPaging="false" DataKeyNames="LegNum">
                                            <Columns>
                                                <telerik:GridClientSelectColumn UniqueName="SelectionID" HeaderText="Select" />
                                                <telerik:GridBoundColumn DataField="LegNum" HeaderText="Leg" CurrentFilterFunction="Contains"
                                                    FilterDelay="4000" ShowFilterIcon="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="DepartAir" HeaderText="From" CurrentFilterFunction="Contains"
                                                    FilterDelay="4000" ShowFilterIcon="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="ArrivalAir" HeaderText="To" CurrentFilterFunction="Contains"
                                                    FilterDelay="4000" ShowFilterIcon="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="DepartDate" HeaderText="Depart Date" DataType="System.DateTime"
                                                    CurrentFilterFunction="Contains" FilterDelay="4000" ShowFilterIcon="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="ArrivalDate" HeaderText="Arrival Date" CurrentFilterFunction="Contains"
                                                    FilterDelay="4000" ShowFilterIcon="false">
                                                </telerik:GridBoundColumn>
                                            </Columns>
                                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                        </MasterTableView>
                                        <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                                            <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="True">
                                            </Scrolling>
                                            <Selecting AllowRowSelect="true" />
                                        </ClientSettings>
                                    </telerik:RadGrid>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_10">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td width="70%" align="left">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="tdLabel100">
                                                <span class="mnd_text">Destination Trip</span>
                                            </td>
                                            <td class="tdtext50">
                                                <asp:TextBox ID="tbDestinationTrip" runat="server" CssClass="tdtext50" MaxLength="25"
                                                    onKeyPress="return fnAllowNumeric(this, event)" AutoPostBack="true" OnTextChanged="DestinationTrip_TextChanged"></asp:TextBox>
                                            </td>
                                            <td align="left">
                                                <asp:Button runat="server" ID="btnDestinationTrip" OnClientClick="javascript:openWin('RadRetrievePopup');return false;" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkNewTrip" AutoPostBack="true" runat="server" Text="New Trip"
                                                    OnCheckedChanged="chkNewTrip_CheckedChanged" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="30%" align="right">
                                    <table cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td style="text-align: left">
                                                <asp:Button ID="bntInsert" Text="Insert" ToolTip="Trip legs will Insert Source Legs to Destination Legs" runat="server" CssClass="button" OnClick="btnInsert_Click" />
                                            </td>
                                            <td>
                                                <asp:Button ID="bntAppend" Text="Append" ToolTip="Trip legs will append Source Legs to Destination Legs" runat="server" CssClass="button" OnClick="btnAppend_Click" />
                                                <asp:HiddenField ID="hdnCallVal" runat="server" />
                                                <asp:HiddenField ID="hdnOldTripID" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_5">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%" class="movetrip_grid">
                            <tr>
                                <td>
                                    <telerik:RadGrid ID="dgDestination" runat="server" Skin="Office2007" AutoGenerateColumns="true"
                                        OnPageIndexChanged="Destination_PageIndexChanged" Width="100%" AllowPaging="false"
                                        AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="false" AllowSorting="false"
                                        ShowHeader="true" ShowFooter="false" ShowColumnHeaders="true">
                                        <MasterTableView AllowFilteringByColumn="false" CommandItemDisplay="Bottom" AllowPaging="false" DataKeyNames="LegID">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="LegNum" HeaderText="Leg" CurrentFilterFunction="Contains"
                                                    FilterDelay="4000" ShowFilterIcon="false">
                                                </telerik:GridBoundColumn>
                                                
                                                <telerik:GridBoundColumn DataField="DepartAir" HeaderText="From" CurrentFilterFunction="Contains"
                                                    FilterDelay="4000" ShowFilterIcon="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="ArrivalAir" HeaderText="To" CurrentFilterFunction="Contains"
                                                    FilterDelay="4000" ShowFilterIcon="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="DepartDate" HeaderText="Depart Date" DataType="System.DateTime"
                                                    CurrentFilterFunction="Contains" FilterDelay="4000" ShowFilterIcon="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="ArrivalDate" HeaderText="Arrival Date" CurrentFilterFunction="Contains"
                                                    FilterDelay="4000" ShowFilterIcon="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="LegID" HeaderText="Leg" CurrentFilterFunction="Contains"
                                                    Display="false" FilterDelay="4000" ShowFilterIcon="false">
                                                </telerik:GridBoundColumn>
                                            </Columns>
                                            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                        </MasterTableView>
                                        <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                                            <Scrolling AllowScroll="True" UseStaticHeaders="false" SaveScrollPosition="True">
                                            </Scrolling>
                                            <Selecting AllowRowSelect="true" />
                                        </ClientSettings>
                                    </telerik:RadGrid>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="tblspace_5">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="button" OnClick="btnSave_Click" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancel" Text="Cancel" runat="server" ValidationGroup="cancel"
                                        CssClass="button" OnClick="btnCancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
