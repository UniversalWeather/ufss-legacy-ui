﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaxInfoPopup.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.Preflight.PaxInfoPopup" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PAX Info</title>
    <link href="../../../Scripts/jquery.alerts.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jquery.alerts.js") %>"></script>
        <script type="text/javascript">

            function openWin(radWin) {

                var url = '';

                if (radWin == "rdPaxGroupPopup") {
                    url = '/Views/Transactions/Preflight/PaxGroupPopup.aspx?PassengerGroupCD=' + document.getElementById('<%=tbPaxGroup.ClientID%>').value;
                }
                var oWnd = radopen(url, radWin);
            }


            function OnClientCloseFlightDefaultPurposePopup(oWnd, args) {
                var combo = $find("<%= tbDefaultBlockedSeat.ClientID %>");
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=tbDefaultBlockedSeat.ClientID%>").value = arg.FlightPurposeCD;
                        document.getElementById("<%=cvDefaultBlockedSeat.ClientID%>").innerHTML = "";
                        var step = "DefaultBlockedSeat_TextChanged";
                        var ajaxManager = $find("<%= RadAjaxManager1.ClientID %>");
                        if (step) {
                            ajaxManager.ajaxRequest(step);
                        }
                    }
                    else {
                        document.getElementById("<%=tbDefaultBlockedSeat.ClientID%>").value = "";
                        combo.clearSelection();
                    }
                }
            }

            function ConfirmClose(WinName) {
                var oManager = GetRadWindowManager();
                var oWnd = oManager.GetWindowByName(WinName);
                //Find the Close button on the page and attach to the 
                //onclick event
                var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                CloseButton.onclick = function () {
                    CurrentWinName = oWnd.Id;
                    //radconfirm is non-blocking, so you will need to provide a callback function
                    radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                }
            }

            function OnClientPaxGroupClose(oWnd, args) {
                //get the transferred arguments
                var arg = args.get_argument();
                if (arg !== null) {
                    if (arg) {
                        document.getElementById("<%=hdnPaxGroupID.ClientID%>").value = arg.PassengerGroupID;
                        document.getElementById("<%=tbPaxGroup.ClientID%>").value = arg.PassengerGroupCD;
                        document.getElementById("<%=lbcvPaxGroup.ClientID%>").innerHTML = "";
                    }
                    else {
                        document.getElementById("<%=hdnPaxGroupID.ClientID%>").value = "";
                        document.getElementById("<%=tbPaxGroup.ClientID%>").value = "";
                        document.getElementById("<%=lbcvPaxGroup.ClientID%>").innerHTML = "";
                    }
                }
            }


            function GetDimensions(sender, args) {
                var bounds = sender.getWindowBounds();
                return;
            }
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }
        </script>
        <script type="text/javascript">
            //  var oArg = new Object();
            // var grid = $find("<%= dgPassengerCatalog.ClientID %>");
            //            //this function is used to close this window
            function CloseAndRebind(arg) {
                //alert(arg);
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.refreshGrid(arg);
            }

            //this function is used to set the frame element
            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz az well)
                return oWindow;
            }

            //this function is used to get the selected code
            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgPassengerCatalog.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "PassengerRequestorCD");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "PassengerName")
                    var cell3 = MasterTable.getCellByColumnUniqueName(row, "PassengerRequestorID")
                    var cell4 = MasterTable.getCellByColumnUniqueName(row, "PhoneNum")
                }

                if (selectedRows.length > 0) {
                    oArg.PassengerRequestorCD = cell1.innerHTML;
                    oArg.PassengerName = cell2.innerHTML;
                    oArg.PassengerRequestorID = cell3.innerHTML;
                    oArg.PhoneNum = cell4.innerHTML;
                }
                else {
                    oArg.PassengerRequestorCD = "";
                    oArg.PassengerName = "";
                    oArg.PassengerRequestorID = "";
                    oArg.PhoneNum = "";

                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }

            }

            function openWinPop(radWin) {
                var url = '';
                if (radWin == "RadFlightDefaultPurposePopup") {
                    url = '/Views/Settings/Fleet/FlightPurposePopUp.aspx?FDP=Yes&FlightPurposeCD=' + document.getElementById('<%=tbDefaultBlockedSeat.ClientID%>').value;
                }
                var oWnd = radopen(url, radWin);
            }

            function Close() {
                GetRadWindow().Close();
            }

            function RowDblClick() {
                var masterTable = $find("<%= dgPassengerCatalog.ClientID %>").get_masterTableView();
                masterTable.fireCommand("InitInsert", "");
                return false;
            }
            function rebindgrid() {
                var masterTable = $find("<%= dgPassengerCatalog.ClientID %>").get_masterTableView();
                masterTable.rebind();
            }
            function GetGridId() {
                return $find("<%= dgPassengerCatalog.ClientID %>");
            }  
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div id="divCompanyProfileCatalogForm" class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <asp:HiddenField ID="hdnPaxGroupID" runat="server" />
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="rdPaxGroupPopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientPaxGroupClose" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Transactions/Preflight/PaxGroupPopup.aspx">
                </telerik:RadWindow>
            </Windows>
            <Windows>
                <telerik:RadWindow ID="RadFlightDefaultPurposePopup" runat="server" OnClientResizeEnd="GetDimensions"
                    OnClientClose="OnClientCloseFlightDefaultPurposePopup" AutoSize="true" KeepInScreenBounds="true"
                    Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FlightPurposePopUp.aspx">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="divCompanyProfileCatalogForm">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divCompanyProfileCatalogForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divCompanyProfileCatalogForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <table cellpadding="0" cellspacing="0" class="box1">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="tdLabel70">
                                PAX Group
                            </td>
                            <td class="tdLabel180">
                                <asp:TextBox ID="tbPaxGroup" runat="server" CssClass="text60" MaxLength="4" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                    OnTextChanged="Filter_CheckedChanged">
                                </asp:TextBox>
                                <asp:Button ID="btnPaxGroup" OnClientClick="javascript:openWin('rdPaxGroupPopup');return false;"
                                    CssClass="browse-button" runat="server" />
                                <asp:Button ID="Search" runat="server" Text="Search" CssClass="button" OnClick="Filter_CheckedChanged" />
                            </td>
                            <td>
                                <asp:Panel ID="pnlFlightPurpose" runat="server">
                                    <fieldset>
                                        <legend>Passenger Flight Purpose Settings</legend>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chkFillAllLegs" runat="server" OnCheckedChanged="chkFillAllLegs_CheckedChanged"
                                                        Text="Fill Flight Purpose All Legs" AutoPostBack="true" />
                                                    <asp:HiddenField ID="hdnDefaultBlockedSeat" runat="server" />
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    Flight Purpose Override:
                                                </td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="tbDefaultBlockedSeat" CssClass="text40" AutoPostBack="true" OnTextChanged="DefaultBlockedSeat_TextChanged"
                                                                    MaxLength="2" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnDefaultBlockedSeat" runat="server" OnClientClick="javascript:openWinPop('RadFlightDefaultPurposePopup');return false;"
                                                                    CssClass="browse-button"></asp:Button>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                                <td align="right">
                                                    <asp:CustomValidator ID="cvDefaultBlockedSeat" runat="server" ControlToValidate="tbDefaultBlockedSeat"
                                                        ErrorMessage="Invalid Flight Purpose" Display="Dynamic" CssClass="alert-text"
                                                        ValidationGroup="Save"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </asp:Panel>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkCustomerID" runat="server" Text="Customer Only" AutoPostBack="true"
                                                OnCheckedChanged="chkCustomerID_OnCheckedChanged" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkActiveOnly" runat="server" Checked="false" Text="Active Only" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkRequestOnly" runat="server" Checked="false" Text="Requestor Only" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel70">
                            </td>
                            <td class="tdLabel100">
                            </td>
                            <td>
                                <asp:Label ID="lbcvPaxGroup" CssClass="alert-text" Visible="true" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadGrid ID="dgPassengerCatalog" runat="server" AllowMultiRowSelection="true"
                        AllowSorting="true" OnNeedDataSource="dgPassengerCatalog_BindData" OnItemCommand="dgPassengerCatalog_ItemCommand"
                        OnInsertCommand="dgPassengerCatalog_InsertCommand" AutoGenerateColumns="false"
                        Height="330px" PageSize="10" AllowPaging="true" Width="930px" OnDeleteCommand="dgPassengerCatalog_DeleteCommand"
                        OnPageIndexChanged="dgPassengerCatalog_PageIndexChanged" OnPreRender="dgPassengerCatalog_PreRender">
                        <MasterTableView DataKeyNames="PassengerRequestorID,PassengerRequestorCD,PassengerName,DepartmentID,PassengerDescription,PhoneNum,
                            HomebaseID,IsActive,IsRequestor,HomeBaseCD" CommandItemDisplay="Bottom">
                            <Columns>
                                <%--<telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
                                </telerik:GridClientSelectColumn>--%>

                                 <telerik:GridTemplateColumn AllowFiltering="false" UniqueName="CheckBoxTemplateColumn"
                                                HeaderStyle-Width="30px">
                                            <ItemTemplate>
                                              <asp:CheckBox ID="CheckBox1" runat="server" OnCheckedChanged="ToggleRowSelection"
                                                AutoPostBack="True" />
                                            </ItemTemplate>
                                            <HeaderTemplate>
                                              <asp:CheckBox ID="headerChkbox" runat="server" OnCheckedChanged="ToggleSelectedState"
                                                AutoPostBack="True" />
                                            </HeaderTemplate>
                                 </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="PassengerRequestorCD" HeaderText="Code" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="50px"
                                    FilterControlWidth="40px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PassengerName" HeaderText="Passenger/Requestor"
                                    HeaderStyle-Width="180px" FilterControlWidth="170px" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DepartmentCD" HeaderText="Department" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="100px"
                                    FilterControlWidth="80px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DepartmentName" HeaderText="Department Description"
                                    AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                                    HeaderStyle-Width="140px" FilterControlWidth="120px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PhoneNum" HeaderText="Phone" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="StartsWith" HeaderStyle-Width="120px"
                                    FilterControlWidth="100px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="HomeBaseCD" HeaderText="Home" CurrentFilterFunction="StartsWith"
                                    ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="60px"
                                    FilterControlWidth="80px" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn DataField="IsActive" HeaderText="Active" AutoPostBackOnFilter="true"
                                    AllowFiltering="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                                    HeaderStyle-Width="60px">
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridCheckBoxColumn DataField="IsRequestor" HeaderText="Requestor" AutoPostBackOnFilter="true"
                                    AllowFiltering="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                                    HeaderStyle-Width="60px">
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridBoundColumn DataField="PassengerRequestorID" HeaderText="Code" AutoPostBackOnFilter="false"
                                    HeaderStyle-Width="60px" FilterControlWidth="40px" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                                    Display="false" FilterDelay="500">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="HomebaseID" HeaderText="Home ID" AutoPostBackOnFilter="false"
                                    ShowFilterIcon="false" CurrentFilterFunction="EqualTo" Display="false" HeaderStyle-Width="100px"
                                    FilterDelay="500">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div class="grid_icon">
                                    <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                        CommandName="PerformInsert" Visible='<%# IsAuthorized(Permission.Database.AddPassengerRequestor)%>'></asp:LinkButton>
                                    <%-- --%>
                                    <asp:LinkButton ID="lnkInitEdit" runat="server" ToolTip="Edit" CssClass="edit-icon-grid"
                                        CommandName="Edit" Visible='<%# IsAuthorized(Permission.Database.EditPassengerRequestor)%>'
                                        OnClientClick="return ProcessUpdatePopup(GetGridId());"></asp:LinkButton>
                                    <%-- --%>
                                    <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="javascript:return ProcessDeletePopup('dgPassengerCatalog', 'radPaxInfoPopup');"
                                        CssClass="delete-icon-grid" ToolTip="Delete" CommandName="DeleteSelected" Visible='<%# IsAuthorized(Permission.Database.DeletePassengerRequestor)%>'></asp:LinkButton>
                                    <%-- CommandName="DeleteSelected" --%>
                                </div>
                                <div class="grd_ok">
                                    <%-- <asp:Button id="btnSubmit" OnClientClick="returnToParent(); return false;" runat="server" Text="OK" CssClass="button" /> --%>
                                    <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"
                                        CausesValidation="false" CssClass="button" Text="OK"></asp:LinkButton>
                                    <!--<button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                            Ok</button> -->
                                </div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings>
                            <ClientEvents OnRowDblClick="RowDblClick" />
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" UseClientSelectColumnOnly="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
        <asp:Label ID="lbMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    </form>
</body>
</html>
