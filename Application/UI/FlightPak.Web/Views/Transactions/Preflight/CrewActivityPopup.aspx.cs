﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.PreflightService;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Globalization;
using FlightPak.Web.ViewModels;
using System.Web;
using FlightPak.Web.Framework.Constants;
using Omu.ValueInjecter;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.Transactions.PreFlight
{
    public partial class CrewActivityPopup : BaseSecuredPage
    {
        private Int64 CrewID;
        public PreflightMain Trip = new PreflightMain();
        DateTime expiryDate;
        bool blNonLogTripSheet = false;
        private ExceptionManager exManager;
        string CrewFH;
        //private DateTime startDate = DateTime.Today;
        //private DateTime endDate = DateTime.Today.AddDays(30);
        //private DateTime furturestartDate = DateTime.Today;
        //private DateTime futureendDate = DateTime.Today.AddDays(7);  

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Request.QueryString["CrewID"] != null && Request.QueryString["NonLogTripSheet"] != null)
                        {

                            //hdcrewID.Value=Request.QueryString["CrewID"];
                            //CrewID = Convert.ToInt64(hdcrewID.Value);
                            CrewID = Convert.ToInt64(Request.QueryString["CrewID"]);
                            tbCrewMember.Text = Request.QueryString["CrewName"];
                            // hdNonLogTripSheet.Value =Request.QueryString["NonLogTripSheet"];
                            //blNonLogTripSheet = Convert.ToBoolean(hdNonLogTripSheet);
                            blNonLogTripSheet = Convert.ToBoolean(Request.QueryString["NonLogTripSheet"]);
                        }
                        if (!IsPostBack)
                        {
                            // Declare default format
                            string dateFormat = "MM/dd/yyyy";

                            if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                                dateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;

                            // Assign into Date Picker
                            RadDatePicker3.DateInput.DateFormat = dateFormat;
                            RadDatePicker4.DateInput.DateFormat = dateFormat;

                            RadDatePicker3.MaxDate = DateTime.Today;
                            RadDatePicker4.MinDate = DateTime.Today.Date;
                            PreflightTripViewModel tripVM = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                            if (tripVM != null)
                            {
                                Trip.InjectFrom(tripVM.PreflightMain);
                                expiryDate = DateTime.Now.AddDays(30);

                                if (Trip.EstDepartureDT != null)
                                {
                                    if (Trip.EstDepartureDT != DateTime.MinValue)
                                        expiryDate = ((DateTime)Trip.EstDepartureDT).AddMonths(1);
                                    tbFuture.Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0: " + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", expiryDate));

                                }


                                if (Trip.EstDepartureDT != null)
                                {
                                    if (Trip.EstDepartureDT != DateTime.MinValue)
                                        expiryDate = ((DateTime)Trip.EstDepartureDT).AddMonths(-1);
                                    tbHistory.Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0: " + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", expiryDate));
                                }



                                BindCrewActivity(CrewID, ((DateTime)(Trip.EstDepartureDT != null ? Trip.EstDepartureDT : DateTime.UtcNow)), blNonLogTripSheet, "F", Trip.TripNUM.HasValue ? Trip.TripNUM.Value : 0);
                                BindCrewActivityHistory(CrewID, ((DateTime)(Trip.EstDepartureDT != null ? Trip.EstDepartureDT : DateTime.UtcNow)), blNonLogTripSheet, "H", Trip.TripNUM.HasValue ? Trip.TripNUM.Value : 0);
                            }
                        }
                        

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                //catch (System.NullReferenceException ex) { }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }


        protected void btnFutureSearch_OnClick(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DateTime FromDate;
                        DateTime ToDate;
                        PreflightTripViewModel tripVM = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                        if (tripVM != null)
                        {
                            Trip.InjectFrom(tripVM.PreflightMain);
                            if (!string.IsNullOrEmpty(tbFuture.Text))
                            {
                                if (Trip.EstDepartureDT != null)
                                {
                                    if (Convert.ToDateTime(tbFuture.Text) > (Trip.EstDepartureDT))
                                    {
                                        FromDate = (DateTime)Trip.EstDepartureDT;
                                        ToDate = Convert.ToDateTime(tbFuture.Text);

                                        BindCrewActivityDateRange(CrewID, FromDate, ToDate, blNonLogTripSheet, "F", Trip.TripNUM.HasValue ? Trip.TripNUM.Value : 0);
                                    }
                                    else
                                    {
                                        FromDate = Convert.ToDateTime(tbFuture.Text);
                                        ToDate = (DateTime)Trip.EstDepartureDT;

                                        BindCrewActivityDateRange(CrewID, FromDate, ToDate, blNonLogTripSheet, "F", Trip.TripNUM.HasValue ? Trip.TripNUM.Value : 0);
                                    }
                                }
                            }
                            else
                            {
                                dgCrewActivity.DataSource = null;
                                dgCrewActivity.DataBind();
                                RadWindowManager1.RadAlert("Select the Date", 330, 100, "Crew Activity", "alertFuturedateCallFn", null);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }

        protected void btnSearchHistory_OnClick(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DateTime FromDate;
                        DateTime ToDate;
                        PreflightTripViewModel tripVM = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                        if (tripVM != null)
                        {
                            Trip.InjectFrom(tripVM.PreflightMain);
                            if (!string.IsNullOrEmpty(tbHistory.Text))
                            {

                                if (Trip.EstDepartureDT != null)
                                {
                                    if (Convert.ToDateTime(tbHistory.Text) > (Trip.EstDepartureDT))
                                    {
                                        FromDate = (DateTime)Trip.EstDepartureDT;
                                        ToDate = Convert.ToDateTime(tbHistory.Text);
                                        BindCrewActivityHistoryDateRange(CrewID, FromDate, ToDate, blNonLogTripSheet, "H", Trip.TripNUM.HasValue ? Trip.TripNUM.Value : 0);
                                    }
                                    else
                                    {
                                        FromDate = Convert.ToDateTime(tbHistory.Text);
                                        ToDate = (DateTime)Trip.EstDepartureDT;

                                        BindCrewActivityHistoryDateRange(CrewID, FromDate, ToDate, blNonLogTripSheet, "H", Trip.TripNUM.HasValue ? Trip.TripNUM.Value : 0);
                                    }
                                }
                            }
                            else
                            {
                                dgCrewHistoryActivity.DataSource = null;
                                dgCrewHistoryActivity.DataBind();
                                RadWindowManager1.RadAlert("Select the Date", 330, 100, "Crew Activity", "alertHistorydateCallFn", null);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }

        //protected void tbFuture_OnTextChanged(object sender, EventArgs e)
        //{
        //    BindCrewActivity(CrewID, ((DateTime)(string.IsNullOrEmpty(tbFuture.Text) ? Convert.ToDateTime(tbFuture.Text) : DateTime.UtcNow)), blNonLogTripSheet, "F");
        //}

        //protected void tbHistory_OnTextChanged(object sender, EventArgs e)
        //{
        //    BindCrewActivityHistory(CrewID, ((DateTime)(string.IsNullOrEmpty(tbHistory.Text) ? Convert.ToDateTime(tbHistory.Text) : DateTime.UtcNow)), blNonLogTripSheet, "H");
        //}


        //protected void CustomizeDay(object sender, Telerik.Web.UI.Calendar.DayRenderEventArgs e)
        //{
        //    DateTime CurrentDate = e.Day.Date;
        //    if (startDate <= CurrentDate && CurrentDate <= endDate)
        //    {
        //        return;
        //    }

        //    e.Day.IsDisabled = true;
        //    e.Day.IsSelectable = false;
        //    e.Day.IsSelected = false;
        //}


        //protected void CustomizefutureDay(object sender, Telerik.Web.UI.Calendar.DayRenderEventArgs e)
        //{
        //    DateTime CurrentDate = e.Day.Date;
        //    if (furturestartDate >= CurrentDate && endDate <= CurrentDate)
        //    {
        //        return;
        //    }

        //    e.Day.IsDisabled = true;
        //    e.Day.IsSelectable = false;
        //    e.Day.IsSelected = false;
        //}

        //protected override void OnPreRender(EventArgs e)
        //{
        //    base.OnPreRender(e);
        //}

        //protected void RadDatePicker1_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        //{

        //}

        //protected void RadDatePicker2_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        //{

        //}

        private void BindCrewActivity(Int64 crewID, DateTime DepartDate, bool blIsNonLogTripSheet, string CrewFH, long TripNUM)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewID, DepartDate, blIsNonLogTripSheet, CrewFH, TripNUM))
            {
                using (PreflightService.PreflightServiceClient objService = new PreflightService.PreflightServiceClient())
                {
                    CrewFH = "F";
                    var objRetVal = objService.GetCrewActivity(crewID, DepartDate, blIsNonLogTripSheet, CrewFH, TripNUM);

                    try
                    {
                        if (objRetVal.ReturnFlag == true)
                        {
                            dgCrewActivity.DataSource = objRetVal.EntityList;
                            dgCrewActivity.DataBind();
                        }

                        if (dgCrewActivity.Items.Count > 0)
                            dgCrewActivity.Items[0].Selected = true;
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }

        private void BindCrewActivityDateRange(Int64 crewID, DateTime FromDate, DateTime ToDate, bool blIsNonLogTripSheet, string CrewFH, long TripNUM)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewID, FromDate, ToDate, blIsNonLogTripSheet, CrewFH, TripNUM))
            {
                using (PreflightService.PreflightServiceClient objService = new PreflightService.PreflightServiceClient())
                {
                    CrewFH = "F";
                    var objRetVal = objService.GetCrewActivityDateRange(crewID, FromDate, ToDate, blIsNonLogTripSheet, CrewFH, TripNUM);

                    try
                    {
                        if (objRetVal.ReturnFlag == true)
                        {
                            dgCrewActivity.DataSource = objRetVal.EntityList;
                            dgCrewActivity.DataBind();
                        }

                        if (dgCrewActivity.Items.Count > 0)
                            dgCrewActivity.Items[0].Selected = true;
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }

        private void BindCrewActivityHistory(Int64 crewID, DateTime DepartDate, bool blIsNonLogTripSheet, string CrewFH, long TripNUM)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewID, DepartDate, blIsNonLogTripSheet, CrewFH, TripNUM))
            {
                using (PreflightService.PreflightServiceClient objService = new PreflightService.PreflightServiceClient())
                {
                    CrewFH = "H";
                    var objRetVal = objService.GetCrewActivity(crewID, DepartDate, blIsNonLogTripSheet, CrewFH, TripNUM);

                    try
                    {
                        if (objRetVal.ReturnFlag == true)
                        {
                            dgCrewHistoryActivity.DataSource = objRetVal.EntityList;
                            dgCrewHistoryActivity.DataBind();
                        }

                        if (dgCrewHistoryActivity.Items.Count > 0)
                            dgCrewHistoryActivity.Items[0].Selected = true;
                    }
                    catch (Exception ex)
                    {
                        //Manually handled
                    }
                }
            }
        }


        private void BindCrewActivityHistoryDateRange(Int64 crewID, DateTime FromDate, DateTime ToDate, bool blIsNonLogTripSheet, string CrewFH, long TripNUM)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewID, FromDate, ToDate, blIsNonLogTripSheet, CrewFH, TripNUM))
            {
                using (PreflightService.PreflightServiceClient objService = new PreflightService.PreflightServiceClient())
                {
                    CrewFH = "H";
                    var objRetVal = objService.GetCrewActivityDateRange(crewID, FromDate, ToDate, blIsNonLogTripSheet, CrewFH, TripNUM);

                    try
                    {
                        if (objRetVal.ReturnFlag == true)
                        {
                            dgCrewHistoryActivity.DataSource = objRetVal.EntityList;
                            dgCrewHistoryActivity.DataBind();
                        }

                        if (dgCrewHistoryActivity.Items.Count > 0)
                            dgCrewHistoryActivity.Items[0].Selected = true;
                    }
                    catch (Exception ex)
                    {
                        //Manually handled
                    }
                }
            }
        }



        protected void CrewActivity_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                PreflightTripViewModel tripVM = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                if (tripVM != null)
                {
                    Trip.InjectFrom(tripVM.PreflightMain);
                    CrewFH = "F";
                    if (!string.IsNullOrEmpty(tbFuture.Text))
                    {
                        expiryDate = Convert.ToDateTime(tbFuture.Text);
                        BindCrewActivity(CrewID, expiryDate, blNonLogTripSheet, CrewFH, Trip.TripNUM.HasValue? Trip.TripNUM.Value:0);
                    }
                }
            }
        }

        protected void CrewHistoryActivity_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                PreflightTripViewModel tripVM = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                if (tripVM != null)
                {
                    Trip.InjectFrom(tripVM.PreflightMain);
                    CrewFH = "H";
                    if (!string.IsNullOrEmpty(tbHistory.Text))
                    {
                        expiryDate = Convert.ToDateTime(tbHistory.Text);
                        BindCrewActivityHistory(CrewID, expiryDate, blNonLogTripSheet, CrewFH, Trip.TripNUM.HasValue ? Trip.TripNUM.Value : 0);
                    }
                }
            }
        }

        protected void CrewHistoryActivity_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridPagerItem)
                        {
                            GridPagerItem pager = (GridPagerItem)e.Item;
                            Label lbl = (Label)pager.FindControl("ChangePageSizeLabel");
                            lbl.Visible = false;

                            RadComboBox combo = (RadComboBox)pager.FindControl("PageSizeComboBox");
                            combo.Visible = false;
                        }
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem item = (GridDataItem)e.Item;

                            if (item["DtyPeriod"].Text != "&nbsp;")
                            {
                                DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "DtyPeriod");
                                if (date != null)
                                    item["DtyPeriod"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", date));
                            }
                            if (item["UtcDep"].Text != "&nbsp;")
                            {
                                DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "UtcDep");
                                if (date != null)
                                    item["UtcDep"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", date));
                            }
                            if (item["UtcArr"].Text != "&nbsp;")
                            {
                                DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "UtcArr");
                                if (date != null)
                                    item["UtcArr"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", date));
                            }

                            if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(item["FHrs"].Text)))
                            {
                                item["FHrs"].Text = FPKConversionHelper.ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((item["FHrs"].Text).ToString()), 3).ToString());
                                if (!(Convert.ToString(item["FHrs"].Text).Contains(":")))
                                {
                                    item["FHrs"].Text = item["FHrs"].Text + ":00";
                                }
                            }
                            else
                            {
                                item["FHrs"].Text = (System.Math.Round(Convert.ToDecimal((item["FHrs"].Text).ToString()), 1).ToString());
                            }

                            if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(item["DHrs"].Text)))
                            {
                                item["DHrs"].Text = FPKConversionHelper.ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((item["DHrs"].Text).ToString()), 3).ToString());
                                if (!(Convert.ToString(item["DHrs"].Text).Contains(":")))
                                {
                                    item["DHrs"].Text = item["DHrs"].Text + ":00";
                                }
                            }
                            else
                            {
                                item["DHrs"].Text = (System.Math.Round(Convert.ToDecimal((item["DHrs"].Text).ToString()), 1).ToString());
                            }

                            if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(item["RHrs"].Text)))
                            {
                                item["RHrs"].Text = FPKConversionHelper.ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((item["RHrs"].Text).ToString()), 3).ToString());
                                if (!(Convert.ToString(item["RHrs"].Text).Contains(":")))
                                {
                                    item["RHrs"].Text = item["RHrs"].Text + ":00";
                                }
                            }
                            else
                            {
                                item["RHrs"].Text = (System.Math.Round(Convert.ToDecimal((item["RHrs"].Text).ToString()), 1).ToString());
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                //catch (System.NullReferenceException ex) { }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }

        


        protected void CrewActivity_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridPagerItem)
                        {
                            GridPagerItem pager = (GridPagerItem)e.Item;
                            Label lbl = (Label)pager.FindControl("ChangePageSizeLabel");
                            lbl.Visible = false;

                            RadComboBox combo = (RadComboBox)pager.FindControl("PageSizeComboBox");
                            combo.Visible = false;
                        }
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem item = (GridDataItem)e.Item;

                            if (item["DtyPeriod"].Text != "&nbsp;")
                            {
                                DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "DtyPeriod");
                                if (date != null)
                                    item["DtyPeriod"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", date));
                            }
                            if (item["LocDep"].Text != "&nbsp;")
                            {
                                DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "LocDep");
                                if (date != null)
                                    item["LocDep"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", date));
                            }
                            if (item["LocArr"].Text != "&nbsp;")
                            {
                                DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "LocArr");
                                if (date != null)
                                    item["LocArr"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", date));
                            }
                            if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(item["FHrs"].Text)))
                            {
                                item["FHrs"].Text = FPKConversionHelper.ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((item["FHrs"].Text).ToString()), 3).ToString());
                                if (!(Convert.ToString(item["FHrs"].Text).Contains(":")))
                                {
                                    item["FHrs"].Text = item["FHrs"].Text + ":00";
                                }
                            }
                            else
                            {
                                item["FHrs"].Text = (System.Math.Round(Convert.ToDecimal((item["FHrs"].Text).ToString()), 1).ToString());
                            }

                            if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(item["DHrs"].Text)))
                            {
                                item["DHrs"].Text = FPKConversionHelper.ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((item["DHrs"].Text).ToString()), 3).ToString());
                                if (!(Convert.ToString(item["DHrs"].Text).Contains(":")))
                                {
                                    item["DHrs"].Text = item["DHrs"].Text + ":00";
                                }
                            }
                            else
                            {
                                item["DHrs"].Text = (System.Math.Round(Convert.ToDecimal((item["DHrs"].Text).ToString()), 1).ToString());
                            }

                            if ((UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2) && (!string.IsNullOrEmpty(item["RHrs"].Text)))
                            {
                                item["RHrs"].Text = FPKConversionHelper.ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((item["RHrs"].Text).ToString()), 3).ToString());
                                if (!(Convert.ToString(item["RHrs"].Text).Contains(":")))
                                {
                                    item["RHrs"].Text = item["RHrs"].Text + ":00";
                                }
                            }
                            else
                            {
                                item["RHrs"].Text = (System.Math.Round(Convert.ToDecimal((item["RHrs"].Text).ToString()), 1).ToString());
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightCrew);
                }
            }
        }
    }
}