﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using FlightPak.Common;
using FlightPak.Common.Constants;
using FlightPak.Web.PreflightService;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Telerik.Web.UI;

namespace FlightPak.Web.Views.Transactions.Preflight
{

    public partial class PreflightAPIS : BaseSecuredPage
    {
        private ExceptionManager exManager;
        public PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
        public PreflightService.PreflightMain PreflightTrip = new PreflightService.PreflightMain();
        public Int64 TripID = 0;
        public List<string> APISExceptions = new List<string>();

        public List<string> MainExceptions = new List<string>();
        public List<string> LegsExceptions = new List<string>();
        public List<string> CrewExceptions = new List<string>();
        public List<string> PAXExceptions = new List<string>();

        private string APIS;
        bool ValidTrip = true;
        StringBuilder sbAPISExceptions = new StringBuilder();

        protected void page_prerender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (dgPreflightRetrieveAPIS.SelectedItems.Count == 0)
                {
                    //  dgPreflightRetrieveAPIS.SelectedIndexes.Add(0);
                }
                if (hdHomeBase.Value.IndexOf(',') > 0)
                {
                    lbHomeBase.Text = System.Web.HttpUtility.HtmlEncode("(Multiple)");
                    lbHomeBase.CssClass = "";
                    lbHomeBase.ForeColor = System.Drawing.Color.Red;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (dgPreflightLeg.Items.Count == 0)
                    {
                        dgPreflightLeg.MasterTableView.NoMasterRecordsText = "No qualifying legs for APIS on the selected trip";
                        dgPreflightLeg.MasterTableView.NoDetailRecordsText = "No qualifying legs for APIS on the selected trip";
                    }

                    CheckAutorization(Permission.Preflight.ViewPreflightAPIS);

                    if (!IsAuthorized(Permission.Preflight.AddPreflightAPIS))
                    {
                        btnValidateFlightLegs.Visible = false;
                        btnSubmitUWA.Visible = false;
                        btnSubmitCBP.Visible = false;
                    }

                    if (UserPrincipal.Identity._clientId == null)
                        btnClientCode.Enabled = true;
                    else
                        btnClientCode.Enabled = false;
                    if (UserPrincipal.Identity._clientId != null)
                    {
                        tbSearchClientCode.Text = UserPrincipal.Identity._clientCd;
                        hdClientCodeID.Value = UserPrincipal.Identity._clientId.ToString();
                        FlightPakMasterService.Client client = new FlightPakMasterService.Client();
                        client = GetClient((long)UserPrincipal.Identity._clientId);
                        if (client != null)
                        {
                            lbClientCode.Text = System.Web.HttpUtility.HtmlEncode(client.ClientDescription);
                        }
                        tbSearchClientCode.ReadOnly = true;
                    }
                    else
                    {
                        tbSearchClientCode.ReadOnly = false;
                        //hdClientCodeID.Value = string.Empty;
                    }
                    Response.Expires = -1;
                    if (!IsPostBack)
                    {
                        if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            RadDatePicker1.DateInput.DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            RadDatePicker1.DateInput.DisplayDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            DatePicker.DateInput.DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            DatePicker.DateInput.DisplayDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                        }

                        if (UserPrincipal.Identity._fpSettings._IsDeactivateHomeBaseFilter == false)
                        {
                            chkHomebase.Checked = true;
                        }


                        DateTime estDepart = DateTime.Now;
                        if (UserPrincipal.Identity._fpSettings._TripsheetSearchBack != null)
                        {
                            estDepart = estDepart.AddDays(-((double)UserPrincipal.Identity._fpSettings._TripsheetSearchBack));
                        }
                        else
                        {
                            estDepart = estDepart.AddDays(-(30));
                        }
                        tbDepDate.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", estDepart);
                        DefaultSelection();
                    }

                    if ((Session["APISTripExceptions"] != null) && (rtPreflightAPIS.Tabs[1].Selected == true))
                    {
                        APISExceptions = (List<string>)Session["APISTripExceptions"];
                        BindExceptions(APISExceptions);
                        rtPreflightAPIS.Tabs[1].Selected = true;
                        RadPageView2.Visible = true;
                        RadPageView2.Selected = true;
                    }
                    else if (Session["CurrentPreFlightAPISTrip"] != null)
                    {
                        Trip = (PreflightMain)Session["CurrentPreFlightAPISTrip"];
                        tbAPISExceptions.Text = Trip.APISException;
                    }

                    if (Request.QueryString["APIS"] != null)
                    {
                        APIS = Request.QueryString["APIS"];
                    }


                }, FlightPak.Common.Constants.Policy.UILayer);

            }

        }

        protected void dgPreflightRetrieveAPIS_BindData(object sender, GridNeedDataSourceEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dgPreflightRetrieveAPIS.DataSource = DoSearchFilter();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightAPIS);
                }
            }

        }

        protected void dgPreflightRetrieveAPIS_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {

                        }
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            TableCell cell = (TableCell)Item["Ddate"];
                            TableCell cell1 = (TableCell)Item["Asubmit"];
                            TableCell cell2 = (TableCell)Item["Aexcp"];
                            if ((!string.IsNullOrEmpty(cell.Text)) && (cell.Text != "&nbsp;"))
                            {
                                cell.Text = System.Web.HttpUtility.HtmlEncode( String.Format("{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", Convert.ToDateTime(cell.Text)));
                            }
                            if ((!string.IsNullOrEmpty(cell1.Text)) && (cell1.Text != "&nbsp;"))
                            {
                                cell1.Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " H:mm" + "}", Convert.ToDateTime(cell1.Text)));
                            }
                            if ((!string.IsNullOrEmpty(cell2.Text)) && (cell2.Text != "&nbsp;"))
                            {
                                e.Item.ForeColor = System.Drawing.Color.Red;
                            }

                            if (!string.IsNullOrEmpty(APIS))
                            {
                                string TripNum = string.Empty;
                                if (Session["CurrentPreFlightTrip"] != null)
                                {
                                    PreflightTrip = (PreflightMain)Session["CurrentPreFlightTrip"];
                                }

                                if (PreflightTrip.TripNUM != null)
                                {
                                    TripNum = PreflightTrip.TripNUM.ToString();
                                }

                                if (Item["TripNUM"].Text.Trim() == TripNum)
                                {
                                    dgPreflightRetrieveAPIS.SelectedIndexes.Clear();
                                    Item.Selected = true;


                                    TripID = Convert.ToInt64(Item["TripID"].Text);
                                    Int64 convTripNum = 0;
                                    Int64 Tripnum = 0;
                                    if (Int64.TryParse(TripNum, out convTripNum))
                                        Tripnum = convTripNum;

                                    Session.Remove("CurrentPreFlightAPISTrip");
                                    _LoadTriptoSessionOnPageLoad(TripID, Tripnum);
                                    if (tbAPISExceptions.Text == "Validate Flight Legs Exception")
                                    {
                                        List<string> lstPrefLeg = new List<string>();
                                        GridDataItem sTrip = Item;
                                        string sTripID = sTrip.GetDataKeyValue("TripID").ToString();

                                        long CountryIDX = -1;
                                        using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objCountryVal = CountryService.GetCountryWithFilters("US",0).EntityList;
                                            if (objCountryVal.Count > 0)
                                                CountryIDX = ((FlightPakMasterService.Country)objCountryVal[0]).CountryID;

                                            CountryService.Close();

                                            List<PreflightLeg> Dt = new List<PreflightLeg>();
                                            //string[] FAR = Convert.ToString("121,125,135").Split(',');
                                            string[] FAR = Convert.ToString("91").Split(',');
                                            Trip = (PreflightMain)Session["CurrentPreFlightAPISTrip"];
                                            Dt = Trip.PreflightLegs.Where(u => ((u.Airport.CountryID == CountryIDX && u.Airport1.CountryID != CountryIDX) || (u.Airport.CountryID != CountryIDX && u.Airport1.CountryID == CountryIDX)) && u.IsDeleted == false && u.FedAviationRegNUM != null && FAR.Contains(Convert.ToString(u.FedAviationRegNUM.Trim()))).ToList();
                                            if (Dt.Count > 0)
                                            {
                                                foreach (PreflightLeg pl in Dt)
                                                {
                                                    lstPrefLeg.Add(pl.LegID.ToString());
                                                }
                                            }
                                            if (lstPrefLeg.Count > 0)
                                            {
                                                ValidateFlightLegs(sTripID, lstPrefLeg, false);
                                            }
                                        }
                                    }
                                    BindLegDataBind();

                                }

                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightAPIS);
                }
            }
        }

        protected void dgPreflightLeg_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                        }
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;
                            TableCell cell = (TableCell)Item["LDdate"];
                            TableCell cell1 = (TableCell)Item["LAdate"];
                            if ((!string.IsNullOrEmpty(cell.Text)) && (cell.Text != "&nbsp;"))
                            {
                                cell.Text =System.Web.HttpUtility.HtmlEncode( String.Format("{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " H:mm" + "}", Convert.ToDateTime(cell.Text)));
                            }
                            if ((!string.IsNullOrEmpty(cell1.Text)) && (cell1.Text != "&nbsp;"))
                            {
                                cell1.Text = System.Web.HttpUtility.HtmlEncode(String.Format("{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " H:mm" + "}", Convert.ToDateTime(cell1.Text)));
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightAPIS);
                }
            }
        }

        protected void tbSearchClientCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    RadAjaxManager.GetCurrent(Page).FocusControl(btnClientCode);
                    lbClientCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    lbcvClientCode.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    hdClientCodeID.Value = string.Empty;

                    if (!string.IsNullOrEmpty(tbSearchClientCode.Text))
                    {

                        List<FlightPak.Web.FlightPakMasterService.Client> ClientList = new List<FlightPakMasterService.Client>();
                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objDstsvc.GetClientWithFilters(0, tbSearchClientCode.Text.ToUpper().Trim(),false);
                            objDstsvc.Close();
                            if (objRetVal.ReturnFlag)
                            {
                                ClientList = objRetVal.EntityList;
                                if (ClientList != null && ClientList.Count > 0)
                                {
                                    lbClientCode.Text = System.Web.HttpUtility.HtmlEncode(ClientList[0].ClientDescription);
                                    hdClientCodeID.Value = ClientList[0].ClientID.ToString();
                                }
                                else
                                {
                                    lbcvClientCode.Text = System.Web.HttpUtility.HtmlEncode("Client Code Does Not Exist");
                                    hdClientCodeID.Value = "";
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbSearchClientCode);
                                }
                            }
                            else
                            {
                                lbcvClientCode.Text = System.Web.HttpUtility.HtmlEncode("Client Code Does Not Exist");
                                RadAjaxManager.GetCurrent(Page).FocusControl(tbSearchClientCode);
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void tbHomeBase_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (!string.IsNullOrEmpty(hdHomeBase.Value) && hdHomeBase.Value.IndexOf(",") > 0)
                        {
                            lbHomeBase.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        }
                        else
                        {
                            lbHomeBase.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            lbcvHomeBase.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            if (!string.IsNullOrEmpty(tbHomeBase.Text))
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objDstsvc.GetCompanyWithFilters(tbHomeBase.Text.ToUpper().Trim(),0,true,false);
                                    List<FlightPakMasterService.GetCompanyWithFilters> CompanyList = new List<FlightPakMasterService.GetCompanyWithFilters>();
                                    if (objRetVal.ReturnFlag)
                                    {

                                        CompanyList = objRetVal.EntityList;
                                        if (CompanyList != null && CompanyList.Count > 0)
                                        {
                                            lbHomeBase.Text = System.Web.HttpUtility.HtmlEncode(CompanyList[0].BaseDescription);
                                            hdHomeBase.Value = CompanyList[0].HomebaseID.ToString();
                                            tbHomeBase.Text = CompanyList[0].HomebaseCD;
                                            lbcvHomeBase.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        }
                                        else
                                        {
                                            lbcvHomeBase.Text = System.Web.HttpUtility.HtmlEncode("Homebase Code Does Not Exist");
                                            hdHomeBase.Value = string.Empty;
                                            RadAjaxManager.GetCurrent(Page).FocusControl(tbHomeBase);
                                        }
                                    }
                                    else
                                    {
                                        lbcvHomeBase.Text = System.Web.HttpUtility.HtmlEncode("Homebase Code Does Not Exist");
                                        hdHomeBase.Value = "";
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbHomeBase);
                                    }
                                }
                            }
                            else
                            {
                                lbHomeBase.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                hdHomeBase.Value = string.Empty;
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void RetrieveSearch_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    dgPreflightRetrieveAPIS.DataSource = DoSearchFilter();
                    dgPreflightRetrieveAPIS.DataBind();
                    if (dgPreflightRetrieveAPIS.Items.Count > 0)
                    {
                        //dgPreflightRetrieveAPIS.Items[0].Selected = true;
                        Session.Remove("CurrentPreFlightAPISTrip");
                        _LoadTriptoSession();
                        BindLegDataBind();
                    }
                    RadAjaxManager.GetCurrent(Page).FocusControl(dgPreflightRetrieveAPIS);

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        public RadDatePicker DatePicker { get { return this.RadDatePicker1; } }

        public List<View_PreflightMainList> DoSearchFilter()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Int64 ClientID = 0;
                Int64 homeBaseID = 0;
                string homebaseIDCSVStr = string.Empty;
                if (UserPrincipal != null)
                {
                    homeBaseID = (long)UserPrincipal.Identity._homeBaseId;
                }

                if (!string.IsNullOrEmpty(hdHomeBase.Value))
                {
                    homebaseIDCSVStr = hdHomeBase.Value;
                    chkHomebase.Checked = false;
                }
                else
                {

                }

                DateTime? estDepart;
                if (!string.IsNullOrEmpty(tbDepDate.Text))
                    estDepart = DateTime.ParseExact(tbDepDate.Text, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                else
                    estDepart = DateTime.MinValue;

                List<View_PreflightMainList> preFlightLists = new List<View_PreflightMainList>();

                if (!string.IsNullOrEmpty(hdClientCodeID.Value))
                {
                    ClientID = Convert.ToInt64(hdClientCodeID.Value);
                }
                else
                    ClientID = 0;

                using (PreflightService.PreflightServiceClient objService = new PreflightService.PreflightServiceClient())
                {
                    var ObjPrefmainlist = objService.GetAllPreflightMainList(chkHomebase.Checked ? homeBaseID : 0,
                        ClientID,
                    ChkTripsheet.Checked ? "T" : null,
                    ChkWorksheet.Checked ? "W" : null,
                    ChkHold.Checked ? "H" : null,
                    ChkCancelled.Checked ? "X" : null,
                    ChkSchedServ.Checked ? "S" : null,
                    ChUnFillFilled.Checked ? "U" : null,
                    ChkExcLog.Checked ? true : false,
                    (DateTime)estDepart, hdHomeBase.Value);

                    objService.Close();

                    if (ObjPrefmainlist.ReturnFlag)
                    {
                        if (chkSubmittedTrip.Checked)
                        {
                            var filterdata = ObjPrefmainlist.EntityList.Where(x => Convert.ToString(x.APISStatus) != "" && x.APISStatus != null).ToList();
                            preFlightLists = filterdata;
                        }
                        else
                            preFlightLists = ObjPrefmainlist.EntityList;
                    }
                    return preFlightLists;
                    if (hdHomeBase.Value.IndexOf(',') > 0)
                    {
                        lbHomeBase.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lblMultiHomebase.Visible = true;
                    }
                }
            }
        }

        private FlightPak.Web.FlightPakMasterService.Client GetClient(Int64 ClientID)
        {
            FlightPakMasterService.MasterCatalogServiceClient objDstsvc = null;
            FlightPak.Web.FlightPakMasterService.Client client = null;
            List<FlightPakMasterService.Client> ClientList = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        client = new FlightPakMasterService.Client();
                        var objRetVal = objDstsvc.GetClientWithFilters(ClientID,string.Empty,false);
                        ClientList = new List<FlightPakMasterService.Client>();

                        if (objRetVal.ReturnFlag)
                        {
                            ClientList = objRetVal.EntityList;
                            if (ClientList != null && ClientList.Count > 0)
                            {
                                client = ClientList[0];
                            }
                            else
                                client = null;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
            return client;
        }

        protected void dgPreflightRetrieveAPIS_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session.Remove("CurrentPreFlightAPISTrip");
                        _LoadTriptoSession();
                        if (tbAPISExceptions.Text == "Validate Flight Legs Exception")
                        {
                            List<string> lstPrefLeg = new List<string>();
                            GridDataItem sTrip = (GridDataItem)dgPreflightRetrieveAPIS.SelectedItems[0];
                            string sTripID = sTrip.GetDataKeyValue("TripID").ToString();

                            long CountryIDX = -1;
                            using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objCountryVal = CountryService.GetCountryWithFilters("US",0).EntityList.ToList();
                                if (objCountryVal.Count > 0)
                                    CountryIDX = ((FlightPakMasterService.Country)objCountryVal[0]).CountryID;

                                CountryService.Close();

                                List<PreflightLeg> Dt = new List<PreflightLeg>();
                                //string[] FAR = Convert.ToString("121,125,135").Split(',');
                                string[] FAR = Convert.ToString("91").Split(',');
                                Trip = (PreflightMain)Session["CurrentPreFlightAPISTrip"];
                                Dt = Trip.PreflightLegs.Where(u => ((u.Airport.CountryID == CountryIDX && u.Airport1.CountryID != CountryIDX) || (u.Airport.CountryID != CountryIDX && u.Airport1.CountryID == CountryIDX)) && u.IsDeleted == false && u.FedAviationRegNUM != null && FAR.Contains(Convert.ToString(u.FedAviationRegNUM.Trim()))).ToList();
                                if (Dt.Count > 0)
                                {
                                    foreach (PreflightLeg pl in Dt)
                                    {
                                        lstPrefLeg.Add(pl.LegID.ToString());
                                    }
                                }
                                if (lstPrefLeg.Count > 0)
                                {
                                    ValidateFlightLegs(sTripID, lstPrefLeg, false);
                                }
                            }
                        }
                        BindLegDataBind();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightAPIS);
                }
            }
        }

        private void DefaultSelection()
        {
            //using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            //{
            //    dgPreflightRetrieveAPIS.Rebind();
            //    if (dgPreflightRetrieveAPIS.MasterTableView.Items.Count > 0)
            //    {
            //        dgPreflightRetrieveAPIS.SelectedIndexes.Add(0);
            //        Session["CurrentPreFlightAPISTrip"] = dgPreflightRetrieveAPIS.Items[0];
            //        _LoadTriptoSession();
            //        BindLegDataBind();
            //    }
            //    else
            //    {
            //    }
            //}
        }

        protected void ValidateFlightLegs_Click(object sender, EventArgs e)
        {
            List<string> lstPrefLeg = null;
            GridDataItem sTrip = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    lstPrefLeg = new List<string>();
                    sTrip = (GridDataItem)dgPreflightRetrieveAPIS.SelectedItems[0];
                    string sTripID = sTrip.GetDataKeyValue("TripID").ToString();

                    foreach (GridDataItem PreLegItem in dgPreflightLeg.SelectedItems)
                    {
                        lstPrefLeg.Add(PreLegItem.GetDataKeyValue("LegID").ToString());
                    }
                    if (lstPrefLeg.Count > 0)
                    {
                        ValidateFlightLegs(sTripID, lstPrefLeg, true);
                    }
                    else
                    {
                        RadWindowManager1.RadAlert("Please select Flight Legs to be validated.", 360, 50, "FlightPak System", "alertCallBackFn", null);
                    }

                    RadAjaxManager.GetCurrent(Page).FocusControl(btnSubmitUWA);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void SubmitUWA_Click(object sender, EventArgs e)
        {
            List<string> lstPrefLeg = null;
            GridDataItem sTrip = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    lstPrefLeg = new List<string>();
                    sTrip = (GridDataItem)dgPreflightRetrieveAPIS.SelectedItems[0];
                    string sTripID = sTrip.GetDataKeyValue("TripID").ToString();
                    foreach (GridDataItem PreLegItem in dgPreflightLeg.SelectedItems)
                    {
                        lstPrefLeg.Add(PreLegItem.GetDataKeyValue("LegID").ToString());
                    }

                    if (lstPrefLeg.Count > 0)
                    {
                        //try
                        //{
                        using (var client = new PreflightServiceClient())
                        {

                            var ReturnValue = client.EAPISSubmit(Convert.ToInt64(sTripID), lstPrefLeg);
                            if (!ReturnValue.ReturnFlag)
                            {
                                if (ReturnValue.EntityInfo.Count > 0)
                                {
                                    Session["APISTripExceptions"] = ReturnValue.EntityInfo;
                                    BindExceptions(ReturnValue.EntityInfo);
                                    rtPreflightAPIS.Tabs[1].Selected = true;
                                    RadPageView2.Visible = true;
                                    RadPageView2.Selected = true;
                                }
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Trip submitted to eAPIS successfully.", 300, 200, "FlightPak System", "");
                            }

                            client.Close();
                        }
                        //}
                        //catch (Exception ex)
                        //{
                        //    //manually Handled
                        //    //RadWindowManager1.RadAlert("Unable to connect to EAPIS Service.", 300, 200, "FlightPak System", "");
                        //    RadWindowManager1.RadAlert("Unable to connect to eAPIS Service.", 360, 50, "FlightPak System", "alertUnableAPISFn", null);
                        //}
                    }
                    else
                    {
                        RadWindowManager1.RadAlert("Please select Flight Legs to be validated.", 360, 50, "FlightPak System", "alertCallBackFn", null);
                        //RadWindowManager1.RadAlert("Please select Flight Legs to be validated.", 400, 100, "FlightPak System", "");
                    }
                    RadAjaxManager.GetCurrent(Page).FocusControl(btnSubmitCBP);
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void SubmitCBP_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                RadAjaxManager.GetCurrent(Page).FocusControl(btnSubmitCBP);
            }
        }

        protected void Close_Click(object sender, EventArgs e)
        {
            //lbScript.Text = "<script type='text/javascript'>CloseRadWindow('CloseWindow');</script>";
        }

        protected void _LoadTriptoSessionOnPageLoad(long tripID, long tripNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tripID, tripNum))
            {

                TripID = tripID;

                Int64 Tripnum = 0;

                Tripnum = tripNum;

                if (Session["CurrentPreFlightAPISTrip"] != null)
                {
                    using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                    {
                        var objRetVal = PrefSvc.GetTrip(TripID);
                        if (objRetVal.ReturnFlag)
                        {
                            Trip = objRetVal.EntityList[0];
                            Trip.Mode = TripActionMode.NoChange;
                            Trip.State = TripEntityState.NoChange;
                            Session["CurrentPreFlightAPISTrip"] = Trip;
                            Trip.AllowTripToNavigate = true;
                            Trip.AllowTripToSave = true;
                        }
                    }
                }
                else
                {
                    using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                    {
                        var objRetVal = PrefSvc.GetTrip(TripID);
                        if (objRetVal.ReturnFlag)
                        {
                            Trip = objRetVal.EntityList[0];
                            Trip.Mode = TripActionMode.NoChange;
                            Trip.State = TripEntityState.NoChange;
                            Trip.AllowTripToNavigate = true;
                            Trip.AllowTripToSave = true;
                            Session["CurrentPreFlightAPISTrip"] = Trip;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(Trip.APISException))
                    tbAPISExceptions.Text = Trip.APISException;
                else
                {
                    tbAPISExceptions.Text = "";
                    Session.Remove("APISTripExceptions");
                }
            }
        }

        protected void _LoadTriptoSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                GridDataItem item = null;
                if (dgPreflightRetrieveAPIS.SelectedItems.Count > 0)
                {
                    item = (GridDataItem)dgPreflightRetrieveAPIS.SelectedItems[0];
                    TripID = Convert.ToInt64(item["TripID"].Text);
                    Int64 convTripNum = 0;
                    Int64 Tripnum = 0;
                    if (Int64.TryParse(item["TripNUM"].Text, out convTripNum))
                        Tripnum = convTripNum;

                    if (Session["CurrentPreFlightAPISTrip"] != null)
                    {
                        using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                        {
                            var objRetVal = PrefSvc.GetTrip(TripID);
                            if (objRetVal.ReturnFlag)
                            {
                                Trip = objRetVal.EntityList[0];
                                Trip.Mode = TripActionMode.NoChange;
                                Trip.State = TripEntityState.NoChange;
                                Session["CurrentPreFlightAPISTrip"] = Trip;
                                Trip.AllowTripToNavigate = true;
                                Trip.AllowTripToSave = true;
                            }
                        }
                    }
                    else
                    {
                        using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                        {
                            var objRetVal = PrefSvc.GetTrip(TripID);
                            if (objRetVal.ReturnFlag)
                            {
                                Trip = objRetVal.EntityList[0];
                                Trip.Mode = TripActionMode.NoChange;
                                Trip.State = TripEntityState.NoChange;
                                Trip.AllowTripToNavigate = true;
                                Trip.AllowTripToSave = true;
                                Session["CurrentPreFlightAPISTrip"] = Trip;
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(Trip.APISException))
                        tbAPISExceptions.Text = Trip.APISException;
                    else
                    {
                        tbAPISExceptions.Text = "";
                        Session.Remove("APISTripExceptions");
                    }
                }
            }
        }

        protected void BindLegDataBind()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                long CountryIDX = -1;
                using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objCountryVal = CountryService.GetCountryWithFilters("US",0).EntityList;
                    if (objCountryVal.Count > 0)
                    {
                        CountryIDX = ((FlightPakMasterService.Country)objCountryVal[0]).CountryID;
                        hdnCountryID.Value = CountryIDX.ToString();
                    }


                    List<PreflightLeg> Dt = new List<PreflightLeg>();
                    if (Session["CurrentPreFlightAPISTrip"] != null)
                    {
                        Trip = (PreflightMain)Session["CurrentPreFlightAPISTrip"];
                        //tbAPISExceptions.Text = Trip.APISException;
                        if (Trip.PreflightLegs != null)
                        {
                            Dt = Trip.PreflightLegs;
                            //string[] FAR = Convert.ToString("121,125,135").Split(',');
                            string[] FAR = Convert.ToString("91").Split(',');
                            dgPreflightLeg.DataSource = (from u in Dt
                                                         where (((u.Airport.CountryID == CountryIDX && u.Airport1.CountryID != CountryIDX) || (u.Airport.CountryID != CountryIDX && u.Airport1.CountryID == CountryIDX)) && u.IsDeleted == false && u.FedAviationRegNUM != null && FAR.Contains(Convert.ToString(u.FedAviationRegNUM.Trim()))) // not permanent code
                                                         select
                                                          new
                                                          {
                                                              LegID = u.LegID,
                                                              TripID = u.TripID,
                                                              LegNum = u.LegNUM,
                                                              DepartCountry = u.Airport.CountryID == null ? 0 : u.Airport1.CountryID,
                                                              ArrivalCountry = u.Airport1.CountryID == null ? 0 : u.Airport.CountryID,
                                                              DepartAir = u.Airport.IcaoID == null ? string.Empty : u.Airport1.IcaoID,
                                                              ArrivalAir = u.Airport1.IcaoID == null ? string.Empty : u.Airport.IcaoID,
                                                              DepartDate = u.DepartureDTTMLocal == null ? null : u.DepartureDTTMLocal,
                                                              ArrivalDate = u.ArrivalDTTMLocal == null ? null : u.ArrivalDTTMLocal,
                                                              ETE = u.ElapseTM != null ? u.ElapseTM : 0,
                                                              UWAID = u.UWAID == null ? string.Empty : u.UWAID,
                                                              CBPConfNumber = u.ConfirmID == null ? string.Empty : u.ConfirmID,
                                                              USABorder = u.USCrossing == null ? string.Empty : u.USCrossing,
                                                              DutyEnd = u.IsDutyEnd == null ? false : u.IsDutyEnd,
                                                              DutyErr = u.CrewDutyAlert == null ? string.Empty : u.CrewDutyAlert,
                                                              FlightCost = u.FlightCost,
                                                              USAFlightio = u.Airport.CountryID == CountryIDX ? "Inbound" : "Outbound",
                                                              FAR = u.FedAviationRegNUM == null ? string.Empty : u.FedAviationRegNUM
                                                          }).OrderBy(x => x.LegNum);
                            dgPreflightLeg.DataBind();
                        }
                    }
                    else
                    {
                        dgPreflightLeg.DataSource = Dt;
                        dgPreflightLeg.DataBind();
                    }
                }
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

        }

        protected void BindExceptions(List<string> APISExceptionList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(APISExceptionList))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (APISExceptionList.Count > 0)
                    {
                        sbAPISExceptions.Clear();
                        foreach (string str in APISExceptionList)
                        {
                            sbAPISExceptions.Append(Environment.NewLine);
                            sbAPISExceptions.Append(str);
                        }
                    }
                    tbAPISExceptions.Text = sbAPISExceptions.ToString();
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        protected void ValidateFlightLegs(string sTripID, List<string> lstPrefLeg, bool bindexception)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sTripID, lstPrefLeg, bindexception))
            {
                using (var client = new PreflightServiceClient())
                {
                    APISExceptions.Clear();
                    MainExceptions.Clear();
                    LegsExceptions.Clear();
                    CrewExceptions.Clear();
                    PAXExceptions.Clear();
                    var APISTrip = client.GetAPIS(Convert.ToInt64(sTripID));
                    if (APISTrip.ReturnFlag)
                    {
                        var prefMain = APISTrip.EntityList[0];
                        #region Main

                        if (prefMain.Fleet == null)
                        {
                            MainExceptions.Add("Fleet data is empty"); // added by Gavs  
                            ValidTrip = false;
                        }

                        #region Emergency Contact
                        if (prefMain.EmergencyContactID == null)
                        {
                            if (prefMain.Fleet != null)
                            {
                                MainExceptions.Add("Emergency Contact is empty for Tail " + Convert.ToString(prefMain.Fleet.TailNum));
                            }
                            ValidTrip = false;
                        }
                        else
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                if (prefMain.Fleet != null)
                                {
                                    var ObjRetVal = ObjService.GetAllEmergencyContactWithFilters((long)prefMain.Fleet.EmergencyContactID,string.Empty,false).EntityList;

                                    if (ObjRetVal.Count < 0)
                                    {
                                        MainExceptions.Add("Invalid Emergency Contact for Tail " + Convert.ToString(prefMain.Fleet.TailNum));
                                        ValidTrip = false;
                                    }

                                    if ((prefMain.EmergencyContact.FirstName == null) || (prefMain.EmergencyContact.FirstName == string.Empty))
                                    {
                                        MainExceptions.Add("Emergency Contact - First Name is empty for Tail " + Convert.ToString(prefMain.Fleet.TailNum));
                                        ValidTrip = false;
                                    }

                                    if ((prefMain.EmergencyContact.MiddleName == null) || (prefMain.EmergencyContact.MiddleName == string.Empty))
                                    {
                                        MainExceptions.Add("Emergency Contact - Middle Name is empty for Tail " + Convert.ToString(prefMain.Fleet.TailNum));
                                        ValidTrip = false;
                                    }
                                    if (string.IsNullOrEmpty(prefMain.EmergencyContact.LastName))
                                    {
                                        MainExceptions.Add("Emergency Contact - Last Name is empty for Tail " + Convert.ToString(prefMain.Fleet.TailNum));
                                        ValidTrip = false;
                                    }

                                    if ((prefMain.EmergencyContact.PhoneNum == null) || (prefMain.EmergencyContact.PhoneNum == string.Empty))
                                    {
                                        MainExceptions.Add("Emergency Contact - Phone is empty for Tail " + Convert.ToString(prefMain.Fleet.TailNum));
                                        ValidTrip = false;
                                    }
                                    if (string.IsNullOrEmpty(prefMain.EmergencyContact.FaxNum))
                                    {
                                        MainExceptions.Add("Emergency Contact - FaxNum is empty for Tail " + Convert.ToString(prefMain.Fleet.TailNum));
                                        ValidTrip = false;
                                    }

                                    if ((prefMain.EmergencyContact.EmailAddress == null) || (prefMain.EmergencyContact.EmailAddress == string.Empty))
                                    {
                                        MainExceptions.Add("Emergency Contact - E-mail is empty for Tail " + Convert.ToString(prefMain.Fleet.TailNum));
                                        ValidTrip = false;
                                    }

                                    if (string.IsNullOrEmpty(prefMain.Fleet.Notes))// newly added as per the APIS Submit Exceptions in UWA Manager.
                                    {
                                        MainExceptions.Add("Fleet Notes is empty for Trip # " + prefMain.TripNUM);
                                        ValidTrip = false;
                                    }
                                }
                            }
                        }
                        #endregion

                        #region Aircarft
                        if (prefMain.AircraftID == null)
                        {
                            MainExceptions.Add("Aircraft data is empty"); // added by Gavs
                            ValidTrip = false;
                        }
                        else
                        {
                            if (prefMain.Fleet != null)
                            {
                                if (string.IsNullOrEmpty(prefMain.Fleet.TailNum))
                                {
                                    MainExceptions.Add("Tail Number is empty for Trip # " + prefMain.TripNUM);
                                    ValidTrip = false;
                                }
                            }

                            if ((prefMain.Aircraft.AircraftDescription == null) || (prefMain.Aircraft.AircraftDescription == string.Empty))
                            {
                                MainExceptions.Add("Aircraft Type Description is empty");
                                ValidTrip = false;
                            }

                            if (prefMain.Aircraft.Fleets == null)
                            {
                                MainExceptions.Add("Aircraft Fleet data is empty"); // added by Gavs
                                ValidTrip = false;
                            }
                            else
                            {
                                Int64 Fleetid = Convert.ToInt64("0");
                                if (prefMain.FleetID != null)
                                    Fleetid = Convert.ToInt64(prefMain.FleetID);

                                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var fltpairlist = Service.GetFleetPair(Fleetid).EntityList;
                                    Service.Close();
                                    if (fltpairlist != null)
                                    {
                                        var fltpair = fltpairlist.SingleOrDefault();
                                        if (fltpair == null)
                                        {
                                            MainExceptions.Add("Aircraft Fleet data is empty"); // added by Gavs
                                            ValidTrip = false;
                                        }
                                        else
                                        {
                                            if ((fltpair.AircraftColor1 == string.Empty) || (fltpair.AircraftColor1 == null))
                                            {
                                                MainExceptions.Add("Aircraft Colors value is empty");
                                                ValidTrip = false;
                                            }
                                            if ((fltpair.BuyAircraftAdditionalFeeDOM == string.Empty) || (fltpair.BuyAircraftAdditionalFeeDOM == null))
                                            {
                                                MainExceptions.Add("Aircraft Decal Number is empty ");
                                                ValidTrip = false;
                                            }
                                            #region "Aircraft Operator Details"
                                            if (string.IsNullOrEmpty(fltpair.OwnerLesse))
                                            {
                                                MainExceptions.Add("Aircraft Operator/Company Name is empty");
                                                ValidTrip = false;
                                            }

                                            //if ((string.IsNullOrEmpty(fltpair.Company)) || ((string.IsNullOrEmpty(fltpair.OLastName)) && (string.IsNullOrEmpty(fltpair.OFirstName)) && (string.IsNullOrEmpty(fltpair.OMiddleName))))
                                            //{
                                            //    MainExceptions.Add("Aircraft Operator/Company Name is empty");
                                            //    ValidTrip = false;
                                            //}

                                            if (string.IsNullOrEmpty(fltpair.OLastName))
                                            {
                                                MainExceptions.Add("Aircraft Operator Last Name  is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.OFirstName))
                                            {
                                                MainExceptions.Add("Aircraft Operator First Name  is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.OMiddleName))
                                            {
                                                MainExceptions.Add("Aircraft Operator Middle Name  is empty");
                                                ValidTrip = false;
                                            }

                                            if (string.IsNullOrEmpty(fltpair.Address1))
                                            {
                                                MainExceptions.Add("Aircraft Operator Address Line 1  is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.Address2))
                                            {
                                                MainExceptions.Add("Aircraft Operator Address Line 2  is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.OCityName))
                                            {
                                                MainExceptions.Add("Aircraft Operator City  is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.OStateName))
                                            {
                                                MainExceptions.Add("Aircraft Operator State/Province  is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.ZipCD))
                                            {
                                                MainExceptions.Add("Aircraft Operator Postal Code is empty");
                                                ValidTrip = false;
                                            }
                                            if ((fltpair.OCountryID == null))
                                            {
                                                MainExceptions.Add("Aircraft Operator Country is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.OPhoneNum))
                                            {
                                                MainExceptions.Add("Aircraft Operator Phone is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.OFaxNum))
                                            {
                                                MainExceptions.Add("Aircraft Operator Fax is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.OEmailAddress))
                                            {
                                                MainExceptions.Add("Aircraft Operator E-mail is empty");
                                                ValidTrip = false;
                                            }

                                            #endregion

                                            #region "Aircraft Owner Details"

                                            if (string.IsNullOrEmpty(fltpair.OwnerLesse) || (string.IsNullOrEmpty(fltpair.PLastName) && string.IsNullOrEmpty(fltpair.PFirstName) && string.IsNullOrEmpty(fltpair.PMiddlename)))
                                            {
                                                MainExceptions.Add("Aircraft Owner/Lessee/Company Name is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.PLastName))
                                            {
                                                MainExceptions.Add("Aircraft Owner/Lessee Last Name is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.PFirstName))
                                            {
                                                MainExceptions.Add("Aircraft Owner/Lessee First Name is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.PMiddlename))
                                            {
                                                MainExceptions.Add("Aircraft Owner/Lessee Middle Name is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.Addr1))
                                            {
                                                MainExceptions.Add("Aircraft Owner/Lessee Address Line #  1 is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.Addr2))
                                            {
                                                MainExceptions.Add("Aircraft Owner/Lessee Address Line #  2 is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.CityName))
                                            {
                                                MainExceptions.Add("Aircraft Owner/Lessee CityName is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.StateName))
                                            {
                                                MainExceptions.Add("Aircraft Owner/Lessee StateName is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.PostalZipCD))
                                            {
                                                MainExceptions.Add("Aircraft Owner/Lessee Postal Code is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(Convert.ToString(fltpair.CountryID)))
                                            {
                                                MainExceptions.Add("Aircraft Owner/Lessee Country is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.PhoneNum))
                                            {
                                                MainExceptions.Add("Aircraft Owner/Lessee Phone Number is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.FaxNum))
                                            {
                                                MainExceptions.Add("Aircraft Owner/Lessee Fax Number is empty");
                                                ValidTrip = false;
                                            }
                                            if (string.IsNullOrEmpty(fltpair.EmailAddress))
                                            {
                                                MainExceptions.Add("Aircraft Owner/Lessee E-mail Address is empty");
                                                ValidTrip = false;
                                            }
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        MainExceptions.Add("Aircraft Fleet data is empty"); // added by Gavs
                                        ValidTrip = false;
                                    }
                                }
                            }
                        }
                        #endregion

                        #region Preflight Leg

                        if (prefMain.PreflightLegs != null)
                        {
                            foreach (PreflightLeg prefLeg in prefMain.PreflightLegs.OrderBy(x => x.LegNUM).ToList())
                            {
                                foreach (string strLegID in lstPrefLeg)
                                {
                                    if (prefLeg.LegID.ToString() == strLegID)
                                    {
                                        #region LEG ERROR SECTION
                                        #region "Leg Departure Details"
                                        if ((prefLeg.DepartICAOID == null) || (prefLeg.DepartICAOID.ToString() == string.Empty))
                                        {
                                            LegsExceptions.Add("Departure ICAO is empty for Leg " + prefLeg.LegNUM);
                                            ValidTrip = false;
                                        }
                                        if (string.IsNullOrEmpty(prefLeg.Airport1.CityName))
                                        {
                                            LegsExceptions.Add("Departure City is empty for Leg " + prefLeg.LegNUM);
                                            ValidTrip = false;
                                        }
                                        if (string.IsNullOrEmpty(prefLeg.Airport1.StateName))
                                        {
                                            LegsExceptions.Add("Departure State is empty for Leg " + prefLeg.LegNUM);
                                            ValidTrip = false;
                                        }
                                        if (prefLeg.Airport1.CountryID == null)
                                        {
                                            LegsExceptions.Add("Departure Country is empty for Leg " + prefLeg.LegNUM);
                                            ValidTrip = false;
                                        }
                                        if (prefLeg.DepartureDTTMLocal == null)
                                        {
                                            LegsExceptions.Add("Departure Local Date-Time is empty for Leg " + prefLeg.LegNUM);
                                            ValidTrip = false;
                                        }
                                        if ((prefLeg.DepartureGreenwichDTTM == null) || (prefLeg.DepartureGreenwichDTTM.ToString() == string.Empty))
                                        {
                                            LegsExceptions.Add("Departure UTC Date-Time is empty for Leg " + prefLeg.LegNUM);
                                            ValidTrip = false;
                                        }
                                        #endregion

                                        #region "Leg Arrival Details"
                                        if ((prefLeg.ArriveICAOID == null) || (prefLeg.ArriveICAOID.ToString() == string.Empty))
                                        {
                                            LegsExceptions.Add("Arrival ICAO is empty for Leg " + prefLeg.LegNUM);
                                            ValidTrip = false;
                                        }
                                        if (string.IsNullOrEmpty(prefLeg.Airport.CityName))
                                        {
                                            LegsExceptions.Add("Arrival City is empty for Leg " + prefLeg.LegNUM);
                                            ValidTrip = false;
                                        }
                                        if (string.IsNullOrEmpty(prefLeg.Airport.StateName))
                                        {
                                            LegsExceptions.Add("Arrival State is empty for Leg " + prefLeg.LegNUM);
                                            ValidTrip = false;
                                        }
                                        if (prefLeg.Airport.CountryID == null)
                                        {
                                            LegsExceptions.Add("Arrival Country is empty for Leg " + prefLeg.LegNUM);
                                            ValidTrip = false;
                                        }
                                        if ((prefLeg.ArrivalDTTMLocal == null) || (prefLeg.ArrivalDTTMLocal.ToString() == string.Empty))
                                        {
                                            LegsExceptions.Add("Arrival Local Date-Time is empty for Leg " + prefLeg.LegNUM);
                                            ValidTrip = false;
                                        }
                                        if ((prefLeg.ArrivalGreenwichDTTM == null) || (prefLeg.ArrivalGreenwichDTTM.ToString() == string.Empty))
                                        {
                                            LegsExceptions.Add("Arrival UTC Date-Time is empty for Leg " + prefLeg.LegNUM);
                                            ValidTrip = false;
                                        }

                                        if ((prefLeg.ArrivalDTTMLocal < prefLeg.DepartureDTTMLocal))
                                        {
                                            LegsExceptions.Add("Arrival Date-Time is earlier than Departure Date-Time for Leg " + prefLeg.LegNUM);
                                            ValidTrip = false;
                                        }

                                        if ((prefLeg.ArrivalDTTMLocal == prefLeg.DepartureDTTMLocal))
                                        {
                                            LegsExceptions.Add("Arrival Date-Time is same as Departure Date-Time for Leg " + prefLeg.LegNUM);
                                            ValidTrip = false;
                                        }

                                        #endregion

                                        if (string.IsNullOrEmpty(prefLeg.USCrossing))
                                        {
                                            LegsExceptions.Add("USA Border Crossing Info is empty for Leg " + prefLeg.LegNUM);
                                            ValidTrip = false;
                                        }
                                        #endregion

                                        #region Crew ERROR Section

                                        if ((prefLeg.PreflightCrewLists == null) || (prefLeg.PreflightCrewLists.Count == 0))
                                        {
                                            CrewExceptions.Add("Crew not available for Leg " + prefLeg.LegNUM); // added by Gavs
                                            ValidTrip = false;
                                        }
                                        else
                                        {
                                            #region "For Each Crew"
                                            foreach (PreflightCrewList prefCrewList in prefLeg.PreflightCrewLists)
                                            {
                                                List<string> ForeachCrewlist = new List<string>();
                                                if (prefCrewList.CrewID == null)
                                                {
                                                    ForeachCrewlist.Add("Crew Details are not available for Leg " + prefLeg.LegNUM); // added by Gavs
                                                    ValidTrip = false;
                                                }
                                                else
                                                {
                                                    #region "Crew Personal/Permanet Details"
                                                    using (FlightPakMasterService.MasterCatalogServiceClient Crewservice = new FlightPakMasterService.MasterCatalogServiceClient())
                                                    {
                                                        var crew = Crewservice.GetAllCrewWithFilters(0, false, false, false, string.Empty, 0, (long)prefCrewList.CrewID,string.Empty).EntityList.FirstOrDefault();
                                                        Crewservice.Close();
                                                        if (crew != null)
                                                        {
                                                            if (string.IsNullOrEmpty(Convert.ToString(crew.FirstName) + Convert.ToString(crew.LastName) + Convert.ToString(crew.MiddleInitial)))
                                                            {
                                                                ForeachCrewlist.Add("Crew Name is empty");
                                                                ValidTrip = false;
                                                            }
                                                            if (string.IsNullOrEmpty(crew.FirstName))
                                                            {
                                                                ForeachCrewlist.Add("First Name is empty");
                                                                ValidTrip = false;
                                                            }
                                                            if (string.IsNullOrEmpty(crew.LastName))
                                                            {
                                                                ForeachCrewlist.Add("Last Name is empty");
                                                                ValidTrip = false;
                                                            }
                                                            if (string.IsNullOrEmpty(crew.MiddleInitial))
                                                            {
                                                                ForeachCrewlist.Add("Middle Name is empty");
                                                                ValidTrip = false;
                                                            }
                                                            if (string.IsNullOrEmpty(crew.Gender))
                                                            {
                                                                ForeachCrewlist.Add("Gender is empty");
                                                                ValidTrip = false;
                                                            }
                                                            if (string.IsNullOrEmpty(crew.PhoneNum))
                                                            {
                                                                ForeachCrewlist.Add("Phone Number is empty");
                                                                ValidTrip = false;
                                                            }
                                                            if (string.IsNullOrEmpty(Convert.ToString(crew.Citizenship)))
                                                            {
                                                                ForeachCrewlist.Add("Citizenship Info is empty");
                                                                ValidTrip = false;
                                                            }
                                                            if ((crew.CountryID == null) || (crew.CountryID==0))
                                                            {
                                                                ForeachCrewlist.Add("Residence Country Info is empty");
                                                                ValidTrip = false;
                                                            }
                                                            if ((crew.BirthDT == null) || (crew.BirthDT.ToString() == string.Empty))
                                                            {
                                                                ForeachCrewlist.Add("Date of Birth is empty");
                                                                ValidTrip = false;
                                                            }
                                                            else
                                                            {
                                                                if (crew.BirthDT > System.DateTime.Now)
                                                                {
                                                                    ForeachCrewlist.Add("Date of Birth is a future date");
                                                                    ValidTrip = false;
                                                                }

                                                                if (Convert.ToDateTime(crew.BirthDT).Year < 1000)
                                                                {
                                                                    ForeachCrewlist.Add("Date of Birth is invalid");
                                                                    ValidTrip = false;
                                                                }
                                                            }
                                                            if (string.IsNullOrEmpty(crew.CityOfBirth))
                                                            {
                                                                ForeachCrewlist.Add("City of Birth is empty");
                                                                ValidTrip = false;
                                                            }
                                                            if (string.IsNullOrEmpty(crew.StateofBirth))
                                                            {
                                                                ForeachCrewlist.Add("State/Province of Birth is empty");
                                                                ValidTrip = false;
                                                            }
                                                            else
                                                            {
                                                                if (crew.StateofBirth.ToString().Count() != 2)
                                                                {
                                                                    ForeachCrewlist.Add("State/Province of Birth should be of characters 2");
                                                                    ValidTrip = false;
                                                                }
                                                            }
                                                            if (crew.CountryOfBirth == null)
                                                            {
                                                                ForeachCrewlist.Add("Country of Birth is empty");
                                                                ValidTrip = false;
                                                            }
                                                            if (string.IsNullOrEmpty(crew.Addr1))
                                                            {
                                                                ForeachCrewlist.Add("Permanent address Line # 1 is empty");
                                                                ValidTrip = false;
                                                            }
                                                            if (string.IsNullOrEmpty(crew.Addr2))
                                                            {
                                                                ForeachCrewlist.Add("Permanent address Line # 2 is empty");
                                                                ValidTrip = false;
                                                            }
                                                            if (string.IsNullOrEmpty(crew.CityName))
                                                            {
                                                                ForeachCrewlist.Add("Permanent Address City is empty");
                                                                ValidTrip = false;
                                                            }
                                                            if (string.IsNullOrEmpty(crew.StateName))
                                                            {
                                                                ForeachCrewlist.Add("Permanent Address State/Province is empty");
                                                                ValidTrip = false;
                                                            }
                                                            if (string.IsNullOrEmpty(crew.PostalZipCD))
                                                            {
                                                                ForeachCrewlist.Add("Permanent Address Postal Code is empty");
                                                                ValidTrip = false;
                                                            }
                                                            if (crew.CountryID == null)
                                                            {
                                                                ForeachCrewlist.Add("Permanent Address Country Info is empty");
                                                                ValidTrip = false;
                                                            }

                                                            #region "Crew Licence"

                                                            if (string.IsNullOrEmpty(crew.PilotLicense1))
                                                            {
                                                                ForeachCrewlist.Add("License is not available");
                                                                ValidTrip = false;
                                                            }

                                                            if ((crew.License1ExpiryDT == null) || (crew.License1ExpiryDT.ToString() == string.Empty))
                                                            {
                                                                ForeachCrewlist.Add("License Expiration Date is empty");
                                                                ValidTrip = false;
                                                            }
                                                            else
                                                            {
                                                                if ((crew.License1ExpiryDT <= prefLeg.ArrivalGreenwichDTTM))
                                                                {
                                                                    ForeachCrewlist.Add("License expires on/before Leg Arrival Date");
                                                                    ValidTrip = false;
                                                                }
                                                            }

                                                            if (string.IsNullOrEmpty(crew.LicenseCountry1))
                                                            {
                                                                ForeachCrewlist.Add("License - Issuing Country is empty");
                                                                ValidTrip = false;
                                                            }

                                                            #endregion
                                                        }
                                                        else
                                                        {
                                                            ForeachCrewlist.Add("Crew Details are not available for Leg " + prefLeg.LegNUM); // added by Gavs
                                                            ValidTrip = false;
                                                        }
                                                    }
                                                    #endregion

                                                    #region "Crew Address while in US"

                                                    if (string.IsNullOrEmpty(prefCrewList.Crew.Addr1))
                                                    {
                                                        ForeachCrewlist.Add("Address while in US Line # 1 is empty");
                                                        ValidTrip = false;
                                                    }
                                                    if (string.IsNullOrEmpty(prefCrewList.Crew.Addr2))
                                                    {
                                                        ForeachCrewlist.Add("Address while in US Line # 2 is empty");
                                                        ValidTrip = false;
                                                    }
                                                    if (string.IsNullOrEmpty(prefCrewList.Crew.CityName))
                                                    {
                                                        ForeachCrewlist.Add("Address while in US City Name is empty");
                                                        ValidTrip = false;
                                                    }
                                                    #endregion

                                                    #region Crew Passport
                                                    if (prefCrewList.PassportID != null)
                                                    {
                                                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                                        {
                                                            var Passport = objDstsvc.GetPassportbyPassportId(Convert.ToInt64(prefCrewList.PassportID), true).EntityList.First();
                                                            objDstsvc.Close();
                                                            if (Passport != null)
                                                            {
                                                                if (string.IsNullOrEmpty(Passport.PassportNum))
                                                                {
                                                                    ForeachCrewlist.Add("Passport Number is empty");
                                                                    ValidTrip = false;
                                                                }
                                                                if ((Passport.PassportExpiryDT == null) || (Passport.PassportExpiryDT.ToString() == string.Empty))
                                                                {
                                                                    ForeachCrewlist.Add("Passport Expiration Date is empty");
                                                                    ValidTrip = false;
                                                                }

                                                                if (Passport.CountryID == null)
                                                                {
                                                                    ForeachCrewlist.Add("Passport - Issuing Country is empty");
                                                                    ValidTrip = false;
                                                                }
                                                                if ((Passport.PassportExpiryDT <= prefLeg.ArrivalGreenwichDTTM))
                                                                {
                                                                    ForeachCrewlist.Add("Passport expires on/before Leg Arrival Date");
                                                                    ValidTrip = false;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ForeachCrewlist.Add("Passport Details are not available");
                                                                ValidTrip = false;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ForeachCrewlist.Add("Passport Details are not available");
                                                        ValidTrip = false;
                                                    }

                                                    #endregion
                                                }

                                                if (ForeachCrewlist != null && ForeachCrewlist.Count > 0)
                                                {
                                                    CrewExceptions.Add("Leg " + prefLeg.LegNUM);
                                                    CrewExceptions.Add("Duty Type: " + Convert.ToString(prefCrewList.DutyTYPE) + " - " + Convert.ToString(prefCrewList.Crew.CrewCD));
                                                    CrewExceptions.AddRange(ForeachCrewlist);
                                                }

                                            }


                                            #endregion
                                        }

                                        #endregion

                                        #region Passenger ERROR Section
                                        if ((prefLeg.PreflightPassengerLists == null) || (prefLeg.PreflightPassengerLists.Count == 0))
                                        {
                                            PAXExceptions.Add("Passenger not available for Leg " + prefLeg.LegNUM);
                                            ValidTrip = false;
                                        }
                                        else
                                        {

                                            #region For Each PAX
                                            foreach (PreflightPassengerList prefPassLst in prefLeg.PreflightPassengerLists)
                                            {
                                                List<string> ForeachPAXlist = new List<string>();
                                                if (prefPassLst.PassengerID == null)
                                                {
                                                    ForeachPAXlist.Add("Passenger Details are not available for Leg " + prefLeg.LegNUM);
                                                    ValidTrip = false;
                                                }
                                                else
                                                {
                                                    #region "Passenger Personal/Permanet Details"

                                                    using (FlightPakMasterService.MasterCatalogServiceClient Paxservice = new FlightPakMasterService.MasterCatalogServiceClient())
                                                    {
                                                        var pax = Paxservice.GetAllPassengerWithFilters(0, 0, (long)prefPassLst.PassengerID,string.Empty,false,false,string.Empty,0).EntityList.FirstOrDefault();
                                                        Paxservice.Close();

                                                        if (string.IsNullOrEmpty(pax.FirstName + pax.LastName + pax.MiddleInitial))
                                                        {
                                                            ForeachPAXlist.Add("Passenger Name is empty");
                                                            ValidTrip = false;
                                                        }
                                                        else
                                                        {
                                                            if (string.IsNullOrEmpty(pax.FirstName))
                                                            {
                                                                ForeachPAXlist.Add("First Name is empty");
                                                                ValidTrip = false;
                                                            }

                                                            if (string.IsNullOrEmpty(pax.LastName))
                                                            {
                                                                ForeachPAXlist.Add("Last Name is empty");
                                                                ValidTrip = false;
                                                            }

                                                            if (string.IsNullOrEmpty(pax.MiddleInitial))
                                                            {
                                                                ForeachPAXlist.Add("Middle Name is empty");
                                                                ValidTrip = false;
                                                            }
                                                        }

                                                        if (string.IsNullOrEmpty(pax.Gender))
                                                        {
                                                            ForeachPAXlist.Add("Gender is empty");
                                                            ValidTrip = false;
                                                        }
                                                        if (string.IsNullOrEmpty(pax.PhoneNum))
                                                        {
                                                            ForeachPAXlist.Add("Phone Number is empty");
                                                            ValidTrip = false;
                                                        }
                                                        if ((pax.DateOfBirth == null) || (pax.DateOfBirth.ToString() == string.Empty))
                                                        {
                                                            ForeachPAXlist.Add("Date of Birth is empty");
                                                            ValidTrip = false;
                                                        }
                                                        else
                                                        {
                                                            if (pax.DateOfBirth > System.DateTime.Now)
                                                            {
                                                                ForeachPAXlist.Add("Date of Birth is a future date");
                                                                ValidTrip = false;
                                                            }
                                                        }
                                                        if (pax.CountryID == null)
                                                        {
                                                            ForeachPAXlist.Add("Country of Citizenship is empty");
                                                            ForeachPAXlist.Add("Nationality is empty");
                                                            ValidTrip = false;
                                                        }
                                                        if (pax.CountryOfResidenceID == null)
                                                        {
                                                            ForeachPAXlist.Add("Country of Residence is empty");
                                                            ValidTrip = false;
                                                        }
                                                    }
                                                    #endregion

                                                    #region "Passenger Address While In US"
                                                    if (string.IsNullOrEmpty(prefPassLst.Street))
                                                    {
                                                        ForeachPAXlist.Add("Address While In US Line # 1 is empty");
                                                        ValidTrip = false;
                                                    }
                                                    if (string.IsNullOrEmpty(prefPassLst.CityName))
                                                    {
                                                        ForeachPAXlist.Add("Address While In US City is empty");
                                                        ValidTrip = false;
                                                    }
                                                    #endregion

                                                    #region Passenger Passport

                                                    //if ((prefPassLst.Passenger.CrewPassengerPassports == null) || (prefPassLst.Passenger.CrewPassengerPassports.ToString() == string.Empty) || (prefPassLst.Passenger.CrewPassengerPassports.Count == 0))
                                                    //{
                                                    //    ForeachPAXlist.Add("Passport Details are not available");
                                                    //    ValidTrip = false;
                                                    //}
                                                    //else
                                                    //{
                                                    if (prefPassLst.PassportID != null)
                                                    {
                                                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                                        {
                                                            var Passport = objDstsvc.GetPassportbyPassportId(Convert.ToInt64(prefPassLst.PassportID), false).EntityList.First();
                                                            objDstsvc.Close();
                                                            if (Passport != null)
                                                            {
                                                                if ((Passport.CountryID == null))
                                                                {
                                                                    ForeachPAXlist.Add("Passport - Issuing Country is empty");
                                                                    ValidTrip = false;
                                                                }

                                                                if (string.IsNullOrEmpty(Passport.PassportNum))
                                                                {
                                                                    ForeachPAXlist.Add("Passport Number is empty");
                                                                    ValidTrip = false;
                                                                }

                                                                if ((Passport.PassportExpiryDT == null) || (Passport.PassportExpiryDT.ToString() == string.Empty))
                                                                {
                                                                    ForeachPAXlist.Add("Passport Expiration Date is empty");
                                                                    ValidTrip = false;
                                                                }

                                                                if ((Passport.PassportExpiryDT <= prefLeg.ArrivalGreenwichDTTM))
                                                                {
                                                                    ForeachPAXlist.Add("Passport expires on/before Leg Arrival Date");
                                                                    ValidTrip = false;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ForeachPAXlist.Add("Passport Details are not available");
                                                                ValidTrip = false;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ForeachPAXlist.Add("Passport Details are not available");
                                                        ValidTrip = false;
                                                    }

                                                    //}

                                                    #endregion

                                                    #region "Passenger Visa Details"

                                                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvcVisa = new FlightPakMasterService.MasterCatalogServiceClient())
                                                    {
                                                        FlightPakMasterService.CrewPassengerVisa PassVisa = new FlightPakMasterService.CrewPassengerVisa();
                                                        PassVisa.CrewID = 0;
                                                        PassVisa.PassengerRequestorID = prefPassLst.PassengerID;
                                                        var Visalist = objDstsvcVisa.GetCrewVisaListInfo(PassVisa).EntityList;
                                                        objDstsvcVisa.Close();
                                                        if (Visalist != null)
                                                        {
                                                            var Visa = Visalist.SingleOrDefault();
                                                            if (Visa == null)
                                                            {
                                                                ForeachPAXlist.Add("Visa Details are empty");
                                                                ValidTrip = false;
                                                            }
                                                            else
                                                            {
                                                                if (string.IsNullOrEmpty(Visa.VisaNum))
                                                                {
                                                                    ForeachPAXlist.Add("Visa Number is empty");
                                                                    ValidTrip = false;
                                                                }
                                                                if ((Visa.ExpiryDT == null) || (Visa.ExpiryDT.ToString() == string.Empty))
                                                                {
                                                                    ForeachPAXlist.Add("Visa Expiration Date is empty");
                                                                    ValidTrip = false;
                                                                }
                                                                else
                                                                {
                                                                    if ((Visa.ExpiryDT <= prefLeg.ArrivalGreenwichDTTM))
                                                                    {
                                                                        ForeachPAXlist.Add("Visa expires on/before Leg Arrival Date");
                                                                        ValidTrip = false;
                                                                    }
                                                                }

                                                                if (string.IsNullOrEmpty(Visa.CountryCD))
                                                                {
                                                                    ForeachPAXlist.Add("Visa - Issuing Country is empty");
                                                                    ValidTrip = false;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            ForeachPAXlist.Add("Visa Details are empty");
                                                            ValidTrip = false;
                                                        }
                                                    }
                                                    #endregion
                                                }


                                                if (ForeachPAXlist != null && ForeachPAXlist.Count > 0)
                                                {
                                                    PAXExceptions.Add("Leg " + prefLeg.LegNUM);
                                                    PAXExceptions.Add("Pax: " + Convert.ToString(prefPassLst.Passenger.PassengerRequestorCD));
                                                    PAXExceptions.AddRange(ForeachPAXlist);
                                                }
                                            }

                                            #endregion
                                        }

                                        #endregion

                                    }
                                }
                            }
                        }
                        #endregion

                        #endregion

                        if (MainExceptions != null && MainExceptions.Count > 0)
                        {
                            APISExceptions.Add("MAIN ERROR SECTION:");
                            APISExceptions.AddRange(MainExceptions);
                        }

                        if (LegsExceptions != null && LegsExceptions.Count > 0)
                        {
                            if (APISExceptions != null && APISExceptions.Count > 0)
                                APISExceptions.Add("\r\nLEG ERROR SECTION:");
                            else
                                APISExceptions.Add("LEG ERROR SECTION:");
                            APISExceptions.AddRange(LegsExceptions);
                        }

                        if (CrewExceptions != null && CrewExceptions.Count > 0)
                        {
                            if (APISExceptions != null && APISExceptions.Count > 0)
                                APISExceptions.Add("\r\nCREW ERROR SECTION:");
                            else
                                APISExceptions.Add("CREW ERROR SECTION:");
                            APISExceptions.AddRange(CrewExceptions);
                        }

                        if (PAXExceptions != null && PAXExceptions.Count > 0)
                        {
                            if (APISExceptions != null && APISExceptions.Count > 0)
                                APISExceptions.Add("\r\nPAX ERROR SECTION:");
                            else
                                APISExceptions.Add("PAX ERROR SECTION:");
                            APISExceptions.AddRange(PAXExceptions);
                        }
                    }



                    if (!ValidTrip)
                    {
                        Session["APISTripExceptions"] = APISExceptions;
                        BindExceptions(APISExceptions);
                        if (bindexception)
                        {
                            client.UpdateAPISException(Convert.ToInt64(sTripID), "Validate Flight Legs Exception");
                            if (Session["CurrentPreFlightTrip"] != null)
                            {
                                PreflightService.PreflightMain ChkTrip = new PreflightService.PreflightMain();
                                ChkTrip = (PreflightMain)Session["CurrentPreFlightTrip"];
                                if (ChkTrip.TripID == Convert.ToInt64(sTripID))
                                {
                                    var retvalue = client.GetTrip(Convert.ToInt64(sTripID));
                                    if (retvalue.ReturnFlag)
                                    {
                                        ChkTrip = retvalue.EntityList[0];
                                        ChkTrip.Mode = TripActionMode.NoChange;
                                        ChkTrip.State = TripEntityState.NoChange;
                                        Session["CurrentPreFlightTrip"] = ChkTrip;
                                        ChkTrip.AllowTripToNavigate = true;
                                        ChkTrip.AllowTripToSave = true;
                                    }
                                }
                            }
                            rtPreflightAPIS.Tabs[1].Selected = true;
                            RadPageView2.Visible = true;
                            RadPageView2.Selected = true;
                        }
                    }
                }
            }
        }
    }
}