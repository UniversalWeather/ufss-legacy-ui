﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Preflight.Master"
    AutoEventWireup="true" Theme="Default" CodeBehind="PreFlightMain.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.PreFlight.PreFlightMain"
    ClientIDMode="AutoID" %>

<%@ MasterType VirtualPath="~/Framework/Masters/Preflight.Master" %>
<%@ Register Src="~/UserControls/UCPreflightFooterButton.ascx" TagName="Footer" TagPrefix="UCPreflight" %>
<asp:Content ID="Content2" ContentPlaceHolderID="SettingBodyContent" runat="server">
     <%=Scripts.Render("~/bundles/preflightMain") %>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="rdPreflightEMAIL" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                AutoSize="true" Width="780px" Height="650px" Title="E-mail Notification" NavigateUrl="~/Views/Transactions/Preflight/EmailTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCompanyMasterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"  OnClientAutoSizeEnd="OnClientAutoSizeEnd"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCompanyCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/CompanyMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdClientCodePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCodeClose" AutoSize="true" KeepInScreenBounds="true" Modal="true" CssClass="Clientcode_popup"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/ClientCodePopup.aspx" OnClientAutoSizeEnd="OnClientAutoSizeEnd">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFleetProfilePopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientTailNoClose" AutoSize="true" KeepInScreenBounds="true" CssClass="Fleetprofile_popup"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx" OnClientAutoSizeEnd="OnClientAutoSizeEnd">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radFleetProfileCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radDepartmentPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientDeptClose" AutoSize="true" KeepInScreenBounds="true" Modal="true" OnClientAutoSizeEnd="OnClientAutoSizeEnd"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radDepartmentCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientDeptClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/DepartmentAuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAuthorizationPopup" runat="server" OnClientResizeEnd="GetDimensions" Height="350px" Width="650px"
                OnClientClose="OnClientAuthClose" AutoSize="true" KeepInScreenBounds="true" Modal="true" OnClientAutoSizeEnd="OnClientAutoSizeEnd"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCQCPopups" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientCloseCharterQuotCustomerPopup" AutoSize="true" KeepInScreenBounds="true" OnClientAutoSizeEnd="OnClientAutoSizeEnd"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/CharterQuote/CharterQuoteCustomerPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCQCPopup" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Settings/Fleet/FleetProfilePopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAuthorizationCRUDPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientAuthClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AuthorizationPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAccountMasterPopup" runat="server" OnClientResizeEnd="GetDimensions" CssClass="Account_popup"
                OnClientClose="OnClientAccountClose" AutoSize="true" KeepInScreenBounds="true" OnClientAutoSizeEnd="OnClientAutoSizeEnd"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Company/AccountMasterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radAccountMasterCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdType" runat="server" OnClientResizeEnd="GetDimensions" OnClientClose="OnClientTypeClose"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false" OnClientAutoSizeEnd="OnClientAutoSizeEnd"
                NavigateUrl="~/Views/Settings/Fleet/AircraftPopup.aspx" >
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewRosterPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientReleasedClose" AutoSize="true" KeepInScreenBounds="true" OnClientAutoSizeEnd="OnClientAutoSizeEnd"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/CrewRosterPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radCrewRosterCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close" OnClientAutoSizeEnd="OnClientAutoSizeEnd"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdEmergencyContact" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientEmergencyContactClose" AutoSize="true" KeepInScreenBounds="true" OnClientAutoSizeEnd="OnClientAutoSizeEnd"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/EmergencyContactPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientNameClose" AutoSize="true" KeepInScreenBounds="true" Modal="true" OnClientAutoSizeEnd="OnClientAutoSizeEnd"
                Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/PassengerRequestorsPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="radPaxInfoCRUDPopup" runat="server" Height="800px" Width="1100px"
                ReloadOnShow="true" KeepInScreenBounds="true" Modal="true" Behaviors="close"
                VisibleStatusbar="false">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdDispatcher" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientDispatcherClose" AutoSize="true" KeepInScreenBounds="true" OnClientAutoSizeEnd="OnClientAutoSizeEnd"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/People/DispatcherPopup.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadRetrievePopup" runat="server" OnClientResizeEnd="GetDimensions" OnClientClose="OnTripSearchClose"
                AutoSize="true" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/PreflightSearch.aspx" OnClientAutoSizeEnd="OnClientAutoSizeEnd">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdSIFL" runat="server" OnClientResizeEnd="GetDimensions" AutoSize="true"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                NavigateUrl="~/Views/Transactions/Preflight/PreflightSIFL.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Width="400px" Height="236px" Title="Copy Trip" NavigateUrl="~/Views/Transactions/Preflight/CopyTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdAdvanceCopyTrip" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="true" Title="Copy Trip" KeepInScreenBounds="true" Modal="true" VisibleStatusbar="false"
                Behaviors="Close" NavigateUrl="~/Views/Transactions/Preflight/AdvancedTripCopy.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdMoveTrip" runat="server" OnClientResizeEnd="GetDimensions"
                AutoSize="false" KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Width="990px" Title="Move Trip" NavigateUrl="~/Views/Transactions/Preflight/MoveTrip.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdAirportPage" runat="server" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" AutoSize="true" Modal="true" ReloadOnShow="false" Behaviors="Close"
                VisibleStatusbar="false" Title="Airport Information">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdHistory" runat="server" AutoSize="true" OnClientResizeEnd="GetDimensions"
                KeepInScreenBounds="true" Modal="true" Behaviors="Close" VisibleStatusbar="false"
                Title="History" NavigateUrl="~/Views/Transactions/History.aspx">
            </telerik:RadWindow>
            <telerik:RadWindow ID="rdFleetChargeHistory" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientFleetChargeHistory" AutoSize="true" KeepInScreenBounds="true"
                Modal="true" Behaviors="Close" VisibleStatusbar="false" NavigateUrl="~/Views/Settings/Fleet/FleetChargeHistory.aspx">
            </telerik:RadWindow>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
                <div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <input type="hidden" ID="hdnclientID"/>
    <div id="DivExternalForm" runat="server" class="ExternalForm">
        <input type="hidden" ID="hdDepartDate" />
        <input type="hidden" ID="hdShowRequestor" />
        <input type="hidden" ID="hdHighlightTailno" />
        <input type="hidden" ID="hdIsDepartAuthReq" />
        <input type="hidden" ID="hdCalculateLeg" Value="false" />
        <input type="hidden" ID="hdIsAutomaticDispatchNum" />
        <asp:HiddenField runat="server" ID="hdnPreflightMainInitializationObject"/>
        <asp:HiddenField runat="server" ID="hdnUserPrincipalInitializationObject"/>
        <div style="width: 718px;">
            <asp:Panel runat="server" ID="pnlPreflight" Visible="true">
                <div style="float: left; width: 355px; padding: 5px 0 0 0;">
                    <fieldset>
                        <legend>Departure</legend>
                        <table cellspacing="0" cellpadding="2">
                            <tr>
                                <td class="tdLabel80">
                                    <label ID="lbHomeBaseIcao" data-bind="attr: { title: Preflight.PreflightMain.HomebaseTooltip }">Home Base</label>
                                </td>
                                <td class="tdLabel90" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <span class="departure_home_input">
                                                    <input style="width:48px;" type="text" ID="tbHomeBase" data-bind="value: Preflight.PreflightMain.HomeBaseAirportICAOID, enable: EditMode, event: { change: HomebaseValidator }" maxlength="4" class="text50" onKeyPress="return fnAllowAlphaNumeric(this, event)"
                                                            onBlur="return RemoveSpecialChars(this)"/>
                                                    <input type="hidden" data-bind="value: Preflight.PreflightMain.HomebaseID" ID="hdHomeBase" />
                                                    <input type="hidden" ID="hdOldHomeBase" />
                                                    <input type="hidden" data-bind="value: Preflight.PreflightMain.HomebaseAirportID" ID="hdHomeBaseAirportID" />
                                                </span>
                                            </td>
                                            <td>                                    
                                                <input class="search_but_icon" type="button" title="Search for Home Base" ID="btnHomeBase" data-bind="enable: EditMode, css: BrowseBtn" onclick="    javascript: openWin('radCompanyMasterPopup'); return false;" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel80">
                                    Client Code
                                </td>
                                <td class="tdLabel90" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <span class="departure_client_input">
                                                    <input style="width:48px;" type="text" ID="tbClientCode" data-bind="value: Preflight.PreflightMain.Client.ClientCD, enable: EditMode, event: { change: ClientCodeValidation }" maxlength="5" class="text50"/>
                                                    <input type="hidden" ID="hdClientCode" data-bind="value: Preflight.PreflightMain.Client.ClientID"/>
                                                </span>
                                            </td>
                                            <td>
                                                <input class="search_but_icon" type="button" title="View Clients"  ID="btnClientCode" data-bind="enable: EditMode, css: BrowseBtn" onclick="javascript: openWin('rdClientCodePopup'); return false;" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel170" colspan="2" valign="top">
                                    <label ID="lbHomeBase" data-bind="text: Preflight.PreflightMain.Company.BaseDescription, attr: { title: Preflight.PreflightMain.HomebaseTooltip }" class="input_no_bg"></label>
                                    <label ID="lbcvHomeBase" class="alert-text" Visible="true"></label>
                                </td>
                                <td valign="top" colspan="2">
                                    <label ID="lbClientCode" class="input_no_bg" data-bind="text: Preflight.PreflightMain.Client.ClientDescription" Visible="true"></label>
                                    <label ID="lbcvClientCode" class="alert-text"></label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel80">
                                    <span class="mnd_text">Depart Date</span>
                                </td>
                                <td class="tdLabel90">
                                    <input ID="tbDepartDate" data-bind="datepicker: Preflight.PreflightMain.ClientEstDepartureDT, enable: EditMode, event: { change: DepartDateValidation }" class="text80" maxlength="10" type="text" readonly/>
                                </td> 
                                <td class="tdLabel90" colspan="2">
                                    <label style="float: left"><input type="checkbox" data-bind="checked: Preflight.PreflightMain.IsPrivate, enable: EditMode" ID="chkPrivate" />Confidential</label>
                                <%--</td>--%>
                                <%--<td align="left">--%>
                                    <input type="button" ID="btnPrivateAllLegs" data-bind="enable: EditMode, css: CopyIcon, event: { click: confirmCopyPrivateToAllLeg }" title="Copy Private to all legs" style="float: left;margin: 3px 0 0 24px"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="tblspace_28">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div style="float: left; width: 360px; padding-top: 5px;">
                    <fieldset>
                        <legend>Aircraft</legend>
                        <table cellspacing="0" cellpadding="2" border="0">
                            <tr>
                                <td>
                                    <table border="0">
                                        <tr>
                                            <td class="tdLabel50">
                                                Tail No.
                                            </td>
                                            <td valign="top">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <input type="text" ID="tbTailNo" data-bind="value: Preflight.PreflightMain.Fleet.TailNum, enable: EditMode, event: { change: TailNumValidation}" class="text80" 
                                                                   maxlength="9"/>
                                                            <input type="hidden" ID="hdTailNo" data-bind="value: Preflight.PreflightMain.Fleet.FleetID" />
                                                            <input type="hidden" ID="hdOldTailNo"/>
                                                            <input type="hidden" ID="hdMaximumPassenger" data-bind="value: Preflight.PreflightMain.Fleet.MaximumPassenger" />
                                                        </td>
                                                        <td>
                                                            <input type="button" data-bind="enable: EditMode, css: BrowseBtn"   title="Search for Tail No." id="btnTailNo" onclick="javascript: openWin('radFleetProfilePopup'); return false;" />
                                                        </td>
                                                        <td>
                                                             <input type="button" ID="btnTailNoChargeHistory" data-bind="enable: EditMode,css: CharterRateBtn" onclick="javascript:openFleetChargeHistory();return false;" title="Fleet Charge History" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                            </td>
                                            <td class="tdLabel70">
                                                Flight No.
                                            </td>
                                            <td valign="top">
                                                <input type="text" ID="tbFlightNumber" data-bind="value: Preflight.PreflightMain.FlightNUM.Trimmed(), enable: EditMode" maxlength="12" class="text50"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdLabel170" colspan="5" valign="top">
                                                <label ID="lbcvTailNo" class="alert-text"></label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <table border="0">
                                        <tr>
                                            <td class="tdLabel50">
                                                <span class="mnd_text">Type</span>
                                            </td>
                                            <td valign="top">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <input type="text" ID="tbType" data-bind="value: Preflight.PreflightMain.Aircraft.AircraftCD, enable: EditMode, event: { change: AirTypeValidation }" class="text50" maxlength="10" />
                                                            <input type="hidden" ID="hdType" data-bind="value: Preflight.PreflightMain.Aircraft.AircraftID" />
                                                        </td>
                                                        <td>
                                                            <input type="button" data-bind="enable: EditMode, css: BrowseBtn" title="Search for Type Code" ID="btnType" onclick="javascript: openWin('rdType'); return false;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td class="tdLabel75">
                                                Released By
                                            </td>
                                            <td valign="top">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <input type="text" ID="tbReleasedby" data-bind="value: Preflight.PreflightMain.Crew.CrewCD, enable: EditMode, event: { change: CrewCodeValidator }" 
                                                                   class="text50" maxlength="5" onKeyPress="return fnAllowAlphaNumeric(this,event)"/>
                                                            <input type="hidden" ID="hdReleasedby" data-bind="value: Preflight.PreflightMain.Crew.CrewID" />
                                                        </td>
                                                        <td>
                                                            <input type="button" data-bind="enable: EditMode,css: BrowseBtn" title="View Requestors" ID="btnReleasedBy" onclick="javascript:openWin('radCrewRosterPopup');return false;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="tdLabel155" valign="top">
                                                <label ID="lbType" data-bind="text: Preflight.PreflightMain.Aircraft.AircraftDescription" class="input_no_bg"></label>
                                                <label ID="lbcvType" class="alert-text"></label>
                                                <label id="rfvType" style="display: none" class="alert-text">Type is Required.</label>
                                            </td>
                                            <td valign="top">
                                                <label ID="lbReleasedby" data-bind="text: Preflight.PreflightMain.Crew.ReleasedbyDesc" class="input_no_bg"></label>
                                                <label ID="lbcvReleasedby" class="alert-text"></label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="tdLabel120">
                                                Emergency Contact
                                            </td>
                                            <td class="tdLabel100" colspan="3" valign="top">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <input type="text" ID="tbEmergencyContact" data-bind="value: Preflight.PreflightMain.EmergencyContact.EmergencyContactCD, enable: EditMode, event: { change: ContactValidation }" class="text50" maxlength="5"
                                                                   onKeyPress="return fnAllowAlphaNumeric(this,event)"/>
                                                            <input type="hidden" ID="hdEmergencyContact" data-bind="value: Preflight.PreflightMain.EmergencyContact.EmergencyContactID" />
                                                        </td>
                                                        <td>
                                                             <input type="button" title="View Emergency Contacts" data-bind="enable: EditMode, css: BrowseBtn" ID="btnEmergencyContact" onclick="    javascript: openWin('rdEmergencyContact'); return false;" />
                                                        </td>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label ID="lbEmergencyContact" data-bind="text: Preflight.PreflightMain.EmergencyContact.EmergencyContactDesc" class="input_no_bg"></label>
                                    <label ID="lbcvEmergencyContact" class="alert-text"></label>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div style="float: left; width: 565px; padding: 5px 0 0 0px;">
                    <fieldset>
                        <legend>Requestor</legend>
                        <table cellspacing="0" style="height: 117px" cellpadding="2" width="100%">
                            <tr>
                                <td class="tdLabel90">
                                    Requestor
                                </td>
                                <td class="tdLabel120">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <input type="text" ID="tbRequestor" class="text80" data-bind="value: Preflight.PreflightMain.Passenger.PassengerRequestorCD, enable: EditMode, event: { change: RequestorValidator }" maxlength="5" onKeyPress="return fnAllowAlphaNumeric(this, event)"/>
                                                <input type="hidden" ID="hdRequestor" data-bind="value: Preflight.PreflightMain.Passenger.PassengerRequestorID" />
                                            </td>
                                            <td>
                                                <input type="button" title="View Requestors" data-bind="enable: EditMode, css: BrowseBtn" ID="btnName" onclick="    javascript: openWin('radPaxInfoPopup'); return false;" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel60" valign="top">
                                    <input type="button" ID="btnNameAllLegs" data-bind="enable: EditMode, css: CopyIcon, event: { click: confirmCopyRequestorToAllLeg }" title="Copy Requestor to all legs"/>
                                </td>
                                <td class="tdLabel90" valign="top">
                                    Phone
                                </td>
                                <td valign="top" colspan="2">
                                    <input type="text" ID="tbPhone" maxlength="25" data-bind="value: Preflight.PreflightMain.Passenger.AdditionalPhoneNum, enable: EditMode" class="text80" onKeyPress="return fnAllowAlphaNumeric(this, event)"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" valign="top">
                                    <label ID="lbRequestor" data-bind="text: Preflight.PreflightMain.Passenger.RequestorDesc" class="input_no_bg"></label>
                                    <label ID="lbcvRequestor" class="alert-text"></label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel90">
                                    Account No.
                                </td>
                                <td class="tdLabel120" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <input type="text" ID="tbAccountNo" data-bind="value: Preflight.PreflightMain.Account.AccountNum, enable: EditMode, event: { change: AccountValidator }" onkeypress="return fnAllowNumericAndChar(this, event,'.')"
                                                    onblur="return RemoveSpecialChars(this)" maxlength="32" class="text80"/>
                                                <input type="hidden" ID="hdAccountNo" data-bind="value: Preflight.PreflightMain.AccountID" />
                                            </td>
                                            <td>
                                                <input type="button" title="View Account Numbers" data-bind="enable: EditMode, css: BrowseBtn" ID="btnAccountNo" onclick="    javascript: openWin('radAccountMasterPopup'); return false;" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel60" valign="top">
                                    <input type="button" ID="btnAccountAllLegs" data-bind="enable: EditMode, css: CopyIcon, event: { click: confirmCopyAccountToAllLeg }" title="Copy Account No. to all legs"/>
                                </td>
                                <td class="tdLabel90">
                                    Request Date
                                </td>
                                <td colspan="2" valign="top">
                                    <input type="text" ID="tbRequestDate" class="text80" data-bind="datepicker: Preflight.PreflightMain.ClientRequestDT, enable: EditMode"
                                           onKeyPress="return fnAllowNumericAndChar(this, event,'/')" onchange="parseDate(this, event);SavePreflightMain(); javascript:return ValidateDate(this);"
                                           maxlength="10"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" valign="top">
                                    <label ID="lbAccountNo" data-bind="text: Preflight.PreflightMain.Account.AccountDescription" class="input_no_bg"></label>
                                    <label ID="lbcvAccountNo" class="alert-text"></label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel90">
                                    Department
                                </td>
                                <td class="tdLabel120" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <input type="text" ID="tbDepartment" data-bind="value: Preflight.PreflightMain.Department.DepartmentCD, enable: EditMode, event: { change: DepartmentValidator }"
                                                       class="text80" maxlength="8" onKeyPress="return fnAllowAlphaNumeric(this, event)"/>
                                                <input type="hidden" ID="hdDepartment" data-bind="value: Preflight.PreflightMain.Department.DepartmentID" />
                                            </td>
                                            <td>
                                                <input type="button" title="View Departments" data-bind="enable: EditMode, css: BrowseBtn" ID="btnDepartment" onclick="    javascript: openWin('radDepartmentPopup'); return false;" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel60">
                                    <input type="button" ID="btnDepartmentAllLegs" data-bind="enable: EditMode, css: CopyIcon, event: { click: confirmCopyDepartmentToAllLeg }" title="Copy Department to all legs" />
                                </td>
                                <td class="tdLabel90">
                                    Authorization
                                </td>
                                <td class="tdLabel120" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <input type="text" ID="tbAuthorization" data-bind="value: Preflight.PreflightMain.DepartmentAuthorization.AuthorizationCD, enable: EditMode, event: { change: AuthorizationValidator }" class="text80" maxlength="8"
                                                       onKeyPress="return fnAllowAlphaNumeric(this,event)"/>
                                                <input type="hidden" ID="hdAuthorization" data-bind="value: Preflight.PreflightMain.AuthorizationID" />
                                            </td>
                                            <td>
                                                <input type="button" title="View Authorizations" data-bind="enable: EditMode, css: BrowseBtn" ID="btnAuth" onclick=" return fnValidateAuth();" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top">
                                    <input type="button" ID="btnAuthorizationAllLegs" data-bind="enable: EditMode, css: CopyIcon, event: { click: confirmCopyAuthorizationToAllLeg }" title="Copy Authorization to all legs"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel270" colspan="3" valign="top">
                                    <label ID="lbDepartment" data-bind="text: Preflight.PreflightMain.Department.DepartmentName" class="input_no_bg"></label>
                                    <label ID="lbcvDepartment" class="alert-text"></label>
                                </td>
                                <td colspan="3" valign="top">
                                    <label ID="lbAuthorization" data-bind="text: Preflight.PreflightMain.DepartmentAuthorization.DeptAuthDescription" class="input_no_bg"></label>
                                    <label ID="lbcvAuthorization" class="alert-text"></label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel100">
                                    Charter Customer
                                </td>
                                <td class="tdLabel120" valign="top">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <input type="text" ID="tbCQCustomer" class="text80" data-bind="value: Preflight.PreflightMain.CQCustomer.CQCustomerCD, CQCustomerEnableDisable: EditMode, CQCheckFor: 'Main', event: { change: CQCustomerValidator }" maxlength="8" onKeyPress="return fnAllowAlphaNumeric(this, event)" />
                                                <input type="hidden" ID="hdCQCustomer" data-bind="value: Preflight.PreflightMain.CQCustomerID" />
                                            </td>
                                            <td>
                                                <input type="button" title="View CQCustomer" ID="btCQcustomerPopup" onclick="javascript: openWin('radCQCPopups'); return false;" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel60">
                                    <input type="button" ID="btCQCustomer_AllLeg s" data-bind="enable: EditMode, css: CopyIcon, event: { click: confirmCopyCQCustomerToAllLeg }" title="Copy Charter Customer to all legs"/>
                                </td>
                                <td class="tdLabel90">
                                </td>
                                <td class="tdLabel120" valign="top">
                                </td>
                                <td valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel270" colspan="6" valign="top">
                                    <label ID="lbCQCustomer" data-bind="text: Preflight.PreflightMain.CQCustomer.CQCustomerName" class="input_no_bg"></label>
                                    <label ID="lbcvCQCustomer" class="alert-text"></label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdLabel90" valign="top">
                                    Trip Purpose
                                </td>
                                <td class="tdLabel360" colspan="4" valign="top">
                                    <input type="text" ID="tbDescription" class="text362" data-bind="value: Preflight.PreflightMain.TripDescription, enable: EditMode" maxlength="40" />
                                </td>
                                <td valign="top">
                                    <input type="button" ID="btnDescriptionAllLegs" data-bind="enable: EditMode, css: CopyIcon, event: { click: confirmCopyTripPurposeToAllLeg }" title="Copy Trip Purpose to all legs" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div class="preflight_trip_status_wraper" style="float: left; width: 150px; padding: 5px 0 0 0px;">
                    <fieldset>
                        <legend>Trip Status</legend>
                        <label><input type="radio" id="radWorksheet" name="TripStatus" value="W" data-bind="checked: Preflight.PreflightMain.TripStatus, enable: EditMode" />Worksheet</label>
                        <label><input type="radio" id="radTripsheet" name="TripStatus" value="T" data-bind="checked: Preflight.PreflightMain.TripStatus, enable: EditMode" />Tripsheet</label>
                        <label><input type="radio" id="radUnfulfilled" name="TripStatus" value="U" data-bind="checked: Preflight.PreflightMain.TripStatus, enable: EditMode" />Unfulfilled</label>
                        <label><input type="radio" id="radCanceled" name="TripStatus" value="X" data-bind="checked: Preflight.PreflightMain.TripStatus, enable: EditMode" />Canceled</label>
                        <label><input type="radio" id="radHold" name="TripStatus" value="H" data-bind="checked: Preflight.PreflightMain.TripStatus, enable: EditMode" />Hold</label>
                    </fieldset>
                </div>
                <div style="float: left; width: 718px; padding: 5px 0 0 0px;">
                    <fieldset>
                        <legend>Flight Details</legend>
                        <table cellspacing="0" width="100%" cellpadding="0">
                            <tr>
                                <td class="tdLabel100">
                                    Dispatch No.
                                </td>
                                <td class="tdLabel100">
                                    <input type="text" ID="tbDispatchNumber" data-bind="value: Preflight.PreflightMain.DispatchNUM, enable: EditMode" class="text80" maxlength="12"/>
                                </td>
                                <td class="tdLabel70">
                                    Dispatcher
                                </td>
                                <td class="tdLabel150">
                                     <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <input type="text" ID="tbDispatcher" data-bind="value: Preflight.PreflightMain.DispatcherUserName, enable: EditMode, event: { change: DispatcherValidator }" class="text60"/>
                                            </td>
                                            <td>
                                                <input type="button" ToolTip="View Dispatchers" ID="btnDispatcher" data-bind="enable: EditMode, css: BrowseBtn" onclick="    javascript: openWin('rdDispatcher'); return false;" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tdLabel50">
                                    Cost
                                </td>
                                <td class="pr_radtextbox_85_readonly">
                                    <input type="text" id="tbCost" data-bind="formatCurrency: Preflight.PreflightMain.strFlightCost,enable:false" class="formatCurrency"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                </td>
                                <td colspan="4">
                                    <label ID="lbDispatcher" data-bind="text: Preflight.PreflightMain.UserMaster.DispatcherDesc" style="font-weight: bold" class="input_no_bg"></label>
                                    <label ID="lbcvDispatcher" class="alert-text"></label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                </td>
                                <td colspan="4">
                                    <label ID="lbPhoneNumber" data-bind="text: Preflight.PreflightMain.UserMaster.PhoneNum" style="font-weight: bold" class="input_no_bg"></label>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <fieldset style="margin-top: 6px;">
                        <legend>Notes</legend>
                        <div class="prefilight_notes_wrap">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="column_notes">
                                        Trip Alert
                                    </td>
                                    <td class="column_notes">
                                        Trip Notes
                                    </td>
                                    <td class="column_notes">
                                        Cancellation Description
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pr_main_radmask">
                                        <textarea ID="tbTripAlert" data-bind="value: Preflight.PreflightMain.Notes, enable: EditMode" title="Trip Alert"></textarea>
                                    </td>
                                    <td class="pr_main_radmask">
                                         <textarea ID="tbTripNotes" data-bind="value: Preflight.PreflightMain.TripSheetNotes, enable: EditMode" title="Trip Notes"></textarea>
                                    </td>
                                    <td class="pr_main_radmask">
                                         <textarea ID="tbCancellationdesc" data-bind="value: Preflight.PreflightMain.CancelDescription, enable: EditMode" title="Cancellation Description"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </div>
            </asp:Panel>
        </div>
        <div style="width: 718px; text-align: right; padding: 5px 0 0 0;">
            <table width="718px" cellspacing="6">
                <tr>
                    <td align="right" style="padding: 11px 0 7px;">
                        <UCPreflight:Footer ID="PreflightFooter" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
        <table id="tblHidden" style="display: none;">
            <tr>
                <td>
                    <input type="button" ID="btnCopyYes" value="Button" />
                    <input type="button" ID="btnCopyNo" value="Button" />
                    <input type="button" ID="btnDateYes" value="Button"/>
                    <input type="button" ID="btnDateNo" value="Button" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
