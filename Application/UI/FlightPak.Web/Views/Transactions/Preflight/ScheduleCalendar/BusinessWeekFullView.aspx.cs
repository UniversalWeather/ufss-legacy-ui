﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using System.Text;
using FlightPak.Web.PreflightService;
using System.Drawing;
using FlightPak.Web.UserControls;
using System.Collections.ObjectModel;
using FlightPak.Web.CommonService;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.UI.WebControls;
using FlightPak.Web.BusinessLite.Preflight;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar
{
    public partial class BusinessWeekFullView : ScheduleCalendarBase
    {
        private int BusinessFleetTreeCount { get; set; }
        private int BusinessCrewTreeCount { get; set; }
        bool IsDateCheck = true;
        private string SelectedFleetCrewIDs = string.Empty;  

        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadWindow3.VisibleOnPageLoad = false;
                        RadWindow4.VisibleOnPageLoad = false;
                        RadWindow5.VisibleOnPageLoad = false;
                        RadWindow6.VisibleOnPageLoad = false;
                        RadDatePicker1.DateInput.DateFormat = ApplicationDateFormat;
                        RadBusinessWeekScheduler.TimelineView.ColumnHeaderDateFormat = "ddd " + ApplicationDateFormat;

                        Session["SCAdvancedTab"] = CurrentDisplayOption.BusinessWeek;

                        ContentPlaceHolder contentPlaceHolder = (ContentPlaceHolder)Master.FindControl("MainContent");
                        UserControl ucSCMenu = (UserControl)contentPlaceHolder.FindControl("TabSchedulingCalendar");
                        ucSCMenu.Visible = false;
                        Master.FindControl("logo").Visible = false;
                        Master.FindControl("Combo").Visible = false;
                        Master.FindControl("menu").Visible = false;
                        Master.FindControl("globalNav").Visible = false;
                        Master.FindControl("footer").Visible = false;

                        // page load tasks...
                        if (!Page.IsPostBack)
                        {
                            // check / uncheck default view check box...
                            if (DefaultView == ModuleNameConstants.Preflight.BusinessWeek)
                            {
                                ChkDefaultView.Checked = true;
                            }
                            else
                            {
                                ChkDefaultView.Checked = false;
                            }
                            //To reduce service call - Vishwa
                            FilterCriteria.userSettingAvailable = base.scWrapper.userSettingAvailable;
                            FilterCriteria.retriveSystemDefaultSettings = base.scWrapper.retrieveSystemDefaultSettings;
                            //End
                            var selectedDay = DateTime.Today;
                            if (Session["SCSelectedDay"] != null)
                            {
                                selectedDay = (DateTime)Session["SCSelectedDay"];
                            }
                            else
                            {
                                selectedDay = DateTime.Today;
                            }

                            StartDate = selectedDay;
                            EndDate = GetBusinessWeekEndDate(StartDate);
                            Session["SCSelectedDay"] = StartDate;

                            tbDate.Text = selectedDay.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            RadDatePicker1.SelectedDate = selectedDay;

                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));


                            loadTree(false);

                            Session.Remove("SCBusinessWeekNextCount");
                            Session.Remove("SCBusinessWeekPreviousCount");
                            Session.Remove("SCBusinessWeekLastCount");
                            Session.Remove("SCBusinessWeekFirstCount");
                        }
                        else
                        {                            
                            CheckValidDate();
                        }
                        RadBusinessWeekScheduler.SelectedDate = StartDate;

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.BusinessWeek);
                }
            }

        }

           /// <summary>
        /// To Validate Date
        /// </summary>
        /// <returns></returns>
        protected bool CheckValidDate()
        {
            StartDate = (DateTime)Session["SCSelectedDay"];
            try
            {
                if (!string.IsNullOrEmpty(tbDate.Text))
                {
                    DateTime inputDate = Convert.ToDateTime(tbDate.Text.ToString(DateTimeInfo), DateTimeInfo);
                    if (inputDate != StartDate)
                    {
                        if (inputDate < MinDate || inputDate > MaxDate)
                        {
                            ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid date range: " + tbDate.Text + "', 275, 100,'System Messages'); return false;});", true);
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            return true;
                        }
                        DateInput = inputDate;
                        tbDate.TextChanged += tbDate_OnTextChanged;
                        return true;
                    }
                }
                else
                {
                    tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                    return true;
                }
            }
            catch (FormatException)
            { // manually handled
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "radalert", "Sys.Application.add_load(function(){radalert('Invalid date format: " + tbDate.Text + "', 275, 100,'System Messages'); return false;});", true);
                tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                return true;
            }
            EndDate = GetBusinessWeekEndDate(StartDate);

            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
            //RadDatePicker1.SelectedDate = StartDate;

            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
            if (FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)
            {
                return true;
            }
            else
            {
                // avoid duplicate calls for loadTree();
                string id = Page.Request.Params["__EVENTTARGET"];

                if (id == null || (id != string.Empty && id.Contains("hdnPostBack"))) //post back event is triggered on close of entry popups / refresh icon.
                {
                    loadTree(false);
                }

            }
            return IsDateCheck;
        }
        protected void btnApply_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadPanelBar2.FindItemByText("Fleet").Expanded = false;
                        RadPanelBar2.FindItemByText("Crew").Expanded = false;
                        RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;
                        if (FilterCriteria.ValidateValue())
                        {
                            // get settings from database and update filterSettings option to user settings... and then save
                            var filterAndDisplaySettings = (AdvancedFilterSettings)Session["SCUserSettings"];

                            var serializedXml = XMLSerializationHelper.Serialize<AdvancedFilterSettings>(filterAndDisplaySettings);
                            var userSettingsString = FilterCriteria.GetFilterSettings(CurrentAdvancedFilterTab.FilterCriteria, serializedXml);

                            var options = XMLSerializationHelper.Deserialize<AdvancedFilterSettings>(userSettingsString);
                            FilterOptions = options.Filters;
                            Session["SCUserSettings"] = options;


                            loadTree(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.BusinessWeek);
                }
            }
        }

        protected void btnToday_Click(Object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadDatePicker1.SelectedDate = DateTime.Today;
                        tbDate.Text = DateTime.Today.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                        StartDate = DateTime.Today;
                        EndDate = GetBusinessWeekEndDate(StartDate);
                        Session["SCSelectedDay"] = StartDate;
                        RadBusinessWeekScheduler.SelectedDate = StartDate;
                        loadTree(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.BusinessWeek);
                }
            }
        }

        protected void tbDate_OnTextChanged(Object sender, EventArgs e)
        {
            //check if it is validate date
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsDateCheck && DateInput >= MinDate && DateInput <= MaxDate)
                        {
                            StartDate = DateInput;
                            EndDate = GetBusinessWeekEndDate(StartDate);
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            RadBusinessWeekScheduler.SelectedDate = StartDate;
                            loadTree(false);
                        }                        

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.BusinessWeek);
                }
            }
        }

        // This event is used to bind back color and folre color for an appointment (trip) based on its flight category code..
        protected void RadBusinessWeekScheduler_AppointmentDataBound(object sender, Telerik.Web.UI.SchedulerEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Web.Entities.SchedulingCalendar.Appointment item = (FlightPak.Web.Entities.SchedulingCalendar.Appointment)e.Appointment.DataItem;
                        e.Appointment.Resources.Add(new Resource("Leg", "LegNum", item.LegNum.ToString()));
                        e.Appointment.Resources.Add(new Resource("StartDate", "StartDate", e.Appointment.Start.ToString()));
                        //Rendition based on red-eye option - if red eye option is not selected, shorten the end date to the start date of the appointment...
                        if (DisplayOptions.BusinessWeek != null && !DisplayOptions.BusinessWeek.DisplayRedEyeFlightsInContinuation && item.RecordType != "C")
                        {
                            if (item.ArrivalDisplayTime.Date != item.DepartureDisplayTime.Date)
                            {
                                e.Appointment.End = item.StartDate;
                            }
                        }

                        // if black and white color not selected - default is off – can only be selected if Aircraft color is not selected; 

                        if (DisplayOptions.BusinessWeek != null)
                        {
                            if (DisplayOptions.BusinessWeek.BlackWhiteColor)
                            {
                                e.Appointment.BackColor = Color.White;
                                e.Appointment.ForeColor = Color.Black;
                            }
                            else
                            {
                                if (!DisplayOptions.BusinessWeek.AircraftColors)
                                {
                                    if (item.RecordType == "T")
                                    {
                                        if (item.IsRONAppointment)
                                        {
                                            // when aircraft colors not selected - if not set calendar will use Aircraft Color from flight category
                                            if (BusinessFleetTreeCount > 0) // fleet view
                                            {
                                                if (AircraftDutyTypes.Count > 0 && !string.IsNullOrEmpty(item.AircraftDutyCD))
                                                {
                                                    var dutyCode = AircraftDutyTypes.Where(x => x.AircraftDutyCD.Trim() == item.AircraftDutyCD.Trim()).FirstOrDefault();
                                                    if (dutyCode != null)
                                                    {
                                                        e.Appointment.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                                                        e.Appointment.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                                                    }
                                                }
                                            }
                                            else if (BusinessCrewTreeCount > 0) // crew view => CrewTreeCount > 0
                                            {
                                                if (CrewDutyTypes.Count > 0 && !string.IsNullOrEmpty(item.CrewDutyTypeCD))
                                                {
                                                    var dutyCode = CrewDutyTypes.Where(x => x.DutyTypeCD.Trim() == item.CrewDutyTypeCD.Trim()).FirstOrDefault();
                                                    if (dutyCode != null)
                                                    {
                                                        e.Appointment.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                                                        e.Appointment.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            SetFlightCategoryColor(e, item);
                                        }
                                    }
                                    else if (item.RecordType == "M")
                                    {
                                        // when aircraft colors not selected - if not set calendar will use Aircraft Color from flight category
                                        SetAircraftDutyColor(e, item);
                                    }
                                    else if (item.RecordType == "C")
                                    {
                                        // set crew duty type color...
                                        SetCrewDutyColor(e, item);
                                    }
                                }
                                else if (DisplayOptions.BusinessWeek.AircraftColors && item.TailNum == null && item.RecordType == "C")
                                {
                                    SetCrewDutyColor(e, item);
                                }
                                else
                                {
                                    if (item.AircraftForeColor != null && item.AircraftBackColor != null)
                                    {
                                        e.Appointment.BackColor = Color.FromName(item.AircraftBackColor);
                                        e.Appointment.ForeColor = Color.FromName(item.AircraftForeColor);
                                    }
                                }
                            }
                        }
                        e.Appointment.ToolTip = "";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.BusinessWeek);
                }
            }
        }

        private void SetAircraftDutyColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {
                var appointmentAircraftDuty = item.AircraftDutyID;
                if (AircraftDutyTypes.Count > 0)
                {
                    var dutyCode = AircraftDutyTypes.Where(x => x.AircraftDutyID == appointmentAircraftDuty).FirstOrDefault();
                    if (dutyCode != null)
                    {
                        e.Appointment.BackColor = Color.FromName(dutyCode.BackgroundCustomColor);
                        e.Appointment.ForeColor = Color.FromName(dutyCode.ForeGrndCustomColor);
                    }
                }
            }
        }

        private void SetFlightCategoryColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {
                var appointmentCategory = item.FlightCategoryID;
                // Aircraft Color – if on display will use color set in flight category 
                if (FlightCategories.Count > 0)
                {
                    var categoryCode = FlightCategories.Where(x => x.FlightCategoryID == appointmentCategory).FirstOrDefault();
                    if (categoryCode != null)
                    {
                        e.Appointment.BackColor = Color.FromName(categoryCode.BackgroundCustomColor);
                        e.Appointment.ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                    }
                }
            }
        }

        private void SetCrewDutyColor(Telerik.Web.UI.SchedulerEventArgs e, FlightPak.Web.Entities.SchedulingCalendar.Appointment item)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(e, item))
            {

                var appointmentCategory = item.CrewDutyType;
                // Aircraft Color – if on display will use color set in flight category 
                if (CrewDutyTypes.Count > 0)
                {
                    var categoryCode = CrewDutyTypes.Where(x => x.DutyTypeID == appointmentCategory).FirstOrDefault();
                    if (categoryCode != null)
                    {
                        e.Appointment.BackColor = Color.FromName(categoryCode.BackgroundCustomColor);
                        e.Appointment.ForeColor = Color.FromName(categoryCode.ForeGrndCustomColor);
                    }
                }
            }
        }

        private void loadTree(bool IsSave)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //fleet trips list
                if (!Page.IsPostBack)
                {
                    DisplayOptionsPopup objFleetCrewIDs = new DisplayOptionsPopup();
                    SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.BusinessWeek);

                    string[] FleetCrew = SelectedFleetCrewIDs.Split('|');

                    Collection<TreeviewParentChildNodePair> Fleetlst = new Collection<TreeviewParentChildNodePair>();
                    Collection<TreeviewParentChildNodePair> Crewlst = new Collection<TreeviewParentChildNodePair>();

                    if (!string.IsNullOrEmpty(FleetCrew[0]))
                    {
                        string[] Fleetids = FleetCrew[0].Split(',');
                        foreach (string fIDs in Fleetids)
                        {
                            string[] ParentChild = fIDs.Split('$');
                            if (ParentChild.Length > 1)
                                Fleetlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                            else
                                Fleetlst.Add(new TreeviewParentChildNodePair("Fleet", fIDs));
                        }
                        BindFleetTree(Fleetlst);
                        BindCrewTree(Crewlst);
                    }
                    else if (!string.IsNullOrEmpty(FleetCrew[1]))
                    {
                        string[] Crewids = FleetCrew[1].Split(',');
                        foreach (string cIDs in Crewids)
                        {
                            string[] ParentChild = cIDs.Split('$');
                            if (ParentChild.Length > 1)
                                Crewlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                            else
                                Crewlst.Add(new TreeviewParentChildNodePair("Crew", cIDs));
                        }
                        BindFleetTree(Fleetlst);
                        BindCrewTree(Crewlst);
                    }
                    else
                    {
                        BindFleetTree(Fleetlst);
                        BindCrewTree(Crewlst);
                    }
                }

                using (var preflightServiceClient = new PreflightServiceClient())
                {
                    DisplayOptionsPopup objFleetCrewIDs = new DisplayOptionsPopup();

                    // check if Sunday/ Saturday is selected as First day / last day for the view...
                    if (IsSave)
                    {
                        var selectedFleetIDs = GetCheckedTreeNodes(FleetTreeView);
                        var selectedCrewIDs = GetCheckedTreeNodes(CrewTreeView);

                        objFleetCrewIDs.SetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.BusinessWeek, selectedFleetIDs, selectedCrewIDs, null, false);

                        BusinessFleetTreeCount = FleetTreeView.CheckedNodes.Count;
                        BusinessCrewTreeCount = CrewTreeView.CheckedNodes.Count;

                        Collection<string> SelectedFleetIDs = new Collection<string>();
                        foreach (TreeviewParentChildNodePair s in selectedFleetIDs)
                        {
                            SelectedFleetIDs.Add(s.ChildNodeId);
                        }

                        Collection<string> SelectedCrewIDs = new Collection<string>();
                        foreach (TreeviewParentChildNodePair s in selectedCrewIDs)
                        {
                            SelectedCrewIDs.Add(s.ChildNodeId);
                        }

                        BindFleetCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.BusinessWeek, SelectedFleetIDs, SelectedCrewIDs);
                        //BindFleetTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.BusinessWeek, selectedFleetIDs);
                        //BindCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.BusinessWeek, selectedCrewIDs);
                    }
                    else
                    {
                        SelectedFleetCrewIDs = objFleetCrewIDs.GetUserScheduleCalenderPrefernces(ModuleNameConstants.Preflight.BusinessWeek);
                        if (!string.IsNullOrEmpty(SelectedFleetCrewIDs))
                        {
                            string[] FleetCrew = SelectedFleetCrewIDs.Split('|');
                            var FleetIDs = FleetCrew[0];
                            var CrewIDs = FleetCrew[1];

                            if ((string.IsNullOrEmpty(FleetIDs) && FleetIDs.Count() == 0) && (string.IsNullOrEmpty(CrewIDs) && CrewIDs.Count() == 0))
                            {
                                if (FleetTreeView.Visible && CrewTreeView.Visible && FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)
                                {
                                    FleetTreeView.CheckAllNodes();
                                }
                            }

                            Collection<TreeviewParentChildNodePair> Fleetlst = new Collection<TreeviewParentChildNodePair>();
                            Collection<TreeviewParentChildNodePair> Crewlst = new Collection<TreeviewParentChildNodePair>();

                            if (!string.IsNullOrEmpty(FleetCrew[0]))
                            {
                                string[] Fleetids = FleetCrew[0].Split(',');
                                foreach (string fIDs in Fleetids)
                                {
                                    string[] ParentChild = fIDs.Split('$');
                                    if (ParentChild.Length > 1)
                                        Fleetlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                                    else
                                        Fleetlst.Add(new TreeviewParentChildNodePair("Fleet", fIDs));
                                }
                            }
                            else if (!string.IsNullOrEmpty(FleetCrew[1]))
                            {
                                string[] Crewids = FleetCrew[1].Split(',');
                                foreach (string cIDs in Crewids)
                                {
                                    string[] ParentChild = cIDs.Split('$');
                                    if (ParentChild.Length > 1)
                                        Crewlst.Add(new TreeviewParentChildNodePair(ParentChild[0], ParentChild[1]));
                                    else
                                        Crewlst.Add(new TreeviewParentChildNodePair("Crew", cIDs));
                                }
                            }
                            else
                                Fleetlst = GetCheckedTreeNodes(FleetTreeView);

                            BusinessFleetTreeCount = FleetTreeView.CheckedNodes.Count;
                            BusinessCrewTreeCount = CrewTreeView.CheckedNodes.Count;

                            Collection<string> fleetlst = new Collection<string>();
                            foreach (TreeviewParentChildNodePair s in Fleetlst)
                            {
                                fleetlst.Add(s.ChildNodeId);
                            }

                            Collection<string> crewlst = new Collection<string>();
                            foreach (TreeviewParentChildNodePair s in Crewlst)
                            {
                                crewlst.Add(s.ChildNodeId);
                            }
                            BindFleetCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.BusinessWeek, fleetlst, crewlst);

                            /*if ((!string.IsNullOrEmpty(FleetIDs) && FleetIDs.Count() > 0) || (Fleetlst.Count > 0))
                            {
                                BindFleetTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.BusinessWeek, Fleetlst);
                            }

                            if (!string.IsNullOrEmpty(CrewIDs) && CrewIDs.Count() > 0)
                            {
                                BindCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.BusinessWeek, Crewlst);
                            }*/
                        }
                    }
                    /*if (FleetTreeView.Visible && CrewTreeView.Visible && FleetTreeView.CheckedNodes.Count == 0 && CrewTreeView.CheckedNodes.Count == 0)
                    {
                        FleetTreeView.CheckAllNodes();
                    }

                    var inputFromFleetTree = GetCheckedTreeNodes(FleetTreeView).Distinct().ToList();
                    BusinessFleetTreeCount = inputFromFleetTree.Count; // this property is used in appointment data bound to identify the view 
                    var inputFromCrewTree = GetCheckedTreeNodes(CrewTreeView).Distinct().ToList();
                    BusinessCrewTreeCount = inputFromCrewTree.Count; // this property is used in appointment data bound to identify the view 

                    if (inputFromFleetTree.Count > 0)
                    {
                        BindFleetTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.BusinessWeek, new Collection<string>(inputFromFleetTree));
                    }
                    else if (inputFromCrewTree.Count > 0)
                    {
                        BindCrewTripInfo(preflightServiceClient, StartDate, EndDate, FilterOptions, DisplayOptions, ModuleNameConstants.Preflight.BusinessWeek, new Collection<string>(inputFromCrewTree));
                    }*/
                }

                RadPanelBar2.FindItemByText("Fleet").Visible = true;
                RadPanelBar2.FindItemByText("Fleet").Expanded = true;
                RadPanelBar2.FindItemByText("Crew").Visible = true;
                RadPanelBar2.FindItemByText("Crew").Expanded = true;
                RadPanelBar2.FindItemByText("Filter Criteria").Expanded = true;

                RightPane.Collapsed = true;
            }
        }

        private void BindFleetCrewTripInfo(PreflightServiceClient preflightServiceClient, DateTime businessWeekStartDate, DateTime businessWeekEndDate, FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria filterCriteria, DisplayOptions displayOptions, string viewMode, Collection<string> inputFromFleetTree, Collection<string> inputFromCrewTree)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(preflightServiceClient, businessWeekStartDate, businessWeekEndDate, filterCriteria, displayOptions, viewMode, inputFromFleetTree))
            {
                PreflightService.FilterCriteria serviceFilterCriteria = ConvertToServiceFilterCriteria(filterCriteria);

                Collection<Entities.SchedulingCalendar.Appointment> fleetappointments = new Collection<Entities.SchedulingCalendar.Appointment>();
                Collection<Entities.SchedulingCalendar.Appointment> crewappointments = new Collection<Entities.SchedulingCalendar.Appointment>();

                if (inputFromFleetTree.Count > 0)
                {
                    var calendarData = preflightServiceClient.GetFleetCalendarData(businessWeekStartDate, businessWeekEndDate, serviceFilterCriteria, inputFromFleetTree.ToList(), false, false, true);
                    
                    if (displayOptions.BusinessWeek.CrewCalActivity) // if crew cal activity  not checked,  display crew entries with out tail num
                    {
                        
                        CrewTreeView.CheckAllNodes();
                        var inputFromCrewTreeAll = GetCheckedTreeNodes(CrewTreeView, false);
                        CrewTreeView.UncheckAllNodes();
                        Collection<string> InputFromCrewTreeAll = new Collection<string>();
                        foreach (TreeviewParentChildNodePair s in inputFromCrewTreeAll)
                        {
                            InputFromCrewTreeAll.Add(s.ChildNodeId);
                        }
                        var crewcalendarData = preflightServiceClient.GetCrewCalendarData(businessWeekStartDate, businessWeekEndDate, serviceFilterCriteria, InputFromCrewTreeAll.ToList(), true);

                        // remove records of type='C' and tailNum == null...
                        fleetappointments = ConvertToFleetAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, businessWeekStartDate, businessWeekEndDate);
                        crewappointments = ConvertToCrewAppointmentEntity(crewcalendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, businessWeekStartDate, businessWeekEndDate);
                    }
                    else
                    {
                        // by default REcordType, T, M and C with tailNum will be displayed...
                        calendarData.EntityList = calendarData.EntityList.Where(x => x.TailNum != null).ToList();
                        fleetappointments = ConvertToFleetAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, businessWeekStartDate, businessWeekEndDate);
                    }
                    //RadBusinessWeekScheduler.DataSource = appointments.OrderBy(x => x.StartDate).ToList();
                    //RadBusinessWeekScheduler.DataStartField = "StartDate";
                    //RadBusinessWeekScheduler.DataEndField = "EndDate";
                    //RadBusinessWeekScheduler.DataSubjectField = "Description";
                    // RadBusinessWeekScheduler.DataKeyField = "TailNum";

                    // Calculate maximum number of appointments per stack and change the row height...
                    var maxAppointmentsPerStack = (fleetappointments.Count() == 0) ? 0 : fleetappointments.GroupBy(a => a.StartDate.Date).Max(g => g.Count());
                    if (maxAppointmentsPerStack < 6)
                    {
                        //RadBusinessWeekScheduler.RowHeight = 422;
                    }
                    else
                    {
                        //RadBusinessWeekScheduler.RowHeight = 60;
                    }
                }
               // Collection<Entities.SchedulingCalendar.Appointment> crewappointments = new Collection<Entities.SchedulingCalendar.Appointment>();
                else if (inputFromCrewTree.Count > 0)
                {
                    var calendarData = preflightServiceClient.GetCrewCalendarData(businessWeekStartDate, businessWeekEndDate, serviceFilterCriteria, inputFromCrewTree.ToList(), true);
                    crewappointments = ConvertToCrewAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, businessWeekStartDate, businessWeekEndDate);
                    //var appointments = ConvertToCrewAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode);

                    //Collection<Entities.SchedulingCalendar.Appointment> crewappointments = new Collection<Entities.SchedulingCalendar.Appointment>();
                   /* if (displayOptions.BusinessWeek.CrewCalActivity) // if crew cal activity  not checked,  display crew entries with out tail num
                    {
                        // remove records of type='C' and tailNum == null...
                        crewappointments = ConvertToCrewAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, businessWeekStartDate, businessWeekEndDate);
                    }
                    else
                    {
                        // by default REcordType, T, M and C with tailNum will be displayed...
                        calendarData.EntityList = calendarData.EntityList.Where(x => x.TailNum != null).ToList();
                        crewappointments = ConvertToCrewAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, businessWeekStartDate, businessWeekEndDate);
                    }*/

                    //RadBusinessWeekScheduler.DataSource = crewappointments.OrderBy(x => x.StartDate).ToList();                    

                    //RadBusinessWeekScheduler.DataStartField = "StartDate";
                    //RadBusinessWeekScheduler.DataEndField = "EndDate";
                    //RadBusinessWeekScheduler.DataSubjectField = "Description";
                    // RadBusinessWeekScheduler.DataKeyField = "CrewCD";
                }

                if (fleetappointments.OrderBy(x => x.StartDate).ToList().Count > 0 && crewappointments.OrderBy(x => x.StartDate).ToList().Count > 0)
                {
                    var fleetappointmentscc = fleetappointments.OrderBy(x => x.StartDate).ThenBy(x => x.DepartureDisplayTime).ThenBy(x => x.ArrivalDisplayTime).ToList();
                    var crewappointmentscc = crewappointments.OrderBy(x => x.StartDate).ThenBy(x => x.DepartureDisplayTime).ThenBy(x => x.ArrivalDisplayTime).ToList();
                    //RadBusinessWeekScheduler.DataSource = fleetappointments.OrderBy(x => x.StartDate).ToList().Concat(crewappointments.OrderBy(x => x.StartDate).ToList()).ToList();
                    RadBusinessWeekScheduler.DataSource = fleetappointmentscc.Concat(crewappointmentscc).OrderBy(x => x.StartDate).GroupBy(e => new { e.TripNUM, e.LegNum, e.StartDate }).Select(g => g.First()).ToList();
                }
                else if (fleetappointments.OrderBy(x => x.StartDate).ToList().Count > 0)
                {
                    RadBusinessWeekScheduler.DataSource = fleetappointments.OrderBy(x => x.StartDate).ToList();
                }
                else if (crewappointments.OrderBy(x => x.StartDate).ToList().Count > 0)
                {
                    RadBusinessWeekScheduler.DataSource = crewappointments.OrderBy(x => x.StartDate).ToList();
                }
                else
                    RadBusinessWeekScheduler.DataSource = fleetappointments.OrderBy(x => x.StartDate).ToList();

                //RadBusinessWeekScheduler.DataSource = fleetappointments.OrderBy(x => x.StartDate).ToList();
                RadBusinessWeekScheduler.DataStartField = "StartDate";
                RadBusinessWeekScheduler.DataEndField = "EndDate";
                RadBusinessWeekScheduler.DataSubjectField = "Description";

                RadBusinessWeekScheduler.DataBind();
            }
        }

        //private void BindCrewTripInfo(PreflightServiceClient preflightServiceClient, DateTime businessWeekStartDate, DateTime businessWeekEndDate, FlightPak.Web.Views.Transactions.Preflight.ScheduleCalendar.FilterCriteria filterCriteria, DisplayOptions displayOptions, string viewMode, Collection<string> inputFromCrewTree)
        //{

        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(preflightServiceClient, businessWeekStartDate, businessWeekEndDate, filterCriteria, displayOptions, viewMode, inputFromCrewTree))
        //    {

        //        PreflightService.FilterCriteria serviceFilterCriteria = ConvertToServiceFilterCriteria(filterCriteria);

        //        if (inputFromCrewTree.Count > 0)
        //        {
        //            var calendarData = preflightServiceClient.GetCrewCalendarData(businessWeekStartDate, businessWeekEndDate, serviceFilterCriteria, inputFromCrewTree.ToList(), true);

        //            //var appointments = ConvertToCrewAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode);

        //            Collection<Entities.SchedulingCalendar.Appointment> appointments = new Collection<Entities.SchedulingCalendar.Appointment>();
        //            if (displayOptions.BusinessWeek.CrewCalActivity) // if crew cal activity  not checked,  display crew entries with out tail num
        //            {
        //                // remove records of type='C' and tailNum == null...
        //                appointments = ConvertToCrewAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, businessWeekStartDate, businessWeekEndDate);
        //            }
        //            else
        //            {
        //                // by default REcordType, T, M and C with tailNum will be displayed...
        //                calendarData.EntityList = calendarData.EntityList.Where(x => x.TailNum != null).ToList();
        //                appointments = ConvertToCrewAppointmentEntity(calendarData.EntityList, filterCriteria.TimeBase.ToString(), displayOptions, viewMode, businessWeekStartDate, businessWeekEndDate);
        //            }

        //            RadBusinessWeekScheduler.DataSource = appointments.OrderBy(x => x.StartDate).ToList();

        //            RadBusinessWeekScheduler.DataStartField = "StartDate";
        //            RadBusinessWeekScheduler.DataEndField = "EndDate";
        //            RadBusinessWeekScheduler.DataSubjectField = "Description";
        //            // RadBusinessWeekScheduler.DataKeyField = "CrewCD";
        //        }

        //        RadBusinessWeekScheduler.DataBind();
        //    }
        //}

        private void BindFleetTree(Collection<TreeviewParentChildNodePair> selectedFleetIDs)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    var list = preflightServiceClient.GetFleetInfoForFleetTree(FilterOptions.HomeBaseOnly); // if homebaseonly option is selected, filter by homebaseID
                    if (list != null)
                    {
                        var fleetList = list.EntityList;
                        //Changes done for the Bug ID - 5828
                        RadTreeNode parentNode = BindFullAndGroupedFleetTreeNodesNew(fleetList, selectedFleetIDs);

                        FleetTreeView.Nodes.Add(parentNode);
                        FleetTreeView.CheckBoxes = true;
                        FleetTreeView.CheckChildNodes = true;

                        FleetTreeView.DataBind();
                    }
                }
            }
        }

        private void BindCrewTree(Collection<TreeviewParentChildNodePair> selectedcrewIDs)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PreflightServiceClient preflightServiceClient = new PreflightServiceClient())
                {
                    var list = preflightServiceClient.GetCrewInfoForCrewTree(FilterOptions.HomeBaseOnly);  //if homebaseonly option is selected in filter, filter by homebaseId
                    if (list != null)
                    {
                        var crewList = list.EntityList; //this list contains crew codes with filter NoCalendarDisplay != true

                        RadTreeNode parentNode = BindFullAndGroupedCrewTreeNodesNew(crewList, selectedcrewIDs);

                        CrewTreeView.Nodes.Add(parentNode);
                        CrewTreeView.CheckBoxes = true;
                        CrewTreeView.CheckChildNodes = true;

                        CrewTreeView.DataBind();
                    }
                }
            }
        }

        public Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> ConvertToFleetAppointmentEntity(List<FleetCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, DateTime weekStartDate, DateTime weekEndDate)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode))
            {

                Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> appointmentCollection = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();

                var distinctTripNos = calendarData.Select(x => x.TripID).Distinct().ToList();

                DateTime? FirstdayTripDate = null;
                Int64? FirstdayTripID = null;

                foreach (var tripNo in distinctTripNos)
                {
                    // build appointment data based on first/last leg display option

                    var groupedLegs = calendarData.Where(p => p.TripID == tripNo).ToList().OrderBy(x => x.DepartureDisplayTime).ToList();
                    var lastLeg = groupedLegs.OrderByDescending(x => x.LegNUM).First();

                    foreach (var leg in groupedLegs)
                    {
                        if (displayOptions.BusinessWeek.FirstLastLeg)
                        {
                            if (leg.LegNUM == 1 || leg.LegNUM == lastLeg.LegNUM)
                            {
                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                SetFleetAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                appointmentCollection.Add(appointment);

                            }
                            else if (leg.LegNUM == 0) // RON appointment
                            {
                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                SetFleetAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                appointmentCollection.Add(appointment);
                            }
                        }
                        else // No first leg last leg selected
                        {
                            if ((leg.RecordType == "M" || leg.RecordType == "C") && leg.TailNum.ToUpper() != "DUMMY" && leg.DepartureDisplayTime != leg.ArrivalDisplayTime)
                            {
                                int i = 0;
                                DateTime StartDate = Convert.ToDateTime(leg.DepartureDisplayTime);
                                DateTime EndDate = Convert.ToDateTime(leg.ArrivalDisplayTime);
                                TimeSpan span = EndDate.Subtract(StartDate);
                                int totaldays = 0;

                                if (StartDate.TimeOfDay <= EndDate.TimeOfDay)
                                {
                                    totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 1;
                                }
                                else
                                {
                                    totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 2;
                                }

                                while (i < totaldays)
                                {
                                    leg.DepartureDisplayTime = (i == 0) ? leg.DepartureDisplayTime : StartDate.AddDays(i).Date.AddTicks(1);
                                    leg.ArrivalDisplayTime = (i < totaldays - 1) ? StartDate.AddDays(i + 1).Date.AddTicks(-1) : EndDate;

                                    if (leg.DepartureDisplayTime >= weekStartDate && leg.DepartureDisplayTime <= weekEndDate)
                                    {
                                        FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();                                        
                                        SetFleetAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                        appointmentCollection.Add(appointment);
                                    }
                                    i++;
                                }
                            }
                            else
                            {
                                if (leg.DepartureDisplayTime >= weekStartDate && leg.DepartureDisplayTime <= weekEndDate)
                                {
                                    FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                    SetFleetAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                    if (appointment.RecordType == "T")
                                    {
                                        if (appointment.LegNum != 0)
                                        {
                                            FirstdayTripDate = appointment.DepartureDisplayTime.Date;
                                            FirstdayTripID = appointment.TripId;
                                            appointmentCollection.Add(appointment);
                                        }
                                        else if (appointment.IsRONAppointment && FirstdayTripDate != null && FirstdayTripID != null)
                                        {
                                            if (appointment.DepartureDisplayTime.Date == FirstdayTripDate && appointment.TripId == FirstdayTripID)
                                            {
                                                FirstdayTripDate = null;
                                                FirstdayTripID = null;
                                            }
                                            else
                                                appointmentCollection.Add(appointment);
                                        }
                                        else
                                            appointmentCollection.Add(appointment);
                                    }
                                    else
                                        appointmentCollection.Add(appointment);
                                }
                            }
                        }
                    }
                }
                return appointmentCollection;
            }
        }

        private void SetFleetAppointment(List<FleetCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, FleetCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode, tripLeg, appointment))
            {
                appointment = SetFleetCommonProperties(calendarData, timeBase, tripLeg, appointment);

                // todo: build description for appointment based on view selected and display options for rendering
                if (tripLeg.RecordType == "T")
                {
                    var options = displayOptions.BusinessWeek;
                    if (tripLeg.LegNUM == 0) // dummy appointment
                    {
                        if (options.FirstLastLeg == false)
                        {
                            appointment.Description = appointment.TailNum + " ( " + appointment.AircraftDutyCD + " ) ";


                            switch (options.DepartArriveInfo)
                            {
                                case DepartArriveInfo.ICAO:
                                    appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                                    break;

                                case DepartArriveInfo.AirportName:
                                    appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                                    break;

                                case DepartArriveInfo.City:
                                    appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                                    break;
                            }
                            appointment.Description = string.Format("{0}{1}{2}", appointment.Description,
                                                                    appointment.ArrivalICAOID,
                                                                    BuildWeeklyFleetAndCrewToolTipDescription(
                                                                        appointment, viewMode, displayOptions));

                        }
                    }
                    else // db trips
                    {
                        appointment.Description = string.Format("{0}{1}",
                                                                BuildBusinessWeekAppointmentDescription(appointment,
                                                                                                        null,
                                                                                                        calendarData,
                                                                                                        displayOptions),
                                                                BuildWeeklyFleetAndCrewToolTipDescription(appointment,
                                                                                                          viewMode,
                                                                                                          displayOptions));
                    }
                }
                else // entries...
                {

                    switch (displayOptions.BusinessWeek.DepartArriveInfo)
                    {
                        case DepartArriveInfo.ICAO:
                            appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                            appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                            break;

                        case DepartArriveInfo.AirportName:
                            appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                            break;

                        case DepartArriveInfo.City:
                            appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                            break;

                    }

                    StringBuilder builder = new StringBuilder();
                    builder.Append("<b>");
                    builder.Append(appointment.TailNum);
                    builder.Append("</b>");
                    builder.Append("</br>");
                    if (appointment.RecordType == "M")
                    {
                        if (!string.IsNullOrEmpty(appointment.AircraftDutyCD))
                        {
                            builder.Append(" ( ");
                            builder.Append(appointment.AircraftDutyCD);
                            builder.Append(" ) ");
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(appointment.CrewDutyTypeCD))
                        {
                            builder.Append(" ( ");
                            builder.Append(appointment.CrewDutyTypeCD);
                            builder.Append(" ) ");
                        }
                    }

                    if (appointment.RecordType == "M")
                    {
                        if (appointment.StartDate == appointment.HomelDepartureTime)
                        {
                            builder.Append(appointment.StartDate.ToString("HH:mm"));
                        }
                        else
                        {
                            builder.Append("00:00");
                        }
                        builder.Append(" ");
                        builder.Append(appointment.DepartureICAOID);
                        builder.Append(" ");

                        if (appointment.ArrivalDisplayTime == appointment.HomeArrivalTime)
                        {
                            builder.Append(appointment.ArrivalDisplayTime.ToString("HH:mm"));
                        }
                        else
                        {
                            builder.Append("23:59");
                        }
                        
                        if (!string.IsNullOrEmpty(appointment.AircraftDutyDescription))
                        {
                            builder.Append("</br>");
                            builder.Append(appointment.AircraftDutyDescription);                           
                        }
                    }
                    else
                    {
                        builder.Append(appointment.StartDate.ToString("HH:mm"));
                        builder.Append(" ");

                        builder.Append(appointment.DepartureICAOID);
                        builder.Append(" ");

                        builder.Append(appointment.EndDate.ToString("HH:mm"));
                        builder.Append(" ");
                                               
                        if (!string.IsNullOrEmpty(appointment.CrewDutyTypeDescription))
                        {
                            builder.Append("</br>");
                            builder.Append(appointment.CrewDutyTypeDescription);                            
                        }
                    }
                    //builder.Append(appointment.StartDate.ToString("HH:mm"));
                    //builder.Append(" ");

                    //builder.Append(appointment.DepartureICAOID);
                    //builder.Append(" ");

                    //builder.Append(appointment.EndDate.ToString("HH:mm"));
                    //builder.Append("</br>");

                    builder.Append(appointment.Notes);
                    appointment.Description = string.Format("{0}{1}", builder.ToString(), BuildWeeklyFleetAndCrewOnlyToolTipDescription(appointment, viewMode, displayOptions));
                }
                appointment.ViewMode = viewMode;
            }
        }

        //Convert calendar data result to Appointment entity
        public Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> ConvertToCrewAppointmentEntity(List<CrewCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, DateTime weekStartDate, DateTime weekEndDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode))
            {
                Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment> appointmentCollection = new Collection<FlightPak.Web.Entities.SchedulingCalendar.Appointment>();

                var distinctTripNos = calendarData.Select(x => x.TripID).Distinct().ToList();
                
                DateTime? FirstdayTripDate = null;
                Int64? FirstdayTripID = null;

                foreach (var tripNo in distinctTripNos)
                {
                    // build appointment data based on first/last leg display option

                    var groupedLegs = calendarData.Where(p => p.TripID == tripNo).ToList().OrderBy(x => x.DepartureDisplayTime).ToList();
                    var lastLeg = groupedLegs.OrderByDescending(x => x.LegNUM).First();

                    foreach (var leg in groupedLegs)
                    {
                        if (displayOptions.WeeklyCrew.FirstLastLeg)
                        {
                            if (leg.LegNUM == 1 || leg.LegNUM == lastLeg.LegNUM)
                            {
                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                SetCrewAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                appointmentCollection.Add(appointment);
                            }
                        }
                        else // No first leg last leg selected
                        {
                            if (leg.RecordType == "C" && leg.DepartureDisplayTime != leg.ArrivalDisplayTime)
                            {
                                int i = 0;
                                DateTime StartDate = Convert.ToDateTime(leg.DepartureDisplayTime);
                                DateTime EndDate = Convert.ToDateTime(leg.ArrivalDisplayTime);
                                TimeSpan span = EndDate.Subtract(StartDate);
                                int totaldays = 0;

                                if (StartDate.TimeOfDay <= EndDate.TimeOfDay)
                                {
                                    totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 1;
                                }
                                else
                                {
                                    totaldays = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(span.Days))) + 2;
                                }

                                while (i < totaldays)
                                {
                                    leg.DepartureDisplayTime = (i == 0) ? leg.DepartureDisplayTime : StartDate.AddDays(i);
                                    leg.ArrivalDisplayTime = (i < totaldays - 1) ? StartDate.AddDays(i) : EndDate;

                                    FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                    SetCrewAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                    appointmentCollection.Add(appointment);
                                    i++;
                                }
                            }
                            else
                            {
                                FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment = new FlightPak.Web.Entities.SchedulingCalendar.Appointment();
                                SetCrewAppointment(calendarData, timeBase, displayOptions, viewMode, leg, appointment);
                                if (appointment.RecordType == "T")
                                {
                                    if (appointment.LegNum != 0)
                                    {
                                        FirstdayTripDate = appointment.DepartureDisplayTime.Date;
                                        FirstdayTripID = appointment.TripId;
                                        appointmentCollection.Add(appointment);
                                    }
                                    else if (appointment.IsRONAppointment && FirstdayTripDate != null && FirstdayTripID != null)
                                    {
                                        if (appointment.DepartureDisplayTime.Date == FirstdayTripDate && appointment.TripId == FirstdayTripID)
                                        {
                                            FirstdayTripDate = null;
                                            FirstdayTripID = null;
                                        }
                                        else
                                            appointmentCollection.Add(appointment);
                                    }
                                    else
                                        appointmentCollection.Add(appointment);
                                }
                                else
                                    appointmentCollection.Add(appointment);
                            }
                        }
                    }
                }
                return appointmentCollection;
            }
        }

        private void SetCrewAppointment(List<CrewCalendarDataResult> calendarData, string timeBase, DisplayOptions displayOptions, string viewMode, CrewCalendarDataResult tripLeg, FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(calendarData, timeBase, displayOptions, viewMode, tripLeg, appointment))
            {

                SetCommonCrewProperties(calendarData, timeBase, tripLeg, appointment);

                // todo: build description for appointment based on view selected and display options for rendering
                if (tripLeg.RecordType == "T")
                {
                    if (tripLeg.LegNUM == 0) // dummy appointment
                    {
                        var builder = new StringBuilder();
                        builder.Append("<b>" + appointment.CrewCD);
                        builder.Append(" ");
                        builder.Append(appointment.TailNum);
                        builder.Append("</b></br>(");
                        builder.Append(appointment.CrewDutyTypeCD + " ) ");

                        var options = displayOptions.BusinessWeek;
                        switch (options.DepartArriveInfo)
                        {
                            case DepartArriveInfo.ICAO:
                                appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                                break;

                            case DepartArriveInfo.AirportName:
                                appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                                break;

                            case DepartArriveInfo.City:
                                appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                                break;

                        }
                        builder.Append(" ");
                        builder.Append(appointment.ArrivalICAOID);


                        var flightNumber = string.Empty;
                        if (options.FlightNumber)
                        {
                            flightNumber = " Flt No.: " + appointment.FlightNum;
                            builder.Append(" ");
                            builder.Append(flightNumber);
                        }

                        appointment.Description = string.Format("{0}{1}", builder.ToString(),BuildWeeklyFleetAndCrewToolTipDescription(appointment, viewMode, displayOptions));
                        
                    }
                    else // db trips
                    {
                        appointment.Description = string.Format("{0}{1}", BuildBusinessWeekAppointmentDescription(appointment, calendarData, null, displayOptions),BuildWeeklyFleetAndCrewToolTipDescription(appointment, viewMode, displayOptions));
                    }
                }
                else // entries...
                {
                    switch (displayOptions.BusinessWeek.DepartArriveInfo)
                    {
                        case DepartArriveInfo.ICAO:
                            appointment.DepartureICAOID = tripLeg.DepartureICAOID;
                            appointment.ArrivalICAOID = tripLeg.ArrivalICAOID;
                            break;

                        case DepartArriveInfo.AirportName:
                            appointment.DepartureICAOID = tripLeg.DepartureAirportName; // same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalAirportName;
                            break;

                        case DepartArriveInfo.City:
                            appointment.DepartureICAOID = tripLeg.DepartureCity;// same field is used to display airportname / id and city based on selected option..
                            appointment.ArrivalICAOID = tripLeg.ArrivalCity;
                            break;

                    }

                    StringBuilder builder = new StringBuilder();
                    //builder.Append("<b>");
                    //builder.Append(appointment.CrewCD);
                    //builder.Append(" ");
                    //builder.Append(appointment.TailNum);
                    //builder.Append("</b>");
                    //builder.Append("</br>");
                    //if (!string.IsNullOrEmpty(appointment.CrewDutyTypeCD))
                    //{
                    //    builder.Append(" ( ");
                    //    builder.Append(appointment.CrewDutyTypeCD);
                    //    builder.Append(" ) ");
                    //}

                    //builder.Append(appointment.StartDate.ToString("HH:mm"));
                    //builder.Append(" ");

                    //builder.Append(appointment.DepartureICAOID);
                    //builder.Append(" ");

                    //builder.Append(appointment.EndDate.ToString("HH:mm"));
                    //builder.Append("</br>");

                    if (appointment.RecordType == "M")
                    {
                        if (!string.IsNullOrEmpty(appointment.AircraftDutyCD))
                        {
                            builder.Append("(");
                            builder.Append(appointment.AircraftDutyCD);
                            builder.Append(") ");
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(appointment.CrewDutyTypeCD))
                        {
                            builder.Append("(");
                            builder.Append(appointment.CrewDutyTypeCD);
                            builder.Append(") ");
                        }
                    }

                    if (appointment.RecordType == "C")
                    {
                        if (appointment.StartDate == appointment.HomelDepartureTime)
                        {
                            builder.Append(appointment.StartDate.ToString("HH:mm"));
                        }
                        else
                        {
                            builder.Append("00:00");
                        }
                        builder.Append(" ");
                        builder.Append(appointment.DepartureICAOID);
                        builder.Append(" ");

                        if (appointment.ArrivalDisplayTime == appointment.HomeArrivalTime)
                        {
                            builder.Append(appointment.ArrivalDisplayTime.ToString("HH:mm"));
                        }
                        else
                        {
                            builder.Append("23:59");
                        }                                               
                        if (!string.IsNullOrEmpty(appointment.CrewDutyTypeDescription))
                        {
                            builder.Append("</br>");
                            builder.Append(appointment.CrewDutyTypeDescription);                            
                        }
                    }
                    else
                    {
                        builder.Append(appointment.StartDate.ToString("HH:mm"));
                        builder.Append(" ");

                        builder.Append(appointment.DepartureICAOID);
                        builder.Append(" ");

                        builder.Append(appointment.EndDate.ToString("HH:mm"));
                        builder.Append(" ");

                        if (!string.IsNullOrEmpty(appointment.AircraftDutyDescription))
                        {
                            builder.Append("</br>");
                            builder.Append(appointment.AircraftDutyDescription);                            
                        }
                    }

                    builder.Append(appointment.Notes);
                    appointment.Description = string.Format("{0}{1}", builder.ToString(), BuildWeeklyFleetAndCrewOnlyToolTipDescription(appointment, viewMode, displayOptions));
                }
                appointment.ViewMode = viewMode;
            }
        }

        protected DateTime GetBusinessWeekStartDate(DateTime selectedDate)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selectedDate))
            {
                return selectedDate.AddDays(-(int)selectedDate.DayOfWeek).AddDays(1);
            }
        }

        // To get end day of the Month...
        public static DateTime GetBusinessWeekEndDate(DateTime businessWeekStartDate)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(businessWeekStartDate))
            {

                return businessWeekStartDate.AddDays(15).AddSeconds(-1);

            }

        }

        // description for appointment
        public string BuildBusinessWeekAppointmentDescription(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, List<CrewCalendarDataResult> calendarData, List<FleetCalendarDataResult> fleetCalendarData, DisplayOptions displayOptions)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, calendarData, fleetCalendarData, displayOptions))
            {

                var description = new StringBuilder();
                var options = displayOptions.BusinessWeek;
                bool ShowAllDetails = appointment.ShowAllDetails;

                description.Append("<b>");
                description.Append(appointment.TailNum);
                description.Append("</b>");
                description.Append("<br />\n");

                if (options.ShowTrip)
                {

                    description.Append("Trip: ");
                    description.Append(appointment.TripNUM);
                    description.Append(" ");
                }

             
                if (ShowAllDetails)
                {
                    description.Append(options.FlightNumber ? string.Format("<strong> Flt No:</strong>{0}", appointment.FlightNum) : "");
                }

                if (options.ShowTripStatus)
                {
                    description.Append("( ");
                    description.Append(appointment.TripStatus);
                    description.Append(" ) ");
                }
                description.Append("<br />\n");

                if (options.ShowTrip)
                {
                    description.Append("Leg No.: ");
                    description.Append(appointment.LegNum);
                }

                if (ShowAllDetails)
                {
                    description.Append(options.FlightCategory
                                           ? string.Format("<strong> Cat : </strong>{0}",
                                                           appointment.FlightCategoryCode)
                                           : "");
                    description.Append("<br />\n");
                }
                else
                {
                    description.Append("<br />\n");
                }

                if (calendarData != null && calendarData.Count > 0) // calendar crew data
                {
                    var departureArrivalInfo = options.DepartArriveInfo;
                    GetDepartureArrivalInfo(appointment, departureArrivalInfo, description);
                    description.Append(" ");

                }
                else // calendar fleet data
                {

                    var departureArrivalInfo = options.DepartArriveInfo;
                    GetDepartureArrivalInfo(appointment, departureArrivalInfo, description);
                    description.Append(" ");
                }
                //To display Arrival Date in Scheduler
                if (!options.DisplayRedEyeFlightsInContinuation && appointment.ArrivalDisplayTime.Date != appointment.DepartureDisplayTime.Date && appointment.RecordType == "T")
                {
                    description.Append("<br />\n");
                    description.Append("* Arrival Date: ");
                    //description.Append(appointment.ArrivalDisplayTime.ToShortDateString());
                    description.Append(String.Format("{0:" + ApplicationDateFormat + "}", Convert.ToDateTime(appointment.ArrivalDisplayTime.ToShortDateString().ToString())));
                    description.Append(" *");
                    description.Append("<br />\n");
                    description.Append(" ");             
                }

                return description.ToString();

            }

        }

        //description for tooltip

        public string BuildWeeklyFleetAndCrewToolTipDescription(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, string viewMode, DisplayOptions displayOptions)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, viewMode, displayOptions))
            {

                var description = new StringBuilder();
                var options = displayOptions.BusinessWeek;
                description.Append("<table class='calendarinfo'><tr><td>");
                if (appointment.ShowAllDetails)
                {
                    //description.Append(options.FlightNumber ? string.Format("<strong>Flt No.: </strong>{0}", appointment.FlightNum) : " ");
                    //description.Append(options.FlightCategory ? string.Format("<strong> Cat: </strong>{0} </td></tr>", appointment.FlightCategoryCode) : " </td></tr>");

                    description.Append(options.ETE ? string.Format("<tr><td ><strong>ETE: </strong>{0}", appointment.ETE) : "<tr><td >");
                    description.Append(options.CummulativeETE ? string.Format("<strong> Cum ETE:</strong>{0}</td></tr>", appointment.CumulativeETE) : "</td></tr> ");

                    description.Append(options.TotalETE ? string.Format("<tr><td><strong>Trip ETE: </strong>{0}", appointment.TotalETE) : "<tr><td >");
                    description.Append(options.Requestor ? string.Format("<strong> Req: </strong>{0} </td></tr>", appointment.PassengerRequestorCD) : "</td></tr> ");

                    description.Append(options.PaxCount ? string.Format("<tr><td><strong>PAX: </strong>{0}", appointment.PassengerCount) : "<tr><td >");
                    description.Append(options.SeatsAvailable ? string.Format("<strong> Seats Avail: </strong>{0} </td></tr>", appointment.ReservationAvailable) : "</td></tr> ");

                    description.Append(options.Department ? string.Format("<tr><td><strong>Dept: </strong>{0} ", appointment.DepartmentCD) : "<tr><td >");
                    description.Append(options.Authorization ? string.Format("<strong>Auth: </strong>{0}  </td></tr>", appointment.AuthorizationCD) : "</td></tr> ");

                    description.Append(options.TripPurpose ? string.Format("<tr><td colspan='2'><strong>Trip Purp.: </strong>{0} </td></tr>", appointment.TripPurpose) : "<tr><td colspan='2'></td></tr>");
                    description.Append(options.LegPurpose ? string.Format("<tr><td colspan='2'><strong>Leg Purp.: </strong>{0} </td></tr>", appointment.FlightPurpose) : "<tr><td colspan='2'></td></tr>");

                    description.Append(options.Crew ? string.Format(" <tr><td colspan='2'><strong>Crew: </strong>{0}", string.IsNullOrEmpty(appointment.CrewCD) ? appointment.CrewCodes : appointment.CrewCD) : "<tr><td colspan='2'>");

                }
                else // show details based on Trip Privacy Settins from company profile
                {
                    description.Append((PrivateTrip.IsShowFlightNumber && options.FlightNumber) ? string.Format("<strong>Flt No.: </strong>{0} ", appointment.FlightNum) : " ");
                    description.Append((PrivateTrip.IsShowFlightCategory && options.FlightCategory) ? string.Format("<strong>Cat: </strong>{0} </td></tr>", appointment.FlightCategoryCode) : " </td></tr>");


                    description.Append((PrivateTrip.IsShowETE && options.ETE) ? string.Format("<tr><td><strong>ETE: </strong>{0}", appointment.ETE) : "<tr><td>");
                    description.Append((PrivateTrip.IsShowCumulativeETE && options.CummulativeETE) ? string.Format("<strong> Cum ETE: </strong>{0}  </td></tr>", appointment.CumulativeETE) : "</td></tr>");

                    description.Append((PrivateTrip.IsShowTotalETE && options.TotalETE) ? string.Format("<tr><td ><strong>Trip ETE: </strong>{0} ", appointment.TotalETE) : "<tr><td> ");
                    description.Append((PrivateTrip.IsShowRequestor && options.Requestor) ? string.Format("<strong> Req: </strong>{0} </td></tr>", appointment.PassengerRequestorCD) : " </td></tr>");

                    description.Append((PrivateTrip.IsShowPaxCount && options.PaxCount) ? string.Format("<tr><td><strong>PAX: </strong>{0} ", appointment.PassengerCount) : "<tr><td> ");
                    description.Append((PrivateTrip.IsShowSeatsAvailable && options.SeatsAvailable) ? string.Format(" <strong> Seats Avail: </strong>{0} </td></tr>", appointment.ReservationAvailable) : "</td></tr>");

                    description.Append((PrivateTrip.IsShowDepartment && options.Department) ? string.Format("<tr><td><strong>Dept: </strong>{0}", appointment.DepartmentCD) : " <tr><td>");
                    description.Append((PrivateTrip.IsShowAuthorization && options.Authorization) ? string.Format("<strong>Auth: </strong>{0}  </td></tr>", appointment.AuthorizationCD) : " </td></tr>");

                    description.Append((PrivateTrip.IsShowTripPurpose && options.TripPurpose) ? string.Format("<tr><td colspan='2'><strong>Trip Purp.: </strong>{0} </td></tr>", appointment.TripPurpose) : "<tr><td colspan='2'></td></tr>");
                    description.Append((PrivateTrip.IsShowLegPurpose && options.LegPurpose) ? string.Format("<tr><td colspan='2'><strong>Leg Purp.: </strong>{0} </td></tr>", appointment.FlightPurpose) : "<tr><td colspan='2'></td></tr>");

                    description.Append((PrivateTrip.IsShowCrew && options.Crew) ? string.Format(" <tr><td colspan='2'><strong>Crew: </strong>{0}", string.IsNullOrEmpty(appointment.CrewCD) ? appointment.CrewCodes : appointment.CrewCD) : "<tr><td colspan='2'>");
                }
                description.Append("  </td></tr> </table>");

                return description.ToString();
            }
        }

        public string BuildWeeklyFleetAndCrewToolTipDescriptionAppt(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, string viewMode, DisplayOptions displayOptions)
        {
            var description = new StringBuilder();
            // var groupedLegs = calendarData.Where(p => p.TripNUM == appointment.TripNUM).ToList();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, displayOptions))
            {

                var options = displayOptions.BusinessWeek;
                //if (!options.DisplayRedEyeFlightsInContinuation && appointment.ArrivalDisplayTime.Date != appointment.DepartureDisplayTime.Date)
                //{
                //    description.Append("<br />\n");
                //    description.Append("** Arrival Date: ");
                //    description.Append(appointment.ArrivalDisplayTime.ToShortDateString());
                //    description.Append(" **");
                //    description.Append("<br />\n");
                //}

                description.Append("\n");
                if (appointment.ShowAllDetails)
                {
                    description.Append("<br />");
                    if (options.FlightNumber)
                    {
                        description.Append("<strong>Flt No.: </strong>");
                        description.Append(appointment.FlightNum);
                        description.Append("     ");
                    }
                    //description.Append("<br />");

                    if (options.FlightCategory)
                    {
                        description.Append("<strong>Category: </strong>");
                        description.Append(appointment.FlightCategoryCode);

                    }
                    //description.Append("<br />");

                    description.Append(" <tr><td colspan='2'>");
                    if (options.ETE)
                    {
                        description.Append("<strong>ETE: </strong>");
                        description.Append(appointment.ETE);
                        description.Append(" ");
                    }
                    description.Append("</td><td>");
                    if (options.CummulativeETE)
                    {
                        description.Append("<br /><strong>Cum ETE: </strong>");
                        description.Append(appointment.CumulativeETE);
                        description.Append(" ");
                    }
                    description.Append("<br />");

                    if (options.TotalETE)
                    {
                        description.Append("<br /><strong>Total ETE: </strong>");
                        description.Append(appointment.TotalETE);
                        description.Append(" ");
                    }
                    description.Append("<br />");

                    if (options.PaxCount)
                    {
                        description.Append("<strong>PAX: </strong>");
                        description.Append(appointment.PassengerCount);
                        description.Append("     ");
                    }
                    //description.Append("<br />");
                    if (options.Requestor)
                    {
                        description.Append("<strong>Req: </strong>");
                        description.Append(appointment.PassengerRequestorCD);
                        description.Append(" ");
                    }
                    description.Append("<br />");
                    if (options.Department)
                    {
                        description.Append("<strong>Dept: </strong>");
                        description.Append(appointment.DepartmentCD);
                        description.Append("     ");
                    }

                    if (options.Authorization)
                    {
                        description.Append("<strong>Auth: </strong>");
                        description.Append(appointment.AuthorizationCD);
                        description.Append(" ");
                    }
                    //description.Append("<br />");
                    if (options.TripPurpose)
                    {
                        description.Append("<br /><strong>Trip Purp.: </strong>");
                        description.Append(appointment.TripPurpose);
                        description.Append(" ");
                    }
                    //description.Append("<br />");
                    if (options.LegPurpose)
                    {
                        description.Append("<br /><strong>Leg Purp.: </strong>");
                        description.Append(appointment.FlightPurpose);
                        description.Append(" ");
                    }
                    //description.Append("<br />");
                    if (options.SeatsAvailable)
                    {
                        description.Append("<br /><strong>Seats Avail: </strong>");
                        description.Append(appointment.ReservationAvailable);
                        description.Append(" ");
                    }
                    //description.Append("<br />");

                    if (options.Crew)
                    {
                        description.Append("<br /><strong>Crew: </strong>");
                        if (!string.IsNullOrEmpty(appointment.CrewCD))
                        {
                            description.Append(appointment.CrewCodes);
                            //description.Append(appointment.CrewCD);
                        }
                        else
                        {
                            description.Append(appointment.CrewCodes);
                        }
                    }
                }
                else // show details based on Trip Privacy Settins from company profile
                {
                    description.Append("<br />");
                    if (PrivateTrip.IsShowFlightNumber && options.FlightNumber)
                    {
                        description.Append("<strong>Flt No.: </strong>");
                        description.Append(appointment.FlightNum);
                        description.Append("  ");
                    }
                    //description.Append("<br />");

                    if (PrivateTrip.IsShowFlightCategory && options.FlightCategory)
                    {
                        description.Append("<strong>Category: </strong>");
                        description.Append(appointment.FlightCategoryCode);

                    }
                    description.Append("\n");
                    if (PrivateTrip.IsShowETE && options.ETE)
                    {
                        description.Append("<strong>ETE: </strong>");
                        description.Append(appointment.CumulativeETE);
                        description.Append(" ");
                    }
                    //description.Append("</td><td>");
                    if (PrivateTrip.IsShowCumulativeETE && options.CummulativeETE)
                    {
                        description.Append("<br /><strong>Cum ETE: </strong>");
                        description.Append(appointment.CumulativeETE);
                        description.Append(" ");
                    }
                    if (PrivateTrip.IsShowTotalETE && options.TotalETE)
                    {
                        description.Append("<br /><strong>Total ETE: </strong>");
                        description.Append(appointment.TotalETE);
                        description.Append(" ");
                    }

                    description.Append("<br />");
                    if (PrivateTrip.IsShowPaxCount && options.PaxCount)
                    {
                        description.Append("<strong>PAX: </strong>");
                        description.Append(appointment.PassengerCount);
                        description.Append("     ");
                    }
                    //description.Append("<\n>");
                    if (PrivateTrip.IsShowRequestor && options.Requestor)
                    {
                        description.Append("<strong>Req: </strong>");
                        description.Append(appointment.PassengerRequestorCD);
                        description.Append(" ");
                    }
                    description.Append("<br />");
                    if (PrivateTrip.IsShowDepartment && options.Department)
                    {
                        description.Append("<strong>Dept: </strong>");
                        description.Append(appointment.DepartmentCD);
                        description.Append("     ");
                    }
                    //description.Append("<br />");
                    if (PrivateTrip.IsShowAuthorization && options.Authorization)
                    {
                        description.Append("<strong>Auth: </strong>");
                        description.Append(appointment.AuthorizationCD);
                        description.Append(" ");
                    }
                    // description.Append("<br />");
                    if (PrivateTrip.IsShowTripPurpose && options.TripPurpose)
                    {
                        description.Append("<br /><strong>Trip Purp.: </strong>");
                        description.Append(appointment.TripPurpose);
                        description.Append(" ");
                    }
                    //description.Append("<br />");
                    if (PrivateTrip.IsShowLegPurpose && options.LegPurpose)
                    {
                        description.Append("<br /><strong>Leg Purp.: </strong>");
                        description.Append(appointment.FlightPurpose);
                        description.Append(" ");
                    }
                    //description.Append("<br />");
                    if (PrivateTrip.IsShowSeatsAvailable && options.SeatsAvailable)
                    {
                        description.Append("<br /><strong>Seats Avail: </strong>");
                        description.Append(appointment.ReservationAvailable);
                        description.Append(" ");
                    }
                    //description.Append("<br />");

                    if (PrivateTrip.IsShowCrew && options.Crew)
                    {

                        description.Append("<br /><strong>Crew: </strong>");
                        if (!string.IsNullOrEmpty(appointment.CrewCD))
                        {
                            //description.Append(appointment.CrewCD);
                            description.Append(appointment.CrewCodes);
                        }
                        else
                        {
                            description.Append(appointment.CrewCodes);
                        }
                    }
                }
                //description.Append(" </td></tr></table>");
            }
            return description.ToString();
        }

        public string BuildWeeklyFleetAndCrewOnlyToolTipDescription(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, string viewMode, DisplayOptions displayOptions)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, viewMode, displayOptions))
            {
                var description = new StringBuilder();
                var options = displayOptions.BusinessWeek;
                description.Append("<br />");
                description.Append("<strong>Home Base: </strong>");
                description.Append(appointment.HomebaseCD);
                description.Append(" ");
                return description.ToString();
            }
        }

        public string BuildWeeklyFleetAndCrewOnlyToolTipDescriptionAppt(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, string viewMode, DisplayOptions displayOptions)
        {
            var description = new StringBuilder();
            // var groupedLegs = calendarData.Where(p => p.TripNUM == appointment.TripNUM).ToList();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, displayOptions))
            {
                var options = displayOptions.BusinessWeek;
                description.Append("<br />");
                description.Append("<strong>Home Base: </strong>");
                description.Append(appointment.HomebaseCD);
                description.Append("<br />");
            }

            return description.ToString();
        }

        //Navigation bar implementation

        //Departure and Arrival information for tooltip
        private void GetDepartureArrivalInfo(FlightPak.Web.Entities.SchedulingCalendar.Appointment appointment, DepartArriveInfo departureArrivalInfo, StringBuilder description)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(appointment, departureArrivalInfo, description))
            {
                if (appointment.ShowAllDetails)
                {
                    description.Append(appointment.DepartureDisplayTime.ToString("HH:mm"));
                    description.Append(" ");
                    switch (departureArrivalInfo)
                    {
                        case DepartArriveInfo.ICAO:

                            description.Append(appointment.DepartureICAOID);
                            description.Append(" | ");
                            description.Append(appointment.ArrivalICAOID != null ? appointment.ArrivalICAOID : string.Empty);

                            break;

                        case DepartArriveInfo.AirportName:

                            description.Append(appointment.DepartureAirportName);
                            description.Append(" | ");
                            description.Append(appointment.ArrivalAirportName != null ? appointment.ArrivalAirportName : string.Empty);

                            break;

                        case DepartArriveInfo.City:

                            description.Append(appointment.DepartureCity);
                            description.Append(" | ");
                            description.Append(appointment.ArrivalCity != null ? appointment.ArrivalCity : string.Empty);

                            break;

                    }
                    description.Append(" ");
                    description.Append(appointment.ArrivalDisplayTime.ToString("HH:mm"));
                }
                else // show as trip privacy settings from company profile
                {
                    if (PrivateTrip.IsShowArrivalDepartTime)
                    {
                        description.Append(appointment.DepartureDisplayTime.ToString("HH:mm"));
                        description.Append(" ");
                    }

                    if (PrivateTrip.IsShowArrivalDepartICAO)
                    {
                        switch (departureArrivalInfo)
                        {
                            case DepartArriveInfo.ICAO:

                                description.Append(appointment.DepartureICAOID);
                                description.Append(" | ");
                                description.Append(appointment.ArrivalICAOID != null ? appointment.ArrivalICAOID : string.Empty);

                                break;

                            case DepartArriveInfo.AirportName:

                                description.Append(appointment.DepartureAirportName);
                                description.Append(" | ");
                                description.Append(appointment.ArrivalAirportName != null ? appointment.ArrivalAirportName : string.Empty);

                                break;

                            case DepartArriveInfo.City:

                                description.Append(appointment.DepartureCity);
                                description.Append(" | ");
                                description.Append(appointment.ArrivalCity != null ? appointment.ArrivalCity : string.Empty);

                                break;

                        }
                    }

                    if (PrivateTrip.IsShowArrivalDepartTime)
                    {
                        if (PrivateTrip.IsShowArrivalDepartICAO)
                        {
                            description.Append(" ");
                        }
                        else
                        {
                            description.Append(" | ");

                        }

                        description.Append(appointment.ArrivalDisplayTime.ToString("HH:mm"));
                    }
                }
            }
        }

        protected void RadBusinessWeekScheduler_OnAppointmentContextMenuItemClicked(object sender, Telerik.Web.UI.AppointmentContextMenuItemClickedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session.Remove("CurrentPreFlightTrip");
                        if (e.MenuItem.Value == string.Empty)
                        {
                            return;
                        }

                        var appointment = (Telerik.Web.UI.Appointment)e.Appointment;
                        var appointmentNumber = appointment.ID;

                        //get trip by tripNumber
                        Int64 tripId = Convert.ToInt64(appointmentNumber);
                        if (tripId != 0)
                        {

                            using (var preflightServiceClient = new PreflightServiceClient())
                            {
                                var tripInfo = preflightServiceClient.GetTrip(tripId);
                                var trip = tripInfo.EntityList[0];
                                if (tripInfo.EntityList != null && tripInfo.EntityList.Count > 0 && tripInfo.EntityList[0] != null)
                                {
                                    //Set the trip.Mode and Trip.State to NoChage for read only                    
                                    trip.Mode = TripActionMode.Edit;
                                    Session["CurrentPreFlightTrip"] = trip;
                                    Session[WebSessionKeys.CurrentPreflightTripMain] = trip;
                                    PreflightTripManager.populateCurrentPreflightTrip(trip);

                                    if (e.MenuItem.Text == "Add Fleet Calendar Entry" || e.MenuItem.Text == "Fleet Calendar Entry")
                                    {// Fleet calendar entry
                                        if (tripInfo.EntityList != null && tripInfo.EntityList.Count > 0)
                                        {
                                            string tailNumber = trip.Fleet != null ? trip.Fleet.TailNum : null;
                                            RadWindow3.NavigateUrl = e.MenuItem.Value + "?tailNum=" + tailNumber + "&startDate=" + appointment.Start + "&endDate=" + appointment.Start; // no need to pass tailNum since it has no resource
                                        }
                                        else
                                            RadWindow3.NavigateUrl = e.MenuItem.Value + "?tailNum=" + null + "&startDate=" + appointment.Start + "&endDate=" + appointment.Start; // no need to pass tailNum since it has no resource
                                        RadWindow3.Visible = true;
                                        RadWindow3.VisibleOnPageLoad = true;
                                        return;
                                    }
                                }
                                if (e.MenuItem.Text == "Outbound Instruction") // outbound instruction menu selected...
                                {
                                    trip.Mode = TripActionMode.Edit;
                                    trip.State= TripEntityState.Modified;
                                    RadWindow5.NavigateUrl = e.MenuItem.Value + "?IsCalender=1";
                                    RadWindow5.Visible = true;
                                    RadWindow5.VisibleOnPageLoad = true;
                                    RadWindow5.Title = string.Format("Outbound Instructions - Trip Number: {0} And Tail Number: {1}", Convert.ToString(trip.TripNUM), Convert.ToString(trip.Fleet.TailNum));
                                    return;
                                }
                                else if (e.MenuItem.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                                {
                                    Response.Redirect("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml" + "&tripnum=" + Microsoft.Security.Application.Encoder.HtmlEncode(Convert.ToString(trip.TripNUM)) + "&tripid=" + Microsoft.Security.Application.Encoder.HtmlEncode(Convert.ToString(trip.TripID)));
                                }
                                else if (e.MenuItem.Text == "Leg Notes") // Leg Notes menu selected...
                                {
                                    trip.Mode = TripActionMode.Edit;
                                    trip.State = TripEntityState.Modified;
                                    RadWindow6.NavigateUrl = e.MenuItem.Value + "?IsCalender=1&Leg=" + radSelectedLegNum.Value;
                                    RadWindow6.Visible = true;
                                    RadWindow6.VisibleOnPageLoad = true;
                                    return;
                                }
                                else if (e.MenuItem.Text == "Add Crew Calendar Entry" || e.MenuItem.Text == "Crew Calendar Entry")
                                { // Crew calendar entry
                                    RadWindow4.NavigateUrl = e.MenuItem.Value + "?crewCode=" + null + "&startDate=" + appointment.Start + "&endDate=" + appointment.Start;// no need to pass crewCode since it has no resource
                                    RadWindow4.Visible = true;
                                    RadWindow4.VisibleOnPageLoad = true;
                                    return;
                                }
                                else if (e.MenuItem.Text == "Add New Trip")
                                // for new trip, departDate, REquestorDate, aircraft code, tailnum all those details to be passed on...
                                {
                                    //Set the trip.Mode and Trip.State to NoChage for read only                    
                                    trip.Mode = TripActionMode.NoChange;
                                    trip.Mode = TripActionMode.Edit;
                                    PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                                    PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                                    Trip.EstDepartureDT = PreflightTripManagerLite.GetUserHomebaseLocalDateTime(Convert.ToDateTime(radSelectedStartDate.Value)); //todo: get exact column date
                                    /// 12/11/2012
                                    ///Changes done for displaying the homebase of Fleet in preflight 
                                    if (trip.Fleet != null)
                                    {
                                        PreflightService.Company HomebaseSample = new PreflightService.Company();
                                        Int64 HomebaseAirportId = 0;
                                        fleetsample.TailNum = trip.Fleet.TailNum;
                                        fleetsample.FleetID = trip.Fleet.FleetID;
                                        Trip.FleetID = fleetsample.FleetID;
                                        if (fleetsample.FleetID != null)
                                        {
                                            Int64 fleetHomebaseId = 0;
                                            Int64 fleetId = Convert.ToInt64(fleetsample.FleetID);
                                            using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                var FleetList = FPKMasterService.GetFleetIDInfo(fleetId).EntityList.ToList();
                                                if (FleetList.Count > 0)
                                                {
                                                    GetFleetIDInfo FleetCatalogEntity = FleetList[0];
                                                    if (FleetCatalogEntity.HomebaseID != null)
                                                    {
                                                        fleetHomebaseId = Convert.ToInt64(FleetCatalogEntity.HomebaseID);
                                                        fleetsample.TypeDescription = FleetCatalogEntity.AircraftDescription;
                                                    }
                                                }
                                                var CompanyList = FPKMasterService.GetListInfoByHomeBaseId(fleetHomebaseId).EntityList.ToList();
                                                if (CompanyList.Count > 0)
                                                {
                                                    GetAllCompanyMasterbyHomeBaseId CompanyCatalogEntity = CompanyList[0];
                                                    if (CompanyCatalogEntity.HomebaseAirportID != null)
                                                    {
                                                        HomebaseAirportId = Convert.ToInt64(CompanyCatalogEntity.HomebaseAirportID);
                                                        HomebaseSample.BaseDescription = CompanyCatalogEntity.BaseDescription;
                                                    }
                                                }
                                                var AirportList = FPKMasterService.GetAirportByAirportIDFBO(HomebaseAirportId).EntityList.ToList();
                                                if (AirportList.Count > 0)
                                                {
                                                    GetAirportByAirportIDFBO AirportCatalogEntity = AirportList[0];
                                                    if (AirportCatalogEntity.IcaoID != null)
                                                    {
                                                        Trip.HomeBaseAirportICAOID = AirportCatalogEntity.IcaoID.Trim();

                                                    }
                                                    if (AirportCatalogEntity.AirportID != null)
                                                    {
                                                        Trip.HomeBaseAirportID = AirportCatalogEntity.AirportID;
                                                    }
                                                }

                                            }
                                            Trip.HomebaseID = fleetHomebaseId;
                                            Trip.Company = HomebaseSample;
                                        }
                                    }
                                    Trip.Fleet = fleetsample;

                                    PreflightService.Aircraft aircraftSample = new PreflightService.Aircraft();

                                    if (trip.Fleet.Aircraft != null)
                                    {
                                        aircraftSample.AircraftID = trip.Fleet.AircraftID.Value;
                                        aircraftSample.AircraftCD = trip.Fleet.AircraftCD;
                                        aircraftSample.AircraftDescription = trip.Fleet.Aircraft.AircraftDescription;
                                        Trip.AircraftID = aircraftSample.AircraftID;
                                    }
                                    Trip.Aircraft = aircraftSample;


                                    if (HomeBaseId != null && HomeBaseId != 0 && Trip.HomebaseID <= 0)
                                    {
                                        /// 12/11/2012
                                        ///Changes done for displaying the homebase of login user
                                        PreflightService.Company HomebaseSample = new PreflightService.Company();
                                        HomebaseSample.HomebaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                        HomebaseSample.BaseDescription = BaseDescription;
                                        Trip.HomeBaseAirportICAOID = HomeBaseCode;
                                        Trip.HomeBaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                        Trip.HomebaseID = HomeBaseId;
                                        Trip.Company = HomebaseSample;
                                    }
                                    if (ClientId != null && ClientId != 0)
                                    {
                                        Trip.ClientID = ClientId;
                                        PreflightService.Client Client = new PreflightService.Client();
                                        Client.ClientCD = ClientCode;
                                        Client.ClientID = ClientId.HasValue ? ClientId.Value : 0;
                                        Trip.Client = Client;
                                    }

                                    Session["CurrentPreFlightTrip"] = Trip;
                                    Session.Remove("PreflightException");
                                    Trip.State = TripEntityState.Added;
                                    Trip.Mode = TripActionMode.Edit;
                                    Response.Redirect(e.MenuItem.Value);

                                    /*if (trip.TripNUM != null && trip.TripNUM != 0)
                                    {
                                        Session["CurrentPreFlightTrip"] = Trip;
                                        Trip.State = TripEntityState.Added;
                                        Trip.Mode = TripActionMode.Edit;
                                        Response.Redirect(e.MenuItem.Value);
                                    }
                                    else
                                    {                                        
                                        Session["CurrentPreFlightTrip"] = null;
                                        Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                                    }*/                                    
                                }

                                else if (e.MenuItem.Text == "Add New Trip")
                                // for new trip, departDate, REquestorDate, aircraft code, tailnum all those details to be passed on...                       
                                {
                                    PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                                    PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                                    Trip.EstDepartureDT = PreflightTripManagerLite.GetUserHomebaseLocalDateTime(Convert.ToDateTime(radSelectedStartDate.Value)); 
                                    if (tripInfo.EntityList != null && tripInfo.EntityList.Count > 0 && tripInfo.EntityList[0] != null)
                                    {
                                        if (trip.Fleet != null)
                                        {
                                            PreflightService.Company HomebaseSample = new PreflightService.Company();
                                            Int64 HomebaseAirportId = 0;
                                            fleetsample.TailNum = trip.Fleet.TailNum;
                                            fleetsample.FleetID = trip.Fleet.FleetID;
                                            Trip.FleetID = fleetsample.FleetID;
                                            if (fleetsample.FleetID != null)
                                            {
                                                Int64 fleetHomebaseId = 0;
                                                Int64 fleetId = Convert.ToInt64(fleetsample.FleetID);
                                                using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    var FleetList = FPKMasterService.GetFleetIDInfo(fleetId).EntityList.ToList();
                                                    if (FleetList.Count > 0)
                                                    {
                                                        GetFleetIDInfo FleetCatalogEntity = FleetList[0];
                                                        if (FleetCatalogEntity.HomebaseID != null)
                                                        {
                                                            fleetHomebaseId = Convert.ToInt64(FleetCatalogEntity.HomebaseID);
                                                            fleetsample.TypeDescription = FleetCatalogEntity.AircraftDescription;
                                                        }
                                                    }
                                                    var CompanyList = FPKMasterService.GetListInfoByHomeBaseId(fleetHomebaseId).EntityList.ToList();
                                                    if (CompanyList.Count > 0)
                                                    {
                                                        GetAllCompanyMasterbyHomeBaseId CompanyCatalogEntity = CompanyList[0];
                                                        if (CompanyCatalogEntity.HomebaseAirportID != null)
                                                        {
                                                            HomebaseAirportId = Convert.ToInt64(CompanyCatalogEntity.HomebaseAirportID);
                                                            HomebaseSample.BaseDescription = CompanyCatalogEntity.BaseDescription;
                                                        }
                                                    }
                                                    var AirportList = FPKMasterService.GetAirportByAirportIDFBO(HomebaseAirportId).EntityList.ToList();
                                                    if (AirportList.Count > 0)
                                                    {
                                                        GetAirportByAirportIDFBO AirportCatalogEntity = AirportList[0];
                                                        if (AirportCatalogEntity.IcaoID != null)
                                                        {
                                                            Trip.HomeBaseAirportICAOID = AirportCatalogEntity.IcaoID.Trim();

                                                        }
                                                        if (AirportCatalogEntity.AirportID != null)
                                                        {
                                                            Trip.HomeBaseAirportID = AirportCatalogEntity.AirportID;
                                                        }
                                                    }

                                                }
                                                Trip.HomebaseID = fleetHomebaseId;
                                                Trip.Company = HomebaseSample;
                                            }
                                        }
                                        Trip.Fleet = fleetsample;

                                        PreflightService.Aircraft aircraftSample = new PreflightService.Aircraft();

                                        if (trip.Fleet.Aircraft != null)
                                        {
                                            aircraftSample.AircraftID = trip.Fleet.AircraftID.Value;
                                            aircraftSample.AircraftCD = trip.Fleet.AircraftCD;
                                            aircraftSample.AircraftDescription = trip.Fleet.Aircraft.AircraftDescription;
                                            Trip.AircraftID = aircraftSample.AircraftID;
                                        }
                                        Trip.Aircraft = aircraftSample;
                                    }
                                }
                                if (HomeBaseId != null && HomeBaseId != 0)
                                {
                                    PreflightService.Company HomebaseSample = new PreflightService.Company();
                                    HomebaseSample.HomebaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                    HomebaseSample.BaseDescription = BaseDescription;
                                    trip.HomeBaseAirportICAOID = HomeBaseCode;
                                    trip.HomeBaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                    trip.HomebaseID = HomeBaseId;
                                    trip.Company = HomebaseSample;
                                }
                                if (ClientId != null && ClientId != 0)
                                {
                                    trip.ClientID = ClientId;
                                    PreflightService.Client Client = new PreflightService.Client();
                                    Client.ClientCD = ClientCode;
                                    Client.ClientID = ClientId.HasValue ? ClientId.Value : 0;
                                    trip.Client = Client;
                                }

                                Session["CurrentPreFlightTrip"] = trip;
                                Session.Remove("PreflightException");
                                trip.State = TripEntityState.Added;
                                trip.Mode = TripActionMode.Edit;
                                Response.Redirect(e.MenuItem.Value);

                                /*if (trip.TripNUM != null && trip.TripNUM != 0)
                                {
                                    Session["CurrentPreFlightTrip"] = trip;
                                    trip.State = TripEntityState.Added;
                                    trip.Mode = TripActionMode.Edit;
                                    Response.Redirect(e.MenuItem.Value);
                                }
                                else
                                {
                                    Session["CurrentPreFlightTrip"] = null;
                                    Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                                }*/
                            }
                        }
                        else if (e.MenuItem.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                        {
                            var tripnum = string.Empty;
                            Response.Redirect("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml" + "&tripnum=" + tripnum);
                        }
                        else
                        {
                            Response.Redirect("../PreflightMain.aspx?seltab=PreFlight");
                        }


                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.BusinessWeek);
                }
            }
        }
     
        protected void RadBusinessWeekScheduler_OnTimeSlotContextMenuItemClicked(object sender, Telerik.Web.UI.TimeSlotContextMenuItemClickedEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        Session.Remove("CurrentPreFlightTrip");
                        if (e.MenuItem.Text == "Add Fleet Calendar Entry" || e.MenuItem.Text == "Fleet Calendar Entry")
                        {// Fleet calendar entry

                            RadWindow3.NavigateUrl = e.MenuItem.Value + "?tailNum=" + null + "&startDate=" + e.TimeSlot.Start + "&endDate=" + e.TimeSlot.Start; // no need to pass tailNum since it has no resource
                            RadWindow3.Visible = true;
                            RadWindow3.VisibleOnPageLoad = true;
                            return;
                        }
                        else if (e.MenuItem.Text == "Add Crew Calendar Entry" || e.MenuItem.Text == "Crew Calendar Entry")
                        { // Crew calendar entry
                            RadWindow4.NavigateUrl = e.MenuItem.Value + "?crewCode=" + null + "&startDate=" + e.TimeSlot.Start + "&endDate=" + e.TimeSlot.Start;// no need to pass crewCode since it has no resource
                            RadWindow4.Visible = true;
                            RadWindow4.VisibleOnPageLoad = true;
                            return;
                        }
                        else if (e.MenuItem.Text == "Tripsheet Report Writer") // Leg Notes menu selected... 
                        {
                            var tripnum = string.Empty;
                            Response.Redirect("~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PRETSOverview.xml" + "&tripnum=" + tripnum);
                        }
                        if (e.MenuItem.Value == "../PreFlightMain.aspx") // for new trip, departDate, REquestorDate, aircraft code, tailnum all those details to be passed on...
                        {
                            PreflightService.PreflightMain Trip = new PreflightService.PreflightMain();
                            PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                            Trip.EstDepartureDT = PreflightTripManagerLite.GetUserHomebaseLocalDateTime(e.TimeSlot.Start);

                            if (HomeBaseId != null && HomeBaseId != 0)
                            {
                                /// 12/11/2012
                                ///Changes done for displaying the homebase of  login user
                                PreflightService.Company HomebaseSample = new PreflightService.Company();
                                HomebaseSample.HomebaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                HomebaseSample.BaseDescription = BaseDescription;
                                Trip.HomeBaseAirportICAOID = HomeBaseCode;
                                Trip.HomeBaseAirportID = HomeAirportId.HasValue ? HomeAirportId.Value : 0;
                                Trip.HomebaseID = HomeBaseId;
                                Trip.Company = HomebaseSample;
                            }
                            if (ClientId != null && ClientId != 0)
                            {
                                Trip.ClientID = ClientId;
                                PreflightService.Client Client = new PreflightService.Client();
                                Client.ClientCD = ClientCode;
                                Client.ClientID = ClientId.HasValue ? ClientId.Value : 0;
                                Trip.Client = Client;
                            }

                            if (e.MenuItem.Text == "Add New Trip")
                            {
                                Session["CurrentPreFlightTrip"] = Trip;
                                Session.Remove("PreflightException");
                                Trip.State = TripEntityState.Added;
                                Trip.Mode = TripActionMode.Edit;
                            }
                            else if (Trip.TripNUM != null && Trip.TripNUM != 0)
                            {
                                Session["CurrentPreFlightTrip"] = Trip;
                                Trip.State = TripEntityState.Added;
                                Trip.Mode = TripActionMode.Edit;                                
                            }
                            else                            
                            {
                                Session["CurrentPreFlightTrip"] = null;
                                Session[WebSessionKeys.CurrentPreflightTrip] = null;
                            }
                                                            
                            Response.Redirect(e.MenuItem.Value);
                        }
                        else
                        {
                            Response.Redirect(e.MenuItem.Value);
                        }
                        // Response.Redirect(e.MenuItem.Value);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.BusinessWeek);
                }
            }
        }
        //Navigation bar implementation
        protected void prevButton_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCBusinessWeekPreviousCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(-1);
                            EndDate = GetBusinessWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            Session["SCSelectedDay"] = StartDate;
                            //RadDatePicker1.SelectedDate = StartDate;
                            RadBusinessWeekScheduler.SelectedDate = StartDate;
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            loadTree(false);
                        }
                        else
                        {
                            DateTime sessionpreviousDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionpreviousDay.AddDays(-1);
                            EndDate = GetBusinessWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            //RadDatePicker1.SelectedDate = StartDate;
                            RadBusinessWeekScheduler.SelectedDate = StartDate;
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }
                        Session["SCBusinessWeekPreviousCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.BusinessWeek);
                }
            }
        }

        protected void nextButton_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCBusinessWeekNextCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(1);
                            EndDate = GetBusinessWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            RadBusinessWeekScheduler.SelectedDate = StartDate;
                            //RadDatePicker1.SelectedDate = StartDate;
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);

                        }
                        else
                        {
                            DateTime sessionnextDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionnextDay.AddDays(1);
                            EndDate = GetBusinessWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            //RadDatePicker1.SelectedDate = StartDate;
                            RadBusinessWeekScheduler.SelectedDate = StartDate;
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }
                        Session["SCBusinessWeekNextCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.BusinessWeek);
                }
            }
        }

        protected void btnLast_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCBusinessWeekLastCount"]) == 0)
                        {
                            // DateTime today = DateTime.Today;
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            StartDate = selectedDate.AddDays(15);
                            EndDate = GetBusinessWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            //RadDatePicker1.SelectedDate = StartDate;
                            RadBusinessWeekScheduler.SelectedDate = StartDate;
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);

                        }
                        else
                        {
                            DateTime sessionnextDay = (DateTime)Session["SCSelectedDay"];
                            StartDate = sessionnextDay.AddDays(15);
                            EndDate = GetBusinessWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            //RadDatePicker1.SelectedDate = StartDate;
                            RadBusinessWeekScheduler.SelectedDate = StartDate;
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }

                        Session["SCBusinessWeekLastCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.BusinessWeek);
                }
            }
        }

        protected void btnFirst_click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        int count = 0;

                        if (Convert.ToInt32(Session["SCBusinessWeekFirstCount"]) == 0)
                        {
                            DateTime selectedDate;
                            if (Session["SCSelectedDay"] == null)
                            {
                                selectedDate = DateTime.Today;
                            }
                            else
                            {
                                selectedDate = (DateTime)Session["SCSelectedDay"];
                            }
                            //DateTime today = DateTime.Today;
                            StartDate = selectedDate.AddDays(-15);
                            EndDate = GetBusinessWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            //RadDatePicker1.SelectedDate = StartDate;
                            RadBusinessWeekScheduler.SelectedDate = StartDate;

                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }
                        else
                        {
                            DateTime selectedDate = (DateTime)Session["SCSelectedDay"];
                            StartDate = selectedDate.AddDays(-15);
                            EndDate = GetBusinessWeekEndDate(StartDate);
                            lbStartDate.Text = System.Web.HttpUtility.HtmlEncode(StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            lbEndDate.Text = System.Web.HttpUtility.HtmlEncode(EndDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture));
                            //RadDatePicker1.SelectedDate = StartDate;
                            RadBusinessWeekScheduler.SelectedDate = StartDate;
                            tbDate.Text = StartDate.ToString(ApplicationDateFormat, CultureInfo.InvariantCulture);
                            Session["SCSelectedDay"] = StartDate;
                            loadTree(false);
                        }
                        Session["SCBusinessWeekFirstCount"] = ++count;
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.BusinessWeek);
                }
            }
        }

        protected void ChkDefaultView_OnCheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (var commonServiceClient = new CommonServiceClient())
                        {
                            if (ChkDefaultView.Checked)
                            {
                                commonServiceClient.UpdateCalendarDefaultView(ModuleNameConstants.Preflight.BusinessWeek);
                            }
                            else
                            {
                                commonServiceClient.UpdateCalendarDefaultView(null);
                            }
                        }
                        loadTree(false);
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.BusinessWeek);
                }
            }
        }
    }
}
